<?php
use Psi\Eos\Entrata\CPropertyMediaCategories;
use Psi\Eos\Entrata\CPropertyMedias;
use Psi\Eos\Entrata\CCompanyMediaFiles;
use Psi\Eos\Entrata\CWebsites;
use Psi\Eos\Entrata\CMaintenancePriorities;
use Psi\Eos\Entrata\CMaintenanceLocations;
use Psi\Eos\Entrata\CPropertySmsMessages;
use Psi\Eos\Entrata\CUnitTypes;
use Psi\Eos\Entrata\CAddOns;
use Psi\Eos\Entrata\CSpecials;
use Psi\Eos\Entrata\CAmenities;
use Psi\Eos\Entrata\CMaintenanceStatuses;
use Psi\Eos\Entrata\CMaintenanceProblems;
use Psi\Eos\Entrata\CPropertyMaintenanceLocations;
use Psi\Eos\Entrata\CPetTypes;
use Psi\Eos\Entrata\CPropertyFloorplans;
use Psi\Eos\Entrata\CAddOnCategories;
use Psi\Eos\Entrata\CPropertyMaintenanceTemplates;
use Psi\Eos\Entrata\CMaintenanceRequests;
use Psi\Eos\Entrata\CMaintenanceAvailabilityExceptions;
use Psi\Eos\Entrata\CPropertyBuildings;
use Psi\Eos\Entrata\CPropertyFloors;
use Psi\Eos\Entrata\CAddOnGroups;
use Psi\Eos\Entrata\CPropertyMaintenancePriorities;
use Psi\Eos\Entrata\CPropertyMaintenanceStatuses;
use Psi\Eos\Entrata\CPropertyMaintenanceProblems;
use Psi\Eos\Entrata\CPropertyDetails;
use Psi\Eos\Entrata\CPropertyUnits;
use Psi\Eos\Entrata\CCompanyEmployees;
use Psi\Eos\Entrata\Custom\CAddOnLeaseAssociations;
use Psi\Eos\Entrata\CUnitSpaces;
use Psi\Eos\Entrata\Custom\CAmenityRateAssociations;
use Psi\Eos\Entrata\Custom\CPetRateAssociations;
use Psi\Eos\Entrata\CPropertyAddresses;
use Psi\Eos\Entrata\CPropertyNotifications;
use Psi\Eos\Entrata\CPropertyPreferences;
use Psi\Eos\Entrata\CCompanyEmployeeContacts;
use Psi\Eos\Entrata\CPropertyProducts;

use Psi\Libraries\UtilObjectModifiers\CObjectModifiers;

class CProperty extends CBaseProperty {

	const ID_BATMAN_5TH_3RD_TESTING					= 194943;
	const ID_CONVENTIONAL_VANILLA					= 497213;
	const ID_EAST_PARK								= 172775;
	const ACCOUNT_VERIFICATION_TYPE_STORED_BILLING	= 1;
	const ACCOUNT_VERIFICATION_TYPE_EVERY_PAYMENT	= 2;
	const WAIVE_PAYMENT_REQUIREMENT_FOR_ROOMATES 						= 1;
	const DIVIDE_PAYMENT_REQUIREMENT_BY_NUMBER_OF_RESPONSIBLE_ROOMATES 	= 2;
	const CHARGES_MUST_BE_PAID_FULL 									= 1;
	const CURRENT_BALANCE_MUST_BE_PAID_IN_FULL							= 3;
	const VIEW_COMPANY_USERS_PROPERTIES		= 'view_company_users_properties';
	const LOOKUP_TYPE_DEFAULT_PROPERTY		= 'default_property';
	const LOOKUP_TYPE_BULK_INVOICE_VENDOR	= 'bulk_invoice_vendor';

	const IN_PROGRESS	= 'In Progress';
	const COMPLETED		= 'Completed';

	const TAX_ID         = 'tax_id';
	const ALTERNATE_ID   = 'alternate_id';
	const ALL_PROPERTIES = 'all properties';
	protected $m_fltMinDeposit;
	protected $m_fltMaxDeposit;
	protected $m_fltMiles;
	protected $m_fltLongitude;
	protected $m_fltLatitude;
	protected $m_fltTotalScore;

	protected $m_boolHasRemainingBudget;
	protected $m_boolIsInSemKeyword;
	protected $m_boolIsFeatured;
	protected $m_boolHasPublishedOnlineApplication;
	protected $m_boolIsValidRentArCodeId;
	protected $m_boolIsArMigrationMode;
	protected $m_boolIsBulkAddProperty;
	protected $m_boolIsApMigrationMode;

	protected $m_intRowCount;
	protected $m_intAvgSquareFeet;
	protected $m_intPropertyConciergeServicesCount;
	protected $m_intPropertyFloorplansCount;
	protected $m_intPropertyFloorsCount;
	protected $m_intPropertyBuildingsCount;
	protected $m_intPropertyUnitsCount;
	protected $m_intPropertyApplicationsCount;
	protected $m_intPropertyPhotosCount;
	protected $m_intPropertyVirtualToursCount;
	protected $m_intPropertyWallCategoriesCount;
	protected $m_intPropertyClassifiedCategoriesCount;
	protected $m_intPropertyUnitTypesCount;
	protected $m_intPropertyLeadSourcesCount;
	protected $m_intCommunityAmenitiesCount;
	protected $m_intApartmentAmenitiesCount;
	protected $m_intPropertyMaintenanceLocationsCount;
	protected $m_intPropertyMaintenanceProblemsCount;
	protected $m_intPropertyMaintenanceTemplateId;
	protected $m_intIntegrationClientTypeId;
	protected $m_intPropertyNotificationEmailsCount;
	protected $m_intUnitSpacesCount;
	protected $m_intPropertyApplicationCount;
	protected $m_intCompletedApplicationsCount;
	protected $m_intGuestCardCount;
	protected $m_intApplicationCount;
	protected $m_intPropertyEventResultsCount;
	protected $m_intPropertyLeasingAgentsCount;
	protected $m_intScheduledChargePostDurationDays;
	protected $m_intClientStatusTypeId;
	protected $m_intSalesTaxResultTypeId;
	protected $m_intDatabaseId;
	protected $m_intSemPropertyDetailId;
	protected $m_intSemPropertyStatusTypeId;
	protected $m_intHideMoreInfoButton;
	protected $m_intShowApplicationFeeOption;
	protected $m_intIsPropertyUnitsAvailable;
	protected $m_boolIsLimited;
	protected $m_intAllowAllocations;
	protected $m_intPropertyGroupId;
	protected $m_intGlTreeId;
	protected $m_intInternetListingServiceId;
	protected $m_boolAllowMultipleAccountProperties;
	protected $m_intParentPropertyId;
	protected $m_intAssociatedAccountId;
	protected $m_intOccupancyTypeId;
	protected $m_intBankAccountTypeId;
	protected $m_intReimbursedPropertyId;
	protected $m_intIsAllocation;

	protected $m_arrintChildPropertyIds;
	protected $m_arrintClosedDays;
	protected $m_arrintLeaseTermOptions;
	protected $m_arrintInternetListingServiceIds;

	protected $m_arrstrSupportedLocaleCodes;

	protected $m_arrobjPropertyFloorplans;
	protected $m_arrobjReviews;
	protected $m_arrobjSpecials;
	protected $m_arrobjCompanyMediaFiles;
	protected $m_arrobjPropertyPhoneNumbers;
	protected $m_arrobjPropertyBuildings;
	protected $m_arrobjApplications;
	protected $m_arrobjLeases;
	protected $m_arrobjRateRanges;
	protected $m_arrobjPropertyAddresses;
	protected $m_arrobjWebsites;
	protected $m_arrobjPropertyPreferences;
	protected $m_arrobjWebsitePreferences;
	protected $m_arrobjPropertyAreas;
	protected $m_arrobjCareers;
	protected $m_arrobjCommunityAmenities;
	protected $m_arrobjApartmentAmenities;
	protected $m_arrobjPropertyCraigsListAreas;
	protected $m_arrobjSemCommunityAmenities;
	protected $m_arrobjChildProperties;
	protected $m_arrobjPropertyWebsitePreferences;
	protected $m_arrobjLeadSources;
	protected $m_arrobjLeaseTerms;
	protected $m_arrobjPropertyOfficeHours;
	protected $m_arrobjPetRateAssociations;
	protected $m_arrobjCompanyPricings;
	protected $m_arrobjCompanyEmployees;
	protected $m_arrobjDocuments;
	protected $m_arrobjEventResults;
	protected $m_arrobjPropertyDocuments;
	protected $m_arrobjMerchantAccounts;
	protected $m_arrobjGlAccountProperties;
	protected $m_arrobjPropertyPaymentTypes;

	protected $m_objPrimaryPropertyAddress;
	protected $m_objCompanyMediaFile;
	protected $m_objOfficeEmailAddress;
	protected $m_objOfficePhoneNumber;
	protected $m_objPrimaryPropertyPhoneNumber;
	protected $m_objLogoCompanyMediaFile;
	protected $m_objPortfolioLogoCompanyMediaFile;

	protected $m_objOverviewMarketingMediaAssociation;
	protected $m_objPropertyLogoMarketingMediaAssociation;
	protected $m_objPortfolioLogoMarketingMediaAssociation;

	protected $m_objPropertyType;
	protected $m_objPhotosCompanyMediaFiles;
	protected $m_objWebsite;
	protected $m_objEnabledDefaultWebsite;
	protected $m_objPropertyDetail;
	protected $m_objChildProperty;
	protected $m_objProperty;
	protected $m_objSemPropertyDetail;
	protected $m_objPropertyPhoneNumber;
	protected $m_objSemAndOrganicStat;
	protected $m_objSemBudget;
	protected $m_objSemSetting;
	protected $m_objPropertyGlSetting;
	protected $m_objTimeZone;
	protected $m_objApPayeeProperty;
	protected $m_objPropertyGroup;
	protected $m_objRentArCode;
	protected $m_objDepositArCode;
	protected $m_objMtmRentArCode;
	protected $m_objPropertyChargeSetting;
	protected $m_objLeaseTerm;
	protected $m_objVoipDatabase;

	protected $m_strClientName;
	protected $m_strProspectPortalPropertyAnchor;
	protected $m_strProspectPortalPropertyCaption;
	protected $m_strOnlineApplicationUrl;
	protected $m_strMoreInfoAction;
	protected $m_strLocalWebsiteDomain;
	protected $m_strWebsiteDomain;
	protected $m_strCorporateWebsiteUri;
	protected $m_strCommunityWebsiteUri;
	protected $m_strOfficePhoneNumber;
	protected $m_strSeoCity;
	protected $m_strSeoState;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strStreetLine3;
	protected $m_strCity;
	protected $m_strPostalCode;
	protected $m_strStateCode;
	protected $m_strLastExternalClickDatetime;
	protected $m_strMarketingMediaAssociationUrl;
	protected $m_strPrimaryEmailAddress;
	protected $m_strVacancyPhoneNumber;
	protected $m_strPortfolioName;
	protected $m_strPropertyTypeName;
	protected $m_strCompanyRegionName;
	protected $m_strResidentPortalUrl;
	protected $m_strStateName;
	protected $m_strProvince;
	protected $m_strGlTreeName;
	protected $m_strGlPostMonth;
	protected $m_strResidentInsureSubDomain;
	protected $m_strResidentInsurePhoneNumber;
	protected $m_strPropertyManagerName;
	protected $m_strPrimaryPhoneNumber;
	protected $m_strOfficeFaxNumber;
	protected $m_strContractTerminationRequestId;
	protected $m_strContractTerminationRequestDate;
	protected $m_strContractPropertyTerminationDate;
	protected $m_strMarketingPropertyName;
	protected $m_strPropertyPreferenceKey;
	protected $m_strPropertyPreferenceValue;
	protected $m_strSystemMessageEmailContent;
	protected $m_strApPostMonth;
	protected $m_intPropertyStatus;

	protected $m_arrmixRequiredParametes;

	public function __construct() {
		parent::__construct();

		$this->m_arrobjPropertyPhoneNumbers			= [];
		$this->m_arrobjPropertyBuildings			= [];
		$this->m_arrobjCompanyMediaFiles			= [];
		$this->m_arrobjPropertyAddresses			= [];
		$this->m_arrobjWebsites						= [];
		$this->m_arrobjPropertyFloorplans			= [];
		$this->m_arrobjPropertyCraigsListAreas		= [];
		$this->m_arrobjPropertyWebsitePreferences	= [];
		$this->m_arrobjApartmentAmenities			= [];
		$this->m_arrobjCommunityAmenities			= [];
		$this->m_arrobjPropertyUnits				= [];
		$this->m_arrobjReviews						= [];
		$this->m_arrobjCompanyEmployees				= [];

		$this->m_arrintChildPropertyIds = NULL;

		$this->m_intAllowAllocations = 0;

		$this->m_boolHasPublishedOnlineApplication	= false;
		$this->m_boolIsBulkAddProperty				= false;
	}

	public function isIntegrated( $objDatabase ) {
		// A property in integrated only if
		// # it is associated with integration database that having staus ACTIVE or ON_HOLD
		// # and that integration database is not type of MIGRATE ( Migration )

		$strSql = 'SELECT
						pid.property_id as property_id
					FROM
						property_integration_databases pid
						JOIN integration_databases id ON( pid.cid = id.cid AND pid.integration_database_id = id.id )
					WHERE
						pid.cid = ' . ( int ) $this->getCid() . '
						AND pid.property_id = ' . ( int ) $this->getId() . '
						AND id.integration_client_status_type_id != ' . CIntegrationClientStatusType::DISABLED . '
						AND id.integration_client_type_id NOT IN ( ' . implode( ',', CIntegrationClientType::$c_arrintMigrateIntegrationClientTypeIds ) . ',' . CIntegrationClientType::EMH . ',' . CIntegrationClientType::EMH_ONE_WAY . ' );';

		$arrintPropertyIds = fetchData( $strSql, $objDatabase );

		return ( true == isset ( $arrintPropertyIds[0] ) && 0 < ( int ) $arrintPropertyIds[0] );
	}

	public function isRevenueManagementEnabled( $objDatabase ) {

		$strSql = 'SELECT
						p.id
					FROM
						properties p
						LEFT JOIN (
								SELECT
									ptv.id, ptv.cid, ptv.property_id
								FROM
									property_transmission_vendors ptv
									JOIN company_transmission_vendors ctv ON ( ctv.cid = ptv.cid AND ctv.id = ptv.company_transmission_vendor_id AND ctv.transmission_type_id = ' . ( int ) CTransmissionType::REVENUE_MANAGEMENT . ' )
								WHERE
									ptv.cid = ' . $this->getCid() . '
									AND ptv.property_id = ' . $this->getId() . '
								LIMIT 1
						) AS ptv ON ( ptv.cid = p.cid AND ptv.property_id = p.id )
						LEFT JOIN (
								SELECT
									DISTINCT ON ( pp.property_id )
									pp.id,
									pp.cid,
									pp.property_id
								FROM
									property_products ppr
									JOIN property_preferences pp ON ( pp.cid = ppr.cid AND pp.property_id = ' . $this->getId() . ' AND pp.key = \'PRICING_IS_POSTING\' AND pp.value = \'1\' )
									JOIN property_preferences pp1 ON ( pp1.cid = ppr.cid AND pp1.property_id = ' . $this->getId() . ' AND pp1.key = \'PRICING_LEASES\' AND pp1.value <> \'None\' )
								WHERE
									ppr.cid = ' . $this->getCid() . '
									AND ppr.ps_product_id = ' . CPsProduct::PRICING . '
									AND ( ppr.property_id = pp.property_id OR ppr.property_id IS NULL )
								LIMIT 1
						) pp ON ( pp.cid = p.cid AND pp.property_id = p.id )
						LEFT JOIN (
								SELECT
									DISTINCT ON ( pid.property_id )
									pid.id,
									pid.cid,
									pid.property_id
								FROM
									property_integration_databases pid
									JOIN integration_databases idb ON ( idb.cid = pid.cid AND idb.id = pid.integration_database_id AND idb.integration_client_status_type_id = ' . CIntegrationClientStatusType::ACTIVE . ' )
									JOIN integration_clients ic ON ( ic.cid = idb.cid AND ic.integration_database_id = idb.id AND ic.integration_client_status_type_id = ' . CIntegrationClientStatusType::ACTIVE . ' AND ic.integration_service_id IN ( ' . CIntegrationService::RETRIEVE_UNIT_OPTIMIZED_PRICING . ', ' . CIntegrationService::RETRIEVE_UNITS_OPTIMIZED_PRICING . ' ) )
									JOIN property_preferences pp ON ( pp.cid = pid.cid AND pp.property_id = pid.property_id AND pp.key = \'OPTIMIZATION_PRICING_THROUGH_PM_SOFTWARE\' AND pp.value = \'1\' )
								WHERE
									pid.cid = ' . $this->getCid() . '
									AND pid.property_id = ' . $this->getId() . '
								LIMIT 1
						) AS idb ON ( idb.cid = p.cid AND idb.property_id = p.id )
					WHERE
						p.is_disabled = 0
						AND p.cid = ' . $this->getCid() . '
						AND p.id = ' . $this->getId() . '
						AND ( pp.id IS NOT NULL OR ptv.id IS NOT NULL OR idb.id IS NOT NULL )
					LIMIT 1;';

		$arrintPropertyId = fetchData( $strSql, $objDatabase );

		return ( true == isset ( $arrintPropertyId[0] ) && 0 < ( int ) $arrintPropertyId[0] );
	}

	/**
	 * Static Functions
	 *
	 */

	public static function formatSeoPropertyName( $strPropertyName ) {
		return trim( \Psi\CStringService::singleton()->strtolower( \Psi\CStringService::singleton()->preg_replace( [ '/[^a-zA-Z0-9\s_\-]+/', '/(\s|-|_)+/' ], [ '', '-' ], $strPropertyName ) ) );
	}

	/**
	 * Get or Fetch Functions
	 *
	 */

	public function getOrFetchMerchantAccounts( $objDatabase ) {

		if( false == valArr( $this->m_arrobjMerchantAccounts ) ) {
			$this->m_arrobjMerchantAccounts = $this->fetchMerchantAccounts( $objDatabase );
		}

		return $this->m_arrobjMerchantAccounts;
	}

	public function getOrFetchSingularWebsite( $objDatabase ) {
		if( false == isset( $this->m_objWebsite ) || false == valObj( $this->m_objWebsite, 'CWebsite' ) ) {
			$this->m_objWebsite = $this->fetchSingularWebsite( $objDatabase );
		}

		return $this->m_objWebsite;
	}

	public function getOrFetchPrimaryPropertyAddress( $objDatabase ) {
		if( false == isset( $this->m_objPrimaryPropertyAddress ) || false == valObj( $this->m_objPrimaryPropertyAddress, 'CPropertyAddress' ) ) {
			$this->m_objPrimaryPropertyAddress = $this->fetchPrimaryAddress( $objDatabase );
		}

		return $this->m_objPrimaryPropertyAddress;
	}

	public function getOrFetchPrimaryPropertyPhoneNumber( $objDatabase ) {

		if( false == isset( $this->m_objPrimaryPropertyPhoneNumber ) || false == valObj( $this->m_objPrimaryPropertyPhoneNumber, 'CPropertyPhoneNumber' ) ) {
			$this->m_objPrimaryPropertyPhoneNumber = $this->fetchPropertyPhoneNumberByPhoneNumberType( CPhoneNumberType::PRIMARY, $objDatabase );
		}

		return $this->m_objPrimaryPropertyPhoneNumber;
	}

	public function getOrFetchOfficePhoneNumber( $objDatabase ) {

		if( false == isset( $this->m_objOfficePhoneNumber ) || false == valObj( $this->m_objOfficePhoneNumber, 'CPropertyPhoneNumber' ) ) {
			$this->m_objOfficePhoneNumber = $this->fetchPropertyPhoneNumberByPhoneNumberType( CPhoneNumberType::OFFICE, $objDatabase );
		}

		return $this->m_objOfficePhoneNumber;
	}

	public function getOrFetchOverviewMarketingMediaAssociation( $objDatabase ) {

		if( false == isset( $this->m_objMarketingMediaAssociation ) || false == valObj( $this->m_objMarketingMediaAssociation, 'CMarketingMediaAssociation' ) ) {
			$this->m_objMarketingMediaAssociation = $this->fetchOverviewMarketingMediaAssociation( $objDatabase );
		}
		return $this->m_objMarketingMediaAssociation;
	}

	public function getOrFetchLogoMarketingMediaAssociation( $objDatabase ) {
		if( false == isset( $this->m_objMarketingMediaAssociation ) || false == valObj( $this->m_objMarketingMediaAssociation, 'CMarketingMediaAssociation' ) ) {
			$this->m_objLogoMarketingMediaAssociation = $this->fetchLogoMarketingMediaAssociation( $objDatabase );
		}

		return $this->m_objLogoMarketingMediaAssociation;
	}

	public function getOrFetchPropertyType( $objDatabase ) {
		if( false == isset( $this->m_objPropertyType ) || false == valObj( $this->m_objPropertyType, 'CPropertyType' ) ) {
			$this->m_objPropertyType = $this->fetchPropertyType( $objDatabase );
		}

		return $this->m_objPropertyType;
	}

	public function getOrFetchPropertyPhoneNumbers( $objDatabase ) {
		if( false == isset( $this->m_arrobjPropertyPhoneNumbers ) || false == valArr( $this->m_arrobjPropertyPhoneNumbers ) ) {
			$this->m_arrobjPropertyPhoneNumbers = $this->fetchPropertyPhoneNumbers( $objDatabase );
		}

		return $this->m_arrobjPropertyPhoneNumbers;
	}

	public function getOrFetchPropertyPreferences( $objDatabase ) {
		if( false == isset( $this->m_arrobjPropertyPreferences ) || false == valArr( $this->m_arrobjPropertyPreferences ) ) {
			$this->m_arrobjPropertyPreferences = $this->fetchPropertyPreferences( $objDatabase );
		}

		return $this->m_arrobjPropertyPreferences;
	}

	public function getOrFetchPublishedPropertyPhoneNumbersByPhoneNumberTypeIds( $arrintPhoneNumberTypeIds, $objDatabase ) {

		if( false == isset( $this->m_arrobjPropertyPhoneNumbers ) || false == valArr( $this->m_arrobjPropertyPhoneNumbers ) ) {
			$this->m_arrobjPropertyPhoneNumbers = $this->fetchPublishedPropertyPhoneNumbersByPhoneNumberTypeIds( $arrintPhoneNumberTypeIds, $objDatabase );
		}

		return $this->m_arrobjPropertyPhoneNumbers;
	}

	public function getOrFetchLeadSources( $objDatabase ) {

		if( false == isset( $this->m_arrobjLeadSources ) || false == valArr( $this->m_arrobjLeadSources ) ) {
			$this->m_arrobjLeadSources = $this->fetchLeadSources( $objDatabase );
		}

		return $this->m_arrobjLeadSources;
	}

	public function getOrFetchPublishedLeadSources( $objDatabase ) {

		if( false == isset( $this->m_arrobjLeadSources ) || false == valArr( $this->m_arrobjLeadSources ) ) {
			$this->m_arrobjLeadSources = $this->getChildProperty()->fetchPublishedLeadSources( $objDatabase );
		}

		return $this->m_arrobjLeadSources;
	}

	public function getOrFetchPropertyDetail( $objDatabase ) {

		if( false == isset( $this->m_objPropertyDetail ) || false == valObj( $this->m_objPropertyDetail, 'CPropertyDetail' ) ) {
			$this->m_objPropertyDetail = $this->fetchPropertyDetail( $objDatabase );
		}

		return $this->m_objPropertyDetail;
	}

	public function getOrFetchPropertyFloorplans( $objDatabase ) {
		if( false == valArr( $this->m_arrobjPropertyFloorplans ) ) {
			$this->m_arrobjPropertyFloorplans = $this->fetchPropertyFloorplans( $objDatabase );
		}

		return $this->m_arrobjPropertyFloorplans;
	}

	public function getOrfetchTimezone( $objDatabase ) {
		if( false == valObj( $this->m_objTimeZone, 'CTimeZone' ) ) {
			$this->m_objTimeZone = $this->fetchTimeZone( $objDatabase );
		}

		return $this->m_objTimeZone;
	}

	public function getPropertyPreferenceKey() {
		return $this->m_strPropertyPreferenceKey;
	}

	public function getPropertyPreferenceValue() {
		return $this->m_strPropertyPreferenceValue;
	}

	public function getOrFetchSeoCity( $objDatabase ) {
		if( true == is_null( $this->m_strSeoCity ) ) {
			$objPrimaryPropertyAddress = $this->getOrFetchPrimaryPropertyAddress( $objDatabase );
			if( false == is_null( $objPrimaryPropertyAddress ) && true == valObj( $objPrimaryPropertyAddress, 'CPropertyAddress' ) ) {
				$this->m_strSeoCity = $objPrimaryPropertyAddress->getSeoCity();
			}
		}

		return $this->m_strSeoCity;
	}

	public function getOrFetchSeoState( $objDatabase ) {
		if( true == is_null( $this->m_strSeoState ) ) {
			$objPrimaryPropertyAddress = $this->getOrFetchPrimaryPropertyAddress( $objDatabase );
			if( false == is_null( $objPrimaryPropertyAddress ) && true == valObj( $objPrimaryPropertyAddress, 'CPropertyAddress' ) ) {
				$this->m_strSeoState = $objPrimaryPropertyAddress->getSeoState();
			}
		}

		return $this->m_strSeoState;
	}

	public function getOrFetchChildPropertyIds( $objDatabase, $arrintRequiredProducts = [] ) {

		if( false == valArr( $this->m_arrintChildPropertyIds ) ) {
			$this->m_arrintChildPropertyIds = $this->fetchChildPropertyIds( $objDatabase, $arrintRequiredProducts );
		}

		return $this->m_arrintChildPropertyIds;
	}

	public function getOrFetchParentPropertyId( $objDatabase, $arrintRequiredProducts = [] ) {

		if( false == valId( $this->m_intParentPropertyId ) ) {
			$this->m_intParentPropertyId = $this->fetchParentPropertyId( $objDatabase, $arrintRequiredProducts );
		}

		return $this->m_intParentPropertyId;
	}

	public function getOrFetchChildPropertyById( $intChildPropertyId, $objDatabase ) {
		if( false == valObj( $this->m_objChildProperty, 'CProperty' ) || $intChildPropertyId != $this->m_objChildProperty->getId() ) {
			$this->m_objChildProperty = $this->fetchChildPropertyById( $intChildPropertyId, $objDatabase );
		}

		return $this->m_objChildProperty;
	}

	public function getOrFetchChildProperties( $objDatabase ) {
		if( false == valArr( $this->m_arrobjChildProperties ) ) {
			$this->m_arrobjChildProperties = $this->fetchChildProperties( $objDatabase );
		}

		return $this->m_arrobjChildProperties;
	}

	public function getOrFetchProperty( $objDatabase ) {
		if( ( true == is_null( $this->m_objProperty ) || false == valObj( $this->m_objProperty, 'CProperty' ) ) && false == is_null( $this->getPropertyId() ) ) {

			$this->m_objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		}

		return $this->m_objProperty;
	}

	public function getOrFetchLeaseTerms( $objDatabase ) {
		if( false == valArr( $this->m_arrobjLeaseTerms ) ) {
			$this->m_arrobjLeaseTerms = $this->fetchLeaseTerms( $objDatabase );
		}

		return $this->m_arrobjLeaseTerms;
	}

	public function getOrFetchSpecials( $objDatabase ) {
		if( false == valArr( $this->m_arrobjSpecials ) ) {
			$this->m_arrobjSpecials = $this->fetchSpecials( $objDatabase );
		}

		return $this->m_arrobjSpecials;
	}

	public function getOrFetchPropertyLeadSourcesCount( $objDatabase ) {
		if( false == is_numeric( $this->m_intPropertyLeadSourcesCount ) || 0 == $this->m_intPropertyLeadSourcesCount ) {

			$this->m_intPropertyLeadSourcesCount = \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPublishedPropertyLeadSourcesCountByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		}

		return $this->m_intPropertyLeadSourcesCount;
	}

	public function getOrFetchPropertyEventResultsCount( $objDatabase, $strCondition = NULL ) {
		if( false == is_numeric( $this->m_intPropertyEventResultsCount ) || 0 == $this->m_intPropertyEventResultsCount ) {
			$this->m_intPropertyEventResultsCount = \Psi\Eos\Entrata\CPropertyEventResults::createService()->fetchPropertyEventResultsCountByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $strCondition );
		}

		return $this->m_intPropertyEventResultsCount;
	}

	public function getOrFetchPropertyGlSetting( $objClientDatabase ) {

		if( false == valObj( $this->m_objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
			$this->m_objPropertyGlSetting = CPropertyGlSettings::fetchCustomPropertyGlSettingsByPropertyIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
		}

		return $this->m_objPropertyGlSetting;
	}

	public function getOrFetchPropertyChargeSetting( $objClientDatabase ) {

		if( false == valObj( $this->m_objPropertyChargeSetting, 'CPropertyChargeSetting' ) ) {
			$this->m_objPropertyChargeSetting = $this->fetchPropertyChargeSetting( $objClientDatabase );
		}

		return $this->m_objPropertyChargeSetting;
	}

	public function getOrFetchEnabledDefaultWebsite( $objClientDatabase ) {

		if( false == isset( $this->m_objEnabledDefaultWebsite ) || false == valObj( $this->m_objEnabledDefaultWebsite, 'CWebsite' ) ) {
			$this->m_objEnabledDefaultWebsite = $this->fetchEnabledDefaultWebsite( $objClientDatabase );
		}

		return $this->m_objEnabledDefaultWebsite;
	}

	public function getOrFetchPropertyDefaultWebsite( $objClientDatabase ) {

		if( false == isset( $this->m_objWebsite ) || false == valObj( $this->m_objWebsite, 'CWebsite' ) ) {
			$this->m_objWebsite = $this->fetchDefaultWebsite( $objClientDatabase );
		}

		return $this->m_objWebsite;
	}

	public function getOrFetchPropertyGroup( $objDatabase ) {
		if( false == valObj( $this->m_objPropertyGroup, 'CPropertyGroup' ) ) {
			$this->m_objPropertyGroup = \Psi\Eos\Entrata\CPropertyGroups::createService()->fetchPropertyGroupByCidByPropertyId( $this->getCid(), $this->getId(), $objDatabase );
		}

		return $this->m_objPropertyGroup;
	}

	public function getOrFetchRentArCode( $objDatabase ) {

		$this->m_objPropertyGlSetting = $this->getOrFetchPropertyGlSetting( $objDatabase );
		if( false == valObj( $this->m_objRentArCode, 'CArCode' ) && true == valObj( $this->m_objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
			$this->m_objRentArCode = \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByIdByCid( $this->m_objPropertyGlSetting->getRentArCodeId(), $this->getCid(), $objDatabase );
		}

		return $this->m_objRentArCode;
	}

	public function getOrFetchDepositArCode( $objDatabase ) {

		$this->m_objPropertyGlSetting = $this->getOrFetchPropertyGlSetting( $objDatabase );
		if( false == valObj( $this->m_objDepositArCode, 'CArCode' ) ) {
			$this->m_objDepositArCode = \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByIdByCid( $this->m_objPropertyGlSetting->getDepositArCodeId(), $this->getCid(), $objDatabase );
		}

		return $this->m_objDepositArCode;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getRequiredParameters() {
		return $this->m_arrmixRequiredParametes;
	}

	public function getResidentPortalUrl() {
		return $this->m_strResidentPortalUrl;
	}

	public function getPropertyTypeName() {
		return $this->m_strPropertyTypeName;
	}

	public function getCompanyRegionName() {
		return $this->m_strCompanyRegionName;
	}

	public function getStateName() {
		return $this->m_strStateName;
	}

	public function getProvince() {
		return $this->m_strProvince;
	}

	public function getMerchantAccounts() {
		return $this->m_arrobjMerchantAccounts;
	}

	public function getScheduledChargePostDurationDays() {
		return $this->m_intScheduledChargePostDurationDays;
	}

	public function getAllowAllocations() {
		return $this->m_intAllowAllocations;
	}

	public function getPropertyStatus() {
		return $this->m_intPropertyStatus;
	}

	public function setPropertyStatus( $intPropertyStatus ) {
		$this->m_intPropertyStatus = $intPropertyStatus;
	}

	public function getSemPropertyDetailId() {
		return $this->m_intSemPropertyDetailId;
	}

	public function getSeoFormattedPropertyName() {
		return urlencode( \Psi\CStringService::singleton()->strtolower( str_replace( ' ', '-', $this->getPropertyName() ) ) );
	}

	public function getMiles() {
		return $this->m_fltMiles;
	}

	public function getWebsite() {
		return $this->m_objWebsite;
	}

	public function getOfficePhoneNumber() {
		return $this->m_strOfficePhoneNumber;
	}

	public function getLocalWebsiteDomain() {
		return $this->m_strLocalWebsiteDomain;
	}

	public function getWebsiteDomain() {
		return $this->m_strWebsiteDomain;
	}

	public function getMoreInfoAction() {
		return $this->m_strMoreInfoAction;
	}

	public function getHasPublishedOnlineApplication() {
		return $this->m_boolHasPublishedOnlineApplication;
	}

	public function getHideMoreInfoButton() {
		return $this->m_intHideMoreInfoButton;
	}

	public function getShowApplicationFeeOption() {
		return $this->m_intShowApplicationFeeOption;
	}

	public function getApplications() {
		return $this->m_arrobjApplications;
	}

	public function getLeases() {
		return $this->m_arrobjLeases;
	}

	public function getRentArCode() {
		return $this->m_objRentArCode;
	}

	public function getCompanyMediaFile() {
		return $this->m_objCompanyMediaFile;
	}

	public function getPropertyCraigsListAreas() {
		return $this->m_arrobjPropertyCraigsListAreas;
	}

	public function getPropertyPreferences() {
		return $this->m_arrobjPropertyPreferences;
	}

	public function getWebsitePreferences() {
		return $this->m_arrobjWebsitePreferences;
	}

	public function getPropertyType() {
		return $this->m_objPropertyType;
	}

	public function getOverviewMarketingMediaAssociation() {
		return $this->m_objOverviewMarketingMediaAssociation;
	}

	public function getOverviewMarketingMediaAssociationUrl() {
		return $this->m_strOverviewMarketingMediaAssociationUrl;
	}

	public function getLogoCompanyMediaFile() {
		return $this->m_objLogoCompanyMediaFile;
	}

	public function getPortfolioLogoCompanyMediaFile() {
		return $this->m_objPortfolioLogoCompanyMediaFile;
	}

	public function getPrimaryPropertyAddress() {
		return $this->m_objPrimaryPropertyAddress;
	}

	public function getPropertyApplicationCount() {
		return $this->m_intPropertyApplicationCount;
	}

	public function getPrimaryPropertyPhoneNumber() {
		return $this->m_objPrimaryPropertyPhoneNumber;
	}

	public function getPropertyFloorplans() {
		return $this->m_arrobjPropertyFloorplans;
	}

	public function getReviews() {
		return $this->m_arrobjReviews;
	}

	public function getSpecials() {
		return $this->m_arrobjSpecials;
	}

	public function getLeaseTerms() {
		return $this->m_arrobjLeaseTerms;
	}

	public function getPropertyOfficeHours() {
		return $this->m_arrobjPropertyOfficeHours;
	}

	public function getPetRateAssociations() {
		return $this->m_arrobjPetRateAssociations;
	}

	public function getWebsites() {
		return $this->m_arrobjWebsites;
	}

	public function getPropertyPhoneNumbers() {
		return $this->m_arrobjPropertyPhoneNumbers;
	}

	public function getPropertyBuildings() {
		return $this->m_arrobjPropertyBuildings;
	}

	public function getCompanyMediaFiles() {
		return $this->m_arrobjCompanyMediaFiles;
	}

	public function getPropertyUnits() {
		return $this->m_arrobjPropertyUnits;
	}

	public function getRateRanges() {
		return $this->m_arrobjRateRanges;
	}

	public function getPropertyAddresses() {
		return $this->m_arrobjPropertyAddresses;
	}

	public function getOfficeEmailAddress() {
		return $this->m_objOfficeEmailAddress;
	}

	public function getPrimaryEmailAddress() {
		return $this->m_strPrimaryEmailAddress;
	}

	public function getPortfolioName() {
		return $this->m_strPortfolioName;
	}

	public function getYearBuilt() {

		if( $this->m_strYearBuilt == NULL || false === \Psi\CStringService::singleton()->strpos( $this->m_strYearBuilt, '/' ) ) {
			return $this->m_strYearBuilt;
		}

		$arrstrDateParts = explode( '/', $this->m_strYearBuilt );

		if( 3 <= \Psi\Libraries\UtilFunctions\count( $arrstrDateParts ) ) {
			return $arrstrDateParts[2];
		}
	}

	public function getYearRemodeled() {

		if( $this->m_strYearRemodeled == NULL || false === \Psi\CStringService::singleton()->strpos( $this->m_strYearRemodeled, '/' ) ) {
			return $this->m_strYearRemodeled;
		}

		$arrstrDateParts = explode( '/', $this->m_strYearRemodeled );

		if( 3 <= \Psi\Libraries\UtilFunctions\count( $arrstrDateParts ) ) {
			return $arrstrDateParts[2];
		}
	}

	public function getUnformattedYearBuilt() {
		return $this->m_strYearBuilt;
	}

	public function getUnformattedYearRemodeled() {
		return $this->m_strYearRemodeled;
	}

	public function getMinDeposit() {
		return $this->m_fltMinDeposit;
	}

	public function getMaxDeposit() {
		return $this->m_fltMaxDeposit;
	}

	public function getAvgSquareFeet() {
		return $this->m_intAvgSquareFeet;
	}

	public function getProspectPortalPropertyAnchor() {
		return $this->m_strProspectPortalPropertyAnchor;
	}

	public function getProspectPortalPropertyCaption() {
		return $this->m_strProspectPortalPropertyCaption;
	}

	public function getOnlineApplicationUrl() {
		return $this->m_strOnlineApplicationUrl;
	}

	public function getLeadSources() {
		return $this->m_arrobjLeadSources;
	}

	public function getPropertyConciergeServicesCount() {
		return $this->m_intPropertyConciergeServicesCount;
	}

	public function getPropertyFloorplansCount() {
		return $this->m_intPropertyFloorplansCount;
	}

	public function getPropertyFloorsCount() {
		return $this->m_intPropertyFloorsCount;
	}

	public function getPropertyBuildingsCount() {
		return $this->m_intPropertyBuildingsCount;
	}

	public function getPropertyUnitsCount() {
		return $this->m_intPropertyUnitsCount;
	}

	public function getPropertyApplicationsCount() {
		return $this->m_intPropertyApplicationsCount;
	}

	public function getPropertyPhotosCount() {
		return $this->m_intPropertyPhotosCount;
	}

	public function getPropertyVirtualToursCount() {
		return $this->m_intPropertyVirtualToursCount;
	}

	public function getPropertyWallCategoriesCount() {
		return $this->m_intPropertyWallCategoriesCount;
	}

	public function getPropertyClassifiedCategoriesCount() {
		return $this->m_intPropertyClassifiedCategoriesCount;
	}

	public function getPropertyUnitTypesCount() {
		return $this->m_intPropertyUnitTypesCount;
	}

	public function getPropertyLeadSourcesCount() {
		return $this->m_intPropertyLeadSourcesCount;
	}

	public function getCommunityAmenitiesCount() {
		return $this->m_intCommunityAmenitiesCount;
	}

	public function getApartmentAmenitiesCount() {
		return $this->m_intApartmentAmenitiesCount;
	}

	public function getPropertyMaintenanceLocationsCount() {
		return $this->m_intPropertyMaintenanceLocationsCount;
	}

	public function getPropertyMaintenanceProblemsCount() {
		return $this->m_intPropertyMaintenanceProblemsCount;
	}

	public function getPropertyMaintenanceTemplateId() {
		return $this->m_intPropertyMaintenanceTemplateId;
	}

	public function getIntegrationClientTypeId() {
		return $this->m_intIntegrationClientTypeId;
	}

	public function getPropertyNotificationEmailsCount() {
		return $this->m_intPropertyNotificationEmailsCount;
	}

	public function getCommunityWebsiteUri() {
		return $this->m_strCommunityWebsiteUri;
	}

	public function getCorporateWebsiteUri() {
		return $this->m_strCorporateWebsiteUri;
	}

	public function getPropertyAreas() {
		return $this->m_arrobjPropertyAreas;
	}

	public function getCareers() {
		return $this->m_arrobjCareers;
	}

	public function getApplicationCount() {
		return $this->m_intApplicationCount;
	}

	public function getGuestCardCount() {
		return $this->m_intGuestCardCount;
	}

	public function getCompletedApplicationsCount() {
		return $this->m_intCompletedApplicationsCount;
	}

	public function getPropertyEventResultsCount() {
		return $this->m_intPropertyEventResultsCount;
	}

	public function getPropertyLeasingAgentsCount() {
		return $this->m_intPropertyLeasingAgentsCount;
	}

	public function getPropertyDetail() {
		return $this->m_objPropertyDetail;
	}

	public function getIsPropertyUnitsAvailable() {
		return $this->m_intIsPropertyUnitsAvailable;
	}

	public function getCommunityAmenities() {
		return $this->m_arrobjCommunityAmenities;
	}

	public function getApartmentAmenities() {
		return $this->m_arrobjApartmentAmenities;
	}

	public function getSemPropertyStatusTypeId() {
		return $this->m_intSemPropertyStatusTypeId;
	}

	public function getIsLimited() {
		return $this->m_boolIsLimited;
	}

	public function getTimezoneSpecificTimestamp( $strDateTime, $objClientDatabase ) {
		if( true == is_null( $strDateTime ) || 0 == strlen( trim( $strDateTime ) ) ) {
			return NULL;
		}

		$objTimeZone		= $this->getOrfetchTimezone( $objClientDatabase );
		$strTimeZoneName	= ( true == valObj( $objTimeZone, 'CTimeZone' ) ) ? $objTimeZone->getTimeZoneName() : 'America/Denver';

		return strtotime( getConvertedDateTimeByTimeZoneName( $strDateTime, 'm/d/Y H:i:s', $strTimeZoneName ) );
	}

	public function getMstTimestamp( $strDateTime, $objDatabase ) {
		if( true == is_null( $strDateTime ) || 0 == strlen( trim( $strDateTime ) ) ) {
			return NULL;
		}

		$objTimeZone		= $this->fetchTimeZone( $objDatabase );
		$strTimeZoneName	= ( true == valObj( $objTimeZone, 'CTimeZone' ) ) ? $objTimeZone->getTimeZoneName() : 'America/Denver';

		return strtotime( getConvertedDateTime( $strDateTime, 'm/d/Y H:i:s', $strTimeZoneName, 'America/Denver' ) );
	}

	public function getUnitSpacesCount() {
		return $this->m_intUnitSpacesCount;
	}

	public function getSeoPropertyName() {
		return self::formatSeoPropertyName( $this->getPropertyName() );
	}

	public function getSeoCity() {
		return \Psi\CStringService::singleton()->strtolower( \Psi\CStringService::singleton()->preg_replace( [ '/[^0-9a-zA-Z\s\-]+/i', '/\s+/' ], [ '', '-' ], $this->m_strSeoCity ) );
	}

	public function getSeoState() {
		return \Psi\CStringService::singleton()->strtolower( \Psi\CStringService::singleton()->preg_replace( [ '/[^0-9a-zA-Z\s\-]+/i', '/\s+/' ], [ '', '-' ], $this->m_strSeoState ) );
	}

	public function getRawProperty() {

		$strSitePlaneImage					= '';

		if( true == valArr( $this->getCompanyMediaFiles() ) ) {
			foreach( $this->getCompanyMediaFiles() as $objCompanyMediaFile ) {
				$strSitePlaneImage = $objCompanyMediaFile->getFullsizeUri();
			}
		}

		// Few website prefereces, we will considerd those as a part of proeprty object
		$arrstrWebsitePreferences			= $this->getPropertyWebsitePreferences();
		$boolIsShowCheckAvailabilityLink	= false;

		if( true == valArr( $arrstrWebsitePreferences ) ) {

			if( true == array_key_exists( 'HIDE_REQUEST_TO_HOLD', $arrstrWebsitePreferences ) && 1 == $arrstrWebsitePreferences['HIDE_REQUEST_TO_HOLD']->getValue() ) {
				$boolIsShowCheckAvailabilityLink = $arrstrWebsitePreferences['HIDE_REQUEST_TO_HOLD']->getValue();
			}
		}

		// Amenities and their Label
		$strPropertyOtherAmenityLabel	= '';
		$strCommunityAmenityLabel		= '';
		$arrstrApartmentAmenities		= [];
		$arrstrCommunityAmenities		= [];
		$arrstrPropertyPreferences		= [];
		$arrstrWebsitePreferences		= [];

		$intWebsitePreferencesCounter = 0;

		if( CPropertyType::HOME_OWNERS_ASSOCIATION != $this->getPropertyTypeId() ) {

			// Amenities
			if( true == valArr( $this->getApartmentAmenities() ) ) {
				foreach( $this->getApartmentAmenities() as $objApartmentAmenity ) {
					$arrstrApartmentAmenities[$objApartmentAmenity->getId()] = $objApartmentAmenity->getRawApartmentAmenity();
				}
			}

			if( true == valArr( $this->getCommunityAmenities() ) ) {
				foreach( $this->getCommunityAmenities() as $objCommunityAmenity ) {
					$arrstrCommunityAmenities[$objCommunityAmenity->getId()] = $objCommunityAmenity->getRawCommunityAmenity();
				}
			}

			$strPropertyOtherAmenityLabel	= $this->getPropertyOtherAmenitiesLabel();
			$strCommunityAmenityLabel		= $this->getCommunityAmenitiesLabel();
		}

		if( true == valArr( $this->getPropertyPreferences() ) ) {

			$intCounter = 0;

			foreach( $this->getPropertyPreferences() as $objPropertyPreference ) {

				$arrstrPropertyPreferences[$intCounter]['key']		= $objPropertyPreference->getKey();
				$arrstrPropertyPreferences[$intCounter]['value']	= $objPropertyPreference->getValue();

				$intCounter++;
			}

		}

		if( true == valArr( $this->getWebsitePreferences() ) ) {

			foreach( $this->getWebsitePreferences() as $objWebsitePreference ) {

				$arrstrWebsitePreferences[$intWebsitePreferencesCounter]['key']		= $objWebsitePreference->getKey();
				$arrstrWebsitePreferences[$intWebsitePreferencesCounter]['value']	= $objWebsitePreference->getValue();

				$intWebsitePreferencesCounter++;
			}
		}

		if( CPropertyType::COMMERCIAL != $this->getPropertyTypeId() && CPropertyType::HOME_OWNERS_ASSOCIATION != $this->getPropertyTypeId() && true == $this->getHasPublishedOnlineApplication() ) {

			$arrstrWebsitePreferences[$intWebsitePreferencesCounter]['key']		= 'SHOW_APPLY_NOW_LINK';
			$arrstrWebsitePreferences[$intWebsitePreferencesCounter]['value']	= 1;
		}

		return [
			'id'							=> $this->getId(),
			'propertyName'					=> $this->getPropertyName(),
			'propertyPreferences'			=> $arrstrPropertyPreferences,
			'websitePreferences'			=> $arrstrWebsitePreferences,
			'CommunityAmenitiesLabel'		=> $strCommunityAmenityLabel,
			'isShowCheckAvailabilityLink'	=> $boolIsShowCheckAvailabilityLink,
			'propertyOtherAmenitiesLabel'	=> $strPropertyOtherAmenityLabel,
			'propertySitePlanImage'			=> $strSitePlaneImage,
			'apartmentAmenities'			=> $arrstrApartmentAmenities,
			'communityAmenities'			=> $arrstrCommunityAmenities
		];
	}

	public function getCommunityAmenitiesLabel() {
		switch( $this->getPropertyTypeId() ) {
			case CPropertyType::COMMERCIAL:
				$strCommunityAmenityLabel = 'Area Amenities';
				break;

			case CPropertyType::STORAGE:
				$strCommunityAmenityLabel = 'Unit Features';
				break;

			default:
				$strCommunityAmenityLabel = 'Apartment Amenities';
		}

		return $strCommunityAmenityLabel;
	}

	public function getPropertyOtherAmenitiesLabel() {

		$strPropertyOtherAmenityLabel = 'Other Amenities';

		if( true == in_array( $this->getPropertyTypeId(), [ CPropertyType::APARTMENT, CPropertyType::SINGLE_FAMILY ], false ) ) {
			$strPropertyOtherAmenityLabel = 'Community Amenities';
		} elseif( CPropertyType::COMMERCIAL == $this->getPropertyTypeId() ) {
			$strPropertyOtherAmenityLabel = 'Property Amenities';
		} elseif( CPropertyType::STORAGE == $this->getPropertyTypeId() ) {
			$strPropertyOtherAmenityLabel = 'Facility Features';
		}

		return $strPropertyOtherAmenityLabel;
	}

	public function getPropertyWebsitePreferences() {
		return $this->m_arrobjPropertyWebsitePreferences;
	}

	public function getChildPropertyIds() {
		return $this->m_arrintChildPropertyIds;
	}

	public function getChildProperty() {
		return $this->m_objChildProperty;
	}

	public function getProperty() {
		return $this->m_objProperty;
	}

	public function getSemPropertyDetail() {
		return $this->m_objSemPropertyDetail;
	}

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function getStreetLine2() {
		return $this->m_strStreetLine2;
	}

	public function getStreetLine3() {
		return $this->m_strStreetLine3;
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function getHasRemainingBudget() {
		return $this->m_boolHasRemainingBudget;
	}

	public function getIsInSemKeyword() {
		return $this->m_boolIsInSemKeyword;
	}

	public function getIsValidRentArCodeId() {

		return $this->m_boolIsValidRentArCodeId;
	}

	public function getIsArMigrationMode() {

		return $this->m_boolIsArMigrationMode;
	}

	public function getLastExternalClickDatetime() {
		return $this->m_strLastExternalClickDatetime;
	}

	public function getDatabaseId() {
		return $this->m_intDatabaseId;
	}

	public function getSemCommunityAmenities() {
		return $this->m_arrobjSemCommunityAmenities;
	}

	public function getPropertyPhoneNumber() {
		return $this->m_objPropertyPhoneNumber;
	}

	public function getIsFeatured() {
		return $this->m_boolIsFeatured;
	}

	public function getTotalScore() {
		return $this->m_fltTotalScore;
	}

	public function getSemAndOrganicStat() {
		return $this->m_objSemAndOrganicStat;
	}

	public function getSemBudget() {
		return $this->m_objSemBudget;
	}

	public function getSemSetting() {
		return $this->m_objSemSetting;
	}

	public function getPropertyGlSetting() {
		return $this->m_objPropertyGlSetting;
	}

	public function getPropertyChargeSetting() {
		return $this->m_objPropertyChargeSetting;
	}

	public function getLongitude() {
		return $this->m_fltLongitude;
	}

	public function getLatitude() {
		return $this->m_fltLatitude;
	}

	public function getChildProperties() {
		return $this->m_arrobjChildProperties;
	}

	public function getClosedDays() {
		return $this->m_arrintClosedDays;
	}

	public function getCompanyPricings() {
		return $this->m_arrobjCompanyPricings;
	}

	public function getCompanyEmployees() {
		return $this->m_arrobjCompanyEmployees;
	}

	public function getResidentInsureSubDomain() {
		return $this->m_strResidentInsureSubDomain;
	}

	public function getResidentInsurePhoneNumber() {
		return $this->m_strResidentInsurePhoneNumber;
	}

	public function getPropertyManagerName() {
		return $this->m_strPropertyManagerName;
	}

	public function getDocuments() {
		return $this->m_arrobjDocuments;
	}

	public function getListItems() {
		return $this->m_arrobjListItems;
	}

	public function getPropertyDocuments() {
		return $this->m_arrobjPropertyDocuments;
	}

	public function getClientName() {
		return $this->m_strClientName;
	}

	public function getClientStatusTypeId() {
		return $this->m_intClientStatusTypeId;
	}

	public function getSalesTaxResultTypeId() {
		return $this->m_intSalesTaxResultTypeId;
	}

	public function getGlAccountProperties() {
		return $this->m_arrobjGlAccountProperties;
	}

	public function getGlTreeName() {
		return $this->m_strGlTreeName;
	}

	public function getGlTreeId() {
		return $this->m_intGlTreeId;
	}

	public function getInternetListingServiceId() {
		return $this->m_intInternetListingServiceId;
	}

	public function getPropertyGroupId() {
		return $this->m_intPropertyGroupId;
	}

	public function getInternetListingServiceIds() {
		return $this->m_arrintInternetListingServiceIds;
	}

	public function getAllowMultipleAccountProperties() {
		return $this->m_boolAllowMultipleAccountProperties;
	}

	public function getMarketingPropertyName() {
		return $this->m_strMarketingPropertyName;
	}

	public function getRowCount() {
		return $this->m_intRowCount;
	}

	public function getPetPolicy() {
		return $this->m_strPetPolicy;
	}

	public function getIsBulkAddProperty() {
		return $this->m_boolIsBulkAddProperty;
	}

	public function getParkingPolicy() {
		return $this->m_strParkingPolicy;
	}

	public function getPrimaryPhoneNumber() {
		return $this->m_strPrimaryPhoneNumber;
	}

	public function getOfficeFaxNumber() {
		return $this->m_strOfficeFaxNumber;
	}

	public function getContractTerminationRequestDate() {
		return $this->m_strContractTerminationRequestDate;
	}

	public function getContractTerminationRequestId() {
		return $this->m_strContractTerminationRequestId;
	}

	public function getContractPropertyTerminationDate() {
		return $this->m_strContractPropertyTerminationDate;
	}

	public function getDefaultLeaseTerm( $objDatabase, $boolIsProspect = true, $boolIsRenewal = false, $intOccupancyTypeId = NULL ) {

		$intDefaultLeaseTerm = 12;

		$objDefaultLeaseTerm = CLeaseTerms::fetchLeaseTermByTermMonthByOccupancyTypeIdPropertyIdByCid( $intDefaultLeaseTerm, $intOccupancyTypeId, $this->getId(), $this->getCid(), $objDatabase, $boolIsProspect, $boolIsRenewal );
		if( false == valObj( $objDefaultLeaseTerm, 'CLeaseTerm' ) ) {
			$objDefaultLeaseTerm = CLeaseTerms::fetchPublishedMaxLeaseTermByOccupancyTypeIdByCidByPropertyId( $this->getCid(), $intOccupancyTypeId, $this->getId(), $objDatabase, $boolIsProspect, $boolIsRenewal );
		}
		if( false == valObj( $objDefaultLeaseTerm, 'CLeaseTerm' ) ) {
			$objDefaultLeaseTerm = CLeaseTerms::fetchPublishedMaxLeaseTermByCidByPropertyId( $this->getCid(), $this->getId(), $objDatabase, $boolIsProspect, $boolIsRenewal );
		}

		if( true == valObj( $objDefaultLeaseTerm, 'CLeaseTerm' ) ) {
			$intDefaultLeaseTerm = $objDefaultLeaseTerm->getTermMonth();
		}

		return $intDefaultLeaseTerm;
	}

	public function getEsaFileName( $strExtension = 'pdf' ) {
		return 'esa_' . $this->getId() . '_v3.' . $strExtension;
	}

	public function getParentChildPropertyIds( $objDatabase = NULL ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		$arrintPropertyIds    = [ $this->getId() ];
		$arrintChildPropertyIds = $this->getOrFetchChildPropertyIds( $objDatabase );
		if( true == valId( $this->getPropertyId() ) ) {
			$arrintPropertyIds[] = $this->getPropertyId();
		}
		if( true == valArr( $arrintChildPropertyIds ) ) {
			$arrintPropertyIds = array_merge( $arrintPropertyIds, $arrintChildPropertyIds );
		}

		return array_unique( $arrintPropertyIds );
	}

	public function getMarketedAsPropertyName() {

		$strPropertyName = $this->getMarketingName();

		return ( true == valStr( $strPropertyName ) ? $strPropertyName : $this->getPropertyName() );
	}

	public function getSystemMessageEmailContent() {
		return $this->m_strSystemMessageEmailContent;
	}

	public function getGlPostMonth() {
		return $this->m_strGlPostMonth;
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function getApPostMonth() {
		return $this->m_strApPostMonth;
	}

	public function getBankAccountTypeId() {
		return $this->m_intBankAccountTypeId;
	}

	public function getReimbursedPropertyId() {
		return $this->m_intReimbursedPropertyId;
	}

	public function getIsApMigrationMode() {
		return $this->m_boolIsApMigrationMode;
	}

	public function getIsAllocation() {
		return $this->m_intIsAllocation;
	}

	/**
	 * Add Functions
	 *
	 */

	public function addCompanyEmployee( $objCompanyEmployee ) {
		$this->m_arrobjCompanyEmployees[$objCompanyEmployee->getId()] = $objCompanyEmployee;
	}

	public function addApplication( $objApplication ) {
		$this->m_arrobjApplications[$objApplication->getId()] = $objApplication;
	}

	public function addLease( $objLease ) {
		$this->m_arrobjLeases[$objLease->getId()] = $objLease;
	}

	public function addLeadSource( $objLeadSource ) {
		$this->m_arrobjLeadSources[$objLeadSource->getId()] = $objLeadSource;
	}

	public function addErrorMsgs( $arrobjErrorMsgs ) {
		if( true == valArr( $arrobjErrorMsgs ) ) {
			foreach( $arrobjErrorMsgs as $objErrorMsg ) {
				$this->addErrorMsg( $objErrorMsg );
			}
		}
	}

	public function addPropertyArea( $objPropertyArea ) {
		$this->m_arrobjPropertyAreas[$objPropertyArea->getId()] = $objPropertyArea;
	}

	public function addCareer( $objCareer ) {
		$this->m_arrobjCareers[$objCareer->getId()] = $objCareer;
	}

	public function addCommunityAmenity( $objCommunityAmenity ) {
		$this->m_arrobjCommunityAmenities[$objCommunityAmenity->getId()] = $objCommunityAmenity;
	}

	public function addApartmentAmenity( $objApartmentAmenity ) {
		$this->m_arrobjApartmentAmenities[$objApartmentAmenity->getId()] = $objApartmentAmenity;
	}

	public function addPropertyCraigsListArea( $objPropertyCraigsListArea ) {
		$this->m_arrobjPropertyCraigsListAreas[$objPropertyCraigsListArea->getId()] = $objPropertyCraigsListArea;
	}

	public function addSemCommunityAmenity( $objSemCommunityAmenity ) {
		$this->m_arrobjSemCommunityAmenities[$objSemCommunityAmenity->getId()] = $objSemCommunityAmenity;
	}

	public function addChildProperty( $objChildProperty ) {
		$this->m_arrobjChildProperties[$objChildProperty->getId()] = $objChildProperty;
	}

	public function addCompanyPricing( $objCompanyPricing ) {
		$this->m_arrobjCompanyPricings[$objCompanyPricing->getCompanyPricingTypeId()] = $objCompanyPricing;
	}

	public function addDocument( $objDocument ) {
		$this->m_arrobjDocuments[$objDocument->getId()] = $objDocument;
	}

	public function addParentPropertyDocument( $objParentDocument ) {
		$this->m_arrobjPropertyDocuments[$objParentDocument->getParentDocumentId()] = $objParentDocument;
	}

	public function addListItem( $objListItem ) {
		$this->m_arrobjListItems[$objListItem->getId()] = $objListItem;
	}

	public function addSpecial( $objSpecial ) {
		$this->m_arrobjSpecials[$objSpecial->getId()] = $objSpecial;
	}

	public function addPropertyPreference( $objPropertyPreference ) {
		$this->m_arrobjPropertyPreferences[$objPropertyPreference->getKey()] = $objPropertyPreference;
	}

	public function addPropertyFloorplan( $objPropertyFloorplan ) {
		$this->m_arrobjPropertyFloorplans[$objPropertyFloorplan->getId()] = $objPropertyFloorplan;
	}

	public function addReview( $objReview ) {
		$this->m_arrobjReviews[$objReview->getId()] = $objReview;
	}

	public function addWebsite( $objWebsite ) {
		$this->m_arrobjWebsites[$objWebsite->getId()] = $objWebsite;
	}

	public function addPropertyPhoneNumber( $objPropertyPhoneNumber ) {
		$this->m_arrobjPropertyPhoneNumbers[$objPropertyPhoneNumber->getId()] = $objPropertyPhoneNumber;
	}

	public function addPropertyBuilding( $objPropertyBuilding ) {
		$this->m_arrobjPropertyBuildings[$objPropertyBuilding->getId()] = $objPropertyBuilding;
	}

	public function addCompanyMediaFile( $objCompanyMediaFile ) {
		$this->m_arrobjCompanyMediaFiles[$objCompanyMediaFile->getId()] = $objCompanyMediaFile;
	}

	public function addPropertyUnit( $objPropertyUnit ) {
		$this->m_arrobjPropertyUnits[$objPropertyUnit->getId()] = $objPropertyUnit;
	}

	public function addPropertyAddress( $objPropertyAddress ) {
		$this->m_arrobjPropertyAddresses[$objPropertyAddress->getId()] = $objPropertyAddress;
	}

	public function addPropertyOfficeHour( $objPropertyOfficeHours ) {
		$this->m_arrobjPropertyOfficeHours[$objPropertyOfficeHours->getId()] = $objPropertyOfficeHours;
	}

	public function addPetRateAssociation( $objPetRateAssociations ) {
		$this->m_arrobjPetRateAssociations[$objPetRateAssociations->getId()] = $objPetRateAssociations;
	}

	public function addAmenityRateAssociation( $objCommunityAmenities ) {
		$this->m_arrobjCommunityAmenities[$objCommunityAmenities->getId()] = $objCommunityAmenities;
	}

	public function addLeaseTerm( $arrobjLeaseTerm ) {
		$this->m_arrobjLeaseTerms[$arrobjLeaseTerm->getId()] = $arrobjLeaseTerm;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );

		if( true == isset( $arrmixValues['min_deposit'] ) ) {
			$this->setMinDeposit( $arrmixValues['min_deposit'] );
		}
		if( true == isset( $arrmixValues['max_deposit'] ) ) {
			$this->setMaxDeposit( $arrmixValues['max_deposit'] );
		}
		if( true == isset( $arrmixValues['avg_square_feet'] ) ) {
			$this->setAvgSquareFeet( $arrmixValues['avg_square_feet'] );
		}
		if( true == isset( $arrmixValues['property_application_count'] ) ) {
			$this->setPropertyApplicationCount( $arrmixValues['property_application_count'] );
		}
		if( true == isset( $arrmixValues['prospect_portal_property_anchor'] ) ) {
			$this->setProspectPortalPropertyAnchor( $arrmixValues['prospect_portal_property_anchor'] );
		}
		if( true == isset( $arrmixValues['prospect_portal_property_caption'] ) ) {
			$this->setProspectPortalPropertyCaption( $arrmixValues['prospect_portal_property_caption'] );
		}
		if( true == isset( $arrmixValues['hide_more_info_button'] ) ) {
			$this->setHideMoreInfoButton( $arrmixValues['hide_more_info_button'] );
		}
		if( true == isset( $arrmixValues['more_info_action'] ) ) {
			$this->setMoreInfoAction( $arrmixValues['more_info_action'] );
		}
		if( true == isset( $arrmixValues['show_application_fee_option'] ) ) {
			$this->setShowApplicationFeeOption( $arrmixValues['show_application_fee_option'] );
		}
		if( true == isset( $arrmixValues['local_website_domain'] ) ) {
			$this->setLocalWebsiteDomain( $arrmixValues['local_website_domain'] );
		}
		if( true == isset( $arrmixValues['website_domain'] ) ) {
			$this->setWebsiteDomain( $arrmixValues['website_domain'] );
		}
		if( true == isset( $arrmixValues['property_concierge_services_count'] ) ) {
			$this->setPropertyConciergeServicesCount( $arrmixValues['property_concierge_services_count'] );
		}
		if( true == isset( $arrmixValues['property_floorplans_count'] ) ) {
			$this->setPropertyFloorplansCount( $arrmixValues['property_floorplans_count'] );
		}
		if( true == isset( $arrmixValues['property_buildings_count'] ) ) {
			$this->setPropertyBuildingsCount( $arrmixValues['property_buildings_count'] );
		}
		if( true == isset( $arrmixValues['property_units_count'] ) ) {
			$this->setPropertyUnitsCount( $arrmixValues['property_units_count'] );
		}
		if( true == isset( $arrmixValues['property_applications_count'] ) ) {
			$this->setPropertyApplicationsCount( $arrmixValues['property_applications_count'] );
		}
		if( true == isset( $arrmixValues['property_photos_count'] ) ) {
			$this->setPropertyPhotosCount( $arrmixValues['property_photos_count'] );
		}
		if( true == isset( $arrmixValues['property_virtual_tours_count'] ) ) {
			$this->setPropertyVirtualToursCount( $arrmixValues['property_virtual_tours_count'] );
		}
		if( true == isset( $arrmixValues['property_classified_categories_count'] ) ) {
			$this->setPropertyClassifiedCategoriesCount( $arrmixValues['property_classified_categories_count'] );
		}
		if( true == isset( $arrmixValues['property_unit_types_count'] ) ) {
			$this->setPropertyUnitTypesCount( $arrmixValues['property_unit_types_count'] );
		}
		if( true == isset( $arrmixValues['property_lead_sources_count'] ) ) {
			$this->setPropertyLeadSourcesCount( $arrmixValues['property_lead_sources_count'] );
		}
		if( true == isset( $arrmixValues['community_amenities_count'] ) ) {
			$this->setCommunityAmenitiesCount( $arrmixValues['community_amenities_count'] );
		}
		if( true == isset( $arrmixValues['apartment_amenities_count'] ) ) {
			$this->setApartmentAmenitiesCount( $arrmixValues['apartment_amenities_count'] );
		}
		if( true == isset( $arrmixValues['property_maintenance_locations_count'] ) ) {
			$this->setPropertyMaintenanceLocationsCount( $arrmixValues['property_maintenance_locations_count'] );
		}
		if( true == isset( $arrmixValues['property_maintenance_problems_count'] ) ) {
			$this->setPropertyMaintenanceProblemsCount( $arrmixValues['property_maintenance_problems_count'] );
		}
		if( true == isset( $arrmixValues['property_maintenance_template_id'] ) ) {
			$this->setPropertyMaintenanceTemplateId( $arrmixValues['property_maintenance_template_id'] );
		}
		if( true == isset( $arrmixValues['community_website_uri'] ) ) {
			$this->setCommunityWebsiteUri( $arrmixValues['community_website_uri'] );
		}
		if( true == isset( $arrmixValues['corporate_website_uri'] ) ) {
			$this->setCorporateWebsiteUri( $arrmixValues['corporate_website_uri'] );
		}
		if( true == isset( $arrmixValues['application_count'] ) ) {
			$this->setApplicationCount( $arrmixValues['application_count'] );
		}
		if( true == isset( $arrmixValues['guest_card_count'] ) ) {
			$this->setGuestCardCount( $arrmixValues['guest_card_count'] );
		}
		if( true == isset( $arrmixValues['office_phone_number'] ) ) {
			$this->setOfficePhoneNumber( $arrmixValues['office_phone_number'] );
		}
		if( true == isset( $arrmixValues['street_line1'] ) ) {
			$this->setStreetLine1( $arrmixValues['street_line1'] );
		}
		if( true == isset( $arrmixValues['street_line2'] ) ) {
			$this->setStreetLine2( $arrmixValues['street_line2'] );
		}
		if( true == isset( $arrmixValues['street_line3'] ) ) {
			$this->setStreetLine3( $arrmixValues['street_line3'] );
		}
		if( true == isset( $arrmixValues['city'] ) ) {
			$this->setCity( $arrmixValues['city'] );
		}
		if( true == isset( $arrmixValues['postal_code'] ) ) {
			$this->setPostalCode( $arrmixValues['postal_code'] );
		}
		if( true == isset( $arrmixValues['state_code'] ) ) {
			$this->setStateCode( $arrmixValues['state_code'] );
		}
		if( true == isset( $arrmixValues['property_manager_name'] ) ) {
			$this->setPropertyManagerName( $arrmixValues['property_manager_name'] );
		}
		if( true == isset( $arrmixValues['row_count'] ) ) {
			$this->setRowCount( $arrmixValues['row_count'] );
		}

		if( true == isset( $arrmixValues['has_remaining_budget'] ) ) {
			$this->setHasRemainingBudget( $arrmixValues['has_remaining_budget'] );
		}
		if( true == isset( $arrmixValues['is_in_sem_keyword'] ) ) {
			$this->setIsInSemKeyword( $arrmixValues['is_in_sem_keyword'] );
		}
		if( true == isset( $arrmixValues['last_external_click_datetime'] ) ) {
			$this->setLastExternalClickDatetime( $arrmixValues['last_external_click_datetime'] );
		}

		if( true == isset( $arrmixValues['database_id'] ) ) {
			$this->setDatabaseId( $arrmixValues['database_id'] );
		}
		if( true == isset( $arrmixValues['miles'] ) ) {
			$this->setMiles( $arrmixValues['miles'] );
		}
		if( true == isset( $arrmixValues['sem_property_detail_id'] ) ) {
			$this->setSemPropertyDetailId( $arrmixValues['sem_property_detail_id'] );
		}

		if( true == isset( $arrmixValues['longitude'] ) ) {
			$this->setLongitude( $arrmixValues['longitude'] );
		}
		if( true == isset( $arrmixValues['latitude'] ) ) {
			$this->setLatitude( $arrmixValues['latitude'] );
		}

		if( true == isset( $arrmixValues['overview_company_media_file_url'] ) ) {
			$this->setOverviewMarketingMediaAssociationUrl( $arrmixValues['overview_company_media_file_url'] );
		}
		if( true == isset( $arrmixValues['sem_property_status_type_id'] ) ) {
			$this->setSemPropertyStatusTypeId( $arrmixValues['sem_property_status_type_id'] );
		}
		if( true == isset( $arrmixValues['primary_email_address'] ) ) {
			$this->setPrimaryEmailAddress( $arrmixValues['primary_email_address'] );
		}

		if( true == isset( $arrmixValues['is_limited'] ) ) {
			$this->setIsLimited( $arrmixValues['is_limited'] );
		}
		if( true == isset( $arrmixValues['allow_allocations'] ) ) {
			$this->setAllowAllocations( $arrmixValues['allow_allocations'] );
		}
		if( true == isset( $arrmixValues['total_score'] ) ) {
			$this->setTotalScore( $arrmixValues['total_score'] );
		}
		if( true == isset( $arrmixValues['seo_city'] ) ) {
			$this->setSeoCity( $arrmixValues['seo_city'] );
		}
		if( true == isset( $arrmixValues['is_featured'] ) ) {
			$this->setIsFeatured( $arrmixValues['is_featured'] );
		}
		if( true == isset( $arrmixValues['portfolio_name'] ) ) {
			$this->setPortfolioName( $arrmixValues['portfolio_name'] );
		}
		if( true == isset( $arrmixValues['scheduled_charge_post_duration_days'] ) ) {
			$this->setScheduledChargePostDurationDays( $arrmixValues['scheduled_charge_post_duration_days'] );
		}
		if( true == isset( $arrmixValues['client_name'] ) ) {
			$this->setClientName( $arrmixValues['client_name'] );
		}
		if( true == isset( $arrmixValues['property_status'] ) ) {
			$this->setPropertyStatus( $arrmixValues['property_status'] );
		}
		if( true == isset( $arrmixValues['property_type_name'] ) ) {
			$this->setPropertyTypeName( $arrmixValues['property_type_name'] );
		}
		if( true == isset( $arrmixValues['state_name'] ) ) {
			$this->setStateName( $arrmixValues['state_name'] );
		}
		if( true == isset( $arrmixValues['province'] ) ) {
			$this->setProvince( $arrmixValues['province'] );
		}
		if( true == isset( $arrmixValues['gl_tree_name'] ) ) {
			$this->setGlTreeName( ( true == $boolIsStripSlashes ) ? stripslashes( $arrmixValues['gl_tree_name'] ) : $arrmixValues['gl_tree_name'] );
		}
		if( true == isset( $arrmixValues['gl_tree_id'] ) ) {
			$this->setGlTreeId( ( true == $boolIsStripSlashes ) ? stripslashes( $arrmixValues['gl_tree_id'] ) : $arrmixValues['gl_tree_name'] );
		}
		if( true == isset( $arrmixValues['internet_listing_service_id'] ) ) {
			$this->setInternetListingServiceId( ( true == $boolIsStripSlashes ) ? stripslashes( $arrmixValues['internet_listing_service_id'] ) : $arrmixValues['internet_listing_service_id'] );
		}

		if( true == isset( $arrmixValues['property_group_id'] ) ) {
			$this->setPropertyGroupId( $arrmixValues['property_group_id'] );
		}
		if( true == isset( $arrmixValues['sales_tax_result_type_id'] ) ) {
			$this->setSalesTaxResultTypeId( $arrmixValues['sales_tax_result_type_id'] );
		}
		if( true == isset( $arrmixValues['internet_listing_service_ids'] ) ) {
			$this->setInternetListingServiceIds( $arrmixValues['internet_listing_service_ids'] );
		}
		if( true == isset( $arrmixValues['integration_client_type_id'] ) ) {
			$this->setIntegrationClientTypeId( $arrmixValues['integration_client_type_id'] );
		}

		if( true == isset( $arrmixValues['property_units_count'] ) ) {
			$this->setPropertyUnitsCount( $arrmixValues['property_units_count'] );
		}
		if( true == isset( $arrmixValues['pet_policy'] ) ) {
			$this->setPetPolicy( $arrmixValues['pet_policy'] );
		}
		if( true == isset( $arrmixValues['parking_policy'] ) ) {
			$this->setParkingPolicy( $arrmixValues['parking_policy'] );
		}
		if( true == isset( $arrmixValues['primary_phone_number'] ) ) {
			$this->setPrimaryPhoneNumber( $arrmixValues['primary_phone_number'] );
		}
		if( true == isset( $arrmixValues['office_fax_number'] ) ) {
			$this->setOfficeFaxNumber( $arrmixValues['office_fax_number'] );
		}
		if( true == isset( $arrmixValues['contract_termination_request_date'] ) ) {
			$this->setContractTerminationRequestDate( $arrmixValues['contract_termination_request_date'] );
		}
		if( true == isset( $arrmixValues['contract_property_termination_date'] ) ) {
			$this->setContractPropertyTerminationDate( $arrmixValues['contract_property_termination_date'] );
		}
		if( true == isset( $arrmixValues['contract_termination_request_id'] ) ) {
			$this->setContractTerminationRequestId( $arrmixValues['contract_termination_request_id'] );
		}
		if( true == isset( $arrmixValues['is_valid_rent_ar_code_id'] ) ) {
			$this->setIsValidRentArCodeId( $arrmixValues['is_valid_rent_ar_code_id'] );
		}
		if( isset( $arrmixValues['is_ar_migration_mode'] ) ) {
			$this->setIsArMigrationMode( trim( stripcslashes( $arrmixValues['is_ar_migration_mode'] ) ) );
		}
		if( true == isset( $arrmixValues['property_preference_key'] ) ) {
			$this->setPropertyPreferenceKey( $arrmixValues['property_preference_key'] );
		}
		if( true == isset( $arrmixValues['property_preference_value'] ) ) {
			$this->setPropertyPreferenceValue( $arrmixValues['property_preference_value'] );
		}
		if( true == isset( $arrmixValues['associated_account_id'] ) ) {
			$this->setAssociatedAccountId( $arrmixValues['associated_account_id'] );
		}
		if( true == isset( $arrstrValues['supported_locale_codes'] ) ) {
			$this->setSupportedLocaleCodes( $arrmixValues['supported_locale_codes'] );
		}
		if( true == isset( $arrmixValues['gl_post_month'] ) ) {
			$this->setGlPostMonth( ( true == $boolIsStripSlashes ) ? stripslashes( $arrmixValues['gl_post_month'] ) : $arrmixValues['gl_post_month'] );
		}
		if( true == isset( $arrmixValues['ap_post_month'] ) ) {
			$this->setApPostMonth( ( true == $boolIsStripSlashes ) ? stripslashes( $arrmixValues['ap_post_month'] ) : $arrmixValues['ap_post_month'] );
		}
		if( true == isset( $arrmixValues['bank_account_type_id'] ) ) {
			$this->setBankAccountTypeId( ( true == $boolIsStripSlashes ) ? stripslashes( $arrmixValues['bank_account_type_id'] ) : $arrmixValues['bank_account_type_id'] );
		}
		if( true == isset( $arrmixValues['reimbursed_property_id'] ) ) {
			$this->setReimbursedPropertyId( ( true == $boolIsStripSlashes ) ? stripslashes( $arrmixValues['reimbursed_property_id'] ) : $arrmixValues['reimbursed_property_id'] );
		}
		if( true == isset( $arrmixValues['is_ap_migration_mode'] ) ) {
			$this->setIsApMigrationMode( ( true == $boolIsStripSlashes ) ? stripslashes( $arrmixValues['is_ap_migration_mode'] ) : $arrmixValues['is_ap_migration_mode'] );
		}
		if( true == isset( $arrmixValues['is_allocation'] ) ) {
			$this->setIsAllocation( ( true == $boolIsStripSlashes ) ? stripslashes( $arrmixValues['is_allocation'] ) : $arrmixValues['is_allocation'] );
		}

		if( true == isset( $arrmixValues['occupancy_type_id'] ) ) {
			$this->setOccupancyTypeId( $arrmixValues['occupancy_type_id'] );
		}

	}

	public function setPropertyTypeName( $strPropertyTypeName ) {
		$this->m_strPropertyTypeName = $strPropertyTypeName;
	}

	public function setCompanyRegionName( $strCompanyRegionName ) {
		$this->m_strCompanyRegionName = $strCompanyRegionName;
	}

	public function setStateName( $strStateName ) {
		$this->m_strStateName = $strStateName;
	}

	public function setProvince( $strProvince ) {
		$this->m_strtProvince = $strProvince;
	}

	public function setScheduledChargePostDurationDays( $intScheduledChargePostDurationDays ) {
		$this->m_intScheduledChargePostDurationDays = $intScheduledChargePostDurationDays;
	}

	public function setCompanyEmployees( $arrobjCompanyEmployees ) {
		$this->m_arrobjCompanyEmployees = $arrobjCompanyEmployees;
	}

	public function setSeoCity( $strSeoCity ) {
		$this->m_strSeoCity = $strSeoCity;
	}

	public function setSeoState( $strSeoState ) {
		$this->m_strSeoState = $strSeoState;
	}

	public function setAllowAllocations( $intAllowAllocations ) {
		$this->m_intAllowAllocations = $intAllowAllocations;
	}

	public function setSemPropertyDetailId( $intSemPropertyDetailId ) {
		$this->m_intSemPropertyDetailId = $intSemPropertyDetailId;
	}

	public function setShortDescription( $strShortDescription, $strLocaleCode = NULL ) {
		$this->m_strShortDescription = CStrings::strTrimDef( $strShortDescription, 240, NULL, true, true );
		parent::setShortDescription( $this->m_strShortDescription, $strLocaleCode );
	}

	public function setFullDescription( $strFullDescription, $strLocaleCode = NULL ) {
		$this->m_strFullDescription = CStrings::strTrimDef( $strFullDescription, -1, NULL, true, true );
		parent::setFullDescription( $this->m_strFullDescription, $strLocaleCode );
	}

	public function setPropertyCraigsListAreas( $arrobjPropertyCraigsListAreas ) {
		$this->m_arrobjPropertyCraigsListAreas = $arrobjPropertyCraigsListAreas;
	}

	public function setMiles( $fltMiles ) {
		$this->m_fltMiles = $fltMiles;
	}

	public function setWebsite( $objWebsite ) {
		$this->m_objWebsite = $objWebsite;
	}

	public function setOfficePhoneNumber( $strOfficePhoneNumber ) {
		$this->m_strOfficePhoneNumber = $strOfficePhoneNumber;
	}

	public function setCommunityWebsiteUri( $strCommunityWebsiteUri ) {
		$this->m_strCommunityWebsiteUri = $strCommunityWebsiteUri;
	}

	public function setCorporateWebsiteUri( $strCorporateWebsiteUri ) {
		$this->m_strCorporateWebsiteUri = $strCorporateWebsiteUri;
	}

	public function setPropertyConciergeServicesCount( $intPropertyConciergeServicesCount ) {
		$this->m_intPropertyConciergeServicesCount = $intPropertyConciergeServicesCount;
	}

	public function setPropertyFloorplansCount( $intPropertyFloorplansCount ) {
		$this->m_intPropertyFloorplansCount = $intPropertyFloorplansCount;
	}

	public function setPropertyFloorsCount( $intPropertyFloorsCount ) {
		$this->m_intPropertyFloorsCount = $intPropertyFloorsCount;
	}

	public function setCompanyMediaFile( $objCompanyMediaFile ) {
		$this->m_objCompanyMediaFile = $objCompanyMediaFile;
	}

	public function setPropertyBuildingsCount( $intPropertyBuildingsCount ) {
		$this->m_intPropertyBuildingsCount = $intPropertyBuildingsCount;
	}

	public function setPropertyUnitsCount( $intPropertyUnitsCount ) {
		$this->m_intPropertyUnitsCount = $intPropertyUnitsCount;
	}

	public function setPropertyApplicationsCount( $intPropertyApplicationsCount ) {
		$this->m_intPropertyApplicationsCount = $intPropertyApplicationsCount;
	}

	public function setPropertyPhotosCount( $intPropertyPhotosCount ) {
		$this->m_intPropertyPhotosCount = $intPropertyPhotosCount;
	}

	public function setPropertyVirtualToursCount( $intPropertyVirtualToursCount ) {
		$this->m_intPropertyVirtualToursCount = $intPropertyVirtualToursCount;
	}

	public function setPropertyWallCategoriesCount( $intPropertyWallCategoriesCount ) {
		$this->m_intPropertyWallCategoriesCount = $intPropertyWallCategoriesCount;
	}

	public function setPropertyClassifiedCategoriesCount( $intPropertyClassifiedCategoriesCount ) {
		$this->m_intPropertyClassifiedCategoriesCount = $intPropertyClassifiedCategoriesCount;
	}

	public function setPropertyUnitTypesCount( $intPropertyUnitTypesCount ) {
		$this->m_intPropertyUnitTypesCount = $intPropertyUnitTypesCount;
	}

	public function setPropertyLeadSourcesCount( $intPropertyLeadSourcesCount ) {
		$this->m_intPropertyLeadSourcesCount = $intPropertyLeadSourcesCount;
	}

	public function setCommunityAmenitiesCount( $intCommunityAmenitiesCount ) {
		$this->m_intCommunityAmenitiesCount = $intCommunityAmenitiesCount;
	}

	public function setApartmentAmenitiesCount( $intApartmentAmenitiesCount ) {
		$this->m_intApartmentAmenitiesCount = $intApartmentAmenitiesCount;
	}

	public function setPropertyMaintenanceLocationsCount( $intPropertyMaintenanceLocationsCount ) {
		$this->m_intPropertyMaintenanceLocationsCount = $intPropertyMaintenanceLocationsCount;
	}

	public function setPropertyMaintenanceProblemsCount( $intPropertyMaintenanceProblemsCount ) {
		$this->m_intPropertyMaintenanceProblemsCount = $intPropertyMaintenanceProblemsCount;
	}

	public function setPropertyMaintenanceTemplateId( $intPropertyMaintenanceTemplateId ) {
		$this->m_intPropertyMaintenanceTemplateId = $intPropertyMaintenanceTemplateId;
	}

	public function setIntegrationClientTypeId( $intIntegrationClientTypeId ) {
		$this->m_intIntegrationClientTypeId = $intIntegrationClientTypeId;
	}

	public function setPropertyNotificationEmailsCount( $intPropertyNotificationEmailsCount ) {
		$this->m_intPropertyNotificationEmailsCount = $intPropertyNotificationEmailsCount;
	}

	public function setPropertyPreferences( $arrobjPropertyPreferences ) {
		$this->m_arrobjPropertyPreferences = $arrobjPropertyPreferences;
	}

	public function setWebsitePreferences( $arrobjWebsitePreferences ) {
		$this->m_arrobjWebsitePreferences = $arrobjWebsitePreferences;
	}

	public function setIsValidRentArCodeId( $boolIsValidRentArCodeId ) {
		$this->m_boolIsValidRentArCodeId = $boolIsValidRentArCodeId;
	}

	public function setIsArMigrationMode( $boolIsArMigrationMode ) {
		$this->m_boolIsArMigrationMode = CStrings::strToBool( $boolIsArMigrationMode );
	}

	public function setYearBuilt( $strYearBuilt ) {
		$strYearBuilt = trim( $strYearBuilt );

		if( is_numeric( $strYearBuilt ) ) {
			if( $strYearBuilt >= 0 && $strYearBuilt <= 99 ) {
				$strYearBuilt += 1900;
			}
			$strYearBuilt = $strYearBuilt ? sprintf( '%02d/%02d/%04d', 1, 1, ( int ) $strYearBuilt ) : NULL;
		}
		$this->m_strYearBuilt = CStrings::strTrimDef( $strYearBuilt, -1, NULL, true );
	}

	public function setYearRemodeled( $strYearRemodeled ) {
		$strYearRemodeled = trim( $strYearRemodeled );

		if( is_numeric( $strYearRemodeled ) ) {
			if( $strYearRemodeled >= 0 && $strYearRemodeled <= 99 ) {
				$strYearRemodeled += 1900;
			}
			$strYearRemodeled = $strYearRemodeled ? sprintf( '%02d/%02d/%04d', 1, 1, ( int ) $strYearRemodeled ) : NULL;
		}
		$this->m_strYearRemodeled = CStrings::strTrimDef( $strYearRemodeled, -1, NULL, true );
	}

	public function setDefaults() {
		$this->setTimeZoneId( CTimeZone::MOUNTAIN_STANDARD_TIME );
		$this->setOccupancyTypeIds( [ COccupancyType::CONVENTIONAL ] );
		$this->setDefaultOccupancyTypeId( COccupancyType::CONVENTIONAL );
	}

	public function setResidentPortalUrl( $strResidentPortalUrl ) {
		$this->m_strResidentPortalUrl = $strResidentPortalUrl;
	}

	public function setRentArCode( $objRentArCode ) {
		$this->m_objRentArCode = $objRentArCode;
	}

	public function setLocalWebsiteDomain( $strLocalWebsiteDomain ) {
		$this->m_strLocalWebsiteDomain = $strLocalWebsiteDomain;
	}

	public function setWebsiteDomain( $strWebsiteDomain ) {
		$this->m_strWebsiteDomain = $strWebsiteDomain;
	}

	public function setMoreInfoAction( $strMoreInfoAction ) {
		$this->m_strMoreInfoAction = $strMoreInfoAction;
	}

	public function setHideMoreInfoButton( $intHideMoreInfoButton ) {
		$this->m_intHideMoreInfoButton = $intHideMoreInfoButton;
	}

	public function setShowApplicationFeeOption( $intShowApplicationFeeOption ) {
		$this->m_intShowApplicationFeeOption = $intShowApplicationFeeOption;
	}

	public function setOverviewMarketingMediaAssociation( $objOverviewMarketingMediaAssociation ) {
		$this->m_objOverviewMarketingMediaAssociation = $objOverviewMarketingMediaAssociation;
	}

	public function setOverviewMarketingMediaAssociationUrl( $strMarketingMediaAssociationUrl ) {
		$this->m_strOverviewMarketingMediaAssociationUrl = $strMarketingMediaAssociationUrl;
	}

	public function setLogoCompanyMediaFile( $objLogoCompanyMediaFile ) {
		$this->m_objLogoCompanyMediaFile = $objLogoCompanyMediaFile;
	}

	public function setPropertyLogoMarketingMediaAssociation( $objPropertyLogoMarketingMediaAssociation ) {
		$this->m_objPropertyLogoMarketingMediaAssociation = $objPropertyLogoMarketingMediaAssociation;
	}

	public function setPortfolioLogoMarketingMediaAssociation( $objPortfolioLogoMarketingMediaAssociation ) {
		$this->m_objPortfolioLogoMarketingMediaAssociation = $objPortfolioLogoMarketingMediaAssociation;
	}

	public function setCompanyMediaFiles( $arrobjCompanyMediaFiles ) {
		$this->m_arrobjCompanyMediaFiles = $arrobjCompanyMediaFiles;
	}

	public function setSpecials( $arrobjSpecials ) {
		$this->m_arrobjSpecials = $arrobjSpecials;
	}

	public function setLeaseTerms( $arrobjLeaseTerms ) {
		$this->m_arrobjLeaseTerms = $arrobjLeaseTerms;
	}

	public function setPropertyOfficeHours( $arrobjPropertyOfficeHours ) {
		$this->m_arrobjPropertyOfficeHours = $arrobjPropertyOfficeHours;
	}

	public function setPetRateAssociations( $arrobjPetRateAssociations ) {
		$this->m_arrobjPetRateAssociations = $arrobjPetRateAssociations;
	}

	public function setPropertyApplicationCount( $intPropertyApplicationCount ) {
		$this->m_intPropertyApplicationCount = $intPropertyApplicationCount;
	}

	public function setPrimaryPropertyAddress( $objPrimaryPropertyAddress ) {
		$this->m_objPrimaryPropertyAddress = $objPrimaryPropertyAddress;
	}

	public function setPrimaryPropertyPhoneNumber( $objPrimaryPropertyPhoneNumber ) {
		$this->m_objPrimaryPropertyPhoneNumber = $objPrimaryPropertyPhoneNumber;
	}

	public function setAvgSquareFeet( $intAvgSquareFeet ) {
		$this->m_intAvgSquareFeet = $intAvgSquareFeet;
	}

	public function setMinDeposit( $fltMinDeposit ) {
		$this->m_fltMinDeposit = $fltMinDeposit;
	}

	public function setMaxDeposit( $fltMaxDeposit ) {
		$this->m_fltMaxDeposit = $fltMaxDeposit;
	}

	public function setOfficeEmailAddress( $strOfficeEmailAddress ) {
		$this->m_objOfficeEmailAddress = $strOfficeEmailAddress;
	}

	public function setPrimaryEmailAddress( $strPrimaryEmailAddress ) {
		$this->m_strPrimaryEmailAddress = $strPrimaryEmailAddress;
	}

	public function setProspectPortalPropertyAnchor( $strProspectPortalPropertyAnchor ) {
		$this->m_strProspectPortalPropertyAnchor = $strProspectPortalPropertyAnchor;
	}

	public function setProspectPortalPropertyCaption( $strProspectPortalPropertyCaption ) {
		$this->m_strProspectPortalPropertyCaption = $strProspectPortalPropertyCaption;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName	= CStrings::strTrimDef( $strPropertyName, 50, NULL, true );
	}

	public function setOnlineApplicationUrl( $strOnlineApplicationUrl ) {
		$this->m_strOnlineApplicationUrl = $strOnlineApplicationUrl;
	}

	public function setLeadSources( $arrobjLeadSources ) {
		$this->m_arrobjLeadSources = $arrobjLeadSources;
	}

	public function setHasPublishedOnlineApplication( $boolHasPublishedOnlineApplication ) {
		$this->m_boolHasPublishedOnlineApplication = $boolHasPublishedOnlineApplication;
	}

	public function setPropertyFloorplans( $arrobjPropertyFloorplans ) {
		$this->m_arrobjPropertyFloorplans = $arrobjPropertyFloorplans;
	}

	public function setReview( $arrobjReviews ) {
		$this->m_arrobjReviews = $arrobjReviews;
	}

	public function setApplicationCount( $intApplicationCount ) {
		$this->m_intApplicationCount = $intApplicationCount;
	}

	public function setGuestCardCount( $intGuestCardCount ) {
		$this->m_intGuestCardCount = $intGuestCardCount;
	}

	public function setCompletedApplicationsCount( $intCompletedApplicationsCount ) {
		$this->m_intCompletedApplicationsCount = $intCompletedApplicationsCount;
	}

	public function setPropertyEventResultsCount( $intPropertyEventResultsCount ) {
		$this->m_intPropertyEventResultsCount = $intPropertyEventResultsCount;
	}

	public function setPropertyLeasingAgentsCount( $intPropertyLeasingAgentsCount ) {
		$this->m_intPropertyLeasingAgentsCount = $intPropertyLeasingAgentsCount;
	}

	public function setPropertyDetail( $objPropertyDetail ) {
		$this->m_objPropertyDetail = $objPropertyDetail;
	}

	public function setIsPropertyUnitsAvailable( $intIsPropertyUnitsAvailable ) {
		$this->m_intIsPropertyUnitsAvailable = $intIsPropertyUnitsAvailable;
	}

	public function setUnitSpacesCount( $intUnitSpacesCount ) {
		$this->m_intUnitSpacesCount = $intUnitSpacesCount;
	}

	public function setCommunityAmenities( $arrobjCommunityAmenities ) {
		$this->m_arrobjCommunityAmenities = $arrobjCommunityAmenities;
	}

	public function setPropertyUnits( $arrobjPropertyUnits ) {
		$this->m_arrobjPropertyUnits = $arrobjPropertyUnits;
	}

	public function setRateRanges( $arrobjRateRanges ) {
		$this->m_arrobjRateRanges = $arrobjRateRanges;
	}

	public function setPortfolioName( $strPortfolioName ) {
		$this->m_strPortfolioName = $strPortfolioName;
	}

	public function setPetPolicy( $strPetPolicy ) {
		$this->m_strPetPolicy = $strPetPolicy;
	}

	public function setParkingPolicy( $strParkingPolicy ) {
		$this->m_strParkingPolicy = $strParkingPolicy;
	}

	public function setPrimaryPhoneNumber( $strPrimaryPhoneNumber ) {
		$this->m_strPrimaryPhoneNumber = $strPrimaryPhoneNumber;
	}

	public function setOfficeFaxNumber( $strOfficeFaxNumber ) {
		return $this->m_strOfficeFaxNumber = $strOfficeFaxNumber;
	}

	public function setTimezoneUsingLatitudeAndLongitude( $objPrimaryPropertyAddress ) {

		$intTimezoneId = CTimeZone::MOUNTAIN_STANDARD_TIME;

		if( true == is_null( $objPrimaryPropertyAddress ) || false == valObj( $objPrimaryPropertyAddress, 'CPropertyAddress' ) ) {
			return $intTimezoneId;
		}
		if( true == is_null( $objPrimaryPropertyAddress->getLatitude() ) || true == is_null( $objPrimaryPropertyAddress->getLongitude() ) ) {
			return $intTimezoneId;
		}

		$objDocument = new DOMDocument();

		set_time_limit( 10 );

		if( $objDocument->load( 'http://ws.geonames.org/timezone?lat=' . $objPrimaryPropertyAddress->getLatitude() . '&lng=' . $objPrimaryPropertyAddress->getLongitude() ) ) {
			$objTimezone	= $objDocument->getElementsByTagName( 'gmtOffset' );
			$fltOffSet		= ( int ) $objTimezone->item( 0 )->nodeValue;

			$fltOffset = json_encode( $fltOffSet );
		}

		switch( $fltOffset ) {
			case '-12':
				$intTimezoneId = CTimeZone::INERNATIONAL_DATE_TIME_LINE_WEST;
				break;

			case '-11':
				$intTimezoneId = CTimeZone::BERING_TIME;
				break;

			case '-10':
				$intTimezoneId = CTimeZone::ALASKA_HAWAII_STANDARD_TIME;
				break;

			case '-9':
				$intTimezoneId = CTimeZone::YUKON_STANDARD_TIME;
				break;

			case '-8':
				$intTimezoneId = CTimeZone::PACIFIC_STANDARD_TIME;
				break;

			case '-7':
				$intTimezoneId = CTimeZone::MOUNTAIN_STANDARD_TIME;
				break;

			case '-6':
				$intTimezoneId = CTimeZone::CENTRAL_STANDARD_TIME;
				break;

			case '-5':
				$intTimezoneId = CTimeZone::EASTERN_STANDARD_TIME;
				break;

			case '-4':
				$intTimezoneId = CTimeZone::ATLANTIC_STANDARD_TIME;
				break;

			default:
				$intTimezoneId = CTimeZone::MOUNTAIN_STANDARD_TIME;
		}

		return $intTimezoneId;
	}

	public function setPropertyWebsitePreferences( $arrobjWebsitePreferences ) {
		$this->m_arrobjPropertyWebsitePreferences = $arrobjWebsitePreferences;
	}

	public function setApartmentAmenities( $arrobjApartmentAmenities ) {
		$this->m_arrobjApartmentAmenities = $arrobjApartmentAmenities;
	}

	public function setSemPropertyDetail( $objSemPropertyDetail ) {
		$this->m_objSemPropertyDetail = $objSemPropertyDetail;
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->m_strStreetLine1 = $strStreetLine1;
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->m_strStreetLine2 = $strStreetLine2;
	}

	public function setStreetLine3( $strStreetLine3 ) {
		$this->m_strStreetLine3 = $strStreetLine3;
	}

	public function setCity( $strCity ) {
		$this->m_strCity = $strCity;
	}

	public function setPostalCode( $strPostalCode ) {
		$this->m_strPostalCode = $strPostalCode;
	}

	public function setStateCode( $strStateCode ) {
		$this->m_strStateCode = $strStateCode;
	}

	public function setChildPropertyIds( $arrintChildPropertyIds ) {
		$this->m_arrintChildPropertyIds = $arrintChildPropertyIds;
	}

	public function setChildPropertyId( $objChildProperty ) {
		$this->m_objChildProperty = $objChildProperty;
	}

	public function setChildProperty( $objChildProperty ) {
		$this->m_objChildProperty = $objChildProperty;
	}

	public function setProperty( $objProperty ) {
		$this->m_objProperty = $objProperty;
	}

	public function setHasRemainingBudget( $boolHasRemainingBudget ) {
		$this->m_boolHasRemainingBudget = $boolHasRemainingBudget;
	}

	public function setIsInSemKeyword( $boolIsInSemKeyword ) {
		$this->m_boolIsInSemKeyword = $boolIsInSemKeyword;
	}

	public function setLastExternalClickDatetime( $strLastExternalClickDatetime ) {
		$this->m_strLastExternalClickDatetime = $strLastExternalClickDatetime;
	}

	public function setDatabaseId( $intDatabaseId ) {
		$this->m_intDatabaseId = $intDatabaseId;
	}

	public function setPropertyPhoneNumber( $objPropertyPhoneNumber ) {
		$this->m_objPropertyPhoneNumber = $objPropertyPhoneNumber;
	}

	public function setPropertyPreferenceKey( $strPropertyPreferenceKey ) {
		$this->m_strPropertyPreferenceKey = CStrings::strTrimDef( $strPropertyPreferenceKey, 64, NULL, true );
	}

	public function setPropertyPreferenceValue( $strPropertyPreferenceValue ) {
		$this->m_strPropertyPreferenceValue = CStrings::strTrimDef( $strPropertyPreferenceValue, -1, NULL, true );
	}

	public function setIsFeatured( $boolIsFeatured ) {
		$this->m_boolIsFeatured = CStrings::strToBool( $boolIsFeatured );
	}

	public function setTotalScore( $fltTotalScore ) {
		$this->m_fltTotalScore = $fltTotalScore;
	}

	public function setSemAndOrganicStat( $objSemAndOrganicStat ) {
		$this->m_objSemAndOrganicStat = $objSemAndOrganicStat;
	}

	public function setSemBudget( $objSemBudget ) {
		$this->m_objSemBudget = $objSemBudget;
	}

	public function setSemSetting( $objSemSetting ) {
		$this->m_objSemSetting = $objSemSetting;
	}

	public function setPropertyGlSetting( $objPropertyGlSetting ) {
		$this->m_objPropertyGlSetting = $objPropertyGlSetting;
	}

	public function setPropertyChargeSetting( $objPropertyChargeSetting ) {
		$this->m_objPropertyChargeSetting = $objPropertyChargeSetting;
	}

	public function setLongitude( $fltLongitude ) {
		$this->m_fltLongitude = $fltLongitude;
	}

	public function setLatitude( $fltLatitude ) {
		$this->m_fltLatitude = $fltLatitude;
	}

	public function setChildProperties( $arrobjChildProperties ) {
		$this->m_arrobjChildProperties = $arrobjChildProperties;
	}

	public function setClosedDays( $strClosedDays ) {
		$this->m_arrintClosedDays = $strClosedDays;
	}

	public function setSemPropertyStatusTypeId( $intSemPropertyStatusTypeId ) {
		$this->m_intSemPropertyStatusTypeId = $intSemPropertyStatusTypeId;
	}

	public function setIsLimited( $strIsLimited ) {
		$this->m_boolIsLimited = ( 't' == $strIsLimited ) ? true : false;
	}

	public function setResidentInsureSubDomain( $strResidentInsureSubDomain ) {
		$this->m_strResidentInsureSubDomain = $strResidentInsureSubDomain;
	}

	public function setResidentInsurePhoneNumber( $strResidentInsurePhoneNumber ) {
		$this->m_strResidentInsurePhoneNumber = $strResidentInsurePhoneNumber;
	}

	public function setPropertyManagerName( $strPropertyManagerName ) {
		$this->m_strPropertyManagerName = $strPropertyManagerName;
	}

	public function setPropertyDocuments( $arrobjPropertyDocuments ) {
		$this->m_arrobjPropertyDocuments = $arrobjPropertyDocuments;
	}

	public function setClientName( $strClientName ) {
		$this->m_strClientName = $strClientName;
	}

	public function setClientStatusTypeId( $intClientStatusTypeId ) {
		$this->m_intClientStatusTypeId = $intClientStatusTypeId;
	}

	public function setSalesTaxResultTypeId( $intSalesTaxResultTypeId ) {
		$this->m_intSalesTaxResultTypeId = $intSalesTaxResultTypeId;
	}

	public function setGlAccountProperties( $arrobjGlAccountProperties ) {
		$this->m_arrobjGlAccountProperties = $arrobjGlAccountProperties;
	}

	public function setGlTreeName( $strGlTreeName ) {
		return $this->m_strGlTreeName = $strGlTreeName;
	}

	public function setGlTreeId( $intGlTreeId ) {
		return $this->m_intGlTreeId = $intGlTreeId;
	}

	public function setInternetListingServiceId( $intInternetListingServiceId ) {
		return $this->m_intInternetListingServiceId = $intInternetListingServiceId;
	}

	public function setPropertyGroupId( $intPropertyGroupId ) {
		$this->m_intPropertyGroupId = $intPropertyGroupId;
	}

	public function setInternetListingServiceIds( $arrintInternetListingServiceIds ) {
		$this->m_arrintInternetListingServiceIds = explode( ',', $arrintInternetListingServiceIds );
	}

	public function setContractTerminationRequestDate( $strContractRequestDate ) {
		$this->m_strContractTerminationRequestDate = $strContractRequestDate;
	}

	public function setContractTerminationRequestId( $strContractTerminationRequestId ) {
		$this->m_strContractTerminationRequestId = $strContractTerminationRequestId;
	}

	public function setContractPropertyTerminationDate( $strContractPropertyTerminationDate ) {
		$this->m_strContractPropertyTerminationDate = $strContractPropertyTerminationDate;
	}

	public function setAllowMultipleAccountProperties( $boolAllowMultipleAccountProperties ) {
		$this->m_boolAllowMultipleAccountProperties = $boolAllowMultipleAccountProperties;
	}

	public function setMarketingPropertyName( $strMarketingPropertyName ) {
		$this->m_strMarketingPropertyName = $strMarketingPropertyName;
	}

	public function setRowCount( $intRowCount ) {
		$this->m_intRowCount = $intRowCount;
	}

	public function setIsBulkAddProperty( $boolBulkProperties ) {
		$this->m_boolIsBulkAddProperty = $boolBulkProperties;
	}

	public function setAmenityRateAssociation( $objApartmentAmenities ) {
		$this->m_arrobjApartmentAmenities[$objApartmentAmenities->getId()] = $objApartmentAmenities;
	}

	public function setSystemMessageEmailContent( $strSystemMessageEmailContent ) {
		$this->m_strSystemMessageEmailContent = $strSystemMessageEmailContent;
	}

	public function setAssociatedAccountId( $intAccountId ) {
		$this->m_intAssociatedAccountId = $intAccountId;
	}

	public function setSupportedLocaleCodes( $mixLocaleCodes ) {
		if( true == valStr( $mixLocaleCodes ) ) {
			$this->m_arrstrSupportedLocaleCodes = explode( ',', $mixLocaleCodes );
		} elseif( true == valArr( $mixLocaleCodes ) ) {
			$this->m_arrstrSupportedLocaleCodes = $mixLocaleCodes;
		} else {
			$this->m_arrstrSupportedLocaleCodes = [];
		}
	}

	public function setGlPostMonth( $strGlPostMonth ) {
		return $this->m_strGlPostMonth = $strGlPostMonth;
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->m_intOccupancyTypeId = $intOccupancyTypeId;
	}

	public function setRequiredParameters( $arrmixParameters ) {
		$this->m_arrmixRequiredParametes = $arrmixParameters;
	}

	public function setApPostMonth( $strGlPostMonth ) {
		$this->m_strApPostMonth = $strGlPostMonth;
	}

	public function setBankAccountTypeId( $intBankAccountTypeId ) {
		$this->m_intBankAccountTypeId = $intBankAccountTypeId;
	}

	public function setReimbursedPropertyId( $intReimbursedPropertyId ) {
		$this->m_intReimbursedPropertyId = $intReimbursedPropertyId;
	}

	public function setIsApMigrationMode( $boolIsApMigrationMode ) {
		$this->m_boolIsApMigrationMode = $boolIsApMigrationMode;
	}

	public function setIsAllocation( $intIsAllocation ) {
		$this->m_intIsAllocation = $intIsAllocation;
	}

	/**
	 * Create Functions
	 **/

	public function createCommunityAmenity() {
		$objCommunityAmenity = new CAmenityRateAssociation();
		$objCommunityAmenity->setCid( $this->m_intCid );
		$objCommunityAmenity->setArOriginId( CArOrigin::AMENITY );
		$objCommunityAmenity->setAmenityTypeId( CAmenityType::COMMUNITY );
		$objCommunityAmenity->setArCascadeId( CArCascade::PROPERTY );

		return $objCommunityAmenity;
	}

	public function createApartmentAmenity() {
		$objApartmentAmenity = new CAmenityRateAssociation();
		$objApartmentAmenity->setCid( $this->m_intCid );
		$objApartmentAmenity->setArOriginId( CArOrigin::AMENITY );
		$objApartmentAmenity->setAmenityTypeId( CAmenityType::APARTMENT );
		$objApartmentAmenity->setArCascadeId( CArCascade::PROPERTY );

		return $objApartmentAmenity;
	}

	public function createAddOnGroup( $intAddOnTypeId = NULL ) {
		$objAddOnGroup = new CAddOnGRoup();
		$objAddOnGroup->setCid( $this->getCid() );
		$objAddOnGroup->setPropertyId( $this->getId() );
		$objAddOnGroup->setAddOnTypeId( $intAddOnTypeId );
		$objAddOnGroup->setCategorizationTypeId( CCategorizationType::MANUAL );
		$objAddOnGroup->setLockToLeaseDates( true );

		return $objAddOnGroup;
	}

	public function createAddOnCategory( $intAddOnTypeId = NULL ) {
		$objAddOnCategory = new CAddOnCategory();
		$objAddOnCategory->setCid( $this->getCid() );
		$objAddOnCategory->setPropertyId( $this->getId() );
		$objAddOnCategory->setAddOnTypeId( $intAddOnTypeId );
		$objAddOnCategory->setIsPublished( true );

		return $objAddOnCategory;
	}

	public function createSpecialRateAssociation() {
		$objSpecialRateAssociation = new CSpecialRateAssociation();

		$objSpecialRateAssociation->setCid( $this->getCid() );
		$objSpecialRateAssociation->setPropertyId( $this->getId() );
		$objSpecialRateAssociation->setArCascadeId( CArCascade::PROPERTY );
		$objSpecialRateAssociation->setArCascadeReferenceId( $this->getId() );
		$objSpecialRateAssociation->setArOriginId( CArOrigin::SPECIAL );

		return $objSpecialRateAssociation;
	}

	public function createLeaseAssociation() {
		$objLeaseAssociation = new CLeaseAssociation();

		$objLeaseAssociation->setCid( $this->getCid() );
		$objLeaseAssociation->setPropertyId( $this->getId() );
		$objLeaseAssociation->setArCascadeId( CArCascade::PROPERTY );
		$objLeaseAssociation->setArCascadeReferenceId( $this->getId() );

		return $objLeaseAssociation;
	}

	public function createPropertyIdentificationType() {
		$objPropertyIdentificationType = new CPropertyIdentificationType();
		$objPropertyIdentificationType->setPropertyId( $this->getId() );
		$objPropertyIdentificationType->setCid( $this->getCid() );

		return $objPropertyIdentificationType;
	}

	public function createPropertyRateAssociation() {
		$objPropertyRateAssociation = new CAmenityRateAssociation();
		$objPropertyRateAssociation->setArOriginId( CArOrigin::AMENITY );
		$objPropertyRateAssociation->setArCascadeId( CArCascade::PROPERTY );
		$objPropertyRateAssociation->setPropertyId( $this->getId() );
		$objPropertyRateAssociation->setCid( $this->getCid() );

		return $objPropertyRateAssociation;
	}

	public function createPropertyUtility( $objDatabase ) {
		$objPropertyUtility = new CPropertyUtility();
		$objPropertyUtility->setCid( $this->getCid() );
		$objPropertyUtility->setPropertyId( $this->getId() );

		if( true == $this->isIntegrated( $objDatabase ) ) {
			$objPropertyUtility->setRemotePrimaryKey( $this->getRemotePrimaryKey() );
		}

		return $objPropertyUtility;
	}

	public function createPropertyUtilityType() {
		$objPropertyUtilityType = new CPropertyUtilityType();
		$objPropertyUtilityType->setDefaults();
		$objPropertyUtilityType->setCid( $this->getCid() );
		$objPropertyUtilityType->setPropertyId( $this->getId() );

		return $objPropertyUtilityType;
	}

	public function createLeaseTerm( $intTermMonth = NULL ) {
		$objLeaseTerm = new CLeaseTerm();
		$objLeaseTerm->setCid( $this->getCid() );
		$objLeaseTerm->setPropertyId( $this->getId() );

		if( 0 < ( int ) $intTermMonth ) {
			$objLeaseTerm->setName( ( int ) $intTermMonth . ( 1 < $intTermMonth ? ' months' : ' month' ) );
			$objLeaseTerm->setTermMonth( ( int ) $intTermMonth );
		}

		return $objLeaseTerm;
	}

	public function createSemAdGroupAssociation( $objSemAdGroup ) {
		$objSemAdGroupAssociation = new CSemAdGroupAssociation();
		$objSemAdGroupAssociation->setPropertyId( $this->getId() );
		$objSemAdGroupAssociation->setCid( $this->getCid() );
		$objSemAdGroupAssociation->setSemCampaignId( $objSemAdGroup->getSemCampaignId() );
		$objSemAdGroupAssociation->setSemAdGroupId( $objSemAdGroup->getId() );
		$objSemAdGroupAssociation->setAssociationDatetime( ' NOW() ' );

		return $objSemAdGroupAssociation;
	}

	public function createSemKeywordAssociation( $objSemAdGroup ) {
		$objSemKeywordAssociation = new CSemKeywordAssociation();
		$objSemKeywordAssociation->setPropertyId( $this->getId() );
		$objSemKeywordAssociation->setCid( $this->getCid() );
		$objSemKeywordAssociation->setSemCampaignId( $objSemAdGroup->getSemCampaignId() );
		$objSemKeywordAssociation->setSemAdGroupId( $objSemAdGroup->getId() );
		$objSemKeywordAssociation->setAssociationDatetime( ' NOW() ' );

		return $objSemKeywordAssociation;
	}

	public function createSemSetting() {
		$objSemSetting = new CSemSetting();
		$objSemSetting->setCid( $this->getCid() );
		$objSemSetting->setPropertyId( $this->getId() );
		$objSemSetting->setFrequencyId( CFrequency::MONTHLY );
		$objSemSetting->setStartDate( date( 'm/d/Y' ) );

		return $objSemSetting;
	}

	public function createSemTransaction() {
		$objSemTransaction = new CSemTransaction();
		$objSemTransaction->setCid( $this->getCid() );
		$objSemTransaction->setPropertyId( $this->getId() );
		$objSemTransaction->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );

		if( true == isset ( $_SERVER['HTTP_REFERER'] ) ) {
			$objSemTransaction->setReferringUrl( $_SERVER['HTTP_REFERER'] );
		}
		$objSemTransaction->setIpAddress( getRemoteIpAddress() );

		return $objSemTransaction;
	}

	public function createSemAndOrganicStat() {
		$objSemAndOrganicStat = new CSemAndOrganicStat();
		$objSemAndOrganicStat->setCid( $this->getCid() );
		$objSemAndOrganicStat->setPropertyId( $this->getId() );
		$objSemAndOrganicStat->setTrafficDate( date( 'm/d/Y' ) );

		return $objSemAndOrganicStat;
	}

	public function createSemPropertyDetail() {
		$objSemPropertyDetail = new CSemPropertyDetail();
		$objSemPropertyDetail->setCid( $this->getCid() );
		$objSemPropertyDetail->setPropertyId( $this->getId() );

		return $objSemPropertyDetail;
	}

	public function createLeaseExpirationLimit() {
		$objLeaseExpirationLimit = new CLeaseExpirationLimit();
		$objLeaseExpirationLimit->setCid( $this->getCid() );
		$objLeaseExpirationLimit->setPropertyId( $this->getId() );

		return $objLeaseExpirationLimit;
	}

	public function createArPaymentTransmission() {
		$objArPaymentTransmission = new CArPaymentTransmission();
		$objArPaymentTransmission->setCid( $this->getCid() );
		$objArPaymentTransmission->setPropertyId( $this->getId() );
		$objArPaymentTransmission->setTransmissionDatetime( date( 'm/d/Y H:i:s' ) );

		return $objArPaymentTransmission;
	}

	public function createCompanyMediaFolders( $objRootMediaLibraryCompanyMediaFolder, $objDatabase ) {
		$objPropertyCompanyMediaFolder = $this->createCompanyMediaFolder();
		$objPropertyCompanyMediaFolder->setParentMediaFolderId( $objRootMediaLibraryCompanyMediaFolder->getId() );
		$objPropertyCompanyMediaFolder->setFolderName( $this->getPropertyName() );
		$objPropertyCompanyMediaFolder->setId( $objPropertyCompanyMediaFolder->fetchNextId( $objDatabase ) );

		$objPropertyPhotosCompanyMediaFolder = $this->createCompanyMediaFolder();
		$objPropertyPhotosCompanyMediaFolder->setParentMediaFolderId( $objPropertyCompanyMediaFolder->getId() );
		$objPropertyPhotosCompanyMediaFolder->setFolderName( 'Photos' );
		$objPropertyPhotosCompanyMediaFolder->setOrderNum( 1 );

		$objPropertyVirtualToursCompanyMediaFolder = $this->createCompanyMediaFolder();
		$objPropertyVirtualToursCompanyMediaFolder->setParentMediaFolderId( $objPropertyCompanyMediaFolder->getId() );
		$objPropertyVirtualToursCompanyMediaFolder->setFolderName( 'Virtual Tours' );
		$objPropertyVirtualToursCompanyMediaFolder->setOrderNum( 2 );

		$objPropertyFloorplansCompanyMediaFolder = $this->createCompanyMediaFolder();
		$objPropertyFloorplansCompanyMediaFolder->setParentMediaFolderId( $objPropertyCompanyMediaFolder->getId() );
		$objPropertyFloorplansCompanyMediaFolder->setFolderName( 'Floorplans' );
		$objPropertyFloorplansCompanyMediaFolder->setOrderNum( 3 );

		$objPropertyOtherCompanyMediaFolder = $this->createCompanyMediaFolder();
		$objPropertyOtherCompanyMediaFolder->setParentMediaFolderId( $objPropertyCompanyMediaFolder->getId() );
		$objPropertyOtherCompanyMediaFolder->setFolderName( 'Other' );
		$objPropertyOtherCompanyMediaFolder->setOrderNum( 4 );

		return [ $objPropertyCompanyMediaFolder, $objPropertyPhotosCompanyMediaFolder, $objPropertyVirtualToursCompanyMediaFolder, $objPropertyFloorplansCompanyMediaFolder, $objPropertyOtherCompanyMediaFolder ];
	}

	public function createLease() {
		$objLease = new CLease();
		$objLease->setCid( $this->getCid() );
		$objLease->setPropertyId( $this->getId() );
		$objLease->setLeaseIntervalTypeId( CLeaseIntervalType::APPLICATION );

		return $objLease;
	}

	public function createDefaultPropertyPreferences() {
		$objPropertyPreferenceProrateEnable = $this->createPropertyPreference();
		$objPropertyPreferenceProrateEnable->setKey( 'FIXED_PRORATE_ENABLE' );
		$objPropertyPreferenceProrateEnable->setValue( 0 );

		$objPropertyPreferenceAutoPostRecurringPayments = $this->createPropertyPreference();
		$objPropertyPreferenceAutoPostRecurringPayments->setKey( 'AUTO_POST_RECURRING_PAYMENTS' );
		$objPropertyPreferenceAutoPostRecurringPayments->setValue( 1 );

		return [ $objPropertyPreferenceProrateEnable, $objPropertyPreferenceAutoPostRecurringPayments ];
	}

	public function createDefaultPaymentPropertyPreferences() {
		$objPropertyPreferenceAllowWaiverOfConvenienceFees = $this->createPropertyPreference();
		$objPropertyPreferenceAllowWaiverOfConvenienceFees->setKey( 'ALLOW_WAIVER_OF_CONVENIENCE_FEES' );
		$objPropertyPreferenceAllowWaiverOfConvenienceFees->setValue( 1 );

		$objPropertyPreferenceRequireWaiverOfConvenienceFees = $this->createPropertyPreference();
		$objPropertyPreferenceRequireWaiverOfConvenienceFees->setKey( 'REQUIRE_WAIVER_OF_CONVENIENCE_FEES' );
		$objPropertyPreferenceRequireWaiverOfConvenienceFees->setValue( 1 );

		return [ $objPropertyPreferenceAllowWaiverOfConvenienceFees, $objPropertyPreferenceRequireWaiverOfConvenienceFees ];
	}

	public function createDefaultResidentPaymentPropertyPreferences() {
		$objPropertyPreferenceAllowStaticAndVariableRecurringPayment = $this->createPropertyPreference();
		$objPropertyPreferenceAllowStaticAndVariableRecurringPayment->setKey( 'ALLOW_STATIC_AND_VARIABLE_RECURRING_PAYMENTS ' );
		$objPropertyPreferenceAllowStaticAndVariableRecurringPayment->setValue( 1 );

		return $objPropertyPreferenceAllowStaticAndVariableRecurringPayment;
	}

	public function createDefaultResidentPortalPropertyPreference() {
		$objPropertyPreferenceTurnOffResidentInsurePortion = $this->createPropertyPreference();
		$objPropertyPreferenceTurnOffResidentInsurePortion->setKey( 'TURN_OFF_RESIDENT_INSURE_PORTION' );
		$objPropertyPreferenceTurnOffResidentInsurePortion->setValue( 1 );

		return $objPropertyPreferenceTurnOffResidentInsurePortion;
	}

	public function createDefaultAssetLocationPropertyPreference() {
		$objPropertyPreferenceDefaultAssetLocation = $this->createPropertyPreference();
		$objPropertyPreferenceDefaultAssetLocation->setKey( CPropertyPreference::KEY_DEFAULT_ASSET_LOCATION_ID );
		$objPropertyPreferenceDefaultAssetLocation->setPropertySettingKeyId( CPropertySettingKey::DEFAULT_ASSET_LOCATION_ID );

		return $objPropertyPreferenceDefaultAssetLocation;
	}

	public function createDefaultBlockAllPaymentsFromEvictingPropertyPreference() {
		$objPropertyPreferenceBlockAllPaymentsFromEvicting = $this->createPropertyPreference();
		$objPropertyPreferenceBlockAllPaymentsFromEvicting->setKey( 'BLOCK_ALL_PAYMENTS_FROM_EVICTING' );
		$objPropertyPreferenceBlockAllPaymentsFromEvicting->setValue( 1 );

		return $objPropertyPreferenceBlockAllPaymentsFromEvicting;
	}

	public function createCompanyMediaFolder() {
		$objCompanyMediaFolder = new CCompanyMediaFolder();
		$objCompanyMediaFolder->setCid( $this->m_intCid );
		$objCompanyMediaFolder->setPropertyId( $this->getId() );

		return $objCompanyMediaFolder;
	}

	public function createRate() {
		$objRate = new CRate();
		$objRate->setCid( $this->m_intCid );
		$objRate->setPropertyId( $this->getId() );
		$objRate->setArCascadeId( CArCascade::PROPERTY );
		$objRate->setArCascadeReferenceId( $this->getId() );

		return $objRate;
	}

	public function createRateAssociation() {
		$objRateAssociation = new CRateAssociation();
		$objRateAssociation->setCid( $this->m_intCid );
		$objRateAssociation->setPropertyId( $this->m_intId );
		$objRateAssociation->setArCascadeId( CArCascade::PROPERTY );
		$objRateAssociation->setArCascadeReferenceId( $this->m_intId );

		return $objRateAssociation;
	}

	public function createIntegrationClientKeyValue() {
		$objIntegrationClientKeyValue = new CIntegrationClientKeyValue();
		$objIntegrationClientKeyValue->setCid( $this->m_intCid );
		$objIntegrationClientKeyValue->setPropertyId( $this->getId() );
		$objIntegrationClientKeyValue->setKey( 'REMOTE_WAITING_LIST_UNIT_ID' );

		return $objIntegrationClientKeyValue;
	}

	public function createPropertyMaintenanceProblem() {
		$objPropertyMaintenanceProblem = new CPropertyMaintenanceProblem();
		$objPropertyMaintenanceProblem->setCid( $this->m_intCid );
		$objPropertyMaintenanceProblem->setPropertyId( $this->getId() );

		return $objPropertyMaintenanceProblem;
	}

	public function createPropertyMaintenanceLocation() {
		$objPropertyMaintenanceLocation = new CPropertyMaintenanceLocation();
		$objPropertyMaintenanceLocation->setCid( $this->m_intCid );
		$objPropertyMaintenanceLocation->setPropertyId( $this->getId() );

		return $objPropertyMaintenanceLocation;
	}

	public function createPropertyMaintenanceTemplate() {
		$objPropertyMaintenanceTemplate = new CPropertyMaintenanceTemplate();
		$objPropertyMaintenanceTemplate->setCid( $this->m_intCid );
		$objPropertyMaintenanceTemplate->setPropertyId( $this->getId() );

		return $objPropertyMaintenanceTemplate;
	}

	public function createOfficePhoneNumber() {
		$objOfficePhoneNumber = new CPropertyPhoneNumber();
		$objOfficePhoneNumber->setCid( $this->m_intCid );
		$objOfficePhoneNumber->setPropertyId( $this->getId() );
		$objOfficePhoneNumber->setPhoneNumberTypeId( CPhoneNumberType::OFFICE );
		$objOfficePhoneNumber->setIsPublished( 1 );

		return $objOfficePhoneNumber;
	}

	public function createOfficeFaxNumber() {
		$objOfficeFaxNumber = new CPropertyPhoneNumber();
		$objOfficeFaxNumber->setCid( $this->m_intCid );
		$objOfficeFaxNumber->setPropertyId( $this->getId() );
		$objOfficeFaxNumber->setPhoneNumberTypeId( CPhoneNumberType::FAX );
		$objOfficeFaxNumber->setIsPublished( 1 );

		return $objOfficeFaxNumber;
	}

	public function createResidentOfficePhoneNumber() {
		$objResidentOfficePhoneNumber = new CPropertyPhoneNumber();
		$objResidentOfficePhoneNumber->setCid( $this->m_intCid );
		$objResidentOfficePhoneNumber->setPropertyId( $this->getId() );
		$objResidentOfficePhoneNumber->setPhoneNumberTypeId( CPhoneNumberType::RESIDENT_OFFICE );
		$objResidentOfficePhoneNumber->setIsPublished( 1 );

		return $objResidentOfficePhoneNumber;
	}

	public function createPropertyArea() {
		$objPropertyArea = new CPropertyArea();
		$objPropertyArea->setCid( $this->getCid() );
		$objPropertyArea->setPropertyId( $this->getId() );
		$objPropertyArea->setIsPublished( 1 );

		return $objPropertyArea;
	}

	public function createMaintenancePhoneNumber() {
		$objMaintenancePhoneNumber = new CPropertyPhoneNumber();
		$objMaintenancePhoneNumber->setCid( $this->m_intCid );
		$objMaintenancePhoneNumber->setPropertyId( $this->getId() );
		$objMaintenancePhoneNumber->setPhoneNumberTypeId( CPhoneNumberType::MAINTENANCE );
		$objMaintenancePhoneNumber->setIsPublished( 1 );

		return $objMaintenancePhoneNumber;
	}

	public function createPropertyLeadSource() {
		$objPropertyLeadSource = new CPropertyLeadSource();
		$objPropertyLeadSource->setCid( $this->m_intCid );
		$objPropertyLeadSource->setPropertyId( $this->getId() );

		return $objPropertyLeadSource;
	}

	public function createCallPhoneNumber() {
		$objCallPhoneNumber = new CCallPhoneNumber();
		$objCallPhoneNumber->setCid( $this->m_intCid );
		$objCallPhoneNumber->setPropertyId( $this->getId() );

		return $objCallPhoneNumber;
	}

	public function createMaintenanceEmergencyPhoneNumber() {
		$objMaintenanceEmergencyPhoneNumber = new CPropertyPhoneNumber();
		$objMaintenanceEmergencyPhoneNumber->setCid( $this->m_intCid );
		$objMaintenanceEmergencyPhoneNumber->setPropertyId( $this->getId() );
		$objMaintenanceEmergencyPhoneNumber->setPhoneNumberTypeId( CPhoneNumberType::MAINTENANCE_EMERGENCY );
		$objMaintenanceEmergencyPhoneNumber->setIsPublished( 1 );

		return $objMaintenanceEmergencyPhoneNumber;
	}

	public function createOfficeEmailAddress() {
		$objPropertyEmailAddress = new CPropertyEmailAddress();
		$objPropertyEmailAddress->setCid( $this->m_intCid );
		$objPropertyEmailAddress->setPropertyId( $this->getId() );
		$objPropertyEmailAddress->setEmailAddressTypeId( CEmailAddressType::PRIMARY );
		$objPropertyEmailAddress->setIsPublished( 1 );

		return $objPropertyEmailAddress;
	}

	public function createOfficeHours() {
		$objPropertyOfficeHours = new CPropertyOfficeHour();
		$objPropertyOfficeHours->setCid( $this->m_intCid );
		$objPropertyOfficeHours->setPropertyId( $this->getId() );
		$objPropertyOfficeHours->setIsPublished( 1 );

		return $objPropertyOfficeHours;
	}

	public function createPropertyOfficeHour() {
		$objPropertyOfficeHour = new CPropertyOfficeHour();
		$objPropertyOfficeHour->setCid( $this->m_intCid );
		$objPropertyOfficeHour->setPropertyId( $this->getId() );
		$objPropertyOfficeHour->setIsPublished( 1 );

		return $objPropertyOfficeHour;
	}

	public function createPrimaryAddress() {
		$objPrimaryAddress = new CPropertyAddress();
		$objPrimaryAddress->setCid( $this->m_intCid );
		$objPrimaryAddress->setPropertyId( $this->getId() );
		$objPrimaryAddress->setAddressTypeId( CAddressType::PRIMARY );
		$objPrimaryAddress->setIsPublished( 1 );
		$objPrimaryAddress->setTimeZoneId( CTimeZone::MOUNTAIN_STANDARD_TIME );

		return $objPrimaryAddress;
	}

	public function createPropertySubsidyDetail() {
		$objPropertySubsidyDetail = new CPropertySubsidyDetail();
		$objPropertySubsidyDetail->setCid( $this->m_intCid );
		$objPropertySubsidyDetail->setPropertyId( $this->getId() );

		return $objPropertySubsidyDetail;
	}

	public function createSubsidyCertification() {
		$objSubsidyCertification = new CSubsidyCertification();
		$objSubsidyCertification->setCid( $this->m_intCid );
		$objSubsidyCertification->setPropertyId( $this->getId() );

		return $objSubsidyCertification;
	}

	public function createSubsidyContract() {
		$objSubsidyContract = new CSubsidyContract();
		$objSubsidyContract->setCid( $this->m_intCid );
		$objSubsidyContract->setPropertyId( $this->getId() );

		return $objSubsidyContract;
	}

	public function createPropertyIntegrationDatabase() {
		$objPropertyIntegrationDatabase = new CPropertyIntegrationDatabase();
		$objPropertyIntegrationDatabase->setCid( $this->m_intCid );
		$objPropertyIntegrationDatabase->setPropertyId( $this->getId() );

		return $objPropertyIntegrationDatabase;
	}

	public function createRemittanceAddress() {
		$objRemittanceAddress = new CPropertyAddress();
		$objRemittanceAddress->setCid( $this->m_intCid );
		$objRemittanceAddress->setPropertyId( $this->getId() );
		$objRemittanceAddress->setAddressTypeId( CAddressType::PAYMENT );
		$objRemittanceAddress->setIsPublished( 1 );

		return $objRemittanceAddress;
	}

	public function createPrimaryOnSiteContact() {
		$objPrimaryOnsiteContact = new CPropertyOnSiteContact();
		$objPrimaryOnsiteContact->setCid( $this->m_intCid );
		$objPrimaryOnsiteContact->setPropertyId( $this->getId() );
		$objPrimaryOnsiteContact->setOnSiteContactTypeId( CContactType::MANAGER );
		$objPrimaryOnsiteContact->setIsPublished( 1 );

		return $objPrimaryOnsiteContact;
	}

	public function createPropertyAddress() {
		$objPropertyAddress = new CPropertyAddress();
		$objPropertyAddress->setCid( $this->m_intCid );
		$objPropertyAddress->setPropertyId( $this->getId() );

		return $objPropertyAddress;
	}

	public function createPropertyBuilding() {
		$objPropertyBuilding = new CPropertyBuilding();
		$objPropertyBuilding->setCid( $this->m_intCid );
		$objPropertyBuilding->setPropertyId( $this->getId() );

		return $objPropertyBuilding;
	}

	public function createPropertyUnit() {
		$objPropertyUnit = new CPropertyUnit();
		$objPropertyUnit->setDefaults();
		$objPropertyUnit->setCid( $this->m_intCid );
		$objPropertyUnit->setPropertyId( $this->getId() );

		return $objPropertyUnit;
	}

	public function createPropertyFloorplan() {
		$objPropertyFloorplan = new CPropertyFloorplan();
		$objPropertyFloorplan->setCid( $this->m_intCid );
		$objPropertyFloorplan->setPropertyId( $this->getId() );

		return $objPropertyFloorplan;
	}

	public function createPropertyFloor() {
		$objPropertyFloor = new CPropertyFloor();
		$objPropertyFloor->setCid( $this->m_intCid );
		$objPropertyFloor->setPropertyId( $this->getId() );

		return $objPropertyFloor;
	}

	public function createUnitType() {
		$objUnitType = new CUnitType();
		$objUnitType->setCid( $this->m_intCid );
		$objUnitType->setPropertyId( $this->getId() );

		return $objUnitType;
	}

	public function createPropertyMaintenanceLocationProblem() {
		$objPropertyMaintenanceLocationProblem = new CPropertyMaintenanceLocationProblem();
		$objPropertyMaintenanceLocationProblem->setCid( $this->m_intCid );
		$objPropertyMaintenanceLocationProblem->setPropertyId( $this->getId() );

		return $objPropertyMaintenanceLocationProblem;
	}

	public function createPropertyCategory() {
		$objPropertyCategory = new CPropertyCategory();
		$objPropertyCategory->setCid( $this->m_intCid );
		$objPropertyCategory->setPropertyId( $this->getId() );

		return $objPropertyCategory;
	}

	public function createPropertyEmailAddress() {
		$objPropertyEmailAddress = new CPropertyEmailAddress();
		$objPropertyEmailAddress->setCid( $this->m_intCid );
		$objPropertyEmailAddress->setPropertyId( $this->getId() );

		return $objPropertyEmailAddress;
	}

	public function createPropertyDetail() {
		$objPropertyDetail = new CPropertyDetail();
		$objPropertyDetail->setCid( $this->m_intCid );
		$objPropertyDetail->setPropertyId( $this->getId() );
		$objPropertyDetail->setLateFeeLastPostedOn( date( 'm/d/Y' ) );

		return $objPropertyDetail;
	}

	public function createPropertyPhoneNumber() {
		$objPropertyPhoneNumber = new CPropertyPhoneNumber();
		$objPropertyPhoneNumber->setCid( $this->m_intCid );
		$objPropertyPhoneNumber->setPropertyId( $this->getId() );

		return $objPropertyPhoneNumber;
	}

	public function createPropertyDocument() {
		$objPropertyDocument = new CPropertyDocument();
		$objPropertyDocument->setCid( $this->m_intCid );
		$objPropertyDocument->setPropertyId( $this->getId() );

		return $objPropertyDocument;
	}

	public function createPropertyMedia() {
		$objPropertyMedia = new CPropertyMedia();
		$objPropertyMedia->setCid( $this->m_intCid );
		$objPropertyMedia->setPropertyId( $this->getId() );

		return $objPropertyMedia;
	}

	public function createPropertyMerchantAccount() {
		$objPropertyMerchantAccount = new CPropertyMerchantAccount();
		$objPropertyMerchantAccount->setCid( $this->m_intCid );
		$objPropertyMerchantAccount->setPropertyId( $this->getId() );

		return $objPropertyMerchantAccount;
	}

	public function createPropertySetting( $boolIsNotification = false ) {
		$objPropertySetting = new CPropertyPreference();
		if( true == $boolIsNotification ) {
			$objPropertySetting = new CPropertyNotification();
		}
		$objPropertySetting->setCid( $this->m_intCid );
		$objPropertySetting->setPropertyId( $this->getId() );

		return $objPropertySetting;
	}

	public function createSettingsTemplateAssignment() {
		$objSettingsTemplateAssignment = new CSettingsTemplateAssignment();
		$objSettingsTemplateAssignment->setCid( $this->getCid() );
		$objSettingsTemplateAssignment->setPropertyId( $this->getId() );

		return $objSettingsTemplateAssignment;
	}

	public function createPropertyPreference() {
		$objPropertyPreference = new CPropertyPreference();
		$objPropertyPreference->setCid( $this->m_intCid );
		$objPropertyPreference->setPropertyId( $this->getId() );

		return $objPropertyPreference;
	}

	public function createPropertyKeyValue( $strKey, $strValue ) {
		$objPropertyKeyValue = new CPropertyKeyValue();
		$objPropertyKeyValue->setCid( $this->m_intCid );
		$objPropertyKeyValue->setPropertyId( $this->getId() );
		$objPropertyKeyValue->setKey( $strKey );
		$objPropertyKeyValue->setValue( $strValue );

		return $objPropertyKeyValue;
	}

	public function createPropertyAmenityAvailability() {
		$objPropertyAmenityAvailability = new CPropertyAmenityAvailability();
		$objPropertyAmenityAvailability->setPropertyId( $this->getId() );
		$objPropertyAmenityAvailability->setCid( $this->m_intCid );

		return $objPropertyAmenityAvailability;
	}

	public function createFloorplanAmenity() {
		$objFloorplanAmenity = new CFloorplanAmenity();
		$objFloorplanAmenity->setPropertyId( $this->getId() );
		$objFloorplanAmenity->setCid( $this->m_intCid );
		$objFloorplanAmenity->setIsOptional( 0 );

		return $objFloorplanAmenity;
	}

	public function createAmenityRateAssociation() {
		$objAmenityRateAssociation = new CAmenityRateAssociation();
		$objAmenityRateAssociation->setPropertyId( $this->getId() );
		$objAmenityRateAssociation->setCid( $this->m_intCid );
		$objAmenityRateAssociation->setIsOptional( false );

		return $objAmenityRateAssociation;
	}

	public function createPropertyEvent() {
		$objPropertyEvent = new CPropertyEvent();
		$objPropertyEvent->setPropertyId( $this->getId() );
		$objPropertyEvent->setCid( $this->m_intCid );

		return $objPropertyEvent;
	}

	public function createAmenityReservation() {
		$objAmenityReservation = new CPropertyAddOnReservation();
		$objAmenityReservation->setPropertyId( $this->getId() );
		$objAmenityReservation->setCid( $this->m_intCid );

		return $objAmenityReservation;
	}

	public function createPropertyAnnouncement() {
		$objPropertyAnnouncement = new CPropertyAnnouncement();
		$objPropertyAnnouncement->setPropertyId( $this->getId() );
		$objPropertyAnnouncement->setCid( $this->m_intCid );

		return $objPropertyAnnouncement;
	}

	public function createPropertyConciergeService() {
		$objPropertyConciergeService = new CPropertyConciergeService();
		$objPropertyConciergeService->setPropertyId( $this->getId() );
		$objPropertyConciergeService->setCid( $this->m_intCid );

		return $objPropertyConciergeService;
	}

	public function createAccount( $objClientDatabase ) {
		$objAccount = new CAccount();
		$objAccount->setAccountName( $this->getPropertyName() );
		$objAccount->setCid( $this->m_intCid );
		$objAccount->setInvoicesEmailed( 1 );
		$objAccount->setInvoicesMailed( 1 );
		$objAccount->setBilltoEmailAddress( $this->getOfficeEmailAddress() );

		$objPropertyAddress = $this->fetchPrimaryAddress( $objClientDatabase );

		if( true == isset( $objPropertyAddress ) && true == valObj( $objPropertyAddress, 'CPropertyAddress' ) ) {

			$objAccount->setBilltoStreetLine1( $objPropertyAddress->getStreetLine1() );
			$objAccount->setBilltoStreetLine2( $objPropertyAddress->getStreetLine2() );
			$objAccount->setBilltoStreetLine3( $objPropertyAddress->getStreetLine3() );
			$objAccount->setBilltoCity( $objPropertyAddress->getCity() );
			$objAccount->setBilltoStateCode( $objPropertyAddress->getStateCode() );
			$objAccount->setBilltoPostalCode( $objPropertyAddress->getPostalCode() );

		}

		return $objAccount;
	}

	public function createWebsite() {
		$objWebsite = new CWebsite();
		$objWebsite->setCid( $this->getCid() );
		$objWebsite->setName( $this->getPropertyName() );
		$objWebsite->setSubDomain( $this->getPropertyName() );
		$objWebsite->setCaption( $this->getPropertyName() );
		$objWebsite->setCompanyLogo( $this->getPropertyName() );

		return $objWebsite;
	}

	public function createPropertyArCode() {
		$objPropertyArCode = new CPropertyArCode();
		$objPropertyArCode->setCid( $this->getCid() );
		$objPropertyArCode->setPropertyId( $this->getId() );
		$objPropertyArCode->setRemotePrimaryKey( 1 );

		return $objPropertyArCode;
	}

	public function createInspection() {
		$objInspection = new CInspection();
		$objInspection->setPropertyId( $this->getId() );
		$objInspection->setCid( $this->getCid() );

		return $objInspection;
	}

	public function createMaintenanceRequest() {
		$objMaintenanceRequest = new CMaintenanceRequest();
		$objMaintenanceRequest->setPropertyId( $this->getId() );
		$objMaintenanceRequest->setCid( $this->getCid() );
		$objMaintenanceRequest->setIsFloatingMaintenanceRequest( true );

		return $objMaintenanceRequest;
	}

	public function createPropertyLeasingAgent() {
		$objPropertyLeasingAgent = new CPropertyLeasingAgent();
		$objPropertyLeasingAgent->setPropertyId( $this->getId() );
		$objPropertyLeasingAgent->setCid( $this->getCid() );

		return $objPropertyLeasingAgent;
	}

	public function createPropertyEventResult() {
		$objPropertyEventResult = new CPropertyEventResult();
		$objPropertyEventResult->setDefaults();
		$objPropertyEventResult->setPropertyId( $this->getId() );
		$objPropertyEventResult->setCid( $this->getCid() );

		return $objPropertyEventResult;
	}

	public function createCustomerClassified() {
		$objCustomerClassified = new CCustomerClassified();
		$objCustomerClassified->setCid( $this->getCid() );
		$objCustomerClassified->setPropertyId( $this->getId() );

		return $objCustomerClassified;
	}

	public function createPropertyClassifiedCategory() {
		$objPropertyClassifiedCategory = new CPropertyClassifiedCategory();
		$objPropertyClassifiedCategory->setPropertyId( $this->getId() );
		$objPropertyClassifiedCategory->setCid( $this->m_intCid );

		return $objPropertyClassifiedCategory;
	}

	public function createPropertyMaintenanceStatus() {
		$objPropertyMaintenanceStatus = new CPropertyMaintenanceStatus();
		$objPropertyMaintenanceStatus->setPropertyId( $this->getId() );
		$objPropertyMaintenanceStatus->setCid( $this->m_intCid );

		return $objPropertyMaintenanceStatus;
	}

	public function createPropertyMaintenancePriority() {
		$objPropertyMaintenancePriority = new CPropertyMaintenancePriority();
		$objPropertyMaintenancePriority->setPropertyId( $this->getId() );
		$objPropertyMaintenancePriority->setCid( $this->m_intCid );

		return $objPropertyMaintenancePriority;
	}

	public function createPropertyNotification() {
		$objPropertyNotification = new CPropertyNotification();
		$objPropertyNotification->setCid( $this->m_intCid );
		$objPropertyNotification->setPropertyId( $this->getId() );

		return $objPropertyNotification;
	}

	public function createPropertySellingPoint() {
		$objPropertySellingPoint = new CPropertySellingPoint();
		$objPropertySellingPoint->setCid( $this->m_intCid );
		$objPropertySellingPoint->setPropertyId( $this->getId() );

		return $objPropertySellingPoint;
	}

	public function createPropertyInstruction() {
		$objPropertyInstruction = new CPropertyInstruction();
		$objPropertyInstruction->setCid( $this->getCid() );
		$objPropertyInstruction->setPropertyId( $this->getId() );

		return $objPropertyInstruction;
	}

	public function createPetRateAssociation() {
		$objPetTypeRateAssociation = new CPetRateAssociation();
		$objPetTypeRateAssociation->setArOriginId( CArOrigin::PET );
		$objPetTypeRateAssociation->setPropertyId( $this->getId() );
		$objPetTypeRateAssociation->setCid( $this->getCid() );
		$objPetTypeRateAssociation->setIsOptional( true );

		return $objPetTypeRateAssociation;
	}

	public function createFloorplanRateAssociation() {
		$objFloorplanRateAssociation = new CAmenityRateAssociation();
		$objFloorplanRateAssociation->setArOriginId( CArOrigin::AMENITY );
		$objFloorplanRateAssociation->setArCascadeId( CArCascade::FLOOR_PLAN );
		$objFloorplanRateAssociation->setPropertyId( $this->getId() );
		$objFloorplanRateAssociation->setCid( $this->getCid() );

		return $objFloorplanRateAssociation;
	}

	public function createUnitTypeRateAssociation() {
		$objUnitTypeRateAssociation = new CAmenityRateAssociation();
		$objUnitTypeRateAssociation->setArOriginId( CArOrigin::AMENITY );
		$objUnitTypeRateAssociation->setArCascadeId( CArCascade::UNIT_TYPE );
		$objUnitTypeRateAssociation->setPropertyId( $this->getId() );
		$objUnitTypeRateAssociation->setCid( $this->getCid() );

		return $objUnitTypeRateAssociation;
	}

	public function createUnitSpaceRateAssociation() {
		$objUnitSpaceRateAssociation = new CAmenityRateAssociation();
		$objUnitSpaceRateAssociation->setArOriginId( CArOrigin::AMENITY );
		$objUnitSpaceRateAssociation->setArCascadeId( CArCascade::SPACE );
		$objUnitSpaceRateAssociation->setPropertyId( $this->getId() );
		$objUnitSpaceRateAssociation->setCid( $this->getCid() );

		return $objUnitSpaceRateAssociation;
	}

	public function createAddOnRateAssociation() {
		$objAddOnRateAssociation = new CRateAssociation();
		$objAddOnRateAssociation->setArCascadeId( CArCascade::PROPERTY );
		$objAddOnRateAssociation->setArCascadeReferenceId( $this->getId() );
		$objAddOnRateAssociation->setArOriginId( CArOrigin::ADD_ONS );
		$objAddOnRateAssociation->setPropertyId( $this->getId() );
		$objAddOnRateAssociation->setCid( $this->getCid() );

		return $objAddOnRateAssociation;
	}

	public function createAddOnLeaseTerm() {
		$objAddOnLeaseTerm = new CAddOnLeaseTerm();
		$objAddOnLeaseTerm->setPropertyId( $this->getId() );
		$objAddOnLeaseTerm->setCid( $this->getCid() );
		return $objAddOnLeaseTerm;
	}

	public function createCustomerConciergeService() {
		$objCustomerConciergeService = new CCustomerConciergeService();
		$objCustomerConciergeService->setCid( $this->getCid() );
		$objCustomerConciergeService->setPropertyId( $this->getId() );

		return $objCustomerConciergeService;
	}

	public function createApplication( $intLeaseIntervalTypeId = CLeaseIntervalType::APPLICATION ) {
		$objApplication = new CApplication();
		$objApplication->setCid( $this->getCid() );
		$objApplication->setPropertyId( $this->getId() );
		$objApplication->setApplicationStageId( CApplicationStage::PRE_APPLICATION );
		$objApplication->setApplicationStatusId( CApplicationStatus::COMPLETED );
		$objApplication->setLeaseIntervalTypeId( $intLeaseIntervalTypeId );
		if( true == is_null( $this->m_objDatabase ) ) {
			$this->loadDatabase( CDatabaseUserType::PS_PROPERTYMANAGER );
		}
		$objApplication->setDatabase( $this->m_objDatabase );

		return $objApplication;
	}

	public function createIlsEmail() {
		$objIlsEmail = new CIlsEmail();
		$objIlsEmail->setCid( $this->getCid() );
		$objIlsEmail->setPropertyId( $this->getId() );

		return $objIlsEmail;
	}

	public function createContactSubmission() {
		$objContactSubmission = new CContactSubmission();
		$objContactSubmission->setCid( $this->getCid() );
		$objContactSubmission->setPropertyId( $this->getId() );

		return $objContactSubmission;
	}

	public function createMarketingIntegrationSubscription() {
		$objMarketingIntegrationSubscription = new CMarketingIntegrationSubscription();
		$objMarketingIntegrationSubscription->setCid( $this->getCid() );
		$objMarketingIntegrationSubscription->setPropertyId( $this->getId() );

		return $objMarketingIntegrationSubscription;
	}

	public function createKeyword() {
		$objKeyword = new CKeyword();
		$objKeyword->setCid( $this->getCid() );
		$objKeyword->setPropertyId( $this->getId() );
		$objKeyword->setMessageOriginatorId( CMessageOriginator::PS_NUMBER_1 );
		$objKeyword->setSelectionDatetime( date( 'Y-m-d H:i:s' ) );

		return $objKeyword;
	}

	public function createPropertySmsMessage() {
		$objPropertySmsMessage = new CPropertySmsMessage();
		$objPropertySmsMessage->setCid( $this->getCid() );
		$objPropertySmsMessage->setPropertyId( $this->getId() );

		return $objPropertySmsMessage;
	}

	public function createClAd() {
		$objClAd = new CClAd();

		$objClAd->setDefaults();
		$objClAd->setCid( $this->getCid() );
		$objClAd->setPropertyId( $this->getId() );

		return $objClAd;
	}

	public function createPropertyPsProduct() {
		$objPropertyPsProduct = new CPropertyPsProduct();
		$objPropertyPsProduct->setDefaults();
		$objPropertyPsProduct->setPropertyId( $this->getId() );
		$objPropertyPsProduct->setCid( $this->getCid() );

		return $objPropertyPsProduct;
	}

	public function createPropertyLateFeeFormula() {
		$objPropertyLateFeeFormula = new CPropertyLateFeeFormula();
		$objPropertyLateFeeFormula->setPropertyId( $this->getId() );
		$objPropertyLateFeeFormula->setCid( $this->getCid() );

		return $objPropertyLateFeeFormula;
	}

	public function createPropertyTax() {
		$objPropertyTax = new CPropertyTax();
		$objPropertyTax->setPropertyId( $this->getId() );
		$objPropertyTax->setCid( $this->getCid() );

		return $objPropertyTax;
	}

	public function createGreeting() {
		$objGreeting = new CGreeting();
		$objGreeting->setPropertyId( $this->getId() );
		$objGreeting->setCid( $this->getCid() );

		return $objGreeting;
	}

	public function createLeaseStartWindow() {
		$objLeaseStartWindow = new CLeaseStartWindow();
		$objLeaseStartWindow->setPropertyId( $this->getId() );
		$objLeaseStartWindow->setCid( $this->getCid() );

		return $objLeaseStartWindow;
	}

	public function createPropertyOccupation() {
		$objPropertyOccupation = new CPropertyOccupation();
		$objPropertyOccupation->setPropertyId( $this->getId() );
		$objPropertyOccupation->setCid( $this->getCid() );

		return $objPropertyOccupation;
	}

	public function createPropertyMoveOutListItem() {
		$objPropertyMoveOutListItem = new CPropertyListItem();
		$objPropertyMoveOutListItem->setPropertyId( $this->getId() );
		$objPropertyMoveOutListItem->setCid( $this->getCid() );
		$objPropertyMoveOutListItem->setListTypeId( CListType::MOVE_OUT );

		return $objPropertyMoveOutListItem;
	}

	public function createPropertyCancelListItem() {
		$objPropertyCancelListItem = new CPropertyListItem();
		$objPropertyCancelListItem->setPropertyId( $this->getId() );
		$objPropertyCancelListItem->setCid( $this->getCid() );
		$objPropertyCancelListItem->setListTypeId( CListType::LEAD_CANCELLATION );

		return $objPropertyCancelListItem;
	}

	public function createCompanyPricing() {
		$objCompanyPricing = new CCompanyPricing();
		$objCompanyPricing->setCid( $this->getCid() );
		$objCompanyPricing->setPropertyId( $this->getId() );

		return $objCompanyPricing;
	}

	public function createReview() {
		$objReview = new CReview();
		$objReview->setCid( $this->getCid() );
		$objReview->setPropertyId( $this->getId() );

		return $objReview;
	}

	public function createAddOn() {
		$objAddOn = new CAddOn();
		$objAddOn->setCid( $this->getCid() );
		$objAddOn->setPropertyId( $this->getId() );

		return $objAddOn;
	}

	public function createAudioRecordingCompanyMediaFolder( $objDatabase ) {
		$objAudioRecordingCompanyMediaFolder = NULL;

		$objPropertyCompanyMediaFolder = $this->fetchParentCompanyMediaFolder( $objDatabase );

		if( false == valObj( $objPropertyCompanyMediaFolder, 'CCompanyMediaFolder' ) ) {
			// Create company media folder for this property.
			$objPropertyCompanyMediaFolder = $this->createCompanyMediaFolder();
			$objPropertyCompanyMediaFolder->setFolderName( $this->getPropertyName() );
			$objPropertyCompanyMediaFolder->setId( $objPropertyCompanyMediaFolder->fetchNextId( $objDatabase ) );

			if( false == $objPropertyCompanyMediaFolder->insert( SYSTEM_USER_ID, $objDatabase ) ) {
				return false;
			}
		}

		// If Folder doest not exists we need to create one for this we need parent folder of media libarry of company to add property media folder.
		$objAudioRecordingCompanyMediaFolder = new CCompanyMediaFolder();
		$objAudioRecordingCompanyMediaFolder->setCid( $this->getCid() );
		$objAudioRecordingCompanyMediaFolder->setPropertyId( $this->getId() );
		$objAudioRecordingCompanyMediaFolder->setParentMediaFolderId( $objPropertyCompanyMediaFolder->getId() );
		$objAudioRecordingCompanyMediaFolder->setFolderName( 'Audio Recordings' );

		if( false == $objAudioRecordingCompanyMediaFolder->insert( SYSTEM_USER_ID, $objDatabase ) ) {
			return NULL;
		}

		return $objAudioRecordingCompanyMediaFolder;
	}

	public function createPropertyTransmissionVendor() {
		$objPropertyTransmissionVendor = new CPropertyTransmissionVendor();
		$objPropertyTransmissionVendor->setCid( $this->getCid() );
		$objPropertyTransmissionVendor->setPropertyId( $this->getId() );

		return $objPropertyTransmissionVendor;
	}

	public function createPropertyTransmissionVendorRemoteLocation() {
		$objPropertyTransmissionVendorRemoteLocation = new CPropertyTransmissionVendorRemoteLocation();
		$objPropertyTransmissionVendorRemoteLocation->setCid( $this->getCid() );
		$objPropertyTransmissionVendorRemoteLocation->setPropertyId( $this->getId() );

		return $objPropertyTransmissionVendorRemoteLocation;
	}

	public function createPropertyTransmissionVendorRemoteLocationLog() {
		$objPropertyTransmissionVendorRemoteLocationLog = new CPropertyTransmissionVendorRemoteLocationLog();
		$objPropertyTransmissionVendorRemoteLocationLog->setCid( $this->getCid() );
		$objPropertyTransmissionVendorRemoteLocationLog->setPropertyId( $this->getId() );

		return $objPropertyTransmissionVendorRemoteLocationLog;
	}

	public function createClub() {
		$objClub = new CClub();
		$objClub->setCid( $this->getCid() );
		$objClub->setPropertyId( $this->getId() );
		$objClub->setIsPublished( 1 );

		return $objClub;
	}

	public function createClubComment() {
		$objClubComment = new CClubComment();
		$objClubComment->setCid( $this->getCid() );
		$objClubComment->setPropertyId( $this->getId() );

		return $objClubComment;
	}

	public function createOrFetchPropertyCallSetting( $objVoipDatabase ) {

		$objPropertyCallSetting = CPropertyCallSettings::fetchPropertyCallSettingsByCidByPropertyId( $this->getCid(), $this->getId(), $objVoipDatabase );

		if( false == valObj( $objPropertyCallSetting, 'CPropertyCallSetting' ) ) {

			$objPropertyCallSetting = new CPropertyCallSetting();
			$objPropertyCallSetting->setDefaults();
			$objPropertyCallSetting->setCid( $this->getCid() );
			$objPropertyCallSetting->setPropertyId( $this->getId() );
			$objPropertyCallSetting->setPropertyName( $this->getPropertyName() );
			$objPropertyCallSetting->setTimeZoneId( $this->getTimeZoneId() );
		}

		return $objPropertyCallSetting;
	}

	public function createCompanyEmployeeContact( $intCompanyEmployeeContactTypeId = NULL ) {
		$objCompanyEmployeeContact = new CCompanyEmployeeContact();
		$objCompanyEmployeeContact->setCid( $this->getCid() );
		$objCompanyEmployeeContact->setPropertyId( $this->getId() );

		if( true == valId( $intCompanyEmployeeContactTypeId ) ) {
			$objCompanyEmployeeContact->setCompanyEmployeeContactTypeId( $intCompanyEmployeeContactTypeId );
		}

		return $objCompanyEmployeeContact;
	}

	public function createCompanyEmployeeContactAvailability() {
		$objCompanyEmployeeContactAvailability = new CCompanyEmployeeContactAvailability();
		$objCompanyEmployeeContactAvailability->setCid( $this->getCid() );
		$objCompanyEmployeeContactAvailability->setPropertyId( $this->getId() );

		return $objCompanyEmployeeContactAvailability;
	}

	public function createPropertyMediaCategory() {
		$objPropertyMediaCategory = new CPropertyMediaCategory();
		$objPropertyMediaCategory->setCid( $this->getCid() );
		$objPropertyMediaCategory->setPropertyId( $this->getId() );

		return $objPropertyMediaCategory;
	}

	public function createLateNoticeType() {
		$objLateNoticeType = new CLateNoticetype();
		$objLateNoticeType->setCid( $this->getCid() );
		$objLateNoticeType->setPropertyId( $this->getId() );

		return $objLateNoticeType;
	}

	public function createLateFeeBatch() {
		$objLateFeeBatch = new CLateFeeBatch();
		$objLateFeeBatch->setCid( $this->getCid() );
		$objLateFeeBatch->setPropertyId( $this->getId() );
		$objLateFeeBatch->setBatchDatetime( 'NOW' );

		return $objLateFeeBatch;
	}

	public function createLateFeeBatchDetails() {
		$objLateFeeBatchDetail = new CLateFeeBatchDetail();
		$objLateFeeBatchDetail->setCid( $this->getCid() );
		$objLateFeeBatchDetail->setPropertyId( $this->getId() );

		return $objLateFeeBatchDetail;
	}

	public function createLateFeeFormula() {
		$objLateFeeFormula = new CLateFeeFormula();
		$objLateFeeFormula->setCid( $this->getCid() );
		// $objLateFeeFormula->setPropertyId( $this->getId() );
		$objLateFeeFormula->setLateFeePostTypeId( CLateFeePostType::SUM_OF_BOTH );

		return $objLateFeeFormula;
	}

	public function createMaintenanceAvailabilityException() {
		$objPropertyMaintenanceAvailabilityException = new CMaintenanceAvailabilityException();
		$objPropertyMaintenanceAvailabilityException->setCid( $this->m_intCid );
		$objPropertyMaintenanceAvailabilityException->setPropertyId( $this->getId() );

		return $objPropertyMaintenanceAvailabilityException;
	}

	public function createMaintenanceAvailability() {
		$objPropertyMaintenanceAvailability = new CMaintenanceAvailability();
		$objPropertyMaintenanceAvailability->setCid( $this->m_intCid );
		$objPropertyMaintenanceAvailability->setPropertyId( $this->getId() );

		return $objPropertyMaintenanceAvailability;
	}

	public function createRenewalTemplateTier( $intRenewalTemplateId = NULL, $intRenewalOfferType = NULL ) {
		$objRenewalTemplateTier = new CRenewalTemplateTier();
		$objRenewalTemplateTier->setCid( $this->m_intCid );
		$objRenewalTemplateTier->setPropertyId( $this->getId() );
		$objRenewalTemplateTier->setRenewalTemplateId( $intRenewalTemplateId );
		$objRenewalTemplateTier->setRenewalOfferTypeId( $intRenewalOfferType );

		return $objRenewalTemplateTier;
	}

	public function createRoommateInterest() {
		$objRoommateInterest = new CRoommateInterest();
		$objRoommateInterest->setDefaults();
		$objRoommateInterest->setCid( $this->m_intCid );

		return $objRoommateInterest;
	}

	public function createRoommateInterestOption() {
		$objRoommateInterestOption = new CRoommateInterestOption();
		$objRoommateInterestOption->setDefaults();
		$objRoommateInterestOption->setCid( $this->m_intCid );

		return $objRoommateInterestOption;
	}

	public function createPropertyRoommateInterest() {
		$objPropertyRoommateInterest = new CPropertyRoommateInterest();
		$objPropertyRoommateInterest->setDefaults();
		$objPropertyRoommateInterest->setCid( $this->m_intCid );
		$objPropertyRoommateInterest->setPropertyId( $this->getId() );

		return $objPropertyRoommateInterest;
	}

	public function createPropertyAdvanceDayException() {

		$objPropertyAdvanceDayException = new CPropertyAdvanceDayException();

		$objPropertyAdvanceDayException->setCid( $this->m_intCid );
		$objPropertyAdvanceDayException->setPropertyId( $this->getId() );
		$objPropertyAdvanceDayException->setDefaults();

		return $objPropertyAdvanceDayException;
	}

	public function createSocialMediaActivity() {
		$objSocialMediaActivity = new CSocialMediaActivity();
		$objSocialMediaActivity->setPropertyId( $this->getId() );
		$objSocialMediaActivity->setCid( $this->getCid() );

		return $objSocialMediaActivity;
	}

	public function createSocialPost() {
		$objSocialPost = new CSocialPost();
		$objSocialPost->setPropertyId( $this->getId() );
		$objSocialPost->setCid( $this->getCid() );

		return $objSocialPost;
	}

	public function createPropertyLedgerFilter() {
		$objPropertyLedgerFilter = new CPropertyLedgerFilter();
		$objPropertyLedgerFilter->setPropertyId( $this->getId() );
		$objPropertyLedgerFilter->setCid( $this->getCid() );

		return $objPropertyLedgerFilter;
	}

	public function createPropertyLeasingGoal() {
		$objPropertyLeasingGoal = new CPropertyLeasingGoal();
		$objPropertyLeasingGoal->setCid( $this->m_intCid );
		$objPropertyLeasingGoal->setPropertyId( $this->getId() );

		return $objPropertyLeasingGoal;
	}

	public function createPropertySettingLog( $intPropertySettingKeyId, $strAction, $strNewValue, $strNewText, $intPsCategoryId = NULL, $intReferenceCategoryId = NULL ) {

		$objPropertySettingLog = new CPropertySettingLog();
		$objPropertySettingLog->setCid( $this->m_intCid );
		$objPropertySettingLog->setPropertyId( $this->getId() );
		$objPropertySettingLog->setPropertySettingKeyId( $intPropertySettingKeyId );
		$objPropertySettingLog->setAction( $strAction );
		$objPropertySettingLog->setNewValue( $strNewValue );
		$objPropertySettingLog->setNewText( $strNewText );

		if( valId( $intPsCategoryId ) && valId( $intReferenceCategoryId ) ) {
			$arrintPsCategoryDetails['ps_category_id']			= $intPsCategoryId;
			$arrintPsCategoryDetails['reference_category_id']	= $intReferenceCategoryId;

			$objPropertySettingLog->setDetails( json_encode( array_merge( objectToArray( $objPropertySettingLog->getDetails() ), [ 'ps_category' => $arrintPsCategoryDetails ] ) ) );
		}

		return $objPropertySettingLog;
	}

	public function createPropertyReviewAttribute() {
		$objPropertyReviewAttribute = new CPropertyReviewAttribute();
		$objPropertyReviewAttribute->setCid( $this->getCid() );
		$objPropertyReviewAttribute->setPropertyId( $this->getId() );

		return $objPropertyReviewAttribute;
	}

	public function createUnitSpaceExclusion( $intUnitExclusionReasonTypeId = CUnitExclusionReasonType::CONSTRUCTION_UNIT ) {
		$objUnitSpaceExclusion = new CUnitSpaceExclusion();
		$objUnitSpaceExclusion->setDefaults();
		$objUnitSpaceExclusion->setCid( $this->getCid() );
		$objUnitSpaceExclusion->setPropertyId( $this->getId() );
		$objUnitSpaceExclusion->setUnitExclusionReasonTypeId( $intUnitExclusionReasonTypeId );

		return $objUnitSpaceExclusion;
	}

	public function createScheduledBlackoutDate() {
		$objScheduldeBlackoutDate = new CScheduledBlackoutDate();
		$objScheduldeBlackoutDate->setDefaults();
		$objScheduldeBlackoutDate->setCid( $this->getCid() );
		$objScheduldeBlackoutDate->setPropertyId( $this->getId() );

		return $objScheduldeBlackoutDate;
	}

	public function createFloorplanGroup() {

		$objFloorplanGroup = new CFloorplanGroup();
		$objFloorplanGroup->setCid( $this->getCid() );
		$objFloorplanGroup->setPropertyId( $this->getId() );
		$objFloorplanGroup->setIsPublished( 1 );

		return $objFloorplanGroup;
	}

	public function createPropertyFloorplanGroup() {
		$objPropertyFloorplanGroup = new CPropertyFloorplanGroup();
		$objPropertyFloorplanGroup->setCid( $this->getCid() );
		$objPropertyFloorplanGroup->setPropertyId( $this->getId() );

		return $objPropertyFloorplanGroup;
	}

	public function createPropertySpaceConfiguration() {

		$objPropertySpaceConfiguration = new CPropertySpaceConfiguration();
		$objPropertySpaceConfiguration->setCid( $this->getCid() );
		$objPropertySpaceConfiguration->setPropertyId( $this->getId() );

		return $objPropertySpaceConfiguration;
	}

	public function createArLookupTable() {
		$objArLookupTable = new CArLookupTable();
		$objArLookupTable->setCid( $this->getCid() );
		$objArLookupTable->setPropertyId( $this->getId() );

		return $objArLookupTable;
	}

	public function createArLookupLevel() {
		$objArLookupLevel = new CArLookupLevel();
		$objArLookupLevel->setCid( $this->getCid() );
		$objArLookupLevel->setPropertyId( $this->getId() );

		return $objArLookupLevel;
	}

	public function createPropertyLeaseTermExpiration() {
		$objPropertyLeaseTermExpiration = new CPropertyLeaseTermExpiration();
		$objPropertyLeaseTermExpiration->setCid( $this->getCid() );
		$objPropertyLeaseTermExpiration->setPropertyId( $this->getId() );

		return $objPropertyLeaseTermExpiration;
	}

	public function createIntegrationContactType() {

		$objIntegrationContactType = new CIntegrationContactType();
		$objIntegrationContactType->setCid( $this->getCid() );
		$objIntegrationContactType->setPropertyId( $this->getId() );

		return $objIntegrationContactType;
	}

	public function createPropertyVehicle() {
		$objPropertyVehicle = new CPropertyVehicle();
		$objPropertyVehicle->setPropertyId( $this->getId() );
		$objPropertyVehicle->setCid( $this->m_intCid );

		return $objPropertyVehicle;
	}

	public function createImportRequest() {
		$objImportRequest = new CImportRequest();
		$objImportRequest->setCid( $this->getCid() );
		$objImportRequest->setPropertyId( $this->getId() );
		$objImportRequest->setName( $this->getPropertyName() );
		$objImportRequest->setImportStatusTypeId( CImportStatusType::INITIALIZED );

		return $objImportRequest;
	}

	public function createScreeningNotification() {
		$objScreeningNotification = new CScreeningNotification();
		$objScreeningNotification->setCid( $this->getCid() );
		$objScreeningNotification->setPropertyId( $this->getId() );

		return $objScreeningNotification;
	}

	public function createWaitlist() {
		$objWaitList = new CWaitList();
		$objWaitList->setCid( $this->getCid() );

		return $objWaitList;
	}

	public function createIntegrationWorker( $intIntegrationServiceId, $intCompanyUserId, $objDatabase ) {
		return CIntegrationFactory::createWorker( $this->getId(), $this->getCid(), $intIntegrationServiceId, $intCompanyUserId, $objDatabase );
	}

	public function createPropertyHour( $intPropertyHourTypeId = NULL ) {
		$objPropertyHour = new CPropertyHour();
		$objPropertyHour->setCid( $this->m_intCid );
		$objPropertyHour->setPropertyId( $this->getId() );
		$objPropertyHour->setIsPublished( 1 );
		$objPropertyHour->setPropertyHourTypeId( $intPropertyHourTypeId );

		return $objPropertyHour;
	}

	public function createSettingsTemplate( $boolIsDefault = false ) {
		$objSettingsTemplate = new CSettingsTemplate();
		$objSettingsTemplate->setCid( $this->getCid() );
		$objSettingsTemplate->setPropertyId( $this->getId() );
		$objSettingsTemplate->setTemplateName( $this->getPropertyName() );
		$objSettingsTemplate->setSettingsTemplateTypeId( CSettingsTemplateType::TEMPLATE );

		if( true == $boolIsDefault ) {
			$objSettingsTemplate->setIsDefault( 1 );
		}

		return $objSettingsTemplate;
	}

	public function createComparableAssociation() {
		$objComparableAssociation = new CComparableAssociation();
		$objComparableAssociation->setPropertyId( $this->getId() );
		$objComparableAssociation->setCid( $this->m_intCid );

		return $objComparableAssociation;
	}

	public function createCall( $strPhoneNumber, $objVoipDatabase ) {
		$objCall = new CCall();

		$objCall->setCid( $this->getCid() );
		$objCall->setPropertyId( $this->getId() );
		$objCall->setCallTypeId( CCallType::MANUAL_FOLLOWUP_CALL );
		$objCall->setOriginPhoneNumber( CCallPhoneNumber::LEASING_CENTER_LINE );
		$objCall->setCallStatusTypeId( CCallStatusType::CALLING );
		$objCall->setCallPriorityId( CCallPriority::NORMAL );
		$objCall->setCallResultId( CCallResult::LEAD );
		$objCall->setVoiceTypeId( CVoiceType::FEMALE );
		$objCall->setDestinationPhoneNumber( $strPhoneNumber );

		if( false == $objCall->insert( SYSTEM_USER_ID, $objVoipDatabase ) ) {
			trigger_error( __( 'Application Error: Unable to save call record.' ), E_USER_WARNING );
			return;
		}

		$objCallEvent = $objCall->createCallEvent();
		$objCallEvent->setCallEventTypeId( CCallEventType::STARTING );

		if( false == $objCallEvent->insert( SYSTEM_USER_ID, $objVoipDatabase ) ) {
			trigger_error( 'Application Error: Unable to save call event record.', E_USER_WARNING );
			return;
		}

		return $objCall;
	}

	public function createStudentLeasingCap() {
		$objStudentLeasingCap = new CStudentLeasingCap();
		$objStudentLeasingCap->setCid( $this->getCid() );
		$objStudentLeasingCap->setPropertyId( $this->getId() );

		return $objStudentLeasingCap;
	}

	public function createFollowup( $intFollowupTypeId, $arrmixMessage ) {
		$objFollowup = new CFollowup();
		$objFollowup->setCid( $this->getCid() );
		$objFollowup->setPropertyId( $this->getId() );
		$objFollowup->setFollowupTypeId( $intFollowupTypeId );
		$objFollowup->setDetails( json_encode( $arrmixMessage ) );

		return $objFollowup;
	}

	public function createPropertyArAllocationPriority() {

		$objPropertyArAllocationPriority = new CPropertyArAllocationPriority();
		$objPropertyArAllocationPriority->setCid( $this->getCid() );
		$objPropertyArAllocationPriority->setPropertyId( $this->getId() );

		return $objPropertyArAllocationPriority;
	}

	public function createPropertyFaq() {

		$objPropertyFaq = new CPropertyFaq();
		$objPropertyFaq->setCid( $this->getCid() );
		$objPropertyFaq->setPropertyId( $this->getId() );

		return $objPropertyFaq;
	}

	public function createPropertyFaqGroup() {

		$objPropertyFaqGroup = new CPropertyFaqGroup();
		$objPropertyFaqGroup->setCid( $this->getCid() );
		$objPropertyFaqGroup->setPropertyId( $this->getId() );

		return $objPropertyFaqGroup;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchAmenityRateAssociationsByArCascadeIdByAmenityTypeId( $intCascadeId, $intAmenityTypeId, $objDatabase, $boolFetchData = false, $intPageNo = NULL, $intPageSize = NULL, $boolIsPublished = false, $arrstrSearchFilter = [] ) {
		return CAmenityRateAssociations::createService()->fetchAmenityRateAssociationsByArCascadeIdByAmenityTypeIdByPropertyIdByCid( $intCascadeId, $intAmenityTypeId, $this->getId(), $this->getCid(), $objDatabase, $boolFetchData, $intPageNo, $intPageSize, $boolIsPublished, $arrintOccupancyTypeIds = $this->getOccupancyTypeIds(), $arrstrSearchFilter );
	}

	public function fetchPropertyChargeSetting( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyChargeSettings::createService()->fetchPropertyChargeSettingByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchActiveBankAccounts( $objDatabase ) {
		return CBankAccounts::fetchActiveBankAccountsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAddOnGroups( $intAddOnTypeId, $objDatabase, $boolIsPublishedOnly = false ) {
		return CAddOnGroups::createService()->fetchCustomAddOnGroupsByAddOnTypeIdByPropertyIdByCid( $intAddOnTypeId, $this->getId(), $this->getCid(), $objDatabase, $boolIsPublishedOnly );
	}

	public function fetchAddOnGroup( $intAddOnGroupId, $objDatabase ) {
		return CAddOnGroups::createService()->fetchAddOnGroupByIdByPropertyIdByCid( $intAddOnGroupId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAddOnCategoryById( $intAddOnCategoryId, $objDatabase ) {
		return CAddOnCategories::createService()->fetchAddOnCategoryByIdByPropertyIdByCid( $intAddOnCategoryId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchIntegratedAddOns( $objDatabase ) {
		return CAddOns::createService()->fetchIntegratedAddOnsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchIntegratedAddOnLeaseAssociations( $objDatabase ) {
		return CAddOnLeaseAssociations::createService()->fetchIntegratedAdOnLeaseAssociationsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnitTypesByPropertyFloorplanIds( $arrintPropertyFloorplanIds, $objDatabase ) {

		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return CUnitTypes::createService()->fetchUnitTypesByPropertyIdsByPropertyFloorplanIdsByCid( $arrintPropertyIds, $arrintPropertyFloorplanIds, $this->getCid(), $objDatabase );
	}

	public function fetchPublishedPropertyLeadSourceByCidByInternetListingServiceId( $intInternetListingServiceId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPublishedPropertyLeadSourceByPropertyIdByInternetListingServiceIdByCid( $this->getId(), $intInternetListingServiceId, $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpacesByUnitSpaceStatusTypeIdsByLeaseStatusTypeIds( $arrintUnitSpaceStatusTypeIds, $arrintLeaseStatusTypeIds, $objDatabase ) {
		return CUnitSpaces::createService()->fetchUnitSpacesByPropertyIdByUnitSpaceStatusTypeIdsByLeaseStatusTypeIdsByCid( $this->getId(), $arrintUnitSpaceStatusTypeIds, $arrintLeaseStatusTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchMaintenanceRequestsWithPriorityNameByMonthByYear( $strMonth, $strYear, $objDatabase ) {
		return CMaintenanceRequests::createService()->fetchMaintenanceRequestsWithPriorityNameByPropertyIdByCidByMonthByYear( $this->getId(), $this->getCid(), $strMonth, $strYear, $objDatabase );
	}

	public function fetchSimplePropertyMaintenanceAvailability( $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceAvailabilities::createService()->fetchSimplePropertyMaintenanceAvailabilityByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchParentPropertyMaintenanceTemplatesByMaintenanceRequestTypeIds( $arrintMaintenanceRequestTypeIds, $objDatabase, $intLeaseId = NULL ) {
		return CPropertyMaintenanceTemplates::createService()->fetchParentPropertyMaintenanceTemplatesByPropertyIdByCidMaintenanceRequestTypeIds( $this->getCid(), $this->getId(), $arrintMaintenanceRequestTypeIds, $objDatabase, $intLeaseId );
	}

	public function fetchArCodeByDefaultArCodeId( $intDefaultArCodeId, $objDatabase ) {
		require_once PATH_LIBRARIES_PSI . 'SearchFilters/CArCodesFilter.class.php';

		$objArCodesFilter = new CArCodesFilter();
		$objArCodesFilter->setDefaultArCodeIds( [ ( int ) $intDefaultArCodeId ] );
		$objArCodesFilter->setPropertyId( $this->getId() );
		$objArCodesFilter->setReturnSingleCode( true );

		return \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodesByCidByArCodesFilter( $this->getCid(), $objArCodesFilter, $objDatabase );
	}

	public function fetchCustomerByNameFirstByNameLastByUnitNumber( $strNameFirst, $strNameLast, $strUnitNumber, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByNameFirstByNameLastByUnitNumberByPropertyIdByCid( $strNameFirst, $strNameLast, $strUnitNumber, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyUsersCountByCompanyUserPreferenceKeys( $arrstrCompanyUserPreferenceKeys, $objDatabase, $arrobjPropertyPreferences = [] ) {
		if( true == valArr( $this->m_arrobjPropertyPreferences ) ) {
			$arrobjPropertyPreferences = $this->m_arrobjPropertyPreferences;
		}
		$boolIsValidPropertyPreferenceObject = ( true == valObj( getArrayElementByKey( 'LEAD_CHAT_ROUTING_CONDITION', $arrobjPropertyPreferences ), 'CPropertyPreference' ) );
		if( true == $boolIsValidPropertyPreferenceObject && 'ALL_CHATS' == $arrobjPropertyPreferences['LEAD_CHAT_ROUTING_CONDITION']->getValue() ) {
			return true;
		}
		$intCompanyUserCount = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUsersCountByCompanyUserPreferenceKeysByPropertyIdByCid( $arrstrCompanyUserPreferenceKeys, $this->getId(), $this->getCid(), $objDatabase );
		if( true == $boolIsValidPropertyPreferenceObject && 0 == $intCompanyUserCount && 'NO_USER_AVAILABLE' == $arrobjPropertyPreferences['LEAD_CHAT_ROUTING_CONDITION']->getValue() ) {
			return true;
		}

		return $intCompanyUserCount;
	}

	public function fetchPropertyUtilityTypes( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchPropertyUtilityTypesByPropertyIdByCid( $this->getId(), $this->getCid(), $objUtilitiesDatabase );
	}

	public function fetchPropertyUtilitySetting( $objUtilityBillingFilter, $objUtilitiesDatabase ) {

		$objUtilityBillingFilter->setCid( $this->getCid() );
		$objUtilityBillingFilter->setPropertyId( $this->getId() );

		return \Psi\Eos\Utilities\CPropertyUtilitySettings::createService()->fetchComplexPropertyUtilitySettingByUtilityBillingFilter( $objUtilityBillingFilter, $objUtilitiesDatabase );
	}

	public function fetchArCodes( $objDatabase, $objArCodesFilter = NULL, $arrintArCodeTypeIds = NULL, $boolShowPropertyLedgerFilterArCode = false, $objPropertyGlSetting = NULL ) {

		require_once PATH_LIBRARIES_PSI . 'SearchFilters/CArCodesFilter.class.php';

		if( false == valObj( $objArCodesFilter, 'CArCodesFilter' ) ) {
			$objArCodesFilter = new CArCodesFilter();
		}

		if( false == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
			$objPropertyGlSetting = $this->getOrFetchPropertyGlSetting( $objDatabase );
		}

		$objArCodesFilter->setPropertyId( $this->getId() );

		if( true == valArr( $arrintArCodeTypeIds ) ) {
			$objArCodesFilter->setArCodeTypeIds( $arrintArCodeTypeIds );
		}

		if( true == valArr( $objArCodesFilter->getExcludeArCodeTypeIds() ) ) {
			$objArCodesFilter->setExcludeArCodeTypeIds( $objArCodesFilter->getExcludeArCodeTypeIds() );
		}

		// First we will look to see if this is an integrated property. If so we will load integrated ar codes.
		if( true == valStr( $this->getRemotePrimaryKey() )
		    && true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' )
		    && false == $objPropertyGlSetting->getActivateStandardPosting() ) {

			$objIntegrationDatabase = $this->fetchIntegrationDatabase( $objDatabase );

			if( true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) && CIntegrationClientType::REAL_PAGE != $objIntegrationDatabase->getIntegrationClientTypeId() ) {
				$objArCodesFilter->setIntegrationDatabasePreferenceId( CArCodesFilter::INTEGRATION_DATABASE_PREFERENCE_NOT_NULL );
				$objArCodesFilter->setIntegrationDatabaseId( $objIntegrationDatabase->getId() );
			}
		}

		return \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodesByCidByArCodesFilter( $this->getCid(), $objArCodesFilter, $objDatabase, $boolIsExactMatch = true, $boolShowPropertyLedgerFilterArCode );
	}

	public function fetchSemSetting( $objDatabase ) {
		return CSemSettings::fetchSemSettingByPropertyId( $this->getId(), $objDatabase );
	}

	public function fetchSemPropertyDetail( $objDatabase ) {
		return CSemPropertyDetails::fetchSemPropertyDetailByPropertyId( $this->getId(), $objDatabase );
	}

	public function fetchSingularWebsite( $objDatabase, $boolIsTemplateTypeRequried = false ) {
		return CWebsites::createService()->fetchSingularWebsiteByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsTemplateTypeRequried );
	}

	public function fetchPublishedCompanyApplications( $objDatabase ) {
		return CCompanyApplications::fetchPublishedCompanyApplicationsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedCompanyApplicationsByOccupancyTypeIds( $arrintOccupancyTypeIds, $objDatabase ) {
		return CCompanyApplications::fetchPublishedCompanyApplicationsByPropertyIdByOccupancyTypeIdsByCid( $this->getId(), $arrintOccupancyTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchCheckedTermsAndConditions( $strKey, $objDatabase ) {
		return CCompanyApplications::fetchPublishedCompanyApplicationsByPropertyIdByApplicationPreferencekeyByCid( $this->getId(), $strKey, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyApplications( $objDatabase ) {
		return CCompanyApplications::fetchCompanyApplicationsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchApplicationsByIds( $arrintApplicationIds, $objDatabase ) {
		return CApplications::fetchApplicationsByPropertyIdByIdsByCid( $this->getId(), $arrintApplicationIds, $this->getCid(), $objDatabase );
	}

	public function fetchPublishedCompanyApplicationById( $intCompanyApplicationId, $objDatabase ) {
		return CCompanyApplications::fetchPublishedCompanyApplicationByPropertyIdByIdByCid( $this->getId(), $intCompanyApplicationId, $this->getCid(), $objDatabase );
	}

	public function fetchArCodeMerchantAccountIdsByMerchantAccountIds( $arrintMerchantAccountIds, $objDatabase ) {
		return CPropertyMerchantAccounts::fetchArCodeMerchantAccountIdsByPropertyIdByMerchantAccountIdsByCid( $this->getId(), $arrintMerchantAccountIds, $this->getCid(), $objDatabase );
	}

	public function fetchArPaymentTransmissionById( $intArPaymentTransmissionId, $objDatabase ) {
		return CArPaymentTransmissions::fetchArPaymentTransmissionsByIdByPropertyIdByCid( $intArPaymentTransmissionId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCustomers( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchNetworkingEnrolledCustomers( $objDatabase ) {
		return Psi\Eos\Entrata\CCustomerPortalSettings::createService()->fetchNetworkingEnrolledCustomersByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCustomerPortalSettingByFacebookUser( $intFacebookUser, $objDatabase ) {
		return Psi\Eos\Entrata\CCustomerPortalSettings::createService()->fetchCustomerPortalSettingByPropertyIdByFacebookUserByCid( $this->getId(), $intFacebookUser, $this->getCid(), $objDatabase );
	}

	public function fetchUnpostedScheduledPayments( $objDatabase ) {
		return CScheduledPayments::fetchUnpostedScheduledPaymentsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWarningScheduledPayments( $objDatabase ) {
		return CScheduledPayments::fetchWarningScheduledPaymentsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchResidentPortalData( $objDatabase ) {
		$this->getOrFetchPrimaryPropertyAddress( $objDatabase );
		$this->getOrFetchOverviewMarketingMediaAssociation( $objDatabase );
		$this->getOrFetchPropertyPreferences( $objDatabase );
	}

	public function fetchCachedPropertyPreferencesKeyedByKey( $objDatabase ) {
		$arrobjPropertyPreferences = [];

		$arrstrPropertyPreferences = CPropertyPreferences::createService()->fetchCachedPropertyPreferencesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		if( true == valArr( $arrstrPropertyPreferences ) ) {
			$objPropertyPreference = $this->createPropertyPreference();

			foreach( $arrstrPropertyPreferences as $strKey => $strValue ) {
				$objPropertyPreferenceCloned = clone $objPropertyPreference;
				$objPropertyPreferenceCloned->setKey( $strKey );
				$objPropertyPreferenceCloned->setValue( $strValue );
				$arrobjPropertyPreferences[$strKey] = $objPropertyPreferenceCloned;
			}
		}

		return $arrobjPropertyPreferences;
	}

	public function fetchAvailableUnitSpacesByLeaseStartDate( $strLeaseStartDate, $arrobjPropertyPreferences, $objDatabase ) {

		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return CUnitSpaces::createService()->fetchAvailableUnitSpacesByPropertyIdsByLeaseStartDateByCid( $arrintPropertyIds, $strLeaseStartDate, $arrobjPropertyPreferences, $this->getCid(), $objDatabase );
	}

	public function fetchAvailableUnitSpaces( $objDatabase ) {
		return CUnitSpaces::createService()->fetchAvailableUnitSpacesByCidByPropertyId( $this->getCid(), $this->getId(), $objDatabase );
	}

	public function fetchUnitSpacesByResidentSyncParameters( $intPropertyFloorplanId, $intPropertyUnitId, $intPropertyBuildingId, $intUnitTypeId, $intUnitNumber, $boolIsAvailableUnitsOnly, $objDatabase ) {
		return CUnitSpaces::createService()->fetchUnitSpacesByResidentSyncParametersByCid( $this->getId(), $intPropertyFloorplanId, $intPropertyUnitId, $intPropertyBuildingId, $intUnitTypeId, $intUnitNumber, $boolIsAvailableUnitsOnly, $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpaceByBuildingIdByUnitNumber( $intPropertyBuildingId, $intUnitNumber, $objDatabase ) {
		return CUnitSpaces::createService()->fetchUnitSpaceByPropertyIdBuildingIdByUnitNumberByCid( $this->getId(), $intPropertyBuildingId, $intUnitNumber, $this->m_intCid, $objDatabase );
	}

	public function fetchAdditionalPropertyAddresses( $objDatabase ) {
		$arrintExcludedAddressTypes = [ CAddressType::PRIMARY ];

		return CPropertyAddresses::createService()->fetchPropertyAddressesByPropertyIdExcludeAddressTypeIdsByCid( $this->getId(), $arrintExcludedAddressTypes, $this->getCid(), $objDatabase );
	}

	public function fetchAdditionalPropertyEmailAddresses( $objDatabase ) {
		$arrintExcludedEmailAddressTypes = [ CEmailAddressType::PRIMARY ];

		return CPropertyEmailAddresses::fetchPropertyEmailAddressesByPropertyIdExcludeEmailAddressTypeIdsByCid( $this->getId(), $arrintExcludedEmailAddressTypes, $this->getCid(), $objDatabase );
	}

	public function fetchAdditionalPropertyPhoneNumbers( $objDatabase ) {
		$arrintExcludedPhoneNumberTypes = [ CPhoneNumberType::OFFICE, CPhoneNumberType::FAX, CPhoneNumberType::MAINTENANCE_EMERGENCY, CPhoneNumberType::RESIDENT_OFFICE ];

		return CPropertyPhoneNumbers::fetchPropertyPhoneNumbersByPropertyIdExcludePhoneNumberTypeIdsByCid( $this->getId(), $arrintExcludedPhoneNumberTypes, $this->getCid(), $objDatabase );
	}

	public function fetchSingleAdditionalPropertyPhoneNumber( $objDatabase ) {
		return CPropertyPhoneNumbers::fetchSingleAdditionalPropertyPhoneNumberByPropertyIdByCid( $this->getId(), CPhoneNumberType::PRIMARY, $this->getCid(), $objDatabase );
	}

	public function fetchPublishedAmenityRateAssociationsByArCascadeIdByAmenityTypeIds( $intArCascadeId, $arrintAmenityTypeIds, $objDatabase ) {
		return CAmenityRateAssociations::createService()->fetchPublishedAmenityRateAssociationsByArCascadeIdByAmenityTypeIdsByPropertyIdByCid( $intArCascadeId, $arrintAmenityTypeIds, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedMaintenanceLocations( $objDatabase, $arrstrSystemCodesFilterForLocations = NULL, $boolIsFromResidentPortal = false, $boolIsIntegratedProperty = false, $intIntegrationClientTypeId = 0 ) {
		return CMaintenanceLocations::createService()->fetchPublishedMaintenanceLocationsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $arrstrSystemCodesFilterForLocations, $boolIsFromResidentPortal, $boolIsIntegratedProperty, $intIntegrationClientTypeId );
	}

	public function fetchSimplePublishedMaintenanceLocationsBySystemCodesFilterIds( $objDatabase, $arrstrSystemCodesFilterForLocations = NULL, $boolIsFromResidentPortal = false, $boolIsIntegratedProperty = false, $intIntegrationClientTypeId = 0 ) {
		return CMaintenanceLocations::createService()->fetchSimplePublishedMaintenanceLocationsBySystemCodesFilterIdsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $arrstrSystemCodesFilterForLocations, $boolIsFromResidentPortal, $boolIsIntegratedProperty, $intIntegrationClientTypeId );
	}

	public function fetchAllMaintenanceLocations( $objDatabase, $boolIsFromBulkUnitType ) {
		return CMaintenanceLocations::createService()->fetchAllMaintenanceLocationsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsFromBulkUnitType );
	}

	public function fetchDefaultWebsite( $objDatabase ) {
		return CWebsites::createService()->fetchDefaultWebsiteByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchEnabledDefaultWebsite( $objClientDatabase, $boolIsTemplateTypeRequried = false ) {
		return CWebsites::createService()->fetchEnabledDefaultWebsiteByPropertyIdByCid( $this->getId(), $this->getCid(), $objClientDatabase, $boolIsTemplateTypeRequried );
	}

	public function fetchPropertyMaintenanceLocationByMaintenanceLocationId( $intMaintenanceLocationId, $objDatabase ) {
		return CPropertyMaintenanceLocations::createService()->fetchPropertyMaintenanceLocationByMaintenanceLocationIdByPropertyIdByCid( $intMaintenanceLocationId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyMaintenanceLocations( $objDatabase, $boolIsPublished = false ) {
		return CPropertyMaintenanceLocations::createService()->fetchCustomPropertyMaintenanceLocationsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsPublished );
	}

	public function fetchInspectionPropertyMaintenanceLocations( $objDatabase, $boolIsPublished = true ) {
		return CPropertyMaintenanceLocations::createService()->fetchInspectionPropertyMaintenanceLocationsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsPublished );
	}

	public function fetchOpenMakeReadySubMaintenanceRequestsProblems( $objDatabase, $boolFetchData = false ) {
		return CMaintenanceProblems::createService()->fetchOpenMakeReadySubMaintenanceRequestsProblemsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolFetchData );
	}

	public function fetchMaintenanceProblemDropdownData( $boolIsByLocation, $boolIsFromResidentPortal, $objDatabase, $boolIsIntegratedProperty = false, $strProblemType = CMaintenanceProblem::INCLUDE_IN_BOTH, $boolIsSortByCategory = false, $boolIsUnitTypeLocation = false, $intPropertyUnitId = 0 ) {
		return CPropertyMaintenanceProblems::createService()->fetchMaintenanceProblemDropdownDataByPropertyIdByCid( $this->getId(), $this->getCid(), $boolIsByLocation, $boolIsFromResidentPortal, $objDatabase, $boolIsIntegratedProperty, $strProblemType, $boolIsSortByCategory, $boolIsUnitTypeLocation, $intPropertyUnitId );
	}

	public function fetchPropertyMaintenanceProblemById( $intPropertyMaintenanceProblemId, $objDatabase ) {
		return CPropertyMaintenanceProblems::createService()->fetchCustomPropertyMaintenanceProblemByIdByCid( $intPropertyMaintenanceProblemId, $this->getCid(), $objDatabase, $this->getId() );
	}

	public function fetchPropertyMaintenanceProblemByName( $strPropertyMaintenanceProblemName, $boolIsSubCategory, $objDatabase ) {
		return CPropertyMaintenanceProblems::createService()->fetchPropertyMaintenanceProblemByNameByCid( $this->getId(), $strPropertyMaintenanceProblemName, $boolIsSubCategory, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyMaintenanceProblems( $objDatabase ) {
		return CPropertyMaintenanceProblems::createService()->fetchCustomPropertyMaintenanceProblemsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchMaintenanceProblemsByMaintenanceProblemIdByProblemTypes( $intMaintenanceProblemId, $arrintProblemTypes, $objDatabase ) {
		return CMaintenanceProblems::createService()->fetchMaintenanceProblemsByMaintenanceProblemIdByProblemTypesByPropertyIdByCid( $intMaintenanceProblemId, $arrintProblemTypes, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchMaintenanceProblemsWithParentNameWithLocationsByMaintenanceProblemTypeIds( $arrintMaintenanceProblemTypeIds, $objDatabase, $strProblemType = CMaintenanceProblem::INCLUDE_IN_BOTH, $isMaintenanceProblemPublished = false ) {
		return CPropertyMaintenanceProblems::createService()->fetchMaintenanceProblemsWithParentNameWithLocationsByPropertyIdByProblemTypeIdsByCid( $this->getCid(), $this->getId(), $arrintMaintenanceProblemTypeIds, $objDatabase, $strProblemType, $isMaintenanceProblemPublished );
	}

	public function fetchParentPropertyMaintenanceTemplates( $objDatabase ) {
		return CPropertyMaintenanceTemplates::createService()->fetchParentPropertyMaintenanceTemplatesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyMaintenanceMakeReadyTemplates( $objDatabase, $intLeaseId = NULL ) {
		return CPropertyMaintenanceTemplates::createService()->fetchMakeReadyMaintenanceTemplatesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $intLeaseId );
	}

	public function fetchMaintenanceStatuses( $objDatabase ) {
		return CMaintenanceStatuses::createService()->fetchMaintenanceStatusesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchDefaultMaintenanceStatus( $objDatabase ) {
		return CMaintenanceStatuses::createService()->fetchDefaultMaintenanceStatusByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedMaintenanceStatuses( $objDatabase ) {
		return CMaintenanceStatuses::createService()->fetchPublishedMaintenanceStatusesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchClosedOrDeletedMaintenanceStatuses( $boolIsMakeReadyMaintenanceRequest, $strMaintenanceStatusNames, $objDatabase ) {
		return CMaintenanceStatuses::createService()->fetchMaintenanceStatusesByNameByPropertyIdByCid( $boolIsMakeReadyMaintenanceRequest, $strMaintenanceStatusNames, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedPropertyMaintenanceStatuses( $objDatabase ) {
		return CPropertyMaintenanceStatuses::createService()->fetchPublishedPropertyMaintenanceStatusesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedMaintenanceProblems( $objDatabase, $boolIsFromResidentPortal = true ) {
		return CMaintenanceProblems::createService()->fetchPublishedMaintenanceProblemsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsFromResidentPortal );
	}

	public function fetchPublishedPropertyMaintenanceProblems( $boolIsSubCategory, $objDatabase, $arrstrSystemCodesFilterForCategories = NULL ) {
		return CPropertyMaintenanceProblems::createService()->fetchPublishedPropertyMaintenanceProblemsByPropertyIdByCid( $this->getId(), $boolIsSubCategory, $this->getCid(), $objDatabase, $arrstrSystemCodesFilterForCategories );
	}

	public function fetchIntegratedPublishedPropertyMaintenanceProblems( $boolIsSubCategory, $objDatabase, $arrstrSystemCodesFilterForCategories = NULL ) {
		return CPropertyMaintenanceProblems::createService()->fetchIntegratedPublishedPropertyMaintenanceProblemsByPropertyIdByCid( $this->getId(), $boolIsSubCategory, $this->getCid(), $objDatabase, $arrstrSystemCodesFilterForCategories );
	}

	public function fetchPropertyMaintenancePriorityByMaintenancePriorityId( $intMaintenancePriorityId, $objDatabase ) {
		return CPropertyMaintenancePriorities::createService()->fetchPropertyMaintenancePriorityByPropertyIdByIdByCid( $this->getId(), $intMaintenancePriorityId, $this->getCid(), $objDatabase );
	}

	public function fetchPublishedMaintenancePriorities( $objDatabase, $boolIsFromResidentPortal = false ) {
		return CMaintenancePriorities::createService()->fetchPublishedMaintenancePrioritiesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsFromResidentPortal );
	}

	public function fetchPropertyMaintenancePriorities( $objDatabase ) {
		return CPropertyMaintenancePriorities::createService()->fetchPropertyMaintenancePriortiesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyMaintenanceProblemByMaintenanceProblemId( $intMaintenanceProblemId, $objDatabase ) {
		return CPropertyMaintenanceProblems::createService()->fetchPropertyMaintenanceProblemByPropertyIdByMaintenanceProblemIdByCid( $this->getId(), $intMaintenanceProblemId, $this->getCid(), $objDatabase );
	}

	public function fetchWebsites( $objDatabase ) {
		return CWebsites::createService()->fetchWebsitesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPrimaryWebsites( $objDatabase ) {
		return CWebsites::createService()->fetchPrimaryWebsitesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertySpesificWebsite( $objDatabase ) {
		return CWebsites::createService()->fetchPropertySpesificWebsiteByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertySpesificWebsites( $objDatabase ) {
		return CWebsites::createService()->fetchPropertySpesificWebsitesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationClientType( $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClientTypes::createService()->fetchIntegrationClientTypeByPropertyIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationClientTypeId( $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClientTypes::createService()->fetchIntegrationClientTypeIdByPropertyIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationClientKeyValueByKey( $strKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClientKeyValues::createService()->fetchIntegrationClientKeyValueByPropertyIdByKeyByCid( $this->getId(), $strKey, $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationClientKeyValueByKeys( $arrstrKeys, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClientKeyValues::createService()->fetchIntegrationClientKeyValuesByKeysByPropertyIdByCid( $arrstrKeys, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationClientKeyValueByKeyByLimit( $strKey, $intLimit, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClientKeyValues::createService()->fetchIntegrationClientKeyValueByPropertyIdByKeyByLimitByCid( $this->getId(), $strKey, $intLimit, $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationClientKeyValueByKeyWithNullUnitTypeId( $strKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClientKeyValues::createService()->fetchIntegrationClientKeyValueByPropertyIdByKeyWithNullUnitTypeIdByCid( $this->getId(), $strKey, $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationClientKeyValuesByKey( $strKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClientKeyValues::createService()->fetchIntegrationClientKeyValuesByPropertyIdByKeyByCid( $this->getId(), $strKey, $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationClientKeyValuesByValue( $strValue, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClientKeyValues::createService()->fetchIntegrationClientKeyValuesByPropertyIdByValueByCid( $this->getId(), $strValue, $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationClientKeyValuesByKeyKeyedByValue( $strKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClientKeyValues::createService()->fetchIntegrationClientKeyValuesByPropertyIdByKeyKeyedByValueByCid( $this->getId(), $strKey, $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationClientKeyValuesByValueByKey( $strValue, $strKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClientKeyValues::createService()->fetchIntegrationClientKeyValuesByPropertyIdByValueByKeyByCid( $this->getId(), $strValue, $strKey, $this->getCid(), $objDatabase );
	}

	public function fetchMaintenancePhoneNumber( $objDatabase ) {
		return CPropertyPhoneNumbers::fetchPropertyPhoneNumberByPropertyIdByPhoneNumberTypeIdByCid( $this->getId(), CPhoneNumberType::MAINTENANCE, $this->getCid(), $objDatabase );
	}

	public function fetchMaintenancePhoneNumberByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CPropertyPhoneNumbers::fetchPropertyPhoneNumberByPropertyIdPhoneNumberTypeIdRemotePrimaryKeyByCid( $this->getId(), CPhoneNumberType::MAINTENANCE, $this->getCid(), $strRemotePrimaryKey, $objDatabase );
	}

	public function fetchMaintenanceEmergencyPhoneNumber( $objDatabase ) {
		return CPropertyPhoneNumbers::fetchPropertyPhoneNumberByPropertyIdByPhoneNumberTypeIdByCid( $this->getId(), CPhoneNumberType::MAINTENANCE_EMERGENCY, $this->getCid(), $objDatabase );
	}

	public function fetchPrimaryAddress( $objDatabase ) {
		return CPropertyAddresses::createService()->fetchPropertyAddressByPropertyIdByAddressTypeIdByCid( $this->getId(), CAddressType::PRIMARY, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyAddressByAddressTypeId( $intAddressTypeId, $objDatabase ) {
		return CPropertyAddresses::createService()->fetchPropertyAddressByPropertyIdByAddressTypeIdByCid( $this->getId(), $intAddressTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyAddressesByAddressTypeIds( $arrintAddressTypeIds, $objDatabase ) {
		return CPropertyAddresses::createService()->fetchPropertyAddressesByPropertyIdAddressTypeIdsByCid( $this->getId(), $arrintAddressTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchNonPrimaryPropertyAddresses( $objDatabase ) {
		return CPropertyAddresses::createService()->fetchNonPrimaryPropertyAddressesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyPhoneNumberByPhoneNumberType( $intPhoneNumberTypeId, $objDatabase ) {
		return CPropertyPhoneNumbers::fetchPropertyPhoneNumberByPropertyIdByPhoneNumberTypeIdByCid( $this->getId(), $intPhoneNumberTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyPhoneNumberByLeadSourceId( $intLeadSourceId, $objDatabase ) {
		return CPropertyPhoneNumbers::fetchPropertyPhoneNumberByLeadSourceIdByPropertyIdByCid( $intLeadSourceId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyAreas( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyAreas::createService()->fetchCompanyAreasByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPrimaryAddressByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CPropertyAddresses::createService()->fetchPropertyAddressByPropertyIdAddressTypeIdRemotePrimaryKeyByCid( $strRemotePrimaryKey, $this->getId(), CAddressType::PRIMARY, $this->getCid(), $objDatabase );
	}

	public function fetchPrimaryOnSiteContact( $objDatabase ) {
		return CPropertyOnSiteContacts::fetchPropertyOnSiteContactByPropertyIdContactTypeIdByCid( $this->getId(), CContactType::MANAGER, $this->getCid(), $objDatabase );
	}

	public function fetchPrimaryOnSiteContactByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CPropertyOnSiteContacts::fetchPropertyOnSiteContactByPropertyIdContactTypeIdRemotePrimaryKeyByCid( $strRemotePrimaryKey, $this->getId(), CContactType::MANAGER, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyMedia( $intCompanyMediaFileId, $objDatabase ) {
		return CPropertyMedias::createService()->fetchPropertyMediaByPropertyIdByMediaFileIdByCid( $this->getId(), $intCompanyMediaFileId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyMediasByCompanyMediaFileId( $intCompanyMediaFileId, $objDatabase ) {
		return CPropertyMedias::createService()->fetchPropertyMediasByPropertyIdByCompanyMediaFileIdByCid( $this->getId(), $intCompanyMediaFileId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyMedias( $objDatabase ) {
		return CPropertyMedias::createService()->fetchPropertyMediasByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchRemittanceAddress( $objDatabase ) {
		return CPropertyAddresses::createService()->fetchPropertyAddressByPropertyIdByAddressTypeIdByCid( $this->getId(), CAddressType::PAYMENT, $this->getCid(), $objDatabase );
	}

	public function fetchRemittanceAddressByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CPropertyAddresses::createService()->fetchPropertyAddressByPropertyIdAddressTypeIdRemotePrimaryKeyByCid( $strRemotePrimaryKey, $this->getId(), CAddressType::PAYMENT, $this->getCid(), $objDatabase );
	}

	public function fetchAmenityCount( $objClientDatabase ) {
		$strSql = 'SELECT
						count( a.id )
					FROM
						rate_associations ra
						JOIN amenities a ON (
							ra.cid = a.cid AND ra.ar_origin_reference_id = a.id
							AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
							AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . ' )
					WHERE
						ra.property_id = ' . ( int ) $this->getId() . '
						AND ra.cid = ' . ( int ) $this->getCid();

		$arrintResponse = fetchData( $strSql, $objClientDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) {
			return $arrintResponse[0]['count'];
		}
	}

	public function fetchPropertyLevelPublishedAmenityCountByCid( $intCid, $objClientDatabase ) {
		$strSql = ' SELECT
						count(*)
					FROM
						rate_associations ra
					JOIN amenities AS a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id )
					WHERE
						ra.ar_cascade_reference_id = ' . ( int ) $this->getId() . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND ra.property_id = ' . ( int ) $this->getId() . '
						AND ra.is_published = true
						AND a.cid = ' . ( int ) $intCid;

		$arrintResponse = fetchData( $strSql, $objClientDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) {
			return $arrintResponse[0]['count'];
		}

		return 0;
	}

	public function fetchPropertyCategories( $objDatabase ) {
		return CPropertyCategories::fetchPropertyCategoriesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyMerchantAccounts( $objDatabase, $boolIsGetCommercialAccount = false ) {
		return CPropertyMerchantAccounts::fetchCustomPropertyMerchantAccountsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolGetDisabledData = false, $boolIsGetCommercialAccount );
	}

	public function fetchPrimaryPropertyMerchantAccount( $objDatabase, $boolIsGetDisabledData = false, $boolIsGetCommercialAccount = false ) {
		return CPropertyMerchantAccounts::fetchPropertyMerchantAccountByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsGetDisabledData, $boolIsGetCommercialAccount );
	}

	public function fetchPropertyMerchantAccountsWithCommercialMerchantAccount( $objDatabase, $boolIsGetDisabledData = false ) {
		return CPropertyMerchantAccounts::fetchPropertyMerchantAccountsWithCommercialMerchantAccountByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsGetDisabledData );
	}

	public function fetchPropertyBuilding( $intPropertyBuildingId, $objDatabase ) {
		return CPropertyBuildings::createService()->fetchPropertyBuildingByIdByCid( $intPropertyBuildingId, $this->getCid(), $objDatabase );
	}

	public function fetchFirstPropertyBuilding( $objDatabase ) {
		return CPropertyBuildings::createService()->fetchFirstPropertyBuildingByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyBuildingByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CPropertyBuildings::createService()->fetchPropertyBuildingByPropertyIdByRemotePrimaryKeyByCid( $this->getId(), $strRemotePrimaryKey, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyBuildingByRemotePrimaryKeys( $arrstrRemotePrimaryKeys, $objDatabase ) {
		return CPropertyBuildings::createService()->fetchPropertyBuildingByPropertyIdByRemotePrimaryKeysByCid( $this->getId(), $arrstrRemotePrimaryKeys, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyBuildings( $objDatabase, $boolIsSortByBuildingName = false, $strSortBy = NULL, $strOrderBy = NULL, $boolIsMarketBuildings = false ) {
		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return CPropertyBuildings::createService()->fetchPropertyBuildingsByPropertyIdsByCid( array_filter( $arrintPropertyIds ), $this->getCid(), $objDatabase, $boolIsSortByBuildingName, $strSortBy, $strOrderBy, $boolIsMarketBuildings );
	}

	public function fetchSimplePropertyBuildings( $objDatabase ) {

		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return CPropertyBuildings::createService()->fetchPropertyBuildingsByCidByPropertyIds( $this->getCid(), $arrintPropertyIds, $objDatabase );
	}

	public function fetchAllPropertyUnits( $objDatabase, $boolReturnArray = false ) {
		return CPropertyUnits::createService()->fetchPropertyUnitsByPropertyIdOrderByUnitNumberByCid( $this->getId(), NULL, $this->getCid(), $objDatabase, false, NULL, NULL, false, $boolReturnArray );
	}

	public function fetchPropertyFloorplansByIds( $arrintPropertyFloorplanIds, $objDatabase ) {
		return CPropertyFloorplans::createService()->fetchPropertyFloorplansByPropertyIdByIdsByCid( $this->getId(), $arrintPropertyFloorplanIds, $this->getCid(), $objDatabase );
	}

	public function fetchLroOptimizationPropertyUnits( $objDatabase ) {
		return CPropertyUnits::createService()->fetchLroOptimizationPropertyUnitsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAvailablePropertyUnits( $objDatabase, $boolIsExcludeCommercialUnits = false ) {
		return CPropertyUnits::createService()->fetchAvailablePropertyUnitsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, false, false, false, $boolIsExcludeCommercialUnits );
	}

	public function fetchAvailablePropertyUnitsIncludingChildProperties( $objDatabase ) {
		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return CPropertyUnits::createService()->fetchAvailablePropertyUnitsByPropertyIdsByCid( $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchAvailablePropertyUnitsByPropertyFloorplanIdsByMoveInDate( $arrintPropertyFloorplanIds, $strMoveInDate, $objDatabase, $boolIsFromEmpoyeePortal = NULL ) {
		return CPropertyUnits::createService()->fetchAvailablePropertyUnitsByPropertyIdByPropertyFloorplanIdsByMoveInDateByCid( $this->getId(), $arrintPropertyFloorplanIds, $strMoveInDate, $this->getOrFetchPropertyPreferences( $objDatabase ), NULL, $this->getCid(), $objDatabase, $boolIsFromEmpoyeePortal );
	}

	public function fetchCompanyEvents( $objDatabase ) {
		return CCompanyEvents::fetchCompanyEventsByPropertyIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchPaginatedCompanyEvents( $strEventsSearchFilter, $objPagination, $arrintPropertyIds, $objDatabase ) {
		return CCompanyEvents::fetchPaginatedCompanyEventsByPropertyIdByCid( $this->getId(), $this->m_intCid, $strEventsSearchFilter, $objPagination, $arrintPropertyIds, $objDatabase );
	}

	public function fetchAnnouncementsByAnnouncementTypeIds( $arrintAnnouncementTypeIds, $objDatabase, $objPagination = NULL, $strAnnouncementSearchFilter = NULL ) {
		return CAnnouncements::fetchAnnouncementsByCidByPropertyIdByAnnouncementTypeIds( $this->m_intCid, $this->getId(), $arrintAnnouncementTypeIds, $objDatabase, $objPagination, $strAnnouncementSearchFilter );
	}

	public function fetchResidentCenterCompanyEvents( $objDatabase ) {
		return CCompanyEvents::fetchResidentCenterCompanyEventsByPropertyIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchCreatedEventsByCustomerId( $intCustomerId, $objDatabase ) {
		return CCompanyEvents::fetchCreatedCompanyEventsByPropertyIdByCustomerIdByCid( $this->getId(), $intCustomerId, $this->m_intCid, $objDatabase );
	}

	public function fetchCompanyEvent( $intCompanyEventId, $objDatabase ) {
		if( false == isset( $intCompanyEventId ) ) {
			return NULL;
		}

		return CCompanyEvents::fetchCompanyEventByIdByPropertyIdByCid( $intCompanyEventId, $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchCompanyEventByIdByCustomerId( $intCompanyEventId, $intCustomerId, $arrintPropertyIds, $objDatabase ) {
		return CCompanyEvents::fetchCompanyEventByIdByPropertyIdByCustomerIdByCid( $intCompanyEventId, $this->getId(), $arrintPropertyIds, $intCustomerId, $this->m_intCid, $objDatabase );
	}

	public function fetchCompanyEventsByIds( $arrintCompanyEventIds, $objDatabase ) {
		return CCompanyEvents::fetchCompanyEventsByIdsByPropertyIdByClient( $arrintCompanyEventIds, $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchCompanyMediaFilesByEventIds( $arrintCompanyEventIds, $intMediaType, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByCompanyEventIdsByMediaTypeIdByCid( $arrintCompanyEventIds, $intMediaType, $this->m_intCid, $objDatabase );
	}

	public function fetchCompanyMediaFilesByClubIds( $arrintClubIds, $intMediaType, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByClubIdsByMediaTypeIdByCid( $arrintClubIds, $intMediaType, $this->m_intCid, $objDatabase );
	}

	public function fetchCompanyMediaFilesByCompanyConciergeServiceIds( $arrintCompanyConciergeServiceIds, $intMediaType, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByCompanyConciergeServiceIdsByMediaTypeIdByCid( $arrintCompanyConciergeServiceIds, $intMediaType, $this->m_intCid, $objDatabase );
	}

	public function fetchClubById( $intClubId, $objDatabase, $boolReturnDataInArray = false ) {
		return CClubs::fetchClubByIdByPropertyIdByCid( $intClubId, $this->getId(), $this->m_intCid, $objDatabase, $boolReturnDataInArray );
	}

	public function fetchCreatedClubsByCustomerId( $intCustomerId, $objDatabase ) {
		return CClubs::fetchCreatedClubsByPropertyIdByCustomerIdByCid( $this->getId(), $intCustomerId, $this->getCid(), $objDatabase );
	}

	public function fetchClubCountByName( $strName, $objDatabase ) {
		return CClubs::fetchClubCountByNameByPropertyIdByCid( $strName, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchClubCountByNameById( $strName, $intClubId, $objDatabase ) {
		return CClubs::fetchClubCountByNameByIdByPropertyIdByCid( $strName, $intClubId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPaginatedClubs( $strClubsSearchFilter, $objPagination, $arrintPropertyIds, $objDatabase, $boolReturnDataInArray = false ) {
		return CClubs::fetchPaginatedClubsByPropertyIdByCid( $this->getId(), $this->getCid(), $strClubsSearchFilter, $objPagination, $arrintPropertyIds, $objDatabase, $boolReturnDataInArray );
	}

	public function fetchAnnouncementByAnnouncementId( $intAnnouncementId, $objDatabase ) {
		if( false == isset( $intAnnouncementId ) ) {
			return NULL;
		}

		return CAnnouncements::fetchAnnouncementByAnnouncementIdByClientByPropertyId( $intAnnouncementId, $this->m_intCid, $this->getId(), $objDatabase );
	}

	public function fetchAnnouncementPsProductByAnnouncementId( $intAnnouncementId, $objDatabase ) {
		return CAnnouncementPsProducts::fetchAnnouncementPsProductByAnnouncementIdByPropertyIdByCid( $intAnnouncementId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchLeadSourceById( $intLeadSourceId, $objDatabase, $intTemplatePropertyId = NULL ) {
		$intPropertyId = ( false == empty( $intTemplatePropertyId ) ) ? $intTemplatePropertyId : $this->getId();

		return \Psi\Eos\Entrata\CLeadSources::createService()->fetchLeadSourceByIdByPropertyIdByCid( $intLeadSourceId, $intPropertyId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyLeadSourceByLeadSourceId( $intLeadSourceId, $objDatabase, $intTemplatePropertyId = NULL ) {
		$intPropertyId = ( false == empty( $intTemplatePropertyId ) ) ? $intTemplatePropertyId : $this->getId();

		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPropertyLeadSourceByLeadSourceIdByPropertyIdByCid( $intLeadSourceId, $intPropertyId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyLeadSources( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPropertyLeadSourcesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchLeadSources( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeadSources::createService()->fetchCustomLeadSourcesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCallPhoneNumbers( $objDatabase ) {
		return CCallPhoneNumbers::fetchCallPhoneNumbersByPropertyId( $this->getId(), $objDatabase );
	}

	public function fetchOutboundDefaultCallPhoneNumber( $objDatabase ) {
		return CCallPhoneNumbers::fetchOutboundDefaultCallPhoneNumberByPropertyIdByCid( $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchValidCallPhoneNumbers( $arrintExcludedCallPhoneNumberIds, $arrintVanityNumberTypeIds, $objDatabase, $boolIsVerified = true, $boolIsSuspended = true, $boolIsDeleted = true ) {
		return CCallPhoneNumbers::fetchValidCallPhoneNumbersByPropertyId( $this->getId(), $arrintExcludedCallPhoneNumberIds, $arrintVanityNumberTypeIds, $objDatabase, $boolIsVerified, $boolIsSuspended, $boolIsDeleted );
	}

	public function fetchPropertyLeadSourcesByCallPhoneNumberId( $intCallPhoneNumberId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPropertyLeadSourcesByPropertyIdByCallPhoneNumberIdByCid( $this->getId(), $intCallPhoneNumberId, $this->getCid(), $objDatabase );
	}

	public function fetchAllPropertyLeadSourcesByCallPhoneNumberId( $intCallPhoneNumberId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchAllPropertyLeadSourcesByPropertyIdByCallPhoneNumberIdByCid( $this->getId(), $intCallPhoneNumberId, $this->getCid(), $objDatabase );
	}

	public function fetchCallPhoneNumberById( $intCallPhoneNumberId, $objDatabase ) {
		return CCallPhoneNumbers::fetchCallPhoneNumberByPropertyIdById( $this->getId(), $intCallPhoneNumberId, $objDatabase );
	}

	public function fetchPublishedLeadSources( $objDatabase, $boolExcludingIsPublished = false, $boolExcludeCompanyLeadSources = false ) {
		return \Psi\Eos\Entrata\CLeadSources::createService()->fetchPublishedLeadSourcesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolExcludingIsPublished, $boolExcludeCompanyLeadSources );
	}

	public function fetchLeadSourceByName( $strName, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeadSources::createService()->fetchLeadSourceByPropertyIdByNameByCid( $this->getId(), $strName, $this->getCid(), $objDatabase );
	}

	public function fetchLeadSourceByInternetListingServiceId( $intInternetListingServiceId, $objDatabase, $boolIsRemotePrimaryKey = false, $strInternetListingServiceName = NULL, $boolFetchLeadSourceByName = true ) {
		return \Psi\Eos\Entrata\CLeadSources::createService()->fetchLeadSourceByPropertyIdByInternetListingServiceIdByCid( $this->getId(), $intInternetListingServiceId, $this->getCid(), $objDatabase, $boolIsRemotePrimaryKey, $strInternetListingServiceName, $boolFetchLeadSourceByName );
	}

	public function fetchUnassociatedLeadSources( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeadSources::createService()->fetchUnassociatedLeadSourcesByPropertyIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchAmenitiesByAmenityTypeId( $intAmenityType, $objDatabase ) {
		return CAmenities::createService()->fetchAmenitiesByAmenityTypeIdByPropertyIdsByCid( [ $this->getId() ], $intAmenityType, $this->getCid(), $objDatabase );
	}

	public function fetchApartmentAmenitiesByUnitSpaceIds( $arrintUnitSpaceIds, $objDatabase ) {
		return CAmenities::createService()->fetchApartmentAmenitiesByUnitSpaceIdsByPropertyIdByCid( $arrintUnitSpaceIds, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAmenityRateAssocaitionWithRatesByAmenityTypeId( $intAmenityTypeId, $objDatabase ) {
		return CAmenityRateAssociations::createService()->fetchAmenityRateAssociationWithRatesByAmenityTypeIdByPropertyIdByCid( $intAmenityTypeId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAmenityRateAssociationsByAmenityTypeId( $intAmenityTypeId, $objDatabase ) {
		return CAmenityRateAssociations::createService()->fetchAmenityRateAssociationsByAmenityTypeIdByPropertyIdByCid( $intAmenityTypeId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAmenityRateAssociationsByRateAssociationIds( $arrintRateAssociationIds, $objDatabase ) {
		return CAmenityRateAssociations::createService()->fetchAmenityRateAssociationsByRateAssociationIdsByCid( $arrintRateAssociationIds, $this->getCid(), $objDatabase );
	}

	public function fetchAllAmenityRateAssociationsByAmenityTypeId( $intAmenityTypeId, $objDatabase ) {
		return CAmenityRateAssociations::createService()->fetchAllAmenityRateAssociationsByAmenityTypeIdByPropertyIdByCid( $intAmenityTypeId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedOptionalAmenityRateAssociationsByUnitSpaceId( $intUnitSpaceId, $objDatabase ) {
		return CAmenityRateAssociations::createService()->fetchPublishedOptionalAmenityRateAssociationsByUnitSpaceIdByPropertyIdByCid( $intUnitSpaceId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedOptionalAmenityRateAssociationsByFloorplanId( $intPropertyFloorplanId, $objDatabase ) {
		return CAmenityRateAssociations::createService()->fetchPublishedOptionalAmenityRateAssociationsByFloorplanIdByPropertyIdByCid( $intPropertyFloorplanId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedOptionalAmenityRateAssociationsByArCascadeIdByPropertyFloorplanId( $intArcascadeId, $intPropertyFloorplanId, $objDatabase ) {
		return CAmenityRateAssociations::createService()->fetchPublishedOptionalAmenityRateAssociationsByArCascadeIdByPropertyFloorplanIdByPropertyIdByCid( $intArcascadeId, $intPropertyFloorplanId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAmenityRateAssociationsByUnitSpaceId( $intUnitSpaceId, $objDatabase ) {
		return CAmenityRateAssociations::createService()->fetchAmenityRateAssociationsByUnitSpaceIdByPropertyIdByCid( $intUnitSpaceId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAmenityRateAssociationsWithFloorplanAmenityNameByPropertyFloorplanId( $intPropertyFloorplanId, $objDatabase ) {
		return CAmenityRateAssociations::createService()->fetchAmenityRateAssociationsWithFloorplanAmenityNameByPropertyFloorplanIdByPropertyIdByCid( $intPropertyFloorplanId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedAmenityRateAssociationsWithAmenityNameByUnitSpaceIds( $arrintPropertyUnitIds, $objDatabase, $boolIncludeNegativeFeatures = false ) {
		return CAmenityRateAssociations::createService()->fetchPublishedAmenityRateAssociationsWithAmenityNameByUnitSpaceIdsByPropertyIdByCid( $arrintPropertyUnitIds, $this->getId(), $this->getCid(), $objDatabase, $boolIncludeNegativeFeatures );
	}

	public function fetchAmenityRateAssociationCompanyMediaFileDependencyByCompanyMediaFileIdByArOriginReferenceId( $intCompanyMediaFileId, $intArOriginReferenceId, $objDatabase ) {
		return CAmenityRateAssociations::createService()->fetchAmenityRateAssociationCompanyMediaFileDependencyByCompanyMediaFileIdByArOriginReferenceIdByCid( $intCompanyMediaFileId, $intArOriginReferenceId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchSpecials( $objDatabase ) {
		return CSpecials::createService()->fetchProspectSpecialsByPropertyIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchRecurringPaymentSpecials( $objDatabase ) {
		return CSpecials::createService()->fetchRecurringPaymentSpecialsByPropertyIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchSpecialById( $intSpecialId, $objDatabase ) {
		if( false == isset( $intSpecialId ) ) {
			return NULL;
		}

		return CSpecials::createService()->fetchSpecialByIdByPropertyIdByCid( $intSpecialId, $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchSpecialsByIds( $arrintSpecialIds, $objDatabase ) {
		if( false == valArr( $arrintSpecialIds ) ) {
			return NULL;
		}

		return CSpecials::createService()->fetchSpecialsByPropertyIdByIdsByCid( $this->getId(), $arrintSpecialIds, $this->getCid(), $objDatabase );
	}

	public function fetchResidentCenterServices( $objDatabase ) {
		return CResidentCenterServices::fetchResidentCenterServicesByCidAndPropertyId( $this->m_intCid, $this->getId(), $objDatabase );
	}

	public function fetchCompanyClassifiedCategoriesCount( $objDatabase ) {
		return CCompanyClassifiedCategories::fetchCompanyClassifiedCategoriesCountByPropertyIdByCid( $this->m_intCid, $this->getId(), $objDatabase );
	}

	public function fetchCompanyConciergeServices( $objDatabase, $boolIsPublished = NULL ) {
		return CCompanyConciergeServices::fetchCompanyConciergeServicesByPropertyIdByCid( $this->getId(), $this->m_intCid, $objDatabase, $boolIsPublished );
	}

	public function fetchCompanyConciergeServiceCount( $objDatabase, $boolIsPublished = NULL ) {
		return CCompanyConciergeServices::fetchCompanyConciergeServicesCountByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsPublished );
	}

	public function fetchCompanyConciergeService( $intCompanyConciergeServiceId, $objDatabase ) {
		if( false == isset( $intCompanyConciergeServiceId ) ) {
			return NULL;
		}

		return CCompanyConciergeServices::fetchCompanyConciergeServiceByIdByPropertyIdByCid( $intCompanyConciergeServiceId, $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchPropertyPreferences( $objDatabase ) {
		return CPropertyPreferences::createService()->fetchCustomPropertyPreferencesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyPreferencesByKeys( $arrstrKeys, $objDatabase, $boolIsCheckValue = true, $arrmixMultipleOccupancyData = NULL ) {
		return CPropertyPreferences::createService()->fetchPropertyPreferencesByPropertyIdsByKeysByCid( [ $this->getId() ], $arrstrKeys, $this->getCid(), $objDatabase, $boolIsCheckValue, false, false, $arrmixMultipleOccupancyData );
	}

	public function fetchSimplePropertyPreferencesByKeys( $arrstrKeys, $objDatabase, $boolIsCheckValue = true ) {
		return CPropertyPreferences::createService()->fetchSimplePropertyPreferencesByPropertyIdByKeysByCid( $this->getId(), $arrstrKeys, $this->getCid(), $objDatabase, $boolIsCheckValue );
	}

	public function fetchPropertyPreferencesByKeysKeyedByKey( $arrstrKeys, $objDatabase ) {
		return CPropertyPreferences::createService()->fetchPropertyPreferencesByKeysKeyedByKeyByPropertyIdsByCid( $arrstrKeys, [ $this->getId() ], $this->getCid(), $objDatabase );
	}

	public function fetchPublishedFeaturedAmenityRateAssociationsByArCascadeIdByAmenityTypeId( $intArCascadeId, $intAmenityTypeId, $objDatabase ) {
		return CAmenityRateAssociations::createService()->fetchPublishedFeaturedAmenityRateAssociationsByArCascadeIdByAmenityTypeIdByPropertyIdByCid( $intArCascadeId, $intAmenityTypeId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAmenitiesByFloorplanIds( $arrintPropertyFloorplanIds, $objDatabase ) {
		return CAmenities::createService()->fetchAmenitiesByPropertyIdByFloorplanIdsByCid( $this->getId(), $arrintPropertyFloorplanIds, $this->getCid(), $objDatabase );
	}

	public function fetchPublishedOptionalAmenityRateAssociationsByArCascadeIdByAmenityTypeIds( $intArCascadeId, $arrintAmenityTypeIds, $objDatabase, $boolIsPublished = true ) {
		return CAmenityRateAssociations::createService()->fetchPublishedOptionalAmenityRateAssociationsByArCascadeIdByAmenityTypeIdsByPropertyIdByCid( $intArCascadeId, $arrintAmenityTypeIds, $this->getId(), $this->getCid(), $objDatabase, $boolIsPublished );
	}

	public function fetchAmenityRateAssociationArCascadeIdByArCascadeReferenceIdByArOriginReferenceId( $intArCascadeId, $intArCascadeReferenceId, $intArOriginReferenceId, $objDatabase ) {
		return CAmenityRateAssociations::createService()->fetchAmenityRateAssociationByArCascadeIdByArCascadeReferenceIdByArOriginReferenceIdByPropertyIdByCid( $intArCascadeId, $intArCascadeReferenceId, $intArOriginReferenceId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyPhoneNumbers( $objDatabase ) {
		return CPropertyPhoneNumbers::fetchPropertyPhoneNumbersByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyPhoneNumber( $intPropertyPhoneNumberId, $objDatabase ) {
		return CPropertyPhoneNumbers::fetchPropertyPhoneNumberByIdByPropertyIdByCid( $intPropertyPhoneNumberId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchSimplePropertyPhoneNumbersByPropertyId( $objDatabase ) {
		return CPropertyPhoneNumbers::fetchSimplePropertyPhoneNumbersByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyEmailAddresses( $objDatabase ) {
		return CPropertyEmailAddresses::fetchPropertyEmailAddressesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyEmailAddress( $intPropertyEmailAddressId, $objDatabase ) {
		return CPropertyEmailAddresses::fetchPropertyEmailAddressByIdByPropertyIdByCid( $intPropertyEmailAddressId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyAddresses( $objDatabase ) {
		return CPropertyAddresses::createService()->fetchPropertyAddressesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyAddress( $intPropertyAddressId, $objDatabase ) {
		return CPropertyAddresses::createService()->fetchPropertyAddressByIdByCid( $intPropertyAddressId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyType( $objDatabase ) {
		return CPropertyTypes::fetchPropertyTypeByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyPreferenceByKey( $strKey, $objDatabase ) {
		return CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( $strKey, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyPreferenceByKeys( $arrstrKey, $objDatabase ) {
		return CPropertyPreferences::createService()->fetchPropertyPreferencesByKeysByPropertyIdByCid( $arrstrKey, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyPreferenceKeysValuesByKeys( $arrstrKey, $objDatabase ) {
		return CPropertyPreferences::createService()->fetchPropertyPreferencesKeysValuesByKeysByPropertyIdByCid( $arrstrKey, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyOfficeHours( $objDatabase ) {
		return CPropertyHours::fetchCustomOfficePropertyHoursByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyHoursWithLunchHoursByPropertyHourType( $intPropertyHourType, $objDatabase, $intPropertyId = NULL ) {
		$intPropertyId = ( true == isset( $intPropertyId ) ) ? $intPropertyId : $this->getId();

		return CPropertyHours::fetchPropertyHoursWithLunchHoursByPropertyHourTypeByPropertyIdByCid( $intPropertyHourType, $intPropertyId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyHoursByPropertyHourType( $intPropertyHourType, $objDatabase ) {
		return CPropertyHours::fetchPropertyHoursByPropertyHourTypeByPropertyIdByCid( $intPropertyHourType, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyOfficeHoursKeyedByWeekday( $objDatabase ) {
		return CPropertyHours::fetchPropertyOfficeHoursKeyedByWeekdayByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyHoursKeyedByWeekdayByPropertyHourTypeId( $intPropertyHourTypeId, $objDatabase ) {
		return CPropertyHours::fetchPropertyHoursKeyedByWeekdayByPropertyHourTypeIdByPropertyIdByCid( $intPropertyHourTypeId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchOldFormatPropertyOfficeHour( $objDatabase ) {
		return CPropertyHours::fetchOldFormatPropertyOfficeHourByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchOldFormatPropertyHourByPropertyHourType( $intPropertyHourType, $objDatabase ) {
		return CPropertyHours::fetchOldFormatPropertyHourByPropertyHourTypeByPropertyIdByCid( $intPropertyHourType, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteProperty( $intWebsiteId, $objDatabase ) {
		return \Psi\Eos\Entrata\CWebsiteProperties::createService()->fetchWebsitePropertyByWebsiteIdPropertyIdByCid( $intWebsiteId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsitePropertyByTargetWebsiteId( $intWebsiteId, $objDatabase ) {
		return \Psi\Eos\Entrata\CWebsiteProperties::createService()->fetchWebsitePropertyByTargetWebsiteIdPropertyIdByCid( $intWebsiteId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchOfficeEmailAddress( $objDatabase ) {
		return CPropertyEmailAddresses::fetchPropertyEmailAddressByPropertyIdEmailAddressTypeIdByCid( $this->getId(), CEmailAddressType::PRIMARY, $this->getCid(), $objDatabase );
	}

	public function fetchOfficeEmailAddressByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CPropertyEmailAddresses::fetchPropertyEmailAddressByPropertyIdEmailAddressTypeIdRemotePrimaryKeyByCid( $strRemotePrimaryKey, $this->getId(), CEmailAddressType::PRIMARY, $this->getCid(), $objDatabase );
	}

	public function fetchOfficeFaxNumber( $objDatabase ) {
		return CPropertyPhoneNumbers::fetchPropertyPhoneNumberByPropertyIdByPhoneNumberTypeIdByCid( $this->getId(), CPhoneNumberType::FAX, $this->getCid(), $objDatabase );
	}

	public function fetchOfficeFaxNumberByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CPropertyPhoneNumbers::fetchPropertyPhoneNumberByPropertyIdPhoneNumberTypeIdRemotePrimaryKeyByCid( $this->getId(), CPhoneNumberType::FAX, $this->getCid(), $strRemotePrimaryKey, $objDatabase );
	}

	public function fetchOfficePhoneNumber( $objDatabase ) {
		return CPropertyPhoneNumbers::fetchPropertyPhoneNumberByPropertyIdByPhoneNumberTypeIdByCid( $this->getId(), CPhoneNumberType::OFFICE, $this->getCid(), $objDatabase );
	}

	public function fetchOfficePhoneNumberByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CPropertyPhoneNumbers::fetchPropertyPhoneNumberByPropertyIdPhoneNumberTypeIdRemotePrimaryKeyByCid( $this->getId(), CPhoneNumberType::OFFICE, $this->getCid(), $strRemotePrimaryKey, $objDatabase );
	}

	public function fetchResidentOfficePhoneNumber( $objDatabase ) {
		return CPropertyPhoneNumbers::fetchPropertyPhoneNumberByPropertyIdByPhoneNumberTypeIdByCid( $this->getId(), CPhoneNumberType::RESIDENT_OFFICE, $this->getCid(), $objDatabase );
	}

	public function fetchPhoneNumbersByPhoneNumberTypeIds( $arrintPhoneNumberTypeIds, $objDatabase ) {
		return CPropertyPhoneNumbers::fetchPropertyPhoneNumbersByPropertyIdsByPhoneNumberTypeIdsByCid( [ $this->getId() ], $arrintPhoneNumberTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyDetail( $objDatabase ) {
		return CPropertyDetails::createService()->fetchPropertyDetailByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyDetailByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CPropertyDetails::createService()->fetchPropertyDetailByPropertyIdByRemotePrimaryKeyByCid( $strRemotePrimaryKey, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpaces( $strAvailabilityDate, $objDatabase, $intSpaceConfigurationId = NULL, $intLeaseTermId = NULL, $intLeaseStartWindowId = NULL,  $strSpaceNumbers = NULL, $arrintUnitSpaceIds = [] ) {
		return CUnitSpaces::createService()->fetchUnitSpacesBySearchUnitNumberFloorLevelAvailabilityByCid( $this->getId(), NULL, $strAvailabilityDate, NULL, NULL, $this->getCid(), $objDatabase, false, $intSpaceConfigurationId, $intLeaseTermId, $intLeaseStartWindowId, $strSpaceNumbers, $arrintUnitSpaceIds );
	}

	public function fetchUnitSpacesByUnitNumber( $strUnitNumber, $objDatabase, $boolIsFromProspectPortal = true ) {
		return CUnitSpaces::createService()->fetchUnitSpacesByUnitNumberByPropertyIdByCid( $strUnitNumber, $this->getId(), $this->getCid(), $objDatabase, $boolIsFromProspectPortal );
	}

	public function fetchAvailableUnitSpacesByUnitNumber( $strUnitNumber, $arrobjPropertyPreferences, $objDatabase, $strLeaseStartDate = NULL, $boolIsFromProspectPortal = true ) {
		return CUnitSpaces::createService()->fetchAvailableUnitSpacesByUnitNumberByPropertyIdByCid( $strUnitNumber, $this->getId(), $arrobjPropertyPreferences, $this->getCid(), $objDatabase, $strLeaseStartDate, $boolIsFromProspectPortal );
	}

	public function fetchUnitSpacesByUnitIds( $arrstrUnitNumbers, $objPropertyUnitsFilter = NULL, $objDatabase ) {
		return CUnitSpaces::createService()->fetchUnitSpacesBySearchUnitIdsByCid( $this->getId(), $arrstrUnitNumbers, $objPropertyUnitsFilter, $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpacesByPropertyBuildingId( $intPropertyBuildingId, $strAvailabilityDate, $objDatabase ) {
		return CUnitSpaces::createService()->fetchUnitSpacesByPropertyBuildingIdBySearchUnitNumberFloorLevelAvailabilityByCid( $this->getId(), $intPropertyBuildingId, NULL, NULL, $strAvailabilityDate, NULL, NULL, $this->getCid(), $objDatabase );
	}

	public function fetchAllUnitSpaces( $objDatabase ) {
		return CUnitSpaces::createService()->fetchUnitSpacesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAllUnitSpacesByCidByPropertyIdByRemovingDefaultSpaceNumber( $objDatabase ) {
		return CUnitSpaces::createService()->fetchAllUnitSpacesByPropertyIdByRemovingDefaultSpaceNumberByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAllUnitAddresses( $objDatabase ) {
		return \Psi\Eos\Entrata\CUnitAddresses::createService()->fetchUnitPrimaryAddressesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpace( $intUnitSpaceId, $objDatabase ) {
		return CUnitSpaces::createService()->fetchUnitSpaceByIdByPropertyIdByCid( $intUnitSpaceId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPhaseUnitSpaceByPropertyUnitId( $intPropertyUnitId, $objDatabase ) {
		return CUnitSpaces::createService()->fetchPhaseUnitSpaceByPropertyIdByPropertyUnitIdByCid( $this->getId(), $intPropertyUnitId, $this->getCid(), $objDatabase );
	}

	public function fetchPhaseUnitSpace( $intUnitSpaceId, $objDatabase ) {
		return CUnitSpaces::createService()->fetchPhaseUnitSpaceByIdByPropertyIdByCid( $intUnitSpaceId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpaceByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CUnitSpaces::createService()->fetchUnitSpaceByRemotePrimaryKeyByPropertyIdByCid( $strRemotePrimaryKey, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUnitsByAvailabilityDate( $strAvailabilityDate, $objDatabase ) {
		$arrintPropertyUnitIds = [];
		if( false == is_null( $arrobjUnitSpaces = $this->fetchUnitSpaces( $strAvailabilityDate, $objDatabase ) ) ) {
			foreach( $arrobjUnitSpaces as $objUnitSpace ) {
				if( false == in_array( $objUnitSpace->getPropertyUnitId(), $arrintPropertyUnitIds ) ) {
					$arrintPropertyUnitIds[] = $objUnitSpace->getPropertyUnitId();
				}
			}
		}

		return CPropertyUnits::createService()->fetchPropertyUnitsByIdsPropertyId( $arrintPropertyUnitIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyUnitByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CPropertyUnits::createService()->fetchPropertyUnitByPropertyIdByRemotePrimaryKeyByCid( $this->getId(), $strRemotePrimaryKey, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUnitByRemotePrimaryKeys( $arrstrRemotePrimaryKeys, $objDatabase ) {
		return CPropertyUnits::createService()->fetchPropertyUnitByPropertyIdByRemotePrimaryKeysByCid( $this->getId(), $arrstrRemotePrimaryKeys, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUnitById( $intPropertyUnitId, $objDatabase ) {
		return CPropertyUnits::createService()->fetchPropertyUnitByIdByCid( $intPropertyUnitId, $this->getCid(), $objDatabase );
	}

	public function fetchCustomPropertyUnitById( $intPropertyUnitId, $objDatabase ) {
		return CPropertyUnits::createService()->fetchCustomPropertyUnitByIdByCid( $intPropertyUnitId, $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpaceById( $intUnitSpaceId, $objDatabase ) {
		return CUnitSpaces::createService()->fetchUnitSpaceByIdByCid( $intUnitSpaceId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUnitsByIds( $arrintPropertyUnitIds, $objDatabase ) {
		return CPropertyUnits::createService()->fetchPropertyUnitsCountUnitNumberByIdsByCid( $arrintPropertyUnitIds, $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpacesByIds( $arrintUnitSpacesIds, $objDatabase ) {
		return CUnitSpaces::createService()->fetchUnitSpacesByIdsByCid( $arrintUnitSpacesIds, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUnitByUnitNumber( $strUnitNumber, $objDatabase ) {
		return CPropertyUnits::createService()->fetchPropertyUnitByPropertyIdByUnitNumberByCid( $this->getId(), $strUnitNumber, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUnitByPropertyBuildingId( $intPropertyBuildingId, $objDatabase ) {
		return CPropertyUnits::createService()->fetchPropertyUnitByPropertyIdByPropertyBuildingIdByCid( $this->getId(), $intPropertyBuildingId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUnitByPropertyBuildingIdByUnitNumber( $intPropertyBuildingId, $strUnitNumber, $objDatabase ) {
		return CPropertyUnits::createService()->fetchPropertyUnitByPropertyIdByPropertyBuildingIdByUnitNumberByCid( $this->getId(), $intPropertyBuildingId, $strUnitNumber, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUnitByPropertyId( $objDatabase ) {
		return CPropertyUnits::createService()->fetchSimplePropertyUnitByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUnitByPropertyFloorId( $intPropertyFloorId, $objDatabase ) {
		return CPropertyUnits::createService()->fetchPropertyUnitByPropertyFloorIdByPropertyIdByCid( $intPropertyFloorId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchMerchantAccount( $objClientDatabase, $intPaymentMediumId = NULL ) {
		if( NULL == $intPaymentMediumId ) {
			$intPaymentMediumId = CPaymentMedium::WEB;
		}

		return CMerchantAccounts::fetchDefaultMerchantAccountByCidByPropertyId( $this->getCid(), $this->getId(), $objClientDatabase, $intPaymentMediumId );
	}

	public function fetchDefaultMerchantAccount( $objClientDatabase, $intPaymentMediumId = NULL ) {
		if( NULL == $intPaymentMediumId ) {
			$intPaymentMediumId = CPaymentMedium::WEB;
		}

		return CMerchantAccounts::fetchDefaultMerchantAccountByCidByPropertyId( $this->m_intCid, $this->getId(), $objClientDatabase, $intPaymentMediumId );
	}

	public function fetchActiveMerchantAccountCount( $objClientDatabase, $intPaymentMediumId = NULL ) {
		return CMerchantAccounts::fetchActiveMerchantAccountCountByPropertyIdsByCid( [ $this->getId() ], $this->m_intCid, $objClientDatabase, $intPaymentMediumId );
	}

	public function fetchMerchantAccounts( $objClientDatabase ) {
		$this->m_arrobjMerchantAccounts = CMerchantAccounts::fetchActiveMerchantAccountsByPropertyIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );

		return $this->m_arrobjMerchantAccounts;
	}

	public function fetchMerchantAccountsByArCodeIds( $arrintArCodeIds, $objClientDatabase, $intPaymentMediumId = NULL, $boolUseView = false ) {
		return CMerchantAccounts::fetchMerchantAccountsByPropertyIdByArCodeIdsByCid( $this->getId(), $arrintArCodeIds, $this->getCid(), $objClientDatabase, $intPaymentMediumId, $boolUseView );
	}

	public function fetchMerchantAccountIdsByArCodeIds( $arrintArCodeIds, $objDatabase ) {
		$intPropertyId = ( false == is_null( $this->getChildProperty() ) ) ? $this->getChildProperty()->getId() : $this->getId();

		return CPropertyMerchantAccounts::fetchCompanyMerchantAccountIdsByPropertyIdByArCodeIdsByCid( $intPropertyId, $arrintArCodeIds, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyAreaById( $intPropertyAreaId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyAreas::createService()->fetchPropertyAreaByPropertyIdByIdByCid( $this->getId(), $intPropertyAreaId, $this->getCid(), $objDatabase );
	}

	public function fetchAllPropertyApplications( $objDatabase ) {
		return CPropertyApplications::fetchAllPropertyApplicationsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyApplications( $objDatabase ) {
		return CPropertyApplications::fetchCustomPropertyApplicationsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyApplication( $intPropertyApplicationId, $objDatabase ) {
		return CPropertyApplications::fetchPropertyApplicationByIdByCid( $intPropertyApplicationId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyApplicationPreferencesByCompanyApplicationId( $intCompanyApplicationId, $objDatabase ) {
		return CPropertyApplicationPreferences::fetchPropertyApplicationPreferencesByCompanyApplicationIdByPropertyIdByApplicationPreferenceTypeIdByCid( $intCompanyApplicationId, $this->getId(), CApplicationPreferenceType::TYPE_GLOBAL, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyApplicationPreferenceByKeyByCompanyApplicationId( $strKey, $intCompanyApplicationId, $objDatabase ) {
		$intApplicationPreferenceTypeId = CApplicationPreferenceType::TYPE_GLOBAL;

		return CPropertyApplicationPreferences::fetchPropertyApplicationPreferenceByKeyByCompanyApplicationIdByPropertyIdByApplicationPreferenceTypeIdByCid( $strKey, $intCompanyApplicationId, $this->getId(), $intApplicationPreferenceTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyFloorplanById( $intPropertyFloorplanId, $objDatabase ) {
		return CPropertyFloorplans::createService()->fetchPropertyFloorplanByIdByPropertyIdByCid( $intPropertyFloorplanId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyFloorplanWithUnitsAndUnitTypesCountById( $intPropertyFloorplanId, $objDatabase, $boolFetchUnitTypesAndUnitsAssociatedToChildProperty = false ) {
		return CPropertyFloorplans::createService()->fetchPropertyFloorplanUnitsAndUnitTypesCountByIdByPropertyIdByCid( $intPropertyFloorplanId, $this->getId(), $this->getCid(), $objDatabase, $boolFetchUnitTypesAndUnitsAssociatedToChildProperty = false );
	}

	public function fetchPhasePropertyFloorplanById( $intPropertyFloorplanId, $objDatabase ) {
		return CPropertyFloorplans::createService()->fetchPhasePropertyFloorplanByIdByPropertyIdByCid( $intPropertyFloorplanId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebVisiblePropertyFloorplanById( $intPropertyFloorplanId, $objDatabase ) {
		return CPropertyFloorplans::createService()->fetchWebVisiblePropertyFloorplanByIdByPropertyIdByCid( $intPropertyFloorplanId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyFloorById( $intPropertyFloorId, $objDatabase, $arrintPropertyIds = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			$arrintPropertyIds					= ( false == isset( $this->m_arrintChildPropertyIds ) ) ? $this->getOrFetchChildPropertyIds( $objDatabase ) : $this->getChildPropertyIds();
			$arrintPropertyIds[$this->getId()]	= $this->getId();
		}

		return CPropertyFloors::createService()->fetchPropertyFloorByIdByPropertyIdByCid( $intPropertyFloorId, $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyFloor( $objDatabase ) {
		return CPropertyFloors::createService()->fetchPropertyFloorByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnitTypeById( $intUnitTypeId, $objDatabase ) {
		return CUnitTypes::createService()->fetchUnitTypeByIdByPropertyIdByCid( $intUnitTypeId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnitTypeByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CUnitTypes::createService()->fetchUnitTypeByPropertyIdByRemotePrimaryKeyByCid( $this->getId(), $strRemotePrimaryKey, $this->getCid(), $objDatabase );
	}

	public function fetchUnitTypeByRemotePrimaryKeys( $arrstrRemotePrimaryKeys, $objDatabase ) {
		return CUnitTypes::createService()->fetchUnitTypeByPropertyIdByRemotePrimaryKeysByCid( $this->getId(), $arrstrRemotePrimaryKeys, $this->getCid(), $objDatabase );
	}

	public function fetchUnitTypesByIds( $arrintUnitTypeIds, $objDatabase ) {
		if( false == valArr( $arrintUnitTypeIds ) ) {
			return NULL;
		}

		return CUnitTypes::createService()->fetchUnitTypesByPropertyIdByIdsByCid( $this->getId(), $arrintUnitTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchAssociatedUnitTypesByPropertyFloorPlanId( $intPropertyFloorplanId, $objDatabase ) {
		return CUnitTypes::createService()->fetchAssociatedUnitTypesByPropertyFloorPlanIdByIdPropertyIdByCid( $intPropertyFloorplanId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyFloorplanByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CPropertyFloorplans::createService()->fetchPropertyFloorplanByPropertyIdByRemotePrimaryKeyByCid( $this->getId(), $strRemotePrimaryKey, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyFloorplanByRemotePrimaryKeys( $arrstrRemotePrimaryKey, $objDatabase ) {
		return CPropertyFloorplans::createService()->fetchPropertyFloorplanByRemotePrimaryKeysByPropertyIdByCid( $arrstrRemotePrimaryKey, $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchDocument( $intDocumentId, $objDatabase ) {
		return CDocuments::fetchDocumentByPropertyIdByDocumentIdByCid( $this->getId(), $intDocumentId, $this->getCid(), $objDatabase );
	}

	public function fetchActiveLeaseDocumentPacketById( $intDocumentId, $objDatabase ) {
		return CDocuments::fetchActiveLeaseDocumentPacketByIdByPropertyIdByCid( $intDocumentId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchProcessPaymentAsAuthOnly( $objDatabase ) {
		$strPropertyPreference = 'PAYMENTS_AUTH_ONLY';
		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( $strPropertyPreference, $this->getId(), $this->getCid(), $objDatabase );

		return ( true == is_null( $objPropertyPreference ) ) ? false : $objPropertyPreference->getValue();
	}

	public function fetchClient( $objDatabase ) {
		return CClients::fetchClientById( $this->m_intCid, $objDatabase );
	}

	public function fetchOverviewMarketingMediaAssociation( $objDatabase ) {
		$this->m_objMarketingMediaAssociation = $this->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::OVERVIEW, $objDatabase, true );
		return $this->m_objMarketingMediaAssociation;
	}

	public function fetchLogoMarketingMediaAssociation( $objDatabase ) {
		return $this->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objDatabase, true );
	}

	public function fetchCompanyMediaFiles( $objDatabase ) {
		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByPropertyIdsByCid( $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFilesByMediaTypeIdsKeyedByMediaTypeId( $arrintMediaTypeIds, $objDatabase ) {
		if( false == valArr( $arrintMediaTypeIds ) ) {
			return NULL;
		}

		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByPropertyIdByMediaTypeIdsKeyedByMediaTypeIdByCid( $this->getId(), $arrintMediaTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFileCountByMediaTypeId( $intMediaTypeId, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFileCountByPropertyIdByMediaTypeIdByCid( $this->getId(), $intMediaTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFileCountByMediaTypeIds( $arrintMediaTypeIds, $objDatabase ) {
		if( false == valArr( $arrintMediaTypeIds ) ) {
			return NULL;
		}

		return CCompanyMediaFiles::createService()->fetchCompanyMediaFileCountByPropertyIdByMediaTypeIdsByCid( $this->getId(), $arrintMediaTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFileById( $intCompanyMediaFileId, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFileByIdByCid( $intCompanyMediaFileId, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFilesByMediaTypeId( $intMediaTypeId, $objDatabase, $intLimit = NULL ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByPropertyIdByMediaTypeIdByCid( $this->getId(), $intMediaTypeId, $this->getCid(), $objDatabase, $intLimit );
	}

	public function fetchCompanyMediaFilesByMediaTypeIds( $arrintMediaTypeIds, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByPropertyIdByMediaTypeIdsByCid( $this->getId(), $arrintMediaTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFileByMediaTypeId( $intMediaTypeId, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFileByPropertyIdByMediaTypeIdByCid( $this->getId(), $intMediaTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchLandscapeCompanyMediaFilesByMediaTypeIdByLimit( $intMediaTypeId, $intLimit = NULL, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchLandscapeCompanyMediaFilesByPropertyIdByMediaTypeIdByLimitByCid( $this->getId(), $intMediaTypeId, $intLimit, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFilesByMediaTypeIdByAltByOrientationByLimit( $intMediaTypeId, $strMediaAlt, $strMediaAltExclude, $strOrientation, $intLimit = NULL, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByPropertyIdByMediaTypeIdByAltByOrientationByLimitByCid( $this->getId(), $intMediaTypeId, $strMediaAlt, $strMediaAltExclude, $strOrientation, $intLimit, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyKeyValues( $objDatabase ) {
		return CPropertyKeyValues::fetchPropertyKeyValuesByPropertyId( $this->getId(), $objDatabase );
	}

	public function fetchPropertyFloorplans( $objDatabase ) {
		return CPropertyFloorplans::createService()->fetchCustomPropertyFloorplansByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, false, false, $this->getOccupancyTypeIds() );
	}

	public function fetchPropertyFloors( $objDatabase ) {
		return CPropertyFloors::createService()->fetchCustomPropertyFloorsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyBuildingFloors( $intPropertyBuildingId, $objDatabase ) {
		return CPropertyFloors::createService()->fetchPropertyFloorsByPropertyBuildingIdByCid( $intPropertyBuildingId, $this->getCid(), $objDatabase );
	}

	public function fetchPhasePropertyFloors( $objDatabase ) {
		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return CPropertyFloors::createService()->fetchPropertyFloorsByPropertyIdsByCid( $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyFloorplanByName( $strName, $objDatabase ) {
		return CPropertyFloorplans::createService()->fetchPropertyFloorplanByPropertyIdByNameByCid( $this->getId(), $strName, $this->getCid(), $objDatabase );
	}

	public function fetchUnitTypes( $objDatabase, $intPropertyId = NULL, $boolIncludeDeletedUnitTypes = false ) {

		$intParentPropertyId = self::getOrFetchParentPropertyId( $objDatabase );

		if( true == valId( $intParentPropertyId ) && false == is_null( $intPropertyId ) ) {
			$intPropertyId = $intParentPropertyId;
		} else {
			$intPropertyId = $this->getId();
		}

		return CUnitTypes::createService()->fetchCustomUnitTypesByPropertyIdByCid( $intPropertyId, $this->getCid(), $objDatabase, $boolIncludeDeletedUnitTypes );
	}

	public function fetchPropertyUnitTypes( $objDatabase, $intPropertyFloorplanId = NULL, $boolFetchUnitTypesAssociatedToChildPropertyFloorplan = false ) {
		return CUnitTypes::createService()->fetchPropertyUnitTypesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $intPropertyFloorplanId, $boolFetchUnitTypesAssociatedToChildPropertyFloorplan );
	}

	public function fetchPublishedUnitTypes( $objDatabase ) {
		return CUnitTypes::createService()->fetchPublishedUnitTypesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedUnitSpaces( $objDatabase, $boolIncludeExcludedUnitSpaces = false ) {
		return CUnitSpaces::createService()->fetchPublishedUnitSpacesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIncludeExcludedUnitSpaces );
	}

	public function fetchPhaseUnitTypes( $objDatabase ) {
		return CUnitTypes::createService()->fetchPhaseUnitTypesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnitTypeNotAssociatedToFloorplan( $objDatabase ) {
		return CUnitTypes::createService()->fetchUnitTypeByPropertyIdNotAssociatedToFloorplanByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyLeasingAgents( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeasingAgents::createService()->fetchCustomPropertyLeasingAgentsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyLeasingAgentByCompanyEmployeeId( $intCompanyEmployeeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeasingAgents::createService()->fetchPropertyLeasingAgentByPropertyIdByCompanyEmployeeIdByCid( $this->getId(), $intCompanyEmployeeId, $this->getCid(), $objDatabase );
	}

	public function fetchActivePropertyLeasingAgents( $objDatabase, $boolIsLoadAdminUsers = false, $boolIsLoadDisabledUsers = false ) {
		return \Psi\Eos\Entrata\CPropertyLeasingAgents::createService()->fetchActivePropertyLeasingAgentsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsLoadAdminUsers, $boolIsLoadDisabledUsers );
	}

	public function fetchIntegratedPropertyLeasingAgent( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeasingAgents::createService()->fetchIntegratedPropertyLeasingAgentByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchEventResults( $objDatabase ) {
		return \Psi\Eos\Entrata\CEventResults::createService()->fetchEventResultsByCidByPropertyId( $this->m_intCid, $this->getId(), $objDatabase );
	}

	public function fetchPropertyEventResults( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyEventResults::createService()->fetchCustomPropertyEventResultsByPropertyIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchCancelledTypePropertyListItemCount( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyListItems::createService()->fetchPropertyListItemCountByListTypeIdByPropertyIdByCid( CListType::LEAD_CANCELLATION, $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchPropertyUnitsByPropertyFloorId( $intPropertyFloorId, $objDatabase, $arrintPropertyIds = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			$arrintPropertyIds = [ $this->getId() ];
		}

		return CPropertyUnits::createService()->fetchPropertyUnitsByPropertyFloorIdByPropertyIdByCid( $intPropertyFloorId, $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchAllPropertyUnitsByPropertyFloorId( $intPropertyFloorId, $objDatabase ) {
		return CPropertyUnits::createService()->fetchAllPropertyUnitsByPropertyFloorIdByPropertyIdByCid( $intPropertyFloorId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchMaintenanceNotificationEmails( $objDatabase ) {

		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'MAINTENANCE_NOTIFICATION_EMAIL', $this->getId(), $this->m_intCid, $objDatabase );
		if( false == isset( $objPropertyPreference ) ) {
			return NULL;
		}

		return CEmail::createEmailArrayFromString( $objPropertyPreference->getValue() );
	}

	public function fetchPaymentNotificationEmails( $objDatabase ) {

		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'PAYMENT_NOTIFICATION_EMAIL', $this->getId(), $this->m_intCid, $objDatabase );
		if( false == isset( $objPropertyPreference ) ) {
			return NULL;
		}

		return CEmail::createEmailArrayFromString( $objPropertyPreference->getValue() );
	}

	public function fetchPaymentReturnNotificationEmails( $objDatabase ) {

		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'PAYMENT_RETURN_NOTIFICATION_EMAIL', $this->getId(), $this->m_intCid, $objDatabase );
		if( false == isset( $objPropertyPreference ) ) {
			return NULL;
		}

		return CEmail::createEmailArrayFromString( $objPropertyPreference->getValue() );
	}

	public function fetchDistributionNotificationEmails( $objDatabase ) {

		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'DISTRIBUTIONS_NOTIFICATION_EMAIL', $this->getId(), $this->m_intCid, $objDatabase );
		if( false == isset( $objPropertyPreference ) ) {
			return NULL;
		}

		return CEmail::createEmailArrayFromString( $objPropertyPreference->getValue() );
	}

	public function fetchFailedDistributionNotificationEmails( $objDatabase ) {

		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'FAILED_DISTRIBUTIONS_NOTIFICATION_EMAIL', $this->getId(), $this->m_intCid, $objDatabase );
		if( false == isset( $objPropertyPreference ) ) {
			return NULL;
		}

		return CEmail::createEmailArrayFromString( $objPropertyPreference->getValue() );
	}

	public function fetchApplicationNotificationEmails( $objDatabase ) {

		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'APPLICATION_NOTIFICATION_EMAIL', $this->getId(), $this->m_intCid, $objDatabase );
		if( false == isset( $objPropertyPreference ) ) {
			return NULL;
		}

		return CEmail::createEmailArrayFromString( $objPropertyPreference->getValue() );
	}

	public function fetchGuestCardNotificationEmails( $objDatabase ) {

		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'GUESTCARD_NOTIFICATION_EMAIL', $this->getId(), $this->m_intCid, $objDatabase );
		if( false == isset( $objPropertyPreference ) ) {
			return NULL;
		}

		return CEmail::createEmailArrayFromString( $objPropertyPreference->getValue() );
	}

	public function fetchGuestCardNotificationPreferenceObj( $objDatabase ) {

		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'GUESTCARD_NOTIFICATION_EMAIL', $this->getId(), $this->m_intCid, $objDatabase );
		if( false == isset( $objPropertyPreference ) ) {
			return NULL;
		}

		return $objPropertyPreference;
	}

	public function fetchManagerContactNotificationEmails( $objDatabase ) {

		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'MANAGER_CONTACT_NOTIFICATION_EMAIL', $this->getId(), $this->m_intCid, $objDatabase );
		if( false == isset( $objPropertyPreference ) ) {
			return NULL;
		}

		return CEmail::createEmailArrayFromString( $objPropertyPreference->getValue() );
	}

	public function fetchResidentPortalManagerContactNotificationEmails( $objDatabase ) {

		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'RESIDENT_PORTAL_MANAGER_CONTACT_NOTIFICATION_EMAIL', $this->getId(), $this->m_intCid, $objDatabase );
		if( false == isset( $objPropertyPreference ) ) {
			return NULL;
		}

		return CEmail::createEmailArrayFromString( $objPropertyPreference->getValue() );
	}

	public function fetchConciergeServiceNotificationEmails( $objDatabase ) {

		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'CONCIERGE_NOTIFICATION_EMAIL', $this->getId(), $this->m_intCid, $objDatabase );
		if( false == isset( $objPropertyPreference ) ) {
			return NULL;
		}

		if( false == isset( $objPropertyPreference ) ) {
			return [];
		} else {
			return CEmail::createEmailArrayFromString( $objPropertyPreference->getValue() );
		}
	}

	public function fetchCustomFormsNotificationEmails( $objDatabase ) {

		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'CUSTOM_FORM_NOTIFICATION_EMAIL', $this->getId(), $this->m_intCid, $objDatabase );
		if( false == isset( $objPropertyPreference ) ) {
			return NULL;
		}

		if( false == isset( $objPropertyPreference ) ) {
			return [];
		} else {
			return CEmail::createEmailArrayFromString( $objPropertyPreference->getValue() );
		}
	}

	public function fetchCustomerReferralNotificationEmails( $objDatabase ) {

		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'REFERRAL_NOTIFICATION_EMAIL', $this->getId(), $this->m_intCid, $objDatabase );
		if( false == isset( $objPropertyPreference ) ) {
			return NULL;
		}

		return CEmail::createEmailArrayFromString( $objPropertyPreference->getValue() );
	}

	public function fetchEnrollmentNotificationEmails( $objDatabase ) {

		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'ENROLLMENT_NOTIFICATION_EMAIL', $this->getId(), $this->m_intCid, $objDatabase );
		if( false == isset( $objPropertyPreference ) ) {
			return NULL;
		}

		return CEmail::createEmailArrayFromString( $objPropertyPreference->getValue() );
	}

	public function fetchPropertyEvent( $intCompanyEventId, $objDatabase ) {
		if( false == isset( $intCompanyEventId ) ) {
			return NULL;
		}

		return CPropertyEvents::fetchPropertyEventByCompanyEventIdByPropertyIdByCid( $intCompanyEventId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyAnnouncementByAnnouncementId( $intAnnouncementId, $objDatabase ) {
		if( false == isset( $intAnnouncementId ) ) {
			return NULL;
		}

		return CPropertyAnnouncements::fetchPropertyAnnouncementByAnnouncementIdByPropertyIdByCid( $intAnnouncementId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedAnnouncementsByPsProductId( $intPsProductId, $objDatabase ) {
		return CAnnouncements::fetchPublishedAnnouncementsByPropertyIdsByCidByAnnouncementTypeIdsByPsProduct( $this->m_intCid, [ $this->getId() ], [ CAnnouncementType::PROPERTY, CAnnouncementType::COMPANY ], $intPsProductId, $objDatabase );
	}

	public function fetchAnnouncementsByPsProductId( $intPsProductId, $objDatabase ) {
		return \Psi\Eos\Entrata\CAnnouncements::createService()->fetchAnnouncementsByPropertyIdsByCidByAnnouncementTypeIdsByPsProduct( $this->m_intCid, [ $this->getId() ], [ CAnnouncementType::PROPERTY, CAnnouncementType::COMPANY ], $intPsProductId, $objDatabase );
	}

	public function fetchUndeliveredPackageCountByCidByCustomerIdByLeaseIds( $intCustomerId, $arrintLeaseIds, $objDatabase ) {
		return CPackages::fetchUndeliveredPackageCountByPropertyIdByCidByCustomerIdByLeaseIds( $this->getId(), $this->m_intCid, $intCustomerId, $arrintLeaseIds, $objDatabase );
	}

	public function fetchUndeliveredPackageByCidByCustomerIdByLeaseIds( $intCustomerId, $arrintLeaseIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CPackages::createService()->fetchUndeliveredPackageByPropertyIdByCidByCustomerIdByLeaseIds( $this->getId(), $this->m_intCid, $intCustomerId, $arrintLeaseIds, $objDatabase );
	}

	public function fetchPropertyLeadSourcesWithNullRemotePrimaryKey( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPropertyLeadSourcesByPropertyIdWithNullRemotePrimarykeyByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyConciergeServiceByCompanyConciergeServiceId( $intCompanyConciergeServiceId, $objDatabase ) {
		return CPropertyConciergeServices::fetchPropertyConciergeServiceByCompanyConciergeServiceIdByPropertyIdByCid( $intCompanyConciergeServiceId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnassociatedCompanyCategories( $objDatabase ) {
		return CCompanyCategories::fetchUnassociatedCompanyCategoriesByPropertyIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchCompanyCategories( $objDatabase ) {
		return CCompanyCategories::fetchCompanyCategoriesByPropertyIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchUnassociatedAmenitiesWithRateAssociationsByArCascadeId( $intArCascadeId, $objDatabase ) {
		return CAmenities::createService()->fetchUnassociatedAmenitiesWithRateAssociationsByArCascadeIdByPropertyIdByCid( $intArCascadeId, $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchUnassociatedAmenitiesWithRateAssociationsByArCascadeIdByArCascadeReferenceId( $intArCascadeId, $intArCascadeReferenceId, $objDatabase ) {
		return CAmenities::createService()->fetchUnassociatedAmenitiesWithRateAssociationsByArCascadeIdByArCascadeReferenceIdByPropertyIdByCid( $intArCascadeId, $intArCascadeReferenceId, $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchCompanyCategory( $intCompanyCategoryId, $objDatabase ) {
		return CCompanyCategories::fetchCompanyCategoryByIdByPropertyIdByCid( $intCompanyCategoryId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchScheduledPaymentsAssociatedToNonIntegratedLeases( $objDatabase ) {
		return CScheduledPayments::fetchScheduledPaymentsAssociatedToNonIntegratedLeasesPropertyIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchMaintenanceProblemsByMaintenanceProblemTypeIds( $arrintMaintenanceProblemTypeIds, $objDatabase, $boolIsPropertyAssociatedProblem = false, $strProblemType = CMaintenanceProblem::INCLUDE_IN_BOTH ) {
		return CMaintenanceProblems::createService()->fetchAllMaintenanceProblemsByPropertyIdByCidByMaintenanceProblemTypeIds( $this->getId(), $this->m_intCid, $arrintMaintenanceProblemTypeIds, $objDatabase, $boolIsPropertyAssociatedProblem, $strProblemType );
	}

	public function fetchAllMaintenanceStatuses( $objDatabase ) {
		return CMaintenanceStatuses::createService()->fetchAllMaintenanceStatusesByPropertyIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchAllMaintenancePriorities( $objDatabase ) {
		return CMaintenancePriorities::createService()->fetchAllMaintenancePrioritiesByPropertyIdByCid( $this->m_intCid, $this->getId(), $objDatabase );
	}

	public function fetchFloorplanMedias( $objDatabase, $boolFetch3DFloorPlanMedia = true ) {
		$arrintMarketingMediaSubTypeIds = [ CMarketingMediaSubType::FLOORPLAN_2D, CMarketingMediaSubType::FLOORPLAN_3D ];

		if( false == $boolFetch3DFloorPlanMedia ) {
			$arrintMarketingMediaSubTypeIds = [ CMarketingMediaSubType::FLOORPLAN_2D ];
		}

		$arrintPropertyIds = ( false == isset( $this->m_arrintChildPropertyIds ) ) ? $this->getOrFetchChildPropertyIds( $objDatabase ) : $this->getChildPropertyIds();
		$arrintPropertyIds[$this->getId()] = $this->getId();

		$arrobjFloorplans = CPropertyFloorplans::createService()->fetchPropertyFloorplansByPropertyIdsByCid( $arrintPropertyIds, $this->getCid(), $objDatabase );

		if( true == valArr( $arrobjFloorplans ) ) {
			return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsByReferenceIdsByMediaTypeIdByMediaSubTypeIdsByDetailsFieldByCid( array_keys( $arrobjFloorplans ), CMarketingMediaType::FLOORPLAN_MEDIA, $arrintMarketingMediaSubTypeIds, 'is_default', [ 1 ], $this->getCid(), $objDatabase, 'mma.order_num' );
		}
		return NULL;
	}

	public function fetchAllFloorplanMedias( $objDatabase, $boolIsConsiderChildProperty = false ) {

		if( true == $boolIsConsiderChildProperty ) {
			$arrintPropertyIds = ( false == isset( $this->m_arrintChildPropertyIds ) ) ? $this->getOrFetchChildPropertyIds( $objDatabase ) : $this->getChildPropertyIds();

			$arrintPropertyIds[$this->getId()] = $this->getId();
		} else {
			$arrintPropertyIds[$this->getId()] = $this->getId();
		}
		$arrobjFloorplans = CPropertyFloorplans::createService()->fetchPropertyFloorplansByPropertyIdsByCid( $arrintPropertyIds, $this->getCid(), $objDatabase );

		if( true == valArr( $arrobjFloorplans ) ) {
			return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsByReferenceIdsByMediaTypeIdByMediaSubTypeIdsByCid( array_keys( $arrobjFloorplans ), CMarketingMediaType::FLOORPLAN_MEDIA, [ CMarketingMediaSubType::FLOORPLAN_2D, CMarketingMediaSubType::FLOORPLAN_3D, CMarketingMediaSubType::FLOORPLAN_PDF ], $this->getCid(), $objDatabase );
		}
		return [];
	}

	public function fetchAllPublishedFloorplanMedias( $objDatabase, $boolIsConsiderChildProperty = false ) {

		if( true == $boolIsConsiderChildProperty ) {
			$arrintPropertyIds = ( false == isset( $this->m_arrintChildPropertyIds ) ) ? $this->getOrFetchChildPropertyIds( $objDatabase ) : $this->getChildPropertyIds();

			$arrintPropertyIds[$this->getId()] = $this->getId();
		} else {
			$arrintPropertyIds[$this->getId()] = $this->getId();
		}
		$arrobjFloorplans = CPropertyFloorplans::createService()->fetchPublishedPropertyFloorplansByPropertyIdsByCid( $arrintPropertyIds, $this->getCid(), $objDatabase );

		if( true == valArr( $arrobjFloorplans ) ) {
			return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsByReferenceIdsByMediaTypeIdByMediaSubTypeIdsByCid( array_keys( $arrobjFloorplans ), CMarketingMediaType::FLOORPLAN_MEDIA, [ CMarketingMediaSubType::FLOORPLAN_2D, CMarketingMediaSubType::FLOORPLAN_3D, CMarketingMediaSubType::FLOORPLAN_PDF ], $this->getCid(), $objDatabase );
		}
		return [];
	}

	public function fetchFloorplan2D3DMedias( $objDatabase, $boolIsConsiderChildProperty = false ) {

		if( true == $boolIsConsiderChildProperty ) {
			$arrintPropertyIds = ( false == isset( $this->m_arrintChildPropertyIds ) ) ? $this->getOrFetchChildPropertyIds( $objDatabase ) : $this->getChildPropertyIds();

			$arrintPropertyIds[$this->getId()] = $this->getId();
		} else {
			$arrintPropertyIds[$this->getId()] = $this->getId();
		}
		$arrobjFloorplans = CPropertyFloorplans::createService()->fetchPropertyFloorplansByPropertyIdsByCid( $arrintPropertyIds, $this->getCid(), $objDatabase );

		if( true == valArr( $arrobjFloorplans ) ) {
			return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsByReferenceIdsByMediaTypeIdByMediaSubTypeIdsByCid( array_keys( $arrobjFloorplans ), CMarketingMediaType::FLOORPLAN_MEDIA, [ CMarketingMediaSubType::FLOORPLAN_2D, CMarketingMediaSubType::FLOORPLAN_3D ], $this->getCid(), $objDatabase );
		}
		return [];
	}

	public function fetchPublishedFloorplan2D3DMedias( $objDatabase, $boolIsConsiderChildProperty = false ) {

		if( true == $boolIsConsiderChildProperty ) {
			$arrintPropertyIds = ( false == isset( $this->m_arrintChildPropertyIds ) ) ? $this->getOrFetchChildPropertyIds( $objDatabase ) : $this->getChildPropertyIds();

			$arrintPropertyIds[$this->getId()] = $this->getId();
		} else {
			$arrintPropertyIds[$this->getId()] = $this->getId();
		}
		$arrobjFloorplans = CPropertyFloorplans::createService()->fetchPublishedPropertyFloorplansByPropertyIdsByCid( $arrintPropertyIds, $this->getCid(), $objDatabase );

		if( true == valArr( $arrobjFloorplans ) ) {
			return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsByReferenceIdsByMediaTypeIdByMediaSubTypeIdsByCid( array_keys( $arrobjFloorplans ), CMarketingMediaType::FLOORPLAN_MEDIA, [ CMarketingMediaSubType::FLOORPLAN_2D, CMarketingMediaSubType::FLOORPLAN_3D ], $this->getCid(), $objDatabase );
		}
		return [];
	}

	public function fetchPublishedFloorplansMediaCount( $objDatabase ) {

		$arrobjFloorplans = CPropertyFloorplans::createService()->fetchPublishedPropertyFloorplansByPropertyIdsByCid( [ $this->getId() ], $this->getCid(), $objDatabase );

		if( true == valArr( $arrobjFloorplans ) ) {
			return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationCountByReferenceIdsByMediaTypeIdByMediaSubTypeIdsByCid( array_keys( $arrobjFloorplans ), CMarketingMediaType::FLOORPLAN_MEDIA, [ CMarketingMediaSubType::FLOORPLAN_2D, CMarketingMediaSubType::FLOORPLAN_3D, CMarketingMediaSubType::FLOORPLAN_PDF ], $this->getCid(), $objDatabase );
		}
		return 0;
	}

	public function fetchDefaultAmenitiesByAminityTypeId( $intAmenityTypeId, $objDatabase, $boolIsIncludeAllCascades = false ) {

		if( false == valId( $intAmenityTypeId ) ) {
			return NULL;
		}

		$arrintPropertyIds = ( false == isset( $this->m_arrintChildPropertyIds ) ) ? $this->getOrFetchChildPropertyIds( $objDatabase ) : $this->getChildPropertyIds();

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return CAmenities::createService()->fetchDefaultAmenitiesByAmenityTypeIdByPropertyIdsByCid( $intAmenityTypeId, $arrintPropertyIds, $this->getCid(), $objDatabase, $boolIsIncludeAllCascades );
	}

	public function fetchPublishedPropertyFloorplans( $objDatabase, $boolIsIncludeChildProperties = true, $intLimit = NULL ) {

		if( true == isset( $this->m_arrintChildPropertyIds ) && true == $boolIsIncludeChildProperties ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		if( false == valId( $this->m_intParentPropertyId ) ) {
			$this->m_intParentPropertyId = self::fetchParentPropertyId( $objDatabase );
		}

		if( true == isset( $this->m_intParentPropertyId ) && true == valId( $this->m_intParentPropertyId ) ) {
			$arrintPropertyIds[$this->m_intParentPropertyId] = $this->m_intParentPropertyId;
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return CPropertyFloorplans::createService()->fetchPublishedPropertyFloorplansByPropertyIdsByCid( $arrintPropertyIds, $this->getCid(), $objDatabase, $intLimit );
	}

	public function fetchPublishedPropertyFloorplansWithAvailableUnitsCountByLeaseStartDate( $strLeaseStartDate, $objDatabase ) {

		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		if( false == valId( $this->m_intParentPropertyId ) ) {
			$this->m_intParentPropertyId = self::fetchParentPropertyId( $objDatabase );
		}

		if( true == isset( $this->m_intParentPropertyId ) && true == valId( $this->m_intParentPropertyId ) ) {
			$arrintPropertyIds[$this->m_intParentPropertyId] = $this->m_intParentPropertyId;
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return CPropertyFloorplans::createService()->fetchPublishedPropertyFloorplansByPropertyIdsByLeaseStartDateWithAvailableUnitsCountByCid( $arrintPropertyIds, $strLeaseStartDate, $this->getOrFetchPropertyPreferences( $objDatabase ), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedPropertyFloorplansByNoOfBedRooms( $intNoOfBedRooms, $objDatabase ) {
		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return CPropertyFloorplans::createService()->fetchPublishedPropertyFloorplansByPropertyIdsByNoOfBedRoomsByCid( $arrintPropertyIds, $intNoOfBedRooms, $this->getCid(), $objDatabase );
	}

	public function fetchPublishedPropertyFloorplansByFloorplanGroupIdsByNoOfBedRooms( $arrintFloorplanGroupIds, $intNoOfBedRooms, $objDatabase ) {
		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return CPropertyFloorplans::createService()->fetchPublishedPropertyFloorplansByPropertyIdsByFloorplanGroupIdsByNoOfBedRoomsByCid( $arrintPropertyIds, $arrintFloorplanGroupIds, $intNoOfBedRooms, $this->getCid(), $objDatabase );
	}

	public function fetchPublishedPropertyAddresses( $objDatabase ) {
		return CPropertyAddresses::createService()->fetchPublishedPropertyAddressesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedPropertyEmailAddresses( $objDatabase ) {
		return CPropertyEmailAddresses::fetchPublishedPropertyEmailAddressesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedPropertyPhoneNumbersByPhoneNumberTypeIds( $arrintPhoneNumberTypeIds, $objDatabase ) {
		return CPropertyPhoneNumbers::fetchPublishedPropertyPhoneNumbersByPropertyIdPhoneNumberTypeIdsByCid( $this->getId(), $arrintPhoneNumberTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchPublishedPropertyPhoneNumbers( $objDatabase ) {
		return CPropertyPhoneNumbers::fetchPublishedPropertyPhoneNumbersByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedProspectPortalSpecialRateAssociations( $objDatabase ) {
		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return \Psi\Eos\Entrata\Custom\CSpecialRateAssociations::createService()->fetchCurrentPublishedSpecialRateAssocaitionsByPropertyIdsByCid( $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchPublishedProspectPortalSpecialRateAssociationsCount( $objDatabase, $intArCascadeId = NULL, $arrintArCascadeReferenceIds = NULL ) {
		return current( ( array ) \Psi\Eos\Entrata\Custom\CSpecialRateAssociations::createService()->fetchPublishedProspectPortalSpecialRateAssociationsCountByPropertyIdsByCid( [ $this->getId() ], $this->getCid(), $objDatabase, $intArCascadeId, $arrintArCascadeReferenceIds ) );
	}

	public function fetchIntegratedRateAssociations( $objDatabase ) {
		return \Psi\Eos\Entrata\Custom\CSpecialRateAssociations::createService()->fetchIntegratedRateAssociationsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedProspectPortalSpecials( $objDatabase, $boolIsLoadChildPropertySpecials = true, $strMoveInDate = NULL, $arrintExcludeSpecialTypeIds = NULL ) {
		if( true == $boolIsLoadChildPropertySpecials && true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return CSpecials::createService()->fetchCurrentPublishedProspectSpecialsByPropertyIdsByCid( $arrintPropertyIds, $this->getCid(), $objDatabase, $strMoveInDate, $arrintExcludeSpecialTypeIds );
	}

	public function fetchWebVisiblePropertyAddresses( $objDatabase ) {
		return CPropertyAddresses::createService()->fetchWebVisiblePropertyAddressesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebVisiblePropertyPhoneNumbers( $objDatabase ) {
		return CPropertyPhoneNumbers::fetchWebVisiblePropertyPhoneNumbersByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyIntegrationDatabase( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyIntegrationDatabases::createService()->fetchPropertyIntegrationDatabaseByPropertyIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchRelatedPropertyIdsByPsProductId( $intPsProductId, $objDatabase ) {
		$intParentPropertytId = ( true == is_null( $this->getPropertyId() ) ? $this->getId() : $this->getPropertyId() );

		return \Psi\Eos\Entrata\CProperties::createService()->fetchRelatedPropertyIdsByParentPropertyIdByCidByPsProductId( $intParentPropertytId, $this->getCid(), $intPsProductId, $objDatabase );
	}

	public function fetchCustomerClassifiedsWithCustomerNameByCompanyClassifiedCategoryId( $intCompanyClassifiedCategoryId, $objDatabase ) {
		return CCustomerClassifieds::fetchCustomerClassifiedsWithCustomerNameByCompanyClassifiedCategoryIdByPropertyIdByCid( $intCompanyClassifiedCategoryId, $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchPaginatedCompanyClassifiedCategories( $intPageNo, $intCountPerPage, $objDatabase ) {
		return CCompanyClassifiedCategories::fetchPaginatedCompanyClassifiedCategoriesByPropertyIdByCid( $this->getId(), $this->getCid(), $intPageNo, $intCountPerPage, $objDatabase );
	}

	public function fetchCustomerClassifiedById( $intCustomerClassifiedId, $objDatabase ) {
		if( false == valId( $intCustomerClassifiedId ) ) {
			return NULL;
		}

		return CCustomerClassifieds::fetchCustomerClassifiedByIdByPropertyIdByCid( $intCustomerClassifiedId, $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchPropertyClassifiedCategoryByCompanyClassifiedCategoryId( $intCompanyClassifiedCategoryId, $objDatabase ) {
		return CPropertyClassifiedCategories::fetchPropertyClassifiedCategoryByCompanyClassifiedCategoryIdByPropertyIdByCid( $intCompanyClassifiedCategoryId, $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchPropertyMaintenanceStatuses( $objDatabase ) {
		return CPropertyMaintenanceStatuses::createService()->fetchCustomPropertyMaintenanceStatusesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyMaintenanceStatusByMaintenanceStatusId( $intMaintenanceStatusId, $objDatabase ) {
		return CPropertyMaintenanceStatuses::createService()->fetchPropertyMaintenanceStatusByMaintenanceStatusIdByPropertyIdByCid( $intMaintenanceStatusId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyMaintenanceStatusById( $intPropertyMaintenanceStatusId, $objDatabase ) {
		return CPropertyMaintenanceStatuses::createService()->fetchPropertyMaintenanceStatusByIdByPropertyIdByCid( $intPropertyMaintenanceStatusId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyWrapperByPropertyWrapperTypeId( $intPropertyWrapperTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyWrappers::createService()->fetchPropertyWrapperByPropertyWrapperTypeIdByPropertyIdByCid( $intPropertyWrapperTypeId, $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchPropertyNotifications( $objDatabase ) {
		return CPropertyNotifications::createService()->fetchPropertyNotificationsByPropertyIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchPropertySellingPoints( $objDatabase, $strSellingPointsSortBy = NULL ) {
		return CPropertySellingPoints::fetchPropertySellingPointsByCidByPropertyId( $this->m_intCid, $this->getId(), $objDatabase, $strSellingPointsSortBy );
	}

	public function fetchPropertySellingPointById( $intPropertySellingPointId, $objDatabase ) {
		return CPropertySellingPoints::fetchPropertySellingPointByCidByPropertyIdById( $this->m_intCid, $this->getId(), $intPropertySellingPointId, $objDatabase );
	}

	public function fetchKeyedParcelAlertPropertyNotifications( $objDatabase ) {
		return CPropertyNotifications::createService()->fetchPropertyNotificationsByPropertyIdByNotificationKeysByCid( $this->getId(), [ 'PARCEL_ALERT_SMS_TEXT', 'PARCEL_ALERT_EMAIL_TEXT', 'PARCEL_ALERT_EMAIL_PICKED_UP_TEXT', 'PARCEL_ALERT_EMAIL_DELIVERED_TEXT' ], $this->getCid(), $objDatabase );
	}

	public function fetchKeyedPropertyNotifications( $objDatabase ) {
		return CPropertyNotifications::createService()->fetchPropertyNotificationsKeyedByPropertyIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchPropertyNotificationByKey( $strKey, $objDatabase ) {
		return CPropertyNotifications::createService()->fetchPropertyNotificationByKeyPropertyIdByCid( $strKey, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyTransmissionVendorByCompanyTransmissionVendorIdByTransmissionTypeId( $intCompanyTransmissionVendorId = NULL, $intTransmissionTypeId = NULL, $objDatabase ) {
		return CPropertyTransmissionVendors::fetchPropertyTransmissionVendorByPropertyIdByCompanyTransmissionVendorIdByTransmissionTypeIdByCid( $this->getId(), $intCompanyTransmissionVendorId, $intTransmissionTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyTransmissionVendors( $objDatabase ) {
		return CPropertyTransmissionVendors::fetchPropertyTransmissionVendorsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyTransmissionVendorByTransmissionVendorIdByTransmissionTypeId( $intTransmissionVendorId, $intTransmissionTypeId, $objDatabase ) {
		return CPropertyTransmissionVendors::fetchPropertyTransmissionVendorByPropertyIdByTransmissionVendorIdByTransmissionTypeIdByCid( $this->getId(), $intTransmissionVendorId, $intTransmissionTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyTransmissionVendorByTransmissionTypeId( $intTransmissionTypeId, $objClientDatabase ) {
		return CPropertyTransmissionVendors::fetchPropertyTransmissionVendorByPropertyIdByTransmissionTypeIdByCid( $this->getId(), $intTransmissionTypeId, $this->getCid(), $objClientDatabase );
	}

	public function fetchCompanyTransmissionVendorByTransmissionVendorIds( $arrintTransmissionVendorIds, $objClientDatabase ) {
		return \Psi\Eos\Entrata\CCompanyTransmissionVendors::createService()->fetchCompanyTransmissionVendorByPropertyIdByTransmissionVendorIdsByCid( $this->getId(), $arrintTransmissionVendorIds, $this->getCid(), $objClientDatabase );
	}

	public function fetchPropertyTransmissionVendorByCompanyTransmissionVendorId( $intCompanyTransmissionVendorId, $objClientDatabase ) {
		return CPropertyTransmissionVendors::fetchPropertyTransmissionVendorByPropertyIdByCompanyTransmissionVendorIdByCid( $this->getId(), $intCompanyTransmissionVendorId, $this->getCid(), $objClientDatabase );
	}

	public function fetchPropertyTransmissionVendorByTransmissionVendorId( $intTransmissionVendorId, $objClientDatabase ) {
		return CPropertyTransmissionVendors::fetchPropertyTransmissionVendorByPropertyIdByTransmissionVendorIdByCid( $this->getId(), $intTransmissionVendorId, $this->getCid(), $objClientDatabase );
	}

	public function fetchPropertyTransmissionVendorsByTransmissionTypeId( $intTransmissionTypeId, $objClientDatabase ) {
		$arrintPropertyIds = ( false == isset( $this->m_arrintChildPropertyIds ) ) ? $this->getOrFetchChildPropertyIds( $objClientDatabase ) : $this->m_arrintChildPropertyIds;

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return CPropertyTransmissionVendors::fetchPropertyTransmissionVendorsByPropertyIdsByTransmissionTypeIdByCid( $arrintPropertyIds, $intTransmissionTypeId, $this->getCid(), $objClientDatabase );
	}

	public function fetchLeaseExpirationLimits( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseExpirationLimits::createService()->fetchLeaseExpirationLimitsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyFloorplanByUnitTypeId( $intUnitTypeId, $objDatabase ) {
		return CPropertyFloorplans::createService()->fetchPropertyFloorplanByUnitTypeIdByCid( $intUnitTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchWebVisiblePropertyFloorplanByUnitTypeId( $intUnitTypeId, $objDatabase ) {
		return CPropertyFloorplans::createService()->fetchWebVisiblePropertyFloorplanByUnitTypeIdByCid( $intUnitTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUnitsByUnitTypeId( $intUnitTypeId, $objDatabase, $arrintPropertyIds = NULL, $boolOrderBy = false ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			$arrintPropertyIds = [ $this->getId() ];
		}

		return CPropertyUnits::createService()->fetchPropertyUnitsByUnitTypeIdByPropertyIdByCid( $arrintPropertyIds, $intUnitTypeId, $this->getCid(), $objDatabase, false, $boolOrderBy );
	}

	public function fetchPropertyUnitsByUnitTypeIds( $intUnitTypeIds, $objDatabase, $arrintPropertyIds = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			$arrintPropertyIds = [ $this->getId() ];
		}

		return CPropertyUnits::createService()->fetchPropertyUnitsByUnitTypeIdsByPropertyIdByCid( $arrintPropertyIds, $intUnitTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUnitsByFloorplanId( $intFloorplanId, $objDatabase ) {
		return CPropertyUnits::createService()->fetchPropertyUnitsByFloorplanIdByPropertyIdByCid( $this->getId(), $intFloorplanId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyLeadSourceByInternetListingServiceId( $intInternetListingServiceId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPropertyLeadSourceByInternetListingServiceIdByPropertyIdByCid( $intInternetListingServiceId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedPropertyLeadSourceByInternetListingServiceId( $intInternetListingServiceId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPublishedPropertyLeadSourceByInternetListingServiceIdByPropertyIdByCid( $intInternetListingServiceId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCareersCount( $objDatabase ) {
		return \Psi\Eos\Entrata\CCareers::createService()->fetchCareersCountByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCareers( $objDatabase ) {
		return \Psi\Eos\Entrata\CCareers::createService()->fetchCareersByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchOpenPropertyCareers( $objDatabase ) {
		return \Psi\Eos\Entrata\CCareers::createService()->fetchOpenPropertyCareersByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchTimeZone( $objDatabase ) {
		return CTimeZones::fetchTimeZoneById( $this->getTimeZoneId(), $objDatabase );
	}

	public function fetchPublishedSpecialsByUnitTypeIds( $arrintUnitTypeIds, $objDatabase, $intLeaseStartWindowId = NULL, $strMoveInDate = NULL ) {
		if( false == valArr( $arrintUnitTypeIds ) ) {
			return NULL;
		}

		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return CSpecials::createService()->fetchPublishedSpecialsByPropertyIdByUnitTypeIdsByCid( $arrintPropertyIds, $arrintUnitTypeIds, $this->getCid(), $objDatabase, $intLeaseStartWindowId, $strMoveInDate );
	}

	public function fetchPublishedSpecialsByUnitSpaceIds( $arrintUnitSpaces, $objDatabase, $boolIsMarketedOnly = false, $strMoveInDate = NULL ) {
		if( false == valArr( $arrintUnitSpaces ) ) {
			return NULL;
		}

		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return CSpecials::createService()->fetchPublishedSpecialsByPropertyIdByUnitSpaceIdsByCid( $arrintPropertyIds, $arrintUnitSpaces, $this->getCid(), $objDatabase, $boolIsMarketedOnly, $strMoveInDate );
	}

	public function fetchFloorplanCount( $objDatabase ) {
		return CPropertyFloorplans::createService()->fetchFloorplanCountByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchChildFloorplanCount( $objDatabase ) {
		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return CPropertyFloorplans::createService()->fetchFloorplanCountByChildPropertyIdsByCid( $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyFloorsCount( $objDatabase ) {
		return CPropertyFloors::createService()->fetchPropertyFloorsCountByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedPositionedPropertyUnitCount( $objDatabase ) {
		return CPropertyUnits::createService()->fetchPositionedPropertyUnitCountByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedPositionedPropertyUnits( $objDatabase, $arrintPropertyIds = NULL ) {
		if( true == valArr( $arrintPropertyIds ) ) {
			return CPropertyUnits::createService()->fetchPositionedPropertyUnitsByPropertyIdByCid( $arrintPropertyIds, $this->getCid(), $objDatabase );
		} else {
			return CPropertyUnits::createService()->fetchPositionedPropertyUnitsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		}
	}

	public function fetchReviewCountByReviewTypeIds( $arrintReviewTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CReviews::createService()->fetchReviewCountByPropertyIdByReviewTypeIdsByCid( $this->getId(), $arrintReviewTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchActivePublishedReviewsByReviewTypeId( $intReviewTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CReviews::createService()->fetchActivePublishedReviewsByPropertyIdByCidByReviewTypeId( $this->getId(), $this->getCid(), $intReviewTypeId, $objDatabase );
	}

	public function fetchPropertyFloorplansByFloorplanGroupIds( $arrintFloorplanGroupIds, $objDatabase ) {
		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return CPropertyFloorplans::createService()->fetchPropertyFloorplansByPropertyIdsByFloorplanGroupIdsByCid( $arrintPropertyIds, $arrintFloorplanGroupIds, $this->getCid(), $objDatabase );
	}

	public function fetchFloorplanGroups( $objDatabase ) {
		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return \Psi\Eos\Entrata\CFloorplanGroups::createService()->fetchFloorplanGroupsByPropertyIdsByCid( $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyNotificationsByKeys( $arrstrKeys, $objClientDatabase ) {
		return CPropertyNotifications::createService()->fetchPropertyNotificationsByPropertyIdByNotificationKeysByCid( $this->getId(), $arrstrKeys, $this->getCid(), $objClientDatabase );
	}

	public function fetchPropertyGlSetting( $objClientDatabase ) {
		return CPropertyGlSettings::fetchCustomPropertyGlSettingsByPropertyIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchGlChartBySystemCode( $strSystemCode, $objDatabase ) {
		return CGlCharts::fetchGlChartBySystemCodeByCid( $strSystemCode, $this->getCid(), $objDatabase );
	}

	// @TODO-SEO: We need to remove this function as it is not in use.
	public function fetchPublishedProspectPortalCompanyPopupSpecials( $objDatabase ) {
		return CSpecials::createService()->fetchPublishedProspectPortalPopupSpecialsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchInsuranceCarrierProperty( $objDatabase ) {
		return \Psi\Eos\Insurance\CInsuranceCarrierProperties::createService()->fetchInsuranceCarrierPropertyByPropertyId( $this->getId(), $objDatabase );
	}

	public function fetchPropertyUtilitySettings( $objDatabase ) {
		return \Psi\Eos\Utilities\CPropertyUtilitySettings::createService()->fetchPropertyUtilitySettingsByPropertyId( $this->getId(), $objDatabase );
	}

	public function fetchPropertySmsMessages( $objDatabase ) {
		return CPropertySmsMessages::createService()->fetchCustomPropertySmsMessagesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertySmsMessageById( $intId, $objDatabase ) {
		return CPropertySmsMessages::createService()->fetchPropertySmsMessageByIdByPropertyIdByCid( $intId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchChildPropertyIds( $objDatabase, $arrintRequiredProducts = [] ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchChildPropertyIdsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $arrintRequiredProducts );
	}

	public function fetchParentPropertyId( $objDatabase, $arrintRequiredProducts = [] ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchParentPropertyIdByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $arrintRequiredProducts );
	}

	public function fetchChildPropertyById( $intChildPropertyId, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByPropertyIdByIdByCid( $this->getId(), $intChildPropertyId, $this->getCid(), $objDatabase );
	}

	public function fetchChildProperties( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchCustomPropertiesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchRelatedPropertyIds( $objDatabase ) {
		$intParentPropertytId = is_null( $this->getPropertyId() ) ? $this->getId() : $this->getPropertyId();

		return \Psi\Eos\Entrata\CProperties::createService()->fetchRelatedPropertyIdsByParentPropertyIdByCid( $intParentPropertytId, $this->getCid(), $objDatabase );
	}

	public function fetchPublishedActiveCompanyEventCount( $objDatabase ) {
		return CCompanyEvents::fetchPublishedActiveCompanyEventCountByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedActiveSortedCurrentMonthsCompanyEvents( $objDatabase ) {
		return CCompanyEvents::fetchPublishedActiveSortedCurrentMonthsCompanyEventsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedActiveSortedCompanyEventsByMonthByYear( $intMonth, $intYear, $objDatabase ) {
		return CCompanyEvents::fetchPublishedActiveSortedCompanyEventsByPropertyIdByMonthByYearByCid( $this->getId(), $intMonth, $intYear, $this->getCid(), $objDatabase );
	}

	public function fetchPublishedFutureActiveSortedCompanyEvents( $objDatabase ) {
		return CCompanyEvents::fetchPublishedFutureActiveSortedCompanyEventsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedFutureActiveSortedCompanyAnnouncements( $objDatabase ) {
		return CAnnouncements::fetchPublishedFutureActiveSortedCompanyAnnouncementsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedActiveSortedCompanyEventsByDayByMonthByYear( $intDay, $intMonth, $intYear, $objDatabase ) {
		return CCompanyEvents::fetchPublishedActiveSortedCompanyEventsByPropertyIdByDateByCid( $this->getId(), $intDay, $intMonth, $intYear, $this->getCid(), $objDatabase );
	}

	public function fetchAmenityRateAssociationById( $intRateAssociationId, $objDatabase ) {
		return CAmenityRateAssociations::createService()->fetchAmenityRateAssociationByIdByPropertyIdByCid( $this->getId(), $intRateAssociationId, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyEmployees( $objDatabase, $boolIsDisabled = NULL, $intEmployeeStatusTypeId = NULL, $intCompanyUserId = NULL ) {
		return CCompanyEmployees::createService()->fetchCompanyEmployeesByPropertyIdByCid( $this->getId(), $this->getCid(), $boolIsDisabled, $intEmployeeStatusTypeId, $intCompanyUserId, $objDatabase );
	}

	public function fetchCompanyEmployeeContactsByCompanyEmployeeContactTypeId( $intCompanyEmployeeContactTypeId, $objDatabase, $boolIsSortByOrderNum = false, $boolIsPhoneNumberRequired = false ) {
		return CCompanyEmployeeContacts::createService()->fetchSimpleCompanyEmployeeContactsByCidByPropertyIdByCompanyEmployeeContactTypeId( $this->getCid(), $this->getId(), $intCompanyEmployeeContactTypeId, $objDatabase, $boolIsSortByOrderNum, $boolIsPhoneNumberRequired );
	}

	public function fetchCompanyEmployeeContacts( $objDatabase, $boolIsSortByOrderNum = false ) {
		return CCompanyEmployeeContacts::createService()->fetchCompanyEmployeeContactsByCidByPropertyId( $this->getCid(), $this->getId(), $objDatabase, $boolIsSortByOrderNum );
	}

	public function fetchCompanyEmployeeContactById( $intCompanyEmployeeContactId, $objDatabase ) {
		return CCompanyEmployeeContacts::createService()->fetchCompanyEmployeeContactByIdByPropertyIdByCid( $intCompanyEmployeeContactId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyTaxes( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyTaxes::createService()->fetchCustomPropertyTaxesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyTaxById( $intPropertyTaxId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyTaxes::createService()->fetchPropertyTaxByIdByPropertyIdByCid( $intPropertyTaxId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCallFileByGreetingTypeId( $intGreetingTypeId, $objDatabase ) {
		return CCallFiles::fetchCallFileByPropertyIdByGreetingTypeId( $this->getId(), $intGreetingTypeId, $objDatabase );
	}

	public function fetchPropertyLeadSourceByCallTrackingExtension( $intExtension, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPropertyLeadSourceByPropertyIdByCallTrackingExtensionByCid( $this->getId(), $intExtension, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseTermById( $intLeaseTermId, $objDatabase ) {
		return CLeaseTerms::fetchLeaseTermByPropertyIdByIdByCid( $this->getId(), $intLeaseTermId, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerRelationshipByIdByOccupancyTypeId( $intCustomerRelationshipId, $intOccupancyTypeId, $objDatabase, $boolIsDefaultCustomerRelationship = false ) {
		return CCustomerRelationships::fetchCustomerRelationshipByIdByOccupancyTypeIdByPropertyIdByCid( $intCustomerRelationshipId, $intOccupancyTypeId, $this->getId(), $this->getCid(), $objDatabase, $boolIsDefaultCustomerRelationship );
	}

	public function fetchCustomerRelationshipByNameByOccupancyTypeId( $strName, $intOccupancyTypeId, $objDatabase ) {
		return CCustomerRelationships::fetchCustomerRelationshipByNameByOccupancyTypeIdByPropertyIdByCid( $strName, $intOccupancyTypeId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCustomerRelationshipsByOccupancyTypeIds( $arrintOccupancyTypeIds, $objDatabase, $boolIsShowInEntrata = false, $boolIsShowInWebsite = false ) {
		$intPropertyId = ( false == is_null( $this->getChildProperty() ) && true == valObj( $this->getChildProperty(), 'CProperty' ) ) ? $this->getChildProperty()->getId() : $this->getId();

		return CCustomerRelationships::fetchCustomerRelationshipsByOccupancyTypeIdsByPropertyIdByCid( $arrintOccupancyTypeIds, $intPropertyId, $this->getCid(), $objDatabase, $boolIsShowInEntrata, $boolIsShowInWebsite );
	}

	public function fetchCustomerRelationshipsDataByOccupancyTypeIds( $objDatabase, $boolIsShowInEntrata = false, $boolIsShowInWebsite = false, $arrintOccupancyTypeIds ) {
		$intPropertyId = ( false == is_null( $this->getChildProperty() ) && true == valObj( $this->getChildProperty(), 'CProperty' ) ) ? $this->getChildProperty()->getId() : $this->getId();

		return CCustomerRelationships::fetchCustomerRelationshipsDataByOccupancyTypeIdsByPropertyIdByCid( $intPropertyId, $this->getCid(), $objDatabase, $boolIsShowInEntrata, $boolIsShowInWebsite, $arrintOccupancyTypeIds );
	}

	public function fetchPublishedLeaseTermsByLeaseStartWindowIds( $arrintLeaseStartWindowIds, $objDatabase, $boolIsProspect = true, $boolIsRenewal = false, $boolIsEntrataOnly = true ) {
		return CLeaseTerms::fetchPublishedLeaseTermsByPropertyIdByLeaseStartWindowIdsByCid( $this->getId(), $arrintLeaseStartWindowIds, $this->getCid(), $objDatabase, $boolIsProspect, $boolIsRenewal, $boolIsEntrataOnly );
	}

	public function fetchLeaseTermByTermMonth( $intTermMonth, $objDatabase ) {
		return CLeaseTerms::fetchLeaseTermByPropertyIdByTermMonthByCid( $this->getId(), $intTermMonth, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseTermByTermMonthByName( $intTermMonth, $strName, $objDatabase ) {
		return CLeaseTerms::fetchLeaseTermByPropertyIdByTermMonthByNameByCid( $this->getId(), $intTermMonth, $strName, $this->getCid(), $objDatabase );
	}

	public function fetchBulkAssociatedUnitSpaceAmenities( $objDatabase ) {
		return CAmenities::createService()->fetchAssociatedUnitSpaceAmenitiesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAmenityRateAssociationsByArCascadeIdByArOriginReferenceIds( $intArCascadeId, $arrintApartmentAmenityIds, $objDatabase ) {
		return CAmenityRateAssociations::createService()->fetchAmenityRateAssociationsByArCascadeIdByArOriginReferenceIdsByPropertyIdByCid( $intArCascadeId, $arrintApartmentAmenityIds, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyDocumentByDocumentSubTypeId( $intDocumentSubTypeId, $objDatabase, $boolIsSystem = NULL ) {
		return CPropertyDocuments::fetchPropertyDocumentByPropertyIdByDocumentSubTypeIdByCid( $this->getId(), $intDocumentSubTypeId, $this->getCid(), $objDatabase, $boolIsSystem );
	}

	public function fetchPropertyUtilities( $objDatabase, $strLevel = 'property', $boolResidentResponsible = false, $boolSkipCustomUtilities = false ) {
		return CPropertyUtilities::fetchPropertyUtilitiesByCidByPropertyId( $this->getCid(), $this->getId(), $objDatabase, $strLevel, $boolResidentResponsible, $boolSkipCustomUtilities );
	}

	public function fetchUtilitiesWithIsResponsible( $objDatabase, $objApplication = NULL ) {
		return CUtilities::fetchUtilitiesWithIsResponsibleByCidByPropertyId( $this->getCid(), $this->getId(), $objDatabase, $objApplication );
	}

	public function fetchApplicationById( $intApplicationId, $objDatabase ) {
		return CApplications::fetchApplicationByPropertyIdByIdByCid( $this->getId(), $intApplicationId, $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpacesByUnitNumberByPropertyPreferences( $strUnitNumber, $arrobjPropertyPreferences, $objDatabase, $strMoveInDate = NULL, $boolIsFromProspectPortal = true ) {
		$boolSearchAvailableUnitsOnly = ( false == is_null( getArrayElementByKey( 'ALLOW_APPLICATION_UNAVAILABLE_UNIT', $arrobjPropertyPreferences ) ) ) ? false : true;

		$objIntegrationDatabase = ( 0 < strlen( $this->getRemotePrimaryKey() ) ) ? $this->getChildProperty()->fetchIntegrationDatabase( $objDatabase ) : NULL;

		if( false == $boolSearchAvailableUnitsOnly && true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) ) {

			$arrintIntegrationClientTypes = [ CIntegrationClientType::AMSI, CIntegrationClientType::YARDI, CIntegrationClientType::TIMBERLINE ];

			$boolSearchAvailableUnitsOnly = ( false == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), $arrintIntegrationClientTypes ) );
		}

		if( false == $boolSearchAvailableUnitsOnly ) {
			$arrobjSearchUnitSpaces = ( array ) $this->fetchUnitSpacesByUnitNumber( $strUnitNumber, $objDatabase, $boolIsFromProspectPortal );
		} else {
			$arrobjSearchUnitSpaces = ( array ) $this->fetchAvailableUnitSpacesByUnitNumber( $strUnitNumber, $arrobjPropertyPreferences, $objDatabase, $strMoveInDate, $boolIsFromProspectPortal );
		}

		return $arrobjSearchUnitSpaces;
	}

	public function fetchLateFeeFormulas( $objDatabase, $boolShowIsDisable = true, $intShowIsException = NULL, $boolGetActiveLeases = false, $arrintOccupancyTypeId = NULL, $boolShowHudFormula = true ) {
		return \Psi\Eos\Entrata\CLateFeeFormulas::createService()->fetchLateFeeFormulasByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolShowIsDisable, $intShowIsException, $boolGetActiveLeases, $arrintOccupancyTypeId, $boolShowHudFormula );
	}

	public function fetchPropertyLateFeeFormulas( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLateFeeFormulas::createService()->fetchPropertyLateFeeFormulasByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyScreeningConfiguration( $objDatabase ) {
		return CPropertyScreeningConfigurations::fetchPropertyScreeningConfigurationByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFilesByIds( $arrintCompanyMediaFileIds, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCustomCompanyMediaFilesByIdsByCid( $arrintCompanyMediaFileIds, $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationDatabase( $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationDatabases::createService()->fetchIntegrationDatabaseByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedPropertyApplicationCount( $objDatabase ) {
		return CCompanyApplications::fetchCompanyApplicationCountByPropertyIdsByCid( [ $this->getId() ], $this->getCid(), $objDatabase );
	}

	public function fetchDocumentMergeFieldsByDocumentIds( $arrintDocumentIds, $objDatabase ) {
		if( false == valArr( $arrintDocumentIds ) ) {
			return NULL;
		}

		return CDocumentMergeFields::fetchDocumentMergeFieldsByPropertyIdByDocumentIdsByCid( $this->getId(), $arrintDocumentIds, $this->getCid(), $objDatabase );
	}

	public function fetchPublishedActiveDocumentsByDocumentTypeIdByDocumentAssociationTypeIdByDocumentSubTypeIds( $intDocumentTypeId, $arrintDocumentSubTypeIds, $objDatabase, $arrstrLeaseUsagePreferences = [] ) {
		return CDocuments::fetchPublishedActiveDocumentsByPropertyIdByDocumentTypeIdByDocumentAssociationTypeIdByDocumentSubTypeIdsByCid( $this->getId(), $intDocumentTypeId, $arrintDocumentSubTypeIds, $this->getCid(), $objDatabase, $arrstrLeaseUsagePreferences );
	}

	public function fetchPublishedActiveDocumentsByDocumentTypeIdByDocumentSubTypeId( $intDocumentTypeId, $intDocumentSubTypeId, $objDatabase, $strTargetLocale = NULL ) {
		return CDocuments::fetchPublishedActiveDocumentsByPropertyIdByDocumentTypeIdByDocumentSubTypeIdByCid( $this->getId(), $intDocumentTypeId, $intDocumentSubTypeId, $this->getCid(), $objDatabase, $strTargetLocale );
	}

	public function fetchUnpublishedActiveDocumentsByDocumentTypeIdByDocumentAssociationTypeId( $intDocumentTypeId, $intDocumentAssociationTypeId, $objDatabase ) {
		return CDocuments::fetchUnpublishedActiveDocumentsByPropertyIdByDocumentTypeIdByDocumentAssociationTypeIdByCid( $this->getId(), $intDocumentTypeId, $intDocumentAssociationTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchDocumentById( $intDocumentId, $objDatabase ) {
		return CDocuments::fetchDocumentByIdByPropertyIdByCid( $intDocumentId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPetTypes( $objDatabase, $boolHideRates = NULL ) {
		return CPetTypes::createService()->fetchAssociatedPetTypesByPropertyIdsByCid( [ $this->getId() ], $this->getCid(), $objDatabase, $boolHideRates );
	}

	public function fetchPetRateAssociations( $objDatabase ) {
		// MEGARATES_COMMENTS :replaced from fetchSortedPropertyPetTypesByPropertyIdByCid
		return CPetRateAssociations::createService()->fetchSortedPetRateAssociationsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPetRateAssociationsWithPetTypeName( $objDatabase, $boolFetchData = false ) {
		return CPetRateAssociations::createService()->fetchSortedPetRateAssociationsForPropertyByPropertyIdWithPetTypeNameByCid( $this->getId(), $this->getCid(), $objDatabase, $boolFetchData );
	}

	public function fetchPropertyPetRateAssociationsByUnitSpaceIdWithPetTypeName( $intUnitSpaceId, $objDatabase ) {
		return CPetRateAssociations::createService()->fetchSortedPetRateAssociationsByPropertyIdByUnitSpaceIdsByCidWithPetTypeName( $this->getId(), [ $intUnitSpaceId ], $this->getCid(), $objDatabase );
	}

	public function fetchAmenityRateAssociationsByArCascadeIdByArOriginReferenceId( $intArCascadeId, $intArOriginReferenceId, $objDatabase ) {
		return CAmenityRateAssociations::createService()->fetchAmenityRateAssociationsByArCascadeIdByArOriginReferenceIdByPropertyIdByCid( $intArCascadeId, $intArOriginReferenceId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAmenityRateAssociationByArCascadeIdByArOriginReferenceId( $intArCascadeId, $intArOriginReferenceId, $objDatabase ) {
		return CAmenityRateAssociations::createService()->fetchAmenityRateAssociationByArCascadeIdByArOriginReferenceIdByPropertyIdByCid( $intArCascadeId, $intArOriginReferenceId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAmenityRateAssociationsByArCascadeIdArCascadeReferenceIdsByAmenityTypeId( $arrintArCascadeIdArCascadeReferenceIds, $intAmenityTypeId, $objDatabase, $boolFetchData = false, $boolIsPublished = false ) {
		return CAmenityRateAssociations::createService()->fetchAmenityRateAssociationsByArCascadeIdArCascadeReferenceIdsByAmenityTypeIdByPropertyIdByCid( $arrintArCascadeIdArCascadeReferenceIds, $intAmenityTypeId, $this->getId(), $this->getCid(), $objDatabase, $boolFetchData, $boolIsPublished );
	}

	public function fetchApplicantApplicationByApplicantIdByHistoricaldaysByLeaseIntervalTypeId( $intApplicantId, $intHistoryDays, $intLeaseIntervalTypeId, $objDatabase, $boolIsIncludeChildProperty = true ) {
		$intPropertyId = ( false == is_null( $this->getChildProperty() ) && true == valObj( $this->getChildProperty(), 'CProperty' ) ) ? $this->getChildProperty()->getId() : $this->getId();

		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationByPropertyIdByApplicantIdByHistoricaldaysByLeaseIntervalTypeIdByCid( $intPropertyId, $intApplicantId, $intHistoryDays, $intLeaseIntervalTypeId, $this->getCid(), $objDatabase, $boolIsIncludeChildProperty );
	}

	public function fetchApplicantApplicationByApplicantIdByHistoricaldaysByLeaseIntervalTypeIdByApplicationStageId( $intApplicantId, $intHistoryDays, $intLeaseIntervalTypeId, $intApplicationStageId, $objDatabase, $boolIsIncludeChildProperty = true ) {
		$intPropertyId = ( false == is_null( $this->getChildProperty() ) && true == valObj( $this->getChildProperty(), 'CProperty' ) ) ? $this->getChildProperty()->getId() : $this->getId();

		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationByPropertyIdByApplicantIdByHistoricaldaysByLeaseIntervalTypeIdByApplicationStageIdByCid( $intPropertyId, $intApplicantId, $intHistoryDays, $intLeaseIntervalTypeId, $intApplicationStageId, $this->getCid(), $objDatabase, $boolIsIncludeChildProperty );
	}

	public function fetchRateAssociationByPetTypeId( $intPetTypeId, $objDatabase ) {
		// MEGARATES_COMMENTS : Renamed from fetchPropertyPetTypeByPropertyIdByCompanyPetTypeIdWithNullPropertyUniIdByCid
		return CPetRateAssociations::createService()->fetchPetRateAssociationByArCascadeIdByPetTypeIdByPropertyId( CArCascade::PROPERTY, $intPetTypeId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPetRateAssociationsByPetTypeIds( $arrintPetTypeIds, $objDatabase ) {
		// MEGARATES_COMMENTS :replaced from fetchPropertyPetTypesByPropertyIdByPetTypeIdsByCid
		return CPetRateAssociations::createService()->fetchPetRateAssociationsByPetTypeIdsByPropertyIdByCid( $arrintPetTypeIds, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPetRateAssociationsWithDefaultPetTypeId( $objDatabase ) {
		// MEGARATES_COMMENTS :replaced from fetchPropertyPetTypesByPropertyIdWithPetTypeIdByCid
		return CPetRateAssociations::createService()->fetchPetRateAssociationsWithDefaultPetTypeIdByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchKeywords( $objSmsDatabase ) {
		return \Psi\Eos\Sms\CKeywords::createService()->fetchKeywordsByCidByPropertyId( $this->getCid(), $this->getId(), $objSmsDatabase );
	}

	public function fetchIntegratedLeases( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchIntegratedLeasesByPropertyIdsByCid( [ $this->getId() ], $this->getCid(), $objDatabase );
	}

	public function fetchLeasesByUnitSpaceId( $intUnitSpaceId, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchFutureAndActiveLeasesByUnitSpaceIdByPropertyIdByCid( $intUnitSpaceId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchDocumentPacketsByArTriggerId( $intArTriggerId, $objDatabase, $arrintLeaseIntervalId, $boolExternalPacketsOnly = false, $strPreferredLocaleCode = NULL, $arrintExcludeOccupancyTypeIds = NULL ) {
		return CDocuments::fetchDocumentPacketsByArTriggerTypeIdByPropertyIdByCid( $intArTriggerId, $this->getId(), $this->getCid(), $objDatabase, $arrintLeaseIntervalId, $boolExternalPacketsOnly, $strPreferredLocaleCode, $arrintExcludeOccupancyTypeIds );
	}

	public function fetchLeaseDocuments( $arrstrLeaseUsagePreferences, $objDatabase, $boolOtherEsignDocument = false ) {
		return CDocuments::fetchLeaseDocumentsByLeaseUsagePreferencesByPropertyIdByCid( $arrstrLeaseUsagePreferences, $this->getId(), $this->getCid(), $objDatabase, $boolOtherEsignDocument );
	}

	public function fetchSemKeywordAssociationBySemKeywordId( $intSemKeywordId, $objDatabase ) {
		return CSemKeywordAssociations::fetchSemKeywordAssociationBySemKeywordIdByPropertyIdByCid( $intSemKeywordId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUnits( $objPropertyUnitsFilter, $objDatabase, $boolIsSortByUnitNumberByBuildingName = false, $intPageNo = NULL, $intPageSize = NULL ) {
		return CPropertyUnits::createService()->fetchPropertyUnitsByPropertyIdOrderByUnitNumberByCid( $this->getId(), $objPropertyUnitsFilter, $this->getCid(), $objDatabase, $boolIsSortByUnitNumberByBuildingName, $intPageNo, $intPageSize );
	}

	public function fetchPropertyUnitsCount( $objPropertyUnitsFilter, $objDatabase ) {
		return CPropertyUnits::createService()->fetchPropertyUnitsCountByPropertyIdOrderByUnitNumberByCid( $this->getId(), $objPropertyUnitsFilter, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseTerms( $objDatabase, $boolIsProspect = true, $boolIsRenewal = false ) {
		return CLeaseTerms::fetchCustomLeaseTermsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsProspect, $boolIsRenewal );
	}

	public function fetchPublishedLeaseTerms( $objDatabase, $boolIsProspect = true, $boolIsRenewal = false, $boolShowOnWebsite = false, $boolIsDateBased = false, $strMoveInDate = NULL, $boolIncludeCommercialLeaseTerms = false, $boolIsEntrataOnly = false, $boolIncludeGroupLeaseTerms = false, $intOccupancyTypeId = NULL ) {
		return CLeaseTerms::fetchPublishedLeaseTermsByPropertyIdByCId( $this->getId(), $this->getCid(), $objDatabase, $boolIsProspect, $boolIsRenewal, $boolShowOnWebsite, $boolIsDateBased, $strMoveInDate, $boolIncludeCommercialLeaseTerms, $boolIsEntrataOnly, $boolIncludeGroupLeaseTerms, false, $intOccupancyTypeId );
	}

	public function fetchMixLeaseTerms( $objDatabase, $boolIsProspect = true, $boolIsRenewal = false, $boolShowOnWebsite = false ) {
		return CLeaseTerms::fetchPropertyDateBasedLeaseTermsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsProspect, $boolIsRenewal, $boolShowOnWebsite );
	}

	public function fetchPrimaryWebsite( $objDatabase, $boolCheckIsActive = false ) {
		return CWebsites::createService()->fetchPrimaryWebsiteByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolCheckIsActive );
	}

	public function fetchPropertyPsProducts( $objDatabase ) {
		return CPropertyPsProducts::fetchPropertyPsProductsByPropertyId( $this->getId(), $objDatabase );
	}

	public function fetchXmlSiteMapData( $objDatabase ) {
		$arrintPropertyContent['photos']					= $this->fetchMarketingMediaAssociationsCountByMarketingMediaSubTypeId( CMarketingMediaSubType::PHOTO, $objDatabase );
		$arrintPropertyContent['virtual_tours']				= $this->fetchMarketingMediaAssociationsCountByMarketingMediaSubTypeId( CMarketingMediaSubType::PROPERTY_360_TOUR, $objDatabase );
		$arrintPropertyContent['videos']					= $this->fetchMarketingMediaAssociationsCountByMarketingMediaSubTypeId( CMarketingMediaSubType::PROPERTY_VIDEO, $objDatabase );
		$arrintPropertyContent['property_floorplans']		= $this->fetchFloorplanCount( $objDatabase );
		$arrintPropertyContent['amenities']					= $this->fetchAmenityCount( $objDatabase );
		$arrintPropertyContent['events']					= $this->fetchPublishedActiveCompanyEventCount( $objDatabase );

		return $arrintPropertyContent;
	}

	public function fetchReviewAverageRatings( $arrintReviewTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CReviewDetailRatings::createService()->fetchReviewDetailRatingAverageByPropertyIdByCompanyIdByReviewTypeId( $this->getId(), $this->getCid(), $arrintReviewTypeIds, $objDatabase );
	}

	public function fetchPublishedReviewRatingTypes( $arrintReviewTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CReviewDetailRatings::createService()->fetchAllReviewDetailRatingsAverageByPropertyIdByCompanyIdByReviewTypeId( $this->getId(), $this->getCid(), $arrintReviewTypeIds, $objDatabase );
	}

	public function fetchPaginatedReviewsCount( $arrintReviewTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CReviews::createService()->fetchPaginatedReviewsCountByPropertyIdByCompanyIdByReviewTypeId( $this->getId(), $this->getCid(), $arrintReviewTypeIds, $objDatabase );
	}

	public function fetchReviews( $arrintReviewTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CReviews::createService()->fetchReviewsByPropertyIdsByCidByReviewTypeId( [ $this->getId() ], $this->getCid(), $arrintReviewTypeIds, $objDatabase );
	}

	public function fetchReviewRatingTypes( $intReviewDetailId, $objDatabase ) {
		return \Psi\Eos\Entrata\CReviewDetailRatings::createService()->fetchReviewRatingTypesByPropertyIdByCidByReviewId( $this->getId(), $this->getCid(), $intReviewDetailId, $objDatabase );
	}

	public function fetchPaginatedLatestReview( $objPagination, $arrintReviewTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CReviews::createService()->fetchPaginatedLatestReviewByPropertyIdByCompanyIdByReviewTypeId( $this->getId(), $this->getCid(), $objPagination->getPageNo(), $objPagination->getPageSize(), $arrintReviewTypeIds, $objDatabase );
	}

	public function fetchPaginatedReviews( $objPagination, $arrintReviewTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CReviews::createService()->fetchPaginatedReviewsByPropertyIdByCompanyIdByReviewTypeId( $this->getId(), $this->getCid(), $objPagination->getPageNo(), $objPagination->getPageSize(), $arrintReviewTypeIds, $objDatabase );
	}

	public function fetchRecommendedReviewRatings( $arrintReviewTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CReviews::createService()->fetchRecommendedReviewRatingsByPropertyIdByCompanyIdByReviewTypeId( $this->getId(), $this->getCid(), $arrintReviewTypeIds, $objDatabase );
	}

	public function fetchAllReviewRatings( $arrintReviewTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CReviews::createService()->fetchAllReviewRatingsByPropertyIdByCompanyIdByReviewTypeId( $this->getId(), $this->getCid(), $arrintReviewTypeIds, $objDatabase );
	}

	public function fetchUndeliveredPackagesByPropertyIdsByBuildingIds( $strBuildingIds, $objDatabase ) {

		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return CPackages::fetchDistinctUndeliveredPackagesByPropertyIdsByBuildingIdsByCid( $arrintPropertyIds, $strBuildingIds, $this->getCid(), $objDatabase );
	}

	public function fetchGreetingByGreetingTypeId( $intGreetingTypeId, $objDatabase ) {
		return CGreetings::fetchGreetingByPropertyIdByCidByGreetingTypeId( $this->getId(), $this->getCid(), $intGreetingTypeId, $objDatabase );
	}

	public function fetchPublishedSortedPropertyFloorplans( $objDatabase ) {
		return CPropertyFloorplans::createService()->fetchPublishedSortedPropertyFloorplansByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchNearByCommunities( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchNearByCommunitiesByCid( $this->m_objPrimaryPropertyAddress, $this->getCid(), $this->getId(), $objDatabase );
	}

	public function fetchCalculatedPropertyFloorplansByFloorplanAvailabilityFilterByPropertyPreferences( $objFloorplanAvailabilityFilter, $arrobjPropertyPreferences, $objDatabase, $boolIsSemesterSelection = false, $arrmixLeaseTerms = [], $arrmixSpaceConfigurationsData = [], $intFloorplanGroupId = NULL, $boolIsViewFloorplan = false ) {

		$objFloorplanAvailabilityFilter = ( false == valObj( $objFloorplanAvailabilityFilter, 'CFloorplanAvailabilityFilter' ) ) ? new CFloorplanAvailabilityFilter() : $objFloorplanAvailabilityFilter;
		$arrobjPropertyPreferences		= ( false == valArr( $arrobjPropertyPreferences ) ) ? $this->getOrFetchPropertyPreferences( $objDatabase ) : $arrobjPropertyPreferences;

		$objFloorplanAvailabilityFilter->setCid( $this->getCid() );

		if( true == valArr( $arrmixLeaseTerms ) ) {

			$arrintPropertyIds		= [];
			$arrintPropertyIds[]	= $arrmixLeaseTerms[0]['property_id'];
		} else {

			$arrintPropertyIds = $this->getParentChildPropertyIds( $objDatabase );
		}

		$objFloorplanAvailabilityFilter->setPropertyIds( $arrintPropertyIds );

		$arrobjTempPropertyFloorplans = ( array ) CPropertyFloorplans::createService()->fetchCalculatedPropertyFloorplansByFloorplanAvailabilityFilterByPropertyPreferences( $objFloorplanAvailabilityFilter, $arrobjPropertyPreferences, $objDatabase, $boolIsSemesterSelection, $intFloorplanGroupId, $boolIsViewFloorplan );

		if( true == valArr( $arrmixSpaceConfigurationsData ) && true == valArr( $arrobjTempPropertyFloorplans ) ) {
			$arrobjPropertyFloorplans	= [];
			$arrintPropertyFloorplanIds = [];
			$intTempPropertyFloroplanId = 0;

			foreach( $arrobjTempPropertyFloorplans as $intKey => $objPropertyFloorplan ) {
				if( $intTempPropertyFloroplanId != $objPropertyFloorplan->getId() ) {
					$arrmixPropertyFloorplanData = [];
				}

				if( true == array_key_exists( $objPropertyFloorplan->getId(), $arrintPropertyFloorplanIds ) ) {
					unset( $arrobjTempPropertyFloorplans[$intKey] );

					$arrmixPropertyFloorplanData = $arrobjPropertyFloorplans[$objPropertyFloorplan->getId()]->getSpaceConfigurations();
				} else {
					$arrobjPropertyFloorplans[$objPropertyFloorplan->getId()] = $objPropertyFloorplan;

					if( true == valArr( $arrmixSpaceConfigurationsData ) ) {

						foreach( $arrmixSpaceConfigurationsData as $arrmixSpaceConfigurationData ) {

							if( false == valStr( $objPropertyFloorplan->getSpaceConfigurationId() ) ) {
								$objPropertyFloorplan->setSpaceConfigurationId( CSpaceConfiguration::SPACE_CONFIG_DEFAULT );
							}

							if( $objPropertyFloorplan->getSpaceConfigurationId() == $arrmixSpaceConfigurationData['id'] ) {
								$arrmixPropertyFloorplanData[$arrmixSpaceConfigurationData['id']]['min_rent']					= '';
								$arrmixPropertyFloorplanData[$arrmixSpaceConfigurationData['id']]['max_rent']					= '';
								$arrmixPropertyFloorplanData[$arrmixSpaceConfigurationData['id']]['space_configuration_name']	= $arrmixSpaceConfigurationData['name'];
							}
						}
					}
				}

				if( true == valStr( $objPropertyFloorplan->getSpaceConfigurationId() ) ) {
					$arrmixPropertyFloorplanData[$objPropertyFloorplan->getSpaceConfigurationId()]['min_rent'] = $objPropertyFloorplan->getMinRent();
					$arrmixPropertyFloorplanData[$objPropertyFloorplan->getSpaceConfigurationId()]['max_rent'] = $objPropertyFloorplan->getMaxRent();

					if( $objPropertyFloorplan->getSpaceConfigurationId() != CSpaceConfiguration::SPACE_CONFIG_DEFAULT ) {
						if( true == valArr( $arrmixSpaceConfigurationsData ) ) {
							$arrmixTempSpaceConfigurationsData = rekeyArray( 'id', $arrmixSpaceConfigurationsData );
						}
						$arrmixPropertyFloorplanData[$objPropertyFloorplan->getSpaceConfigurationId()]['space_configuration_name'] = $arrmixTempSpaceConfigurationsData[$objPropertyFloorplan->getSpaceConfigurationId()]['name'];
					}
					$arrmixPropertyFloorplanData[$objPropertyFloorplan->getSpaceConfigurationId()]['space_configuration_name'] = $arrmixTempSpaceConfigurationsData[$objPropertyFloorplan->getSpaceConfigurationId()]['name'];
				}

				if( true == valArr( $arrmixPropertyFloorplanData ) ) {

					foreach( $arrmixPropertyFloorplanData as $intKey => $arrmixPropertyFloorplan ) {

						if( false == valStr( $arrmixPropertyFloorplan['min_rent'] ) && false == valStr( $arrmixPropertyFloorplan['max_rent'] ) ) {

							unset( $arrmixPropertyFloorplanData[$intKey] );
						}
					}
				}

				$arrobjPropertyFloorplans[$objPropertyFloorplan->getId()]->setSpaceConfigurations( $arrmixPropertyFloorplanData );
				$arrintPropertyFloorplanIds[$objPropertyFloorplan->getId()] = $objPropertyFloorplan->getId();
				$intTempPropertyFloroplanId									= $objPropertyFloorplan->getId();
			}

			return $arrobjPropertyFloorplans;

		}

		return $arrobjTempPropertyFloorplans;
	}

	public function fetchCalculatedUnitSpacesByFloorplanAvailabilityFilterByPropertyPreferences( $objFloorplanAvailabilityFilter, $arrobjPropertyPreferences, $objDatabase, $boolIsCalculateForRpcWebService = false, $boolSetParentPropertyId = false ) {
		$arrintPropertyIds		= ( false == isset( $this->m_arrintChildPropertyIds ) ) ? $this->getOrFetchChildPropertyIds( $objDatabase ) : $this->m_arrintChildPropertyIds;
		$arrintPropertyIds[]	= $this->getId();

		if( false != $boolSetParentPropertyId && false == is_null( $this->getPropertyId() ) ) {
			$arrintPropertyIds[] = $this->getPropertyId();
		}

		$objFloorplanAvailabilityFilter->setCid( $this->getCid() );
		$objFloorplanAvailabilityFilter->setPropertyIds( $arrintPropertyIds );

		if( false != $boolIsCalculateForRpcWebService ) {
			return CUnitSpaces::createService()->fetchCalculatedUnitSpacesByFloorplanAvailabilityFilterForRpc( $objFloorplanAvailabilityFilter, $arrobjPropertyPreferences, $objDatabase, true, false, false, $this->loadAvailableUnitSpaceStatusTypeIds( $objDatabase ) );
		}

		return CUnitSpaces::createService()->fetchCalculatedUnitSpacesByFloorplanAvailabilityFilter( $objFloorplanAvailabilityFilter, $arrobjPropertyPreferences, $objDatabase, true );
	}

	public function fetchCalculatedPropertyFloorplansByFloorplanAvailabilityFilterByParentPropertyPreferences( $objFloorplanAvailabilityFilter, $arrobjPropertyPreferences, $objDatabase ) {

		$objFloorplanAvailabilityFilter = ( false == valObj( $objFloorplanAvailabilityFilter, 'CFloorplanAvailabilityFilter' ) ) ? new CFloorplanAvailabilityFilter() : $objFloorplanAvailabilityFilter;
		$arrobjPropertyPreferences		= ( false == valArr( $arrobjPropertyPreferences ) ) ? $this->getOrFetchPropertyPreferences( $objDatabase ) : $arrobjPropertyPreferences;

		$arrintPropertyIds		= [];
		$arrintPropertyIds[]	= $this->getId();

		$objFloorplanAvailabilityFilter->setCid( $this->getCid() );
		$objFloorplanAvailabilityFilter->setPropertyIds( $arrintPropertyIds );

		return CPropertyFloorplans::createService()->fetchCalculatedPropertyFloorplansByFloorplanAvailabilityFilterByPropertyPreferences( $objFloorplanAvailabilityFilter, $arrobjPropertyPreferences, $objDatabase );
	}

	public function fetchCalculatedPropertyFloorplansByFloorplanAvailabilityFilterByPropertyFloorplanIdByPropertyPreferences( $objFloorplanAvailabilityFilter, $intPropertyFloorplanId, $arrobjPropertyPreferences, $objDatabase, $arrmixLeaseTerms = [] ) {

		$objFloorplanAvailabilityFilter = ( false == valObj( $objFloorplanAvailabilityFilter, 'CFloorplanAvailabilityFilter' ) ) ? new CFloorplanAvailabilityFilter() : $objFloorplanAvailabilityFilter;
		$arrobjPropertyPreferences		= ( false == valArr( $arrobjPropertyPreferences ) ) ? $this->getOrFetchPropertyPreferences( $objDatabase ) : $arrobjPropertyPreferences;

		if( true == valArr( $arrmixLeaseTerms ) ) {

			$arrintPropertyIds		= [];
			$arrintPropertyIds[]	= $arrmixLeaseTerms[0]['property_id'];
		} else {

			$arrintPropertyIds		= ( false == isset( $this->m_arrintChildPropertyIds ) ) ? $this->getOrFetchChildPropertyIds( $objDatabase ) : $this->m_arrintChildPropertyIds;
			$arrintPropertyIds[]	= $this->getId();
		}

		$objFloorplanAvailabilityFilter->setCid( $this->getCid() );
		$objFloorplanAvailabilityFilter->setPropertyIds( $arrintPropertyIds );

		if( true == is_numeric( $intPropertyFloorplanId ) && 0 < $intPropertyFloorplanId ) {
			$objFloorplanAvailabilityFilter->setPropertyFloorplanIds( [ $intPropertyFloorplanId ] );
		}

		return CPropertyFloorplans::createService()->fetchCalculatedPropertyFloorplansByFloorplanAvailabilityFilterByPropertyPreferences( $objFloorplanAvailabilityFilter, $arrobjPropertyPreferences, $objDatabase );
	}

	public function fetchCalculatedPropertyFloorplanByFloorplanAvailabilityFilterByPropertyFloorplanIdByPropertyPreferences( $objFloorplanAvailabilityFilter, $intPropertyFloorplanId, $arrobjPropertyPreferences, $objDatabase, $arrmixLeaseTerms = [] ) {

		$objFloorplanAvailabilityFilter = ( false == valObj( $objFloorplanAvailabilityFilter, 'CFloorplanAvailabilityFilter' ) ) ? new CFloorplanAvailabilityFilter() : $objFloorplanAvailabilityFilter;
		$arrobjPropertyPreferences		= ( false == valArr( $arrobjPropertyPreferences ) ) ? $this->getOrFetchPropertyPreferences( $objDatabase ) : $arrobjPropertyPreferences;

		if( true == valArr( $arrmixLeaseTerms ) ) {

			$arrintPropertyIds		= [];
			$arrintPropertyIds[]	= $arrmixLeaseTerms[0]['property_id'];
		} else {

			$arrintPropertyIds		= ( false == isset( $this->m_arrintChildPropertyIds ) ) ? $this->getOrFetchChildPropertyIds( $objDatabase ) : $this->m_arrintChildPropertyIds;
			$arrintPropertyIds[]	= $this->getId();
		}

		$objFloorplanAvailabilityFilter->setCid( $this->getCid() );
		$objFloorplanAvailabilityFilter->setPropertyIds( $arrintPropertyIds );

		if( true == is_numeric( $intPropertyFloorplanId ) && 0 < $intPropertyFloorplanId ) {
			$objFloorplanAvailabilityFilter->setPropertyFloorplanIds( [ $intPropertyFloorplanId ] );
		}

		return CPropertyFloorplans::createService()->fetchCalculatedPropertyFloorplanByFloorplanAvailabilityFilterByPropertyPreferences( $objFloorplanAvailabilityFilter, $arrobjPropertyPreferences, $objDatabase );
	}

	public function fetchLeaseStartWindowsByIds( $objDatabase, $arrintLeaseStartWindowIds ) {
		return CLeaseStartWindows::fetchLeaseStartWindowsByPropertyIdByIdsByCid( $this->m_intId, $arrintLeaseStartWindowIds, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyMoveOutListItemByListItemId( $intListItemId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyListItems::createService()->fetchPropertyListItemByListItemIdByListTypeIdByPropertyIdByCid( $intListItemId, CListType::MOVE_OUT, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedPropertyOccupations( $objDatabase ) {
		return CPropertyOccupations::fetchPublishedPropertyOccupationsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnassociatedOccupations( $objDatabase ) {
		return COccupations::fetchUnassociatedOccupationsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyOccupationByOccupationId( $intOccupationId, $objDatabase ) {
		return CPropertyOccupations::fetchPropertyOccupationByPropertyIdByOccupationIdByCid( $this->getId(), $intOccupationId, $this->getCid(), $objDatabase );
	}

	public function fetchOccupationById( $intOccupationId, $objDatabase ) {
		return COccupations::fetchOccupationByIdByCid( $intOccupationId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyOccupationByOccupationIds( $arrintOccupationIds, $objDatabase ) {
		return CPropertyOccupations::fetchPropertyOccupationByPropertyIdByOccupationIdsByCid( $this->getId(), $arrintOccupationIds, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyLeadSourceByName( $strName, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPropertyLeadSourceByPropertyIdByNameByCid( $this->getId(), $strName, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyPhoneNumberByLeadSourceIdByInternetListingServiceId( $intLeadSourceId, $intInternetListingServiceId, $objDatabase ) {
		return CPropertyPhoneNumbers::fetchPropertyPhoneNumberByLeadSourceIdByPropertyIdByInternetListingServiceIdByCid( $intLeadSourceId, $this->getId(), $intInternetListingServiceId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyLeadSourceIdByInternetListingServiceUrl( $strUrl, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPropertyLeadSourceIdByPropertyIdByInternetListingServiceUrl( $this->getId(), $strUrl, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyPhoneNumberByInternetListingServiceId( $intInternetListingServiceId, $objDatabase ) {
		return CPropertyPhoneNumbers::fetchPropertyPhoneNumberByPropertyIdByInternetListingServiceIdByCid( $this->getId(), $intInternetListingServiceId, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyPricingByCompanyPricingTypeId( $intCompanyPricingTypeId, $objDatabase ) {
		return CCompanyPricings::fetchCompanyPricingByCidByPropertyIdByCompanyPricingTypeId( $this->getCid(), $this->getId(), $intCompanyPricingTypeId, $objDatabase );
	}

	public function fetchAddOns( $objDatabase, $objAddOnsFilter = NULL, $boolIsDataArray = false ) {
		return CAddOns::createService()->fetchSimpleAddOnsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $objAddOnsFilter, $boolIsDataArray );
	}

	public function fetchRenewalAddOns( $objDatabase, $objAddOnsFilter = NULL ) {
		return CAddOns::createService()->fetchRenewalAddOnsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $objAddOnsFilter, false );
	}

	public function fetchAssociatedCompanyIdentificationTypes( $objDatabase ) {
		return CCompanyIdentificationTypes::fetchAssociatedCompanyIdentificationTypesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnassociatedCompanyIdentificationTypes( $objDatabase ) {
		return CCompanyIdentificationTypes::fetchUnassociatedCompanyIdentificationTypesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyIdentificationTypeById( $intCompanyIdentificationTypeId, $objDatabase ) {
		return CCompanyIdentificationTypes::fetchCompanyIdentificationTypeByIdByCid( $intCompanyIdentificationTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyIdentificationTypeByCompanyIdentificationTypeId( $intCompanyIdentificationTypeId, $objDatabase ) {
		return CPropertyIdentificationTypes::fetchPropertyIdentificationTypeByPropertyIdByCompanyIdentificationTypeIdByCid( $this->getId(), $intCompanyIdentificationTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchUtilityAccounts( $objUtilitiesDatabase ) {
		return \Psi\Eos\Utilities\CUtilityAccounts::createService()->fetchUtilityAccountsByPropertyId( $this->getId(), $objUtilitiesDatabase );
	}

	public function fetchPropertyIdentificationTypeByRemotePrimaryKey( $strRemotePrimarykey, $objDatabase ) {
		return CPropertyIdentificationTypes::fetchPropertyIdentificationTypeByPropertyIdByRemotePrimaryKeyByCid( $this->getId(), $strRemotePrimarykey, $this->getCid(), $objDatabase );
	}

	public function fetchSimplePropertyData( $strTableName, $objDatabase ) {
		return fetchData( sprintf( 'SELECT * FROM %s WHERE property_id = %d AND cid = %d', $strTableName, ( int ) $this->getId(), ( int ) $this->getCid() ), $objDatabase );
	}

	public function fetchAllPropertyBuildings( $objDatabase ) {
		return CPropertyBuildings::createService()->fetchPropertyBuildingsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchActiveLeasingAgents( $intLeasingAgentId, $objDatabase, $intCompanyDepartmentId = NULL ) {
		if( false == empty( $intLeasingAgentId ) ) {
			return CCompanyEmployees::createService()->fetchLeasingAgentsByPropertyIdsByIdByCid( [ $this->getId() ], $intLeasingAgentId, $this->getCid(), $objDatabase, $intCompanyDepartmentId );
		} else {
			return CCompanyEmployees::createService()->fetchActiveLeasingAgentsByPropertyIdsByCid( [ ( true == valObj( $this->getChildProperty(), 'CProperty' ) && $this->getId() != $this->getChildProperty()->getId() ) ? $this->getChildProperty()->getId() : $this->getId() ], $this->getCid(), $objDatabase, $intCompanyDepartmentId, $boolIsLeasingAdmin = true );
		}
	}

	public function fetchAllPropertyMaintenanceProblems( $boolIsSubCategory, $objDatabase ) {
		return CPropertyMaintenanceProblems::createService()->fetchAllPropertyMaintenanceProblemsByPropertyIdByCid( $this->getId(), $boolIsSubCategory, $this->getCid(), $objDatabase );
	}

	public function fetchAllUnitSpaceAmenityRateAssociations( $objDatabase ) {
		return CAmenityRateAssociations::createService()->fetchUnitSpaceAmenityRateAssociationsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyEmailAddressByEmailAddressTypeId( $intEmailAddressTypeId, $objDatabase ) {
		return CPropertyEmailAddresses::fetchPropertyEmailAddressByPropertyIdByEmailAddresssTypeIdByCid( $this->getId(), $intEmailAddressTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyAppointmentHoursRekeyedByDay( $objDatabase ) {
		$arrobjPropertyAppointmentHours = ( array ) CPropertyAppointmentHours::fetchCustomPropertyAppointmentHoursByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		$arrobjRekeyedPropertyAppointmentHours	= [];
		$arrstrAppointmentHourDays				= [ 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday' ];

		$arrobjRekeyedDayPropertyAppointmentHours	= rekeyObjects( 'day', $arrobjPropertyAppointmentHours );
		$arrintDays									= array_keys( $arrobjRekeyedDayPropertyAppointmentHours );

		foreach( $arrstrAppointmentHourDays as $intDay => $strAppointmentHourDay ) {
			if( true == in_array( $intDay, $arrintDays ) ) {
				$arrobjRekeyedPropertyAppointmentHours[$intDay] = $arrobjRekeyedDayPropertyAppointmentHours[$intDay];
			}
		}

		return $arrobjRekeyedPropertyAppointmentHours;
	}

	public function fetchPropertyMediaCategoryByPropertyMediaCategoryId( $intPropertyMediaCategoryId, $objDatabase ) {
		return CPropertyMediaCategories::createService()->fetchPropertyMediaCategoryByPropertyIdByIdByCid( $this->getId(), $intPropertyMediaCategoryId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyMediaCategories( $objDatabase ) {
		return CPropertyMediaCategories::createService()->fetchPropertyMediaCategoriesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyMediasByMediaTypeIds( $arrintMediaTypeIds, $objDatabase ) {
		if( false == valArr( $arrintMediaTypeIds ) ) {
			return NULL;
		}

		return CPropertyMedias::createService()->fetchPropertyMediasByPropertyIdByMediaTypeIdsByCid( $this->getId(), $arrintMediaTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyMediasByCompanyMediaFileIds( $arrintCompanyMediaFileIds, $objDatabase ) {
		if( false == valArr( $arrintCompanyMediaFileIds ) ) {
			return NULL;
		}

		return CPropertyMedias::createService()->fetchPropertyMediasByPropertyIdByCompanyMediaFileIdsByCid( $this->getId(), $arrintCompanyMediaFileIds, $this->getCid(), $objDatabase );
	}

	public function fetchCategoryIdByCompanyMediaFileIds( $intPropertyMediaCategoryId, $objDatabase ) {
		return CPropertyMedias::createService()->fetchPropertyCategoryIdByPropertyIdByCompanyMediaFileIdsByCid( $this->getId(), $intPropertyMediaCategoryId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyMediasByPropertyMediaCategoryId( $intMediaTypeId, $intPropertyMediaCategoryId, $objDatabase ) {
		return CPropertyMedias::createService()->fetchPropertyMediasByPropertyIdByPropertyMediaCategoryIdByCid( $this->getId(), $intMediaTypeId, $intPropertyMediaCategoryId, $this->getCid(), $objDatabase );
	}

	public function fetchPublishedPropertyFloorplansByPropertyFloorplanIds( $objDatabase, $arrintPropertyFloorplanIds ) {

		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		$this->m_intParentPropertyId = self::fetchParentPropertyId( $objDatabase );

		if( true == isset( $this->m_intParentPropertyId ) && true == valId( $this->m_intParentPropertyId ) ) {
			$arrintPropertyIds[$this->m_intParentPropertyId] = $this->m_intParentPropertyId;
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return CPropertyFloorplans::createService()->fetchPublishedPropertyFloorplansByPropertyIdByPropertyFloorplanIdsByCid( $arrintPropertyIds, $arrintPropertyFloorplanIds, $this->getCid(), $objDatabase );
	}

	public function fetchAllPropertyUnitswithUnitTypeName( $objDatabase, $boolIsSortByAvailibility = false, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false, $boolIncludeDeletedFloorPlans = false, $boolIsPublished = false, $boolIsPublishedFloorPlans = false ) {
		return CPropertyUnits::createService()->fetchPropertyUnitswithUnitTypeNameByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsSortByAvailibility, $boolIncludeDeletedUnits, $boolIncludeDeletedUnitSpaces, $boolIncludeDeletedFloorPlans, $boolIsPublished, $boolIsPublishedFloorPlans );
	}

	public function fetchAvailablePropertyUnitswithUnitTypeName( $objDatabase, $boolIsSortByAvailibility = false, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false, $boolIncludeDeletedFloorPlans = false, $boolIsPublished = false, $boolIsPublishedFloorPlans = false ) {
		return CPropertyUnits::createService()->fetchAvailablePropertyUnitswithUnitTypeNameByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsSortByAvailibility, $boolIncludeDeletedUnits, $boolIncludeDeletedUnitSpaces, $boolIncludeDeletedFloorPlans, $boolIsPublished, $boolIsPublishedFloorPlans );
	}

	public function fetchExentdedRenewalOfferApplicationsByLeaseIds( $arrintLeaseIds = [], $objDatabase ) {
		return CApplications::fetchExentdedRenewalOfferApplicationsByPropertyIdByLeaseIdsByCid( $this->getId(), $arrintLeaseIds, $this->getCid(), $objDatabase );
	}

	public function fetchResidentPortalEnabledCustomersByCustomerIds( $arrintCustomerIds = [], $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchResidentPortalEnabledCustomersByPropertyIdByCustomerIdsByCid( $this->getId(), $arrintCustomerIds, $this->getCid(), $objDatabase );
	}

	public function fetchDefaultNonLobbyTypeWebsite( $objDatabase, $boolCheckForResidentPortal = false ) {
		return CWebsites::createService()->fetchDefaultNonLobbyTypeWebsiteByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolCheckForResidentPortal );
	}

	public function fetchPropertyProducts( $objDatabase ) {
		return CPropertyProducts::createService()->fetchPropertyProductsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyProductByPsProductId( $intPsProductId, $objDatabase ) {
		return CPropertyProducts::createService()->fetchPropertyProductByPsProductIdByPropertyIdByCid( $intPsProductId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchDefaultLateFeeFormula( $objDatabase ) {
		return \Psi\Eos\Entrata\CLateFeeFormulas::createService()->fetchDefaultLateFeeFormulaByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchDefaultPropertyMaintenancePriority( $objDatabase ) {
		return CPropertyMaintenancePriorities::createService()->fetchDefaultPropertyMaintenancePriorityByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyMaintenanceCustomAvailabilityByDate( $strDate, $objDatabase, $intTemplatePropertyId = NULL ) {
		$intPropertyId = ( true == isset( $intTemplatePropertyId ) ) ? $intTemplatePropertyId : $this->getId();

		return CMaintenanceAvailabilityExceptions::createService()->fetchCustomAvailabilityHolidaysByDateByPropertyIdByCid( $strDate, $intPropertyId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyMaintenanceAvailability( $objDatabase, $intTemplatePropertyId = NULL ) {
		$intPropertyId = ( true == isset( $intTemplatePropertyId ) ) ? $intTemplatePropertyId : $this->getId();

		return \Psi\Eos\Entrata\CMaintenanceAvailabilities::createService()->fetchPropertyMaintenanceAvailabilityDayByPropertyIdByCid( $intPropertyId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyHolidaysWithCompanyHolidayDate( $objDatabase ) {
		return CPropertyHolidays::fetchPropertyHolidaysByPropertyIdByCidWithCompanyHolidayDate( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyMaintenanceAvailabilityException( $objDatabase ) {
		return CMaintenanceAvailabilityExceptions::createService()->fetchMaintenanceAvailabilityExceptionsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyMaintenanceLocationProblemsByMaintenanceProblemId( $intMaintenanceProblemId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyMaintenanceLocationProblems::createService()->fetchPropertyMaintenanceLocationProblemsByMaintenanceProblemIdByPropertyIdByCid( $intMaintenanceProblemId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchActiveDateBasedLeaseTerms( $objDatabase ) {
		return CLeaseTerms::fetchActiveDateBasedLeaseTermsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchSendNoticeLeasesCountByBulkUnitAssignmentFilter( $objBulkUnitAssignmentFilter, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchSendNoticeLeasesCountByBulkUnitAssignmentFilterByPropertyIdByCid( $objBulkUnitAssignmentFilter, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPaginatedSendNoticeLeasesByBulkUnitAssignmentFilter( $objBulkUnitAssignmentFilter, $objDatabase, $intLeaseId = NULL, $objPagination = NULL, $boolIsFromResidentDocument = false, $boolIncludeDeletedUnits = false, $boolIncludeDeletedFloorPlans = false, $strResidentType = NULL, $boolIsIncludeCurrentOptOuts = false ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchPaginatedSendNoticeLeasesByBulkUnitAssignmentFilterByPropertyIdByCid( $objBulkUnitAssignmentFilter, $this->getId(), $this->getCid(), $objDatabase, $intLeaseId, $objPagination, $boolIsFromResidentDocument, $boolIncludeDeletedUnits, $boolIncludeDeletedFloorPlans, $strResidentType, $boolIsIncludeCurrentOptOuts );
	}

	public function fetchAssociatedUnitSpaceAmenities( $objDatabase ) {
		return CAmenities::createService()->fetchAmenitiesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyFloorplansHavingPropertyUnits( $objDatabase, $boolIncludeDisabledFloorplans = false, $boolIsIncludeChildProperties = true ) {
		if( true == isset( $this->m_arrintChildPropertyIds ) && true == $boolIsIncludeChildProperties ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		if( false == valId( $this->m_intParentPropertyId ) ) {
			$this->m_intParentPropertyId = self::fetchParentPropertyId( $objDatabase );
		}

		if( true == isset( $this->m_intParentPropertyId ) && true == valId( $this->m_intParentPropertyId ) ) {
			$arrintPropertyIds[$this->m_intParentPropertyId] = $this->m_intParentPropertyId;
		}

		return CPropertyFloorplans::createService()->fetchPropertyFloorplansByPropertyIdByCidHavingPropertyUnitsFromPropertyIds( $this->getId(), $this->getCid(), $objDatabase, $boolIncludeDisabledFloorplans, $arrintPropertyIds );
	}

	public function fetchAssociatedMaintenanceTemplatesByParentMaintenanceTemplateId( $intMaintenanceTemplateId, $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceTemplates::createService()->fetchAssociatedMaintenanceTemplatesByParentMaintenanceTemplateIdByPropertyIdByCid( $intMaintenanceTemplateId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchDefaultPropertyMakeReadyMaintenanceTemplate( $objDatabase, $intLeaseId = NULL, $boolCheckDefault = NULL ) {
		return current( ( array ) CPropertyMaintenanceTemplates::createService()->fetchDefaultPropertyMakeReadyMaintenanceTemplateByPropertyIdsByCid( [ $this->getId() ], $this->getCid(), $objDatabase, $intLeaseId, $boolCheckDefault ) );
	}

	public function fetchPropertyMaintenanceTemplateByMaintenanceTemplateId( $intMaintenanceTemplateId, $objDatabase ) {
		return CPropertyMaintenanceTemplates::createService()->fetchPropertyMaintenanceTemplateByMaintenanceTemplateIdByPropertyIdByCid( $intMaintenanceTemplateId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedMaintenanceStatusesByMaintenanceStatusTypeIds( $intMaintenanceStatusTypeId, $objDatabase, $boolFetchArray = false ) {
		return CMaintenanceStatuses::createService()->fetchPublishedMaintenanceStatusesByMaintenanceStatusTypeIdsByPropertyIdByCid( $intMaintenanceStatusTypeId, $this->getId(), $this->getCid(), $objDatabase, $boolFetchArray );
	}

	public function fetchMakeReadyMaintenanceRequestsForMakeReadyBoard( $objDatabase ) {
		return CMaintenanceRequests::createService()->fetchMakeReadyMaintenanceRequestsForMakeReadyBoardByCidByPropertyId( $this->getCid(), $this->getId(), NULL, $objDatabase );
	}

	public function fetchSubMaintenanceRequestsByMaintenanceRequestIds( $arrintParentMaintenanceRequestIds, $objDatabase, $boolFetchData = false ) {
		return CMaintenanceRequests::createService()->fetchSubMaintenanceRequestsByMaintenanceRequestIdsByPropertyIdByCid( $arrintParentMaintenanceRequestIds, $this->getId(), $this->getCid(), $objDatabase, $boolFetchData );
	}

	public function fetchPaginatedLeadSourcesByFilters( $intPageNo, $intPageSize, $strLeadSourceName, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeadSources::createService()->fetchPaginatedLeadSourcesByPropertyIdByCidByFilters( $intPageNo, $intPageSize, $strLeadSourceName, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCountLeadSourcesByProperty( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeadSources::createService()->fetchCountLeadSourcesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchReviewNotes( $objDatabase ) {
		return \Psi\Eos\Entrata\CReviewNotes::createService()->fetchReviewNotesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchReviewNotesByReviewIds( $arrintReviewIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CReviewNotes::createService()->fetchReviewNotesByReviewIdsByPropertyIdsByCid( $arrintReviewIds, [ $this->getId() ], $this->getCid(), $objDatabase );
	}

	public function fetchScheduledBlackoutDates( $objDatabase, $intPropertyId = NULL ) {

		$intPropertyId = ( false == empty( $intPropertyId ) ) ? $intPropertyId : $this->getId();

		return CScheduledBlackoutDates::fetchScheduledBlackoutDatesDeatilsByPropertyIdByCid( $intPropertyId, $this->getCid(), $objDatabase );
	}

	public function fetchBlackoutDatesByScheduledBlackoutDateId( $intScheduledBlackoutDateId, $objDatabase ) {
		return \Psi\Eos\Entrata\CBlackoutDates::createService()->fetchBlackoutDatesByScheduledBlackoutDateIdByPropertyIdByCid( $intScheduledBlackoutDateId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAllUnitTypes( $objDatabase ) {
		return CUnitTypes::createService()->fetchAllUnitTypesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnitTypesCountByPropertyFloorPlanIds( $arrintPropertyFloorplanIds, $objDatabase ) {
		return CUnitTypes::createService()->fetchUnitTypesCountByPropertyFloorPlanIdByPropertyIdByCid( $arrintPropertyFloorplanIds, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchFirstPropertyFloorPlan( $objDatabase ) {
		return CPropertyFloorplans::createService()->fetchFirstPropertyFloorplanByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchMinMaxRent( $objDatabase ) {
		return CPropertyFloorplans::createService()->fetchMinMaxRentByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchMinMaxBedBath( $objDatabase ) {
		return CPropertyFloorplans::createService()->fetchMinMaxBedBathByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyEventResultsCountByEventTypeId( $intEventTypeId, $objDatabase ) {

		return \Psi\Eos\Entrata\CPropertyEventResults::createService()->fetchPropertyEventResultsCountByEventTypeIdByPropertyIdByCid( $intEventTypeId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyGlSettingsIsMigrationModeCount( $objClientDatabase ) {
		return CPropertyGlSettings::fetchPropertyGlSettingsIsMigrationModeCountByPropertyIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchSemKeywordsBySemAdGroupId( $intSemAdGroupId, $objAdminDatabase ) {
		return CSemKeywords::fetchSemKeywordsBySemAdGroupIdByPropertyIdByCid( $intSemAdGroupId, $this->getId(), $this->getCid(), $objAdminDatabase );
	}

	public function fetchSemKeywordAssociationsBySemAdGroupId( $intSemAdGroupId, $objAdminDatabase ) {
		return CSemKeywordAssociations::fetchSemKeywordAssociationsBySemAdGroupIdByPropertyIdByCid( $intSemAdGroupId, $this->getId(), $this->getCid(), $objAdminDatabase );
	}

	public function fetchSemAdGroupAssociations( $objAdminDatabase ) {
		return CSemAdGroupAssociations::fetchSemAdGroupAssociationsByByPropertyIdByCid( $this->getId(), $this->getCid(), $objAdminDatabase );
	}

	public function fetchSemAds( $objAdminDatabase ) {
		return CSemAds::fetchSemAdsByPropertyIdByCid( $this->getId(), $this->getCid(), $objAdminDatabase );
	}

	public function fetchLeaseCustomersKeyedByRemotePrimaryKey( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersByPropertyIdsKeyedByRemotePrimaryKeyByCid( [ $this->getId() ], $this->getCid(), $objDatabase );
	}

	public function fetchApplicantApplicationByPhoneNumberAfterDate( $strPhoneNumber, $strDate, $objDatabase ) {
		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationByPhoneNumberByPropertyIdAfterDateByCid( $strPhoneNumber, $this->getId(), $strDate, $this->getCid(), $objDatabase );
	}

	public function fetchAssociatedPropertyLeadSources( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchCustomPropertyLeadSourcesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchParentCompanyMediaFolder( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyMediaFolders::createService()->fetchParentCompanyMediaFolderByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFolderByFolderName( $strFolderName, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyMediaFolders::createService()->fetchParentCompanyMediaFolderByPropertyIdByFolderNameByCid( $this->getId(), $strFolderName, $this->getCid(), $objDatabase );
	}

	public function fetchSpecialRateAssociationsByArOriginReferenceIds( $arrintArOriginReferenceIds, $objDatabase ) {

		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return \Psi\Eos\Entrata\Custom\CSpecialRateAssociations::createService()->fetchSpecialRateAssociationsByPropertyIdsByArOriginReferenceIdsByCid( $arrintPropertyIds, $arrintArOriginReferenceIds, $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteSpecialRateAssociationsByArCascadeIds( $arrintArCascadeIds, $objDatabase, $strMoveInDate = NULL, $intSelectedLeaseStartWindowId = NULL ) {

		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		if( true == valId( $this->getPropertyId() ) ) {
			$arrintPropertyIds[$this->getPropertyId()] = $this->getPropertyId();
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return \Psi\Eos\Entrata\Custom\CSpecialRateAssociations::createService()->fetchWebsiteSpecialRateAssociationsByPropertyIdsByArCascadeIdsByCid( $arrintPropertyIds, $arrintArCascadeIds, $this->getCid(), $objDatabase, $strMoveInDate, $intSelectedLeaseStartWindowId );
	}

	public function fetchWebsiteSpecialRateAssociationsWithLeaseTermIdByArCascadeId( $arrintArCascadeIds, $objDatabase, $strMoveInDate = NULL ) {

		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		if( true == valId( $this->getPropertyId() ) ) {
			$arrintPropertyIds[$this->getPropertyId()] = $this->getPropertyId();
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();

		return \Psi\Eos\Entrata\Custom\CSpecialRateAssociations::createService()->fetchWebsiteSpecialRateAssociationsWithLeaseTermIdByPropertyIdsByArCascadeIdsByCid( $arrintPropertyIds, $arrintArCascadeIds, $this->getCid(), $objDatabase, $strMoveInDate );
	}

	public function fetchPropertyCallSetting( $objVoipDatabase ) {
		return CPropertyCallSettings::fetchPropertyCallSettingsByCidByPropertyId( $this->getCid(), $this->getId(), $objVoipDatabase );
	}

	public function fetchCallsByOriginPhoneNumbers( $arrintPhoneNumbers, $objDatabase ) {
		return \Psi\Eos\Voip\CCalls::createService()->fetchCallsByCidByPropertyIdByOriginPhoneNumbers( $this->getCid(), $this->getId(), $arrintPhoneNumbers, $objDatabase );
	}

	public function fetchPropertyMaintenanceStatusByMaintenanceStatusTypeId( $intMaintenanceStatusTypeId, $objDatabase, $boolIsSetLimit = true ) {
		return CMaintenanceStatuses::createService()->fetchMaintenanceStatusByMaintenanceStatusTypeIdByPropertyIdByCid( $intMaintenanceStatusTypeId, $this->getId(), $this->getCid(), $objDatabase, $boolIsSetLimit );
	}

	public function fetchAllOpenMaintenanceRequestsByPropertyUnitId( $boolHideExternalWorkOrders, $objLease, $objDatabase ) {
		return CMaintenanceRequests::createService()->fetchAllOpenMaintenanceRequestsByPropertyUnitIdByPropertyIdByCid( $boolHideExternalWorkOrders, $objLease, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPrimaryWebsiteWithStandardTemplate( $objDatabase ) {
		return CWebsites::createService()->fetchPrimaryWebsiteWithStandardTemplateByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnassociatedCompanyHolidayIds( $objDatabase ) {
		return CCompanyHolidays::fetchUnassociatedCompanyHolidayIdsByPropertyIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchAssociatedCompanyHolidayIds( $objDatabase ) {
		return CCompanyHolidays::fetchAssociatedCompanyHolidayIdsByPropertyIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchCorporateWebsite( $objDatabase ) {
		return CWebsites::createService()->fetchCorporateWebsiteByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedAmenityRateAssociationsByArCascadeIdByArCascadeReferenceIds( $intArCascadeId, $arrintArCascadeReferenceIds, $objDatabase, $arrintAmenityTypeIds = [ CAmenityType::APARTMENT ] ) {
		return CAmenityRateAssociations::createService()->fetchPublishedAmenityRateAssociationsByArCascadeIdByArCascadeReferenceIdsByPropertyIdByCid( $intArCascadeId, $arrintArCascadeReferenceIds, $this->getId(), $this->getCid(), $objDatabase, $arrintAmenityTypeIds );
	}

	public function fetchAmenityRateAssociationsByArCascadeIdByArCascadeReferenceIdsByAmenityTypeId( $intArCascadeId, $arrintArCascadeReferenceIds, $arrintAmenityTypeIds = [ CAmenityType::APARTMENT ], $objDatabase, $arrmixSearchFilter = NULL ) {
		return CAmenityRateAssociations::createService()->fetchAmenityRateAssociationsByArCascadeIdByArCascadeReferenceIdsByAmenityTypeIdByPropertyIdByCid( $intArCascadeId, $arrintArCascadeReferenceIds, $arrintAmenityTypeIds, $this->getId(), $this->getCid(), $objDatabase, $arrmixSearchFilter );
	}

	public function fetchDefaultCustomerRelationships( $intOccupancyTypeId, $objDatabase ) {
		return CCustomerRelationships::fetchDefaultCustomerRelationshipsByOccupancyTypeIdByPropertyIdByCidRekeyedByCustomerTypeId( $intOccupancyTypeId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAllVehicles( $objDatabase ) {
		return CPropertyVehicles::fetchAllVehiclesByPropertyIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchCompanyMediaFilesByMediaTypeIdsByArOriginIdByArCascadeIdByArCascadeReferenceId( $arrintMediaTypeIds, $intOriginId, $intCascadeId, $intCascadeReferenceId, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByMediaTypeIdsByArOriginIdByArCascadeIdByArCascadeReferenceIdByPropertyIdByCid( $arrintMediaTypeIds, $intOriginId, $intCascadeId, $intCascadeReferenceId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchActiveMarketingMediaAssociationsByMediaSubTypeIdsByArOriginIdByArCascadeIdByArCascadeReferenceId( $arrintMarketingMediaSubTypeIds, $intOriginId, $intCascadeId, $intCascadeReferenceId, $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsByMediaSubTypeIdsByArOriginIdByArCascadeIdByArCascadeReferenceIdByPropertyIdByCid( $arrintMarketingMediaSubTypeIds, $intOriginId, $intCascadeId, $intCascadeReferenceId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchIsIntegratedProperty( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchIsPropertyIntegratedByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

    public function fetchMaintenanceSurveyResponseEmails( $objDatabase ) {
		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'MAINTENANCE_SURVEY_RESPONSE', $this->getId(), $this->m_intCid, $objDatabase );
        if( false == isset( $objPropertyPreference ) ) {
            return NULL;
        }
        return CEmail::createEmailArrayFromString( $objPropertyPreference->getValue() );
    }

	public function fetchDefaultOccupancyTypeMakeReadyMaintenanceTemplateByLeaseId( $intLeaseId, $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceTemplates::createService()->fetchDefaultOccupancyTypeMakeReadyMaintenanceTemplateByPropertyIdByLeaseIdByCid( $this->getId(), $intLeaseId, $this->getCid(), $objDatabase );
	}

	public function fetchOrganizedMarketingMediaAssociations( $strCategoryName = 'GENERAL', $strOrganizeByKey = 'MediaCategoryName', $objDatabase, $boolIsIncludeBrochureImages = false, $intPropertyMediaCategoryId = NULL ) {
		$arrobjMarketingMediaAssociations = \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsAndCategoriesByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $strCategoryName, $this->getId(), CMarketingMediaType::PROPERTY_MEDIA, CMarketingMediaSubType::PHOTO, $this->getCid(), $objDatabase, $boolIsIncludeBrochureImages = false, $intPropertyMediaCategoryId );

		if( false == is_null( $strOrganizeByKey ) ) {
			return CObjectModifiers::createService()->reOrganizeObjectsByCommonKey( $strOrganizeByKey, $arrobjMarketingMediaAssociations );
		} else {
			return $arrobjMarketingMediaAssociations;
		}
	}

	public function fetchMarketingMediaAsssociationCountByCategoriesByMediaSubTypeId( $intMarketingMediaSubType, $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchMarketingMediaAsssociationCountByCategoriesByPropertyIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getId(), CMarketingMediaType::PROPERTY_MEDIA, $intMarketingMediaSubType, $this->getCid(), $objDatabase );
	}

	public function fetchActiveMarketingMediaAsssociationsByCategoryNameByMediaTypeIds( $strCategoryName, $arrintMediaSubTypeIds, $objDatabase, $boolOrderByPropertyMediaOrder = false ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsByReferenceIdByCategoryNameByMediaTypeIdByMediaSubTypeIdsByCid( $this->getId(), $strCategoryName, CMarketingMediaType::PROPERTY_MEDIA, $arrintMediaSubTypeIds, $this->getCid(), $objDatabase, $boolOrderByPropertyMediaOrder );
	}

	/**
	 * Sql functions
	 *
	 */

	public function sqlYearBuilt() {
		return isset( $this->m_strYearBuilt ) ? "'" . $this->m_strYearBuilt . "'" : 'NULL';
	}

	/**
	 * Validation Functions
	 *
	 */

	public function validate( $strAction, $objDatabase = NULL, $boolIsRequired = false, $boolIsDuplicatePropertyName = false ) {
		$boolIsValid = true;

		$objPropertyValidator = new CPropertyValidator();
		$objPropertyValidator->setProperty( $this );

		$boolIsValid &= $objPropertyValidator->validate( $strAction, $objDatabase, $boolIsRequired, $boolIsDuplicatePropertyName );

		return $boolIsValid;
	}

	public function valCheckDuplicationByPropertyName( $objDatabase ) {
		$strSql = 'SELECT id FROM properties WHERE lower( property_name ) = \'' . trim( \Psi\CStringService::singleton()->strtolower( addslashes( $this->getPropertyName() ) ) ) . '\' AND cid = ' . ( int ) $this->getCid() . ' AND is_disabled = 0';

		$arrintPropertyIds	= fetchData( $strSql, $objDatabase );
		$boolIsValid		= valArr( $arrintPropertyIds );

		return ( true == $boolIsValid ) ? false : true;
	}

	public function validateRequiredApplicationPreferences( $arrobjInsertOrUpdatePropertyApplicationPreferences, $objDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$objPropertyTransmissionVendor = $this->fetchPropertyTransmissionVendorByTransmissionTypeId( CTransmissionType::SCREENING, $objDatabase );
		}

		$arrobjInsertOrUpdatePropertyApplicationPreferences = rekeyObjects( 'key', $arrobjInsertOrUpdatePropertyApplicationPreferences );

		if( true == isset( $objPropertyTransmissionVendor ) && true == valObj( $objPropertyTransmissionVendor, 'CPropertyTransmissionVendor' ) ) {
			$arrstrRequiredFields = $objPropertyTransmissionVendor->fetchRequiredTransmissionFields( $objDatabase );

			if( true == valArr( $arrstrRequiredFields ) ) {

				if( true == array_key_exists( 'tax_number', $arrstrRequiredFields ) && true == array_key_exists( 'HIDE_BASIC_INFO_TAX_NUMBER', $arrobjInsertOrUpdatePropertyApplicationPreferences ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number', 'SSN or Tax Number is required for screening.', '' ) );
					$boolIsValid &= false;
				}

				if( true == array_key_exists( 'birth_date', $arrstrRequiredFields ) && false == array_key_exists( 'REQUIRE_BASIC_INFO_BIRTH_DATE', $arrobjInsertOrUpdatePropertyApplicationPreferences ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'birht_date', __( 'Birth Date is required for screening.' ), '' ) );
					$boolIsValid &= false;
				}

				if( true == array_key_exists( 'current_city', $arrstrRequiredFields ) || true == array_key_exists( 'current_state_code', $arrstrRequiredFields ) || true == array_key_exists( 'current_postal_code', $arrstrRequiredFields ) || true == array_key_exists( 'current_move_in_date', $arrstrRequiredFields ) || true == array_key_exists( 'current_move_out_date', $arrstrRequiredFields ) ) {
					if( false == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS', $arrobjInsertOrUpdatePropertyApplicationPreferences ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address', __( 'Current Address detail is required for screening.' ), '' ) );
						$boolIsValid &= false;
					}
				}
			}
		}

		if( true == array_key_exists( 'REQUIRED_PEOPLE_MINIMUM_GUARANTORS', $arrobjInsertOrUpdatePropertyApplicationPreferences ) && true == array_key_exists( 'PEOPLE_MAXIMUM_GUARANTORS', $arrobjInsertOrUpdatePropertyApplicationPreferences )
		    && 0 < $arrobjInsertOrUpdatePropertyApplicationPreferences['REQUIRED_PEOPLE_MINIMUM_GUARANTORS']->getValue()
		    && 0 < $arrobjInsertOrUpdatePropertyApplicationPreferences['PEOPLE_MAXIMUM_GUARANTORS']->getValue()
		    && $arrobjInsertOrUpdatePropertyApplicationPreferences['PEOPLE_MAXIMUM_GUARANTORS']->getValue() < $arrobjInsertOrUpdatePropertyApplicationPreferences['REQUIRED_PEOPLE_MINIMUM_GUARANTORS']->getValue() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Required Maximum Guarantor should be greator than or equal to Minimum Gurantor.' ), '' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function validateGreetingCode( $intGreetingCode, $objDatabase ) {
		$intCreatedOnMonth	= \Psi\CStringService::singleton()->substr( $intGreetingCode, 0, 2 );
		$intCreatedOnDay	= \Psi\CStringService::singleton()->substr( $intGreetingCode, 2, 2 );
		$intGreetingTypeId	= \Psi\CStringService::singleton()->substr( $intGreetingCode, -2 );

		if( date( 'm', strtotime( $this->getCreatedOn() ) ) != $intCreatedOnMonth ) {
			return false;
		}
		if( date( 'd', strtotime( $this->getCreatedOn() ) ) != $intCreatedOnDay ) {
			return false;
		}

		$objGreetingType = CGreetingTypes::fetchGreetingTypeById( $intGreetingTypeId, $objDatabase );

		return !( true == is_null( $objGreetingType ) || false == valObj( $objGreetingType, 'CGreetingType' ) );

	}

	/**
	 * Insert, Update & Delete Functions
	 *
	 */

	public function insertBasicDetails( $intCurrentUserId, $objClientDatabase = NULL, $objAdminDatabase = NULL, $boolPerformAdminPropertyInsert = true ) {
		$boolIsValid = true;

		if( false == valObj( $objAdminDatabase, 'CDatabase' ) || CDatabaseType::ADMIN != $objAdminDatabase->getDatabaseTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to load database object.' ) );
			return false;
		}

		$this->m_objPrimaryAddress		= CPropertyAddresses::createService()->fetchPropertyAddressByPropertyIdByAddressTypeIdByCid( $this->getId(), CAddressType::PRIMARY, $this->getCid(), $objAdminDatabase ) ?? $this->createPrimaryAddress();
		$boolIsValid &= ( false == $this->m_objPrimaryAddress->insert( $intCurrentUserId, $objClientDatabase, $objAdminDatabase, $boolPerformAdminPropertyInsert ) ) ? false : true;

		if( !valObj( $objClientDatabase, 'CDatabase' ) ) {
			return $boolIsValid;
		}

		$this->m_objOfficePhoneNumber	= $this->createOfficePhoneNumber();
		$this->m_objOfficeEmailAddress	= $this->createOfficeEmailAddress();
		$this->m_objPropertyDetail		= $this->createPropertyDetail();

		if( CPropertyType::TEMPLATE != $this->getPropertyTypeId() ) {
			$this->m_objOfficeFaxNumber			= $this->createOfficeFaxNumber();
			$this->m_objPrimaryOnSiteContact	= $this->createPrimaryOnSiteContact();
			$this->m_objMaintenancePhoneNumber	= $this->createMaintenancePhoneNumber();
		}
		// set managerial company id
		$this->m_objPropertyDetail->setManagerialPropertyId( $this->getId() );

		$boolIsValid &= ( false == $this->m_objOfficePhoneNumber->insert( $intCurrentUserId, $objClientDatabase ) ) ? false : true;
		$boolIsValid &= ( false == $this->m_objOfficeEmailAddress->insert( $intCurrentUserId, $objClientDatabase ) ) ? false : true;
		$boolIsValid &= ( false == $this->m_objPropertyDetail->insert( $intCurrentUserId, $objClientDatabase ) ) ? false : true;

		if( CPropertyType::TEMPLATE != $this->getPropertyTypeId() ) {
			$boolIsValid &= ( false == $this->m_objOfficeFaxNumber->insert( $intCurrentUserId, $objClientDatabase ) ) ? false : true;
			$boolIsValid &= ( false == $this->m_objPrimaryOnSiteContact->insert( $intCurrentUserId, $objClientDatabase ) ) ? false : true;
			$boolIsValid &= ( false == $this->m_objMaintenancePhoneNumber->insert( $intCurrentUserId, $objClientDatabase ) ) ? false : true;
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objClientDatabase = NULL, $objAdminDatabase = NULL, $boolIsExcludeOwnerInsertion = false, $boolPerformAdminPropertyInsert = true ) {

		if( false == is_numeric( $this->getId() ) ) {
			$this->fetchNextId( $objAdminDatabase );
		}

		// Insert Owner
		if( false == $boolIsExcludeOwnerInsertion && valObj( $objClientDatabase, 'CDatabase' ) ) {
			$strSql = 'SELECT
							*
						FROM
							owner_init( ' . $this->sqlPropertyName() . ', ' . $this->sqlCid() . ', ' . ( int ) $intCurrentUserId . ',' . $this->getIsDisabled() . ' ) AS owner_id;';

			$arrmixResult	= fetchData( $strSql, $objClientDatabase );
			$intOwnerId		= ( true == isset( $arrmixResult[0] ) ) ? $arrmixResult[0]['owner_id'] : NULL;

			if( false == valId( $intOwnerId ) ) {
				// Unset new id on error
				$this->setId( NULL );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to insert owner record.' ) ) );

				return false;
			}

			$this->setOwnerId( $intOwnerId );
		}

		if( true == $boolPerformAdminPropertyInsert ) {
			$strSql = parent::insert( $intCurrentUserId, $objAdminDatabase, $boolReturnSqlOnly = true );

			if( false === $strSql ) {
				return true;
			}

			if( false == $this->executeSql( $strSql, $this, $objAdminDatabase ) ) {
				return false;
			}
		}

		if( !valObj( $objClientDatabase, 'CDatabase' ) ) {
			return true;
		}

		if( true == is_null( $objClientDatabase ) || false == valObj( $objClientDatabase, 'CDatabase' ) || CDatabaseType::CLIENT != $objClientDatabase->getDatabaseTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Failed to load database object.' ) ) );

			return false;
		}

		$strSql = parent::insert( $intCurrentUserId, $objClientDatabase, $boolReturnSqlOnly = true );

		if( false === $strSql ) {
			return true;
		}

		if( false == $this->executeSql( $strSql, $this, $objClientDatabase ) ) {
			return false;
		}

		if( false == $this->insertGlAccountProperties( $intCurrentUserId, $objClientDatabase ) ) {
			return false;
		}

		$boolIsValid = true;

		if( CPropertyType::SETTINGS_TEMPLATE != $this->getPropertyTypeId() ) {
			// Initialize all default data for the property
			$strSql = 'SELECT
						*
						FROM
							property_init( ' . $this->sqlId() . ', ' . $this->sqlCid() . ', ' . ( int ) $intCurrentUserId . ' ) AS result;';

			if( true == $boolIsValid && false == $this->executeSql( $strSql, $this, $objClientDatabase ) ) {
				// Unset new id on error
				$this->setId( NULL );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to initialize property record. The following error was reported.' ) . $objClientDatabase->errorMsg() ) );
				$boolIsValid &= false;
			}

			if( false == $this->getIsBulkAddProperty() ) {
				// Rebuild property_group_associations
				$strSql = 'SELECT
						*
						FROM
							rebuild_owner_property_groups( ' . $this->sqlCid() . ' ) AS group_result;';
				if( true == $boolIsValid && false == $this->executeSql( $strSql, $this, $objClientDatabase ) ) {
					// Unset new id on error
					$this->setId( NULL );
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to rebuild Owner Property Groups. The following error was reported.' ) . $objClientDatabase->errorMsg() ) );
					$boolIsValid &= false;
				}
			}
		}

		return $boolIsValid;
	}

	public function insertOrUpdate( $intCurrentUserId, $objClientDatabase, $objAdminDatabase = NULL ) {
		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objClientDatabase, $objAdminDatabase );
		} else {
			return $this->update( $intCurrentUserId, $objClientDatabase, $objAdminDatabase );
		}
	}

	public function insertOrUpdateRequiredPropertyApplicationPreferences( $intCompanyTransmissionVendorId, $intCompanyUserId, $objDatabase ) {

		$boolIsValid		= true;
		$intCompanyUserId	= NULL;

		$objPropertyTransmissionVendor = $this->fetchPropertyTransmissionVendorByCompanyTransmissionVendorIdByTransmissionTypeId( $intCompanyTransmissionVendorId, CTransmissionType::SCREENING, $objDatabase );

		if( true == valObj( $objPropertyTransmissionVendor, 'CPropertyTransmissionVendor' ) ) {
			$objCompanyTransmissionVendor	= $objPropertyTransmissionVendor->getOrFetchCompanyTransmissionVendor( $objDatabase );
			$arrstrRequiredFields			= $objPropertyTransmissionVendor->fetchRequiredTransmissionFields( $objDatabase );

			if( true == valArr( $arrstrRequiredFields ) ) {

				$arrobjPropertyApplications = $this->fetchPropertyApplications( $objDatabase );

				$arrstrDeleteKeys = [];
				$arrstrInsertKeys = [];

				if( true == array_key_exists( 'tax_number', $arrstrRequiredFields ) ) {
					array_push( $arrstrDeleteKeys, 'HIDE_BASIC_INFO_TAX_NUMBER' );
				}

				if( true == array_key_exists( 'birth_date', $arrstrRequiredFields ) ) {
					array_push( $arrstrDeleteKeys, 'HIDE_BASIC_INFO_BIRTH_DATE' );
					array_push( $arrstrDeleteKeys, 'REQUIRE_BASIC_INFO_BIRTH_DATE' );
					array_push( $arrstrInsertKeys, 'REQUIRE_BASIC_INFO_BIRTH_DATE' );
				}

				if( true == array_key_exists( 'current_city', $arrstrRequiredFields ) || true == array_key_exists( 'current_state_code', $arrstrRequiredFields ) || true == array_key_exists( 'current_postal_code', $arrstrRequiredFields ) ) {
					array_push( $arrstrDeleteKeys, 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS' );
					array_push( $arrstrDeleteKeys, 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS' );
					array_push( $arrstrInsertKeys, 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS' );
				}

				if( true == array_key_exists( 'lease_term', $arrstrRequiredFields ) ) {
					array_push( $arrstrDeleteKeys, 'HIDE_LEASE_PREFERENCES' );
					array_push( $arrstrDeleteKeys, 'HIDE_LEASE_PREFERENCES_TERM_MONTH' );
					array_push( $arrstrDeleteKeys, 'REQUIRE_LEASE_PREFERENCES_TERM_MONTH' );
					array_push( $arrstrInsertKeys, 'REQUIRE_LEASE_PREFERENCES_TERM_MONTH' );
				}

				if( true == array_key_exists( 'gross_income', $arrstrRequiredFields ) ) {
					array_push( $arrstrDeleteKeys, 'HIDE_FINANCIAL' );
					array_push( $arrstrDeleteKeys, 'HIDE_INCOME' );
					array_push( $arrstrDeleteKeys, 'HIDE_INCOME_TYPE_CURRENT_EMPLOYER' );
					array_push( $arrstrDeleteKeys, 'REQUIRE_INCOME_TYPE_CURRENT_EMPLOYER' );
					array_push( $arrstrInsertKeys, 'REQUIRE_INCOME_TYPE_CURRENT_EMPLOYER' );
				}

				if( true == array_key_exists( 'current_move_in_date', $arrstrRequiredFields ) ) {
					array_push( $arrstrDeleteKeys, 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS_MOVE_IN_DATE' );
					array_push( $arrstrDeleteKeys, 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_MOVE_IN_DATE' );
					array_push( $arrstrInsertKeys, 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_MOVE_IN_DATE' );
				}

				if( true == array_key_exists( 'current_move_out_date', $arrstrRequiredFields ) ) {
					array_push( $arrstrDeleteKeys, 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS_MOVE_OUT_DATE' );
					array_push( $arrstrDeleteKeys, 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_MOVE_OUT_DATE' );
					array_push( $arrstrInsertKeys, 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_MOVE_OUT_DATE' );
				}

				if( true == array_key_exists( 'current_employment_start_date', $arrstrRequiredFields ) ) {
					array_push( $arrstrDeleteKeys, 'HIDE_FINANCIAL' );
					array_push( $arrstrDeleteKeys, 'HIDE_INCOME' );
					array_push( $arrstrDeleteKeys, 'HIDE_INCOME_TYPE_CURRENT_EMPLOYER' );
					array_push( $arrstrDeleteKeys, 'OVERRIDE_INCOME_TYPE_CURRENT_EMPLOYER' );
					array_push( $arrstrDeleteKeys, 'REQUIRE_INCOME_TYPE_CURRENT_EMPLOYER' );
					array_push( $arrstrDeleteKeys, 'HIDE_INCOME_TYPE_CURRENT_EMPLOYER_DATE_STARTED' );
					array_push( $arrstrDeleteKeys, 'REQUIRE_INCOME_TYPE_CURRENT_EMPLOYER_DATE_STARTED' );
					array_push( $arrstrInsertKeys, 'OVERRIDE_INCOME_TYPE_CURRENT_EMPLOYER' );
					array_push( $arrstrInsertKeys, 'REQUIRE_INCOME_TYPE_CURRENT_EMPLOYER' );
					array_push( $arrstrInsertKeys, 'REQUIRE_INCOME_TYPE_CURRENT_EMPLOYER_DATE_STARTED' );
				}

				if( true == array_key_exists( 'enable_income_options', $arrstrRequiredFields ) ) {
					array_push( $arrstrDeleteKeys, 'HIDE_FINANCIAL' );
					array_push( $arrstrDeleteKeys, 'HIDE_INCOME' );
					array_push( $arrstrDeleteKeys, 'HIDE_INCOME_TYPE_CURRENT_EMPLOYER' );
					array_push( $arrstrDeleteKeys, 'OVERRIDE_INCOME_TYPE_CURRENT_EMPLOYER' );
					array_push( $arrstrDeleteKeys, 'REQUIRE_INCOME_TYPE_CURRENT_EMPLOYER' );
					array_push( $arrstrDeleteKeys, 'HIDE_INCOME_TYPE_CURRENT_EMPLOYER_DATE_STARTED' );
					array_push( $arrstrDeleteKeys, 'REQUIRE_INCOME_TYPE_CURRENT_EMPLOYER_DATE_STARTED' );
					array_push( $arrstrDeleteKeys, 'REQUIRE_INCOME_TYPE_CURRENT_EMPLOYER_APPLICANT' );
					array_push( $arrstrDeleteKeys, 'REQUIRE_INCOME_TYPE_CURRENT_EMPLOYER_ADDRESS' );
					array_push( $arrstrDeleteKeys, 'REQUIRE_INCOME_TYPE_CURRENT_EMPLOYER_NAME' );
					array_push( $arrstrDeleteKeys, 'REQUIRE_INCOME_TYPE_CURRENT_EMPLOYER_CONTACT_PHONE_NUMBER' );
					array_push( $arrstrInsertKeys, 'OVERRIDE_INCOME_TYPE_CURRENT_EMPLOYER' );
					array_push( $arrstrInsertKeys, 'REQUIRE_INCOME_TYPE_CURRENT_EMPLOYER' );
					array_push( $arrstrInsertKeys, 'REQUIRE_INCOME_TYPE_CURRENT_EMPLOYER_DATE_STARTED' );
					array_push( $arrstrInsertKeys, 'REQUIRE_INCOME_TYPE_CURRENT_EMPLOYER_APPLICANT' );
					array_push( $arrstrInsertKeys, 'REQUIRE_INCOME_TYPE_CURRENT_EMPLOYER_ADDRESS' );
					array_push( $arrstrInsertKeys, 'REQUIRE_INCOME_TYPE_CURRENT_EMPLOYER_NAME' );
					array_push( $arrstrInsertKeys, 'REQUIRE_INCOME_TYPE_CURRENT_EMPLOYER_CONTACT_PHONE_NUMBER' );
				}

				if( true == valObj( $objCompanyTransmissionVendor, 'CCompanyTransmissionVendor' )
				    && CTransmissionVendor::RENT_GROW == $objCompanyTransmissionVendor->getTransmissionVendorId() ) {

					array_push( $arrstrDeleteKeys, 'REQUIRE_PEOPLE_GUARANATOR' );
					array_push( $arrstrDeleteKeys, 'HIDE_PEOPLE_GUARANATOR' );
					array_push( $arrstrInsertKeys, 'HIDE_PEOPLE_GUARANATOR' );
				}

				$arrstrInsertKeys = array_unique( $arrstrInsertKeys );

				if( true == valArr( $arrobjPropertyApplications ) ) {
					foreach( $arrobjPropertyApplications as $objPropertyApplication ) {

						$arrobjDeletePropertyApplicationPreferences = $objPropertyApplication->fetchPropertyApplicationPreferencesByKeys( $arrstrDeleteKeys, $objDatabase );
						$arrobjInsertPropertyApplicationPreferences = [];

						foreach( $arrstrInsertKeys as $strInsertKey ) {
							$objPropertyApplicationPreference = $objPropertyApplication->createPropertyApplicationPreference();
							$objPropertyApplicationPreference->setKey( $strInsertKey );
							$objPropertyApplicationPreference->setValue( 1 );
							array_push( $arrobjInsertPropertyApplicationPreferences, $objPropertyApplicationPreference );
						}

						if( true == valArr( $arrobjInsertPropertyApplicationPreferences ) ) {
							foreach( $arrobjInsertPropertyApplicationPreferences as $objInsertPropertyApplicationPreference ) {
								$boolIsValid &= $objInsertPropertyApplicationPreference->validate( VALIDATE_INSERT );
							}
						}

						if( true == valArr( $arrobjDeletePropertyApplicationPreferences ) ) {

							foreach( $arrobjDeletePropertyApplicationPreferences as $objUpdatePropertyApplicationPreference ) {
								$boolIsValid &= $objUpdatePropertyApplicationPreference->validate( VALIDATE_DELETE );
							}
						}

						if( false == $boolIsValid ) {
							break;
						}

						if( true == valArr( $arrobjDeletePropertyApplicationPreferences ) ) {
							foreach( $arrobjDeletePropertyApplicationPreferences as $objDeletePropertyApplicationPreference ) {
								if( false == $objDeletePropertyApplicationPreference->delete( SYSTEM_USER_ID, $objDatabase ) ) {
									$objDatabase->rollback();
									$boolIsValid = false;
									break;
								}
							}
						}

						if( true == valArr( $arrobjInsertPropertyApplicationPreferences ) ) {
							foreach( $arrobjInsertPropertyApplicationPreferences as $objInsertPropertyApplicationPreference ) {
								if( false == $objInsertPropertyApplicationPreference->insert( SYSTEM_USER_ID, $objDatabase ) ) {
									$objDatabase->rollback();
									$boolIsValid = false;
									break;
								}
							}
						}
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function insertCompanyMediaFolders( $objClient, $intCompanyUserId, $objClientDatabase ) {

		// If successful, create folders in the media library for this CProperty object
		$objRootMediaLibraryCompanyMediaFolder = $objClient->fetchRootCompanyMediaFolder( $objClientDatabase );

		// If a media library folder is not available, insert it.
		if( false == isset( $objRootMediaLibraryCompanyMediaFolder ) ) {
			// Create the trash can and main media library folders
			$objGenericFolder = new CCompanyMediaFolder();
			$objGenericFolder->setCid( $objClient->getId() );
			$objGenericFolder->setParentMediaFolderId( NULL );
			$objGenericFolder->setFolderName( 'Media Library' );

			if( !$objGenericFolder->insert( $intCompanyUserId, $objClientDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Invalid Folder Creation: Media Library Folder could not be created - {%s,0}.', [ 'CMediaLibraryModule' ] ) ) );

				return false;
			}

			$objGenericFolder->setId( NULL );
			$objGenericFolder->setFolderName( 'Trash Can' );
			$objGenericFolder->setIsTrashCan( 1 );

			if( !$objGenericFolder->insert( $intCompanyUserId, $objClientDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Invalid Folder Creation: Media Library Trash Can could not be created - {%s, 0}.', [ 'CMediaLibraryModule' ] ) ) );

				return false;
			}
		}

		$objRootMediaLibraryCompanyMediaFolder = $objClient->fetchRootCompanyMediaFolder( $objClientDatabase );

		$arrobjPropertyMediaFolders = $this->createCompanyMediaFolders( $objRootMediaLibraryCompanyMediaFolder, $objClientDatabase );

		if( true == valArr( $arrobjPropertyMediaFolders ) ) {
			foreach( $arrobjPropertyMediaFolders as $objPropertyCompanyMediaFolder ) {
				if( false == $objPropertyCompanyMediaFolder->insert( $intCompanyUserId, $objClientDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Invalid Folder Creation: {%s, 0} Folder could not be created.', [ $objPropertyCompanyMediaFolder->getFolderName() ] ) ) );

					return false;
				}
			}
		}

		return true;
	}

	public function insertGlAccountProperties( $intCurrentUserId, $objClientDatabase ) {
		$boolIsValid				= true;
		$arrobjGlAccountProperties = $this->getGlAccountProperties();

		if( false == valArr( $arrobjGlAccountProperties ) ) {

			// If not set then create one
			$arrobjGlAccountProperties = [];
			$this->setGlAccountProperties( $arrobjGlAccountProperties );
		}

		foreach( $arrobjGlAccountProperties as $objGlAccountProperty ) {

			if( false == $objGlAccountProperty->validate( VALIDATE_INSERT, $objClientDatabase ) || false == $objGlAccountProperty->insert( $intCurrentUserId, $objClientDatabase ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, '', __( 'System error, please contact your support executive.' ) ) );
				break;
			}
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objClientDatabase, $objAdminDatabase = NULL ) {

		if( true == is_null( $objClientDatabase ) || false == valObj( $objClientDatabase, 'CDatabase' ) || CDatabaseType::CLIENT != $objClientDatabase->getDatabaseTypeId() || true == is_null( $objAdminDatabase ) || false == valObj( $objAdminDatabase, 'CDatabase' ) || CDatabaseType::ADMIN != $objAdminDatabase->getDatabaseTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Failed to load database object.' ) ) );

			return false;
		}
		$objOldProperty = \Psi\Eos\Admin\CProperties::createService()->fetchPropertyByIdByCid( $this->getId(), $this->getCid(), $objAdminDatabase );

		$strSql = parent::update( $intCurrentUserId, $objAdminDatabase, $boolReturnSqlOnly = true );

		if( false === $strSql ) {
			return true;
		}

		if( false == $this->executeSql( $strSql, $this, $objAdminDatabase ) ) {
			return false;
		}

		if( false == $this->executeSql( $strSql, $this, $objClientDatabase ) ) {
			return false;
		}

		// To keep log of property_name changes
		$boolIsValid = true;

		if( true == valObj( $objOldProperty, 'CProperty' ) && $objOldProperty->getPropertyName() != $this->getPropertyName() ) {

			// Add property log in admin DB
			$strTableName = 'properties';
			$objTableLog	= new CTableLog();
			$boolIsValid	&= $objTableLog->insert( $intCurrentUserId, $objAdminDatabase, $strTableName, $this->getId(), 'UPDATE', $this->getCid(), $objOldProperty->getPropertyName(), $this->getPropertyName(), 'property_name' );

			// Add property log in entrata DB
			$strOldLogMessage	= 'PROPERTY_NAME::' . $objOldProperty->getPropertyName() . '~^~';
			$strNewLogMessage	= 'PROPERTY_NAME::' . $this->getPropertyName() . '~^~';
			$strMessage			= $objOldProperty->getPropertyName() . ' changed by ' . $this->getPropertyName();
			$objTableLog		= new CTableLog();
			$boolIsValid		&= $objTableLog->insert( $intCurrentUserId, $objClientDatabase, $strTableName, $this->getId(), 'UPDATE', $this->getCid(), $strOldLogMessage, $strNewLogMessage, $strMessage );

			$strSql = 'UPDATE unit_spaces SET property_name = \'' . addslashes( $this->getPropertyName() ) . '\' WHERE property_id = ' . ( int ) $this->getId() . ' AND cid = ' . ( int ) $this->getCid() . ';';

			if( false == $objClientDatabase->execute( $strSql ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'The following error reported.' ) ) );
				$boolIsValid = false;
			}

		}

		// To keep log of poperty disabled_on changes
		$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchEmployeeCompanyUserByIdByCidByIsDisabled( $intCurrentUserId, $this->getCid(), $objClientDatabase );

		if( true == valObj( $objOldProperty, 'CProperty' ) && $objOldProperty->getDisabledOn() != $this->getDisabledOn() ) {

			$strTableName	= 'properties';
			$objTableLog	= new CTableLog();

			if( 1 != $this->getIsDisabled() ) {
				$strDisabledMessage = ' Enabled by ';
				$strOldLogMessage	= ' PROPERTY_STATUS::Disabled~^~ ';
				$strNewLogMessage	= ' PROPERTY_STATUS::Enabled~^~ ';
			} else {
				$strDisabledMessage = ' Disabled by ';
				$strOldLogMessage	= ' PROPERTY_STATUS::Enabled~^~ ';
				$strNewLogMessage	= ' PROPERTY_STATUS::Disabled~^~ ';
			}

			$strMessage		= $this->getPropertyName() . $strDisabledMessage . $objCompanyUser->getUsername() . ' on ' . date( 'Y-m-d H:i:s' );
			$boolIsValid	&= $objTableLog->insert( $intCurrentUserId, $objClientDatabase, $strTableName, $this->getId(), 'UPDATE', $this->getCid(), $strOldLogMessage, $strNewLogMessage, $strMessage );
		}

		return $boolIsValid;
	}

	public function updateIntegratedDefaultRentAndDepositGlSettings( $intCurrentUserId, $objRentIntegrationClientKeyValue, $objDepositIntegrationClientKeyValue, $objClientDatabase ) {

		$objPropertyGlSetting = $this->getOrFetchPropertyGlSetting( $objClientDatabase );

		if( true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) ) {

			$boolIsUpdate = false;

			if( true == valObj( $objRentIntegrationClientKeyValue, 'CIntegrationClientKeyValue' ) && true == is_numeric( $objRentIntegrationClientKeyValue->getValue() ) ) {

				$boolIsUpdate = true;
				$objPropertyGlSetting->setRentArCodeId( $objRentIntegrationClientKeyValue->getValue() );
			}

			if( true == valObj( $objDepositIntegrationClientKeyValue, 'CIntegrationClientKeyValue' ) && true == is_numeric( $objDepositIntegrationClientKeyValue->getValue() ) ) {
				$boolIsUpdate = true;
				$objPropertyGlSetting->setDepositArCodeId( $objDepositIntegrationClientKeyValue->getValue() );
			}

			if( true == $boolIsUpdate && false == $objPropertyGlSetting->update( $intCurrentUserId, $objClientDatabase ) ) {
				$this->processErrorMsg( 'Client: ' . $this->m_objClient->getId() . ' [Property : ' . $this->getPropertyName() . ' ID : ' . $this->getId() . '] failed to update gl settings.', E_USER_WARNING, true );

			}

		}

	}

	public function updatePrivateSpaceConfigurationIdOnApplicationsByPropertyIdByCid( $objDatabase ) {
		$strSql = 'UPDATE
						applications a
					SET
						space_configuration_id = CASE WHEN sub.use_space_configurations_for_rent = TRUE THEN ' . CSpaceConfiguration::SPACE_CONFIG_PRIVATE . ' ELSE ' . CSpaceConfiguration::SPACE_CONFIG_DEFAULT . ' END,
						desired_space_configuration_id = CASE
															WHEN sub.use_space_configurations_for_rent = TRUE THEN
																CASE
																	WHEN desired_space_configuration_id = ' . CSpaceConfiguration::SPACE_CONFIG_CONVENTIONAL . ' THEN ' . CSpaceConfiguration::SPACE_CONFIG_PRIVATE . '
																	ELSE desired_space_configuration_id
																END
															ELSE ' . CSpaceConfiguration::SPACE_CONFIG_DEFAULT . '
														 END

					FROM (
						SELECT
							ca.id as application_id,
							ca.cid,
							paor.use_space_configurations_for_rent
						FROM
							cached_leases cl
							JOIN cached_applications ca ON (cl.cid = ca.cid AND cl.id = ca.lease_id )
							JOIN load_properties(ARRAY [ ' . $this->getCid() . ' ]::INT [ ], ARRAY [ ' . $this->getId() . ' ]::INT [ ], ARRAY [ 1 ]) lp ON (cl.cid = lp.cid AND cl.property_id = lp.property_id AND lp.is_test = 0 AND lp.is_disabled = 0 and lp.property_type_id = 4)
							JOIN property_preferences pp ON (lp.cid = pp.cid AND lp.property_id = pp.property_id AND pp.key = \'ENABLE_SEMESTER_SELECTION\')
							JOIN property_ar_origin_rules paor ON (lp.cid = paor.cid AND lp.property_id = paor.property_id )
						WHERE
							ca.cid = ' . $this->getCid() . ' AND
							ca.property_id = ' . $this->getId() . ' AND
							ca.space_configuration_id = ' . CSpaceConfiguration::SPACE_CONFIG_CONVENTIONAL . ' AND
							ca.property_floorplan_id IS NOT NULL AND
							cl.lease_status_type_id NOT IN (' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . ' )
						) as sub
					WHERE
						a.cid = sub.cid AND
						a.id = sub.application_id AND
						a.space_configuration_id = ' . CSpaceConfiguration::SPACE_CONFIG_CONVENTIONAL;

		return !( false == $objDatabase->execute( $strSql ) );

	}

	public function delete( $intCurrentUserId, $objClientDatabase, $objAdminDatabase = NULL ) {

		if( true == is_null( $objClientDatabase ) || false == valObj( $objClientDatabase, 'CDatabase' ) || CDatabaseType::CLIENT != $objClientDatabase->getDatabaseTypeId() || true == is_null( $objAdminDatabase ) || false == valObj( $objAdminDatabase, 'CDatabase' ) || CDatabaseType::ADMIN != $objAdminDatabase->getDatabaseTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Failed to load database object.' ) ) );

			return false;
		}

		if( false == parent::delete( $intCurrentUserId, $objAdminDatabase ) ) {
			return false;
		}

		if( false == parent::delete( $intCurrentUserId, $objClientDatabase ) ) {
			return false;
		}

		return true;
	}

	/**
	 * Other Functions
	 *
	 */

	public function buildFileAssociationFilePath() {
		return $this->getCid() . '/' . $this->getId() . '/leases/esa_files/';
	}

	public function buildFileAssociationFullFilePath() {
		return getMountsPath( $this->getCid(), PATH_MOUNTS_DOCUMENTS ) . $this->buildFileAssociationFilePath();
	}

	public function goLiveWithEntrata( $objDatabase, $boolCheckIsInitialImport = false, $boolCheckScheduledCharge = false ) {

		$intInitialImportedDeposits		= 0;
		$intInitialImportedInvoices		= 0;
		$intInitialImportedTransactions = 0;

		$strCondition = ( false == $boolCheckIsInitialImport ) ? ' AND is_initial_import = false' : NULL;

		// Ar Trasnactions
		$strSql = 'SELECT
						COUNT( id )
					FROM
						ar_transactions
					WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND property_id = ' . ( int ) $this->getId() . $strCondition;

		$arrintCount = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrintCount[0]['count'] ) ) {
			$intInitialImportedTransactions = ( int ) $arrintCount[0]['count'];
		}

		if( 0 != $intInitialImportedTransactions ) {
			return false;
		}

		if( true == $boolCheckScheduledCharge ) {
			// Scheduled Charges
			$strSql = 'SELECT
						COUNT( id )
					FROM
						scheduled_charges
					WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND property_id = ' . ( int ) $this->getId();

			$arrintCount = fetchData( $strSql, $objDatabase );

			if( true == isset ( $arrintCount[0]['count'] ) ) {
				$intInitialImportedRecurringCharges = ( int ) $arrintCount[0]['count'];
			}

			if( 0 != $intInitialImportedRecurringCharges ) {
				return false;
			}
		}

		// Ar Deposits
		$strSql = 'SELECT
						COUNT( ad.id )
					FROM
						ar_deposits ad
						JOIN ar_deposit_transactions adt ON ( ad.cid = adt.cid AND ad.id = adt.ar_deposit_id AND ad.cid = ' . ( int ) $this->getCid() . ' )
						JOIN ar_transactions at ON (adt.ar_transaction_id = at.id AND adt.cid = at.cid)
					WHERE
						at.property_id = ' . ( int ) $this->getId() . '
						AND ad.is_initial_import <> 1
						AND ad.cid = ' . ( int ) $this->getCid();

		$arrintCount = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrintCount[0]['count'] ) ) {
			$intInitialImportedDeposits = ( int ) $arrintCount[0]['count'];
		}

		if( 0 != $intInitialImportedDeposits ) {
			return false;
		}

		// Invoices
		$strSql = ' SELECT
						COUNT( ah.id )
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ah.id = ad.ap_header_id AND ah.cid = ad.cid AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month AND ah.cid = ' . ( int ) $this->getCid() . ' )
					WHERE
						ad.property_id = ' . ( int ) $this->getId() . '
						AND ah.gl_transaction_type_id = ' . CGlTransactionType::AP_CHARGE . '
						AND ah.is_initial_import = FALSE
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL
						AND ad.cid = ' . ( int ) $this->getCid();

		$arrintCount = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrintCount[0]['count'] ) ) {
			$intInitialImportedInvoices = ( int ) $arrintCount[0]['count'];
		}

		// if any non imported items are exists then do not allow to activate the gl
		return !( 0 != $intInitialImportedInvoices );
	}

	public function forceImportOfPropertyCustomers( $intCompanyUserId, $objDatabase, $boolIsPullLedger = false, $boolIsSendEmail = false ) {

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getId(), $this->m_intCid, CIntegrationService::RETRIEVE_CUSTOMERS, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setDontUpdateLastSyncOn( true );
		$objGenericWorker->setProperty( $this );
		$objGenericWorker->setPullLedger( $boolIsPullLedger );
		if( true == $boolIsSendEmail ) {
			$objGenericWorker->setTransportRules( [ 'property_id' => $this->getId() ] );
		}
		return $objGenericWorker->process();
	}

	public function forceImportOfPropertyCustomer( $intCompanyUserId, $objCustomer, $objLease, $objDatabase, $boolIsPullLedger = false ) {

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getId(), $this->m_intCid, CIntegrationService::RETRIEVE_CUSTOMER, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setDontUpdateLastSyncOn( true );
		$objGenericWorker->setDatabase( $objDatabase );
		$objGenericWorker->setCustomer( $objCustomer );
		$objGenericWorker->setLease( $objLease );
		$objGenericWorker->setProperty( $this );
		$objGenericWorker->setPullLedger( $boolIsPullLedger );
		$objGenericWorker->process();

	}

	public function forceImportOfPropertyUnits( $intCompanyUserId, $objDatabase ) {

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getId(), $this->m_intCid, CIntegrationService::RETRIEVE_PROPERTY_UNITS, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setProperty( $this );
		$objGenericWorker->setDontUpdateLastSyncOn( true );
		$objGenericWorker->process();
	}

	public function forceImportOfUnitsOptimisedPricing( $intCompanyUserId, $objDatabase ) {

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getId(), $this->m_intCid, CIntegrationService::RETRIEVE_UNITS_OPTIMIZED_PRICING, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setProperty( $this );
		$objGenericWorker->setDontUpdateLastSyncOn( true );
		$objGenericWorker->process();

	}

	public function exportUtilityTransactions( $intCompanyUserId, $objDatabase, $objUtilityDatabase, $boolIsQueueSyncOverride = false, $intIntegrationSyncTypeId = NULL ) {
		if( true == is_null( $this->getRemotePrimaryKey() ) ) {
			return true;
		}

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getId(), $this->m_intCid, CIntegrationService::SEND_UTILITY_TRANSACTIONS, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setProperty( $this );
		$objGenericWorker->setQueueSyncOverride( $boolIsQueueSyncOverride );
		$objGenericWorker->setIntegrationSyncTypeId( $intIntegrationSyncTypeId );
		$objGenericWorker->setUtilityDatabase( $objUtilityDatabase );

		return $objGenericWorker->process();
	}

	// This function will update the floor plans rent range [nightly script].
	// Function will set the available units rent range But if no unit is available then it will take the all units rent range

	public function handleUpdatePropertyFloorplans( $objDatabase ) {

		$arrobjPropertyPreferences = $this->fetchPropertyPreferences( $objDatabase );
		$arrobjPropertyPreferences = rekeyObjects( 'Key', $arrobjPropertyPreferences );

		$objFloorplanAvailabilityFilter = new CFloorplanAvailabilityFilter();

		$arrobjPropertyFloorplans = $this->fetchCalculatedPropertyFloorplansByFloorplanAvailabilityFilterByPropertyPreferences( $objFloorplanAvailabilityFilter, $arrobjPropertyPreferences, $objDatabase );

		if( false == valArr( $arrobjPropertyFloorplans ) ) {
			return true;
		}

		$boolIsRequiredFetchAllUnits = false;

		foreach( $arrobjPropertyFloorplans as $objPropertyFloorplan ) {
			if( true == is_null( $objPropertyFloorplan->getAvailableUnits() ) || 1 > $objPropertyFloorplan->getAvailableUnits() ) {
				$boolIsRequiredFetchAllUnits = true;
				break;
			}
		}

		// If none of unit is available then use all units to update the unit price range on property floorplan
		if( true == $boolIsRequiredFetchAllUnits ) {
			$objFloorplanAvailabilityFilter->setIsFetchAllUnits( true );
			$arrobjPropertyFloorplansWithAllUnits = $this->fetchCalculatedPropertyFloorplansByFloorplanAvailabilityFilterByPropertyPreferences( $objFloorplanAvailabilityFilter, $arrobjPropertyPreferences, $objDatabase );
		}

		$arrobjTempPropertyFloorplans = $this->fetchPropertyFloorplans( $objDatabase );

		if( false == valArr( $arrobjTempPropertyFloorplans ) ) {
			return true;
		}

		foreach( $arrobjPropertyFloorplans as $objPropertyFloorplan ) {

			// In phases - we load floor plans of child as well as parent but in this case we just need to update the floor plans of this property - NRW
			if( $objPropertyFloorplan->getPropertyId() != $this->getId() ) {
				continue;
			}

			$objTemPropertyFloorplan = $arrobjTempPropertyFloorplans[$objPropertyFloorplan->getId()];

			if( false == valObj( $objTemPropertyFloorplan, 'CPropertyFloorplan' ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Property [ID: {$d, 0}]: Failed to load property floorplan : [ {%d, 1} ]', [ $this->getId(), $objPropertyFloorplan->getId() ] ) ) );
				continue;
			}

			if( true == is_null( $objTemPropertyFloorplan->getFloorPlanName() ) ) {
				continue;
			}

			$boolIsUpdateRequired = false;

			if( $objTemPropertyFloorplan->getNumberOfAvailableUnits() != $objPropertyFloorplan->getAvailableUnits() ) {
				$boolIsUpdateRequired = true;
				$objTemPropertyFloorplan->setNumberOfAvailableUnits( $objPropertyFloorplan->getAvailableUnits() );
			}

			if( 1 != $objTemPropertyFloorplan->getIsManualRentRange() || 1 != $objTemPropertyFloorplan->getIsManualDepositRange() ) {

				// If none of unit is available then use all units to update the unit price range on property floorplan
				if( 0 == ( int ) $objPropertyFloorplan->getAvailableUnits() ) {
					$objPropertyFloorplan = $arrobjPropertyFloorplansWithAllUnits[$objPropertyFloorplan->getId()];
				}

				if( false == valObj( $objPropertyFloorplan, 'CPropertyFloorplan' ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Property [ID: ' . $this->getId() . ']: Failed to Reload property floorplan : [ ' . $objTemPropertyFloorplan->getFloorPlanName() . ' ]' ) );
					continue;
				}

				if( 1 != $objTemPropertyFloorplan->getIsManualRentRange() ) {
					if( $objTemPropertyFloorplan->getMinRent() != $objPropertyFloorplan->getMinRent() ) {
						$boolIsUpdateRequired = true;
						$objTemPropertyFloorplan->setMinRent( $objPropertyFloorplan->getMinRent() );
					}

					if( $objTemPropertyFloorplan->getMaxRent() != $objPropertyFloorplan->getMaxRent() ) {
						$boolIsUpdateRequired = true;
						$objTemPropertyFloorplan->setMaxRent( $objPropertyFloorplan->getMaxRent() );
					}
				}

				if( 1 != $objTemPropertyFloorplan->getIsManualDepositRange() ) {
					if( $objTemPropertyFloorplan->getMinDeposit() != $objPropertyFloorplan->getMinDeposit() ) {
						$boolIsUpdateRequired = true;
						$objTemPropertyFloorplan->setMinDeposit( $objPropertyFloorplan->getMinDeposit() );
					}

					if( $objTemPropertyFloorplan->getMaxDeposit() != $objPropertyFloorplan->getMaxDeposit() ) {
						$boolIsUpdateRequired = true;
						$objTemPropertyFloorplan->setMaxDeposit( $objPropertyFloorplan->getMaxDeposit() );
					}
				}
			}

			if( true == $boolIsUpdateRequired && false == $objTemPropertyFloorplan->update( SYSTEM_USER_ID, $objDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Yield Star Rent Optimization : Failed to update property floorplan : [ ' . $objPropertyFloorplan->getId() . ' ]' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Property [ID: ' . $this->getId() . ']: Failed to update property floorplan : [ ' . $objPropertyFloorplan->getId() . ' ]' ) );
			}
		}

		return true;
	}

	// This function will update the settings when property associated/removed from revenue management vendor or semester selection disabled.

	public function handleUpdateRatesSetup( $boolRestrictToAllowedRates = true, $intCompanyUserId, $objDatabase, $arrintSelectedOccupancyTypeIds = [] ) {

		$objPropertyPreference = ( true == valArr( $this->getOccupancyTypeIds() ) && true == in_array( COccupancyType::STUDENT, $this->getOccupancyTypeIds() ) ) ? $this->fetchPropertyPreferenceByKey( 'ENABLE_SEMESTER_SELECTION', $objDatabase ) : NULL;

		$boolIsStudentProperty				= ( false == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) ? false : true;
		$boolIsRevenueManagementProperty	= $this->isRevenueManagementEnabled( $objDatabase );

		$arrobjUpdatedPropertyArOriginRules = [];
		$arrobjPropertyArOriginRules		= ( array ) \Psi\Eos\Entrata\CPropertyArOriginRules::createService()->fetchPropertyArOriginRuleByPropertyIdsByArOriginIdsByCid( [ $this->getId() ], [ CArOrigin::BASE, CArOrigin::SPECIAL ], $this->getCid(), $objDatabase );

		$objOptimizedPricingPropertyPreference = $this->fetchPropertyPreferenceByKey( 'OPTIMIZATION_PRICING_THROUGH_PM_SOFTWARE', $objDatabase );

		if( false == $boolIsRevenueManagementProperty && true == valObj( $objOptimizedPricingPropertyPreference, 'CPropertyPreference' ) ) {
			$boolIsRevenueManagementProperty = true;
		}

		foreach( $arrobjPropertyArOriginRules as $objPropertyArOriginRule ) {

			if( false == $boolIsStudentProperty ) {

				if( $boolIsRevenueManagementProperty != $objPropertyArOriginRule->getUseLeaseStartWindowsForRent() || $boolIsRevenueManagementProperty != $objPropertyArOriginRule->getUseLeaseTermsForRent() ) {

					if( true == $boolIsRevenueManagementProperty && CArOrigin::SPECIAL != $objPropertyArOriginRule->getArOriginId() ) {

						$objPropertyArOriginRule->setUseLeaseStartWindowsForRent( true );
						$objPropertyArOriginRule->setUseLeaseTermsForRent( true );

					} else {

						$objPropertyArOriginRule->setUseLeaseStartWindowsForRent( false );
					}

					$objPropertyArOriginRule->setUseLeaseStartWindowsForDeposits( false );
					$objPropertyArOriginRule->setUseLeaseStartWindowsForOther( false );

					$objPropertyArOriginRule->setUseSpaceConfigurationsForRent( false );
					$objPropertyArOriginRule->setUseSpaceConfigurationsForDeposits( false );
					$objPropertyArOriginRule->setUseSpaceConfigurationsForOther( false );

					$arrobjUpdatedPropertyArOriginRules[$objPropertyArOriginRule->getId()] = $objPropertyArOriginRule;
				}

			} else {
				if( CArOrigin::BASE == $objPropertyArOriginRule->getArOriginId() ) {

					$objPropertyArOriginRule->setUseLeaseTermsForRent( true );
					$objPropertyArOriginRule->setUseSpaceConfigurationsForRent( true );

				} elseif( CArOrigin::SPECIAL == $objPropertyArOriginRule->getArOriginId() ) {

					$objPropertyArOriginRule->setUseSpaceConfigurationsForRent( false );

				}

				$arrobjUpdatedPropertyArOriginRules[$objPropertyArOriginRule->getId()] = $objPropertyArOriginRule;
			}
		}

		$arrobjInsertUpdatePropertySpaceConfigurations = [];

		$arrobjPropertySpaceConfigurations = ( array ) CPropertySpaceConfigurations::fetchPropertySpaceConfigurationsByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		$arrobjPropertySpaceConfigurations = rekeyObjects( 'SpaceConfigurationId', $arrobjPropertySpaceConfigurations );

		$arrobjDefaultSpaceConfigurations = ( array ) CSpaceConfigurations::fetchSpaceConfigurationsByCid( $this->getCid(), $objDatabase );

		foreach( $arrobjDefaultSpaceConfigurations as $objDefaultSpaceConfiguration ) {

			$objPropertySpaceConfiguration = getArrayElementByKey( $objDefaultSpaceConfiguration->getId(), $arrobjPropertySpaceConfigurations );

			// Conventional Space config
			if( CSpaceConfiguration::SPACE_CONFIG_CONVENTIONAL == $objDefaultSpaceConfiguration->getId() && true == $objDefaultSpaceConfiguration->getIsDefault() ) {

				if( true == valObj( $objPropertySpaceConfiguration, 'CPropertySpaceConfiguration' ) ) {

					$objPropertySpaceConfiguration->setShowInEntrata( false == $boolIsStudentProperty );
					$objPropertySpaceConfiguration->setShowOnWebsite( false == $boolIsStudentProperty );

					$arrobjInsertUpdatePropertySpaceConfigurations[] = $objPropertySpaceConfiguration;

				} elseif( false == $boolIsStudentProperty ) {

					$objPropertySpaceConfiguration = $this->createPropertySpaceConfiguration();
					$objPropertySpaceConfiguration->setSpaceConfigurationId( $objDefaultSpaceConfiguration->getId() );
					$objPropertySpaceConfiguration->setShowInEntrata( true );
					$objPropertySpaceConfiguration->setShowOnWebsite( true );

					$arrobjInsertUpdatePropertySpaceConfigurations[] = $objPropertySpaceConfiguration;
				}
			}

			// Other Space config
			if( CSpaceConfiguration::SPACE_CONFIG_CONVENTIONAL != $objDefaultSpaceConfiguration->getId() ) {

				if( true == valObj( $objPropertySpaceConfiguration, 'CPropertySpaceConfiguration' ) ) {

					$objPropertySpaceConfiguration->setShowInEntrata( true == $boolIsStudentProperty );
					$objPropertySpaceConfiguration->setShowOnWebsite( true == $boolIsStudentProperty );

					$arrobjInsertUpdatePropertySpaceConfigurations[] = $objPropertySpaceConfiguration;

				} elseif( true == $boolIsStudentProperty && true == in_array( $objDefaultSpaceConfiguration->getId(), [ CSpaceConfiguration::SPACE_CONFIG_PRIVATE, CSpaceConfiguration::SPACE_CONFIG_SHARED ] ) ) {

					$objPropertySpaceConfiguration = $this->createPropertySpaceConfiguration();
					$objPropertySpaceConfiguration->setSpaceConfigurationId( $objDefaultSpaceConfiguration->getId() );
					$objPropertySpaceConfiguration->setShowInEntrata( true );
					$objPropertySpaceConfiguration->setShowOnWebsite( true );

					$arrobjInsertUpdatePropertySpaceConfigurations[] = $objPropertySpaceConfiguration;
				}
			}
		}

		switch( NULL ) {

			default:
				foreach( $arrobjInsertUpdatePropertySpaceConfigurations as $objInsertUpdatePropertySpaceConfiguration ) {

					$strAction = 'insertOrUpdate';
					if( false == $boolIsStudentProperty && true == valArr( $arrintSelectedOccupancyTypeIds ) && true == in_array( COccupancyType::MILITARY, $arrintSelectedOccupancyTypeIds ) && false == in_array( COccupancyType::STUDENT, $arrintSelectedOccupancyTypeIds ) && true == in_array( $objInsertUpdatePropertySpaceConfiguration->getSpaceConfigurationId(), [ CSpaceConfiguration::SPACE_CONFIG_PRIVATE, CSpaceConfiguration::SPACE_CONFIG_SHARED ] ) ) {
						$strAction = 'delete';
					}

					if( false == $objInsertUpdatePropertySpaceConfiguration->$strAction( $intCompanyUserId, $objDatabase ) ) {
						break 2;
					}
				}

				foreach( $arrobjUpdatedPropertyArOriginRules as $objUpdatedPropertyArOriginRule ) {

					$objUpdatedPropertyArOriginRule->setIsRestrictToAllowedRates( $boolRestrictToAllowedRates );

					if( false == $objUpdatedPropertyArOriginRule->insertOrUpdate( $intCompanyUserId, $objDatabase ) ) {
						break 2;
					}
				}

				if( true == $boolIsStudentProperty ) {
					$arrmixMessage = [ 'property_id' => $this->getId(), 'cid' => $this->getCid(), 'company_user_id' => $intCompanyUserId ];

					try {
						$objMessage = new CArStudentApplicationSpaceConfigurationMessage();
						$objMessage->fromArray( $arrmixMessage );
						$objSimpleMessageSender = new \Psi\Libraries\Queue\CEntrataMessageSender( new \Psi\Libraries\Queue\Factory\CAmqpMessageFactory(), 'arstudent.application.space_configuration', CONFIG_CLUSTER_NAME, true );
						$objSimpleMessageSender->send( $objMessage );
					} catch( Exception $objException ) {
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Something Went Wrong..!Could not send Message to Queue!' ) );

						return false;
					}
				}
		}

		return true;
	}

	public function importProperty( $intCompanyUserId, $objDatabase ) {
		if( 0 < strlen( $this->getRemotePrimaryKey() ) ) {
			$objIntegrationDatabase = $this->fetchIntegrationDatabase( $objDatabase );
		}

		if( true == isset( $objIntegrationDatabase ) && true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) ) {

			$arrstrTransportRules = [ 'matching_instructions' => [ $this->getRemotePrimaryKey() => 0 ] ];

			$objGenericWorker = CIntegrationFactory::createWorker( $this->getId(), $this->getCid(), CIntegrationService::RETRIEVE_PROPERTIES, $intCompanyUserId, $objDatabase );
			$objGenericWorker->setProperty( $this );
			$objGenericWorker->setDatabase( $objDatabase );
			$objGenericWorker->setTransportRules( $arrstrTransportRules );
			$objGenericWorker->setDontUpdateLastSyncOn( true );

			return $objGenericWorker->process();
		}

		return true;
	}

	public function generateTrafficStatistics() {
		$intPageViews		= 0;
		$intUniqueSessions	= 0;

		$arrobjNewApplications = [];

		if( true == valArr( $this->m_arrobjApplications ) ) {
			foreach( $this->m_arrobjApplications as $objApplication ) {
				switch( $objApplication->getApplicationStatusId() ) {
					case CApplicationStep::BASIC_INFO:
						$intPageViews		+= 1;
						$intUniqueSessions	+= 1;
						break;

					case CApplicationStep::OPTIONS_AND_FEES:
						$intPageViews									+= 2;
						$intUniqueSessions								+= 1;
						$arrobjNewApplications[$objApplication->getId()] = $objApplication;
						break;

					case CApplicationStep::CONTACTS:
						$intPageViews									+= 3;
						$intUniqueSessions								+= 1;
						$arrobjNewApplications[$objApplication->getId()] = $objApplication;
						break;

					case CApplicationStep::PEOPLE:
						$intPageViews									+= 4;
						$intUniqueSessions								+= 1;
						$arrobjNewApplications[$objApplication->getId()] = $objApplication;
						break;

					case CApplicationStep::CUSTOM_FORM:
						$intPageViews									+= 5;
						$intUniqueSessions								+= 1;
						$arrobjNewApplications[$objApplication->getId()] = $objApplication;
						break;

					case CApplicationStep::PAYMENT:
						$intPageViews									+= 6;
						$intUniqueSessions								+= 1;
						$arrobjNewApplications[$objApplication->getId()] = $objApplication;
						break;

					case CApplicationStep::SUMMARY:
						$intPageViews									+= 7;
						$intUniqueSessions								+= 1;
						$arrobjNewApplications[$objApplication->getId()] = $objApplication;
						break;

					case CApplicationStep::CONFIRMATION:
						$intPageViews									+= 8;
						$intUniqueSessions								+= 1;
						$arrobjNewApplications[$objApplication->getId()] = $objApplication;
						break;

					default:
						$intPageViews									+= 2;
						$intUniqueSessions								+= 1;
						$arrobjNewApplications[$objApplication->getId()] = $objApplication;
						break;
				}
			}
		}

		$this->m_arrobjApplications = $arrobjNewApplications;

		return [ 'page_views' => $intPageViews, 'unique_sessions' => $intUniqueSessions ];
	}

	public function generateGreetingCode( $intGreetingTypeId ) {
		$intGreetingTypeId = ( 1 == strlen( $intGreetingTypeId ) ) ? \Psi\CStringService::singleton()->str_pad( $intGreetingTypeId, 2, 0, STR_PAD_LEFT ) : $intGreetingTypeId;
		return date( 'm', strtotime( $this->getCreatedOn() ) ) . date( 'd', strtotime( $this->getCreatedOn() ) ) . $this->getId() . $intGreetingTypeId;
	}

	public function generateResidentPortalWebsiteUrl( $objDatabase ) {

		$intPropertyId = ( false == is_null( $this->getPropertyId() ) ) ? $this->getPropertyId() : $this->getId();

		$objWebsite = CWebsites::createService()->fetchResidentPortalDefaultWebsiteByPropertyIdsByCid( [ $intPropertyId ], $this->getCid(), $objDatabase );

		if( true == valObj( $objWebsite, 'CWebsite' ) ) {

			$strResidentPortalPrefix = ( true == valStr( CConfig::get( 'SECURE_HOST_PREFIX' ) ) ) ? CConfig::get( 'SECURE_HOST_PREFIX' ) : 'https://';
			$strResidentPortalSuffix = ( true == valStr( CConfig::get( 'RESIDENT_PORTAL_SUFFIX' ) ) ) ? CConfig::get( 'RESIDENT_PORTAL_SUFFIX' ) : '.residentportal.com';

			return $strResidentPortalPrefix . $objWebsite->getSubDomain() . $strResidentPortalSuffix . '/';
		}

		return NULL;
	}

	public function loadAvailableUnitSpaceStatusTypeIds( $objDatabase ) {

		$arrintAvailableUnitSpaceStatusTypeIds = CUnitSpaceStatusType::$c_arrintAvailableUnitSpaceStatusTypes;

		$objPropertyChargeSetting = $this->getOrFetchPropertyChargeSetting( $objDatabase );

		if( false == valObj( $objPropertyChargeSetting, 'CPropertyChargeSetting' ) ) {
			return $arrintAvailableUnitSpaceStatusTypeIds;
		}

		if( false == $objPropertyChargeSetting->getShowAvailableNotReadySpaces() ) {
			$arrintAvailableUnitSpaceStatusTypeIds = array_diff( CUnitSpaceStatusType::$c_arrintAvailableUnitSpaceStatusTypes, [ CUnitSpaceStatusType::VACANT_UNRENTED_NOT_READY ] );
		}

		if( false == $objPropertyChargeSetting->getShowNoticeAvailableSpaces() ) {
			$arrintAvailableUnitSpaceStatusTypeIds = array_diff( CUnitSpaceStatusType::$c_arrintAvailableUnitSpaceStatusTypes, [ CUnitSpaceStatusType::NOTICE_UNRENTED ] );
		}

		return $arrintAvailableUnitSpaceStatusTypeIds;
	}

	public function loadNumberOfUnits( $objDatabase ) {
		$strSql = 'SELECT count(id) FROM property_units WHERE property_id = ' . ( int ) $this->getId() . ' AND deleted_on IS NULL AND cid = ' . ( int ) $this->getCid();

		$arrintUnitCount = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintUnitCount[0]['count'] ) ) {
			$this->m_intNumberOfUnits = ( int ) $arrintUnitCount[0]['count'];

			return ( int ) $arrintUnitCount[0]['count'];
		} else {
			return 0;
		}
	}

	public function loadNumberOfUnitsIncludingChildProperty( $objDatabase ) {

		$strSql = 'SELECT
						count( pu.id )
					FROM
						property_units pu
						JOIN unit_spaces us ON ( us.cid = pu.cid AND us.property_id = pu.property_id AND us.property_unit_id = pu.id AND us.deleted_on IS NULL AND ( us.occupancy_type_id <> ' . COccupancyType::COMMERCIAL . ' OR us.occupancy_type_id IS NULL ) )
						LEFT JOIN properties p ON ( p.cid = pu.cid AND p.id = pu.property_id )
					WHERE
						( pu.property_id = ' . ( int ) $this->getId() . ' OR ( p.property_id = ' . ( int ) $this->getId() . ' AND p.is_disabled = 0 ) )
						AND pu.deleted_on IS NULL
						AND pu.cid = ' . ( int ) $this->getCid();

		$arrintUnitCount = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintUnitCount[0]['count'] ) ) {
			$this->m_intNumberOfUnits = ( int ) $arrintUnitCount[0]['count'];
			return ( int ) $arrintUnitCount[0]['count'];
		} else {
			return 0;
		}
	}

	public function determineHasPublishedOnlineApplication( $objDatabase ) {
		$this->m_boolHasPublishedOnlineApplication = CCompanyApplications::determineHasPublishedOnlineApplicationByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		return $this->m_boolHasPublishedOnlineApplication;
	}

	public function pingIntegrationDatabase( $objDatabase ) {
		$objIntegrationDatabase = $this->fetchIntegrationDatabase( $objDatabase );

		if( false == isset( $objIntegrationDatabase ) || false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) || CIntegrationClientStatusType::ACTIVE != $objIntegrationDatabase->getIntegrationClientStatusTypeId() ) {
			return true;
		}

		return $objIntegrationDatabase->pingIntegrationDatabase( $this->getId(), $objDatabase );
	}

	public function checkIsFloatingMaintenanceRequestAllowed( $objDatabase ) {
		$boolIsValid = false;

		$this->m_arrobjPropertyPreferences = $this->fetchPropertyPreferences( $objDatabase );

		if( true == array_key_exists( 'ALLOW_FLOATING_MAINTENANCE_REQUEST', $this->m_arrobjPropertyPreferences ) ) {

			if( false == is_null( $this->m_arrobjPropertyPreferences['ALLOW_FLOATING_MAINTENANCE_REQUEST']->getValue() ) && true == $this->m_arrobjPropertyPreferences['ALLOW_FLOATING_MAINTENANCE_REQUEST']->getValue() ) {
				$boolIsValid = true;
			}
		}

		return $boolIsValid;
	}

	public function loadOnlineApplicationUrlByIlsPropertyPreferenceKey( $strIlsPropertyPreferenceKey, $objClientDatabase ) {

		$objWebsite = CWebsites::createService()->fetchApplicantWebsiteByPropertyIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );

		$this->m_intPropertyApplicationCount = CPropertyApplications::fetchPropertyApplicationCount( ' WHERE property_id = ' . ( int ) $this->getId() . ' AND cid = ' . ( int ) $this->getCid(), $objClientDatabase );

		$strUrl = NULL;

		if( true == isset( $objWebsite ) && true == valObj( $objWebsite, 'CWebsite' ) ) {

			$strUrl = 'https://' . $objWebsite->getWebsiteDomain() . '/Apts/module/aa/cp[id]/' . $this->getId() . '/pop/t/ils/' . CInternetListingService::APARTMENTS . '/';

			switch( $strIlsPropertyPreferenceKey ) {
				case 'EXPORT_TO_APARTMENTSCOM':
					$objPropertyPreference = $this->fetchPropertyPreferenceByKey( 'APTSCOM_KEY', $objClientDatabase );
					if( true == isset( $objPropertyPreference ) && true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && false == is_null( $objPropertyPreference->getValue() ) ) {
						$strUrl .= 'aid/' . $objPropertyPreference->getValue() . '/';
					} else {
						$strUrl .= 'aid/Enter Prop Num Here';
					}
					break;

				default:
					// default case;
					break;
			}
		}

		$this->m_strOnlineApplicationUrl = $strUrl;

		return $this->m_strOnlineApplicationUrl;
	}

	public function resortPropertyFloorplans( $intUpdatedBy, $objDatabase ) {
		$boolIsValid = true;

		$arrobjPropertyFloorplans = ( array ) CPropertyFloorplans::createService()->fetchPropertyFloorplansByPropertyIdOrderByNumberOfBedroomsByCid( $this->getId(), $this->getCid(), $objDatabase );

		foreach( $arrobjPropertyFloorplans as $objPropertyFloorplan ) {
			$arrintOrderNumbers[$objPropertyFloorplan->getOrderNum()] = $objPropertyFloorplan->getOrderNum();
		}

		arsort( $arrintOrderNumbers );

		foreach( $arrobjPropertyFloorplans as $objPropertyFloorplan ) {

			$objPropertyFloorplan->setOrderNum( array_pop( $arrintOrderNumbers ) );
			$boolIsValid &= $objPropertyFloorplan->update( $intUpdatedBy, $objDatabase );
		}

		return $boolIsValid;
	}

	public function buildKeywords( $strPageName, $objDatabase ) {
		$strPageName				= NULL;
		$strPropertyPrimaryAddress	= '';
		$strCity					= '';
		$strState					= '';

		$objPropertyPrimaryAddress = $this->getOrFetchPrimaryPropertyAddress( $objDatabase );

		if( false == is_null( $objPropertyPrimaryAddress ) && true == valObj( $objPropertyPrimaryAddress, 'CPropertyAddress' ) ) {

			if( false == is_null( $objPropertyPrimaryAddress->getCity() ) && true == valStr( $objPropertyPrimaryAddress->getCity() ) ) {
				$strCity = $objPropertyPrimaryAddress->getCity();
			}

			if( false == is_null( $objPropertyPrimaryAddress->getStateCode() ) && true == valStr( $objPropertyPrimaryAddress->getStateCode() ) ) {
				$strState = \Psi\Eos\Admin\CStates::createService()->determineStateNameByStateCode( $objPropertyPrimaryAddress->getStateCode() );
			}

			$strPropertyPrimaryAddress .= ' Apartment Rentals in ' . $strCity . ' ' . $strState;
			$strPropertyPrimaryAddress .= ', Apartment Rentals on ';

			if( false == is_null( $objPropertyPrimaryAddress->getStreetLine1() ) && true == valStr( $objPropertyPrimaryAddress->getStreetLine1() ) && 0 < strlen( $objPropertyPrimaryAddress->getStreetLine1() ) ) {
				$strPropertyPrimaryAddress .= $objPropertyPrimaryAddress->getStreetLine1() . ' ' . $strCity;
			}
		}

		return $strPropertyPrimaryAddress;
	}

	public function buildTitle() {
		return __( '{%s, 0} Apartment Rentals', [ $this->getPropertyName() ] );
	}

	public function formatExtention( $strExtention ) {
		if( 4 == strlen( $strExtention ) ) {
			return $strExtention;
		}

		while( 4 > strlen( $strExtention ) ) {
			$strExtention = '0' . $strExtention;
		}

		return $strExtention;
	}

	public function addCustomerMessage( $intCompanyUserId, $intMessageSenderTypeId, $strSubject, $strMessage, $intAnnouncementId, $objDatabase ) {
		$objCustomerMessage = new CCustomerMessage();
		$objCustomerMessage->setCid( $this->getCid() );
		$objCustomerMessage->setPropertyId( $this->getId() );
		$objCustomerMessage->setSenderId( $intCompanyUserId );
		$objCustomerMessage->setSenderTypeId( $intMessageSenderTypeId );
		$objCustomerMessage->setCustomerMessageTypeId( CCustomerMessageType::ANNOUNCEMENT );
		$objCustomerMessage->setReferenceId( $intAnnouncementId );
		$objCustomerMessage->setSubject( $strSubject );
		$objCustomerMessage->setMessage( $strMessage );

		$arrobjCustomers = ( array ) \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		foreach( $arrobjCustomers as $objCustomer ) {
			if( true == valObj( $objCustomer, 'CCustomer' ) ) {
				$objClonedCustomerMessage = clone $objCustomerMessage;
				$objClonedCustomerMessage->setCustomerId( $objCustomer->getId() );
				if( false == $objClonedCustomerMessage->validate( VALIDATE_INSERT ) || false == $objClonedCustomerMessage->insert( $intCompanyUserId, $objDatabase ) ) {
					continue;
				}
			}
		}

		return false;
	}

	public function isPMSoftwareEnabled( $arrintPsProductIds, $objClientDatabase ) {
		$boolIsPMSoftwareEnabled = false;

		$objPropertyGlSetting = ( false == valObj( $this->m_objPropertyGlSetting, 'CPropertyGlSetting' ) ) ? $this->getOrFetchPropertyGlSetting( $objClientDatabase ) : $this->m_objPropertyGlSetting;

		if( true == array_key_exists( CPsProduct::ENTRATA, $arrintPsProductIds ) && true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) && true == $objPropertyGlSetting->getActivateStandardPosting() ) {
			$boolIsPMSoftwareEnabled = true;
		}

		return $boolIsPMSoftwareEnabled;
	}

	public function isMessageCenterSmsOptionEnabled( $arrintPsProductIds ) {

		if( false == array_key_exists( CPsProduct::MESSAGE_CENTER, $arrintPsProductIds ) ) {
			return false;
		}

		if( false == array_key_exists( 'ps_product_option_ids', $arrintPsProductIds[CPsProduct::MESSAGE_CENTER] ) ) {
			return false;
		}

		if( false == array_key_exists( CPsProductOption::SMS, $arrintPsProductIds[CPsProduct::MESSAGE_CENTER]['ps_product_option_ids'] ) ) {
			return false;
		}

		if( false == in_array( $this->getId(), $arrintPsProductIds[CPsProduct::MESSAGE_CENTER]['ps_product_option_ids'][CPsProductOption::SMS] ) ) {
			return false;
		}

		return true;
	}

	public function createOrFetchPropertyAppointmentHoursRekeyedByDay( $objDatabase, $intPropertyId ) {
		$arrobjPropertyAppointmentHours = CPropertyAppointmentHours::fetchCustomPropertyAppointmentHoursByPropertyIdByCid( $intPropertyId, $this->getCid(), $objDatabase );

		$arrobjRekeyedPropertyAppointmentHours	= [];
		$arrstrAppointmentHourDays				= [ 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday' ];

		$objPropertyAppointmentHour = new CPropertyAppointmentHour();
		$objPropertyAppointmentHour->setCid( $this->getCid() );
		$objPropertyAppointmentHour->setPropertyId( $intPropertyId );

		if( true == valArr( $arrobjPropertyAppointmentHours ) ) {
			$arrobjRekeyedDayPropertyAppointmentHours	= rekeyObjects( 'day', $arrobjPropertyAppointmentHours );
			$arrintDays									= array_keys( $arrobjRekeyedDayPropertyAppointmentHours );

			foreach( $arrstrAppointmentHourDays as $intDay => $strAppointmentHourDay ) {
				if( true == in_array( $intDay, $arrintDays ) ) {
					$arrobjRekeyedPropertyAppointmentHours[$intDay] = $arrobjRekeyedDayPropertyAppointmentHours[$intDay];
				} else {
					$arrobjRekeyedPropertyAppointmentHours[$intDay] = clone $objPropertyAppointmentHour;
					$arrobjRekeyedPropertyAppointmentHours[$intDay]->setDay( $intDay );
				}
			}
		} else {
			foreach( $arrstrAppointmentHourDays as $intDay => $strAppointmentHourDay ) {
				$arrobjRekeyedPropertyAppointmentHours[$intDay] = clone $objPropertyAppointmentHour;
				$arrobjRekeyedPropertyAppointmentHours[$intDay]->setDay( $intDay );
			}
		}

		return $arrobjRekeyedPropertyAppointmentHours;
	}

	public function processSystemEmail( $boolIsForceFullySendEmail = false ) {
		// Do not Process Email, If the property is disabled.Providing an option if we want to forcefully send email.
		if( true == $boolIsForceFullySendEmail ) {
			return false;
		}

		if( true == $this->getIsDisabled() ) {
			return true;
		}

		return false;
	}

	public function isProductEnabled( $arrintPsProductIds, $intPsProductId, $objDatabase ) {

		if( false == valArr( $arrintPsProductIds ) || false == array_key_exists( $intPsProductId, $arrintPsProductIds ) || false == valArr( $arrintPsProductIds[$intPsProductId] ) || false == array_key_exists( 'property_ids', $arrintPsProductIds[$intPsProductId] ) ) {
			return false;
		}

		$arrintPropertyIds = ( array ) $arrintPsProductIds[$intPsProductId]['property_ids'];

		if( false == in_array( $this->getId(), $arrintPropertyIds ) ) {
			return false;
		}

		switch( $intPsProductId ) {
			case CPsProduct::RESIDENT_VERIFY:
				$objPropertyTransmissionVendor	= $this->fetchPropertyTransmissionVendorByTransmissionVendorIdByTransmissionTypeId( CTransmissionVendor::RESIDENT_VERIFY, CTransmissionType::SCREENING, $objDatabase );
				$boolProductEnabled				= ( true == valObj( $objPropertyTransmissionVendor, 'CPropertyTransmissionVendor' ) );
				break;

			default:
				$boolProductEnabled = true;
		}

		return $boolProductEnabled;
	}

	public function isPaymentTypeAllowedForPlatform( $intPaymentTypeId, $intPlatformId, $objClientDatabase ) {

		if( NULL === $this->m_arrobjPropertyPaymentTypes ) {
			$this->m_arrobjPropertyPaymentTypes = CPropertyPaymentTypes::fetchPropertyPaymentTypesByPropertyIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
			$this->m_arrobjPropertyPaymentTypes = rekeyObjects( 'PaymentTypeId', $this->m_arrobjPropertyPaymentTypes );
		}

		if( false == valArr( $this->m_arrobjPropertyPaymentTypes ) ) {
			return false;
		}

		switch( $intPlatformId ) {
			case CPropertyPaymentType::PLATFORM_ENTRATA:
				$boolIsPaymentTypeAllowed = ( true == valObj( $this->getPropertyPaymentType( $intPaymentTypeId ), 'CPropertyPaymentType' ) ) ? $this->m_arrobjPropertyPaymentTypes[$intPaymentTypeId]->getAllowInEntrata() : false;
				break;

			case CPropertyPaymentType::PLATFORM_RESIDENT_PORTAL:
				$boolIsPaymentTypeAllowed = ( true == valObj( $this->getPropertyPaymentType( $intPaymentTypeId ), 'CPropertyPaymentType' ) ) ? $this->m_arrobjPropertyPaymentTypes[$intPaymentTypeId]->getAllowInResidentPortal() : false;
				break;

			case CPropertyPaymentType::PLATFORM_PROSPECT_PORTAL:
				$boolIsPaymentTypeAllowed = ( true == valObj( $this->getPropertyPaymentType( $intPaymentTypeId ), 'CPropertyPaymentType' ) ) ? $this->m_arrobjPropertyPaymentTypes[$intPaymentTypeId]->getAllowInProspectPortal() : false;
				break;

			default:
				$boolIsPaymentTypeAllowed = false;
				break;
		}

		return $boolIsPaymentTypeAllowed;
	}

	public function getPropertyPaymentType( $intPaymentTypeId ) {

		if( false == valArr( $this->m_arrobjPropertyPaymentTypes ) ) {
			return false;
		}
		if( false == array_key_exists( $intPaymentTypeId, $this->m_arrobjPropertyPaymentTypes ) ) {
			return false;
		}

		return $this->m_arrobjPropertyPaymentTypes[$intPaymentTypeId];
	}

	public function areAnyOfPaymentTypesAllowedForPlatform( $arrintPaymentTypeIds, $intPlatformId, $objClientDatabase ) {
		if( false == valArr( $arrintPaymentTypeIds ) ) {
			return false;
		}

		$boolIsAnyPaymentTypesAllowed = false;

		foreach( $arrintPaymentTypeIds as $intPaymentTypeId ) {
			$boolIsAnyPaymentTypesAllowed = $this->isPaymentTypeAllowedForPlatform( $intPaymentTypeId, $intPlatformId, $objClientDatabase );
			if( true == $boolIsAnyPaymentTypesAllowed ) {
				break;
			}
		}

		return $boolIsAnyPaymentTypesAllowed;
	}

	public function determineResidentVerifyScreeningVersion( $intApplicationId, $objDatabase ) {

		$objScreeningApplicationRequest = CScreeningApplicationRequests::fetchScreeningApplicationRequestByApplicationIdByCid( $intApplicationId, $this->getCid(), $objDatabase );

		if( true == valObj( $objScreeningApplicationRequest, 'CScreeningApplicationRequest' ) ) {
			return $objScreeningApplicationRequest->getScreeningVendorId();
		}

		$intResidentVerifyScreeningVersionId = CTransmissionVendor::RESIDENT_VERIFY;

		$objResidentVerifyScreeningController = new CResidentVerifyScreeningController();

		$objPropertyScreeningBillingAccount = $objResidentVerifyScreeningController->loadPropertyScreeningBillingAccountByPropertyIdByCid( $this->getId(), $this->getCid() );

		if( false == valObj( $objPropertyScreeningBillingAccount, 'CScreeningPropertyAccount' ) ) {
			return NULL;
		}

		return $intResidentVerifyScreeningVersionId;
	}

	public function activateOrDeactivateTransmissionVendor( $objCompanyUser, $intTransmissionVendorId, $objDatabase, $intReviewId = NULL, $boolIsPublished = false ) {

		$objPropertyTransmissionVendorRemoteLocationLog = NULL;
		$arrobjInsertReviewDetailEvents = [];

		$strSql						= ' WHERE is_published = 1 AND id = ' . ( int ) $intTransmissionVendorId;
		$intCountTransmissionVendor = \Psi\Eos\Entrata\CTransmissionVendors::createService()->fetchTransmissionVendorCount( $strSql, $objDatabase );
		if( 0 == $intCountTransmissionVendor ) {
			return [ 'Status' => 'Failure', 'Message' => 'Invailid transmission vendor id.' ];
		}

		if( true == valId( $intReviewId ) ) {
			$objReview = \Psi\Eos\Entrata\CReviews::createService()->fetchReviewByIdByPropertyIdByCid( $intReviewId, $this->getId(), $this->getCid(), $objDatabase );

			if( false == valObj( $objReview, 'CReview' ) ) {
				return [ 'Status' => 'Failure', 'Message' => 'Failed to load review.' ];
			}

			$objReviewDetailEvent = $objReview->createReviewDetailEvent( ( true == $boolIsPublished ) ? CReviewEventType::PROSPECT_PORTAL_PUBLISHED : CReviewEventType::PROSPECT_PORTAL_UNPUBLISHED );

			if( true == $boolIsPublished ) {
				$objReviewDetailEvent->setActivityLogMsg( 'Review published to ProspectPortal' );
				$objReviewDetailEvent->setActivityLogIconClass( 'export' );
			} else {
				$objReviewDetailEvent->setActivityLogMsg( 'Review unpublished from ProspectPortal' );
				$objReviewDetailEvent->setActivityLogIconClass( 'unsync default-cursor' );
			}

			$objReviewDetailEvent->setActivityLogUserFullname( $objCompanyUser->getNameFull() ?? $objCompanyUser->getUsername( false ) );
			$objReviewDetailEvent->setActivityLogCompanyUserId( $objCompanyUser->getId() );
			$objReviewDetailEvent->setActivityLoggedOn( __( '{%t, 0, DATETIME_NUMERIC_ISO_PRECISE}' ) );

			$objReviewDetailEvent->setCreatedBy( $objCompanyUser->getId() );
			$objReviewDetailEvent->setCreatedOn( 'NOW()' );
			$arrobjInsertReviewDetailEvents[] = $objReviewDetailEvent;
		} else {

			$arrobjReviews = ( array ) \Psi\Eos\Entrata\CReviews::createService()->fetchReviewsByPropertyIdByCidByTransmissionVendorId( $this->getId(), $this->getCid(), $intTransmissionVendorId, $objDatabase );
			if( false == valArr( $arrobjReviews ) ) {
				return [ 'Status' => 'Failure', 'Message' => 'Failed to load review for transmission vendor id ' . ( int ) $intTransmissionVendorId . '.' ];
			}

			foreach( $arrobjReviews as $objReview ) {
				$objReviewDetailEvent = $objReview->createReviewDetailEvent( CReviewEventType::PROSPECT_PORTAL_UNPUBLISHED );
				$objReviewDetailEvent->setCreatedBy( $objCompanyUser->getId() );
				$objReviewDetailEvent->setCreatedOn( 'NOW()' );
				$objReviewDetailEvent->setActivityLogMsg( 'Review unpublished from ProspectPortal' );
				$objReviewDetailEvent->setActivityLogIconClass( 'unsync default-cursor' );
				$objReviewDetailEvent->setActivityLogUserFullname( $objCompanyUser->getNameFull() ?? $objCompanyUser->getUsername( false ) );
				$objReviewDetailEvent->setActivityLogCompanyUserId( $objCompanyUser->getId() );
				$objReviewDetailEvent->setActivityLoggedOn( __( '{%t, 0, DATETIME_NUMERIC_ISO_PRECISE}' ) );
				$arrobjInsertReviewDetailEvents[] = $objReviewDetailEvent;
			}
		}

		$objPropertyTransmissionVendorRemoteLocation = \Psi\Eos\Entrata\CPropertyTransmissionVendorRemoteLocations::createService()->fetchPropertyTransmissionVendorRemoteLocationByPropertyIdByCidByTransmissionVendorId( $this->getId(), $this->getCid(), $intTransmissionVendorId, $objDatabase );

		if( true == valObj( $objPropertyTransmissionVendorRemoteLocation, 'CPropertyTransmissionVendorRemoteLocation' ) && false == valId( $intReviewId ) ) {

			$objPropertyTransmissionVendorRemoteLocationLog = new CPropertyTransmissionVendorRemoteLocationLog();
			$objPropertyTransmissionVendorRemoteLocationLog->setCid( $this->getCid() );
			$objPropertyTransmissionVendorRemoteLocationLog->setPropertyId( $this->getId() );
			$objPropertyTransmissionVendorRemoteLocationLog->setPreviousPropertyTransmissionVendorRemoteLocationStatusId( CPropertyTransmissionVendorRemoteLocationStatus::ACTIVE );
			$objPropertyTransmissionVendorRemoteLocationLog->setPropertyTransmissionVendorRemoteLocationStatusId( CPropertyTransmissionVendorRemoteLocationStatus::CANCELLED );

			$objPropertyTransmissionVendorRemoteLocation->setDeletedBy( $objCompanyUser->getId() );
			$objPropertyTransmissionVendorRemoteLocation->setDeletedOn( 'NOW()' );

		} elseif( false == valObj( $objPropertyTransmissionVendorRemoteLocation, 'CPropertyTransmissionVendorRemoteLocation' ) && true == valId( $intReviewId ) ) {

			$objPropertyTransmissionVendorRemoteLocationLog = new CPropertyTransmissionVendorRemoteLocationLog();
			$objPropertyTransmissionVendorRemoteLocationLog->setCid( $this->getCid() );
			$objPropertyTransmissionVendorRemoteLocationLog->setPropertyId( $this->getId() );
			$objPropertyTransmissionVendorRemoteLocationLog->setPreviousPropertyTransmissionVendorRemoteLocationStatusId( NULL );
			$objPropertyTransmissionVendorRemoteLocationLog->setPropertyTransmissionVendorRemoteLocationStatusId( CPropertyTransmissionVendorRemoteLocationStatus::ACTIVE );

			$objPropertyTransmissionVendorRemoteLocation = new CPropertyTransmissionVendorRemoteLocation();
			$objPropertyTransmissionVendorRemoteLocation->setCid( $this->getCid() );
			$objPropertyTransmissionVendorRemoteLocation->setPropertyId( $this->getId() );
			$objPropertyTransmissionVendorRemoteLocation->setTransmissionVendorId( $intTransmissionVendorId );
			$objPropertyTransmissionVendorRemoteLocation->setActivatedOn( 'NOW()' );
		}

		switch( NULL ) {
			default:
				$objDatabase->begin();
				if( true == valArr( $arrobjInsertReviewDetailEvents ) && false == \Psi\Eos\Entrata\CReviewDetailEvents::createService()->bulkInsert( $arrobjInsertReviewDetailEvents, $objCompanyUser->getId(), $objDatabase ) ) {
					$objDatabase->rollback();

					return [ 'Status' => 'Failure', 'Message' => 'Failed to update reviews.' ];
				}

				if( true == valObj( $objPropertyTransmissionVendorRemoteLocationLog, 'CPropertyTransmissionVendorRemoteLocationLog' ) ) {

					if( false == $objPropertyTransmissionVendorRemoteLocationLog->insert( $objCompanyUser->getId(), $objDatabase ) ) {
						$objDatabase->rollback();

						return [ 'Status' => 'Failure', 'Message' => 'Failed to insert PropertyTransmissionVendorRemoteLocationLog while Activating/deactivating Transmission Vendor.' ];
					}

					if( true == valObj( $objPropertyTransmissionVendorRemoteLocation, 'CPropertyTransmissionVendorRemoteLocation' ) ) {

						$objPropertyTransmissionVendorRemoteLocation->setPropertyTransmissionVendorRemoteLocationLogId( $objPropertyTransmissionVendorRemoteLocationLog->getId() );

						if( true == valId( $objPropertyTransmissionVendorRemoteLocation->getId() ) ) {
							if( false == $objPropertyTransmissionVendorRemoteLocation->update( $objCompanyUser->getId(), $objDatabase ) ) {
								$objDatabase->rollback();

								return [ 'Status' => 'Failure', 'Message' => 'Failed to update PropertyTransmissionVendorRemoteLocation while Deactivating Transmission Vendor.' ];
							}
						} else {
							if( false == $objPropertyTransmissionVendorRemoteLocation->insert( $objCompanyUser->getId(), $objDatabase ) ) {
								$objDatabase->rollback();

								return [ 'Status' => 'Failure', 'Message' => 'Failed to insert PropertyTransmissionVendorRemoteLocation while Activating Transmission Vendor.' ];
							}
						}
					}

					$objPropertyTransmissionVendorRemoteLocationLog->setPropertyTransmissionVendorRemoteLocationId( $objPropertyTransmissionVendorRemoteLocation->getId() );

					if( false == $objPropertyTransmissionVendorRemoteLocationLog->update( $objCompanyUser->getId(), $objDatabase ) ) {
						$objDatabase->rollback();

						return [ 'Status' => 'Failure', 'Message' => 'Failed to update PropertyTransmissionVendorRemoteLocationLog while Activating/deactivating Transmission Vendor.' ];
					}
				}
		}
		$objDatabase->commit();

		return [ 'Status' => 'Success', 'Message' => 'Successfully Activated/deactivated Transmission Vendor.' ];
	}

	public function getDefaultAssignedUserForWorkOrder( $objDatabase ) {
		$strDefaultUser						= '';
		$objDefaultUserPropertyPreference	= CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'PROPERTY_DEFAULT_MAINTENANCE_USER', $this->getId(), $this->getCid(), $objDatabase );

		if( true == valObj( $objDefaultUserPropertyPreference, 'CPropertyPreference' ) && true == $objDefaultUserPropertyPreference->getValue() ) {
			$boolIsShowAdmin						= false;
			$arrstrPropertyPreferenceKeys			= [ 'INCLUDE_ADMIN_USER_IN_ASSIGN_TO_WORK_ORDER', 'WORK_ORDER_ALLOW_MULTIPLE_USER_DEPARTMENT' ];
			$arrmixDepartmentPropertyPreferences	= ( array ) CPropertyPreferences::createService()->fetchPropertyPreferenceValuesByPropertyIdsByKeysByCid( [ $this->getId() ], $arrstrPropertyPreferenceKeys, $this->getCid(), $objDatabase );

			if( true == valArr( $arrmixDepartmentPropertyPreferences ) && true == isset( $arrmixDepartmentPropertyPreferences[$this->getId()] ) && true == valId( $this->getId() ) ) {
				$arrmixCompanyDepartmentIds = ( true == array_key_exists( 'WORK_ORDER_ALLOW_MULTIPLE_USER_DEPARTMENT', $arrmixDepartmentPropertyPreferences[$this->getId()] ) ) ? explode( ',', $arrmixDepartmentPropertyPreferences[$this->getId()]['WORK_ORDER_ALLOW_MULTIPLE_USER_DEPARTMENT'] ) : [ CCompanyDepartment::DEFAULT_COMPANY_DEPARTMENT_MAINTENANCE_ID ];
				if( true == array_key_exists( 'INCLUDE_ADMIN_USER_IN_ASSIGN_TO_WORK_ORDER', $arrmixDepartmentPropertyPreferences[$this->getId()] ) && true == $arrmixDepartmentPropertyPreferences[$this->getId()]['INCLUDE_ADMIN_USER_IN_ASSIGN_TO_WORK_ORDER'] ) {
					$boolIsShowAdmin = true;
				}

			}

			$arrmixDefaultUserDetails	= ( array ) CCompanyEmployees::createService()->fetchCompanyEmployeeByIdByDepartmentIdsByPropertyIdByCid( $objDefaultUserPropertyPreference->getValue(), $arrmixCompanyDepartmentIds, $this->getId(), $this->getCid(), $objDatabase, $boolIsShowAdmin );
			$strDefaultUser				= ( true == valArr( $arrmixDefaultUserDetails ) ) ? $arrmixDefaultUserDetails[0]['id'] : '';
		}

		return $strDefaultUser;
	}

	public function getMaintenanceRequestIntervals( $objDatabase, $intPriorityId = NULL, $strCreatedOn = NULL, $boolIsDueTime = true, $objMaintenanceTemplate = NULL ) {
		$intInterval		= NULL;
		$strEndDate			= NULL;
		$objTimeZone		= $this->getOrfetchTimezone( $objDatabase );
		$strTimeZoneName	= ( true == valObj( $objTimeZone, 'CTimeZone' ) ) ? $objTimeZone->getTimeZoneName() : 'America/Denver';

		if( false == valId( $intPriorityId ) && false == valObj( $objMaintenanceTemplate, 'CMaintenanceTemplate' ) ) {
			return $strEndDate;
		}
		$intMaintenanceHourTypeId = CMaintenanceHourType::ALL_HOURS;
		if( true == valId( $intPriorityId ) ) {
			$strKey					= ( true == $boolIsDueTime ) ? 'ENABLE_DUE_TIME' : 'ENABLE_RESPONSE_TIME';
			$objPropertyPreference	= CPropertyPreferences::createService()->fetchPropertyPreferenceByPropertyIdByKeyByCid( $this->getId(), $strKey, $this->getCid(), $objDatabase );

			if( false == valObj( $objPropertyPreference, 'CPropertyPreference' ) || false == $objPropertyPreference->getValue() ) {
				return $strEndDate;
			}

			$objPropertyMaintenancePriority = CPropertyMaintenancePriorities::createService()->fetchCustomPropertyMaintenancePriorityByMaintenancePriorityIdByPropertyIdByCid( $intPriorityId, $this->getId(), $this->getCid(), $objDatabase );
			$boolIsAdjustDueDate = false;

			if( true == valObj( $objPropertyMaintenancePriority, 'CPropertyMaintenancePriority' ) ) {
				if( true == $boolIsDueTime ) {
					$intInterval		= $objPropertyMaintenancePriority->getDueInterval();
					$intIntervalTypeId	= $objPropertyMaintenancePriority->getDueIntervalTypeId();
				} else {
					$intInterval		= $objPropertyMaintenancePriority->getResponseInterval();
					$intIntervalTypeId	= $objPropertyMaintenancePriority->getResponseIntervalTypeId();
				}

				$boolIsAdjustDueDate = true;
				if( CMaintenanceHourType::ALL_HOURS == $objPropertyMaintenancePriority->getMaintenanceHourTypeId() ) {
					$boolIsAdjustDueDate = false;
				}
				$intMaintenanceHourTypeId = $objPropertyMaintenancePriority->getMaintenanceHourTypeId();
			}
		} else {
			if( true == valObj( $objMaintenanceTemplate, 'CMaintenanceTemplate' ) ) {
				$intInterval			= $objMaintenanceTemplate->getDueInterval();
				$intIntervalTypeId		= $objMaintenanceTemplate->getDueIntervalTypeId();
				$boolIsAdjustDueDate	= $objMaintenanceTemplate->getExcludeWeekendsHolidays();
			}
		}

		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferenceByPropertyIdByKeyByCid( $this->getId(), 'USE_CUSTOM_MAINTENANCE_AVAILABILITY', $this->getCid(), $objDatabase );

		// If 'Use Custom Maintenance Availability' is set to YES: display property maintenance hours
		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && true == $objPropertyPreference->getValue() ) {
			$arrobjPropertyHours = CPropertyHours::fetchMaintenancePropertyHoursByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		} else {
			// If 'Use Custom Maintenance Availability' is set to NO: display Property office hours
			$arrobjPropertyHours = CPropertyHours::fetchCustomPropertyOfficeHoursByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		}

		return $this->getMaintenanceRequestTime( $objDatabase, $strCreatedOn, $intInterval, $intIntervalTypeId, $boolIsAdjustDueDate, $arrobjPropertyHours, $strTimeZoneName, $intMaintenanceHourTypeId );

	}

	/**
	 * @param $objDatabase
	 * @param $strCreatedOn
	 * @param $intInterval
	 * @param $intIntervalTypeId
	 * @param $boolIsAdjustDueDate
	 * @param $arrobjPropertyHours
	 * @param $strTimeZoneName
	 * @return false|string
	 */
	public function getMaintenanceRequestTime( $objDatabase, $strCreatedOn, $intInterval, $intIntervalTypeId, $boolIsAdjustDueDate, $arrobjPropertyHours, $strTimeZoneName, $intMaintenanceHourTypeId = NULL ) {
		if( true == valId( $intInterval ) && true == valId( $intIntervalTypeId ) && true == $boolIsAdjustDueDate && true == valArr( $arrobjPropertyHours ) ) {
			$arrstrDateHolidayClosed		= []; // non-working holidays array
			$arrstrDateHolidayOpened		= []; // working holidays array
			$intCountWorkingDaysAdded		= 0;
			$strStartDate					= NULL;
			$boolIsHolidayOnEndDate			= false;
			$boolIsEndDateOnAppointmentDate = false;

			if( false == valId( $intMaintenanceHourTypeId ) ) {
				$intMaintenanceHourTypeId = CMaintenanceHourType::OPEN_HOURS_ONLY;
			}

			// get property holiday list
			$arrstrPropertyHolidays = CPropertyHolidays::fetchPropertyHolidayAvailabilityByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
			$arrstrIntervalType = CIntervalTypes::fetchCustomIntervalTypeNameById( $intIntervalTypeId, $objDatabase );

			if( true == valArr( $arrstrPropertyHolidays ) ) {
				foreach( $arrstrPropertyHolidays as $arrstrPropertyHoliday ) {

					if( false == valStr( $arrstrPropertyHoliday['open_time'] ) && false == valStr( $arrstrPropertyHoliday['close_time'] ) ) {
						$arrstrDateHolidayClosed[$arrstrPropertyHoliday['date']] = $arrstrPropertyHoliday;
					} else {
						$arrstrDateHolidayOpened[$arrstrPropertyHoliday['date']] = $arrstrPropertyHoliday;
					}

				}
			}
			$arrobjPropertyHours = rekeyObjects( 'day', $arrobjPropertyHours );
			$strStartDate = ( true == valStr( $strCreatedOn ) ) ? getConvertedDateTime( $strCreatedOn, 'm/d/Y H:i:s', 'America/Denver', $strTimeZoneName ) : getConvertedDateTime( date( 'Y-m-d H:i:s' ), 'm/d/Y H:i:s', 'America/Denver', $strTimeZoneName );

			$boolIsFoundStartDate = false;

			while( false == $boolIsFoundStartDate ) {

				$intCurrentDay = date_format( new DateTime( $strStartDate ), 'w' );
				if( 0 == $intCurrentDay ) {
					$intCurrentDay = 7;
				}

				$boolIsWorkingDay = false;

				if( ( ( true == array_key_exists( $intCurrentDay, $arrobjPropertyHours )
				        && false == is_null( $arrobjPropertyHours[$intCurrentDay]->getCloseTime() )
				        && false == is_null( $arrobjPropertyHours[$intCurrentDay]->getOpenTime() ) )
				      || true == array_key_exists( date_format( new DateTime( $strStartDate ), 'm/d/Y' ), $arrstrDateHolidayOpened ) )
				    && false == array_key_exists( date_format( new DateTime( $strStartDate ), 'm/d/Y' ), $arrstrDateHolidayClosed ) ) {
					$boolIsWorkingDay			= true;
					$strPropertyOpeningDateTime = date( 'Y-m-d H:i:s', strtotime( date( 'Y-m-d', strtotime( $strStartDate ) ) . $arrobjPropertyHours[( int ) $intCurrentDay]->getOpenTime() ) );
					$strPropertyClosingDateTime = date( 'Y-m-d H:i:s', strtotime( date( 'Y-m-d', strtotime( $strStartDate ) ) . $arrobjPropertyHours[( int ) $intCurrentDay]->getCloseTime() ) );
				}

				if( true == $boolIsWorkingDay && strtotime( $strPropertyOpeningDateTime ) > strtotime( $strStartDate ) ) {
					// if the current time is before the property opening hours, make the start time as day's open time
					$strTempStartDate	= date( 'Y-m-d', strtotime( $strStartDate ) );
					$strTempStartTime	= date( 'H:i:s', strtotime( $arrobjPropertyHours[( int ) $intCurrentDay]->getOpenTime() ) );
					$strStartDate		= date( 'Y-m-d H:i:s', strtotime( $strTempStartDate . $strTempStartTime ) );
					$boolIsFoundStartDate = true;
				} elseif( true == $boolIsWorkingDay && strtotime( $strPropertyOpeningDateTime ) < strtotime( $strStartDate ) && strtotime( $strStartDate ) < strtotime( $strPropertyClosingDateTime ) ) {
					// if creation datetime falls within property working hours
					$strStartDate			= date( 'Y-m-d H:i:s', strtotime( $strStartDate ) );
					$boolIsFoundStartDate	= true;
				} else {
					$strTempStartDate	= date( 'Y-m-d', strtotime( $strStartDate . '+1 days' ) );
					$strTempStartTime	= date( 'H:i:s', strtotime( '00:00:01' ) );
					$strStartDate		= date( 'Y-m-d H:i:s', strtotime( $strTempStartDate . $strTempStartTime ) );
				}

			}

			// new current day and current time based on new start date
			$intCurrentDay = date_format( new DateTime( $strStartDate ), 'w' );
			if( 0 == $intCurrentDay ) {
				$intCurrentDay = 7;
			}

			$boolIsClosedAtNight = false;
			if( CMaintenanceHourType::ALL_OPEN_DAY_HOURS == $intMaintenanceHourTypeId ) {
				$intAllOpenDayHoursNextDay  = $intCurrentDay + 1;
				$strAllOpenDayHoursNextDate = date( 'Y-m-d', strtotime( $strStartDate . '+1 days' ) );
				if( 8 == $intAllOpenDayHoursNextDay ) {
					$intAllOpenDayHoursNextDay = 1;
				}
				if( ( ( true == array_key_exists( $intAllOpenDayHoursNextDay, $arrobjPropertyHours )
				        && false == is_null( $arrobjPropertyHours[$intAllOpenDayHoursNextDay]->getCloseTime() )
				        && false == is_null( $arrobjPropertyHours[$intAllOpenDayHoursNextDay]->getOpenTime() ) )
				      || true == array_key_exists( date_format( new DateTime( $strAllOpenDayHoursNextDate ), 'm/d/Y' ), $arrstrDateHolidayOpened ) )
				    && false == array_key_exists( date_format( new DateTime( $strAllOpenDayHoursNextDate ), 'm/d/Y' ), $arrstrDateHolidayClosed ) ) {
					$boolIsClosedAtNight = true;
				}
			}

			$strCurrentTime              = explode( ':', date_format( new DateTime( $strStartDate ), 'H : i' ) );
			$arrstrCurrentDayOpeningTime = explode( ':', $arrobjPropertyHours[( int ) $intCurrentDay]->getOpenTime() );
			$arrstrCurrentDayClosedTime  = explode( ':', $arrobjPropertyHours[( int ) $intCurrentDay]->getCloseTime() );
			if( true == $boolIsClosedAtNight ) {
				$arrstrCurrentDayClosedTime = [ 24, 0 ];
			}

			// check if the due interval type is days or hours.
			if( CIntervalType::DAYS != $intIntervalTypeId ) {
				// If the due interval type is hours or minutes calculate time based on property hours and set the due / response time accordingly

				if( CIntervalType::HOURS == $intIntervalTypeId ) {
					// convert the due interval from hours to minutes
					$intIntervalInMinutes = ( $intInterval * 60 );
				} elseif( CIntervalType::MINUTES == $intIntervalTypeId ) {
					$intIntervalInMinutes = $intInterval;
				}

				$intRemainingIntervalMinutes = $intIntervalInMinutes;
				// get the remaining hours of work for today

				if( $strCurrentTime[0] < $arrstrCurrentDayOpeningTime[0] || ( $strCurrentTime[0] == $arrstrCurrentDayOpeningTime[0] && $strCurrentTime[1] < $arrstrCurrentDayOpeningTime[1] ) ) {
					$intCurrentDayRemainingMinutesOfWork = ( ( $arrstrCurrentDayClosedTime[0] - $arrstrCurrentDayOpeningTime[0] ) * 60 ) + ( $arrstrCurrentDayClosedTime[1] - $arrstrCurrentDayOpeningTime[1] );
				} else {
					$intCurrentDayRemainingMinutesOfWork = ( ( $arrstrCurrentDayClosedTime[0] - $strCurrentTime[0] ) * 60 ) + ( $arrstrCurrentDayClosedTime[1] - $strCurrentTime[1] );
				}

				if( CMaintenanceHourType::OPEN_HOURS_ONLY == $intMaintenanceHourTypeId && true == isset( $arrobjPropertyHours[( int ) $intCurrentDay + CPropertyHour::LUNCH_HOUR_INTERVAL] ) && true == valObj( $arrobjPropertyHours[( int ) $intCurrentDay + CPropertyHour::LUNCH_HOUR_INTERVAL], CPropertyHour::class ) ) {
					// Calculate Lunch hours
					$arrstrLunchOpenTime   = explode( ':', date_format( new DateTime( $arrobjPropertyHours[( int ) $intCurrentDay + CPropertyHour::LUNCH_HOUR_INTERVAL]->getOpenTime() ), 'H : i' ) );
					$arrstrLunchClosedTime = explode( ':', date_format( new DateTime( $arrobjPropertyHours[( int ) $intCurrentDay + CPropertyHour::LUNCH_HOUR_INTERVAL]->getCloseTime() ), 'H : i' ) );
					$intLunchTimeInMinutes = ( ( ( $arrstrLunchClosedTime[0] - $arrstrLunchOpenTime[0] ) * 60 ) + $arrstrLunchClosedTime[1] - $arrstrLunchOpenTime[1] );
					if( true == valId( $intLunchTimeInMinutes ) && $intCurrentDayRemainingMinutesOfWork > $intLunchTimeInMinutes ) {
						$intCurrentDayRemainingMinutesOfWork -= $intLunchTimeInMinutes;
					}
				}

				if( $intCurrentDayRemainingMinutesOfWork >= $intIntervalInMinutes ) {
					// if today's remaining time is more than the due time then set the due date for the same day
					$strEndDate = date( 'Y-m-d H:i:s', strtotime( $strStartDate . ' + ' . ( int ) $intIntervalInMinutes . ' minutes' ) );
					if( CMaintenanceHourType::OPEN_HOURS_ONLY == $intMaintenanceHourTypeId && true == isset( $arrobjPropertyHours[( int ) $intCurrentDay + CPropertyHour::LUNCH_HOUR_INTERVAL] ) && true == valObj( $arrobjPropertyHours[( int ) $intCurrentDay + CPropertyHour::LUNCH_HOUR_INTERVAL], CPropertyHour::class ) ) {
						$strLunchStartDate = date( 'Y-m-d', strtotime( $strEndDate ) ) . ' ' . $arrobjPropertyHours[( int ) $intCurrentDay + CPropertyHour::LUNCH_HOUR_INTERVAL]->getOpenTime();
						if( strtotime( $strEndDate ) > strtotime( $strLunchStartDate ) ) {
							$strEndDate = date( 'Y-m-d H:i:s', strtotime( $strEndDate . ' + ' . ( int ) $intLunchTimeInMinutes . ' minutes' ) );
						}
					}
				} else {

					if( true == valId( $intCurrentDayRemainingMinutesOfWork ) && $intIntervalInMinutes > $intCurrentDayRemainingMinutesOfWork ) {
						// if some time is remaining for today's work but not enough to work completely for due date
						$intRemainingIntervalMinutes = $intIntervalInMinutes - $intCurrentDayRemainingMinutesOfWork;
					} elseif( 0 > $intCurrentDayRemainingMinutesOfWork ) {
						// if the property hours has already passed
						$intRemainingIntervalMinutes = $intIntervalInMinutes;
					}

					// check the subsequent days
					$intNextDay = $intCurrentDay + 1;

					if( 8 == $intNextDay ) {
						$intNextDay = 1;
					}

					// this is the number of days to be cary forward
					$strStartDate = date( 'Y-m-d', strtotime( $strStartDate . ' + 1 days' ) );

					// run following loop till remaining interval minute is reached
					while( 0 < $intRemainingIntervalMinutes ) {
						$boolIsClosedAtNight = false;

						if( ( ( true == isset( $arrobjPropertyHours[$intNextDay] )
						        && false == is_null( $arrobjPropertyHours[$intNextDay]->getOpenTime() )
						        && false == is_null( $arrobjPropertyHours[$intNextDay]->getCloseTime() ) )
						      || true == array_key_exists( date_format( new DateTime( $strStartDate ), 'm/d/Y' ), $arrstrDateHolidayOpened ) )
						    && false == array_key_exists( date_format( new DateTime( $strStartDate ), 'm/d/Y' ), $arrstrDateHolidayClosed ) ) {

							$strNextDayOpenTime  = $arrobjPropertyHours[$intNextDay]->getOpenTime();
							$strNextDayCloseTime = $arrobjPropertyHours[$intNextDay]->getCloseTime();
							if( CMaintenanceHourType::ALL_OPEN_DAY_HOURS == $intMaintenanceHourTypeId ) {
								$intAllOpenDayHoursLastDay  = $intNextDay - 1;
								$strAllOpenDayHoursLastDate = date( 'Y-m-d', strtotime( $strStartDate . '-1 days' ) );
								if( 0 == $intAllOpenDayHoursLastDay ) {
									$intAllOpenDayHoursLastDay = 7;
								}
								$intAllOpenDayHoursNextDay  = $intNextDay + 1;
								$strAllOpenDayHoursNextDate = date( 'Y-m-d', strtotime( $strStartDate . '+1 days' ) );
								if( 8 == $intAllOpenDayHoursNextDay ) {
									$intAllOpenDayHoursNextDay = 1;
								}
								if( ( ( true == array_key_exists( $intAllOpenDayHoursLastDay, $arrobjPropertyHours )
								        && false == is_null( $arrobjPropertyHours[$intAllOpenDayHoursLastDay]->getCloseTime() )
								        && false == is_null( $arrobjPropertyHours[$intAllOpenDayHoursLastDay]->getOpenTime() ) )
								      || true == array_key_exists( date_format( new DateTime( $strAllOpenDayHoursLastDate ), 'm/d/Y' ), $arrstrDateHolidayOpened ) )
								    && false == array_key_exists( date_format( new DateTime( $strAllOpenDayHoursLastDate ), 'm/d/Y' ), $arrstrDateHolidayClosed ) ) {
									$strNextDayOpenTime  = '00:00';
								}
								if( ( ( true == array_key_exists( $intAllOpenDayHoursNextDay, $arrobjPropertyHours )
								        && false == is_null( $arrobjPropertyHours[$intAllOpenDayHoursNextDay]->getCloseTime() )
								        && false == is_null( $arrobjPropertyHours[$intAllOpenDayHoursNextDay]->getOpenTime() ) )
								      || true == array_key_exists( date_format( new DateTime( $strAllOpenDayHoursNextDate ), 'm/d/Y' ), $arrstrDateHolidayOpened ) )
								    && false == array_key_exists( date_format( new DateTime( $strAllOpenDayHoursNextDate ), 'm/d/Y' ), $arrstrDateHolidayClosed ) ) {
									$boolIsClosedAtNight = true;
								}
							}

							$arrstrOpenTime		= explode( ':', date_format( new DateTime( $strNextDayOpenTime ), 'H : i' ) );
							$arrstrClosedTime	= explode( ':', date_format( new DateTime( $strNextDayCloseTime ), 'H : i' ) );
							if( true == $boolIsClosedAtNight ) {
								$arrstrClosedTime = [ 24, 0 ];
							}

							// check no. of working hours available for a property; convert the total working hours in minutes
							$intTotalDayWorkInMinutes = ( ( ( $arrstrClosedTime[0] - $arrstrOpenTime[0] ) * 60 ) + $arrstrClosedTime[1] - $arrstrOpenTime[1] );
							if( CMaintenanceHourType::OPEN_HOURS_ONLY == $intMaintenanceHourTypeId && true == isset( $arrobjPropertyHours[( int ) $intNextDay + CPropertyHour::LUNCH_HOUR_INTERVAL] ) && true == valObj( $arrobjPropertyHours[( int ) $intNextDay + CPropertyHour::LUNCH_HOUR_INTERVAL], CPropertyHour::class ) ) {
								// Calculate Lunch hours
								$arrstrLunchOpenTime   = explode( ':', date_format( new DateTime( $arrobjPropertyHours[( int ) $intNextDay + CPropertyHour::LUNCH_HOUR_INTERVAL]->getOpenTime() ), 'H : i' ) );
								$arrstrLunchClosedTime = explode( ':', date_format( new DateTime( $arrobjPropertyHours[( int ) $intNextDay + CPropertyHour::LUNCH_HOUR_INTERVAL]->getCloseTime() ), 'H : i' ) );
								$intLunchTimeInMinutes = ( ( ( $arrstrLunchClosedTime[0] - $arrstrLunchOpenTime[0] ) * 60 ) + $arrstrLunchClosedTime[1] - $arrstrLunchOpenTime[1] );
								if( true == valId( $intLunchTimeInMinutes ) && $intTotalDayWorkInMinutes > $intLunchTimeInMinutes ) {
									$intTotalDayWorkInMinutes -= $intLunchTimeInMinutes;
								}
							}

							// calculate end date time when remaining interval minutes are less than total working minutes available for the property for current day
							if( $intTotalDayWorkInMinutes >= $intRemainingIntervalMinutes ) {
								$strEndTime = date( 'H:i:s', strtotime( $strNextDayOpenTime . ' + ' . ( int ) $intRemainingIntervalMinutes . ' minutes' ) );
								$strEndDate = date( 'Y-m-d H:i:s', strtotime( $strStartDate . $strEndTime ) );
								if( CMaintenanceHourType::OPEN_HOURS_ONLY == $intMaintenanceHourTypeId && true == isset( $arrobjPropertyHours[( int ) $intNextDay + CPropertyHour::LUNCH_HOUR_INTERVAL] ) && true == valObj( $arrobjPropertyHours[( int ) $intNextDay + CPropertyHour::LUNCH_HOUR_INTERVAL], CPropertyHour::class ) ) {
									$strLunchStartDate = date( 'Y-m-d', strtotime( $strEndDate ) ) . ' ' . $arrobjPropertyHours[( int ) $intNextDay + CPropertyHour::LUNCH_HOUR_INTERVAL]->getOpenTime();
									if( strtotime( $strEndDate ) > strtotime( $strLunchStartDate ) ) {
										$strEndDate = date( 'Y-m-d H:i:s', strtotime( $strEndDate . ' + ' . ( int ) $intLunchTimeInMinutes . ' minutes' ) );
									}
								}
							}

							// calculate interval minutes still available after deducting the total working minutes available for the property for current day
							$intRemainingIntervalMinutes = ( $intRemainingIntervalMinutes - $intTotalDayWorkInMinutes );

						}

						// increment the day
						$intNextDay += 1;

						if( 8 == $intNextDay ) {
							$intNextDay = 1;
						}

						$strStartDate = date( 'Y-m-d', strtotime( $strStartDate . ' + 1 days' ) );
					}
				}
			} elseif( CIntervalType::DAYS == $intIntervalTypeId ) {
				// If the due interval type is days simply add the days to the Due date
				$strEndDate = date( 'Y-m-d H:i:s', strtotime( $strStartDate . ' + ' . ( int ) $intInterval . $arrstrIntervalType[0]['name'] ) );

				$intEndDay = date_format( new DateTime( $strEndDate ), 'w' );

				// check if the End Date is a Holiday
				if( true == array_key_exists( date_format( new DateTime( $strEndDate ), 'm/d/Y' ), $arrstrDateHolidayClosed ) ) {
					$boolIsHolidayOnEndDate = true;
				}

				// check if the end date is on appointment day
				if( true == array_key_exists( $intEndDay, $arrobjPropertyHours ) && true == is_null( $arrobjPropertyHours[$intEndDay]->getCloseTime() ) && true == is_null( $arrobjPropertyHours[$intEndDay]->getOpenTime() ) ) {
					$boolIsEndDateOnAppointmentDate = true;
				}

				if( false == array_key_exists( $intEndDay, $arrobjPropertyHours ) || true == $boolIsEndDateOnAppointmentDate || true == $boolIsHolidayOnEndDate ) {
					$strEndDate = date( 'Y-m-d H:i:s', strtotime( $strEndDate . ' + 24 hours' ) );
				}

				// Increment the dates if holiday or closed day falls in between start date and end date
				While( date( 'Y-m-d', strtotime( $strStartDate ) ) < date( 'Y-m-d', strtotime( $strEndDate ) ) ) {
					$intDay = date_format( new DateTime( $strStartDate ), 'w' );

					if( 0 == $intDay ) {
						$intDay = 7;
					}

					if( ( ( true == array_key_exists( $intDay, $arrobjPropertyHours ) && false == is_null( $arrobjPropertyHours[$intDay]->getCloseTime() ) && false == is_null( $arrobjPropertyHours[$intDay]->getOpenTime() ) ) || true == array_key_exists( date_format( new DateTime( $strStartDate ), 'm/d/Y' ), $arrstrDateHolidayOpened ) ) && false == array_key_exists( date_format( new DateTime( $strStartDate ), 'm/d/Y' ), $arrstrDateHolidayClosed ) ) {

						if( $intCountWorkingDaysAdded == $intInterval ) {
							$strEndDate = date_format( new DateTime( $strStartDate ), 'Y-m-d H:i:s' );
							break;
						}

						$intCountWorkingDaysAdded += 1;
					}

					$strStartDate	= date( 'Y-m-d H:i:s', strtotime( $strStartDate . ' + 24 hours' ) );
					$strEndDate		= date( 'Y-m-d H:i:s', strtotime( $strEndDate . ' + 24 hours' ) );
				}

				// if end date is before or after the opening adjust according to it
				$intCurrentDay	= date_format( new DateTime( $strEndDate ), 'w' );
				$strTempEndDate = date( 'Y-m-d', strtotime( $strEndDate ) );

				if( true == isset( $arrobjPropertyHours[$intCurrentDay] ) ) {
					$strEndDate = date( 'Y-m-d H:i:s', strtotime( $strTempEndDate . $arrobjPropertyHours[( int ) $intCurrentDay]->getCloseTime() ) );
				}
			}
		} elseif( true == valId( $intInterval ) && true == valId( $intIntervalTypeId ) ) {

			$arrstrIntervalType = CIntervalTypes::fetchCustomIntervalTypeNameById( $intIntervalTypeId, $objDatabase );
			$strStartDate	= ( true == valStr( $strCreatedOn ) ) ? getConvertedDateTime( $strCreatedOn, 'm/d/Y H:i:s', 'America/Denver', $strTimeZoneName ) : getConvertedDateTime( date( 'Y-m-d H:i:s' ), 'm/d/Y H:i:s', 'America/Denver', $strTimeZoneName );
			$strEndDate		= date( 'Y-m-d H:i:s', strtotime( $strStartDate . ' + ' . ( int ) $intInterval . $arrstrIntervalType[0]['name'] ) );
		}

		return $strEndDate;

	}

	public function getHasOccupancyType( $intOccupancyTypeId ) {
		return true == valArr( $this->m_arrintOccupancyTypeIds ) && true == in_array( $intOccupancyTypeId, $this->m_arrintOccupancyTypeIds );
	}

	public function loadActiveLeasingAgents( $objDatabase ) {
		$intCompanyManegerialDepartmentId = \Psi\Eos\Entrata\CCompanyDepartments::createService()->fetchCompanyDepartmentIdByNameByCid( 'Managerial', $this->getCid(), $objDatabase );

		return \Psi\Eos\Entrata\CPropertyLeasingAgents::createService()->fetchActivePropertyLeasingAgentsByPropertyIdByCidByCompanyDepartmentId( $this->getId(), $this->getCid(), $intCompanyManegerialDepartmentId, $objDatabase );
	}

	public function createPropertyPaymentTypeArCode() {
		$objPropertyPaymentTypeArCode = new CPropertyPaymentTypeArCode();
		$objPropertyPaymentTypeArCode->setCid( $this->getCid() );
		$objPropertyPaymentTypeArCode->setPropertyId( $this->getId() );

		return $objPropertyPaymentTypeArCode;
	}

	public function createPropertyPaymentType() {
		$objPropertyPaymentType = new CPropertyPaymentType();
		$objPropertyPaymentType->setCid( $this->getCid() );
		$objPropertyPaymentType->setPropertyId( $this->getId() );

		return $objPropertyPaymentType;
	}

	public function getPropertyPrimaryAddress() {
		$objPropertyAddress = CPropertyAddresses::createService()->fetchPropertyAddressByPropertyIdByAddressTypeIdByCid( $this->getId(), CAddressType::PRIMARY, $this->getCid(), $this->m_objDatabase );

		$strPropertyPrimaryAddress = '';
		if( false != valObj( $objPropertyAddress, 'CPropertyAddress' ) ) {
			$strStreetAddress	= implode( ', ', array_filter( [ $objPropertyAddress->getStreetLine1(), $objPropertyAddress->getStreetLine2(), $objPropertyAddress->getStreetLine3() ] ) );
			$strCityAddress		= implode( ', ', array_filter( [ $objPropertyAddress->getCity(), $objPropertyAddress->getStateCode() . ' ' . $objPropertyAddress->getPostalCode() ] ) );
			$strPropertyPrimaryAddress = $strStreetAddress . '<br>' . $strCityAddress;
		}
		return $strPropertyPrimaryAddress;
	}

	public function getPropertyMailingAddress( $objDatabase = NULL ) {
		if( true == empty( $objDatabase ) ) $objDatabase = $this->getDatabase();

		$objPropertyAddress = CPropertyAddresses::createService()->fetchPropertyAddressByPropertyIdByAddressTypeIdByCid( $this->getId(), CAddressType::MAILING, $this->getCid(), $objDatabase );

		if( false == valObj( $objPropertyAddress, 'CPropertyAddress' ) ) {
			$objPropertyAddress = CPropertyAddresses::createService()->fetchPropertyAddressByPropertyIdByAddressTypeIdByCid( $this->getId(), CAddressType::PRIMARY, $this->getCid(), $objDatabase );
		}

		$strPropertyMailingAddress = '';
		if( false != valObj( $objPropertyAddress, 'CPropertyAddress' ) ) {
			$strStreetAddress	= implode( ', ', array_filter( [ $objPropertyAddress->getStreetLine1(), $objPropertyAddress->getStreetLine2(), $objPropertyAddress->getStreetLine3() ] ) );
			$strCityAddress		= implode( ', ', array_filter( [ $objPropertyAddress->getCity(), $objPropertyAddress->getStateCode() . ' ' . $objPropertyAddress->getPostalCode() ] ) );
			$strPropertyMailingAddress = $strStreetAddress . '<br>' . $strCityAddress;
		}

		return $strPropertyMailingAddress;
	}

	public function getOfficePhoneNum() {

		$objVoipDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::VOIP, CDatabaseUserType::PS_VOIP, CDatabaseServerType::POSTGRES, false );

		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			return '';
		}

		$objVoipDatabase->open();
		$objPropertyCallSetting = CPropertyCallSettings::fetchPropertyCallSettingsByCidByPropertyId( $this->getCid(), $this->getId(), $objVoipDatabase );

		$strOfficePhoneNumber = '';
		if( false != valObj( $objPropertyCallSetting, 'CPropertyCallSetting' ) ) {
			$strOfficePhoneNumber = __( '{%h, 0}', [ $objPropertyCallSetting->getOfficePhoneNumber() ] );
		}
		$objVoipDatabase->close();

		return $strOfficePhoneNumber;
	}

	public function getAfterHoursOfficePhoneNumber() {

		$objVoipDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::VOIP, CDatabaseUserType::PS_VOIP, CDatabaseServerType::POSTGRES, false );

		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			return '';
		}

		$objVoipDatabase->open();
		$objPropertyCallSetting = CPropertyCallSettings::fetchPropertyCallSettingsByCidByPropertyId( $this->getCid(), $this->getId(), $objVoipDatabase );

		$strAfterHoursOfficePhoneNumber = '';
		if( false != valObj( $objPropertyCallSetting, 'CPropertyCallSetting' ) ) {
			$strAfterHoursOfficePhoneNumber = __( '{%h, 0}', [ $objPropertyCallSetting->getAfterHoursOfficePhoneNumber() ] );
		}

		$objVoipDatabase->close();

		return $strAfterHoursOfficePhoneNumber;
	}

	public function getMaintenanceEmergencyPhoneNumber() {

		$objVoipDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::VOIP, CDatabaseUserType::PS_VOIP, CDatabaseServerType::POSTGRES, false );

		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			return '';
		}

		$objVoipDatabase->open();
		$objPropertyCallSetting = CPropertyCallSettings::fetchPropertyCallSettingsByCidByPropertyId( $this->getCid(), $this->getId(), $objVoipDatabase );

		$strMaintenanceEmergencyPhoneNumber = '';
		if( false != valObj( $objPropertyCallSetting, 'CPropertyCallSetting' ) ) {
			$strMaintenanceEmergencyPhoneNumber = __( '{%h, 0}', [ $objPropertyCallSetting->getMaintenanceEmergencyPhoneNumber() ] );
		}

		$objVoipDatabase->close();

		return $strMaintenanceEmergencyPhoneNumber;
	}

	public function getAfterHoursMaintenanceEmergencyPhoneNumber() {

		$objVoipDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::VOIP, CDatabaseUserType::PS_VOIP, CDatabaseServerType::POSTGRES, false );

		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			return '';
		}

		$objVoipDatabase->open();

		$objPropertyCallSetting = CPropertyCallSettings::fetchPropertyCallSettingsByCidByPropertyId( $this->getCid(), $this->getId(), $objVoipDatabase );

		$strAfterHoursMaintenanceEmergencyPhoneNumber = '';
		if( false != valObj( $objPropertyCallSetting, 'CPropertyCallSetting' ) ) {
			$strAfterHoursMaintenanceEmergencyPhoneNumber = __( '{%h, 0}', [ $objPropertyCallSetting->getAfterHoursMaintenanceEmergencyPhoneNumber() ] );
		}

		$objVoipDatabase->close();

		return $strAfterHoursMaintenanceEmergencyPhoneNumber;
	}

	public function getMaintenancePhoneNumber() {

		$objVoipDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::VOIP, CDatabaseUserType::PS_VOIP, CDatabaseServerType::POSTGRES, false );

		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			return '';
		}

		$objVoipDatabase->open();
		$objPropertyCallSetting = CPropertyCallSettings::fetchPropertyCallSettingsByCidByPropertyId( $this->getCid(), $this->getId(), $objVoipDatabase );

		$strMaintenancePhoneNumber = '';
		if( false != valObj( $objPropertyCallSetting, 'CPropertyCallSetting' ) ) {
			$strMaintenancePhoneNumber = __( '{%h, 0}', [ $objPropertyCallSetting->getMaintenancePhoneNumber() ] );
		}

		$objVoipDatabase->close();

		return $strMaintenancePhoneNumber;
	}

	public function getAfterHoursMaintenancePhoneNumber() {

		$objVoipDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::VOIP, CDatabaseUserType::PS_VOIP, CDatabaseServerType::POSTGRES, false );

		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			return '';
		}

		$objVoipDatabase->open();
		$objPropertyCallSetting = CPropertyCallSettings::fetchPropertyCallSettingsByCidByPropertyId( $this->getCid(), $this->getId(), $objVoipDatabase );

		$strAfterHoursMaintenancePhoneNumber = '';
		if( false != valObj( $objPropertyCallSetting, 'CPropertyCallSetting' ) ) {
			$strAfterHoursMaintenancePhoneNumber = __( '{%h, 0}', [ $objPropertyCallSetting->getAfterHoursMaintenancePhoneNumber() ] );
		}

		$objVoipDatabase->close();

		return $strAfterHoursMaintenancePhoneNumber;
	}

	public function getPropertyLogo() {
		$arrmixRequiredParameters = $this->getRequiredParameters();
		$boolIsUseLink = isset( $arrmixRequiredParameters['ar_payment_id'] );

		$strPropertyLogo = $this->getMarketingMediaAssociationEmailImage( CMarketingMediaSubType::PROPERTY_LOGO, $boolIsUseLink );
		if( false == valStr( $strPropertyLogo ) ) {
			$objLogoMarketingMediaAssociation = \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getCid(), CMarketingMediaType::COMPANY_PREFERENCES_DEFAULT_IMAGE, CMarketingMediaSubType::COMPANY_LOGO, $this->getCid(), $this->m_objDatabase);
			if( true == valObj( $objLogoMarketingMediaAssociation, 'CMarketingMediaAssociation' ) ) {
				$strUrl			 = ( true == valStr( $objLogoMarketingMediaAssociation->getExternalUri() ) ) ? $objLogoMarketingMediaAssociation->getExternalUri() : '#';

				if( true == $boolIsUseLink ) {
					$strPropertyLogo = '<img src="' . CConfig::get( 'media_library_path' ) . $objLogoMarketingMediaAssociation->getMarketingStorageTypeReference() . '" title="' . \Psi\CStringService::singleton()->htmlentities( $objLogoMarketingMediaAssociation->getMediaAlt() ) . '"/>';
				} else {
					$strPropertyLogo = '<a target="_blank" href="' . $strUrl . '" border="0"><img style="display:block;" src="' . CConfig::get( 'media_library_path' ) . $objLogoMarketingMediaAssociation->getMarketingStorageTypeReference() . '" title="' . \Psi\CStringService::singleton()->htmlentities( $objLogoMarketingMediaAssociation->getMediaAlt() ) . '"/></a>';
				}
			}
		}

		return $strPropertyLogo;
	}

	public function getCompanyCorporateLogo() {
		$strCompanyLogo = '';
		$objLogoMarketingMediaAssociation = \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getCid(), CMarketingMediaType::COMPANY_PREFERENCES_DEFAULT_IMAGE, CMarketingMediaSubType::COMPANY_LOGO, $this->getCid(), $this->m_objDatabase);
		if( true == valObj( $objLogoMarketingMediaAssociation, 'CMarketingMediaAssociation' ) ) {
			$strUrl			 = ( true == valStr( $objLogoMarketingMediaAssociation->getExternalUri() ) ) ? $objLogoMarketingMediaAssociation->getExternalUri() : '#';
			$strCompanyLogo = '<a target="_blank" href="' . $strUrl . '" border="0"><img style="display:block;" src="' . CConfig::get( 'media_library_path' ) . $objLogoMarketingMediaAssociation->getMarketingStorageTypeReference() . '" title="' . \Psi\CStringService::singleton()->htmlentities( $objLogoMarketingMediaAssociation->getMediaAlt() ) . '"/></a>';
		}
		return $strCompanyLogo;
	}

	public function getLogoInResidentPaymentReceipt() {
		$objClient = $this->fetchClient( $this->m_objDatabase );
		if( true == \valObj( $objClient, CClient::class ) ) {
			$objCompanyLogo = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( \CMarketingMediaSubType::COMPANY_LOGO, $this->m_objDatabase );
		}
		$objPropertyLogo = \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getId(), CMarketingMediaType::PROPERTY_MEDIA, CMarketingMediaSubType::PROPERTY_LOGO, $this->getCid(), $this->m_objDatabase );

		$boolAreBothLogosExist    = ( true == \valObj( $objCompanyLogo, \CCompanyMediaFile::class ) && true == \valObj( $objPropertyLogo, \CCompanyMediaFile::class ) );
		$boolIsPropertyLogosExist = ( true == \valObj( $objPropertyLogo, \CMarketingMediaAssociation::class ) );
		$boolIsCompanyLogosExist  = ( true == \valObj( $objCompanyLogo, \CMarketingMediaAssociation::class ) );
		if( true == $boolAreBothLogosExist ) {
			$strHtmlForBothLogos = '<table cellpadding="0" cellspacing="0" border="0" style="width:100%; max-width: 580px;">
				<tbody>
				<tr>
					<td align="left">
						<img  src="' . $objPropertyLogo->getMediaLibraryFullSizeUri() . '" style="align:left;" >
					</td>
					<td align="right">
						<img  src="' . $objCompanyLogo->getMediaLibraryFullSizeUri() . '" style="align:right;" >
					</td>
				</tr>
				</tbody>
			</table>';

			return $strHtmlForBothLogos;
		} elseif( true == $boolIsPropertyLogosExist ) {
			$strHtmlForPropertyLogo = '<table cellpadding="0" cellspacing="0" border="0" style="width:100%; max-width: 580px;">
				<tbody>
				<tr>
					<td width="100%" max-width="580px" height="134" align="center">
						<img  src="' . $objPropertyLogo->getMediaLibraryFullSizeUri() . '" style="align:center;" >
					</td>
				</tr>
				</tbody>
			</table>';

			return $strHtmlForPropertyLogo;
		} elseif( true == $boolIsCompanyLogosExist ) {
			$strHtmlForCompanyLogo = '<table cellpadding="0" cellspacing="0" border="0" style="width:100%; max-width: 580px;">
				<tbody>
				<tr>
					<td width="100%" max-width="580px" height="134" align="center">
						<img  src="' . $objCompanyLogo->getMediaLibraryFullSizeUri() . '" style="align:center;" >
					</td>
				</tr>
				</tbody>
			</table>';

			return $strHtmlForCompanyLogo;
		} else {
			$strHtmlForResidentPayImage = '<table cellpadding="0" cellspacing="0" border="0" style="width:100%; max-width: 580px;">
				<tbody>
				<tr>
					<td width="100%" max-width="580px" height="134" align="center">
						<img src="' . \CConfig::get( 'common_path' ) . '/images/email_images/residentpay.jpg" width="580" height="134" >
					</td>
				</tr>
				</tbody>
			</table>';

			return $strHtmlForResidentPayImage;
		}

	}

	public function getPropertyEmailHeaderImage() {

		$strPropertyEmailHeaderImage = $this->getPropertyEmailImage( CMediaType::PROPERTY_EMAIL_HEADER_IMAGE );

		return $strPropertyEmailHeaderImage;
	}

	public function getPropertyEmailFooterImage() {

		$strPropertyEmailFooterImage = $this->getPropertyEmailImage( CMediaType::PROPERTY_EMAIL_FOOTER_IMAGE );

		return $strPropertyEmailFooterImage;
	}

	public function getPropertyEmailImage( $intMediaTypeId, $boolIsUseLink = false ) {

		$arrobjPropertyMedias = CPropertyMedias::createService()->fetchPropertyMediasByPropertyIdByMediaTypeIdsByCid( $this->getId(), [ $intMediaTypeId ], $this->getCid(), $this->m_objDatabase );

		$strPropertyEmailImage	= '';

		if( true == valArr( $arrobjPropertyMedias ) ) {
			foreach( $arrobjPropertyMedias as $objPropertyMedia ) {
				$strUrl					 = ( true == valStr( $objPropertyMedia->getExternalUri() ) ) ? $objPropertyMedia->getExternalUri() : '#';
				if( true == $boolIsUseLink ) {
					$strPropertyEmailImage = '<img src="' . CConfig::get( 'media_library_path' ) . $objPropertyMedia->getFullsizeUri() . '" title="' . \Psi\CStringService::singleton()->htmlentities( $objPropertyMedia->getMediaAlt() ) . '"/>';
				} else {
					$strPropertyEmailImage = '<a target="_blank" href="' . $strUrl . '" border="0"><img style="display:block;" src="' . CConfig::get( 'media_library_path' ) . $objPropertyMedia->getFullsizeUri() . '" title="' . \Psi\CStringService::singleton()->htmlentities( $objPropertyMedia->getMediaAlt() ) . '"/></a>';
				}
			}
		}

		return $strPropertyEmailImage;
	}

	public function getMarketingMediaAssociationEmailImage( $intMarketingMediaSubTypeId, $boolIsUseLink = false ) {

		$objMarketingMediaAssociation = \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getId(), CMarketingMediaType::PROPERTY_MEDIA, $intMarketingMediaSubTypeId, $this->getCid(), $this->m_objDatabase );
		$strPropertyEmailImage	= '';

		if( true == valObj( $objMarketingMediaAssociation, 'CMarketingMediaAssociation' ) ) {
			$strUrl					 = ( true == valStr( $objMarketingMediaAssociation->getExternalUri() ) ) ? $objMarketingMediaAssociation->getExternalUri() : '#';
			if( true == $boolIsUseLink ) {
				$strPropertyEmailImage = '<img src="' . CConfig::get( 'media_library_path' ) . $objMarketingMediaAssociation->getMarketingStorageTypeReference() . '" title="' . \Psi\CStringService::singleton()->htmlentities( $objMarketingMediaAssociation->getMediaAlt() ) . '"/>';
			} else {
				$strPropertyEmailImage = '<a target="_blank" href="' . $strUrl . '" border="0"><img style="display:block;" src="' . CConfig::get( 'media_library_path' ) . $objMarketingMediaAssociation->getMarketingStorageTypeReference() . '" title="' . \Psi\CStringService::singleton()->htmlentities( $objMarketingMediaAssociation->getMediaAlt() ) . '"/></a>';
			}
		}

		return $strPropertyEmailImage;
	}

	public function loadVoipDatabase() {
		$this->m_objVoipDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::VOIP, CDatabaseUserType::PS_VOIP, CDatabaseServerType::POSTGRES, false );
		return $this->m_objVoipDatabase;
	}

	public function getProspectPortalBaseUrl() {
		$strProspectPortalBaseUrl = '';

		$arrmixPropertyWebsites = ( array ) CWebsites::createService()->fetchDefaultWebsiteByPropertyIdsByCid( [ $this->getId() ], $this->getCid(), $this->m_objDatabase );

		$strProspectPortalPrefix = ( true == defined( 'CONFIG_SECURE_HOST_PREFIX' ) && true == valStr( CONFIG_SECURE_HOST_PREFIX ) ) ? CONFIG_SECURE_HOST_PREFIX : 'https://';
		$strProspectPortalSuffix = ( true == defined( 'CONFIG_PROSPECT_PORTAL_SUFFIX' ) && true == valStr( CONFIG_PROSPECT_PORTAL_SUFFIX ) ) ? CONFIG_PROSPECT_PORTAL_SUFFIX : CConfig::get( 'prospectportal_domain_postfix' );

		if( true == valstr( $arrmixPropertyWebsites[$this->getId()]['community_website_uri'] ) ) {
			$strProspectPortalBaseUrl = $arrmixPropertyWebsites[$this->getId()]['community_website_uri'];
		} else if( false == valstr( $arrmixPropertyWebsites[$this->getId()]['community_website_uri'] ) && true == valstr( $arrmixPropertyWebsites[$this->getId()]['sub_domain'] ) ) {
			$strProspectPortalBaseUrl = $strProspectPortalPrefix . $arrmixPropertyWebsites[$this->getId()]['sub_domain'] . $strProspectPortalSuffix;
		}

		return $strProspectPortalBaseUrl;
	}

	public function getWebsiteUrl( $strPath, $strPropertyPreferenceKey = NULL ) {
		$strProspectPortalBaseUrl = $this->getProspectPortalBaseUrl();

		$strWebsiteUrl = ( true == valstr( $strProspectPortalBaseUrl ) ) ? $strProspectPortalBaseUrl . '/apartments/module/' . $strPath : '';

		$strPropertyPreferenceValue = CPropertyPreferences::createService()->fetchPropertyPreferenceValueByPropertyPreferenceKeyByPropertyIdByCid( $strPropertyPreferenceKey, $this->getId(), $this->getCid(), $this->m_objDatabase );

		if( true == valstr( $strPropertyPreferenceValue ) ) {
			$strWebsiteUrl = $strPropertyPreferenceValue;
		}

		$strWebsiteUrl = ( true == valstr( $strWebsiteUrl ) ) ? $strWebsiteUrl : 'javascript:void(0);';

		return $strWebsiteUrl;
	}

	public function getWebsiteAmenitiesUrl() {
		return $this->getWebsiteUrl( 'amenities', 'PROPERTY_DETAILS_COMMUNITY_WEBSITE_AMENITIES_URI' );
	}

	public function getWebsiteDirectionUrl() {
		return $this->getWebsiteUrl( 'map_and_directions', 'PROPERTY_DETAILS_COMMUNITY_WEBSITE_DIRECTIONS_URI' );
	}

	public function getWebsiteApplicationUrl() {
		return $this->getWebsiteUrl( 'application_authentication', 'PROPERTY_DETAILS_COMMUNITY_WEBSITE_APPLICATION_URI' );
	}

	public function getWebsiteCheckAvailabilityUrl() {
		return $this->getWebsiteUrl( 'check_availability', 'PROPERTY_DETAILS_COMMUNITY_WEBSITE_CHECK_AVAILABILITY_URI' );
	}

	public function getWebsiteContactUsUrl() {
		return $this->getWebsiteUrl( 'corporate_contact', 'PROPERTY_DETAILS_COMMUNITY_WEBSITE_CONTACT_US_URI' );
	}

	public function getWebsitePhotosUrl() {
		return $this->getWebsiteUrl( 'photos/property[id]/' . $this->getId() );
	}

	public function getCorporateWebsiteHomeUrl() {
		$strCorporateWebsiteHomeUrl = '';

		$objPropertyDetail = CPropertyDetails::createService()->fetchPropertyDetailByPropertyIdByCid( $this->getId(), $this->getCid(), $this->m_objDatabase );

		if( true == valObj( $objPropertyDetail, 'CPropertyDetail' ) && true == valStr( $objPropertyDetail->getCorporateWebsiteUri() ) ) {
			$strCorporateWebsiteHomeUrl = $objPropertyDetail->getCorporateWebsiteUri();
		}

		return ( true == valStr( $strCorporateWebsiteHomeUrl ) ) ? $strCorporateWebsiteHomeUrl : 'javascript:void(0);';
	}

	public function getWebsiteHomeUrl() {
		$strProspectPortalBaseUrl = $this->getProspectPortalBaseUrl();

		return ( true == valStr( $strProspectPortalBaseUrl ) ) ? $strProspectPortalBaseUrl : 'javascript:void(0);';
	}

	public function getContactUsGuestCard() {
		return $this->getWebsiteUrl( 'property_info/property[id]/' . $this->getId() . '/launch_guest_card/1' );
	}

	public function getWebsiteFloorPlan() {
		return $this->getWebsiteUrl( 'property_floorplans', 'PROPERTY_DETAILS_COMMUNITY_WEBSITE_FLOOR_PLANS_URI' );
	}

	public function getRwxLoginUrl() {
		$strSecureHostPrefix	= ( true == defined( CONFIG_SECURE_HOST_PREFIX ) ) ? CONFIG_SECURE_HOST_PREFIX : 'https://';
		$strRwxSuffix			= ( false == is_null( CConfig::get( 'rwx_login_suffix' ) ) ) ? CConfig::get( 'rwx_login_suffix' ) : '.entrata.com';
		$objClient              = CClients::fetchSimpleClientById( $this->getCid(), $this->m_objDatabase );
		$strRwxLoginUrl         = '';

		if( true == valObj( $objClient, 'CClient' ) ) {
			$strRwxLoginUrl = $strSecureHostPrefix . $objClient->getRwxDomain() . $strRwxSuffix;
		}
		return $strRwxLoginUrl;
	}

	public function getResidentInsureEnrollLink() {

		$strResidentInsurancePortalPrefix = ( true == defined( 'CONFIG_SECURE_HOST_PREFIX' ) && true == valStr( CONFIG_SECURE_HOST_PREFIX ) ) ? CONFIG_SECURE_HOST_PREFIX : 'https://';
		$strResidentInsurancePortalSuffix = ( true == defined( 'CONFIG_INSURANCE_PORTAL_SUFFIX' ) && true == valStr( CONFIG_INSURANCE_PORTAL_SUFFIX ) ) ? CONFIG_INSURANCE_PORTAL_SUFFIX : '.residentinsure.com';

		$objInsurancePortalDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::INSURANCE, CDatabaseUserType::PS_PROPERTYMANAGER );

		if( true == valObj( $objInsurancePortalDatabase, CDatabase::class ) ) {
			$objInsurancePortalDatabase->open();
			$objInsuranceCarrierProperty = \Psi\Eos\Insurance\CInsuranceCarrierProperties::createService()->fetchInsuranceCarrierPropertyByPropertyId( $this->getId(), $objInsurancePortalDatabase );
			$objInsurancePortalDatabase->close();
		}

		$strResidentInsureEnrollmentLinkUrl = $strResidentInsurancePortalPrefix . 'www.' . $strResidentInsurancePortalSuffix . '?module=home&action=view_home&source=' . urlencode( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( CPolicyLeadSourceType::UNINSURED_RESIDENT_EMAIL, CONFIG_KEY_ID ) );

		if( true == valObj( $objInsuranceCarrierProperty, CInsureanceCarrierProperty::class ) && true == valStr( $objInsuranceCarrierProperty->getSubDomain() ) ) {
			$strResidentInsureEnrollmentLinkUrl = $strResidentInsurancePortalPrefix . $objInsuranceCarrierProperty->getSubDomain() . '.' . $strResidentInsurancePortalSuffix . '?module=enroll&action=create_insurance_policy&source=' . urlencode( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( CPolicyLeadSourceType::UNINSURED_RESIDENT_EMAIL, CONFIG_KEY_ID ) );
		}
		return true == valStr( $strResidentInsureEnrollmentLinkUrl ) ? \Psi\CStringService::singleton()->htmlspecialchars( $strResidentInsureEnrollmentLinkUrl ) : 'javascript:void(0);';
	}

	public function getResidentPortalLoginUrl( $objDatabase = NULL ) {
		if( true == valObj( $objDatabase, 'CDatabase' ) ) {
			$this->setDatabase( $objDatabase );
		}

		$strResidentPortalLoginUrl = '';
		$objWebsiteDefault = CWebsites::createService()->fetchResidentPortalDefaultWebsiteByPropertyIdsByCid( [ $this->m_intId ], $this->m_intCid, $this->m_objDatabase, false, 1, true );
		$strResidentPortalPrefix    = ( \CConfig::get( 'secure_host_prefix' ) ? : 'https://' );
		$strResidentPortalSuffix    = ( \CConfig::get( 'resident_portal_suffix' ) ? : '.residentportal.com' );
		$strRp40SubDomain = $this->getRp40SubDomain();

		if( true == valStr( $strRp40SubDomain ) ) {
			$strResidentPortalLoginUrl  = $strResidentPortalPrefix . $strRp40SubDomain . $strResidentPortalSuffix . '/auth';
		} else {
			if( true === valObj( $objWebsiteDefault, 'CWebsite' ) ) {
				$strResidentPortalLoginUrl = $strResidentPortalPrefix . $objWebsiteDefault->getSubDomain() . $strResidentPortalSuffix . '/resident_portal/?module=authentication&action=view_login';
			} else {
				$arrobjWebsites  = ( array ) CWebsites::createService()->fetchPluralWebsitesByPropertyIdsByCid( [ $this->m_intId ], $this->m_intCid, $this->m_objDatabase );
				if( true == isset( $arrobjWebsites[0] ) && true == valStr( $arrobjWebsites[0]->getSubDomain() ) ) {
					$strResidentPortalLoginUrl  = $strResidentPortalPrefix . $arrobjWebsites[0]->getSubDomain() . $strResidentPortalSuffix . '/resident_portal/?module=authentication&action=view_login';
				}
			}
		}
		return $strResidentPortalLoginUrl;
	}

	public function getResidentPortalEnrollLink() {

		$objPropertyPreference	= CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'RESIDENT_PORTAL_AUTO_LOGIN_DOMAIN', $this->getId(), $this->m_intCid, $this->m_objDatabase );
		$arrobjWebsites  = ( array ) CWebsites::createService()->fetchPluralWebsitesByPropertyIdsByCid( [ $this->getId() ], $this->m_intCid, $this->m_objDatabase );
		$strResidentPortalPrefix = ( true == defined( 'CONFIG_SECURE_HOST_PREFIX' ) && true == valStr( CONFIG_SECURE_HOST_PREFIX ) ) ? CONFIG_SECURE_HOST_PREFIX : 'https://';
		$strResidentPortalSuffix = ( true == defined( 'CONFIG_RESIDENT_PORTAL_SUFFIX' ) && true == valStr( CONFIG_RESIDENT_PORTAL_SUFFIX ) ) ? CONFIG_RESIDENT_PORTAL_SUFFIX : '.residentportal.com';

		if( false == valArr( $arrobjWebsites ) ) {
			return 'javascript:void(0);';
		}

		// Single website association
		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrobjWebsites ) ) {
			foreach( $arrobjWebsites as $objWebsite ) {
				$strResidentEnrollmentDomain = $objWebsite->getSubDomain();
				if( 1 == $objWebsite->getResidentPortalService() ) {
					$strResidentEnrollmentLinkUrl = $strResidentPortalPrefix . $strResidentEnrollmentDomain . $strResidentPortalSuffix . '/auth';
				} else {
					$strResidentEnrollmentLinkUrl = $strResidentPortalPrefix . $strResidentEnrollmentDomain . $strResidentPortalSuffix . '/';
					$strResidentEnrollmentLinkUrl .= 'resident_portal/?module=authentication&show_enroll/';
				}
				return true == valStr( $strResidentEnrollmentLinkUrl ) ? $strResidentEnrollmentLinkUrl : 'javascript:void(0);';
			}
		} else {
			// Multiple website association
			$boolAllResponsive = true;
			$boolAllRPEnabled = true;
			foreach( $arrobjWebsites as $objWebsite ) {

				if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && $objPropertyPreference->getValue() == $objWebsite->getSubDomain() ) {

					// With default website
					$strResidentEnrollmentDomain = $objWebsite->getSubDomain();
					if( 1 == $objWebsite->getResidentPortalService() ) {
						$strResidentEnrollmentLinkUrl = $strResidentPortalPrefix . $strResidentEnrollmentDomain . $strResidentPortalSuffix . '/auth';
					} else {
						$strResidentEnrollmentLinkUrl = $strResidentPortalPrefix . $strResidentEnrollmentDomain . $strResidentPortalSuffix . '/';
						$strResidentEnrollmentLinkUrl .= 'resident_portal/?module=authentication&show_enroll/';
					}
					return true == valStr( $strResidentEnrollmentLinkUrl ) ? $strResidentEnrollmentLinkUrl : 'javascript:void(0);';
				} else {

					// Without default website
					if( 1 == $objWebsite->getResidentPortalService() ) {
						$boolAllResponsive = false;
					} else {
						$boolAllRPEnabled = false;
					}
				}
			}
			$objDefaultWebsite = CWebsites::createService()->fetchResidentPortalDefaultWebsiteByPropertyIdsByCid( [ $this->getId() ], $this->m_intCid, $this->m_objDatabase );
			$strResidentEnrollmentDomain = $objDefaultWebsite->getSubDomain();
			if( false == $boolAllRPEnabled && false == $boolAllResponsive ) {
				$strResidentEnrollmentLinkUrl = $strResidentPortalPrefix . $strResidentEnrollmentDomain . $strResidentPortalSuffix . '/auth';
			} else if( true == $boolAllRPEnabled ) {
				$strResidentEnrollmentLinkUrl = $strResidentPortalPrefix . $strResidentEnrollmentDomain . $strResidentPortalSuffix . '/auth';
			} else {
				$strResidentEnrollmentLinkUrl = $strResidentPortalPrefix . $strResidentEnrollmentDomain . $strResidentPortalSuffix . '/';
				$strResidentEnrollmentLinkUrl .= 'resident_portal/?module=authentication&show_enroll/';
			}
			return true == valStr( $strResidentEnrollmentLinkUrl ) ? $strResidentEnrollmentLinkUrl : 'javascript:void(0);';
		}
	}

	public function getIsRp40Enabled( $objDatabase = NULL ) {
		if( true == valObj( $objDatabase, 'CDatabase' ) ) {
			$this->setDatabase( $objDatabase );
		}
		$objPropertyPreference	= CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'RESIDENT_PORTAL_AUTO_LOGIN_DOMAIN', $this->getId(), $this->m_intCid, $this->m_objDatabase );
		$arrobjWebsites  = ( array ) CWebsites::createService()->fetchPluralWebsitesByPropertyIdsByCid( [ $this->getId() ], $this->m_intCid, $this->m_objDatabase );

		// Single website association
		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrobjWebsites ) ) {
			foreach( $arrobjWebsites as $objWebsite ) {
				if( 1 == $objWebsite->getResidentPortalService() ) {
					return $objWebsite->getSubDomain();
				} else {
					return false;
				}
			}
		} else {
			// Multiple website association
			$boolAllResponsive = true;
			$boolAllRPEnabled = true;
			$strSubdomain = '';
			foreach( $arrobjWebsites as $objWebsite ) {

				if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && $objPropertyPreference->getValue() == $objWebsite->getSubDomain() ) {

					// With default website
					if( 1 == $objWebsite->getResidentPortalService() ) {
						return $objWebsite->getSubDomain();
					} else {
						return false;
					}
				} else {

					// Without default website
					if( 1 == $objWebsite->getResidentPortalService() ) {
						$boolAllResponsive = false;
						$strSubdomain = $objWebsite->getSubDomain();
					} else {
						$boolAllRPEnabled = false;
					}
				}
			}
			if( false == $boolAllRPEnabled && false == $boolAllResponsive ) {
				return $strSubdomain;
			} else if( true == $boolAllRPEnabled ) {
				return $strSubdomain;
			} else {
				return false;
			}
		}
	}

	public function getRp40SubDomain( $objDatabase = NULL ) {
		if( true == valObj( $objDatabase, 'CDatabase' ) ) {
			$this->setDatabase( $objDatabase );
		}
		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'RESIDENT_PORTAL_AUTO_LOGIN_DOMAIN', $this->getId(), $this->m_intCid, $this->m_objDatabase );
		$arrobjWebsites = ( array ) CWebsites::createService()->fetchPluralWebsitesByPropertyIdsByCid( [ $this->getId() ], $this->m_intCid, $this->m_objDatabase );

		if( false == valArr( $arrobjWebsites ) ) return NULL;

		// Single website association
		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrobjWebsites ) ) {
			foreach( $arrobjWebsites as $objWebsite ) {
				if( 1 == $objWebsite->getResidentPortalService() ) {
					return $objWebsite->getSubDomain();
				} else {
					return NULL;
				}
			}
		} else {
			// Multiple website association
			$boolAllResponsive = true;
			$boolAllRPEnabled = true;
			$strSubdomain = '';
			foreach( $arrobjWebsites as $objWebsite ) {

				if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && $objPropertyPreference->getValue() == $objWebsite->getSubDomain() ) {

					// With default website
					if( 1 == $objWebsite->getResidentPortalService() ) {
						return $objWebsite->getSubDomain();
					} else {
						return NULL;
					}
				} else {

					// Without default website
					if( 1 == $objWebsite->getResidentPortalService() ) {
						$boolAllResponsive = false;
						$strSubdomain = $objWebsite->getSubDomain();
					} else {
						$boolAllRPEnabled = false;
					}
				}
			}
			if( ( false == $boolAllRPEnabled && false == $boolAllResponsive ) || true == $boolAllRPEnabled ) {
				return $strSubdomain;
			} else {
				return NULL;
			}
		}
	}

	public function getIsRp40EnabledOnCurrentWebsite( $strSecureBaseName, $objDatabase = NULL ) {
		if( false == valStr( $strSecureBaseName ) ) {
			return false;
		}
		if( true == valObj( $objDatabase, 'CDatabase' ) ) {
			$this->setDatabase( $objDatabase );
		}

		$strParsedUrl = parse_url( $strSecureBaseName );
		$strHost = explode( '.', $strParsedUrl['host'] );
		$strSubDomain = $strHost[0];

		if( true == valStr( $strSubDomain ) ) {
			$objWebsite = CWebsites::createService()->fetchWebsitePreferenceResidentPortalServiceBySubDomain( $strSubDomain, $this->m_objDatabase );
			if( true == valObj( $objWebsite, 'CWebsite' ) && 1 == $objWebsite->getResidentPortalService() ) {
				return true;
			}
		}
		return false;
	}

	public function getPropertyHours() {

		$arrobjPropertyHours = CPropertyHours::fetchPropertyHoursWithLunchHoursByPropertyIdByCid( $this->getId(), $this->m_intCid, $this->m_objDatabase );

		$arrstrDayOfWeek = [ '1' => __( 'Mon' ), '2' => __( 'Tue' ), '3' => __( 'Wed' ), '4' => __( 'Thu' ), '5' => __( 'Fri' ), '6' => __( 'Sat' ), '7' => __( 'Sun' ) ];
		$arrobjPropertyHours = rekeyObjects( 'Day', $arrobjPropertyHours );

		$strPropertyHours = '<br><label><b>' . __( 'Our Office Hours : ' ) . '</b></label><br>';

		foreach( $arrstrDayOfWeek as $strDayOfWeek => $strDayNameOfWeek ) {
			if( true == empty( $arrobjPropertyHours[$strDayOfWeek] ) ) {
				$objPropertiesClosedHours = new CPropertyHour();
				$objPropertiesClosedHours->setDay( $strDayOfWeek );
				$arrobjPropertyHours[$strDayOfWeek] = $objPropertiesClosedHours;
			}
		}
		ksort( $arrobjPropertyHours );

		$strPropertyHours .= '<table style="width:100%; text-align:left; font-size: 1em;">
						<tr style="background: #707070 none repeat scroll 0 0; color: #fff; padding: 10px 8px;">
							<th width="20%" style="padding-left:5px;border: 1px solid white">' . __( 'DAY' ) . '</th>
							<th width="30%" style="padding-left:5px;border: 1px solid white">' . __( 'HOURS' ) . '</th>
							<th width="30%" style="padding-left:5px;border: 1px solid white">' . __( 'LUNCH TIME' ) . '</th></tr>';

		foreach( $arrobjPropertyHours as $objPropertyHour ) {

			if( true == valStr( $objPropertyHour->getOpenTime() ) ) {
				$strOfficeOpenTime = __( '{%t, 0, TIME_SHORT}', [ $objPropertyHour->getOpenTime() ] );
				if( true == valStr( $strOfficeOpenTime ) ) {
					$arrmixTime = explode( ' ', $strOfficeOpenTime );
					$strOfficeOpenTime = $arrmixTime[0] . ' ' . $arrmixTime[1];
				}

				$strOfficeCloseTime = __( '{%t, 0, TIME_SHORT}', [ $objPropertyHour->getCloseTime() ] );

				if( true == valStr( $strOfficeCloseTime ) ) {
					$arrmixTime = explode( ' ', $strOfficeCloseTime );
					$strOfficeCloseTime = $arrmixTime[0] . ' ' . $arrmixTime[1];
				}

				$strOfficeHours = $strOfficeOpenTime . ' - ' . $strOfficeCloseTime;
			} else {
				$strOfficeHours = __( 'Closed' );
			}

			if( 1 == $objPropertyHour->getByAppointmentOnly() ) {
				$strOfficeHours = __( 'By Appointment Only' );
			}

			if( true == valStr( $objPropertyHour->getLunchStartTime() ) ) {
				$strLunchStartTime = __( '{%t, 0, TIME_SHORT}', [ $objPropertyHour->getLunchStartTime() ] );

				if( true == valStr( $strLunchStartTime ) ) {
					$arrmixTime = explode( ' ', $strLunchStartTime );
					$strLunchStartTime = $arrmixTime[0] . ' ' . $arrmixTime[1];
				}

				$strLunchEndTime = __( '{%t, 0, TIME_SHORT}', [ $objPropertyHour->getLunchEndTime() ] );

				if( true == valStr( $strLunchEndTime ) ) {
					$arrmixTime = explode( ' ', $strLunchEndTime );
					$strLunchEndTime = $arrmixTime[0] . ' ' . $arrmixTime[1];
				}

				$strLunchHours = $strLunchStartTime . ' - ' . $strLunchEndTime;
			} else {
				$strLunchHours = '-';
			}

			$strPropertyHours .= '<tr style="background:#f1f1f1; border-bottom:1px solid #dfdfdf;">
						<td style="padding-left:5px; border: 1px solid white">' . $arrstrDayOfWeek[$objPropertyHour->getDay()] . '</td>
						<td style="padding-left:5px; border: 1px solid white">' . $strOfficeHours . '</td>
						<td style="padding-left:5px; border: 1px solid white">' . $strLunchHours . '</td>
					</tr>';
		}
		$strPropertyHours .= '</table>';

		return $strPropertyHours;
	}

	public function getCurrentDate() {
		$objPropertyTimeZone = $this->fetchTimeZone( $this->m_objDatabase );
		$strCurrentDate = '';
		if( true == valObj( $objPropertyTimeZone, 'CTimeZone' ) ) {
			$strCurrentDate = date( 'Y-m-d', strtotime( getConvertedDateTime( date( 'M d, Y h:m A' ), $strFormat = 'M d, Y h:m A', 'America/Denver', $objPropertyTimeZone->getTimeZoneName() ) ) );
		}

		return __( '{%t, 0, DATE_NUMERIC_STANDARD}', [ $strCurrentDate ] );

	}

	public function getCurrentDateTime() {
		$objPropertyTimeZone = $this->fetchTimeZone( $this->m_objDatabase );
		$strCurrentDateByTimeZone = '';
		if( true == valObj( $objPropertyTimeZone, 'CTimeZone' ) ) {
			$strCurrentDateByTimeZone = date( 'M d, Y h:i A', strtotime( getConvertedDateTime( date( 'M d, Y h:i A' ), $strFormat = 'M d, Y h:i A', 'America/Denver', $objPropertyTimeZone->getTimeZoneName() ) ) ) . ' ' . $objPropertyTimeZone->getTimeZoneName();
		}

		return __( '{%t, 0, DATETIME_ALPHA_MEDIUM}', [ $strCurrentDateByTimeZone ] );
	}

	public function getAmenityPhoto() {
		if( false == valStr( $this->getSystemMessageEmailContent() ) ) return NULL;

		$arrmixAmenityPhotoSpansByAmenityIds = [];

		preg_match_all( '/<span data-property_amenity=.*?<\/span>/is', $this->getSystemMessageEmailContent(), $arrstrMatchedAmenityPhotoHtmlSpans );
		$arrstrAmenityPhotoMergeFieldHtmlContents = $arrstrMatchedAmenityPhotoHtmlSpans[0];

		$arrintArOriginReferenceIds = [];
		$arrmixAmenityPhotos		= [];

		if( true == valArr( $arrstrAmenityPhotoMergeFieldHtmlContents ) ) {

			foreach( $arrstrAmenityPhotoMergeFieldHtmlContents as $strAmenityPhotoMergeFieldSpan ) {
				$arrstrAmenityMergeFieldHtmlContents	= explode( '<span data-property_amenity="', $strAmenityPhotoMergeFieldSpan );
				$arrstrDataAttributes					= explode( '"', $arrstrAmenityMergeFieldHtmlContents[1] );
				$arrintPropertyAmenityIds				= explode( '_', $arrstrDataAttributes[0] );

				$arrintArOriginReferenceIds[] = $arrintPropertyAmenityIds[1];

				$arrmixAmenityPhotoSpansByAmenityIds[$arrintPropertyAmenityIds[1]][] = $strAmenityPhotoMergeFieldSpan;
				$arrmixAmenityPhotos[$strAmenityPhotoMergeFieldSpan] = '';
			}

			$arrmixMarketingMediaAssociations = \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchMarketingMediaAssociationsByArOriginIdByArOriginReferenceIdsByArCascadeIdByPropertyIdByCid( CArOrigin::AMENITY, array_unique( $arrintArOriginReferenceIds ), CArCascade::PROPERTY, $this->getId(), $this->m_intCid, $this->m_objDatabase );

			if( true == valArr( $arrmixAmenityPhotoSpansByAmenityIds ) ) {

				if( true == valArr( $arrmixMarketingMediaAssociations ) ) {
					foreach( $arrmixMarketingMediaAssociations as $arrmixMarketingMediaAssociation ) {

						$arrstrAmenityPhotoSpansByAmenityIds	= $arrmixAmenityPhotoSpansByAmenityIds[$arrmixMarketingMediaAssociation['ar_origin_reference_id']];
						$strAmenityPhoto						= '<img src="' . CConfig::get( 'media_library_path' ) . $arrmixMarketingMediaAssociation['marketing_storage_type_reference'] . '" title="' . $arrmixMarketingMediaAssociation['media_alt'] . '"/>';

						if( true == valArr( $arrstrAmenityPhotoSpansByAmenityIds ) ) {
							foreach( $arrstrAmenityPhotoSpansByAmenityIds as $strAmenityPhotoSpan ) {
								$arrmixAmenityPhotos[$strAmenityPhotoSpan] = $strAmenityPhoto;
							}
						}
					}
				}
			}
		}

		return $arrmixAmenityPhotos;
	}

	public function getLeadSourceVanityNumber() {
		if( false == valStr( $this->getSystemMessageEmailContent() ) ) return NULL;

		$arrmixLeadSourceVanityNumber = [];

		preg_match_all( '/<span data-lead-source-id=.*?<\/span>/is', $this->getSystemMessageEmailContent(), $arrstrMatchedLeadSourceHtmlSpans );

		$arrstrVanityNumberMergeFieldSpans = $arrstrMatchedLeadSourceHtmlSpans[0];

		$arrobjLeadSources = ( array ) \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPropertyLeadSourcesWithNameByPropertyIdByCid( $this->getId(), $this->m_intCid, $this->m_objDatabase );

		$objPropertyOfficePhoneNumber = CPropertyPhoneNumbers::fetchPropertyPhoneNumberByPropertyIdByPhoneNumberTypeIdByCid( $this->getId(), CPhoneNumberType::OFFICE, $this->m_intCid, $this->m_objDatabase );

		$strOfficePhoneNumber = '';

		if( true == valObj( $objPropertyOfficePhoneNumber, 'CPropertyPhoneNumber' ) ) {
			$strOfficePhoneNumber = $objPropertyOfficePhoneNumber->getPhoneNumber();
		}

		foreach( $arrobjLeadSources as $objLeadSource ) {
			$strVanityNumber = $objLeadSource->getPhoneNumber();

			if( false == valStr( $strVanityNumber ) ) {
				$strVanityNumber = $strOfficePhoneNumber;
			}

			$arrmixPropertyLeadSources[$objLeadSource->getLeadSourceId()] = __( '{%h, 0}', [ $strVanityNumber ] );
		}

		if( true == valArr( $arrstrVanityNumberMergeFieldSpans ) ) {
			foreach( $arrstrVanityNumberMergeFieldSpans as $strVanityNumberMergeFieldSpan ) {

				$arrstrChunkedData		= explode( '<span data-lead-source-id="', $strVanityNumberMergeFieldSpan );
				$arrstrDataAttributes	= explode( '"', $arrstrChunkedData[1] );
				$intLeadSourceId		= $arrstrDataAttributes[0];

				$arrmixLeadSourceVanityNumber[$strVanityNumberMergeFieldSpan] = '';

				if( false == valArr( $arrmixPropertyLeadSources ) || false == array_key_exists( $intLeadSourceId, $arrmixPropertyLeadSources ) || '' == trim( $arrmixPropertyLeadSources[$intLeadSourceId] ) ) {
					$arrmixLeadSourceVanityNumber[$strVanityNumberMergeFieldSpan] = $strOfficePhoneNumber;
				} else {
					$arrmixLeadSourceVanityNumber[$strVanityNumberMergeFieldSpan] = $arrmixPropertyLeadSources[$intLeadSourceId];
				}
			}

		}

		return $arrmixLeadSourceVanityNumber;
	}

	public function getCompanyOrPropertyHeaderImage() {
		$strCompanyOrPropertyHeaderImage = $this->getCompanyOrPropertyHeader();
		if( false == valStr( $strCompanyOrPropertyHeaderImage ) ) {
			$strCompanyOrPropertyHeaderImage = '<div id="topbar" >
											<table cellpadding="0" cellspacing="0" border="0" width="580" style="width:580px;" bgcolor="#bb3a26">
												<tr>
													<td width="580" height="134">
														<img src="' . CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'prospect_portal_mouse.gif" width="580" height="134" >
													</td>
												</tr>
											</table>
										</div>';
		}

		return $strCompanyOrPropertyHeaderImage;
	}

	public function getCompanyOrPropertyHeader() {
		$strCompanyOrPropertyHeaderImage = '';

		$objMarketingMediaAssociation	= \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getId(), CMarketingMediaType::PROPERTY_MEDIA, CMarketingMediaSubType::PROPERTY_LOGO, $this->getCid(), $this->m_objDatabase );
		$objLogoMarketingMediaAssociation = \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getCId(), CMarketingMediaType::COMPANY_PREFERENCES_DEFAULT_IMAGE, CMarketingMediaSubType::COMPANY_LOGO, $this->getCId(), $this->m_objDatabase);
		$boolIsCustomerPasswordReset = $this->getRequiredParameters()['is_customer_password_reset'] ?? NULL;
		$boolIsRequiredNewVersion = $this->getRequiredParameters()['is_required_new_version'] ?? NULL;

		if( false == isset( $boolIsCustomerPasswordReset ) && true == valObj( $objMarketingMediaAssociation, CMarketingMediaAssociation::class ) && true == valObj( $objLogoMarketingMediaAssociation, CMarketingMediaAssociation::class ) ) {
			$strCompanyOrPropertyHeaderImage = '<div style="margin-bottom:10px;">
												<table cellpadding="0" cellspacing="0" border="0" width="580" style="width:580px;">
													<tr>
														<td align="left">
															<img src="' . $objMarketingMediaAssociation->getMediaLibraryFullSizeUri() . '" style="align:left;" >
														</td>
														<td align="right">
															<img src="' . $objLogoMarketingMediaAssociation->getMediaLibraryFullSizeUri() . '" style="align:left;" >
														</td>
													</tr>
												</table>
											</div>';

		} else if( true == $boolIsCustomerPasswordReset || false == valObj( $objMarketingMediaAssociation, CMarketingMediaAssociation::class ) || false == valObj( $objLogoMarketingMediaAssociation, CMarketingMediaAssociation::class ) ) {
			$strImageTag = '';
			if( true == $boolIsRequiredNewVersion ) {
				if( true == valObj( $objLogoMarketingMediaAssociation, CMarketingMediaAssociation::class ) ) {
					$strImageTag = '<table cellpadding="0" cellspacing="0" border="0" width="580" style="width:580px;"><tr><td width="580" height="134" align="left"><img src="' . $objLogoMarketingMediaAssociation->getMediaLibraryFullSizeUri() . '" style="align:right;" ></td></tr></table>';
					if( true == $boolIsCustomerPasswordReset ) {
						$strImageTag = '<table><tr><td style="width:200px;" class="responsive"><img src="' . $objLogoMarketingMediaAssociation->getMediaLibraryFullSizeUri() . '" style="align:left;" ></td></tr></table>';
					}
				} elseif( true == valObj( $objMarketingMediaAssociation, CMarketingMediaAssociation::class ) ) {
					$strImageTag = '<table cellpadding="0" cellspacing="0" border="0" width="580" style="width:580px;"><tr><td width="580" height="134" align="left"><img src="' . $objMarketingMediaAssociation->getMediaLibraryFullSizeUri() . '" style="align:left;" ></td></tr></table>';
					if( true == $boolIsCustomerPasswordReset ) {
						$strImageTag = '<table><tr><td style="width:200px;" class="responsive"><img src="' . $objMarketingMediaAssociation->getMediaLibraryFullSizeUri() . '" style="align:left;" ></td></tr></table>';

					}
				}
			} else {
				if( true == valObj( $objMarketingMediaAssociation, CMarketingMediaAssociation::class ) ) {
					$strImageTag = '<table cellpadding="0" cellspacing="0" border="0" width="580" style="width:580px;"><tr><td width="580" height="134" align="left"><img src="' . $objMarketingMediaAssociation->getMediaLibraryFullSizeUri() . '" style="align:left;" ></td></tr></table>';
					if( true == $boolIsCustomerPasswordReset ) {
						$strImageTag = '<table><tr><td style="width:200px;" class="responsive"><img src="' . $objMarketingMediaAssociation->getMediaLibraryFullSizeUri() . '" style="align:left;" ></td></tr></table>';

					}
				} elseif( true == valObj( $objLogoMarketingMediaAssociation, CMarketingMediaAssociation::class ) ) {
					$strImageTag = '<table cellpadding="0" cellspacing="0" border="0" width="580" style="width:580px;"><tr><td width="580" height="134" align="left"><img src="' . $objLogoMarketingMediaAssociation->getMediaLibraryFullSizeUri() . '" style="align:right;" ></td></tr></table>';
					if( true == $boolIsCustomerPasswordReset ) {
						$strImageTag = '<table><tr><td style="width:200px;" class="responsive"><img src="' . $objLogoMarketingMediaAssociation->getMediaLibraryFullSizeUri() . '" style="align:left;" ></td></tr></table>';
					}
				}
			}

			if( true == valStr( $strImageTag ) ) {
				$strCompanyOrPropertyHeaderImage = '<div> ' . $strImageTag . ' </div>';
			}
		}
		return $strCompanyOrPropertyHeaderImage;
	}

	public function getCompanyOrPropertyHeaderImageOrResidentPortalImage() {
		$strCompanyOrPropertyHeaderImage = $this->getCompanyOrPropertyHeader();

		if( false == valStr( $strCompanyOrPropertyHeaderImage ) ) {
			$boolIsCustomerPasswordReset = $this->getRequiredParameters()['is_customer_password_reset'] ?? NULL;
			if( true == $boolIsCustomerPasswordReset ) {
				$boolIsTenant = $this->getRequiredParameters()['is_tenant'] ?? true;
				if( true == $boolIsTenant ) {
					return '';
				}
				$boolIsRequiredNewVersion = $this->getRequiredParameters()['is_required_new_version'] ?? NULL;

				if( false == $boolIsRequiredNewVersion ) {
					$strImageSrc = '<img src="' . CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'residentportal.jpg" style="width:580px">';

				} else {
					$boolIsSendToGroup = $this->getRequiredParameters()['is_send_to_group'] ?? NULL;
					if( true == $boolIsSendToGroup ) {
						$strImageSrc = '<img src="' . CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'gp-icon-192.png" style="width:100px">';
					} else {
						$strImageSrc = '<img src="' . CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'rp-icon-512.png" style="width:100px">';
					}
				}

				$strCompanyOrPropertyHeaderImage = '<div id="topbar" >
									<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td style="width:200px;" class="responsive">
												' . $strImageSrc . '
											</td>
										</tr>
									</table>
								</div>';
			} else {
				$strCompanyOrPropertyHeaderImage = '<div id="topbar" >
										<table cellpadding="0" cellspacing="0" border="0" width="580" style="width:580px;" bgcolor="#bb3a26">
											<tr>
												<td width="580" height="134">
													<img src="' . CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'residentportal.jpg" width="580" height="134" >
												</td>
											</tr>
										</table>
									</div>';
			}
		}

		return $strCompanyOrPropertyHeaderImage;

	}

	public function getCompanyOrPropertyHeaderImageForLeasingCenter() {
		$objMarketingMediaAssociation	= \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getId(), CMarketingMediaType::PROPERTY_MEDIA, CMarketingMediaSubType::PROPERTY_LOGO, $this->getCid(), $this->m_objDatabase );

		if( true == valObj( $objMarketingMediaAssociation, CMarketingMediaAssociation::class ) ) {
			return '<img src="' . $objMarketingMediaAssociation->getMediaLibraryFullSizeUri() . '" style="margin: 0;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;height: 50px;width: auto;">';
		}

		$objLogoMarketingMediaAssociation = \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getCId(), CMarketingMediaType::COMPANY_PREFERENCES_DEFAULT_IMAGE, CMarketingMediaSubType::COMPANY_LOGO, $this->getCId(), $this->m_objDatabase);
		if( true == valObj( $objLogoMarketingMediaAssociation, CMarketingMediaAssociation::class ) ) {
			return '<img src="' . $objLogoMarketingMediaAssociation->getMediaLibraryFullSizeUri() . '" style="margin: 0;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;height: 50px;width: auto;">';
		}
		return '';
	}

	public function getCompanyOrPropertyFooterText() {

		$boolIsCompanyOrPropertyLogoLoaded 	= false;
		$strSpanContent 					= '';
		$strCompanyOrPropertyFooterText = '';
		$objMarketingMediaAssociation	= \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getId(), CMarketingMediaType::PROPERTY_MEDIA, CMarketingMediaSubType::PROPERTY_LOGO, $this->getCid(), $this->m_objDatabase );

		$objLogoMarketingMediaAssociation = \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getCId(), CMarketingMediaType::COMPANY_PREFERENCES_DEFAULT_IMAGE, CMarketingMediaSubType::COMPANY_LOGO, $this->getCId(), $this->m_objDatabase);

		if( true == valObj( $objMarketingMediaAssociation, 'CMarketingMediaAssociation' ) || true == valObj( $objLogoMarketingMediaAssociation, 'CMarketingMediaAssociation' ) ) {
			$boolIsCompanyOrPropertyLogoLoaded = true;
		}

		if( false == $boolIsCompanyOrPropertyLogoLoaded ) {
			$strSpanContent = '<span style="font:normal normal normal 10px/15px arial;color:#fff;"> ' . __( 'Copyright {%t, 0, DATE_NUMERIC_YEAR } &nbsp; {%s, 1}. All rights reserved.', [ date( 'Y-m-d' ), CONFIG_COMPANY_NAME_FULL ] ) . ' </span>';
			$strCompanyOrPropertyFooterText = '<div id="footer" >
												<table cellpadding="0" cellspacing="0" border="0" width="580" style="width:580px;">
													<tr>
														<td bgcolor="#bb3a26" align="center" style="padding:5px 15px 5px 15px; font:normal normal normal 11px/18px arial;color:#808080;" height="25px">
															' . $strSpanContent . '
														</td>
													</tr>
												</table>
											</div>';
		}

		return $strCompanyOrPropertyFooterText;

	}

	public function fetchActivePropertyDocumentsByDocumentTypeIdByDocumentSubTypeIdByDocumentAssociationTypeIdBySystemCodeByPropertyIdsByCid( $intDocumentTypeId, $intDocumentSubTypeId, $intDocumentAssociationTypeId, $strFileTypeSystemCode, $strPropertyIds, $intCid, $objDatabase ) {
		return CDocuments::fetchActivePropertyDocumentsByDocumentTypeIdByDocumentSubTypeIdByDocumentAssociationTypeIdBySystemCodeByPropertyIdsByCid( $intDocumentTypeId, $intDocumentSubTypeId, $intDocumentAssociationTypeId, $strFileTypeSystemCode, $strPropertyIds, $intCid, $objDatabase );
	}

	public function fetchActiveDocumentsByDocumentTypeIdByDocumentSubTypeIdByDocumentAssociationTypeIdBySystemCodeByPropertyIdsByCid( $intDocumentTypeId, $intDocumentSubTypeId, $intDocumentAssociationTypeId, $strFileTypeSystemCode, $strPropertyIds, $intCid, $objDatabase ) {
		return CDocuments::fetchActiveDocumentsByDocumentTypeIdByDocumentSubTypeIdByDocumentAssociationTypeIdBySystemCodeByPropertyIdsByCid( $intDocumentTypeId, $intDocumentSubTypeId, $intDocumentAssociationTypeId, $strFileTypeSystemCode, $strPropertyIds, $intCid, $objDatabase );
	}

	public function fetchCustomizedPropertyOfficeHours( $intPropertyId, $intCid, $objDatabase, $boolIsRequiredVersionForAppointment ) {
		$arrmixOfficeHours = [];
		$arrobjPropertyHours = CPropertyHours::fetchPropertyOfficeHoursKeyedByWeekdayByTypeIdByPropertyIdByCid( [ CPropertyHourType::OFFICE_HOURS ], $intPropertyId, $intCid, $objDatabase );
		if( true == valArr( $arrobjPropertyHours ) ) {
			foreach( $arrobjPropertyHours as $strDOW => $objPropertyHour ) {
				if( NULL == $objPropertyHour->getOpenTime() || NULL == $objPropertyHour->getCloseTime() ) {
					if( true == $boolIsRequiredVersionForAppointment && true == $objPropertyHour->getByAppointmentOnly() ) {
						$arrmixOfficeHours[$strDOW] = 'By Appointment';
					} else {
						continue;
					}
				} else {
					$arrmixOfficeHours[$strDOW] = [ 'open' => $objPropertyHour->getOpenTime(), 'close' => $objPropertyHour->getCloseTime() ];
				}
			}
		}
		return $arrmixOfficeHours;
	}

	public function getEqualHousingOpportunityLogo() {
		$strEqualHousingOpportunityLogo = '';
		$arrmixPropertyPreferences		= CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByValueByPropertyIdsByCid( 'EQUAL_HOUSING_OPPORTUNITY_LOGO', '1', [ $this->getId() ], $this->m_intCid, $this->m_objDatabase );

		if( true == valArr( $arrmixPropertyPreferences ) && \Psi\Libraries\UtilFunctions\count( $arrmixPropertyPreferences ) > 0 ) {
			$strEqualHousingOpportunityLogo = '<img src="' . CONFIG_COMMON_PATH . '/images/social_icons/message_center/fheo50.gif">';
		}

		return $strEqualHousingOpportunityLogo;
	}

	public function getPropertyDrivingDirections() {
		$strDrivingDirections = $this->getDrivingDirections();
		if( true == valStr( $strDrivingDirections ) ) {
			return $strDrivingDirections;
		}

		return '-';
	}

	public function partiallyCompleteHeaderText() {
		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferenceByPropertyIdByKeyByCid( $this->getId(), [ 'PARTIALLY_COMPLETED_APPLICATION_HEADER_CUSTOM_TEXT' ], $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {
			return '';
		}

		return $objPropertyPreference->getValue();
	}

	public function partiallyCompleteFooterText() {
		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferenceByPropertyIdByKeyByCid( $this->getId(), [ 'PARTIALLY_COMPLETED_APPLICATION_FOOTER_CUSTOM_TEXT' ], $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {
			return '';
		}

		return $objPropertyPreference->getValue();
	}

	public function getCompanyNameFull() {
		return '<strong>' . CONFIG_COMPANY_NAME_FULL . '</strong>';
	}

	public function getAssociatedAccountId() {
		return $this->m_intAssociatedAccountId;
	}

	public function fetchMarketingMediaById( $intMarketingMediaId, $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMedias::createService()->fetchMarketingMediaByIdByCid( $intMarketingMediaId, $this->getCid(), $objDatabase );
	}

	public function fetchMarketingMediaAssociationById( $intMarketingMediaAssociationId, $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchMarketingMediaAssociationByIdByCid( $intMarketingMediaAssociationId, $this->getCid(), $objDatabase );
	}

	public function fetchActiveMarketingMediaAssociationsByMediaSubTypeId( $intMarketingMediaSubType, $objDatabase, $boolIsSingleAssociation ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getId(), CMarketingMediaType::PROPERTY_MEDIA, $intMarketingMediaSubType, $this->getCid(), $objDatabase, $boolIsSingleAssociation );
	}

	public function fetchActiveMarketingMediaAssociationsByMediaTypeIdByMediaSubTypeIds( $intMarketingMediaTypeId, $arrintMarketingMediaSubTypeIds, $objDatabase, $boolIsSingleAssociation = false ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsByReferenceIdByMediaTypeIdByMediaSubTypeIdsByCidOrderByOrderNum( $this->getId(), $intMarketingMediaTypeId, $arrintMarketingMediaSubTypeIds, $this->getCid(), $objDatabase, $boolIsSingleAssociation );
	}

	public function fetchMarketingMediaAssociationOrderNumByMediaSubTypeIds( $arrintMarketingMediaSubTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchMarketingMediaAssociationOrderNumByMediaTypeIdByMediaSubTypeIds( CMarketingMediaType::PROPERTY_MEDIA, $arrintMarketingMediaSubTypeIds, $objDatabase );
	}

	public function getLeaseExecutedNotificationApplicantContent() {
		return $this->getPropertyPreferenceData( 'LEASE_EXECUTED_NOTIFICATION_APPLICANT_CONTENT' );
	}

	public function getLeaseExecutedNotificationToRenewingContent() {
		return $this->getPropertyPreferenceData( 'LEASE_EXECUTED_NOTIFICATION_TO_RENEWING_RESIDENT' );
	}

	public function getPropertyPreferenceData( $strPropertyPreferenceKey ) {
		$strHtmlContent 		= '';
		$objPropertyPreference 	= CPropertyPreferences::createService()->fetchPropertyPreferenceByPropertyIdByKeyByCid( $this->getPropertyId(), $strPropertyPreferenceKey, $this->getCid(), $this->m_objDatabase );

		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && false == is_null( $objPropertyPreference->getValue() ) ) {
			$strHtmlContent = '<table cellspacing="0" cellpadding="0" border="0" width="100%" >
									<tr>
										<td style="font: normal 11px/18px Arial, Helvetica, sans-serif;color:#808080;" align="left">
										' . $objPropertyPreference->getValue() . '
									</td>
									</tr>
								</table>';
		} else {
			$strHtmlContent = '<table cellspacing="0" cellpadding="0" border="0" width="100%">
									<tr>
										<td style="font:normal normal bold 13px/18px arial; color:#808080; padding-top:30px; padding-bottom:10px;" align="left">
											<strong>' . __( 'Online Lease has been Executed by Both Parties ' ) . '</strong><br/>
										</td>
									</tr>
									<tr>
										<td style="font: normal 11px/18px Arial, Helvetica, sans-serif;color:#808080;" align="left">
											' . __( 'The lease document(s) has been signed by both parties. The executed agreement is attached to this email.' ) . '</br>
										</td>
									</tr>
								</table>';
		}

		return $strHtmlContent;
	}

	public function getCustomPropertyOfficePhoneNumberText() {
		$strHtmlContent = '';

		$objPropertyPhoneNumber 		  = CPropertyPhoneNumbers::fetchPropertyPhoneNumberByPropertyIdByPhoneNumberTypeIdByCid( $this->getId(), CPhoneNumberType::OFFICE, $this->getCid(), $this->m_objDatabase );
		$objPropertyAdditionalPhoneNumber = CPropertyPhoneNumbers::fetchSingleAdditionalPropertyPhoneNumberByPropertyIdByCid( $this->getId(), CPhoneNumberType::PRIMARY, $this->getCid(), $this->m_objDatabase );

		if( true == valObj( $objPropertyPhoneNumber, 'CPropertyPhoneNumber' ) && false == is_null( $objPropertyPhoneNumber->getPhoneNumber() ) ) {
			$strHtmlContent = __( 'If you have any questions, or need help along the way, contact the leasing office at {%h, 0}.', [ $objPropertyPhoneNumber->getPhoneNumber() ] );
		} elseif( true == valObj( $objPropertyAdditionalPhoneNumber, 'CPropertyPhoneNumber' ) && false == is_null( $objPropertyAdditionalPhoneNumber->getPhoneNumber() ) ) {
			$strHtmlContent = __( 'If you have any questions, or need help along the way, contact the leasing office at {%h, 0}.', [ $objPropertyAdditionalPhoneNumber->getPhoneNumber() ] );
		}

		return $strHtmlContent;
	}

	public function getSupportedLocaleCodes( $objDatabase ) {
		if( true == valArr( $this->m_arrstrSupportedLocaleCodes ) ) {
			return $this->m_arrstrSupportedLocaleCodes;
		} else {
			$arrstrSupportedLocaleCodes = CCache::fetchObject( 'supported_locale_codes_cid_' . $this->getCid() . '_pid_' . $this->getId() );
			if( true == is_null( $arrstrSupportedLocaleCodes ) || false === $arrstrSupportedLocaleCodes ) {
				$arrobjSupportedLocaleCodes = ( array ) CPropertyLocales::fetchPropertyLocalesByPropertyIdByCid( $this->getId(), $this->getCid(), $objDatabase );
				$arrstrSupportedLocaleCodes = [];
				if( true == valArr( $arrobjSupportedLocaleCodes ) ) {
					foreach( $arrobjSupportedLocaleCodes as $objSupportedLocaleCode ) {
						$arrstrSupportedLocaleCodes[] = $objSupportedLocaleCode->getLocaleCode();
					}
					CCache::storeObject( 'supported_locale_codes_cid_' . $this->getCid() . '_pid_' . $this->getId(), $arrstrSupportedLocaleCodes, $intSpecificLifetime = 3600, $arrstrTags = [] );
				}
			}
			$this->m_arrstrSupportedLocaleCodes = $arrstrSupportedLocaleCodes;
			return $arrstrSupportedLocaleCodes;
		}
	}

	public function fetchPropertyInsuranceCarriers( $objDatabase ) {
		return CPropertyInsuranceCarriers::fetchAllPropertyInsuranceCarriersByPropertyIdsByCid( [ $this->getId() ], $this->getCid(), $objDatabase );
	}

	public function fetchLastOrderNumberByFaqGroupId( $intFaqGroupId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyFaqs::createService()->fetchLastOrderNumberByPropertyIdByFaqGroupIdByCid( $this->getId(), $intFaqGroupId, $this->getCid(), $objDatabase );
	}

	public function fetchPublishedSpecials( $objDatabase ) {
		if( true == isset( $this->m_arrintChildPropertyIds ) ) {
			$arrintPropertyIds = $this->getChildPropertyIds();
		}

		$arrintPropertyIds[$this->getId()] = $this->getId();
		return CSpecials::createService()->fetchPublishedSpecialsByPropertyIdsByCid( $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchShippingVendors() {
		$objPropertyAddress = CPropertyAddresses::createService()->fetchPropertyAddressByPropertyIdByAddressTypeIdByCid( $this->getId(), CAddressType::PRIMARY, $this->getCid(), $this->m_objDatabase );
		$strPropertyCountryCode = ( true == valObj( $objPropertyAddress, 'CPropertyAddress' ) ) ? $objPropertyAddress->getCountryCode() : NULL;

		$arrobjDefaultShippingVendors    = ( array ) CDefaultShippingVendors::fetchDefaultShippingVendorsByPropertyCountryCode( $strPropertyCountryCode, $this->m_objDatabase );
		$arrobjNonDefaultShippingVendors = ( array ) CShippingVendors::fetchNonDefaultShippingVendorsWithIconByPropertyIdByCid( $this->getId(), $this->getCid(), $this->m_objDatabase );

		$objPropertyPreference = $this->fetchPropertyPreferenceByKey( 'DISABLED_DEFAULT_SHIPPING_VENDORS', $this->m_objDatabase );
		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {
			$arrintListOfDisabledVendors = explode( ',', $objPropertyPreference->getValue() );
			foreach( $arrobjDefaultShippingVendors as $objDefaultShippingVendos ) {
				if( true == in_array( $objDefaultShippingVendos->getId(), $arrintListOfDisabledVendors ) ) {
					$objDefaultShippingVendos->setIsPublished( 0 );
				}
			}
		}

		return array_merge( $arrobjDefaultShippingVendors, $arrobjNonDefaultShippingVendors );
	}

	public function isResidentVerifyEnabled( $objDatabase ) {
		$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdTransmissionVendorIdsByPsProductIdsByCid( $this->getId(), [ CTransmissionVendor::RESIDENT_VERIFY ], [ CPsProduct::RESIDENT_VERIFY ], $this->getCid(), $objDatabase );
		return valObj( $objProperty, 'CProperty' );
	}

	public function getOrFetchResidentPortalUrl() {
		$strResidentPortalUrl = '';
		$intPropertyId = $this->getId();

		if( true == valId( $this->getPropertyId() ) ) {
			$intPropertyId = $this->getPropertyId();
		}

		$objWebsite = CWebsites::createService()->fetchResidentPortalDefaultWebsiteByPropertyIdsByCid( [ $intPropertyId ], $this->getCid(), $this->m_objDatabase );

		if( true == valObj( $objWebsite, 'CWebsite' ) ) {
			$strResidentPortalPrefix = ( true == valStr( CConfig::get( 'SECURE_HOST_PREFIX' ) ) ) ? CConfig::get( 'SECURE_HOST_PREFIX' ) : 'https://';
			$strResidentPortalSuffix = ( true == valStr( CConfig::get( 'RESIDENT_PORTAL_SUFFIX' ) ) ) ? CConfig::get( 'RESIDENT_PORTAL_SUFFIX' ) : '.residentportal' . CConfig::get( 'TLD' );

			$arrmixRequiredParameters = $this->getRequiredParameters();
			$boolIsUseLink = isset( $arrmixRequiredParameters['ar_payment_id'] );
			if( true == $boolIsUseLink ) {
				$strResidentPortalUrl = $objWebsite->getSubDomain() . $strResidentPortalSuffix;
			} else {
				$strResidentPortalUrl = $strResidentPortalPrefix . $objWebsite->getSubDomain() . $strResidentPortalSuffix . '/';
			}
		}
		return $strResidentPortalUrl;
	}

    public function checkIsIntegratedPropertyByIntegrationClientTypeIds( $arrintIntegrationClientTypeIds, $objDatabase ) {

        $boolIsIntegrated = false;

        $objIntegrationDatabase = $this->fetchIntegrationDatabase( $objDatabase );
        if( true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) && true == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), $arrintIntegrationClientTypeIds ) ) {
            $boolIsIntegrated = true;
        }

        return $boolIsIntegrated;
	}

	public function getResidentPortalForgotUrl( $objDatabase = NULL ) {
		if( true == valObj( $objDatabase, 'CDatabase' ) ) {
			$this->setDatabase( $objDatabase );
		}

		$objWebsiteDefault = CWebsites::createService()->fetchResidentPortalDefaultWebsiteByPropertyIdsByCid( [ $this->m_intId ], $this->m_intCid, $this->m_objDatabase, false, 1, true );
		$strResidentPortalPrefix    = ( \CConfig::get( 'secure_host_prefix' ) ?? 'https://' );
		$strResidentPortalSuffix    = ( \CConfig::get( 'resident_portal_suffix' ) ?? 'resident' . CConfig::get( 'tld' ) );

		$strResidentPortalForgotUrl = '';
		if( true === valObj( $objWebsiteDefault, 'CWebsite' ) ) {
			$strResidentPortalForgotUrl      = $strResidentPortalPrefix . $objWebsiteDefault->getSubDomain() . $strResidentPortalSuffix . '/resident_portal/?module=forgot_password&action=forgot_password&return_url=&kill_session=1';
		} elseif( true == valStr( $this->getIsRp40Enabled() ) ) {
			$strResidentPortalForgotUrl      = $strResidentPortalPrefix . $this->getIsRp40Enabled() . $strResidentPortalSuffix . '/auth/reset-password';
		} else {
			$arrobjWebsites  = ( array ) CWebsites::createService()->fetchPluralWebsitesByPropertyIdsByCid( [ $this->m_intId ], $this->m_intCid, $this->m_objDatabase );
			if( true == isset( $arrobjWebsites[0] ) && true == valStr( $arrobjWebsites[0]->getSubDomain() ) ) {
				$strResidentPortalForgotUrl  = $strResidentPortalPrefix . $arrobjWebsites[0]->getSubDomain() . $strResidentPortalSuffix . '/resident_portal/?module=forgot_password&action=forgot_password&return_url=&kill_session=1';
			}
		}
		return $strResidentPortalForgotUrl;
	}

	public function getResidentPortalFooterImage() {
		$strImageUrl = CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'rp_footer.png';
		return '<img src = "' . $strImageUrl . '" alt = ' . $strImageUrl . '/>';
	}

	public function getResidentPortalFooterImageMobile() {
		$strImageUrl = CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'rp_footer_mobile.png';
		return '<img src = "' . $strImageUrl . '" alt = ' . $strImageUrl . '>';
	}

	public function getDefaultPropertySettings( $arrobjPropertyPreferences = [], $arrstrResetPropertySettingsKey = [], $arrstrDefaultPropertySettingsKey = [] ) {

		if( true == valArr( $arrobjPropertyPreferences ) && true == valArr( $arrstrResetPropertySettingsKey ) ) {
			foreach( $arrobjPropertyPreferences as $strPropertySettingKey => $arrobjPropertyPreference ) {
				if( true == in_array( $strPropertySettingKey, $arrstrResetPropertySettingsKey ) ) {
					unset( $arrobjPropertyPreferences[$strPropertySettingKey] );
				}
			}
		}

		if( true == valArr( $arrstrDefaultPropertySettingsKey ) ) {
			$objPropertyPreference = $this->createPropertyPreference();
			foreach( $arrstrDefaultPropertySettingsKey as $strKey => $strValue ) {
				$objPropertyPreferenceCloned = clone $objPropertyPreference;
				$objPropertyPreferenceCloned->setKey( $strKey );
				$objPropertyPreferenceCloned->setValue( $strValue );
				$arrobjPropertyPreferences[$strKey] = $objPropertyPreferenceCloned;
			}
		}

		return $arrobjPropertyPreferences;
	}

	public function getReservationHubLoginUrl() {
		$strSecureHostPrefix	= ( true == defined( CONFIG_SECURE_HOST_PREFIX ) ) ? CONFIG_SECURE_HOST_PREFIX : 'https://';
		$strRwxSuffix			= ( false == is_null( CConfig::get( 'reservation_hub_suffix' ) ) ) ? CConfig::get( 'reservation_hub_suffix' ) : '.reservationhub.com';
		$objClient              = CClients::fetchSimpleClientById( $this->getCid(), $this->m_objDatabase );
		$strRwxLoginUrl         = '';

		if( true == valObj( $objClient, 'CClient' ) ) {
			$strRwxLoginUrl = $strSecureHostPrefix . $objClient->getRwxDomain() . $strRwxSuffix;
		}

		return $strRwxLoginUrl;
	}

	public function checkIsArMigrationModeOn( $objDatabase ) {
		$objPropertyGlSetting = $this->getOrFetchPropertyGlSetting( $objDatabase );
		if( true == $objPropertyGlSetting->getIsArMigrationMode() || true == $objPropertyGlSetting->getIsApMigrationMode() ) {
			return true;
		}

		return false;
	}

	public function checkIsAALetterRequired() {
		$boolIsValid = true;

		if( CCountry::CODE_CANADA == $this->getCountryCode() ) {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function prepareAndUpdateEmailRelayAddress( $intCompanyUserId, $objDatabase, $objAdminDatabase ) {
		$strPropertyName = \Psi\CStringService::singleton()->strtolower( getAlfaNumericValue( $this->getPropertyName() ) );
		$objPropertyAddress = \Psi\Eos\Admin\CPropertyAddresses::createService()->fetchPrimaryPropertyAddressByPropertyIdByCid( $this->getId(), $this->getCid(), $objAdminDatabase );
		$strCity = '';
		if( true == valObj( $objPropertyAddress, CPropertyAddress::class ) ) {
			$strCity = \Psi\CStringService::singleton()->strtolower( getAlfaNumericValue( $objPropertyAddress->getCity() ) );
		}

		$strEmailRelayDomain = '@' . CConfig::get( 'email_relay_domain' );
		$arrintPropertyCount = \Psi\Eos\Admin\CProperties::createService()->fetchPropertiesCountByPropertyName( $strPropertyName, $strCity, $strEmailRelayDomain, $objAdminDatabase );
		$strEmailRelayAddress = $strPropertyName . $strEmailRelayDomain;
		if( true == valArr( $arrintPropertyCount ) ) {
			$strEmailRelayAddress = $strPropertyName . $strCity . '@' . CConfig::get( 'email_relay_domain' );
			if( 0 != $arrintPropertyCount['citycount'] ) {
				$strEmailRelayAddress = $strPropertyName . $strCity . ++$arrintPropertyCount['citycount'] . '@' . CConfig::get( 'email_relay_domain' );
			}
		}

		$this->setEmailRelayAddress( $strEmailRelayAddress );

		if( false == $this->update( $intCompanyUserId, $objDatabase, $objAdminDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to update email relay address' ), NULL ) );
			return false;
		}

		return $strEmailRelayAddress;
	}

	public function getCallId() {
		return $this->getRequiredParameters()['call_id'] ?? NULL;
	}

	public function getCallNotes() {
		$intCallId = $this->getRequiredParameters()['call_id'] ?? '';
		if( false == valId( $intCallId ) ) {
			return '';
		}

		$objVoipDatabase = \Psi\Eos\Connect\CDatabases::createService()->loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::VOIP, CDatabaseUserType::PS_VOIP );
		if( false == valObj( $objVoipDatabase, CDatabase::class ) ) {
			return '';
		}

		$objCall = \Psi\Eos\Voip\CCalls::createService()->fetchCallByCidById( $this->getCid(), $intCallId, $objVoipDatabase );
		if( false == valObj( $objCall, CCall::class ) ) {
			return '';
		}

		$objVoipDatabase->close();

		return $objCall->getCallNotes() ?? '';
	}

	public function getErrorIconImage() {
		$strImageUrl = CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'error-icon.png';
		return '<img src="' . $strImageUrl . '"/>';
	}

	public function getCompanyDefaultImage() {
		$objLogoMarketingMediaAssociation = \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getCId(), CMarketingMediaType::COMPANY_PREFERENCES_DEFAULT_IMAGE, CMarketingMediaSubType::COMPANY_LOGO, $this->getCId(), $this->m_objDatabase);
		if( true == valObj( $objLogoMarketingMediaAssociation, CMarketingMediaAssociation::class ) ) {
			$strImageUrl = $objLogoMarketingMediaAssociation->getMediaLibraryFullSizeUri();
			return '<img src="' . $strImageUrl . '" style="width:100px;">';
		}
		return '';
	}

	public function getPropertyAddress() {
		$boolIsRequiredAddress = $this->getRequiredParameters()['is_required_address'] ?? true;
		$boolIsCustomerPasswordReset = $this->getRequiredParameters()['is_customer_password_reset'] ?? NULL;

		if( false == $boolIsRequiredAddress ) {
			return '';
		}

		$arrobjPropertyAddresses = ( array ) \Psi\Eos\Entrata\CPropertyAddresses::createService()->fetchPropertyAddressWithPhoneNumberByPropertyIdByAddressTypeIdByCid( $this->getId(), CAddressType::PRIMARY, $this->getCid(), $this->m_objDatabase );
		$strPropertyAddress = '';

		if( 0 == \Psi\Libraries\UtilFunctions\count( $arrobjPropertyAddresses ) ) {
			return $strPropertyAddress;
		}

		$arrobjPropertyAddresses = current( $arrobjPropertyAddresses );

		$strPropertyAddress = ( true == $boolIsCustomerPasswordReset && true == valStr( $this->getPropertyName() ) ) ? $this->getPropertyName() . '<br>' : '';
		$strPropertyAddress .= ( true == valArrKeyExists( $arrobjPropertyAddresses, 'street_line1' ) && true == valStr( $arrobjPropertyAddresses['street_line1'] ) ) ? $arrobjPropertyAddresses['street_line1'] . '<br>' : '';
		$strPropertyAddress .= ( true == $boolIsCustomerPasswordReset && true == valArrKeyExists( $arrobjPropertyAddresses, 'street_line2' ) && true == valStr( $arrobjPropertyAddresses['street_line2'] ) ) ? $arrobjPropertyAddresses['street_line2'] . '<br>' : '';
		$strPropertyAddress .= ( true == $boolIsCustomerPasswordReset && true == valArrKeyExists( $arrobjPropertyAddresses, 'street_line3' ) && true == valStr( $arrobjPropertyAddresses['street_line3'] ) ) ? $arrobjPropertyAddresses['street_line3'] . '<br>' : '';
		$strPropertyAddress .= ( true == valArrKeyExists( $arrobjPropertyAddresses, 'city' ) && true == valStr( $arrobjPropertyAddresses['city'] ) ) ? $arrobjPropertyAddresses['city'] : '';
		$strPropertyAddress .= ( ( true == valStr( $strPropertyAddress ) ) ? ', ' : '' ) . $arrobjPropertyAddresses['state_code'];
		$strPropertyAddress .= ( true == valArrKeyExists( $arrobjPropertyAddresses, 'postal_code' ) && true == valStr( $arrobjPropertyAddresses['postal_code'] ) ) ? ( ( true == valStr( $strPropertyAddress ) ) ? ' ' : '' ) . $arrobjPropertyAddresses['postal_code'] . '<br>' : '';
		if( true == $boolIsCustomerPasswordReset && true == valArrKeyExists( $arrobjPropertyAddresses, 'office_phone_number' ) ) {
			$strFormattedNumber = preg_replace( '/^(\d{3})(\d{3})(\d{4})$/', '$1-$2-$3', $arrobjPropertyAddresses['office_phone_number'] );
			$strPropertyAddress .= ( true == valStr( $strFormattedNumber ) ) ? 'P: ' . $strFormattedNumber : '';
		}

		return $strPropertyAddress;
	}

	public function getPropertyLogoOrName() {
		$strPropertyLogo = $this->getMarketingMediaAssociationEmailImage( CMarketingMediaSubType::PROPERTY_LOGO );
		return ( true == valStr( $strPropertyLogo ) ) ? $strPropertyLogo : $this->getPropertyName();
	}

	public function getStrAutoPublished() {
		return ( 1 == $this->getRequiredParameters()['is_auto_published'] ) ? 'auto' : '';
	}

	public function getPublishedUnitSpaces( $intIsRenewal = 0, $intIsAutoPublished = 0 ) {
		$arrobjPublishedRevenuePostRents = ( array ) \Psi\Eos\Entrata\CRevenuePostRents::createService()->fetchPublishedUnitsByPropertyIdByCid( $this->getId(), $this->getCid(), $this->m_objDatabase, $intIsRenewal, $intIsAutoPublished );

		if( true == \valArr( $arrobjPublishedRevenuePostRents ) ) {
			$arrintUnitSpaceIds = \getObjectFieldValuesByFieldName( 'unitSpaceId', $arrobjPublishedRevenuePostRents );
			$arrintUnitTypeIds  = \getObjectFieldValuesByFieldName( 'unitTypeId', $arrobjPublishedRevenuePostRents );

			$arrobjUnitSpaces = ( array ) CUnitSpaces::createService()->fetchUnitSpacesByIdsByPropertyIdByCid( $arrintUnitSpaceIds, $this->getId(), $this->getCid(), $this->m_objDatabase );
			$arrobjUnitTypes  = ( array ) CUnitTypes::createService()->fetchUnitTypesByIdsByCid( $arrintUnitTypeIds, $this->getCid(), $this->m_objDatabase );

			$objRateLogsFilter = new CRateLogsFilter;
			$objRateLogsFilter->setCids( [ $this->getCid() ] );
			$objRateLogsFilter->setPropertyGroupIds( [ $this->getId() ] );
			$objRateLogsFilter->setArOriginIds( [ \CArOrigin::AMENITY ] );
			$objRateLogsFilter->setPublishedUnitsOnly( false );
			$objRateLogsFilter->setMarketedUnitsOnly( true );
			$objRateLogsFilter->setUnitTypeIds( $arrintUnitTypeIds );
			$objRateLogsFilter->setUnitSpaceIds( $arrintUnitSpaceIds );
			$arrintAmenityRateLogs = ( array ) \CPricingRentReviewLibrary::getAmentiesAmountByRatelogFilter( $objRateLogsFilter, $this->m_objDatabase );

			$arrmixConstraintPublishUnitSpaces = [];
			$arrmixPublishUnitSpaces           = [];
			foreach( $arrobjPublishedRevenuePostRents as $arrobjRevenuePostRent ) {
				$intUnitSpaceAmenity = ( true == isset( $arrintAmenityRateLogs['unit_space'][$arrobjRevenuePostRent->getUnitSpaceId()] ) ) ? $arrintAmenityRateLogs['unit_space'][$arrobjRevenuePostRent->getUnitSpaceId()] : 0;
				$intUnitTypeAmenity  = ( true == isset( $arrintAmenityRateLogs['unit_type'][$arrobjRevenuePostRent->getUnitTypeId()] ) ) ? $arrintAmenityRateLogs['unit_type'][$arrobjRevenuePostRent->getUnitTypeId()] : 0;
				$intPropertyAmenity  = ( true == isset( $arrintAmenityRateLogs['property'][$arrobjRevenuePostRent->getPropertyId()] ) ) ? $arrintAmenityRateLogs['property'][$arrobjRevenuePostRent->getPropertyId()] : 0;

				$intAmenity = $intUnitSpaceAmenity + $intUnitTypeAmenity + $intPropertyAmenity;

				$arrmixPublishUnitSpace['optimal_rent']    = $arrobjRevenuePostRent->getStdOptimalRent() + $intAmenity;
				$arrmixPublishUnitSpace['override_rent']   = ( 0 < $arrobjRevenuePostRent->getStdOverrideRent() ) ? $arrobjRevenuePostRent->getStdOverrideRent() + $intAmenity : 0;
				$arrmixPublishUnitSpace['override_reason'] = $arrobjRevenuePostRent->getOverrideReason();
				$arrmixPublishUnitSpace['unit_space_name'] = $arrobjUnitSpaces[$arrobjRevenuePostRent->getUnitSpaceId()]->getUnitNumberCache();
				$arrmixPublishUnitSpace['lookup_code']     = $arrobjUnitTypes[$arrobjRevenuePostRent->getUnitTypeId()]->getLookupCode();

				if( false == is_null( $arrobjRevenuePostRent->getConstrainedOn() ) ) {
					$arrstrConstraint       = $arrobjRevenuePostRent->getConstraintFailureReason();
					$arrstrConstraintReason = explode( 'or', $arrstrConstraint );
					$arrmixRulePercent      = explode( '%', $arrstrConstraintReason[1], 2 );
					$strNegativePositive    = ( 'Over Criteria' == ltrim( $arrmixRulePercent[1] ) ) ? '' : '-';

					$arrmixPublishUnitSpace['rule_exceeded_dollar'] = \Psi\CStringService::singleton()->substr( $arrstrConstraintReason[0], 1 );
					$arrmixPublishUnitSpace['rule_exceeded_percent'] = $strNegativePositive . round( $arrmixRulePercent[0], 2 );

					$arrmixConstraintPublishUnitSpaces[] = $arrmixPublishUnitSpace;
				} else {
					$arrmixPublishUnitSpaces[] = $arrmixPublishUnitSpace;
				}
			}

			return [
				'published_unit_spaces'            => $arrmixPublishUnitSpaces,
				'published_constraint_unit_spaces' => $arrmixConstraintPublishUnitSpaces
			];
		}
	}

	public function getRevenuePublishedUnitsHtml() {
		$intIsRenewal       = $this->getRequiredParameters()['is_renewal'];
		$intIsAutoPublished = $this->getRequiredParameters()['is_auto_published'];

		$arrmixPublishedAllUnitSpaces   = $this->getPublishedUnitSpaces( $intIsRenewal, $intIsAutoPublished );
		$arrmixPublishedUnitSpaces      = $arrmixPublishedAllUnitSpaces['published_unit_spaces'];
		$arrmixPublishedConstraintUnitSpaces = $arrmixPublishedAllUnitSpaces['published_constraint_unit_spaces'];

		$strPublishedHTML = '';
		if( true == valArr( $arrmixPublishedUnitSpaces ) ) {
			$strPublishedHTML = __( '<tr>
										<td style="width:20px;"></td>
										<td style="font-size:14px; color:#ccc; line-height:20px; font-family: "Montserrat", sans-serif;" class="responsive spacer">
											<span style="padding:10px 0px; display:block; text-transform:uppercase;">THE FOLLOWING RENTS WERE PUBLISHED SUCCESSFULLY.</span>
										</td>
										<td style="width:20px;"></td>
									</tr>
									<tr>
										<td style="width:20px;"></td>
										<td>
											<table style="width:90%; border-collapse: collapse;" cellspacing="0" cellpadding="0">
												<thead class="responsive-hide">
												<tr style="text-transform: uppercase; color:#ccc; font-size:14px; text-align:left;">
													<th style="border: 1px solid #CCC; font-weight:400; padding:5px 10px; width:25%;">Unit Space</th>
													<th style="border: 1px solid #CCC; font-weight:400; padding:5px 10px; width:25%;">Unit Type</th>
													<th style="border: 1px solid #CCC; font-weight:400; padding:5px 10px; width:25%;">Optimal Rent</th>
													<th style="border: 1px solid #CCC; font-weight:400; padding:5px 10px; width:25%;">Override Rent</th>
													<th style="border: 1px solid #CCC; font-weight:400; padding:5px 10px; width:25%;">Override Reason</th>
												</tr>
												</thead>
												<tbody class="responsive-table">
												<published_units/>
												</tbody>
											</table>
										</td>
										<td style="width:20px;"></td>
									<tr>' );

			$strPublishedUnitsHTML = '';
			foreach( $arrmixPublishedUnitSpaces as $arrmixPublishedUnit ) {
				$strOptimalAlignClass       = ( 0 < $arrmixPublishedUnit['optimal_rent'] ) ? 'text-align:right;' : 'text-align:center;';
				$strOverrideAlignClass      = ( 0 < $arrmixPublishedUnit['override_rent'] ) ? 'text-align:right;' : 'text-align:center;';
				$strReasonAlignClass = ( isset ( $arrmixPublishedUnit['override_reason'] ) ) ? 'text-align:left;' : 'text-align:center;';
				$strOptimalRent = ( string ) ( 0 < $arrmixPublishedUnit['optimal_rent'] ) ? __( '{%m,0,p:0}', [ $arrmixPublishedUnit['optimal_rent'] ] ) : '-';
				$strOverrideRent = ( string ) ( 0 < $arrmixPublishedUnit['override_rent'] ) ? __( '{%m,0,p:0}', [ $arrmixPublishedUnit['override_rent'] ] ) : '-';
				$strOverrideReason = ( true == isset ( $arrmixPublishedUnit['override_reason'] ) ) ? __( '{%s,0}', [ $arrmixPublishedUnit['override_reason'] ] ) : '-';
				$strUnitSpaceName = ( true == isset ( $arrmixPublishedUnit['unit_space_name'] ) ) ? __( '{%s,0}', [ $arrmixPublishedUnit['unit_space_name'] ] ) : '-';
				$strLookUpCode = ( true == isset( $arrmixPublishedUnit['lookup_code'] ) ) ? __( '{%s,0}', [ $arrmixPublishedUnit['lookup_code'] ] ) : '-';

				$strPublishedUnitsHTML .= '<tr style="text-transform: uppercase;  font-size:16px; text-align:left;">
											<td style="border: 1px solid #CCC; padding:10px; text-align:left;" data-th="Unit Space">
												' . $strUnitSpaceName . '
											</td>
											<td style="border: 1px solid #CCC; padding:10px; text-align:left;" data-th="Unit Type">
												' . $strLookUpCode . ' 
											</td>
											<td style="border: 1px solid #CCC; padding:10px; ' . $strOptimalAlignClass . '" data-th="Optimal Rent">
												' . $strOptimalRent . ' 
											</td>
											<td style="border: 1px solid #CCC; padding:10px; ' . $strOverrideAlignClass . '" data-th="Override Rent">
												' . $strOverrideRent . ' 
											</td>
											<td style="border: 1px solid #CCC; padding:10px; ' . $strReasonAlignClass . '" data-th="Override Reason">
											' . $strOverrideReason . '  
											</td>
										</tr>';
			}
			$strPublishedHTML = str_replace( '<published_units/>', $strPublishedUnitsHTML, $strPublishedHTML );

		}
		$strConstraintHTML = '';
		if( true == valArr( $arrmixPublishedConstraintUnitSpaces ) ) {
			$strConstraintHTML = __( '<tr>
										<td style="width:20px;"></td>
										<td>
											<span style="padding: 10px 0; color:#ccc; display:block; text-transform:uppercase; font-size:14px;">
												THE FOLLOWING RENTS WERE ALSO PUBLISHED BUT DID NOT MEET CRITERIA. PLEASE REVIEW AND MAKE ANY DESIRED CHANGES IN ENTRATA PRICING. 
											</span>
										</td>
										<td style="width:20px;"></td>
									</tr>
									<tr>
										<td style="width:20px;"></td>
										<td>
											<table style="width:100%; border-collapse: collapse;" cellspacing="0" cellpadding="0">
												<thead class="responsive-hide">
													<tr style="text-transform: uppercase; color:#ccc; font-size:14px; text-align:left;">
														<th style="border: 1px solid #CCC; font-weight:400; padding:5px 10px; width:20%;">Unit Space</th>
														<th style="border: 1px solid #CCC; font-weight:400; padding:5px 10px; width:20%;">Unit Type</th>
														<th style="border: 1px solid #CCC; font-weight:400; padding:5px 10px; width:25%;">Optimal Rent</th>
														<th style="border: 1px solid #CCC; font-weight:400; padding:5px 10px; width:25%;">Override Rent</th>
														<th style="border: 1px solid #CCC; font-weight:400; padding:5px 10px; width:25%;">Override Reason</th>
														<th style="border: 1px solid #CCC; font-weight:400; padding:5px 10px; width:30%;">Rule Exceeded($)</th>
														<th style="border: 1px solid #CCC; font-weight:400; padding:5px 10px; width:30%;">Rule Exceeded(%)</th>
													</tr>
												</thead>
												<published_constraint_units/>
												</tbody>
											</table>
										</td>
										<td style="width:20px;"></td>
									</tr>' );
			$strConstraintUnitsHTML = '';
			foreach( $arrmixPublishedConstraintUnitSpaces as $arrmixPublishedConstraintUnit ) {
				$strUnitSpaceName = ( true == isset( $arrmixPublishedConstraintUnit['unit_space_name'] ) ) ? __( '{%s,0}', [ $arrmixPublishedConstraintUnit['unit_space_name'] ] ) : '-';
				$strLookUpCode    = ( true == isset( $arrmixPublishedConstraintUnit['lookup_code'] ) ) ? __( '{%s,0}', [ $arrmixPublishedConstraintUnit['lookup_code'] ] ) : '-';

				$strOverrideReason = ' - ';
				$strOverrideReasonAlignClass = 'text-align:center;';
				if( true == isset( $arrmixPublishedConstraintUnit['override_reason'] ) ) {
					$strOverrideReason = __( '{%s,0}', [ $arrmixPublishedConstraintUnit['override_reason'] ] );
					$strOverrideReasonAlignClass = 'text-align:left;';
				}

				$strOptimalRent = ' - ';
				$strOptimalRentAlignClass = 'text-align:center;';
				if( 0 < $arrmixPublishedConstraintUnit['optimal_rent'] ) {
					$strOptimalRent = __( '{%m,0,p:0}', [ $arrmixPublishedConstraintUnit['optimal_rent'] ] );
					$strOptimalRentAlignClass = 'text-align:right;';
				}

				$strOverrideRent = ' - ';
				$strOverrideRentAlignClass = 'text-align:center;';
				if( 0 < $arrmixPublishedConstraintUnit['override_rent'] ) {
					$strOverrideRent = __( '{%m,0,p:0}', [ $arrmixPublishedConstraintUnit['override_rent'] ] );
					$strOverrideRentAlignClass = 'text-align:right;';
				}
				$strRuleExceededAmount = ' - ';
				$strRuleExceedAmountAlignClass = 'text-align:center;';
				$strRuleExceedPercentAlignClass = 'text-align:center;';
				if( true == isset( $arrmixPublishedConstraintUnit['rule_exceeded_dollar'] ) ) {
					$strRuleExceededAmount = __( '{%m, 0, p:0}', [ ( int ) $arrmixPublishedConstraintUnit['rule_exceeded_dollar'] ] );
					$strRuleExceedAmountAlignClass = 'text-align:right;';
					$strRuleExceedPercentAlignClass = 'text-align:left;';
				}
				$strRuleExceededPercent = ( string ) ( true == isset( $arrmixPublishedConstraintUnit['rule_exceeded_percent'] ) ) ? __( '{%d, 0, p:0}%', [ ( int ) $arrmixPublishedConstraintUnit['rule_exceeded_percent'] ] ) : ' - ';

				$strConstraintUnitsHTML .= '<tr style="text-transform: uppercase;  font-size:16px; text-align:left;">
										<td style="border: 1px solid #CCC; padding:10px; text-align:left;" data-th="Unit Space">
											' . $strUnitSpaceName . '
										</td>
										<td style="border: 1px solid #CCC; padding:10px; text-align:left;" data-th="Unit Type">
											' . $strLookUpCode . '
										</td>
										<td style="border: 1px solid #CCC; padding:10px; ' . $strOptimalRentAlignClass . '" data-th="Optimal Rent">
											' . $strOptimalRent . '
										</td>
										<td style="border: 1px solid #CCC; padding:10px; ' . $strOverrideRentAlignClass . '" data-th="Override Rent">
											' . $strOverrideRent . '
										</td>
										<td style="border: 1px solid #CCC; padding:10px; ' . $strOverrideReasonAlignClass . '" data-th="Override Reason">
											' . $strOverrideReason . '
										</td>
										<td style="border: 1px solid #CCC; padding:10px; ' . $strRuleExceedAmountAlignClass . '" data-th="Rule Exceeded Dollar">
											' . $strRuleExceededAmount . '
										</td>
										<td style="border: 1px solid #CCC; padding:10px; ' . $strRuleExceedPercentAlignClass . '" data-th="Rule Exceeded Percent">
											' . $strRuleExceededPercent . '
										</td>
									</tr>';
			}

			$strConstraintHTML  = str_replace( '<published_constraint_units/>', $strConstraintUnitsHTML, $strConstraintHTML );
		}
		return $strPublishedHTML . $strConstraintHTML;
	}

	public function checkIsRvPermissionAllowedToIntegrationScreeningVendor() {
		$objPropertyCompanyTransmissionVendor = \Psi\Eos\Entrata\CCompanyTransmissionVendors::createService()->fetchCompanyTransmissionVendorByPropertyIdByTransmissionTypeIdByCid( $this->getId(), CTransmissionType::SCREENING, $this->getCid(), $this->m_objDatabase );
		if( true == valObj( $objPropertyCompanyTransmissionVendor, 'CCompanyTransmissionVendor' ) && true == in_array( $objPropertyCompanyTransmissionVendor->getTransmissionVendorId(), CTransmissionVendor::$c_arrintAllowRVPermissionToNonRvVendorIds ) ) {
			return true;
		}
		return false;
	}

	public function getResidentPortalAppBannerImage() {
	$strImageUrl = CONFIG_COMMON_PATH . '/website_templates/_common/images/resident_portal/resident_portal_app/RP-payment-receipts-580x90-v2.jpg';
	$strMobileImageUrl = CONFIG_COMMON_PATH . '/website_templates/_common/images/resident_portal/resident_portal_app/rp_footer_mobile.png';
	$strResidentPortalBanner = '<span class="large_banner" style="display: block;"><img src = "' . $strImageUrl . '" style="margin-top:10px; border:3px solid #EfEfEf; width:580px; border-top:none; border-right:none;" usemap="#resident_receipt" />
			<map name="resident_receipt">
				<area href="https://itunes.apple.com/us/app/resident-portal-mobile/id443831139?mt=8" shape="rect" coords="399,10,513,42" target="_blank" />
				<area href="https://play.google.com/store/apps/details?id=com.psi.residentportal" shape="rect" coords="399,48,513,82" target="_blank" />
			</map></span>
			<span class="mobile_banner" style="display: none;"><img src = "' . $strMobileImageUrl . '" style="margin-top:10px; border:3px solid #EfEfEf; width:325px;border-top:none; border-right:none;" usemap="#resident_receipt" />
			<map name="resident_receipt">
				<area href="https://itunes.apple.com/us/app/resident-portal-mobile/id443831139?mt=8" shape="rect" coords="399,10,513,42" target="_blank" />
				<area href="https://play.google.com/store/apps/details?id=com.psi.residentportal" shape="rect" coords="399,48,513,82" target="_blank" />
			</map></span>';
	return $strResidentPortalBanner;
	}

	public function getPricingAwaitingApprovalUnitSpaces( $intIsRenewal, $arrmixPostedUnitSpaces, $arrmixAwaitingApproval ) {
		if( false == valArr( $arrmixPostedUnitSpaces ) || false == valArr( $arrmixAwaitingApproval ) ) {
			return NULL;
		}

		$arrintUnitTypeIds = array_map( function ( $arrmixUnitSpace ) {
			return $arrmixUnitSpace['unit_type_id'];
		}, $arrmixAwaitingApproval );

		if( false == valArr( $arrintUnitTypeIds ) ) {
			return NULL;
		}
		$arrintUnitTypeIds = array_unique( $arrintUnitTypeIds );
		$arrmixOverriddenUnitSpaces = ( array ) \Psi\Eos\Entrata\CRevenueOverrideRents::createService()->fetchRevenueOverrideRentsByPropertyIdByUnitTypeIdsByUnitSpaceIdCid( [ $this->getId() ], $arrintUnitTypeIds, $arrmixPostedUnitSpaces, $this->getCid(), $this->m_objDatabase, $intIsRenewal );
		$arrobjUnitSpaces           = [];

		if( false == valArr( $arrmixOverriddenUnitSpaces ) ) {
			return NULL;
		}

		if( 0 == $intIsRenewal ) {
			$objRateLogsFilter = new CRateLogsFilter;
			$objRateLogsFilter->setCids( [ $this->getCid() ] );
			$objRateLogsFilter->setPropertyGroupIds( [ $this->getId() ] );
			$objRateLogsFilter->setUnitTypeIds( $arrintUnitTypeIds );
			$objRateLogsFilter->setUnitSpaceIds( $arrmixPostedUnitSpaces );
			$objRateLogsFilter->setArOriginIds( [ CArOrigin::AMENITY ] );
			$objRateLogsFilter->setPublishedUnitsOnly( false );
			$objRateLogsFilter->setMarketedUnitsOnly( true );
			$arrintAmenities = ( array ) CPricingRentReviewLibrary::getAmentiesAmountByRatelogFilter( $objRateLogsFilter, $this->m_objDatabase );
		} else {
			$arrmixAmenityAdjustments = CPricingPostRentLibrary::getRenewalAmenities( $this->getCid(), $this->getId(), $arrmixPostedUnitSpaces, $this->m_objDatabase );
		}

		foreach( $arrmixOverriddenUnitSpaces as $arrmixOverriddenUnitspace ) {
			if( true == in_array( $arrmixOverriddenUnitspace['unit_space_id'], $arrmixPostedUnitSpaces ) ) {
				$objUnitSpace                  = new stdClass();
				$objUnitSpace->lookup_code     = $arrmixOverriddenUnitspace['lookup_code'];
				$objUnitSpace->unit_space_name = $arrmixOverriddenUnitspace['unit_space_name'];
				$intStdOptimalRent             = round( $arrmixOverriddenUnitspace['std_optimal_rent'] );
				$intOverrideRentAmount         = round( $arrmixOverriddenUnitspace['override_rent_amount'] );
				$intAmenityAmount = 0;
				if( 1 == $intIsRenewal ) {
					if( true == valArr( $arrmixAmenityAdjustments ) ) {
						foreach( $arrmixAmenityAdjustments as $arrmixAmenityAdjustment ) {
							if( $arrmixOverriddenUnitspace['unit_space_id'] == $arrmixAmenityAdjustment['id'] ) {
								$intAmenityAmount += $arrmixAmenityAdjustment['rate_amount'];
							}
						}
					}
				} else {
					if( true == isset( $arrintAmenities['unit_space'][$arrmixOverriddenUnitspace['unit_space_id']] ) ) {
						$intAmenityAmount = $arrintAmenities['unit_space'][$arrmixOverriddenUnitspace['unit_space_id']];
					}
					if( true == isset( $arrintAmenities['unit_type'][$arrmixOverriddenUnitspace['unit_type_id']] ) ) {
						$intAmenityAmount += $arrintAmenities['unit_type'][$arrmixOverriddenUnitspace['unit_type_id']];
					}
					if( true == isset( $arrintAmenities['property'][$this->getId()] ) ) {
						$intAmenityAmount += $arrintAmenities['property'][$this->getId()];
					}
				}
				if( 0 < $intStdOptimalRent ) {
					$intStdOptimalRent += $intAmenityAmount;
				}
				if( 0 < $intOverrideRentAmount ) {
					$intOverrideRentAmount += $intAmenityAmount;
				}

				$arrstrFailureReason = explode( ' or ', $arrmixAwaitingApproval[$arrmixOverriddenUnitspace['unit_space_id']]['constraint_result'] );
				$intRuleExceedDollar = str_replace( '$', '', $arrstrFailureReason[0] );
				$arrstrExceedPercent = explode( ' ', $arrstrFailureReason[1] );
				$intPercentExceed    = str_replace( '$', '', $arrstrExceedPercent[0] );
				$strConstraint = '';
				if( $arrstrExceedPercent[1] != 'Over' ) {
					$strConstraint = '-';
				}
				$objUnitSpace->optimal_rent    = $intStdOptimalRent;
				$objUnitSpace->override_rent   = $intOverrideRentAmount;
				$objUnitSpace->override_reason = $arrmixOverriddenUnitspace['override_rent_reason'];
				if( $arrstrExceedPercent[1] != 'Over' ) {
					$objUnitSpace->rule_exceeded_dollar = ( false == is_null( $intRuleExceedDollar ) && 0 < $intRuleExceedDollar ) ? '(' . __( '{%d, 0, p:0}', [ $intRuleExceedDollar ] ) . ')' : '-';
				} else {
					$objUnitSpace->rule_exceeded_dollar = ( false == is_null( $intRuleExceedDollar ) && 0 < $intRuleExceedDollar ) ? __( '{%d, 0, p:0}', [ $intRuleExceedDollar ] ) : '-';
				}
				$objUnitSpace->rule_exceeded_percent = $strConstraint . round( $intPercentExceed, 2 ) . '%';
				$arrobjUnitSpaces[]                  = $objUnitSpace;
			}
		}

		return $arrobjUnitSpaces;
	}

	public function getPricingAwaitingApprovalUnitSpacesHtml() {

		$intIsRenewal           = $this->getRequiredParameters()['is_renewal'] ?? NULL;
		$arrmixPostedUnitSpaces = $this->getRequiredParameters()['posted_unit_spaces'] ?? NULL;
		$arrmixAwaitingApproval = $this->getRequiredParameters()['awaiting_approvals'] ?? NULL;

		$arrobjUnitSpaces = $this->getPricingAwaitingApprovalUnitSpaces( $intIsRenewal, $arrmixPostedUnitSpaces, $arrmixAwaitingApproval );

		$strAwaitingApprovalHTML = __( '<tr>
					<td style="width:20px;"></td>
					<td style="font-size:14px; line-height:20px; text-align:left; font-family: \'Montserrat\', sans-serif;" class="responsive spacer">
						<span style=" display:block; padding-bottom:5px; color:#999;">THE FOLLOWING RENTS <b>WERE NOT</b> PUBLISHED AS THEY DID NOT MEET CRITERIA AND ARE AWAITING APPROVAL.</span>
					</td>
					<td style="width:20px;"></td>
				</tr>
				<tr>
					<td style="width:20px;"></td>
					<td>
						<table style="width:100%; border-collapse: collapse;" cellspacing="0" cellpadding="0">
							<thead class="responsive-hide">
							<tr style="text-transform: uppercase; color:#999; font-size:14px; text-align:left;">
								<th style="border: 1px solid #CCC; font-weight:400; padding:10px; width:90px;vertical-align:top;">Unit Space</th>
								<th style="border: 1px solid #CCC; font-weight:400; padding:10px; width:90px;vertical-align:top;">Unit Type</th>
								<th style="border: 1px solid #CCC; font-weight:400; padding:10px; width:104px;vertical-align:top;">Optimal Rent</th>
								<th style="border: 1px solid #CCC; font-weight:400; padding:10px; width:116px;vertical-align:top;">Override Rent</th>
								<th style="border: 1px solid #CCC; font-weight:400; padding:10px; width:137px;vertical-align:top;">Override Reason</th>
								<th style="border: 1px solid #CCC; font-weight:400; padding:10px; width:120px;vertical-align:top;">Rule Exceeded($)</th>
								<th style="border: 1px solid #CCC; font-weight:400; padding:10px; width:110px;vertical-align:top;">Rule Exceeded(%)</th>
							</tr>
							</thead>
							<tbody class="responsive-table">
							 <awaiting_approval_units/>
							</tbody>
						</table>
					</td>
					<td style="width:20px;"></td>
				</tr>
				<tr>
					<td style="width:20px;"></td>
					<td style="height:1px; background:#CCC;"></td>
					<td style="width:20px;"></td>
				</tr>' );
		$strAwaitingApprovalUnitSpacesHtml = '';
		if( true == valArr( $arrobjUnitSpaces ) ) {
			foreach( $arrobjUnitSpaces as $objUnitSpace ) {
				$strAwaitingApprovalUnitSpacesHtml .= __( ' <tr style="font-size:14px; text-align:left;">
					<td style="border: 1px solid #CCC; padding:10px;text-align: left;" data-th="unit Space">{%s,0}</td>
					<td style="border: 1px solid #CCC; padding:10px;text-align: left;" data-th="Unit Type">{%s,1}</td>
					<td style="border: 1px solid #CCC; padding:10px; {%s,7}" data-th="Optimal Rent">{%m,2,p:0}</td>
					<td style="border: 1px solid #CCC; padding:10px; {%s,8}" data-th="Override Rent">{%m,3,p:0}</td>
					<td style="border: 1px solid #CCC; padding:10px; {%s,9}" data-th="Override Reason">{%s,4}</td>
					<td style="border: 1px solid #CCC; padding:10px; {%s,10}" data-th="Rule Exceeded($)">{%s,5}</td>
					<td style="border: 1px solid #CCC; padding:10px; text-align: left;" data-th="Rule Exceeded(%)">{%s,6}</td>
					</tr>',
					[
						( ( true == isset( $objUnitSpace->unit_space_name ) ) ? $objUnitSpace->unit_space_name : '-' ),
						( ( true == isset( $objUnitSpace->lookup_code ) ) ? $objUnitSpace->lookup_code : '-' ),
						( ( string ) ( 0 < $objUnitSpace->optimal_rent ) ? $objUnitSpace->optimal_rent : '-' ),
						( ( string ) ( 0 < $objUnitSpace->override_rent ) ? $objUnitSpace->override_rent : '-' ),
						( ( true == isset( $objUnitSpace->override_reason ) ) ? $objUnitSpace->override_reason : '-' ),
						( $objUnitSpace->rule_exceeded_dollar ),
						( $objUnitSpace->rule_exceeded_percent ),
						( ( 0 < $objUnitSpace->optimal_rent ) ? 'text-align:right;' : 'text-align:center;' ),
						( ( 0 < $objUnitSpace->override_rent ) ? 'text-align:right;' : 'text-align:center;' ),
						( ( true == isset( $objUnitSpace->override_reason ) ) ? 'text-align:left;' : 'text-align:center;' ),
						( ( 0 < $objUnitSpace->rule_exceeded_dollar ) ? 'text-align:right;' : 'text-align:center;' ),
					] );
			}
			$strAwaitingApprovalHTML = str_replace( '<awaiting_approval_units/>', $strAwaitingApprovalUnitSpacesHtml, $strAwaitingApprovalHTML );
		}

		return $strAwaitingApprovalHTML;
	}

	public function getPricingNewlyAvailableReviewUnits( $arrintNewlyUnitSpaceIds ) {
		$arrmixNewlyAvailableUnitSpaces = ( array ) \Psi\Eos\Entrata\CRevenueOptimalRents::createService()->fetchRevenueOptimalRentsForNewlyAvailableUnitsByUnitSpaceIdsByUnitTypeIdsByPropertyIdsByCid( $arrintNewlyUnitSpaceIds, [ $this->getId() ], $this->getCid(), $this->m_objDatabase );
		if( false == valArr( $arrmixNewlyAvailableUnitSpaces ) ) {
			return [];
		}

		$arrintUnitTypeIds  = \getArrayFieldValuesByFieldName( 'unit_type_id', $arrmixNewlyAvailableUnitSpaces );

		$objRateLogsFilter = new CRateLogsFilter;
		$objRateLogsFilter->setCids( [ $this->getCid() ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $this->getId() ] );
		$objRateLogsFilter->setArOriginIds( [ \CArOrigin::AMENITY ] );
		$objRateLogsFilter->setPublishedUnitsOnly( false );
		$objRateLogsFilter->setMarketedUnitsOnly( true );
		$objRateLogsFilter->setUnitTypeIds( $arrintUnitTypeIds );
		$objRateLogsFilter->setUnitSpaceIds( $arrintNewlyUnitSpaceIds );
		$arrintAmenityRateLogs = ( array ) \CPricingRentReviewLibrary::getAmentiesAmountByRatelogFilter( $objRateLogsFilter, $this->m_objDatabase );

		foreach( $arrmixNewlyAvailableUnitSpaces as $arrmixNewlyAvailableUnitSpace ) {
			if( true == in_array( $arrmixNewlyAvailableUnitSpace['unit_space_id'], $arrintNewlyUnitSpaceIds ) ) {
				$intUnitSpaceAmenity = ( true == isset( $arrintAmenityRateLogs['unit_space'][$arrmixNewlyAvailableUnitSpace['unit_space_id']] ) ) ? $arrintAmenityRateLogs['unit_space'][$arrmixNewlyAvailableUnitSpace['unit_space_id']] : 0;
				$intUnitTypeAmenity  = ( true == isset( $arrintAmenityRateLogs['unit_type'][$arrmixNewlyAvailableUnitSpace['unit_type_id']] ) ) ? $arrintAmenityRateLogs['unit_type'][$arrmixNewlyAvailableUnitSpace['unit_type_id']] : 0;
				$intPropertyAmenity  = ( true == isset( $arrintAmenityRateLogs['property'][$arrmixNewlyAvailableUnitSpace['property_id']] ) ) ? $arrintAmenityRateLogs['property'][$arrmixNewlyAvailableUnitSpace['property_id']] : 0;

				$intAmenity = $intUnitSpaceAmenity + $intUnitTypeAmenity + $intPropertyAmenity;

				$objUnitSpace                  = new stdClass();
				$objUnitSpace->lookup_code     = $arrmixNewlyAvailableUnitSpace['lookup_code'];
				$objUnitSpace->unit_space_name = $arrmixNewlyAvailableUnitSpace['unit_space_name'];
				$objUnitSpace->optimal_rent    = round( $arrmixNewlyAvailableUnitSpace['std_optimal_rent'] ) + $intAmenity;
				$arrobjUnitSpaces[]            = $objUnitSpace;
			}
		}

		return $arrobjUnitSpaces;

	}

	public function getPricingNewlyAvailableReviewUnitsHtml() {
		$arrintNewlyUnitSpaceIds = $this->getRequiredParameters()['newly_available_unit_spaces'] ?? NULL;
		$arrobjUnitSpaces = $this->getPricingNewlyAvailableReviewUnits( $arrintNewlyUnitSpaceIds );

		$strPricingNewlyAvailableReviewHTML = __( '<tr>
          <td style="width:20px;"></td>
          <td>
            <table style="width:100%; border-collapse: collapse;" cellspacing="0" cellpadding="0">
              <thead class="responsive-hide">
              <tr style="text-transform: uppercase; color:#999; font-size:14px; text-align:left;">
                <th style="border: 1px solid #CCC; font-weight:400; padding:10px; width:90px;vertical-align:top;">Unit Space</th>
                <th style="border: 1px solid #CCC; font-weight:400; padding:10px; width:90px;vertical-align:top;">Unit Type</th>
                <th style="border: 1px solid #CCC; font-weight:400; padding:10px; width:104px;vertical-align:top;">Optimal Rent</th>
              </tr>
              </thead>
              <tbody class="responsive-table">
               <review_newly_available_units/>
              </tbody>
            </table>
          </td>
          <td style="width:20px;"></td>
        </tr>
        <tr>
          <td style="width:20px;"></td>
          <td style="height:1px; background:#CCC;"></td>
          <td style="width:20px;"></td>
        </tr>' );
		$strPricingNewlyAvailableReviewUnitSpacesHtml = '';
		if( true == \valArr( $arrobjUnitSpaces ) ) {
			foreach( $arrobjUnitSpaces as $objUnitSpace ) {
				$strPricingNewlyAvailableReviewUnitSpacesHtml .= __( ' <tr style="font-size:14px; text-align:left;">
          <td style="border: 1px solid #CCC; padding:10px;text-align: left;" data-th="unit Space">{%s,0}</td>
          <td style="border: 1px solid #CCC; padding:10px;text-align: left;" data-th="Unit Type">{%s,1}</td>
          <td style="border: 1px solid #CCC; padding:10px;text-align: right;" data-th="Optimal Rent">{%m,2,p:0}</td>
          </tr>',
					[
						( ( true == isset( $objUnitSpace->unit_space_name ) ) ? $objUnitSpace->unit_space_name : '-' ),
						( ( true == isset( $objUnitSpace->lookup_code ) ) ? $objUnitSpace->lookup_code : '-' ),
						( ( string ) ( 0 < $objUnitSpace->optimal_rent ) ? $objUnitSpace->optimal_rent : '-' ),
						( ( 0 < $objUnitSpace->optimal_rent ) ? 'text-align:right;' : 'text-align:center;' ),
					] );
			}
			$strAwaitingApprovalHTML = str_replace( '<review_newly_available_units/>', $strPricingNewlyAvailableReviewUnitSpacesHtml, $strPricingNewlyAvailableReviewHTML );
		}

		return $strAwaitingApprovalHTML;
	}

	public function checkIsStudentProperty( $objDatabase ) {
		if( true == valObj( $this, 'CProperty' ) && true == valObj( $objDatabase, CDatabase::class ) ) {
			$arrobjPropertyPreferences = $this->getPropertyPreferences();

			if( false == valArr( $arrobjPropertyPreferences ) ) {
				$arrobjPropertyPreferences = $this->fetchPropertyPreferencesByKeysKeyedByKey( [ 'ENABLE_SEMESTER_SELECTION' ], $objDatabase );
			}

			if( true == valArr( $this->getOccupancyTypeIds() ) && true == in_array( COccupancyType::STUDENT, $this->getOccupancyTypeIds() ) && true == getPropertyPreferenceValueByKey( 'ENABLE_SEMESTER_SELECTION', $arrobjPropertyPreferences ) ) {
				return true;
			}
		}
		return false;
	}

	public function fetchMarketingMediaAssociationsCountByMarketingMediaSubTypeId( $intMarketingMediaSubTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationCountByReferenceIdByMediaSubTypeIdByCid( $this->getId(), $intMarketingMediaSubTypeId, $this->getCid(), $objDatabase );
	}

	public function getResidentPortalLink() {
		$arrobjWebsites  = ( array ) CWebsites::createService()->fetchPluralWebsitesByPropertyIdsByCid( [ $this->getId() ], $this->m_intCid, $this->m_objDatabase );
		$strResidentPortalPrefix = ( true == defined( 'CONFIG_SECURE_HOST_PREFIX' ) && true == valStr( CONFIG_SECURE_HOST_PREFIX ) ) ? CONFIG_SECURE_HOST_PREFIX : 'https://';
		$strResidentPortalSuffix = ( true == defined( 'CONFIG_RESIDENT_PORTAL_SUFFIX' ) && true == valStr( CONFIG_RESIDENT_PORTAL_SUFFIX ) ) ? CONFIG_RESIDENT_PORTAL_SUFFIX : '.residentportal.com';
		if( false == valArr( $arrobjWebsites ) ) {
			return NULL;
		}
		$boolIsRp40Enable = false;

		// Single website association
		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrobjWebsites ) ) {
			foreach( $arrobjWebsites as $objWebsite ) {
				$strResidentDomain = $objWebsite->getSubDomain();
				if( 1 == $objWebsite->getResidentPortalService() ) {
					$boolIsRp40Enable = true;
					$strResidentEnrollmentLinkUrl = $strResidentPortalPrefix . $strResidentDomain . $strResidentPortalSuffix . '/app';
				} else {
					$strResidentEnrollmentLinkUrl = $strResidentPortalPrefix . $strResidentDomain . $strResidentPortalSuffix . '/resident_portal';
				}
				return true == valStr( $strResidentEnrollmentLinkUrl ) ? [ $strResidentEnrollmentLinkUrl, $boolIsRp40Enable, $objWebsite->getId() ]: NULL;
			}
		} else {
			$objPropertyPreference	= CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'RESIDENT_PORTAL_AUTO_LOGIN_DOMAIN', $this->getId(), $this->m_intCid, $this->m_objDatabase );
			// Multiple website association
			$boolAllResponsive = true;
			$boolAllRPEnabled = true;
			foreach( $arrobjWebsites as $objWebsite ) {
				if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && $objPropertyPreference->getValue() == $objWebsite->getSubDomain() ) {
					// With default website
					$strResidentDomain = $objWebsite->getSubDomain();
					if( 1 == $objWebsite->getResidentPortalService() ) {
						$boolIsRp40Enable = true;
						$strResidentEnrollmentLinkUrl = $strResidentPortalPrefix . $strResidentDomain . $strResidentPortalSuffix . '/app';
					} else {
						$strResidentEnrollmentLinkUrl = $strResidentPortalPrefix . $strResidentDomain . $strResidentPortalSuffix . '/resident_portal';
					}
					return true == valStr( $strResidentEnrollmentLinkUrl ) ? [ $strResidentEnrollmentLinkUrl, $boolIsRp40Enable, $objWebsite->getId() ]: NULL;
				} else {
					// Without default website
					if( 1 == $objWebsite->getResidentPortalService() ) {
						$boolAllResponsive = false;
					} else {
						$boolAllRPEnabled = false;
					}
				}
			}

			$objDefaultWebsite = CWebsites::createService()->fetchResidentPortalDefaultWebsiteByPropertyIdsByCid( [ $this->getId() ], $this->m_intCid, $this->m_objDatabase );

			$strResidentDomain = $objDefaultWebsite->getSubDomain();
			if( false == $boolAllRPEnabled && false == $boolAllResponsive ) {
				$boolIsRp40Enable = true;
				$strResidentEnrollmentLinkUrl = $strResidentPortalPrefix . $strResidentDomain . $strResidentPortalSuffix . '/app';
			} else if( true == $boolAllRPEnabled ) {
				$boolIsRp40Enable = true;
				$strResidentEnrollmentLinkUrl = $strResidentPortalPrefix . $strResidentDomain . $strResidentPortalSuffix . '/app';
			} else {
				$strResidentEnrollmentLinkUrl = $strResidentPortalPrefix . $strResidentDomain . $strResidentPortalSuffix . '/resident_portal';
			}
			return true == valStr( $strResidentEnrollmentLinkUrl ) ? [ $strResidentEnrollmentLinkUrl, $boolIsRp40Enable, $objDefaultWebsite->getId() ]: NULL;
		}
	}

	public function getViewOnEntrataHTML() {
		$objClient          = \Psi\Eos\Entrata\CClients::createService()->fetchClientById( $this->m_intCid, $this->m_objDatabase );
		$strViewResultsUri  = $objClient->getEntrataUrl() . '/?module=pricing_new_rent_reviewxxx';
		$strViewEntrataHTML = __( '<tr>
					<td style="width:20px;"></td>
					<td style="font-size:14px; line-height:16px; text-align: center;">
						<a href= " ' . $strViewResultsUri . ' "  target="_blank" style="color:#fff; font-size:12px; background:#40e87f; display:inline-block; padding:15px 20px; text-decoration:none; border-radius:1px; font-family: \'Montserrat\', sans-serif;">VIEW ON ENTRATA</a>
						<br><br>
					</td>
					<td style="width:20px;"></td>
				</tr>
				<tr><td style="height:30px;" colspan="3"></td></tr>
		' );

		return $strViewEntrataHTML;
	}

	public function fetchActiveMarketingMediaAssociationByMediaSubTypeId( $intMarketingMediaSubType, $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getId(), CMarketingMediaType::PROPERTY_MEDIA, $intMarketingMediaSubType, $this->getCid(), $objDatabase );
	}

	/**
	 * @param \CDatabase $objDatabase
	 * @return boolean boolIsLeaseByUnitSpace
	 * Check if property setting Lease By set to Unit Space
	 */
	public function checkLeaseByUnitSpace( CDatabase $objDatabase ) : bool {

		$intPropertyPreferenceValue = CPropertyPreferences::createService()->fetchPropertyPreferenceValueByPropertyPreferenceKeyByPropertyIdByCid( CPropertySettingKey::STUDENT_AVAILABILITY_LEASE_BY_KEY, $this->getId(), $this->getCid(), $objDatabase );

		if( valId( $intPropertyPreferenceValue ) && 2 == $intPropertyPreferenceValue ) {
			return true;
		}

		return false;
	}

}
?>
