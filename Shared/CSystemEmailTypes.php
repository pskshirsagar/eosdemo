<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CSystemEmailTypes
 * Do not add any new functions to this class.
 */

class CSystemEmailTypes extends CBaseSystemEmailTypes {

	const DEFAULT_HISTORY_LIMIT = 90;

    public static function fetchSystemEmailTypes( $strSql, $objDatabase ) {
        return parent::fetchCachedObjects( $strSql, 'CSystemEmailType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchCachedSystemEmailType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CSystemEmailType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = 60, $boolIsAppendDatabaseId = false );
    }

    public static function fetchAllSystemEmailTypes( $objDatabase, $boolViewConfidentialEmails = true ) {

    	$strWhereCondition = '';
    	if( false == $boolViewConfidentialEmails ) {
    		$strWhereCondition = ' AND is_confidential <> 1 ';
    	}

    	$strSql = 'SELECT * FROM system_email_types WHERE is_published = 1 ' . $strWhereCondition . 'ORDER BY name';

    	return parent::fetchSystemEmailTypes( $strSql, $objDatabase );
    }

    public static function fetchSystemEmailTypeById( $intId, $objDatabase ) {
    	return self::fetchSystemEmailType( 'SELECT * FROM system_email_types WHERE id = ' . ( int ) $intId, $objDatabase );
    }

    public static function fetchCachedSystemEmailTypeById( $intId, $objDatabase ) {
    	return self::fetchSystemEmailType( 'SELECT system_email_priority_id, email_service_provider_id FROM system_email_types WHERE id = ' . ( int ) $intId, $objDatabase );
    }

    public static function fetchAllPaginatedSystemEmailTypes( $intPageNo, $intPageSize, $objDatabase, $strOrderByField = 'id', $strOrderByType = 'ASC' ) {

    	$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
    	$intLimit = $intPageSize;

    	$strSql = 'SELECT * FROM system_email_types	ORDER BY' . ' ' . $strOrderByField . ' ' . $strOrderByType . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

    	return parent::fetchSystemEmailTypes( $strSql, $objDatabase );
    }

    public static function fetchAllPaginatedSystemEmailTypesCount( $objDatabase ) {

		$strSql = 'SELECT
						count(*)
					FROM
						system_email_types';

    	$arrintResponse = fetchData( $strSql, $objDatabase );

    	return $arrintResponse[0]['count'];
    }

    public static function fetchPaginatedSystemEmailTypes( $arrstrSystemEmailTypesFilter, $objDatabase ) {

    	$intOffset 			= ( true == isset( $arrstrSystemEmailTypesFilter['page_no'] ) && true == isset( $arrstrSystemEmailTypesFilter['page_size'] ) ) ? $arrstrSystemEmailTypesFilter['page_size'] * ( $arrstrSystemEmailTypesFilter['page_no'] - 1 ) : 0;
    	$intLimit 			= ( true == isset( $arrstrSystemEmailTypesFilter['page_size'] ) ) ? ( int ) $arrstrSystemEmailTypesFilter['page_size'] : '';
    	$strOrderByField 	= ( true == isset( $arrstrSystemEmailTypesFilter['order_by_field'] ) ) ? $arrstrSystemEmailTypesFilter['order_by_field'] : 'id';

    	if( true == isset( $arrstrSystemEmailTypesFilter['order_by_type'] ) ) {
    		$strOrderByType = ( 'asc' == $arrstrSystemEmailTypesFilter['order_by_type'] ) ? ' ASC' : ' DESC';
    	}

    	$strSql = '	SELECT
    					 *
    				FROM
    					system_email_types
    				ORDER BY ' . ' ' . $strOrderByField . ' ' . $strOrderByType . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

    	return parent::fetchSystemEmailTypes( $strSql, $objDatabase );
    }

	public static function fetchCustomSystemEmailTypesBySystemEmailCategoryId( $intSystemEmailCategoryId, $objDatabase, $strOrderByField = 'name', $strOrderByType = 'ASC' ) {
		return parent::fetchSystemEmailTypes( sprintf( 'SELECT * FROM system_email_types WHERE system_email_category_id = %d AND allow_disabling = TRUE ORDER BY ' . $strOrderByField . ' ' . $strOrderByType, ( int ) $intSystemEmailCategoryId ), $objDatabase );
	}

	public static function fetchSystemEmailTypeByIds( $strSystemEmailTypeIds, $objDatabase ) {

		if( true == empty( $strSystemEmailTypeIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						system_email_types
					WHERE
						id IN( ' . $strSystemEmailTypeIds . ' ) ';

		return parent::fetchSystemEmailTypes( $strSql, $objDatabase );
    }

	public static function fetchSystemEmailTypeLastSyncDateById( $intSystemEmailTypeId, $objDatabase ) {

		$strSql = ' SELECT
						name,
						last_synced_on
					FROM
						system_email_types
					WHERE
						id = ' . ( int ) $intSystemEmailTypeId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchClientSystemEmailTypesWithHistoryLimit( $objEmailDatabase ) {
		$strSql = 'SELECT
						se.system_email_type_id,
						' . self::DEFAULT_HISTORY_LIMIT . ' AS history_limit
					FROM
						system_emails se
						JOIN system_email_types set ON( set.id = se.system_email_type_id )
					WHERE
						cid IS NOT NULL
					GROUP BY
						system_email_type_id
					ORDER BY
						system_email_type_id';

		$arrmixResults = fetchData( $strSql, $objEmailDatabase );

		if( true == valArr( $arrmixResults ) ) {
			$arrmixResults = rekeyArray( 'system_email_type_id', $arrmixResults );
		}

		return $arrmixResults;
	}

	public static function fetchCustomCustomSystemEmailTypesBySystemEmailCategoryId( $arrintSystemEmailCategoryIds, $objDatabase, $arrintSystemEmailTypeIdsToExclude = [] ) {

    	if( false == valArr( $arrintSystemEmailCategoryIds ) ) return NULL;

    	$strSql = 'SELECT 
						* 
					FROM 
						system_email_types 
					WHERE 
						system_email_category_id IN( ' . implode( ',', $arrintSystemEmailCategoryIds ) . ' ) 
						AND allow_disabling = TRUE ';
		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrintSystemEmailTypeIdsToExclude ) ) {
			$strSql .= ' AND id NOT IN ( ' . implode( ',', $arrintSystemEmailTypeIdsToExclude ) . ' ) ';
		}

    	$strSql .= ' ORDER BY system_email_category_id, name ASC ';

		return parent::fetchSystemEmailTypes( $strSql, $objDatabase );
	}

}
?>