<?php

use Psi\Eos\Admin\CAdStats;
use Psi\Eos\Admin\CFileExtensions;

class CAd extends CBaseAd {

	use Psi\Libraries\EosFoundation\TEosStoredObject;

	protected $m_intAdTypeId;
	protected $m_intClickCount;
	protected $m_intViewCount;
	protected $m_strAdImage;
	protected $m_intCid;

    public function __construct() {
        parent::__construct();

		$this->m_intClickCount = 0;
		$this->m_intViewCount = 0;
    }

    /**
     * Create Functions
     */

	public function createAdStat() {

		$objAdStat = new CAdStat();
		$objAdStat->setAdId( $this->getId() );

		return $objAdStat;
	}

    /**
     * Get Functions
     */

	public function getAdTypeId() {
		return $this->m_intAdTypeId;
	}

	public function getClickCount() {
		return $this->m_intClickCount;
	}

	public function getViewCount() {
		return $this->m_intViewCount;
	}

	public function getAdImage() {
		return $this->m_strAdImage;
	}

    /**
     * Set Functions
     */

	public function setAdTypeId( $intAdTypeId ) {
		$this->m_intAdTypeId = $intAdTypeId;
	}

	public function setClickCount( $intClickCount ) {
		$this->m_intClickCount = $intClickCount;
	}

	public function setViewCount( $intViewCount ) {
		$this->m_intViewCount = $intViewCount;
	}

	public function setAdImage( $strAdImage ) {
		$this->m_strAdImage = $strAdImage;
	}

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['ad_type_id'] ) ) $this->setAdTypeId( $arrValues['ad_type_id'] );
		if( true == isset( $arrValues['click_count'] ) ) $this->setClickCount( $arrValues['click_count'] );
		if( true == isset( $arrValues['view_count'] ) ) $this->setViewCount( $arrValues['view_count'] );

		return;
    }

    /**
     * Validate Functions
     */

    public function valId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valAdLocationId() {
        $boolIsValid = true;

        if( true == is_null( $this->getAdLocationId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ad_location_id', 'Location is required.' ) );
        }

        return $boolIsValid;
    }

    public function valPsProductId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPsProductId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'Product is required.' ) );
        }

        return $boolIsValid;
    }

    public function valPsDocumentId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getPsDocumentId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_document_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valHref() {
        $boolIsValid = true;

        if( false == is_null( $this->getHref() ) && false == CValidation::checkUrl( $this->getHref() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'href', 'A valid advertise link is required.' ) );
        }

        return $boolIsValid;
    }

    public function valIsCurrent() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getIsCurrent() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_current', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getIsPublished() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', '' ) );
        // }

        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getOrderNum() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getDeletedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deleted_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getDeletedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deleted_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valHref();
            	$boolIsValid &= $this->valAdLocationId();
            	$boolIsValid &= $this->valPsProductId();
            	break;

            case VALIDATE_DELETE:
                break;

           	default:
           		// default case
           		$boolIsValid = true;
                break;
        }

        return $boolIsValid;
    }

    public function insert( $intCurrentUserId, $objAdminDatabase, $objClientDatabase = NULL ) {

    	if( !parent::insert( $intCurrentUserId, $objAdminDatabase ) ) {
    		return false;
    	}

	    if( !$this->insertOrUpdateStoredObject( $intCurrentUserId, $objAdminDatabase ) ) {
		    return false;
	    }

    	return true;
    }

	public function update( $intCurrentUserId, $objAdminDatabase, $boolReturnSqlOnly = false ) {

		if( !parent::update( $intCurrentUserId, $objAdminDatabase ) ) {
			return false;
		}

		if( !$this->insertOrUpdateStoredObject( $intCurrentUserId, $objAdminDatabase ) ) {
			return false;
		}

		return true;
	}

	/**
     * Other Functions
     */

	public function fetchAdStat( $objDatabase ) {
		return CAdStats::createService()->fetchAdStatByAdId( $this->getId(), $objDatabase );
	}

	public function fetchAdStatByDate( $strDate, $objDatabase ) {
		return CAdStats::createService()->fetchAdStatByAdIdByDate( $this->getId(), $strDate, $objDatabase );
	}

	public function fetchPsDocument( $objDatabase ) {
		$this->m_objPsDocument = CPsDocuments::fetchPsDocumentById( $this->getPsDocumentId(), $objDatabase );

		return $this->m_objPsDocument;
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function uploadObject( $objObjectStorageGateway, $strUploadPath ) {

		if( !valObj( $objObjectStorageGateway, 'IObjectStorageGateway' ) ) {
			trigger_error( __( 'Failed to get IObjectStorage' ), E_USER_WARNING );
			return false;
		}

		$arrmixGatewayRequest = $this->createPutGatewayRequest( [ 'data' => CFileIo::fileGetContents( $strUploadPath ) ] );
		if( valStr( $this->calcStorageContainer() ) ) {
			$arrmixGatewayRequest['container'] = $this->calcStorageContainer();
		}
		$objObjectStorageGatewayResponse = $objObjectStorageGateway->putObject( $arrmixGatewayRequest );
		if( $objObjectStorageGatewayResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to put storage object.' ) ) );
			return false;
		}

		$this->setObjectStorageGatewayResponse( $objObjectStorageGatewayResponse );
		return true;
	}

	protected function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {

		switch( $strVendor ) {

			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				$strPath = 'ads/' . $this->getId() . '/';
				break;

			default:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), 'ads/', date( 'Y' ), date( 'm' ), date( 'd' ), $this->getId() );
		}

		return $strPath . $this->getAdImage();
	}

	protected function calcStorageContainer( $strVendor = NULL ) {

		switch( $strVendor ) {

			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				return PATH_MOUNTS_DOCUMENTS;

			default:
				return CONFIG_OSG_BUCKET_DOCUMENTS;

		}

	}

	protected function createStoredObject() {
		return new \CStoredObject();
	}

	protected function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
		return \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( $this->getCid(), $this->getId(), static::TABLE_NAME, $strReferenceTag, $objDatabase );
	}

	public function downloadObject( $objObjectStorageGateway, $strFileName, $strDispositionType, $boolIsFullPath = false ) {

		$arrmixStorageArgs = $this->fetchStoredObject( $this->m_objDatabase )->createGatewayRequest( [ 'outputFile' => 'temp' ] );

		$arrobjObjectStorageResponse = $objObjectStorageGateway->getObject( $arrmixStorageArgs );
		if( $arrobjObjectStorageResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not present or has been deleted.' ) ) );
			return false;
		}

		$strFullPath = $arrobjObjectStorageResponse['outputFile'];

		if( $boolIsFullPath ) {
			return $strFullPath;
		}

		$arrstrFileInfo = pathinfo( $strFullPath );

		if( valArr( $arrstrFileInfo ) && !is_null( $arrstrFileInfo['extension'] ) ) {
			$objFileExtension = CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrFileInfo['extension'], $this->m_objDatabase );
		}

		if( valObj( $objFileExtension, 'CFileExtension' ) ) {
			header( 'Content-type: ' . $objFileExtension->getMimeType() );
		} else {
			header( 'Content-type: application/octet-stream' );
		}

		header( 'Pragma:public' );
		header( 'Expires:0' );
		header( 'Cache-Control:must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control:public' );
		header( 'Content-Description:File Transfer' );
		header( 'Content-Disposition: ' . $strDispositionType . '; filename="' . $strFileName . '"' );
		echo CFileIo::fileGetContents( $strFullPath );
		exit();
	}

	public function deleteObject( $objObjectStorageGateway, $intUserId ) {

		$arrmixGatewayRequest         = $this->fetchStoredObject( $this->m_objDatabase )->createGatewayRequest();
		$objDeleteObjectResponse      = $objObjectStorageGateway->deleteObject( $arrmixGatewayRequest );

		if( valObj( $objDeleteObjectResponse, 'CRouterObjectStorageGateway' ) && $objDeleteObjectResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to remove the file.' ) );
			return false;
		}

		if( !$this->deleteStoredObject( $intUserId, $this->m_objDatabase ) ) {
			return false;
		}

		return true;
	}

}
?>