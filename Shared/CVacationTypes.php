<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CVacationTypes
 * Do not add any new functions to this class.
 */

class CVacationTypes extends CBaseVacationTypes {

	public static function fetchVacationTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CVacationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchVacationType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CVacationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllVacationTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM vacation_types ORDER BY order_num';
		return self::fetchVacationTypes( $strSql, $objDatabase );
	}
}
?>