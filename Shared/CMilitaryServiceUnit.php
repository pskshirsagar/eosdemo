<?php

class CMilitaryServiceUnit extends CBaseMilitaryServiceUnit {

	protected $m_strMilitaryComponentName;

	public function getMilitaryComponentName() {
		return $this->m_strMilitaryComponentName;
	}

	public function setMilitaryComponentName( $strMilitaryComponentName ) {
		$this->m_strMilitaryComponentName = $strMilitaryComponentName;
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( isset( $arrValues['military_component_name'] ) && $boolDirectSet ) {
			$this->set( 'm_strMilitaryComponentName', trim( stripcslashes( $arrValues['military_component_name'] ) ) );
		} elseif( isset( $arrValues['military_component_name'] ) ) {
			$this->setMilitaryComponentName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['military_component_name'] ) : $arrValues['military_component_name'] );
		}

	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCode( $boolAddErrorMessage = true, $objDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->m_strCode ) ) {
			$boolIsValid = false;

			if( true == $boolAddErrorMessage ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'code', __( 'Unit Identification Code is required.' ) ) );
			}

		} else {
			if( 1 > \Psi\CStringService::singleton()->strlen( $this->m_strCode ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'code', __( 'Unit Id Code must have at least one letter.' ) ) );
			}
		}

		if( false == is_null( $this->getId() ) ) {
			$boolIsMilitaryServiceUnitCode = CMilitaryServiceUnits::fetchMilitaryServiceUnitsByCodeById( $this->getId(), $this->getCode(), $objDatabase );
		} else {
			$boolIsMilitaryServiceUnitCode = CMilitaryServiceUnits::fetchMilitaryServiceUnitsByCode( $this->getCode(), $objDatabase );
		}

		if( true == $boolIsMilitaryServiceUnitCode ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'code', __( 'Unit Identification Code with the \'{%s,0}\' already exists. ', [ $this->m_strCode ] ) ) );
		}

		return $boolIsValid;
	}

	public function valIdentifier( $boolAddErrorMessage = true, $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_strIdentifier ) ) {
			$boolIsValid = false;

			if( true == $boolAddErrorMessage ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'identifier', __( 'Unit Identifier is required.' ) ) );
			}

		} else {
			if( 1 > \Psi\CStringService::singleton()->strlen( $this->m_strIdentifier ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'identifier', __( 'Identifier must have at least one letter.' ) ) );
			}
		}
		if( false == is_null( $this->getId() ) ) {
			$boolIsMilitaryServiceUnitIdentifier = CMilitaryServiceUnits::fetchMilitaryServiceUnitsByIdentifierById( $this->getId(), $this->getIdentifier(), $objDatabase );
		} else {
			$boolIsMilitaryServiceUnitIdentifier = CMilitaryServiceUnits::fetchMilitaryServiceUnitsByIdentifier( $this->getIdentifier(), $objDatabase );
		}

		if( true == $boolIsMilitaryServiceUnitIdentifier ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'identifier', __( 'Unit Identifier with the \'{%s,0}\' already exists. ', [ $this->m_strIdentifier ] ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCode( true, $objDatabase );
				$boolIsValid &= $this->valIdentifier( true, $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>