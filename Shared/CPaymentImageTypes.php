<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPaymentImageTypes
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CPaymentImageTypes extends CBasePaymentImageTypes {

	public static function fetchPaymentImageTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CPaymentImageType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPaymentImageType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CPaymentImageType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllPaymentImagesTypes( $objDatabase ) {
		return self::fetchPaymentImageTypes( 'SELECT * FROM payment_image_types', $objDatabase );
	}

}
?>