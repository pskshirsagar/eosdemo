<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CTables
 * Do not add any new functions to this class.
 */
class CTables extends CBaseTables {

	public static function fetchPaginatedPublishedTables( $intPageNo, $intPageSize, $strOrderByField = NULL, $strOrderByType = NULL, $strSearchKeyword = NULL, $objTablesFilter, $objDatabase, $boolIsRdsSynced = false, $objAdminDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		if( false == is_null( $strOrderByField ) ) {
			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {
			$strOrderBy = ' ORDER BY t.updated_on DESC';
		}

		$strSql = 'SELECT
				        t.*,
				        d.database_name AS database_name
				    FROM
				        tables AS t
				        JOIN databases AS d ON ( d.id = t.database_id )
				    WHERE
				        t.is_published = 1';

		if( false == is_null( $strSearchKeyword ) ) {
			$strSql .= ' AND (  t.table_name ILIKE \'%' . $strSearchKeyword . '%\' OR t.description ILIKE \'%' . $strSearchKeyword . '%\' OR t.keywords ILIKE \'%' . $strSearchKeyword . '%\' ) ';
		}

		if( false == is_null( $objTablesFilter->getDatabaseIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objTablesFilter->getDatabaseIds() ) ) {
			$strSql .= ' AND t.database_id IN ( ' . implode( ', ', $objTablesFilter->getDatabaseIds() ) . ' )';
		}

		if( false == is_null( $objTablesFilter->getTeamIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objTablesFilter->getTeamIds() ) ) {
			$strSql .= ' AND t.team_id IN ( ' . implode( ', ', $objTablesFilter->getTeamIds() ) . ' )';
		}

		$strKeywords = $objTablesFilter->getKeywords();

		if( false == is_null( $strKeywords ) && false == empty( $strKeywords ) ) {
			$strSql .= ' AND t.keywords ILIKE \'%' . $strKeywords . '%\'';
		}

		if( true == $boolIsRdsSynced ) {
			$strSql .= ' AND t.is_cloud_sync IS TRUE ';
		}

		$strDescription = $objTablesFilter->getDescription();

		if( false == is_null( $strDescription ) && false == empty( $strDescription ) ) {
			$strSql .= ' AND t.description ILIKE \'%' . $strDescription . '%\'';
		}

		if( true == $objTablesFilter->getIsNoOwner() ) {
			$arrintTeamIds = array_column( ( array ) fetchdata( 'SELECT id FROM teams WHERE deleted_by IS NOT NULL', $objAdminDatabase ), 'id' );
			$strSql .= ' AND ( t.team_id IS NULL OR t.team_id = 0 ' . ( true == valArr( $arrintTeamIds ) ? 'OR t.team_id IN ( ' . implode( ',', $arrintTeamIds ) . ' ) ' : ' ' ) . ' )';
		}

		$strSql	.= $strOrderBy . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;

		return self::fetchTables( $strSql, $objDatabase );
	}

	public static function fetchAllTables( $objDatabase ) {
		return self::fetchTables( 'SELECT * FROM tables ORDER BY lower( table_name );', $objDatabase );
	}

	public static function fetchPaginatedPublishedTablesCount( $strSearchKeyword, $objTablesFilter, $objDatabase, $boolIsRdsSynced = false, $objAdminDatabase ) {

		$strWhere = ' WHERE is_published = 1';

		if( false == is_null( $strSearchKeyword ) ) {
			$strWhere .= ' AND ( table_name ILIKE \'%' . $strSearchKeyword . '%\' OR description ILIKE \'%' . $strSearchKeyword . '%\' OR keywords ILIKE \'%' . $strSearchKeyword . '%\' ) ';
		}

		if( false == is_null( $objTablesFilter->getDatabaseIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objTablesFilter->getDatabaseIds() ) ) {
			$strWhere .= ' AND database_id IN ( ' . implode( ', ', $objTablesFilter->getDatabaseIds() ) . ' )';
		}

		if( false == is_null( $objTablesFilter->getTeamIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objTablesFilter->getTeamIds() ) ) {
			$strWhere .= ' AND team_id IN( ' . implode( ', ', $objTablesFilter->getTeamIds() ) . ' )';
		}

		$strKeywords = $objTablesFilter->getKeywords();

		if( false == is_null( $strKeywords ) && false == empty( $strKeywords ) ) {
			$strWhere .= ' AND keywords ILIKE \'%' . $strKeywords . '%\'';
		}

		$strDescription = $objTablesFilter->getDescription();

		if( false == is_null( $strDescription ) && false == empty( $strDescription ) ) {
			$strWhere .= ' AND description ILIKE \'%' . $strDescription . '%\'';
		}

		if( true == $objTablesFilter->getIsNoOwner() ) {
			$arrintTeamIds = array_column( ( array ) fetchdata( 'SELECT id FROM teams WHERE deleted_by IS NOT NULL', $objAdminDatabase ), 'id' );
			$strWhere .= ' AND ( team_id IS NULL ' . ( true == valArr( $arrintTeamIds ) ? ' OR team_id IN ( ' . implode( ',', $arrintTeamIds ) . ' )' : '' ) . ' )';
		}

		if( true == $boolIsRdsSynced ) {
			$strWhere .= 'AND is_cloud_sync IS TRUE ';
		}

		return self::fetchTableCount( $strWhere, $objDatabase );
	}

	public static function fetchTablesByIds( $arrintTableIds, $objDatabase ) {

		if( false == valArr( $arrintTableIds ) ) return NULL;

		$strSql = 'Select * from tables where id IN ( ' . implode( ', ', $arrintTableIds ) . ' ) ORDER BY lower(table_name)';
		return self::fetchTables( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTablesByDatabaseId( $intPageNo, $intPageSize, $intDatabaseId, $objDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strSql = 'Select * from tables where database_id =' . ( int ) $intDatabaseId . ' ORDER BY id ASC OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit . ' ';

		return self::fetchTables( $strSql, $objDatabase );
	}

	public static function fetchTablesByDatabaseIds( $arrintDatabaseIds, $objDatabase ) {

		if( false == valArr( $arrintDatabaseIds ) ) return NULL;

		$strSql = 'SELECT * FROM tables WHERE database_id IN ( ' . implode( ', ', $arrintDatabaseIds ) . ' ) ORDER BY table_name ASC';

		return self::fetchTables( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTablesCountByDatabaseId( $intDatabaseId, $objDatabase ) {

		$strWhere = 'Where database_id = ' . ( int ) $intDatabaseId;

		return self::fetchTableCount( $strWhere, $objDatabase );
	}

	public static function fetchTablesByTableNamesByDatabaseIds( $arrstrTableNames, $arrintDatabaseIds, $objDatabase ) {

		if( false == valArr( $arrstrTableNames ) || false == valArr( $arrintDatabaseIds ) ) return NULL;

		$strSql = 'SELECT
   						t.id,
   						t.table_name
					FROM
   						tables t
					WHERE table_name IN  ( \'' . implode( '\', \'', $arrstrTableNames ) . '\' ) AND database_id  IN ( ' . implode( ',', $arrintDatabaseIds ) . ' ) ';

		return self::fetchTables( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTablesByDatabaseIds( $intPageNo, $intPageSize, $arrintDatabaseIds, $strTableName, $objDatabase ) {

		if( false == valArr( $arrintDatabaseIds ) ) return NULL;

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSql = 'SELECT
   						t.*
   					FROM
   						tables t
					WHERE
						t.database_id IN ( ' . implode( ',', $arrintDatabaseIds ) . ' )';

		if( false == empty( $strTableName ) ) {
			$strSql .= ' AND t.table_name like \'%' . $strTableName . '%\' ';
		}

		$strSql .= ' GROUP BY
						t.id ORDER BY t.table_name ASC OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return self::fetchTables( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTablesCountByDatabaseIds( $arrintDatabaseIds, $strTableName, $objDatabase ) {

		if( false == valArr( $arrintDatabaseIds ) ) return NULL;

		$strSql = 'SELECT
					count( * )
				FROM
   					tables t
				WHERE t.database_id IN ( ' . implode( ',', $arrintDatabaseIds ) . ' ) ';

		if( false == empty( $strTableName ) ) {
			$strSql .= ' AND t.table_name like \'%' . $strTableName . '%\'';
		}

		$arrintResponse = fetchData( $strSql, $objDatabase );
		return $arrintResponse[0]['count'];
	}

	public static function fetchTableByTableName( $strTableName, $objDatabase ) {
		$strSql = sprintf( '
				SELECT
					t.*
				FROM
					tables AS t
				WHERE
					t.database_id = ' . CDatabaseType::CLIENT . '
					AND t.table_name = %s',
			pg_escape_literal( $objDatabase->getHandle(), $strTableName ) );

		return self::fetchTable( $strSql, $objDatabase );
	}

}
?>