<?php

class CCardType extends CBaseCardType {

	const VISA = 5;
	const MASTERCARD = 6;
	const DISCOVER = 7;
	const AMERICAN_EXPRESS = 8;
	const DANKORT = 9;
	const DINERS = 10;
	const JCB = 11;
	const MAESTRO = 12;
	const VISA_ELECTRON = 13;

	public static $c_arrstrCardTypesWithCardPrefixAndLengths = [
		self::AMERICAN_EXPRESS => [ '34' => [ '15' ], '37' => [ '15' ] ],
		self::DINERS => [ '36' => [ '14-19' ], '300-305' => [ '16-19' ], '3095' => [ '16-19' ], '38-39' => [ '16-19' ] ],
		self::JCB => [ '3528-3589' => [ '16-19' ], '2131' => [ '16-19' ] ],
		self::DISCOVER => [ '6011' => [ '16-19' ], '622126-622925' => [ '16-19' ], '624000-626999' => [ '16-19' ], '628200-628899' => [ '16-19' ], '64' => [ '16-19' ], '65' => [ '16-19' ] ],
		self::DANKORT => [ '5019' => [ '16' ] ],
		self::MAESTRO => [ '6759'   => [ '12-19' ], '676770' => [ '12-19' ], '676774' => [ '12-19' ], '50' => [ '12-19' ], '56-69'  => [ '12-19' ] ],
		self::MASTERCARD => [ '2221-2720' => [ '16' ], '51-55' => [ '16' ] ],
		self::VISA_ELECTRON => [ '4026' => [ '13-19' ], '417500' => [ '13-19' ], '4405' => [ '13-19' ], '4508' => [ '13-19' ], '4844' => [ '13-19' ], '4913' => [ '13-19' ], '4917' => [ '13-19' ] ],
		self::VISA => [ '4' => [ '13-19' ] ],
	];

	public static $c_arrintCardTypeToPaymentTypeId = [
		self::VISA => CPaymentType::VISA,
		self::MASTERCARD => CPaymentType::MASTERCARD,
		self::DISCOVER => CPaymentType::DISCOVER,
		self::AMERICAN_EXPRESS => CPaymentType::AMEX,
		self::MAESTRO => CPaymentType::MASTERCARD,
		self::DINERS => CPaymentType::DISCOVER,
		self::JCB => CPaymentType::DISCOVER,
		self::DANKORT => CPaymentType::VISA,
		self::VISA_ELECTRON => CPaymentType::VISA
	];

	const STR_VISA					= 'Visa';
	const STR_MASTERCARD			= 'Mastercard';
	const STR_DISCOVER				= 'Discover';
	const STR_AMEX					= 'American Express';
	const STR_DANKORT               = 'Dankort';
	const STR_DINERS                = 'Diners';
	const STR_JCB                   = 'JCB';
	const STR_MAESTRO               = 'Maestro';
	const STR_VISA_ELECTRON             = 'Visa Electron';

	public static $c_arrintCardTypeToPaymentTypeName = [
		self::VISA => self::STR_VISA,
		self::MASTERCARD => self::STR_MASTERCARD,
		self::DISCOVER => self::STR_DISCOVER,
		self::AMERICAN_EXPRESS => self::STR_AMEX,
		self::DANKORT => self::STR_DANKORT,
		self::DINERS => self::STR_DINERS,
		self::JCB => self::STR_JCB,
		self::MAESTRO => self::STR_MAESTRO,
		self::VISA_ELECTRON => self::STR_VISA_ELECTRON
	];

	public static function determineCardTypeIdByCardNumber( $strCardNumber ) {
		if( false == \Psi\Libraries\UtilFunctions\valStr( $strCardNumber ) ) {
			return NULL;
		}

		$strCardNumber = str_replace( [ '-', ' ', '.' ], '', $strCardNumber );
		$strCardNumber = \Psi\CStringService::singleton()->preg_replace( '/[^0-9]/', '0', $strCardNumber );
		$strCardNumber = \Psi\CStringService::singleton()->str_pad( $strCardNumber, 6, '0', STR_PAD_RIGHT );

		$intFirstSixDigits   = ( int ) \Psi\CStringService::singleton()->substr( $strCardNumber, 0, 6 );
		$strCardNumberLength = \strlen( $strCardNumber );

		$intCardTypeId = NULL;
		$arrmixCardTypesWithNumbersAndLengths = \CCardType::$c_arrstrCardTypesWithCardPrefixAndLengths;

		foreach( $arrmixCardTypesWithNumbersAndLengths as $strCardNameKey => $arrmixCardInitials ) {
			foreach( $arrmixCardInitials as $intCardPrefix => $intCardLengths ) {
				$intCardPrefix		= ( string ) $intCardPrefix;
				if( \Psi\CStringService::singleton()->strpos( $intCardPrefix, '-' ) !== false ) {
					$arrintPrefixArray = explode( '-', $intCardPrefix );
					$intPrefixMin = ( int ) \Psi\CStringService::singleton()->str_pad( $arrintPrefixArray[0], 6, '0', STR_PAD_RIGHT );
					$intPrefixMax = ( int ) \Psi\CStringService::singleton()->str_pad( $arrintPrefixArray[1], 6, '9', STR_PAD_RIGHT );
				} else {
					$intPrefixMin = ( int ) \Psi\CStringService::singleton()->str_pad( $intCardPrefix, 6, '0', STR_PAD_RIGHT );
					$intPrefixMax = ( int ) \Psi\CStringService::singleton()->str_pad( $intCardPrefix, 6, '9', STR_PAD_RIGHT );
				}

				$boolIsValidPrefix = $intFirstSixDigits >= $intPrefixMin && $intFirstSixDigits <= $intPrefixMax;

				if( $boolIsValidPrefix ) {
					foreach( $intCardLengths as $intCardLength ) {
						if( \Psi\CStringService::singleton()->strpos( $intCardLength, '-' ) !== false ) {
							$arrintLength = explode( '-', $intCardLength );
							$intMinLength = ( int ) $arrintLength[0];
							$intMaxLength = ( int ) $arrintLength[1];
							$boolIsValidLength = $strCardNumberLength >= $intMinLength && $strCardNumberLength <= $intMaxLength;
						} else {
							$boolIsValidLength = $strCardNumberLength == ( int ) $intCardLength;
						}
						if( $boolIsValidLength ) {
							$intCardTypeId = $strCardNameKey;
							break 3;
						}
					}
				}
			}
		}

		return $intCardTypeId;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCardTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>