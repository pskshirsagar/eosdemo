<?php

class CCheckAccountType extends CBaseCheckAccountType {

	const BUSINESS_CHECKING		= 1;
	const BUSINESS_SAVINGS		= 2;
	const PERSONAL_CHECKING		= 3;
	const PERSONAL_SAVINGS		= 4;
	const OWNER_DISTRIBUTION	= 5;
	const OWNER_CONTRIBUTION	= 6;

	public static $c_arrintPublishedCheckAccountTypes = [
		self::BUSINESS_CHECKING,
		self::BUSINESS_SAVINGS,
		self::PERSONAL_CHECKING,
		self::PERSONAL_SAVINGS
	];

	public static $c_arrstrPublishedCheckAccountTypeNames = [
		self::BUSINESS_CHECKING		=> 'Business Checking',
		self::BUSINESS_SAVINGS		=> 'Business Savings',
		self::PERSONAL_CHECKING		=> 'Personal Checking',
		self::PERSONAL_SAVINGS		=> 'Personal Savings'
	];

	public static $c_arrintBusinessCheckAccountTypes = [
		self::BUSINESS_CHECKING,
		self::BUSINESS_SAVINGS
	];

	public static $c_arrintPersonalCheckAccountTypes = [
		self::PERSONAL_CHECKING,
		self::PERSONAL_SAVINGS
	];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getCheckAccountTypeNameById( $intCheckAccountTypeId ) {

		switch( $intCheckAccountTypeId ) {
			case self::BUSINESS_CHECKING:
				return __( 'Business Checking' );
				break;

			case self::BUSINESS_SAVINGS:
				return __( 'Business Savings' );
				break;

			case self::PERSONAL_CHECKING:
				return __( 'Personal Checking' );
				break;

			case self::PERSONAL_SAVINGS:
				return __( 'Personal Savings' );
				break;

			default:
				return NULL;
		}
	}

}
?>