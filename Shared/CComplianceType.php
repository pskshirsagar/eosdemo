<?php

class CComplianceType extends CBaseComplianceType {

	const DOCUMENTS			= 1;
	const INSURANCE			= 2;
	const CREDIT			= 3;
	const CRIMINAL			= 4;
	const EMPLOYEE_CRIMINAL	= 5;

	public static $c_arrintComplianceTypeIdsToVerify = [
		self::DOCUMENTS => self::DOCUMENTS,
		self::INSURANCE => self::INSURANCE
	];

	public function valId() {
		return true;
	}

	public function valName() {
		return true;
	}

	public function valDescription() {
		return true;
	}

	public function valIsPublished() {
		return true;
	}

	public function valOrderNum() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getComplianceTypeName( $intComplianceTypeId ) {

		$strComplianceTypeName = '';

		switch( $intComplianceTypeId ) {

			case self::DOCUMENTS:
				$strComplianceTypeName = 'Documents';
				break;

			case self::INSURANCE:
				$strComplianceTypeName = 'Insurance';
				break;

			case self::CREDIT:
				$strComplianceTypeName = 'Credit';
				break;

			case self::CRIMINAL:
				$strComplianceTypeName = 'Criminal';
				break;

			default:
				// default action

		}

		return $strComplianceTypeName;
	}

	public static function loadComplianceTypeNames() {

		$arrstrComplianceStatusNames[self::DOCUMENTS]	= 'Documents';
		$arrstrComplianceStatusNames[self::INSURANCE]	= 'Insurance';
		$arrstrComplianceStatusNames[self::CREDIT]	    = 'Credit';

		/*
		 Hiding this criminal functionality temperory
			$arrstrComplianceStatusNames[self::CRIMINAL]	= 'Criminal';
		 */

		return $arrstrComplianceStatusNames;
	}

}
?>