<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CExportBatchTypes
 * Do not add any new functions to this class.
 */

class CExportBatchTypes extends CBaseExportBatchTypes {

	public static function fetchExportBatchTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CExportBatchType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchExportBatchType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CExportBatchType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllExportBatchTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM export_batch_types';
		return self::fetchExportBatchTypes( $strSql, $objDatabase );
	}
}
?>