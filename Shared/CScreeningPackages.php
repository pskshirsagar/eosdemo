<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackages
 * Do not add any new functions to this class.
 */

class CScreeningPackages extends CBaseScreeningPackages {

	public static $c_arrmixTestCaseScreeningPackageIds = [
		64655,64648,64646,64658,64661,64636,64650,64656,64659,64649,64640,64653,64641,64654,64632,64657,64645,64668,64647,64642,64643,64660,64644
	];

	public static function fetchPublishedScreeningPackageByIdByCid( $intScreeningPackageId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						sp.*
					FROM
						screening_packages sp
					WHERE
						id = ' . ( int ) $intScreeningPackageId . '
						AND sp.cid = ' . ( int ) $intCid . '
						AND sp.is_published = 1';

		return self::fetchScreeningPackage( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningPackagesByCid( $intCid, $objDatabase, $boolIsActive = true, $boolSelectAllColumns = true, $boolIsFetchData = false ) {

		$strSelectColumns 	= ( true == $boolSelectAllColumns ) ? ' sp.* ' : ' sp.id ';
		$strWhereCondition 	= ( true == $boolIsActive ) ? ' AND sp.is_active = true ' : '';

		$strSql = 'SELECT ' .
		          $strSelectColumns . ',
					MIN(
							CASE
								WHEN spst.screening_data_provider_id is NULL THEN \'v1\'
								WHEN spst.screening_data_provider_id is NOT NULL THEN \'v2\'
							END
							) as package_version
					FROM
						screening_packages sp
						LEFT JOIN screening_package_screen_type_associations spst ON spst.screening_package_id = sp.id
						LEFT JOIN screening_package_types spt ON spt.id = sp.screening_package_type_id
					WHERE
						sp.is_published = 1
						AND spt.is_published = 1
						AND sp.cid = ' . ( int ) $intCid .
		          $strWhereCondition . '
					GROUP BY
						sp.id
					ORDER BY
						sp.created_on DESC';

		return ( true == $boolIsFetchData ) ? fetchData( $strSql, $objDatabase ) : self::fetchScreeningPackages( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningPackagesByIsActiveByCid( $boolIsActive, $intCid, $objDatabase, $arrintScreeningPackageIds = [] ) {

		$strSql = 'SELECT
						sp.*
					FROM
						screening_packages sp
						LEFT JOIN screening_package_types spt ON spt.id = sp.screening_package_type_id
					WHERE
						sp.cid = ' . ( int ) $intCid . ' 
						AND sp.is_published = 1
					    AND spt.is_published = 1
					    AND spt.id NOT IN ( ' . implode( ',', CScreeningPackageType::$c_arrintVAScreeningPackageTypes ) . ')
					    AND sp.id NOT IN (  ' . sqlIntImplode( self::$c_arrmixTestCaseScreeningPackageIds ) . ')';

		if( 'false' === $boolIsActive || true == is_null( $boolIsActive ) ) {
			$strSql .= ' AND sp.is_active = true';
		}

		if( true == valArr( $arrintScreeningPackageIds ) ) {
			$strSql .= ' AND sp.id IN ( ' . sqlIntImplode( $arrintScreeningPackageIds ) . ' ) ';
		}

		$strSql .= ' ORDER BY created_on DESC';

		return self::fetchScreeningPackages( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningPackagesByIdsByPropertyIdByCid( $arrintScreeningPackageIds, $intCid, $objDatabase ) {

		if( false == valArr( array_filter( $arrintScreeningPackageIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON ( spa.id )
						spa.screening_applicant_type_id,
						sp.id,
						sp.name,
						sp.external_name,
						CASE
							WHEN (
									SELECT
										( screen_type_id = ' . CScreenType::CREDIT . ' OR screen_type_id = ' . CScreenType::EVICTION . ' ) as screen_type_id
									FROM
										screening_package_screen_type_associations
									WHERE
										screen_type_id IN ( ' . CScreenType::CREDIT . ' , ' . CScreenType::EVICTION . ' )
										AND is_published = 1
										AND screening_package_id = sp.id
									LIMIT 1
								)
							THEN 0
							ELSE 1
						END AS is_criminal_only_enabled_for_no_ssn
					FROM
						screening_packages sp
						JOIN screening_package_applicants spa ON ( spa.screening_package_id = sp.id AND spa.is_published = 1 )
						JOIN screening_package_screen_type_associations spsta ON ( spsta.screening_package_id = spa.screening_package_id AND spsta.is_published = 1 )
					WHERE
						sp.id IN ( ' . implode( ',', $arrintScreeningPackageIds ) . ' )
						AND sp.cid = ' . ( int ) $intCid;

		return self::fetchObjects( $strSql, 'CScreeningPackage', $objDatabase, false );
	}

	public static function fetchScreeningPackagesByIdsCid( $arrintApplicantScreeningPackageIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicantScreeningPackageIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						sp.*
					FROM
						screening_packages sp
					WHERE
						id IN ( ' . implode( ',', $arrintApplicantScreeningPackageIds ) . ' )
						AND sp.cid = ' . ( int ) $intCid . '
						AND sp.is_published = 1';

		return self::fetchScreeningPackages( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningPackagesByIds( $arrintScreeningPackageIds, $objDatabase ) {

		if( false == valArr( $arrintScreeningPackageIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						sp.*
					FROM
						screening_packages sp
					WHERE
						id IN ( ' . implode( ',', $arrintScreeningPackageIds ) . ' )
						AND sp.is_published = 1';

		return self::fetchScreeningPackages( $strSql, $objDatabase );
	}

	public static function fetchAllScreeningPackagesByIdsCid( $arrintApplicantScreeningPackageIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicantScreeningPackageIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						sp.*
					FROM
						screening_packages sp
					WHERE
						id IN ( ' . implode( ',', $arrintApplicantScreeningPackageIds ) . ' )
						AND sp.cid = ' . ( int ) $intCid;

		return self::fetchScreeningPackages( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageNameByIdByCid( $intScreeningPackageId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						sp.name
					FROM
						screening_packages sp
					WHERE
						id = ' . ( int ) $intScreeningPackageId . '
						AND sp.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackageNamesByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						name
					FROM
						screening_packages
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_published = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function isPackagePreScreeningEnabled( $intPackageId, $objDatabase ) {

		$strSql = '	SELECT
						is_use_for_prescreening
					FROM
						screening_packages
					WHERE
						id = ' . ( int ) $intPackageId;

		$arrstrData = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrstrData ) && true == CStrings::strToBool( $arrstrData[0]['is_use_for_prescreening'] ) ) ? true : false;
	}

	public static function fetchPublishedScreeningPackagesByScreeningPackageTypeIdsByCid( $arrintScreeningPackageTypeIds, $intCid, $objDatabase, $arrintScreenTypeIds = NULL ) {

		$strWhereCondition = '';

		if( true == valArr( $arrintScreenTypeIds = getIntValuesFromArr( $arrintScreenTypeIds ) ) ) {
			$strWhereCondition = ' AND spsta.screen_type_id NOT IN ( ' . implode( ',',  $arrintScreenTypeIds ) . ' )';
		}

		$strSql = 'SELECT
						sp.*,
						st.id as screen_type_id,
						st.name as screen_type_name
					FROM
						screening_packages sp
						JOIN screening_package_screen_type_associations spsta ON ( spsta.screening_package_id = sp.id )
						JOIN screen_types st ON ( st.id = spsta.screen_type_id )

					WHERE
						sp.screening_package_type_id IN ( ' . implode( ',',  $arrintScreeningPackageTypeIds ) . ' )
						AND sp.cid = ' . ( int ) $intCid . '
						AND sp.is_published = 1
						AND sp.is_active = true
						' . $strWhereCondition . '
					ORDER BY
					 	sp.created_on DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllScreeningPackagesByPackageTypeIdsCid( $arrintPackageTypeIds, $intCid, $objDatabase, $boolIsVersion2ScreeningPackage = false ) {

		if( false == valArr( $arrintPackageTypeIds ) ) {
			return NULL;
		}

		$strJoinClause = $strWhereClause = '';

		if( true == $boolIsVersion2ScreeningPackage ) {

			$strJoinClause = ' JOIN screening_package_screen_type_associations spsta ON ( sp.id = spsta.screening_package_id ) ';
			$strWhereClause = ' AND spsta.screening_data_provider_id IS NOT NULL
								AND spsta.is_published = 1 ';
		}

		$strSql = 'SELECT
						sp.*
					FROM
						screening_packages sp
						' . $strJoinClause . '
					WHERE
						screening_package_type_id IN( ' . implode( ',', $arrintPackageTypeIds ) . ' )
						AND sp.cid = ' . ( int ) $intCid . '
						AND sp.is_published = 1
						AND sp.is_active = true' . $strWhereClause;

		return parent::fetchScreeningPackages( $strSql, $objDatabase );
	}

	public static function fetchPackageCountByIdByCidByNameByScreeningPackageTypeId( $intId, $intCid, $strScreeningPackageName, $intScreeningPackageTypeId, $objDatabase ) {

		$strSql = '	SELECT
						count( id )
					FROM
						screening_packages
					WHERE
						id <> ' . ( int ) $intId . '
						AND cid = ' . ( int ) $intCid . '
						AND is_published = 1
						AND is_active = true
						AND LOWER( name ) = \'' . str_replace( "'", "\'", \Psi\CStringService::singleton()->strtolower( $strScreeningPackageName ) ) . '\'
						AND screening_package_type_id = ' . ( int ) $intScreeningPackageTypeId;

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchAllPublishedScreeningPackagesByScreeningPackageTypeIdsByCid( $arrintScreeningPackageTypeIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintScreeningPackageTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
				   FROM
					    screening_packages
				   WHERE
						screening_package_type_id IN ( ' . implode( ',',  $arrintScreeningPackageTypeIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND is_published = 1
						AND is_active = true
					ORDER BY
					 	created_on DESC';

		return self::fetchScreeningPackages( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningPackagesByIdsByScreeningPackageTypeIdsByCid( $arrintScreeningPackageIds, $arrintScreeningPackageTypeIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintScreeningPackageIds ) || 0 == ( int ) $arrintScreeningPackageTypeIds ) {
			return NULL;
		}

		$strSql = 'SELECT
								*
						   FROM
							    screening_packages
						   WHERE
								id IN ( ' . implode( ',',  $arrintScreeningPackageIds ) . ' )
								AND screening_package_type_id IN ( ' . implode( ',',  $arrintScreeningPackageTypeIds ) . ' )
								AND cid = ' . ( int ) $intCid . '
								AND is_published = 1
							ORDER BY
							    created_on DESC';

		return self::fetchScreeningPackages( $strSql, $objDatabase );
	}

	public static function fetchPublishedScreeningPackagesByIdsByCid( $arrintScreeningPackageIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintScreeningPackageIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						sp.*
					FROM
						screening_packages sp
					WHERE
						is_published = 1
						AND id IN ( ' . implode( ',', $arrintScreeningPackageIds ) . ' )
						AND sp.cid = ' . ( int ) $intCid;

		return self::fetchScreeningPackages( $strSql, $objDatabase );
	}

	public static function fetchHeirarchicalScreeningPackagesByLatestScreeningPackageIdByCid( $intScreeningPackageId, $intCid, $objDatabase ) {
		$strSql = 'WITH RECURSIVE screening_packages_heirarchy (id) as
                    (
                      SELECT
                          id,
                          CID,
                          NAME,
                          parent_package_id,
                          is_use_for_prescreening,
                          screening_package_type_id,
                          is_all_must_pass_credit_and_income,
                          combine_condition_set_value,
                          created_on,
                          created_by
                      FROM
                          screening_packages
                      WHERE
                          id = ' . ( int ) $intScreeningPackageId . '
                          AND CID = ' . ( int ) $intCid . '
                      UNION ALL
                      SELECT
                          sp.id,
                          sp.cid,
                          sp.name,
                          sp.parent_package_id,
                          sp.is_use_for_prescreening,
                          sp.screening_package_type_id,
                          sp.is_all_must_pass_credit_and_income,
                          sp.combine_condition_set_value,
                          sp.created_on,
                          sp.created_by
                      FROM
                          screening_packages_heirarchy,
                          screening_packages sp
                      WHERE
                          sp.id = screening_packages_heirarchy.parent_package_id
                          AND sp.cid = screening_packages_heirarchy.cid)
                    SELECT
                        id,
                        CID,
                        NAME,
                        parent_package_id,
                        is_use_for_prescreening,
                        screening_package_type_id,
                        is_all_must_pass_credit_and_income,
                        combine_condition_set_value,
                        created_on,
                        created_by
                    FROM
                        screening_packages_heirarchy
                    ORDER BY
                        id DESC';

		return parent::fetchScreeningPackages( $strSql, $objDatabase );
	}

    public static function fetchScreeningPackageDetailsByIdByCid( $intScreeningPackageId, $intCid, $objDatabase ) {

        $strSql = '
                    WITH screen_type_data AS (
                    SELECT st2.name, st2.id AS screen_type_id, spsta2.prescreening_set_id, spsta2.screening_package_id,spsta2.screening_criteria_id
                    FROM screen_types st2
                    JOIN screening_package_screen_type_associations spsta2 ON ( st2.id = spsta2.screen_type_id )
                    WHERE
                        spsta2.screening_package_id = ' . ( int ) $intScreeningPackageId . '
                        AND spsta2.is_published = 1
                        GROUP BY st2.name, st2.id, spsta2.prescreening_set_id, spsta2.screening_package_id,spsta2.screening_criteria_id
                    ),
                    screening_package_applicant_details AS (
                    SELECT
                         array_to_string( array_agg ( DISTINCT ( sat.id ) ) , \',\' ) AS applicant_types,
                         spa.screening_package_id
                     FROM
                         screening_package_applicants spa
                         JOIN screening_applicant_types sat ON sat.id = spa.screening_applicant_type_id
                     WHERE    
                         spa.screening_package_id = ' . ( int ) $intScreeningPackageId . '
                         AND spa.is_published = 1 
                         GROUP BY screening_package_id
                    )
                   SELECT
                    sp.id,
                    sp.name,
                    sp.is_use_for_prescreening,
                    sp.combine_condition_set_value,
                    sp.is_used_for_filtering_pending_charges,
                    sct.name AS screening_cumulation_type,
                    spt.name AS Package_Type,
                    json_agg( json_build_object( initcap( std.name ), std.prescreening_set_id ) ORDER BY std.screen_type_id ) AS prescreening_set_details,
                    json_agg( json_build_object( std.screen_type_id, std.screening_criteria_id ) ) AS criteria_details,
                    array_to_string( array_agg ( DISTINCT ( std.screen_type_id ) ) , \',\' ) AS screen_types,
                    spad.applicant_types,
                    array_to_string( array_agg ( DISTINCT initcap( std.name ) ) , \',\' ) AS screen_type_names,
                    sp.screening_package_lease_type_ids
                   FROM
                      screening_packages sp
                      JOIN screen_type_data std ON std.screening_package_id = sp.id
                    JOIN screening_package_types spt ON spt.id = sp.screening_package_type_id
                    JOIN screening_cumulation_types sct ON sct.id = sp.screening_cumulation_type_id 
                    LEFT JOIN screening_package_applicant_details spad ON spad.screening_package_id = sp.id
                   WHERE
                    sp.id = ' . ( int ) $intScreeningPackageId . '
                    AND sp.cid = ' . ( int ) $intCid . '
                    AND sp.is_published = 1
                    AND sp.is_active = true
                    GROUP BY sp.id, sp.name, sp.is_use_for_rv_index_score, sp.is_use_for_prescreening, Package_Type, spad.applicant_types, screening_cumulation_type
                    ORDER BY applicant_types';

        $arrstrData = fetchData( $strSql, $objDatabase );

        return ( true == valArr( $arrstrData ) ) ? $arrstrData[0] : [];
    }

	public static function fetchPublishedScreeningPackagesByLeaseTypeIdByCid( $intLeaseTypeId, $intCid, $objDatabase ) {
		if( false == valId( $intLeaseTypeId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						id
					FROM
						screening_packages
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ' . $intLeaseTypeId . ' = ANY ( screening_package_lease_type_ids )
						AND screening_package_type_id <> ' . CScreeningPackageType::CORPORATE . '
						AND is_published = 1';

		return fetchData( $strSql, $objDatabase );
	}

}
?>