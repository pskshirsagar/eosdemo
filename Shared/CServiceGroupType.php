<?php

class CServiceGroupType extends CBaseServiceGroupType {

	const EXTERNAL            = 1;
	const INTERNAL            = 2;
	const RESIDENTPORTAL      = 3;
	const SITETABLET          = 4;
	const DOCSCAN             = 5;
	const LEADALERT           = 6;
	const PUBLIC_API          = 7;
	const ILS                 = 8;
	const ENTRATA_MAINTENANCE = 9;
	const RESIDENTINSURE      = 10;
	const VENDORACCESSS       = 11;
	const RESIDENTVERIFY      = 12;
	const ENTRATAPRICING      = 13;

	const SYSTEM_CODE_EXTERNAL            = 'external';
	const SYSTEM_CODE_INTERNAL            = 'internal';
	const SYSTEM_CODE_RESIDENTPORTAL      = 'rp';
	const SYSTEM_CODE_SITETABLET          = 'st';
	const SYSTEM_CODE_DOCSCAN             = 'ds';
	const SYSTEM_CODE_LEADALERT           = 'la';
	const SYSTEM_CODE_PUBLIC_API          = 'public';
	const SYSTEM_CODE_ILS                 = 'ils';
	const SYSTEM_CODE_ENTRATA_MAINTENANCE = 'em';
	const SYSTEM_CODE_RESIDENTINSURE      = 'rinsure';
	const SYSTEM_CODE_VENDORACCESS        = 'va';
	const SYSTEM_CODE_RESIDENTVERIFY      = 'rv';
	const SYSTEM_CODE_ENTRATAPRICING      = 'ep';

	public $m_strMajorApiVersion;

	public static $c_arrmixServiceGroupTypeMap = [
		self::SYSTEM_CODE_EXTERNAL            => 'External',
		self::SYSTEM_CODE_INTERNAL            => 'Internal',
		self::SYSTEM_CODE_RESIDENTPORTAL      => 'ResidentPortal',
		self::SYSTEM_CODE_SITETABLET          => 'SiteTablet',
		self::SYSTEM_CODE_DOCSCAN             => 'DocScan',
		self::SYSTEM_CODE_LEADALERT           => 'LeadAlert',
		self::SYSTEM_CODE_PUBLIC_API          => 'Public',
		self::SYSTEM_CODE_ILS                 => 'Ils',
		self::SYSTEM_CODE_ENTRATA_MAINTENANCE => 'EntrataMaintenance',
		self::SYSTEM_CODE_RESIDENTINSURE      => 'ResidentInsure',
		self::SYSTEM_CODE_VENDORACCESS        => 'VendorAccess',
		self::SYSTEM_CODE_RESIDENTVERIFY      => 'ResidentVerify',
		self::SYSTEM_CODE_ENTRATAPRICING      => 'EntrataPricing'
	];

	public static $c_arrintIntegrationResultEnabledServiceGroupTypeIds = [ self::EXTERNAL, self::RESIDENTPORTAL, self::SITETABLET, self::DOCSCAN ];
	public static $c_arrintStrictIntegrationLogServiceGroups = [ self::ILS ];

	public function __construct() {
		parent::__construct();
		// FIXME remove this static declaration once UI implemented at CA which allows user to select specific version.
		$this->m_intMajorApiVersionId = CApiVersion::TMP_DEFAULT_MAJOR_API_VERSION;
		return;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( false != isset( $arrmixValues['major_api_version'] ) ) $this->setMajorApiVersion( $arrmixValues['major_api_version'] );
	}

	public function setMajorApiVersion( $strMajorApiVersion ) {
		$this->m_strMajorApiVersion = $strMajorApiVersion;
	}

	public function getMajorApiVersion() {
		return $this->m_strMajorApiVersion;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>