<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPropertyTypes
 * Do not add any new functions to this class.
 */

class CPropertyTypes extends CBasePropertyTypes {

	public static function fetchPropertyTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CPropertyType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	// function returning array instead of object for property types. used in prospect portal

	public static function fetchPropertyTypesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		$strSql = 'SELECT
						DISTINCT ON ( pt.name )
						pt.id, pt.name
					FROM
						property_types pt
						JOIN properties p ON ( p.property_type_id = pt.id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pt.is_published=1
						ORDER by pt.name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllPropertyTypes( $objDatabase, $strOrderBy = 'order_num' ) {
		$strSql = 'SELECT * FROM property_types ORDER BY ' . $strOrderBy;
		return self::fetchPropertyTypes( $strSql, $objDatabase );
	}

	public static function fetchPropertyTypeByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT ON ( pt.id )
						pt.*
					FROM
						property_types pt
						JOIN properties p ON ( p.property_type_id = pt.id )
					WHERE
						p.id = ' . ( int ) $intPropertyId . '
						AND p.cid = ' . ( int ) $intCid;

		return self::fetchPropertyType( $strSql, $objDatabase );
	}

	// Function Added for fairfield.

	public static function fetchPublishedPropertyTypes( $objDatabase, $strOrderBy = NULL, $arrintPropertyTypeIds = [] ) {
		$strSql  = 'SELECT * FROM property_types WHERE is_published = 1';
		$strSql .= ( true == valArr( $arrintPropertyTypeIds ) ) ? ' OR id IN ( ' . implode( ',', $arrintPropertyTypeIds ) . ' )' : '';
		$strSql .= ( false == is_null( $strOrderBy ) ) ? ' ORDER BY ' . $strOrderBy : '';
		return self::fetchPropertyTypes( $strSql, $objDatabase );
	}

	public static function fetchPropertyTypeIdsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						pt.id
					FROM
						property_types pt
						JOIN properties p ON ( p.property_type_id = pt.id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pt.is_published=1
						ORDER by pt.id desc';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyTypesByPropertyTypeIds( $arrintExcludePropertyTypeIds, $objDatabase, $strOrderBy = 'order_num' ) {

		if( false == valArr( $arrintExcludePropertyTypeIds ) ) return NULL;

		$strSql  = 'SELECT * FROM property_types WHERE id NOT IN ( ' . implode( ',', $arrintExcludePropertyTypeIds ) . ' ) ORDER BY ' . $strOrderBy;
		return self::fetchPropertyTypes( $strSql, $objDatabase );
	}

	public static function fetchPropertyTypesByIds( $arrintPropertyTypeIds, $objDatabase ) {
		$strSql  = 'SELECT * FROM property_types WHERE id IN ( ' . implode( ',', $arrintPropertyTypeIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllPropertyTypesByIds( $arrintPropertyTypeIds, $objDatabase ) {
		$strSql  = 'SELECT * FROM property_types WHERE id IN ( ' . implode( ',', $arrintPropertyTypeIds ) . ' )';

		return self::fetchPropertyTypes( $strSql, $objDatabase );
	}

}
?>