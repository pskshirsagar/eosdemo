<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePolicyStatusTypes
 * Do not add any new functions to this class.
 */

class CInsurancePolicyStatusTypes extends CBaseInsurancePolicyStatusTypes {

	public static function fetchInsurancePolicyStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CInsurancePolicyStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchInsurancePolicyStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CInsurancePolicyStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public function fetchPublishedInsurancePolicyStatusTypes( $objInsurancePortalDatabase ) {
		$strSql = 'SELECT * FROM insurance_policy_status_types WHERE is_published = 1';
		return self::fetchInsurancePolicyStatusTypes( $strSql, $objInsurancePortalDatabase );
	}

	public static function fetchInsurancePolicyStatusTypesByIds( $arrintInsurancePolicyStatusTypeIds, $objInsurancePortalDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						insurance_policy_status_types
					WHERE
						is_published=1
						AND id IN( ' . implode( ',', $arrintInsurancePolicyStatusTypeIds ) . ' ) ';

		return self::fetchInsurancePolicyStatusTypes( $strSql, $objInsurancePortalDatabase );
	}

}
?>