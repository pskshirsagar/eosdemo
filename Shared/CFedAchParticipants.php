<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CFedAchParticipants
 * Do not add any new functions to this class.
 */

class CFedAchParticipants extends CBaseFedAchParticipants {

	public static function fetchFedAchParticipantByRoutingNumber( $strRoutingNumber, $objPaymentDatabase ) {
		return self::fetchFedAchParticipant( sprintf( 'SELECT * FROM %s WHERE routing_number = \'%s\'', 'fed_ach_participants', ( string ) $strRoutingNumber ), $objPaymentDatabase );
	}

	public static function fetchFedAchParticipantDetailsByRoutingNumber( $strRoutingNumber, $objDatabase ) {
		$strSql = 'SELECT
						customer_name, address, city, state_code, postal_code, phone_number
					FROM
						fed_ach_participants
					WHERE
						routing_number = \'' . addslashes( ( string ) ( $strRoutingNumber ) ) . '\'';

		$arrmixResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrmixResponse[0] ) ) return $arrmixResponse[0];

		return NULL;
	}

    public static function fetchPaginatedFedAchParticipants( $intPageNo, $intPageSize, $objAdminDatabase, $arrmixFedAchParticipantsFilter = NULL ) {
		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
    	$intLimit	= ( int ) $intPageSize;

    	$strSql = 'SELECT * FROM fed_ach_participants';

    	if( true == isset( $arrmixFedAchParticipantsFilter['order_by_field'] ) ) {
    		$strSql .= ' ORDER BY ' . $arrmixFedAchParticipantsFilter['order_by_field'] . ' ' . $arrmixFedAchParticipantsFilter['order_type'];
    	}

		$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;

    	return self::fetchFedAchParticipants( $strSql, $objAdminDatabase );
    }

    public static function fetchAllFedAchParticipants( $objPaymentDatabase ) {
     	$strSql = 'SELECT * FROM fed_ach_participants';
     	return self::fetchFedAchParticipants( $strSql, $objPaymentDatabase );
    }

    public static function fetchFedAchParticipantsBySearchFilters( $arrstrFilteredExplodedSearch, $objAdminDatabase ) {
    	$strSql = 'SELECT
						*
    				FROM
    					fed_ach_participants
    				WHERE
						customer_name ILIKE \'%' . implode( '%\' AND customer_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'  OR
						address ILIKE \'%' . implode( '%\' AND address ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' OR
						routing_number ILIKE \'%' . implode( '%\' AND routing_number ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' OR
						city ILIKE \'%' . implode( '%\' AND city ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' OR
						state_code ILIKE \'%' . implode( '%\' AND state_code ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' OR
						postal_code ILIKE \'%' . implode( '%\' AND postal_code ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
					ORDER BY
						customer_name ASC
					LIMIT
						15;';

		return self::fetchFedAchParticipants( $strSql, $objAdminDatabase );
    }
}
?>