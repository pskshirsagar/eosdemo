<?php

class CCompanyStatusType extends CBaseCompanyStatusType {

   	const PROSPECT				= 3;
	const CLIENT				= 4;
	const TERMINATED			= 5;
	const TEST_DATABASE			= 9;
	const SALES_DEMO			= 11;
	const CANCELLED				= 12;
	const EMAIL_SUBSCRIBER		= 13;
	const TEMPLATE				= 14;
	const TERMINATED_PROSPECT	= 5; // TERMINATED_PROSPECT consider same as TERMINATED and manager through "needs_follow_up" field in the ps_leads.
	const NOT_QUALIFIED			= 15;

	public static $c_arrintSelectableCompanyStatusTypeIds = [ self::PROSPECT, self::CLIENT, self::TEST_DATABASE, self::SALES_DEMO ];
	public static $c_arrintLeadCompanyStatusTypeIds = [ self::PROSPECT, self::CLIENT, self::TERMINATED, self::EMAIL_SUBSCRIBER, self::SALES_DEMO ];
	public static $c_arrintActiveLeadCompanyStatusTypeIds = [ self::PROSPECT, self::CLIENT, self::EMAIL_SUBSCRIBER ];
	public static $c_arrintActiveRelevantCompanyStatusTypeIds = [ self::PROSPECT, self::CLIENT, self::TERMINATED ];
	public static $c_arrintTestCompanyStatusTypeIds = [ self::TEST_DATABASE, self::SALES_DEMO, self::TEMPLATE ];
	public static $c_arrintClosedCompanyStatusTypeIds = [ self::CLIENT, self::TERMINATED ];
	public static $c_arrintIrrelevantCompanyStatusTypeIds = [ self::TEST_DATABASE, self::SALES_DEMO, self::CANCELLED, self::TEMPLATE ];
	public static $c_arrintNonTestCompanyStatusTypeIds = [ self::PROSPECT, self::CLIENT, self::TERMINATED, self::CANCELLED, self::EMAIL_SUBSCRIBER ];
	public static $c_arrintCompanyDomainStatusTypeIds = [ self::PROSPECT, self::CLIENT, self::EMAIL_SUBSCRIBER, self::SALES_DEMO ];

	public static $c_arrintProfileCompanyStatusTypeIds = [ self::CLIENT, self::TERMINATED, self::TEST_DATABASE, self::PROSPECT, self::SALES_DEMO, self::EMAIL_SUBSCRIBER, self::CANCELLED, self::NOT_QUALIFIED ];
	public static $c_arrintProfileLeadStatusTypeIds = [ self::PROSPECT, self::EMAIL_SUBSCRIBER, self::CANCELLED, self::TERMINATED, self::NOT_QUALIFIED ];
	public static $c_arrintLikelihoodPercentageCompanyStatusTypeIds = [ self::CLIENT, self::TEST_DATABASE, self::PROSPECT, self::SALES_DEMO, self::EMAIL_SUBSCRIBER ];
	public static $c_arrintTaskCompanyStatusTypeIds = [ self::TERMINATED, self::CANCELLED ];
	public static $c_arrintNonActiveCompanyStatusTypeIds = [ self::PROSPECT, self::TEST_DATABASE, self::SALES_DEMO ];
	public static $c_arrintStdTerminationDashboardExcludeCompanyStatusTypeIds = [ self::SALES_DEMO, self::TEST_DATABASE, self::TERMINATED, self::TEMPLATE ];
	public static $c_arrintClientTerminationDashboardExcludeCompanyStatusTypeIds = [ self::SALES_DEMO, self::TERMINATED, self::TEMPLATE ];

	const C_ARRSTRGACOMPANYSTATUSTYPES = [
		self::CLIENT			    => 'Client',
		self::TEST_DATABASE		    => 'Test',
		self::PROSPECT			    => 'Prospect',
		self::SALES_DEMO		    => 'Demo',
		self::TERMINATED            => 'Terminated',
		self::CANCELLED             => 'Cancelled',
		self::EMAIL_SUBSCRIBER      => 'Email Subscriber',
		self::TEMPLATE              => 'Template',
		self::TERMINATED_PROSPECT   => 'Terminated Prospect'
	];

	public static $c_arrstrBulkEmployeeAssignmentCompanyStatusTypes = [
		self::PROSPECT			=> 'Prospect',
		self::CLIENT			=> 'Client',
		self::TEST_DATABASE		=> 'Test Database',
		self::SALES_DEMO		=> 'Sales Demo',
		self::EMAIL_SUBSCRIBER	=> 'Email Subscriber',
		self::TERMINATED       => 'Terminated'
	];

	public static $c_arrintAllowApproveContractCompanyStatusTypes = [
		self::CLIENT,
		self::TEST_DATABASE,
		self::SALES_DEMO
	];

	public static $c_arrintDefaultCompanyStatusTypeIds = [
		CCompanyStatusType::CLIENT,
		CCompanyStatusType::PROSPECT,
		CCompanyStatusType::SALES_DEMO
	];

    public static $c_arrintTerminatedComapanyStatusTypeIds = [
        self::SALES_DEMO,
        self::TEST_DATABASE,
        self::TERMINATED,
        self::CANCELLED,
	    self::TEMPLATE
    ];

	public static $c_arrintNewSaleEmailCompanyStatusTypeIds = [
		self::PROSPECT,
		self::CLIENT,
		self::EMAIL_SUBSCRIBER,
		self::TERMINATED_PROSPECT
	];

	public static $c_arrintProspectingCompanyStatusTypeIds = [ self::PROSPECT, self::CLIENT ];

	/**
	 * Other Functions
	 */

	public static function getIntegrationActiveCompanyStatusTypeIds() {
		return [ self::PROSPECT, self::CLIENT, self::TEST_DATABASE ];
	}

	public static function companyStatusTypeIdToStr( $intCompanyStatusTypeId ) {
		switch( $intCompanyStatusTypeId ) {
			case self::PROSPECT:
				return 'Prospect';
			case self::CLIENT:
				return 'Client';
			case self::TERMINATED:
				return 'Terminated';
			case self::CANCELLED:
				return 'Cancelled';
			case self::TEST_DATABASE:
				return 'Test Database';
			case self::SALES_DEMO:
				return 'Sales Demo';
			case self::TEMPLATE:
				return 'Template';
			default:
				trigger_error( 'Invalid company status type id.', E_USER_WARNING );
				break;
		}
	}

	public static function leadStatusTypeIdToStr( $intCompanyStatusTypeId ) {
		switch( $intCompanyStatusTypeId ) {
			case self::PROSPECT:
				return 'New';
			case self::TERMINATED:
				return 'Terminated';
			case self::CANCELLED:
				return 'Cancelled';
			case self::EMAIL_SUBSCRIBER:
				return 'Email Subscriber';
			case self::NOT_QUALIFIED:
				return 'Not Qualified';
			default:
				return 'Lead';
		}
	}

	public static function strToCompanyStatusTypeId( $strCompanyStatusType ) {
		switch( $strCompanyStatusType ) {
			case 'Prospect':
				return self::PROSPECT;
			case 'Client':
				return self::CLIENT;
			default:
			   	trigger_error( 'Invalid company status type.', E_USER_WARNING );
				break;
		}
	}

}
?>