<?php

class CPrivacyItem extends CBasePrivacyItem {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valToken( $objDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->getToken() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'token', 'Privacy Item Token is required. ' ) );
		} elseif( ( false == preg_match( '/^[a-zA-Z0-9._]+$/', $this->getToken() ) && CPrivacyItemType::SERVICE == $this->getPrivacyItemTypeId() ) || ( false == preg_match( '/^[a-zA-Z0-9\[\]._]+$/', $this->getToken() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'token', 'Token contains invalid characters , only [a-zA-Z0-9._] are allowed. ' ) );
		}

		$intConflictingPrivacyItemCount = \Psi\Eos\Admin\CPrivacyItems::createService()->fetchPrivacyItemsCountByName( $this->m_intId, $this->sqlToken(), $objDatabase );

		if( true == valStr( $this->sqlToken() ) && 0 < $intConflictingPrivacyItemCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Token', 'Token is already exists.' ) );
		}
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Privacy Item Name is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( false == valStr( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Privacy Item Description is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrivacyItemTypeId() {
		$boolIsValid = true;

		if( false == valStr( $this->getPrivacyItemTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'privacy_item_type_id', 'Privacy Item Type is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valPrivacyItemGroupId() {
		$boolIsValid = true;

		if( false == valStr( $this->getPrivacyItemGroupId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'privacy_item_group_id', 'Privacy Item Group is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valIsDefaultState() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsStateEditable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDetailsEditable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_for_privacy_item':
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valToken( $objDatabase );
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valPrivacyItemTypeId();
				$boolIsValid &= $this->valPrivacyItemGroupId();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>