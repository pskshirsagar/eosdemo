<?php

class CInsurancePolicyStatusType extends CBaseInsurancePolicyStatusType {

	const INCOMPLETE         = 1;
	const ACTIVE             = 2;
	const CANCEL_PENDING     = 3;
	const LAPSED             = 4;
	const CANCELLED          = 5;
	const PAST_COVERAGE      = 6;
	const FUTURE_COVERAGE    = 7;
	const ENROLLED           = 8;
	const EXPIRED            = 9;
	const EXPIRATION_PENDING = 10;
	const NOT_LISTING        = 11;

	public static $c_arrintActiveInsurancePolicyStatusTypeIds = [ self::ACTIVE, self::ENROLLED ];
	public static $c_arrintAllActiveInsurancePolicyStatusTypeIds = [ self::ACTIVE, self::ENROLLED, self::CANCEL_PENDING ];
	public static $c_arrintActiveAndIncompleteInsurancePolicyStatusTypeIds = [ self::ACTIVE, self::ENROLLED, self::FUTURE_COVERAGE ];
	public static $c_arrintActiveAndCancelPendingInsurancePolicyStatusTypeIds = [ self::ACTIVE, self::CANCEL_PENDING ];
	public static $c_arrintInActiveInsurancePolicyStatusTypeIds = [ self::LAPSED, self::CANCELLED, self::EXPIRED ];
	public static $c_arrintNonActiveInsurancePolicyStatusTypeIds = [ self::LAPSED, self::CANCELLED ];
	public static $c_arrstrInsurancePolicyStatusNames = [ self::INCOMPLETE => 'Incomplete', self::ACTIVE => 'Active', self::CANCEL_PENDING => 'Cancel Pending', self::LAPSED => 'Lapsed', self::CANCELLED => 'Cancelled', self::PAST_COVERAGE => 'Past Coverage', self::FUTURE_COVERAGE => 'Future Coverage', self::ENROLLED => 'Enrolled', self::EXPIRED => 'Expired', self::EXPIRATION_PENDING => 'Expiration Pending', self::NOT_LISTING => 'Not Listing' ];
	public static $c_arrintCommissionIneligibleStatusTypeIds = [ self::INCOMPLETE, self::ENROLLED ];

}

?>