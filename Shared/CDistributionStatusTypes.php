<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CDistributionStatusTypes
 * Do not add any new functions to this class.
 */

class CDistributionStatusTypes extends CBaseDistributionStatusTypes {

	public static function fetchDistributionStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDistributionStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDistributionStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDistributionStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

    public static function fetchAllDistributionStatusTypes( $objDatabase ) {

    	$strSql = 'SELECT * FROM distribution_status_types';

    	return self::fetchDistributionStatusTypes( $strSql, $objDatabase );
    }
}
?>