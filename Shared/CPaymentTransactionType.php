<?php

class CPaymentTransactionType extends CBasePaymentTransactionType {

    const AVS_ONLY 						= 1;
    const AUTHORIZE 					= 2;
    const VOID 							= 3;
    const AUTH_CAPTURE 					= 4;
    const CAPTURE 						= 5;
    const CREDIT 						= 6;
    const RECALL						= 7;
    const CHARGED_BACK 					= 8;
    const QUEUE 						= 9;
    const BATCH 						= 10;
    const PSUEDO_CAPTURE				= 11;
    const FAILED_CAPTURE				= 12;
    const CHECK21_AMOUNT_ADJUSTMENT		= 13;
    const BANK_RETURN					= 14;
    const BANK_ADJUSTMENT				= 15;
    const CHARGE_BACK_INITIATED			= 16;
    const CHARGED_BACK_CLIENT_DISPUTED	= 17;

    const CANCEL						= 18;
	const REVERSE_PENDING				= 19;
	const REVERSE						= 20;
	const DELETE   						= 21;
	const ASSOCIATE						= 22;
	const DISASSOCIATE					= 23;
	const PHONE_AUTH_PENDING_EMAIL		= 24;
	const CUSTOMER_ACK_PROMISE_TO_PAY	= 25;
	const AUTH_REVERSAL					= 26;
	const FUNDS_RECEIVED_WESTERN_UNION	= 27;
	const FUNDS_RECEIVED_PAY_NEAR_ME	= 28;
	const REASSOCIATE					= 29;
	const DRAFT_CHARGEBACK_REQUEST		= 30;
	const TRANSFERRED					= 31;
	const GATEWAY_TIMEOUT				= 32;
	const CAPTURED_BY_SMS               = 33;
	const FINALIZE_3DSECURE				= 34;
}
?>