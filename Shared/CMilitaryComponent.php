<?php

class CMilitaryComponent extends CBaseMilitaryComponent {

	const AIRFORCE		   = 1;
	const ARMY			   = 2;
	const COASTGUARD	   = 3;
	const MARINECORPS	   = 4;
	const NATIONALGUARD    = 5;
	const NAVY 			   = 6;
	const AIRNATIONALGUARD = 7;
	const FOREIGNMILITARY  = 8;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>