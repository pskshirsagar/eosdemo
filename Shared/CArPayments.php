<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CArPayments
 * Do not add any new functions to this class.
 */

class CArPayments extends CBaseArPayments {

	public static function fetchArPaymentByAssociatedArPaymentIdByCid( $intArPaymentId, $intCid, $objPaymentDatabase ) {
		return self::fetchArPayment( 'SELECT * FROM view_ar_payments WHERE ar_payment_id = ' . ( int ) $intArPaymentId . ' AND cid = ' . ( int ) $intCid,  $objPaymentDatabase );
	}

	public static function fetchAssociatedArPaymentsByPaymentStatusTypeIdsByArPaymentIdByCid( $arrintPaymentStatusTypeIds, $intArPaymentId, $intCid, $objPaymentDatabase ) {
		return self::fetchArPayments( 'SELECT * FROM view_ar_payments WHERE payment_status_type_id IN ( ' . implode( ',', $arrintPaymentStatusTypeIds ) . ' ) AND ar_payment_id = ' . ( int ) $intArPaymentId . ' AND cid = ' . ( int ) $intCid,  $objPaymentDatabase );
	}

	public static function fetchAssociatedReversingArPaymentsByArPaymentIdByCid( $intArPaymentId, $intCid, $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM view_ar_payments ap WHERE ar_payment_id = ' . ( int ) $intArPaymentId . ' AND cid = ' . ( int ) $intCid . 'AND payment_status_type_id IN ( ' . CPaymentStatusType::REVERSED . ',' . CPaymentStatusType::REVERSAL_PENDING . ' )';
		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchReversedPaymentAmountByOriginalArPaymentIdByCid( $intArPaymentId, $intCid, $objDatabase ) {
		$strSql = 'SELECT sum( payment_amount ) as sum FROM ar_payments' . ' ap WHERE ar_payment_id = ' . ( int ) $intArPaymentId . ' AND cid = ' . ( int ) $intCid . ' AND payment_status_type_id IN ( ' . CPaymentStatusType::REVERSED . ',' . CPaymentStatusType::REVERSAL_PENDING . ' )';

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) && true == isset ( $arrstrData[0]['sum'] ) ) {
			return $arrstrData[0]['sum'];
		}

		return 0;
	}

	public static function fetchReversedConvenienceFeeAmountByOriginalArPaymentIdByCid( $intArPaymentId, $intCid, $objDatabase ) {
		$strSql = 'SELECT sum( convenience_fee_amount ) as sum FROM ar_payments' . ' ap WHERE ar_payment_id = ' . ( int ) $intArPaymentId . ' AND cid = ' . ( int ) $intCid . ' AND payment_status_type_id IN ( ' . CPaymentStatusType::REVERSED . ',' . CPaymentStatusType::REVERSAL_PENDING . ' ) ';

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) && true == isset ( $arrstrData[0]['sum'] ) ) {
			return $arrstrData[0]['sum'];
		}

		return 0;
	}

	public static function fetchCustomArPaymentById( $intId, $objPaymentDatabase ) {
		return self::fetchArPayment( 'SELECT * FROM view_ar_payments WHERE id = ' . ( int ) $intId, $objPaymentDatabase );
	}

	public static function fetchWorksArPaymentByIdByCid( $intId, $intCid, $objClientDatabase ) {
		return self::fetchArPayment( 'SELECT * FROM ar_payments WHERE id = ' . ( int ) $intId . ' AND cid = ' . ( int ) $intCid, $objClientDatabase );
	}

	public static function fetchCustomArPaymentsByArPaymentTransmissionIdByCid( $intArPaymentTransmissionId, $intCid, $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM  view_ar_payments WHERE ar_payment_transmission_id = ' . ( int ) $intArPaymentTransmissionId . ' AND cid = ' . ( int ) $intCid . ' ORDER BY order_num';
		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchCustomArPaymentsByArPaymentTransmissionIdByCompanyMenrchantAccountIdByPropertyIdsByCid( $intArPaymentTransmissionId, $arrintPropertyIds, $intCompanyMenrchantAccountId, $intCid, $objPaymentDatabase ) {
		$strPropertyWhere = ( true == valArr( $arrintPropertyIds ) ) ? ' AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )' : '';
		$strSql = 'SELECT * FROM  view_ar_payments WHERE ar_payment_transmission_id = ' . ( int ) $intArPaymentTransmissionId . $strPropertyWhere . ' AND company_merchant_account_id = ' . ( int ) $intCompanyMenrchantAccountId . '  AND cid = ' . ( int ) $intCid . ' ORDER BY order_num';
		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchArPaymentsByPropertyIdsByPaymentDateTimeByCid( $arrintPropertyIds, $strPaymentDateTime, $intCid, $objClientDatabase, $boolIncludeDeletedUnitSpaces = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		$strSql	= ' SELECT
						DATE_TRUNC( \'month\', ap.payment_datetime) AS payment_month,
						p.id,
						p.property_name,
						p.lookup_code,
						ap.property_id,
						ap.payment_type_id,
						pt.name,
						pa.state_code,
						pa.postal_code,
						COUNT(ap.payment_type_id),
						COUNT(us.id) AS units,
						SUM(ap.payment_amount)
					FROM
						ar_payments ap INNER JOIN properties AS p ON ( p.cid = ap.cid AND ap.property_id = p.id )
						INNER JOIN payment_types AS pt ON ap.payment_type_id = pt.id
						INNER JOIN property_addresses AS pa ON ( pa.cid = p.cid AND pa.property_id = p.id AND pa.is_alternate = false )
						LEFT JOIN leases AS l ON ( l.cid = ap.cid AND l.id = ap.lease_id )
						LEFT JOIN unit_spaces AS us ON ( us.cid = l.cid AND us.id = l.unit_space_id ' . $strCheckDeletedUnitSpacesSql . ' )
					WHERE
						ap.payment_status_type_id NOT IN ( ' . CPaymentStatusType::VOIDED . ', ' . CPaymentStatusType::DECLINED . ', ' . CPaymentStatusType::BATCHING . ' )
						AND ap.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND ap.cid = ' . ( int ) $intCid . '
						AND (' . $strPaymentDateTime . ')
						AND pa.address_type_id = ' . CAddressType::PRIMARY . '
					GROUP BY
						ap.property_id,
						payment_month,
						ap.payment_type_id,
						pt.name,
						p.property_name,
						p.id,
						p.lookup_code,
						pa.state_code,
						pa.postal_code
					ORDER BY
						ap.property_id,
						payment_month,
						ap.payment_type_id';

		$objDataset = $objClientDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return NULL;
		}

		$arrmixPaymentTypeCounts = [];
		if( 0 < $objDataset->getRecordCount() ) {

			while( false == $objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();
				$arrmixValues['payment_month']	= date( 'm/d/Y', strtotime( $arrmixValues['payment_month'] ) );
				$arrmixPaymentTypeCounts[$arrmixValues['property_id']][$arrmixValues['payment_month']][$arrmixValues['payment_type_id']] = $arrmixValues;
				$objDataset->next();
			}
		}

		$objDataset->cleanup();
		return $arrmixPaymentTypeCounts;
	}

	public static function fetchArPaymentsByArPaymentTransmissionIdByPropertyIdsByCompanyMerchantAccountIdByCid( $intArPaymentTransmissionId, $arrintPropertyIds, $intCompanyMerchantAccountId, $intCid, $objPaymentDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						view_ar_payments
					WHERE
						ar_payment_transmission_id = ' . ( int ) $intArPaymentTransmissionId . '
						AND cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . implode( ',', ( array ) $arrintPropertyIds ) . ' )
						AND company_merchant_account_id = ' . ( int ) $intCompanyMerchantAccountId . '
					ORDER BY
						order_num ';

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchCustomArPaymentsByX937FileId( $intX937FileId, $objPaymentDatabase ) {
		$strSql = 'SELECT DISTINCT ON ( ap.id ) ap.* FROM view_ar_payments ap , x937_check_detail_records cdr WHERE ap.id = cdr.ar_payment_id AND ap.cid = cdr.cid AND cdr.x937_file_id = ' . ( int ) $intX937FileId;
		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchRelatedArPaymentsByIdByArPaymentIdByCid( $intId, $intArPaymentId, $intPropertyId, $intCid, $objPaymentDatabase ) {

		$arrstrOrParameters = [];

		$strSql = 'SELECT * FROM ar_payments' . ' WHERE id <> ' . ( int ) $intId . ' AND cid = ' . ( int ) $intCid . ' AND property_id = ' . ( int ) $intPropertyId . ' AND ';

		if( true == is_numeric( $intId ) ) {
			$arrstrOrParameters[] = ' ar_payment_id = ' . ( int ) $intId . ' ';
		}

		if( true == is_numeric( $intArPaymentId ) ) {
			$arrstrOrParameters[] = ' id = ' . ( int ) $intArPaymentId . ' ';
		}

		if( true == valArr( $arrstrOrParameters ) ) {
			$strSql .= '( ' . implode( ' OR ', $arrstrOrParameters ) . ' )';
		} else {
			return NULL;
		}

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchDuplicateTerminalArPayment( $objArPayment, $objPaymentDatabase ) {

		if( false == valObj( $objArPayment, 'CArPayment' ) ) {
			trigger_error( 'CArPayment object not valid', E_USER_ERROR );
			exit;
		}

		if( false == CPaymentTypes::isElectronicPayment( $objArPayment->getPaymentTypeId() ) ) return;
		if( true == is_null( $objArPayment->getCheckNumber() ) || 1 == $objArPayment->getCheckIsMoneyOrder() ) return true;

		$arrintMatchPaymentStatusTypeIds = [ CPaymentStatusType::CAPTURED, CPaymentStatusType::CAPTURING, CPaymentStatusType::AUTHORIZED, CPaymentStatusType::AUTHORIZING ];

		switch( $objArPayment->getPaymentTypeId() ) {

			case CPaymentType::CHECK_21:
			case CPaymentType::ACH:
			case CPaymentType::PAD:

				$strSql = 'SELECT * FROM view_ar_payments
							WHERE
								cid = ' . ( int ) $objArPayment->getCid() . '
								AND check_account_number_encrypted = \'' . addslashes( $objArPayment->getCheckAccountNumberEncrypted() ) . '\'
								AND check_routing_number = \'' . addslashes( $objArPayment->getCheckRoutingNumber() ) . '\'';

				if( false == is_null( $objArPayment->getCheckAuxillaryOnUs() ) ) {
					$strSql .= ' AND check_auxillary_on_us = \'' . addslashes( $objArPayment->getCheckAuxillaryOnUs() ) . '\'';
				}

				if( false == is_null( $objArPayment->getCheckNumber() ) ) {
					$strSql .= ' AND check_number = \'' . addslashes( $objArPayment->getCheckNumber() ) . '\'';
				}

				$strSql .= ' AND payment_datetime > ( NOW() - INTERVAL \'90 Days\' )
								AND payment_type_id IN ( ' . $objArPayment->getPaymentTypeId() . ' )
								AND payment_status_type_id IN ( ' . implode( ',', $arrintMatchPaymentStatusTypeIds ) . ' )
								AND id <> ' . ( int ) $objArPayment->getId() . '
							LIMIT 1';

				return self::fetchArPayment( $strSql, $objPaymentDatabase );
				break;

			case CPaymentType::EMONEY_ORDER:
				// We shouldn't allow a payment with the same MTCN (bill_to_account_numer) to ever be processed more than 1 time
				$strSql = 'SELECT * FROM view_ar_payments
							WHERE
								cid = ' . ( int ) $objArPayment->getCid() . '
								AND payment_datetime > ( NOW() - \'INTERVAL 1 Years\' )
								AND payment_type_id IN ( ' . CPaymentType::EMONEY_ORDER . ' )
								AND payment_status_type_id IN ( ' . implode( ',', $arrintMatchPaymentStatusTypeIds ) . ' )
								AND id <> ' . ( int ) $objArPayment->getId() . '
								AND bill_to_account_number = \'' . $objArPayment->getBillToAccountNumber . '\'
							LIMIT 1';

				return self::fetchArPayment( $strSql, $objPaymentDatabase );
				break;

			default:
				// This section is for cc payments.
				// We shouldn't allow the same cc payment to be processed twice for 5 minutes
				$strSql = 'SELECT * FROM view_ar_payments
							WHERE
								cid = ' . ( int ) $objArPayment->getCid() . '
								AND cc_card_number_encrypted = \'' . addslashes( $objArPayment->getCcCardNumberEncrypted() ) . '\'
								AND payment_datetime > ( NOW() - INTERVAL \'5 Minutes\' )
								AND payment_amount = ' . ( float ) $objArPayment->getPaymentAmount() . '
								AND payment_type_id NOT IN ( ' . CPaymentType::CHECK_21 . ', ' . CPaymentType::ACH . ', ' . CPaymentType::PAD . ', ' . CPaymentType::HAP . ', ' . CPaymentType::BAH . ' )
								AND payment_status_type_id IN ( ' . implode( ',', $arrintMatchPaymentStatusTypeIds ) . ' )
								AND id <> ' . ( int ) $objArPayment->getId() . '
							LIMIT 1';

				return self::fetchArPayment( $strSql, $objPaymentDatabase );
				break;
		}

		return;
	}

	public static function fetchDistributedArPaymentsBySettlementDistributionIdPaymentDistributionTypeIdByCid( $intSettlementDistributionId, $intPaymentDistributionTypeId, $intCid, $objPaymentDatabase ) {

		$strSql = 'SELECT ap.* FROM view_ar_payments ap
					  LEFT JOIN ar_payment_distributions apd
						ON apd.ar_payment_id = ap.id AND apd.cid = ap.cid
					  LEFT JOIN settlement_distributions sd
						ON apd.settlement_distribution_id = sd.id AND apd.cid = sd.cid
					 WHERE sd.id = ' . ( int ) $intSettlementDistributionId . '
					   AND sd.cid = ' . ( int ) $intCid . '
					   AND apd.payment_distribution_type_id = ' . ( int ) $intPaymentDistributionTypeId;

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchArPaymentsBySettlementDistributionIdByCid( $intSettlementDistributionId, $intCid, $objPaymentDatabase ) {

		$strSql = 'SELECT
						ap.*
					FROM
						ar_payments ap,
						ar_payment_distributions apd,
						settlement_distributions sd
					 WHERE
					 	ap.id = apd.ar_payment_id
						AND ap.cid = apd.cid
					 	AND apd.settlement_distribution_id = sd.id
						AND apd.cid = sd.cid
					 	AND sd.id = ' . ( int ) $intSettlementDistributionId . '
					 	AND sd.cid = ' . ( int ) $intCid;

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchArPaymentCountBySettlementDistributionIdByCid( $intSettlementDistributionId, $intCid, $objPaymentDatabase ) {

		$strSql = 'SELECT
						count(ap.id)
					FROM
						ar_payments ap,
						ar_payment_distributions apd,
						settlement_distributions sd
					 WHERE
					 	ap.id = apd.ar_payment_id
						AND ap.cid = apd.cid
					 	AND apd.settlement_distribution_id = sd.id
						AND apd.cid = sd.cid
					 	AND sd.id = ' . ( int ) $intSettlementDistributionId . '
					 	AND sd.cid = ' . ( int ) $intCid;

		$arrstrData = fetchData( $strSql, $objPaymentDatabase );

		if( true == isset ( $arrstrData[0]['count'] ) ) {
			return ( int ) $arrstrData[0]['count'];
		} else {
			return 0;
		}
	}

	public static function fetchAlreadyExportedArPaymentCountBySettlementDistributionIdByCid( $intSettlementDistributionId, $intCid, $objPaymentDatabase ) {

		$strSql = 'SELECT
						count(ap.id)
					FROM
						ar_payments ap,
						ar_payment_distributions apd,
						settlement_distributions sd
					 WHERE
					 	ap.id = apd.ar_payment_id
						AND ap.cid = apd.cid
					 	AND apd.settlement_distribution_id = sd.id
						AND apd.cid = sd.cid
					 	AND sd.id = ' . ( int ) $intSettlementDistributionId . '
					 	AND sd.cid = ' . ( int ) $intCid . '
						AND ap.ar_payment_export_id IS NOT NULL';

		$arrstrData = fetchData( $strSql, $objPaymentDatabase );

		if( true == isset ( $arrstrData[0]['count'] ) ) {
			return ( int ) $arrstrData[0]['count'];
		} else {
			return 0;
		}
	}

	public static function fetchArPaymentByLeaseIdByIdByCid( $intLeaseId, $intId, $intCid, $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM view_ar_payments WHERE cid = ' . ( int ) $intCid . ' AND lease_id = ' . ( int ) $intLeaseId . ' AND id = ' . ( int ) $intId;
		return self::fetchArPayment( $strSql, $objPaymentDatabase );
	}

	public static function fetchCustomArPaymentsByIds( $arrintArPaymentIds, $objDatabase, $boolUseView = false ) {
		if( false == valArr( $arrintArPaymentIds ) ) return NULL;
		$strArPaymentsTable = ( CDatabaseType::PAYMENT == $objDatabase->getDatabaseTypeId() && true == $boolUseView ) ? 'view_ar_payments' : 'ar_payments';

		$strSql = 'SELECT *	FROM ' . $strArPaymentsTable . ' WHERE id IN (' . implode( ',', $arrintArPaymentIds ) . ') AND cid = cid';
		return self::fetchArPayments( $strSql, $objDatabase );
	}

	public static function fetchArPaymentsByIdsByCid( $arrintArPaymentIds, $intCid, $objDatabase, $boolUseView = false ) {
		if( false == valArr( $arrintArPaymentIds ) ) return NULL;
		$strArPaymentsTable = ( CDatabaseType::PAYMENT == $objDatabase->getDatabaseTypeId() && true == $boolUseView ) ? 'view_ar_payments' : 'ar_payments';

		$strSql = 'SELECT *	FROM ' . $strArPaymentsTable . ' WHERE cid = ' . ( int ) $intCid . ' AND id IN (' . implode( ',', $arrintArPaymentIds ) . ')';
		return self::fetchArPayments( $strSql, $objDatabase );
	}

	public static function fetchArPaymentsByLeaseIdPaymentStatusTypeIdsByCid( $intLeaseId, $arrintPaymentStatusTypeIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPaymentStatusTypeIds ) ) return NULL;
		return self::fetchArPayments( 'SELECT * FROM ar_payments WHERE payment_status_type_id in (' . implode( ',', $arrintPaymentStatusTypeIds ) . ') AND lease_id = ' . ( int ) $intLeaseId . ' AND cid = ' . ( int ) $intCid, $objClientDatabase );
	}

	public static function fetchArPaymentsByLeaseIdsByCid( $arrintLeaseIds, $intCid, $objClientDatabase ) {
		return self::fetchArPayments( 'SELECT * FROM ar_payments WHERE lease_id in (' . implode( ',', $arrintLeaseIds ) . ') AND cid = ' . ( int ) $intCid, $objClientDatabase );
	}

	public static function fetchArPaymentsCountByPaymentStatusTypeIdsByLeaseIdsByCid( $arrintPaymentStatusTypeIds, $arrintLeaseIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintLeaseIds ) ) return 0;

		$strSql = 'SELECT
						count(id)
					FROM
						ar_payments
					WHERE
						payment_status_type_id IN (' . implode( ',', $arrintPaymentStatusTypeIds ) . ')
					AND	lease_id IN (' . implode( ',', $arrintLeaseIds ) . ')
					AND cid = ' . ( int ) $intCid;

		$arrmixData = fetchData( $strSql, $objClientDatabase );

		if( true == valArr( $arrmixData ) && true == isset ( $arrmixData[0]['count'] ) ) {
			return $arrmixData[0]['count'];

		} else {
			return 0;
		}
	}

	public static function fetchArPaymentsCountByLeaseIdsByCid( $arrintLeaseIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintLeaseIds ) ) return 0;

		$strSql = 'SELECT
						count(id)
					FROM
						ar_payments
					WHERE
						lease_id IN (' . implode( ',', $arrintLeaseIds ) . ')
					AND cid = ' . ( int ) $intCid;

		$arrmixData = fetchData( $strSql, $objClientDatabase );

		if( true == valArr( $arrmixData ) && true == isset ( $arrmixData[0]['count'] ) ) {
			return $arrmixData[0]['count'];

		} else {
			return 0;
		}
	}

	public static function fetchArPaymentsByLeaseIdsByExcludingPaymentStatusTypeIdsByCid( $arrintLeaseIds, $arrintPaymentStatusTypeIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPaymentStatusTypeIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ar_payments
				   WHERE
				   		payment_status_type_id NOT IN (' . implode( ',', $arrintPaymentStatusTypeIds ) . ')
				   		AND lease_id IN (' . implode( ',', $arrintLeaseIds ) . ')
				   		AND cid = ' . ( int ) $intCid;

		return self::fetchArPayments( $strSql, $objClientDatabase );
	}

	public static function fetchArPaymentsByLeaseIdsByPaymentStatusTypeIdsByCid( $arrintLeaseIds, $arrintPaymentStatusTypeIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPaymentStatusTypeIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ar_payments
				   WHERE
				   		payment_status_type_id IN (' . implode( ',', $arrintPaymentStatusTypeIds ) . ')
				   		AND lease_id IN (' . implode( ',', $arrintLeaseIds ) . ')
				   		AND cid = ' . ( int ) $intCid;

		return self::fetchArPayments( $strSql, $objClientDatabase );
	}

	public static function fetchCustomArPaymentsByLeaseIdByCid( $intLeaseId, $intCid, $objClientDatabase ) {
		return self::fetchArPayments( 'SELECT * FROM ar_payments WHERE cid = ' . ( int ) $intCid . ' AND lease_id = ' . ( int ) $intLeaseId, $objClientDatabase );
	}

	public static function fetchCustomViewArPaymentsByIds( $arrintArPaymentIds, $objPaymentDatabase ) {
		if( false == valArr( $arrintArPaymentIds ) ) return NULL;
		return self::fetchArPayments( 'SELECT * FROM view_ar_payments WHERE id IN ( ' . implode( ',', $arrintArPaymentIds ) . ' )', $objPaymentDatabase );
	}

	public static function fetchViewArPaymentsByIdsByCid( $arrintArPaymentIds, $intCid, $objPaymentDatabase ) {
		if( false == valArr( $arrintArPaymentIds ) ) return NULL;
		return self::fetchArPayments( 'SELECT * FROM view_ar_payments WHERE id IN ( ' . implode( ',', $arrintArPaymentIds ) . ' ) AND cid = ' . ( int ) $intCid, $objPaymentDatabase );
	}

	public static function fetchSimpleArPaymentsByIdsByCid( $arrintArPaymentIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintArPaymentIds ) ) return NULL;
		return self::fetchArPayments( 'SELECT * FROM ar_payments WHERE cid = ' . ( int ) $intCid . ' AND id IN ( ' . implode( ',', $arrintArPaymentIds ) . ' )', $objDatabase );
	}

	public static function fetchArPaymentsPenetrationByPropertyIdsByPaymentTypeIdsByPaymentDateTimeByCid( $arrintPropertyIds, $arrintPaymentTypeIds, $strStartDate, $strEndDate, $intCid, $objClientDatabase, $boolIncludeDeletedUnitSpaces = false ) {

		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		if( false == valArr( $arrintPaymentTypeIds ) ) return NULL;

		$strSql = 'SELECT
						TO_CHAR( DATE_TRUNC( \'month\', ap.payment_datetime ), \'mm/dd/yyyy\') AS payment_month,
						p.property_name AS property_name,
						ap.property_id AS property_id,
						SUM( ap.payment_amount ) AS volume,
						COUNT( DISTINCT us.id ) AS paid_units
					FROM
						properties p
						LEFT JOIN ar_payments ap ON ( ap.cid = p.cid AND ap.property_id = p.id )
						LEFT JOIN leases AS l ON ( ap.cid = l.cid AND l.id = ap.lease_id )
						LEFT JOIN unit_spaces us ON ( us.cid = l.cid AND us.id = l.unit_space_id ' . $strCheckDeletedUnitSpacesSql . ' )
						WHERE
							ap.cid = ' . ( int ) $intCid . '
							AND ap.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
							AND ap.payment_status_type_id NOT IN ( ' . CPaymentStatusType::VOIDED . ',' . CPaymentStatusType::DECLINED . ',' . CPaymentStatusType::BATCHING . ' )
							AND ap.payment_type_id IN ( ' . implode( ',', $arrintPaymentTypeIds ) . ' )
							AND ap.payment_datetime BETWEEN \'' . $strStartDate . ' 00:00:00\' AND \'' . $strEndDate . ' 23:59:59\'
						GROUP BY
							ap.property_id,
							p.property_name,
							payment_month
						ORDER BY
							ap.property_id,
							payment_month';

		$objDataset = $objClientDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return NULL;
		}

		$arrmixPaymentCounts = [];

		if( 0 < $objDataset->getRecordCount() ) {
			while( false == $objDataset->eof() ) {
				$arrmixValues 					= $objDataset->fetchArray();
				$arrmixValues['payment_month'] 	= date( 'm/d/Y', strtotime( $arrmixValues['payment_month'] ) );
				$arrmixPaymentCounts[$arrmixValues['property_id']][$arrmixValues['payment_month']] = $arrmixValues;
				$objDataset->next();
			}
		}

		$objDataset->cleanup();
		return $arrmixPaymentCounts;
	}

	public static function fetchArPaymentsByIdsOrderByIdByCid( $arrintArPaymentIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintArPaymentIds ) ) return NULL;
		return self::fetchArPayments( 'SELECT * FROM ar_payments WHERE id IN ( ' . implode( ',', $arrintArPaymentIds ) . ' ) AND cid = ' . ( int ) $intCid . ' ORDER BY id desc', $objClientDatabase );
	}

	public static function fetchNonIntegratedArPaymentsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON (ap.id)
						ap.*
					FROM
						ar_payments ap,
						integration_clients ic
					WHERE
						ap.cid = ic.cid
						AND ic.integration_service_id = ' . CIntegrationService::SEND_AR_PAYMENT . '
						AND ap.payment_status_type_id = ' . CPaymentStatusType::CAPTURED . '
						AND ap.payment_type_id > ' . CPaymentType::MONEY_ORDER . '
						AND ap.payment_type_id NOT IN ( ' . CPaymentType::HAP . ', ' . CPaymentType::BAH . ' )
						AND ap.cid = ' . ( int ) $intCid . '
						AND ap.remote_primary_key IS NULL
						AND ic.created_on < ap.created_on
						AND ap.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return self::fetchArPayments( $strSql, $objClientDatabase );
	}

	public static function fetchNonIntegratedArPaymentsByApplicationIdByCid( $intApplicationId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						arp.*
					FROM
						application_payments as ap
						INNER JOIN ar_payments as arp ON ( ap.ar_payment_id = arp.id AND ap.cid = arp.cid )
					WHERE
						ap.application_id = ' . ( int ) $intApplicationId . '
						AND ap.cid = ' . ( int ) $intCid . '
						AND remote_primary_key IS NULL';
		return self::fetchArPayments( $strSql, $objClientDatabase );
	}

	public static function fetchIntegratedArPaymentsByApplicationIdByCid( $intApplicationId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						arp.*
					FROM
						application_payments as ap
						INNER JOIN ar_payments as arp ON ( ap.ar_payment_id = arp.id AND ap.cid = arp.cid )
					WHERE
						ap.application_id = ' . ( int ) $intApplicationId . '
						AND ap.cid = ' . ( int ) $intCid . '
						AND remote_primary_key IS NOT NULL';
		return self::fetchArPayments( $strSql, $objClientDatabase );
	}

	public static function fetchNewNonIntegratedNonFloatingArPaymentsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase, $objArPaymentsFilter = NULL, $arrintIntegrationQueuedPaymentIds = [] ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		if( true == valObj( $objArPaymentsFilter, 'CArPaymentsFilter' ) ) {
			$arrstrAndSearchParameters = self::fetchNewSearchCriteria( $objArPaymentsFilter );
		}

		$arrstrAndParameters = [];

		$arrstrExplodedSearch = explode( ' ', $objArPaymentsFilter->getKeywordAdvancedSearch() );

		if( true == valArr( $arrstrExplodedSearch ) ) {
			foreach( $arrstrExplodedSearch as $strExplodedSearch ) {
				if( false === strrchr( $strExplodedSearch, '\'' ) ) {
					$strFilteredExplodedSearch = $strExplodedSearch;
				} else {
					$strFilteredExplodedSearch = pg_escape_string( $strExplodedSearch );
				}
				array_push( $arrstrAndParameters, $strFilteredExplodedSearch );
			}
		}

		$arrstrAndParameters = array_filter( $arrstrAndParameters );

		$strSql = 'SELECT
						DISTINCT ON (ap.id)
						ap.*
					FROM
						ar_payments ap
						JOIN properties p ON( ap.property_id = p.id AND ap.cid = p.cid AND p.remote_primary_key IS NOT NULL AND ( ap.remote_primary_key IS NULL OR ap.return_remote_primary_key IS NULL ) )
						JOIN property_integration_databases pid ON( pid.property_id = p.id AND pid.cid = p.cid AND pid.created_on < ap.created_on )
						JOIN integration_clients ic ON( ic.integration_database_id = pid.integration_database_id AND ic.cid = pid.cid )
						LEFT JOIN property_preferences pp ON( pp.property_id = ap.property_id AND pp.cid = ap.cid AND pp.key = \'SUSPEND_FINANCIAL_DATA_INTEGRATION\' )
						LEFT JOIN ar_payments ap1 ON( ap1.payment_status_type_id = ' . CPaymentStatusType::REVERSED . ' AND ap1.ar_payment_id = ap.id AND ap1.cid = ap.cid )
						LEFT JOIN data_blobs db ON ( db.lease_id = ap.lease_id AND db.customer_id = ap.customer_id AND db.cid = ap.cid )
						LEFT JOIN property_gl_settings pgs ON ( p.id = pgs.property_id AND p.cid = pgs.cid AND pgs.activate_standard_posting = true )
					WHERE
						pp.id IS NULL
						AND pgs.id IS NULL
						AND (
								( ap.remote_primary_key IS NULL AND ap.payment_status_type_id = ' . CPaymentStatusType::CAPTURED . ' AND ic.integration_service_id = ' . CIntegrationService::SEND_AR_PAYMENT . ' )
								OR ( ap.return_remote_primary_key IS NULL AND ap.is_reversed = 1 AND ap.payment_amount = abs( ap1.payment_amount ) AND ap.payment_status_type_id = ' . CPaymentStatusType::CAPTURED . ' AND ic.integration_service_id = ' . CIntegrationService::REVERSE_AR_PAYMENT . ' )
								OR
									( ap.return_remote_primary_key IS NULL
										AND ap.payment_status_type_id IN ( ' . CPaymentStatusType::VOIDED . ',' .

																				CPaymentStatusType::RECALL . ',' .
																				CPaymentStatusType::CHARGED_BACK . ',' .
																				CPaymentStatusType::CHARGE_BACK_PENDING . '
																			)
								 		AND ic.integration_service_id IN( ' . CIntegrationService::RETURN_AR_PAYMENT . ',' .
																				CIntegrationService::REVERSE_AR_PAYMENT . '
																			)
								)
							)
						AND ap.payment_type_id > ' . CPaymentType::MONEY_ORDER . '
						AND ap.payment_type_id NOT IN ( ' . CPaymentType::HAP . ', ' . CPaymentType::BAH . ' )
						AND ap.cid = ' . ( int ) $intCid . '
						AND ap.customer_id IS NOT NULL
						AND ap.lease_id IS NOT NULL
						AND ap.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . '  )';

        if( true == valArr( $arrintIntegrationQueuedPaymentIds ) ) {
            $strSql .= ' AND ap.id NOT IN ( ' . implode( ',', $arrintIntegrationQueuedPaymentIds ) . '  )';
        }

		if( true == isset( $arrstrAndSearchParameters ) && false == is_null( $arrstrAndSearchParameters ) ) {
			$strSql .= ' AND ' . implode( ' AND ', $arrstrAndSearchParameters );
		}

		if( true == valArr( $arrstrAndParameters ) ) {
			$strSql .= ' AND db.blob ILIKE \'%' . implode( '%\' AND db.blob LIKE \'%', $arrstrAndParameters ) . '%\' ';
		}

		return self::fetchArPayments( $strSql, $objClientDatabase );
	}

	public static function fetchNewNonIntegratedFloatingArPaymentsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase, $objArPaymentsFilter = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		if( true == valObj( $objArPaymentsFilter, 'CArPaymentsFilter' ) ) {
			$arrstrAndSearchParameters = self::fetchNewSearchCriteria( $objArPaymentsFilter );
		}

		$arrstrAndParameters = [];

		$arrstrExplodedSearch = explode( ' ', $objArPaymentsFilter->getKeywordAdvancedSearch() );

		if( true == valArr( $arrstrExplodedSearch ) ) {
			foreach( $arrstrExplodedSearch as $strExplodedSearch ) {
				if( false === strrchr( $strExplodedSearch, '\'' ) ) {
					$strFilteredExplodedSearch = $strExplodedSearch;
				} else {
					$strFilteredExplodedSearch = pg_escape_string( $strExplodedSearch );
				}
				array_push( $arrstrAndParameters, $strFilteredExplodedSearch );
			}
		}

		$arrstrAndParameters = array_filter( $arrstrAndParameters );

		$strSql = 'SELECT
						DISTINCT ON (ap.id)
						ap.*
					FROM
						ar_payments ap
						JOIN properties p ON( ap.property_id = p.id AND ap.cid = p.cid AND p.remote_primary_key IS NOT NULL AND ( ap.remote_primary_key IS NULL OR ap.return_remote_primary_key IS NULL ) )
						JOIN property_integration_databases pid ON( pid.property_id = p.id AND pid.cid = p.cid AND pid.created_on < ap.created_on )
						JOIN integration_clients ic ON( ic.integration_database_id = pid.integration_database_id AND ic.cid = pid.cid )
						LEFT JOIN property_preferences pp ON( pp.property_id = ap.property_id AND pp.cid = ap.cid AND pp.key = \'SUSPEND_FINANCIAL_DATA_INTEGRATION\' )
						LEFT JOIN ar_payments ap1 ON( ap1.payment_status_type_id = ' . CPaymentStatusType::REVERSED . ' AND ap1.ar_payment_id = ap.id AND ap1.cid = ap.cid )
						LEFT JOIN data_blobs db ON ( db.lease_id = ap.lease_id AND db.customer_id = ap.customer_id AND db.cid = ap.cid )
						LEFT JOIN property_gl_settings pgs ON ( p.id = pgs.property_id AND p.cid = pgs.cid AND pgs.activate_standard_posting = true )
					WHERE
						pp.id IS NULL
						AND pgs.id IS NULL
						AND (
								( ap.remote_primary_key IS NULL AND ap.payment_status_type_id = ' . CPaymentStatusType::CAPTURED . ' AND ic.integration_service_id = ' . CIntegrationService::SEND_AR_PAYMENT . ' )
								OR ( ap.return_remote_primary_key IS NULL AND ap.is_reversed = 1 AND ap.payment_amount = abs( ap1.payment_amount ) AND ap.payment_status_type_id = ' . CPaymentStatusType::CAPTURED . ' AND ic.integration_service_id = ' . CIntegrationService::REVERSE_AR_PAYMENT . ' )
								OR
									( ap.return_remote_primary_key IS NULL
										AND ap.payment_status_type_id IN ( ' . CPaymentStatusType::VOIDED . ',' .

											CPaymentStatusType::RECALL . ',' .
											CPaymentStatusType::CHARGED_BACK . ',' .
											CPaymentStatusType::CHARGE_BACK_PENDING . '
																			)
								 		AND ic.integration_service_id IN( ' . CIntegrationService::RETURN_AR_PAYMENT . ',' .
									 		CIntegrationService::REVERSE_AR_PAYMENT . '
																			)
								)
							)
						AND ap.payment_type_id > ' . CPaymentType::MONEY_ORDER . '
						AND ap.payment_type_id NOT IN ( ' . CPaymentType::HAP . ', ' . CPaymentType::BAH . ' )
						AND ap.cid = ' . ( int ) $intCid . '
						AND ap.customer_id IS NULL
						AND ap.lease_id IS NULL
						AND ap.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . '  )';

		if( true == isset( $arrstrAndSearchParameters ) && false == is_null( $arrstrAndSearchParameters ) ) {
			$strSql .= ' AND ' . implode( ' AND ', $arrstrAndSearchParameters );
		}

		if( true == valArr( $arrstrAndParameters ) ) {
			$strSql .= ' AND db.blob ILIKE \'%' . implode( '%\' AND db.blob LIKE \'%', $arrstrAndParameters ) . '%\' ';
		}

		return self::fetchArPayments( $strSql, $objClientDatabase );
	}

	public static function fetchLastArPaymentWithDetailByCustomerIdByCidByLeaseId( $arrintPropertyIds, $intCustomerId, $intCurrentPropertyId, $boolIsShowAllTypeOfPayments, $intCid, $intLeaseId, $objClientDatabase, $objPaymentDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$arrintPaymentTypeIds 				 = [ CPaymentType::ACH, CPaymentType::VISA, CPaymentType::MASTERCARD, CPaymentType::DISCOVER, CPaymentType::AMEX, CPaymentType::CHECK_21, CPaymentType::PAD ];

		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) && current( $arrintPropertyIds ) == $intCurrentPropertyId ) {
			if( true == $boolIsShowAllTypeOfPayments ) {
				$arrstrAndSearchParameters[] = ' ( property_id IN (' . implode( ',', $arrintPropertyIds ) . ') OR payment_type_id IN ( ' . implode( ',', $arrintPaymentTypeIds ) . ' ) )';
			} else {
				$arrstrAndSearchParameters[] = ' payment_type_id IN ( ' . implode( ',', $arrintPaymentTypeIds ) . ' ) ';
			}

		} else {

			$arrobjPropertyPreferences			 = CPropertyPreferences::fetchPropertyPreferencesByPropertyIdsByKeysByCid( $arrintPropertyIds, [ 'SHOW_RESIDENT_ALL_TYPE_PAYMENTS' ], $intCid, $objClientDatabase );
			$arrobjPropertyPreferences			 = rekeyObjects( 'PropertyId', $arrobjPropertyPreferences );

			if( true == valArr( $arrobjPropertyPreferences ) ) {
				$arrstrAndSearchParameters[] = ' ( property_id IN (' . implode( ',', $arrintPropertyIds ) . ') OR payment_type_id IN ( ' . implode( ',', $arrintPaymentTypeIds ) . ' ))';
			} else {
				$arrstrAndSearchParameters[] = ' payment_type_id IN ( ' . implode( ',', $arrintPaymentTypeIds ) . ' ) ';
			}
		}

		$strSql = 'SELECT * FROM view_ar_payments' . ' WHERE customer_id::integer = ' . ( int ) $intCustomerId . '::integer AND cid::integer = ' . ( int ) $intCid . '::integer AND payment_status_type_id != ' . CPaymentStatusType::CANCELLED . ( ( false == is_null( $intLeaseId ) ) ? ' AND lease_id =' . ( int ) $intLeaseId . ' ' : '' ) . ( ( false == is_null( $arrstrAndSearchParameters ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' ) . 'ORDER BY payment_datetime DESC, id desc LIMIT 1';

		$arrobjArPayments = self::fetchArPayments( $strSql, $objPaymentDatabase );

		$objArPayment = NULL;

		if( valArr( $arrobjArPayments ) ) {
			$objArPayment = reset( $arrobjArPayments );
		}

		return $objArPayment;
	}

	public static function fetchLastArPaymentsByCustomerIdByLimitByCid( $arrintPropertyIds, $intCustomerId, $intArPaymentLimit, $intCid, $objClientDatabase, $intLeaseId = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$arrintPaymentTypeIds 				 = [ CPaymentType::ACH, CPaymentType::VISA, CPaymentType::MASTERCARD, CPaymentType::DISCOVER, CPaymentType::AMEX, CPaymentType::CHECK_21, CPaymentType::PAD ];

		$arrstrAndSearchParameters[] = ' ap.property_id IN (' . implode( ',', $arrintPropertyIds ) . ') AND ap.payment_type_id IN ( ' . implode( ',', $arrintPaymentTypeIds ) . ' )';

		$strSql = 'SELECT
						ap.*,
						COALESCE( split_part( db.blob, \'~..$\', 5 ), ap.billto_unit_number ) AS billto_unit_number
					FROM 
						ar_payments ap
					LEFT JOIN 
						data_blobs db ON ( db.cid = ap.cid AND db.lease_id = ap.lease_id AND db.customer_id = ap.customer_id )
					' . ' WHERE ap.customer_id::integer = ' . ( int ) $intCustomerId . '::integer AND ap.cid::integer = ' . ( int ) $intCid . '::integer AND ap.payment_status_type_id != ' . CPaymentStatusType::CANCELLED . ( ( false == is_null( $intLeaseId ) ) ? ' AND ap.lease_id =' . ( int ) $intLeaseId . ' ' : '' ) . ( ( false == is_null( $arrstrAndSearchParameters ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' ) . 'ORDER BY ap.payment_datetime DESC, id desc';

		if( false == is_null( $intArPaymentLimit ) ) {
			$strSql .= ' LIMIT ' . ( int ) $intArPaymentLimit;
		}

		return self::fetchArPayments( $strSql, $objClientDatabase );
	}

	public static function fetchLastArPaymentsByLimitByCid( $intCustomerId, $arrintPropertyIds, $intLeaseId, $intCurrentPropertyId, $boolIsShowAllTypeOfPayments, $intArPaymentLimit, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) && current( $arrintPropertyIds ) == $intCurrentPropertyId ) {
			if( true == $boolIsShowAllTypeOfPayments ) {
				$arrstrAndSearchParameters[] = ' ( property_id IN (' . implode( ',', array_keys( $arrintPropertyIds ) ) . ') AND payment_type_id IN ( ' . implode( ',', CPaymentType::$c_arrintFeeQualifyingPaymentTypeIds ) . ' ) )';
			} else {
				$arrstrAndSearchParameters[] = ' payment_type_id IN ( ' . implode( ',', CPaymentType::$c_arrintFeeQualifyingPaymentTypeIds ) . ' ) ';
			}

		} else {

			$arrobjPropertyPreferences			 = CPropertyPreferences::fetchPropertyPreferencesByPropertyIdsByKeysByCid( $arrintPropertyIds, [ 'SHOW_RESIDENT_ALL_TYPE_PAYMENTS' ], $intCid, $objClientDatabase );
			$arrobjPropertyPreferences			 = rekeyObjects( 'PropertyId', $arrobjPropertyPreferences );

			if( true == valArr( $arrobjPropertyPreferences ) ) {
				$arrstrAndSearchParameters[] = ' ( property_id IN (' . implode( ',', array_keys( $arrobjPropertyPreferences ) ) . ') AND payment_type_id IN ( ' . implode( ',', CPaymentType::$c_arrintFeeQualifyingPaymentTypeIds ) . ' ) )';
			} else {
				$arrstrAndSearchParameters[] = ' payment_type_id IN ( ' . implode( ',', CPaymentType::$c_arrintFeeQualifyingPaymentTypeIds ) . ' ) ';
			}
		}
		$strSql = 'SELECT * FROM ar_payments' . ' WHERE customer_id::integer != ' . ( int ) $intCustomerId . '::integer AND cid::integer = ' . ( int ) $intCid . '::integer AND lease_id = ' . ( int ) $intLeaseId . ' AND payment_status_type_id != ' . CPaymentStatusType::CANCELLED . ( ( valArr( $arrstrAndSearchParameters ) ) ? ' AND' . implode( ' AND ', $arrstrAndSearchParameters ) : '' ) . ' ORDER BY payment_datetime DESC, id desc';

		if( false == is_null( $intArPaymentLimit ) ) {
			$strSql .= ' LIMIT ' . ( int ) $intArPaymentLimit;
		}
        return self::fetchArPayments( $strSql, $objClientDatabase );
    }

	public static function fetchCustomPaginatedAdminArPayments( $intPageNumber, $intPageSize, $objPaymentSearch,  $objPaymentDatabase ) {

		$arrstrAndSearchParameters = [];
		$strAdditionalJoins = '';

		// Create SQL parameters.
		if( false == is_null( $objPaymentSearch->getCid() ) ) 			$arrstrAndSearchParameters[] = 'ap.cid = \'' . ( int ) $objPaymentSearch->getCid() . '\'';
		if( false == is_null( $objPaymentSearch->getCompanyMerchantAccountId() )
			&& 'all' != $objPaymentSearch->getCompanyMerchantAccountId()
			&& 0 < strlen( trim( $objPaymentSearch->getCompanyMerchantAccountId() ) ) ) $arrstrAndSearchParameters[] = 'ap.company_merchant_account_id = \'' . ( int ) $objPaymentSearch->getCompanyMerchantAccountId() . '\'';

		// Removing date_trunc() function from  ap.payment_datetime field and using  ap.payment_datetime field with concating h:i:s format beacause query was taking extra execution time to using it.
		if( false == is_null( $objPaymentSearch->getStartDate() ) ) 					$arrstrAndSearchParameters[] = 'ap.payment_datetime  >= \'' . $objPaymentSearch->getStartDate() . ' 00:00:00\'';
		if( false == is_null( $objPaymentSearch->getEndDate() ) ) 						$arrstrAndSearchParameters[] = 'ap.payment_datetime  <= \'' . $objPaymentSearch->getEndDate() . ' 23:59:59\'';

		if( false == is_null( $objPaymentSearch->getBillToNameFirst() ) ) 				$arrstrAndSearchParameters[] = '( ap.billto_name_first ILIKE E\'' . addslashes( $objPaymentSearch->sqlBillToNameFirst() ) . '\' )';
		if( false == is_null( $objPaymentSearch->getBillToNameLast() ) ) 				$arrstrAndSearchParameters[] = '( ap.billto_name_last ILIKE E\'' . addslashes( $objPaymentSearch->sqlBillToNameLast() ) . '\' )';
		if( false == is_null( $objPaymentSearch->getBillToUnitNumber() ) ) 				$arrstrAndSearchParameters[] = 'ap.billto_unit_number ILIKE E\'' . addslashes( $objPaymentSearch->sqlBilltoUnitNumber() ) . '\'';
		if( false == is_null( $objPaymentSearch->getArPaymentId() ) ) 					$arrstrAndSearchParameters[] = 'ap.id = ' . ( int ) $objPaymentSearch->getArPaymentId();
		if( false == is_null( $objPaymentSearch->getPaymentMinAmount() ) ) 				$arrstrAndSearchParameters[] = 'ap.payment_amount >= \'' . ( float ) $objPaymentSearch->getPaymentMinAmount() . '\'';
		if( false == is_null( $objPaymentSearch->getPaymentMaxAmount() ) ) 				$arrstrAndSearchParameters[] = 'ap.payment_amount <= \'' . ( float ) $objPaymentSearch->getPaymentMaxAmount() . '\'';
		if( true == is_numeric( $objPaymentSearch->getX937CheckDetailRecordId() ) )		$arrstrAndSearchParameters[] = 'xcdr.id = ' . ( int ) $objPaymentSearch->getX937CheckDetailRecordId();
		if( true == is_numeric( $objPaymentSearch->getMoneyGramAccountId() ) )			$arrstrAndSearchParameters[] = 'mgbp.money_gram_account_id = ' . ( int ) $objPaymentSearch->getMoneyGramAccountId();
		if( true == is_numeric( $objPaymentSearch->getScheduledPaymentId() ) ) 			$arrstrAndSearchParameters[] = 'ap.scheduled_payment_id = ' . ( int ) $objPaymentSearch->getScheduledPaymentId();
		if( true == is_numeric( $objPaymentSearch->getIsRecurringPayment() ) ) 			$arrstrAndSearchParameters[] = 'ap.scheduled_payment_id IS NOT NULL ';

		if( false == is_null( $objPaymentSearch->getCheckAccountNumberEncrypted() ) && '' != $objPaymentSearch->getCheckAccountNumberEncrypted() ) {
			$arrstrAndSearchParameters[] = 'apb.check_account_number_encrypted IS NOT NULL';
			if( false == is_null( $objPaymentSearch->getCheckAccountNumberBlindIndex() ) ) {
				$arrstrAndSearchParameters[] = '( apb.check_account_number_encrypted = \'' . addslashes( $objPaymentSearch->getCheckAccountNumberEncrypted() ) . '\' OR apb.check_account_number_bindex = \'' . addslashes( $objPaymentSearch->getCheckAccountNumberBlindIndex() ) . '\'  )';
			} else {
				$arrstrAndSearchParameters[] = 'apb.check_account_number_encrypted = \'' . addslashes( $objPaymentSearch->getCheckAccountNumberEncrypted() ) . '\'';
			}
		}
		if( false == is_null( $objPaymentSearch->getCheckRoutingNumber() ) && '' != $objPaymentSearch->getCheckRoutingNumber() ) {
			$arrstrAndSearchParameters[] = 'apb.check_routing_number IS NOT NULL';
			$arrstrAndSearchParameters[] = 'apb.check_routing_number = \'' . $objPaymentSearch->getCheckRoutingNumber() . '\'';
		}

		if( false == is_null( $objPaymentSearch->getCheckNumber() ) && '' != $objPaymentSearch->getCheckNumber() ) {
			$arrstrAndSearchParameters[] = 'ap.check_number IS NOT NULL';
			$arrstrAndSearchParameters[] = 'ap.check_number SIMILAR TO \'(0)*(' . $objPaymentSearch->getCheckNumber() . ')\'';
		}

		if( false == is_null( $objPaymentSearch->getCheckAuxillaryOnUs() ) && '' != $objPaymentSearch->getCheckAuxillaryOnUs() ) {
			$arrstrAndSearchParameters[] = 'apb.check_auxillary_on_us IS NOT NULL';
			$arrstrAndSearchParameters[] = 'apb.check_auxillary_on_us SIMILAR TO \'(0)*(' . $objPaymentSearch->getCheckAuxillaryOnUs() . ')\'';
		}

		if( false == is_null( $objPaymentSearch->getCcCardNumberEncrypted() ) && '' != $objPaymentSearch->getCcCardNumberEncrypted() ) {
			$arrstrAndSearchParameters[] = 'apb.cc_card_number_encrypted IS NOT NULL';
			$arrstrAndSearchParameters[] = 'apb.cc_card_number_encrypted = \'' . addslashes( $objPaymentSearch->getCcCardNumberEncrypted() ) . '\'';
		}

		if( ( false == is_null( $objPaymentSearch->getCheckAccountNumberEncrypted() ) && '' != $objPaymentSearch->getCheckAccountNumberEncrypted() ) || ( false == is_null( $objPaymentSearch->getCcCardNumberEncrypted() ) && '' != $objPaymentSearch->getCcCardNumberEncrypted() ) || ( false == is_null( $objPaymentSearch->getCheckRoutingNumber() ) && '' != $objPaymentSearch->getCheckRoutingNumber() ) || ( false == is_null( $objPaymentSearch->getCheckAuxillaryOnUs() ) && '' != $objPaymentSearch->getCheckAuxillaryOnUs() ) ) {
			$strAdditionalJoins .= ' JOIN ar_payment_billings apb ON ( ap.id = apb.ar_payment_id AND ap.cid = apb.cid )';
		}

		if( true == is_numeric( $objPaymentSearch->getX937CheckDetailRecordId() ) ) {
			$strAdditionalJoins .= ' JOIN x937_check_detail_records xcdr ON ( ap.id = xcdr.ar_payment_id AND ap.cid = xcdr.cid )';
		}

		if( true == is_numeric( $objPaymentSearch->getMoneyGramAccountId() ) ) {
			$strAdditionalJoins .= ' JOIN money_gram_bill_payments mgbp ON( ap.id = mgbp.ar_payment_id AND ap.cid = mgbp.cid )';
		}

		if( false == is_null( $objPaymentSearch->getTraceNumber() ) && 0 < strlen( $objPaymentSearch->getTraceNumber() ) ) {
			$strAdditionalJoins .= ' JOIN nacha_entry_detail_records nedr ON ( ap.id = nedr.ar_payment_id AND nedr.trace_number = \'' . addslashes( $objPaymentSearch->getTraceNumber() ) . '\' )';
		}

		if( false == is_null( $objPaymentSearch->getArCompanyStatusTypeIds() ) && 0 < strlen( $objPaymentSearch->getArCompanyStatusTypeIds() ) ) {
			$strAdditionalJoins .= ' JOIN clients mc ON ( ap.cid = mc.id )';
		}

		$strSql = 'SELECT
						ap.id,
						ap.billto_name_first,
						ap.billto_name_last,
						ap.cid,
						ap.property_id,
						ap.company_merchant_account_id,
						ap.payment_amount,
						ap.convenience_fee_amount,
						ap.company_charge_amount,
						ap.payment_type_id,
						ap.merchant_gateway_id,
						ap.payment_status_type_id,
						ap.scheduled_payment_id,
						ap.payment_datetime,
						ap.is_reversed,
						ap.distribute_on,
						ap.distribute_return_on,
						ap.returned_on,
       					ap.currency_code,
						ap.updated_on,
						ap.created_on
					FROM ar_payments ap ' . $strAdditionalJoins . '
					WHERE ap.company_merchant_account_id IS NOT NULL AND
					' . implode( ' AND ', $arrstrAndSearchParameters );

					if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) $strSql .= ' AND ';

					if( false == is_null( $objPaymentSearch->getArPaymentTypeIds() ) ) {
						$strSql .= ' ap.payment_type_id IN (' . $objPaymentSearch->getArPaymentTypeIds() . ') ';
					} else {
						$strSql .= ' 1=1 ';
					}

					if( false == is_null( $objPaymentSearch->getArMerchantGatewayIds() ) ) {
						$strSql .= ' AND ap.merchant_gateway_id IN (' . $objPaymentSearch->getArMerchantGatewayIds() . ') ';
					}
					if( false == is_null( $objPaymentSearch->getArPaymentStatusTypeIds() ) ) {
						$strSql .= ' AND ap.payment_status_type_id IN ( ' . $objPaymentSearch->getArPaymentStatusTypeIds() . ') ';
					}
					if( false == is_null( $objPaymentSearch->getArCompanyStatusTypeIds() ) ) {
						$strSql .= ' AND mc.company_status_type_id IN (' . $objPaymentSearch->getArCompanyStatusTypeIds() . ')';
					}

					$strSql .= ' ORDER BY ap.payment_datetime DESC OFFSET ' . ( int ) ( ( $intPageNumber - 1 ) * $intPageSize ) . ' LIMIT ' . ( int ) $intPageSize;

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchCustomPaginatedAdminArPaymentsTotalCount( $objPaymentSearch, $objPaymentDatabase ) {

		$arrstrAndSearchParameters = [];
		$strAdditionalJoins = '';

		// Create SQL parameters.
		if( false == is_null( $objPaymentSearch->getCid() ) ) 			$arrstrAndSearchParameters[] = 'ap.cid = \'' . ( int ) $objPaymentSearch->getCid() . '\'';
		if( false == is_null( $objPaymentSearch->getCompanyMerchantAccountId() )
			&& 'all' != $objPaymentSearch->getCompanyMerchantAccountId()
			&& 0 < strlen( trim( $objPaymentSearch->getCompanyMerchantAccountId() ) ) ) 	$arrstrAndSearchParameters[] = 'ap.company_merchant_account_id = \'' . ( int ) $objPaymentSearch->getCompanyMerchantAccountId() . '\'';

		if( false == is_null( $objPaymentSearch->getStartDate() ) ) 					$arrstrAndSearchParameters[] = 'ap.payment_datetime  >= \'' . $objPaymentSearch->getStartDate() . ' 00:00:00\'';
		if( false == is_null( $objPaymentSearch->getEndDate() ) ) 						$arrstrAndSearchParameters[] = 'ap.payment_datetime  <= \'' . $objPaymentSearch->getEndDate() . ' 23:59:59\'';

		if( false == is_null( $objPaymentSearch->getBillToNameFirst() ) ) 				$arrstrAndSearchParameters[] = '( ap.billto_name_first ILIKE E\'' . addslashes( $objPaymentSearch->sqlBillToNameFirst() ) . '\' )';
		if( false == is_null( $objPaymentSearch->getBillToNameLast() ) ) 				$arrstrAndSearchParameters[] = '( ap.billto_name_last ILIKE E\'' . addslashes( $objPaymentSearch->sqlBillToNameLast() ) . '\' )';
		if( false == is_null( $objPaymentSearch->getBillToUnitNumber() ) ) 			$arrstrAndSearchParameters[] = 'ap.billto_unit_number ILIKE E\'' . addslashes( $objPaymentSearch->sqlBillToUnitNumber() ) . '\'';
		if( false == is_null( $objPaymentSearch->getArPaymentId() ) ) 					$arrstrAndSearchParameters[] = 'ap.id = ' . ( int ) $objPaymentSearch->getArPaymentId();
		if( false == is_null( $objPaymentSearch->getPaymentMinAmount() ) ) 			$arrstrAndSearchParameters[] = 'ap.payment_amount >= \'' . ( float ) $objPaymentSearch->getPaymentMinAmount() . '\'';
		if( false == is_null( $objPaymentSearch->getPaymentMaxAmount() ) ) 			$arrstrAndSearchParameters[] = 'ap.payment_amount <= \'' . ( float ) $objPaymentSearch->getPaymentMaxAmount() . '\'';
		if( true == is_numeric( $objPaymentSearch->getX937CheckDetailRecordId() ) )		$arrstrAndSearchParameters[] = 'xcdr.id = ' . ( int ) $objPaymentSearch->getX937CheckDetailRecordId();
		if( true == is_numeric( $objPaymentSearch->getMoneyGramAccountId() ) )			$arrstrAndSearchParameters[] = 'mgbp.money_gram_account_id = ' . ( int ) $objPaymentSearch->getMoneyGramAccountId();
		if( true == is_numeric( $objPaymentSearch->getScheduledPaymentId() ) ) 			$arrstrAndSearchParameters[] = 'ap.scheduled_payment_id = ' . ( int ) $objPaymentSearch->getScheduledPaymentId();
		if( true == is_numeric( $objPaymentSearch->getIsRecurringPayment() ) ) 			$arrstrAndSearchParameters[] = 'ap.scheduled_payment_id IS NOT NULL ';

		if( false == is_null( $objPaymentSearch->getCheckAccountNumberEncrypted() ) && '' != $objPaymentSearch->getCheckAccountNumberEncrypted() ) {
			$arrstrAndSearchParameters[] = 'apb.check_account_number_encrypted IS NOT NULL';
			if( false == is_null( $objPaymentSearch->getCheckAccountNumberBlindIndex() ) ) {
				$arrstrAndSearchParameters[] = '( apb.check_account_number_encrypted = \'' . addslashes( $objPaymentSearch->getCheckAccountNumberEncrypted() ) . '\' OR apb.check_account_number_bindex = \'' . addslashes( $objPaymentSearch->getCheckAccountNumberBlindIndex() ) . '\'  )';
			} else {
				$arrstrAndSearchParameters[] = 'apb.check_account_number_encrypted = \'' . addslashes( $objPaymentSearch->getCheckAccountNumberEncrypted() ) . '\'';
			}
		}

		if( false == is_null( $objPaymentSearch->getCheckRoutingNumber() ) && '' != $objPaymentSearch->getCheckRoutingNumber() ) {
			$arrstrAndSearchParameters[] = 'apb.check_routing_number IS NOT NULL';
			$arrstrAndSearchParameters[] = 'apb.check_routing_number = \'' . $objPaymentSearch->getCheckRoutingNumber() . '\'';
		}

		if( false == is_null( $objPaymentSearch->getCheckNumber() ) && '' != $objPaymentSearch->getCheckNumber() ) {
			$arrstrAndSearchParameters[] = 'ap.check_number IS NOT NULL';
			$arrstrAndSearchParameters[] = 'ap.check_number SIMILAR TO \'(0)*(' . $objPaymentSearch->getCheckNumber() . ')\'';
		}

		if( false == is_null( $objPaymentSearch->getCheckAuxillaryOnUs() ) && '' != $objPaymentSearch->getCheckAuxillaryOnUs() ) {
			$arrstrAndSearchParameters[] = 'apb.check_auxillary_on_us IS NOT NULL';
			$arrstrAndSearchParameters[] = 'apb.check_auxillary_on_us SIMILAR TO \'(0)*(' . $objPaymentSearch->getCheckAuxillaryOnUs() . ')\'';
		}

		if( false == is_null( $objPaymentSearch->getCcCardNumberEncrypted() ) && '' != $objPaymentSearch->getCcCardNumberEncrypted() ) {
			$arrstrAndSearchParameters[] = 'apb.cc_card_number_encrypted IS NOT NULL';
			$arrstrAndSearchParameters[] = 'apb.cc_card_number_encrypted = \'' . addslashes( $objPaymentSearch->getCcCardNumberEncrypted() ) . '\'';
		}

		if( ( false == is_null( $objPaymentSearch->getCheckAccountNumberEncrypted() ) && '' != $objPaymentSearch->getCheckAccountNumberEncrypted() ) || ( false == is_null( $objPaymentSearch->getCcCardNumberEncrypted() ) && '' != $objPaymentSearch->getCcCardNumberEncrypted() ) || ( false == is_null( $objPaymentSearch->getCheckRoutingNumber() ) && '' != $objPaymentSearch->getCheckRoutingNumber() ) || ( false == is_null( $objPaymentSearch->getCheckAuxillaryOnUs() ) && '' != $objPaymentSearch->getCheckAuxillaryOnUs() ) ) {
			$strAdditionalJoins .= ' JOIN ar_payment_billings apb ON ( ap.id = apb.ar_payment_id AND ap.cid = apb.cid )';
		}

		if( false == is_null( $objPaymentSearch->getArCompanyStatusTypeIds() ) && 0 < strlen( $objPaymentSearch->getArCompanyStatusTypeIds() ) ) {
			$strAdditionalJoins .= ' JOIN clients mc ON ( ap.cid = mc.id )';
		}

		if( true == is_numeric( $objPaymentSearch->getX937CheckDetailRecordId() ) ) {
			$strAdditionalJoins .= ' JOIN x937_check_detail_records xcdr ON ( ap.id = xcdr.ar_payment_id AND ap.cid = xcdr.cid )';
		}

		if( true == is_numeric( $objPaymentSearch->getMoneyGramAccountId() ) ) {
			$strAdditionalJoins .= ' JOIN money_gram_bill_payments mgbp ON( ap.id = mgbp.ar_payment_id AND ap.cid = mgbp.cid )';
		}

		if( false == is_null( $objPaymentSearch->getTraceNumber() ) && 0 < strlen( $objPaymentSearch->getTraceNumber() ) ) {
			$strAdditionalJoins .= ' JOIN nacha_entry_detail_records nedr ON ( ap.id = nedr.ar_payment_id AND nedr.trace_number = \'' . addslashes( $objPaymentSearch->getTraceNumber() ) . '\' )';
		}

		$strSql = 'SELECT
						count( ap.id )
					FROM ar_payments ap ' . $strAdditionalJoins . '
					WHERE ap.company_merchant_account_id IS NOT NULL AND
					' . implode( ' AND ', $arrstrAndSearchParameters );
					if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) $strSql .= ' AND ';

					if( 0 < \Psi\Libraries\UtilFunctions\count( $objPaymentSearch->getArPaymentTypeIds() ) ) {
						$strSql .= ' ap.payment_type_id IN (' . $objPaymentSearch->getArPaymentTypeIds() . ') ';
					} else {
						$strSql .= ' 1=1 ';
					}

					if( 0 < \Psi\Libraries\UtilFunctions\count( $objPaymentSearch->getArMerchantGatewayIds() ) ) {
						$strSql .= ' AND ap.merchant_gateway_id IN ( ' . $objPaymentSearch->getArMerchantGatewayIds() . ') ';
					}

					if( 0 < \Psi\Libraries\UtilFunctions\count( $objPaymentSearch->getArPaymentStatusTypeIds() ) ) {
						$strSql .= ' AND ap.payment_status_type_id IN ( ' . $objPaymentSearch->getArPaymentStatusTypeIds() . ') ';
					}

					if( false == is_null( $objPaymentSearch->getArCompanyStatusTypeIds() ) ) {
						$strSql .= ' AND mc.company_status_type_id IN (' . $objPaymentSearch->getArCompanyStatusTypeIds() . ') ';
					}

		$arrmixData = fetchData( $strSql, $objPaymentDatabase );

		if( true == valArr( $arrmixData ) && true == isset ( $arrmixData[0]['count'] ) ) {
			return $arrmixData[0]['count'];

		} else {
			return 0;
		}
	}

	public static function fetchNextCheck21ArPaymentByCid( $intCid, $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM view_ar_payments	WHERE payment_type_id = ' . ( int ) CPaymentType::CHECK_21 . ' AND cid = ' . ( int ) $intCid . ' AND approved_on IS NULL	ORDER BY id DESC LIMIT 1';
		return self::fetchArPayment( $strSql, $objPaymentDatabase );
	}

	public static function fetchLastArPaymentByCcCardNumberByCid( $strCcCardNumber, $intCid, $objPaymentDatabase ) {

		if( true == is_null( $strCcCardNumber ) ) return NULL;

		$strCcCardNumber = CStrings::strTrimDef( $strCcCardNumber, 20, NULL, true );

		$strSql = 'SELECT * FROM view_ar_payments
					WHERE
						payment_type_id IN ( ' . CPaymentType::DISCOVER . ', ' . CPaymentType::AMEX . ', ' . CPaymentType::MASTERCARD . ', ' . CPaymentType::VISA . ' )
						AND cid = ' . ( int ) $intCid . '
						AND cc_card_number_encrypted = \'' . CEncryption::encryptText( $strCcCardNumber, CONFIG_KEY_CC_CARD_NUMBER ) . '\'
					ORDER BY id desc
					LIMIT 1';

		return self::fetchArPayment( $strSql, $objPaymentDatabase );
	}

	public static function fetchArPaymentsByCcCardNumberByCid( $strCcCardNumber, $intCid, $objPaymentDatabase ) {

		if( true == is_null( $strCcCardNumber ) ) return NULL;

		$strCcCardNumber = CStrings::strTrimDef( $strCcCardNumber, 20, NULL, true );

		$strSql = 'SELECT
						*
					FROM
						view_ar_payments
					WHERE
						payment_type_id IN ( ' . CPaymentType::DISCOVER . ', ' . CPaymentType::AMEX . ', ' . CPaymentType::MASTERCARD . ', ' . CPaymentType::VISA . ' )
						AND cid = ' . ( int ) $intCid . '
						AND cc_card_number_encrypted = \'' . CEncryption::encryptText( $strCcCardNumber, CONFIG_KEY_CC_CARD_NUMBER ) . '\'
					ORDER BY id desc';

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchLastArPaymentByAchAccountNumberByAchRoutingNumberByCid( $strAchAccountNumber, $strAchRoutingNumber, $intCid, $objPaymentDatabase ) {

		if( true == is_null( $strAchAccountNumber ) ) return NULL;
		if( true == is_null( $strAchRoutingNumber ) ) return NULL;

		$strAchRoutingNumber = CStrings::strTrimDef( $strAchRoutingNumber, 20, NULL, true );

		$strSql = 'SELECT * FROM view_ar_payments
					WHERE
						payment_type_id IN ( ' . CPaymentType::CHECK_21 . ', ' . CPaymentType::ACH . ', ' . CPaymentType::PAD . ' )
						AND payment_status_type_id IN ( ' . CPaymentStatusType::AUTHORIZED . ', ' . CPaymentStatusType::CAPTURED . ' )
						AND cid = ' . ( int ) $intCid . '
						AND check_account_number_encrypted = \'' . CEncryption::encryptText( trim( $strAchAccountNumber ), CONFIG_KEY_ACH_ACCOUNT_NUMBER ) . '\'
						AND check_routing_number = \'' . addslashes( trim( $strAchRoutingNumber ) ) . '\'
						AND customer_id IS NOT NULL
					ORDER BY id desc
					LIMIT 1';

		return self::fetchArPayment( $strSql, $objPaymentDatabase );
	}

	public static function fetchArPaymentsByAchAccountNumberByAchRoutingNumberByCid( $strAchAccountNumber, $strAchRoutingNumber, $intCid, $objPaymentDatabase ) {
		if( true == is_null( $strAchAccountNumber ) || true == is_null( $strAchRoutingNumber ) ) return NULL;

		$strAchRoutingNumber = CStrings::strTrimDef( $strAchRoutingNumber, 20, NULL, true );

		$strSql = 'SELECT
						*
					FROM
						view_ar_payments
					WHERE
						payment_type_id IN ( ' . CPaymentType::CHECK_21 . ', ' . CPaymentType::ACH . ', ' . CPaymentType::PAD . ' )
						AND payment_status_type_id IN ( ' . CPaymentStatusType::AUTHORIZED . ', ' . CPaymentStatusType::CAPTURED . ' )
						AND cid = ' . ( int ) $intCid . '
						AND check_account_number_encrypted = \'' . CEncryption::encryptText( trim( $strAchAccountNumber ), CONFIG_KEY_ACH_ACCOUNT_NUMBER ) . '\'
						AND check_routing_number = \'' . addslashes( trim( $strAchRoutingNumber ) ) . '\'
						AND customer_id IS NOT NULL
					ORDER BY id desc';

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchLastArPaymentByAchAccountNumberByAchRoutingNumberByExcludingArPaymentIdAndCheckIsMoneyOrderByCid( $strAchAccountNumber, $strAchRoutingNumber, $intArPaymentId, $intCid, $objPaymentDatabase ) {

		if( true == is_null( $strAchAccountNumber ) ) return NULL;
		if( true == is_null( $strAchRoutingNumber ) ) return NULL;

		$strAchRoutingNumber = CStrings::strTrimDef( $strAchRoutingNumber, 20, NULL, true );
		$strAchAccountNumberBindexEncrypted = \Psi\Libraries\Cryptography\CCrypto::createService()->blindIndex( $strAchAccountNumber, CONFIG_SODIUM_KEY_BLIND_INDEX );

		$strSql = 'SELECT * FROM view_ar_payments
					WHERE
						payment_type_id IN ( ' . CPaymentType::CHECK_21 . ', ' . CPaymentType::ACH . ', ' . CPaymentType::PAD . ' )
						AND payment_status_type_id IN ( ' . CPaymentStatusType::AUTHORIZED . ', ' . CPaymentStatusType::CAPTURED . ' )
						AND id != ' . ( int ) $intArPaymentId . '
						AND customer_id IS NOT NULL
						AND check_is_money_order = 0
						AND cid = ' . ( int ) $intCid . '
						AND check_account_number_bindex = \'' . $strAchAccountNumberBindexEncrypted . '\'
						AND check_routing_number = \'' . addslashes( trim( $strAchRoutingNumber ) ) . '\'
					ORDER BY id desc
					LIMIT 1';

		return self::fetchArPayment( $strSql, $objPaymentDatabase );
	}

	// @FIXME: $boolIsAdministrator is never used

	public static function fetchNewPaginatedArPaymentsByPropertyIdsByCid( $arrintPropertyIds, $intPageNo = NULL, $intPageSize = NULL, $objArPaymentsFilter = NULL, $intCid, $objClientDatabase, $boolSortAscending = false, $objPaymentDatabase = NULL, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strCheckDeletedUnitsSql 		 = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql 	 = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		$intOffset 	= ( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) ? ( ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0 ) : 0;
		$intLimit	= ( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) ? $intPageSize : NULL;

		$strSortFormat = ( true == $boolSortAscending ) ? 'ASC' : 'DESC';

		$arrstrAndPaymentsDatabaseSearchParameters = NULL;

		if( true == valObj( $objArPaymentsFilter, 'CArPaymentsFilter' ) ) {
			$objArPayment = CArPayments::fetchWorksArPaymentByIdByCid( $objArPaymentsFilter->getArPaymentId(), $intCid, $objClientDatabase );

			if( false == valObj( $objArPayment, 'CArPayment' ) ) {

				$objArPaymentsFilter->setArTransactionId( $objArPaymentsFilter->getArPaymentId() );
				$objArPaymentsFilter->setArPaymentId( NULL );
			}

			$arrstrAndPaymentsDatabaseSearchParameters = self::fetchNewPaymentsDatabaseSearchCriteria( $objArPaymentsFilter );
			$arrstrAndSearchParameters = self::fetchNewEntrataDatabaseSearchCriteria( $objArPaymentsFilter );

		}

		$arrstrAndParameters = [];

		$arrstrExplodedSearch = explode( ' ', $objArPaymentsFilter->getKeywordAdvancedSearch() );
		$arrstrExplodedSearch = array_filter( array_map( 'trim', $arrstrExplodedSearch ) );

		if( true == valArr( $arrstrExplodedSearch ) ) {
			foreach( $arrstrExplodedSearch as $strExplodedSearch ) {
				if( false === strrchr( $strExplodedSearch, '\'' ) ) {
					$strFilteredExplodedSearch = $strExplodedSearch;
				} else {
					$strFilteredExplodedSearch = pg_escape_string( $strExplodedSearch );
				}
				array_push( $arrstrAndParameters, $strFilteredExplodedSearch );
			}
		}

		$strArPaymentIds = NULL;

		if( true == valObj( $objPaymentDatabase, 'CDatabase' ) && true == valArr( $arrstrAndPaymentsDatabaseSearchParameters ) ) {

			$strSql = 'SELECT
							array_to_string(array_agg(ap.id), \',\') AS ar_payment_ids
						FROM
							ar_payments AS ap
							JOIN ar_payment_billings apb ON ( ap.id = apb.ar_payment_id ) ';

			$strSql .= ' WHERE
							ap.cid = ' . ( int ) $intCid . ' AND ap.payment_status_type_id !=' . CPaymentStatusType::BATCHING . '
							AND ap.property_id IN (' . implode( ',', $arrintPropertyIds ) . ') ';

			$strSql .= ' AND ' . implode( ' AND ', $arrstrAndPaymentsDatabaseSearchParameters );

			if( true == valArr( $arrstrAndSearchParameters ) ) {
				$strSql .= ' AND ' . implode( ' AND ', $arrstrAndSearchParameters );

				if( 1 != \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) {
					if( true == valArr( $arrstrAndParameters ) ) {
						$intCountPaymentParameters = \Psi\Libraries\UtilFunctions\count( $arrstrAndParameters );
						for( $intPaymentParameter = 0; $intPaymentParameter < $intCountPaymentParameters; $intPaymentParameter++ ) {
							$strAndParameters = str_replace( ',', '', $arrstrAndParameters[$intPaymentParameter] );

							$strBuildingName = $strAndParameters;
							$strBillToUnitNumber = $strAndParameters;
							if( \Psi\CStringService::singleton()->strstr( $strAndParameters, '-' ) && 1 < strlen( $strAndParameters ) ) {

								$arrstrUnitNumber = explode( '-', $strAndParameters );
								$strBuildingName = $arrstrUnitNumber[0];
								$strBillToUnitNumber = $arrstrUnitNumber[1];
							}

							$strSql .= ' AND ( ( ap.billto_name_first ILIKE  E\'%' . $strAndParameters . '%\' OR  ap.billto_name_last ILIKE  E\'%' . $strAndParameters . '%\' OR  ap.billto_unit_number ILIKE  E\'%' . $strBillToUnitNumber . '%\' )
										 AND ( ap.billto_name_first ILIKE  E\'%' . $strAndParameters . '%\' OR  ap.billto_name_last ILIKE  E\'%' . $strAndParameters . '%\' OR  ap.billto_unit_number ILIKE  E\'%' . $strBillToUnitNumber . '%\' )
										 AND ( ap.billto_name_first ILIKE  E\'%' . $strAndParameters . '%\' OR  ap.billto_name_last ILIKE  E\'%' . $strAndParameters . '%\' OR  ap.billto_unit_number ILIKE  E\'%' . $strBillToUnitNumber . '%\' ) ) ';
						}
					}
				}
			}

			if( false == is_null( $objArPaymentsFilter->getBilltoUnitNumber() ) ) {
				$strSql .= ' AND LOWER(ap.billto_unit_number) = LOWER(\'' . addslashes( $objArPaymentsFilter->getBilltoUnitNumber() ) . '\') ';
			}

			$arrmixPaymentIds = fetchData( $strSql, $objPaymentDatabase );

			if( true == isset( $arrmixPaymentIds[0] ) && true == isset( $arrmixPaymentIds[0]['ar_payment_ids'] ) ) {
				$strArPaymentIds = $arrmixPaymentIds[0]['ar_payment_ids'];
			}

			if( true == is_null( $strArPaymentIds ) ) {
				$strArPaymentIds = 'NULL';
			}
		}

		$strSql = 'SELECT
						ap.*,
						db.blob
					FROM
						ar_payments AS ap';

		if( false == is_null( $objArPaymentsFilter->getArTransactionId() ) && true == is_numeric( $objArPaymentsFilter->getArTransactionId() ) ) {
			$strSql .= ' LEFT JOIN ar_transactions at ON ( at.ar_payment_id = ap.id AND at.cid = ap.cid )';
		}

		$strSql .= ' LEFT JOIN data_blobs db ON ( db.lease_id = ap.lease_id AND db.customer_id = ap.customer_id AND db.cid = ap.cid )';

		if( false == is_null( $objArPaymentsFilter->getBilltoUnitNumber() ) || true == valArr( $arrstrAndParameters ) ) {
			$strSql .= ' LEFT JOIN leases l ON ( ap.lease_id = l.id AND ap.cid = l.cid )
					 	 LEFT JOIN unit_spaces us ON ( l.unit_space_id = us.id AND l.cid = us.cid ' . $strCheckDeletedUnitSpacesSql . ' )
					 	 LEFT JOIN property_units pu ON ( us.property_unit_id = pu.id AND us.cid = pu.cid ' . $strCheckDeletedUnitsSql . ' )
						 LEFT JOIN property_buildings pb ON ( pb.id = pu.property_building_id AND pb.cid = pu.cid AND pb.deleted_on IS NULL )';
		}

		$arrstrPaymentFilter = $objArPaymentsFilter->getPaymentFilterIds();
		$arrstrPaymentFilter = explode( ',', $arrstrPaymentFilter );

		if( true == arrayValuesExistsInArray( [ CArPaymentsFilter::INTEGRATED_RETURNED_FAILED_PAYMENT ], $arrstrPaymentFilter ) ) {

			$strSql .= ' LEFT JOIN
						 (
							SELECT
								*,
								rank() OVER ( PARTITION BY reference_number
							ORDER BY
								id DESC ) as rn
							FROM
								integration_results
							WHERE
								integration_results.cid = ' . ( int ) $intCid . '
						 ) as ir ON ( ap.cid = ir.cid and ap.id = ir.reference_number )';
		}

		if( 0 < $objArPaymentsFilter->getApplicationId() ) {
			$strSql .= ' INNER JOIN application_payments app ON ( app.ar_payment_id = ap.id AND app.cid = ap.cid AND app.application_id = ' . ( int ) $objArPaymentsFilter->getApplicationId() . ' ) ';
		}

		$strSql .= ' WHERE
						ap.cid = ' . ( int ) $intCid . ' AND ap.payment_status_type_id !=' . CPaymentStatusType::BATCHING . '
						AND ap.property_id IN (' . implode( ',', $arrintPropertyIds ) . ') ';

		if( true == valArr( $arrstrAndSearchParameters ) ) {

			$strSql .= ' AND ' . implode( ' AND ', $arrstrAndSearchParameters );

			if( 1 != \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) {
				if( true == valArr( $arrstrAndParameters ) ) {
					$intCountPaymentParameters = \Psi\Libraries\UtilFunctions\count( $arrstrAndParameters );
					for( $intPaymentParameter = 0;  $intPaymentParameter < $intCountPaymentParameters; $intPaymentParameter++ ) {
						$strAndParameters = str_replace( ',', '', $arrstrAndParameters[$intPaymentParameter] );

						$strBuildingName = $strAndParameters;
						$strBillToUnitNumber = $strAndParameters;
						if( \Psi\CStringService::singleton()->strstr( $strAndParameters, '-' ) && 1 < strlen( $strAndParameters ) ) {

							$arrstrUnitNumber = explode( '-', $strAndParameters );
							$strBuildingName = $arrstrUnitNumber[0];
							$strBillToUnitNumber = $arrstrUnitNumber[1];
						}

						$strSql .= ' AND ( ( ap.billto_name_first ILIKE  E\'%' . $strAndParameters . '%\' OR  ap.billto_name_last ILIKE  E\'%' . $strAndParameters . '%\' OR  ap.billto_unit_number ILIKE  E\'%' . $strBillToUnitNumber . '%\' OR pb.building_name ILIKE  E\'%' . $strBuildingName . '%\' )
									 AND ( ap.billto_name_first ILIKE  E\'%' . $strAndParameters . '%\' OR  ap.billto_name_last ILIKE  E\'%' . $strAndParameters . '%\' OR  ap.billto_unit_number ILIKE  E\'%' . $strBillToUnitNumber . '%\' OR pb.building_name ILIKE  E\'%' . $strBuildingName . '%\' )
									 AND ( ap.billto_name_first ILIKE  E\'%' . $strAndParameters . '%\' OR  ap.billto_name_last ILIKE  E\'%' . $strAndParameters . '%\' OR  ap.billto_unit_number ILIKE  E\'%' . $strBillToUnitNumber . '%\' OR pb.building_name ILIKE  E\'%' . $strBuildingName . '%\' ) ) ';
					}
				}
			}

		}

		if( false == is_null( $objArPaymentsFilter->getBilltoUnitNumber() ) ) {
			$strSql .= ' AND CASE
								WHEN ap.billto_unit_number IS NOT NULL
								THEN LOWER(ap.billto_unit_number) = LOWER(\'' . addslashes( $objArPaymentsFilter->getBilltoUnitNumber() ) . '\')
							ELSE
								( LOWER(pu.unit_number) = LOWER(\'' . addslashes( $objArPaymentsFilter->getBilltoUnitNumber() ) . '\'))
							END ';
		}

		if( true == valArr( $arrstrAndPaymentsDatabaseSearchParameters ) ) {
			$strSql .= ' AND  ap.id IN ( ' . $strArPaymentIds . ' ) ';
		}

		if( true == arrayValuesExistsInArray( [ CArPaymentsFilter::PAYMENT_FILTER_TYPE_APPLICATION ], $arrstrPaymentFilter ) ) {
			$strSql .= ' AND ap.id IN ( SELECT app.ar_payment_id FROM application_payments app WHERE app.cid = ' . ( int ) $objArPaymentsFilter->getCid() . ' )  ';
		}
		if( true == arrayValuesExistsInArray( [ CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_APPLICATION ], $arrstrPaymentFilter ) ) {
			$strSql .= ' AND  ap.id NOT IN ( SELECT app.ar_payment_id FROM application_payments app WHERE app.cid = ' . ( int ) $objArPaymentsFilter->getCid() . ' )  ';
		}

		if( true == arrayValuesExistsInArray( [ CArPaymentsFilter::INTEGRATED_RETURNED_FAILED_PAYMENT ], $arrstrPaymentFilter ) ) {
			$strSql .= ' AND ir.rn = 1 AND ir.is_failed = 1 AND ap.payment_status_type_id =  ' . CPaymentStatusType::RECALL . ' AND ir.integration_service_id = ' . CIntegrationService::RETURN_AR_PAYMENT;
		}

		$strSql .= ' ORDER BY ap.payment_datetime ' . $strSortFormat . ' ';

		if( 0 < $intOffset ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset;
		}

		if( false == is_null( $intLimit ) ) {
			$strSql .= ' LIMIT ' . ( int ) $intLimit;
		}

		return self::fetchArPayments( $strSql, $objClientDatabase );
	}

	public static function fetchCustomCheck21ArPaymentsByCid( $intCid, $objClientDatabase, $arrmixFilterFieldValues ) {
		if( false == valArr( $arrmixFilterFieldValues['property_ids'] ) ) {
			return NULL;
		}
		$arrintPropertyGroupIds     = $arrmixFilterFieldValues['property_group_ids'];
		$strMinPaymentDate          = $arrmixFilterFieldValues['payment_dates']['min-date'];
		$strMaxPaymentDate          = $arrmixFilterFieldValues['payment_dates']['max-date'];
		$intPaymentId               = $arrmixFilterFieldValues['payment_id'];
		$fltMinPaymentAmount        = $arrmixFilterFieldValues['amounts']['min-amount'];
		$fltMaxPaymentAmount        = $arrmixFilterFieldValues['amounts']['max-amount'];
		$arrintPropertyIds          = $arrmixFilterFieldValues['property_ids'];
		$arrintPaymentTypeId        = $arrmixFilterFieldValues['payment_types'];
		$arrintPaymentStatusTypeIds = $arrmixFilterFieldValues['payment_status_types'];
		$arrintPaymentFilterIds			= ( array ) $arrmixFilterFieldValues['filter'];
		$arrintLeaseStatusTypeIds   = ( true == isset( $strMinPaymentDate ) ) ? ( ( strtotime( '06/06/2013' ) <= strtotime( $strMinPaymentDate ) ) ? $arrmixFilterFieldValues['lease_status_type_ids'] : '' ) : $arrmixFilterFieldValues['lease_status_type_ids'];
		$intRecurringPaymentId      = $arrmixFilterFieldValues['recurring_payment_id'];
		$strCheckNumber             = $arrmixFilterFieldValues['check_number'];
		$intLeaseId                 = $arrmixFilterFieldValues['lease_id'];
		$strQuickSearch				= $arrmixFilterFieldValues['quick_search'];
		$arrstrTextSearchParameters = [];

		$arrstrExplodedSearch = explode( ' ', $strQuickSearch );
		$arrstrExplodedSearch = array_filter( array_map( 'trim', $arrstrExplodedSearch ) );

		// For Text Parameter
		if( true == valArr( $arrstrExplodedSearch ) ) {
			foreach( $arrstrExplodedSearch as $strExplodedSearch ) {
				if( false === strrchr( $strExplodedSearch, '\'' ) ) {
					$strFilteredExplodedSearch = $strExplodedSearch;
				} else {
					$strFilteredExplodedSearch = pg_escape_string( $strExplodedSearch );
				}
				array_push( $arrstrTextSearchParameters, $strFilteredExplodedSearch );
			}
		}

		if( true == valArr( $arrintPropertyGroupIds ) ) {
			$strSql = 'SELECT
							property_id
						FROM
							load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', ( array ) $arrintPropertyGroupIds ) . ' ]::integer[] ) lp
						WHERE
							lp.is_disabled = 0';

			$arrintLoadPropertyIds  = fetchData( $strSql, $objClientDatabase );
			$arrintPropertyGroupIds = array_keys( rekeyArray( 'property_id', $arrintLoadPropertyIds ) );
		}

		$arrintFinalPropertyIds = ( array ) ( true == valArr( $arrintPropertyGroupIds ) && true == valArr( $arrintPropertyIds ) ) ? array_intersect( $arrintPropertyGroupIds, $arrintPropertyIds ) : '';

		$strFields = 'SELECT
						ap.id, 
						ap.lease_id, 
						ap.customer_id, 
						ap.payment_amount, 
						ap.payment_datetime,
						db.data_field1 AS billto_name_last_matronymic,
						COALESCE( split_part( db.blob, \'~..$\', 5 ), ap.billto_unit_number ) AS billto_unit_number,
						COALESCE( split_part( db.blob, \'~..$\', 1 ), ap.billto_name_first ) AS billto_name_first,
						COALESCE( split_part( db.blob, \'~..$\', 2 ), ap.billto_name_last ) AS billto_name_last
					FROM
						ar_payments ap
					JOIN properties p ON ( p.cid = ap.cid AND p.id = ap.property_id and p.is_disabled = 0 )
					JOIN payment_status_types pst ON ( pst.id = ap.payment_status_type_id )
					JOIN payment_types pt ON ( pt.id = ap.payment_type_id )
					LEFT JOIN cached_leases cls ON ( cls.cid = ap.cid AND cls.primary_customer_id = ap.customer_id AND cls.property_id = ap.property_id AND cls.id = ap.lease_id )
					LEFT JOIN lease_status_types lst ON (  lst.id = ap.lease_status_type_id )
					LEFT JOIN data_blobs db ON ( db.cid = ap.cid AND db.lease_id = ap.lease_id AND db.customer_id = ap.customer_id )
					LEFT JOIN unit_spaces us ON ( us.cid = ap.cid AND us.property_id = ap.property_id AND us.id = cls.unit_space_id AND us.deleted_on IS NULL )
					LEFT JOIN property_units pu ON ( pu.cid = ap.cid AND pu.property_id = ap.property_id AND pu.id = cls.property_unit_id AND pu.deleted_on IS NULL )
					LEFT JOIN property_buildings pb ON ( pb.cid = pu.cid AND pb.property_id = pu.property_id AND pb.id = pu.property_building_id AND pb.deleted_on IS NULL ) ';

		// WHERE sql
		$strWhereSql = '';
		$strWhereSql .= 'ap.cid = ' . ( int ) $intCid . ' AND ap.payment_status_type_id != ' . CPaymentStatusType::BATCHING;

		if( true == !empty( $strMinPaymentDate ) ) {
			$strWhereSql .= ' AND ap.payment_datetime >= \'' . date( 'Y-m-d', strtotime( $strMinPaymentDate ) ) . ' 00:00:00\'';
		}
		if( true == !empty( $strMaxPaymentDate ) ) {
			$strWhereSql .= ' AND ap.payment_datetime <= \'' . date( 'Y-m-d', strtotime( $strMaxPaymentDate ) ) . ' 23:59:59\'';
		}
		if( true == valId( $intPaymentId ) ) {
			$strWhereSql .= ' AND ap.id = ' . ( int ) $intPaymentId;
		}
		if( true == valStr( $fltMinPaymentAmount ) ) {
			$strWhereSql .= ' AND ap.payment_amount >= ' . ( float ) $fltMinPaymentAmount;
		}
		if( true == valStr( $fltMaxPaymentAmount ) ) {
			$strWhereSql .= ' AND ap.payment_amount <= ' . ( float ) $fltMaxPaymentAmount;
		}
		if( true == valArr( $arrintFinalPropertyIds ) ) {
			$strWhereSql .= ' AND ap.property_id IN ( ' . implode( ',', $arrintFinalPropertyIds ) . ' ) ';
		}

		$strWhereSql .= 'AND ap.payment_type_id IN ( ' . implode( ',', $arrintPaymentTypeId ) . ' ) ';

		// Prepare sql where conditions based on Payment Filter selected from UI.
		$arrmixPaymentFilterConditions = [];

		if( true == in_array( CArPaymentsFilter::PAYMENT_FILTER_TYPE_FLOATING, $arrintPaymentFilterIds ) )
			$arrmixPaymentFilterConditions[CArPaymentsFilter::PAYMENT_FILTER_TYPE_FLOATING] = ' ap.lease_id IS NULL ';
		if( true == in_array( CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_FLOATING, $arrintPaymentFilterIds ) )
			$arrmixPaymentFilterConditions[CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_FLOATING] = '  ap.lease_id IS NOT NULL ';
		if( true == in_array( CArPaymentsFilter::PAYMENT_FILTER_TYPE_RECURRING, $arrintPaymentFilterIds ) )
			$arrmixPaymentFilterConditions[CArPaymentsFilter::PAYMENT_FILTER_TYPE_RECURRING] = ' ap.scheduled_payment_id IS NOT NULL ';
		if( true == in_array( CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_RECURRING, $arrintPaymentFilterIds ) )
			$arrmixPaymentFilterConditions[CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_RECURRING] = ' ap.scheduled_payment_id IS NULL ';
		if( true == in_array( CArPaymentsFilter::PAYMENT_FILTER_TYPE_INTEGRATED, $arrintPaymentFilterIds ) )
			$arrmixPaymentFilterConditions[CArPaymentsFilter::PAYMENT_FILTER_TYPE_INTEGRATED] = '  ap.remote_primary_key IS NOT NULL ';
		if( true == in_array( CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_INTEGRATED, $arrintPaymentFilterIds ) )
			$arrmixPaymentFilterConditions[CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_INTEGRATED] = ' ( ap.remote_primary_key IS NULL AND ap.payment_status_type_id != ' . CPaymentStatusType::DECLINED . ' ) ';

		$strEntrataDbPaymentFilterConditionSql = ( true == valArr( $arrmixPaymentFilterConditions ) ) ? ' AND ( ' . implode( ' AND ',  $arrmixPaymentFilterConditions ) . ' ) ' : ' ';

		if( false == valId( $intPaymentId ) && true == valArr( $arrstrTextSearchParameters ) ) {

			$intTextSearchParametersCount = \Psi\Libraries\UtilFunctions\count( $arrstrTextSearchParameters );
			for( $intPaymentParameter = 0;  $intPaymentParameter < $intTextSearchParametersCount; $intPaymentParameter++ ) {

				$strAndParameters	= str_replace( ',', '', $arrstrTextSearchParameters[$intPaymentParameter] );
				$strBuildingName	= $strBillToUnitNumber = $strAndParameters;

				if( \Psi\CStringService::singleton()->strstr( $strAndParameters, '-' ) && 1 < strlen( $strAndParameters ) ) {

					$arrstrUnitNumber		= explode( '-', $strAndParameters );
					$strBuildingName		= $arrstrUnitNumber[0];
					$strBillToUnitNumber	= $arrstrUnitNumber[1];
				}

				$strWhereSql .= ' AND ( COALESCE( split_part( db.blob, \'~..$\', 1 ), ap.billto_name_first ) ILIKE  E\'%' . $strAndParameters . '%\' OR  COALESCE( split_part( db.blob, \'~..$\', 2 ), ap.billto_name_last ) ILIKE  E\'%' . $strAndParameters . '%\' OR  COALESCE( split_part( db.blob, \'~..$\', 5 ), ap.billto_unit_number ) ILIKE  E\'%' . $strAndParameters . '%\' OR pb.building_name ILIKE  E\'%' . $strBuildingName . '%\' ) ';
			}
		}

		if( true == valArr( $arrintPaymentStatusTypeIds ) ) {
			$strWhereSql .= ' AND ap.payment_status_type_id IN ( ' . implode( ',', $arrintPaymentStatusTypeIds ) . ' )';
		}
		if( true == valArr( $arrintLeaseStatusTypeIds ) ) {
			$strWhereSql .= ' AND ap.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )';
		}
		if( true == valStr( $strCheckNumber ) ) {
			$strWhereSql .= ' AND ap.check_number =  \'' . addslashes( $strCheckNumber ) . '\'';
		}
		if( true == valId( $intLeaseId ) ) {
			$strWhereSql .= ' AND ap.lease_id = ' . ( int ) $intLeaseId;
		}

		$strSql = $strFields .
					' WHERE '
					 . $strWhereSql . ' ' . $strEntrataDbPaymentFilterConditionSql;
		return self::fetchArPayments( $strSql, $objClientDatabase );
	}

	// @TODO: note that $boolGetTotalCount is no longer used. The code cleanup should be done when this function is touched next

	public static function fetchSimpleArPaymentsByCid( $intCid, $objClientDatabase, $objPaymentDatabase, $arrmixFilterFieldValues, $intOffset = 0, $intLimit = 15, $strSortBy = NULL, $boolIsDownload = false, $boolReturnArrObj = false, $boolGetTotalCount = false, $boolIsFromFilter = true, $boolShowDisabledData = false ) {

		if( false == valArr( $arrmixFilterFieldValues['property_ids'] ) ) return NULL;

		$arrintPropertyGroupIds			= $arrmixFilterFieldValues['property_group_ids'];
		$strMinPaymentDate				= $arrmixFilterFieldValues['payment_dates']['min-date'];
		$strMaxPaymentDate				= $arrmixFilterFieldValues['payment_dates']['max-date'];
		$intPaymentId					= $arrmixFilterFieldValues['payment_id'];
		$intTransactionId				= $arrmixFilterFieldValues['transaction_id'];
		$fltMinPaymentAmount			= $arrmixFilterFieldValues['amounts']['min-amount'];
		$fltMaxPaymentAmount			= $arrmixFilterFieldValues['amounts']['max-amount'];
		$arrintPropertyIds				= $arrmixFilterFieldValues['property_ids'];
		$arrmixPaymentTypeIds			= $arrmixFilterFieldValues['payment_types'];
		$arrintPaymentStatusTypeIds		= $arrmixFilterFieldValues['payment_status_types'];
		$arrstrPaymentSourceValues		= $arrmixFilterFieldValues['payment_source'];
		$arrintPaymentFilterIds			= ( array ) $arrmixFilterFieldValues['filter'];
		$arrintLeaseStatusTypeIds		= ( true == isset( $strMinPaymentDate ) ) ? ( ( strtotime( '06/06/2013' ) <= strtotime( $strMinPaymentDate ) ) ? $arrmixFilterFieldValues['lease_status_type_ids'] : '' ) :  $arrmixFilterFieldValues['lease_status_type_ids'];
		$strAccountNumber				= $arrmixFilterFieldValues['account_number'];
		$intRecurringPaymentId			= $arrmixFilterFieldValues['recurring_payment_id'];
		$strCheckNumber					= $arrmixFilterFieldValues['check_number'];
		$intApplicationId				= $arrmixFilterFieldValues['application_id'];
		$intLeaseId						= $arrmixFilterFieldValues['lease_id'];
		$strQuickSearch					= $arrmixFilterFieldValues['quick_search'];

		$arrstrTextSearchParameters = [];

		$arrstrExplodedSearch = explode( ' ', $strQuickSearch );
		$arrstrExplodedSearch = array_filter( array_map( 'trim', $arrstrExplodedSearch ) );

		// For Text Parameter
		if( true == valArr( $arrstrExplodedSearch ) ) {
			foreach( $arrstrExplodedSearch as $strExplodedSearch ) {
				if( false === strrchr( $strExplodedSearch, '\'' ) ) {
					$strFilteredExplodedSearch = $strExplodedSearch;
				} else {
					$strFilteredExplodedSearch = pg_escape_string( $strExplodedSearch );
				}
				array_push( $arrstrTextSearchParameters, $strFilteredExplodedSearch );
			}
		}

		if( true == valId( $intTransactionId ) ) {
			$strMinPaymentDate = $strMaxPaymentDate = NULL;
		}

		if( true == valId( $intPaymentId ) ) {
			$intTransactionId = $strMinPaymentDate = $strMaxPaymentDate = $fltMinPaymentAmount = $fltMaxPaymentAmount = $strAccountNumber = $intRecurringPaymentId = $strCheckNumber = $intApplicationId = $intLeaseId = $strQuickSearch = NULL;
			$arrintPropertyGroupIds = $arrintPaymentFilterIds = $arrintPropertyIds = $arrmixPaymentTypeIds = $arrintPaymentStatusTypeIds = $arrstrPaymentSourceValues = $arrintLeaseStatusTypeIds = [];
		}

		if( true == valArr( $arrintPropertyGroupIds ) ) {
			$strSql = 'SELECT
							property_id
						FROM
							load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', ( array ) $arrintPropertyGroupIds ) . ' ]::integer[] ) lp
						' . ( ( false == $boolShowDisabledData ) ? 'WHERE lp.is_disabled = 0' : '' );

			$arrintLoadPropertyIds = ( array ) fetchData( $strSql, $objClientDatabase );
			$arrintPropertyGroupIds = array_keys( rekeyArray( 'property_id', $arrintLoadPropertyIds ) );
		}
		// JOIN sql
		$strJoinSql = $strWhereSql = $strEntrataDbWhereSql = $strPaymentDbWhereSql = NULL;

		$strSortBy = ( false == valStr( $strSortBy ) ? ' ap.payment_datetime DESC ' : $strSortBy );
		$intOffset = ( 0 < $intOffset ? ' OFFSET ' . ( int ) $intOffset : '' );
		$intLimit = ( 0 < $intLimit ? ' LIMIT ' . ( int ) $intLimit : '' );
		if( true == !empty( $strMinPaymentDate ) || true == !empty( $strMaxPaymentDate ) ) {
			$strTimeZoneFeilds = ' ( ( ap.payment_datetime::TIMESTAMP AT TIME ZONE \'MST\' ) AT TIME ZONE tz.abbr ) as payment_datetime_converted,';
		}
		$strFields = '	ap.id,
						ap.cid,
						ap.property_id,
						p.property_name,
						ap.customer_id,
						ap.lease_id,
						ap.payment_type_id,
						ap.payment_status_type_id,
						pst.name AS payment_status_type,
						pt.name AS payment_type,
						ap.ps_product_id,
						ap.ps_product_option_id,
						ap.payment_datetime,
						' . $strTimeZoneFeilds . '
						ap.check_number,
						ap.captured_on,
						ap.returned_on,
						ap.distribute_on,
						ap.payment_amount,
						ap.convenience_fee_amount,
						ap.donation_amount,
						ap.currency_code,
						apd.is_debit_card,
						apd.is_prepaid_card,
						apd.is_gift_card,
						apd.is_check_card,
						apd.is_credit_card,
						apd.is_international_card,
						apd.is_commercial_card,
						cls.company_name,
						cls.occupancy_type_id,
						lst.id AS lease_status_type_id,
						COALESCE( split_part( db.blob, \'~..$\', 5 ), ap.billto_unit_number ) AS billto_unit_number,
						COALESCE( split_part( db.blob, \'~..$\', 1 ), ap.billto_name_middle ) AS billto_name_middle,
						COALESCE( split_part( db.blob, \'~..$\', 1 ), ap.billto_name_first ) AS billto_name_first,
						COALESCE( split_part( db.blob, \'~..$\', 2 ), ap.billto_name_last ) AS billto_name_last,
						COALESCE( split_part( db.blob, \'~..$\', 7 ), lst.name ) AS lease_status,
						COALESCE( split_part( db.blob, \'~..$\', 13 ), cls.company_name ) AS business_name,
						ap.remote_primary_key,
						ap.return_remote_primary_key,
						ap.is_reversed,
						CASE WHEN app.id IS NOT NULL THEN 1 ELSE 0 END AS is_application_payment';

		$strJoinSql .= 'ar_payments ap ';

		$strTimeZoneJoin = '';
		if( true == !empty( $strMinPaymentDate ) || true == !empty( $strMaxPaymentDate ) ) {
			$strTimeZoneJoin = 'JOIN time_zones tz ON ( tz.id = p.time_zone_id)';
		}
		if( false == $boolGetTotalCount || ( true == $boolGetTotalCount && true == $boolIsFromFilter ) ) {
			$strJoinSql .= 'LEFT JOIN ar_payment_details apd ON ( ap.id = apd.ar_payment_id AND ap.cid = apd.cid )
							JOIN properties p ON ( p.cid = ap.cid AND p.id = ap.property_id ' . ( ( false == $boolShowDisabledData ) ? 'and p.is_disabled = 0' : '' ) . ' ) ' . $strTimeZoneJoin . '
							JOIN payment_status_types pst ON ( pst.id = ap.payment_status_type_id )
							JOIN payment_types pt ON ( pt.id = ap.payment_type_id )
							LEFT JOIN cached_leases cls ON ( cls.cid = ap.cid AND cls.primary_customer_id = ap.customer_id AND cls.property_id = ap.property_id AND cls.id = ap.lease_id )
							LEFT JOIN lease_status_types lst ON (  lst.id = ap.lease_status_type_id )
							LEFT JOIN data_blobs db ON ( db.cid = ap.cid AND db.lease_id = ap.lease_id AND db.customer_id = ap.customer_id )
							LEFT JOIN application_payments app ON ( app.ar_payment_id = ap.id AND app.cid = ap.cid )';
		}
		if( false == valId( $intPaymentId ) && true == valId( $intTransactionId ) ) {
			$strJoinSql .= ' JOIN ar_transactions at ON ( at.cid = ap.cid AND at.property_id = ap.property_id AND at.lease_id = ap.lease_id AND at.ar_payment_id = ap.id AND at.id = ' . ( int ) $intTransactionId . ' ) ';
		}

		// @FIXME: Not sure from where this would come. This CArPaymentsFilter is not seen un UI.
		if( true == in_array( CArPaymentsFilter::INTEGRATED_RETURNED_FAILED_PAYMENT, $arrintPaymentFilterIds ) ) {

			$strJoinSql .= ' JOIN (
									SELECT
										*,
										ROW_NUMBER() OVER ( PARTITION BY cid, reference_number ORDER BY id DESC ) as row_num
									FROM
										integration_results
									WHERE
										cid = ' . ( int ) $intCid . '
										AND integration_service_id = ' . CIntegrationService::RETURN_AR_PAYMENT . '

								) as ir ON ( ap.cid = ir.cid and ap.id = ir.reference_number AND ir.row_num = 1 AND ir.is_failed = 1 ) ';
		}

		if( true == valArr( $arrstrTextSearchParameters ) && ( false == $boolGetTotalCount || ( true == $boolGetTotalCount && true == $boolIsFromFilter ) ) ) {
			$strJoinSql .= ' LEFT JOIN unit_spaces us ON ( us.cid = ap.cid AND us.property_id = ap.property_id AND us.id = cls.unit_space_id AND us.deleted_on IS NULL )
							 LEFT JOIN property_units pu ON ( pu.cid = ap.cid AND pu.property_id = ap.property_id AND pu.id = cls.property_unit_id AND pu.deleted_on IS NULL )
							 LEFT JOIN property_buildings pb ON ( pb.cid = pu.cid AND pb.property_id = pu.property_id AND pb.id = pu.property_building_id AND pb.deleted_on IS NULL ) ';
		}

		$arrintFinalPropertyIds = [];
		$arrintFinalPropertyIds = ( true == valArr( $arrintPropertyGroupIds ) && true == valArr( $arrintPropertyIds ) ) ? array_intersect( $arrintPropertyGroupIds, $arrintPropertyIds ) : '';

		// WHERE sql
		$strWhereSql .= 'ap.cid = ' . ( int ) $intCid . ' AND ap.payment_status_type_id != ' . CPaymentStatusType::BATCHING;

		if( true == !empty( $strMinPaymentDate ) )              $strWhereSql .= ' AND ap.payment_datetime >= \'' . date( 'Y-m-d', strtotime( $strMinPaymentDate ) ) . ' 00:00:00\'';
		if( true == !empty( $strMaxPaymentDate ) )              $strWhereSql .= ' AND ap.payment_datetime <= \'' . date( 'Y-m-d', strtotime( $strMaxPaymentDate ) ) . ' 23:59:59\'';
		if( true == valId( $intPaymentId ) )					$strWhereSql .= ' AND ap.id = ' . ( int ) $intPaymentId;
		if( true == valStr( $fltMinPaymentAmount ) )			$strWhereSql .= ' AND ap.payment_amount >= ' . ( float ) $fltMinPaymentAmount;
		if( true == valStr( $fltMaxPaymentAmount ) )			$strWhereSql .= ' AND ap.payment_amount <= ' . ( float ) $fltMaxPaymentAmount;
		if( true == valArr( $arrintFinalPropertyIds ) )			$strWhereSql .= ' AND ap.property_id IN ( ' . implode( ',', $arrintFinalPropertyIds ) . ' ) ';

		if( true == valArr( $arrmixPaymentTypeIds ) ) {
			$strWhereSql .= 'AND ( ';

			foreach( $arrmixPaymentTypeIds as $intIndex => $arrintPaymentType ) {
				$boolValue = ( 1 == $arrintPaymentType['is_debit_card'] ) ? 'true' : 'false';
				if( 0 < $intIndex ) {
					$strWhereSql .= ' OR ap.payment_type_id = ' . ( int ) $arrintPaymentType['payment_type'];
					if( true == isset( $arrintPaymentType['is_debit_card'] ) )
						$strWhereSql .= ' AND ap.is_debit_card = ' . $boolValue;
				} else {
					$strWhereSql .= 'ap.payment_type_id = ' . ( int ) $arrintPaymentType['payment_type'];
					if( true == isset( $arrintPaymentType['is_debit_card'] ) )
						$strWhereSql .= ' AND ap.is_debit_card = ' . $boolValue;
				}
			}
			$strWhereSql .= ')';
		}

		if( true == valArr( $arrintPaymentStatusTypeIds ) )		$strWhereSql .= ' AND ap.payment_status_type_id IN ( ' . implode( ',', $arrintPaymentStatusTypeIds ) . ' )';

		// Check payment source conditions
		if( true == valArr( $arrstrPaymentSourceValues ) ) {
			// Payment Source could have a product option associated as well.
			$arrintPaymentSourceIds = [];
			$arrintPsProductOptionsKeyedByPaymentSourceId = [];
			foreach( $arrstrPaymentSourceValues as $strPaymentSourceValue ) {
				$arrintPaymentSourceIdAndPsProductOptionId = explode( ',', $strPaymentSourceValue );
				$intPaymentSourceId = $arrintPaymentSourceIdAndPsProductOptionId[0];
				$intPsProductOptionId = ( true == isset( $arrintPaymentSourceIdAndPsProductOptionId[1] ) ) ? $arrintPaymentSourceIdAndPsProductOptionId[1] : NULL;
				if( false == is_null( $intPsProductOptionId ) ) {
					// Ps product options is set.
					if( true == isset( $arrintPsProductOptionsKeyedByPaymentSourceId[$intPaymentSourceId] ) ) {
						// push Ps Product Option onto existing array keyed by this Payment Source ID
						$arrintPsProductOptionsKeyedByPaymentSourceId[$intPaymentSourceId][] = $intPsProductOptionId;
					} else {
						// add new entry with option keyed by payment source
						$arrintPsProductOptionsKeyedByPaymentSourceId[$intPaymentSourceId] = [ $intPsProductOptionId ];
					}
				} else {
					// Add Payment Source to array, Ps Product option isn't set and is ignored.
					$arrintPaymentSourceIds[] = $intPaymentSourceId;
				}
			}

			// Hack for Mobile Portal, since this is controlled by a product option now.
			// Convert Mobile Portal product to Resident Portal product and set Resident Portal Mobile as the option

			if( true == in_array( CPsProduct::MOBILE_PORTAL, $arrintPaymentSourceIds ) ) {
				$arrintPaymentSourceIds = array_diff( $arrintPaymentSourceIds, [ CPsProduct::MOBILE_PORTAL ] );
				if( true == isset( $arrintPsProductOptionsKeyedByPaymentSourceId[CPsProduct::RESIDENT_PORTAL] ) ) {
					$arrintPsProductOptionsKeyedByPaymentSourceId[CPsProduct::RESIDENT_PORTAL][] = CPsProductOption::RESIDENT_PORTAL_MOBILE;
				} else {
					$arrintPsProductOptionsKeyedByPaymentSourceId[CPsProduct::RESIDENT_PORTAL] = [ CPsProductOption::RESIDENT_PORTAL_MOBILE ];
				}
			}

			$arrstrPaymentSourceWhereSql = [];
			if( true == valArr( $arrintPaymentSourceIds ) ) {
				$arrstrPaymentSourceWhereSql[] = '( ap.ps_product_id IN ( ' . implode( ',', $arrintPaymentSourceIds ) . ' ) AND ap.ps_product_option_id IS NULL )';
			}
			if( true == valArr( $arrintPsProductOptionsKeyedByPaymentSourceId ) ) {
				foreach( $arrintPsProductOptionsKeyedByPaymentSourceId as $intPaymentSourceId => $arrintPsProductOptionIds ) {
					$arrstrPaymentSourceWhereSql[] = '( ap.ps_product_id = ' . ( int ) $intPaymentSourceId . ' AND ap.ps_product_option_id IN ( ' . implode( ',', $arrintPsProductOptionIds ) . ' ) ) ';
				}
			}

			$strWhereSql .= ' AND ( ' . implode( ' OR ', $arrstrPaymentSourceWhereSql ) . ' )';
		}

		if( true == valArr( $arrintLeaseStatusTypeIds ) )		$strWhereSql .= ' AND ap.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )';
		if( true == valId( $intRecurringPaymentId ) )			$strWhereSql .= ' AND ap.scheduled_payment_id = ' . ( int ) $intRecurringPaymentId;
		if( true == valStr( $strCheckNumber ) )					$strWhereSql .= ' AND ap.check_number =  \'' . addslashes( $strCheckNumber ) . '\'';
		if( true == valId( $intLeaseId ) )						$strWhereSql .= ' AND ap.lease_id = ' . ( int ) $intLeaseId;

		if( true == valId( $intApplicationId ) && ( false == $boolGetTotalCount || ( true == $boolGetTotalCount && true == $boolIsFromFilter ) ) ) {
			$strEntrataDbWhereSql .= ' AND app.application_id = ' . ( int ) $intApplicationId;
		}

		if( true == valStr( $strAccountNumber ) )				$strPaymentDbWhereSql .= ' AND ( apb.check_routing_number =  \'' . addslashes( $strAccountNumber ) . '\' OR apb.check_account_number_lastfour_encrypted =  \'' . addslashes( $strAccountNumber ) . '\' )';

		// Prepare sql where conditions based on Payment Filter selected from UI.
		$arrmixPaymentFilterConditions = [];
		$arrmixPaymentFilterConditionsForScannedCheck = [];

		if( true == in_array( CArPaymentsFilter::PAYMENT_FILTER_TYPE_FLOATING, $arrintPaymentFilterIds ) )
			$arrmixPaymentFilterConditions[CArPaymentsFilter::PAYMENT_FILTER_TYPE_FLOATING] = ' ap.lease_id IS NULL ';
		if( true == in_array( CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_FLOATING, $arrintPaymentFilterIds ) )
			$arrmixPaymentFilterConditions[CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_FLOATING] = '  ap.lease_id IS NOT NULL ';

		if( true == in_array( CArPaymentsFilter::PAYMENT_FILTER_TYPE_APPLICATION, $arrintPaymentFilterIds ) && ( false == $boolGetTotalCount || ( true == $boolGetTotalCount && true == $boolIsFromFilter ) ) )
			$arrmixPaymentFilterConditions[CArPaymentsFilter::PAYMENT_FILTER_TYPE_APPLICATION] = ' app.id IS NOT NULL ';
		if( true == in_array( CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_APPLICATION, $arrintPaymentFilterIds ) && ( false == $boolGetTotalCount || ( true == $boolGetTotalCount && true == $boolIsFromFilter ) ) )
			$arrmixPaymentFilterConditions[CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_APPLICATION] = ' app.id IS NULL ';

		if( true == in_array( CArPaymentsFilter::PAYMENT_FILTER_TYPE_RECURRING, $arrintPaymentFilterIds ) )
			$arrmixPaymentFilterConditions[CArPaymentsFilter::PAYMENT_FILTER_TYPE_RECURRING] = ' ap.scheduled_payment_id IS NOT NULL ';
		if( true == in_array( CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_RECURRING, $arrintPaymentFilterIds ) )
			$arrmixPaymentFilterConditions[CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_RECURRING] = ' ap.scheduled_payment_id IS NULL ';

		if( true == in_array( CArPaymentsFilter::PAYMENT_FILTER_TYPE_INTEGRATED, $arrintPaymentFilterIds ) )
			$arrmixPaymentFilterConditions[CArPaymentsFilter::PAYMENT_FILTER_TYPE_INTEGRATED] = '  ap.remote_primary_key IS NOT NULL ';
		if( true == in_array( CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_INTEGRATED, $arrintPaymentFilterIds ) )
			$arrmixPaymentFilterConditions[CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_INTEGRATED] = ' ( ap.remote_primary_key IS NULL AND ap.payment_status_type_id != ' . CPaymentStatusType::DECLINED . ' ) ';

		if( true == in_array( CArPaymentsFilter::PAYMENT_FILTER_TYPE_CHECK21_TYPE_CHECK, $arrintPaymentFilterIds ) )
			$arrmixPaymentFilterConditionsForScannedCheck[CArPaymentsFilter::PAYMENT_FILTER_TYPE_CHECK21_TYPE_CHECK] = ' apb.check_is_money_order = 0 AND ap.payment_type_id = ' . CPaymentType::CHECK_21;
		if( true == in_array( CArPaymentsFilter::PAYMENT_FILTER_TYPE_CHECK21_TYPE_MONEY_ORDER, $arrintPaymentFilterIds ) )
			$arrmixPaymentFilterConditionsForScannedCheck[CArPaymentsFilter::PAYMENT_FILTER_TYPE_CHECK21_TYPE_MONEY_ORDER] = ' apb.check_is_money_order = 1 AND ap.payment_type_id = ' . CPaymentType::CHECK_21;

		// @FIXME: Not sure from where this would come. This CArPaymentsFilter is not seen un UI.
		if( true == in_array( CArPaymentsFilter::INTEGRATED_RETURNED_FAILED_PAYMENT, $arrintPaymentFilterIds ) )
			$strWhereSql .= ' AND ap.payment_status_type_id =  ' . CPaymentStatusType::RECALL;

		// Codition for text search parameter in entrata db
		if( false == valId( $intPaymentId ) && true == valArr( $arrstrTextSearchParameters ) ) {

			$intTextSearchParametersCount = \Psi\Libraries\UtilFunctions\count( $arrstrTextSearchParameters );
			for( $intPaymentParameter = 0;  $intPaymentParameter < $intTextSearchParametersCount; $intPaymentParameter++ ) {

				$strAndParameters	= str_replace( ',', '', $arrstrTextSearchParameters[$intPaymentParameter] );
				$strBuildingName	= $strBillToUnitNumber = $strAndParameters;

				if( \Psi\CStringService::singleton()->strstr( $strAndParameters, '-' ) && 1 < strlen( $strAndParameters ) ) {

					$arrstrUnitNumber		= explode( '-', $strAndParameters );
					$strBuildingName		= $arrstrUnitNumber[0];
					$strBillToUnitNumber	= $arrstrUnitNumber[1];
				}

				if( false == $boolGetTotalCount || ( true == $boolGetTotalCount && true == $boolIsFromFilter ) ) {
					$strEntrataDbWhereSql .= ' AND ( COALESCE( split_part( db.blob, \'~..$\', 1 ), ap.billto_name_first ) ILIKE  E\'%' . $strAndParameters . '%\' OR  COALESCE( split_part( db.blob, \'~..$\', 2 ), ap.billto_name_last ) ILIKE  E\'%' . $strAndParameters . '%\' OR  COALESCE( split_part( db.blob, \'~..$\', 5 ), ap.billto_unit_number ) ILIKE  E\'%' . $strAndParameters . '%\' OR pb.building_name ILIKE  E\'%' . $strBuildingName . '%\' ) ';
				}

				$strPaymentDbWhereSql	.= ' AND ( ap.billto_name_first ILIKE E\'%' . $strAndParameters . '%\' OR  ap.billto_name_last ILIKE E\'%' . $strAndParameters . '%\' OR  ap.billto_unit_number ILIKE E\'%' . $strAndParameters . '%\' ) ';
			}
		}

		$strPaymentDbPaymentFilterConditionSql = ( true == valArr( $arrmixPaymentFilterConditionsForScannedCheck ) ) ? ' AND ( ' . implode( ' AND ', $arrmixPaymentFilterConditionsForScannedCheck ) . ' ) ' : ' ';

		$strArPaymentIds = NULL;

		// Get Payment Id's from payment db
		if( true == valObj( $objPaymentDatabase, 'CDatabase' ) && ( true == valArr( $arrmixPaymentFilterConditionsForScannedCheck ) || true == valStr( $strAccountNumber ) ) ) {

			$strSql = 'SELECT
							array_to_string( array_agg( ap.id ), \',\' ) AS ar_payment_ids
						FROM
							ar_payments AS ap
							JOIN ar_payment_billings apb ON ( ap.id = apb.ar_payment_id )
						WHERE
							' . $strWhereSql . '
							' . $strPaymentDbPaymentFilterConditionSql . '
							' . $strPaymentDbWhereSql;

			$arrintPaymentIds = fetchData( $strSql, $objPaymentDatabase );
			$arrintPaymentIds = current( $arrintPaymentIds );
			if( true == isset( $arrintPaymentIds ) && true == isset( $arrintPaymentIds['ar_payment_ids'] ) ) {
				$strArPaymentIds = $arrintPaymentIds['ar_payment_ids'];
			}

			if( true == valStr( $strArPaymentIds ) ) {
				$strWhereSql .= ' AND ap.id IN ( ' . $strArPaymentIds . ' ) ';
			}
		}

		if( false == valId( $intPaymentId ) && false == valStr( $strArPaymentIds ) && true == valArr( $arrmixPaymentFilterConditionsForScannedCheck ) ) {
			return [];
		}

		$strEntrataDbPaymentFilterConditionSql = ( true == valArr( $arrmixPaymentFilterConditions ) ) ? ' AND ( ' . implode( ' AND ',  $arrmixPaymentFilterConditions ) . ' ) ' : ' ';

		if( true == !empty( $strMinPaymentDate ) || true == !empty( $strMaxPaymentDate ) ) {
			$strWithSql = ' WITH arpayments AS ( SELECT '
			          . $strFields .
			          ' FROM '
			          . $strJoinSql .
			          ' WHERE '
			          . $strWhereSql . ' ' . $strEntrataDbPaymentFilterConditionSql . ' ' . $strEntrataDbWhereSql;

			if( false == $boolGetTotalCount ) {
				$strWithSql .= ' ORDER BY '
				           . $strSortBy;
			}

			if( false == $boolIsDownload && false == $boolGetTotalCount ) {
				$strWithSql .= $intOffset;
				$strWithSql .= $intLimit;
			}
			$strWithSql .= ')';
			$strPaymentDateTimeWhereSql = '';
			if( true == !empty( $strMinPaymentDate ) )              $strPaymentDateTimeWhereSql .= ' arpayments.payment_datetime_converted >= \'' . date( 'Y-m-d', strtotime( $strMinPaymentDate ) ) . ' 00:00:00\'';
			if( true == !empty( $strMaxPaymentDate ) ) {
				if( true == valStr( $strPaymentDateTimeWhereSql ) ) {
					$strPaymentDateTimeWhereSql .= ' AND ';
				}
				$strPaymentDateTimeWhereSql .= 'arpayments.payment_datetime_converted <= \'' . date( 'Y-m-d', strtotime( $strMaxPaymentDate ) ) . ' 23:59:59\'';
			}

			$strSql = $strWithSql . ' SELECT
			                            arpayments.*
			                          FROM 
			                            ar_payments ap 
									 JOIN arpayments ON ( ap.id = arpayments.id AND ap.cid = arpayments.cid )
									 WHERE ' .
									    $strPaymentDateTimeWhereSql;

		} else {
			$strSql = ' SELECT '
			          . $strFields .
			          ' FROM '
			          . $strJoinSql .
			          ' WHERE '
			          . $strWhereSql . ' ' . $strEntrataDbPaymentFilterConditionSql . ' ' . $strEntrataDbWhereSql;

			if( false == $boolGetTotalCount ) {
				$strSql .= ' ORDER BY '
				           . $strSortBy;
			}

			if( false == $boolIsDownload && false == $boolGetTotalCount ) {
				$strSql .= $intOffset;
				$strSql .= $intLimit;
			}

		}

		if( true == $boolReturnArrObj && false == $boolGetTotalCount ) {
			return self::fetchArPayments( $strSql, $objClientDatabase );
		} elseif( true == $boolGetTotalCount ) {
			$strSql = 'SELECT COUNT(*) FROM  ' . $strJoinSql . ' WHERE ' . $strWhereSql . ' ' . $strEntrataDbPaymentFilterConditionSql . ' ' . $strEntrataDbWhereSql . ' ';

			$arrintResponse = fetchData( $strSql, $objClientDatabase );

			if( true == isset( $arrintResponse[0]['count'] ) ) {
				return $arrintResponse[0]['count'];
			}

			return 0;
		} else {
			return rekeyArray( 'id', fetchData( $strSql, $objClientDatabase ) );
		}

	}

	public static function fetchPaginatedArPaymentsByCustomerIdByPropertyIdsByCid( $arrintPropertyIds, $intCustomerId, $intPageNo = NULL, $intPageSize = NULL, $objArPaymentsFilter = NULL, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
			$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit  = ( int ) $intPageSize;
		}

		$arrstrAndSearchParameters = [];

		if( true == $objArPaymentsFilter->getUseSelectedDate() ) {
			if( false == is_null( $objArPaymentsFilter->getSelectedStartDate() ) ) 					$arrstrAndSearchParameters[] = ' ap.payment_datetime >= \'' . date( 'Y-m-d', strtotime( $objArPaymentsFilter->getSelectedStartDate() ) ) . ' 00:00:00\'';
			if( false == is_null( $objArPaymentsFilter->getSelectedEndDate() ) ) 					$arrstrAndSearchParameters[] = ' ap.payment_datetime <= \'' . date( 'Y-m-d', strtotime( $objArPaymentsFilter->getSelectedEndDate() ) ) . ' 23:59:59\'';
		}

		$arrintPaymentTypeIds 		= [ CPaymentType::ACH, CPaymentType::VISA, CPaymentType::MASTERCARD, CPaymentType::DISCOVER, CPaymentType::AMEX, CPaymentType::CHECK_21, CPaymentType::EMONEY_ORDER, CPaymentType::PAD ];
		$arrobjPropertyPreferences	= CPropertyPreferences::fetchPropertyPreferencesByPropertyIdsByKeysByCid( $arrintPropertyIds, [ 'SHOW_RESIDENT_ALL_TYPE_PAYMENTS' ], $intCid, $objClientDatabase );
		$arrobjPropertyPreferences	= rekeyObjects( 'PropertyId', $arrobjPropertyPreferences );

		if( true == valArr( $arrobjPropertyPreferences ) ) {
			$arrstrAndSearchParameters[] = ' ( ap.property_id IN (' . implode( ',', array_keys( $arrobjPropertyPreferences ) ) . ') OR ap.payment_type_id IN ( ' . implode( ',', $arrintPaymentTypeIds ) . ' ))';
		} else {
			$arrstrAndSearchParameters[] = ' ap.payment_type_id IN ( ' . implode( ',', $arrintPaymentTypeIds ) . ' ) ';
		}

		// On 1/23/2009 DJB added payment type ids to query to only show electronic payments, instead of all types.
		// At some point we may have companies who want to show all payments so we will have to decide how to handle that (with a setting, or based off what products have been purchased).
		$strSql = 'SELECT
						*
				   FROM
				   		ar_payments AS ap' . '
				   WHERE
				   		ap.customer_id = ' . ( int ) $intCustomerId . '
				   		AND ap.cid = ' . ( int ) $intCid . '
				   		AND ap.payment_status_type_id != ' . CPaymentStatusType::CANCELLED . '
				   		AND ap.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
				   		' . ( ( false == is_null( $arrstrAndSearchParameters ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' ) . '

				   ORDER BY
				   			ap.id DESC';
		if( true == isset( $intOffset ) && 0 < $intOffset ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset;
		}

		if( true == isset( $intLimit ) && false == is_null( $intLimit ) ) {
			$strSql .= ' LIMIT ' . ( int ) $intLimit;
		}

		return self::fetchArPayments( $strSql, $objClientDatabase );
	}

	public static function fetchPaginatedArPaymentsCountByCustomerIdByPropertyIdsByCid( $arrintPropertyIds, $intCustomerId, $objArPaymentsFilter = NULL, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$arrstrAndSearchParameters = [];

		if( true == $objArPaymentsFilter->getUseSelectedDate() ) {
			if( false == is_null( $objArPaymentsFilter->getSelectedStartDate() ) ) 					$arrstrAndSearchParameters[] = ' ap.payment_datetime >= \'' . date( 'Y-m-d', strtotime( $objArPaymentsFilter->getSelectedStartDate() ) ) . ' 00:00:00\'';
			if( false == is_null( $objArPaymentsFilter->getSelectedEndDate() ) ) 					$arrstrAndSearchParameters[] = ' ap.payment_datetime <= \'' . date( 'Y-m-d', strtotime( $objArPaymentsFilter->getSelectedEndDate() ) ) . ' 23:59:59\'';
		}

		$strSql = 'SELECT
						count( ap.id )
				   FROM
						ar_payments AS ap' . '
				   WHERE
						ap.customer_id = ' . ( int ) $intCustomerId . '
						AND ap.cid = ' . ( int ) $intCid . '
						AND ap.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						' . ( ( false == is_null( $arrstrAndSearchParameters ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );

		$arrintResponse = fetchData( $strSql, $objClientDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchPaginatedUndistributedArPaymentsCountByPropertyIdsByArPaymentsFilterByCid( $arrintPropertyIds, $objEntrataFormFilter, $intCid, $objPaymentDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$arrstrAndSearchParameters = [];

		if( true == valObj( $objEntrataFormFilter, 'CEntrataFormFilter' ) ) {

			if( false == is_null( $objEntrataFormFilter->getFilterFieldValue( 'start_date' ) ) ) {
				$arrstrAndSearchParameters[] = ' ap.payment_datetime >= \'' . date( 'Y-m-d', strtotime( $objEntrataFormFilter->getFilterFieldValue( 'start_date' ) ) ) . ' 00:00:00\'';
			}
			if( false == is_null( $objEntrataFormFilter->getFilterFieldValue( 'end_date' ) ) ) {
				$arrstrAndSearchParameters[] = ' ap.payment_datetime <= \'' . date( 'Y-m-d', strtotime( $objEntrataFormFilter->getFilterFieldValue( 'end_date' ) ) ) . ' 23:59:59\'';
			}

			if( false == is_null( $objEntrataFormFilter->getFilterFieldValue( 'unsettled_payment_id' ) ) && '' != $objEntrataFormFilter->getFilterFieldValue( 'unsettled_payment_id' ) ) {
				$arrstrAndSearchParameters[] = ' ap.id::integer = ' . ( int ) $objEntrataFormFilter->getFilterFieldValue( 'unsettled_payment_id' );
			}

			if( true == valArr( $arrintPropertyIds ) ) {
				$arrstrAndSearchParameters[] = ' ap.property_id::integer IN (' . implode( ',', $arrintPropertyIds ) . ')';
			}

		}

	   	if( 0 == \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) {
			return NULL;
		}

		$arrintPaymentStatusTypeIds 		= [ CPaymentStatusType::AUTHORIZED, CPaymentStatusType::CAPTURED, CPaymentStatusType::PHONE_CAPTURE_PENDING ];
		$arrintDistributionStatusTypeIds 	= [ CDistributionStatusType::DEBIT_FAILED, CDistributionStatusType::DEBIT_RETURN, CDistributionStatusType::CREDIT_FAILED, CDistributionStatusType::CREDIT_RETURN ];

		$strSql = 'SELECT
						count(ap.id)
					FROM
						ar_payments AS ap
					JOIN
						(
							SELECT company_merchant_account_id, cid, is_controlled_distribution AS ach_is_controlled_distribution
								FROM merchant_account_methods
									WHERE payment_type_id = ' . ( int ) CPaymentType::ACH . ' 
						) mam ON ( mam.cid = ap.cid AND mam.company_merchant_account_id = ap.company_merchant_account_id AND mam.ach_is_controlled_distribution = 1 )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
					  	AND ap.payment_status_type_id IN ( ' . implode( ',', $arrintPaymentStatusTypeIds ) . ' )
						AND (	NOT EXISTS (
												SELECT apd.id
												FROM ar_payment_distributions apd
							  					JOIN settlement_distributions sd ON (	sd.cid = apd.cid
					  							AND sd.id = apd.settlement_distribution_id
					  							AND apd.ar_payment_id = ap.id
					  							AND apd.cid = ap.cid
							  					AND apd.payment_distribution_type_id = 1
							  					AND sd.distribution_status_type_id NOT IN (' . implode( ',', $arrintDistributionStatusTypeIds ) . '))
							 			)
							)'
						. ( ( false == is_null( $arrstrAndSearchParameters ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );

		$arrintResponse = fetchData( $strSql, $objPaymentDatabase );
		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];
		return 0;
	}

	public static function fetchSearchCriteria( $objArPaymentsFilter, $boolIsSkipDeclinedStatusType = false ) {

		if( true == $boolIsSkipDeclinedStatusType ) {

			$arrintPaymentStatusTypeIds = [ CPaymentStatusType::CAPTURED . ',' . CPaymentStatusType::AUTHORIZED . ',' . CPaymentStatusType::PHONE_CAPTURE_PENDING . ',' . CPaymentStatusType::RECEIVED ];
		}

		$arrstrAndSearchParameters = [];

		// Create SQL parameters.
		if( false == is_null( $objArPaymentsFilter->getArPaymentId() ) && true == is_numeric( $objArPaymentsFilter->getArPaymentId() ) ) {
			 			$arrstrAndSearchParameters[] = ' ap.id::integer = ' . $objArPaymentsFilter->getArPaymentId();
		} else {

			if( false == is_null( $objArPaymentsFilter->getPropertyIds() ) ) 				$arrstrAndSearchParameters[] = ' ap.property_id::integer IN (' . $objArPaymentsFilter->getPropertyIds() . ')';
			if( false == is_null( $objArPaymentsFilter->getArPaymentTypeIds() ) ) 			$arrstrAndSearchParameters[] = ' ap.payment_type_id IN (' . $objArPaymentsFilter->getArPaymentTypeIds() . ')';
			if( false == is_null( $objArPaymentsFilter->getBilltoNameFirst() ) ) 			$arrstrAndSearchParameters[] = ' ap.billto_name_first ILIKE E\'%' . addslashes( $objArPaymentsFilter->sqlBilltoNameFirst() ) . '%\'';
			if( false == is_null( $objArPaymentsFilter->getBilltoNameLast() ) ) 			$arrstrAndSearchParameters[] = ' ap.billto_name_last ILIKE E\'%' . addslashes( $objArPaymentsFilter->sqlBilltoNameLast() ) . '%\'';
			if( false == is_null( $objArPaymentsFilter->getPaymentMaxAmount() ) ) 			$arrstrAndSearchParameters[] = ' ap.payment_amount <= ' . ( float ) $objArPaymentsFilter->getPaymentMaxAmount();
			if( false == is_null( $objArPaymentsFilter->getPaymentMinAmount() ) ) 			$arrstrAndSearchParameters[] = ' ap.payment_amount >= ' . ( float ) $objArPaymentsFilter->getPaymentMinAmount();
			if( false == is_null( $objArPaymentsFilter->getStartDate() ) ) 					$arrstrAndSearchParameters[] = ' ap.payment_datetime >= \'' . date( 'Y-m-d', strtotime( $objArPaymentsFilter->getStartDate() ) ) . ' 00:00:00\'';
			if( false == is_null( $objArPaymentsFilter->getEndDate() ) ) 					$arrstrAndSearchParameters[] = ' ap.payment_datetime <= \'' . date( 'Y-m-d', strtotime( $objArPaymentsFilter->getEndDate() ) ) . ' 23:59:59\'';
			if( false == is_null( $objArPaymentsFilter->getIsShowNonIntegratedPayments() ) ) $arrstrAndSearchParameters[] = ' ap.remote_primary_key IS NULL ';
			if( false == is_null( $objArPaymentsFilter->getIsRecurringPayment() ) ) 			$arrstrAndSearchParameters[] = ' ap.scheduled_payment_id IS NOT NULL ';
			if( false == is_null( $objArPaymentsFilter->getScheduledPaymentId() ) )			$arrstrAndSearchParameters[] = ' ap.scheduled_payment_id = ' . ( int ) $objArPaymentsFilter->getScheduledPaymentId();

			if( false == $boolIsSkipDeclinedStatusType && false == is_null( $objArPaymentsFilter->getArPaymentStatusTypeIds() ) ) {
				$arrstrAndSearchParameters[] = ' ap.payment_status_type_id in (' . $objArPaymentsFilter->getArPaymentStatusTypeIds() . ')';
			} elseif( true == $boolIsSkipDeclinedStatusType && false == is_null( $arrintPaymentStatusTypeIds ) ) {
				$arrstrAndSearchParameters[] = ' ap.payment_status_type_id in (' . implode( ', ', $arrintPaymentStatusTypeIds ) . ')';
			}

			if( 1 == ( int ) $objArPaymentsFilter->getHideApplicationPayments() && 0 == ( int ) $objArPaymentsFilter->getIsApplicationPayment() && 0 < $objArPaymentsFilter->getCid() ) {
				$arrstrAndSearchParameters[] = ' ap.id NOT IN ( SELECT app.ar_payment_id FROM application_payments app WHERE app.cid = ' . ( int ) $objArPaymentsFilter->getCid() . ' )  ';
			}

			if( 0 == ( int ) $objArPaymentsFilter->getHideApplicationPayments() && 1 == ( int ) $objArPaymentsFilter->getIsApplicationPayment() && 0 < $objArPaymentsFilter->getCid() ) {
				$arrstrAndSearchParameters[] = ' ap.id IN ( SELECT app.ar_payment_id FROM application_payments app WHERE app.cid = ' . ( int ) $objArPaymentsFilter->getCid() . ' )  ';
			}

			if( 1 == ( int ) $objArPaymentsFilter->getIsAttachedToResident() && 0 == ( $objArPaymentsFilter->getIsFloating() ) ) $arrstrAndSearchParameters[] = ' ap.lease_id IS NOT NULL';
			if( 0 == ( int ) $objArPaymentsFilter->getIsAttachedToResident() && 1 == ( $objArPaymentsFilter->getIsFloating() ) ) $arrstrAndSearchParameters[] = ' ap.lease_id IS NULL';
		}

	   	if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) {
			return $arrstrAndSearchParameters;
		} else {
			return NULL;
		}
	}

	public static function fetchNewPaymentsDatabaseSearchCriteria( $objArPaymentsFilter ) {

		$arrstrAndSearchParameters = [];

		$objArPaymentsFilter->setCheckAuxillaryOnUs( $objArPaymentsFilter->getCheckAccountNumber() );

		if( false == is_null( $objArPaymentsFilter->getArPaymentId() ) && true == is_numeric( $objArPaymentsFilter->getArPaymentId() ) ) {
			$arrstrAndSearchParameters[] = ' ap.id::integer = ' . $objArPaymentsFilter->getArPaymentId();
		} else {
			$arrstrPaymentFilter = $objArPaymentsFilter->getPaymentFilterIds();
			$arrstrPaymentFilter = explode( ',', $arrstrPaymentFilter );

			if( true == is_null( $objArPaymentsFilter->getArTransactionId() ) || false == is_numeric( $objArPaymentsFilter->getArTransactionId() ) ) {
				if( true == arrayValuesExistsInArray( [ CArPaymentsFilter::PAYMENT_FILTER_TYPE_CHECK21_TYPE_CHECK ], $arrstrPaymentFilter ) ) $arrstrAndSearchParameters[] = ' apb.check_is_money_order = 0 ';
				if( true == arrayValuesExistsInArray( [ CArPaymentsFilter::PAYMENT_FILTER_TYPE_CHECK21_TYPE_MONEY_ORDER ], $arrstrPaymentFilter ) ) $arrstrAndSearchParameters[] = ' apb.check_is_money_order = 1 ';
				if( false == is_null( $objArPaymentsFilter->getCheckAuxillaryOnUs() ) && '' != trim( $objArPaymentsFilter->getCheckAuxillaryOnUs() ) ) $arrstrAndSearchParameters[] = ' ( apb.check_auxillary_on_us = \'' . $objArPaymentsFilter->getCheckAuxillaryOnUs() . '\' OR ap.check_number = \'' . $objArPaymentsFilter->getCheckAccountNumber() . '\' )';
				if( false == is_null( $objArPaymentsFilter->getCheckRoutingNumber() ) && '' != trim( $objArPaymentsFilter->getCheckRoutingNumber() ) ) $arrstrAndSearchParameters[] = ' apb.check_routing_number =  \'' . addslashes( $objArPaymentsFilter->getCheckRoutingNumber() ) . '\'';
				if( false == is_null( $objArPaymentsFilter->getCheckAccountNumberLastfourEncrypted() ) && '' != trim( $objArPaymentsFilter->getCheckAccountNumberLastfourEncrypted() ) ) $arrstrAndSearchParameters[] = ' apb.check_account_number_lastfour_encrypted = \'' . addslashes( $objArPaymentsFilter->getCheckAccountNumberLastfourEncrypted() ) . '\'';
			}
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) {
			return $arrstrAndSearchParameters;
		} else {
			return NULL;
		}
	}

	public static function fetchNewEntrataDatabaseSearchCriteria( $objArPaymentsFilter ) {

		$arrstrAndSearchParameters = [];

		// Create SQL parameters.
		if( false == is_null( $objArPaymentsFilter->getArPaymentId() ) && true == is_numeric( $objArPaymentsFilter->getArPaymentId() ) ) {
			$arrstrAndSearchParameters[] = ' ap.id::integer = ' . $objArPaymentsFilter->getArPaymentId();
		} elseif( false == is_null( $objArPaymentsFilter->getArTransactionId() ) && true == is_numeric( $objArPaymentsFilter->getArTransactionId() ) ) {
			$arrstrAndSearchParameters[] = ' at.id::integer = ' . $objArPaymentsFilter->getArTransactionId();

		} else {
			if( false == is_null( $objArPaymentsFilter->getArLeaseStatusTypeIds() ) && strtotime( '06/06/2013' ) <= strtotime( $objArPaymentsFilter->getStartDate() ) ) {
				$arrstrAndSearchParameters[] = ' ap.lease_status_type_id::integer IN (' . $objArPaymentsFilter->getArLeaseStatusTypeIds() . ')';
			}

			if( false == is_null( $objArPaymentsFilter->getPropertyIds() ) ) 				$arrstrAndSearchParameters[] = ' ap.property_id::integer IN (' . $objArPaymentsFilter->getPropertyIds() . ')';
			if( false == is_null( $objArPaymentsFilter->getArPaymentTypeIds() ) ) 			$arrstrAndSearchParameters[] = ' ap.payment_type_id IN (' . $objArPaymentsFilter->getArPaymentTypeIds() . ' ) ';
			if( false == is_null( $objArPaymentsFilter->getBilltoNameFirst() ) ) 			$arrstrAndSearchParameters[] = ' ap.billto_name_first ILIKE E\'%' . addslashes( $objArPaymentsFilter->sqlBilltoNameFirst() ) . '%\'';
			if( false == is_null( $objArPaymentsFilter->getBilltoNameLast() ) ) 			$arrstrAndSearchParameters[] = ' ap.billto_name_last ILIKE E\'%' . addslashes( $objArPaymentsFilter->sqlBilltoNameLast() ) . '%\'';
			if( false == is_null( $objArPaymentsFilter->getPaymentMaxAmount() ) ) 			$arrstrAndSearchParameters[] = ' ap.payment_amount <= ' . ( float ) $objArPaymentsFilter->getPaymentMaxAmount();
			if( false == is_null( $objArPaymentsFilter->getPaymentMinAmount() ) ) 			$arrstrAndSearchParameters[] = ' ap.payment_amount >= ' . ( float ) $objArPaymentsFilter->getPaymentMinAmount();
			if( false == is_null( $objArPaymentsFilter->getStartDate() ) ) 					$arrstrAndSearchParameters[] = ' ap.payment_datetime >= \'' . date( 'Y-m-d', strtotime( $objArPaymentsFilter->getStartDate() ) ) . ' 00:00:00\'';
			if( false == is_null( $objArPaymentsFilter->getEndDate() ) ) 					$arrstrAndSearchParameters[] = ' ap.payment_datetime <= \'' . date( 'Y-m-d', strtotime( $objArPaymentsFilter->getEndDate() ) ) . ' 23:59:59\'';
			if( false == is_null( $objArPaymentsFilter->getIsShowNonIntegratedPayments() ) ) $arrstrAndSearchParameters[] = ' ap.remote_primary_key IS NULL ';
			if( false == is_null( $objArPaymentsFilter->getIsSplit() ) ) 					$arrstrAndSearchParameters[] = ' ap.is_split = 1 ';
			if( false == is_null( $objArPaymentsFilter->getScheduledPaymentId() ) )			$arrstrAndSearchParameters[] = ' ap.scheduled_payment_id = ' . ( int ) $objArPaymentsFilter->getScheduledPaymentId();

			if( false == is_null( $objArPaymentsFilter->getArPaymentStatusTypeIds() ) ) {
				$arrstrAndSearchParameters[] = ' ap.payment_status_type_id in (' . $objArPaymentsFilter->getArPaymentStatusTypeIds() . ')';
			}

			if( false == is_null( $objArPaymentsFilter->getArPaymentSourceIds() ) ) {

				$arrstrArPaymentSourceIds = explode( ',', $objArPaymentsFilter->getArPaymentSourceIds() );
				$strAllPaymentSourceIds = $objArPaymentsFilter->getArPaymentSourceIds();
				$strAllSources = '';
				if( true == arrayValuesExistsInArray( [ CPsProduct::STR_ALL_PAYMENT_SOURCE_CONSTANT ], $arrstrArPaymentSourceIds ) ) {
					array_pop( $arrstrArPaymentSourceIds );
					$strAllSources = ' or ( 1 = 1 ) ';
				}

				$strArPaymentSourceIds = implode( ',', $arrstrArPaymentSourceIds );
				$arrstrAndSearchParameters[] = ' ( ap.ps_product_id in (' . $strArPaymentSourceIds . ' ) ' . $strAllSources . ' ) ';
			}

			$arrstrPaymentFilter = $objArPaymentsFilter->getPaymentFilterIds();
			$arrstrPaymentFilter = explode( ',', $arrstrPaymentFilter );

			if( true == valArr( $arrstrPaymentFilter ) ) {
				if( true == arrayValuesExistsInArray( [ CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_FLOATING ], $arrstrPaymentFilter ) ) $arrstrAndSearchParameters[] = ' ap.lease_id IS NOT NULL';
				if( true == arrayValuesExistsInArray( [ CArPaymentsFilter::PAYMENT_FILTER_TYPE_FLOATING ], $arrstrPaymentFilter ) ) $arrstrAndSearchParameters[] = ' ap.lease_id IS NULL';
				if( true == arrayValuesExistsInArray( [ CArPaymentsFilter::PAYMENT_FILTER_TYPE_RECURRING ], $arrstrPaymentFilter ) ) $arrstrAndSearchParameters[] = ' ap.scheduled_payment_id IS NOT NULL ';
				if( true == arrayValuesExistsInArray( [ CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_RECURRING ], $arrstrPaymentFilter ) ) $arrstrAndSearchParameters[] = ' ap.scheduled_payment_id IS NULL ';

				if( true == arrayValuesExistsInArray( [ CArPaymentsFilter::PAYMENT_FILTER_TYPE_INTEGRATED ], $arrstrPaymentFilter ) ) $arrstrAndSearchParameters[] = ' ap.remote_primary_key IS NOT NULL ';
				//  When one select Filter as Non Integrated, it should not return payments with Declined status.
				if( true == arrayValuesExistsInArray( [ CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_INTEGRATED ], $arrstrPaymentFilter ) ) {
					$arrstrAndSearchParameters[] = ' ap.remote_primary_key IS NULL ';
					$arrstrAndSearchParameters[] = ' ap.payment_status_type_id != ' . CPaymentStatusType::DECLINED;
				}
			}
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) {
			return $arrstrAndSearchParameters;
		} else {
			return NULL;
		}
	}

	public static function fetchNewSearchCriteria( $objArPaymentsFilter, $boolIsSkipDeclinedStatusType = false ) {

		if( true == $boolIsSkipDeclinedStatusType ) {

			$arrintPaymentStatusTypeIds = [ CPaymentStatusType::CAPTURED . ',' . CPaymentStatusType::AUTHORIZED . ',' . CPaymentStatusType::PHONE_CAPTURE_PENDING . ',' . CPaymentStatusType::RECEIVED ];
		}

		$arrstrAndSearchParameters = [];

		// Create SQL parameters.
		if( false == is_null( $objArPaymentsFilter->getArPaymentId() ) && true == is_numeric( $objArPaymentsFilter->getArPaymentId() ) ) {
			$arrstrAndSearchParameters[] = ' ap.id::integer = ' . $objArPaymentsFilter->getArPaymentId();
		} elseif( false == is_null( $objArPaymentsFilter->getArTransactionId() ) && true == is_numeric( $objArPaymentsFilter->getArTransactionId() ) ) {
			$arrstrAndSearchParameters[] = ' at.id::integer = ' . $objArPaymentsFilter->getArTransactionId();

		} else {
			if( false == is_null( $objArPaymentsFilter->getArLeaseStatusTypeIds() ) && strtotime( '06/06/2013' ) <= strtotime( $objArPaymentsFilter->getStartDate() ) ) {
				$arrstrAndSearchParameters[] = ' ap.lease_status_type_id::integer IN (' . $objArPaymentsFilter->getArLeaseStatusTypeIds() . ')';
			}

			if( false == is_null( $objArPaymentsFilter->getPropertyIds() ) ) 				$arrstrAndSearchParameters[] = ' ap.property_id::integer IN (' . $objArPaymentsFilter->getPropertyIds() . ')';
			if( false == is_null( $objArPaymentsFilter->getArPaymentTypeIds() ) ) 			$arrstrAndSearchParameters[] = ' ap.payment_type_id IN (' . $objArPaymentsFilter->getArPaymentTypeIds() . ' ) ';
			if( false == is_null( $objArPaymentsFilter->getBilltoNameFirst() ) ) 			$arrstrAndSearchParameters[] = ' ap.billto_name_first ILIKE E\'%' . addslashes( $objArPaymentsFilter->sqlBilltoNameFirst() ) . '%\'';
			if( false == is_null( $objArPaymentsFilter->getBilltoNameLast() ) ) 			$arrstrAndSearchParameters[] = ' ap.billto_name_last ILIKE E\'%' . addslashes( $objArPaymentsFilter->sqlBilltoNameLast() ) . '%\'';
			if( false == is_null( $objArPaymentsFilter->getPaymentMaxAmount() ) ) 			$arrstrAndSearchParameters[] = ' ap.payment_amount <= ' . ( float ) $objArPaymentsFilter->getPaymentMaxAmount();
			if( false == is_null( $objArPaymentsFilter->getPaymentMinAmount() ) ) 			$arrstrAndSearchParameters[] = ' ap.payment_amount >= ' . ( float ) $objArPaymentsFilter->getPaymentMinAmount();
			if( false == is_null( $objArPaymentsFilter->getStartDate() ) ) 					$arrstrAndSearchParameters[] = ' ap.payment_datetime >= \'' . date( 'Y-m-d', strtotime( $objArPaymentsFilter->getStartDate() ) ) . ' 00:00:00\'';
			if( false == is_null( $objArPaymentsFilter->getEndDate() ) ) 					$arrstrAndSearchParameters[] = ' ap.payment_datetime <= \'' . date( 'Y-m-d', strtotime( $objArPaymentsFilter->getEndDate() ) ) . ' 23:59:59\'';
			if( false == is_null( $objArPaymentsFilter->getIsShowNonIntegratedPayments() ) ) $arrstrAndSearchParameters[] = ' ap.remote_primary_key IS NULL ';
			if( false == is_null( $objArPaymentsFilter->getIsSplit() ) ) 					$arrstrAndSearchParameters[] = ' ap.is_split = 1 ';
			if( false == is_null( $objArPaymentsFilter->getScheduledPaymentId() ) )			$arrstrAndSearchParameters[] = ' ap.scheduled_payment_id = ' . ( int ) $objArPaymentsFilter->getScheduledPaymentId();
			if( false == is_null( $objArPaymentsFilter->getCheckAccountNumber() ) )			$arrstrAndSearchParameters[] = ' ap.check_number = \'' . $objArPaymentsFilter->getCheckAccountNumber() . '\'';

			if( false == $boolIsSkipDeclinedStatusType && false == is_null( $objArPaymentsFilter->getArPaymentStatusTypeIds() ) ) {
				$arrstrAndSearchParameters[] = ' ap.payment_status_type_id in (' . $objArPaymentsFilter->getArPaymentStatusTypeIds() . ')';
			} elseif( true == $boolIsSkipDeclinedStatusType && false == is_null( $arrintPaymentStatusTypeIds ) ) {
				$arrstrAndSearchParameters[] = ' ap.payment_status_type_id in (' . implode( ', ', $arrintPaymentStatusTypeIds ) . ')';
			}

			$arrstrPaymentFilter = $objArPaymentsFilter->getPaymentFilterIds();
			$arrstrPaymentFilter = explode( ',', $arrstrPaymentFilter );

			if( true == valArr( $arrstrPaymentFilter ) ) {

				if( true == arrayValuesExistsInArray( [ CArPaymentsFilter::PAYMENT_FILTER_TYPE_APPLICATION ], $arrstrPaymentFilter ) && 0 < $objArPaymentsFilter->getCid() ) {
					$arrstrAndSearchParameters[] = ' ap.id IN ( SELECT app.ar_payment_id FROM application_payments app WHERE app.cid = ' . ( int ) $objArPaymentsFilter->getCid() . ' )  ';
				}

				if( true == arrayValuesExistsInArray( [ CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_APPLICATION ], $arrstrPaymentFilter ) && 0 < $objArPaymentsFilter->getCid() ) {
					$arrstrAndSearchParameters[] = ' ap.id NOT IN ( SELECT app.ar_payment_id FROM application_payments app WHERE app.cid = ' . ( int ) $objArPaymentsFilter->getCid() . ' )  ';
				}
				if( true == arrayValuesExistsInArray( [ CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_FLOATING ], $arrstrPaymentFilter ) ) $arrstrAndSearchParameters[] = ' ap.lease_id IS NOT NULL';
				if( true == arrayValuesExistsInArray( [ CArPaymentsFilter::PAYMENT_FILTER_TYPE_FLOATING ], $arrstrPaymentFilter ) ) $arrstrAndSearchParameters[] = ' ap.lease_id IS NULL';

				if( true == arrayValuesExistsInArray( [ CArPaymentsFilter::PAYMENT_FILTER_TYPE_RECURRING ], $arrstrPaymentFilter ) ) $arrstrAndSearchParameters[] = ' ap.scheduled_payment_id IS NOT NULL ';
				if( true == arrayValuesExistsInArray( [ CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_RECURRING ], $arrstrPaymentFilter ) ) $arrstrAndSearchParameters[] = ' ap.scheduled_payment_id IS NULL ';

				if( true == arrayValuesExistsInArray( [ CArPaymentsFilter::PAYMENT_FILTER_TYPE_INTEGRATED ], $arrstrPaymentFilter ) ) $arrstrAndSearchParameters[] = ' ap.remote_primary_key IS NOT NULL ';
				//  When one select Filter as Non Integrated, it should not return payments with Declined status.
				if( true == arrayValuesExistsInArray( [ CArPaymentsFilter::PAYMENT_FILTER_TYPE_NON_INTEGRATED ], $arrstrPaymentFilter ) ) {
					$arrstrAndSearchParameters[] = ' ap.remote_primary_key IS NULL ';
					$arrstrAndSearchParameters[] = ' ap.payment_status_type_id != ' . CPaymentStatusType::DECLINED;
				}
			}
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) {
			return $arrstrAndSearchParameters;
		} else {
			return NULL;
		}
	}

	public static function fetchArPaymentsDailyTotals( $intCid, $objClientDatabase, $objArPaymentsFilter = NULL, $arrintPropertyIds = NULL ) {

		if( true == valObj( $objArPaymentsFilter, 'CArPaymentsFilter' ) ) {
			$arrstrAndSearchParameters = self::fetchSearchCriteria( $objArPaymentsFilter, true );
		}

	   	if( 0 == \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) {
			return NULL;
		}

		$strSql = ' SELECT
							TO_CHAR(payment_datetime, \'YYYY-MM-DD\') AS daily,
							payment_type_id,
							SUM(payment_amount) AS total
					FROM
							ar_payments AS ap
					WHERE ap.cid = ' . ( int ) $intCid . '
							' . ( ( false == is_null( $arrstrAndSearchParameters ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );

		if( 0 == \Psi\Libraries\UtilFunctions\count( $objArPaymentsFilter->getPropertyIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) {
			$strSql .= ' AND ap.property_id::integer in ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}

		$strSql .= ' GROUP BY daily, payment_type_id ORDER BY daily, payment_type_id';

		$arrstrData = fetchData( $strSql, $objClientDatabase );

		return $arrstrData;
	}

	public static function fetchUndistributedArPaymentsCountByCid( $intCid, $objPaymentDatabase ) {

		$arrintPaymentStatusTypeIds 	 = [ CPaymentStatusType::AUTHORIZED, CPaymentStatusType::CAPTURED, CPaymentStatusType::PHONE_CAPTURE_PENDING ];
		$arrintDistributionStatusTypeIds = [ CDistributionStatusType::DEBIT_FAILED, CDistributionStatusType::DEBIT_RETURN, CDistributionStatusType::CREDIT_FAILED, CDistributionStatusType::CREDIT_RETURN ];

		$strSql = 'SELECT
						count(ap.id)
					FROM
						ar_payments AS ap,
						company_merchant_accounts cma,
						merchant_account_methods mam
					WHERE
						ap.company_merchant_account_id = cma.id
						AND ap.cid = cma.cid
						AND mam.company_merchant_account_id = cma.id
						AND mam.cid = cma.cid
						AND mam.payment_type_id = ' . CPaymentType::ACH . '
						AND mam.payment_medium_id = ' . CPaymentMedium::WEB . '
						AND mam.is_controlled_distribution = 1
						AND ap.cid = ' . ( int ) $intCid . '
					  	AND ap.payment_status_type_id IN ( ' . implode( ',', $arrintPaymentStatusTypeIds ) . ' )
					  	AND ap.distribution_blocked_on IS NULL
						AND ( NOT EXISTS (
							  SELECT apd2.id
							  FROM ar_payment_distributions apd2
							  INNER JOIN settlement_distributions sd2 ON (apd2.settlement_distribution_id = sd2.id AND apd2.cid = sd2.cid)
							  WHERE apd2.ar_payment_id = ap.id
					  		  AND apd2.cid = ap.cid
							  AND apd2.payment_distribution_type_id = ' . CPaymentDistributionType::SETTLEMENT . '
							  AND sd2.distribution_status_type_id NOT IN (' . implode( ',', $arrintDistributionStatusTypeIds ) . ')
							)
						)';

		$arrintResponse = fetchData( $strSql, $objPaymentDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchCustomArPaymentByIdByCid( $intArPaymentId, $intCid, $objDatabase ) {

		$strArPaymentsTable = ( CDatabaseType::CLIENT == $objDatabase->getDatabaseTypeId() ) ? 'ar_payments' : 'view_ar_payments';
		$strSql = 'SELECT * FROM ' . $strArPaymentsTable . ' WHERE cid = ' . ( int ) $intCid . ' AND id = ' . ( int ) $intArPaymentId;
		return self::fetchArPayment( $strSql, $objDatabase );
	}

	public static function fetchPaymentStatusTypeIdByIdByCid( $intArPaymentId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						payment_status_type_id
					FROM
						ar_payments
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intArPaymentId;

		return self::fetchColumn( $strSql, 'payment_status_type_id', $objDatabase );
	}

	public static function fetchArPaymentReversedByArPaymentIdByCid( $intArPaymentId, $intCid, $objDatabase ) {

		$strArPaymentsTable = ( CDatabaseType::CLIENT == $objDatabase->getDatabaseTypeId() ) ? 'ar_payments' : 'view_ar_payments';
		$strSql = 'SELECT * FROM ' . $strArPaymentsTable . ' WHERE payment_status_type_id IN ( ' . CPaymentStatusType::REVERSED . ', ' . CPaymentStatusType::TRANSFERRED . ') AND ar_payment_id = ' . ( int ) $intArPaymentId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchArPayment( $strSql, $objDatabase );
	}

	public static function fetchCustomArPaymentByIdByPaymentTypeId( $intArPaymentId, $intPaymentTypeId, $objDatabase ) {

		$strArPaymentTable = ( CDatabaseType::CLIENT == $objDatabase->getDatabaseTypeId() ) ? 'ar_payments' : 'view_ar_payments';
		$strSql = 'SELECT * FROM ' . $strArPaymentTable . ' WHERE id = ' . ( int ) $intArPaymentId . ' AND payment_type_id = \'' . ( int ) $intPaymentTypeId . '\' AND cid = cid';
		return self::fetchArPayment( $strSql, $objDatabase );
	}

	public static function fetchCustomArPaymentByIdByPaymentTypeIds( $intArPaymentId, $arrintPaymentTypeIds, $objDatabase ) {

		$strArPaymentTable = ( CDatabaseType::CLIENT == $objDatabase->getDatabaseTypeId() ) ? 'ar_payments' : 'view_ar_payments';
		$strSql = 'SELECT * FROM ' . $strArPaymentTable . ' WHERE id = ' . ( int ) $intArPaymentId . ' AND payment_type_id IN ( ' . implode( ',', $arrintPaymentTypeIds ) . ' ) AND cid = cid';
		return self::fetchArPayment( $strSql, $objDatabase );
	}

	/**
	 * SCRIPT TO RETRIEVE UNBATCHED CUSTOMER
	 * PAYMENTS FOR BATCH PROCESSING
	 */

	public static function fetchCreditCardAuthorizedPaymentsThatNeedToBatchCapture( $intMerchantGatewayId, $strCutoffDate, $objPaymentDatabase ) {
		$strCutoffDate = date( 'm-d-Y H:i:s', strtotime( $strCutoffDate ) );
		$arrintApprovedPaymentTypes = [ CPaymentType::VISA, CPaymentType::MASTERCARD, CPaymentType::AMEX, CPaymentType::DISCOVER ];

		$strSql = '
				SELECT
					ap.*
				FROM
					view_ar_payments ap
				JOIN clients mc on ap.cid = mc.id
				JOIN company_merchant_accounts cma ON ( cma.id = ap.company_merchant_account_id )
				JOIN merchant_account_methods mam ON ( mam.company_merchant_account_id = cma.id AND mam.payment_medium_id = ap.payment_medium_id AND mam.payment_type_id = ap.payment_type_id )
				WHERE
					ap.merchant_gateway_id = ' . ( int ) $intMerchantGatewayId . '
					AND ap.payment_status_type_id = ' . ( int ) CPaymentStatusType::CAPTURED . '
					AND ap.payment_type_id IN (' . implode( ',', $arrintApprovedPaymentTypes ) . ')
					AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
					AND mam.is_controlled_distribution = 1
					AND cma.is_disabled = 0
					AND ap.payment_amount > 0
					AND ap.captured_on < \'' . $strCutoffDate . '\'
					AND ap.batched_on IS NULL
				ORDER BY
					ap.cid, ap.id ';

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchCreditCardReversedPaymentsThatNeedToBatchCapture( $intMerchantGatewayId, $strCutoffDate, $objPaymentDatabase ) {
		$strCutoffDate = date( 'm-d-Y H:i:s', strtotime( $strCutoffDate ) );
		$arrintApprovedPaymentTypes = [ CPaymentType::VISA, CPaymentType::MASTERCARD, CPaymentType::AMEX, CPaymentType::DISCOVER ];

		$strSql = '
				SELECT
					ap.*
				FROM
					view_ar_payments ap
				JOIN clients mc on ap.cid = mc.id
				JOIN company_merchant_accounts cma ON ( cma.id = ap.company_merchant_account_id )
				JOIN merchant_account_methods mam ON ( mam.company_merchant_account_id = cma.id AND mam.payment_medium_id = ap.payment_medium_id AND mam.payment_type_id = ap.payment_type_id )
				LEFT JOIN ar_payment_transactions apt ON ( ap.id = apt.ar_payment_id AND apt.payment_transaction_type_id IN ( ' . CPaymentTransactionType::CREDIT . ', ' . CPaymentTransactionType::RECALL . ' ) )
				WHERE
					ap.merchant_gateway_id = ' . ( int ) $intMerchantGatewayId . '
					AND ap.payment_status_type_id = ' . ( int ) CPaymentStatusType::REVERSED . '
					AND ap.payment_type_id IN (' . implode( ',', $arrintApprovedPaymentTypes ) . ')
					AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
					AND DATE_TRUNC ( \'day\', ap.auto_capture_on ) <= DATE_TRUNC( \'day\', NOW() )
					AND mam.is_controlled_distribution = 1
					AND cma.is_disabled = 0
					AND ap.payment_amount < 0
					--AND ap.captured_on < \'' . $strCutoffDate . '\'
					AND ap.batched_on IS NULL
					AND ap.ar_payment_id IS NOT NULL
					AND apt.id IS NOT NULL
				ORDER BY
					ap.cid, ap.id ';

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchUnBatchedPositiveAchArPaymentsByPaymentTypeIdsByProcessingBank( $objProcessingBank, $objPaymentDatabase, $strCutoffDate, $intProcessingBankAccountId, $intMerchantGatewayId, $intLimit ) {

		// Calculate the cutoff date (scrub parameter and verify string format)
		$strCutoffDate = date( 'm/d/Y H:i:s', strtotime( $strCutoffDate ) );

		$strDateFilter = ' <  \'' . $strCutoffDate . '\' ';

		$strSql = 'WITH cma AS (
							SELECT id FROM company_merchant_accounts WHERE is_disabled = 0 and processing_bank_id = ' . $objProcessingBank->getId() . '
						)
					SELECT
						DISTINCT ON ( ap.company_name, ap.id )
						' . ( int ) $intProcessingBankAccountId . ' as processing_bank_account_id_alt,
						' . ( int ) $intMerchantGatewayId . ' as merchant_gateway_id_alt,
						ap.*
					FROM
						( SELECT vap.*, mc.company_name FROM
							view_ar_payments vap,
							clients mc
						  WHERE
							vap.cid = mc.id
							AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
							AND vap.payment_amount > 0 
							AND ( 
							      vap.payment_type_id = ' . CPaymentType::ACH . ' 
							      OR ( vap.payment_type_id = ' . CPaymentType::CHECK_21 . ' AND vap.check_is_converted = 1 )
							     )
							AND vap.payment_status_type_id = ' . ( int ) CPaymentStatusType::CAPTURED . '
							AND vap.batched_on IS NULL
							AND vap.captured_on ' . $strDateFilter . ' ) AS ap
						JOIN cma ON ( cma.id = ap.company_merchant_account_id )
						LEFT OUTER JOIN ( SELECT
								nacha_entry_detail_records.*
							FROM
								nacha_entry_detail_records
								INNER JOIN nacha_files ON ( nacha_entry_detail_records.nacha_file_id = nacha_files.id )
							WHERE
								nacha_files.nacha_file_type_id = ' . CNachaFileType::AR_PAYMENTS . ' ) AS nedr ON (nedr.ar_payment_id = ap.id ) 
					WHERE 
					   1=1 
					   AND nedr.id IS NULL 
			        LIMIT ' . ( int ) $intLimit;

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchUnBatchedPositiveArPaymentsByPaymentTypeIdsByProcessingBank( $arrintPaymentTypeIds, $objProcessingBank, $objPaymentDatabase, $strCutoffDate, $intProcessingBankAccountId = NULL, $intLimit = NULL, $intModBy = NULL, $intCurrentCompare = NULL, $boolRentWeekQueryFilter = false ) {

		if( false == valArr( $arrintPaymentTypeIds ) ) return NULL;

		// Calculate the cutoff date (scrub parameter and verify string format)
		$strCutoffDate = date( 'm/d/Y H:i:s', strtotime( $strCutoffDate ) );

		// If we are querying ACH, we also need to grab check21 payments that have "check_is_converted = 1"

		$intPaymentTypeCategoryCount = 0;

		$strDateFilter = ' <  \'' . $strCutoffDate . '\' ';
		if( true == $boolRentWeekQueryFilter ) {
			$strDateFilter = ' BETWEEN ( NOW() - INTERVAL \'1 months\' ) AND \'' . $strCutoffDate . '\' ';
		}

		// add a limit if we are in rent week, are only batching ACH payments, and the processing bank is Zions Bank
		$strRentWeekLimit = '';
		if( true == $boolRentWeekQueryFilter && 1 == \Psi\Libraries\UtilFunctions\count( $arrintPaymentTypeIds ) && CPaymentType::ACH == $arrintPaymentTypeIds[0] && CProcessingBank::ZIONS_BANK == $objProcessingBank->getId() ) {
			$strRentWeekLimit = ' LIMIT 50000 ';
		}

		$strSql = 'WITH cma AS (
							SELECT id FROM company_merchant_accounts WHERE is_disabled = 0
						)
					SELECT
						DISTINCT ON ( ap.company_name, ap.id )
						mam.processing_bank_account_id as processing_bank_account_id_alt,
						mam.merchant_gateway_id as merchant_gateway_id_alt,
						ap.*
					FROM
						( SELECT vap.*, mc.company_name FROM
							view_ar_payments vap,
							clients mc
						  WHERE
							vap.cid = mc.id
							AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
							AND vap.payment_amount > 0 ';

		// This section is kind of confusing, but while querying ACH payments for batching, we should also get check21 payments that have the "check_is_converted = 1" flag.
		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrintPaymentTypeIds ) && CPaymentType::ACH == $arrintPaymentTypeIds[0] ) {
			$strSql .= ' AND ( vap.payment_type_id = ' . ( int ) $arrintPaymentTypeIds[0] . ' OR ( vap.payment_type_id = ' . CPaymentType::CHECK_21 . ' AND vap.check_is_converted = 1 ))';

		} elseif( 1 == \Psi\Libraries\UtilFunctions\count( $arrintPaymentTypeIds ) && CPaymentType::CHECK_21 == $arrintPaymentTypeIds[0] ) {
			$strSql .= ' AND vap.payment_type_id IN ( ' . ( int ) implode( ',', $arrintPaymentTypeIds ) . ' ) AND vap.check_is_converted <> 1';

		} elseif( false == in_array( CPaymentType::ACH, $arrintPaymentTypeIds ) && false == in_array( CPaymentType::CHECK_21, $arrintPaymentTypeIds ) ) {
			$strSql .= ' AND vap.payment_type_id IN ( ' . ( int ) implode( ',', $arrintPaymentTypeIds ) . ' ) ';

		} else {
			trigger_error( 'This condition should never be reached.', E_USER_ERROR );
			exit;
		}

		$strSql .= '
							AND vap.payment_status_type_id = ' . ( int ) CPaymentStatusType::CAPTURED . '
							AND vap.batched_on IS NULL
							AND vap.captured_on ' . $strDateFilter . $strRentWeekLimit . ' ) AS ap
						JOIN cma ON ( cma.id = ap.company_merchant_account_id )
						JOIN merchant_account_methods mam ON ( mam.company_merchant_account_id = cma.id AND mam.payment_medium_id = ap.payment_medium_id AND mam.payment_type_id = ap.payment_type_id )
						JOIN processing_bank_accounts pba ON ( mam.processing_bank_account_id = pba.id )
						JOIN processing_banks pb ON ( pba.processing_bank_id = pb.id ) ';

		if( true == in_array( CPaymentType::CHECK_21, $arrintPaymentTypeIds ) ) {
			$strSql .= ' JOIN ar_payment_images api ON ( ap.id = api.ar_payment_id AND api.payment_image_type_id = ' . ( int ) CPaymentImageType::REVERSE . ' AND api.endorsed_on IS NOT NULL ) ';
		}

		if( true == in_array( CPaymentType::ACH, $arrintPaymentTypeIds ) ) {

			$strSql .= ' LEFT OUTER JOIN ( SELECT
							  		nacha_entry_detail_records.*
							  	FROM
							  		nacha_entry_detail_records
							  		INNER JOIN nacha_files ON ( nacha_entry_detail_records.nacha_file_id = nacha_files.id )
							  	WHERE
							  		nacha_files.nacha_file_type_id = ' . CNachaFileType::AR_PAYMENTS . ' ) AS nedr ON (nedr.ar_payment_id = ap.id ) ';

		}

		if( true == in_array( CPaymentType::CHECK_21, $arrintPaymentTypeIds ) ) {
			$strSql .= ' LEFT OUTER JOIN x937_check_detail_records xcdr ON ( xcdr.ar_payment_id = ap.id ) ';
		}

		if( true == in_array( CPaymentType::VISA, $arrintPaymentTypeIds )
				|| true == in_array( CPaymentType::MASTERCARD, $arrintPaymentTypeIds )
				|| true == in_array( CPaymentType::DISCOVER, $arrintPaymentTypeIds )
				|| true == in_array( CPaymentType::AMEX, $arrintPaymentTypeIds ) ) {

			// We need to check for an eft batch with this payment in it.
			$strSql .= ' LEFT OUTER JOIN ( SELECT
										  		batched_ar_payments.*
										  	FROM
										  		batched_ar_payments
										  		INNER JOIN eft_batches ON ( batched_ar_payments.eft_batch_id = eft_batches.id )
										  	WHERE
										  		eft_batches.eft_batch_type_id = ' . CEftBatchType::EFT_BATCH_TYPE_CREDIT_CARD_REVERSALS . ' ) AS bap ON ( bap.ar_payment_id = ap.id ) ';
		}

		$strSql .= '
					WHERE
						pba.processing_bank_id = ' . ( int ) $objProcessingBank->getId();

		if( true == is_numeric( $intProcessingBankAccountId ) ) {
			$strSql .= ' AND mam.processing_bank_account_id = ' . ( int ) $intProcessingBankAccountId;
		}

		$strSql .= '	AND mam.is_controlled_distribution = 1 ';

		if( true == in_array( CPaymentType::ACH, $arrintPaymentTypeIds ) ) {
			$strSql .= ' AND nedr.id IS NULL ';
			$intPaymentTypeCategoryCount++;
		}

		if( true == in_array( CPaymentType::CHECK_21, $arrintPaymentTypeIds ) ) {
			$strSql .= ' AND xcdr.id IS NULL ';
			$intPaymentTypeCategoryCount++;
		}

		if( true == in_array( CPaymentType::VISA, $arrintPaymentTypeIds )
				|| true == in_array( CPaymentType::MASTERCARD, $arrintPaymentTypeIds )
				|| true == in_array( CPaymentType::DISCOVER, $arrintPaymentTypeIds )
				|| true == in_array( CPaymentType::AMEX, $arrintPaymentTypeIds ) ) {

			$strSql .= ' AND bap.id IS NULL ';
			$intPaymentTypeCategoryCount++;
		}

		if( CONFIG_ENVIRONMENT != 'development' ) {
	  		$strSql .= ' AND ( mam.gateway_username_encrypted <> \'' . CEncryption::encryptText( '123>4684177', CONFIG_KEY_GATEWAY_USERNAME ) . '\' OR mam.gateway_username_encrypted IS NULL ) ';
		}

		if( false == is_null( $intModBy ) && false == is_null( $intCurrentCompare ) ) {
			$strSql .= ' AND ap.company_merchant_account_id % ' . ( int ) $intModBy . ' = ' . ( int ) $intCurrentCompare;
		}

		// $strSql .= ' ORDER BY
		//				ap.company_name, ap.id ';

	  	if( true == is_numeric( $intLimit ) ) {
			$strSql .= ' LIMIT ' . ( int ) $intLimit;
	  	}

	  	if( 1 != $intPaymentTypeCategoryCount ) {
	  		trigger_error( 'You cannot query ach, cc, and check21 together.  They must be queried separately.', E_USER_ERROR );
	  		exit;
	  	}

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchUnBatchedNegativeArPaymentsByPaymentTypeIdsByProcessingBank( $arrintPaymentTypeIds, $objProcessingBank, $objPaymentDatabase ) {

		$arrintOverridingDistributionStatusTypeIds = [ CDistributionStatusType::CREDITED, CDistributionStatusType::CREDITING, CDistributionStatusType::DEBITED, CDistributionStatusType::DEBITING ];

		if( true == in_array( CPaymentType::CHECK_21, $arrintPaymentTypeIds ) ) {
			trigger_error( 'Check21 payments cannot be reversed.', E_USER_ERROR );
			exit;
		}

		// HERE WE NEED TO MAKE SURE WE DON'T BATCH OUT NEGATIVE AR PAYMENTS IF SETTLEMENT DISTRIBUTIONS HAVEN'T SUCCEEDED,
		// AND BEEN SETTLED WITHOUT RETURNS FOR 2 DAYS.

		$intPaymentTypeCategoryCount = 0;

		$strSql = 'SELECT
						DISTINCT ON ( ap.company_name, ap.id )
						ap.*
					FROM
						( SELECT
								vap.*,
								mc.company_name
							FROM
								view_ar_payments vap,
								clients mc
							WHERE
								vap.cid = mc.id
								AND vap.payment_amount < 0
								AND vap.created_on > ( NOW() - INTERVAL \'6 months\' )
								AND DATE_TRUNC ( \'day\', vap.auto_capture_on ) <= DATE_TRUNC( \'day\', NOW() )
								AND vap.payment_type_id IN ( ' . ( int ) implode( ',', $arrintPaymentTypeIds ) . ' )
								AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
								AND vap.payment_status_type_id = ' . CPaymentStatusType::REVERSED . '
								AND vap.batched_on IS NULL
								AND vap.ar_payment_id IS NOT NULL ) AS ap

					JOIN company_merchant_accounts cma ON ( cma.id = ap.company_merchant_account_id )
					JOIN merchant_account_methods mam ON ( mam.company_merchant_account_id = cma.id AND mam.payment_medium_id = ap.payment_medium_id AND mam.payment_type_id = ap.payment_type_id )
					JOIN processing_bank_accounts pba ON ( mam.processing_bank_account_id = pba.id )
					JOIN ar_payment_distributions apd ON ( apd.ar_payment_id = ap.id )
					JOIN settlement_distributions sd ON ( apd.settlement_distribution_id = sd.id ) ';

		if( true == in_array( CPaymentType::ACH, $arrintPaymentTypeIds ) ) {

			$strSql .= ' LEFT OUTER JOIN ( SELECT
										  		nacha_entry_detail_records.*
										  	FROM
										  		nacha_entry_detail_records
										  		INNER JOIN nacha_files ON ( nacha_entry_detail_records.nacha_file_id = nacha_files.id )
										  	WHERE
										  		nacha_files.nacha_file_type_id = ' . CNachaFileType::AR_PAYMENTS . ' ) AS nedr ON (nedr.ar_payment_id = ap.id ) ';

		}

		if( true == in_array( CPaymentType::VISA, $arrintPaymentTypeIds )
				|| true == in_array( CPaymentType::MASTERCARD, $arrintPaymentTypeIds )
				|| true == in_array( CPaymentType::DISCOVER, $arrintPaymentTypeIds )
				|| true == in_array( CPaymentType::AMEX, $arrintPaymentTypeIds ) ) {

			// We need to check for an eft batch with this payment in it.
			$strSql .= ' LEFT OUTER JOIN ( SELECT
										  		batched_ar_payments.*
										  	FROM
										  		batched_ar_payments
										  		INNER JOIN eft_batches ON ( batched_ar_payments.eft_batch_id = eft_batches.id )
										  	WHERE
										  		eft_batches.eft_batch_type_id = ' . CEftBatchType::EFT_BATCH_TYPE_CREDIT_CARD_REVERSALS . ' ) AS bap ON ( bap.ar_payment_id = ap.id ) ';
		}

				$strSql .= ' WHERE
								pba.processing_bank_id = ' . ( int ) $objProcessingBank->getId() . '
								AND ap.processing_bank_account_id IS NOT NULL
								AND sd.distribution_datetime <= ( NOW() - INTERVAL \'2 days\' )
								AND apd.payment_distribution_type_id = ' . CPaymentDistributionType::REVERSAL . '
								AND sd.distribution_status_type_id IN ( ' . implode( ',', $arrintOverridingDistributionStatusTypeIds ) . ' )
								AND mam.is_controlled_distribution = 1
								AND cma.is_disabled = 0 ';

		if( true == in_array( CPaymentType::ACH, $arrintPaymentTypeIds ) ) {
			$strSql .= ' AND nedr.id IS NULL ';
			$intPaymentTypeCategoryCount++;
		}

		if( true == in_array( CPaymentType::VISA, $arrintPaymentTypeIds )
				|| true == in_array( CPaymentType::MASTERCARD, $arrintPaymentTypeIds )
				|| true == in_array( CPaymentType::DISCOVER, $arrintPaymentTypeIds )
				|| true == in_array( CPaymentType::AMEX, $arrintPaymentTypeIds ) ) {

			$strSql .= ' AND bap.id IS NULL ';
			$intPaymentTypeCategoryCount++;
		}

		if( CONFIG_ENVIRONMENT != 'development' ) {
	  		$strSql .= ' AND ( mam.gateway_username_encrypted <> \'' . CEncryption::encryptText( '123>4684177', CONFIG_KEY_GATEWAY_USERNAME ) . '\' OR mam.gateway_username_encrypted IS NULL ) ';
		}

		$strSql .= ' ORDER BY
						ap.company_name, ap.id ';

	  	if( 1 != $intPaymentTypeCategoryCount ) {
	  		trigger_error( 'You cannot query ach && cc types together.  They must be queried separately.', E_USER_ERROR );
	  		exit;
	  	}

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	// This function is used to query reversals that are ready to be credited to residents.

	public static function fetchUnBatchedNegativeCreditCardArPaymentsByMerchantGatewayId( $intMerchantGatewayId, $objPaymentDatabase ) {

		$arrintOverridingDistributionStatusTypeIds = [ CDistributionStatusType::CREDITED, CDistributionStatusType::CREDITING, CDistributionStatusType::DEBITED, CDistributionStatusType::DEBITING ];
		$arrintCreditCardPaymentTypeIds = [ CPaymentType::VISA, CPaymentType::MASTERCARD, CPaymentType::DISCOVER, CPaymentType::AMEX ];

		// HERE WE NEED TO MAKE SURE WE DON'T BATCH OUT NEGATIVE AR PAYMENTS IF SETTLEMENT DISTRIBUTIONS HAVEN'T SUCCEEDED,
		// AND BEEN SETTLED WITHOUT RETURNS FOR 2 DAYS.

		$strSql = 'SELECT
						DISTINCT ON ( ap.company_name, ap.id )
						ap.*
					FROM
						( SELECT
								vap.*,
								mc.company_name
							FROM
								view_ar_payments vap,
								clients mc
							WHERE
								vap.cid = mc.id
								AND vap.payment_amount < 0
								AND DATE_TRUNC ( \'day\', vap.auto_capture_on ) <= DATE_TRUNC( \'day\', NOW() )
								AND vap.payment_type_id IN ( ' . implode( ',', $arrintCreditCardPaymentTypeIds ) . ' )
								AND mc.company_status_type_id IN ( ' . CCompanyStatusType::CLIENT . ', ' . CCompanyStatusType::TERMINATED . ' ) 
								AND vap.payment_status_type_id = ' . CPaymentStatusType::REVERSED . '
								AND vap.batched_on IS NULL
								AND vap.merchant_gateway_id = ' . ( int ) $intMerchantGatewayId . '
								AND vap.ar_payment_id IS NOT NULL ) AS ap

						JOIN company_merchant_accounts cma ON ( cma.id = ap.company_merchant_account_id )
						JOIN merchant_account_methods mam ON ( mam.company_merchant_account_id = cma.id AND mam.payment_medium_id = ap.payment_medium_id AND mam.payment_type_id = ap.payment_type_id )
						JOIN ar_payment_distributions apd ON ( apd.ar_payment_id = ap.id )
						JOIN settlement_distributions sd ON ( apd.settlement_distribution_id = sd.id )
						LEFT JOIN ar_payment_transactions apt ON ( ap.id = apt.ar_payment_id AND apt.payment_transaction_type_id IN ( ' . CPaymentTransactionType::CREDIT . ', ' . CPaymentTransactionType::RECALL . ' ) )
					WHERE
						distribution_datetime <= ( NOW() ) ' // - INTERVAL \'2 days\' )
						. '
						AND apd.payment_distribution_type_id = ' . CPaymentDistributionType::REVERSAL . '
						AND sd.distribution_status_type_id IN ( ' . implode( ',', $arrintOverridingDistributionStatusTypeIds ) . ' )
						AND mam.is_controlled_distribution = 1
						AND cma.is_disabled = 0
						AND apt.id IS NULL ';

		if( CONFIG_ENVIRONMENT != 'development' ) {
			$strSql .= ' AND mam.gateway_username_encrypted <> \'' . CEncryption::encryptText( '123>4684177', CONFIG_KEY_GATEWAY_USERNAME ) . '\' ';
		}

		$strSql .= ' ORDER BY
						ap.company_name, ap.id ';

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchPreApplicationArPaymentsByApplicantApplicationIdByCid( $intApplicantApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( true == is_null( $intApplicantApplicationId ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						arp.*
				   FROM
						ar_payments arp
						INNER JOIN application_payments ap ON ( arp.id = ap.ar_payment_id AND arp.cid = ap.cid )
						INNER JOIN applicant_applications aa ON ( ap.applicant_id = aa.applicant_id AND ap.cid = aa.cid ' . $strCheckDeletedAASql . ' )
				   WHERE
						ap.application_id = aa.application_id
						AND aa.id = ' . ( int ) $intApplicantApplicationId . '
						AND ap.cid = ' . ( int ) $intCid . '
						AND ap.application_stage_id = ' . ( int ) CApplicationStage::PRE_APPLICATION . '
						AND ap.application_status_id = ' . ( int ) CApplicationStatus::STARTED . '
						AND	arp.payment_status_type_id <> ' . ( int ) CPaymentStatusType::CANCELLED;

		return self::fetchArPayments( $strSql, $objDatabase );
	}

	public static function fetchMainApplicationArPaymentsByApplicantApplicationIdByCid( $intApplicantApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( true == is_null( $intApplicantApplicationId ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						arp.*
					FROM
						ar_payments arp
						INNER JOIN application_payments ap	ON ( arp.id = ap.ar_payment_id AND arp.cid = ap.cid )
						INNER JOIN applicant_applications aa ON ( ap.applicant_id = aa.applicant_id AND ap.cid = aa.cid ' . $strCheckDeletedAASql . ' )
					WHERE
						ap.application_id = aa.application_id
					AND ap.cid = ' . ( int ) $intCid . '
					AND ap.application_stage_id = ' . ( int ) CApplicationStage::APPLICATION . '
					AND ap.application_status_id = ' . ( int ) CApplicationStatus::COMPLETED . '
					AND	arp.payment_status_type_id <> ' . ( int ) CPaymentStatusType::CANCELLED . '
					AND aa.id = ' . ( int ) $intApplicantApplicationId;

		return self::fetchArPayments( $strSql, $objDatabase );
	}

	// Need to Repalce with fetchApplicationArPaymentsByApplicantIdByApplicationIdByApplicationStageIdByApplicationStatusTypeIdByCid

	public static function fetchApplicationArPaymentsByApplicantIdByApplicationIdByApplicationStageStatusIdByCid( $intApplicantId, $intApplicationId, $intCid, $intApplicationStageId, $intApplicationStatusId, $objDatabase ) {
		if( true == is_null( $intApplicantId ) || true == is_null( $intApplicationId ) ) return NULL;

		$strSql = 'SELECT arp.*
					FROM
						ar_payments arp
						INNER JOIN application_payments ap ON ( ap.ar_payment_id = arp.id AND ap.cid = arp.cid )
					WHERE
					  	ap.applicant_id = ' . ( int ) $intApplicantId . '
					AND ap.cid = ' . ( int ) $intCid . '
					AND ap.application_id = ' . ( int ) $intApplicationId;

		if( false == is_null( $intApplicationStageId ) && false == is_null( $intApplicationStatusId ) ) {
			$strSql .= ' AND ap.application_stage_id = ' . ( int ) $intApplicationStageId . ' AND ap.application_status_id = ' . ( int ) $intApplicationStatusId;
		}

		return self::fetchArPayments( $strSql, $objDatabase );
	}

	public static function fetchPhoneCapturePendingApplicationArPaymentsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		if( true == is_null( $intApplicationId ) ) return NULL;

		$strSql = 'SELECT arp.*
					FROM
						ar_payments arp
						INNER JOIN application_payments ap ON ( ap.ar_payment_id = arp.id AND ap.cid = arp.cid )
					WHERE
					  	ap.cid = ' . ( int ) $intCid . '
					  	AND arp.payment_type_id in ( ' . implode( ',', CPaymentType::$c_arrintCreditCardPaymentTypes ) . ' )
					  	AND arp.payment_status_type_id = ' . ( int ) CPaymentStatusType::PHONE_CAPTURE_PENDING . '
						AND ap.application_id = ' . ( int ) $intApplicationId;

		return self::fetchArPayments( $strSql, $objDatabase );
	}

	public static function fetchApplicationArPaymentByApplicantIdByApplicationIdByCidByPaymentStatusId( $intApplicantId, $intApplicationId, $intPaymentType, $intCid, $objClientDatabase, $boolIncludeDeletedAA = false ) {

		if( true == is_null( $intApplicantId ) || true == is_null( $intApplicationId ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT a.id, arp.*
					FROM
						applications a
						JOIN applicant_applications aa ON ( aa.application_id = a.id AND a.cid = aa.cid ' . $strCheckDeletedAASql . ' )
						JOIN applicants app ON ( app.id = aa.applicant_id AND app.cid = aa.cid )
						RIGHT JOIN ar_payments arp ON ( arp.lease_id = a.lease_id AND arp.customer_id = app.customer_id AND arp.cid = a.cid )
					WHERE
						a.cid = ' . ( int ) $intCid . '
					  	AND aa.application_id = ' . ( int ) $intApplicationId . '
					  	AND aa.applicant_id = ' . ( int ) $intApplicantId . '
						AND arp.payment_type_id = ' . ( int ) $intPaymentType . '
						AND arp.payment_status_type_id = ' . ( int ) CPaymentStatusType::CAPTURED . '
						AND arp.id NOT IN (
											   SELECT
												   ar_payment_id
											   FROM
												   application_payments ap
											   WHERE
												   ap.cid = ' . ( int ) $intCid . '
												   AND ap.application_id = ' . ( int ) $intApplicationId . '
												   AND ap.applicant_id = ' . ( int ) $intApplicantId . ' )
						LIMIT 1;';

		return self::fetchArPayment( $strSql, $objClientDatabase );
	}

	public static function fetchArPaymentsByApplicantApplicationIdsByCid( $arrintApplicantApplicationIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrintApplicantApplicationIds ) ) return NULL;

		$arrintPaymentStatusTypeIds 	= [ CPaymentStatusType::RECEIVED, CPaymentStatusType::AUTHORIZED, CPaymentStatusType::CAPTURED, CPaymentStatusType::PHONE_CAPTURE_PENDING ];
		$arrintApplicationStageStatusIds = [ CApplicationStage::PRE_QUALIFICATION => [ CApplicationStatus::STARTED ], CApplicationStage::APPLICATION => [ CApplicationStatus::STARTED, CApplicationStatus::COMPLETED ] ];

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						arp.*
					FROM
						ar_payments arp
						JOIN application_payments ap ON ( arp.id = ap.ar_payment_id AND arp.cid = ap.cid )
						JOIN applicant_applications aa ON ( ap.applicant_id = aa.applicant_id AND ap.cid = aa.cid ' . $strCheckDeletedAASql . ' )
					WHERE
						ap.application_id = aa.application_id
					AND aa.id IN ( ' . implode( ',', $arrintApplicantApplicationIds ) . ' )
					AND ( ap.application_stage_id, ap.application_status_id ) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )
					AND arp.cid = ' . ( int ) $intCid . '
					AND arp.payment_status_type_id IN ( ' . implode( ',', $arrintPaymentStatusTypeIds ) . ')';

		return self::fetchArPayments( $strSql, $objDatabase );
	}

	/**
	 * New Settlement Distribution Functions
	 * Functions Used To Retrieve UnDistributed
	 * Payments For Distribution
	 */

	// This function is one of the most dangerous functions in the database.
	// Be EXTREMELY careful not to make a mistake.
	// This function retrieves all the payments not yet settled

	public static function fetchUndistributedArPaymentsByProcessingBank( $objProcessingBank, $objPaymentDatabase ) {

		CArPayments::fetchUndistributedArPaymentsByProcessingBankModOnMerchantAccountId( $objProcessingBank, NULL, NULL, $objPaymentDatabase, NULL );
	}

	public static function fetchUndistributedArPaymentsByProcessingBankModOnMerchantAccountId( $objProcessingBank, $intModBy, $intModStep, $objPaymentDatabase, $strClientCondition, $boolRentWeekQueryFilter = false ) {

		// THE PAYMENTS QUERIED FOR SETTLEMENT SHOULD BE DESIGNATED BY THE PROCESSING BANK ACCOUNT ID ON THE MERCHANT ACCOUNT (NOT THE AR PAYMENT).
		// THIS MEANS THAT IF WE SWITCH FROM ONE PROCESSING BANK TO ANOTHER ON A MERCHANT ACCOUNT, THAT THE NEW BANK WILL HAVE TO SETTLE FUNDS
		// FROM THE OLD BANK TO THE CLIENTS DISTRIBUTION ACCOUNT.  IF THE MAIN BANK ON THE MERCHANT ACCOUNT IS ZIONS, BUT CREDIT CARDS ARE PROCESSED
		// THROUGH A FIRST REGIONAL INTERMEDIARY, THE ZIONS SETTLEMENT DISTRIBUTION SCRIPT WILL GRAB THE FUNDS FROM THE FRB CREDIT CARD INTERMEDIARY
		// AND SETTLE IT TO THE CLIENTS DISTRIBUTION ACCOUNT ALONG WITH THE ACH AND CHECK 21 VOLUME FOR THE DAY.  CHARLES AT FIRST REGIONAL TOLD ME
		// THIS WOULD WORK.  I HOPE HE'S RIGHT.

		$arrintOverridingDistributionStatusTypeIds 	= [ CDistributionStatusType::CREDITED, CDistributionStatusType::CREDITING, CDistributionStatusType::DEBITED, CDistributionStatusType::DEBITING ];
		$arrintApprovedPaymentTypeIds 				= [ CPaymentType::ACH, CPaymentType::VISA, CPaymentType::MASTERCARD, CPaymentType::DISCOVER, CPaymentType::AMEX, CPaymentType::CHECK_21, CPaymentType::EMONEY_ORDER, CPaymentType::PAD ];

		if( true == is_null( $strClientCondition ) ) {
			$strClientCondition = ' mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' ';
		}

		$strDateFilter = '';
		if( true == $boolRentWeekQueryFilter ) {
			$strDateFilter = 'AND vap.batched_on > ( NOW() - INTERVAL \'3 months\' ) ';
		}

		$strSql = 'SELECT
						DISTINCT ON ( ap.id ) ap.*
					FROM
					  ( SELECT vap.*, mc.company_name FROM
							view_ar_payments vap,
							clients mc
						  WHERE
							vap.cid = mc.id
							AND ' . $strClientCondition . '
							AND vap.payment_amount > 0
							AND vap.distribute_on IS NOT NULL
							AND vap.distribution_blocked_on IS NULL
							AND DATE_TRUNC ( \'day\', vap.distribute_on ) <= DATE_TRUNC( \'day\', NOW() )
							AND vap.payment_type_id IN ( ' . implode( ',', $arrintApprovedPaymentTypeIds ) . ' )
							AND vap.payment_status_type_id IN ( ' . CPaymentStatusType::CAPTURED . ',' . CPaymentStatusType::RECALL . ' )
							AND vap.batched_on IS NOT NULL
							' . $strDateFilter . ' ) AS ap
						JOIN company_merchant_accounts cma ON ( cma.id = ap.company_merchant_account_id )
						JOIN merchant_account_methods mam ON ( mam.company_merchant_account_id = cma.id AND mam.payment_medium_id = ap.payment_medium_id AND mam.payment_type_id = ap.payment_type_id )
						LEFT JOIN (
							SELECT
								DISTINCT ON ( apd.ar_payment_id )
							  	apd.ar_payment_id
						  	FROM
								ar_payment_distributions apd
								INNER JOIN settlement_distributions sd ON ( apd.settlement_distribution_id = sd.id )
						  	WHERE
								apd.payment_distribution_type_id = ' . CPaymentDistributionType::SETTLEMENT . '
								AND sd.distribution_status_type_id IN ( ' . implode( ',', $arrintOverridingDistributionStatusTypeIds ) . ' )
					  	) dist_settle ON ( ap.id = dist_settle.ar_payment_id )
					  	LEFT JOIN (
							SELECT
								DISTINCT ON ( apd.ar_payment_id )
								apd.ar_payment_id
							FROM
								ar_payment_distributions apd
								INNER JOIN settlement_distributions sd ON ( apd.settlement_distribution_id = sd.id )
							WHERE
								apd.payment_distribution_type_id = ' . CPaymentDistributionType::RECALL . '
								AND sd.distribution_status_type_id IN ( ' . implode( ',', $arrintOverridingDistributionStatusTypeIds ) . ' )
						) dist_return ON ( ap.id = dist_return.ar_payment_id )

					WHERE
						mam.is_controlled_distribution = 1
						AND cma.is_disabled = 0
						AND ap.processing_bank_account_id IS NOT NULL
						AND cma.processing_bank_id = ' . ( int ) $objProcessingBank->getId() . '
						AND cma.merchant_processing_type_id <> ' . CMerchantProcessingType::INTERNATIONAL . '
						AND 1 =
							CASE
								WHEN dist_settle.ar_payment_id IS NULL AND ap.payment_status_type_id = 10 THEN 1
								WHEN dist_settle.ar_payment_id IS NULL AND ap.payment_status_type_id = 15 AND dist_return.ar_payment_id IS NOT NULL THEN 1
								ELSE 0
							END
						AND ( cma.disable_distribution_until IS NULL OR cma.disable_distribution_until <= NOW() ) ';

		if( NULL !== $intModBy && NULL !== $intModStep ) {
			$strSql .= ' AND cma.id % ' . ( int ) $intModBy . ' = ' . ( int ) $intModStep . "\n";
		}

		if( CONFIG_ENVIRONMENT != 'development' && CONFIG_ENVIRONMENT != 'stage' ) {
	  		$strSql .= ' AND ( mam.gateway_username_encrypted <> \'' . CEncryption::encryptText( '123>4684177', CONFIG_KEY_GATEWAY_USERNAME ) . '\' OR mam.gateway_username_encrypted IS NULL ) ';
		}

		if( CONFIG_ENVIRONMENT == 'development' ) {
	  		$strSql .= ' LIMIT 10 ';
		}

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	// This function is one of the most dangerous functions in the database.
	// Be EXTREMELY careful not to make a mistake.  This regulates the transactions that get settled by our settlement scripts.

	public static function fetchUndistributedReturnedArPaymentsByProcessingBankModOnMerchantAccountId( $objProcessingBank, $intModBy, $intModStep, $objPaymentDatabase, $strClientCondition, $boolRentWeekQueryFilter = false ) {

		// THE PAYMENTS QUERIED FOR SETTLEMENT SHOULD BE DESIGNATED BY THE PROCESSING BANK ACCOUNT ID ON THE MERCHANT ACCOUNT (NOT THE AR PAYMENT).
		// THIS MEANS THAT IF WE SWITCH FROM ONE PROCESSING BANK TO ANOTHER ON A MERCHANT ACCOUNT, THAT THE NEW BANK WILL HAVE TO CREDIT MONEY
		// BACK TO THE OLD PROCESSING BANK ACCOUNT AT THE OLD BANK.  HOPEFULLY THIS WILL WORK.

		$arrintApprovedPaymentTypeIds 				= [ CPaymentType::ACH, CPaymentType::VISA, CPaymentType::MASTERCARD, CPaymentType::DISCOVER, CPaymentType::AMEX, CPaymentType::CHECK_21, CPaymentType::EMONEY_ORDER, CPaymentType::PAD ];
		$arrintApprovedPaymentStatusTypeIds 		= [ CPaymentStatusType::RECALL, CPaymentStatusType::RETURN_REPRESENTED, CPaymentStatusType::CHARGE_BACK_PENDING, CPaymentStatusType::CHARGED_BACK, CPaymentStatusType::CHARGE_BACK_FAILED ];
		$arrintOverridingDistributionStatusTypeIds 	= [ CDistributionStatusType::CREDITED, CDistributionStatusType::CREDITING, CDistributionStatusType::DEBITED, CDistributionStatusType::DEBITING ];

		if( true == is_null( $strClientCondition ) ) {
			$strClientCondition = ' mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' ';
		}

		$strDateFilter = '';
		if( true == $boolRentWeekQueryFilter ) {
			$strDateFilter = 'AND vap.batched_on > ( NOW() - INTERVAL \'6 months\' ) ';
		}

		$strSql = 'SELECT
						DISTINCT ON ( ap.id ) ap.*
					FROM
					  ( SELECT vap.*, mc.company_name FROM
							view_ar_payments vap,
							clients mc
						  WHERE
							vap.cid = mc.id
							AND ' . $strClientCondition . '
							AND vap.payment_amount > 0
							AND vap.distribute_on IS NOT NULL
							AND vap.distribute_return_on IS NOT NULL
							AND vap.distribution_blocked_on IS NULL
					  		AND DATE_TRUNC ( \'day\', vap.distribute_on ) <= DATE_TRUNC( \'day\', NOW() )
							AND DATE_TRUNC ( \'day\', vap.distribute_return_on ) <= DATE_TRUNC( \'day\', NOW() )
							AND vap.payment_type_id IN ( ' . implode( ',', $arrintApprovedPaymentTypeIds ) . ' )
							AND vap.payment_status_type_id IN ( ' . implode( ',', $arrintApprovedPaymentStatusTypeIds ) . ' )
							AND vap.batched_on IS NOT NULL
							' . $strDateFilter . ' ) AS ap
						JOIN company_merchant_accounts cma ON ( cma.id = ap.company_merchant_account_id )
						JOIN merchant_account_methods mam ON ( mam.company_merchant_account_id = cma.id AND mam.payment_medium_id = ap.payment_medium_id AND mam.payment_type_id = ap.payment_type_id )
						JOIN ar_payment_distributions apd ON ( apd.ar_payment_id = ap.id AND apd.payment_distribution_type_id = ' . CPaymentDistributionType::SETTLEMENT . ' )
						JOIN settlement_distributions sd ON ( apd.settlement_distribution_id = sd.id AND sd.distribution_status_type_id IN ( ' . implode( ',', $arrintOverridingDistributionStatusTypeIds ) . ' ))

						LEFT JOIN ( SELECT
												DISTINCT ON ( ar_payment_distributions.ar_payment_id )
												ar_payment_distributions.ar_payment_id
											FROM
										  		ar_payment_distributions
										  		JOIN settlement_distributions ON ( ar_payment_distributions.settlement_distribution_id = settlement_distributions.id AND settlement_distributions.distribution_status_type_id IN ( ' . implode( ',', $arrintOverridingDistributionStatusTypeIds ) . ' ))
										  	WHERE
										  	 	ar_payment_distributions.payment_distribution_type_id = ' . CPaymentDistributionType::RECALL . '
										) AS return_settlements ON ( ap.id = return_settlements.ar_payment_id )

					WHERE
						mam.is_controlled_distribution = 1
						AND ap.processing_bank_account_id IS NOT NULL
						AND cma.processing_bank_id = ' . ( int ) $objProcessingBank->getId() . '
						AND cma.merchant_processing_type_id <> ' . CMerchantProcessingType::INTERNATIONAL . '
						AND return_settlements.ar_payment_id IS NULL ';

		if( NULL !== $intModBy && NULL !== $intModStep ) {
			$strSql .= ' AND cma.id % ' . ( int ) $intModBy . ' = ' . ( int ) $intModStep . "\n";
		}

		if( CONFIG_ENVIRONMENT != 'development' && CONFIG_ENVIRONMENT != 'stage' ) {
	  		$strSql .= ' AND ( mam.gateway_username_encrypted <> \'' . CEncryption::encryptText( '123>4684177', CONFIG_KEY_GATEWAY_USERNAME ) . '\' OR mam.gateway_username_encrypted IS NULL ) ';
		}

		if( CONFIG_ENVIRONMENT == 'development' ) {
	  		$strSql .= ' LIMIT 10 ';
		}

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchUndistributedReversedArPaymentsByProcessingBankModOnMerchantAccountId( $objProcessingBank, $intModBy, $intModStep, $objPaymentDatabase, $strClientCondition, $boolRentWeekQueryFilter = false ) {

		$arrintApprovedPaymentTypeIds 				= [ CPaymentType::ACH, CPaymentType::VISA, CPaymentType::MASTERCARD, CPaymentType::DISCOVER, CPaymentType::AMEX, CPaymentType::EMONEY_ORDER, CPaymentType::PAD ];
		$arrintOverridingDistributionStatusTypeIds 	= [ CDistributionStatusType::CREDITED, CDistributionStatusType::CREDITING, CDistributionStatusType::DEBITED, CDistributionStatusType::DEBITING ];

		if( true == is_null( $strClientCondition ) ) {
			$strClientCondition = ' mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' ';
		}

		$strDateFilter = 'AND DATE_TRUNC( \'day\', vap.distribute_on ) <= DATE_TRUNC( \'day\', NOW() )';
		if( true == $boolRentWeekQueryFilter ) {
			$strDateFilter = 'AND DATE_TRUNC( \'day\', vap.distribute_on ) BETWEEN DATE_TRUNC( \'day\', ( NOW() - INTERVAL \'6 months\' ) ) AND DATE_TRUNC( \'day\', NOW() )';
		}

		// IN THIS FUNCTION, WHEN THE DISTRIBUTION IS CREATED, WE ASSIGN THE PROCESSING BANK ACCOUNT ID (FROM THE MERCHANT ACCOUNT METHOD) TO THE REVERSING PAYMENTS.  THIS IS CRITICAL.
		// THIS FUNCTION PULLS PAYMENTS WITHOUT BATCHED_ON FIELDS SET BECAUSE WITH REVERSALS WE PULL MONEY FROM THE CLIENT FIRST, THEN WE GIVE TO THE RESIDENT AFTER THE
		// SETTLEMENT DISTRIBUTION SUCCEEDS.

		$strSql = 'SELECT
						DISTINCT ON ( ap.id )
						(
							CASE
							WHEN
								ap.payment_type_id IN ( ' . CPaymentType::VISA . ', ' . CPaymentType::MASTERCARD . ', ' . CPaymentType::DISCOVER . ', ' . CPaymentType::AMEX . ') AND ap.processing_bank_account_id IS NOT NULL
							THEN
								ap.processing_bank_account_id
							ELSE
								mam.processing_bank_account_id
							END
						) as processing_bank_account_id_alt,
						( CASE
							WHEN
								ap.payment_type_id IN ( ' . CPaymentType::VISA . ', ' . CPaymentType::MASTERCARD . ', ' . CPaymentType::DISCOVER . ', ' . CPaymentType::AMEX . ') AND ap.merchant_gateway_id IS NOT NULL
							THEN
								ap.merchant_gateway_id
							ELSE
								mam.merchant_gateway_id
							END
						) as merchant_gateway_id_alt,
						ap.*
					FROM
					  ( SELECT vap.* FROM
							view_ar_payments vap,
							clients mc
						  WHERE
							vap.cid = mc.id
							AND ' . $strClientCondition . '
							AND vap.payment_amount < 0
							AND vap.ar_payment_id IS NOT NULL
							AND vap.distribute_on IS NOT NULL
							AND vap.distribution_blocked_on IS NULL
							' . $strDateFilter . '
							AND vap.payment_type_id IN ( ' . implode( ',', $arrintApprovedPaymentTypeIds ) . ' )
					  	 	AND vap.payment_status_type_id IN ( ' . CPaymentStatusType::REVERSAL_RETURN . ',' . CPaymentStatusType::REVERSAL_CANCELLED . ',' . CPaymentStatusType::REVERSED . ',' . CPaymentStatusType::TRANSFERRED . ',' . CPaymentStatusType::CHARGE_BACK_PENDING . ',' . CPaymentStatusType::CHARGED_BACK . ' )
						) AS ap
						JOIN company_merchant_accounts cma ON ( cma.id = ap.company_merchant_account_id )
						JOIN merchant_account_methods mam ON ( mam.company_merchant_account_id = cma.id AND mam.payment_medium_id = ap.payment_medium_id AND mam.payment_type_id = ap.payment_type_id )
						LEFT JOIN ( SELECT
												DISTINCT ON ( ar_payment_distributions.ar_payment_id )
												ar_payment_distributions.ar_payment_id
											FROM
										  		ar_payment_distributions
										  		JOIN settlement_distributions ON ( ar_payment_distributions.settlement_distribution_id = settlement_distributions.id  AND settlement_distributions.distribution_status_type_id IN ( ' . implode( ',', $arrintOverridingDistributionStatusTypeIds ) . ' ))
										  	WHERE
										  	 	ar_payment_distributions.payment_distribution_type_id = ' . CPaymentDistributionType::REVERSAL . '
										) AS sd ON ( ap.id = sd.ar_payment_id )
					WHERE
						mam.is_controlled_distribution = 1
						AND cma.is_disabled = 0
						AND cma.processing_bank_id = ' . ( int ) $objProcessingBank->getId() . '
						AND cma.merchant_processing_type_id <> ' . CMerchantProcessingType::INTERNATIONAL . '
						AND sd.ar_payment_id IS NULL
						AND ( cma.disable_distribution_until IS NULL OR cma.disable_distribution_until <= NOW() ) ';

		if( NULL !== $intModBy && NULL !== $intModStep ) {
			$strSql .= ' AND cma.id % ' . ( int ) $intModBy . ' = ' . ( int ) $intModStep . "\n";
		}

		if( CONFIG_ENVIRONMENT != 'development' && CONFIG_ENVIRONMENT != 'stage' ) {
	  		$strSql .= ' AND ( mam.gateway_username_encrypted <> \'' . CEncryption::encryptText( '123>4684177', CONFIG_KEY_GATEWAY_USERNAME ) . '\' OR mam.gateway_username_encrypted IS NULL ) ';
		}

		if( CONFIG_ENVIRONMENT == 'development' ) {
	  		$strSql .= ' LIMIT 10 ';
		}

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchUndistributedReturnedReversedArPaymentsByProcessingBankModOnMerchantAccountId( $objProcessingBank, $intModBy, $intModStep, $objPaymentDatabase, $strClientCondition, $boolRentWeekQueryFilter = false ) {

		$arrintApprovedPaymentTypeIds 				= [ CPaymentType::ACH, CPaymentType::VISA, CPaymentType::MASTERCARD, CPaymentType::DISCOVER, CPaymentType::AMEX, CPaymentType::EMONEY_ORDER, CPaymentType::PAD ];
		$arrintOverridingDistributionStatusTypeIds 	= [ CDistributionStatusType::CREDITED, CDistributionStatusType::CREDITING, CDistributionStatusType::DEBITED, CDistributionStatusType::DEBITING ];

		if( true == is_null( $strClientCondition ) ) {
			$strClientCondition = ' mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' ';
		}

		$strDateFilter = 'AND DATE_TRUNC ( \'day\', vap.distribute_on ) <= DATE_TRUNC( \'day\', NOW() )';
		if( true == $boolRentWeekQueryFilter ) {
			$strDateFilter = 'AND DATE_TRUNC( \'day\', vap.distribute_on ) BETWEEN DATE_TRUNC( \'day\', ( NOW() - INTERVAL \'6 months\' ) ) AND DATE_TRUNC( \'day\', NOW() )';
		}

		$strSql = 'SELECT
						DISTINCT ON ( ap.id ) ap.*
					FROM
					  ( SELECT vap.*, mc.company_name FROM
							view_ar_payments vap,
							clients mc
						  WHERE
							vap.cid = mc.id
							AND vap.ar_payment_id IS NOT NULL
							AND ' . $strClientCondition . '
							AND vap.payment_amount < 0
							AND vap.distribute_on IS NOT NULL
							AND vap.distribute_return_on IS NOT NULL
							AND vap.distribution_blocked_on IS NULL
					  		' . $strDateFilter . '
							AND DATE_TRUNC ( \'day\', vap.distribute_return_on ) <= DATE_TRUNC( \'day\', NOW() )
							AND vap.payment_type_id IN ( ' . implode( ',', $arrintApprovedPaymentTypeIds ) . ' )
							AND vap.payment_status_type_id IN ( ' . CPaymentStatusType::REVERSAL_RETURN . ', ' . CPaymentStatusType::REVERSAL_CANCELLED . ' )) AS ap
						JOIN company_merchant_accounts cma ON ( cma.id = ap.company_merchant_account_id )
						JOIN merchant_account_methods mam ON ( mam.company_merchant_account_id = cma.id AND mam.payment_medium_id = ap.payment_medium_id AND mam.payment_type_id = ap.payment_type_id )
						JOIN ar_payment_distributions apd ON ( apd.ar_payment_id = ap.id AND apd.payment_distribution_type_id = ' . CPaymentDistributionType::REVERSAL . ' )
						JOIN settlement_distributions sd ON ( apd.settlement_distribution_id = sd.id AND sd.distribution_status_type_id IN ( ' . implode( ',', $arrintOverridingDistributionStatusTypeIds ) . ' ))
						LEFT OUTER JOIN nacha_entry_detail_records nedr ON ( nedr.ar_payment_id = ap.id AND nedr.returned_on IS NULL )
						LEFT JOIN ( SELECT
												DISTINCT ON ( ar_payment_distributions.ar_payment_id )
												ar_payment_distributions.ar_payment_id
											FROM
										  		ar_payment_distributions
										  		JOIN settlement_distributions ON ( ar_payment_distributions.settlement_distribution_id = settlement_distributions.id AND settlement_distributions.distribution_status_type_id IN ( ' . implode( ',', $arrintOverridingDistributionStatusTypeIds ) . ' ))
										  	WHERE
										  	 	ar_payment_distributions.payment_distribution_type_id = ' . CPaymentDistributionType::REVERSAL_RETURN . '
										) AS reversal_return_settlements ON ( ap.id = reversal_return_settlements.ar_payment_id )
					WHERE
						mam.is_controlled_distribution = 1
						AND ap.processing_bank_account_id IS NOT NULL
						AND cma.processing_bank_id = ' . ( int ) $objProcessingBank->getId() . '
						AND cma.merchant_processing_type_id <> ' . CMerchantProcessingType::INTERNATIONAL . '
						AND reversal_return_settlements.ar_payment_id IS NULL
						AND nedr.id IS NULL ';

		if( NULL !== $intModBy && NULL !== $intModStep ) {
			$strSql .= ' AND cma.id % ' . ( int ) $intModBy . ' = ' . ( int ) $intModStep . "\n";
		}

		if( CONFIG_ENVIRONMENT != 'development' && CONFIG_ENVIRONMENT != 'stage' ) {
			$strSql .= ' AND ( mam.gateway_username_encrypted <> \'' . CEncryption::encryptText( '123>4684177', CONFIG_KEY_GATEWAY_USERNAME ) . '\' OR mam.gateway_username_encrypted IS NULL ) ';
		}

		if( CONFIG_ENVIRONMENT == 'development' ) {
			$strSql .= ' LIMIT 10 ';
		}

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchIntegratedArPaymentsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM ar_payments' . ' WHERE cid = ' . ( int ) $intCid . ' AND customer_id = ' . ( int ) $intCustomerId . ' AND remote_primary_key IS NOT NULL';
		return self::fetchArPayments( $strSql, $objDatabase );
	}

	public static function fetchAllStanardAuthorizedArPayments( $objPaymentDatabase, $intDatabaseId = NULL, $arrobjClients = NULL ) {

		$strSql = 'SELECT
						arp.*,
						DATE_TRUNC( \'day\', NOW())::date - DATE_TRUNC( \'day\', arp.payment_datetime)::date AS payment_date_diff
					FROM
						view_ar_payments arp
						JOIN clients c ON ( arp.cid = c.id )
					WHERE
						c.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
						AND payment_type_id IN ( ' . implode( ',', CPaymentType::$c_arrintWebAllowedPaymentTypes ) . ' )
						AND arp.payment_status_type_id IN ( ' . CPaymentStatusType::AUTHORIZED . ', ' . CPaymentStatusType::PHONE_CAPTURE_PENDING . ' )
						AND DATE_TRUNC( \'day\', arp.payment_datetime ) <= DATE_TRUNC( \'day\', ( NOW() - INTERVAL \'60 Days\' ))';

		if( true == valArr( $arrobjClients ) ) {
			$strSql .= ' AND arp.cid IN( ' . implode( ',', array_keys( $arrobjClients ) ) . ')';
		}

		if( true == valId( $intDatabaseId ) ) {
			$strSql .= ' AND c.database_id = ' . $intDatabaseId;
		}

		$strSql .= '
					ORDER BY
						arp.payment_datetime ASC';

		if( CONFIG_ENVIRONMENT == 'development' ) {
			$strSql .= ' LIMIT 10 ';
		}

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchAllPhoneAuthorizationPendingArPayments( $objPaymentDatabase, $intDatabaseId = NULL, $arrobjClients = NULL ) {

		$strSql = 'SELECT
						arp.*, DATE_TRUNC( \'day\', NOW())::date - DATE_TRUNC( \'day\', arp.payment_datetime)::date AS payment_date_diff
					FROM
						view_ar_payments arp
						INNER JOIN
						clients mc
					ON
						( arp.cid = mc.id )
					WHERE
						mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
						AND arp.payment_status_type_id = ' . CPaymentStatusType::PHONE_CAPTURE_PENDING;

		if( true == valArr( $arrobjClients ) ) {
			$strSql .= ' AND arp.cid IN( ' . implode( ',', array_keys( $arrobjClients ) ) . ')';
		}

		if( true == valId( $intDatabaseId ) ) {
			$strSql .= ' AND mc.database_id = ' . $intDatabaseId;
		}

		$strSql .= ' AND ( DATE_TRUNC( \'day\', arp.payment_datetime ) = DATE_TRUNC( \'day\', ( NOW() - INTERVAL \'1 Days\' ))
							OR DATE_TRUNC( \'day\', arp.payment_datetime ) = DATE_TRUNC( \'day\', ( NOW() - INTERVAL \'2 Days\' ))
							OR DATE_TRUNC( \'day\', arp.payment_datetime ) = DATE_TRUNC( \'day\', ( NOW() - INTERVAL \'6 Days\' ))
							OR DATE_TRUNC( \'day\', arp.payment_datetime ) <= DATE_TRUNC( \'day\', ( NOW() - INTERVAL \'7 Days\' )) ) ';

		$strSql .= ' AND arp.id NOT IN ( SELECT
											ar_payment_id
										 FROM
										 	ar_payment_transactions
										 WHERE
										 	payment_transaction_type_id IN( ' . CPaymentTransactionType::PHONE_AUTH_PENDING_EMAIL . ', ' . CPaymentTransactionType::CANCEL . ' )
										 	AND cid IN( ' . implode( ',', array_keys( $arrobjClients ) ) . ')
										 AND DATE_TRUNC( \'day\', updated_on ) =  DATE_TRUNC( \'day\',  NOW() ) ) ';

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	// For timberline we need to post unconfirmed payments but this will affect with old payments which were not confirmed, so payments from 2010 will be posted. - NRW

	public static function fetchUnexportedArPaymentsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		$arrintNonElectronicPaymentTypes = [ CPaymentType::CASH, CPaymentType::CHECK, CPaymentType::MONEY_ORDER, CPaymentType::HAP, CPaymentType::BAH ];

		$strSql = 'SELECT
						ap.*
					FROM
						ar_payments ap
					WHERE
							ap.lease_id = ' . ( int ) $intLeaseId . '
						AND ( ap.ar_payment_export_id IN ( SELECT id FROM ar_payment_exports WHERE ar_payment_exports.cid = ' . ( int ) $intCid . ' AND confirmed_on IS NULL )
								OR
								ap.ar_payment_export_id IS NULL
							)
						AND ap.created_on >= \'01-01-2010 00:00:00\'
						AND ap.remote_primary_key IS NULL
						AND ap.payment_type_id NOT IN ( ' . implode( ',', $arrintNonElectronicPaymentTypes ) . ' )
						AND ap.payment_status_type_id = ' . CPaymentStatusType::CAPTURED . '
						AND ap.cid = ' . ( int ) $intCid;

		return self::fetchArPayments( $strSql, $objDatabase );
	}

	public static function fetchCustomFailedUndistributedArPayments( $objPaymentDatabase ) {

		$arrintOverridingDistributionStatusTypeIds 	= [
			CDistributionStatusType::CREDITED,
			CDistributionStatusType::CREDITING,
			CDistributionStatusType::DEBITED,
			CDistributionStatusType::DEBITING
		];

		$arrintApprovedPaymentTypeIds = [
			CPaymentType::ACH,
			CPaymentType::VISA,
			CPaymentType::MASTERCARD,
			CPaymentType::DISCOVER,
			CPaymentType::AMEX,
			CPaymentType::CHECK_21
		];

		$arrintApprovedPaymentStatusTypeIds = [
			CPaymentStatusType::RECALL,
			CPaymentStatusType::CHARGE_BACK_PENDING,
			CPaymentStatusType::CHARGED_BACK,
			CPaymentStatusType::CHARGE_BACK_FAILED
		];

		$strSql = 'SELECT
						DISTINCT ON ( ap.id ) ap.*
					FROM
					  ( SELECT vap.*, mc.company_name FROM
							view_ar_payments vap,
							clients mc
						  WHERE
							vap.cid = mc.id
							AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
							AND vap.payment_amount > 0
							AND (
									vap.distribute_on   >=  ( NOW() + INTERVAL \'1 day\'  )OR
									vap.distribute_return_on  >= ( NOW() + INTERVAL \'1 day\' )
								)
							AND vap.payment_type_id IN ( ' . implode( ',', $arrintApprovedPaymentTypeIds ) . ' )
							AND vap.payment_status_type_id = ' . CPaymentStatusType::CAPTURED . ') AS ap
						JOIN company_merchant_accounts cma ON ( cma.id = ap.company_merchant_account_id AND cma.cid = ap.cid )
						JOIN merchant_account_methods mam ON ( mam.company_merchant_account_id = cma.id AND mam.payment_medium_id = ap.payment_medium_id AND mam.payment_type_id = ap.payment_type_id AND mam.cid = cma.cid AND mam.cid = ap.cid )
						-- EXCLUDE RECORDS WHICH ARE ALREADY SETTELED OR IN PROCESS
						LEFT OUTER JOIN ( SELECT
												DISTINCT ON ( ar_payment_distributions.ar_payment_id )
												ar_payment_distributions.ar_payment_id,
												ar_payment_distributions.cid
											FROM
										  		ar_payment_distributions
										  		JOIN settlement_distributions ON ( ar_payment_distributions.settlement_distribution_id = settlement_distributions.id AND ar_payment_distributions.cid = settlement_distributions.cid AND settlement_distributions.distribution_status_type_id IN ( ' . implode( ',', $arrintOverridingDistributionStatusTypeIds ) . ' ))
										  	WHERE
										  	 	ar_payment_distributions.payment_distribution_type_id in ( ' . CPaymentDistributionType::SETTLEMENT . ' , ' . CPaymentDistributionType::REVERSAL . ' , ' . CPaymentDistributionType::REVERSAL . ' )
										) AS sd ON ( ap.id = sd.ar_payment_id AND ap.cid = sd.cid )

					WHERE
						mam.is_controlled_distribution = 1
						AND cma.is_disabled = 0
						AND sd.ar_payment_id IS NULL
						AND ( cma.disable_distribution_until IS NULL OR cma.disable_distribution_until <= NOW() )';

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchUnReconPreppedManuallyReturnedx937ArPayments( $objPaymentDatabase ) {

		$strSql = 'SELECT
						ap.*
					FROM
						view_ar_payments ap
						LEFT OUTER JOIN x937_return_items xri ON ( ap.id = xri.ar_payment_id )
					WHERE
						ap.payment_type_id = ' . ( int ) CPaymentType::CHECK_21 . '
						AND ap.check_is_converted <> 1
						AND ap.return_type_id IS NOT NULL
						AND ap.is_recon_prepped <> 1
						AND xri.id IS NULL;';

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchCustomUnReconPreppedManuallyReturnedNachaArPayments( $objPaymentDatabase ) {

		$strSql = 'SELECT
						ap.*
					FROM
						ar_payments ap
						JOIN clients mc ON ap.cid = mc.id AND mc.company_status_type_id = 4
						LEFT OUTER JOIN nacha_return_addenda_detail_records nradr ON ( ap.id = nradr.ar_payment_id )
						LEFT JOIN ar_payments ap2 ON ap2.id = ap.ar_payment_id AND ap2.payment_status_type_id = ' . CPaymentStatusType::TRANSFERRED . '
					WHERE
						ap.payment_type_id = ' . ( int ) CPaymentType::ACH . '
						AND ap.return_type_id IS NOT NULL
						AND ap.is_recon_prepped <> 1
						AND nradr.id IS NULL
						AND ap.captured_on > ( NOW() - INTERVAL \'3 months\' )
						AND ap2.id IS NULL
					ORDER BY
						ap.id;';

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchAssociatedArPaymentsByArDepositIdByCid( $intArDepositId, $intCid, $objPaymentDatabase ) {

		$strSql = 'SELECT
						ap.*
					FROM
						ar_payments ap
						JOIN ar_transactions ar ON ( ap.id = ar.ar_payment_id AND ap.cid = ar.cid )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
					AND ar_deposit_id = ' . ( int ) $intArDepositId;

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchCreditCardPaymentsByLeaseStatusTypeIdsByStastisticsEmailFilter( $strCommaSeperatedLeaseStatusTypes, $objStastisticsEmailFilter, $objDatabase ) {

		if( ( false == valArr( $objStastisticsEmailFilter->getPropertyIds() ) )
			|| ( false == valArr( $objStastisticsEmailFilter->getPaymentTypeIds() ) )
			|| ( 0 == strlen( $objStastisticsEmailFilter->getStartDate() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getEndDate() ) ) ) {
			return NULL;
		}

		$arrintPropertyIds = $objStastisticsEmailFilter->getPropertyIds();

		$strSql = 'SELECT
						sub_query.cid,
						sub_query.property_id,
						COUNT ( * ) AS credit_card_payment_count
					FROM
						(
							SELECT
								DISTINCT ON ( l.id ) ap.cid,
								ap.property_id AS property_id,
								l.id,
								COUNT ( ap.id ) AS payment_count
							FROM
								ar_payments ap
								JOIN leases l ON ( l.id = ap.lease_id AND l.cid = ap.cid )
								JOIN Properties AS ppt_disabled ON ( ppt_disabled.id = l.property_id AND l.cid = ppt_disabled.cid AND ppt_disabled.is_disabled != 1 )
								JOIN lease_customers lc ON ( l.id = lc.lease_id AND l.cid = lc.cid AND lc.lease_status_type_id IN ( ' . $strCommaSeperatedLeaseStatusTypes . ' ) AND lc.customer_id = ap.customer_id AND lc.cid = ap.cid )
								LEFT OUTER JOIN application_payments app ON ( app.ar_payment_id = ap.id AND app.cid = ap.cid AND app.cid =  ' . ( int ) $objStastisticsEmailFilter->getCid() . ' )
							WHERE
								ap.cid = ' . ( int ) $objStastisticsEmailFilter->getCid() . '
								AND ap.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
								AND ap.payment_status_type_id = ' . CPaymentStatusType::CAPTURED . '
								AND ap.payment_type_id	IN ( ' . CPaymentType::DISCOVER . ' ,' . CPaymentType::MASTERCARD . ' ,' . CPaymentType::VISA . ' ,' . CPaymentType::AMEX . ' )
								AND ap.payment_datetime::date >= \'' . $objStastisticsEmailFilter->getStartDate() . '\'
								AND ap.payment_datetime::date <= \'' . $objStastisticsEmailFilter->getEndDate() . '\'
								AND app.id IS NULL
							GROUP BY
								l.id,
								ap.property_id,
								ap.cid
						) AS sub_query
					WHERE
						sub_query.cid = ' . ( int ) $objStastisticsEmailFilter->getCid() . '
					GROUP BY
						sub_query.property_id,
						sub_query.cid';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRecurringPaymentsByLeaseStatusTypeIdsByStastisticsEmailFilter( $strCommaSeperatedLeaseStatusTypes, $objStastisticsEmailFilter, $objDatabase ) {

		if( ( false == valArr( $objStastisticsEmailFilter->getPropertyIds() ) )
			|| ( false == valArr( $objStastisticsEmailFilter->getPaymentStatusTypeIds() ) )
			|| ( 0 == strlen( $objStastisticsEmailFilter->getStartDate() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getEndDate() ) ) ) {
			return NULL;
		}

		$arrintPropertyIds = $objStastisticsEmailFilter->getPropertyIds();

		$strSql = 'SELECT
						recurring_payment.cid,
						recurring_payment.property_id,
						COUNT ( * ) AS recurring_payment_count
					FROM
						(
							SELECT
								DISTINCT ON ( l.id ) ap.cid,
								ap.property_id AS property_id,
								l.id,
								COUNT ( ap.id ) AS payment_count
							FROM
								ar_payments ap
								JOIN leases l ON ( l.id = ap.lease_id AND l.cid = ap.cid )
								INNER JOIN Properties AS ppt_disabled ON ( ppt_disabled.id = l.property_id AND l.cid = ppt_disabled.cid AND ppt_disabled.is_disabled != 1 )
								INNER JOIN lease_customers lc ON ( l.id = lc.lease_id AND l.cid = lc.cid AND lc.lease_status_type_id IN ( ' . $strCommaSeperatedLeaseStatusTypes . ' ) AND lc.customer_id = ap.customer_id AND lc.cid = ap.cid )
								LEFT OUTER JOIN application_payments app ON ( app.ar_payment_id = ap.id AND app.cid = ap.cid AND app.cid = ' . ( int ) $objStastisticsEmailFilter->getCid() . ' )
							WHERE
								ap.cid = ' . ( int ) $objStastisticsEmailFilter->getCid() . '
								AND ap.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
								AND ap.payment_status_type_id = ' . CPaymentStatusType::CAPTURED . '
								AND ap.payment_type_id IN ( ' . implode( ',', $objStastisticsEmailFilter->getPaymentTypeIds() ) . ' )
								AND ap.payment_datetime::date >= \'' . $objStastisticsEmailFilter->getStartDate() . '\'::date
								AND ap.payment_datetime::date <= \'' . $objStastisticsEmailFilter->getEndDate() . '\'::date
								AND app.id IS NULL
								AND ap.scheduled_payment_id IS NOT NULL
							GROUP BY
								l.id,
								ap.property_id,
								ap.cid
						) AS recurring_payment
					WHERE
						recurring_payment.cid = ' . ( int ) $objStastisticsEmailFilter->getCid() . '
					GROUP BY
						recurring_payment.property_id,
						recurring_payment.cid';

 		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAchPaymentsByLeaseStatusTypeIdsByStastisticsEmailFilter( $strCommaSeperatedLeaseStatusTypes, $objStastisticsEmailFilter, $objDatabase ) {

		if( ( false == valArr( $objStastisticsEmailFilter->getPropertyIds() ) )
		|| ( 0 == strlen( $objStastisticsEmailFilter->getStartDate() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getEndDate() ) ) ) {
			return NULL;
		}

		$arrintPropertyIds = $objStastisticsEmailFilter->getPropertyIds();

		$strSql = 'SELECT
						total_ach_payment_cnt.cid,
						total_ach_payment_cnt.property_id,
						COUNT ( total_ach_payment_cnt.* ) AS ach_payment_count
					FROM
						(
							SELECT
								DISTINCT ON ( l.id ) ap.cid,
								ap.property_id AS property_id,
								l.id,
								COUNT ( ap.id ) AS payment_count
							FROM
								ar_payments ap
								JOIN leases l ON ( l.id = ap.lease_id AND l.cid = ap.cid )
								JOIN Properties AS ppt_disabled ON ( ppt_disabled.id = l.property_id AND l.cid = ppt_disabled.cid AND ppt_disabled.is_disabled != 1 )
								JOIN lease_customers lc ON ( l.id = lc.lease_id AND l.cid = lc.cid AND lc.lease_status_type_id IN ( ' . $strCommaSeperatedLeaseStatusTypes . '  ) AND lc.customer_id = ap.customer_id AND lc.cid = ap.cid )
								LEFT OUTER JOIN application_payments app ON ( app.ar_payment_id = ap.id AND app.cid = ap.cid AND app.cid = ' . ( int ) $objStastisticsEmailFilter->getCid() . ' )
							WHERE
								ap.cid = ' . ( int ) $objStastisticsEmailFilter->getCid() . '
								AND ap.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
								AND ap.payment_status_type_id = ' . CPaymentStatusType::CAPTURED . '
								AND ap.payment_type_id	= ' . CPaymentType::ACH . '
								AND ap.payment_datetime::date >= \'' . $objStastisticsEmailFilter->getStartDate() . '\'
								AND ap.payment_datetime::date <= \'' . $objStastisticsEmailFilter->getEndDate() . '\'
								AND app.id IS NULL
							GROUP BY
								l.id,
								ap.property_id,
								ap.cid
						) AS total_ach_payment_cnt
					WHERE
						total_ach_payment_cnt.cid = ' . ( int ) $objStastisticsEmailFilter->getCid() . '
					GROUP BY
						total_ach_payment_cnt.property_id,
						total_ach_payment_cnt.cid';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFirstPaymentInBatchBySettlementDistributionIdByCid( $intSettlementDistributionId, $intCid, $objPaymentDatabase ) {

		$strSql = 'SELECT
						cp.*
					FROM
						ar_payments cp JOIN ar_payment_distributions cpd ON( cp.id = cpd.ar_payment_id AND cp.cid = cpd.cid )
					WHERE
						cpd.settlement_distribution_id = ' . ( int ) $intSettlementDistributionId . '
						AND cpd.cid = ' . ( int ) $intCid . '
					ORDER BY
						cp.created_on
					LIMIT 1';

		return self::fetchArPayment( $strSql, $objPaymentDatabase );
	}

	public static function fetchNewArPaymentIds( $intNumNewArPaymentId, $objPaymentDatabase ) {
		if( 0 >= intval( $intNumNewArPaymentId ) ) return NULL;

		CArPayment::updateArPaymentSequence( $objPaymentDatabase, ( int ) $intNumNewArPaymentId );

		$strSql	= 'SELECT nextval( \'ar_payments_id_seq\'::text ) AS id FROM generate_series( 1, ' . ( int ) $intNumNewArPaymentId . ' )';

		$objDataset = $objPaymentDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return NULL;
		}

		$arrintArPaymentIds = [];
		if( 0 < $objDataset->getRecordCount() ) {

			while( false == $objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();
				$arrintArPaymentIds[] = $arrmixValues['id'];
				$objDataset->next();
			}
		}

		$objDataset->cleanup();
		return $arrintArPaymentIds;
	}

	public static function fetchArPaymentsCountByPropertyIdsByPaymentStatusTypeIdsByCid( $arrintPropertyIds, $arrintPaymentStatusTypeIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		if( false == valArr( $arrintPaymentStatusTypeIds ) ) return NULL;

		$strSql = 'SELECT
						count( id ),
						payment_status_type_id
					FROM
						ar_payments
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND payment_status_type_id IN ( ' . implode( ',', $arrintPaymentStatusTypeIds ) . ' )
						AND block_association <> 1
						GROUP BY payment_status_type_id';

		$arrintResponse = fetchData( $strSql, $objClientDatabase );
		if( true == valArr( $arrintResponse ) ) {
			return  rekeyArray( 'payment_status_type_id', $arrintResponse );
		}

		return [];
	}

	public static function fetchCustomArPaymentsByApplicationIds( $arrintApplicationIds, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						ar.*,
						aa.id as applicant_application_id,
						aa.application_id as application_id
					FROM
						ar_payments ar
						JOIN application_payments ap ON ( ap.ar_payment_id = ar.id AND ap.cid = ar.cid )
						JOIN applicant_applications aa ON ( aa.applicant_id = ap.applicant_id AND aa.cid = ap.cid AND aa.application_id = ap.application_id ' . $strCheckDeletedAASql . ' )
					WHERE
						aa.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND ar.payment_status_type_id IN( ' . CPaymentStatusType::AUTHORIZED . ', ' . CPaymentStatusType::CAPTURED . ', ' . CPaymentStatusType::CANCELLED . ' )
						AND aa.completed_on IS NULL
						AND	(ap.application_stage_id, ap.application_status_id) IN ( ' . sqlIntMultiImplode( [ CApplicationStage::PRE_APPLICATION => [ CApplicationStatus::STARTED ], CApplicationStage::APPLICATION => [ CApplicationStatus::COMPLETED ] ] ) . ')';

		return self::fetchArPayments( $strSql, $objDatabase );
	}

	public static function fetchCustomMoneyGramArPaymentsByApplicationIds( $arrintApplicationIds, $objDatabase, $boolIncludeDeletedAA = false ) {

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT arp.*,
						aa.id as applicant_application_id,
						aa.application_id as application_id,
						aa.applicant_id as applicant_id
					FROM
						applications a
						JOIN applicant_applications aa ON ( aa.application_id = a.id AND aa.cid = a.cid ' . $strCheckDeletedAASql . ' )
						JOIN applicants app ON ( app.id = aa.applicant_id AND app.cid = a.cid )
						RIGHT JOIN ar_payments arp ON ( arp.lease_id = a.lease_id AND arp.customer_id = app.customer_id AND arp.cid = a.cid )
					WHERE
						arp.payment_type_id = ' . CPaymentType::EMONEY_ORDER . '
						AND arp.payment_status_type_id = ' . CPaymentStatusType::CAPTURED . '
						AND aa.completed_on IS NULL
						AND aa.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND a.application_completed_on IS NULL
						AND ( ( a.application_stage_id = ' . CApplicationStage::APPLICATION . ' AND a.application_status_id = ' . ( int ) CApplicationStatus::PARTIALLY_COMPLETED . ' ) OR a.requires_capture = TRUE )
						AND to_char( a.created_on, \'MM/DD/YYYY\' )::date >= ( CURRENT_DATE - integer \'60\' )::date';

		return self::fetchArPayments( $strSql, $objDatabase );
	}

	public static function fetchArPaymentsByApplicantIdByApplicationIdByApplicationStageStatusIdsByCid( $intApplicantId, $intApplicationId, $arrintApplicationStageStatusIds, $intCid, $objDatabase, $boolIsPhoneAuthPending = false ) {
		if( false == valArr( $arrintApplicationStageStatusIds ) ) return true;

		if( true == $boolIsPhoneAuthPending ) {
			$arrintPaymentStatusTypeIds[] = CPaymentStatusType::PHONE_CAPTURE_PENDING;
		} else {
			$arrintPaymentStatusTypeIds[] = CPaymentStatusType::CAPTURED;
			$arrintPaymentStatusTypeIds[] = CPaymentStatusType::AUTHORIZED;
		}

		$strSql = 'SELECT
						ar.*
					FROM
						ar_payments ar
						JOIN application_payments ap ON ( ap.ar_payment_id = ar.id AND ap.cid = ar.cid )
					WHERE
						ap.applicant_id = ' . ( int ) $intApplicantId . '
						AND ap.cid = ' . ( int ) $intCid . '
						AND ap.application_id = ' . ( int ) $intApplicationId . '
						AND ar.payment_status_type_id IN ( ' . implode( ',', $arrintPaymentStatusTypeIds ) . ' )
						AND ( ap.application_stage_id, ap.application_status_id ) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )';

		return self::fetchArPayments( $strSql, $objDatabase );
	}

	public static function fetchApplicationArPaymentsStatusByApplicantIdByApplicationIdByCid( $intApplicantId, $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( ap.application_id )
						CASE
							WHEN ' . CPaymentStatusType::PHONE_CAPTURE_PENDING . ' = ar.payment_status_type_id THEN 1
							WHEN ar.payment_status_type_id IN( ' . CPaymentStatusType::AUTHORIZED . ', ' . CPaymentStatusType::CAPTURED . ' ) THEN 2
							WHEN ar.payment_status_type_id NOT IN( ' . CPaymentStatusType::AUTHORIZED . ', ' . CPaymentStatusType::CAPTURED . ', ' . CPaymentStatusType::PHONE_CAPTURE_PENDING . ' ) THEN 3
						END AS payment_status_type
					FROM
						ar_payments ar
						JOIN application_payments ap ON ( ap.ar_payment_id = ar.id AND ap.cid = ar.cid )
					WHERE
						ap.applicant_id = ' . ( int ) $intApplicantId . '
						AND ap.cid = ' . ( int ) $intCid . '
						AND ap.application_id = ' . ( int ) $intApplicationId;

		$arrstrData = fetchData( $strSql, $objDatabase );
		if( true == valArr( $arrstrData ) && true == isset ( $arrstrData[0]['payment_status_type'] ) ) {
			return $arrstrData[0]['payment_status_type'];
		}

		return 0;
	}

	public static function fetchCustomSimplePropertyIdsByArPaymentIds( $arrintArPaymentIds, $objDatabase ) {
		if( false == valArr( $arrintArPaymentIds ) ) return NULL;
		$strSql = 'SELECT ar.id,ar.property_id FROM ar_payments ar WHERE ar.id  IN ( ' . implode( ',', $arrintArPaymentIds ) . ' ) AND ar.cid = ar.cid';
		return $arrmixResult = fetchData( $strSql, $objDatabase );
	}

	public static function fetchIntegratedBatchesByLastSyncOnByIntegrationDatabaseIdByCid( $strLastSyncOn, $intIntegrationDatabaseId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ap.company_merchant_account_id,
						ap.distribute_on
					FROM
						ar_payments ap
						JOIN integration_result_references irr ON( irr.cid = ap.cid AND irr.reference_number = ap.id )
						JOIN integration_results ir ON( ir.cid = ap.cid AND ir.id = irr.integration_result_id AND ir.integration_service_id =' . CIntegrationService::SEND_AR_PAYMENT . ' )
						JOIN property_integration_databases pid ON( pid.cid = ap.cid AND pid.property_id = ap.property_id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND pid.integration_database_id = ' . ( int ) $intIntegrationDatabaseId . '
						AND ir.result_datetime BETWEEN \'' . $strLastSyncOn . '\' AND NOW()
						AND ap.remote_primary_key IS NOT NULL
					GROUP BY ap.company_merchant_account_id, ap.distribute_on';

		return $arrmixResult = fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomUnbatchedArPaymentsByDateAndProcessingBankAccountIdAndMerchantGatewayId( $strBeginTime, $strEndTime, $intProcessingBankAccountId, $intMerchantGatewayId, $objDatabase ) {
		$strSql = 'SELECT
						ap.*
					FROM
						ar_payments ap
						JOIN clients as mc ON mc.id = ap.cid
						LEFT JOIN batched_ar_payments AS bap ON ( bap.ar_payment_id = ap.id AND bap.cid = ap.cid )
					WHERE
						bap.id IS NULL
						AND mc.company_status_type_id IN ( ' . CCompanyStatusType::CLIENT . ',' . CCompanyStatusType::TERMINATED . ')
						AND ap.payment_datetime BETWEEN \'' . $strBeginTime . '\' AND \'' . $strEndTime . '\'
						AND ap.processing_bank_account_id = ' . ( int ) $intProcessingBankAccountId . '
						AND ap.merchant_gateway_id = ' . ( int ) $intMerchantGatewayId;

		return self::fetchArPayments( $strSql, $objDatabase );
	}

	public static function fetchCustomUnbatchedArPaymentDatesByProcessingBankAccountIdAndMerchantGatewayId( $intProcessingBankAccountId, $intMerchantGatewayId, $objDatabase ) {

		$strSql = 'SELECT
						date_trunc(\'day\', ap.payment_datetime - INTERVAL \'1 hour\') as payment_date
					FROM
						ar_payments ap
						JOIN clients as mc ON mc.id = ap.cid
						LEFT JOIN batched_ar_payments AS bap ON ( bap.ar_payment_id = ap.id AND bap.cid = ap.cid )
					WHERE
						bap.id IS NULL
						AND mc.company_status_type_id IN ( ' . CCompanyStatusType::CLIENT . ',' . CCompanyStatusType::TERMINATED . ')
						AND ap.processing_bank_account_id = ' . ( int ) $intProcessingBankAccountId . '
						AND ap.merchant_gateway_id = ' . ( int ) $intMerchantGatewayId . '
					GROUP BY
						date_trunc(\'day\', ap.payment_datetime - INTERVAL \'1 hour\')
					ORDER BY
						date_trunc(\'day\', ap.payment_datetime - INTERVAL \'1 hour\')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomArPaymentsByEftBatchId( $intEftBatchId, $objPaymentDatabase ) {
		$strSql = 'SELECT
						ap.*
					FROM
						ar_payments ap
						JOIN batched_ar_payments bap ON ( bap.ar_payment_id = ap.id AND bap.cid = ap.cid )
					WHERE
						bap.eft_batch_id = ' . ( int ) $intEftBatchId . '
					UNION
					SELECT
						ap.*
					FROM
						ar_payments ap
						JOIN batched_ar_payments bap ON ( bap.ar_payment_id = ap.id AND bap.cid = ap.cid )
						JOIN eft_batches eb ON eb.id = bap.eft_batch_id
					WHERE
						eb.aggregate_eft_batch_id = ' . ( int ) $intEftBatchId;

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchCustomArPaymentsByChargeBackId( $intChargeBackId, $objPaymentDatabase ) {
		$strSql = 'SELECT
						ap.*
					FROM
						ar_payments ap
						JOIN charge_backs cb ON cb.ar_payment_id = ap.id AND cb.cid = ap.cid
					WHERE
						cb.id = ' . ( int ) $intChargeBackId;

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	// fetch all company merchant account transactions by company merchant account ids and payment type id which goes over $100K in the year.

	public static function fetchCustomArPaymentsByMerchantAccountId( $intCompanyMerchantAccountId, $objDatabase ) {

		if( false == is_numeric( $intCompanyMerchantAccountId ) ) return NULL;

		$strSql = 'SELECT
						ap.company_merchant_account_id,
						ap.payment_type_id,
						DATE_TRUNC( \'year\', ap.payment_datetime ) AS payment_month,
						cma.account_name,
						mc.company_name,
						sum( ap.total_amount ) AS total_amount
					FROM
						ar_payments as ap
						JOIN company_merchant_accounts as cma ON ( ap.company_merchant_account_id = cma.id AND ap.cid = cma.cid )
						JOIN clients as mc ON ( ap.cid = mc.id AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' )
					WHERE
						ap.company_merchant_account_id = ' . ( int ) $intCompanyMerchantAccountId . '
						AND ap.merchant_gateway_id IN (' . CMerchantGateway::VANTIV_CREDIT_CARD . ', ' . CMerchantGateway::LITLE_CREDIT_CARD . ')
						AND ap.payment_type_id IN ( ' . CPaymentType::VISA . ', ' . CPaymentType::MASTERCARD . ' )
						AND ap.payment_status_type_id = ' . CPaymentStatusType::CAPTURED . '
						AND DATE_TRUNC( \'year\', ap.payment_datetime ) > DATE_TRUNC( \'year\', NOW() - INTERVAL \' 3 year \' )
						AND DATE_TRUNC( \'year\', ap.payment_datetime ) >= \'2013/01/01\'
					GROUP BY
						ap.company_merchant_account_id,
						ap.payment_type_id,
						DATE_TRUNC( \'year\', ap.payment_datetime ),
						cma.account_name,
						mc.company_name
					ORDER BY
						payment_month DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomArPaymentsDetailsByMerchantAccountId( $intCompanyMerchantAccountId, $objDatabase ) {

		if( false == is_numeric( $intCompanyMerchantAccountId ) ) return NULL;

		$strSql = 'SELECT
						ap.id,
						ap.company_merchant_account_id,
						ap.payment_type_id,
						ap.payment_datetime,
						ap.total_amount,
						cma.account_name,
						mc.company_name
					FROM
						ar_payments as ap
						JOIN company_merchant_accounts as cma ON ( ap.company_merchant_account_id = cma.id AND ap.cid = cma.cid )
						JOIN clients as mc ON ( ap.cid = mc.id AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' )
					WHERE
						ap.company_merchant_account_id = ' . ( int ) $intCompanyMerchantAccountId . '
						AND ap.merchant_gateway_id IN (' . CMerchantGateway::VANTIV_CREDIT_CARD . ', ' . CMerchantGateway::LITLE_CREDIT_CARD . ')
						AND ap.payment_type_id IN ( ' . CPaymentType::VISA . ', ' . CPaymentType::MASTERCARD . ' )
						AND ap.payment_status_type_id = ' . CPaymentStatusType::CAPTURED . '
						AND DATE_TRUNC( \'year\', ap.payment_datetime ) > DATE_TRUNC( \'year\', NOW() - INTERVAL \' 3 year \' )
						AND DATE_TRUNC( \'year\', ap.payment_datetime ) >= \'2013/01/01\'
					GROUP BY
						ap.id,
						ap.company_merchant_account_id,
						ap.payment_type_id,
						ap.payment_datetime,
						ap.total_amount,
						cma.account_name,
						mc.company_name
					ORDER BY
						ap.payment_type_id,
						ap.payment_datetime DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchArPaymentCount( $strWhere = NULL, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ar_payments', $objDatabase );
	}

	public static function fetchArPaymentsByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						view_ar_payments
					WHERE
						cid = ' . ( int ) $intCid . ' AND
						customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )';

		return self::fetchArPayments( $strSql, $objDatabase );
	}

	public static function fetchTotalOfArPaymentsByCustomDateByCustomerIdByLeaseIdByCid( $strCustomDate, $intCustomerId, $intLeaseId, $intCid, $objDatabase ) {

		if( false == valStr( $strCustomDate ) || false == valId( $intCustomerId ) || false == valId( $intLeaseId ) ) return NULL;

		$strSql = 'SELECT
						SUM( payment_amount ) as total_payment_amount
					FROM
						ar_payments
					WHERE
						cid = ' . ( int ) $intCid . ' AND
						customer_id = ' . ( int ) $intCustomerId . ' AND
						lease_id = ' . ( int ) $intLeaseId . ' AND
						payment_status_type_id IN ( ' . CPaymentStatusType::AUTHORIZED . ',' . CPaymentStatusType::CAPTURED . ' ) AND
						cast( created_on as date ) >= cast( \'' . $strCustomDate . '\'  as date )';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['total_payment_amount'] ) ) return $arrintResponse[0]['total_payment_amount'];
	}

	public static function fetchArPaymentsByCustomerIdsByPropertyIdsByCid( $arrintCustomerIds, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerIds ) && false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						view_ar_payments
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND	customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )';

		return self::fetchArPayments( $strSql, $objDatabase );
	}

	public static function fetchArPaymentByLeaseIdByDateByCidByPropertyId( $intLeaseId, $strDate, $intCid, $intPropertyId, $objDatabase ) {

		if( false == valStr( $strDate ) ) return NULL;
		$strSql = 'SELECT
						*
					FROM
						ar_payments
					WHERE
					lease_id = ' . ( int ) $intLeaseId . '
					AND cid = ' . ( int ) $intCid . '
					AND property_id = ' . ( int ) $intPropertyId . '
					AND payment_status_type_id IN ( ' . CPaymentStatusType::AUTHORIZED . ',' . CPaymentStatusType::CAPTURED . ' )
					AND cast( created_on as date ) >= cast( \'' . $strDate . '\'  as date ) - interval \'7 days\'
					AND cast( created_on as date ) < cast(\'' . $strDate . '\' as date) order by created_on desc limit 1
				';

		return self::fetchArPayment( $strSql, $objDatabase );
	}

	public static function fetchArPaymentByLeaseIdByDateByCidByPropertyIdByCustomerId( $intLeaseId, $strDate, $intCid, $intPropertyId, $intCustomerId, $objDatabase ) {

		if( false == valId( $intLeaseId ) || false == valStr( $strDate ) || false == valId( $intPropertyId ) || false == valId( $intCustomerId ) ) return NULL;

		$strSql = 'SELECT
						count( id )
					FROM
						ar_payments
					WHERE
						lease_id = ' . ( int ) $intLeaseId . '
						AND customer_id = ' . ( int ) $intCustomerId . '
						AND cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND payment_status_type_id IN ( ' . CPaymentStatusType::AUTHORIZED . ',' . CPaymentStatusType::CAPTURED . ' )
						AND to_char( cast( created_on as date ), \'yyyy-mm\' ) = to_char( cast( \'' . $strDate . '\' as date ), \'yyyy-mm\' )
						AND cast( created_on as date ) < cast( \'' . $strDate . '\' as date) limit 1';
		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];
	}

	public static function fetchArPaymentsByArPaymentTransmissionIdsByPropertyIdsByCid( $arrintArPaymentTransmissionIds, $arrintPropertyIds, $intCid, $objPaymentDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintArPaymentTransmissionIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						view_ar_payments
					WHERE
						ar_payment_transmission_id IN( ' . implode( ',', $arrintArPaymentTransmissionIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						ORDER BY
							order_num ';

		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchCustomArPaymentsByArPaymentTransmissionIdsByCid( $arrintArPaymentTransmissionIds, $intCid, $objPaymentDatabase ) {

		if( false == valArr( $arrintArPaymentTransmissionIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ar_payment_transmission_id
					FROM
						ar_payments
					WHERE
						created_on >= ( NOW() - INTERVAL \'30 days\' )
						AND ar_payment_transmission_id IN( ' . implode( ',', $arrintArPaymentTransmissionIds ) . ' )
						AND payment_status_type_id = ' . CPaymentStatusType::BATCHING . '
						AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objPaymentDatabase );
	}

	public static function fetchQuickSearchArPayments( $arrmixFilteredExplodedSearch, $objDatabase ) {

		$strAccountIdCondition = ( true == isset( $arrmixFilteredExplodedSearch[0] ) && is_numeric( $arrmixFilteredExplodedSearch[0] ) ) ? '  ap.id = ' . ( int ) $arrmixFilteredExplodedSearch[0] . ' OR ap.property_id = ' . ( int ) $arrmixFilteredExplodedSearch[0] : '';

		if( true == isset( $arrmixFilteredExplodedSearch[0] ) && true == is_numeric( $arrmixFilteredExplodedSearch[0] ) ) {

			$strSql = 'SELECT
							ap.id,
							ap.property_id
						FROM
							view_ar_payments ap
						WHERE
							(' . $strAccountIdCondition . ') AND ap.company_merchant_account_id IS NOT NULL
						ORDER BY
							ap.id
						LIMIT
							10';

			return fetchData( $strSql, $objDatabase );
		}

		return NULL;
	}

	public static function fetchSumOfUnAllocatedArPaymentsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT sum( vat.transaction_amount_due )
					FROM
						view_ar_transactions vat
					WHERE
						vat.cid = ' . ( int ) $intCid . '
						AND vat.lease_id = ' . ( int ) $intLeaseId . '
						AND vat.transaction_amount_due < 0
						AND is_deleted = false
						AND ( ar_code_type_id IS NULL OR ar_code_type_id NOT IN ( ' . CDefaultArCode::BEGINNING_DEPOSIT_HELD . ', ' . CDefaultArCode::BEGINNING_DEPOSIT_ADJUSTMENT . ' ) )';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrmixResult[0]['sum'] ) ) {
			return $arrmixResult[0]['sum'];
		} else {
			return 0;
		}
	}

	public static function fetchDeclinedPaymentsByCustomerIdByPropertyIdByCid( $intCustomerId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ar_payments ap
					WHERE
						ap.payment_status_type_id = ' . CPaymentStatusType::DECLINED . '
						AND ap.customer_id = ' . ( int ) $intCustomerId . '
						AND ap.property_id = ' . ( int ) $intPropertyId . '
						AND ap.cid = ' . ( int ) $intCid . '
						AND ap.payment_datetime > ( NOW() - INTERVAL \'24 hours\' )
					ORDER BY
						payment_datetime DESC';

		return self::fetchArPayments( $strSql, $objDatabase );
	}

	public static function fetchNonIntegratedPaymentsFromLastThirtyDaysByCidByPropertyIds( $intCid, $objDatabase, $arrintPropertyIds = NULL ) {

		$strSql = '	SELECT
						ap.*,
						apd.exported_on
					FROM
						ar_payments ap
						LEFT JOIN ar_payment_details apd ON ( apd.cid = ap.cid AND apd.ar_payment_id = ap.id )
					WHERE
						ap.cid = ' . ( int ) $intCid . ' ';

		if( true == isset( $arrintPropertyIds ) && true == valArr( $arrintPropertyIds ) ) {
			$strSql .= ' AND ap.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}

		$strSql .= ' AND ap.payment_status_type_id IN( ' . CPaymentStatusType::REVERSED . ', ' . CPaymentStatusType::RECALL . ', ' . CPaymentStatusType::CAPTURED . ' )
					 AND ap.remote_primary_key IS NOT NULL
					 AND
					 (
						( ap.payment_status_type_id IN ( ' . CPaymentStatusType::CAPTURED . ' ) AND ap.remote_primary_key NOT LIKE \'%completed%\' )
						OR ( ap.payment_status_type_id IN ( ' . CPaymentStatusType::REVERSED . ', ' . CPaymentStatusType::RECALL . ' ) AND
						( ap.return_remote_primary_key IS NOT NULL AND ap.remote_primary_key NOT LIKE \'%completed%\') )
					 )
					 AND ap.payment_datetime > ( NOW() - INTERVAL \'30 days\' );';

		return CArPayments::fetchArPayments( $strSql, $objDatabase );

	}

	public static function fetchSendPaymentsByCidByPropertyIds( $intCid, $objDatabase, $arrintPropertyIds = NULL ) {

		$strSql = ' SELECT
						ap.*,
						apd.exported_on
					FROM
						ar_payments ap
						LEFT JOIN ar_payment_details apd ON ( apd.cid = ap.cid AND apd.ar_payment_id = ap.id )
					WHERE
						ap.cid = ' . ( int ) $intCid . ' ';

		if( true == isset( $arrintPropertyIds ) && true == valArr( $arrintPropertyIds ) ) {
			$strSql .= ' AND ap.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}

			$strSql .= ' AND
						ap.payment_status_type_id IN ( ' . CPaymentStatusType::CAPTURED . ', ' . CPaymentStatusType::REVERSED . ', ' . CPaymentStatusType::RECALL . ' ) AND
						ap.remote_primary_key IS NULL
						AND ap.payment_datetime > ( NOW() - INTERVAL \'30 days\' );';

		return CArPayments::fetchArPayments( $strSql, $objDatabase );
	}

	public static function fetchCountOfPaymentsByCustomerIdByPropertyIdByCid( $intCustomerId, $intPropertyId, $intLeaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						count(ap.id)
					FROM
						ar_payments ap
						JOIN leases l ON ( l.id = ap.lease_id AND l.cid = ap.cid )
						JOIN lease_customers lc ON ( l.id = lc.lease_id AND l.cid = lc.cid AND lc.customer_id = ap.customer_id AND lc.cid = ap.cid )
					WHERE
						ap.payment_status_type_id IN (' . CPaymentStatusType::AUTHORIZED . ',' . CPaymentStatusType::CAPTURED . ',' . CPaymentStatusType::PHONE_CAPTURE_PENDING . ')
						AND ap.customer_id = ' . ( int ) $intCustomerId . '
						AND ap.property_id = ' . ( int ) $intPropertyId . '
						AND ap.lease_id = ' . ( int ) $intLeaseId . '
						AND ap.cid = ' . ( int ) $intCid . '
						AND ap.payment_datetime > ( NOW() - INTERVAL \'24 hours\' )';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];
	}

	public static function fetchSumOfUnAllocatedArPaymentsByLeaseIdsByCid( $arrintLeaseIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseIds ) ) return;

		$strSql = 'SELECT
						abs( sum( vat.transaction_amount_due ) ) AS transaction_amount_due,
						vat.lease_id
					FROM
						view_ar_transactions vat
					WHERE
						vat.cid = ' . ( int ) $intCid . '
						AND vat.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND vat.transaction_amount_due < 0
						AND is_deleted = false
						AND ( ar_code_type_id IS NULL OR ar_code_type_id NOT IN ( ' . CDefaultArCode::BEGINNING_DEPOSIT_HELD . ', ' . CDefaultArCode::BEGINNING_DEPOSIT_ADJUSTMENT . ' ) )
					GROUP BY
						vat.lease_id';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchWorksArPaymentsByPropertyIdsByPaymentStatusTypeIdByCid( $arrintPropertyIds, $intPaymentStatusTypeId, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		$strSql = 'SELECT
						ap.*,
						CASE
							WHEN pgs.id IS NOT NULL AND pgs.activate_standard_posting = true
							THEN pgs.ar_post_month
							ELSE NULL
						END AS post_month,
						CASE
							WHEN pgs.activate_standard_posting IS NULL
							THEN false
							ELSE pgs.activate_standard_posting
						END AS activate_standard_posting,
						p.property_name
					FROM
						ar_payments ap
						JOIN properties p ON ( ap.property_id = p.id AND ap.cid = p.cid )
						LEFT JOIN property_gl_settings pgs ON ( p.id = pgs.property_id AND p.cid = pgs.cid )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.payment_status_type_id = ' . ( int ) $intPaymentStatusTypeId . '
						AND ap.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND 1 = CASE
									WHEN pgs.id IS NOT NULL AND pgs.is_ar_migration_mode = true
									THEN 0
									ELSE 1
								END
					Order by
							p.property_name';

		return self::fetchArPayments( $strSql, $objClientDatabase );
	}

	// Dashboard Query

	public static function fetchPaginatedCapturingArPaymentsByDashboardFilterByCid( $objDashboardFilter, $intOffset, $intLimit, $strSortBy, $intCid, $objClientDatabase ) {
		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = '';

		if( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) {
			$strPriorityWhere = ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )';
		}

		$strSql = 'SELECT
						*
					FROM (
						SELECT
							CASE
								WHEN TRIM( dp.financial_payments->>\'urgent_amount_greater_than\' ) != \'\' AND ap.payment_amount >= ( dp.financial_payments->>\'urgent_amount_greater_than\' )::int THEN 3
								WHEN TRIM( dp.financial_payments->>\'urgent_amount_less_than\' ) != \'\' AND ap.payment_amount < ( dp.financial_payments->>\'urgent_amount_less_than\' )::integer THEN 3
								WHEN TRIM( dp.financial_payments->>\'urgent_payment_submitted_since\' ) != \'\' AND get_date_difference( \'hour\', NOW(), ap.created_on ) >= ( dp.financial_payments->>\'urgent_payment_submitted_since\' )::int THEN 3
								WHEN TRIM ( dp.financial_payments->>\'urgent_payment_source_ids\' ) != \'\' AND ap.ps_product_id = ANY ( TRIM ( dp.financial_payments->>\'urgent_payment_source_ids\' )::integer[] ) THEN 3
								WHEN TRIM( dp.financial_payments->>\'important_amount_greater_than\' ) != \'\' AND ap.payment_amount >= ( dp.financial_payments->>\'important_amount_greater_than\' )::int THEN 2
								WHEN TRIM( dp.financial_payments->>\'important_amount_less_than\' ) != \'\' AND ap.payment_amount < ( dp.financial_payments->>\'important_amount_less_than\' )::integer THEN 2
								WHEN TRIM( dp.financial_payments->>\'important_payment_submitted_since\' ) != \'\' AND get_date_difference( \'hour\', NOW(), ap.created_on ) >= ( dp.financial_payments->>\'important_payment_submitted_since\' )::int THEN 2
								WHEN TRIM ( dp.financial_payments->>\'important_payment_source_ids\' ) != \'\' AND ap.ps_product_id = ANY ( TRIM ( dp.financial_payments->>\'important_payment_source_ids\' )::integer[] ) THEN 2
								ELSE 1
							END as priority,
							ap.id,
							ap.cid,
							ap.payment_type_id,
							func_format_customer_name( ap.billto_name_first, ap.billto_name_last ) as customer_name,
							COALESCE( cl.building_name || \' - \' || cl.unit_number_cache, cl.building_name, cl.unit_number_cache ) as unit_number,
							cl.property_name,
							ap.property_id,
							ap.receipt_datetime,
							ap.payment_amount,
							pst.name as payment_status,
							pt.name as payment_type,
							cl.primary_phone_number AS phone_number,
							tz.time_zone_name
						FROM
							ar_payments ap
							JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ], NULL, ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? 'true' : 'false' ) . ' ) AS lp ON ( lp.cid = ap.cid AND lp.property_id = ap.property_id )
							JOIN cached_leases cl ON ( cl.cid = ap.cid AND cl.id = ap.lease_id )
							JOIN property_gl_settings pgs ON ( pgs.cid = ap.cid AND pgs.property_id = ap.property_id AND pgs.is_ar_migration_mode = false )
							JOIN payment_status_types pst ON ( pst.id = ap.payment_status_type_id AND pst.id = ' . CPaymentStatusType::AUTHORIZED . ' )
							JOIN payment_types pt ON ( pt.id = ap.payment_type_id )
							JOIN properties p ON ( p.cid = ap.cid AND p.id = ap.property_id )
							JOIN time_zones tz ON ( tz.id = p.time_zone_id )
							LEFT JOIN dashboard_priorities dp ON ( dp.cid = ap.cid )
						WHERE
							ap.cid = ' . ( int ) $intCid . '
							AND ap.captured_on IS NULL
					) a_payments
					WHERE
						cid = ' . ( int ) $intCid . '
						AND payment_amount > 0
						' . $strPriorityWhere . '
					ORDER BY
						' . $strSortBy . '
					OFFSET
						' . ( int ) $intOffset . '
					LIMIT
						' . ( int ) $intLimit;

		return fetchData( $strSql, $objClientDatabase );
	}

	// Dashboard Query

	public static function fetchCapturingArPaymentsCountByDashboardFilterByCid( $objDashboardFilter, $intCid, $objClientDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = '';
		$strSql = '';

		if( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) {
			$strPriorityWhere = ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )';
		}

		$strCreateTempTableSql = '
			DROP TABLE IF EXISTS ar_payments_authorized;
			CREATE TEMP TABLE ar_payments_authorized AS (
				SELECT
					ap.*
				FROM
					ar_payments ap
					JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ], NULL, ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? 'true' : 'false' ) . '  ) AS lp ON lp.property_id = ap.property_id
				WHERE
					ap.cid = ' . ( int ) $intCid . '
					AND ap.payment_status_type_id = ' . CPaymentStatusType::AUTHORIZED . '
					AND ap.captured_on IS NULL
			);';

		$strSql .= 'SELECT
						*
					FROM (
						SELECT
							ap.id,
							ap.cid,
							CASE
								WHEN TRIM( dp.financial_payments->>\'urgent_amount_greater_than\' ) != \'\' AND ap.payment_amount >= ( dp.financial_payments->>\'urgent_amount_greater_than\' )::int THEN 3
								WHEN TRIM( dp.financial_payments->>\'urgent_amount_less_than\' ) != \'\' AND ap.payment_amount < ( dp.financial_payments->>\'urgent_amount_less_than\' )::integer THEN 3
								WHEN TRIM( dp.financial_payments->>\'urgent_payment_submitted_since\' ) != \'\' AND get_date_difference( \'hour\', NOW(), ap.created_on ) >= ( dp.financial_payments->>\'urgent_payment_submitted_since\' )::int THEN 3
								WHEN TRIM ( dp.financial_payments->>\'urgent_payment_source_ids\' ) != \'\' AND ap.ps_product_id = ANY ( TRIM ( dp.financial_payments->>\'urgent_payment_source_ids\' )::integer[] ) THEN 3
								WHEN TRIM( dp.financial_payments->>\'important_amount_greater_than\' ) != \'\' AND ap.payment_amount >= ( dp.financial_payments->>\'important_amount_greater_than\' )::int THEN 2
								WHEN TRIM( dp.financial_payments->>\'important_amount_less_than\' ) != \'\' AND ap.payment_amount < ( dp.financial_payments->>\'important_amount_less_than\' )::integer THEN 2
								WHEN TRIM( dp.financial_payments->>\'important_payment_submitted_since\' ) != \'\' AND get_date_difference( \'hour\', NOW(), ap.created_on ) >= ( dp.financial_payments->>\'important_payment_submitted_since\' )::int THEN 2
								WHEN TRIM ( dp.financial_payments->>\'important_payment_source_ids\' ) != \'\' AND ap.ps_product_id = ANY ( TRIM ( dp.financial_payments->>\'important_payment_source_ids\' )::integer[] ) THEN 2
								ELSE 1
							END as priority,
							cl.property_id,
							p.property_name
						FROM
							ar_payments_authorized AS ap
							JOIN cached_leases cl ON ( cl.cid = ap.cid AND cl.id = ap.lease_id )
							JOIN properties p ON ( p.cid = cl.cid AND p.id = cl.property_id )
							JOIN property_gl_settings pgs ON ( pgs.cid = ap.cid AND pgs.property_id = ap.property_id AND pgs.is_ar_migration_mode = false )
							LEFT JOIN dashboard_priorities dp ON ( dp.cid = ap.cid )
						WHERE
							ap.cid = ' . ( int ) $intCid . '
					) a_payments
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere . '
					ORDER BY
						priority DESC ' .
					( false == $boolIsGroupByProperties ? '
						OFFSET
							0
						LIMIT
							100'
					: '' );

		if( false == $boolIsGroupByProperties ) {
			$strSql = ( ( 0 != $intMaxExecutionTimeOut )? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '' ) . 'SELECT COUNT(1) AS count, COALESCE( MAX( priority ), 1 ) as priority FROM ( ' . $strSql . ' ) as subQ';
			return fetchOrCacheData( $strCreateTempTableSql . $strSql, $intCacheLifetime = 900, NULL, $objClientDatabase );
		} else {
			$strSql = 'SELECT COUNT(1) AS approvals_payments, priority, property_name, property_id FROM ( ' . $strSql . ' ) as subQ GROUP BY property_name, property_id, priority';
			return fetchData( $strCreateTempTableSql . $strSql, $objClientDatabase );
		}

	}

	public static function fetchPaginatedARPaymentsByDashboardFilterByPaymentStatusTypeIdCid( $objDashboardFilter, $intPaymentStatusTypeId, $intOffset, $intLimit, $strSortBy, $intCid, $objClientDatabase ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = '';

		if( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) {
			$strPriorityWhere = ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )';
		}

		$strSql = 'SELECT
						*
					FROM (
						SELECT
							ap.id,
							ap.cid,
							func_format_customer_name( ap.billto_name_first, ap.billto_name_last ) as billto_name,
							abs( ap.payment_amount ) as payment_amount,
							pst.name AS payment_status_name,
							pt.name AS payment_type_name,
							lst.name AS lease_status_type_name,
							p.property_name,
							CASE
								WHEN TRIM ( dp.approvals_reversals->>\'urgent_amount_greater_than\' ) != \'\' AND abs( ap.payment_amount ) >= ( TRIM ( dp.approvals_reversals->>\'urgent_amount_greater_than\' ) ::FLOAT ) THEN
									3
								WHEN TRIM ( dp.approvals_reversals->>\'urgent_payment_type_ids\' ) != \'\' AND pt.id = ANY ( TRIM ( dp.approvals_reversals->>\'urgent_payment_type_ids\' )::integer[] ) THEN
									3
								WHEN TRIM ( dp.approvals_reversals->>\'important_amount_greater_than\' ) != \'\' AND abs( ap.payment_amount ) >= ( TRIM ( dp.approvals_reversals->>\'important_amount_greater_than\' ) ::FLOAT ) THEN
									2
								WHEN TRIM ( dp.approvals_reversals->>\'important_payment_type_ids\' ) != \'\' AND pt.id = ANY ( TRIM ( dp.approvals_reversals->>\'important_payment_type_ids\' )::integer[] ) THEN
									2
								ELSE
									1
							END as priority
					 	FROM
							ar_payments ap
							JOIN payment_status_types pst ON ( ap.payment_status_type_id = pst.id )
							JOIN payment_types pt ON ( ap.payment_type_id = pt.id )
							JOIN properties p ON ( ap.cid = p.cid AND ap.property_id = p.id AND p.is_disabled = 0 )
							JOIN property_gl_settings pgs ON( ap.cid = pgs.cid AND p.id = pgs.property_id AND pgs.is_ar_migration_mode = false )
							JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[' . implode( $objDashboardFilter->getPropertyGroupIds(), ',' ) . '] ) lp ON ap.property_id = lp.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ?  ' AND lp.is_disabled = 0 ' : '' ) . '
							LEFT JOIN lease_status_types lst ON ( ap.lease_status_type_id = lst.id )
							LEFT JOIN dashboard_priorities dp ON ( ap.cid = dp.cid )
						WHERE
							ap.cid = ' . ( int ) $intCid . '
							AND ap.payment_status_type_id = ' . ( int ) $intPaymentStatusTypeId . '
					) a_reversals
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere . '
					ORDER BY ' . $strSortBy;

		if( 0 < $intOffset ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset;
		}

		if( false == is_null( $intLimit ) ) {
			$strSql .= ' LIMIT ' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPaginatedArPaymentsCountByDashboardFilterByPaymentStatusTypeIdCid( $objDashboardFilter, $intPaymentStatusTypeId, $intCid, $objClientDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;
		if( false == isset( $intPaymentStatusTypeId ) ) return NULL;

		$strPriorityWhere = '';

		if( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) {
			$strPriorityWhere = ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )';
		}

		$strSql = ( 0 != $intMaxExecutionTimeOut )? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\';' : '';

		$strCreateTempTableSql = '
			DROP TABLE IF EXISTS ar_payments_authorized;
			CREATE TEMP TABLE ar_payments_authorized AS (
				SELECT
					ap.*
				FROM
					ar_payments ap
					JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ], NULL, ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? 'true' : 'false' ) . '  ) AS lp ON lp.property_id = ap.property_id
				WHERE
					ap.cid = ' . ( int ) $intCid . '
					AND ap.payment_status_type_id = ' . ( int ) $intPaymentStatusTypeId . '
					AND ap.block_association <> 1
			);';

		$strSql .= 'SELECT
						count( 1 ) ' .
						( false == $boolIsGroupByProperties ? ', MAX ' : ' AS approvals_payments, property_id, property_name, ' ) . '
						( priority ) AS priority
					FROM (
						SELECT
							ap.id,
							ap.cid, ' .
							( true == $boolIsGroupByProperties ? '
							ap.property_id,
							p.property_name,
							' : '' ) . '
							CASE
								WHEN TRIM ( dp.approvals_reversals->>\'urgent_amount_greater_than\' ) != \'\' AND abs( ap.payment_amount ) >= ( TRIM ( dp.approvals_reversals->>\'urgent_amount_greater_than\' ) ::FLOAT ) THEN
								3
								WHEN TRIM ( dp.approvals_reversals->>\'urgent_payment_type_ids\' ) != \'\' AND pt.id = ANY ( TRIM ( dp.approvals_reversals->>\'urgent_payment_type_ids\' )::integer[] ) THEN
									3
								WHEN TRIM ( dp.approvals_reversals->>\'important_amount_greater_than\' ) != \'\' AND abs( ap.payment_amount ) >= ( TRIM ( dp.approvals_reversals->>\'important_amount_greater_than\' ) ::FLOAT ) THEN
									2
								WHEN TRIM ( dp.approvals_reversals->>\'important_payment_type_ids\' ) != \'\' AND pt.id = ANY ( TRIM ( dp.approvals_reversals->>\'important_payment_type_ids\' )::integer[] ) THEN
									2
								ELSE
									1
							END as priority
						FROM
							ar_payments_authorized AS ap ' .
							( true == $boolIsGroupByProperties ? 'JOIN properties p ON ( p.cid = ap.cid AND p.id = ap.property_id ) ' : '' ) . '
							JOIN payment_types pt ON ( ap.payment_type_id = pt.id )
							LEFT JOIN dashboard_priorities dp ON ( ap.cid = dp.cid )
						WHERE
							ap.cid = ' . ( int ) $intCid . '
					) a_reversals
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere . ' ' .
					( true == $boolIsGroupByProperties ? '
					GROUP BY
						property_id,
						property_name,
						priority '
					: '' ) . '
					ORDER BY
						priority DESC ' .
					( false == $boolIsGroupByProperties ? '
					LIMIT 100' : '' );

		if( true == $boolIsGroupByProperties ) {
			return fetchData( $strCreateTempTableSql . $strSql, $objClientDatabase );
		}

		$arrintResponse = fetchOrCacheData( $strCreateTempTableSql . $strSql, $intCacheLifetime = 900, NULL, $objClientDatabase );
		if( true == valArr( $arrintResponse ) ) {
			return $arrintResponse[0];
		}

		return;
	}

	public static function updateArPaymentsIncentiveCompanyChargeAmountByIds( $intUserId, $arrintArPaymentIds, $fltIncentiveCompanyChargeAmount, $objPaymentDatabase, $objClientDatabase ) {
		$strSql = 'UPDATE
						ar_payments
					SET
						incentive_company_charge_amount = ' . $fltIncentiveCompanyChargeAmount . ',
						updated_by = ' . ( int ) $intUserId . ',
						updated_on = NOW()
					WHERE
						id IN ( ' . implode( ',', $arrintArPaymentIds ) . ' )';

		if( false == fetchData( $strSql, $objPaymentDatabase ) ) {
			return false;
		}
		if( false == fetchData( $strSql, $objClientDatabase ) ) {
			return false;
		}

		return true;
	}

	public static function fetchCheckNumbersByArPaymentIdsByCid( $arrintArPaymentIds, $intCid, $objDatabase ) {

		$arrintArPaymentIds = array_filter( ( array ) $arrintArPaymentIds );
		if( false == valArr( $arrintArPaymentIds ) ) return;

		$strSql = 'SELECT
						ap.id AS ar_payment_id,
						ap.check_number
					FROM
						ar_payments ap
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.id IN ( ' . implode( ',', $arrintArPaymentIds ) . ' )';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchSimpleArPaymentsByArPaymentIdsByCid( $arrintArPaymentIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintArPaymentIds ) ) return;

		$strSql = 'SELECT
						ap.id,
						ap.check_number,
						ap.property_id,
						ap.lease_id,
						ap.payment_type_id,
						ap.billto_unit_number,
						ap.total_amount,
						ap.payment_amount,
						ap.donation_amount,
						ap.convenience_fee_amount,
						ap.payment_status_type_id,
						ap.payment_datetime,
						ap.created_on,
						ma.merchant_name
					FROM
						ar_payments ap
						JOIN merchant_accounts ma ON ( ap.company_merchant_account_id = ma.id AND ap.cid = ma.cid )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.id IN ( ' . implode( ',', $arrintArPaymentIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchArPaymentsWithDelayCaptureOnApplicationApprovalByLeaseIdByCid( $intLeaseId, $intCid, $objClientDatabase, $objPaymentDatabase ) {

		$strSql = '	SELECT ap.id
					FROM ar_payments ap
						JOIN ar_codes ac ON ( ap.cid = ac.cid AND ap.ar_code_id = ac.id AND ac.cid = ' . ( int ) $intCid . ' )
						JOIN property_preferences pp ON ( pp.cid = ap.cid AND pp.property_id = ap.property_id AND pp.cid = ' . ( int ) $intCid . ' AND pp.key = \'CAPTURE_ON_APPLICATION_APPROVAL\' AND pp.value = \'1\' )
					WHERE ap.cid = ' . ( int ) $intCid . '
						AND ap.lease_id = ' . ( int ) $intLeaseId . '
						AND ap.payment_status_type_id = ' . ( int ) CPaymentStatusType::AUTHORIZED . '
						AND ac.capture_on_application_approval = true';

		$arrmixData = fetchData( $strSql, $objClientDatabase );

		if( false == valArr( $arrmixData ) )		return NULL;

		$arrintArPaymentIds = [];
		foreach( $arrmixData as $arrintMixData ) {
			$arrintArPaymentIds[] = $arrintMixData['id'];
		}

		$strSql = 'SELECT * FROM view_ar_payments WHERE cid = ' . ( int ) $intCid . ' AND lease_id = ' . ( int ) $intLeaseId . ' AND id IN ( ' . implode( ',', $arrintArPaymentIds ) . ' )';
		return self::fetchArPayments( $strSql, $objPaymentDatabase );
	}

	public static function fetchArPaymentStatusByArPaymentIdByCid( $intArPaymentId, $intCid, $objDatabase ) {

		$strSql = 'SELECT payment_status_type_id FROM ar_payments WHERE id = ' . ( int ) $intArPaymentId . ' AND cid = ' . ( int ) $intCid;

		$arrmixResult = fetchData( $strSql, $objDatabase );

		if( true == is_numeric( $arrmixResult[0]['payment_status_type_id'] ) ) {
			return $arrmixResult[0]['payment_status_type_id'];
		} else {
			return false;
		}
	}

	public static function fetchArPaymentsByCustomerPaymentAccountIdsByPropertyIdsByCid( $arrintCustomerPaymentAccountIds, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false === valArr( $arrintCustomerPaymentAccountIds ) || false === valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT cid, customer_payment_account_id, max( created_on ) AS created_on FROM ar_payments WHERE cid = ' . ( int ) $intCid . ' AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )  AND customer_payment_account_id IN ( ' . implode( ',', $arrintCustomerPaymentAccountIds ) . ' ) GROUP BY cid, customer_payment_account_id, property_id';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchArPaymentsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase, $boolIsFromView = false ) {

		if( false == valId( $intLeaseId ) ) return NULL;

		$strTableName = ( true == $boolIsFromView ) ? 'view_ar_payments' : 'ar_payments';

		$strSql = 'SELECT
						*
					FROM '
					. $strTableName .
					' WHERE
						payment_type_id IN ( ' . implode( ',', CPaymentType::$c_arrintElectronicPaymentTypeIdsWithoutCheck21 ) . ' )
						AND	payment_status_type_id = ' . CPaymentStatusType::CAPTURED . '
						AND batched_on IS NOT NULL
						AND is_reversed = 0
						AND lease_id = ' . ( int ) $intLeaseId . '
						AND customer_id IS NOT NULL
						AND cid = ' . ( int ) $intCid;

		return self::fetchArPayments( $strSql, $objDatabase );
	}

	public static function fetchAssociatedNegativeArPaymentByPaymentStatusTypeIdByArPaymentIdByCid( $intPaymentStatusTypeId, $intArPaymentId, $intCid, $objPaymentDatabase ) {
		return self::fetchArPayment( 'SELECT * FROM view_ar_payments WHERE payment_status_type_id = ' . ( int ) $intPaymentStatusTypeId . ' AND payment_amount < 0 AND ar_payment_id = ' . ( int ) $intArPaymentId . ' AND cid = ' . ( int ) $intCid,  $objPaymentDatabase );
	}

	public static function checkIsReturnArPaymentByCustomerIdByLeaseIdByPropertyIdByCidByPaymentStatusType( $intCustomerId, $arrintLeaseIds, $arrintPropertyIds, $intCid, $arrintPaymentStatusTypeIds, $objDatabase ) {

		if( false == valArr( array_filter( $arrintPropertyIds ) ) ) return NULL;

		$strSql = 'SELECT
						count( ap.id ) as ap_count
					FROM
						ar_payments ap
					WHERE
						ap.customer_id = ' . ( int ) $intCustomerId . '
						AND ap.cid = ' . ( int ) $intCid . '
						AND ap.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND ap.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ap.payment_status_type_id IN ( ' . implode( ',', $arrintPaymentStatusTypeIds ) . ' )
						AND ap.returned_on IS NOT NULL
						AND ap.returned_on >= ( date_trunc ( \'day\', NOW ( ) ) - INTERVAL \'1 MONTH\' )';

		$arrintCount = fetchData( $strSql, $objDatabase );
		return ( true == isset ( $arrintCount[0]['ap_count'] ) && 0 < $arrintCount[0]['ap_count'] ) ? true : false;

	}

	/**
	 * @param $intArPaymentId
	 * @param $intCid
	 * @param $objPaymentDatabase
	 * @return null|int
	 */
	public static function fetchTransferredArPaymentIdByPaymentIdByCid( $intArPaymentId, $intCId,  $objPaymentDatabase ) {

		$strSql = 'SELECT id, COUNT ( ar_payment_id ) FROM ar_payments WHERE ar_payment_id = ' . ( int ) $intArPaymentId . ' AND cid = ' . ( int ) $intCId . ' AND payment_status_type_id = ' . CPaymentStatusType::TRANSFERRED . ' GROUP BY id, ar_payment_id';
		$arrmixArPaymentData = fetchData( $strSql, $objPaymentDatabase );
		$intPaymentId = NULL;
		if( 0 < $arrmixArPaymentData[0]['count'] ) {
			$intPaymentId = CArPayments::fetchArPaymentIdByArPaymentIdByCid( $arrmixArPaymentData[0]['id'], $intCId, $objPaymentDatabase );
		}

		return $intPaymentId;
	}

	/**
	 * @param $intArPaymentId
	 * @param $intCid
	 * @param $objPaymentDatabase
	 * @return null|int
	 */
	public static function fetchArPaymentIdByArPaymentIdByCid( $intArPaymentId, $intCid, $objPaymentDatabase ) {

		$strSql = 'SELECT id FROM ar_payments WHERE ar_payment_id = ' . ( int ) $intArPaymentId . ' AND cid = ' . ( int ) $intCid;
		$arrmixResult = fetchData( $strSql, $objPaymentDatabase );

		return ( true == is_numeric( $arrmixResult[0]['id'] ) ) ? $arrmixResult[0]['id'] : NULL;
	}

	public function fetchLitleUnsettledArPayments( $objDatabase ) {

		$strSql = 'SELECT
						ap.id as ar_payment_id,
						ap.payment_type_id,
						ap.cid,
						ap.captured_on,
						ap.company_merchant_account_id,
						ap.payment_status_type_id,
						ap.payment_amount, ap.convenience_fee_amount, ap.total_amount,
						ap.company_charge_amount
					FROM
						ar_payments ap
					INNER JOIN clients c ON c.id = ap.cid AND c.company_status_type_id in ( ' . CCompanyStatusType::CLIENT . ', ' . CCompanyStatusType::TERMINATED . '  )
					LEFT JOIN batched_ar_payments bap ON bap.ar_payment_id = ap.id
					WHERE
						ap.captured_on < ( NOW() - INTERVAL \'2 days\' )
						AND ap.processing_bank_account_id = ' . CProcessingBankAccount::ZIONS_CREDIT_CARD_LITLE . '
						AND ap.payment_status_type_id = ' . CPaymentStatusType::CAPTURED . '
						AND ap.ar_payment_id IS NULL
						AND bap.id IS NULL
					ORDER BY captured_on DESC';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchArPaymentById( $intId, $objDatabase ) {
		return self::fetchArPayment( sprintf( 'SELECT * FROM view_ar_payments WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchLeaseCustomersForArPayments( $arrstrLeaseIdCustomerId, $objDatabase ) {
		if( false == valArr( $arrstrLeaseIdCustomerId ) ) return NULL;

		$strSql = 'SELECT lease_id, customer_id FROM lease_customers WHERE ( lease_id,customer_id) IN (' . implode( ',', $arrstrLeaseIdCustomerId ) . ')';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDuplicatePaymentsWithInHours( $intPropertyId, $intCustomerId, $intLeaseId, $intCid, $fltPaymentAmount, $objClientDatabase, $intHours = 48 ) {

		$strSql = 'SELECT * FROM ar_payments
					WHERE property_id = ' . ( int ) $intPropertyId . '
						AND customer_id = ' . ( int ) $intCustomerId . '
						AND payment_amount = ' . ( float ) $fltPaymentAmount . '
						AND lease_id		= ' . ( int ) $intLeaseId . '
						AND cid = ' . ( int ) $intCid . '
						AND ( payment_status_type_id = ' . CPaymentStatusType::AUTHORIZED . ' OR payment_status_type_id = ' . CPaymentStatusType::CAPTURED . ')
						AND created_on >= ( NOW() - INTERVAL \'' . $intHours . ' hours\' )';

		return CArPayments::fetchArPayments( $strSql, $objClientDatabase );
	}

	public static function fetchArPaymentsCountByPaymentStatusTypeIdByPaymentTypesByCid( $intPaymentStatusTypeId, $arrobjPaymentTypes, $intCid, $objPaymentDatabase ) {

		if( false == valId( $intPaymentStatusTypeId ) || false == valArr( $arrobjPaymentTypes ) ) return NULL;

		$strSql = 'SELECT
							count(vap.id)
		               FROM
		               		view_ar_payments vap
		               WHERE
		               		vap.cid = ' . ( int ) $intCid . '
		               		AND vap.payment_status_type_id = ' . ( int ) $intPaymentStatusTypeId . '
		               		AND vap.payment_type_id IN ( ' . implode( ',', array_keys( $arrobjPaymentTypes ) ) . ' )
		               		AND vap.payment_amount > 0
		               		AND vap.auto_capture_on <= NOW()';

		$arrmixData = fetchData( $strSql, $objPaymentDatabase );

		if( true == valArr( $arrmixData ) && true == isset ( $arrmixData[0]['count'] ) ) {
			return $arrmixData[0]['count'];
		} else {
			return 0;
		}
	}

	public static function fecthCustomPaymentsByUpdatedOnByPaymentIdsByCid( $intCid, $objDatabase, $strStartDate = NULL, $strEndDate = NULL, $arrintArPaymentIds = NULL ) {

		$strSqlWhere = '';

		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			return false;
		}

		if( CDatabaseType::PAYMENT == $objDatabase->getDataBaseTypeId() ) {
			if( true == valStr( $strEndDate ) ) {
				$strSqlWhere .= ' ap.updated_on BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'';
			} else {
				$strSqlWhere .= ' ap.updated_on >= DATE_TRUNC( \'DAY\', \'' . $strStartDate . '\'::DATE - INTERVAL \'1 DAYS\' )
							 AND ap.updated_on < DATE_TRUNC( \'DAY\', \'' . $strStartDate . '\'::DATE );';
			}
		} elseif( true == valArr( $arrintArPaymentIds ) ) {
			$strSqlWhere .= ' ap.id IN ( ' . implode( ',', $arrintArPaymentIds ) . ' );';
		}

		$strSql = 'SELECT
						ap.id,
						ap.customer_id,
						ap.captured_on,
						ap.batched_on,
						ap.returned_on,
						ap.distribute_on,
						ap.distribute_return_on,
						ap.return_type_id,
						ap.payment_status_type_id,
						ap.is_split
					FROM
						ar_payments ap
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ' . $strSqlWhere;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCcEncryptedAchLastFourDigitEncryptedByPaymentId( $arrintPaymentIds, $objPaymentDatabase ) {
		if( false == valArr( $arrintPaymentIds ) ) return NULL;

		$strSql = '	SELECT 
						ar_payment_id,
						cc_card_number_encrypted, 
						check_account_number_lastfour_encrypted,
						check_account_number_encrypted
					FROM
						ar_payment_billings 
					WHERE
						ar_payment_id IN ( ' . implode( ',', $arrintPaymentIds ) . ' )';

		return rekeyArray( 'ar_payment_id', fetchData( $strSql, $objPaymentDatabase ) );
	}

	public static function fetchArPaymentsByArPaymentIds( $arrintArPaymentId, $intCid, $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM ar_payments WHERE id IN (' . implode( ',', ( $arrintArPaymentId ) ) . ') AND cid = ' . ( int ) $intCid;
		return fetchData( $strSql, $objPaymentDatabase );
	}

	public static function fecthCustomArPaymentsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $arrintArPaymentIds = [], $arrintPaymentStatusTypeIds = [], $intPaymentTypeId = NULL, $intLeaseId = NULL, $intCustomerId = NULL, $strPaymentDateFrom = '', $strPaymentDateTo = '', $intStoredBillingId = NULL ) {

		$strSql = 'SELECT
						ap.id,
						ap.billto_name_first,
						ap.billto_name_last,
						ap.billto_unit_number,
						ap.billto_street_line1,
						ap.billto_street_line2,
						ap.billto_street_line3,
						ap.billto_city,
						ap.billto_state_code,
						ap.billto_postal_code,
						ap.billto_phone_number,
						ap.billto_email_address,
						ap.payment_status_type_id,
						ap.payment_datetime::date,
						ap.payment_amount,
						ap.convenience_fee_amount,
						ap.total_amount,
						ap.captured_on::date,
						ap.returned_on::date,
						ap.distribute_on::date,
						ap.customer_id,
						ap.lease_id,
						ap.property_id,
						ap.customer_payment_account_id,
						ap.scheduled_payment_id,
						ap.check_account_number_encrypted,
						ap.cc_card_number_encrypted,
						pst.name as payment_status_type,
						pt.name as payment_type
					FROM
						view_ar_payments ap
						JOIN payment_status_types pst ON ( ap.payment_status_type_id = pst.id )
						JOIN payment_types pt ON ( ap.payment_type_id = pt.id )
					WHERE
						ap.property_id = ' . ( int ) $intPropertyId . '
						AND ap.cid = ' . ( int ) $intCid;

		if( false != valArr( $arrintArPaymentIds ) ) {
			$strSql .= ' AND ap.id IN (' . implode( ',', $arrintArPaymentIds ) . ')';
		}
		if( false != valArr( $arrintPaymentStatusTypeIds ) ) {
			$strSql .= ' AND ap.payment_status_type_id IN (' . implode( ',', $arrintPaymentStatusTypeIds ) . ')';
		}
		if( false == is_null( $intPaymentTypeId ) ) {
			$strSql .= ' AND payment_type_id = ' . ( int ) $intPaymentTypeId;
		}
		if( false == is_null( $intLeaseId ) ) {
			$strSql .= ' AND ap.lease_id = ' . ( int ) $intLeaseId;
		}
		if( false == is_null( $intCustomerId ) ) {
			$strSql .= ' AND ap.customer_id = ' . ( int ) $intCustomerId;
		}
		if( false != valStr( $strPaymentDateFrom ) && false != valStr( $strPaymentDateTo ) ) {
			$strSql .= ' AND ap.payment_datetime BETWEEN \'' . $strPaymentDateFrom . ' 00:00:00\' AND \'' . $strPaymentDateTo . ' 23:59:59\'';
		}
		if( false == is_null( $intStoredBillingId ) ) {
			$strSql .= ' AND ap.customer_payment_account_id = ' . ( int ) $intStoredBillingId;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaymentCostByPaymentStatusTypeIdByPaymentDateTimeByCids( $intPaymentStatusTypeId, $strStartMonth, $arrintClientIds, $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM ( SELECT
										ap.cid AS cid,
										DATE_TRUNC ( \'month\', ap.payment_datetime ) AS month,
										COALESCE( sum( ap.payment_cost ), 0 ) as cost
									FROM
										ar_payments ap
									WHERE
										ap.payment_datetime > \'' . $strStartMonth . '\'
										AND ap.cid IN ( ' . implode( ',', $arrintClientIds ) . ' )
										AND ap.payment_status_type_id = ' . ( int ) $intPaymentStatusTypeId . '
									GROUP BY
										ap.cid,
										DATE_TRUNC ( \'month\', ap.payment_datetime ) ) as subquery1
							ORDER BY cid, month ASC';

		return fetchData( $strSql, $objPaymentDatabase );
	}

	public static function fetchPaymentCostByPaymentDateTimeByCidsByPaymentStatusType( $strStartMonth, $arrintClientIds, $intPaymentStatusTypeId, $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM ( SELECT
										ap.cid AS cid,
										DATE_TRUNC ( \'month\', ap.payment_datetime ) AS month,
										COALESCE( sum( ap.payment_cost ), 0 ) as cost
									FROM
										ar_payments ap,
										clients c
									WHERE
										ap.cid = c.id
										AND ap.payment_datetime > \'' . $strStartMonth . '\'
										AND ap.cid IN ( ' . implode( ',', $arrintClientIds ) . ' )
										AND ap.payment_status_type_id = ' . ( int ) $intPaymentStatusTypeId . '
									GROUP BY
										ap.cid,
										DATE_TRUNC ( \'month\', ap.payment_datetime ) ) as subquery1
							ORDER BY cid, month ASC';

		return fetchData( $strSql, $objPaymentDatabase );
	}

	public static function fetchArPaymentsByCustomerIdsByCidByCreatedOnAfterDate( $arrintCustomerIds, $intCid, $strCreatedOn, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) || false == valStr( $strCreatedOn ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ar_payments
					WHERE
						cid = ' . ( int ) $intCid . ' AND
						customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' ) AND
						created_on >= \'' . $strCreatedOn . '\'';

		return self::fetchArPayments( $strSql, $objDatabase );
	}

	public static function fetchDeclinedPaymentsByEntrataUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ar_payments ap
					WHERE
						ap.payment_status_type_id = ' . CPaymentStatusType::DECLINED . '
						AND ap.created_by = ' . ( int ) $intCompanyUserId . '
						AND ap.cid = ' . ( int ) $intCid . '
						AND ap.payment_datetime > ( NOW() - INTERVAL \'24 hours\' )
					ORDER BY
						payment_datetime DESC';

		return self::fetchArPayments( $strSql, $objDatabase );
	}

	public static function fetchAssociatedArPaymentsByArTransactionIdsByLeaseIdByPropertyIdByCid( $arrintArTransactionIds, $intLeaseId, $intPropertyId, $intCid, $objDatabase, $boolIsReturnKeyedArray = true ) {

		if( false == valArr( $arrintArTransactionIds ) || false == valId( $intLeaseId ) || false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					 ap.*,
					 at.id as ar_transaction_id
					FROM
						ar_payments ap
						JOIN ar_transactions at ON ( ap.id = at.ar_payment_id AND ap.cid = at.cid )
					WHERE
						at.lease_id = ' . ( int ) $intLeaseId . '
						AND at.property_id = ' . ( int ) $intPropertyId . '
						AND at.cid = ' . ( int ) $intCid . '
						AND at.id IN ( ' . implode( ',', $arrintArTransactionIds ) . ' )
						AND at.ar_payment_id IS NOT NULL';

		return self::fetchArPayments( $strSql, $objDatabase, $boolIsReturnKeyedArray );
	}

	public function fetchCustomArPaymentByIdsByCid( $intArPaymentIds, $intCid, $objDatabase ) {

		if( false == \Psi\Libraries\UtilFunctions\valArr( $intArPaymentIds ) || false == \Psi\Libraries\UtilFunctions\valId( $intCid ) ) {
			return NULL;
		}

		$strArPaymentsTable = ( CDatabaseType::CLIENT == $objDatabase->getDatabaseTypeId() ) ? 'ar_payments' : 'view_ar_payments';
		$strSql = 'SELECT * FROM ' . $strArPaymentsTable . ' WHERE cid = ' . ( int ) $intCid;
		if( 1 == \Psi\Libraries\UtilFunctions\count( $intArPaymentIds ) ) {
			$strSql .= ' AND id = ' . $intArPaymentIds[0];
		} else {
			$strSql .= ' AND id IN ( ' . implode( ',', $intArPaymentIds ) . ' )';
		}

		return self::fetchArPayments( $strSql, $objDatabase );
	}

}
?>
