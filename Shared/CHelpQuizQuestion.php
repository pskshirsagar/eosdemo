<?php

class CHelpQuizQuestion extends CBaseHelpQuizQuestion {

	const MINIMUM_HELP_QUIZ_QUESTIONS                 = 2;
	const MAXIMUM_HELP_QUIZ_QUESTION                  = 25;
	const MINIMUM_HELP_QUIZ_QUESTION_ANSWERS          = 2;
	const MAXIMUM_HELP_QUIZ_QUESTION_ANSWERS          = 5;
	const QUESTION_OPTIONS                            = 'Option';
	const QUESTION_MULTI_CHOICE                       = 'Multi Choice';
	const SURVEY_QUESTION_SHORT                       = 'Short';
	const SURVEY_QUESTION_SCALE                       = 'Scale';
	const MAXIMUM_CUSTOM_COURSE_QUIZ_QUESTION_ANSWERS = 4;
	const MAXIMUM_TEST_OUT_QUIZ_QUESTION_ANSWERS = 4;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHelpQuizId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valParentQuestionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valQuestion() {
		$boolIsValid = true;

		if( false == valStr( $this->getQuestion() ) || true == preg_match( '/<[^>]*>/', $this->getQuestion() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'question', 'Question is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsPublished() {
	$boolIsValid = true;
	return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRequiresSync() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valQuestion();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>