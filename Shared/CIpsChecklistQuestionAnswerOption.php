<?php

class CIpsChecklistQuestionAnswerOption extends CBaseIpsChecklistQuestionAnswerOption {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIpsChecklistQuestionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAnswer() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAssociatedQuestionIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>