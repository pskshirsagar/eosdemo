<?php

class CScreeningPackagesProperty extends CBaseScreeningPackagesProperty {

	protected $m_intLeaseTypeId;
	protected $m_intCustomerTypeId;
	protected $m_intCustomerRelationshipId;
	protected $m_intScreeningApplicantRequirementTypeId;

    public function setLeaseTypeId( $intLeaseTypeId ) {
    	$this->m_intLeaseTypeId = $intLeaseTypeId;
    }

    public function setCustomerTypeId( $intCustomerTypeId ) {
    	$this->m_intCustomerTypeId = $intCustomerTypeId;
    }

    public function setCustomerRelationshipId( $intCustomerRelationshipId ) {
    	$this->m_intCustomerRelationshipId = $intCustomerRelationshipId;
    }

     public function setScreeningApplicantRequirementTypeId( $intScreeningApplicantRequirementTypeId ) {
    	$this->m_intScreeningApplicantRequirementTypeId = $intScreeningApplicantRequirementTypeId;
     }

    public function getLeaseTypeId() {
    	return $this->m_intLeaseTypeId;
    }

    public function getCustomerTypeId() {
    	return $this->m_intCustomerTypeId;
    }

    public function getCustomerRelationshipId() {
    	return $this->m_intCustomerRelationshipId;
    }

 	public function getScreeningApplicantRequirementTypeId() {
    	return $this->m_intScreeningApplicantRequirementTypeId;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScreeningPackageId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsActive() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

    	parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

    	if( isset( $arrmixValues['lease_type_id'] ) )				$this->setLeaseTypeId( $arrmixValues['lease_type_id'] );
    	if( isset( $arrmixValues['customer_type_id'] ) ) 			$this->setCustomerTypeId( $arrmixValues['customer_type_id'] );
    	if( isset( $arrmixValues['customer_relationship_id'] ) ) 	$this->setCustomerRelationshipId( $arrmixValues['customer_relationship_id'] );
    	if( isset( $arrmixValues['screening_applicant_requirement_type_id'] ) ) 	$this->setScreeningApplicantRequirementTypeId( $arrmixValues['screening_applicant_requirement_type_id'] );

    	return;
    }

    public function archivePropertyScreeningPackages( $intCid, $intPropertyId, $intUserId, $objDatabase ) {

    	$boolIsValid = true;

    	$strSql = 'UPDATE
				       screening_packages_properties
				   SET
				       is_active = 0,
				       updated_by = ' . ( int ) $intUserId . ',
				       updated_on = NOW()
				   WHERE
                       cid = ' . ( int ) $intCid . '
				       AND property_id = ' . ( int ) $intPropertyId;

    	$objDatabase->begin();

		$boolIsValid = fetchData( $strSql, $objDatabase );
		if( false == $boolIsValid ) $objDatabase->rollback();

    	$objDatabase->commit();

    	return $boolIsValid;
    }

	public function generateInsertScreeningSql( $intCurrentUserId ) {
		$strSql    = 'INSERT INTO screening_packages_properties ( "cid", "property_id", "screening_package_id", "is_active", "updated_by", "updated_on", "created_by", "created_on")
						VALUES';
		$strSql .= '( ' . $this->getCid() . ', ' . $this->getPropertyId() . ', ' . $this->getScreeningPackageId() . ', ' . $this->getIsActive() . ',' . ( int ) $intCurrentUserId . ', now(), ' . ( int ) $intCurrentUserId . ',now() );';
		return $strSql;
	}

	public function generateArchiveScreeningSql( $intCurrentUserId ) {
		$strSql = 'UPDATE screening_packages_properties
					SET
					    is_active = 0,
					    updated_by = ' . ( int ) $intCurrentUserId . ',
					    updated_on = NOW ( )
					WHERE
					    screening_package_id = ' . ( int ) $this->getScreeningPackageId() . '
					    AND is_active = 1;';

		return $strSql;
	}

}
?>