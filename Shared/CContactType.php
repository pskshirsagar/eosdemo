<?php

class CContactType extends CBaseContactType {

	const PRIMARY 			= 1;
	const MANAGER 			= 2;
	const OWNER   			= 3;
	const MAINTENANCE 		= 4;
	const BILLING 			= 5;
	const IMPLEMENTATION 	= 6;
}
?>