<?php

class COwnerType extends CBaseOwnerType {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultOwnerTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Entity Type Name is required.' ) ) );
			$boolIsValid = false;
		}

		if( true == $boolIsValid ) {

			$strWhere = ' WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND LOWER( name ) = LOWER( \'' . addslashes( $this->getName() ) . '\' )
							AND id <> ' . ( int ) $this->getId() . ' ';

			if( 0 < \Psi\Eos\Entrata\COwnerTypes::createService()->fetchRowCount( $strWhere, 'owner_types', $objDatabase ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( "Entity type name '{%s, 0}' already exists.", [ $this->getName() ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsEnabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDelete( $boolIsSystem ) {
		$boolIsValid = true;

		if( 't' == $boolIsSystem ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'System entity types cannot be deleted. Only entity types added by users can be deleted.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid = $this->valName( $objDatabase );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				$boolIsValid = $this->valDelete( $this->getIsSystem() );
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
	}

	public function setName( $strName ) {
		$this->m_strName = $strName;
	}

	public function setIsSystem( $boolIsSystem ) {
		$this->m_boolIsSystem = $boolIsSystem;
	}

	public function setIsEnabled( $boolIsEnabled ) {
		$this->m_boolIsEnabled = $boolIsEnabled;
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function sqlCreatedOn() {

		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	/**
	 * Get Functions
	 */

	public function getName() {
		return $this->m_strName;
	}

	public function getIsSystem() {
		return $this->m_boolIsSystem;
	}

	public function getIsEnabled() {
		return $this->m_boolIsEnabled;
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public static function getOwnerTypeToString() {

		return [
			self::LLC					=> 'LLC',
			self::CLIENT				=> 'MC',
			self::TEMPORARY				=> 'Temporary',
			self::C_CORPORATION			=> 'C Corporation',
			self::S_CORPORATION			=> 'S Corporation',
			self::PARTNERSHIP			=> 'Partnership',
			self::SOLE_PROPRIETORSHIP	=> 'Sole Proprietorship',
			self::HUD_OWNED				=> 'HUD'
		];
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();
		$strNextOrderNum = '( SELECT MAX(order_num) FROM ' . static::TABLE_NAME . ' ) + 1';

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id,cid, name, description, is_enabled,updated_by,updated_on,created_by,created_on,order_num )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlName() . ', ' .
		          $this->sqlName() . ', ' .
		          $this->sqlIsEnabled() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $strNextOrderNum . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) {
							$strSql .= ' name = ' . $this->sqlName() . ',';
						} elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) {
							$strSql .= ' name = ' . $this->sqlName() . ',';
							$boolUpdate = true;
						}
						if( false == $this->getAllowDifferentialUpdate() ) {
							$strSql .= ' is_enabled = ' . $this->sqlIsEnabled() . ',';
						} elseif( true == array_key_exists( 'IsEnabled', $this->getChangedColumns() ) ) {
							$strSql .= ' is_enabled = ' . $this->sqlIsEnabled() . ',';
							$boolUpdate = true;
						}
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
									WHERE
										id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

}
?>