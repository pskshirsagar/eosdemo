<?php

class CPaymentImageType extends CBasePaymentImageType {

	const FRONT 		= 1;
	const REVERSE 		= 2;
	const SIGNATURE 	= 3;

}
?>