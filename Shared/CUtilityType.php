<?php

class CUtilityType extends CBaseUtilityType {

	const WATER  				= 1;
	const GAS  					= 2;
	const ELECTRIC  			= 3;
	const SEWER					= 4;
	const CUSTOM 				= 5;
	const TRASH  				= 6;
	const PHONE  				= 7;
	const INTERNET  			= 8;
	const CABLE  				= 9;
	const LAUNDRY_SERVICES  	= 10;
	const HOT_WATER  			= 11;
	const COLD_WATER  			= 12;
	const CONVERGENT_BILLING	= 13;
	const WATER_WASTE_WATER		= 14;
	const HOT_WATER_ENERGY		= 15;
	const PEST_CONTROL			= 16;
	const STORMWATER           	= 17;
	const EMS_FEE             	= 18;
	const HVAC              	= 19;
	const VCR_ELECTRIC			= 20;
	const VCR_GAS				= 21;
	const RECYCLING				= 22;
	const TOWING				= 23;
	const LEASING				= 24;
	const OTHER					= 99;
	const BOILER				= 25;

	const CUSTOM2					= 26;
	const CUSTOM3					= 27;
	const CUSTOM4					= 28;
	const CUSTOM5					= 29;
	const CUSTOM6					= 30;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

	public static $c_arrintClientDetailUtilityTypes	= [
		self::GAS,
		self::WATER,
		self::SEWER,
		self::TRASH,
		self::ELECTRIC,
		self::PEST_CONTROL,
		self::HOT_WATER_ENERGY
	];

	public static function getClientDetailUtilityTypes() {
		$arrmixUtilityTypes = [
			self::GAS				=> 'Gas',
			self::WATER				=> 'Water',
			self::SEWER				=> 'Sewer',
			self::TRASH				=> 'Solid Waste',
			self::ELECTRIC			=> 'Electric',
			self::PEST_CONTROL		=> 'Pest Control',
			self::HOT_WATER_ENERGY	=> 'Hot Water Energy'
		];

		return $arrmixUtilityTypes;
	}

	public static $c_arrintNCStateUtilityTypes	= [
		self::WATER,
		self::COLD_WATER,
		self::HOT_WATER
	];

	public static $c_arrintNJStateUtilityTypes	= [
		self::GAS,
		self::ELECTRIC
	];

	public static $c_arrintDefaultUtilityTypes	= [
		self::WATER,
		self::ELECTRIC,
		self::SEWER,
		self::TRASH,
		self::GAS
	];

	public static $c_arrintWaterRelatedUtilityTypes	= [
		self::WATER,
		self::HOT_WATER,
		self::COLD_WATER,
		self::STORMWATER,
	];

	public static $c_arrintBaseFeeUtilityTypes	= [
		self::WATER,
		self::GAS,
		self::SEWER,
		self::COLD_WATER,
		self::CONVERGENT_BILLING,
		self::HOT_WATER,
		self::TRASH,
		self::ELECTRIC,
		self::STORMWATER
	];

	public static $c_arrintCustomUtilityTypes	= [
		self::CUSTOM,
		self::CUSTOM2,
		self::CUSTOM3,
		self::CUSTOM4,
		self::CUSTOM5,
		self::CUSTOM6
	];
}
?>