<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPaymentTransactionTypes
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CPaymentTransactionTypes extends CBasePaymentTransactionTypes {

	public static function fetchPaymentTransactionTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CPaymentTransactionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPaymentTransactionType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CPaymentTransactionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllPaymentTransactionTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM payment_transaction_types';
		return self::fetchPaymentTransactionTypes( $strSql, $objDatabase );
	}

	public static function fetchSimpleAllPaymentTransactionTypes( $objDatabase ) {
		$strSql = 'SELECT id, name FROM payment_transaction_types';
		$arrmixResultData = fetchData( $strSql, $objDatabase );

		return $arrmixResultData;
	}

}
?>