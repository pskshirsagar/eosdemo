<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CPropertyScreeningConfigurations
 * Do not add any new functions to this class.
 */

class CPropertyScreeningConfigurations extends CBasePropertyScreeningConfigurations {

	public static function fetchPropertyScreeningConfigurationsByScreeningConfigIdByPropertyIdsByCid( $intScreeningConfigId, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						property_screening_configurations
					WHERE
						is_published = 1
						AND screening_configuration_id = ' . ( int ) $intScreeningConfigId . '
						AND property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return self::fetchPropertyScreeningConfigurations( $strSql, $objDatabase );
	}

	public static function fetchPropertyScreeningConfigurationByScreeningConfigurationIdByPropertyIdByCid( $intScreeningConfigId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						property_screening_configurations
					WHERE
						is_published = 1
						AND screening_configuration_id = ' . ( int ) $intScreeningConfigId . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchPropertyScreeningConfiguration( $strSql, $objDatabase );
	}

 	public static function fetchPropertyScreeningConfigurationByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						property_screening_configurations
					WHERE
						is_published = 1
						AND cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
					ORDER BY
						id DESC
					LIMIT
						1 OFFSET 0';

		return self::fetchPropertyScreeningConfiguration( $strSql, $objDatabase );
	}

}
?>