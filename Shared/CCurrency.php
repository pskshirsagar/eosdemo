<?php

class CCurrency extends CBaseCurrency {

	const SYMBOL_USD = '$';
	const SYMBOL_CAD = '$';
	const SYMBOL_AUD = '$';
	const SYMBOL_CLP = '$';
	const SYMBOL_MXN = '$';
	const SYMBOL_CNY = '¥';
	const SYMBOL_EUR = '€';
	const SYMBOL_CLF = 'UF';
	const SYMBOL_JPY = '¥';
	const SYMBOL_GBP = '£';
	const SYMBOL_BHD = 'BD';

	const CURRENCY_CODE_USD = 'USD';
	const CURRENCY_CODE_CAD = 'CAD';
	const CURRENCY_CODE_AUD = 'AUD';
	const CURRENCY_CODE_CLP = 'CLP';
	const CURRENCY_CODE_MXN = 'MXN';
	const CURRENCY_CODE_CNY = 'CNY';
	const CURRENCY_CODE_EUR = 'EUR';
	const CURRENCY_CODE_CLF = 'CLF';
	const CURRENCY_CODE_JPY = 'JPY';
	const CURRENCY_CODE_GBP = 'GBP';
	const CURRENCY_CODE_BHD = 'BHD';

	/**
	 * @deprecated@ Please refactor to use SYMBOL_USD
	 */
	const USD_SYMBOL = '$';
	/**
	 * @deprecated@ Please refactor to use CURRENCY_CODE_USD
	 */
	const DEFAULT_CURRENCY_CODE = 'USD';
	/**
	 * @deprecated@ Please refactor to use CURRENCY_CODE_CAD
	 */
	const CANADA_CURRENCY_CODE = 'CAD';

	/**
	 * @var array
	 */
	static $c_arrstrPaymentFacilitatorCurrencyCodes = [
		self::CURRENCY_CODE_USD,
		self::CURRENCY_CODE_CAD
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSymbol() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFormating() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRounding() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * @param $currencyCode
	 * @return $currencySymbol
	 * This function will accept currency code and returns the currency symbol
	 */
	public static function getCurrencySymbol( $strCurrencyCode = 'USD' ) {

		switch( $strCurrencyCode ) {
			case 'AUD':
				return self::SYMBOL_AUD;
			case 'BHD':
				return self::SYMBOL_BHD;
			case 'CAD':
				return self::SYMBOL_CAD;
			case 'CLF':
				return self::SYMBOL_CLF;
			case 'CLP':
				return self::SYMBOL_CLP;
			case 'CNY':
				return self::SYMBOL_CNY;
			case 'EUR':
				return self::SYMBOL_EUR;
			case 'GBP':
				return self::SYMBOL_GBP;
			case 'JPY':
				return self::SYMBOL_JPY;
			case 'MXN':
				return self::SYMBOL_MXN;
			default:
				return self::SYMBOL_USD;
		}
	}

}
?>