<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CAllocationTypes
 * Do not add any new functions to this class.
 */

class CAllocationTypes extends CBaseAllocationTypes {

	/**
	 * Fetch Functions
	 */

	public static function fetchAllocationTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CAllocationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllocationType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CAllocationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllAllocationTypes( $objDatabase ) {
		return self::fetchAllocationTypes( 'SELECT * FROM allocation_types', $objDatabase );
	}

}
?>