<?php

class CAllocationType extends CBaseAllocationType {

	const STANDARD		= 1;
	const DEPOSIT_ONLY 	= 2;
	const NO_DEPOSIT 	= 3;

}
?>