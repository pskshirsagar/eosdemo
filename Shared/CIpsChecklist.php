<?php

class CIpsChecklist extends CBaseIpsChecklist {

	const PROPERTY_CHECKLIST			= 'property';
	const PRE_MIGRATION_CHECKLIST		= 'pre-migration';
	const POST_MIGRATION_CHECKLIST		= 'post-migration';
	const GO_LIVE_CHECKLIST				= 'go-live';
	const COMPANY_LIVE_CHECKLIST_ID		= 25;
	const MIGRATION_CHECKLIST 			= 'migration';

	public static $c_arrstrChecklistCategories = [
		CIpsChecklist::PROPERTY_CHECKLIST,
		CIpsChecklist::PRE_MIGRATION_CHECKLIST,
		CIpsChecklist::MIGRATION_CHECKLIST,
		CIpsChecklist::POST_MIGRATION_CHECKLIST,
		CIpsChecklist::GO_LIVE_CHECKLIST
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle( $objAdminDatabase ) {
		$boolIsValid = false;

		if( false == \valStr( $this->m_strTitle ) || true == preg_match( '/[\\\\@#!%&*+:?;,.\/]/', $this->m_strTitle ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Please enter valid Checklist Name' ) );
			return $boolIsValid;
		}

		if( 2 >= strlen( $this->m_strTitle ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Checklist name should contain atleast 3 letters ' ) );
			return $boolIsValid;
		}

		if( true == \valObj( $objAdminDatabase, 'CDatabase' ) ) {
			if( true == \valId( $this->getId() ) ) {
				$objIpsChecklist = \Psi\Eos\Admin\CIpsChecklists::createService()->fetchIpsChecklistById( $this->getId(), $objAdminDatabase );
				if( false == \valObj( $objIpsChecklist, 'CIpsChecklist' ) ) {
					return $boolIsValid;
				} elseif( \Psi\CStringService::singleton()->strtolower( $this->m_strTitle ) == \Psi\CStringService::singleton()->strtolower( $objIpsChecklist->getTitle() ) ) {
					return true;
				}
			}
			$boolIsValid = $this->isChecklistTitleExist( $this->m_strTitle, $this->m_intIpsPortalVersionId, $objAdminDatabase );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case 'validate_check_list':
				$boolIsValid &= $this->valTitle( $objAdminDatabase );
				break;

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function isChecklistTitleExist( $strTitle, $intIpsPortalVersionId, $objAdminDatabase ) {
		$boolIsValid = true;
		$intExistTitleCount = \Psi\Eos\Admin\CIpsChecklists::createService()->fetchIpsChecklistCountByTitleByIpsPortalVersionId( $strTitle, $intIpsPortalVersionId, $objAdminDatabase );

		if( 0 < $intExistTitleCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Checklist name already exists' ) );
		}
		return $boolIsValid;
	}

}
?>
