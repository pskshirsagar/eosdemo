<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReservedSubdomains
 * Do not add any new functions to this class.
 */

class CReservedSubdomains extends CBaseReservedSubdomains {

	public static function fetchReservedSubdomain( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CReservedSubdomain', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchReservedSubdomainRowCount( $strDomain, $objDatabase ) {
		return self::fetchReservedSubdomainCount( 'WHERE lower( sub_domain ) = \'' . \Psi\CStringService::singleton()->strtolower( $strDomain ) . '\'', $objDatabase );
	}

}
?>