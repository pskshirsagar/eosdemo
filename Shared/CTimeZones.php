<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTimeZones
 * Do not add any new functions to this class.
 */

class CTimeZones extends CBaseTimeZones {

	public static function fetchTimeZones( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CTimeZone', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchTimeZone( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CTimeZone', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchAllTimeZones( $objDatabase ) {
		$strSql = 'SELECT * FROM time_zones ORDER BY order_num';

		return parent::fetchTimeZones( $strSql, $objDatabase );
	}

	public static function fetchTimeZoneByGmtOffset( $intGmtOffset, $objDatabase ) {
		if( false == is_numeric( $intGmtOffset ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						time_zones tz
					WHERE
						tz.gmt_offset = ' . ( int ) $intGmtOffset . 'LIMIT 1';

		return parent::fetchTimeZone( $strSql, $objDatabase );
	}

	public static function fetchTimeZoneByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolSqlOnly = false ) {
		if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT * FROM
						time_zones tz
						JOIN properties p ON ( tz.id = p.time_zone_id )
					WHERE
						p.id = ' . ( int ) $intPropertyId . '
						AND p.cid = ' . ( int ) $intCid;

		if( true == $boolSqlOnly ) {
			return $strSql;
		} else {
			return parent::fetchTimeZone( $strSql, $objDatabase );
		}
	}

	public static function fetchTimeZonesByCid( $intCid, $objDatabase ) {
		if( false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						p.id,
						tz.time_zone_name
					FROM
						time_zones tz
						JOIN properties p ON ( tz.id = p.time_zone_id )
					WHERE
						p.cid = ' . ( int ) $intCid;

		$arrmixPropertyTimeZonesData = fetchData( $strSql, $objDatabase );
		$arrmixPropertyTimeZones = [];

		if( true == valArr( $arrmixPropertyTimeZonesData ) ) {
			foreach( $arrmixPropertyTimeZonesData as $arrmixPropertyTimeZoneData ) {
				$arrmixPropertyTimeZones[$arrmixPropertyTimeZoneData['id']] = $arrmixPropertyTimeZoneData['time_zone_name'];
			}
		}

		return $arrmixPropertyTimeZones;
	}

	public static function fetchTimeZoneNameByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						tz.*
					FROM
						time_zones tz
						LEFT JOIN properties p ON ( tz.id = p.time_zone_id )
					WHERE
						p.id = ' . ( int ) $intPropertyId . '
						AND p.cid = ' . ( int ) $intCid;

		return parent::fetchTimeZone( $strSql, $objDatabase );
	}

	public static function fetchSimpleTimeZonesByCidByPropertyIds( $intCid, $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql	= 'SELECT * FROM
						time_zones tz
						JOIN properties p ON ( tz.id = p.time_zone_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';

		return parent::fetchTimeZones( $strSql, $objDatabase );
	}

	public static function fetchCustomTimeZonesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql	= 'SELECT
						*
					FROM
						time_zones tz
					JOIN
						properties p ON ( tz.id = p.time_zone_id )
					WHERE
						p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';

		return parent::fetchTimeZones( $strSql, $objDatabase );
	}

	public static function fetchSimpleTimeZonesWithPropertyIdByCidByPropertyIds( $intCid, $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql	= 'SELECT
						p.id as property_id,
						tz.*
					FROM
						time_zones tz
						JOIN properties p ON ( tz.id = p.time_zone_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleTimeZonesWithPropertyIdByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $intLastSyncOn = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strWhereSql = '';
		if( true == valId( $intLastSyncOn ) ) {
			$strWhereSql = ' AND p.updated_on > \'' . date( 'Y-m-d H:i:s', $intLastSyncOn ) . '\'';
		}

		$strSql	= 'SELECT
						p.id as property_id,
						tz.*
					FROM
						time_zones tz
						JOIN properties p ON ( tz.id = p.time_zone_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' . $strWhereSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleTimeZoneNamesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql	= 'SELECT
						p.id as property_id,
						tz.time_zone_name
					FROM
						time_zones tz
						JOIN properties p ON ( tz.id = p.time_zone_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleTimeZoneNamesByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strSql	= 'SELECT
						p.id as property_id,
						tz.time_zone_name,
						tz.name
					FROM
						time_zones tz 
						JOIN properties p ON ( tz.id = p.time_zone_id ) 
						JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $arrintPropertyGroupIds ) . ' ], ARRAY[]::INT[], true ) lp ON (lp.property_id=p.id)
					WHERE
						p.cid = ' . ( int ) $intCid;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTimeZonesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						tz.*
					FROM
						time_zones tz
						JOIN properties p ON ( tz.id = p.time_zone_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';

		return parent::fetchTimeZones( $strSql, $objDatabase );
	}

	public static function fetchTimeZonesByCloudReferenceId( $strCloudReferenceId, $objDatabase ) {
		$strWhereSql = ( true == valStr( $strCloudReferenceId ) ) ? ' AND cloud_reference_id = \'' . $strCloudReferenceId . '\' ' : '';

		$strSql = 'SELECT * FROM time_zones Where true ' . $strWhereSql . ' ORDER BY order_num';

		return parent::fetchTimeZones( $strSql, $objDatabase );
	}

}

?>