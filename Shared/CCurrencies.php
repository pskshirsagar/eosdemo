<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCurrencies
 * Do not add any new functions to this class.
 */

class CCurrencies extends CBaseCurrencies {

	public static function fetchPublishedCurrencies( $objDatabase ) {
		return self::fetchCurrencies( 'SELECT * FROM currencies WHERE is_published = TRUE', $objDatabase );
	}

	public static function fetchCurrencyByAccountId( $intAccountId, $objDatabase ) {

		$strSql = 'SELECT
						cur.currency_code,
						cur.symbol
					FROM
						currencies cur
						JOIN accounts a ON ( a.currency_code = cur.currency_code )
					WHERE
						a.id = ' . ( int ) $intAccountId;

		return fetchData( $strSql, $objDatabase );
	}

    public static function fetchCurrencyByCurrencyCode( $strCurrencyCode, $objDatabase ) {
        return self::fetchCurrency( sprintf( 'SELECT * FROM currencies WHERE currency_code = \'%s\'', $strCurrencyCode ), $objDatabase );
    }

    public static function fetchCurrencies( $strSql, $objDatabase ) {
        $arrobjCurrencies = NULL;
        $objCurrency = NULL;

        $objDataset = $objDatabase->createDataset();

        if( false == $objDataset->execute( $strSql ) ) {
            $objDataset->cleanup();
            return NULL;
        }

        if( 0 < $objDataset->getRecordCount() ) {
            $arrobjCurrencies = [];

            while( false == $objDataset->eof() ) {
                $arrmixValues = $objDataset->fetchArray();

                $objCurrency = new CCurrency();
                $objCurrency->setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );
                $objCurrency->setSerializedOriginalValues( serialize( $arrmixValues ) );

                if( false == is_null( $objCurrency->getCurrencyCode() ) ) {
                    $arrobjCurrencies[$objCurrency->getCurrencyCode()] = $objCurrency;
                } else {
                    $arrobjCurrencies[] = $objCurrency;
                }

                $objDataset->next();
            }
        }

        $objDataset->cleanup();

        return $arrobjCurrencies;
    }

    public static function fetchCurrency( $strSql, $objDatabase ) {
        $objCurrency = NULL;

        $arrobjCurrencies = self::fetchCurrencies( $strSql, $objDatabase );
        if( true == valArr( $arrobjCurrencies ) ) {
            if( 1 < \Psi\Libraries\UtilFunctions\count( $arrobjCurrencies ) ) {
                trigger_error( 'Expecting a single record when multiple records returned. Sql = ' . $strSql, E_USER_WARNING );
                return NULL;
            }

            $objCurrency = array_shift( $arrobjCurrencies );
        }

        return $objCurrency;
    }

	public static function fetchAllCurrencies( $objDatabase ) {
		return self::fetchCurrencies( 'SELECT * FROM currencies', $objDatabase );
	}

	public static function fetchPublishedCurrencyCodesByCid( $intCId, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT( cur.currency_code )
					FROM
						currencies cur
						JOIN accounts a ON ( a.currency_code = cur.currency_code AND a.cid = ' . ( int ) $intCId . ' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCountryDetailsByCurrencyCode( $strCountryCode,$objDatabase ) {

		$strWhereCondition = '';
		if( false == is_null( $strCountryCode ) ) {
			$strWhereCondition = ' AND cur.currency_code = \'' . $strCountryCode . '\' ';
		}

		$strSql = 'SELECT
						c.name,
						LOWER( c.code ) AS code,
						cur.symbol,
						cur.currency_code
					FROM
						currencies AS cur
						JOIN countries AS c ON ( cur.currency_code = c.currency_code )
					WHERE
						cur.is_published IS TRUE
						' . $strWhereCondition . '
					ORDER BY
						cur.order_num ASC';

		return  fetchData( $strSql, $objDatabase );
	}

}
?>