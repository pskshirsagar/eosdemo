<?php

class CVacationType extends CBaseVacationType {

	const ILLNESS				= 1;
	const FAMILY_EMERGENCY		= 2;
	const SCHEDULED				= 3;
	const UNSCHEDULED			= 4;
	const MATERNITY				= 5;
	const BIRTHDAY				= 15;
	const PATERNITY				= 6;
	const MARRIAGE				= 7;
	const MARRIAGE_ANNIVERSARY	= 8;
	const OFFICIAL_TOUR			= 9;
	const WEEKEND_WORKING		= 10;
	const BEREAVEMENT			= 11;
	const WORK_FROM_HOME		= 12;
	const COMPENSATORY_OFF		= 13;
	const SPECIAL_LEAVES		= 100;
	const OPTIONAL_HOLIDAY		= 14;
	const COVID_HOME_ISOLATION  = 16;
	const COVID_HOSPITALIZATION  = 17;

	const MATERNITY_LEAVE_DURATION_WEEKS	= 26;
	const DAILY_EXPECTED_WORKING_HOURS		= 8;
	const PATERNITY_LEAVES_DURATION_DAYS	= 3;
	const MARRIAGE_LEAVE_DURATION_DAYS		= 4;
	const BEREAVEMENT_LEAVE_DURATION_DAYS	= 3;
	const WORK_FROM_HOME_DURATION_DAYS		= 3;
	const OPTIONAL_HOLIDAY_DURATION_DAYS	= 2;
	const COVID_HOME_ISOLATION_DURATION_DAYS = 5;
	const COVID_HOSPITALIZATION_DURATION_DAYS = 10;

    const WEDDING_ANNIVERSARY               = 'Wedding Anniversary';

	public static $c_arrintPaidVacationType	= [ self::OFFICIAL_TOUR, self::WEEKEND_WORKING, self::BEREAVEMENT, self::COMPENSATORY_OFF ];
	public static $c_arrintSpecialLeaves 		= [ self::MATERNITY, self::PATERNITY, self::MARRIAGE, self::MARRIAGE_ANNIVERSARY, self::BIRTHDAY, self::OFFICIAL_TOUR, self::BEREAVEMENT, self::COMPENSATORY_OFF, self::OPTIONAL_HOLIDAY, self::COVID_HOME_ISOLATION, self::COVID_HOSPITALIZATION ];
	public static $c_arrintLeavePoolVacationTypes = [ CVacationType::FAMILY_EMERGENCY, CVacationType::SCHEDULED, CVacationType::UNSCHEDULED, CVacationType::ILLNESS ];

	public static function loadSmartyConstants( $objSmarty ) {

		$objSmarty->assign( 'VACATION_TYPE_ILLNESS',				self::ILLNESS );
		$objSmarty->assign( 'VACATION_TYPE_FAMILY_EMERGENCY',		self::FAMILY_EMERGENCY );
		$objSmarty->assign( 'VACATION_TYPE_SCHEDULED',				self::SCHEDULED );
		$objSmarty->assign( 'VACATION_TYPE_UNSCHEDULED',			self::UNSCHEDULED );
		$objSmarty->assign( 'VACATION_TYPE_MATERNITY',				self::MATERNITY );
		$objSmarty->assign( 'VACATION_TYPE_PATERNITY',				self::PATERNITY );
		$objSmarty->assign( 'VACATION_TYPE_MARRIAGE',				self::MARRIAGE );
		$objSmarty->assign( 'VACATION_TYPE_MARRIAGE_ANNIVERSARY',	self::MARRIAGE_ANNIVERSARY );
		$objSmarty->assign( 'VACATION_TYPE_OFFICIAL_TOUR',			self::OFFICIAL_TOUR );
	}

	public static function getVacationNames( $strCountryCode, $strAnniversaryDate, $strGender, $boolIsMarried, $boolIsFresher, $boolIsEdit, $boolIsMaternityOrPaternityLeaveApplied, $boolFromLams = false, $boolCompensatoryOff, $boolOptionalHoliday = false, $boolIsVacationCovidHomeIsolation = false, $boolIsVacationCovidHospitalization = false ) {

		if( true == $boolCompensatoryOff || true == $boolIsEdit ) {
			$arrstrVacationNames[CVacationType::COMPENSATORY_OFF] 	= 'Compensatory Off';
		}

		$arrstrVacationNames[CVacationType::FAMILY_EMERGENCY]	= 'Family Emergency';
		$arrstrVacationNames[CVacationType::ILLNESS]			= 'Illness';

		if( CCountry::CODE_INDIA == $strCountryCode ) {

			if( ( true == is_null( $strAnniversaryDate ) && false == $boolIsMarried && true == $boolIsFresher ) || ( true == $boolIsEdit && true == $boolIsMarried ) ) {
				$arrstrVacationNames[CVacationType::MARRIAGE]     = 'Marriage';
			}

			if( false == is_null( $strAnniversaryDate ) ) {
				$arrstrVacationNames[CVacationType::MARRIAGE_ANNIVERSARY]     = CVacationType::WEDDING_ANNIVERSARY;
			}

			if( ( false == is_null( $strAnniversaryDate ) && false == $boolIsMaternityOrPaternityLeaveApplied ) || ( true == $boolIsEdit && true == $boolIsMaternityOrPaternityLeaveApplied ) ) {
				( 'F' == $strGender ) ? $arrstrVacationNames[CVacationType::MATERNITY] = 'Maternity' : $arrstrVacationNames[CVacationType::PATERNITY] = 'Paternity';
			}

			if( true == $boolOptionalHoliday ) {
				$arrstrVacationNames[CVacationType::OPTIONAL_HOLIDAY]			= 'Optional Holiday';
			}

			$arrstrVacationNames[CVacationType::OFFICIAL_TOUR]     = 'Official Tour';

			$arrstrVacationNames[CVacationType::BEREAVEMENT] 	= 'Bereavement';
			$arrstrVacationNames[CVacationType::BIRTHDAY]		= 'Birthday';
			if( false == $boolIsVacationCovidHomeIsolation ) {
				$arrstrVacationNames[CVacationType::COVID_HOME_ISOLATION] = 'Covid Home Isolation';
			}
			if( false == $boolIsVacationCovidHospitalization ) {
				$arrstrVacationNames[CVacationType::COVID_HOSPITALIZATION] = 'Covid Hospitalization';
			}

		}

		$arrstrVacationNames[CVacationType::SCHEDULED]		= 'Scheduled';
		$arrstrVacationNames[CVacationType::UNSCHEDULED]	= 'Unscheduled';
		if( CCountry::CODE_INDIA == $strCountryCode && true == $boolIsFresher && false == $boolFromLams ) {
			$arrstrVacationNames[CVacationType::WORK_FROM_HOME]     = 'Work From Home';
		}

		return $arrstrVacationNames;
	}

	public static function getMaternityVacationHours( $strVacationBeginDate ) {

		if( false == valStr( $strVacationBeginDate ) ) {
			return  0;
		}

		$strAllowedMaternityLeavesEndDate		= date( 'm/d/Y', strtotime( '+' . self::MATERNITY_LEAVE_DURATION_WEEKS . ' weeks', strtotime( $strVacationBeginDate ) ) );
		$objDateIntervalAllowedMaternityLeaves 	= date_diff( date_create( $strAllowedMaternityLeavesEndDate ), date_create( $strVacationBeginDate ) );

		return $objDateIntervalAllowedMaternityLeaves->days * self::DAILY_EXPECTED_WORKING_HOURS;
	}

}
?>