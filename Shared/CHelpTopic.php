<?php

class CHelpTopic extends CBaseHelpTopic {

	const ENTRATA_SYNC_REQUIRED 	= 1;
	const ENTRATA_SYNC_NOT_REQUIRED = 0;

	protected $m_intHelpTopicModuleId;

    public function setHelpTopicModuleId( $intHelpTopicModuleId ) {
    	$this->m_intHelpTopicModuleId = $intHelpTopicModuleId;
    }

    public function getHelpTopicModuleId() {
    	return $this->m_intHelpTopicModuleId;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['help_topic_module_id'] ) ) $this->setHelpTopicModuleId( $arrmixValues['help_topic_module_id'] );
    	return;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTitle() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTopicTitle( $objDatabase ) {

    	$boolIsValid = true;

    	if( 0 >= strlen( $this->getTitle() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Name of topic is required. ' ) );
    	}

    	if( false == \Psi\CStringService::singleton()->preg_match( '/^[a-zA-Z0-9 .\-\_\&\'\"\!\?\/]+$/', $this->getTitle() ) && 0 != strlen( $this->getTitle() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Invalid name of topic. ' ) );
    	}

		$arrstrHelpTopicTitles = \Psi\Eos\Admin\CHelpTopics::createService()->fetchHelpTopicByTitleByCid( $this->getTitle(), $this->getId(), CClient::ID, $objDatabase );

		if( true == valArr( $arrstrHelpTopicTitles ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Name of topic already exists.' ) );
			$boolIsValid &= false;
		}

    	return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_UPDATE:
        	case VALIDATE_INSERT:
        		$boolIsValid &= $this->valTopicTitle( $objDatabase );
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>