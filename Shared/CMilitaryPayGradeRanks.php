<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CMilitaryPayGradeRanks
 * Do not add any new functions to this class.
 */

class CMilitaryPayGradeRanks extends CBaseMilitaryPayGradeRanks {

	public static function fetchMilitaryPayGradeRanks( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMilitaryPayGradeRank', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMilitaryPayGradeRank( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMilitaryPayGradeRank', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>