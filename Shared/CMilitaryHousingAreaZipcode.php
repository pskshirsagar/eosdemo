<?php

class CMilitaryHousingAreaZipcode extends CBaseMilitaryHousingAreaZipcode {

	protected $m_strExternalReference;

	public function getExternalReference() {
		return $this->m_strExternalReference;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['external_reference'] ) )							$this->setExternalReference( $arrmixValues['external_reference'] );
		return;
	}

	public function setExternalReference( $strExternalReference ) {
		$this->m_strExternalReference = $strExternalReference;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMilitaryHousingAreaId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valZipcode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '
					VALUES ( ' .
			$strId . ', ' .
			$this->sqlMilitaryHousingAreaId() . ', ' .
			$this->sqlZipcode() . ', ' .
			$this->sqlIsPublished() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = array();
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' military_housing_area_id = ' . $this->sqlMilitaryHousingAreaId() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlMilitaryHousingAreaId() ) != $this->getOriginalValueByFieldName( 'military_housing_area_id' ) ) {
			$arrstrOriginalValueChanges['military_housing_area_id'] = $this->sqlMilitaryHousingAreaId();
			$strSql .= ' military_housing_area_id = ' . $this->sqlMilitaryHousingAreaId() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' zipcode = ' . $this->sqlZipcode() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlZipcode() ) != $this->getOriginalValueByFieldName( 'zipcode' ) ) {
			$arrstrOriginalValueChanges['zipcode'] = $this->sqlZipcode();
			$strSql .= ' zipcode = ' . $this->sqlZipcode() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' is_published = ' . $this->sqlIsPublished() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlIsPublished() ) != $this->getOriginalValueByFieldName( 'is_published' ) ) {
			$arrstrOriginalValueChanges['is_published'] = $this->sqlIsPublished();
			$strSql .= ' is_published = ' . $this->sqlIsPublished() . ',';
			$boolUpdate = true;
		}

		$strSql = rtrim( $strSql, ',' );
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

}
?>