<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CPaymentDistributionTypes
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CPaymentDistributionTypes extends CBasePaymentDistributionTypes {

	public static function fetchPaymentDistributionTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CPaymentDistributionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPaymentDistributionType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CPaymentDistributionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllPaymentDistributionTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM payment_distribution_types';

		return self::fetchPaymentDistributionTypes( $strSql, $objDatabase );
	}

	public static function paymentDistributionTypeIdToStr( $intPaymentDistributionTypeId ) {
		switch( $intPaymentDistributionTypeId ) {
			case CPaymentDistributionType::SETTLEMENT:
				return 'Settlement';
			case CPaymentDistributionType::REVERSAL_RETURN:
				return 'Reversal Return';
			case CPaymentDistributionType::RECALL:
				return 'Return';
			case CPaymentDistributionType::REVERSAL:
				return 'Reversal';
			case CPaymentDistributionType::INTERNAL_TRANSFER:
				return 'Internal Transfer';
			default:
				trigger_error( 'Invalid payment distribution type ID.', E_USER_WARNING );
				break;
		}
	}

}
?>