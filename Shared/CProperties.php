<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CProperties
 * Do not add any new functions to this class.
 */

use Psi\Libraries\UtilObjectModifiers\CObjectModifiers;
use Psi\Libraries\UtilPagination\CPagination;

class CProperties extends CBaseProperties {

	public static function fetchIsPropertyIntegratedByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						count( pid.id )
					FROM
						property_integration_databases pid,
						integration_databases id
					WHERE
						pid.integration_database_id = id.id
						AND pid.cid = id.cid
						AND id.integration_client_type_id NOT IN ( ' . CIntegrationClientType::MIGRATION . ',' . CIntegrationClientType::EMH . ',' . CIntegrationClientType::EMH_ONE_WAY . ' )
						AND pid.property_id = ' . ( int ) $intPropertyId . '
						AND pid.cid = ' . ( int ) $intCid;

		$arrintCount = fetchData( $strSql, $objDatabase );

		return ( true == isset( $arrintCount[0]['count'] ) && 0 < $arrintCount[0]['count'] );
	}

	public static function fetchIntegratedPropertiesCountByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return 0;
		}

		$strSql = 'SELECT
						count( pid.id )
					FROM
						property_integration_databases pid,
						integration_databases id
					WHERE
						pid.integration_database_id = id.id
						AND pid.cid = id.cid
						AND id.integration_client_status_type_id NOT IN ( ' . CIntegrationClientStatusType::DISABLED . ', ' . CIntegrationClientStatusType::ON_HOLD . ' )
						AND id.integration_client_type_id NOT IN ( ' . CIntegrationClientType::MIGRATION . ',' . CIntegrationClientType::EMH . ',' . CIntegrationClientType::EMH_ONE_WAY . ' )
						AND pid.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pid.cid = ' . ( int ) $intCid;

		$arrintCount = fetchData( $strSql, $objDatabase );

		return ( true == isset ( $arrintCount[0]['count'] ) ) ? ( int ) $arrintCount[0]['count'] : 0;
	}

	public static function fetchNonEntrataCoreNonIntegratedActivePropertyIdsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						p.id,
						p.property_name
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND pgs.property_id = p.id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND pgs.activate_standard_posting = false
						AND p.is_disabled = 0
						AND p.id NOT IN (
							SELECT
								pid.property_id
							FROM
								property_integration_databases pid
							WHERE
								pid.cid = ' . ( int ) $intCid . ' )
					ORDER BY p.id ';

		$arrmixRecords = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixRecords ) ) {
			return NULL;
		}

		foreach( $arrmixRecords as $arrmixProperty ) {
			$arrmixProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
		}

		return $arrmixProperties;
	}

	public static function fetchIntegratedPropertyIdsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						p.id,
						p.property_name
					FROM
						properties p
						JOIN property_integration_databases pid ON ( pid.cid = p.cid AND pid.property_id = p.id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
					ORDER BY p.id ';

		$arrmixRecords = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixRecords ) ) {
			return NULL;
		}

		foreach( $arrmixRecords as $arrmixProperty ) {
			$arrmixProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
		}

		return $arrmixProperties;
	}

	public static function fetchAllPropertiesDetails( $objDatabase ) {
		$strSql = 'SELECT
						p.account_id,
						p.property_name
					FROM
						properties p';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchActiveCommercialPropertiesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIsIncludeTestAndDisabled = false ) {
		if( false == valArr( $arrintPropertyIds ) || false === valId( $intCid ) || false === valObj( $objDatabase, 'CDatabase' ) ) {
			return NULL;
		}
		$strSqlWhere = '';

		if( false == $boolIsIncludeTestAndDisabled ) {
			$strSqlWhere = ' AND p.is_disabled = 0 AND p.is_test = 0 ';
		}

		$strSql = 'SELECT
						*
					FROM
						properties p
					WHERE
						p.id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
						AND p.cid = ' . ( int ) $intCid . '
						AND p.occupancy_type_ids && ARRAY[' . COccupancyType::COMMERCIAL . ']
						' . $strSqlWhere . '
					ORDER BY
						LOWER( p.property_name ) ASC';

		return self::fetchProperties( $strSql, $objDatabase );

	}

	public static function fetchActivePropertyIdsByCid( $intCid, $objDatabase, $arrintPropertyIds = NULL, $boolSortByPropertyName = false, $boolShowDisabledProperties = false ) {

		$strCondition = '';

		if( false == $boolShowDisabledProperties ) {
			$strCondition = 'AND p.is_disabled = 0';
		}

		if( true == valArr( $arrintPropertyIds ) ) {
			$strCondition .= ' AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
			$strOrderBy   = ' lower ( p.property_name ) ';
		} else {
			$strOrderBy = ' p.id ';
			if( true == $boolSortByPropertyName ) {
				$strOrderBy = ' lower ( p.property_name ) ';
			}
		}

		$strSql = 'SELECT
						p.id,
						p.property_name
					FROM
						properties p
					WHERE
						p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						AND p.cid = ' . ( int ) $intCid . '
						' . $strCondition . '
					ORDER BY ' . $strOrderBy;

		$arrmixRecords = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixRecords ) ) {
			return NULL;
		}

		foreach( $arrmixRecords as $arrmixProperty ) {
			$arrmixProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
		}

		return $arrmixProperties;
	}

	public static function fetchPropertiesWithoutAccountAssociationByCid( $intCid, $objAdminDatabase ) {

		$strSql = 'SELECT
						p.*
					FROM
						properties p
						LEFT JOIN account_properties ap ON ( p.cid = ap.cid AND p.id = ap.property_id )
						LEFT JOIN contract_properties cp ON ( p.cid = cp.cid AND p.id = cp.property_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
						AND ap.account_id IS NULL
						AND cp.recurring_account_id IS NULL
					ORDER BY p.property_name';

		return self::fetchProperties( $strSql, $objAdminDatabase );
	}

	public static function loadLocalWebsiteDomainsByCid( $arrobjProperties, $intCid, $objDatabase ) {

		if( false == valArr( $arrobjProperties ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						( CASE
							WHEN ( website_domain IS NOT NULL )
							THEN ( \'http://\' || website_domain )
							WHEN ( sub_domain IS NOT NULL )
							THEN ( \'http://\' || sub_domain || \'' . CConfig::get( 'prospect_portal_suffix' ) . '\' )
						END ) AS local_website_domain,
						p.id AS property_id
					FROM
						properties p
						JOIN website_properties wp ON ( p.cid = wp.cid AND p.id = wp.property_id )
						LEFT JOIN websites w ON ( wp.cid = w.cid AND wp.website_id = w.id )
						LEFT OUTER JOIN website_domains wd ON ( w.cid = wd.cid AND w.id = wd.website_id )
					WHERE
						p.is_disabled <> 1
						AND wp.hide_on_prospect_portal <> 1
						AND p.id IN ( ' . implode( ',', array_keys( $arrobjProperties ) ) . ' )
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
						w.created_on ASC LIMIT 1';

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) {
			foreach( $arrstrData as $arrstrDataPoint ) {

				if( true == is_numeric( $arrstrDataPoint['property_id'] )
				    && true == array_key_exists( $arrstrDataPoint['property_id'], $arrobjProperties )
				    && 0 < strlen( trim( $arrstrDataPoint['local_website_domain'] ) ) ) {
					$arrobjProperties[$arrstrDataPoint['property_id']]->setLocalWebsiteDomain( $arrstrDataPoint['local_website_domain'] );
				}
			}
		}
	}

	public static function fetchNonIntegratedPropertiesByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						properties
					WHERE
						cid = ' . ( int ) $intCid . '
						AND remote_primary_key IS NULL
					ORDER BY
						lower ( property_name ) ASC,
						order_num';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCustomVacancyPropertyById( $intPropertyId, $objDatabase ) {

		$strSql = 'SELECT
						p.*,
						mac.database_id AS database_id
					FROM
						properties p,
						clients mac
					WHERE
						mac.id = p.cid
						AND p.id = ' . ( int ) $intPropertyId . '';

		return self::fetchProperty( $strSql, $objDatabase );
	}

	// Added this funtion purposefully as it is breaking up the VoIP Property Greetings.

	public static function fetchCustomGreetingPropertyById( $intPropertyId, $objDatabase ) {

		$strSql = 'SELECT
						p.*,
						mac.database_id AS database_id
					FROM
						properties p,
						clients mac
					WHERE
						mac.id = p.cid
						AND p.id = ' . ( int ) $intPropertyId . '';

		return self::fetchProperty( $strSql, $objDatabase );
	}

	public static function fetchNonIntegratedActivePropertiesByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM
						properties
					WHERE
						cid = ' . ( int ) $intCid . '
						AND remote_primary_key IS NULL
						AND is_disabled = 0
					ORDER BY
						lower ( property_name ) ASC,
						order_num';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchNonIntegratedPropertiesByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM
						properties
					WHERE
						id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND remote_primary_key IS NULL
						AND is_disabled <> 1
						AND property_type_id <> ' . CPropertyType::SETTINGS_TEMPLATE . '
						AND property_type_id <> ' . CPropertyType::TEMPLATE . '
					ORDER BY
						lower ( property_name ) ASC,
						order_num';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchNonIntegratedPropertiesCountByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT COUNT( id ) AS count
					FROM
						properties
					WHERE
						id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND remote_primary_key IS NULL
						AND is_disabled <> 1';

		$arrintProperties = ( array ) fetchData( $strSql, $objDatabase );

		return $arrintProperties[0]['count'];
	}

	public static function fetchNonIntegratedMaintenancePropertiesCountByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolShowDisabledData = false ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT COUNT( p.id ) AS count
					FROM
						properties p
						LEFT JOIN property_preferences pp ON ( p.id = pp.property_id AND p.cid = pp.cid AND pp.key = \'BLOCK_ALL_WORK_ORDER_CALLS_RELATED_INTEGRATION\' )
					WHERE
						p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.cid = ' . ( int ) $intCid . '
						AND ( p.remote_primary_key IS NULL
								or pp.value= \'1\' )
						AND p.is_disabled = ' . ( ( false == $boolShowDisabledData ) ? 0 : 1 );

		$arrintProperties = ( array ) fetchData( $strSql, $objDatabase );

		return $arrintProperties[0]['count'];
	}

	public static function fetchPropertiesByAccountIdByCid( $intAccountId, $intCid, $objAdminDatabase, $boolUseParentFunction = false, $boolIsActiveProperty = false, $objPagination = NULL ) {

		if( false == is_numeric( $intAccountId ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		// As discussed with Mitesh, added a parameter to call base file function.
		// This function already present in this custom file for many days. As per the new requirement new field 'account_id' is added to 'properties' table and generated base file.
		// In base file same function name fetchPropertiesByAccountIdByCid is generated dyanamically.
		// As this function alredy exists in custom singular file there was no way to call base file function.
		// Hence with an extra parameter we have added a facility to call the base file function.

		if( false != $boolUseParentFunction ) {
			return parent::fetchPropertiesByAccountIdByCid( $intAccountId, $intCid, $objAdminDatabase );
		}

		$strWhereCondition = '';
		if( true == $boolIsActiveProperty ) {
			$strWhereCondition = ' AND p.is_disabled <> 1 AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' ) AND p.termination_date IS NULL AND p.is_test = 0 AND p.is_managerial = 0 ';
		}

		$strOffsetCondition = '';
		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strOffsetCondition = 'OFFSET ' . ( int ) $objPagination->getOffset() . '
									LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		$strSql = 'SELECT
						DISTINCT ON ( p.property_name, p.id )
						p.*
					FROM
						properties p,
						account_properties ap
					WHERE
						p.id = ap.property_id
						AND ap.account_id = ' . ( int ) $intAccountId . '
						AND p.cid = ' . ( int ) $intCid
		          . $strWhereCondition . '
					ORDER BY
						p.property_name, p.id ' . $strOffsetCondition;

		return self::fetchProperties( $strSql, $objAdminDatabase );
	}

	public static function fetchPropertiesByCids( $arrintCids, $objDatabase, $strOrderBy = NULL, $boolIsFetchObject = true ) {

		if( false == \valIntArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM properties
						WHERE cid IN ( ' . implode( ',', $arrintCids ) . ' )
							AND property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
							AND is_disabled <> 1';

		if( false == is_null( $strOrderBy ) && true == valStr( $strOrderBy ) ) {
			$strSql .= ' ORDER BY ' . $strOrderBy;
		}

		if( true == $boolIsFetchObject ) {
			return self::fetchProperties( $strSql, $objDatabase );
		} else {
			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchPropertiesDetailsByCids( $arrintCids, $objDatabase, $arrmixFilteredExplodedSearch = NULL ) {

		if( true == valArr( $arrmixFilteredExplodedSearch ) ) {
			$strSearchCondition = ( 1 == \Psi\Libraries\UtilFunctions\count( $arrmixFilteredExplodedSearch ) && is_numeric( $arrmixFilteredExplodedSearch[0] ) ) ? ' to_char( p.id, \'99999999\' ) LIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' OR' : '';
			$strWhereCondition = ' AND( ' . $strSearchCondition . ' p.property_name ILIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' ) ';
			$strLimit = ( true == $arrmixFilteredExplodedSearch['limit'] ) ? ' LIMIT 10 ' : '';
		}

		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}
		$strSql = 'SELECT
							p.id,
							p.property_name,
							c.company_name,
							c.rwx_domain
						FROM
							properties p
							JOIN clients c ON p.cid = c.id
						WHERE cid IN ( ' . implode( ',', $arrintCids ) . ' )
							AND is_disabled <> 1
							AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' ) ' . $strWhereCondition . '
						ORDER BY
						p.property_name, p.id' . $strLimit;

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchSimplePropertiesForPaperlessReportByCids( $intCid, $objDatabase, $boolIsIncludeFourMonthOlderProperties = false, $boolIsIncludeNonIntegratedNonEntrataProperties = false ) {

		$strSql = 'SELECT
						p.id,
						p.property_name
					FROM
						properties p
						JOIN property_integration_databases pid ON ( pid.property_id = p.id and pid.cid = p.cid )
						JOIN integration_databases id ON ( pid.cid = id.cid AND pid.integration_database_id = id.id )
						JOIN property_gl_settings pgs ON ( pgs.property_id = p.id and pgs.cid = p.cid )
					WHERE
						p.is_disabled <> 1
						AND p.is_test <> 1
						AND ( ( id.integration_client_type_id NOT IN ( ' . implode( ',', CIntegrationClientType::$c_arrintMigrateIntegrationClientTypeIds ) . ' )
						AND id.integration_client_status_type_id != ' . CIntegrationClientStatusType::DISABLED . ' ) OR pgs.activate_standard_posting = true )
						AND p.cid = ' . ( int ) $intCid;

		if( true == $boolIsIncludeNonIntegratedNonEntrataProperties ) {
			$strSql = 'SELECT
						p.id,
						p.property_name
					FROM
						properties p
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled <> 1';
		}

		if( true == $boolIsIncludeFourMonthOlderProperties ) {
			$strSql .= ' AND 0 < ( SELECT count( l.id ) FROM leases l WHERE l.property_id = p.id AND l.cid = p.cid AND p.cid = ' . ( int ) $intCid . ' ) ';
		} else {
			$strSql .= ' AND p.created_on < (current_date - INTERVAL \'4 months\')';
		}

		$strSql .= ' ORDER BY p.property_name ';

		$arrmixRecords = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixRecords ) ) {
			return NULL;
		}

		foreach( $arrmixRecords as $arrmixProperty ) {
			$arrmixProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
		}

		return $arrmixProperties;
	}

	public static function fetchPropertiesByIsDisabledByCid( $intCid, $intIsDisabled = NULL, $objDatabase, $boolExcludeTerminated = false ) {

		$strSql = 'SELECT * FROM properties WHERE property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' ) AND cid = ' . ( int ) $intCid;

		if( false == is_null( $intIsDisabled ) ) {
			$strSql .= ' AND is_disabled = ' . ( int ) $intIsDisabled;
		}

		if( true == $boolExcludeTerminated ) {
			$strSql .= ' AND termination_date IS NULL ';
		}

		$strSql .= ' ORDER BY
						property_name';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchIntegratedPropertiesNotAssociatedToIntegrationDatabaseByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( p.id )
						p.*
					FROM
						properties p,
						property_integration_databases pid
					WHERE
						p.id = pid.property_id AND p.cid = pid.cid
						AND pid.integration_database_id <> ' . ( int ) $intIntegrationDatabaseId . '
						AND p.cid = ' . ( int ) $intCid . ' ';

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchPropertiesByRemotePrimaryKeysByIntegrationDatabaseIdByCid( $arrstrPropertyRemotePrimaryKeys, $intIntegrationDatabaseId, $intCid, $objDatabase ) {

		if( false == valArr( $arrstrPropertyRemotePrimaryKeys ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON ( p.id )
						p.*
					FROM
						properties p,
						property_integration_databases pid
					WHERE
						p.id = pid.property_id AND p.cid = pid.cid
						AND lower ( p.remote_primary_key ) IN ( \'' . implode( '\',\'', $arrstrPropertyRemotePrimaryKeys ) . '\' )
						AND pid.integration_database_id = ' . ( int ) $intIntegrationDatabaseId . '
						AND p.cid = ' . ( int ) $intCid;

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByCustomerIdByCid( $intCustomerId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( p.id ) p.*
					FROM
						customers cc,
						lease_customers clc,
						leases cl,
						properties p
					WHERE
						cc.id = clc.customer_id AND cc.cid = clc.cid
						AND cl.id = clc.lease_id AND cl.cid = clc.cid
						AND p.id = cl.property_id AND p.cid = cl.cid
						AND cc.id = ' . ( int ) $intCustomerId . '
						AND p.cid = ' . ( int ) $intCid;

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public function fetchPropertiesByWebsiteIdByCid( $intWebsiteId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						p.*,
						( SELECT array_to_string( array( SELECT locale_code FROM property_locales pl WHERE pl.cid = p.cid AND pl.property_id = p.id ), \',\' ) ) as supported_locale_codes
					FROM
						properties AS p,
						website_properties AS wp
					WHERE
						p.id = wp.property_id
						AND p.cid = wp.cid
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
						lower ( p.property_name ) ASC';

		return $this->fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchCustomPropertyRemotePrimaryKeysByWebsiteIdByIntegrationDatabaseIdByCid( $intWebsiteId, $intIntegrationDatabaseId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						p.id,
						p.remote_primary_key
					FROM
						properties p
						JOIN website_properties wp ON( p.id = wp.property_id AND p.cid = wp.cid )
						JOIN property_integration_databases pid ON( pid.cid = p.cid AND pid.property_id = p.id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						AND pid.integration_database_id = ' . ( int ) $intIntegrationDatabaseId;

		$arrstrProperties   = ( array ) fetchData( $strSql, $objClientDatabase );
		$arrstrPropertyRpks = [];

		foreach( $arrstrProperties as $arrstrProperty ) {
			$arrstrPropertyRpks[$arrstrProperty['id']] = $arrstrProperty['remote_primary_key'];
		}

		return $arrstrPropertyRpks;
	}

	public static function fetchParentChildPropertiesByWebsiteIdByCid( $intWebsiteId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						p.*
					FROM
						properties AS p,
						website_properties AS wp
					WHERE
						( p.id = wp.property_id AND p.cid = wp.cid OR p.property_id = wp.property_id AND p.cid = wp.cid )
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
						lower ( p.property_name ) ASC';

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchPublishedPropertiesByWebsiteIdByCid( $intWebsiteId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						p.*
					FROM
						properties AS p,
						website_properties AS wp
					WHERE
						p.id = wp.property_id AND p.cid = wp.cid
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						AND p.is_disabled <> 1
						AND wp.hide_on_prospect_portal <> 1
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
						lower ( p.property_name ) ASC';

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchMinMaxRentByWebsiteIdByCid( $intWebsiteId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						COALESCE ( min( p.min_rent ), 0 ) AS min_rent,
						COALESCE ( max( p.max_rent ), 0 ) AS max_rent
					FROM
						properties AS p,
						website_properties AS wp
					WHERE
						p.id = wp.property_id
						AND wp.cid = p.cid
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						AND p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled <> 1';

		$strSql .= ' AND wp.hide_on_prospect_portal <> 1';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchMinMaxBedBathByWebsiteIdByCid( $intWebsiteId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
							COALESCE( min( p.min_bedrooms ), 0 ) AS min_bedrooms,
							COALESCE( max( p.max_bedrooms ), 0 ) AS max_bedrooms,
							COALESCE( min( p.min_bathrooms ), 0 ) AS min_bathrooms,
							COALESCE( max( p.max_bathrooms ), 0 ) AS max_bathrooms
						FROM
							properties AS p,
							website_properties AS wp
						WHERE
							p.id = wp.property_id
							AND wp.cid = p.cid
							AND wp.website_id = ' . ( int ) $intWebsiteId . '
							AND p.cid = ' . ( int ) $intCid . '
							AND p.is_disabled <> 1';

		$strSql .= ' AND wp.hide_on_prospect_portal <> 1';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchProspectPortalPropertyIdsByWebsiteIdByCid( $intWebsiteId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						p.id
					FROM
						properties AS p,
						website_properties AS wp
					WHERE
						p.id = wp.property_id AND p.cid = wp.cid
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						AND wp.hide_on_prospect_portal != 1
						AND p.is_disabled != 1
						p.cid = ' . ( int ) $intCid . '
						AND p.property_id IS NULL';

		$arrmixProperties = fetchData( $strSql, $objClientDatabase );

		$arrintFinalPropertyIds = [];

		if( true == valArr( $arrmixProperties ) ) {
			foreach( $arrmixProperties as $arrintPropertyIds ) {
				$arrintFinalPropertyIds[$arrintPropertyIds['id']] = $arrintPropertyIds['id'];
			}
		}

		return $arrintFinalPropertyIds;
	}

	public static function hasMobilePortalByWebsiteIdByCidByPropertyId( $intWebsiteId, $intCid, $intPropertyId, $objClientDatabase ) {

		$strSql = 'SELECT
						count( p.id ) AS count
					FROM
						properties p
						JOIN website_properties wp ON ( p.cid = wp.cid AND p.id = wp.property_id )
					WHERE
						wp.website_id = ' . ( int ) $intWebsiteId . '
						AND wp.property_id = ' . ( int ) $intPropertyId . '
						AND p.is_disabled != 1
						AND p.cid = ' . ( int ) $intCid;

		$arrmixProperties = ( array ) fetchData( $strSql, $objClientDatabase );

		return ( isset( $arrmixProperties[0]['count'] ) && 0 < $arrmixProperties[0]['count'] );
	}

	public static function fetchMobilePortalPropertyIdsByWebsiteIdByCid( $intWebsiteId, $intCid, $objClientDatabase, $boolExcludeChildProperties = true ) {

		$strSql = 'SELECT
						p.id
					FROM
						properties AS p,
						website_properties AS wp
					WHERE
						p.id = wp.property_id AND p.cid = wp.cid
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						AND p.is_disabled != 1
						AND p.cid = ' . ( int ) $intCid;

		if( true == $boolExcludeChildProperties ) {
			$strSql .= ' AND p.property_id IS NULL ';
		}

		$strSql .= ' ORDER BY lower ( p.property_name ) ASC';

		$arrmixProperties = fetchData( $strSql, $objClientDatabase );

		$arrintFinalPropertyIds = [];

		if( true == valArr( $arrmixProperties ) ) {
			$arrintFinalPropertyIds = array_keys( rekeyArray( 'id', $arrmixProperties ) );
		}

		return $arrintFinalPropertyIds;
	}

	public static function hasCustomerSpecificMobilePortalPropertiesByWebsiteIdByCustomerIdByCid( $intWebsiteId, $intCustomerId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						count( p.id ) AS count
					FROM
						properties p
						JOIN website_properties wp ON ( p.cid = wp.cid AND p.id = wp.property_id )
						JOIN leases l ON ( p.cid = l.cid AND p.id = l.property_id )
						JOIN lease_customers lc ON ( l.cid = lc.cid AND l.id = lc.lease_id )
						JOIN customers c ON ( c.cid = lc.cid AND c.id = lc.customer_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled != 1
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						AND c.id = ' . ( int ) $intCustomerId;

		$arrmixProperties = fetchData( $strSql, $objClientDatabase );

		return ( isset( $arrmixProperties[0]['count'] ) && 0 < $arrmixProperties[0]['count'] );
	}

	public static function fetchResidentPortalPropertyIdsByWebsiteIdByCid( $intWebsiteId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						p.id
					FROM
						properties p
						JOIN website_properties wp ON ( p.cid = wp.cid AND p.id = wp.property_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled != 1
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						AND wp.hide_on_resident_portal != 1
					ORDER BY
						lower ( p.property_name ) ASC';

		$arrmixProperties       = fetchData( $strSql, $objClientDatabase );
		$arrintFinalPropertyIds = [];

		if( true == valArr( $arrmixProperties ) ) {
			foreach( $arrmixProperties as $arrintPropertyIds ) {
				$arrintFinalPropertyIds[$arrintPropertyIds['id']] = $arrintPropertyIds['id'];
			}
		}

		return $arrintFinalPropertyIds;
	}

	public static function fetchResidentPortalRelatedPropertyIdsByWebsiteIdByCid( $intWebsiteId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						child_p.id
					FROM
						properties p
						LEFT JOIN properties AS child_p ON ( p.cid = child_p.cid AND ( p.id = child_p.property_id OR p.id = child_p.id ) )
						JOIN website_properties AS wp ON ( p.cid = wp.cid AND p.id = wp.property_id )
					WHERE
						wp.website_id = ' . ( int ) $intWebsiteId . '
						AND wp.hide_on_resident_portal != 1
						AND p.is_disabled != 1
						AND child_p.is_disabled != 1
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
						lower ( p.property_name ) ASC';

		$arrmixProperties = fetchData( $strSql, $objClientDatabase );

		$arrintFinalPropertyIds = [];

		if( true == valArr( $arrmixProperties ) ) {
			foreach( $arrmixProperties as $arrintPropertyIds ) {
				$arrintFinalPropertyIds[$arrintPropertyIds['id']] = $arrintPropertyIds['id'];
			}
		}

		return $arrintFinalPropertyIds;
	}

	public static function fetchPropertyByWebsiteIdByPropertyNameByCityByCid( $intWebsiteId, $strPropertyName, $strCity, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT p.*
					FROM
						properties AS p,
						website_properties AS wp,
						property_addresses AS pa
					WHERE
						p.id = wp.property_id AND p.cid = wp.cid
						AND p.id = pa.property_id AND p.cid = pa.cid AND pa.is_alternate = false
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						AND regexp_replace( p.property_name,\'[^0-9a-zA-Z]+\', \'\', \'g\' ) ILIKE \'' . \Psi\CStringService::singleton()->preg_replace( '/[^0-9a-zA-Z]+/', '', $strPropertyName ) . '\'';

		if( false == is_null( $strCity ) ) {
			$strSql .= ' AND regexp_replace( pa.city,\'[^0-9a-zA-Z]+\', \'\', \'g\' ) ILIKE \'' . \Psi\CStringService::singleton()->preg_replace( '/[^0-9a-zA-Z]+/', '', $strCity ) . '\'';
		} else {
			$strSql .= ' AND pa.city IS NULL';
		}

		$strSql .= '	AND p.cid = ' . ( int ) $intCid . ' AND p.is_disabled != 1
					LIMIT 1';

		return self::fetchProperty( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyByWebsiteIdBySeoPropertyNameByCid( $intWebsiteId, $strSeoPropertyName, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						p.*
					FROM
						properties AS p,
						website_properties AS wp,
						property_addresses AS pa
					WHERE
						p.id = wp.property_id AND p.cid = wp.cid
						AND p.id = pa.property_id AND p.cid = pa.cid AND pa.is_alternate = false
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						AND lower( regexp_replace( p.property_name,\'[^0-9a-zA-Z]+\', \'\', \'g\' ) ) LIKE lower( \'' . \Psi\CStringService::singleton()->preg_replace( '/[^0-9a-zA-Z]+/', '', $strSeoPropertyName ) . '\' )
						AND p.is_disabled != 1
						AND p.cid = ' . ( int ) $intCid . '
					LIMIT 1';

		return self::fetchProperty( $strSql, $objClientDatabase );
	}

	public static function fetcIntegratedResidentPortalPropertiesByIdsByWebsiteIdByCid( $arrintPropertyIds, $intWebsiteId, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return [];
		}

		$strSql = 'SELECT
						p.*,
						ict.id AS integration_client_type_id
					FROM
						properties p
						JOIN website_properties wp ON ( ( p.id = wp.property_id OR p.property_id = wp.property_id ) AND p.cid = wp.cid )
						JOIN property_integration_databases pid ON ( p.id = pid.property_id AND p.cid = pid.cid )
						JOIN integration_databases id ON ( id.cid = pid.cid AND id.id = pid.integration_database_id )
						JOIN integration_client_types ict ON ( ict.id = id.integration_client_type_id AND id.cid = ' . ( int ) $intCid . ' )
					WHERE
						wp.website_id = ' . ( int ) $intWebsiteId . '
						AND wp.hide_on_resident_portal != 1
						AND p.remote_primary_key IS NOT NULL
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
						lower ( p.property_name ) ASC';

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchIsIntegratedPropertyByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $intMetadataType = 0 ) {

		switch( $intMetadataType ) {
			case 1:
				$strWhere = ' AND id.integration_client_type_id IN ( ' . implode( ',', CMaintenanceProblem::$c_arrintIntegratedClientTypes ) . ' ) ';
				break;

			case 2:
				$strWhere = ' AND id.integration_client_type_id IN ( ' . implode( ',', CMaintenanceStatus::$c_arrintIntegratedClientTypes ) . ' ) ';
				break;

			case 3:
				$strWhere = ' AND id.integration_client_type_id IN ( ' . implode( ',', CMaintenancePriority::$c_arrintIntegratedClientTypes ) . ' ) ';
				break;

			case 4:
				$strWhere = ' AND id.integration_client_type_id NOT IN ( ' . implode( ',', CIntegrationClientType::$c_arrintMigrateIntegrationClientTypeIds ) . ', ' . CIntegrationClientType::AMSI . ') ';
				break;

			default:
				$strWhere = ' AND id.integration_client_type_id NOT IN ( ' . implode( ',', CIntegrationClientType::$c_arrintMigrateIntegrationClientTypeIds ) . ' ) ';
				break;
		}

		$strSql = 'SELECT
						COUNT( p.id ) as cnt
					FROM
						properties p
						JOIN property_integration_databases pid ON ( p.id = pid.property_id AND p.cid = pid.cid )
						JOIN integration_databases id ON ( id.cid = pid.cid AND id.id = pid.integration_database_id )
						LEFT JOIN property_preferences pp ON ( p.id = pp.property_id AND p.cid = pp.cid AND pp.key = \'BLOCK_ALL_WORK_ORDER_CALLS_RELATED_INTEGRATION\' )
					WHERE
						p.remote_primary_key IS NOT NULL
						AND p.id = ' . ( int ) $intPropertyId . '
						AND p.cid = ' . ( int ) $intCid . '
						AND ( pp.value IS NULL
						OR pp.value = \'0\' )
						' . $strWhere . '
						AND id.integration_client_status_type_id <> ' . CIntegrationClientStatusType::DISABLED;

		$arrintCount = fetchData( $strSql, $objDatabase );

		return ( true == isset ( $arrintCount[0]['cnt'] ) ) ? ( int ) $arrintCount[0]['cnt'] : 0;

	}

	public static function fetchSimpleIntegratedPropertiesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( true != valArr( $arrintPropertyIds ) ) {
			return true;
		}

		$strSql = 'SELECT
						p.id,
						COUNT( p.id ) as cnt
					FROM
						properties p
						JOIN property_integration_databases pid ON ( p.id = pid.property_id AND p.cid = pid.cid )
						JOIN integration_databases id ON ( id.cid = pid.cid AND id.id = pid.integration_database_id )
						LEFT JOIN property_preferences pp ON ( p.id = pp.property_id AND p.cid = pp.cid AND pp.key = \'BLOCK_ALL_WORK_ORDER_CALLS_RELATED_INTEGRATION\' )
					WHERE
						p.remote_primary_key IS NOT NULL
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.cid = ' . ( int ) $intCid . '
						AND ( pp.value IS NULL
						OR pp.value = \'0\' )
						AND id.integration_client_type_id NOT IN ( ' . implode( ',', CIntegrationClientType::$c_arrintMigrateIntegrationClientTypeIds ) . ' )
						AND id.integration_client_status_type_id <> ' . CIntegrationClientStatusType::DISABLED . '
						GROUP BY
							p.id';

		$arrmixPropertyIds = fetchData( $strSql, $objDatabase );

		return $arrmixPropertyIds;
	}

	public static function fetchSimpleIntegratedOrNonIntegratedPropertiesByCid( $intCid, $objDatabase, $boolIsIntegrated = false, $arrintPropertyIds = NULL, $strOrberBy = NULL ) {
		$strSqlInspection = '';

		if( NULL != $arrintPropertyIds ) {
			$strSqlInspection = 'AND p.id in ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		}

		if( NULL == $strOrberBy ) {
			$strOrberBy = 'property_name';
		}

		$strIntegrationSql = 'AND ( p.remote_primary_key IS NULL
								OR
							 p.remote_primary_key IS NOT NULL
							 AND ( pp.value = \'1\' OR id.integration_client_type_id IN ( ' . implode( ',', CIntegrationClientType::$c_arrintMigrateIntegrationClientTypeIds ) . ' ) OR id.integration_client_status_type_id = ' . CIntegrationClientStatusType::DISABLED . ' ) )';

		if( true == $boolIsIntegrated ) {
			$strIntegrationSql = 'AND ( p.remote_primary_key IS NOT NULL
								 AND ( id.integration_client_type_id NOT IN ( ' . implode( ',', CIntegrationClientType::$c_arrintMigrateIntegrationClientTypeIds ) . ' )
								 AND id.integration_client_status_type_id != ' . CIntegrationClientStatusType::DISABLED . ' AND ( pp.value != \'1\' OR pp.value IS NULL ) ) )';
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.is_disabled,
						ps.name AS state_name,
						util_get_system_translated( \'name\', pt.name, pt.details ) AS property_type_name
					FROM
						properties p
						LEFT JOIN property_integration_databases pid ON ( p.id = pid.property_id AND p.cid = pid.cid )
						LEFT JOIN integration_databases id ON ( id.cid = pid.cid AND id.id = pid.integration_database_id )
						LEFT JOIN property_preferences pp ON ( p.id = pp.property_id AND p.cid = pp.cid AND pp.key = \'BLOCK_ALL_WORK_ORDER_CALLS_RELATED_INTEGRATION\' )
						LEFT JOIN property_types pt ON ( p.property_type_id = pt.id )
						LEFT JOIN property_addresses pa ON ( pa.cid = p.cid AND p.id = pa.property_id AND pa.is_alternate = false )
						LEFT JOIN states ps ON ( pa.state_code = ps.code )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0 ' . $strSqlInspection . ' ' . $strIntegrationSql . '
						AND p.property_type_id NOT IN ( ' . implode( ',', CPropertyType::$c_arrintExcludePropertyTypeIds ) . ' )
					ORDER BY ' . $strOrberBy;

		$arrmixProperties = fetchData( $strSql, $objDatabase );

		return rekeyArray( 'id', $arrmixProperties );
	}

	public static function fetchSimplePropertiesByPropertyIdsByCid( $intCid, $objDatabase, $arrintPropertyIds = NULL, $strOrberBy = NULL, $boolShowDisabledData = false ) {
		$strSqlInspection = '';

		if( NULL != $arrintPropertyIds ) {
			$strSqlInspection = 'AND p.id in ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		}

		if( NULL == $strOrberBy ) {
			$strOrberBy = 'property_name';
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.is_disabled,
						p.occupancy_type_ids,
						ps.name AS state_name,
						util_get_system_translated( \'name\', pt.name, pt.details ) AS property_type_name
					FROM
						properties p
						LEFT JOIN property_types pt ON ( p.property_type_id = pt.id )
						LEFT JOIN property_addresses pa ON ( pa.cid = p.cid AND p.id = pa.property_id AND pa.is_alternate = false )
						LEFT JOIN states ps ON ( pa.state_code = ps.code )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = ' . ( ( false == $boolShowDisabledData ) ? 0 : 1 ) . ' ' . $strSqlInspection . '
					ORDER BY ' . $strOrberBy;

		$arrmixProperties = fetchData( $strSql, $objDatabase );

		return rekeyArray( 'id', $arrmixProperties );
	}

	public static function fetchProspectPortalPropertiesByWebsiteIdByCid( $intWebsiteId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						p.*,
						wp.hide_more_info_button,
						wp.show_application_fee_option
					FROM
						properties AS p,
						website_properties AS wp
					WHERE
						p.id = wp.property_id AND p.cid = wp.cid
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						AND wp.hide_on_prospect_portal != 1
						AND p.is_disabled != 1
						AND p.property_id IS NULL
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
						lower ( p.property_name ) ASC';

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchCustomAllProspectPortalPropertiesByWebsiteIdByCid( $intWebsiteId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						p.id,
						p.property_type_id,
						p.property_name
					FROM
						properties AS p,
						website_properties AS wp
					WHERE
						p.id = wp.property_id AND p.cid = wp.cid
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						AND p.is_disabled != 1
						AND p.property_id IS NULL
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
						lower ( p.property_name ) ASC';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchProspectPortalPropertiesByWebsiteIdByPropertyTypeIdByCid( $intWebsiteId, $intPropertyTypeId, $intCid, $objClientDatabase ) {
		if( false == isset ( $intPropertyTypeId ) || false == is_numeric( $intPropertyTypeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.*,
						wp.hide_more_info_button,
						wp.show_application_fee_option
					FROM
						properties AS p,
						website_properties AS wp
					WHERE
						p.id = wp.property_id AND p.cid = wp.cid
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						AND wp.hide_on_prospect_portal != 1
						AND p.is_disabled != 1
						AND p.property_id IS NULL
						AND p.property_type_id = ' . ( int ) $intPropertyTypeId . '
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
						lower ( p.property_name ) ASC';

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchProspectPortalPropertyByIdByWebsiteIdByCid( $intPropertyId, $intWebsiteId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						p.*
					FROM
						properties AS p,
						website_properties AS wp
					WHERE
						p.id = wp.property_id AND p.cid = wp.cid
						AND wp.property_id = ' . ( int ) $intPropertyId . '
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						AND wp.hide_on_prospect_portal != 1
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
						lower ( p.property_name ) ASC';

		return self::fetchProperty( $strSql, $objClientDatabase );
	}

	public static function fetchPropertiesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $objPropertyFilter = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$arrstrAdditionalAndStatements = [];
		if( NULL != $objPropertyFilter ) {
			if( 1 == $objPropertyFilter->getHideDisabled() ) {
				$arrstrAdditionalAndStatements[] = ' is_disabled <> 1 AND termination_date IS NULL ';
			}

			if( false != $objPropertyFilter->getExcludeZeroUnitCount() ) {
				$arrstrAdditionalAndStatements[] = ' number_of_units > 0 ';
			}

			if( false != $objPropertyFilter->getExcludeIsManagerial() ) {
				$arrstrAdditionalAndStatements[] = ' is_managerial = 0 ';
			}
		}

		$strWhere = '';
		if( false != valArr( $arrstrAdditionalAndStatements ) ) {
			$strWhere = ' AND ' . implode( ' AND ', $arrstrAdditionalAndStatements );
		}

		$strSql = 'SELECT
						*
					FROM
						properties
					WHERE
						id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND cid = ' . ( int ) $intCid .
		          $strWhere . '
					ORDER BY
						lower ( property_name ),
						id';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertiesByPropertyIds( $arrintPropertyIds, $objDatabase, $boolIsReturnKeyedArray = true ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM properties WHERE id in ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						ORDER BY
							lower ( property_name ),
							id';

		return self::fetchProperties( $strSql, $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchPropertiesByCompanyUserIdByPropertyIdsByCid( $intCompanyUserId, $arrintPropertyIds, $intCid, $objDatabase, $boolShowEnabled = NULL, $boolIsFeatured = NULL, $boolSortParentPropertyFirst = false, $intPageNo = NULL, $intPageSize = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSelect                  = '';
		$strJoin                    = '';
		$strSortParentPropertyFirst = ( false != $boolSortParentPropertyFirst ) ? ' gcup.property_id ASC nulls FIRST,' : '';

		if( false != $boolIsFeatured ) {
			$strSelect = ', pd.is_featured';
			$strJoin   = ' LEFT JOIN property_details pd ON ( gcup.id = pd.property_id AND gcup.cid = pd.cid )';
		}

		$strSql = self::buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator = false, $boolShowEnabled );

		$strSql = 'SELECT
						gcup.* ' . $strSelect . '
					FROM
						( ' . $strSql . ' ) as gcup ' .
		          $strJoin . '
					WHERE
						gcup.cid = ' . ( int ) $intCid . '
						AND gcup.id in ( ' . implode( ',', $arrintPropertyIds ) . ' )
					ORDER BY ' .
		          $strSortParentPropertyFirst . '
						gcup.property_name,
						gcup.order_num';

		if( NULL != $intPageNo && NULL != $intPageSize ) {
			$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit  = ( int ) $intPageSize;

			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByIdsByFieldNamesByCompanyUserIdByCid( $arrintPropertyIds, $arrstrFieldNames, $intCompanyUserId, $intCid, $objDatabase, $boolReturnArray = false ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrstrFieldNames ) ) {
			return [];
		}

		$strSql = self::buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator = false, $boolShowEnabled = false );

		$strSql = 'SELECT
						' . implode( ',', $arrstrFieldNames ) . '
					FROM
						( ' . $strSql . ' ) gcup

					WHERE
						gcup.cid = ' . ( int ) $intCid . '
						AND gcup.id in ( ' . implode( ',', $arrintPropertyIds ) . ' )
					ORDER BY
						lower ( gcup.property_name )';

		if( true == $boolReturnArray ) {
			return fetchData( $strSql, $objDatabase );
		}

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertyByCompanyUserIdByPropertyIdByCid( $intCompanyUserId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = self::buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator = false, $boolShowEnabled = false );

		$strSql = 'SELECT * FROM
						( ' . $strSql . ' ) as SubQ
					WHERE
						id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
						AND company_user_id = ' . ( int ) $intCompanyUserId;

		return self::fetchProperty( $strSql, $objDatabase );
	}

	public static function fetchPropertyByIdBySessionDataByCid( $intId, $intIsDisabledSessionData, $intCid, $objDatabase ) {
		$strSql = ( true == is_null( $intIsDisabledSessionData ) || 0 == $intIsDisabledSessionData ) ? ' AND is_disabled = 0 ' : '';

		return self::fetchProperty( 'SELECT * FROM properties WHERE id=' . ( int ) $intId . $strSql . ' AND cid =' . ( int ) $intCid, $objDatabase );
	}

	public static function fetchParentPropertiesByCompanyUserIdByPropertyIdsByCid( $intCompanyUserId, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = self::buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator = false, $boolShowEnabled = false );

		$strSql = 'SELECT * FROM
						( ' . $strSql . ' ) as SubQ
					WHERE
						id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND property_id IS NULL
					ORDER BY
						lower ( property_name ),
						order_num';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertiesWithScheduledPayments( $objClientDatabase ) {
		$strSql = 'SELECT
						DISTINCT ON ( p.property_name, p.id )
						p.*
					FROM
						properties p
						JOIN scheduled_payments sp ON ( p.cid = sp.cid AND p.id = sp.property_id )
						JOIN clients mc ON ( p.cid = mc.id )
						JOIN property_merchant_accounts pma ON ( p.cid = pma.cid AND p.id = pma.property_id )
						JOIN merchant_accounts ma ON ( ma.cid = pma.cid AND ma.id = pma.company_merchant_account_id )
					WHERE
						company_status_type_id = ' . CCompanyStatusType::CLIENT . '
						AND p.is_disabled <> 1
						AND sp.deleted_on IS NULL
						AND pma.ar_code_id IS NULL
						AND pma.disabled_by IS NULL
						AND pma.disabled_on IS NULL
						AND ma.is_disabled <> 1
					ORDER BY
						p.property_name,
						p.id';

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchPropertiesWithScheduledPaymentsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
								DISTINCT ON ( p.property_name, p.id ) p.*
							FROM
								properties p
								JOIN scheduled_payments sp ON ( p.cid = sp.cid AND p.id = sp.property_id )
								JOIN clients mc ON ( p.cid = mc.id )
								JOIN property_merchant_accounts pma ON ( p.cid = pma.cid AND p.id = pma.property_id )
							WHERE
								p.cid = ' . ( int ) $intCid . '
								AND company_status_type_id = ' . CCompanyStatusType::CLIENT . '
								AND p.is_disabled <> 1
								AND sp.deleted_on IS NULL
								AND pma.disabled_by IS NULL
								AND pma.disabled_on IS NULL
							ORDER BY
								p.property_name,
								p.id;';

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyIdsWithScheduledPaymentsByCompanyMerchantAccountIdsByCid( $intCid, $arrintCompanyMerchantAccountIds, $objClientDatabase ) {
		if( false == valArr( $arrintCompanyMerchantAccountIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT p.id
					FROM
						properties p
						JOIN scheduled_payments sp ON ( p.cid = sp.cid AND p.id = sp.property_id )
						JOIN clients mc ON ( p.cid = mc.id )
						JOIN property_merchant_accounts pma ON ( p.cid = pma.cid AND p.id = pma.property_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
						AND pma.company_merchant_account_id IN (' . implode( ',', $arrintCompanyMerchantAccountIds ) . ')
						AND pma.ar_code_id IS NULL
						AND p.is_disabled <> 1
						AND sp.deleted_on IS NULL
						AND pma.disabled_by IS NULL
						AND pma.disabled_on IS NULL';

		$arrmixProperties = ( array ) fetchData( $strSql, $objClientDatabase );

		$arrintFinalPropertyIds = [];

		foreach( $arrmixProperties as $arrintPropertyIds ) {
			$arrintFinalPropertyIds[] = $arrintPropertyIds['id'];
		}

		return $arrintFinalPropertyIds;
	}

	public static function fetchPropertiesByCidByCompanyUserId( $intCid, $intCompanyUserId, $objDatabase, $boolIsAdministrator = false, $boolReturnArray = false, $boolIsIncludeTemplateProperties = false ) {
		$strSql = self::buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator );

		$arrintExcludePropertyTypes = ( false == $boolIsIncludeTemplateProperties ) ? [ CPropertyType::SETTINGS_TEMPLATE, CPropertyType::TEMPLATE ] : [ CPropertyType::SETTINGS_TEMPLATE ];
		$strOrderByClause           = ( true == $boolIsIncludeTemplateProperties ) ? ' CASE WHEN property_type_id = ' . CPropertyType::TEMPLATE . ' THEN 0 ELSE 1 END, ' : '';

		$strSelectSql = '*';
		if( true == $boolReturnArray ) {
			$strSelectSql = 'id';
		}

		$strSql = 'SELECT ' . $strSelectSql . ' FROM
						( ' . $strSql . ' ) as SubQ
					WHERE
						is_disabled = 0
						AND cid = ' . ( int ) $intCid . '
						AND property_type_id NOT IN( ' . implode( ',', $arrintExcludePropertyTypes ) . ' )
					ORDER BY
						' . $strOrderByClause . '
						lower( property_name ) ASC,
						order_num';

		if( true == $boolReturnArray ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return self::fetchProperties( $strSql, $objDatabase );
		}

	}

	public static function fetchPropertiesWithTypeWithPmSoftwareByCid( $arrintPropertyIds, $intCid, $strPropertyNameSearch = NULL, $objDatabase, $boolReturnArray = false, $boolPropertySortbyPmSoftware = false, $arrintIntegrationClientTypeIds = [] ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strPropertyLocaleJoin     = '';
		$strSelectPropertyLocaleId = '';
		if( false == empty( CLocaleContainer::createService()->getLocaleCode() ) ) {
			$strPropertyLocaleJoin     = 'LEFT JOIN property_locales pl ON ( pl.property_id = p.id AND  pl.cid = p.cid AND pl.locale_code = \'' . CLocaleContainer::createService()->getLocaleCode() . '\' )';
			$strSelectPropertyLocaleId = 'pl.id as locale_id,';
		}

		if( true == $boolPropertySortbyPmSoftware && true == valArr( $arrintIntegrationClientTypeIds ) ) {
			$strSql = 'SELECT
							p.*,
							' . $strSelectPropertyLocaleId . '
							util_get_system_translated( \'name\', pt.name, pt.details ) AS property_type_name,
							CASE
								WHEN id.name IS NULL THEN \'' . __( 'Entrata Core and Migration' ) . '\'
								ELSE id.name
							END AS pm_software,
							CASE
								WHEN ' . COccupancyType::STUDENT . ' = ANY( p.occupancy_type_ids ) AND pp.id IS NOT NULL THEN 1
								ELSE 0
							END AS is_student,
							CASE
								WHEN p.remote_primary_key IS NOT NULL THEN 1
								ELSE 0
							END AS is_integrated,
							( SELECT array_to_string( array( SELECT locale_code FROM property_locales pl WHERE pl.cid = p.cid AND pl.property_id = p.id ), \',\' ) ) as supported_locale_codes
						FROM
							properties p
							LEFT JOIN property_types pt ON ( pt.id = p.property_type_id )
							LEFT JOIN property_integration_databases pid ON ( pid.cid = p.cid AND p.id = pid.property_id )
							LEFT JOIN integration_databases id ON ( id.cid = pid.cid AND id.id = pid.integration_database_id AND ( p.remote_primary_key IS NULL OR id.integration_client_type_id IN ( ' . implode( ',', $arrintIntegrationClientTypeIds ) . ' ) ) )
							LEFT JOIN property_preferences pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.key = \'ENABLE_SEMESTER_SELECTION\' AND pp.value IS NOT NULL ) 
							' . $strPropertyLocaleJoin . '
						WHERE
							p.is_disabled <> 1 ';

		} else {
			$strSql = 'SELECT
							p.*,
							' . $strSelectPropertyLocaleId . '
							util_get_system_translated( \'name\', pt.name, pt.details ) AS property_type_name,
							CASE
								WHEN ' . COccupancyType::STUDENT . ' = ANY( p.occupancy_type_ids ) AND pp.id IS NOT NULL THEN 1
								ELSE 0
							END AS is_student,
							CASE
								WHEN p.remote_primary_key IS NOT NULL THEN 1
								ELSE 0
							END AS is_integrated,
							( SELECT array_to_string( array( SELECT locale_code FROM property_locales pl WHERE pl.cid = p.cid AND pl.property_id = p.id ), \',\' ) ) as supported_locale_codes
						FROM
							properties p
							LEFT JOIN property_types pt ON ( pt.id = p.property_type_id )
							LEFT JOIN property_preferences pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.key = \'ENABLE_SEMESTER_SELECTION\' AND pp.value IS NOT NULL )
							' . $strPropertyLocaleJoin . '
						WHERE
							1 = 1 ';

		}

		$strSql .= 'AND p.cid = ' . ( int ) $intCid . '
					AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )';

		$strSql .= ' AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';

		if( true == valStr( $strPropertyNameSearch ) ) {
			$strSql .= 'AND p.property_name Ilike \'%' . addslashes( $strPropertyNameSearch ) . '%\'';
		}

		$strSql .= ' ORDER BY
						LOWER ( p.property_name ) ASC';

		if( true == $boolReturnArray ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return self::fetchProperties( $strSql, $objDatabase );
		}
	}

	public static function fetchPaginatedPropertiesByPropertiesFilterByPropertyIdsByCid( $intCid, $objPropertiesFilter, $objPagination, $objDatabase, $boolReturnArray = false, $boolReturnPropertyIds = false ) {

		if( true == valArr( $objPropertiesFilter->getPropertyGroupIds() ) ) {
			$strPropertyGroupsJoin = 'JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( ',', $objPropertiesFilter->getPropertyGroupIds() ) . ']::INTEGER[] ) as lp ON lp.cid = p.cid AND lp.property_id = p.id';
		} else {
			return NULL;
		}

		$strSortSql = '';

		switch( trim( $objPropertiesFilter->getSortBy() ) ) {
			case 'property_name':
				$strSortSql .= ' ORDER BY p.property_name ' . addslashes( $objPropertiesFilter->getSortDirection() );
				break;

			case 'property_type':
				$strSortSql .= ' ORDER BY property_type_name ' . addslashes( $objPropertiesFilter->getSortDirection() );
				break;

			default:
				$strSortSql .= ' ORDER BY p.property_name ASC ';
				break;
		}

		$strSql = 'SELECT DISTINCT
						p.*,
						pl.id as locale_id,
						util_get_system_translated( \'name\', pt.name, pt.details ) AS property_type_name
					FROM
						properties p
						LEFT JOIN property_types pt ON ( pt.id = p.property_type_id )
						LEFT JOIN property_locales pl ON ( pl.property_id = p.id AND  pl.cid = p.cid AND pl.locale_code = \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) ';

		if( true == $boolReturnPropertyIds ) {
			$strSql = 'SELECT
							DISTINCT p.id,
							p.property_name
						FROM
							properties p ';
		}

		$strSql .= $strPropertyGroupsJoin . ' WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ', ' . CPropertyType::CENTRAL_LEASING_OFFICE . ' )';

		if( true == valStr( $objPropertiesFilter->getPropertyName() ) ) {
			$strSql .= 'AND p.property_name Ilike \'%' . addslashes( $objPropertiesFilter->getPropertyName() ) . '%\'';
		}

		if( true == valStr( $objPropertiesFilter->getKeywordSearch() ) ) {
			$strSql .= ' AND ( p.property_name Ilike \'%' . addslashes( $objPropertiesFilter->getKeywordSearch() ) . '%\' OR p.id = ' . ( int ) addslashes( $objPropertiesFilter->getKeywordSearch() ) . ' OR p.lookup_code Ilike \'%' . addslashes( $objPropertiesFilter->getKeywordSearch() ) . '%\') ';
		}

		if( true == valArr( $objPropertiesFilter->getPropertyTypes() ) ) {
			$strSql .= ' AND p.property_type_id IN ( ' . implode( ',', $objPropertiesFilter->getPropertyTypes() ) . ' )';
		}

		if( false == ( bool ) $objPropertiesFilter->getIsShowDisabled() ) {
			$strSql .= ' AND p.is_disabled = 0';
		}
		$strSql .= ' ' . $strSortSql;

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		if( true == $boolReturnArray ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return self::fetchProperties( $strSql, $objDatabase );
		}
	}

	public static function fetchTotalPropertiesCountByCidByPropertiesFilterByPropertyIds( $intCid, $objPropertiesFilter, $objDatabase ) {

		if( true == valArr( $objPropertiesFilter->getPropertyGroupIds() ) ) {
			$strPropertyGroupsJoin = 'JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( ',', $objPropertiesFilter->getPropertyGroupIds() ) . ']::INTEGER[] ) as lp ON lp.cid = p.cid AND lp.property_id = p.id';
		} else {
			return NULL;
		}

		$strSql = 'SELECT
						COUNT( p.id ) AS COUNT
					FROM
						properties p
						' . $strPropertyGroupsJoin . '
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ', ' . CPropertyType::CENTRAL_LEASING_OFFICE . ' )';

		if( true == valStr( $objPropertiesFilter->getPropertyName() ) ) {
			$strSql .= 'AND p.property_name Ilike \'%' . addslashes( $objPropertiesFilter->getPropertyName() ) . '%\'';
		}

		if( true == valStr( $objPropertiesFilter->getKeywordSearch() ) ) {
			$strSql .= ' AND ( p.property_name Ilike \'%' . addslashes( $objPropertiesFilter->getKeywordSearch() ) . '%\' OR p.id = ' . ( int ) addslashes( $objPropertiesFilter->getKeywordSearch() ) . ' OR p.lookup_code Ilike \'%' . addslashes( $objPropertiesFilter->getKeywordSearch() ) . '%\') ';
		}

		if( true == valArr( $objPropertiesFilter->getPropertyTypes() ) ) {
			$strSql .= 'AND p.property_type_id IN ( ' . implode( ',', $objPropertiesFilter->getPropertyTypes() ) . ' )';
		}

		if( false == ( bool ) $objPropertiesFilter->getIsShowDisabled() ) {
			$strSql .= 'AND p.is_disabled = 0';
		}

		return self::fetchColumn( $strSql, 'count', $objDatabase );

	}

	public static function fetchPMSoftwareEnabledPropertiesByCompanyUserIdByCid( $intCompanyUserId, $boolIsAdministrator, $intCid, $objDatabase, $intPageNo = NULL, $intPageSize = NULL, $boolIsEnabledLateFees = false, $boolIsManagerial = false ) {
		$boolIsPaginationRequired = false;
		if( false == is_null( $intPageSize ) && false == is_null( $intPageNo ) ) {
			$intOffset                = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit                 = ( int ) $intPageSize;
			$boolIsPaginationRequired = true;
		}

		$strJoin         = '';
		$strAndCondition = '';

		if( false == $boolIsAdministrator ) {
			$strJoin .= 'JOIN view_company_user_properties vcup ON ( vcup.company_user_id = ' . ( int ) $intCompanyUserId . ' AND vcup.property_id = p.id AND vcup.cid = p.cid ) ';
		}

		if( true == $boolIsEnabledLateFees ) {
			$strJoin         .= 'JOIN property_preferences ppre ON ( pgs.cid = ppre.cid AND p.id = ppre.property_id AND ppre.key = \'ENABLED_LATE_FEES\' ) ';
			$strAndCondition .= ' AND ( ppre.value = \'1\' ) ';
		}

		if( true == $boolIsManagerial ) {
			$strAndCondition .= ' AND p.is_managerial = 1';
		}

		$strSql = 'SELECT
						DISTINCT ON( lower( p.property_name ), order_num, p.id )
						p.*,
						pgs.activate_standard_posting
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id AND p.is_disabled = 0 AND pgs.activate_standard_posting = true )
						JOIN property_products pp ON ( pgs.cid = pp.cid AND ( p.id = pp.property_id OR pp.property_id IS NULL ) )
						' . $strJoin . '
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND pp.ps_product_id = ' . CPsProduct::ENTRATA . '
						' . $strAndCondition . '
					ORDER BY
						lower( p.property_name ) ASC,
						order_num';

		if( true == $boolIsPaginationRequired ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPMSoftwareEnabledPropertiesCountByCompanyUserIdByCid( $intCompanyUserId, $boolIsAdministrator, $intCid, $objDatabase, $boolIsEnabledLateFees = false, $boolIsManagerial = false ) {
		$strJoin         = '';
		$strAndCondition = '';

		if( false == $boolIsAdministrator ) {
			$strJoin .= 'JOIN view_company_user_properties vcup ON ( vcup.company_user_id = ' . ( int ) $intCompanyUserId . ' AND vcup.property_id = p.id AND vcup.cid = p.cid ) ';
		}

		if( true == $boolIsEnabledLateFees ) {
			$strJoin         .= 'JOIN property_preferences ppre ON ( pgs.cid = ppre.cid AND p.id = ppre.property_id AND ppre.key = \'ENABLED_LATE_FEES\' ) ';
			$strAndCondition .= ' AND ( ppre.value = \'1\' ) ';
		}

		if( true == $boolIsManagerial ) {
			$strAndCondition .= ' AND p.is_managerial = 1';
		}

		$strSql = 'SELECT
						COUNT( DISTINCT p.id )
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id AND pgs.activate_standard_posting = true AND p.is_disabled = 0 )
						JOIN property_products pp ON ( p.cid = pp.cid )
						' . $strJoin . '
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND ( pp.property_id IS NULL OR p.id = pp.property_id )
						AND pp.ps_product_id = ' . CPsProduct::ENTRATA . '
						' . $strAndCondition . '';

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchPropertiesByCompanyUserByIntegrationClientTypeIdsByCid( $objCompanyUser, $arrintIntegrationClientTypeIds, $objDatabase ) {
		if( false == valArr( $arrintIntegrationClientTypeIds ) ) {
			return;
		}

		$strQuery = ' ';
		if( false == $objCompanyUser->getIsPSIUser() ) {
			$strQuery = ' AND cu.cid = p.cid ';
		}

		$strSql = 'SELECT
						p.*,
						cup.company_user_id AS company_user_id,
						CASE
							WHEN id.integration_client_type_id IS NULL OR id.integration_client_type_id IN ( ' . implode( ',', $arrintIntegrationClientTypeIds ) . ' )
							THEN 1
							ELSE 0
						END AS allow_allocations
					FROM
						public.properties p
						JOIN public.company_users cu ON ( cu.id = ' . ( int ) $objCompanyUser->getId() . $strQuery . ' AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT OUTER JOIN public.view_company_user_properties cup ON ( cup.company_user_id = ' . ( int ) $objCompanyUser->getId() . ' AND cu.id = cup.company_user_id AND ( cu.cid = cup.cid AND cu.default_company_user_id IS NULL ) AND cu.is_administrator = 0 )
						LEFT OUTER JOIN public.property_integration_databases pid ON ( pid.property_id = p.id AND pid.cid = p.cid )
						LEFT OUTER JOIN public.integration_databases id ON ( id.id = pid.integration_database_id AND id.cid = pid.cid )
					WHERE
						p.cid = ' . ( int ) $objCompanyUser->getCid() . '
						AND ( cu.is_administrator = 1 OR cup.property_id = p.id )
						AND p.is_disabled = 0
					ORDER BY
						lower ( property_name ) ASC,
						order_num';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertyIdsByCidByCompanyUserIdBySessionData( $intCid, $intCompanyUserId, $intIsDisabledSessionData, $objDatabase, $arrintPropertyIds = [], $boolIsReturnSql = false, $boolShowHistoricalAccess = false ) {
		$boolShowEnabled = ( true == is_null( $intIsDisabledSessionData ) || 0 == $intIsDisabledSessionData );
		$boolShowDisabled = ( 1 == $intIsDisabledSessionData && $boolShowHistoricalAccess ) ? true : false;

		$strSql = self::buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator = false, $boolShowEnabled, false, false, [], false, $boolShowDisabled );

		$strSql = 'SELECT id FROM
						( ' . $strSql . ' ) as SubQ
					ORDER BY
						lower ( property_name ) ASC,
						order_num';

		if( true == $boolIsReturnSql ) {
			return $strSql;
		}

		$arrmixProperties = ( array ) fetchData( $strSql, $objDatabase );

		$arrintFinalPropertyIds = [];

		foreach( $arrmixProperties as $arrintPropertyIds ) {
			$arrintFinalPropertyIds[] = $arrintPropertyIds['id'];
		}

		return $arrintFinalPropertyIds;
	}

	public static function fetchParentPropertyIdsByCidByCompanyUserIdBySessionData( $intCid, $intCompanyUserId, $intIsDisabledSessionData, $objDatabase, $boolIsAdministrator = false ) {
		$boolShowEnabled = ( true == is_null( $intIsDisabledSessionData ) || 0 == $intIsDisabledSessionData );

		$strSql = self::buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator, $boolShowEnabled );

		$strSql = 'SELECT id FROM
						( ' . $strSql . ' ) as SubQ
					WHERE
						property_id IS NULL
						AND cid = ' . ( int ) $intCid . '
					ORDER BY
						lower ( property_name ) ASC,
						order_num';

		$arrmixProperties = ( array ) fetchData( $strSql, $objDatabase );

		$arrintPropertyIds = [];

		foreach( $arrmixProperties as $arrmixProperty ) {
			$arrintPropertyIds[] = $arrmixProperty['id'];
		}

		return $arrintPropertyIds;
	}

	public static function fetchCustomPropertiesByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT
						*
					FROM
						properties
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_disabled = 0
						AND id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					ORDER BY
						property_name ASC';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertiesDataByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT
						id,property_name
					FROM
						properties
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_disabled = 0
						AND id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					ORDER BY
						property_name ASC';

		$strSqlResult = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $strSqlResult ) ) ? rekeyArray( 'id', $strSqlResult ) : NULL;
	}

	public static function fetchCustomPropertiesByIds( $arrintPropertyIds, $objDatabase, $boolActive = true ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		if( true == $boolActive ) {
			$strWhereCondition = ' AND is_disabled = 0 ';
		}

		$strSql = 'SELECT * FROM properties WHERE id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' . $strWhereCondition . '
						ORDER BY
							property_name ASC';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertiesByIdsByCids( $arrintPropertyIds, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						properties
					WHERE
						is_disabled = 0 AND
						id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND
						cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
					ORDER BY
						property_name ASC';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesWithDatabaseIdByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.*,
						mc.database_id
					FROM
						properties p,
						clients mc
					WHERE
						p.cid = mc.id
						AND	p.is_disabled = 0
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
						p.property_name ASC';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesWithPrimaryAddressByPropertyIdsByWebsiteIdByCid( $arrintPropertyIds, $intWebsiteId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.*,
						wp.more_info_action,
						pa.city
					FROM
						properties p
						LEFT JOIN website_properties wp ON ( wp.cid = p.cid AND wp.property_id = p.id )
						LEFT JOIN property_addresses pa ON ( pa.cid = p.cid AND pa.property_id = p.id AND pa.address_type_id = ' . CAddressType::PRIMARY . ' AND pa.is_alternate = false )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						AND	p.is_disabled = 0
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					ORDER BY
						p.property_name ASC';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchAllPropertiesByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strQuery = '';
		if( false == is_null( $intCid ) ) {
			$strQuery = ' AND cid = ' . ( int ) $intCid;
		}
		$strSql = 'SELECT * FROM properties WHERE id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' . $strQuery . '
						ORDER BY
							property_name ASC';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCustomAllPropertiesByIds( $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT * FROM properties WHERE id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						ORDER BY
							property_name ASC';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesWithRemotePrimaryKeyNotNullByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM properties WHERE cid = ' . ( int ) $intCid . ' AND remote_primary_key IS NOT NULL
						ORDER BY
							lower ( property_name )';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertyByWebsiteIdByIdByCid( $intWebsiteId, $intPropertyId, $intCid, $objClientDatabase ) {
		if( false == isset ( $intWebsiteId ) || false == is_numeric( $intWebsiteId ) ) {
			return NULL;
		}
		if( false == isset ( $intPropertyId ) || false == is_numeric( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.*
					FROM
						properties AS p,
						website_properties AS wp
					WHERE
						p.id = wp.property_id
						AND p.cid = wp.cid
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						AND p.id = ' . ( int ) $intPropertyId . '
						AND p.cid = ' . ( int ) $intCid . '
					LIMIT 1';

		return self::fetchProperty( $strSql, $objClientDatabase );
	}

	public static function fetchPropertiesByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $objPropertyFilter = NULL, $boolShowDisabledData = false, $boolFetchGlPostMonth = false ) {
		$arrstrAdditionalAndStatements = [ '1 = 1 ' ];
		$strOrderBy = NULL;
		if( false == valIntArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSelect              = '';
		$strJoinSql             = '';
		$strDisabledCondition   = '';

		if( true == $boolShowDisabledData ) {
			$strJoinSql = ' JOIN property_products pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.ps_product_id = ' . \CPsProduct::HISTORICAL_ACCESS . ') ';
			$strDisabledCondition = ' AND p.is_disabled = 1 ';
		}

		if( true == isset ( $objPropertyFilter ) ) {

			if( 1 == $objPropertyFilter->getParentOnly() ) {
				$arrstrAdditionalAndStatements[] = ' p.property_id IS NULL ';
			}

			if( true == is_numeric( $objPropertyFilter->getExludePropertyId() ) ) {
				$arrstrAdditionalAndStatements[] = ' p.id <> ' . ( int ) $objPropertyFilter->getExludePropertyId();
			}

			if( 1 == $objPropertyFilter->getHideDisabled() && true != $boolShowDisabledData ) {
				$arrstrAdditionalAndStatements[] = ' p.is_disabled <> 1 ';
			}

			if( false == is_null( $objPropertyFilter->getPropertyName() ) && 0 < strlen( $objPropertyFilter->getPropertyName() ) && 'Search Properties' != trim( $objPropertyFilter->getPropertyName() ) ) {
				$arrstrAdditionalAndStatements[] = ' p.property_name ILIKE E\'%' . $objPropertyFilter->sqlPropertyName() . '%\'';
			}

			if( false != $objPropertyFilter->getExcludeZeroUnitCount() ) {
				$arrstrAdditionalAndStatements[] = ' p.number_of_units > 0 ';
			}

			if( false != $objPropertyFilter->getExcludeIsManagerial() ) {
				$arrstrAdditionalAndStatements[] = ' p.is_managerial = 0 ';
			}

			if( false != $objPropertyFilter->getSortParentFirst() ) {
				$strOrderBy = ' p.property_id ASC nulls FIRST, ';
			}
		}

		if( true == $boolFetchGlPostMonth ) {
			$strSelect  = ', to_char( pgs.gl_post_month, \'MM/YYYY\') AS gl_post_month ';
			$strJoinSql .= ' JOIN property_gl_settings pgs ON ( p.id = pgs.property_id AND p.cid = pgs.cid ) ';
		}

		$strSql = 'SELECT
						p.*
						' . $strSelect . '
					FROM
						properties p
						' . $strJoinSql . '
					WHERE
						p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.cid = ' . ( int ) $intCid . '
						AND ' . implode( ' AND ', $arrstrAdditionalAndStatements ) . '
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						' . $strDisabledCondition . '
					ORDER BY
						' . $strOrderBy . '
						lower ( p.property_name ) ASC,
						p.order_num';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchNonMigratedPropertiesByIdsByProductIdByCid( $arrintPropertyIds, $intPsProductId, $intCid, $objDatabase ) {
		if( ( false == valArr( $arrintPropertyIds ) ) || ( false == is_numeric( $intPsProductId ) ) ) {
			return false;
		}
		$strSql = 'SELECT
						p.*
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid and pgs.property_id = p.id ) 
						JOIN property_products pp ON ( p.cid = pp.cid )
					WHERE
						p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.cid = ' . ( int ) $intCid . '
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						AND p.is_disabled = 0
						AND pgs.is_ar_migration_mode = false 
						AND pgs.is_ap_migration_mode = false
						AND ( p.id = pp.property_id )
						AND pp.ps_product_id = ' . ( int ) $intPsProductId . '
					ORDER BY
						lower ( p.property_name ) ASC,
						p.order_num';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertyDetailsByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						id,
						property_name
					FROM
						properties
					WHERE
						id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ', ' . CPropertyType::CLIENT . ' )
						AND is_disabled <> 1
					ORDER BY
						lower ( property_name ) ASC,
						order_num';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchPropertyDetailsByIdsByPropertyFilterByCid( $arrintPropertyIds, $intCid, $objDatabase, $objPropertyFilter = NULL ) {

		$arrstrAdditionalAndStatements = [ '1 = 1 ' ];

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		if( true == isset ( $objPropertyFilter ) ) {
			if( false == is_null( $objPropertyFilter->getPropertyName() ) && 0 < strlen( $objPropertyFilter->getPropertyName() ) && 'Search Properties' != trim( $objPropertyFilter->getPropertyName() ) ) {
				$arrstrAdditionalAndStatements[] = ' property_name ILIKE E\'%' . $objPropertyFilter->sqlPropertyName() . '%\'';
			}

			if( 1 == $objPropertyFilter->getHideDisabled() ) {
				$arrstrAdditionalAndStatements[] = ' is_disabled <> 1 ';
			}

			if( 1 == $objPropertyFilter->getParentOnly() ) {
				$arrstrAdditionalAndStatements[] = ' property_id IS NULL ';
			}

			if( true == is_numeric( $objPropertyFilter->getExludePropertyId() ) ) {
				$arrstrAdditionalAndStatements[] = ' id <> ' . ( int ) $objPropertyFilter->getExludePropertyId();
			}
		}

		$strSql = '	SELECT
						id, property_name, remote_primary_key
					FROM
						properties
					WHERE
						id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND ' . implode( ' AND ', $arrstrAdditionalAndStatements ) . '
					ORDER BY
						lower ( property_name ) ASC,
						order_num';

		$strSqlResult = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $strSqlResult ) ) ? rekeyArray( 'id', $strSqlResult ) : NULL;
	}

	public static function fetchActivePropertiesByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $objPropertyFilter = NULL ) {
		$arrstrAdditionalAndStatements = [ '1 = 1 ' ];
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		if( true == isset ( $objPropertyFilter ) ) {
			if( false == is_null( $objPropertyFilter->getPropertyName() ) && 0 < strlen( $objPropertyFilter->getPropertyName() ) && 'Search Properties' != trim( $objPropertyFilter->getPropertyName() ) ) {
				$arrstrAdditionalAndStatements[] = ' p.property_name ILIKE E\'%' . $objPropertyFilter->sqlPropertyName() . '%\'';
			}

			if( 1 == $objPropertyFilter->getHideDisabled() ) {
				$arrstrAdditionalAndStatements[] = ' p.is_disabled <> 1 ';
			}

			if( 1 == $objPropertyFilter->getParentOnly() ) {
				$arrstrAdditionalAndStatements[] = ' p.property_id IS NULL ';
			}

			if( true == is_numeric( $objPropertyFilter->getExludePropertyId() ) ) {
				$arrstrAdditionalAndStatements[] = ' p.id <> ' . ( int ) $objPropertyFilter->getExludePropertyId();
			}
		}

		$strSql = 'SELECT
						p.*,
                        to_char( pgs.gl_post_month, \'MM/YYYY\') AS gl_post_month
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( p.id = pgs.property_id AND p.cid = pgs.cid )
					WHERE
						p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.cid = ' . ( int ) $intCid . '
						AND ' . implode( ' AND ', $arrstrAdditionalAndStatements ) . '
						AND pgs.activate_standard_posting = true
					ORDER BY
						lower ( property_name ) ASC,
						order_num';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesWithActiveCompanyApplicationsByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.*
					FROM
						properties p
						INNER JOIN property_applications pa ON ( p.cid = pa.cid AND p.id = pa.property_id AND pa.is_published = 1 )
						INNER JOIN company_applications ca ON ( ca.cid = pa.cid AND ca.id = pa.company_application_id AND ca.is_published = 1 AND ca.deleted_on IS NULL )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.is_disabled <> 1
					ORDER BY
						lower ( property_name ) ASC,
						order_num';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPaginatedPropertiesByIdsByCid( $intPageNo, $intPageSize, $arrintPropertyIds, $intCid, $objDatabase, $objPropertyFilter = NULL ) {
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$arrstrAdditionalAndStatements = [ '1 = 1 ' ];
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		if( true == isset ( $objPropertyFilter ) ) {
			if( false == is_null( $objPropertyFilter->getPropertyName() ) && 0 < strlen( $objPropertyFilter->getPropertyName() ) && 'Find a Property...' != trim( $objPropertyFilter->getPropertyName() ) ) {
				$arrstrAdditionalAndStatements[] = ' property_name ILIKE E\'%' . $objPropertyFilter->sqlPropertyName() . '%\'';
			}

			if( 1 == $objPropertyFilter->getHideDisabled() ) {
				$arrstrAdditionalAndStatements[] = ' is_disabled <> 1 AND termination_date IS NULL ';
			}

			if( 1 == $objPropertyFilter->getParentOnly() ) {
				$arrstrAdditionalAndStatements[] = ' property_id IS NULL ';
			}

			if( true == is_numeric( $objPropertyFilter->getExludePropertyId() ) ) {
				$arrstrAdditionalAndStatements[] = ' id <> ' . ( int ) $objPropertyFilter->getExludePropertyId();
			}

			if( false != $objPropertyFilter->getExcludeZeroUnitCount() ) {
				$arrstrAdditionalAndStatements[] = ' number_of_units > 0 ';
			}

			if( false != $objPropertyFilter->getExcludeIsManagerial() ) {
				$arrstrAdditionalAndStatements[] = ' is_managerial = 0 ';
			}
		}

		$strSql = 'SELECT
						*
					FROM
						properties
					WHERE
						id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND ' . implode( ' AND ', $arrstrAdditionalAndStatements ) . '
					ORDER BY
						lower ( property_name ) ASC, order_num
					OFFSET
						' . ( int ) $intOffset . '
					LIMIT
						' . ( int ) $intLimit;

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesWithOfficePhoneNumberByScheduledCallIdByCid( $intScheduledCallId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						DISTINCT ON ( p.id )
						p.*,
						ppn.phone_number AS office_phone_number
					FROM
						properties p,
						property_scheduled_calls psc,
						property_phone_numbers ppn
					WHERE
						p.id = psc.property_id
						AND p.cid = psc.cid
						AND p.id = ppn.property_id
						AND p.cid = ppn.cid
						AND ppn.phone_number_type_id = ' . CPhoneNumberType::OFFICE . '
						AND psc.scheduled_call_id = ' . ( int ) $intScheduledCallId . '
						AND p.cid = ' . ( int ) $intCid;

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchPropertiesWithOfficePhoneNumberByScheduledCallIdByPropertyIdsByCid( $intScheduledCallId, $arrintPropertyIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON ( p.id )
						p.*,
						ppn.phone_number AS office_phone_number
					FROM
						properties p,
						property_scheduled_calls psc,
						property_phone_numbers ppn
					WHERE
						p.id = psc.property_id
						AND p.cid = psc.cid
						AND p.id = ppn.property_id
						AND p.cid = ppn.cid
						AND ppn.phone_number_type_id = ' . CPhoneNumberType::OFFICE . '
						AND psc.scheduled_call_id = ' . ( int ) $intScheduledCallId . '
						AND p.id IN ( ' . implode( ',', array_keys( $arrintPropertyIds ) ) . ' )
						AND p.cid = ' . ( int ) $intCid;

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyCountByWebsiteIdByCid( $intWebsiteId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						count(distinct p.id)
					FROM
						properties p,
						website_properties wp
					WHERE
						p.id = wp.property_id
						AND p.cid = wp.cid
						AND ( wp.hide_on_resident_portal = 0 OR wp.hide_on_resident_portal IS NULL )
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						ANd p.cid = ' . ( int ) $intCid;

		$arrstrData = fetchData( $strSql, $objClientDatabase );

		if( true == valArr( $arrstrData ) && true == isset ( $arrstrData[0]['count'] ) && 0 < $arrstrData[0]['count'] ) {
			return $arrstrData[0]['count'];
		}

		return 0;
	}

	public static function filterRestrictedWords( $strPropertyName ) {
		$strPropertyName = preg_replace( '/^the\s+|^a\s+|^an\s+|^of\s+/', '', ( string ) $strPropertyName );

		if( preg_match( '/^the\s+|^a\s+|^an\s+|^of\s+/', $strPropertyName ) ) {
			return self::filterRestrictedWords( $strPropertyName );
		} else {
			return $strPropertyName;
		}
	}

	public static function buildCorePortalSearchSqlByCid( $intWebsiteId, $objPropertySearch, $intPsProductId, $intCid, $boolParentOnly = true, $boolIsCustomSearch = false, $boolGetPropertyCount = false ) {
		// IF YOU CHANGE THIS FUNCTION YOU HAVE TO TEST FOR QUERY SPEED

		$arrstrAdditionalWhereParameters = [];

		$boolJoinOnPropertyCategory = false;

		// This variable stores whether user is searching for something besides location. If there is a
		// search where property name, or something of that nature is entered, we show all properties matching those results
		// Here are the rest of the search parameters

		if( true == isset( $objPropertySearch ) ) {
			$strPropertyNameToSearch = \Psi\CStringService::singleton()->preg_replace( '/[^a-zA-Z0-9]/', '_', html_entity_decode( $objPropertySearch->getPropertyName() ) );

			if( false == is_null( $objPropertySearch->getPropertyName() ) && 0 != strlen( trim( $objPropertySearch->getPropertyName() ) ) ) {
				array_push( $arrstrAdditionalWhereParameters, 'p.property_name ILIKE \'%' . addslashes( trim( $strPropertyNameToSearch ) ) . '%\'' );
			}

			if( true == is_numeric( $objPropertySearch->getPropertyTypeId() ) ) {
				array_push( $arrstrAdditionalWhereParameters, 'p.property_type_id=' . ( int ) $objPropertySearch->getPropertyTypeId() );
			}

			if( true == valStr( $objPropertySearch->getPropertyGroupType() ) && true == valStr( $objPropertySearch->getPropertyGroupName() ) ) {
				array_push( $arrstrAdditionalWhereParameters, 'pgt.name = \'' . addslashes( $objPropertySearch->getPropertyGroupType() ) . '\'
						AND pg.name = \'' . addslashes( $objPropertySearch->getPropertyGroupName() ) . '\'' );
			}

			if( false == is_null( $objPropertySearch->getAllowDogs() ) && 0 != strlen( trim( $objPropertySearch->getAllowDogs() ) ) ) {
				array_push( $arrstrAdditionalWhereParameters, 'p.allows_dogs=1' );
			}

			if( false == is_null( $objPropertySearch->getAllowCats() ) && 0 != strlen( trim( $objPropertySearch->getAllowCats() ) ) ) {
				array_push( $arrstrAdditionalWhereParameters, 'p.allows_cats=1' );
			}

			if( false == is_null( $objPropertySearch->getAllowSmokingOnly() ) && 2 != $objPropertySearch->getAllowSmokingOnly() && 0 != strlen( trim( $objPropertySearch->getAllowSmokingOnly() ) ) ) {
				array_push( $arrstrAdditionalWhereParameters, 'pd.allows_smoking=' . ( int ) $objPropertySearch->getAllowSmokingOnly() );
			}

			if( false == is_null( $objPropertySearch->getPetFriendly() ) && 0 != strlen( trim( $objPropertySearch->getPetFriendly() ) ) ) {
				array_push( $arrstrAdditionalWhereParameters, '( p.allows_cats=1 OR p.allows_dogs=1 )' );
			}

			if( true == is_numeric( $objPropertySearch->getCompanyCategoryId() ) ) {
				array_push( $arrstrAdditionalWhereParameters, 'pc.company_category_id = ' . ( int ) $objPropertySearch->getCompanyCategoryId() . ' AND pc.is_published = 1' );
				$boolJoinOnPropertyCategory = true;
			}

			if( false == is_null( $objPropertySearch->getCountryCode() ) && 0 != strlen( trim( $objPropertySearch->getCountryCode() ) ) ) {
				array_push( $arrstrAdditionalWhereParameters, 'pa.country_code=\'' . addslashes( $objPropertySearch->getCountryCode() ) . '\'' );
			}

			if( false == is_null( $objPropertySearch->getStateCode() ) && 0 != strlen( trim( $objPropertySearch->getStateCode() ) ) && false == is_numeric( $objPropertySearch->getCompanyAreaId() ) && false == is_numeric( $objPropertySearch->getCompanyRegionId() ) ) {
				array_push( $arrstrAdditionalWhereParameters, '( pa.state_code=\'' . addslashes( $objPropertySearch->getStateCode() ) . '\' OR co_area.state_code = \'' . addslashes( $objPropertySearch->getStateCode() ) . '\')' );
			}

			if( false == is_null( $objPropertySearch->getCities() ) && true == valArr( $objPropertySearch->getCities() ) ) {

				$intCout = 0;
				$strSql  = '';

				foreach( $objPropertySearch->getCities() as $strCity ) {

					$intCout++;

					if( 1 == $intCout ) {
						$strSql = $strSql . 'pa.city ILIKE \'%' . addslashes( $strCity ) . '%\'';
					} else {
						$strSql = $strSql . ' OR pa.city ILIKE \'%' . addslashes( $strCity ) . '%\'';
					}
				}

				array_push( $arrstrAdditionalWhereParameters, '( ' . $strSql . ' )' );

			} elseif( false == is_null( $objPropertySearch->getCity() ) && 0 != strlen( trim( $objPropertySearch->getCity() ) ) ) {
				array_push( $arrstrAdditionalWhereParameters, 'pa.city ILIKE \'%' . addslashes( $objPropertySearch->getCity() ) . '%\'' );
			}

			// If miles redius is set that means it's a new search and we don't have to search by postal code. In new search we look by miles.

			if( false == is_numeric( $objPropertySearch->getMilesRadius() ) && ( ( false == $objPropertySearch->getIsMultipleCountry() && true == is_numeric( $objPropertySearch->getPostalCode() ) ) || true == $objPropertySearch->getIsMultipleCountry() ) && 0 != strlen( trim( $objPropertySearch->getPostalCode() ) ) && 'Zip Code' != $objPropertySearch->getPostalCode() ) {
				array_push( $arrstrAdditionalWhereParameters, 'pa.postal_code=\'' . addslashes( trim( $objPropertySearch->getPostalCode() ) ) . '\'' );
			}

			if( true == $objPropertySearch->getSearchExactBedBath() ) {

				if( false == is_null( $objPropertySearch->getNumberOfBedrooms() ) && true == is_numeric( $objPropertySearch->getNumberOfBedrooms() ) ) {
					array_push( $arrstrAdditionalWhereParameters, '( p.min_bedrooms = ' . ( int ) $objPropertySearch->getNumberOfBedrooms() . ' OR p.max_bedrooms = ' . ( int ) $objPropertySearch->getNumberOfBedrooms() . ' )' );
				}

				if( false == is_null( $objPropertySearch->getNumberOfBathrooms() ) && true == is_numeric( $objPropertySearch->getNumberOfBathrooms() ) ) {
					array_push( $arrstrAdditionalWhereParameters, '( p.min_bathrooms = ' . ( float ) $objPropertySearch->getNumberOfBathrooms() . ' OR p.max_bathrooms = ' . ( float ) $objPropertySearch->getNumberOfBathrooms() . ' )' );
				}

			} else {

				if( false == is_null( $objPropertySearch->getNumberOfBedrooms() ) && true == is_numeric( $objPropertySearch->getNumberOfBedrooms() ) ) {
					array_push( $arrstrAdditionalWhereParameters, 'COALESCE( p.min_bedrooms, 0 ) <= ' . ( int ) $objPropertySearch->getNumberOfBedrooms() );
				}

				if( false == is_null( $objPropertySearch->getNumberOfBedrooms() ) && true == is_numeric( $objPropertySearch->getNumberOfBedrooms() ) ) {
					array_push( $arrstrAdditionalWhereParameters, 'COALESCE( p.max_bedrooms, p.min_bedrooms, 0 ) >= ' . ( int ) $objPropertySearch->getNumberOfBedrooms() );
				}

				if( false == is_null( $objPropertySearch->getNumberOfBathrooms() ) && true == is_numeric( $objPropertySearch->getNumberOfBathrooms() ) ) {
					array_push( $arrstrAdditionalWhereParameters, 'COALESCE( p.min_bathrooms, 0 ) <= ' . ( float ) $objPropertySearch->getNumberOfBathrooms() );
				}

				if( false == is_null( $objPropertySearch->getNumberOfBathrooms() ) && true == is_numeric( $objPropertySearch->getNumberOfBathrooms() ) ) {
					array_push( $arrstrAdditionalWhereParameters, 'COALESCE( p.max_bathrooms, p.min_bathrooms, 0 ) >= ' . ( float ) $objPropertySearch->getNumberOfBathrooms() );
				}
			}

			$arrstrMinRentWhereParameters = [];
			$arrstrMaxRentWhereParameters = [];
			$arrstrRentWhereParameters    = [];

			if( 0 <= $objPropertySearch->getMinRent() && true == is_numeric( $objPropertySearch->getMinRent() ) ) {
				// array_push( $arrstrMinRentWhereParameters, 'p.min_rent >= ' . ( int ) $objPropertySearch->getMinRent() );
				array_push( $arrstrMaxRentWhereParameters, 'COALESCE( p.max_rent, 0 ) >= ' . ( int ) $objPropertySearch->getMinRent() );
			}

			// In new property search we have drop down options like 1981-1990, 1991-2000 etc.
			if( 0 < strlen( $objPropertySearch->getYearBuilt() ) ) {

				$arrintBuiltYear = explode( '-', $objPropertySearch->getYearBuilt() );

				if( true == valArr( $arrintBuiltYear ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrintBuiltYear ) ) {
					array_push( $arrstrAdditionalWhereParameters, 'p.year_built >= \'1/1/' . ( int ) $arrintBuiltYear[0] . '\'::date' );
					array_push( $arrstrAdditionalWhereParameters, 'p.year_built <= \'1/1/' . ( int ) $arrintBuiltYear[1] . '\'::date' );
				}
			}

			if( true == is_numeric( $objPropertySearch->getMaxRent() ) && 0 <= $objPropertySearch->getMaxRent() && $objPropertySearch->getMaxRent() <= 3000 ) {
				// previous condition was checking min_rent <= $_REQUEST[max_rent] which was giving wrong result when property min_rent is above $3000, Task ID: 151614
				$strMaxRentCondition = 'COALESCE( p.min_rent, 0 ) <= ' . ( int ) $objPropertySearch->getMaxRent();

				if( 3000 == $objPropertySearch->getMaxRent() ) {
					$strMaxRentCondition = '( COALESCE( p.min_rent, 0 ) <= ' . ( int ) $objPropertySearch->getMaxRent() . ' OR p.max_rent >= ' . ( int ) $objPropertySearch->getMaxRent() . ' )';
				}

				array_push( $arrstrMinRentWhereParameters, $strMaxRentCondition );
			}

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrMinRentWhereParameters ) ) {
				array_push( $arrstrRentWhereParameters, ' ( ' . implode( ' AND ', $arrstrMinRentWhereParameters ) . ' ) ' );
			}

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrMaxRentWhereParameters ) ) {
				array_push( $arrstrRentWhereParameters, ' ( ' . implode( ' AND ', $arrstrMaxRentWhereParameters ) . ' ) ' );
			}

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrRentWhereParameters ) ) {
				array_push( $arrstrAdditionalWhereParameters, ' ( ' . implode( ' AND ', $arrstrRentWhereParameters ) . ' ) ' );
			}

			$arrstrMinSquareFeetWhereParameters = [];
			$arrstrMaxSquareFeetWhereParameters = [];
			$arrstrSquareFeetWhereParameters    = [];

			if( 0 != $objPropertySearch->getMinSquareFeet() && true == is_numeric( $objPropertySearch->getMinSquareFeet() ) ) {
				array_push( $arrstrMinSquareFeetWhereParameters, 'p.min_square_feet >= ' . ( int ) $objPropertySearch->getMinSquareFeet() );
				array_push( $arrstrMaxSquareFeetWhereParameters, 'p.max_square_feet >= ' . ( int ) $objPropertySearch->getMinSquareFeet() );
			}

			if( true == is_numeric( $objPropertySearch->getMaxSquareFeet() ) ) {
				array_push( $arrstrMinSquareFeetWhereParameters, 'p.min_square_feet <= ' . ( int ) $objPropertySearch->getMaxSquareFeet() );
				array_push( $arrstrMaxSquareFeetWhereParameters, 'p.max_square_feet <= ' . ( int ) $objPropertySearch->getMaxSquareFeet() );
			}

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrMinSquareFeetWhereParameters ) ) {
				array_push( $arrstrSquareFeetWhereParameters, ' ( ' . implode( ' AND ', $arrstrMinSquareFeetWhereParameters ) . ' ) ' );
			}

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrMaxSquareFeetWhereParameters ) ) {
				array_push( $arrstrSquareFeetWhereParameters, ' ( ' . implode( ' AND ', $arrstrMaxSquareFeetWhereParameters ) . ' ) ' );
			}

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrSquareFeetWhereParameters ) ) {
				array_push( $arrstrAdditionalWhereParameters, ' ( ' . implode( ' OR ', $arrstrSquareFeetWhereParameters ) . ' ) ' );
			}

			$arrstrMinBedroomsWhereParameters = [];
			$arrstrMaxBedroomsWhereParameters = [];
			$arrstrBedroomsWhereParameters    = [];

			if( 0 != $objPropertySearch->getMinBedrooms() && true == is_numeric( $objPropertySearch->getMinBedrooms() ) ) {
				array_push( $arrstrMinBedroomsWhereParameters, 'p.min_bedrooms >= ' . ( int ) $objPropertySearch->getMinBedrooms() );
				array_push( $arrstrMaxBedroomsWhereParameters, 'p.max_bedrooms >= ' . ( int ) $objPropertySearch->getMinBedrooms() );
			}

			if( true == is_numeric( $objPropertySearch->getMaxBedrooms() ) ) {
				array_push( $arrstrMinBedroomsWhereParameters, 'p.min_bedrooms <= ' . ( int ) $objPropertySearch->getMaxBedrooms() );
				array_push( $arrstrMaxBedroomsWhereParameters, 'p.max_bedrooms <= ' . ( int ) $objPropertySearch->getMaxBedrooms() );
			}

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrMinBedroomsWhereParameters ) ) {
				array_push( $arrstrBedroomsWhereParameters, ' ( ' . implode( ' AND ', $arrstrMinBedroomsWhereParameters ) . ' ) ' );
			}

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrMaxBedroomsWhereParameters ) ) {
				array_push( $arrstrBedroomsWhereParameters, ' ( ' . implode( ' AND ', $arrstrMaxBedroomsWhereParameters ) . ' ) ' );
			}

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrBedroomsWhereParameters ) ) {
				array_push( $arrstrAdditionalWhereParameters, ' ( ' . implode( ' OR ', $arrstrBedroomsWhereParameters ) . ' ) ' );
			}

			$arrstrMinBathroomsWhereParameters = [];
			$arrstrMaxBathroomsWhereParameters = [];
			$arrstrBathroomsWhereParameters    = [];

			if( 0 != $objPropertySearch->getMinBathrooms() && true == is_numeric( $objPropertySearch->getMinBathrooms() ) ) {
				array_push( $arrstrMinBathroomsWhereParameters, 'p.min_bathrooms >= ' . ( int ) $objPropertySearch->getMinBathrooms() );
				array_push( $arrstrMaxBathroomsWhereParameters, 'p.max_bathrooms >= ' . ( int ) $objPropertySearch->getMinBathrooms() );
			}

			if( true == is_numeric( $objPropertySearch->getMaxBathrooms() ) ) {
				array_push( $arrstrMinBathroomsWhereParameters, 'p.min_bathrooms <= ' . ( int ) $objPropertySearch->getMaxBathrooms() );
				array_push( $arrstrMaxBathroomsWhereParameters, 'p.max_bathrooms <= ' . ( int ) $objPropertySearch->getMaxBathrooms() );
			}

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrMinBathroomsWhereParameters ) ) {
				array_push( $arrstrBathroomsWhereParameters, ' ( ' . implode( ' AND ', $arrstrMinBathroomsWhereParameters ) . ' ) ' );
			}

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrMaxBathroomsWhereParameters ) ) {
				array_push( $arrstrBathroomsWhereParameters, ' ( ' . implode( ' AND ', $arrstrMaxBathroomsWhereParameters ) . ' ) ' );
			}

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrBathroomsWhereParameters ) ) {
				array_push( $arrstrAdditionalWhereParameters, ' ( ' . implode( ' OR ', $arrstrBathroomsWhereParameters ) . ' ) ' );
			}
		}

		// Added extra sub query to find out miles.
		if( false == $boolGetPropertyCount ) {
			$strSql = ' SELECT
								id,
								cid,
								property_type_id,
								property_name,
								vaultware_number,
								year_built,
								number_of_units,
								allows_cats,
								allows_dogs,
								community_website_uri,
								corporate_website_uri,
								is_featured,
								hide_more_info_button,
								show_application_fee_option,
								more_info_action,
								min_rent,
								max_rent,
								min_bedrooms,
								max_bedrooms,
								min_bathrooms,
								max_bathrooms,
								min_square_feet,
								max_square_feet,
								city AS city,
								city AS seo_city,
								state_code,
								short_description,
								full_description,
								property_application_count';

			if( true == isset( $objPropertySearch ) && 0 < strlen( $objPropertySearch->getLatitude() ) && 0 < strlen( $objPropertySearch->getLongitude() ) && 0 < $objPropertySearch->getMilesRadius() ) {
				$strSql .= ',miles';
			}

			$strSql .= '	FROM ( ';

			$strSql .= ' SELECT
									DISTINCT ON ( p.id )
									p.id,
									p.cid,
									p.property_type_id,
									p.property_name,
									p.vaultware_number,
									p.year_built,
									p.number_of_units,
									p.allows_cats,
									p.allows_dogs,
									pd.community_website_uri,
									pd.corporate_website_uri,
									pd.is_featured,
									wp.hide_more_info_button,
									wp.show_application_fee_option,
									wp.more_info_action,
									p.min_rent,
									p.max_rent,
									p.min_bedrooms,
									p.max_bedrooms,
									p.min_bathrooms,
									p.max_bathrooms,
									p.min_square_feet,
									p.max_square_feet,
									p.short_description,
									p.full_description,
									pa.city,
									pa.state_code,
									COUNT ( c_app.id ) AS property_application_count';

			if( true == isset( $objPropertySearch ) && 'sort_number' == $objPropertySearch->getSearchResultSortBy() ) {
				$strSql .= ', CASE WHEN ( 0 = wp.order_num ) THEN NULL ELSE wp.order_num END AS priority_search_order_num';
				$strSql .= ', CASE WHEN ( 0 = p.order_num ) THEN NULL ELSE p.order_num END AS modified_order_num';
			}
		} else {
			$strSql = ' SELECT COUNT( id ) from ( SELECT DISTINCT ON ( p.id ) p.id ';
		}
		if( true == isset( $objPropertySearch ) && 0 < $objPropertySearch->getMilesRadius() && 0 < strlen( $objPropertySearch->getLatitude() ) && 0 < strlen( $objPropertySearch->getLongitude() ) ) {

			$strSql .= ',
									CASE
									WHEN ( pa.latitude::Float = ' . ( double ) $objPropertySearch->getLatitude() . ' AND pa.longitude::Float = ' . ( double ) $objPropertySearch->getLongitude() . ' )
									THEN 0
									ELSE(
										3958.75 * ACos( Sin( ' . ( double ) $objPropertySearch->getLatitude() . ' / 57.2958 ) *
										Sin( pa.latitude::Float / 57.2958 ) +
										Cos( ' . ( double ) $objPropertySearch->getLatitude() . ' / 57.2958 ) *
										Cos( pa.latitude::Float / 57.2958 ) *
										Cos( pa.longitude::Float / 57.2958 - ' . ( double ) $objPropertySearch->getLongitude() . ' / 57.2958 ) )
									)
									END AS miles ';
		}

		$strSql .= ' FROM properties AS p ';

		if( true == isset( $objPropertySearch ) && true == valStr( $objPropertySearch->getPropertyGroupType() ) && true == valStr( $objPropertySearch->getPropertyGroupName() ) ) {
			$strSql .= ' LEFT JOIN property_group_associations AS pga ON ( pga.cid = p.cid AND p.id = pga.property_id )
									LEFT JOIN property_groups AS pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
									LEFT JOIN property_group_types AS pgt ON ( pgt.cid = pg.cid AND pg.property_group_type_id = pgt.id AND pgt.deleted_by IS NULL AND pgt.deleted_on IS NULL )';
		}

		if( true == isset( $objPropertySearch ) && true == $objPropertySearch->getShowAvailableOnly() ) {
			$strAvailablePropertiesSql = ' AND EXISTS ( SELECT 1 FROM unit_spaces us WHERE us.cid = wp.cid AND us.property_id = wp.property_id AND us.is_available AND us.deleted_on IS NULL ) ';
		}

		$strSql .= ' JOIN property_details AS pd ON ( p.id = pd.property_id AND p.cid = pd.cid )
								JOIN website_properties AS wp ON ( p.id = wp.property_id AND p.cid = wp.cid ' . $strAvailablePropertiesSql . ' ) ';

		// If filter indicates we should only show properties with available units, join on available units.
		if( true == isset( $objPropertySearch ) ) {
			if( ( true == $objPropertySearch->getShowAvailableOnly() && 0 < strlen( $objPropertySearch->getMoveInDate() ) ) || ( true == $objPropertySearch->getShowAvailableOnly() && 0 == strlen( $objPropertySearch->getMoveInDate() ) ) ) {
				$strSql .= 'JOIN property_units pu ON ( pu.property_id = p.id AND pu.cid = p.cid AND pu.deleted_on IS NULL AND pu.cid = ' . ( int ) $intCid . ' )';

			} elseif( false == $objPropertySearch->getShowAvailableOnly() && 0 < strlen( $objPropertySearch->getMoveInDate() ) ) {
				$strSql .= 'JOIN property_units pu ON ( pu.property_id = p.id AND pu.cid = p.cid AND pu.deleted_on IS NULL )';
			}
		}

		if( true == isset( $objPropertySearch ) && true == valStr( $objPropertySearch->getBrandOptions() ) ) {
			if( 'avalon' == $objPropertySearch->getBrandOptions() ) {
				$strSql .= ' LEFT OUTER JOIN property_preferences pp ON ( pp.property_id = p.id AND pp.cid = p.cid ) AND pp.key = \'BRAND_OPTIONS\'';
			} else {
				$strSql .= ' JOIN property_preferences pp ON ( pp.property_id = p.id AND pp.cid = p.cid ) AND pp.key = \'BRAND_OPTIONS\' AND pp.value = \'' . $objPropertySearch->getBrandOptions() . '\'';
			}
		}

		// If this has a company area filter
		// #1.) If company_area_id and company_region_id are NULL, only get properties that are not in a region or area of this selected state.
		// #2.) If company_region_id is set, don't show property unless it has a direct association to that region.
		// #3.) If company_area_id is set, don't show property, unless it has a direct association to that area.

		if( true == isset( $objPropertySearch ) && true == is_numeric( $objPropertySearch->getCid() ) && true == valArr( $objPropertySearch->getCompanyRegionIds() ) && 1 < \Psi\Libraries\UtilFunctions\count( $objPropertySearch->getCompanyRegionIds() ) ) {
			// If both are set, we only search off of company area id
			$strSql .= ' JOIN property_areas AS pr_area ON ( p.id = pr_area.property_id AND p.cid = pr_area.cid )';

			$strSql .= ' JOIN company_areas AS co_area ON ( pr_area.company_area_id = co_area.id
																		AND pr_area.cid = co_area.cid
																		AND (
																			co_area.id IN ( ' . implode( ',', $objPropertySearch->getCompanyRegionIds() ) . ' )
																			AND co_area.cid = ' . ( int ) $intCid . '
																			OR co_area.company_area_id IN ( ' . implode( ',', $objPropertySearch->getCompanyRegionIds() ) . ' )
																			OR co_area.company_area_id IN ( SELECT id FROM company_areas WHERE company_area_id IN ( ' . implode( ',', $objPropertySearch->getCompanyRegionIds() ) . ' ) AND cid = ' . ( int ) $intCid . ' )
																			 )
																	 )';

		} elseif( true == isset( $objPropertySearch ) && true == is_numeric( $objPropertySearch->getCompanyRegionId() ) ) {

			// If both are set, we only search off of company area id
			$strSql .= ' JOIN property_areas AS pr_area ON ( p.id = pr_area.property_id AND p.cid = pr_area.cid )';

			if( true == is_numeric( $objPropertySearch->getCompanySubAreaId() ) ) {

				$strSql .= ' JOIN company_areas AS co_area ON ( pr_area.company_area_id = co_area.id AND pr_area.cid = co_area.cid AND co_area.id = ' . ( int ) $objPropertySearch->getCompanySubAreaId() . ' AND co_area.cid = ' . ( int ) $intCid . ' )';
			} elseif( true == is_numeric( $objPropertySearch->getCompanyAreaId() ) ) {

				$strSql .= ' JOIN company_areas AS co_area ON ( pr_area.company_area_id = co_area.id AND pr_area.cid = co_area.cid AND ( co_area.id = ' . ( int ) $objPropertySearch->getCompanyAreaId() . ' OR co_area.company_area_id = ' . ( int ) $objPropertySearch->getCompanyAreaId() . ' AND co_area.cid = ' . ( int ) $intCid . ' ) )';
			} else {
				$strSql .= ' JOIN company_areas AS co_area ON ( pr_area.company_area_id = co_area.id
																		AND pr_area.cid = co_area.cid
																		AND (
																			co_area.id = ' . ( int ) $objPropertySearch->getCompanyRegionId() . '
																			AND co_area.cid = ' . ( int ) $intCid . '
																			OR co_area.company_area_id = ' . ( int ) $objPropertySearch->getCompanyRegionId() . '
																			OR co_area.company_area_id IN ( SELECT id FROM company_areas WHERE company_area_id = ' . ( int ) $objPropertySearch->getCompanyRegionId() . ' AND cid = ' . ( int ) $intCid . ' )
																			 )
																	 )';
			}

		} else {
			if( true == isset( $objPropertySearch ) && 0 != strlen( trim( $objPropertySearch->getStateCode() ) ) ) {

				$strSql .= 'LEFT OUTER JOIN property_areas AS pr_area ON ( p.id = pr_area.property_id AND p.cid = pr_area.cid )
										LEFT OUTER JOIN company_areas AS co_area ON ( pr_area.company_area_id = co_area.id AND pr_area.cid = co_area.cid )';
			}
		}

		if( true == $boolJoinOnPropertyCategory ) {
			$strSql .= ' JOIN property_categories AS pc ON ( pc.property_id = p.id AND pc.cid = p.cid ) ';
		}

		$strSql .= ' JOIN property_addresses AS pa ON ( p.id = pa.property_id AND p.cid = pa.cid AND pa.address_type_id = ' . CAddressType::PRIMARY . ' AND pa.cid = ' . ( int ) $intCid . ' AND pa.is_alternate = false ) ';

		$strSql .= ' LEFT OUTER JOIN property_applications AS p_app ON ( p_app.property_id = p.id AND p_app.cid = p.cid )
							 LEFT OUTER JOIN company_applications AS c_app ON ( c_app.id = p_app.company_application_id AND c_app.cid = p_app.cid AND c_app.is_published = 1 AND c_app.deleted_on IS NULL )';

		if( true == isset ( $objPropertySearch ) ) {
			if( false == is_null( $objPropertySearch->getCommunityDefaultAmenityIds() ) && false == is_null( $objPropertySearch->getApartmentDefaultAmenityIds() ) ) {
				$mixSearchByAmenity = $objPropertySearch->getCommunityDefaultAmenityIds() . ',' . $objPropertySearch->getApartmentDefaultAmenityIds();
			} elseif( false == is_null( $objPropertySearch->getCommunityDefaultAmenityIds() ) ) {
				$mixSearchByAmenity = $objPropertySearch->getCommunityDefaultAmenityIds();
			} else {
				$mixSearchByAmenity = $objPropertySearch->getApartmentDefaultAmenityIds();
			}
		}

		if( true == isset ( $objPropertySearch ) ) {
			if( true == isset( $mixSearchByAmenity ) && false == is_null( $objPropertySearch->getDefaultAmenityIds() ) ) {
				$mixSearchByAmenity = $mixSearchByAmenity . ',' . $objPropertySearch->getDefaultAmenityIds();
			} elseif( false == is_null( $objPropertySearch->getDefaultAmenityIds() ) ) {
				$mixSearchByAmenity = $objPropertySearch->getDefaultAmenityIds();
			}
		}

		if( true == isset( $mixSearchByAmenity ) && false == empty( $mixSearchByAmenity ) ) {
			if( false == $boolIsCustomSearch ) {
				$strSql .= ' JOIN rate_associations AS ra ON ( ra.property_id = p.id AND ra.cid = p.cid )
									JOIN amenities AS a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id AND a.cid = ' . ( int ) $intCid . ' )
									JOIN amenity_filters AS af ON (
																	CASE WHEN af.default_amenity_id IN ( 1000, 1001 )
																		THEN ( af.id = a.amenity_filter_id AND af.id IN ( ' . $mixSearchByAmenity . ' ) AND af.cid = a.cid )
																	ELSE
																		( af.default_amenity_id = a.default_amenity_id AND af.id IN ( ' . $mixSearchByAmenity . ' ) AND af.cid = a.cid )
																	END )';
			} else {
				$strSql .= ' JOIN rate_associations AS ra ON ( ra.property_id = p.id AND ra.cid = p.cid )
									JOIN amenities AS a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id AND a.default_amenity_id IN ( ' . $mixSearchByAmenity . ' ) AND a.cid = ' . ( int ) $intCid . ' )';
			}
		}

		// This section is specifically used for MAC so availability can be checked based on the move in date.
		// If the move in date they are looking for doesn't show any available units, the property is excluded from the search.

		if( true == isset( $objPropertySearch ) && 0 < strlen( $objPropertySearch->getMoveInDate() ) && 'Move-in Date' != addslashes( $objPropertySearch->getMoveInDate() ) ) {

			$strMoveInDate = $objPropertySearch->getMoveInDate();

			if( false == CValidation::validateDate( $objPropertySearch->getMoveInDate() ) ) {
				$strMoveInDate = date( 'm/d/Y' );
			}

			$intMoveInDate = strtotime( $strMoveInDate );

			if( $intMoveInDate < time() ) {
				$strMoveInDate = date( 'm/d/Y' );
			}

			$strMoveInParameters = ' AND ( us.reserve_until IS NULL OR us.reserve_until <= CURRENT_DATE )';

			$strMoveInParameters .= \Psi\Eos\Entrata\CUnitSpaces::createService()->buildUnitSpaceAvailabilityConditionSql( $strMoveInDate, $boolShowOnWebsite = false );

			$strSql .= ' JOIN unit_spaces AS us ON ( us.property_unit_id = pu.id AND us.cid = pu.cid AND us.deleted_on IS NULL AND us.unit_space_status_type_id IN ( ' . implode( ',', CUnitSpaceStatusType::$c_arrintAvailableUnitSpaceStatusTypes ) . ' ) AND us.cid = ' . ( int ) $intCid . ' )
								 JOIN property_charge_settings pcs ON ( pcs.cid = us.cid AND pcs.property_id = us.property_id ' . $strMoveInParameters . ' ) ';
		}

		// $strPortalTypeFieldReference = ( 'portal' == $boolApplicationLayer ) ? 'wp.hide_on_prospect_portal' : 'wp.hide_on_resident_portal';

		switch( $intPsProductId ) {

			case 'portal':
				$strPortalTypeFieldReference = 'wp.hide_on_prospect_portal';
				break;

			default:
				$strPortalTypeFieldReference = 'wp.hide_on_resident_portal';
				break;
		}

		$strSql .= '
						WHERE ';

// 				 if( 0 < $objPropertySearch->getMilesRadius() ) {
// 					$strSql .= ' miles <= ' . $objPropertySearch->getMilesRadius() . ' AND ';
// 				}

		$strSql .= ' wp.website_id = ' . ( int ) $intWebsiteId . '
							AND wp.cid = ' . ( int ) $intCid . '
							AND ' . $strPortalTypeFieldReference . ' != 1
							AND p.is_disabled <> 1 ';

		if( true == $boolParentOnly ) {
			$strSql .= ' AND p.property_id IS NULL ';
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAdditionalWhereParameters ) ) {
			$strSql .= ' AND ' . implode( ' AND ', $arrstrAdditionalWhereParameters );
		}

		if( true == isset( $objPropertySearch ) && true == valStr( $objPropertySearch->getBrandOptions() ) && 'avalon' == $objPropertySearch->getBrandOptions() ) {
			$strSql .= ' AND ( pp.key IS NULL OR pp.value IS NULL ) ';
		}

		if( false == $boolGetPropertyCount ) {
			$strSql .= '
						GROUP BY
							p.id,
							p.cid,
							p.property_type_id,
							p.property_name,
							p.vaultware_number,
							p.year_built,
							p.number_of_units,
							p.allows_cats,
							p.allows_dogs,
							p.min_rent,
							p.max_rent,
							p.min_square_feet,
							p.max_square_feet,
							p.min_bedrooms,
							p.max_bedrooms,
							p.min_bathrooms,
							p.max_bathrooms,
							p.short_description,
							p.full_description,
							pa.city,
							pa.state_code,
							pd.community_website_uri,
							pd.corporate_website_uri,
							pd.is_featured,
							wp.hide_more_info_button,
							wp.show_application_fee_option,
							wp.more_info_action,
							pa.latitude,
							pa.longitude';

			if( true == isset( $objPropertySearch ) && 'sort_number' == $objPropertySearch->getSearchResultSortBy() ) {
				$strSql .= ', priority_search_order_num, modified_order_num ';
			}
		} elseif( true == isset( $objPropertySearch ) && 0 < $objPropertySearch->getMilesRadius() && 0 < strlen( $objPropertySearch->getLatitude() ) && 0 < strlen( $objPropertySearch->getLongitude() ) ) {
			$strSql .= ' GROUP BY p.id,
								 pa.latitude,
								 pa.longitude';
		} else {
			$strSql .= ' GROUP BY p.id';
		}

		$strSql .= ' ) AS SUB_QUERY1 ';

		if( true == isset( $objPropertySearch ) && 0 < $objPropertySearch->getMilesRadius() && 0 < strlen( $objPropertySearch->getLatitude() ) && 0 < strlen( $objPropertySearch->getLongitude() ) ) {
			$strSql .= ' WHERE miles < ' . ( int ) $objPropertySearch->getMilesRadius();
		}

		if( false == $boolGetPropertyCount ) {

			$strSql .= ' ORDER BY';

			if( true == isset( $objPropertySearch ) && 0 < $objPropertySearch->getMilesRadius() && 0 < strlen( $objPropertySearch->getLatitude() ) && 0 < strlen( $objPropertySearch->getLongitude() ) ) {
				$strSql .= ' miles, ';
			}

			if( true == isset( $objPropertySearch ) && 'sort_number' == $objPropertySearch->getSearchResultSortBy() ) {
				$strSql .= ' priority_search_order_num ASC, modified_order_num ASC,';
			}

			if( true == isset( $objPropertySearch ) && 'is_featured' == $objPropertySearch->getSearchResultSortBy() ) {
				$strSql .= ' is_featured DESC,';
			}

			if( true == isset( $objPropertySearch ) && 'price_low_to_high' == $objPropertySearch->getSearchResultSortBy() ) {
				$strSql .= ' min_rent ASC,';
			}

			if( true == isset( $objPropertySearch ) && 'price_high_to_low' == $objPropertySearch->getSearchResultSortBy() ) {
				$strSql .= ' max_rent DESC,';
			}

			$strSql .= ' LOWER( property_name ) ASC';
		}

		return $strSql;
	}

	public static function fetchAllowedProspectPortalPropertiesByWebsiteIdByCid( $intSelectedPage, $intCountPerPage, $intWebsiteId, $objPropertySearch, $boolApplicationLayer, $intCid, $objDatabase, $boolParentOnly = true, $boolIsCustomSearch = false ) {
		$strSql = self::buildCorePortalSearchSqlByCid( $intWebsiteId, $objPropertySearch, $boolApplicationLayer, $intCid, $boolParentOnly, $boolIsCustomSearch );

		if( 0 != $intSelectedPage && 0 != $intCountPerPage ) {
			$strSql .= ' OFFSET ' . ( int ) ( ( $intSelectedPage - 1 ) * $intCountPerPage ) . ' LIMIT ' . ( int ) $intCountPerPage;
		}

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchAllAllowedProspectPortalPropertiesByWebsiteIdByCid( $intWebsiteId, $objPropertySearch, $boolApplicationLayer, $intCid, $objDatabase, $boolParentOnly = true ) {
		$strSql = self::buildCorePortalSearchSqlByCid( $intWebsiteId, $objPropertySearch, $boolApplicationLayer, $intCid, $boolParentOnly );

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchAllowedPropertyCountByWebsiteIdByCid( $intWebsiteId, $objPropertySearch, $boolApplicationLayer, $intCid, $objDatabase, $boolParentOnly = true, $boolIsCustomSearch = false ) {
		$strSql = self::buildCorePortalSearchSqlByCid( $intWebsiteId, $objPropertySearch, $boolApplicationLayer, $intCid, $boolParentOnly, $boolIsCustomSearch, $boolGetPropertyCount = true );

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrintResponse[0]['count'] ) ) {
			return $arrintResponse[0]['count'];
		}

		return 0;
	}

	public static function fetchPropertyByRemotePrimaryKeyByCid( $strRemotePrimaryKey, $intCid, $objDatabase ) {
		$strSql = 'SELECT *
					FROM properties
					WHERE cid = ' . ( int ) $intCid . '
					AND lower ( remote_primary_key ) = \'' . ( string ) addslashes( \Psi\CStringService::singleton()->strtolower( trim( $strRemotePrimaryKey ) ) ) . '\'';

		return self::fetchProperty( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByCid( $intCid, $objDatabase, $arrintPropertyIds = NULL, $boolShowDisabledData = false ) {

		$strWhereCondition = $strOrderby = '';
		if( true == $boolShowDisabledData ) {
			$strOrderby .= ' is_disabled,';
		}
		if( true == valArr( $arrintPropertyIds ) ) {
			$strWhereCondition = ' AND id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}

		$strOrderby .= ' lower ( property_name )';

		$strSql = 'SELECT
						*
					FROM
						properties
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strWhereCondition . '
						' . ( ( false == $boolShowDisabledData ) ? ' AND is_disabled = 0 ' : '' ) . '
						AND property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
					ORDER BY
					' . $strOrderby;

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByCidData( $intCid, $objDatabase, $strPropertyIds = NULL ) {

		$strWhereCondition = '';
		if( true == valStr( $strPropertyIds ) ) {
			$strWhereCondition = ' AND id IN ( ' . $strPropertyIds . ' ) ';
		}

		$strSql = 'SELECT
						id,
						cid,
						property_name
					FROM
						properties
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_disabled = 0
						AND property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						' . $strWhereCondition . ' 
					ORDER BY
						lower ( property_name )';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		return $arrmixResult;
	}

	public static function fetchPropertyIdsByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						id
					FROM
						properties
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_disabled = 0
						AND property_type_id NOT IN ( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' ) ';

		$arrmixProperties = fetchData( $strSql, $objDatabase );

		$arrintFinalPropertyIds = [];

		if( true == valArr( $arrmixProperties ) ) {
			foreach( $arrmixProperties as $arrintPropertyIds ) {
				$arrintFinalPropertyIds[] = $arrintPropertyIds['id'];
			}
		}

		return $arrintFinalPropertyIds;

	}

	public static function fetchPropertiesByRemotePrimaryKeysByCid( $arrstrRemotePrimaryKeys, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrRemotePrimaryKeys ) ) {
			return NULL;
		}
		$arrstrRemotePrimaryKeys = array_flip( array_change_key_case( array_flip( $arrstrRemotePrimaryKeys ) ) );

		$strSql = 'SELECT *
					FROM properties
					WHERE cid = ' . ( int ) $intCid . '
					AND is_disabled = 0
					AND lower( remote_primary_key ) IN (\'' . implode( "','", $arrstrRemotePrimaryKeys ) . '\')';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchAllPropertiesByCid( $intCid, $objDatabase, $boolOrderByIsdisabled = false ) {
		$strOrderByClause = ' ORDER BY
										lower ( property_name )';

		if( false != $boolOrderByIsdisabled ) {
			$strOrderByClause = ' ORDER BY
										is_disabled,
										lower ( property_name )';
		}

		$strWhere = CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE;

		$strSql = 'SELECT * FROM properties	WHERE cid = ' . ( int ) $intCid . ' AND property_type_id NOT IN(' . $strWhere . ' ) ' . $strOrderByClause;

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPaginatedPropertiesAndCountByCid( $intCid, $objDatabase, $objPagination = NULL ) {
		if( false == is_numeric( $intCid ) ) {
			return NULL;
		}

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSelectSql = 'SELECT count(p.id) over() as row_count, p.* ';
			$strOffsetSql = ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		} else {
			return NULL;
		}

		$strFromSql = 'FROM
						properties p
					WHERE
						p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' ) AND p.cid = ' . ( int ) $intCid . ' ' . $strCondition . '';

		$strSql = $strSelectSql . $strFromSql . ' ORDER BY LOWER( property_name ) ASC' . $strOffsetSql;

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchAllPmEnabledPropertiesByCids( $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) {
			return;
		}

		$strSql = 'SELECT
						p.*
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( pgs.property_id = p.id AND pgs.cid = p.cid )
					WHERE
						pgs.activate_standard_posting = true
						AND p.cid IN ( ' . implode( ',', $arrintCids ) . ' )';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchActivePmEnabledPropertiesByCid( $intCid, $objDatabase, $arrintPropertyGroupIds = [] ) {
		if( false == valId( $intCid ) ) {
			return;
		}

		$strPropertyGroupIds = ( true == valArr( $arrintPropertyGroupIds ) ) ? implode( ', ', $arrintPropertyGroupIds ) : '';

		$strSql = '
			SELECT
				p.*
			FROM
				properties p
				JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INT[], ARRAY[ ' . $strPropertyGroupIds . ' ]::INT[], ARRAY[ ' . CPsProduct::ENTRATA . ' ]::INT[], TRUE ) lp ON lp.cid = p.cid AND lp.property_id = p.id
			WHERE
				p.cid = ' . ( int ) $intCid . '
				AND p.is_disabled = 0
			ORDER BY
				p.property_name';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCustomAllPropertiesBySemPropertyStatusTypeIds( $arrintSemPropertyStatusTypeIds, $objDatabase ) {
		$strSql = 'SELECT
						p.*, mc.database_id
					FROM
						properties AS p
						JOIN clients mc ON ( mc.id = p.cid )
						JOIN sem_property_details spd ON ( spd.property_id = p.id )
					WHERE
						p.is_disabled <> 1
						AND sem_property_status_type_id IN ( ' . implode( ',', $arrintSemPropertyStatusTypeIds ) . ' )
					ORDER BY
						mc.database_id ASC';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchEnabledPropertyCountByCid( $intCid, $objDatabase, $intCompanyStatusTypeId = NULL ) {
		$strSql = 'SELECT count( p.id )';

		if( false == is_null( $intCompanyStatusTypeId ) ) {
			$strSql .= ' FROM
							properties p,
							clients mc
						WHERE
							p.cid = ' . ( int ) $intCid . '
							AND p.cid = mc.id
							AND mc.company_status_type_id = ' . ( int ) $intCompanyStatusTypeId;
		} else {
			$strSql .= ' FROM properties p WHERE p.cid = ' . ( int ) $intCid;
		}

		$strSql .= ' AND p.is_disabled = 0';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) {
			return $arrintResponse[0]['count'];
		}

		return 0;
	}

	public static function fetchPropertiesByPropertyPreferenceKeyByPropertyPreferenceValueByCid( $strKey, $strValue, $intCid, $objClientDatabase, $arrintPropertyIds = NULL ) {
		$strPropertyIds = ( true == valArr( $arrintPropertyIds ) ) ? ' AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ') ' : '';

		$strSql = 'SELECT
						p.*
					FROM
						properties p
						JOIN property_preferences pp ON ( pp.property_id = p.id AND pp.cid = p.cid )
					WHERE
						p.cid = ' . ( int ) $intCid . $strPropertyIds . '
						AND pp.key = \'' . addslashes( $strKey ) . '\'
						AND pp.value = \'' . addslashes( $strValue ) . '\'
					ORDER BY
						lower ( p.property_name ) ASC,
						p.order_num';

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchPropertiesByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objClientDatabase ) {
		if( false == is_numeric( $intIntegrationDatabaseId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON ( p.property_name, p.id )
						p.*
					FROM
						properties p,
						property_integration_databases pid,
						integration_databases id
					WHERE
						p.id = pid.property_id
						AND p.cid = pid.cid
						AND pid.integration_database_id = id.id
						AND pid.cid = id.cid
						AND id.id = ' . ( int ) $intIntegrationDatabaseId . '
						ANd id.cid = ' . ( int ) $intCid . '
					ORDER BY
						p.property_name, p.id';

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchCustomPropertiesByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objClientDatabase ) {
		if( false == is_numeric( $intIntegrationDatabaseId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON ( p.property_name, p.id )
						p.*
					FROM
						properties p,
						property_integration_databases pid,
						integration_databases id
					WHERE
						p.id = pid.property_id
						AND p.cid = pid.cid
						AND pid.integration_database_id = id.id
						AND pid.cid = id.cid
						AND id.id = ' . ( int ) $intIntegrationDatabaseId . '
						ANd id.cid = ' . ( int ) $intCid . '
					ORDER BY
						p.property_name, p.id';

		$strSqlResult = fetchData( $strSql, $objClientDatabase );

		return ( true == valArr( $strSqlResult ) ) ? rekeyArray( 'id', $strSqlResult ) : NULL;
	}

	public static function fetchPaginatedPropertiesByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objPagination, $boolIsCount = false, $objClientDatabase ) {
		$strOffestSql = '';
		if( true == $boolIsCount ) {
			$strSelectSql = 'SELECT count( p.id ) AS count ';

		} else {
			if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {

				$strSelectSql = 'SELECT
							DISTINCT ON ( p.property_name, p.id )
							p.*';
				$strOffestSql = ' ORDER BY p.property_name, p.id
							OFFSET ' . ( int ) $objPagination->getOffset() . '
							LIMIT ' . ( int ) $objPagination->getPageSize();
			}
		}

		$strSql = $strSelectSql . 'FROM
						properties p,
						property_integration_databases pid,
						integration_databases id
						WHERE
							p.id = pid.property_id
							AND p.cid = pid.cid
							AND pid.integration_database_id = id.id
							AND pid.cid = id.cid
							AND id.id = ' . ( int ) $intIntegrationDatabaseId . '
							ANd id.cid = ' . ( int ) $intCid . $strOffestSql;

		if( true == $boolIsCount ) {
			$arrintResponse = fetchData( $strSql, $objClientDatabase );
			if( true == isset ( $arrintResponse[0]['count'] ) ) {
				return $arrintResponse[0]['count'];
			}
		} else {
			return self::fetchProperties( $strSql, $objClientDatabase );
		}

	}

	public static function fetchActivePropertiesByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objClientDatabase ) {
		if( false == is_numeric( $intIntegrationDatabaseId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON ( p.property_name, p.id )
						p.*
					FROM
						properties p,
						property_integration_databases pid,
						integration_databases id
					WHERE
						p.id = pid.property_id
						AND p.cid = pid.cid
						AND pid.integration_database_id = id.id
						AND pid.cid = id.cid
						AND id.id = ' . ( int ) $intIntegrationDatabaseId . '
						AND id.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
					ORDER BY
						p.property_name, p.id';

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchActivePropertyRPKsByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objClientDatabase ) {
		if( false == is_numeric( $intIntegrationDatabaseId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON ( p.id )
						p.id,
						p.remote_primary_key
					FROM
						properties p,
						property_integration_databases pid,
						integration_databases id
					WHERE
						p.id = pid.property_id
						AND p.cid = pid.cid
						AND pid.integration_database_id = id.id
						AND pid.cid = id.cid
						AND id.id = ' . ( int ) $intIntegrationDatabaseId . '
						AND id.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
					ORDER BY
						p.id';

		$arrmixData = fetchData( $strSql, $objClientDatabase );

		$arrmixProperties = [];

		if( true == valArr( $arrmixData ) ) {
			foreach( $arrmixData as $arrintProperty ) {
				$arrmixProperties[$arrintProperty['remote_primary_key']] = $arrintProperty['id'];
			}
		}

		return $arrmixProperties;
	}

	public static function fetchDisabledPropertiesByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objClientDatabase ) {
		if( false == is_numeric( $intIntegrationDatabaseId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON (p.id) p.*
					FROM
						properties p
						JOIN property_integration_databases pid ON ( p.id = pid.property_id AND p.cid = pid.cid )
						JOIN integration_databases id ON ( id.id = pid.integration_database_id AND id.cid = pid.cid )
					WHERE
						id.id = ' . ( int ) $intIntegrationDatabaseId . '
						AND id.cid = ' . ( int ) $intCid . '
						AND p.remote_primary_key IS NOT NULL
						AND p.is_disabled = 1
					ORDER BY p.id';

		return parent::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyByIntegrationDatabaseIdByRemotePrimaryKeyByCid( $intIntegrationDatabaseId, $strRemotePrimaryKey, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						p.*
					FROM
						properties p,
						property_integration_databases pid,
						integration_databases id
					WHERE
						p.id = pid.property_id
						AND p.cid = pid.cid
						AND pid.integration_database_id = id.id
						AND pid.cid = id.cid
						AND lower ( p.remote_primary_key ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( trim( $strRemotePrimaryKey ) ) ) . '\'
						AND id.id = ' . ( int ) $intIntegrationDatabaseId . '
						AND id.cid = ' . ( int ) $intCid . '
					ORDER BY
						p.property_name, p.id';

		return self::fetchProperty( $strSql, $objClientDatabase );
	}

	public static function fetchSeparatelyAssociatedPropertiesByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objClientDatabase ) {
		if( false == is_numeric( $intIntegrationDatabaseId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON ( p.id )
						p.*
					FROM
						properties p,
						property_integration_databases pid,
						integration_databases id
					WHERE
						p.id = pid.property_id
						AND p.cid = pid.cid
						AND pid.integration_database_id = id.id
						AND pid.cid = id.cid
						AND id.id <> ' . ( int ) $intIntegrationDatabaseId . '
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
						p.id';

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchSeparatelyAssociatedPropertyIdsByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objClientDatabase ) {
		if( false == is_numeric( $intIntegrationDatabaseId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT p.id
					FROM
						properties p,
						property_integration_databases pid,
						integration_databases id
					WHERE
						p.id = pid.property_id
						AND p.cid = pid.cid
						AND pid.integration_database_id = id.id
						AND pid.cid = id.cid
						AND id.id <> ' . ( int ) $intIntegrationDatabaseId . '
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
						p.id';

		$arrintPropertyIds = fetchData( $strSql, $objClientDatabase );

		return $arrintPropertyIds;
	}

	public static function fetchAssociatedPropertiesByIntegrationClientTypeIdsByCid( $arrintIntegrationClientTypeIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintIntegrationClientTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON ( p.id )
						p.*
					FROM
						properties p,
						property_integration_databases pid,
						integration_databases id
					WHERE
						p.id = pid.property_id
						AND p.cid = pid.cid
						AND pid.integration_database_id = id.id
						AND pid.cid = id.cid
						AND id.integration_client_type_id IN ( ' . implode( ',', $arrintIntegrationClientTypeIds ) . ' )
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
						p.id';

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchAssociatedPropertyIdsByIntegrationClientTypeIdsByCid( $arrintIntegrationClientTypeIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintIntegrationClientTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id
					FROM
						properties p,
						property_integration_databases pid,
						integration_databases id
					WHERE
						p.id = pid.property_id
						AND p.cid = pid.cid
						AND pid.integration_database_id = id.id
						AND pid.cid = id.cid
						AND id.integration_client_type_id IN ( ' . implode( ',', $arrintIntegrationClientTypeIds ) . ' )
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
						p.id';

		$arrmixProperties = fetchData( $strSql, $objClientDatabase );

		$arrintAssociatedPropertyIds = [];
		if( true == valArr( $arrmixProperties ) ) {
			foreach( $arrmixProperties as $arrintPropertyIds ) {
				$arrintAssociatedPropertyIds[$arrintPropertyIds['id']] = $arrintPropertyIds['id'];
			}
		}

		return $arrintAssociatedPropertyIds;
	}

	public static function fetchPropertyByWebsiteIdByRemotePrimaryKeyByCid( $intWebsiteId, $strRemotePrimaryKey, $intCid, $objClientDatabase ) {
		if( false == isset ( $intWebsiteId ) || false == is_numeric( $intWebsiteId ) ) {
			return NULL;
		}
		if( false == isset ( $strRemotePrimaryKey ) || false == is_numeric( $strRemotePrimaryKey ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.*
					FROM
						properties AS p,
						website_properties AS wp
					WHERE
						p.id = wp.property_id
						AND p.cid = wp.cid
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						AND wp.cid = ' . ( int ) $intCid . '
						AND p.remote_primary_key = \'' . addslashes( $strRemotePrimaryKey ) . '\'
					LIMIT 1';

		return self::fetchProperty( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyReportDataByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.lookup_code,
						p.full_description,
						( SELECT COUNT( id ) FROM property_concierge_services pcs WHERE pcs.cid = p.cid AND pcs.property_id = p.id AND pcs.cid = ' . ( int ) $intCid . ' ) AS property_concierge_services_count,
						( SELECT COUNT( id ) FROM property_buildings pb WHERE pb.cid = p.cid AND pb.property_id = p.id AND pb.deleted_on IS NULL AND pb.cid = ' . ( int ) $intCid . ' ) AS property_buildings_count,
						( SELECT COUNT( id ) FROM property_units pu WHERE pu.cid = p.cid AND pu.deleted_on IS NULL AND pu.property_id = p.id AND pu.cid = ' . ( int ) $intCid . ' ) AS property_units_count,
						( SELECT COUNT( id ) FROM property_applications pa WHERE pa.cid = p.cid AND pa.property_id = p.id AND pa.cid = ' . ( int ) $intCid . ' AND pa.company_application_id IN ( SELECT id FROM company_applications WHERE deleted_on IS NULL AND is_published = 1 AND cid = ' . ( int ) $intCid . ' ) ) AS property_applications_count,
						( SELECT COUNT( id ) FROM marketing_media_associations mma WHERE mma.cid = p.cid AND mma.reference_id = p.id AND mma.cid = ' . ( int ) $intCid . ' AND mma.marketing_media_sub_type_id = ' . CMarketingMediaSubType::PHOTO . ' AND deleted_by IS NULL AND deleted_on IS NULL ) AS property_photos_count,
						( SELECT COUNT( id ) FROM marketing_media_associations mma WHERE mma.cid = p.cid AND mma.reference_id = p.id AND mma.cid = ' . ( int ) $intCid . ' AND mma.marketing_media_sub_type_id = ' . CMarketingMediaSubType::PROPERTY_360_TOUR . ' AND deleted_by IS NULL AND deleted_on IS NULL ) AS property_virtual_tours_count,
						( SELECT COUNT( id ) FROM marketing_media_associations mma WHERE mma.cid = p.cid AND mma.reference_id = p.id AND mma.cid = ' . ( int ) $intCid . ' AND mma.marketing_media_sub_type_id = ' . CMarketingMediaSubType::PROPERTY_VIDEO . ' AND deleted_by IS NULL AND deleted_on IS NULL ) AS property_videos_count,
						( SELECT COUNT( id ) FROM property_floorplans pf WHERE pf.cid = p.cid AND pf.property_id = p.id AND pf.deleted_on IS NULL AND pf.cid = ' . ( int ) $intCid . ' ) AS property_floorplans_count,
						( SELECT COUNT( id ) FROM unit_types ut WHERE ut.cid = p.cid AND ut.property_id = p.id AND ut.cid = ' . ( int ) $intCid . ' ) AS property_unit_types_count,
						( SELECT COUNT( id ) FROM property_lead_sources pls WHERE pls.cid = p.cid AND pls.deleted_by IS NULL AND pls.deleted_on IS NULL AND pls.property_id = p.id AND pls.cid = ' . ( int ) $intCid . ') AS property_lead_sources_count,
						( SELECT
							COUNT( a.id )
						FROM
							amenities a
							JOIN rate_associations ra ON ( a.cid = ra.cid AND a.id = ra.ar_origin_reference_id )
						WHERE
							ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
							AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
							AND ra.ar_cascade_reference_id = p.id
							AND a.amenity_type_id = ' . CAmenityType::COMMUNITY . '
							AND ra.cid = ' . ( int ) $intCid . ' ) AS community_amenities_count,
						( SELECT COUNT( id ) FROM property_maintenance_locations pml WHERE pml.cid = p.cid AND pml.property_id = p.id AND pml.cid = ' . ( int ) $intCid . ' ) AS property_maintenance_locations_count,
						( SELECT COUNT( id ) FROM property_maintenance_problems pmp WHERE pmp.cid = p.cid AND pmp.property_id = p.id AND pmp.cid = ' . ( int ) $intCid . ' ) AS property_maintenance_problems_count,
						( SELECT
							COUNT( a.id )
						FROM
							amenities a
							JOIN rate_associations ra ON ( a.cid = ra.cid AND a.id = ra.ar_origin_reference_id )
						WHERE
							ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
							AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
							AND ra.property_id = p.id
							AND a.amenity_type_id = ' . CAmenityType::APARTMENT . '
							AND ra.cid = ' . ( int ) $intCid . ' ) AS apartment_amenities_count,
						( SELECT COUNT( id ) FROM property_preferences WHERE cid = p.cid AND property_id = p.id AND cid = ' . ( int ) $intCid . ' AND key IN ( \'MANAGER_CONTACT_NOTIFICATION_EMAIL\', \'MAINTENANCE_NOTIFICATION_EMAIL\', \'PAYMENT_NOTIFICATION_EMAIL\', \'PAYMENT_RETURN_NOTIFICATION_EMAIL\', \'CONCIERGE_NOTIFICATION_EMAIL\', \'APPLICATION_NOTIFICATION_EMAIL\', \'GUESTCARD_NOTIFICATION_EMAIL\', \'SITETAB_GUESTCARD_NOTIFICATION_EMAIL\', \'REFERRAL_NOTIFICATION_EMAIL\', \'CUSTOM_FORMS\', \'DISTRIBUTIONS_NOTIFICATION_EMAIL\', \'FEEDBACK_NOTIFICATION_EMAIL\', \'ENROLLMENT_NOTIFICATION_EMAIL\' ) AND value IS NOT NULL ) AS property_preferences_count,
						( SELECT email_address FROM property_email_addresses WHERE cid = p.cid AND property_id = p.id AND cid = ' . ( int ) $intCid . ' AND email_address_type_id = ' . CEmailAddressType::PRIMARY . 'LIMIT 1 ) AS property_email_address,
						( SELECT name_full FROM property_on_site_contacts WHERE cid = p.cid AND property_id = p.id AND cid = ' . ( int ) $intCid . ' AND on_site_contact_type_id = ' . CContactType::MANAGER . ' LIMIT 1 ) AS property_manager_name
					FROM
						properties p
					WHERE
						p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.cid = ' . ( int ) $intCid . '
					GROUP BY
						p.id,
						p.cid,
						p.property_name,
						p.lookup_code,
						p.full_description
					ORDER BY
						lower ( p.property_name ) ASC';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPropertiesByCompanyApplicationIdByCompanyUserIdByCid( $intCompanyApplicationId, $intCompanyUserId, $intCid, $objClientDatabase ) {
		if( false == isset( $intCompanyApplicationId ) || false == is_numeric( $intCompanyApplicationId ) ) {
			return NULL;
		}

		$strSql = self::buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId );

		$strSql = 'SELECT
						p.*
					FROM
						( ' . $strSql . ' ) as p
						JOIN property_applications pa ON ( pa.cid = p.cid AND pa.property_id = p.id AND pa.company_application_id = ' . ( int ) $intCompanyApplicationId . ' )
					WHERE
						p.cid = ' . ( int ) $intCid . '
					ORDER BY
						lower ( p.property_name ) ASC,
						p.order_num';

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchPropertiesByPropertyApplicationIdsByCid( $arrintPropertyApplicationIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPropertyApplicationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM properties p
					WHERE p.id IN ( SELECT pa.property_id FROM property_applications pa
									WHERE pa.cid =' . ( int ) $intCid . '
									AND pa.id IN ( ' . implode( ',', $arrintPropertyApplicationIds ) . ' ) )
						AND p.cid =' . ( int ) $intCid . '
						AND p.is_disabled = 0
					ORDER BY
						lower ( p.property_name ) ASC,
						p.order_num';

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchPropertiesForJobPortalGoogleMapByCid( $intWebsiteId, $objJobSearch, $intCid, $objClientDatabase ) {
		$arrstrAdditionalWhereParameters = [];

		if( true == isset ( $objJobSearch ) ) {
			$strCountryCode = $objJobSearch->getCountryCode();
			$strStateCode   = $objJobSearch->getStateCode();
			$strCity        = $objJobSearch->getCity();
			$strPostalCode  = $objJobSearch->getPostalCode();
			( true == empty( $strCountryCode ) )
				? false : array_push( $arrstrAdditionalWhereParameters, 'pa.country_code=\'' . addslashes( $objJobSearch->getCountryCode() ) . '\'' );

			( true == empty( $strStateCode ) || true == is_numeric( $objJobSearch->getCompanyAreaId() ) || true == is_numeric( $objJobSearch->getCompanyRegionId() ) )
				? false : array_push( $arrstrAdditionalWhereParameters, 'pa.state_code=\'' . addslashes( $objJobSearch->getStateCode() ) . '\'' );

			( true == empty( $strCity ) )
				? false : array_push( $arrstrAdditionalWhereParameters, 'pa.city=\'' . addslashes( $objJobSearch->getCity() ) . '\'' );

			( true == empty( $strPostalCode ) )
				? false : array_push( $arrstrAdditionalWhereParameters, 'pa.postal_code=\'' . addslashes( $objJobSearch->getPostalCode() ) . '\'' );
		}

		$strSql = 'SELECT
						p.*
					FROM
						properties AS p
						JOIN website_properties AS wp ON ( p.id = wp.property_id AND p.cid = wp.cid ) ';

		if( true == isset( $objJobSearch ) && true == is_numeric( $objJobSearch->getCompanyRegionId() ) && true == is_numeric( $objJobSearch->getCompanyAreaId() ) ) {
			// If both are set, we only search off of company area id
			$strSql .= ' JOIN property_areas AS pr_area ON ( p.id = pr_area.property_id AND p.cid = pr_area.cid )
										 JOIN company_areas AS co_area ON ( pr_area.company_area_id = co_area.id AND pr_area.cid = co_area.cid AND co_area.id = ' . ( int ) $objJobSearch->getCompanyAreaId() . ' AND co_area.cid = ' . ( int ) $intCid . ' ) ';

		} else {
			if( true == isset( $objJobSearch ) && true == is_numeric( $objJobSearch->getCompanyRegionId() ) ) {
				// If only region is set, things get tricky. We have to look in do a union and get the regions of associated areas + associated regions
				$strSql .= ' JOIN property_areas AS pr_area ON ( p.id = pr_area.property_id AND p.cid = pr_area.cid ) ';
				// Join on the union of company areas where id = company region or the parent of an attached area
				$strSql .= ' JOIN (	(
									SELECT
										*,
										NULL AS child_id
									FROM
										company_areas
									WHERE
										id = ' . ( int ) $objJobSearch->getCompanyRegionId() . '
										AND cid = ' . ( int ) $intCid . '
								)
								UNION
								(
									SELECT
										ca_parent.*,
										ca_child.id AS child_id
									FROM
										company_areas ca_child
										JOIN company_areas ca_parent ON ( ca_child.company_area_id = ca_parent.id AND ca_child.cid = ca_parent.cid AND ca_parent.id = ' . ( int ) $objJobSearch->getCompanyRegionId() . ' AND ca_parent.cid = ' . ( int ) $intCid . ' ) )
								) AS co_area ON ( co_area.id = pr_area.company_area_id AND co_area.cid = pr_area.cid OR co_area.child_id = pr_area.company_area_id AND co_area.cid = pr_area.cid ) ';
			}
		}

		$strSql .= '
					LEFT OUTER JOIN property_addresses AS pa ON ( pa.property_id = p.id AND pa.cid = p.cid AND pa.is_alternate = false )
				WHERE
					wp.website_id = ' . ( int ) $intWebsiteId . '
					AND wp.cid = ' . ( int ) $intCid . '
					AND	( wp.hide_on_job_portal IS NULL OR wp.hide_on_job_portal = 0 ) ';

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAdditionalWhereParameters ) ) {
			$strSql .= ' AND ' . implode( ' AND ', $arrstrAdditionalWhereParameters );
		}

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT * FROM properties WHERE id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' ) AND cid = ' . ( int ) $intCid . '
						ORDER BY
							lower ( property_name )';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCountPropertyCountryCodesByIdsByNotCountryCodeByCid( $arrintPropertyIds, $strCountryCode, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return 0;
		}
		$strSql = 'SELECT
						COUNT(*)
					FROM
						properties
					WHERE
						id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND country_code != \'' . $strCountryCode . '\'
						AND property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						AND cid = ' . ( int ) $intCid;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) {
			return $arrintResponse[0]['count'];
		}

		return 0;
	}

	public static function fetchFeaturedPropertiesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.short_description
					FROM
						properties AS p
						INNER JOIN property_details AS pd ON ( p.id = pd.property_id AND p.cid = pd.cid )
					WHERE
						p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pd.is_featured = TRUE
						AND p.cid = ' . ( int ) $intCid;

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchFeaturedPropertiesByWebsiteIdByCid( $intWebsiteId, $intCid, $objClientDatabase, $strOrderBy = '' ) {

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.cid
					FROM
						properties p
						JOIN website_properties wp ON ( p.id = wp.property_id AND p.cid = wp.cid AND p.is_disabled = 0 AND p.cid = ' . ( int ) $intCid . ' AND wp.website_id = ' . ( int ) $intWebsiteId . ' )
						JOIN property_details pd ON ( p.id = pd.property_id AND p.cid = pd.cid AND pd.is_featured = TRUE )';

		if( 0 < strlen( $strOrderBy ) ) {
			$strSql .= ' ORDER BY ' . $strOrderBy;
		}

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCorporatePropertiesByWebsiteIdByPropertyIdsByCid( $intWebsiteId, $arrintPropertyIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.full_description,
						wp.more_info_action,
						wp.hide_more_info_button,
						pd.community_website_uri
					FROM
						website_properties AS wp,
						properties AS p INNER JOIN property_details AS pd 	ON ( p.id = pd.property_id AND p.cid = pd.cid )
					WHERE
						p.id = wp.property_id
						AND p.cid = wp.cid
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.cid = ' . ( int ) $intCid . '
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
					ORDER BY
						p.order_num';

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchRandomFeaturedPropertiesByWebsiteIdByPropertyIdsByLimitByCid( $intWebsiteId, $arrintPropertyIds, $intCid, $objClientDatabase, $intLimit = NULL, $boolRandom = false, $strOrderByField = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.cid,
						p.property_name,
						p.short_description,
						p.min_rent,
						p.max_rent,
						p.number_of_units,
						wp.more_info_action,
						wp.hide_more_info_button,
						pd.community_website_uri
					FROM
						website_properties AS wp,
						properties AS p INNER JOIN property_details AS pd 	ON ( p.cid = pd.cid AND p.id = pd.property_id )
					WHERE
						p.id = wp.property_id
						AND p.cid = wp.cid
						AND pd.is_featured = TRUE
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.cid = ' . ( int ) $intCid . '
						AND wp.website_id = ' . ( int ) $intWebsiteId;

		if( true == $boolRandom ) {
			$strSql .= ' ORDER BY
							RANDOM() ';
		} else {
			if( false == is_null( $strOrderByField ) ) {
				$strSql .= ' ORDER BY ' . addslashes( $strOrderByField );
			} else {
				$strSql .= ' ORDER BY
								pd.updated_on DESC ';
			}
		}

		if( false == is_null( $intLimit ) ) {
			$strSql .= 'LIMIT ' . ( int ) $intLimit;
		}

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	// If we decide to support daily budgets, we will have to make modifications to this query ( concerning the loading of budgets ).

	public static function buildCustomVacancySearchSql( $objPropertySearch, $objPagination = NULL ) {
		// IF YOU CHANGE THIS FUNCTION YOU HAVE TO TEST FOR QUERY SPEED

		// There has to be an ad_group or a postal code in the property search in order for this function to work.
		if( false == valObj( $objPropertySearch, 'CPropertySearch' ) || ( false == is_numeric( $objPropertySearch->getPostalCode() ) && false == valStr( $objPropertySearch->getImplodedSemAdGroupIds() ) ) ) {
			return NULL;
		}

		// Make sure there is a band radius, otherwise, use 15
		if( false == is_numeric( $objPropertySearch->getBandRadius() ) ) {
			$objPropertySearch->setBandRadius( 15 );
		}

		// Make sure there is a band radius, otherwise, use 15
		if( false == is_numeric( $objPropertySearch->getMilesRadius() ) ) {
			$objPropertySearch->setMilesRadius( 50 );
		}

		$objSemAdGroup = $objPropertySearch->getSemAdGroup();

		$boolJoinOnDefaultAmenities = ( false == is_null( $objPropertySearch->getDefaultAmenityIds() ) && 0 < strlen( trim( $objPropertySearch->getDefaultAmenityIds() ) ) );

		// There are two types of searches. 1.) Zip Code Based or 2.) Ad Group Based

		// If it's a zip-code based search, we know it's not a click from google.
		if( true == is_numeric( $objPropertySearch->getPostalCode() ) ) {

			$strSql = ' SELECT * FROM (
							SELECT
								DISTINCT ON ( id )
								id,
								property_name,
								band,
								miles,
								has_remaining_budget,
								rand_sort_order,
								is_in_sem_keyword,
								cid,
								min_rent,
								max_rent,
								max_bedrooms,
								max_bathrooms,
								short_description,
								full_description,
								database_id,
								sem_property_status_type_id
							FROM (
								 SELECT
									p.id,
									p.cid,
									p.property_name,
									p.min_rent,
									p.max_rent,
									p.max_bedrooms,
									p.max_bathrooms,
									p.short_description,
									p.full_description,
									mc.database_id,
									spd.longitude,
									spd.latitude,
									spd.sem_property_status_type_id,
									mod( ( ( ( p.id ) + 1000 ) - ' . ( int ) $objPropertySearch->getRandomSortOrder() . ' ), 1000 ) AS rand_sort_order,
									CASE
										WHEN ( pc.latitude = spd.latitude AND spd.longitude = pc.longitude )
										THEN 0
										ELSE ( ACOS( SIN( pc.latitude/57.2958 )*SIN( spd.latitude/57.2958 )+COS( pc.latitude/57.2958 )*COS( spd.latitude/57.2958 )*COS( spd.longitude/57.2958-pc.longitude/57.2958 ) )*3963 )
									END AS miles,
									CASE
										WHEN ( pc.latitude = spd.latitude AND spd.longitude = pc.longitude )
										THEN 0
										ELSE round( ( ACOS( SIN( pc.latitude/57.2958 )*SIN( spd.latitude/57.2958 )+COS( pc.latitude/57.2958 )*COS( spd.latitude/57.2958 )*COS( spd.longitude/57.2958-pc.longitude/57.2958 ) )*3963 ) / ' . ( int ) $objPropertySearch->getBandRadius() . ' )
									END AS band,
									CASE
										WHEN sb.property_id IS NOT NULL
										THEN 1
										ELSE 0
									END AS has_remaining_budget,
									0 AS is_in_sem_keyword
								FROM
									properties AS p
									JOIN clients mc ON ( mc.id = p.cid )
									JOIN sem_property_details spd ON ( spd.property_id = p.id ) ';

			if( true == $boolJoinOnDefaultAmenities ) {
				$strSql .= ' JOIN sem_property_amenities spa ON ( spa.property_id = p.id ) ';
			}

			$strSql .= '		JOIN postal_codes pc ON ( pc.postal_code = \'' . addslashes( trim( $objPropertySearch->getPostalCode() ) ) . '\' )
									LEFT OUTER JOIN (
										SELECT
											property_id AS property_id,
											max( budget_date ) AS budget_date
										FROM
											sem_budgets
										WHERE
											remaining_budget > 0
										GROUP BY
											property_id
									 ) AS sb ON ( p.id = sb.property_id AND DATE_TRUNC ( \'month\', budget_date ) = DATE_TRUNC ( \'month\', NOW() ) )
									JOIN property_preferences pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.key <> \'ENABLE_SEMESTER_SELECTION\' AND pp.value IS NOT NULL )

								WHERE
									p.is_disabled <> 1
									-- [SG - we are already adding this in $arrstrAdditionalWhereParameters ] AND spd.sem_property_status_type_id = ' . CSemPropertyStatusType::ENABLED . '
									AND p.property_type_id = ' . CPropertyType::APARTMENT . '
									AND mc.id NOT IN (' . implode( ',', CClients::$c_arrintMacroScriptCids ) . ')
									AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
									AND spd.longitude IS NOT NULL
									AND spd.latitude IS NOT NULL
									AND pc.longitude IS NOT NULL
									AND pc.latitude IS NOT NULL ';

			if( 0 <= $objPropertySearch->getNumberOfBedrooms() && 0 < strlen( $objPropertySearch->getNumberOfBedrooms() ) ) {
				if( 0 == $objPropertySearch->getNumberOfBedrooms() ) {
					$strSql .= ' AND p.min_bedrooms = 0 ';

				} elseif( 0 < $objPropertySearch->getNumberOfBedrooms() ) {
					$strSql .= ' AND p.max_bedrooms >= ' . ( int ) $objPropertySearch->getNumberOfBedrooms();
				}
			}

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAdditionalWhereParameters ) ) {
				$strSql .= ' AND ' . implode( ' AND ', $arrstrAdditionalWhereParameters );
			}

			$strSql .= '
								GROUP BY
									p.id,
									pc.longitude,
									pc.latitude,
									spd.longitude,
									spd.latitude,
									sb.property_id,
									p.cid,
									p.property_name,
									p.min_rent,
									p.max_rent,
									p.max_bedrooms,
									p.max_bathrooms,
									p.short_description,
									p.full_description,
									mc.database_id,
									spd.sem_property_status_type_id
								ORDER BY
									band ASC,
									has_remaining_budget DESC,
									is_in_sem_keyword DESC,
									rand_sort_order ASC ) AS SUB_QUERY2
						WHERE ';
			$strSql .= 'miles < ' . ( int ) $objPropertySearch->getMilesRadius() . ' OR is_in_sem_keyword = 1 ) AS SUB_QUERY2

					ORDER BY ';

			if( 0 < strlen( $objPropertySearch->getOrderBy() ) ) {

				if( 'price_lowest_first' == $objPropertySearch->getOrderBy() ) {
					$strSql .= 'min_rent,max_rent,';

				} elseif( 'price_highest_first' == $objPropertySearch->getOrderBy() ) {
					$strSql .= 'min_rent DESC NULLS LAST,max_rent DESC,';

				} elseif( 'miles' == $objPropertySearch->getOrderBy() ) {
					$strSql .= 'miles,';
				}
			}

			$strSql .= 'band ASC,
						has_remaining_budget DESC,
						is_in_sem_keyword DESC,
						rand_sort_order ASC';

			// This condition facilitates search if this is a state or custom search
		} else {
			if( true == isset ( $objSemAdGroup ) && true == in_array( $objSemAdGroup->getSemLocationTypeId(), [ CSemLocationType::STATE, CSemLocationType::CUSTOM, CSemLocationType::TOURIST_ATTARACTION, CSemLocationType::UNIVERSITY ] ) ) {

				$strSql = ' SELECT * FROM (
							SELECT
								DISTINCT ON ( id )
								id,
								has_remaining_budget,
								rand_sort_order,
								is_in_sem_keyword,
								cid,
								property_name,
								min_rent,
								max_rent,
								max_bedrooms,
								max_bathrooms,
								short_description,
								full_description,
								database_id,
								sem_property_status_type_id
							FROM (
								 SELECT
									p.id,
									p.cid,
									p.property_name,
									p.min_rent,
									p.max_rent,
									p.max_bedrooms,
									p.max_bathrooms,
									p.short_description,
									p.full_description,
									mc.database_id,
									spd.sem_property_status_type_id,
									spd.longitude,
									spd.latitude,
									mod( ( ( ( p.id ) + 1000 ) - ' . ( int ) $objPropertySearch->getRandomSortOrder() . ' ), 1000 ) AS rand_sort_order,
									CASE
										WHEN sb.property_id IS NOT NULL
										THEN 1
										ELSE 0
									END AS has_remaining_budget';

				if( true == isset ( $objPropertySearch ) && true == is_numeric( $objPropertySearch->getSemKeywordId() ) ) {
					$strSql .= ', CASE
									WHEN ska.id IS NOT NULL
									THEN 1
									ELSE 0
								END AS is_in_sem_keyword ';
				} else {
					$strSql .= ', 0 AS is_in_sem_keyword ';
				}

				$strSql .= ' FROM
									properties AS p
									JOIN clients mc ON ( mc.id = p.cid )
									JOIN sem_property_details spd ON ( spd.property_id = p.id ) ';

				if( true == $boolJoinOnDefaultAmenities ) {
					$strSql .= ' JOIN sem_property_amenities spa ON ( spa.property_id = p.id ) ';
				}

				$strSql .= '		JOIN sem_ad_groups sag ON ( sag.id IN ( ' . $objPropertySearch->getImplodedSemAdGroupIds() . ' ) ) ';

				if( true == isset( $objPropertySearch ) && true == is_numeric( $objPropertySearch->getSemKeywordId() ) ) {
					$strSql .= '
									LEFT OUTER JOIN sem_ad_group_associations saga ON ( sag.id = saga.sem_ad_group_id AND saga.property_id = p.id )
									LEFT OUTER JOIN sem_keywords sk ON ( saga.sem_ad_group_id = sk.sem_ad_group_id AND sk.id = ' . ( int ) $objPropertySearch->getSemKeywordId() . ' )
									JOIN sem_keyword_associations ska ON ( ska.sem_keyword_id = sk.id AND saga.property_id = p.id ) ';
				}

				$strSql .= ' 		LEFT OUTER JOIN sem_budgets sb ON ( p.id = sb.property_id AND DATE_TRUNC ( \'month\', budget_date ) = DATE_TRUNC ( \'month\', NOW() ) AND remaining_budget > 0 )
									LEFT JOIN property_preferences pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.key <> \'ENABLE_SEMESTER_SELECTION\' AND pp.value IS NOT NULL )

								WHERE
									p.is_disabled <> 1
									-- [SG - we are already adding this in $arrstrAdditionalWhereParameters ]AND spd.sem_property_status_type_id = ' . CSemPropertyStatusType::ENABLED . '
									AND p.property_type_id = ' . CPropertyType::APARTMENT . '
									AND mc.id NOT IN (' . implode( ',', CClients::$c_arrintMacroScriptCids ) . ')
									AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
									AND spd.longitude IS NOT NULL
									AND spd.latitude IS NOT NULL
									AND sag.longitude IS NOT NULL
									AND sag.latitude IS NOT NULL ';

				if( 0 <= $objPropertySearch->getNumberOfBedrooms() && 0 < strlen( $objPropertySearch->getNumberOfBedrooms() ) ) {
					if( 0 == $objPropertySearch->getNumberOfBedrooms() ) {
						$strSql .= ' AND p.min_bedrooms = 0 ';

					} elseif( 0 < $objPropertySearch->getNumberOfBedrooms() ) {
						$strSql .= '	AND p.max_bedrooms >= ' . ( int ) $objPropertySearch->getNumberOfBedrooms();
					}
				}

				if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAdditionalWhereParameters ) ) {
					$strSql .= ' AND ' . implode( ' AND ', $arrstrAdditionalWhereParameters );
				}

				$strSql .= '
						GROUP BY
							p.id,
							spd.longitude,
							spd.latitude,
							sb.property_id,
							p.cid,
							p.property_name,
							p.min_rent,
							p.max_rent,
							p.max_bedrooms,
							p.max_bathrooms,
							p.short_description,
							p.full_description,
							mc.database_id,
							spd.sem_property_status_type_id';

				if( true == isset ( $objPropertySearch ) && true == is_numeric( $objPropertySearch->getSemKeywordId() ) ) {
					$strSql .= ',ska.id';
				}

				$strSql .= '
						ORDER BY
							is_in_sem_keyword DESC,
							has_remaining_budget DESC,
							rand_sort_order ASC
					 ) AS SUB_QUERY1

					WHERE
						is_in_sem_keyword = 1 ) AS SUB_QUERY2

				ORDER BY ';

				if( 0 < strlen( $objPropertySearch->getOrderBy() ) ) {
					if( 'price_lowest_first' == $objPropertySearch->getOrderBy() ) {
						$strSql .= 'min_rent,max_rent,';

					} elseif( 'price_highest_first' == $objPropertySearch->getOrderBy() ) {
						$strSql .= 'min_rent DESC NULLS LAST, max_rent DESC,';
					}
				}

				$strSql .= '
					has_remaining_budget DESC,
					rand_sort_order ASC';

				// This final condition is for all other types of searches by State / City.
			} else {
				$strSql = ' SELECT * FROM (
							SELECT
								DISTINCT ON ( id )
								id,
								band,
								miles,
								has_remaining_budget,
								rand_sort_order,
								is_in_sem_keyword,
								cid,
								property_name,
								min_rent,
								max_rent,
								max_bedrooms,
								max_bathrooms,
								short_description,
								full_description,
								database_id,
								sem_property_status_type_id
							FROM (
								 SELECT
									p.id,
									p.cid,
									p.property_name,
									p.min_rent,
									p.max_rent,
									p.max_bedrooms,
									p.max_bathrooms,
									p.short_description,
									p.full_description,
									mc.database_id,
									spd.sem_property_status_type_id,
									spd.longitude,
									spd.latitude,
									mod( ( ( ( p.id ) + 1000 ) - ' . ( int ) $objPropertySearch->getRandomSortOrder() . ' ), 1000 ) AS rand_sort_order,

									CASE
										WHEN ( sag.latitude = spd.latitude AND spd.longitude = sag.longitude )
										THEN 0
										ELSE ( ACOS( SIN( sag.latitude/57.2958 )*SIN( spd.latitude/57.2958 )+COS( sag.latitude/57.2958 )*COS( spd.latitude/57.2958 )*COS( spd.longitude/57.2958-sag.longitude/57.2958 ) )*3963 )
									END AS miles,

									CASE
										WHEN ( sag.latitude = spd.latitude AND spd.longitude = sag.longitude )
										THEN 0
										ELSE round( ( ACOS( SIN( sag.latitude/57.2958 )*SIN( spd.latitude/57.2958 )+COS( sag.latitude/57.2958 )*COS( spd.latitude/57.2958 )*COS( spd.longitude/57.2958-sag.longitude/57.2958 ) )*3963 ) / ' . ( float ) $objPropertySearch->getBandRadius() . ' )
									END AS band,

									CASE
										WHEN sb.property_id IS NOT NULL
										THEN 1
										ELSE 0
									END AS has_remaining_budget';

				if( true == isset ( $objPropertySearch ) && true == is_numeric( $objPropertySearch->getSemKeywordId() ) ) {
					$strSql .= ', CASE
															WHEN ska.id IS NOT NULL
															THEN 1
															ELSE 0
														END AS is_in_sem_keyword ';
				} else {
					$strSql .= ', 0 AS is_in_sem_keyword ';
				}

				$strSql .= ' FROM
									properties AS p
									JOIN clients mc ON ( mc.id = p.cid )
									JOIN sem_property_details spd ON ( spd.property_id = p.id ) ';

				if( true == $boolJoinOnDefaultAmenities ) {
					$strSql .= ' JOIN sem_property_amenities spa ON ( spa.property_id = p.id ) ';
				}

				$strSql .= '		JOIN sem_ad_groups sag ON ( sag.id IN ( ' . $objPropertySearch->getImplodedSemAdGroupIds() . ' ) ) ';

				if( true == isset( $objPropertySearch ) && true == is_numeric( $objPropertySearch->getSemKeywordId() ) ) {
					$strSql .= '
									LEFT OUTER JOIN sem_ad_group_associations saga ON ( sag.id = saga.sem_ad_group_id AND saga.property_id = p.id )
									LEFT OUTER JOIN sem_keywords sk ON ( saga.sem_ad_group_id = sk.sem_ad_group_id AND sk.id = ' . ( int ) $objPropertySearch->getSemKeywordId() . ' )
									JOIN sem_keyword_associations ska ON ( ska.sem_keyword_id = sk.id AND saga.property_id = p.id ) ';
				}

				$strSql .= '		LEFT OUTER JOIN sem_budgets sb ON ( p.id = sb.property_id AND DATE_TRUNC ( \'month\', budget_date ) = DATE_TRUNC ( \'month\', NOW() ) AND remaining_budget > 0 )

								WHERE
									p.is_disabled <> 1
									-- [SG - we are already adding this in $arrstrAdditionalWhereParameters ] AND spd.sem_property_status_type_id = ' . CSemPropertyStatusType::ENABLED . '
									AND p.property_type_id = ' . CPropertyType::APARTMENT . '
									AND mc.id NOT IN (' . implode( ',', CClients::$c_arrintMacroScriptCids ) . ')
									AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
									AND spd.longitude IS NOT NULL
									AND spd.latitude IS NOT NULL
									AND sag.longitude IS NOT NULL
									AND sag.latitude IS NOT NULL ';

				if( 0 <= $objPropertySearch->getNumberOfBedrooms() && 0 < strlen( $objPropertySearch->getNumberOfBedrooms() ) ) {
					if( 0 == $objPropertySearch->getNumberOfBedrooms() ) {
						$strSql .= ' AND p.min_bedrooms = 0 ';

					} elseif( 0 < $objPropertySearch->getNumberOfBedrooms() ) {
						$strSql .= '	AND p.max_bedrooms >= ' . ( float ) $objPropertySearch->getNumberOfBedrooms();
					}
				}

				if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAdditionalWhereParameters ) ) {
					$strSql .= ' AND ' . implode( ' AND ', $arrstrAdditionalWhereParameters );
				}

				$strSql .= '
						GROUP BY
							p.id,
							sag.longitude,
							sag.latitude,
							spd.longitude,
							spd.latitude,
							sb.property_id,
							p.cid,
							p.property_name,
							p.min_rent,
							p.max_rent,
							p.max_bedrooms,
							p.max_bathrooms,
							p.short_description,
							p.full_description,
							mc.database_id,
							spd.sem_property_status_type_id';

				if( true == isset ( $objPropertySearch ) && true == is_numeric( $objPropertySearch->getSemKeywordId() ) ) {
					$strSql .= ',ska.id';
				}

				$strSql .= '
						ORDER BY
							is_in_sem_keyword DESC,
							band ASC,
							has_remaining_budget DESC,
							rand_sort_order ASC
					 ) AS SUB_QUERY1

					WHERE
						miles < ' . ( int ) $objPropertySearch->getMilesRadius() . '
						OR is_in_sem_keyword = 1
				) AS SUB_QUERY2

				ORDER BY ';

				if( 0 < strlen( $objPropertySearch->getOrderBy() ) ) {

					if( 'price_lowest_first' == $objPropertySearch->getOrderBy() ) {
						$strSql .= 'min_rent,max_rent,';

					} elseif( 'price_highest_first' == $objPropertySearch->getOrderBy() ) {
						$strSql .= 'min_rent DESC NULLS LAST, max_rent DESC,';

					} elseif( 'miles' == $objPropertySearch->getOrderBy() ) {
						$strSql .= 'miles,';
					}
				}

				$strSql .= '
					is_in_sem_keyword DESC,
					band ASC,
					has_remaining_budget DESC,
					rand_sort_order ASC';
			}
		}

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$intOffset = ( 0 < $objPagination->getPageNo() ) ? $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) : 0;
			$intLimit  = ( int ) $objPagination->getPageSize();

			$strSql .= ' OFFSET ' . ( int ) $intOffset . '
							LIMIT ' . ( int ) $intLimit;

		}

		return $strSql;
	}

	public static function fetchChildPropertyIdsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $arrintRequiredProducts = [] ) {
		$strSql = 'SELECT
						p.id
					FROM
						properties p';
		$strSql .= ( true == valArr( $arrintRequiredProducts ) ) ? ' JOIN property_products pp ON p.cid = pp.cid AND p.id = pp.property_id AND pp.ps_product_id IN ( ' . implode( ',', $arrintRequiredProducts ) . ' )' : '';

		$strSql .= ' WHERE
						p.property_id = ' . ( int ) $intPropertyId . '
						AND p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled <> 1 ';

		$arrmixProperties = fetchData( $strSql, $objDatabase );

		$arrintChildPropertyIds = [];
		if( true == valArr( $arrmixProperties ) ) {
			foreach( $arrmixProperties as $arrintPropertyIds ) {
				$arrintChildPropertyIds[$arrintPropertyIds['id']] = $arrintPropertyIds['id'];
			}
		}

		return $arrintChildPropertyIds;
	}

	public static function fetchParentPropertyIdByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $arrintRequiredProducts = [] ) {

		$strSql = 'SELECT
						p.property_id
					FROM
						properties p';
		$strSql .= ( true == valArr( $arrintRequiredProducts ) ) ? ' JOIN property_products pp ON p.cid = pp.cid AND p.id = pp.property_id AND pp.ps_product_id IN ( ' . implode( ',', $arrintRequiredProducts ) . ' )' : '';

		$strSql .= ' WHERE
						p.id = ' . ( int ) $intPropertyId . '
						AND p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled <> 1 ';

		$arrmixProperty = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixProperty ) ) {
			if( true == valId( $arrmixProperty[0]['property_id'] ) ) {
				return ( int ) $arrmixProperty[0]['property_id'];
			}
		}

		return NULL;
	}

	public static function fetchChildPropertiesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						id,
						property_id,
						property_name
					FROM
						properties
					WHERE
						property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND is_disabled <> 1 ';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchChildPropertyIdsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIsWithParentProperty = false ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						id,
						property_id
					FROM
						properties
					WHERE
						property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND is_disabled <> 1 ';

		$arrmixProperties = fetchData( $strSql, $objDatabase );

		$arrintChildPropertyIds = NULL;

		if( true == valArr( $arrmixProperties ) ) {
			if( true == $boolIsWithParentProperty ) {
				foreach( $arrmixProperties as $arrintPropertyIds ) {
					$arrintChildPropertyIds[$arrintPropertyIds['id']] = $arrintPropertyIds['property_id'];
				}
			} else {
				foreach( $arrmixProperties as $arrintPropertyIds ) {
					$arrintChildPropertyIds[$arrintPropertyIds['id']] = $arrintPropertyIds['id'];
				}
			}
		}

		return $arrintChildPropertyIds;
	}

	public static function fetchPropertyByPropertyIdByIdByCid( $intPropertyId, $intId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM properties WHERE property_id = ' . ( int ) $intPropertyId . ' AND id = ' . ( int ) $intId . ' AND cid = ' . ( int ) $intCid . ' AND is_disabled <> 1 ';

		return self::fetchProperty( $strSql, $objDatabase );
	}

	// We are missing a condition in this query. There is a 3rd condition we added
	// that handles state and custom ad groups.
	// TODO: We must add this 3rd scenario here.

	public static function buildVacancySearchCountSql( $objPropertySearch, $objPagination = NULL ) {
		// setting the paramenter to null as it is never used in the method
		$objPagination = NULL;

		// IF YOU CHANGE THIS FUNCTION, YOU HAVE TO TEST FOR QUERY SPEED

		// There has to be an ad_group or a postal code in the property search in order for this function to work.

		if( false == valObj( $objPropertySearch, 'CPropertySearch' ) || ( false == is_numeric( $objPropertySearch->getPostalCode() ) && false == valStr( $objPropertySearch->getImplodedSemAdGroupIds() ) ) ) {
			return 'SELECT 0 AS count';
		}

		// Make sure there is a band radius, otherwise, use 15
		if( false == is_numeric( $objPropertySearch->getMilesRadius() ) ) {
			$objPropertySearch->setMilesRadius( 50 );
		}

		$objSemAdGroup = $objPropertySearch->getSemAdGroup();

		$boolJoinOnDefaultAmenities = ( false == is_null( $objPropertySearch->getDefaultAmenityIds() ) && 0 < strlen( trim( $objPropertySearch->getDefaultAmenityIds() ) ) );

		// There are two types of searches. 1.) Zip Code Based or 2.) Ad Group Based

		// If it's a zip-code based search, we know it's not a click from google.
		if( true == is_numeric( $objPropertySearch->getPostalCode() ) ) {

			$strSql = ' SELECT count( id ) AS count FROM (
							SELECT
								DISTINCT ON ( id )
								id,
								miles
							FROM (
								 SELECT
									p.id,
									CASE
										WHEN ( pc.latitude = spd.latitude AND spd.longitude = pc.longitude )
										THEN 0
										ELSE ( ACOS( SIN( pc.latitude/57.2958 )*SIN( spd.latitude/57.2958 )+COS( pc.latitude/57.2958 )*COS( spd.latitude/57.2958 )*COS( spd.longitude/57.2958-pc.longitude/57.2958 ) )*3963 )
									END AS miles
								 FROM
									properties AS p
									JOIN clients c ON ( c.id = p.cid )
									JOIN sem_property_details spd ON ( spd.property_id = p.id )
									JOIN postal_codes pc ON ( pc.postal_code = \'' . addslashes( trim( $objPropertySearch->getPostalCode() ) ) . '\' ) ';

			if( true == $boolJoinOnDefaultAmenities ) {
				$strSql .= ' JOIN sem_property_amenities spa ON ( spa.property_id = p.id ) ';
			}

			$strSql .= ' WHERE
									p.is_disabled <> 1
									AND spd.sem_property_status_type_id = ' . CSemPropertyStatusType::ENABLED . '
									AND p.property_type_id = ' . CPropertyType::APARTMENT . '
									AND p.cid NOT IN (' . implode( ',', CClients::$c_arrintMacroScriptCids ) . ')
									AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
									AND spd.longitude IS NOT NULL
									AND spd.latitude IS NOT NULL
									AND pc.longitude IS NOT NULL
									AND pc.latitude IS NOT NULL ';

			if( false == is_null( $objPropertySearch->getCid() ) ) {
				$strSql .= ' AND p.cid = ' . ( int ) $objPropertySearch->getCid() . ' ';
			}

			if( 0 <= $objPropertySearch->getNumberOfBedrooms() && 0 < strlen( $objPropertySearch->getNumberOfBedrooms() ) ) {
				if( 0 == $objPropertySearch->getNumberOfBedrooms() ) {
					$strSql .= ' AND p.min_bedrooms = 0 ';

				} elseif( 0 < $objPropertySearch->getNumberOfBedrooms() ) {
					$strSql .= ' AND p.max_bedrooms >= ' . ( int ) $objPropertySearch->getNumberOfBedrooms();
				}
			}

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAdditionalWhereParameters ) ) {
				$strSql .= ' AND ' . implode( ' AND ', $arrstrAdditionalWhereParameters );
			}

			$strSql .= '
								GROUP BY
									p.id,
									spd.latitude,
									spd.longitude,
									pc.latitude,
									pc.longitude ) AS SUB_QUERY2
						WHERE
							miles < ' . ( int ) $objPropertySearch->getMilesRadius() . ' ) AS SUB_QUERY2 ';

			// This condition facilitates search if this is a state or custom search
		} else {
			if( true == isset ( $objSemAdGroup ) && true == in_array( $objSemAdGroup->getSemLocationTypeId(), [ CSemLocationType::STATE, CSemLocationType::CUSTOM, CSemLocationType::TOURIST_ATTARACTION, CSemLocationType::UNIVERSITY ] ) ) {

				$strSql = ' SELECT count( id ) AS count FROM (
							SELECT
								DISTINCT ON ( id )
								id,
								is_in_sem_keyword
							FROM (

								SELECT
									p.id, ';

				if( true == isset( $objPropertySearch ) && true == is_numeric( $objPropertySearch->getSemKeywordId() ) ) {
					$strSql .= ' CASE
								WHEN ska.id IS NOT NULL
									THEN 1
									ELSE 0
								END AS is_in_sem_keyword ';
				} else {
					$strSql .= ' 0 AS is_in_sem_keyword ';
				}

				$strSql .= '
								FROM
									properties AS p
									JOIN clients c ON ( c.id = p.cid )
									JOIN sem_property_details spd ON ( spd.property_id = p.id )
									JOIN sem_ad_groups sag ON ( sag.id IN ( ' . $objPropertySearch->getImplodedSemAdGroupIds() . ' ) )
									LEFT OUTER JOIN sem_budgets sb ON ( p.id = sb.property_id AND DATE_TRUNC ( \'month\', budget_date ) = DATE_TRUNC ( \'month\', NOW() ) AND remaining_budget > 0 ) ';

				if( true == isset( $objPropertySearch ) && true == is_numeric( $objPropertySearch->getSemKeywordId() ) ) {
					$strSql .= '
									LEFT OUTER JOIN sem_ad_group_associations saga ON ( sag.id = saga.sem_ad_group_id AND saga.property_id = p.id )
									LEFT OUTER JOIN sem_keywords sk ON ( saga.sem_ad_group_id = sk.sem_ad_group_id AND sk.id = ' . ( int ) $objPropertySearch->getSemKeywordId() . ' )
									JOIN sem_keyword_associations ska ON ( ska.sem_keyword_id = sk.id AND saga.property_id = p.id ) ';
				}

				if( true == $boolJoinOnDefaultAmenities ) {
					$strSql .= ' JOIN sem_property_amenities spa ON ( spa.property_id = p.id ) ';
				}

				$strSql .= '

								WHERE
									p.is_disabled <> 1
									AND spd.sem_property_status_type_id = ' . CSemPropertyStatusType::ENABLED . '
									AND p.property_type_id = ' . CPropertyType::APARTMENT . '
									AND p.cid NOT IN (' . implode( ',', CClients::$c_arrintMacroScriptCids ) . ')
									AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
									AND spd.longitude IS NOT NULL
									AND spd.latitude IS NOT NULL
									AND sag.longitude IS NOT NULL
									AND sag.latitude IS NOT NULL ';

				if( false == is_null( $objPropertySearch->getCid() ) ) {
					$strSql .= ' AND p.cid = ' . ( int ) $objPropertySearch->getCid() . ' ';
				}

				if( 0 <= $objPropertySearch->getNumberOfBedrooms() && 0 < strlen( $objPropertySearch->getNumberOfBedrooms() ) ) {
					if( 0 == $objPropertySearch->getNumberOfBedrooms() ) {
						$strSql .= ' AND p.min_bedrooms = 0 ';

					} elseif( 0 < $objPropertySearch->getNumberOfBedrooms() ) {
						$strSql .= '	AND p.max_bedrooms >= ' . ( int ) $objPropertySearch->getNumberOfBedrooms();
					}
				}

				if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAdditionalWhereParameters ) ) {
					$strSql .= ' AND ' . implode( ' AND ', $arrstrAdditionalWhereParameters );
				}

				$strSql .= '
						GROUP BY
							p.id,
							is_in_sem_keyword

					) AS SUB_QUERY1

					WHERE
						is_in_sem_keyword = 1 ) AS SUB_QUERY2 ';

			} else {

				$strSql = 'SELECT count( id ) FROM (
						SELECT
							DISTINCT ON ( id )
							id,
							is_in_sem_keyword,
							miles
						FROM (
							 SELECT
								p.id, ';

				if( true == isset( $objPropertySearch ) && true == is_numeric( $objPropertySearch->getSemKeywordId() ) ) {
					$strSql .= ' CASE
												WHEN ska.id IS NOT NULL
												THEN 1
												ELSE 0
											END AS is_in_sem_keyword, ';
				} else {
					$strSql .= ' 0 AS is_in_sem_keyword, ';
				}

				$strSql .= '
								CASE
									WHEN ( sag.latitude = spd.latitude AND spd.longitude = sag.longitude )
									THEN 0
									ELSE ( ACOS( SIN( sag.latitude/57.2958 )*SIN( spd.latitude/57.2958 )+COS( sag.latitude/57.2958 )*COS( spd.latitude/57.2958 )*COS( spd.longitude/57.2958-sag.longitude/57.2958 ) )*3963 )
								END AS miles
							FROM
								properties AS p
								JOIN clients c ON ( c.id = p.cid )
								JOIN sem_property_details spd ON ( spd.property_id = p.id )
								JOIN sem_ad_groups sag ON ( sag.id IN ( ' . $objPropertySearch->getImplodedSemAdGroupIds() . ' ) ) ';

				if( true == isset ( $objPropertySearch ) && true == is_numeric( $objPropertySearch->getSemKeywordId() ) ) {
					$strSql .= '
									LEFT OUTER JOIN sem_ad_group_associations saga ON ( sag.id = saga.sem_ad_group_id AND saga.property_id = p.id )
									LEFT OUTER JOIN sem_keywords sk ON ( saga.sem_ad_group_id = sk.sem_ad_group_id AND sk.id = ' . ( int ) $objPropertySearch->getSemKeywordId() . ' )
									JOIN sem_keyword_associations ska ON ( ska.sem_keyword_id = sk.id AND saga.property_id = p.id ) ';
				}

				if( true == $boolJoinOnDefaultAmenities ) {
					$strSql .= ' JOIN sem_property_amenities spa ON ( spa.property_id = p.id ) ';
				}

				$strSql .= ' 	WHERE
									p.is_disabled <> 1
									AND spd.sem_property_status_type_id = ' . CSemPropertyStatusType::ENABLED . '
									AND p.property_type_id = ' . CPropertyType::APARTMENT . '
									AND p.cid NOT IN (' . implode( ',', CClients::$c_arrintMacroScriptCids ) . ')
									AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
									AND spd.longitude IS NOT NULL
									AND spd.latitude IS NOT NULL
									AND sag.longitude IS NOT NULL
									AND sag.latitude IS NOT NULL ';

				if( 0 <= $objPropertySearch->getNumberOfBedrooms() && 0 < strlen( $objPropertySearch->getNumberOfBedrooms() ) ) {
					if( 0 == $objPropertySearch->getNumberOfBedrooms() ) {
						$strSql .= ' AND p.min_bedrooms = 0 ';

					} elseif( 0 < $objPropertySearch->getNumberOfBedrooms() ) {
						$strSql .= ' AND p.max_bedrooms >= ' . ( int ) $objPropertySearch->getNumberOfBedrooms();
					}
				}

				if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAdditionalWhereParameters ) ) {
					$strSql .= ' AND ' . implode( ' AND ', $arrstrAdditionalWhereParameters );
				}

				$strSql .= '
						GROUP BY
							p.id,
							spd.latitude,
							spd.longitude,
							sag.latitude,
							sag.longitude,
							is_in_sem_keyword

					 ) AS SUB_QUERY1

					WHERE
						miles < ' . ( int ) $objPropertySearch->getMilesRadius() . ' OR is_in_sem_keyword = 1 ) AS SUB_QUERY2';
			}
		}

		return $strSql;
	}

	public static function fetchSingleRandomFeaturedPropertyByWebsiteIdByPropertyIdsByCid( $intWebsiteId, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.*,
						pd.community_website_uri,
						wp.more_info_action
					FROM
						website_properties AS wp,
						properties AS p INNER JOIN property_details AS pd ON ( p.cid = pd.cid AND p.id = pd.property_id )
					WHERE
						wp.website_id = ' . ( int ) $intWebsiteId . '
						AND p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pd.is_featured = TRUE
						AND p.id = wp.property_id
						AND p.cid = wp.cid
					ORDER BY
						RANDOM()
					LIMIT 1';

		return self::fetchProperty( $strSql, $objDatabase );
	}

	public static function fetchPropertyForApprovalProcessByIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT ON ( p.id )
						p.id,
						p.property_type_id,
						p.short_description,
						p.full_description,
						pa.longitude AS longitude,
						pa.latitude AS latitude,
						pa.street_line1 AS street_line1,
						pa.city AS city,
						pa.postal_code AS postal_code,
						( SELECT count( id ) FROM property_floorplans WHERE is_published = true AND property_id = p.id AND deleted_on IS NULL AND cid = p.cid ) AS property_floorplans_count,
						( SELECT count( a.id ) FROM rate_associations ra JOIN amenities a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id AND a.amenity_type_id = ' . CAmenityType::COMMUNITY . ' ) WHERE ra.property_id = p.id AND ra.cid = p.cid ) AS community_amenities_count,
						ppn.phone_number AS office_phone_number,
						pea.email_address AS primary_email_address
					FROM
						properties p
						LEFT OUTER JOIN property_addresses pa ON ( p.cid = pa.cid AND p.id = pa.property_id AND address_type_id = ' . CAddressType::PRIMARY . ' AND pa.is_alternate = false )
						LEFT OUTER JOIN property_phone_numbers ppn ON ( ppn.cid = p.cid AND ppn.property_id = p.id AND ppn.phone_number_type_id = 3 )
						LEFT OUTER JOIN property_email_addresses pea ON ( pea.cid = p.cid AND pea.property_id = p.id AND pea.email_address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						p.id = ' . ( int ) $intPropertyId . '
						AND p.cid = ' . ( int ) $intCid;

		return self::fetchProperty( $strSql, $objDatabase );
	}

	public static function fetchCustomPaginatedPropertiesBySemPropertyStatusTypeIds( $intPageNo, $intPageSize, $arrintSemPropertyStatusTypeIds, $objDatabase ) {
		if( false == valArr( $arrintSemPropertyStatusTypeIds ) ) {
			return;
		}

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strSql = 'SELECT
						p.*,
						mc.database_id AS database_id
					FROM
						properties p,
						clients mc,
						sem_property_details spd
					WHERE
						p.id = spd.property_id
						AND mc.id = p.cid
						AND spd.sem_property_status_type_id IN ( ' . implode( ',', $arrintSemPropertyStatusTypeIds ) . ' )
					ORDER BY
						p.property_name
					OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertiesCountBySemPropertyStatusTypeIds( $arrintSemPropertyStatusTypeIds, $objDatabase ) {
		if( false == valArr( $arrintSemPropertyStatusTypeIds ) ) {
			return 0;
		}

		$strSql = 'SELECT
						count( p.id )
					FROM
						properties p,
						sem_property_details spd
					WHERE
						p.id = spd.property_id
						AND spd.sem_property_status_type_id IN ( ' . implode( ',', $arrintSemPropertyStatusTypeIds ) . ' )';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) {
			return $arrintResponse[0]['count'];
		}

		return 0;
	}

	public static function fetchCustomPropertiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchProperties( sprintf( 'SELECT * FROM properties WHERE property_id = %d AND cid = %d AND is_disabled <> 1', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchNeighborhoodPropertiesByPropertyAddressByPropertyIdByIdsByMilesByCid( $objPrimaryPropertyAddress, $intPropertyId, $arrintPropertyIds, $intCid, $objDatabase, $intMilesWithin = 50 ) {
		if( false == valObj( $objPrimaryPropertyAddress, 'CPropertyAddress' ) ) {
			return NULL;
		}

		if( true == is_null( $objPrimaryPropertyAddress->getLongitude() ) || true == is_null( $objPrimaryPropertyAddress->getLatitude() ) ) {
			return NULL;
		}

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						miles,
						id,
						property_name,
						cid,
						street_line1,
						street_line2,
						city,
						postal_code,
						state_code,
						longitude,
						latitude
					FROM (
						SELECT
							p.id,
							p.property_name,
							p.cid,
							pa.street_line1,
							pa.street_line2,
							pa.city,
							pa.postal_code,
							pa.state_code,
							pa.longitude,
							pa.latitude,
							CASE
								WHEN ( \'' . ( double ) $objPrimaryPropertyAddress->getLatitude() . '\' = pa.latitude AND pa.longitude = \'' . ( double ) $objPrimaryPropertyAddress->getLongitude() . '\' )
								THEN 0
								ELSE
									(
										3958.75 * ACos( Sin( ' . ( double ) $objPrimaryPropertyAddress->getLatitude() . ' / 57.2958 ) *
										Sin( pa.latitude::Float / 57.2958 ) +
										Cos( ' . ( double ) $objPrimaryPropertyAddress->getLatitude() . ' / 57.2958 ) *
										Cos( pa.latitude::Float / 57.2958 ) *
										Cos( pa.longitude::Float / 57.2958 - ' . ( double ) $objPrimaryPropertyAddress->getLongitude() . ' / 57.2958 ) )
									) END AS miles
						FROM
							 properties p,
							 property_addresses pa

						WHERE
							 p.id = pa.property_id
							 AND p.cid = pa.cid
							 AND p.is_disabled = 0
							 AND p.cid = ' . ( int ) $intCid . '
							 AND p.id != ' . ( int ) $intPropertyId . '
							 AND pa.latitude IS NOT NULL
							 AND pa.longitude IS NOT NULL
							 AND pa.is_alternate = false
							 AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					) AS sub_query
					WHERE miles < ' . ( int ) $intMilesWithin . '
					ORDER BY
						miles; ';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchNearbyPropertiesByLatitudeByLongitudeByIdsByMilesByCid( $strLatitude, $strLongitude, $arrintPropertyIds, $intCid, $objDatabase, $intMilesWithin = 50 ) {
		if( 0 == strlen( $strLatitude ) || 0 == strlen( $strLongitude ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						miles,
						id,
						property_name,
						cid,
						street_line1,
						street_line2,
						city,
						postal_code,
						state_code,
						longitude,
						latitude
					FROM (
								SELECT
									p.id,
									p.property_name,
									p.cid,
									pa.street_line1,
									pa.street_line2,
									pa.city,
									pa.postal_code,
									pa.state_code,
									pa.longitude,
									pa.latitude,
									CASE
										WHEN ( \'' . addslashes( $strLatitude ) . '\' = pa.latitude AND pa.longitude = \'' . addslashes( $strLongitude ) . '\' )
										THEN 0
										ELSE
											(
												3958.75 * ACos( Sin( ' . addslashes( $strLatitude ) . ' / 57.2958 ) *
												Sin( pa.latitude::Float / 57.2958 ) +
												Cos( ' . addslashes( $strLatitude ) . ' / 57.2958 ) *
												Cos( pa.latitude::Float / 57.2958 ) *
												Cos( pa.longitude::Float / 57.2958 - ' . addslashes( $strLongitude ) . ' / 57.2958 ) )
											) END AS miles
								FROM
									 properties p,
									 property_addresses pa
								WHERE
									 p.id = pa.property_id
									 AND p.cid = pa.cid
									 AND p.is_disabled = 0
									 AND p.cid = ' . ( int ) $intCid . '
									 AND pa.latitude IS NOT NULL
									 AND pa.longitude IS NOT NULL
									 AND pa.is_alternate = false
									 AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
							) AS sub_query
					WHERE
						miles < ' . ( int ) $intMilesWithin . '
					ORDER BY
						miles; ';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertiesWithDatabaseIdByPostalCode( $intPostalCode, $objDatabase ) {
		$strSql = 'SELECT
						p.id,
						p.property_name,
						mc.database_id
					FROM
						properties p,
						sem_property_details spd,
						clients mc
					WHERE
						p.id = spd.property_id
						AND p.cid = mc.id
						AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
						AND p.is_disabled = 0
						AND spd.postal_code IS NOT NULL
						AND spd.postal_code = \'' . ( int ) $intPostalCode . '\'';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCustomNearByPropertiesWithDatabaseIdByLatitudeByLongitudeByMiles( $fltLatitude, $fltLongitude, $fltMilesWithin, $objDatabase ) {
		if( 0 == strlen( $fltLatitude ) || 0 == strlen( $fltLongitude ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						miles,
						id,
						property_name,
						database_id,
						cid,
						street_line1,
						street_line2,
						city,
						postal_code,
						state_code,
						longitude,
						latitude
					FROM (
							SELECT
								p.id,
								p.property_name,
								p.cid,
								mc.database_id,
								spd.street_line1,
								spd.street_line2,
								spd.city,
								spd.postal_code,
								spd.state_code,
								spd.longitude,
								spd.latitude,
								CASE
									WHEN ( \'' . ( double ) $fltLatitude . '\' = spd.latitude AND spd.longitude = \'' . $fltLongitude . '\' )
									THEN 0
									ELSE
										(
											3958.75 * ACos( Sin( ' . ( double ) $fltLatitude . ' / 57.2958 ) *
											Sin( spd.latitude::Float / 57.2958 ) +
											Cos( ' . ( double ) $fltLatitude . ' / 57.2958 ) *
											Cos( spd.latitude::Float / 57.2958 ) *
											Cos( spd.longitude::Float / 57.2958 - ' . ( double ) $fltLongitude . ' / 57.2958 ) )
										) END AS miles
							FROM
								 properties p,
								 clients mc,
								 sem_property_details spd
							WHERE
								 p.id = spd.property_id
								 AND p.cid = mc.id
								 AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
								 AND p.is_disabled = 0
								 AND spd.latitude IS NOT NULL
								 AND spd.longitude IS NOT NULL
						) AS sub_query
					WHERE
						miles < ' . ( float ) $fltMilesWithin . '
					ORDER BY
						miles';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchNearByCommunitiesByCid( $objPrimaryPropertyAddress, $intCid, $intPropertyId, $objDatabase ) {
		if( false == valObj( $objPrimaryPropertyAddress, 'CPropertyAddress' ) ) {
			return NULL;
		}

		if( true == is_null( $objPrimaryPropertyAddress->getLongitude() ) || true == is_null( $objPrimaryPropertyAddress->getLatitude() ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						miles,
						id,
						property_name,
						cid,
						street_line1,
						street_line2,
						city,
						postal_code,
						state_code,
						longitude,
						latitude,
						min_rent,
						max_rent,
						max_bedrooms,
						max_bathrooms,
						short_description,
						full_description
					FROM (
							SELECT
								p.id ,
								p.property_name,
								p.cid,
								spd.street_line1,
								spd.street_line2,
								spd.city,
								spd.postal_code,
								spd.state_code,
								spd.longitude,
								spd.latitude,
								p.min_rent,
								p.max_rent,
								p.max_bedrooms,
								p.max_bathrooms,
								p.short_description,
								p.full_description,
								CASE
									WHEN ( \'' . ( double ) $objPrimaryPropertyAddress->getLatitude() . '\' = spd.latitude AND spd.longitude = \'' . ( double ) $objPrimaryPropertyAddress->getLongitude() . '\' )
									THEN 0
									ELSE
										(
											3958.75 * ACos( Sin( ' . ( double ) $objPrimaryPropertyAddress->getLatitude() . ' / 57.2958 ) *
											Sin( spd.latitude::Float / 57.2958 ) +
											Cos( ' . ( double ) $objPrimaryPropertyAddress->getLatitude() . ' / 57.2958 ) *
											Cos( spd.latitude::Float / 57.2958 ) *
											Cos( spd.longitude::Float / 57.2958 - ' . ( double ) $objPrimaryPropertyAddress->getLongitude() . ' / 57.2958 ) )
										) END AS miles
									FROM
										 properties p,
										 sem_property_details spd
									WHERE
										p.id = spd.property_id
										AND p.is_disabled = 0
										AND spd.sem_property_status_type_id IN ( ' . CSemPropertyStatusType::ENABLED . ', ' . CSemPropertyStatusType::PAUSED . ', ' . CSemPropertyStatusType::DEMO . ' )
										AND p.cid = ' . ( int ) $intCid . '
										AND p.id != ' . ( int ) $intPropertyId . '
										AND spd.latitude IS NOT NULL
										AND spd.longitude IS NOT NULL
								) AS sub_query
					WHERE
						miles < 50
					ORDER BY
						miles; ';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function buildCorePortalSearchRelevanceSql( $intWebsiteId, $objPropertySearch, $boolApplicationLayer, $intCid, $objDatabase ) {
		$intAllowedMaxScore               = 11;
		$strCaseParameters                = '';
		$strSearchScore                   = '';
		$strPropertyPreferencesParameters = '';
		$arrstrWhereParameters            = [];

		if( true == isset ( $objPropertySearch ) ) {

			if( true == is_numeric( $objPropertySearch->getPropertyTypeId() ) ) {
				array_push( $arrstrWhereParameters, 'p.property_type_id=' . ( int ) $objPropertySearch->getPropertyTypeId() . ' AND p.cid = ' . ( int ) $intCid );
			}

			if( true == $objPropertySearch->getShowAvailableOnly() ) {
				array_push( $arrstrWhereParameters, 'p.has_availability = 1' );
			}

			if( false == is_null( $objPropertySearch->getAllowDogs() ) && 0 != strlen( trim( $objPropertySearch->getAllowDogs() ) ) ) {
				array_push( $arrstrWhereParameters, 'p.allows_dogs=1' );
			}

			if( false == is_null( $objPropertySearch->getAllowCats() ) && 0 != strlen( trim( $objPropertySearch->getAllowCats() ) ) ) {
				array_push( $arrstrWhereParameters, 'p.allows_cats=1' );
			}

			if( false == is_null( $objPropertySearch->getPetFriendly() ) && 0 != strlen( trim( $objPropertySearch->getPetFriendly() ) ) ) {
				array_push( $arrstrWhereParameters, '( p.allows_cats = 1 OR p.allows_dogs = 1 )' );
			}

			if( false == is_null( $objPropertySearch->getPricingOptionStudent() ) && 0 != strlen( trim( $objPropertySearch->getPricingOptionStudent() ) ) ) {
				$strPropertyPreferencesParameters = ' INNER JOIN property_preferences pp on ( p.id = pp.property_id AND p.cid = pp.cid
			 AND pp.key = \'PRICING_OPTIONS\' AND ( pp.value = \'STUDENT\' OR pp.value = \'QUASI\' ) ) ';
			}

			if( false == is_null( $objPropertySearch->getPropertyName() ) && 0 != strlen( trim( $objPropertySearch->getPropertyName() ) ) ) {

				$arrstrPropertyNames = explode( ' ', trim( $objPropertySearch->getPropertyName() ) );
				$strCaseParameters   .= ', CASE WHEN p.property_name ILIKE \'%' . addslashes( trim( $objPropertySearch->getPropertyName() ) ) . '%\' THEN 0';

				foreach( $arrstrPropertyNames as $strPropertyName ) {
					if( 0 < strlen( trim( $strPropertyName ) ) ) {
						$strCaseParameters .= 'WHEN p.property_name ILIKE \'%' . addslashes( $strPropertyName ) . '%\' THEN 1';
					}
				}

				$strCaseParameters .= ' ELSE ' . ( int ) $intAllowedMaxScore + 1;
				$strCaseParameters .= 'END AS property_name_score';
				$strSearchScore    .= ' + property_name_score::numeric';
			}

			// Number of bedroom
			if( false == is_null( $objPropertySearch->getNumberOfBedrooms() ) && true == is_numeric( $objPropertySearch->getNumberOfBedrooms() ) ) {

				$strCaseParameters .= ', CASE WHEN p.max_bedrooms < ' . ( int ) $objPropertySearch->getNumberOfBedrooms() . ' THEN ( ' . ( int ) $objPropertySearch->getNumberOfBedrooms() . ' - p.max_bedrooms )
											 WHEN p.min_bedrooms > ' . ( int ) $objPropertySearch->getNumberOfBedrooms() . ' THEN .25
											 ELSE 0
										END AS bedrooms_score';

				array_push( $arrstrWhereParameters, 'p.min_bedrooms IS NOT NULL' );
				array_push( $arrstrWhereParameters, 'p.max_bedrooms IS NOT NULL' );
				$strSearchScore .= ' + bedrooms_score::numeric';
			}

			// Number of bathroom
			if( false == is_null( $objPropertySearch->getNumberOfBathrooms() ) && true == is_numeric( $objPropertySearch->getNumberOfBathrooms() ) ) {

				$strCaseParameters .= ', CASE WHEN p.max_bathrooms < ' . ( float ) $objPropertySearch->getNumberOfBathrooms() . ' THEN ( ' . ( float ) $objPropertySearch->getNumberOfBathrooms() . ' - p.max_bathrooms )
											WHEN p.min_bathrooms > ' . ( float ) $objPropertySearch->getNumberOfBathrooms() . ' THEN .25
											ELSE 0
										END AS bathrooms_score';

				array_push( $arrstrWhereParameters, 'p.min_bathrooms IS NOT NULL' );
				array_push( $arrstrWhereParameters, 'p.max_bathrooms IS NOT NULL' );
				$strSearchScore .= ' + bathrooms_score::numeric';
			}

			// Square Feet
			if( ( 0 != $objPropertySearch->getMinSquareFeet() && true == is_numeric( $objPropertySearch->getMinSquareFeet() ) ) || ( 0 != $objPropertySearch->getMaxSquareFeet() && true == is_numeric( $objPropertySearch->getMaxSquareFeet() ) ) ) {
				$fltMinSquareFeet = $objPropertySearch->getMinSquareFeet();
				$fltMaxSquareFeet = $objPropertySearch->getMaxSquareFeet();

				if( ( 0 != $objPropertySearch->getMinSquareFeet() && true == is_numeric( $objPropertySearch->getMinSquareFeet() ) ) && ( 0 == $objPropertySearch->getMaxSquareFeet() || false == is_numeric( $objPropertySearch->getMaxSquareFeet() ) ) ) {
					$fltMinSquareFeet = $objPropertySearch->getMinSquareFeet();
					$fltMaxSquareFeet = $objPropertySearch->getMinSquareFeet();
				}

				if( ( 0 == $objPropertySearch->getMinSquareFeet() || false == is_numeric( $objPropertySearch->getMinSquareFeet() ) ) && ( 0 != $objPropertySearch->getMaxSquareFeet() && true == is_numeric( $objPropertySearch->getMaxSquareFeet() ) ) ) {
					$fltMinSquareFeet = $objPropertySearch->getMaxSquareFeet();
					$fltMaxSquareFeet = $objPropertySearch->getMaxSquareFeet();
				}

				$strCaseParameters .= ', CASE WHEN p.max_square_feet < ' . $fltMinSquareFeet . '
												 THEN ( ( ' . ( float ) $fltMinSquareFeet . '/p.max_square_feet::numeric ) - 1 ) / .2
											WHEN p.min_square_feet > ' . ( float ) $fltMaxSquareFeet . '
												 THEN .25
											ELSE 0
										END AS square_feet_score';

				if( 0 != $objPropertySearch->getMinSquareFeet() && true == is_numeric( $objPropertySearch->getMinSquareFeet() ) ) {
					array_push( $arrstrWhereParameters, 'p.min_square_feet IS NOT NULL' );
				}

				if( 0 != $objPropertySearch->getMaxSquareFeet() && true == is_numeric( $objPropertySearch->getMaxSquareFeet() ) ) {
					array_push( $arrstrWhereParameters, 'p.max_square_feet IS NOT NULL' );
				}

				$strSearchScore .= ' + square_feet_score::numeric';
			}

			// Rent
			if( ( 0 != $objPropertySearch->getMinRent() && true == is_numeric( $objPropertySearch->getMinRent() ) ) || ( 0 != $objPropertySearch->getMaxRent() && true == is_numeric( $objPropertySearch->getMaxRent() ) ) ) {

				$strCaseParameters .= ', CASE ';

				$strMaxRentWhen = ' WHEN p.min_rent > ' . ( float ) $objPropertySearch->getMaxRent() . ' THEN ( ( p.min_rent / ' . ( float ) $objPropertySearch->getMaxRent() . '::numeric ) - 1 ) / .1 ';
				$strMinRentWhen = ' WHEN p.max_rent < ' . ( float ) $objPropertySearch->getMinRent() . ' THEN .5 ';

				if( ( 0 == $objPropertySearch->getMinRent() || false == is_numeric( $objPropertySearch->getMinRent() ) ) && ( 0 != $objPropertySearch->getMaxRent() && true == is_numeric( $objPropertySearch->getMaxRent() ) ) ) {
					$strCaseParameters .= $strMaxRentWhen;
				} else {
					if( ( 0 != $objPropertySearch->getMinRent() && true == is_numeric( $objPropertySearch->getMinRent() ) ) && ( 0 == $objPropertySearch->getMaxRent() || false == is_numeric( $objPropertySearch->getMaxRent() ) ) ) {
						$strCaseParameters .= $strMinRentWhen;
					} else {
						$strCaseParameters .= $strMaxRentWhen . $strMinRentWhen;
					}
				}

				$strCaseParameters .= ' ELSE 0 END AS rent_score';

				if( 0 != $objPropertySearch->getMinRent() && true == is_numeric( $objPropertySearch->getMinRent() ) ) {
					array_push( $arrstrWhereParameters, 'p.min_rent IS NOT NULL' );
				}
				if( 0 != $objPropertySearch->getMaxRent() && true == is_numeric( $objPropertySearch->getMaxRent() ) ) {
					array_push( $arrstrWhereParameters, 'p.max_rent IS NOT NULL' );
				}

				$strSearchScore .= ' + rent_score::numeric';
			}

			// General Amenities or community Ammenities
			if( false == is_null( $objPropertySearch->getCommunityDefaultAmenityIds() ) && 0 != strlen( $objPropertySearch->getCommunityDefaultAmenityIds() ) ) {
				$arrintCommunityDefaultAmenityIds = explode( ',', $objPropertySearch->getCommunityDefaultAmenityIds() );

				$strCaseParameters .= ', ' . \Psi\Libraries\UtilFunctions\count( $arrintCommunityDefaultAmenityIds ) . ' - (
										SELECT count( id )
										FROM (
											SELECT DISTINCT( a.default_amenity_id ) AS id,
											ra.cid
											FROM rate_associations AS ra
											INNER JOIN amenities AS a
												ON ( ra.ar_origin_reference_id = a.id AND ra.cid = a.cid
													 AND ra.ar_origin_id = ' . CArOrigin::AMENITY . '
													 AND ra.ar_cascade_id = ' . CArCascade::PROPERTY . '
													 AND a.amenity_type_id = ' . CAmenityType::COMMUNITY . ' )
												AND ra.is_published = true
												AND a.cid = ' . ( int ) $intCid . '
												AND a.default_amenity_id IN ( ' . $objPropertySearch->getCommunityDefaultAmenityIds() . ' )
											WHERE ra.property_id = p.id AND ra.cid = p.cid ) AS s_amen WHERE s_amen.cid = ' . ( int ) $intCid . ' ) AS general_amenity_score';

				$strSearchScore .= ' + general_amenity_score::numeric';

			} else {
				if( false == is_null( $objPropertySearch->getDefaultAmenityIds() ) && 0 != strlen( $objPropertySearch->getDefaultAmenityIds() ) ) {
					$arrintDefaultAmenityIds = explode( ',', $objPropertySearch->getDefaultAmenityIds() );

					$strCaseParameters .= ', ' . \Psi\Libraries\UtilFunctions\count( $arrintDefaultAmenityIds ) . ' - (
										SELECT count( id )
										FROM (
											SELECT DISTINCT( a.default_amenity_id ) AS id,
											ra.cid
											FROM rate_associations AS ra
											INNER JOIN amenities AS a
												ON ( ra.ar_origin_reference_id = a.id AND ra.cid = a.cid
													 AND ra.ar_origin_id = ' . CArOrigin::AMENITY . '
													 AND ra.ar_cascade_id = ' . CArCascade::PROPERTY . '
													 AND a.amenity_type_id = ' . CAmenityType::COMMUNITY . ' )
												AND ra.is_published = true
												AND a.cid = ' . ( int ) $intCid . '
												AND a.default_amenity_id IN ( ' . $objPropertySearch->getDefaultAmenityIds() . ' )
											WHERE ra.property_id = p.id AND ra.cid = p.cid ) AS s_amen WHERE s_amen.cid = ' . ( int ) $intCid . ' ) AS general_amenity_score';

					$strSearchScore .= ' + general_amenity_score::numeric';

				}
			}

			// Specific Amenities or Unit Ammenities
			if( false == is_null( $objPropertySearch->getApartmentDefaultAmenityIds() ) && 0 != strlen( $objPropertySearch->getApartmentDefaultAmenityIds() ) ) {
				$arrintApartmentDefaultAmenityIds = explode( ',', $objPropertySearch->getApartmentDefaultAmenityIds() );

				$strCaseParameters .= ', ' . \Psi\Libraries\UtilFunctions\count( $arrintApartmentDefaultAmenityIds ) . ' - (
										SELECT count( id )
											FROM (
												SELECT DISTINCT( a.default_amenity_id ) AS id,
													ra.cid
												FROM rate_associations AS ra
													INNER JOIN amenities AS a
												ON ( ra.ar_origin_reference_id = a.id AND ra.cid = a.cid
														 AND ra.ar_origin_id = ' . CArOrigin::AMENITY . '
													 AND ra.ar_cascade_id = ' . CArCascade::PROPERTY . '
													 AND a.amenity_type_id = ' . CAmenityType::APARTMENT . ' )
												AND ra.is_published = true
													AND a.cid = ' . ( int ) $intCid . '
												AND a.default_amenity_id IN ( ' . $objPropertySearch->getApartmentDefaultAmenityIds() . ' )
											WHERE ra.property_id = p.id AND ra.cid = p.cid ) AS s_amen WHERE s_amen.cid = ' . ( int ) $intCid . ' ) AS specific_amenity_score';

				$strSearchScore .= ' + specific_amenity_score::numeric';

			}

			// Year build
			if( 0 < strlen( $objPropertySearch->getYearBuilt() ) ) {

				$strCaseParameters .= ', CASE WHEN p.year_built <= \'1/1/' . ( int ) $objPropertySearch->getYearBuilt() . ' THEN 1 ELSE 0 END AS built_score';

				array_push( $arrstrWhereParameters, 'p.year_built IS NOT NULL' );
				$strSearchScore .= ' + built_score::numeric';
			}

			$fltLongitude = NULL;
			$fltLatitude  = NULL;

			// getLongitude, getLatitude by city
			if( false == is_null( $objPropertySearch->getCity() ) && 0 != strlen( trim( $objPropertySearch->getCity() ) ) ) {
				$fltLongitude = $objPropertySearch->getLongitude();
				$fltLatitude  = $objPropertySearch->getLatitude();
			}

			// getLongitude, getLatitude by area code
			if( ( false == is_null( $objPropertySearch->getCompanyRegionId() ) && 0 != strlen( $objPropertySearch->getCompanyRegionId() ) ) || ( false == is_null( $objPropertySearch->getCompanyAreaId() ) && 0 != strlen( $objPropertySearch->getCompanyAreaId() ) ) ) {

				if( false == is_null( $objPropertySearch->getCompanyAreaId() ) && 0 != strlen( $objPropertySearch->getCompanyAreaId() ) ) {
					$intCompanyAreaId = $objPropertySearch->getCompanyAreaId();
				} else {
					$intCompanyAreaId = $objPropertySearch->getCompanyRegionId();
				}

				$objCompanyArea = \Psi\Eos\Entrata\CCompanyAreas::createService()->fetchCompanyAreaByIdByCid( $intCompanyAreaId, $intCid, $objDatabase );

				if( true == valObj( $objCompanyArea, 'CCompanyArea' ) ) {
					$fltLongitude = $objCompanyArea->getLongitude();
					$fltLatitude  = $objCompanyArea->getLatitude();
				}
			}

			// getLongitude, getLatitude by zip
			if( true == is_numeric( $objPropertySearch->getMilesRadius() ) && false == is_null( $objPropertySearch->getPostalCode() ) && 0 != strlen( trim( $objPropertySearch->getPostalCode() ) ) ) {
				$fltLongitude = $objPropertySearch->getLongitude();
				$fltLatitude  = $objPropertySearch->getLatitude();
			}

			if( ( false == is_null( $objPropertySearch->getCompanyRegionId() ) && 0 != strlen( $objPropertySearch->getCompanyRegionId() ) )
			    || ( true == is_null( $objPropertySearch->getCompanyAreaId() ) && 0 != strlen( $objPropertySearch->getCompanyAreaId() ) )
			    || ( 0 < strlen( $fltLongitude ) && 0 < strlen( $fltLatitude ) )
			    || ( false == is_null( $objPropertySearch->getPostalCode() ) && 0 != strlen( $objPropertySearch->getPostalCode() ) )
			    || ( ( false == is_null( $objPropertySearch->getCity() ) && 0 != strlen( $objPropertySearch->getCity() ) )
			         && ( false == is_null( $objPropertySearch->getStateCode() ) && 0 != strlen( $objPropertySearch->getStateCode() ) ) ) ) {
				$strCaseParameters .= ', CASE ';

				if( false == is_null( $objPropertySearch->getPostalCode() ) ) {
					$strCaseParameters .= ' WHEN \'' . addslashes( $objPropertySearch->getPostalCode() ) . '\' = pa.postal_code THEN 0 ';

				} else {
					if( false == is_null( $objPropertySearch->getCompanyAreaId() ) && 0 != strlen( $objPropertySearch->getCompanyAreaId() ) ) {
						$strCaseParameters .= 'WHEN EXISTS ( SELECT * FROM property_areas AS p_area WHERE p.id = p_area.property_id AND p.cid = p_area.cid AND p_area.company_area_id = ' . ( int ) $objPropertySearch->getCompanyAreaId() . ' AND p_area.cid = ' . ( int ) $intCid . ' )
								THEN 0 ';

					} else {
						if( ( false == is_null( $objPropertySearch->getCompanyRegionId() ) && 0 != strlen( $objPropertySearch->getCompanyRegionId() ) ) && ( true == is_null( $objPropertySearch->getCompanyAreaId() ) || 0 == strlen( $objPropertySearch->getCompanyAreaId() ) ) ) {
							$strCaseParameters .= 'WHEN p.id IN ( SELECT
								property_id
							FROM
								property_areas
							WHERE
								company_area_id = ' . ( int ) $objPropertySearch->getCompanyRegionId() . '
								AND cid = ' . ( int ) $intCid . '
								OR company_area_id IN ( SELECT
															ca_child.id
														FROM
															company_areas ca_child
															JOIN company_areas ca_parent ON ( ca_child.company_area_id = ca_parent.id AND ca_child.cid = ca_parent.cid ) AND ca_parent.id = ' . ( int ) $objPropertySearch->getCompanyRegionId() . ' AND ca_parent.cid = ' . ( int ) $intCid . ' ) )
								THEN 0 ';
						} else {
							if( ( true == is_null( $objPropertySearch->getCompanyRegionId() ) || 0 == strlen( $objPropertySearch->getCompanyRegionId() ) ) && ( true == is_null( $objPropertySearch->getCompanyAreaId() ) || 0 == strlen( $objPropertySearch->getCompanyAreaId() ) ) && ( false == is_null( $objPropertySearch->getCity() ) && 0 != strlen( $objPropertySearch->getCity() ) ) && ( false == is_null( $objPropertySearch->getStateCode() ) && 0 != strlen( $objPropertySearch->getStateCode() ) ) ) {
								$strCaseParameters .= ' WHEN pa.city ILIKE \'' . addslashes( $objPropertySearch->getCity() ) . '\' AND pa.state_code = \'' . addslashes( $objPropertySearch->getStateCode() ) . '\'
										THEN 0';
							}
						}
					}
				}

				if( 0 < strlen( $fltLongitude ) && 0 < strlen( $fltLatitude ) ) {
					$strCaseParameters .= ' WHEN ( \'' . ( double ) $fltLatitude . '\' = pa.latitude AND pa.longitude = \'' . ( double ) $fltLongitude . '\' )
												THEN 0
											WHEN pa.latitude IS NOT NULL AND pa.longitude IS NOT NULL
												THEN
												( 3958.75 * ACos( Sin( ' . ( double ) $fltLatitude . ' / 57.2958 ) *
												 Sin( pa.latitude::Float / 57.2958 ) +
												 Cos( ' . ( double ) $fltLatitude . ' / 57.2958 ) *
												 Cos( pa.latitude::Float / 57.2958 ) *
												 Cos( pa.longitude::Float / 57.2958 - ' . ( double ) $fltLongitude . ' / 57.2958 ) )
												) ^ 2 / 400
											ELSE 10
											END AS miles_score';

				} else {
					$strCaseParameters .= ' WHEN pa.latitude IS NULL AND pa.longitude IS NULL THEN 10 ELSE 10 END AS miles_score ';
				}

				$strSearchScore .= ' + miles_score::numeric';

			} else {
				if( false == is_null( $objPropertySearch->getStateCode() ) && 0 != strlen( $objPropertySearch->getStateCode() ) ) {
					array_push( $arrstrWhereParameters, 'pa.state_code = \'' . addslashes( $objPropertySearch->getStateCode() ) . '\'' );
				}
			}

			$strMoveInParameters = '';
			if( 0 < strlen( $objPropertySearch->getMoveInDate() ) ) {

				$strMoveInDate = $objPropertySearch->getMoveInDate();

				if( false == CValidation::validateDate( $objPropertySearch->getMoveInDate() ) ) {
					$strMoveInDate = date( 'm/d/Y' );
				}

				$intMoveInDate = strtotime( $strMoveInDate );

				if( $intMoveInDate < time() ) {
					$strMoveInDate = date( 'm/d/Y' );
				}

				$strMoveInParameters .= ', CASE WHEN EXISTS (
												SELECT
													us.*,
													days_after_available_on_for_available_unit.value AS days_after_available_on_for_available_unit,
													days_after_available_on_for_notice_unit.value AS days_after_available_on_for_notice_unit,
													days_before_available_on.value AS days_before_available_on
												FROM
													unit_spaces AS us
													LEFT OUTER JOIN property_preferences AS days_after_available_on_for_available_unit
													ON ( days_after_available_on_for_available_unit.property_id = us.property_id
													AND days_after_available_on_for_available_unit.cid = us.cid )
													AND days_after_available_on_for_available_unit.key = \'DAYS_AFTER_AVAILABLE_ON_AVAILABLE_UNIT\'
													LEFT OUTER JOIN property_preferences AS days_after_available_on_for_notice_unit
													ON ( days_after_available_on_for_notice_unit.property_id = us.property_id
													AND days_after_available_on_for_notice_unit.cid = us.cid )
													AND days_after_available_on_for_notice_unit.key = \'DAYS_AFTER_AVAILABLE_ON_NOTICE_UNIT\'
													LEFT OUTER JOIN property_preferences AS days_before_available_on
													ON ( days_before_available_on.property_id = us.property_id
													AND days_before_available_on.cid = us.cid )
													AND days_before_available_on.key = \'DAYS_BEFORE_AVAILABLE_ON\'
												WHERE
													us.property_id = p.id
													AND us.deleted_on IS NULL
													AND us.cid = p.cid
													AND us.unit_space_status_type_id IN ( ' . implode( ',', CUnitSpaceStatusType::$c_arrintAvailableUnitSpaceStatusTypes ) . ' )
													AND (
															(
																us.unit_space_status_type_id IN ( ' . CUnitSpaceStatusType::VACANT_UNRENTED_READY . ',' . CUnitSpaceStatusType::VACANT_UNRENTED_NOT_READY . ' )
																AND
																( days_after_available_on_for_available_unit.value IS NULL
																	OR
																		\'' . addslashes( $strMoveInDate ) . '\' <= CURRENT_DATE + ( days_after_available_on_for_available_unit.value || \' days\' )::INTERVAL
																	OR
																		\'' . addslashes( $strMoveInDate ) . '\' <= us.available_on + ( days_after_available_on_for_available_unit.value || \' days\' )::INTERVAL
																)
															)
														OR
															(
																us.unit_space_status_type_id IN ( ' . CUnitSpaceStatusType::NOTICE_UNRENTED . ' )
																AND
																( days_after_available_on_for_notice_unit.value IS NULL
																	OR
																		\'' . addslashes( $strMoveInDate ) . '\' <= CURRENT_DATE + ( days_after_available_on_for_notice_unit.value || \' days\' )::INTERVAL
																	OR
																		\'' . addslashes( $strMoveInDate ) . '\' <= us.available_on + ( days_after_available_on_for_notice_unit.value || \' days\' )::INTERVAL
																)
															)
														)

													AND
														(
															us.available_on IS NOT NULL OR days_before_available_on.value IS NULL
															OR
															\'' . addslashes( $strMoveInDate ) . '\' >= us.available_on - ( days_before_available_on.value || \' days\' )::INTERVAL
														)
													AND ( us.reserve_until IS NULL OR us.reserve_until <= \'' . addslashes( $strMoveInDate ) . '\' )
										) THEN 0 ELSE 4 END AS move_in_score';

				$strSearchScore .= ' + move_in_score::numeric';
			}
		}

		$strWhereParameters          = '';
		$strPortalTypeFieldReference = ( 'portal' == $boolApplicationLayer ) ? 'wp.hide_on_prospect_portal' : 'wp.hide_on_resident_portal';

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrWhereParameters ) ) {
			$strWhereParameters = ' AND ' . implode( ' AND ', $arrstrWhereParameters );
		}

		$strSql = 'SELECT *
					FROM (
						SELECT
							*,
							( 0 ' . $strSearchScore . ' ) AS total_score
						FROM (
							SELECT
								p.id,
								p.cid,
								p.property_type_id,
								p.vaultware_number,
								p.property_name,
								p.number_of_units,
								p.min_rent,
								p.max_rent,
								p.min_square_feet,
								p.max_square_feet,
								p.min_bedrooms,
								p.max_bedrooms,
								p.min_bathrooms,
								p.max_bathrooms,
								p.year_built,
								pd.community_website_uri,
								pd.corporate_website_uri,
								wp.hide_more_info_button,
								wp.show_application_fee_option,
								wp.more_info_action,
								pa.city,
								pa.city AS seo_city,
								pa.state_code,
								CASE WHEN ( 0 = wp.order_num ) THEN NULL ELSE wp.order_num END AS modified_order_num
								' . $strCaseParameters . ' ' . $strMoveInParameters . ',
								(SELECT
									count(c_app.id)
								 FROM
									property_applications as p_app
									LEFT OUTER JOIN company_applications AS c_app ON ( c_app.id = p_app.company_application_id AND c_app.cid = p_app.cid AND c_app.is_published = 1 AND c_app.deleted_on IS NULL )
								 WHERE
									p_app.property_id = p.id AND p_app.cid = p.cid
								) AS property_application_count
							FROM
								properties p
								INNER JOIN property_details AS pd ON ( p.id = pd.property_id AND p.cid = pd.cid )
								INNER JOIN website_properties AS wp ON ( wp.website_id = ' . ( int ) $intWebsiteId . ' AND p.id = wp.property_id AND p.cid = wp.cid AND ' . $strPortalTypeFieldReference . ' <> 1 )
								INNER JOIN property_addresses AS pa ON ( p.id = pa.property_id AND p.cid = pa.cid AND pa.address_type_id = ' . CAddressType::PRIMARY . ' AND pa.is_alternate = false )
								' . $strPropertyPreferencesParameters . '
							WHERE
								p.is_disabled <> 1
								AND p.cid = ' . ( int ) $intCid . ' AND p.property_id IS NULL';

		if( 0 < strlen( $strWhereParameters ) ) {
			$strSql .= $strWhereParameters;
		}

		$strSql .= '		 ) AS pts
						) AS pts
						WHERE total_score <= ' . ( int ) $intAllowedMaxScore . '
					ORDER BY
						total_score';

		if( true == isset( $objPropertySearch ) && 'sort_number' == $objPropertySearch->getSearchResultSortBy() ) {
			$strSql .= ', modified_order_num';
		}

		$strSql .= ', property_name';

		return $strSql;
	}

	public static function fetchAllowedRelevancePropertyCountByWebsiteIdByCid( $intWebsiteId, $objPropertySearch, $boolApplicationLayer, $intCid, $objDatabase ) {
		$strSql = self::buildCorePortalSearchRelevanceSql( $intWebsiteId, $objPropertySearch, $boolApplicationLayer, $intCid, $objDatabase );

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) {
			return \Psi\Libraries\UtilFunctions\count( $arrstrData );
		} else {
			return 0;
		}
	}

	public static function fetchAllowedRelevanceProspectPortalPropertiesByWebsiteIdByCid( $intSelectedPage, $intCountPerPage, $intWebsiteId, $objPropertySearch, $boolApplicationLayer, $intCid, $objDatabase ) {
		$strSql = self::buildCorePortalSearchRelevanceSql( $intWebsiteId, $objPropertySearch, $boolApplicationLayer, $intCid, $objDatabase );

		if( 0 != $intSelectedPage && 0 != $intCountPerPage ) {
			$strSql .= ' OFFSET ' . ( int ) ( ( $intSelectedPage - 1 ) * $intCountPerPage ) . ' LIMIT ' . ( int ) $intCountPerPage;
		}

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByAddressTypeIdByCid( $intAddressTypeId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						p.*,pa.state_code
					FROM
						properties AS p
						LEFT JOIN property_addresses pa ON ( p.cid = pa.cid AND p.id = pa.property_id AND pa.address_type_id = ' . ( int ) $intAddressTypeId . ' AND pa.cid = ' . ( int ) $intCid . ' AND pa.is_alternate = false )
					WHERE
						p.cid = ' . ( int ) $intCid;

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchActivePropertiesByCid( $intCid, $objDatabase, $boolExcludeTestAndManagerial = false ) {
		$strWhere = '';
		if( true == $boolExcludeTestAndManagerial ) {
			$strWhere = ' AND is_test = 0 AND is_managerial = 0 ';
		}
		$strSql = 'SELECT * FROM properties WHERE is_disabled <> 1 AND property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' ) AND cid = ' . ( int ) $intCid . ' AND termination_date IS NULL ' . $strWhere . '
					ORDER BY
					  LOWER( property_name ) ASC';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchActivePropertyAccountsByCid( $intCid, $objDatabase, $objPagination = NULL, $boolCountOnly = false, $strQuickSearch = NULL ) {
		$strSearchCriteria = '';

		if( true == valStr( $strQuickSearch ) ) {
			$strSearchCriteria = ' AND ( p.property_name ILIKE E\'%' . $strQuickSearch . '%\' ';
			$strSearchCriteria .= ( 1 == \Psi\Libraries\UtilFunctions\count( $strQuickSearch ) && is_numeric( $strQuickSearch ) ) ? ' OR to_char( p.id, \'99999999\' ) LIKE \'%' . $strQuickSearch . '%\' ' : '';
			$strSearchCriteria .= ')';
		}

		if( $boolCountOnly == false && true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strOffsetSql   = ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
			$strOrderByType = ( 1 == $objPagination->getOrderByType() ) ? 'desc' : 'asc';

			switch( \Psi\CStringService::singleton()->strtolower( $objPagination->getOrderByField() ) ) {
				case 'id':
					$strOrderByField = 'p.id ';
					break;

				case 'property_name':
					$strOrderByField = ' LOWER( p.property_name)';
					break;

				case NULL:
				default:
					$strOrderByField = ' LOWER( p.property_name ) ';
			}

			$strOrderBy = ' ORDER BY ' . $strOrderByField . $strOrderByType;
		} else {

			$strOrderBy = ' ORDER BY LOWER( p.property_name ) ASC ';
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.country_code,
						count ( DISTINCT cp.recurring_account_id ) as no_of_account_ids,
						array_to_string(array_agg(distinct cp.recurring_account_id), \' \') as account_id,
						count ( DISTINCT cp.id ) AS no_of_contract_properties
					FROM
						properties p
						LEFT JOIN contract_properties cp ON ( p.id = cp.property_id AND p.cid = cp.cid )
						LEFT JOIN contracts co ON ( co.id = cp.contract_id AND co.cid = cp.cid AND co.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . ' )
						LEFT JOIN accounts a ON ( a.id = cp.recurring_account_id )
					WHERE
						p.is_disabled <> 1
						AND ( a.cid = cp.cid OR a.cid IS NULL )
						AND p.cid = ' . ( int ) $intCid . '
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						AND p.termination_date IS NULL
						AND cp.termination_date IS NULL
						' . $strSearchCriteria . '
					GROUP BY
						p.id
						' . $strOrderBy . '
						' . $strOffsetSql;

		if( true == $boolCountOnly ) {
			$arrintResponse = fetchData( $strSql, $objDatabase );

			return \Psi\Libraries\UtilFunctions\count( $arrintResponse );
		} else {
			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchOccupancyTypeIdsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						array_to_json(p.occupancy_type_ids) as occupancy_type_ids
					FROM
						properties p
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id = ' . ( int ) $intPropertyId;

		$arrintPropertyType = fetchData( $strSql, $objDatabase );

		return ( true == isset( $arrintPropertyType[0]['occupancy_type_ids'] ) ) ? $arrintPropertyType[0]['occupancy_type_ids'] : NULL;
	}

	public static function fetchPaginatedActivePropertiesAndCountByCid( $intCid, $objDatabase, $objPagination = NULL ) {

		if( false == is_numeric( $intCid ) ) {
			return NULL;
		}

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSelectSql = 'SELECT count(p.id) over() AS row_count, p.* ';
			$strOffestSql = ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		} else {
			return NULL;
		}

		$strFromSql = '	FROM
						properties p
					WHERE
						p.is_disabled <> 1 AND
						p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' ) AND
						p.cid = ' . ( int ) $intCid . ' AND
						p.termination_date IS NULL';

		$strSql = $strSelectSql . $strFromSql . ' ORDER BY LOWER( p.property_name ) ASC ' . $strOffestSql;

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByWebsiteSubdomain( $strSubdomain, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						p.*
					FROM
						properties p
						JOIN website_properties wp on ( p.cid = wp.cid AND p.id = wp.property_id )
						JOIN websites w on ( w.cid = wp.cid AND w.id = wp.website_id )
					WHERE
						w.sub_domain = \'' . $strSubdomain . '\'
						AND p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled <> 1';

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchPropertiesByIdsByFieldNamesByCid( $arrintPropertyIds, $arrstrFieldNames, $intCid, $objDatabase, $boolReturnArray = false ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrstrFieldNames ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						' . implode( ',', $arrstrFieldNames ) . '
					FROM
						properties
					WHERE
						id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
					ORDER BY
						lower ( property_name );';
		if( true == $boolReturnArray ) {
			return fetchData( $strSql, $objDatabase );
		}

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByPropertyGroupIdsByFieldNamesByCid( $arrintPropertyGroupIds, $arrstrFieldNames, $intCid, $objDatabase, $boolReturnArray = false, $arrintPropertyIds = [], $arrintPsProductIds = [], $boolShowDisabledData = false, $boolIsUseCollateSort = false ) {
		if( false == valArr( $arrintPropertyGroupIds ) || false == valArr( $arrstrFieldNames ) ) {
			return NULL;
		}

		$strLoadPropertiesInputParams = 'ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( ',', $arrintPropertyGroupIds ) . ']::INTEGER[]';

		$strLoadPropertiesJoinCondition = '';
		if( true == valArr( $arrintPropertyIds ) ) {
			$strLoadPropertiesJoinCondition = ' AND lp.property_id = ANY( ARRAY[' . implode( ',', $arrintPropertyIds ) . ']::INTEGER[] )';
		}

		if( true == valArr( $arrintPsProductIds ) ) {
			$strLoadPropertiesInputParams .= ', ARRAY[' . implode( ',', $arrintPsProductIds ) . ']';
		}

		$strOrderBySql = 'p.property_name';
		if( $boolIsUseCollateSort ) {
			$strOrderBySql = $objDatabase->getCollateSort( 'p.property_name' );
		}

		$strSql = '
			SELECT
				' . implode( ',', $arrstrFieldNames ) . ',
				p.occupancy_type_ids
			FROM
				properties p
				JOIN load_properties( ' . $strLoadPropertiesInputParams . ' ) lp ON ( p.cid = lp.cid AND p.id = lp.property_id AND lp.is_disabled = ' . ( ( false == $boolShowDisabledData ) ? 0 : 1 ) . ' ' . $strLoadPropertiesJoinCondition . ' )
			WHERE
				p.cid = ' . ( int ) $intCid . '
				AND p.is_managerial = 0
			ORDER BY
				' . $strOrderBySql;

		if( true == $boolReturnArray ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return self::fetchProperties( $strSql, $objDatabase );
		}
	}

	public static function fetchPropertyDetailsByPropertyId( $intPropertyId, $objClientDatabase ) {

		$strSql = 'SELECT
						property_name,
						email_address
					FROM
						properties p
						LEFT JOIN property_email_addresses pea ON ( p.id = pea.property_id )
					WHERE
						p.id = ' . ( int ) $intPropertyId;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyOfficeDetailsByPropertyIdByCid( $intPropertyId, $intCid, $objClientDatabase ) {

		if( false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						CONCAT_WS(\', \',pa.street_line1, pa.street_line2, pa.street_line3, pa.city, 
						CONCAT_WS(\' \', pa.state_code, pa.postal_code ) ) AS property_address,
						pea.email_address,
						ppn.phone_number
					FROM
						properties p
						LEFT JOIN property_addresses pa ON ( p.id = pa.property_id AND p.cid = pa.cid AND pa.address_type_id = ' . CAddressType::PRIMARY . ' AND pa.is_alternate = false )
						LEFT JOIN property_email_addresses pea ON ( p.id = pea.property_id AND p.cid = pea.cid AND pea.email_address_type_id = ' . CEmailAddressType::PRIMARY . ' )
						LEFT JOIN property_phone_numbers ppn ON ( p.id = ppn.property_id AND p.cid = ppn.cid AND ppn.phone_number_type_id = ' . CPhoneNumberType::OFFICE . ' )
					WHERE
						p.cid = ' . ( int ) $intCid . '
					AND
						p.id = ' . ( int ) $intPropertyId;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPmEnabledPropertiesByPropertyGroupIdsByFieldNamesByCid( $arrintPropertyGroupIds, $arrstrFieldNames, $intCid, $objDatabase, $boolReturnArray = false, $arrintPropertyIds = [], $arrintPsProductIds = [], $arrstrKeys = [] ) {
		$strSqlJoin = '';

		if( false == valArr( $arrintPropertyGroupIds ) || false == valArr( $arrstrFieldNames ) ) {
			return NULL;
		}

		if( true == valArr( $arrstrKeys ) ) {
			$strSqlJoin = ' JOIN property_preferences pp ON ( pp.cid = pgs.cid AND pp.property_id = pgs.property_id  )';
		}
		$strLoadPropertiesInputParams = 'ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( ',', $arrintPropertyGroupIds ) . ']::INTEGER[]';

		$strLoadPropertiesJoinCondition = '';
		if( true == valArr( $arrintPropertyIds ) ) {
			$strLoadPropertiesJoinCondition = ' AND lp.property_id = ANY( ARRAY[' . implode( ',', $arrintPropertyIds ) . ']::INTEGER[] )';
		}

		if( true == valArr( $arrintPsProductIds ) ) {
			$strLoadPropertiesInputParams .= ', ARRAY[' . implode( ',', $arrintPsProductIds ) . ']';
		}

		$strSql = 'SELECT
						' . implode( ',', $arrstrFieldNames ) . '
					FROM
						properties p
						JOIN load_properties( ' . $strLoadPropertiesInputParams . ' ) lp ON ( p.cid = lp.cid AND p.id = lp.property_id AND lp.is_disabled = 0 ' . $strLoadPropertiesJoinCondition . ' )
						JOIN property_gl_settings pgs ON (pgs.cid = p.cid AND pgs.property_id = p.id AND activate_standard_posting = true)' .
		          $strSqlJoin . '
						WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_managerial = 0';
		if( true == valArr( $arrstrKeys ) ) {
			$strSql .= ' AND pp.key IN (\'' . implode( "','", $arrstrKeys ) . '\' )';
		}
		$strSql .= 'ORDER BY
						LOWER( property_name );';

		if( true == $boolReturnArray ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return self::fetchProperties( $strSql, $objDatabase );
		}
	}

	public static function fetchEntrataPropertiesByIdsByFieldNamesByCid( $arrintPropertyIds, $arrstrFieldNames, $intCid, $objDatabase, $boolReturnArray = false ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}
		if( false == valArr( $arrstrFieldNames ) ) {
			return NULL;
		}

		$strSql = 'SELECT ' . implode( ',', $arrstrFieldNames ) . '
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id AND pgs.activate_standard_posting = true )
					WHERE
						p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
						lower ( p.property_name );';

		if( true == $boolReturnArray ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return self::fetchProperties( $strSql, $objDatabase );
		}
	}

	public static function fetchSimplePropertiesByIdsByFieldNamesByCid( $arrintPropertyIds, $arrstrFieldNames, $intCid, $objDatabase, $objPropertyFilter = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}
		if( false == valArr( $arrstrFieldNames ) ) {
			return NULL;
		}

		$arrstrAdditionalAndStatements = [ '1 = 1 ' ];

		if( true == isset ( $objPropertyFilter ) ) {
			if( false == is_null( $objPropertyFilter->getPropertyName() ) && 0 < strlen( $objPropertyFilter->getPropertyName() ) && 'Search Properties' != trim( $objPropertyFilter->getPropertyName() ) ) {
				$arrstrAdditionalAndStatements[] = ' property_name ILIKE E\'%' . $objPropertyFilter->sqlPropertyName() . '%\'';
			}

			if( 1 == $objPropertyFilter->getHideDisabled() ) {
				$arrstrAdditionalAndStatements[] = ' is_disabled <> 1 ';
			}

			if( 1 == $objPropertyFilter->getParentOnly() ) {
				$arrstrAdditionalAndStatements[] = ' property_id IS NULL ';
			}

			if( true == is_numeric( $objPropertyFilter->getExludePropertyId() ) ) {
				$arrstrAdditionalAndStatements[] = ' id <> ' . ( int ) $objPropertyFilter->getExludePropertyId();
			}
		}

		$strSql = 'SELECT ' . implode( ',', $arrstrFieldNames ) . '
					FROM
						properties
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ' . implode( ' AND ', $arrstrAdditionalAndStatements ) . '
						AND property_type_id NOT IN ( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
					ORDER BY
						lower ( property_name );';

		$arrstrProperties = fetchData( $strSql, $objDatabase );

		$arrstrReorderedProperties = [];

		if( true == valArr( $arrstrProperties ) ) {
			foreach( $arrstrProperties as $arrstrProperty ) {
				$arrstrReorderedProperties[$arrstrProperty['id']] = $arrstrProperty;
			}
		}

		return $arrstrReorderedProperties;
	}

	public static function fetchSimplePropertiesByPropertyGroupIdsByFieldNamesByCid( $arrintPropertyGroupIds, $arrstrFieldNames, $intCid, $objDatabase, $intShowDisabledProperties = 0, $objPropertyFilter = NULL, $arrintPsProductIds = NULL, $strSortOrder = NULL ) {
		if( false == valArr( $arrintPropertyGroupIds ) ) {
			return NULL;
		}
		if( false == valArr( $arrstrFieldNames ) ) {
			return NULL;
		}

		$arrstrAdditionalAndStatements = [ '1 = 1 ' ];

		if( true == isset ( $objPropertyFilter ) ) {
			if( false == is_null( $objPropertyFilter->getPropertyName() ) && 0 < strlen( $objPropertyFilter->getPropertyName() ) && 'Search Properties' != trim( $objPropertyFilter->getPropertyName() ) ) {
				$arrstrAdditionalAndStatements[] = ' p.property_name ILIKE E\'%' . $objPropertyFilter->sqlPropertyName() . '%\'';
			}

			if( 1 == $objPropertyFilter->getHideDisabled() ) {
				$arrstrAdditionalAndStatements[] = ' p.is_disabled <> 1 ';
			}

			if( 1 == $objPropertyFilter->getParentOnly() ) {
				$arrstrAdditionalAndStatements[] = ' p.property_id IS NULL ';
			}

			if( true == is_numeric( $objPropertyFilter->getExludePropertyId() ) ) {
				$arrstrAdditionalAndStatements[] = ' p.id <> ' . ( int ) $objPropertyFilter->getExludePropertyId();
			}
		}

		if( true == valArr( $arrintPsProductIds ) ) {
			$strSubSql = ' JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $arrintPropertyGroupIds ) . ' ], ARRAY[ ' . implode( ',', $arrintPsProductIds ) . ' ] ) lp ON ( ' . ( ( 1 == $intShowDisabledProperties ) ? '' : 'lp.is_disabled = ' . ( int ) $intShowDisabledProperties . ' AND ' ) . 'lp.property_id = p.id )';
		} else {
			$strSubSql = ' JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $arrintPropertyGroupIds ) . ' ] ) lp ON ( ' . ( ( 1 == $intShowDisabledProperties ) ? '' : 'lp.is_disabled = ' . ( int ) $intShowDisabledProperties . ' AND ' ) . 'lp.property_id = p.id )';
		}

		$strSql = 'SELECT ' . implode( ',', $arrstrFieldNames ) . '
					FROM
						properties p' .
		          $strSubSql . '
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND ' . implode( ' AND ', $arrstrAdditionalAndStatements ) . '
					ORDER BY
						lower ( property_name )';

		if( true == isset( $strSortOrder ) ) {
			$strSql .= ' ' . $strSortOrder . ';';
		}

		$arrstrProperties = fetchData( $strSql, $objDatabase );

		$arrstrReorderedProperties = [];

		if( true == valArr( $arrstrProperties ) ) {
			foreach( $arrstrProperties as $arrstrProperty ) {
				$arrstrReorderedProperties[$arrstrProperty['id']] = $arrstrProperty;
			}
		}

		return $arrstrReorderedProperties;
	}

	public static function fetchPropertyIdsByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objDatabase, $arrintPsProductIds = NULL, $boolIsFetchDisabledProperties = true ) {
		if( false == valArr( $arrintPropertyGroupIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $arrintPropertyGroupIds ) . ' ] ';

		if( true == valArr( $arrintPsProductIds ) ) {
			$strSql .= ' , ARRAY[ ' . implode( ',', $arrintPsProductIds ) . ' ] ';
		}

		$strSql .= ' )';

		if( false == $boolIsFetchDisabledProperties ) {
			$strSql .= ' WHERE is_disabled = 0 ';
		}

		$arrmixPropertyIds = fetchData( $strSql, $objDatabase );

		$arrintPropertyIds = ( true == valArr( $arrmixPropertyIds ) ) ? rekeyArray( 'property_id', $arrmixPropertyIds ) : [];

		return array_keys( $arrintPropertyIds );
	}

	public static function fetchEntrataEnabledPropertyIdsByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyGroupIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						lp.property_id
					FROM
						load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $arrintPropertyGroupIds ) . ' ] ) as lp
						JOIN property_gl_settings AS pgs ON ( lp.cid = pgs.cid and lp.property_id = pgs.property_id and lp.is_disabled = 0 AND pgs.activate_standard_posting = true )
					WHERE
						lp.cid = ' . ( int ) $intCid . ';';

		$arrmixPropertyIds = fetchData( $strSql, $objDatabase );

		$arrintPropertyIds = ( true == valArr( $arrmixPropertyIds ) ) ? rekeyArray( 'property_id', $arrmixPropertyIds ) : [];

		return array_keys( $arrintPropertyIds );
	}

	public static function fetchProspectPortalPropertiesWithCityWithWebsiteDomainByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						p.*,
						( CASE
								WHEN ( wd.website_domain IS NOT NULL )
									THEN ( \'http://\' || website_domain )
								ELSE ( \'http://\' || sub_domain || \'' . CConfig::get( 'prospect_portal_suffix' ) . '\' )
						END ) AS website_domain,
						pa.city
					FROM
						properties p
						JOIN property_addresses AS pa ON ( pa.cid = p.cid AND p.id = pa.property_id AND pa.address_type_id = ' . CAddressType::PRIMARY . ' AND pa.cid = ' . ( int ) $intCid . ' AND pa.is_alternate = false )
						JOIN website_properties wp ON ( wp.cid = p.cid AND p.id = wp.property_id AND hide_on_prospect_portal = 0 )
						JOIN websites w ON ( w.cid = wp.cid AND w.id = wp.website_id AND deleted_on IS NULL AND w.is_disabled = 0 )
						JOIN website_templates wt ON ( w.website_template_id = wt.id AND wt.is_singular = 1 )
						LEFT JOIN website_domains wd ON ( wd.cid = w.cid AND wd.website_id = w.id AND wd.is_primary = 1 )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.property_id IS NULL
						AND p.is_disabled = 0';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchSimplePropertiesByCompanyUserIdWithWebsitePermissionsByCid( $intCompanyUserId, $intCid, $objDatabase, $boolIsAdministrator = false ) {
		if( false == $boolIsAdministrator ) {
			$strSql = 'SELECT
							DISTINCT ON ( p.id )
							p.id,
							p.property_name
						FROM
							properties p
							JOIN view_company_user_properties cup ON ( cup.cid = p.cid AND cup.property_id = p.id )
							JOIN company_user_websites cuw ON ( cuw.cid = cup.cid AND cuw.company_user_id = cup.company_user_id )
							JOIN website_properties wp ON ( wp.cid = cup.cid AND wp.property_id = cup.property_id )
						WHERE
							p.cid = ' . ( int ) $intCid . '
							AND cup.company_user_id = ' . ( int ) $intCompanyUserId . '
							AND p.is_disabled = 0';
		} else {
			$strSql = 'SELECT
							p.id,
							p.property_name
						FROM
							properties p
						WHERE
							p.cid = ' . ( int ) $intCid . '
							AND p.is_disabled = 0
						ORDER BY
							p.property_name ASC';
		}

		$arrmixProperties = ( array ) fetchData( $strSql, $objDatabase );

		$arrstrProperties = [];

		foreach( $arrmixProperties as $arrmixProperty ) {
			$arrstrProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
		}

		return $arrstrProperties;
	}

	public static function fetchSimplePropertiesByPsProductIdsByCid( $arrintPsProductIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPsProductIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT( p.id ),
						p.id,
						p.property_name
					FROM
						properties p
						JOIN property_products AS pp ON ( pp.property_id = p.id OR pp.property_id IS NULL )
					WHERE
						pp.cid = p.cid
						AND pp.cid = ' . ( int ) $intCid . '
						AND pp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
						AND p.is_disabled = 0
					ORDER BY
						p.property_name';

		$arrmixProperties = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixProperties ) ) {
			return NULL;
		}

		foreach( $arrmixProperties as $arrstrProperty ) {
			$arrstrProperties[$arrstrProperty['id']] = $arrstrProperty;
		}

		return $arrstrProperties;
	}

	public static function fetchPropertiesByIdsByPsProductIdsByCid( $arrintPropertyIds, $arrintPsProductIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPsProductIds ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT( p.id ),
						p.id,
						p.property_name,
						p.details->>\'email_relay_address\' as email_relay_address
					FROM
						properties p
						JOIN property_products AS pp ON ( pp.property_id = p.id OR pp.property_id IS NULL )
					WHERE
						pp.cid = p.cid
						AND pp.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
						AND p.is_disabled = 0
					ORDER BY
						p.property_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPermissionedPropertiesByIdsByPsProductIdByCompanyUserIdByCid( $arrintPropertyIds, $intPsProductId, $intEmailOwnerId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSubSql = self::fetchPropertyIdsByCidByCompanyUserIdBySessionData( $intCid, $intEmailOwnerId, NULL, $objDatabase, $arrintPropertyIds, true );

		$strSql = 'SELECT
						DISTINCT( p.id ),
						p.id
					FROM
						properties p
						JOIN property_products AS pp ON ( pp.cid = p.cid AND ( pp.property_id = p.id OR pp.property_id IS NULL ) )
					WHERE
						pp.cid = ' . ( int ) $intCid . '
						AND pp.ps_product_id = ' . ( int ) $intPsProductId . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.id IN ( ' . $strSubSql . ' )
						AND p.is_disabled = 0';

		$arrmixResponses = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixResponses ) ) {
			return NULL;
		}

		$arrintEnabaledPropertyIds = [];

		foreach( $arrmixResponses as $arrmixResponse ) {
			$arrintEnabaledPropertyIds[$arrmixResponse['id']] = $arrmixResponse['id'];
		}

		return $arrintEnabaledPropertyIds;
	}

	public static function fetchCustomEntrataMationProperties( $objDatabase, $arrintCids = NULL ) : array {

		$strCondition = ( true == valArr( $arrintCids ) ) ? ' AND p.cid IN ( ' . implode( ',', $arrintCids ) . ') ' : '';

		$strSql = 'SELECT
					p.*
					FROM
						properties p
						JOIN property_products pp ON ( pp.cid = p.cid AND pp.ps_product_id = ' . CPsProduct::ENTRATAMATION . ' AND ( pp.property_id = p.id OR pp.property_id IS NULL ) )
						JOIN property_units AS pu ON ( p.id = pu.property_id and p.cid = pu.cid )
					WHERE
						pp.cid = p.cid
						AND pu.details->>\'is_smart_ready\' = \'1\'
						AND p.is_disabled = 0 ' . $strCondition . '
					GROUP BY
						p.cid,
						p.id';

		return ( array ) self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByPsProductIdsByCid( $arrintPsProductIds, $intCid, $objDatabase, $intShowDisabledProperties = 0 ) {
		if( false == valArr( $arrintPsProductIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT( p.id ),
						p.*
					FROM
						properties p
						JOIN property_products AS pp ON ( pp.cid = p.cid AND pp.property_id = p.id OR pp.property_id IS NULL )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND pp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
						' . ( ( 1 == $intShowDisabledProperties ) ? '' : ' AND p.is_disabled = ' . ( int ) $intShowDisabledProperties . ' ' ) . '
					ORDER BY
						p.property_name';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchAllPmEnabledPropertiesForAdvanceByPostMonthByPropertyIdsCid( $strPostMonth, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valStr( $strPostMonth ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.lookup_code,
						pgt.id AS property_group_type_id,
						util_get_translated( \'name\', pgt.name, pgt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS property_group_type_name,
						pg.id AS property_group_id,
						pg.name AS property_group_name
					FROM
						properties AS p
						JOIN property_gl_settings AS pgs ON ( pgs.cid = p.cid AND p.id = pgs.property_id )
						LEFT JOIN property_group_associations AS pga ON ( pga.cid = p.cid AND p.id = pga.property_id )
						LEFT JOIN property_groups AS pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						LEFT JOIN property_group_types AS pgt ON ( pgt.cid = pg.cid AND pg.property_group_type_id = pgt.id AND pgt.is_published = 1 AND ( pgt.system_code != \'' . CPropertyGroupType::PROPERTY_LIST_SYSTEM_CODE . '\' OR pgt.system_code IS NULL ) AND pgt.deleted_by IS NULL AND pgt.deleted_on IS NULL )
					WHERE
						pgs.activate_standard_posting = true
						AND p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' . '
						AND ( pgs.ar_post_month = \'' . $strPostMonth . '\'
								OR pgs.ap_post_month = \'' . $strPostMonth . '\'
								OR pgs.gl_post_month = \'' . $strPostMonth . '\' )
					ORDER BY
						p.property_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllPmEnabledPropertiesForRollbackByPropertyIdsByPeriodIdsByPeriodClosingsFilterCid( $arrintPropertyIds, $arrintPeriodIds, $objPeriodClosingsFilter, $intCid, $objDatabase ) {
		if( false == valObj( $objPeriodClosingsFilter, 'CPeriodClosingsFilter' )
		    || false == valArr( $arrintPropertyIds )
		    || false == valArr( $arrintPeriodIds )
		    || false == valStr( $objPeriodClosingsFilter->getPostMonth() )
		    || ( 0 == $objPeriodClosingsFilter->getAccountsReceivable() && 0 == $objPeriodClosingsFilter->getAccountsPayable() && 0 == $objPeriodClosingsFilter->getGeneralLedger() ) ) {
			return NULL;
		}

		$strCondition = NULL;

		if( 1 == $objPeriodClosingsFilter->getAccountsReceivable() ) {
			$strCondition = $strCondition . ' AND pgs.ar_post_month = \'' . $objPeriodClosingsFilter->getPostMonth() . '\'';
		}

		if( 1 == $objPeriodClosingsFilter->getAccountsPayable() ) {
			$strCondition = $strCondition . ' AND pgs.ap_post_month = \'' . $objPeriodClosingsFilter->getPostMonth() . '\'';
		}

		if( 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {
			$strCondition = $strCondition . ' AND pgs.gl_post_month = \'' . $objPeriodClosingsFilter->getPostMonth() . '\'';
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.lookup_code,
						pgt.id AS property_group_type_id,
						util_get_translated( \'name\', pgt.name, pgt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS property_group_type_name,
						pg.id AS property_group_id,
						pg.name AS property_group_name
					FROM
						properties AS p
						JOIN property_gl_settings AS pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id )
						JOIN periods pe ON ( p.cid = pe.cid AND p.id = pe.property_id AND pe.id IN ( ' . implode( ',', $arrintPeriodIds ) . ' ) )
						LEFT JOIN property_group_associations AS pga ON ( p.id = pga.property_id AND p.cid = pga.cid )
						LEFT JOIN property_groups AS pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						LEFT JOIN property_group_types AS pgt ON ( pgt.cid = pg.cid AND pg.property_group_type_id = pgt.id AND pgt.is_published = 1 AND ( pgt.system_code != \'' . CPropertyGroupType::PROPERTY_LIST_SYSTEM_CODE . '\' OR pgt.system_code IS NULL ) AND pgt.deleted_by IS NULL AND pgt.deleted_on IS NULL )
					WHERE
						pgs.activate_standard_posting = true
						AND p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' .
		          $strCondition . '
					ORDER BY
						p.property_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertiesForLockPeriodByPostMonthByPropertyIdsByCid( $strPostMonth, $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.lookup_code,
						pgt.id AS property_group_type_id,
						util_get_translated( \'name\', pgt.name, pgt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS property_group_type_name,
						pg.id AS property_group_id,
						pg.name AS property_group_name
					FROM
						periods pd
						JOIN properties p ON ( p.cid = pd.cid AND p.id = pd.property_id )
						JOIN property_gl_settings pgs ON ( pgs.cid = pd.cid AND pgs.property_id = pd.property_id )
						LEFT JOIN property_group_associations AS pga ON ( pga.cid = p.cid AND p.id = pga.property_id )
						LEFT JOIN property_groups AS pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						LEFT JOIN property_group_types AS pgt ON ( pgt.cid = pg.cid AND pg.property_group_type_id = pgt.id AND pgt.is_published = 1 AND ( pgt.system_code != \'' . CPropertyGroupType::PROPERTY_LIST_SYSTEM_CODE . '\' OR pgt.system_code IS NULL ) AND pgt.deleted_by IS NULL AND pgt.deleted_on IS NULL )
					WHERE
						pd.cid = ' . ( int ) $intCid . '
						AND DATE_TRUNC( \'month\', pd.post_month ) = \'' . date( 'm-d-Y', strtotime( $strPostMonth ) ) . '\'
						AND pd.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ( ( pd.post_month < pgs.ar_post_month AND ar_locked_on IS NULL)
								OR
								( pd.post_month < pgs.ap_post_month AND ap_locked_on IS NULL)
								OR
								( pd.post_month < pgs.gl_post_month AND gl_locked_on IS NULL) )
					ORDER BY
						p.property_name';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchAllPmEnabledPropertiesForUnlockByPropertyIdsByPeriodIdsByPeriodClosingsFilterCid( $arrintPropertyIds, $arrintPeriodIds, $objPeriodClosingsFilter, $intCid, $objDatabase ) {
		if( false == valObj( $objPeriodClosingsFilter, 'CPeriodClosingsFilter' )
		    || false == valArr( $arrintPropertyIds )
		    || false == valArr( $arrintPeriodIds )
		    || false == valStr( $objPeriodClosingsFilter->getPostMonth() )
		    || ( 0 == $objPeriodClosingsFilter->getAccountsReceivable() && 0 == $objPeriodClosingsFilter->getAccountsPayable() && 0 == $objPeriodClosingsFilter->getGeneralLedger() ) ) {
			return NULL;
		}

		$strCondition = NULL;

		if( 1 == $objPeriodClosingsFilter->getAccountsReceivable() ) {
			$strCondition = $strCondition . ' AND pgs.ar_lock_month = \'' . $objPeriodClosingsFilter->getPostMonth() . '\'';
		}

		if( 1 == $objPeriodClosingsFilter->getAccountsPayable() ) {
			$strCondition = $strCondition . ' AND pgs.ap_lock_month = \'' . $objPeriodClosingsFilter->getPostMonth() . '\'';
		}

		if( 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {
			$strCondition = $strCondition . ' AND pgs.gl_lock_month = \'' . $objPeriodClosingsFilter->getPostMonth() . '\'';
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.lookup_code,
						pgt.id AS property_group_type_id,
						util_get_translated( \'name\', pgt.name, pgt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS property_group_type_name,
						pg.id AS property_group_id,
						pg.name AS property_group_name
					FROM
						properties AS p
						JOIN property_gl_settings AS pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id )
						JOIN periods pe ON ( p.cid = pe.cid AND p.id = pe.property_id AND pe.id IN ( ' . implode( ',', $arrintPeriodIds ) . ' ) )
						LEFT JOIN property_group_associations AS pga ON ( pga.cid = p.cid AND p.id = pga.property_id )
						LEFT JOIN property_groups AS pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						LEFT JOIN property_group_types AS pgt ON ( pgt.cid = pg.cid AND pg.property_group_type_id = pgt.id AND pgt.is_published = 1 AND ( pgt.system_code != \'' . CPropertyGroupType::PROPERTY_LIST_SYSTEM_CODE . '\' OR pgt.system_code IS NULL ) AND pgt.deleted_by IS NULL AND pgt.deleted_on IS NULL )
					WHERE
						pgs.activate_standard_posting = true
						AND p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' .
		          $strCondition . '
					ORDER BY
						p.property_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimplePropertiesInfoByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.lookup_code,
						p.remote_primary_key,
						ppn.phone_number AS office_phone_number,
						tz.time_zone_name,
						tz.abbr AS time_zone_abbreviation,
						tz.gmt_offset AS time_zone_gmt_offset
					FROM
						properties AS p
						LEFT JOIN property_phone_numbers AS ppn ON ( ppn.cid = p.cid AND ppn.property_id = p.id AND ppn.phone_number_type_id = ' . ( int ) CPhoneNumberType::OFFICE . ' )
						LEFT JOIN time_zones AS tz ON ( tz.id = p.time_zone_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
					ORDER BY
						property_name';

		$arrmixProperties = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixProperties ) ) {
			return NULL;
		}

		foreach( $arrmixProperties as $arrstrProperty ) {
			$arrstrProperties[$arrstrProperty['id']] = $arrstrProperty;
		}

		return $arrstrProperties;
	}

	public static function fetchCustomPropertyNamesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						id,
						property_name
					FROM
						properties
					WHERE
						id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
					ORDER BY
						lower ( property_name ) ASC';

		$arrmixProperties = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixProperties ) ) {
			return NULL;
		}

		foreach( $arrmixProperties as $arrmixProperty ) {
			$arrstrProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
		}

		return $arrstrProperties;
	}

	public static function fetchAllPropertiesByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase, $boolShowEnabled = NULL, $boolIsFeatured = NULL, $boolIsAdministrator = false, $boolIsExcludeTemplateProperties = false ) {
		$strSelect = '';
		$strJoin   = '';

		if( false != $boolIsFeatured ) {
			$strSelect = ', pd.is_featured';
			$strJoin   = ' LEFT JOIN property_details pd ON ( gcup.id = pd.property_id AND gcup.cid = pd.cid )';
		}

		$strSql = self::buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator, $boolShowEnabled, false, $boolIsExcludeTemplateProperties );

		$strSql = 'SELECT
						gcup.* ' . $strSelect . '
					FROM
						( ' . $strSql . ' ) as gcup ' .
		          $strJoin . '
					WHERE
						gcup.cid = ' . ( int ) $intCid . '
					ORDER BY
						LOWER( gcup.property_name ) ASC,
						gcup.order_num';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPmEnabledPropertyIdsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == isset( $intCid ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id
					 FROM
						properties p,
						property_gl_settings pgs
					WHERE
						( p.id = pgs.property_id AND p.cid = pgs.cid )
						AND p.cid = ' . ( int ) $intCid . '
						AND pgs.activate_standard_posting = true
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					ORDER BY
						p.property_name ASC';

		$arrmixPropertyData         = ( array ) fetchData( $strSql, $objDatabase );
		$arrintPmEnabledPropertyIds = [];

		foreach( $arrmixPropertyData as $arrintPropertyIds ) {
			$arrintPmEnabledPropertyIds[$arrintPropertyIds['id']] = $arrintPropertyIds['id'];
		}

		return $arrintPmEnabledPropertyIds;
	}

    public static function fetchPmEnabledOrEntrataCorePropertyIdsByCid( $intCid, $objDatabase, $intPropertyId = NULL ) {
		$strWhere = '';
        if( false == isset( $intCid ) ) {
            return NULL;
        }

        if( true == valId( $intPropertyId ) ) {
        	$strWhere = ' AND p.id = ' . ( int ) $intPropertyId;
        }

      $strSql = 'SELECT
						DISTINCT p.id
				    FROM
						properties p
						JOIN property_gl_settings pgs ON ( p.id = pgs.property_id AND p.cid = pgs.cid )
						LEFT JOIN property_products pp ON ( pp.cid = p.cid AND pp.property_id= p.id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						' . $strWhere . '
						AND ( pgs.activate_standard_posting = true
						OR pp.ps_product_id IN ( ' . CPsProduct::ENTRATA . ' ) )';

        $arrmixPropertyData         = ( array ) fetchData( $strSql, $objDatabase );
        $arrintPmEnabledPropertyIds = [];

        foreach( $arrmixPropertyData as $arrintPropertyIds ) {
            $arrintPmEnabledPropertyIds[$arrintPropertyIds['id']] = $arrintPropertyIds['id'];
        }

        return $arrintPmEnabledPropertyIds;
    }

    public static function fetchPmEnabledPropertiesByWebsiteIdByCid( $intWebsiteId, $intCid, $objDatabase, $arrintPropertyIds = NULL ) {
		if( false == isset( $intWebsiteId ) || false == isset( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ( p.* )
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id )
						JOIN property_products pp ON ( pp.cid = p.cid AND p.id = pp.property_id )
						JOIN website_properties wp ON ( wp.cid = p.cid AND wp.property_id = p.id AND wp.hide_on_prospect_portal = 0 )
					WHERE
						wp.website_id = ' . ( int ) $intWebsiteId . '
						AND ( pp.ps_product_id = ' . CPsProduct::ENTRATA . ' OR pp.id iS NULL )
						AND pgs.activate_standard_posting = true
						AND p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0 ' .
		          ( ( true == valArr( $arrintPropertyIds ) ) ? ( ' AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' ) : NULL ) . '
					ORDER BY
						p.property_name ASC';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCustomPaginatedPropertyDetailsByPropertyIdsByCid( $intPageNo, $intPageSize, $arrintPropertyIds, $intCid, $objDatabase, $strSearchVal = NULL, $boolFetchObject = true, $boolAllowDisabled = false ) {
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strWhereCondition = '';
		if( true === $boolAllowDisabled ) {
			$strWhereCondition = ' AND ( p.is_disabled = 0 OR p.is_managerial = 1 ) ';
		}
		$strSql = 'SELECT	p.id,
							p.property_name,
							p.number_of_units,
							p.termination_date,
							p.is_disabled,
							p.is_managerial,
							pa.street_line1,
							pa.street_line2,
							pa.city,
							pa.state_code,
							p.country_code,
							pa.postal_code,
							p.property_type_id,
							p.occupancy_type_ids,
							CASE WHEN p.property_type_id = ' . CPropertyType::CORPORATE . ' THEN 1 ELSE 0 END AS is_corporate_entity
					FROM properties p JOIN
						 property_addresses pa ON ( p.cid = pa.cid AND p.id = pa.property_id AND pa.address_type_id = ' . CAddressType::PRIMARY . ' AND pa.is_alternate = false )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						' . $strWhereCondition . '
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';

		if( false == is_null( $strSearchVal ) ) {
			$strSql .= ' AND p.property_name ILIKE \'%' . $strSearchVal . '%\'';
		}

		$strSql .= ' ORDER BY
						p.property_name ';

		if( 0 != $intLimit || 0 != $intOffset ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		if( false == $boolFetchObject ) {
			return fetchData( $strSql, $objDatabase );
		}

		return self::fetchProperties( $strSql, $objDatabase );

	}

	public static function fetchPropertiesByTransmissionVendorIdsByPsProductIdsByCid( $arrintTransmissionVendorIds, $arrintPsProductIds, $intCid, $objDatabase, $boolAllPropertyDetails = true, $boolIsActiveProperty = false, $boolOnlyPropertyId = false ) {
		if( false == valArr( $arrintTransmissionVendorIds ) || false == valArr( $arrintPsProductIds ) ) {
			return NULL;
		}

		$strSelectClause = ' p.* ';

		if( false == $boolAllPropertyDetails ) {
			$strSelectClause = ( true == $boolOnlyPropertyId ) ? ' p.id ' : ' p.id, p.property_name, p.lookup_code, p.cid ';
		}

		$strWhereClause = ( true == $boolIsActiveProperty ) ? ' AND p.is_disabled <> 1' : '';

		$strSql = 'SELECT
						DISTINCT ON ( p.id )' .
		          $strSelectClause . '
					FROM
						properties AS p
						JOIN property_transmission_vendors AS ptv ON ( ptv.cid = p.cid AND ptv.property_id = p.id )
						JOIN company_transmission_vendors AS ctv ON ( ctv.cid = ptv.cid AND ctv.id = ptv.company_transmission_vendor_id )
						JOIN property_products AS pp ON ( pp.cid = p.cid AND pp.ps_product_id = ' . implode( ',', $arrintPsProductIds ) . ' )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND ctv.transmission_vendor_id IN ( ' . implode( ',', $arrintTransmissionVendorIds ) . ' )
						AND ( pp.property_id = p.id OR pp.property_id IS NULL )'
		          . $strWhereClause;

		if( true == $boolOnlyPropertyId ) {
			return fetchData( $strSql, $objDatabase );
		}

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchAllowedProspectPortalPropertiesByWebsiteIdByUserGivenPrecedencyByCid( $intWebsiteId, $objPropertySearch, $strPsProduct, &$arrobjProperties, $intCid, $objDatabase, $intBroadStoneLiving = NULL ) {
		$strSql = self::buildCorePortalSearchUserGivenPrecedencySqlByCid( $intWebsiteId, $objPropertySearch, $strPsProduct, $intCid, true, $objDatabase, $intBroadStoneLiving );

		$arrobjConstructedProperties = self::fetchProperties( $strSql, $objDatabase );

		$arrobjConstructedProperties = rekeyObjects( 'propertyname', $arrobjConstructedProperties );

		$strUnderConstructionSql = self::buildCorePortalSearchUserGivenPrecedencySqlByCid( $intWebsiteId, $objPropertySearch, $strPsProduct, $intCid, false, $objDatabase, $intBroadStoneLiving );

		$arrobjUnderConstructionProperties = self::fetchProperties( $strUnderConstructionSql, $objDatabase );

		$arrobjUnderConstructionProperties = rekeyObjects( 'propertyname', $arrobjUnderConstructionProperties );

		if( ( true == valArr( $arrobjConstructedProperties ) ) && ( true == valArr( $arrobjUnderConstructionProperties ) ) ) {
			$arrobjAllProperties = array_merge( $arrobjConstructedProperties, $arrobjUnderConstructionProperties );
		} else {
			if( true == valArr( $arrobjConstructedProperties ) && false == valArr( $arrobjUnderConstructionProperties ) ) {
				$arrobjAllProperties = rekeyObjects( 'propertyname', $arrobjConstructedProperties );
			} else {
				if( true == valArr( $arrobjUnderConstructionProperties ) && false == valArr( $arrobjConstructedProperties ) ) {
					$arrobjAllProperties = rekeyObjects( 'propertyname', $arrobjUnderConstructionProperties );
				} else {
					$arrobjAllProperties = self::fetchProperties( $strSql, $objDatabase );
				}
			}
		}

		if( false == is_null( $arrobjAllProperties ) ) {
			uksort( $arrobjAllProperties, 'strcasecmp' );
			foreach( $arrobjAllProperties as $arrobjUnderConstruction ) {
				$arrobjProperties[$arrobjUnderConstruction->getId()] = $arrobjUnderConstruction;
			}
		}

		if( false == is_null( $arrobjProperties ) ) {
			return \Psi\Libraries\UtilFunctions\count( $arrobjProperties );        // str_replace( $strSql ) || preg_replace SELECT * with count( id )
		} else {
			return 0;
		}
	}

	public static function buildCorePortalSearchUserGivenPrecedencySqlByCid( $intWebsiteId, $objPropertySearch, $strPsProduct, $intCid, $boolHasRent = true, $objDatabase, $intBroadStoneLiving = NULL ) {
		$intAllowedMaxScore               = 11;
		$strCaseParameters                = '';
		$strSearchScore                   = '';
		$strPropertyPreferencesParameters = '';
		$arrstrWhereParameters            = [];

		$intUserPrecedency        = 0;
		$strOrderByWhatsImportant = '';
		$strInnerJoin             = '';
		$strWhereCondition        = '';

		if( true == isset( $_REQUEST['property_search']['what_is_important'] ) ) {
			$intUserPrecedency = $_REQUEST['property_search']['what_is_important'];
		}

		switch( $intUserPrecedency ) {
			case 1:
				// COST
				$strOrderByWhatsImportant = 'rent_score';
				break;

			case 2:
				// LOCATION
				if( true == isset( $_REQUEST['property_search']['city_state_zip'] ) && false == is_null( $_REQUEST['property_search']['city_state_zip'] ) ) {
					if( true == is_numeric( $objPropertySearch->getMilesRadius() ) && false == is_null( $objPropertySearch->getPostalCode() ) && 0 != strlen( trim( $objPropertySearch->getPostalCode() ) ) ) {
						$strOrderByWhatsImportant = 'miles_score';
					}
				}
				break;

			case 3:
				// COMMUNITY ADVANTAGES
				if( true == isset( $_REQUEST['property_search']['community_default_amenity_ids'] ) && false == is_null( $_REQUEST['property_search']['community_default_amenity_ids'] ) ) {
					$strOrderByWhatsImportant = 'general_amenity_score';
				}
				break;

			case 4:
				// HOME ADVANTAGES
				if( true == isset( $_REQUEST['property_search']['apartment_default_amenity_ids'] ) && false == is_null( $_REQUEST['property_search']['apartment_default_amenity_ids'] ) ) {
					$strOrderByWhatsImportant = 'specific_amenity_score';
				}
				break;

			default:
				$strOrderByWhatsImportant = '';
		}

		if( true == isset ( $objPropertySearch ) ) {

			$strPropertyNameToSearch = \Psi\CStringService::singleton()->preg_replace( '/[^a-zA-Z0-9]/', '_', html_entity_decode( $objPropertySearch->getPropertyName() ) );
			$objPropertySearch->setPropertyName( $strPropertyNameToSearch );
			if( false == is_null( $objPropertySearch->getPropertyName() ) && 0 != strlen( trim( $objPropertySearch->getPropertyName() ) ) ) {

				$strCaseParameters .= ', CASE WHEN p.property_name ILIKE \'%' . addslashes( trim( $objPropertySearch->getPropertyName() ) ) . '%\' THEN 0';

				$strCaseParameters .= ' END AS property_name_score';
				$strSearchScore    .= ' + property_name_score::numeric';
			}

			// Number of bedroom
			if( false == is_null( $objPropertySearch->getNumberOfBedrooms() ) && true == is_numeric( $objPropertySearch->getNumberOfBedrooms() ) ) {

				$strCaseParameters .= ', CASE WHEN p.max_bedrooms < ' . ( int ) $objPropertySearch->getNumberOfBedrooms() . ' THEN ( ' . ( int ) $objPropertySearch->getNumberOfBedrooms() . ' - p.max_bedrooms )
											 WHEN p.min_bedrooms > ' . ( int ) $objPropertySearch->getNumberOfBedrooms() . ' THEN .25
											 ELSE 0
										END AS bedrooms_score';

				array_push( $arrstrWhereParameters, 'p.min_bedrooms IS NOT NULL' );
				array_push( $arrstrWhereParameters, 'p.max_bedrooms IS NOT NULL' );
				$strSearchScore .= ' + bedrooms_score::numeric';
			}

			// Rent
			if( true == $boolHasRent ) {
				if( ( 0 <= $objPropertySearch->getMinRent() && true == is_numeric( $objPropertySearch->getMinRent() ) ) || ( 0 <= $objPropertySearch->getMaxRent() && true == is_numeric( $objPropertySearch->getMaxRent() ) ) ) {

					$strCaseParameters .= ', CASE ';

					$strMaxRentWhen = ' WHEN p.min_rent > ' . ( float ) $objPropertySearch->getMaxRent() . ' THEN ( ( p.min_rent / ' . ( float ) $objPropertySearch->getMaxRent() . '::numeric ) - 1 ) / .1 ';
					$strMinRentWhen = ' WHEN p.max_rent < ' . ( float ) $objPropertySearch->getMinRent() . ' THEN .5 ';

					if( ( 0 == $objPropertySearch->getMinRent() || false == is_numeric( $objPropertySearch->getMinRent() ) ) && ( 0 != $objPropertySearch->getMaxRent() && true == is_numeric( $objPropertySearch->getMaxRent() ) ) ) {
						$strCaseParameters .= $strMaxRentWhen;
					} else {
						if( ( 0 != $objPropertySearch->getMinRent() && true == is_numeric( $objPropertySearch->getMinRent() ) ) && ( 0 == $objPropertySearch->getMaxRent() || false == is_numeric( $objPropertySearch->getMaxRent() ) ) ) {
							$strCaseParameters .= $strMinRentWhen;
						} else {
							$strCaseParameters .= $strMaxRentWhen . $strMinRentWhen;
						}
					}

					if( 0 == $objPropertySearch->getMinRent() && 0 == $objPropertySearch->getMaxRent() ) {
						array_push( $arrstrWhereParameters, ' ( ( p.min_rent = 0 OR p.min_rent IS NULL ) AND ( p.max_rent = 0 OR p.max_rent IS NULL ) ) ' );
					}

					$strCaseParameters .= ' ELSE 0 END AS rent_score';
					if( 0 != $objPropertySearch->getMinRent() && true == is_numeric( $objPropertySearch->getMinRent() ) ) {
						array_push( $arrstrWhereParameters, 'p.min_rent IS NOT NULL' );
					}

					$strSearchScore .= ' + rent_score::numeric';
				}
			}
			if( false == $boolHasRent ) {
				$strInnerJoin      = ' INNER JOIN property_preferences AS pp ON ( p.id = pp.property_id AND p.cid = pp.cid )';
				$strWhereCondition = ' AND pp.key = \'UNDER_CONSTRUCTION_PROPERTY\' AND p.min_rent IS NULL AND p.max_rent IS NULL';
			}

			// Broadstone Living
			if( ( true == isset( $_REQUEST['search_type'] ) && true == valStr( $_REQUEST['search_type'] ) ) || false == is_null( $intBroadStoneLiving ) ) {
				$strStateCode = '';
				if( true == is_null( $objPropertySearch->getCompanyRegionId() ) && 0 == strlen( $objPropertySearch->getCompanyRegionId() ) && true == isset( $_REQUEST['search_type']['broadstone_living'] ) ) {
					$strStateCode = 'AND pa.state_code = \'' . addslashes( $objPropertySearch->getStateCode() ) . '\'';
				}
				$strInnerJoin .= ' INNER JOIN property_group_associations AS pga on ( pga.property_id = pa.property_id ' . $strStateCode . ' AND pa.cid = pga.cid ) ';
				$strInnerJoin .= ' INNER JOIN property_groups AS pg ON ( pg.id = pga.property_group_id AND pg.name = \'Broadstone Living\' AND pga.cid = pg.cid AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )';
			}

			// General Amenities or community Ammenities
			if( false == is_null( $objPropertySearch->getCommunityDefaultAmenityIds() ) && 0 != strlen( $objPropertySearch->getCommunityDefaultAmenityIds() ) ) {
				$arrintCommunityDefaultAmenityIds = explode( ',', $objPropertySearch->getCommunityDefaultAmenityIds() );
				$strCaseParameters                .= ', ' . \Psi\Libraries\UtilFunctions\count( $arrintCommunityDefaultAmenityIds ) . ' - (
										SELECT count( id )
										FROM (
											SELECT DISTINCT( a.default_amenity_id ) AS id,
											ra.cid
											FROM rate_associations AS ra
											INNER JOIN amenities AS a
												ON ( ra.ar_origin_reference_id = a.id AND ra.cid = a.cid
													 AND ra.ar_origin_id = ' . CArOrigin::AMENITY . '
													 AND ra.ar_cascade_id = ' . CArCascade::PROPERTY . '
													 AND a.amenity_type_id = ' . CAmenityType::COMMUNITY . ' )
												AND ra.is_published = true
												AND a.cid = ' . ( int ) $intCid . '
												AND a.default_amenity_id IN ( ' . $objPropertySearch->getCommunityDefaultAmenityIds() . ' )
											WHERE ra.property_id = p.id AND ra.cid = p.cid ) AS s_amen WHERE s_amen.cid = ' . ( int ) $intCid . ' ) AS general_amenity_score';

				$strSearchScore .= ' + general_amenity_score::numeric';

			} else {
				if( false == is_null( $objPropertySearch->getDefaultAmenityIds() ) && 0 != strlen( $objPropertySearch->getDefaultAmenityIds() ) ) {
					$arrintDefaultAmenityIds = explode( ',', $objPropertySearch->getDefaultAmenityIds() );
					$strCaseParameters       .= ', ' . \Psi\Libraries\UtilFunctions\count( $arrintDefaultAmenityIds ) . ' - (
										SELECT count( id )
										FROM (
											SELECT DISTINCT( a.default_amenity_id ) AS id,
											ra.cid
											FROM rate_associations AS ra
											INNER JOIN amenities AS a
												ON ( ra.ar_origin_reference_id = a.id AND ra.cid = a.cid
													 AND ra.ar_origin_id = ' . CArOrigin::AMENITY . '
													 AND ra.ar_cascade_id = ' . CArCascade::PROPERTY . '
													 AND a.amenity_type_id = ' . CAmenityType::COMMUNITY . ' )
												AND ra.is_published = true
												AND a.cid = ' . ( int ) $intCid . '
												AND a.default_amenity_id IN ( ' . $objPropertySearch->getDefaultAmenityIds() . ' )
											WHERE ra.property_id = p.id AND ra.cid = p.cid ) AS s_amen WHERE s_amen.cid = ' . ( int ) $intCid . ' ) AS general_amenity_score';

					$strSearchScore .= ' + general_amenity_score::numeric';
				}
			}

			// Specific Amenities or Unit Ammenities
			if( false == is_null( $objPropertySearch->getApartmentDefaultAmenityIds() ) && 0 != strlen( $objPropertySearch->getApartmentDefaultAmenityIds() ) ) {
				$arrintApartmentDefaultAmenityIds = explode( ',', $objPropertySearch->getApartmentDefaultAmenityIds() );
				$strCaseParameters                .= ', ' . \Psi\Libraries\UtilFunctions\count( $arrintApartmentDefaultAmenityIds ) . ' - (
										SELECT count( id )
											FROM (
												SELECT DISTINCT( a.default_amenity_id ) AS id,
													ra.cid
												FROM rate_associations AS ra
													INNER JOIN amenities AS a
												ON ( ra.ar_origin_reference_id = a.id AND ra.cid = a.cid
														 AND ra.ar_origin_id = ' . CArOrigin::AMENITY . '
													 AND ra.ar_cascade_id = ' . CArCascade::PROPERTY . '
													 AND a.amenity_type_id = ' . CAmenityType::APARTMENT . ' )
												AND ra.is_published = true
													AND a.cid = ' . ( int ) $intCid . '
												AND a.default_amenity_id IN ( ' . $objPropertySearch->getApartmentDefaultAmenityIds() . ' )
											WHERE ra.property_id = p.id AND ra.cid = p.cid ) AS s_amen WHERE s_amen.cid = ' . ( int ) $intCid . ' ) AS specific_amenity_score';

				$strSearchScore .= ' + specific_amenity_score::numeric';

			}

			$fltLongitude = NULL;
			$fltLatitude  = NULL;

			// getLongitude, getLatitude by city
			if( false == is_null( $objPropertySearch->getStateCode() ) && 0 != strlen( $objPropertySearch->getStateCode() ) ) {

				$strStateCodeToSearch = \Psi\CStringService::singleton()->preg_replace( '/[^a-zA-Z0-9]/', '_', html_entity_decode( $objPropertySearch->getStateCode() ) );
				$objPropertySearch->setStateCode( trim( $strStateCodeToSearch ) );

				// array_push( $arrstrWhereParameters, 'pa.state_code = \'' . addslashes( $objPropertySearch->getStateCode() ) . '\'' );
			}
			if( false == is_null( $objPropertySearch->getCity() ) && 0 != strlen( trim( $objPropertySearch->getCity() ) ) ) {

				$strCityToSearch = \Psi\CStringService::singleton()->preg_replace( '/[^a-zA-Z0-9]/', '_', html_entity_decode( trim( $objPropertySearch->getCity() ) ) );
				$objPropertySearch->setCity( $strCityToSearch );

				$fltLongitude = $objPropertySearch->getLongitude();
				$fltLatitude  = $objPropertySearch->getLatitude();
			}

			// getLongitude, getLatitude by area code
			if( ( false == is_null( $objPropertySearch->getCompanyRegionId() ) && 0 != strlen( $objPropertySearch->getCompanyRegionId() ) ) || ( false == is_null( $objPropertySearch->getCompanyAreaId() ) && 0 != strlen( $objPropertySearch->getCompanyAreaId() ) ) ) {

				if( false == is_null( $objPropertySearch->getCompanyAreaId() ) && 0 != strlen( $objPropertySearch->getCompanyAreaId() ) ) {
					$intCompanyAreaId = $objPropertySearch->getCompanyAreaId();
				} else {
					$intCompanyAreaId = $objPropertySearch->getCompanyRegionId();
				}

				$objCompanyArea = \Psi\Eos\Entrata\CCompanyAreas::createService()->fetchCompanyAreaByIdByCid( $intCompanyAreaId, $intCid, $objDatabase );

				if( true == valObj( $objCompanyArea, 'CCompanyArea' ) ) {
					$fltLongitude = $objCompanyArea->getLongitude();
					$fltLatitude  = $objCompanyArea->getLatitude();
				}
			}

			// getLongitude, getLatitude by zip
			if( true == is_numeric( $objPropertySearch->getMilesRadius() ) && false == is_null( $objPropertySearch->getPostalCode() ) && 0 != strlen( trim( $objPropertySearch->getPostalCode() ) ) ) {

				$fltLongitude = $objPropertySearch->getLongitude();
				$fltLatitude  = $objPropertySearch->getLatitude();
			}

			if( ( false == is_null( $objPropertySearch->getCompanyRegionId() ) && 0 != strlen( $objPropertySearch->getCompanyRegionId() ) )
			    || ( true == is_null( $objPropertySearch->getCompanyAreaId() ) && 0 != strlen( $objPropertySearch->getCompanyAreaId() ) )
			    || ( 0 < strlen( $fltLongitude ) && 0 < strlen( $fltLatitude ) )
			    || ( false == is_null( $objPropertySearch->getPostalCode() ) && 0 != strlen( $objPropertySearch->getPostalCode() ) )
			    || ( false == is_null( $objPropertySearch->getCity() ) && 0 != strlen( $objPropertySearch->getCity() ) )
			    || ( false == is_null( $objPropertySearch->getStateCode() ) && 0 != strlen( $objPropertySearch->getStateCode() ) ) ) {
				$strCaseParameters .= ', CASE ';

				if( false == is_null( $objPropertySearch->getPostalCode() ) ) {
					$strCaseParameters .= ' WHEN \'' . addslashes( $objPropertySearch->getPostalCode() ) . '\' = pa.postal_code THEN 0 ';

				} else {
					if( false == is_null( $objPropertySearch->getCompanyAreaId() ) && 0 != strlen( $objPropertySearch->getCompanyAreaId() ) ) {
						$strCaseParameters .= 'WHEN EXISTS ( SELECT * FROM property_areas AS p_area WHERE p.id = p_area.property_id AND p.cid = p_area.cid AND p_area.company_area_id = ' . ( int ) $objPropertySearch->getCompanyAreaId() . ' AND p_area.cid = ' . ( int ) $intCid . ' )
								THEN 0 ';

					} else {
						if( ( false == is_null( $objPropertySearch->getCompanyRegionId() ) && 0 != strlen( $objPropertySearch->getCompanyRegionId() ) ) && ( true == is_null( $objPropertySearch->getCompanyAreaId() ) || 0 == strlen( $objPropertySearch->getCompanyAreaId() ) ) ) {
							$strCaseParameters .= 'WHEN pa.city ILIKE ( SELECT
																ca_child.name
															FROM
																company_areas ca_child
															WHERE ca_child.id = ' . ( int ) $objPropertySearch->getCompanyRegionId() . ' OR ca_child.company_area_id = ' . ( int ) $objPropertySearch->getCompanyRegionId() . ' AND ca_child.cid = ' . ( int ) $intCid . ' LIMIT 1 ) THEN 0 WHEN p.id IN ( SELECT
								property_id
							FROM
								property_areas
							WHERE
								cid = ' . ( int ) $intCid . '
								AND company_area_id = ' . ( int ) $objPropertySearch->getCompanyRegionId() . '
								OR company_area_id IN ( SELECT
															ca_child.id
														FROM
															company_areas ca_child
															JOIN company_areas ca_parent ON ( ca_child.company_area_id = ca_parent.id AND ca_child.cid = ca_parent.cid AND ca_parent.id = ' . ( int ) $objPropertySearch->getCompanyRegionId() . ' AND ca_parent.cid = ' . ( int ) $intCid . ' ) ) )
								THEN 0 ';
						} else {
							if( ( true == is_null( $objPropertySearch->getCompanyRegionId() ) || 0 == strlen( $objPropertySearch->getCompanyRegionId() ) ) && ( true == is_null( $objPropertySearch->getCompanyAreaId() ) || 0 == strlen( $objPropertySearch->getCompanyAreaId() ) ) && ( false == is_null( $objPropertySearch->getCity() ) && 0 != strlen( $objPropertySearch->getCity() ) ) && ( false == is_null( $objPropertySearch->getStateCode() ) && 0 != strlen( $objPropertySearch->getStateCode() ) ) ) {
								$strCaseParameters .= ' WHEN pa.city ILIKE \'' . addslashes( $objPropertySearch->getCity() ) . '\' AND pa.state_code = \'' . addslashes( $objPropertySearch->getStateCode() ) . '\'
										THEN 0';
							} else {
								if( ( true == is_null( $objPropertySearch->getCompanyRegionId() ) || 0 == strlen( $objPropertySearch->getCompanyRegionId() ) ) && ( true == is_null( $objPropertySearch->getCompanyAreaId() ) || 0 == strlen( $objPropertySearch->getCompanyAreaId() ) ) && ( true == is_null( $objPropertySearch->getCity() ) || 0 == strlen( $objPropertySearch->getCity() ) ) && ( false == is_null( $objPropertySearch->getStateCode() ) && 0 != strlen( $objPropertySearch->getStateCode() ) ) ) {

									$strCaseParameters .= ' WHEN pa.state_code = \'' . addslashes( $objPropertySearch->getStateCode() ) . '\'
												OR p.id IN ( SELECT
																property_id
															FROM
																property_areas
															WHERE company_area_id IN ( SELECT
																						id
																					FROM
																						company_areas
																					WHERE
																						state_code = \'' . addslashes( $objPropertySearch->getStateCode() ) . '\' AND cid = ' . ( int ) $intCid . '
															 )
															AND cid = ' . ( int ) $intCid . '
																					 )
															AND p.cid = ' . ( int ) $intCid . ' THEN 0';

								} else {
									if( ( true == is_null( $objPropertySearch->getCompanyRegionId() ) || 0 == strlen( $objPropertySearch->getCompanyRegionId() ) ) && ( true == is_null( $objPropertySearch->getCompanyAreaId() ) || 0 == strlen( $objPropertySearch->getCompanyAreaId() ) ) && ( false == is_null( $objPropertySearch->getCity() ) && 0 != strlen( $objPropertySearch->getCity() ) ) && ( true == is_null( $objPropertySearch->getStateCode() ) || 0 == strlen( $objPropertySearch->getStateCode() ) ) ) {
										$strCaseParameters .= ' WHEN pa.city ILIKE \'' . addslashes( $objPropertySearch->getCity() ) . '\'
										THEN 0';
									}
								}
							}
						}
					}
				}

				if( 0 < strlen( $fltLongitude ) && 0 < strlen( $fltLatitude ) ) {
					$strCaseParameters .= ' WHEN ( \'' . ( double ) $fltLatitude . '\' = pa.latitude AND pa.longitude = \'' . ( double ) $fltLongitude . '\' )
												THEN 0
											WHEN pa.latitude IS NOT NULL AND pa.longitude IS NOT NULL
												THEN
												( 3958.75 * ACos( Sin( ' . ( double ) $fltLatitude . ' / 57.2958 ) *
												 Sin( pa.latitude::Float / 57.2958 ) +
												 Cos( ' . ( double ) $fltLatitude . ' / 57.2958 ) *
												 Cos( pa.latitude::Float / 57.2958 ) *
												 Cos( pa.longitude::Float / 57.2958 - ' . ( double ) $fltLongitude . ' / 57.2958 ) )
												 ) ^ 2 / 400
											ELSE 10
											END AS miles_score';

				} else {
					$strCaseParameters .= ' WHEN pa.latitude IS NULL AND pa.longitude IS NULL THEN 10 ELSE 10 END AS miles_score';
				}

				$strSearchScore .= ' + miles_score::numeric';

			} else {
				if( false == is_null( $objPropertySearch->getStateCode() ) && 0 != strlen( $objPropertySearch->getStateCode() ) ) {
					array_push( $arrstrWhereParameters, 'pa.state_code = \'' . addslashes( $objPropertySearch->getStateCode() ) . '\'' );
				}
			}
		}

		$strWhereParameters          = '';
		$strPortalTypeFieldReference = ( 'portal' == $strPsProduct ) ? 'wp.hide_on_prospect_portal' : 'wp.hide_on_resident_portal';

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrWhereParameters ) ) {
			$strWhereParameters = ' AND ' . implode( ' AND ', $arrstrWhereParameters );
		}

		$strSql = 'SELECT *
					FROM (
						SELECT *
							, ( 0 ' . $strSearchScore . ' )
							 AS total_score
						FROM (
							SELECT
								p.id,
								p.cid,
								p.property_type_id,
								p.vaultware_number,
								p.property_name,
								p.number_of_units,
								p.min_rent,
								p.max_rent,
								p.min_square_feet,
								p.max_square_feet,
								p.min_bedrooms,
								p.max_bedrooms,
								p.min_bathrooms,
								p.max_bathrooms,
								p.year_built,
								pd.community_website_uri,
								pd.corporate_website_uri,
								wp.hide_more_info_button,
								wp.show_application_fee_option,
								wp.more_info_action,
								pa.city,
								pa.state_code,
								CASE WHEN ( 0 = wp.order_num ) THEN NULL ELSE wp.order_num END AS modified_order_num
								' . $strCaseParameters . '
							FROM
								properties p
								INNER JOIN property_details AS pd ON ( p.id = pd.property_id AND p.cid = pd.cid )
								INNER JOIN website_properties AS wp ON ( wp.website_id = ' . ( int ) $intWebsiteId . ' AND wp.cid = ' . ( int ) $intCid . ' AND p.id = wp.property_id AND p.cid = wp.cid AND ' . $strPortalTypeFieldReference . ' != 1 )
								INNER JOIN property_addresses AS pa ON ( p.id = pa.property_id AND p.cid = pa.cid AND pa.address_type_id = ' . CAddressType::PRIMARY . ' AND pa.is_alternate = false )
							' . $strInnerJoin . '

							' . $strPropertyPreferencesParameters . '
							WHERE
								p.is_disabled <> 1
								AND p.property_id IS NULL ' . $strWhereCondition . ' ';

		if( 0 < strlen( $strWhereParameters ) ) {
			$strSql .= $strWhereParameters;
		}

		$strSql .= '		) AS pts
						) AS pts ';

		$strSql .= 'WHERE cid = ' . ( int ) $intCid;

		if( false == empty( $strOrderByWhatsImportant ) && 0 < strlen( trim( $strOrderByWhatsImportant ) ) ) {
			$strSql .= ' AND total_score <= ' . ( int ) $intAllowedMaxScore;
			$strSql .= ' ORDER BY ' . $strOrderByWhatsImportant . ', total_score';
		} else {
			if( true == is_numeric( $objPropertySearch->getMilesRadius() ) && false == is_null( $objPropertySearch->getPostalCode() ) && 0 != strlen( trim( $objPropertySearch->getPostalCode() ) ) ) {
				$strSql .= ' AND total_score <= ' . ( int ) $intAllowedMaxScore;
				$strSql .= ' ORDER BY
									miles_score';
			} else {
				$strSql .= ' AND total_score = 0';
				$strSql .= ' ORDER BY
									total_score';
			}
		}

		if( true == isset( $objPropertySearch ) && 'sort_number' == $objPropertySearch->getSearchResultSortBy() ) {
			$strSql .= ', modified_order_num';
		}

		$strSql .= ', property_name';

		return $strSql;
	}

	public static function fetchPropertiesAddressesUnitCountByCidByIds( $intCid, $arrintPropertyIds, $objDatabase ) {
		$strSql = ' SELECT
						p.property_name,
						pa.property_id,
						pa.street_line1,
						pa.street_line2,
						pa.city,
						pa.state_code,
						pa.postal_code,
						COALESCE( p.number_of_units, 0 ) as number_of_units,
						count(pu.id) AS property_units_count
					FROM
						properties p
						JOIN property_addresses pa ON ( pa.cid = p.cid AND p.id = pa.property_id AND pa.address_type_id = ' . CAddressType::PRIMARY . ' AND pa.is_alternate = false )
						LEFT JOIN property_units pu ON ( pu.cid = p.cid AND p.id = pu.property_id AND pu.deleted_on IS NULL )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					GROUP BY
						p.property_name,
						pa.property_id,
						pa.street_line1,
						pa.street_line2,
						pa.city,
						pa.state_code,
						pa.postal_code,
						p.number_of_units
					ORDER BY
						lower ( p.property_name )';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchMigrateOrNonIntegratedPropertiesByCid( $intCid, $objDatabase, $boolIsSimpleData = false, $arrintPropertyIds = NULL, $boolIsCorporateCustomer = false, $boolIsNotFromCorporate = false, $boolShowDisabledData = false ) {

		$strSql = 'SELECT
					' . ( ( true == $boolIsSimpleData ) ? 'p.id' : 'p.*' ) . '
				FROM
					properties p
					LEFT JOIN property_integration_databases pid ON ( pid.cid = ' . ( int ) $intCid . ' AND pid.property_id = p.id AND pid.cid = p.cid )
					LEFT JOIN integration_databases idb ON ( idb.cid = ' . ( int ) $intCid . ' AND idb.id = pid.integration_database_id AND idb.cid = pid.cid )
				WHERE
					p.cid = ' . ( int ) $intCid . '
					' . ( ( false == $boolShowDisabledData ) ? ' AND p.is_disabled <> 1' : '' ) . '
					AND ( p.remote_primary_key IS NULL OR pid.id IS NULL OR idb.integration_client_type_id IN ( ' . implode( ',', CIntegrationClientType::$c_arrintMigrateIntegrationClientTypeIds ) . ' ) )';

		$strSql .= ( true == valArr( $arrintPropertyIds ) ) ? ' AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' : '';

		if( true == $boolIsCorporateCustomer ) {
			$strSql .= ' AND p.property_type_id = ' . CPropertyType::CORPORATE;
		}

		if( true == $boolIsNotFromCorporate ) {
			$strSql .= ' AND p.property_type_id <> ' . CPropertyType::CORPORATE;
		}

		$strSql .= ' ORDER BY
						LOWER( property_name ) ASC';

		if( false == $boolIsSimpleData ) {
			return self::fetchProperties( $strSql, $objDatabase );
		}

		$arrmixProperties       = ( array ) fetchData( $strSql, $objDatabase );
		$arrintFinalPropertyIds = [];

		foreach( $arrmixProperties as $arrintPropertyIds ) {
			$arrintFinalPropertyIds[$arrintPropertyIds['id']] = $arrintPropertyIds['id'];
		}

		return $arrintFinalPropertyIds;
	}

	public static function fetchPropertiesWithClientByIds( $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return false;
		}

		$strSql = 'SELECT
						p.*,
						mc.company_name AS client_name
					FROM
						properties p
						JOIN clients mc ON ( mc.id = p.cid )
					WHERE
						p.is_disabled = 0
						AND p.termination_date IS NULL
						AND p.cid IS NOT NULL
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchSimplePropertiesInfoAndPropertyGroupsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.lookup_code,
						p.property_name,
						pgt.id AS property_group_type_id,
						util_get_translated( \'name\', pgt.name, pgt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS property_group_type_name,
						pg.id AS property_group_id,
						pg.name AS property_group_name
					FROM
						properties AS p
						LEFT JOIN property_group_associations AS pga ON ( pga.cid = p.cid AND p.id = pga.property_id )
						LEFT JOIN property_groups AS pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						LEFT JOIN property_group_types AS pgt ON ( pg.property_group_type_id = pgt.id AND pg.cid = pgt.cid AND pgt.deleted_by IS NULL AND pgt.deleted_on IS NULL )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ( pgt.system_code NOT IN ( \'' . CPropertyGroupType::ALL_PROPERTY_SYSTEM_CODE . '\', \'' . CPropertyGroupType::PROPERTY_LIST_SYSTEM_CODE . '\' )
								OR pgt.system_code IS NULL )
					GROUP BY
						p.id,
						p.lookup_code,
						p.property_name,
						pgt.id,
						pgt.name,
						pg.id,
						pg.name,
						pgt.details-->\'' . CLocaleContainer::createService()->getLocaleCode() . '-->name\'
					ORDER BY
						LOWER( p.property_name ),
						LOWER( pgt.name ),
						LOWER( pg.name ) ';

		$arrmixProperties = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixProperties ) ) {
			return NULL;
		}

		return $arrmixProperties;
	}

	public static function fetchPropertiesByPropertyIdsByPropertyGroupTypeByPropertyGroupNameByCid( $arrintPropertyIds, $strPropertyGroupType, $strPropertyGroupName, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						p.*
					FROM
						properties AS p
						LEFT JOIN property_group_associations AS pga ON ( pga.cid = p.cid AND p.id = pga.property_id )
						LEFT JOIN property_groups AS pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						LEFT JOIN property_group_types AS pgt ON ( pgt.cid = pg.cid AND pg.property_group_type_id = pgt.id AND pgt.deleted_by IS NULL AND pgt.deleted_on IS NULL )
					WHERE
						pgt.name = \'' . addslashes( $strPropertyGroupType ) . '\'
						AND pg.name = \'' . addslashes( $strPropertyGroupName ) . '\'
						AND p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND p.is_disabled = 0
					ORDER BY
						property_name';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesWithStatesWithPropertyTypesByPropertyIdsByCid( $arrintPropertyIds, $strOrderBy, $intCid, $objDatabase ) {
		if( false == valStr( $strOrderBy ) ) {
			$strOrderBy = 'property_name';
		}

		$strSql = 'SELECT
						cp.id,
						cp.cid,
						cp.property_type_id,
						cp.company_region_id,
						cp.remote_primary_key,
						cp.property_name,
						cp.lookup_code,
						cp.year_built,
						cp.year_remodeled,
						cp.short_description,
						cp.full_description,
						cp.has_availability,
						cp.is_disabled,
						util_get_system_translated( \'name\', pt.name, pt.details ) AS property_type_name,
						pa.state_code,
						ps.name AS state_name,
						pa.province
					FROM
						properties cp
						LEFT JOIN property_types pt ON cp.property_type_id = pt.id
						LEFT JOIN property_addresses pa ON ( pa.cid = cp.cid AND cp.id = pa.property_id AND pa.is_alternate = false )
						LEFT JOIN states ps ON ( pa.state_code = ps.code )
					WHERE
						cp.cid =' . ( int ) $intCid . '
						AND cp.id in ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pa.address_type_id = ' . CAddressType::PRIMARY . '
						AND cp.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
					ORDER BY ' . $strOrderBy;

		return parent::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByMerchantAccountIdByCid( $intMerchantAccountId, $intCid, $objDatabase, $boolIsOrderBy = false ) {
		$strSql = 'SELECT
						p.*
					FROM
						properties p
						JOIN property_merchant_accounts pma ON ( pma.cid = p.cid AND p.id = pma.property_id AND pma.disabled_by IS NULL AND pma.disabled_on IS NULL )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND pma.company_merchant_account_id = ' . ( int ) $intMerchantAccountId;
		if( true == $boolIsOrderBy ) {
			$strSql .= ' ORDER By p.property_name ';
		}

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByCompanyUserIdByMerchantAccountIdByCid( $intCompanyUserId, $intMerchantAccountId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						p.*
					FROM
						properties p
						JOIN property_merchant_accounts pma ON ( pma.cid = p.cid AND p.id = pma.property_id AND pma.disabled_by IS NULL AND pma.disabled_on IS NULL )
						JOIN view_company_user_properties cup ON ( cup.cid = p.cid AND p.id = cup.property_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND cup.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND pma.company_merchant_account_id = ' . ( int ) $intMerchantAccountId;

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchAllPropertiesByCids( $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		return self::fetchProperties( 'SELECT * FROM properties WHERE cid IN ( ' . implode( ',', $arrintCids ) . ' )', $objDatabase );
	}

	public static function fetchCustomActivePropertiesByCids( $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.cid
					FROM
						properties P
					WHERE
						p.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						AND p.is_disabled = 0';

		$arrmixProperties = ( array ) fetchData( $strSql, $objDatabase );

		$arrintActivePropertyIds = [];

		foreach( $arrmixProperties as $arrmixProperty ) {
			$arrintActivePropertyIds[$arrmixProperty['cid']][$arrmixProperty['id']] = $arrmixProperty['id'];
		}

		return $arrintActivePropertyIds;
	}

	public static function fetchAllPropertiesByIdsByCids( $arrintPropertyIds, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						properties
					WHERE
						cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					ORDER BY
						property_name';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchMigratedPropertiesByCid( $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						p.*
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND pgs.property_id = p.id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND pgs.is_ap_migration_mode = true';

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchPropertiesCountForMigratedProperties( $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						COUNT( p.id ) AS count
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND pgs.property_id = p.id )
					WHERE
						pgs.cid = ' . ( int ) $intCid . '
						AND pgs.is_ap_migration_mode = true';

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchCountMigratedPropertiesByCid( $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						COUNT( p.id ) AS count
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND pgs.property_id = p.id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND pgs.is_ap_migration_mode = true';

		return parent::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchOrganizedCompanyMediaFilesByPropertyIdsByMediaFileTypeIdsByCid( $strCategoryName = 'GENERAL', $strOrganizeByKey = NULL, $arrintPropertyIds, $arrintMediaTypeIds, $intCid, $objDatabase ) {
		$arrobjCompanyMediaFiles = \Psi\Eos\Entrata\CCompanyMediaFiles::createService()->fetchCategorizedCompanyMediaFilesByPropertyIdsByMediaTypeIdsByCid( $strCategoryName, $arrintPropertyIds, $arrintMediaTypeIds, $intCid, $objDatabase );

		if( false == is_null( $strOrganizeByKey ) ) {
			return CObjectModifiers::createService()->reOrganizeObjectsByCommonKey( $strOrganizeByKey, $arrobjCompanyMediaFiles );
		} else {
			return $arrobjCompanyMediaFiles;
		}
	}

	public function fetchOrganizedMarketingMediaAssociationsByPropertyIdsByMediaSubTypeIdsByCid( $strCategoryName = 'GENERAL', $strOrganizeByKey = NULL, $arrintPropertyIds, $arrintMediaSubTypeIds, $intCid, $objDatabase ) {
		$arrobjMarketingMediaAssociations = \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsAndCategoriesByReferenceIdsByMediaTypeIdByMediaSubTypeIdsByCid( $strCategoryName, $arrintPropertyIds, CMarketingMediaType::PROPERTY_MEDIA, $arrintMediaSubTypeIds, $intCid, $objDatabase );

		if( false == is_null( $strOrganizeByKey ) ) {
			return CObjectModifiers::createService()->reOrganizeObjectsByCommonKey( $strOrganizeByKey, $arrobjMarketingMediaAssociations );
		} else {
			return $arrobjMarketingMediaAssociations;
		}
	}

	public static function fetchSimplePropertyIdsByCompanyUserIdBySessionDataForPaymentByCid( $intCompanyUserId, $intIsDisabledSessionData, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						up.property_id AS property_id
					FROM
						view_company_user_properties up
						JOIN properties p ON ( p.cid = up.cid AND p.id = up.property_id AND up.company_user_id = ' . ( int ) $intCompanyUserId . ' AND up.cid = ' . ( int ) $intCid . ' )
						LEFT JOIN property_merchant_accounts pmc ON ( pmc.cid = p.cid AND p.id = pmc.property_id AND pmc.disabled_by IS NULL AND pmc.disabled_on IS NULL )
						LEFT JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND p.id = pgs.property_id AND pgs.activate_standard_posting = true )
						LEFT JOIN property_products pp ON ( pp.cid = p.cid AND p.id = pp.property_id AND pp.ps_product_id = ' . CPsProduct::ENTRATA . ' )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND ( pmc.id is not NULL OR pgs.id is not NULL OR pp.id is not NULL )';

		if( true == is_null( $intIsDisabledSessionData ) || 0 == $intIsDisabledSessionData ) {
			$strSql .= ' AND p.is_disabled = 0 ';
		}

		$strSql .= ' ORDER BY
						lower ( p.property_name ) ASC,
						order_num ';

		$arrmixProperties       = fetchData( $strSql, $objDatabase );
		$arrintFinalPropertyIds = [];

		if( true == valArr( $arrmixProperties ) ) {
			foreach( $arrmixProperties as $arrintPropertyIds ) {
				$arrintFinalPropertyIds[] = $arrintPropertyIds['property_id'];
			}
		}

		return $arrintFinalPropertyIds;
	}

	public static function fetchPropertiesAddressByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.*,
						lower ( pa.city ) AS city,
						lower ( pa.street_line1 ) AS street_line1,
						pa.state_code,
						pa.postal_code
					FROM
						properties p
						JOIN property_addresses pa ON ( pa.cid = p.cid AND p.id = pa.property_id AND pa.is_alternate = false )
					WHERE
						p.id in ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
						p.property_name';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCustomSearchProperties( $arrmixFilteredExplodedSearch, $objDatabase ) {
		$strPropertyIdCondition = ( 1 == \Psi\Libraries\UtilFunctions\count( $arrmixFilteredExplodedSearch ) && is_numeric( $arrmixFilteredExplodedSearch[0] ) ) ? ' to_char( p.id, \'99999999\' ) LIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' OR ' : '';

		$strSql = 'SELECT
						p.id,
						p.property_name,
						c.ps_lead_id,
						c.id AS company_id,
						c.company_name
					FROM
						properties p
						JOIN clients c ON ( c.id = p.cid )
					WHERE
						( ' . $strPropertyIdCondition . ' p.property_name ILIKE E\'%' . implode( '%\' AND p.property_name ILIKE E\'%', $arrmixFilteredExplodedSearch ) . '%\' )
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
					ORDER BY
						p.id
					LIMIT
						10';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyNamesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $arrintPropertyTypeIds = NULL, $strKey = NULL, $boolIsSkipDisabledProperties = false, $strEnableAncillaryRevenue = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strJoinCondition = NULL;
		if( true == valStr( $strKey ) ) {
			$strJoinCondition = ' JOIN property_preferences pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.key = \'' . $strKey . '\' )';
			if( true == valStr( $strEnableAncillaryRevenue ) ) {
				$strJoinCondition .= ' JOIN property_preferences pp1 ON ( pp1.cid = p.cid AND pp1.property_id = p.id AND pp1.key = \'' . $strEnableAncillaryRevenue . '\' )';
			}
		}

		$strSql = 'SELECT
						p.id,
						p.property_name
					FROM
						properties p
						' . $strJoinCondition . '
					WHERE
						p.id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' ) ' .
		          ( ( true == $boolIsSkipDisabledProperties ) ? 'AND p.is_disabled = 0' : '' ) . '
						AND p.cid = ' . ( int ) $intCid .
		          ( ( true == valArr( $arrintPropertyTypeIds ) ) ? ' AND p.property_type_id IN ( ' . implode( ',', $arrintPropertyTypeIds ) . ' )' : '' ) . '
					ORDER BY
						lower ( p.property_name )';

		$arrmixProperties = ( array ) fetchData( $strSql, $objDatabase );

		$arrstrProperties = [];

		foreach( $arrmixProperties as $arrmixProperty ) {
			$arrstrProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
		}

		return $arrstrProperties;
	}

	public static function fetchCustomPropertyById( $intId, $objDatabase ) {
		return self::fetchProperty( sprintf( 'SELECT * FROM properties WHERE id = %d ', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPropertyNamesWithoutBankSetupByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND ( pgs.rent_bank_account_id IS NULL OR pgs.deposit_bank_account_id IS NULL )
						AND p.id IN	( ' . implode( ', ', $arrintPropertyIds ) . ' )
					ORDER BY
						lower ( p.property_name )';

		$arrmixProperties = ( array ) fetchData( $strSql, $objDatabase );

		$arrstrProperties = [];

		foreach( $arrmixProperties as $arrmixProperty ) {
			$arrstrProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
		}

		return $arrstrProperties;
	}

	public static function fetchSimplePropertiesByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $objPropertyFilter = NULL ) {

		$strTemplateCondition = '
			AND property_type_id <> ' . CPropertyType::SETTINGS_TEMPLATE . ' 
			AND property_type_id <> ' . CPropertyType::TEMPLATE . '';

		$arrstrAdditionalAndStatements = [ ' 1 = 1 ' ];

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		if( true == valObj( $objPropertyFilter, 'CPropertyFilter' ) ) {
			if( true == valStr( $objPropertyFilter->getPropertyName() ) && 'Search Properties' != trim( $objPropertyFilter->getPropertyName() ) ) {
				$arrstrAdditionalAndStatements[] = ' property_name ILIKE E\'%' . $objPropertyFilter->sqlPropertyName() . '%\'';
			}

			if( 1 == $objPropertyFilter->getHideDisabled() ) {
				$arrstrAdditionalAndStatements[] = ' is_disabled <> 1 ';
			}

			if( 1 == $objPropertyFilter->getParentOnly() ) {
				$arrstrAdditionalAndStatements[] = ' property_id IS NULL ';
			}

			if( true == is_numeric( $objPropertyFilter->getExludePropertyId() ) ) {
				$arrstrAdditionalAndStatements[] = ' id <> ' . ( int ) $objPropertyFilter->getExludePropertyId();
			}

			if( true == $objPropertyFilter->getShowSettingsTemplates() ) {
				$strTemplateCondition = '';
			}
		}

		$strSql = 'SELECT
						id,
						property_name,
						property_type_id,
						occupancy_type_ids
					FROM
						properties
					WHERE
						id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND ' . implode( ' AND ', $arrstrAdditionalAndStatements ) . $strTemplateCondition . '
					ORDER BY
						lower ( property_name ) ASC,
						order_num';

		$arrstrPropertiesData = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrstrPropertiesData ) ) {
			return NULL;
		}

		foreach( $arrstrPropertiesData as $arrstrPropertyData ) {
			$arrstrProperties[$arrstrPropertyData['id']] = $arrstrPropertyData;
		}

		return $arrstrProperties;
	}

	public static function fetchPropertiesByPropertyPreferenceKeyByCid( $strPropertyPreferenceKey, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						p.*
					FROM
						properties p
						JOIN property_preferences pp ON ( pp.cid = p.cid
														 AND pp.property_id = p.id
														 AND pp.key = \'' . $strPropertyPreferenceKey . '\' )
						JOIN property_gl_settings pgs ON (	pgs.cid = p.cid
															AND p.id = pgs.property_id
															AND pgs.activate_standard_posting = true )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND pp.value IS NOT NULL
					ORDER BY
						p.property_name';

		return parent::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByPropertyPreferenceKeyByPropertyIdsByCid( $strPropertyPreferenceKey, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valStr( $strPropertyPreferenceKey ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.*
					FROM
						properties p
						JOIN property_preferences pp ON ( pp.cid = p.cid
														 AND pp.property_id = p.id
														 AND pp.key = \'' . $strPropertyPreferenceKey . '\' )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND pp.value IS NOT NULL
						AND p.id = ANY ( array[' . implode( ',', $arrintPropertyIds ) . '] )
					ORDER BY
						p.property_name';

		return parent::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertyIdsByOccupancyTypeIdByPropertyPreferenceKeyByCid( $intOccupancyTypeId, $strPropertyPreferenceKey, $intCid, $objDatabase, $boolCheckPropertyGlSetting = true, $strCountryCode = NULL ) {
		$strSql = 'SELECT
						p.id
					FROM
						properties p
						JOIN property_preferences pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.key = \'' . $strPropertyPreferenceKey . '\' )';
		if( false != $boolCheckPropertyGlSetting ) {
			$strSql .= '
			JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND p.id = pgs.property_id AND pgs.activate_standard_posting = true )';
		}
		$strSql .= '
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND ' . ( int ) $intOccupancyTypeId . ' =  ANY( p.occupancy_type_ids ) 
						AND pp.value IS NOT NULL';
		if( false != valStr( $strCountryCode ) ) {
			$strSql .= '
						AND p.country_code = \'' . $strCountryCode . '\'';
		}
		$strSql .= '
					ORDER BY
						p.property_name';

		$arrmixProperties = fetchData( $strSql, $objDatabase );

		$arrintPropertyIds = NULL;

		if( true == valArr( $arrmixProperties ) ) {
			$arrintPropertyIds = array_keys( rekeyArray( 'id', $arrmixProperties ) );
		}

		return $arrintPropertyIds;
	}

	public static function fetchActivePropertiesWithGlTreeByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						p.*,
						gt.name AS gl_tree_name,
						gt.id AS gl_tree_id
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id )
						JOIN property_preferences pp ON ( pp.cid = p.cid AND pp.property_id = p.id )
						LEFT JOIN gl_trees gt ON ( gt.cid = pgs.cid AND pgs.gl_tree_id = gt.id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
						AND ( ( p.property_type_id <> ' . ( int ) CPropertyType::SETTINGS_TEMPLATE . '
						AND p.property_type_id <> ' . ( int ) CPropertyType::TEMPLATE . ' AND pp.key <> \'ENABLE_SEMESTER_SELECTION\' AND pp.value IS NOT NULL ) OR ( ' . COccupancyType::STUDENT . ' = ANY( p.occupancy_type_ids ) AND pp.key = \'ENABLE_SEMESTER_SELECTION\' AND pp.value IS NOT NULL ) )
					ORDER BY
						p.property_name ASC';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchSimplePropertiesGroupedByGlTreesByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						gt.id AS gl_tree_id,
						pgs.property_id
					FROM
						property_gl_settings pgs
						JOIN properties p ON ( p.cid = pgs.cid AND p.id = pgs.property_id )
						JOIN gl_trees gt ON ( gt.cid = pgs.cid AND gt.id = pgs.gl_tree_id )
					WHERE
						pgs.cid = ' . ( int ) $intCid . '
					ORDER BY
						 lower ( p.property_name ) ASC';

		$arrmixTempData                   = fetchData( $strSql, $objDatabase );
		$arrmixPropertiesGroupedByGlTrees = [];

		if( true == valArr( $arrmixTempData ) ) {
			foreach( $arrmixTempData as $arrmixData ) {
				$arrmixPropertiesGroupedByGlTrees[$arrmixData['gl_tree_id']][] = $arrmixData['property_id'];
			}
		}

		return $arrmixPropertiesGroupedByGlTrees;
	}

	public static function fetchIsPropertyExistByIdByKeyByCid( $intPropertyId, $strKey, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						p.id
					FROM
						properties p
					JOIN
						property_preferences pp ON ( pp.cid = p.cid AND pp.property_id = p.id )
					WHERE
						p.id = ' . ( int ) $intPropertyId . '
						AND p.cid = ' . ( int ) $intCid . '
						AND pp.key = \'' . addslashes( $strKey ) . '\'';

		$objProperty = parent::fetchProperty( $strSql, $objDatabase );

		return !( valObj( $objProperty, 'CProperty' ) );
	}

	public static function fetchPropertyNamesWithoutPropertySettingsByKeysByPropertyIdByCid( $arrintPropertyIds, $arrstrKeys, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrstrKeys ) ) {
			return NULL;
		}

		$intAliasCounter = 1;
		$strJoins        = '';
		$strWhere        = '';
		foreach( $arrstrKeys as $strKey ) {
			$strAlias = 'pp' . $intAliasCounter;
			$strJoins .= ' LEFT JOIN property_preferences ' . $strAlias . ' ON ( p.cid = ' . $strAlias . '.cid AND p.id = ' . $strAlias . '.property_id AND ' . $strAlias . '.key = ' . pg_escape_literal( $objDatabase->getHandle(), $strKey ) . ' AND ' . $strAlias . '.value IS NOT NULL AND ' . $strAlias . '.value != \'0\' )';
			if( $intAliasCounter > 1 ) {
				$strWhere .= 'OR ';
			}
			$strWhere .= $strAlias . '.id IS NULL ';
			$intAliasCounter++;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name
					FROM
						properties p';
		$strSql .= $strJoins;
		$strSql .= 'WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND ( ' . $strWhere . ' )
					ORDER BY
						lower ( p.property_name )';

		$arrmixProperties = ( array ) fetchData( $strSql, $objDatabase );

		$arrstrProperties = [];

		foreach( $arrmixProperties as $arrmixProperty ) {
			$arrstrProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
		}

		return $arrstrProperties;
	}

	public static function fetchDisabledPropertiesByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						properties p
					WHERE
						p.is_disabled = 1
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
						lower ( p.property_name )';

		return parent::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchNumberOfUnitsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || true == is_null( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						SUM( p.number_of_units ) AS number_of_units,
						p.id
					FROM
						properties p
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					GROUP BY
						p.id';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchProductUnAssociatedPropertiesByPsProductIds( $arrintPsProductIds, $intCid, $objDatabase, $arrstrPropertyPreferences = NULL ) {
		if( false == valArr( $arrintPsProductIds ) ) {
			return NULL;
		}

		if( true == valArr( $arrstrPropertyPreferences ) ) {

			$strSql = 'SELECT
							DISTINCT( p.id ),
							p.*
						FROM
							properties p
							LEFT JOIN property_products pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' ) )
							LEFT JOIN property_preferences ppf ON ( ppf.cid = p.cid AND ppf.property_id = p.id AND ppf.key IN ( \'' . implode( ',', $arrstrPropertyPreferences ) . '\' ) AND ppf.value = \'1\' )
						WHERE
							p.cid = ' . ( int ) $intCid . '
							AND ( pp.id IS NULL OR CASE WHEN pp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' ) THEN ppf.id IS NOT NULL ELSE FALSE END )
							AND p.is_disabled = 0
						ORDER BY
							p.property_name';
		} else {
			$strSql = 'SELECT
							DISTINCT( p.id ),
							p.*
						FROM
							properties p
							LEFT JOIN property_products pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' ) )
						WHERE
							p.cid = ' . ( int ) $intCid . '
							AND pp.id IS NULL
							AND p.is_disabled = 0
						ORDER BY
							p.property_name';
		}

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchTestPropertiesByCid( $intCid, $objDatabase ) {
		if( true == is_null( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						properties p
					WHERE
						p.cid= ' . ( int ) $intCid . '
						AND p.is_test = 1';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchSimplePropertyByIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		if( true == empty( $intPropertyId ) || false == is_numeric( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						properties p
					WHERE
						p.id = ' . ( int ) $intPropertyId . ' AND p.cid = ' . ( int ) $intCid;

		return self::fetchProperty( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByKeyByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						p.id,
						p.property_name AS property_group_name,
						pp.key AS property_preference_key,
						pp.value AS property_preference_value
					FROM
						properties p
						JOIN property_preferences pp ON ( p.cid = pp.cid AND p.id = pp.property_id)
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND pp.key IN ( \'REQUIRE_POS_FOR_INVOICES\' )
						AND pp.value IN (\'1\')
						AND p.is_disabled = 0
					group by p.id,
						p.property_name,
						pp.key,
						pp.value';

		return parent::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesWithHighRiseByPropertyPreferenceKeyByCidByWebsiteId( $strPropertyPreferenceKey, $intCid, $intWebsiteId, $objDatabase ) {
		$strSql = 'SELECT
						p.*, pp.key, pp.value
					FROM
						website_properties wp,
						properties p
						JOIN property_preferences pp ON ( pp.cid = p.cid
														 AND pp.property_id = p.id
														 AND pp.key = \'' . $strPropertyPreferenceKey . '\' )
					WHERE
						p.id = wp.property_id
						AND p.cid = ' . ( int ) $intCid . '
						AND p.cid = wp.cid
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						AND wp.hide_on_prospect_portal != 1
						AND pp.value IS NOT NULL
					ORDER BY
						p.property_name';

		return parent::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchRelatedPropertyIdsByParentPropertyIdByCid( $intParentPropertytId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						p.id
					FROM
						properties p
					WHERE
						( p.id = ' . ( int ) $intParentPropertytId . ' OR p.property_id = ' . ( int ) $intParentPropertytId . ' )
						AND p.cid = ' . ( int ) $intCid;

		$arrmixProperties         = fetchData( $strSql, $objDatabase );
		$arrintRelatedPropertyIds = [];
		if( true == valArr( $arrmixProperties ) ) {
			foreach( $arrmixProperties as $arrintPropertyIds ) {
				$arrintRelatedPropertyIds[$arrintPropertyIds['id']] = $arrintPropertyIds['id'];
			}
		}

		return $arrintRelatedPropertyIds;

	}

	public static function fetchRelatedPropertyIdsExcludedByPropertyIdsByKey( $arrintPropertyIds, $intCid, $arrstrKey, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id
					FROM
						properties p
						LEFT JOIN property_preferences pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.key IN ( \'' . implode( "','", $arrstrKey ) . '\' ) )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pp.value IS NULL';

		$arrintPropertyIds = fetchData( $strSql, $objDatabase );

		return $arrintPropertyIds;
	}

	public static function fetchRelatedPropertyIdsByParentPropertyIdByCidByPsProductId( $intPropertytId, $intCid, $intPsProductId, $objDatabase ) {
		$strSql = 'SELECT
						p.id
					FROM
						properties p
						JOIN property_products pp ON (
							( p.id = pp.property_id OR pp.property_id IS NULL )
							AND p.cid = pp.cid
							AND pp.cid = ' . ( int ) $intCid . '
							AND pp.ps_product_id = ' . ( int ) $intPsProductId . '
						)
					WHERE
						( p.id = ' . ( int ) $intPropertytId . ' OR p.property_id = ' . ( int ) $intPropertytId . ' )
						AND p.cid = ' . ( int ) $intCid . '
					GROUP BY
						p.id';

		return array_keys( rekeyArray( 'id', ( array ) fetchData( $strSql, $objDatabase ) ) );
	}

	public static function fetchAllPaginatedPropertiesAndCountByPropertyIdsAndCid( $intCid, $objDatabase, $objPagination = NULL, $boolCountOnly = false, $strPropertyIds ) {
		if( false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSelectSql    = 'SELECT p.* ';
		$strOffestSql    = '';
		$strConditionSql = '';

		if( true == $boolCountOnly ) {
			$strSelectSql .= ', count( p.id ) over() AS row_count ';
		}
		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strOffestSql = ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		$strFromSql = 'FROM
							properties p
						WHERE
							p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' ) AND p.cid = ' . ( int ) $intCid;

		if( true == valStr( $strPropertyIds ) ) {
			$strConditionSql = ' AND p.id IN ( ' . $strPropertyIds . ' ) ';
		}

		$strSql = $strSelectSql . $strFromSql . $strConditionSql . ' ORDER BY LOWER( property_name ) ASC ' . $strOffestSql;

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCustomAllPropertiesUsingAutoPostCharges( CDatabase $objDatabase ) {

		$strSql = 'SELECT
						p.*
					FROM
						properties p
						JOIN property_products pp ON ( pp.cid = p.cid AND pp.ps_product_id = ' . CPsProduct::ENTRATA . ' AND ( pp.property_id = p.id OR pp.property_id IS NULL ) )
						JOIN property_charge_settings pcs ON ( pcs.cid = p.cid AND pcs.property_id = p.id )
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND pgs.property_id = p.id AND pgs.activate_standard_posting = true AND pgs.is_ar_migration_mode = false )
					WHERE
						pcs.auto_post_scheduled_charges = true
						AND EXISTS (
									SELECT
										1
									FROM
										leases l
										JOIN scheduled_charges sc ON ( sc.cid = l.cid AND sc.lease_id = l.id )
									WHERE
										l.cid = p.cid
										AND l.property_id = p.id
									)
					GROUP BY
						p.cid,
						p.id';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCustomAllPropertiesUsingAutoPostChargesByPropertyIdsByCid( $intCid, $arrintPropertyIds, CDatabase $objDatabase ) {

		$strSql = 'SELECT
						p.*
					FROM
						properties p
						JOIN property_products pp ON ( pp.cid = p.cid AND pp.ps_product_id = ' . CPsProduct::ENTRATA . ' AND ( pp.property_id = p.id OR pp.property_id IS NULL ) )
						JOIN property_charge_settings pcs ON ( pcs.cid = p.cid AND pcs.property_id = p.id )
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND pgs.property_id = p.id AND pgs.activate_standard_posting = true AND pgs.is_ar_migration_mode = false )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pcs.auto_post_scheduled_charges = true
						AND EXISTS (
									SELECT
										1
									FROM
										leases l
										JOIN scheduled_charges sc ON ( sc.cid = l.cid AND sc.lease_id = l.id )
									WHERE
										l.cid = p.cid
										AND l.property_id = p.id
									)
					GROUP BY
						p.cid,
						p.id';

		return self::fetchProperties( $strSql, $objDatabase );

	}

	public static function fetchSimpleAllPropertyIdsUsingAutoPostCharges( CDatabase $objDatabase, $boolOnlyFailedProperties = false ) {

		$strSqlJoinCondition = ( true == $boolOnlyFailedProperties ? 'JOIN property_details pd ON ( pcs.cid = pd.cid AND pcs.property_id = pd.property_id AND pd.charges_last_posted_on < CURRENT_DATE )' : '' );

		$strSql = 'SELECT
						p.cid,
						p.id,
						CASE
							WHEN c.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' AND pps.ps_product_id IS NOT NULL THEN 2
							WHEN c.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' AND pps.ps_product_id IS NULL THEN 1
							ELSE 0
						END AS priority
					FROM
						properties p
						JOIN clients c ON ( p.cid = c.id )
						JOIN property_products pp ON ( pp.cid = p.cid AND pp.ps_product_id = ' . CPsProduct::ENTRATA . ' AND ( pp.property_id = p.id OR pp.property_id IS NULL ) )
						LEFT JOIN property_products pps ON ( pps.cid = p.cid AND pps.ps_product_id = ' . CPsProduct::RESIDENT_PAY . ' AND ( pps.property_id = p.id OR pps.property_id IS NULL ) )
						JOIN property_charge_settings pcs ON ( pcs.cid = p.cid AND pcs.property_id = p.id )
						' . $strSqlJoinCondition . '
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND pgs.property_id = p.id AND pgs.activate_standard_posting = true AND pgs.is_ar_migration_mode = false )
					WHERE
						pcs.auto_post_scheduled_charges = true
						AND EXISTS (
									SELECT
										1
									FROM
										leases l
										JOIN scheduled_charges sc ON ( sc.cid = l.cid AND sc.lease_id = l.id )
									WHERE
										l.cid = p.cid
										AND l.property_id = p.id
									)
					GROUP BY
						p.cid,
						p.id,
						c.company_status_type_id,
						pps.ps_product_id
					ORDER BY
						priority DESC';

		$arrmixProperties = fetchData( $strSql, $objDatabase );

		return $arrmixProperties;
	}

	public static function fetchCustomPaperlessEmailPropertyDetailsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objResidentPortalDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		// exclude clients for full domain Resident Portal url, TaskId: 187211 - ARI
		// 2109 : Winn Residential, 3678 : Northland Investment Corporation
		$arrintCidsExcludedForFullDomain = [ 2109, 3678 ];
		$arrintOnlinePaymentMediumIds    = [ CPaymentMedium::WEB, CPaymentMedium::RECURRING ];

		$strSql = ' WITH PPT AS(
						SELECT
							*
						FROM
							property_payment_types
						WHERE
							property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
							AND cid = ' . ( int ) $intCid . '
							AND allow_in_resident_portal = TRUE
					)
					SELECT
						DISTINCT ON ( p.id ) p.cid,
						p.id AS property_id,
						p.property_name,
						website_details.website_domain,
						CASE
							WHEN
								pp1.property_id IS NULL
							THEN
								1
							ELSE
								0
						END AS send_rent_reminder,
						CASE
							WHEN
								pp2.property_id IS NULL
							THEN
								1
							ELSE
								0
						END AS activate_an_auto_payment,
						CASE
							WHEN
								pp3.property_id IS NOT NULL AND pp3.value IS NOT NULL
							THEN
								pp3.value
							ELSE
								\'1\'
						END AS rent_is_due_on,
						CASE
							WHEN
								pp5.property_id IS NOT NULL THEN 1
							ELSE
								0
						END AS show_monthly_balance,
						CASE
							WHEN
								pp4.property_id IS NOT NULL OR website_details.wpr2_id IS NOT NULL
							THEN
								0
							ELSE
								1
						END AS submit_maintenance_request,
						CASE
							WHEN
								website_details.wpr1_id IS NOT NULL
							THEN
								1
							ELSE
								0
						END AS is_account_number,
						CASE
							WHEN
								ppn.property_id IS NOT NULL AND ppn.phone_number IS NOT NULL
							THEN
								ppn.phone_number
							ELSE
								(
									SELECT
										phone_number
									FROM
										property_phone_numbers
									WHERE
										property_phone_numbers.property_id = p.id
										AND property_phone_numbers.cid = p.cid
										AND phone_number_type_id IN (' . CPhoneNumberType::OFFICE . ', ' . CPhoneNumberType::RESIDENT_OFFICE . ' )
										AND phone_number IS NOT NULL
									ORDER BY
										phone_number_type_id
									LIMIT
										1
								)
						END AS property_phone_number,
						CASE
							WHEN
								ma.account_name IS NOT NULL
							THEN
								1
							ELSE
								0
						END AS merchant_account_set,
						CASE
							WHEN
								pp6.property_id IS NOT NULL AND pp6.value IS NOT NULL
							THEN
								pp6.value
							ELSE
								\'' . CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS . '\'
						END AS from_email_address,
						default_property_media_file_details.fullsize_uri AS cmf_full_size_uri,
						default_company_media_files_details.fullsize_uri AS dcmf_full_size_uri,
						CASE
							WHEN
								mm.merchant_account_id IS NOT NULL
								AND ppt_ach.property_id IS NOT NULL
							THEN
								1
							ELSE
								0
						END AS is_echeck_enabled,
						CASE
							WHEN
								mm1.merchant_account_id IS NOT NULL
								AND ppt_cc.property_id IS NOT NULL
							THEN
								1
							ELSE
								0
						END AS is_cc_enabled,
						CASE
							WHEN
								ppt_emo.property_id IS NOT NULL
							THEN
								1
							ELSE
								0
						END AS is_moneygram_enabled
					FROM
						properties p
						JOIN
						(
							SELECT
								DISTINCT ON ( wp.property_id, wp.cid ) wp.property_id,
								wp.cid,
								CASE
									WHEN
										website_domain IS NOT NULL
									THEN
										website_domain
									ELSE
										lower ( sub_domain ) || \'' . CConfig::get( 'resident_portal_suffix' ) . '\'
								END AS website_domain,
								CASE
									WHEN
										pp.id IS NOT NULL AND pp.value IS NOT NULL AND pp.key = \'RESIDENT_PORTAL_AUTO_LOGIN_DOMAIN\' AND LOWER ( pp.value ) = LOWER ( w.sub_domain )
									THEN
										-1
									WHEN
										wd.id IS NULL
									THEN
										1
									ELSE
										0
								END AS orderby,
								wpr1.id AS wpr1_id,
								wpr2.id AS wpr2_id
							FROM
								websites w
								JOIN website_properties wp ON ( wp.website_id = w.id AND wp.cid = w.cid AND wp.hide_on_resident_portal <> 1 )
								JOIN website_templates wt ON ( wt.id = w.website_template_id AND wt.website_template_type_id <> ' . CWebsiteTemplateType::TEMPLATE_TYPE_LOBBY_DISPLAY . ' )
								LEFT JOIN website_domains wd ON ( w.id = wd.website_id AND w.cid = wd.cid AND wd.cid NOT IN ( ' . implode( ',', $arrintCidsExcludedForFullDomain ) . ' ) AND wd.property_id IS NULL )
								LEFT JOIN property_preferences pp ON ( pp.property_id = wp.property_id AND pp.cid = wp.cid AND pp.key = \'RESIDENT_PORTAL_AUTO_LOGIN_DOMAIN\' )
								LEFT JOIN property_preferences pp1 ON ( pp1.property_id = wp.property_id AND pp1.cid = wp.cid AND pp1.key = \'RP_2_DEFAULT_ENROLLMENT_REQUIREMENT\' AND pp1.value = \'secondary_number\' )
								LEFT JOIN website_preferences wpr2 ON ( wpr2.website_id = w.id AND wpr2.cid = w.cid AND wpr2.key = \'MAINTENANCE_TAB_STATUS\' AND wpr2.value <> \'2\' )
							WHERE
								wp.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
								AND ( wd.id IS NULL
								OR wd.is_primary = 1 )
								AND w.is_disabled <> 1
								AND w.deleted_on IS NULL
							ORDER BY
								wp.property_id,
								wp.cid,
								orderby,
								wp.website_id
						) AS website_details ON ( website_details.property_id = p.id AND website_details.cid = p.cid )
						LEFT JOIN property_phone_numbers ppn ON ( ppn.property_id = p.id AND ppn.cid = p.cid AND ppn.phone_number_type_id = 1 )
						LEFT JOIN property_merchant_accounts pma ON ( p.id = pma.property_id AND p.cid = pma.cid )
						LEFT JOIN merchant_accounts ma ON ( pma.company_merchant_account_id = ma.id AND pma.cid = ma.cid )
						LEFT JOIN merchant_methods mm ON ( ma.id = mm.merchant_account_id AND ma.cid = mm.cid AND mm.payment_type_id = ' . CPaymentType::ACH . ' AND mm.is_enabled = 1 )
						LEFT JOIN merchant_methods mm1 ON ( ma.id = mm1.merchant_account_id AND ma.cid = mm1.cid AND mm1.payment_type_id IN ( ' . CPaymentType::VISA . ', ' . CPaymentType::MASTERCARD . ', ' . CPaymentType::DISCOVER . ', ' . CPaymentType::AMEX . ' ) AND mm1.is_enabled = 1 )
						LEFT JOIN property_preferences pp1 ON ( p.id = pp1.property_id AND p.cid = pp1.cid AND pp1.key = \'DONT_SEND_RENT_REMINDERS\' )
						LEFT JOIN property_preferences pp2 ON ( p.id = pp2.property_id AND p.cid = pp2.cid AND pp2.key = \'ALLOW_STATIC_AND_VARIABLE_RECURRING_PAYMENTS\' AND pp2.value = \'4\' )
						LEFT JOIN property_preferences pp3 ON ( p.id = pp3.property_id AND p.cid = pp3.cid AND pp3.key = \'RENT_IS_DUE_ON_DAY\' )
						LEFT JOIN property_preferences pp4 ON ( p.id = pp4.property_id AND p.cid = pp4.cid AND pp4.key = \'HIDE_MAINTENANCE_TAB\' )
						LEFT JOIN property_preferences pp5 ON ( p.id = pp5.property_id AND p.cid = pp5.cid AND pp5.key = \'SHOW_RESIDENT_BALANCE\' )
						LEFT JOIN property_preferences pp6 ON ( p.id = pp6.property_id AND p.cid = pp6.cid AND pp6.key = \'RESIDENT_PORTAL_PAPERLESS_EMAIL\' )
						LEFT JOIN PPT ppt_ach ON ( p.id = ppt_ach.property_id AND p.cid = ppt_ach.cid AND ppt_ach.payment_type_id = ' . CPaymentType::ACH . ' )
						LEFT JOIN (
							SELECT
								cid,
								property_id
							FROM
								PPT
							WHERE
								payment_type_id IN ( ' . implode( ',', CPaymentType::$c_arrintCreditCardPaymentTypes ) . ' )
							GROUP BY
								cid,
								property_id
						) AS ppt_cc ON ( p.id = ppt_cc.property_id AND p.cid = ppt_cc.cid )
						LEFT JOIN PPT ppt_emo ON ( p.id = ppt_emo.property_id AND p.cid = ppt_emo.cid AND ppt_emo.payment_type_id = ' . CPaymentType::EMONEY_ORDER . ' )
						LEFT JOIN (
							SELECT
								cmf.cid,
								cmf.fullsize_uri
							FROM
								property_medias pm
								JOIN company_media_files cmf ON ( pm.company_media_file_id = cmf.id AND pm.cid = cmf.cid )
							WHERE
								cmf.media_type_id = ' . CMediaType::LOGO . '
							ORDER BY
								cmf.id
						) AS default_property_media_file_details ON ( default_property_media_file_details.cid = p.cid )

						LEFT JOIN (
							SELECT
								cmf1.*
							FROM
								company_media_files cmf1
								JOIN company_medias cm ON ( cmf1.id = cm.company_media_file_id AND cmf1.cid = cm.cid )
							WHERE
								cmf1.media_type_id = ' . CMarketingMediaType::COMPANY_PREFERENCES_DEFAULT_IMAGE . '
							ORDER BY
								cmf1.id
						) AS default_company_media_files_details ON ( default_company_media_files_details.cid = p.cid )
					WHERE
						pma.ar_code_id IS NULL
						AND pma.disabled_by IS NULL
						AND pma.disabled_on IS NULL
						AND ( mm.payment_medium_id IN ( ' . implode( ',', $arrintOnlinePaymentMediumIds ) . ' ) OR mm1.payment_medium_id IN ( ' . implode( ',', $arrintOnlinePaymentMediumIds ) . ' ) )
						AND p.is_disabled <> 1
						AND p.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objResidentPortalDatabase );
	}

	public static function fetchIntegratedPropertyIdsByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						pid.property_id
					FROM
						property_integration_databases pid
						JOIN integration_databases id ON ( id.cid = pid.cid AND pid.integration_database_id = id.id )
					WHERE
						id.integration_client_status_type_id = ' . CIntegrationClientStatusType::ACTIVE . '
						AND id.integration_client_type_id NOT IN ( ' . implode( ',', CIntegrationClientType::$c_arrintMigrateIntegrationClientTypeIds ) . ' )
						AND pid.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pid.cid = ' . ( int ) $intCid;

		$arrmixProperties            = fetchData( $strSql, $objDatabase );
		$arrintIntegratedPropertyIds = [];

		if( true == valArr( $arrmixProperties ) ) {
			foreach( $arrmixProperties as $arrintPropertyIds ) {
				$arrintIntegratedPropertyIds[$arrintPropertyIds['property_id']] = $arrintPropertyIds['property_id'];
			}
		}

		return $arrintIntegratedPropertyIds;
	}

	public static function fetchPropertiesByMaintenanceTemplateIdByCid( $intMaintenanceTemplateId, $intCid, $objDatabase ) {
		if( true == empty( $intMaintenanceTemplateId ) || true == empty( $intCid ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						p.*,
						pmt.id AS property_maintenance_template_id
					FROM
						properties p
						JOIN property_maintenance_templates pmt ON ( pmt.cid = p.cid AND pmt.property_id = p.id )
					WHERE
						pmt.maintenance_template_id = ' . ( int ) $intMaintenanceTemplateId . '
						AND pmt.cid = ' . ( int ) $intCid;

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertyNamesByCid( $intCid, $objDatabase, $boolShowEnabled = false, $arrintSkipPropertyIds = NULL, $boolSkipPropertyTypes = false ) {
		$strWhereIsDisabled = NULL;

		if( true == $boolShowEnabled ) {
			$strWhereIsDisabled .= ' AND is_disabled != 1';
		}

		if( true == valArr( $arrintSkipPropertyIds ) ) {
			$strWhereIsDisabled .= ' AND id NOT IN (' . implode( ',', $arrintSkipPropertyIds ) . ' ) ';
		}

		if( true == $boolSkipPropertyTypes ) {
			$strWhereIsDisabled .= ' AND property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' ) ';
		}

		$strSql = 'SELECT
						id,
						property_name
					FROM
						properties
					WHERE
						cid = ' . ( int ) $intCid . $strWhereIsDisabled . '
					ORDER BY
						lower ( property_name )';

		$arrmixProperties = ( array ) fetchData( $strSql, $objDatabase );

		$arrstrProperties = [];

		foreach( $arrmixProperties as $arrmixProperty ) {
			$arrstrProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
		}

		return $arrstrProperties;
	}

	public static function fetchEnabledPropertyIdsByCidByCompanyStatusTypeIdsByPsProductId( $intCid, $arrintCompanyStatusTypeIds, $intPsProductId, $objDatabase ) {
		if( false == valArr( $arrintCompanyStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT( p.id )
					FROM
						properties p
						JOIN clients mc ON ( p.cid = mc.id )
						JOIN property_products pps ON ( p.cid = pps.cid AND p.property_id = pps.property_id OR pps.property_id IS NULL )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND	mc.company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' )
						AND pps.ps_product_id = ' . ( int ) $intPsProductId . '
						AND p.is_disabled = 0';

		$arrmixProperties         = fetchData( $strSql, $objDatabase );
		$arrintEnabledPropertyIds = [];

		if( true == valArr( $arrmixProperties ) ) {
			foreach( $arrmixProperties as $arrintPropertyIds ) {
				$arrintEnabledPropertyIds[$arrintPropertyIds['id']] = $arrintPropertyIds['id'];
			}
		}

		return $arrintEnabledPropertyIds;

	}

	public static function fetchTotalEnabledPropertiesCountByCidByCompanyStatusTypeIdsByPsProductId( $intCid, $arrintCompanyStatusTypeIds, $intPsProductId, $objDatabase ) {
		if( false == valArr( $arrintCompanyStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						count( DISTINCT( p.id ) )
					FROM
						properties p
						JOIN clients mc ON ( p.cid = mc.id )
						JOIN property_products pps ON ( pps.cid = p.cid AND pps.property_id = p.id OR pps.property_id IS NULL )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND	mc.company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' )
						AND pps.ps_product_id = ' . ( int ) $intPsProductId . '
						AND p.is_disabled = 0';

		$arrmixProperties             = fetchData( $strSql, $objDatabase );
		$arrintEnabledPropertiesCount = [];

		if( true == valArr( $arrmixProperties ) ) {
			foreach( $arrmixProperties as $arrintPropertyIds ) {
				$arrintEnabledPropertiesCount['count'] = $arrintPropertyIds['count'];
			}
		}

		return $arrintEnabledPropertiesCount;
	}

	public static function fetchPropertiesByProductsPermissionsByCid( $intCid, $intCompanyUserId, $arrintProductIds, $objDatabase ) {
		if( false == valArr( $arrintProductIds ) ) {
			return NULL;
		}

		$strSql = self::buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId );

		$strSql = 'SELECT
						DISTINCT ( p.id ),
						p.*
					FROM
						( ' . $strSql . ' ) p
						JOIN property_products AS pp ON ( pp.cid = p.cid AND pp.property_id = p.id OR pp.property_id IS NULL )
					WHERE
						pp.cid = ' . ( int ) $intCid . '
						AND pp.ps_product_id IN ( ' . implode( ',', $arrintProductIds ) . ' )
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
					ORDER BY
						p.property_name ASC,
						p.order_num';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPMEnabledPropertiesByCid( $intCid, $objDatabase, $strKey = NULL, $arrintIds = NULL, $intOccupancyTypeId = NULL, $boolIntegratedPropeties = false, $boolIncludeMilitaryProperties = false, $arrintOccupancyTypeIds = NULL ) {
		$strSql = 'SELECT
				DISTINCT ( p.* ),
				' . $objDatabase->getCollateSort( 'p.property_name' ) . '
			FROM
				properties p
				JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id )
				JOIN property_products pp ON ( p.cid = pp.cid AND ( p.id = pp.property_id OR pp.property_id IS NULL ) ) ';
		if( true == valStr( $strKey ) ) {
			$strSql .= 'JOIN property_preferences ppr ON ( p.id = ppr.property_id
																				AND p.cid = ppr.cid
																				AND ppr.value IS NOT NULL
																				' . ( ( false == $boolIncludeMilitaryProperties ) ? ' AND ppr.key = \'' . $strKey . '\'' : '' ) . ' ) ';
		}
		$strWhereCondition = '';
		if( true == valArr( $arrintOccupancyTypeIds ) ) {
			$strWhereCondition = ' AND p.occupancy_type_ids && ARRAY[' . implode( ',', $arrintOccupancyTypeIds ) . ']';
			$strWhereCondition .= ' AND ( ( ' . ( int ) $intOccupancyTypeId . ' = ANY( p.occupancy_type_ids ) AND ppr.key = \'' . $strKey . '\' ) ' . ( ( true == $boolIncludeMilitaryProperties ) ? ' OR p.property_type_id = ' . CPropertyType::MILITARY : '' ) . ' ) ';
		} elseif( true == valId( $intOccupancyTypeId ) ) {
			$strWhereCondition = ' AND ' . ( int ) $intOccupancyTypeId . ' = ANY( p.occupancy_type_ids )  ';
			if( true == valStr( $strKey ) ) {
				$strWhereCondition = ' AND ' . ( int ) $intOccupancyTypeId . ' = ANY( p.occupancy_type_ids )  AND ppr.key = \'' . $strKey . '\'  ' . ( ( true == $boolIncludeMilitaryProperties ) ? ' OR p.property_type_id = ' . CPropertyType::MILITARY : '' );
			}
		}

		if( true == $boolIntegratedPropeties ) {
			$strSql .= 'LEFT JOIN property_integration_databases pid ON ( pid.cid = p.cid AND pid.property_id = p.id ) ';
		}

		$strSql .= 'WHERE
				p.cid = ' . ( int ) $intCid . '
				' . $strWhereCondition . '
				AND pgs.activate_standard_posting = true
				AND p.is_disabled = 0';

		$strSql .= ( true == $boolIntegratedPropeties ) ? ' AND ( pgs.activate_standard_posting = true AND pp.ps_product_id = 1 OR ( pid.id IS NOT NULL AND p.remote_primary_key IS NOT NULL ) ) ' : ' AND pgs.activate_standard_posting = true AND pp.ps_product_id = 1';

		if( true == valArr( $arrintIds ) ) {
			$strSql .= ' AND p.id IN ( ' . implode( ',', $arrintIds ) . ')';
		}

		$strSql .= ' ORDER BY
				p.property_name ASC';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPMEnabledPropertiesCountByOccupancyTypeIdByPropertyPreferenceKeyByIds( $intOccupancyTypeId, $strPropertyPreferenceKey, $arrintIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) {
			return false;
		}

		$strFrom  = 'properties p
						JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id )
						JOIN property_products pp ON ( p.cid = pp.cid AND ( p.id = pp.property_id OR pp.property_id IS NULL ) )
						JOIN property_preferences ppr ON ( p.id = ppr.property_id AND p.cid = ppr.cid AND ppr.value IS NOT NULL AND ppr.key = \'' . $strPropertyPreferenceKey . '\' ) ';
		$strWhere = 'WHERE p.cid = ' . ( int ) $intCid . '
						AND pp.ps_product_id = 1
						AND ' . ( int ) $intOccupancyTypeId . ' = ANY ( p.occupancy_type_ids )
						AND pgs.activate_standard_posting = true
						AND p.is_disabled = 0
						AND p.id IN ( ' . implode( ',', $arrintIds ) . ')';

		return self::fetchRowCount( $strWhere, $strFrom, $objDatabase );
	}

	public static function fetchAllPmEnabledPropertiesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						pgt.id AS property_group_type_id,
						util_get_translated( \'name\', pgt.name, pgt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS property_group_type_name,
						pg.id AS property_group_id,
						pg.name AS property_group_name
					FROM
						properties AS p
						JOIN property_gl_settings AS pgs ON ( pgs.cid = p.cid AND p.id = pgs.property_id )
						LEFT JOIN property_group_associations AS pga ON ( pga.cid = p.cid AND p.id = pga.property_id )
						LEFT JOIN property_groups AS pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						LEFT JOIN property_group_types AS pgt ON ( pgt.cid = pg.cid AND pg.property_group_type_id = pgt.id AND pgt.deleted_by IS NULL AND pgt.deleted_on IS NULL )
					WHERE
						pgs.activate_standard_posting = true
						AND p.is_disabled <> 1
						AND p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					ORDER BY
						LOWER( p.property_name ),
						LOWER( pgt.name ),
						LOWER( pg.name )';

		$arrmixProperties = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixProperties ) ) {
			return NULL;
		}

		return $arrmixProperties;
	}

	public static function fetchPmEnabledPropertiesByPropertyPreferenceKeysByCid( $arrstrPropertyPreferenceKeys, $intCid, $objDatabase, $intPsCategoryId = NULL, $arrintReferenceCategoryIds = NULL ) {
		$strSql = 'SELECT
						p.id,pp.key
					FROM
						properties p
						JOIN property_preferences pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.key IN ( \'' . implode( "','", $arrstrPropertyPreferenceKeys ) . '\' ) )
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND pgs.property_id = p.id AND pgs.activate_standard_posting = true )
				WHERE
					p.cid = ' . ( int ) $intCid . '
					AND pp.value IS NOT NULL'
					. \Psi\Eos\Entrata\CPropertyPreferences::createService()->getPsCategoryWhereClauseSql( $intPsCategoryId, $arrintReferenceCategoryIds, 'pp' ) . '
				ORDER BY
					p.property_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchManagerialPropertyByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						p.*
					FROM
						properties p
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						AND is_managerial = 1';

		return parent::fetchProperty( $strSql, $objDatabase );
	}

	public static function fetchManagerialPropertyIdsByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						p.id
					FROM
						properties p
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						AND p.is_managerial = 1';

		return array_keys( rekeyArray( 'id', fetchData( $strSql, $objDatabase ) ) );

	}

	public static function fetchParentChildPropertiesByPropertyPreferenceKeysByPropertyPreferenceValueByCid( $arrstrKey, $strValue, $arrintPropertyIds, $intCid, $objClientDatabase, $intPsCategoryId = NULL, $arrintReferenceCategoryIds = NULL ) {

		$strWhereClauseSql = \Psi\Eos\Entrata\CPropertyPreferences::createService()->getPsCategoryWhereClauseSql( $intPsCategoryId, $arrintReferenceCategoryIds, 'pp' );

		$strSql = 'SELECT
						p.*
					FROM
						properties p
						JOIN property_preferences pp ON ( pp.cid = p.cid AND pp.property_id = p.id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pp.key IN ( \'' . implode( "', '", $arrstrKey ) . '\' )
						AND pp.value = \'' . addslashes( $strValue ) . '\''
						. $strWhereClauseSql . '
						AND ( p.property_id IS NULL
							 OR p.property_id IN (
													SELECT
														p.id
													FROM
														properties p
														JOIN property_preferences pp ON ( pp.property_id = p.id AND pp.cid = p.cid )
													WHERE
														p.cid = ' . ( int ) $intCid . '
														AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
														AND pp.key IN ( \'' . addslashes( implode( "', '", $arrstrKey ) ) . '\' )
														AND pp.value = \'' . addslashes( $strValue ) . '\''
		                                                . $strWhereClauseSql . '
														AND p.property_id IS NULL
													)
							)
					ORDER BY
						p.property_name,
						p.id';

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchParentChildPropertyIdsByPropertyPreferenceKeysByPropertyPreferenceValueByPropertyIdByCid( $arrstrKey, $strValue, $intPropertyId, $intCid, $objClientDatabase, $arrintPropertyTypeIdsToExclude = [ CPropertyType::SUBSIDIZED, CPropertyType::STUDENT, CPropertyType::SETTINGS_TEMPLATE, CPropertyType::TEMPLATE ] ) {

		$strSql = 'WITH required_properties AS (
												SELECT
													p.*
												FROM
													properties p
												WHERE
													p.cid = ' . ( int ) $intCid . '
													AND p.property_type_id NOT IN ( ' . implode( ',', $arrintPropertyTypeIdsToExclude ) . ' )
												)
					SELECT
						p.id,
						CASE
							WHEN p.property_id IS NULL THEN 1
							ELSE 0
						END AS property_type
					FROM
						required_properties p
						JOIN property_preferences pp ON ( pp.cid = p.cid AND pp.property_id = p.id )
						JOIN properties pr ON ( pr.cid = p.cid AND ( pr.id = p.id OR pr.id = p.property_id OR pr.property_id = p.id OR pr.property_id = p.property_id ) )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND pr.id = ' . ( int ) $intPropertyId . '
						AND pp.key IN ( \'' . implode( "', '", $arrstrKey ) . '\' )
						AND pp.value = \'' . addslashes( $strValue ) . '\'';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchParentChildPropertyIdsByPropertyPreferenceKeyByPropertyPreferenceValueByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objClientDatabase, $boolShowDisabledData = false ) {

		$arrintPropertyTypeIds = [ CPropertyType::SETTINGS_TEMPLATE, CPropertyType::TEMPLATE ];

		if( false == valArr( $arrintPropertyGroupIds ) ) {
			return [];
		}

		$strSql = 'SELECT
						p.id
					FROM
						properties p
						JOIN wait_lists wl ON ( wl.cid = p.cid AND ( wl.property_group_id = p.id OR wl.property_group_id = p.property_id ) AND wl.is_active IS TRUE )
						JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[' . implode( ',', $arrintPropertyGroupIds ) . '] ) lp ON lp.property_id = p.id ' . ( ( false == $boolShowDisabledData ) ?  ' AND lp.is_disabled = 0 ' : '' ) . '
						LEFT JOIN
						(
							SELECT
								p.id,
								p.cid
							FROM
								properties p
								JOIN wait_lists wl ON ( wl.cid = p.cid AND ( wl.property_group_id = p.id OR wl.property_group_id = p.property_id ) AND wl.is_active IS TRUE )
							WHERE
								p.cid = ' . ( int ) $intCid . '
								AND p.property_type_id NOT IN ( ' . implode( ',', $arrintPropertyTypeIds ) . ' )
								AND p.property_id IS NULL
						) p_parent ON ( p_parent.cid = p.cid AND p.property_id = p_parent.id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND ( p.property_id IS NULL OR p_parent.id IS NOT NULL )
						AND p.property_type_id NOT IN ( ' . implode( ',', $arrintPropertyTypeIds ) . ' )';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyByCidByPropertyName( $intCid, $strPropertyName, $objClientDatabase ) {
		$strSql = 'SELECT
						p.*
					FROM
						properties p
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.property_name ILIKE \'%' . $strPropertyName . '%\'';

		return parent::fetchProperty( $strSql, $objClientDatabase );
	}

	public static function fetchMultiPropertyAssociatedAccountIdsByAccountIdsByCid( $intCid, $arrintAccountIds, $objDatabase ) {

		if( false == valArr( $arrintAccountIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.account_id,
						count(1)
					FROM
						properties p
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.account_id IN ( ' . implode( ',', $arrintAccountIds ) . ' )
					GROUP BY
						p.account_id
					HAVING
						count(p.*) > 1';

		$arrmixProperties = fetchData( $strSql, $objDatabase );

		$arrintFinalPropertyAssociatedAccountIds = [];
		if( true == valArr( $arrmixProperties ) ) {
			foreach( $arrmixProperties as $arrintPropertyIds ) {
				$arrintFinalPropertyAssociatedAccountIds[$arrintPropertyIds['account_id']] = $arrintPropertyIds['account_id'];
			}
		}

		return $arrintFinalPropertyAssociatedAccountIds;
	}

	public static function fetchPropertyIdsByDocumentIdsByCid( $arrintDocumentIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintDocumentIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						pgd.document_id
					FROM
						properties p
						JOIN property_group_associations pga ON ( p.id = pga.property_id AND pga.cid = p.cid )
						JOIN property_group_documents pgd ON ( pgd.property_group_id = pga.property_group_id AND pgd.cid = pga.cid )
						JOIN property_groups pg ON ( pg.cid = pgd.cid AND pgd.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						pgd.is_published = 1
						AND p.cid = ' . ( int ) $intCid . '
						AND pgd.document_id IN ( ' . implode( ',', $arrintDocumentIds ) . ' )';

		$arrmixProperties = fetchData( $strSql, $objClientDatabase );

		$arrintFinalPropertyIds = [];
		if( true == valArr( $arrmixProperties ) ) {
			foreach( $arrmixProperties as $arrintPropertyIds ) {
				$arrintFinalPropertyIds[] = $arrintPropertyIds['id'];
			}
		}

		return $arrintFinalPropertyIds;
	}

	public static function fetchRelatedPropertyIdsByParentPropertyIdsByCid( $arrintParentPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintParentPropertyIds ) ) {
			return;
		}

		$strSql = 'SELECT
						p.id
					FROM
						properties p
					WHERE
						( p.id IN ( ' . implode( ',', $arrintParentPropertyIds ) . ' ) OR p.property_id IN ( ' . implode( ',', $arrintParentPropertyIds ) . ' ) )
						AND p.cid = ' . ( int ) $intCid;

		$arrmixParentProperties = fetchData( $strSql, $objDatabase );

		$arrintRelatedPropertyIds = [];

		if( true == valArr( $arrmixParentProperties ) ) {
			foreach( $arrmixParentProperties as $arrintPropertyIds ) {
				$arrintRelatedPropertyIds[$arrintPropertyIds['id']] = $arrintPropertyIds['id'];
			}
		}

		return $arrintRelatedPropertyIds;

	}

	public static function fetchActivePropertiesNameByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						id,
						property_name
					FROM
						properties
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_disabled = 0
						AND property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
					ORDER BY
						lower ( property_name )';

		$arrstrPropertiesData = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrstrPropertiesData ) ) {
			return NULL;
		}

		foreach( $arrstrPropertiesData as $arrstrPropertyData ) {
			$arrstrProperties[$arrstrPropertyData['id']] = $arrstrPropertyData['property_name'];
		}

		return $arrstrProperties;
	}

	public static function fetchPropertiesByPsProductIdByCids( $intPsProductId, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id, p.property_name, p.cid
					FROM
						properties p
						JOIN property_products AS pp ON ( pp.property_id = p.id OR pp.property_id IS NULL )
					WHERE
						pp.cid = p.cid
						AND pp.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND pp.ps_product_id = ' . ( int ) $intPsProductId . '
						AND p.is_disabled = 0';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyByIdByFieldNamesByCid( $intPropertyId, $arrstrFieldNames, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						' . implode( ',', $arrstrFieldNames ) . '
					FROM
						properties p
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id = ' . ( int ) $intPropertyId . '';

		$arrmixPropertyData = fetchData( $strSql, $objClientDatabase );

		return $arrmixPropertyData[0];
	}

	public static function fetchCustomAllCustomPropertiesByIds( $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						properties
					WHERE
						id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					ORDER BY
						property_name ASC';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchActivePropertiesUnitsCountByPsLeadId( $intPsleadId, $objDatabase ) {

		$strSql = 'SELECT
						count( p.id ) as total_active_properties,
						SUM( p.number_of_units ) as total_active_units
					FROM
						properties p
						LEFT JOIN clients mc on ( p.cid = mc.id )
						LEFT JOIN ps_leads pl ON ( mc.ps_lead_id = pl.id )
						JOIN
							(
								SELECT
									cp.property_id,
									cp.commission_bucket_id,
									cp.cid,
									DATE_TRUNC ( \'month\', cp.close_date ) AS close_month,
									rank ( ) OVER ( PARTITION BY cp.property_id ORDER BY cp.close_date ASC, cp.commission_bucket_id ASC, cp.id )
								FROM
									contract_properties cp
									JOIN contracts c ON ( cp.contract_id = c.id AND cp.cid = c.cid )
									LEFT JOIN contract_termination_requests ctr ON ( ctr.cid = cp.cid AND ctr.id = cp.contract_termination_request_id )
								WHERE
									cp.commission_bucket_id <> ' . CCommissionBucket::RENEWAL . '
									AND c.ps_lead_id = ' . ( int ) $intPsleadId . '
									AND c.contract_status_type_id IN ( ' . implode( ',', CContractStatusType::$c_arrintCompletedContractStatusTypeIds ) . ' )
									AND COALESCE ( ctr.contract_termination_reason_id, 0 ) <> ' . CContractTerminationReason::DUPLICATE_CONTRACT . '
							) AS cp_active ON ( cp_active.property_id = p.id AND cp_active.cid = p.cid AND rank = 1 )
					WHERE
						p.is_disabled <> 1 AND
						p.is_test <> 1 AND
						pl.id = ' . ( int ) $intPsleadId . ' AND
						p.termination_date IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPublishedPropertiesDetailsByWebsiteIdByHasAvailabilityByCid( $intWebsiteId, $intCid, $objClientDatabase, $intHasAvailability = NULL ) {
		$strAvailabilityJoinCondition = ( $intHasAvailability == 1 ) ? ' JOIN unit_spaces us ON ( us.cid = p.cid AND us.property_id = p.id AND us.deleted_on IS NULL AND us.is_available ) ' : '';

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.marketing_name
					FROM
						properties p
						JOIN website_properties wp ON ( wp.cid = p.cid AND wp.property_id = p.id AND wp.website_id = ' . ( int ) $intWebsiteId . ' AND wp.hide_on_prospect_portal <> 1 )
						' . $strAvailabilityJoinCondition . '
					WHERE
						p.is_disabled <> 1
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
						lower ( p.property_name ) ASC';

		$arrmixProperties        = fetchData( $strSql, $objClientDatabase );
		$arrstrPropertiesDetails = [];

		foreach( $arrmixProperties as $arrstrProperties ) {
			$arrstrPropertiesDetails[$arrstrProperties['id']] = ( true == valStr( $arrstrProperties['marketing_name'] ) ) ? $arrstrProperties['marketing_name'] : $arrstrProperties['property_name'];
		}

		return $arrstrPropertiesDetails;
	}

	public static function fetchPropertiesByIdsByCids( $arrintPropertyIds, $arrintCids, $objDatabase, $boolIsReturnKeyedArray = true, $strOrderBy = NULL ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM properties WHERE id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' ) AND cid IN ( ' . implode( ', ', $arrintCids ) . ' ) ';

		if( false == is_null( $strOrderBy ) && true == valStr( $strOrderBy ) ) {
			$strSql .= ' ORDER BY ' . $strOrderBy;
		}

		return self::fetchProperties( $strSql, $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchPaginatedIntegratedPropertiesByIntegrationDatabaseIdOrIntegrationDatabaseIdNullByCid( $intIntegrationDatabaseId, $intCid, $objPagination, $objClientDatabase ) {
		$strSql = 'SELECT
						 p.id as property_id,p.property_name,p.remote_primary_key,p.lookup_code,pid.created_on as integrated_on,pid.integration_database_id as integration_database_id, p.is_disabled, p.disabled_on
					FROM
						properties p
						LEFT JOIN property_integration_databases pid ON p.id = pid.property_id AND p.cid = pid.cid
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.remote_primary_key IS NOT NULL
						AND ( pid.integration_database_id = ' . ( int ) $intIntegrationDatabaseId . ' OR pid.integration_database_id IS NULL )
						 order by p.property_name OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();

		return fetchData( $strSql, $objClientDatabase );

	}

	public static function fetchPaginatedIntegratedPropertiesCountByIntegrationDatabaseIdOrIntegrationDatabaseIdNullByCid( $intIntegrationDatabaseId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						count(*)
					FROM
						properties p
						LEFT JOIN property_integration_databases pid ON p.id = pid.property_id AND p.cid = pid.cid
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.remote_primary_key IS NOT NULL
						AND ( pid.integration_database_id = ' . ( int ) $intIntegrationDatabaseId . ' OR pid.integration_database_id IS NULL )';

		$arrintCount = fetchData( $strSql, $objClientDatabase );

		return ( true == isset ( $arrintCount[0]['count'] ) ) ? ( int ) $arrintCount[0]['count'] : 0;

	}

	public static function fetchPropertyIdsByPropertyPreferenceKeyByPropertyPreferenceValueByPropertyTypeIdsByPropertyIdsByCid( $strKey, $strValue, $arrintExcludedPropertyTypeIds, $arrintPropertyIds = NULL, $intCid, $objDatabase, $arrintPropertyTypeIds = NULL, $boolIsDisabled = false ) {

		$strWhereClause = '1 = 1';
		if( true == valArr( $arrintPropertyTypeIds ) || true == valArr( $arrintExcludedPropertyTypeIds ) ) {
			$strWhereClause .= ' AND p.property_type_id ';
			$strWhereClause .= ( true == valArr( $arrintPropertyTypeIds ) ) ? ' IN ( ' . implode( ',', $arrintPropertyTypeIds ) . ' )' : 'NOT IN ( ' . implode( ',', $arrintExcludedPropertyTypeIds ) . ' )';
		}

		$strJoinCLause = '';
		if( true == valStr( $strKey ) ) {
			$strJoinCLause  .= 'JOIN property_preferences pp ON ( pp.cid = p.cid AND pp.property_id = p.id ) ';
			$strWhereClause .= 'AND pp.key = \'' . addslashes( $strKey ) . '\' AND pp.value = \'' . addslashes( $strValue ) . '\' ';
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.cid
					FROM
						properties p '
		          . $strJoinCLause . '
					WHERE ' .
		          $strWhereClause .
		          ( ( true == $boolIsDisabled ) ? ' AND p.is_disabled = 0 ' : '' ) .
		          ( ( 0 < $intCid ) ? ' AND p.cid = ' . ( int ) $intCid : '' ) .
		          ( ( true == valArr( $arrintPropertyIds ) ) ? ' AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )' : '' ) . ' ORDER BY p.property_name ASC ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActivePropertiesByIdsByCids( $arrintPropertyIds, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM properties WHERE is_disabled = 0 AND id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' ) AND cid IN ( ' . implode( ', ', $arrintCids ) . ' ) ';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesWithAddressByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $intAddressTypeId = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.*,
						lower ( pa.city ) AS city,
						lower ( pa.street_line1 ) AS street_line1,
						pa.state_code,
						pa.postal_code
					FROM
						properties p
						JOIN property_addresses pa ON ( p.id = pa.property_id AND p.cid = pa.cid AND pa.is_alternate = false )
					WHERE
						p.id in ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						AND p.cid = ' . ( int ) $intCid;

		if( false == is_null( $intAddressTypeId ) ) {
			$strSql .= ' AND pa.address_type_id = ' . ( int ) $intAddressTypeId;
		}

		$strSql .= ' ORDER BY
						p.property_name';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objDatabase, $boolConditionForPropertyType = false, $boolShowDisabledData = false ) {
		if( false == valArr( $arrintPropertyGroupIds ) ) {
			return NULL;
		}

		$strWhereCondition = ( true == $boolConditionForPropertyType ) ? ' p.property_type_id NOT IN (' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ') AND' : '';

		$strSql = 'SELECT
						DISTINCT p.*,
						pg2.id as property_group_id
					FROM
						property_groups pg
						JOIN property_group_associations pga ON pga.cid = pg.cid AND pga.property_group_id = pg.id
						JOIN properties p ON p.cid = pga.cid AND p.id = pga.property_id
						JOIN property_group_types pgt ON pgt.cid = p.cid AND pgt.system_code = \'' . CPropertyGroupType::PROPERTY_LIST_SYSTEM_CODE . '\' AND pgt.deleted_by IS NULL AND pgt.deleted_on IS NULL
						JOIN property_groups pg2 ON pg2.cid = p.cid AND pg2.property_group_type_id = pgt.id
						JOIN property_group_associations pga2 ON pga2.cid = pg2.cid AND pga2.property_group_id = pg2.id AND pga2.property_id = p.id
					WHERE
						' . $strWhereCondition . '
						pg.id in ( ' . implode( ',', $arrintPropertyGroupIds ) . ' )
						' . ( ( false == $boolShowDisabledData ) ? ' AND p.is_disabled = 0 ' : '' ) . '
						AND p.is_managerial <> 1
						AND pg.cid = ' . ( int ) $intCid . '
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL';

		$strSql .= ' ORDER BY
						p.property_name';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPmEnabledPropertyCountByCid( $intCompanyUserId, $boolIsAdministrator, $intCid, $objDatabase ) {
		if( false == isset( $intCid ) ) {
			return NULL;
		}

		$strQuery = ' ';
		if( false == $boolIsAdministrator ) {
			$strQuery = 'JOIN view_company_user_properties cup ON ( cup.company_user_id = ' . ( int ) $intCompanyUserId . ' AND cup.property_id = p.id AND cup.cid = p.cid ) ';
		}

		$strSql = 'SELECT
						count( DISTINCT( p.id ) ) AS property_count
					FROM
						properties p ' . $strQuery . '
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND pgs.property_id = p.id AND pgs.activate_standard_posting = true )
						JOIN property_products pp ON ( pp.cid = p.cid AND pp.property_id = p.id OR pp.property_id IS NULL )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND pp.ps_product_id = ' . CPsProduct::ENTRATA . '
						AND p.is_disabled = 0';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomResidentInsurePropertiesByKeyByCids( $strKey, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						p.*
					FROM
						properties p
					WHERE
						id NOT IN (
									SELECT
										pp.property_id
									FROM
										property_preferences pp
									WHERE
										pp.KEY = \'' . addslashes( $strKey ) . '\'
										AND pp.cid IN ( ' . implode( ',', $arrintCids ) . ' )
								)
						AND p.cid IN ( ' . implode( ',', $arrintCids ) . ' ) ';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCustomResidentPortalPropertiesByKeyByCids( $strMoveInEmailSetting, $strRenterInsuranceSetting, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
							p.*
						FROM
							properties p
						WHERE
							p.id NOT IN (
										SELECT
											pp.property_id
										FROM
											property_preferences pp
										WHERE
											pp.property_id IN (
																SELECT
																	pp1.property_id
																FROM
																	property_preferences pp1
																WHERE
																	pp1.key = \'' . addslashes( $strRenterInsuranceSetting ) . '\'
																	AND pp1.cid IN ( ' . implode( ',', $arrintCids ) . ' )
																)
											AND pp.key = \'' . addslashes( $strMoveInEmailSetting ) . '\'
											OR ( pp.key = \'' . addslashes( $strMoveInEmailSetting ) . '\' OR pp.key = \'' . addslashes( $strRenterInsuranceSetting ) . '\' )
											AND pp.cid IN ( ' . implode( ',', $arrintCids ) . ' )
									)
							AND p.cid IN ( ' . implode( ',', $arrintCids ) . ' ) ';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchEntrataPropertiesWithFieldNamesByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $arrstrFieldNames, $intCid, $objDatabase, $boolReturnArray = false ) {
		if( false == valArr( $arrintPropertyGroupIds ) || false == valArr( $arrstrFieldNames ) ) {
			return NULL;
		}

		$strSql = 'SELECT ' . implode( ',', $arrstrFieldNames ) . '
					FROM
						properties p
						JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $arrintPropertyGroupIds ) . ' ], array[ ' . CPsProduct::ENTRATA . ' ] ) pg ON pg.property_id = p.id
					WHERE
						p.cid = ' . ( int ) $intCid . '
					ORDER BY
						lower ( p.property_name );';

		if( true == $boolReturnArray ) {
			return fetchData( $strSql, $objDatabase );

		}

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchNumberOfUnitsByPropertyIdsByCids( $arrintPropertyIds, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						SUM( p.number_of_units ) AS number_of_units,
						p.cid
					FROM
						properties p
					WHERE
						p.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					GROUP BY
						p.cid';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesFieldsByIdsByEmailAddressTypeIdsByCid( $arrintPropertyIds, $arrintEmailAddressTypeIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						p.id,
						p.property_name,
						pea.email_address,
						posc.name_full as property_manager_name
					FROM
						properties p
						JOIN property_email_addresses pea ON ( p.cid = pea.cid AND p.id = pea.property_id )
						JOIN property_on_site_contacts posc ON ( p.cid = posc.cid AND p.id = posc.property_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND pea.email_address_type_id IN ( ' . implode( ', ', $arrintEmailAddressTypeIds ) . ' )';

		$arrmixData      = fetchData( $strSql, $objClientDatabase );
		$arrmixFinalData = [];

		foreach( $arrmixData as $arrmixChunk ) {
			$arrmixFinalData[$arrmixChunk['id']] = $arrmixChunk;
		}

		return $arrmixFinalData;
	}

	public static function fetchSimplePropertiesByCompanyUserIdByCompanyTransmissionVendorIdByCid( $intCompanyUserId, $intCompanyTransmissionVendorId, $intCid, $objDatabase ) {
		$strSql = self::buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId );

		$strSql = 'SELECT
						p.id,
						p.property_name
					FROM
						( ' . $strSql . ' ) as p
						JOIN property_transmission_vendors ptv ON ( ptv.cid = p.cid AND ptv.property_id = p.id )
						JOIN company_transmission_vendors ctv ON ( ctv.cid = ptv.cid AND ctv.id = ptv.company_transmission_vendor_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
						AND ctv.id = ' . ( int ) $intCompanyTransmissionVendorId . '
					ORDER BY
						p.property_name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPmEnabledPropertiesByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objClientDatabase, $boolIsDisabled = true, $boolIsHistoricalAccessMode = false ) {

		$strCondition = $strJoinCondition = '';

		if( false == $boolIsDisabled ) {
			$strCondition = ' AND p.is_disabled = 0';
		}

		if( true == $boolIsHistoricalAccessMode && true == $boolIsDisabled ) {
			$strJoinCondition = ' JOIN property_products pp ON ( p.cid = pp.cid AND p.id = pp.property_id AND pp.ps_product_id = ' . \CPsProduct::HISTORICAL_ACCESS . ' )';
			$strCondition = ' AND p.is_disabled = 1';
		}

		$strSql = 'SELECT
						p.id,
						p.property_name
					FROM
						company_users cu
						JOIN properties p ON ( p.cid = cu.cid AND cu.is_administrator = 1 )
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND pgs.property_id = p.id AND pgs.activate_standard_posting = true )
						' . $strJoinCondition . '
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						AND cu.id = ' . ( int ) $intCompanyUserId .
		          $strCondition . '

					UNION

					SELECT
						p.id,
						p.property_name
					FROM
						company_users cu
						JOIN company_user_property_groups cupg ON ( cupg.cid = cu.cid AND cupg.company_user_id = cu.id AND cu.is_administrator = 0 )
						JOIN property_group_associations pga ON ( cupg.cid = pga.cid AND cupg.property_group_id = pga.property_group_id )
						JOIN property_groups pg ON ( pg.cid = cu.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN properties p ON ( p.cid = cu.cid AND p.id = pga.property_id )
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND pgs.property_id = p.id AND pgs.activate_standard_posting = true )
						' . $strJoinCondition . '
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						AND cu.id = ' . ( int ) $intCompanyUserId .
		          $strCondition;

		$arrmixData = rekeyArray( 'id', fetchData( $strSql, $objClientDatabase ) );

		return $arrmixData;
	}

	public static function fetchPropertyIdsByPropertyPreferenceKeyByCid( $strPropertyPreferenceKey, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						p.id
					FROM
						properties p
						JOIN property_preferences pp ON ( p.cid = pp.cid AND pp.property_id = p.id AND pp.key = \'' . $strPropertyPreferenceKey . '\' )
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid AND pgs.property_id = p.id AND pgs.activate_standard_posting = true )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND pp.value IS NOT NULL';

		$arrmixProperties = ( array ) fetchData( $strSql, $objDatabase );

		return array_keys( rekeyArray( 'id', $arrmixProperties ) );
	}

	public static function fetchPropertyIdsByPropertyPreferenceKeyByCids( $strPropertyPreferenceKey, $arrintCids, $objDatabase ) {

		if( false == valStr( $strPropertyPreferenceKey ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT p.id
                            FROM properties p
                                JOIN property_preferences pp ON (p.id = pp.property_id AND p.cid = pp.cid)
							WHERE
								p.is_disabled <> 1
    		                    AND p.cid IN ( ' . implode( ',', $arrintCids ) . ' )
								AND pp.key = \'' . $strPropertyPreferenceKey . '\'
								AND pp.value = \'1\' ';

		return rekeyArray( 'id', fetchData( $strSql, $objDatabase ) );
	}

	public static function fetchPropertyCountByIntegrationDatabaseIdByRemotePrimaryKeyByCid( $intIntegrationDatabaseId, $strRemotePrimaryKey, $intCid, $objClientDatabase ) {
		$strFrom = 'properties p,
						property_integration_databases pid,
						integration_databases id';

		$strWhere = ' WHERE
						p.id = pid.property_id
						AND p.cid = pid.cid
						AND pid.integration_database_id = id.id
						AND pid.cid = id.cid
						AND lower ( p.remote_primary_key ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( trim( $strRemotePrimaryKey ) ) ) . '\'
						AND id.id = ' . ( int ) $intIntegrationDatabaseId . '
						AND id.cid = ' . ( int ) $intCid;

		return self::fetchRowCount( $strWhere, $strFrom, $objClientDatabase );
	}

	public static function fetchPropertyContactInfoByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.*,
						pea.email_address AS primary_email_address,
						ppn.phone_number AS primary_phone_number,
						ppn2.phone_number AS office_fax_number,
						pa.street_line1,
						pa.street_line2,
						pa.street_line3,
						pa.city,
						pa.state_code,
						pa.postal_code,
						pa.country_code,
						pa.longitude,
						pa.latitude,
						posc.name_full AS property_manager_name,
						pd.corporate_website_uri,
						pd.community_website_uri,
						pd.pet_policy,
						pd.parking_policy
					FROM properties p
						LEFT JOIN property_email_addresses pea ON ( pea.property_id = p.id AND p.cid = pea.cid AND email_address_type_id = ' . CEmailAddressType::PRIMARY . ' )
						LEFT JOIN property_phone_numbers ppn ON ( ppn.property_id = p.id AND p.cid = ppn.cid AND ppn.phone_number_type_id = ' . CPhoneNumberType::OFFICE . ' )
						LEFT JOIN property_phone_numbers ppn2 ON ( ppn2.property_id = p.id AND p.cid = ppn2.cid AND ppn2.phone_number_type_id = ' . CPhoneNumberType::FAX . ' )
						LEFT JOIN property_addresses pa ON ( pa.property_id = p.id AND p.cid = pa.cid AND pa.address_type_id = ' . CAddressType::PRIMARY . ' AND pa.is_alternate = false )
						LEFT JOIN property_on_site_contacts posc ON ( posc.property_id = p.id AND posc.cid = p.cid AND on_site_contact_type_id = ' . CContactType::MANAGER . ' )
						LEFT JOIN property_details pd ON ( pd.property_id = p.id AND pd.cid = p.cid )
					WHERE
						p.id = ' . ( int ) $intPropertyId . '
						AND p.cid = ' . ( int ) $intCid;

		return self::fetchProperty( $strSql, $objDatabase );

	}

	public static function fetchSimplePropertyContactInfoByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		// field name should be same as merge field name
		$strSql = 'SELECT
						p.id as property_id,
						p.property_name,
						pea.email_address as property_email_address,
						ppn.phone_number as property_phone_number,
						ppn2.phone_number as resident_office_phone_number,
						ppn3.phone_number as office_fax_number,
						pa.address_type_id,
						pa.street_line1,
						pa.street_line2,
						pa.street_line3,
						pa.city,
						pa.state_code,
						pa.postal_code,
						posc.name_full property_manager_name,
						pd.corporate_website_uri
					FROM properties p
						LEFT JOIN property_email_addresses pea ON ( pea.property_id = p.id AND p.cid = pea.cid AND email_address_type_id = ' . CEmailAddressType::PRIMARY . ' )
						LEFT JOIN property_phone_numbers ppn ON ( ppn.property_id = p.id AND p.cid = ppn.cid AND ppn.phone_number_type_id = ' . CPhoneNumberType::OFFICE . ' )
						LEFT JOIN property_phone_numbers ppn2 ON ( ppn2.property_id = p.id AND p.cid = ppn2.cid AND ppn2.phone_number_type_id = ' . CPhoneNumberType::RESIDENT_OFFICE . ' )
						LEFT JOIN property_phone_numbers ppn3 ON ( ppn3.property_id = p.id AND p.cid = ppn3.cid AND ppn3.phone_number_type_id = ' . CPhoneNumberType::FAX . ' )
						LEFT JOIN property_addresses pa ON ( pa.property_id = p.id AND p.cid = pa.cid AND ( pa.address_type_id = ' . CAddressType::PRIMARY . ' OR pa.address_type_id = ' . CAddressType::MAILING . ' ) AND pa.is_alternate = false )
						LEFT JOIN property_on_site_contacts posc ON ( posc.property_id = p.id AND posc.cid = p.cid AND on_site_contact_type_id = ' . CContactType::MANAGER . ' )
						LEFT JOIN property_details pd ON ( pd.property_id = p.id AND pd.cid = p.cid )
					WHERE p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND p.cid = ' . ( int ) $intCid . ' ';

		$arrmixProperties = ( array ) fetchData( $strSql, $objDatabase );

		return $arrmixProperties;
	}

	public static function fetchSimpleEnabledPropertyIdsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						p.id AS property_id
					FROM
						properties p
						JOIN view_company_user_properties cup ON ( cup.cid = p.cid AND cup.property_id = p.id )
					WHERE
						p.is_disabled = 0
						AND cup.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND cup.cid = ' . ( int ) $intCid . '
					ORDER BY
						lower ( p.property_name ) ASC,
						order_num';

		$arrmixProperties = fetchData( $strSql, $objDatabase );

		$arrintPropertyIds = [];

		if( true == valArr( $arrmixProperties ) ) {
			foreach( $arrmixProperties as $arrintProperty ) {
				$arrintPropertyIds[$arrintProperty['property_id']] = $arrintProperty['property_id'];
			}
		}

		return $arrintPropertyIds;
	}

	public static function fetchPropertyDetailsByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						id,
						property_name
					FROM
						properties
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_disabled = 0
					ORDER BY
						lower ( property_name )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEnabledPropertiesCountByCids( $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						mc.id as cid,
						count( p.id ) as properties_count
					FROM
						properties as p
						JOIN clients mc ON ( p.cid = mc.id )
					WHERE
						mc.id = ANY( array[ ' . implode( ', ', $arrintCids ) . ' ] )
						AND p.is_disabled = 0
					GROUP BY
						mc.id';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchCachedChildPropertyIdsByCidByWebsiteId( $intCid, $intWebsiteId, $objDatabase ) {
		$arrintChildPropertyIds = CCache::fetchObject( 'Child_Properties_' . ( int ) $intCid . '_' . ( int ) $intWebsiteId );

		if( false === $arrintChildPropertyIds || false == valArr( $arrintChildPropertyIds ) ) {
			$arrintChildPropertyIds = self::fetchResidentPortalRelatedPropertyIdsByWebsiteIdByCid( $intWebsiteId, $intCid, $objDatabase );
			CCache::storeObject( 'Child_Properties_' . ( int ) $intCid . '_' . ( int ) $intWebsiteId, $arrintChildPropertyIds, $intSpecificLifetime = 900, $arrstrTags = [] );
		}

		return $arrintChildPropertyIds;
	}

	public static function fetchCachedChildPropertyIdsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		sort( $arrintPropertyIds );
		$arrintChildPropertyIds = CCache::fetchObject( 'Child_Properties_By_Properties_' . ( int ) $intCid . '_' . implode( '_', $arrintPropertyIds ) );

		if( false == valArr( $arrintChildPropertyIds ) ) {
			$arrintChildPropertyIds = ( array ) self::fetchChildPropertyIdsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase );
			CCache::storeObject( 'Child_Properties_By_Properties_' . ( int ) $intCid . '_' . implode( '_', $arrintPropertyIds ), $arrintChildPropertyIds, $intSpecificLifetime = 900, $arrstrTags = [] );
		}

		return $arrintChildPropertyIds;
	}

	public static function fetchCustomPropertyDetailsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.cid,
						mc.company_name,
						mc.ps_lead_id
					FROM
						properties p
						JOIN clients mc ON (p.cid = mc.id)
					WHERE
						p.id IN (' . implode( ',', $arrintPropertyIds ) . ')
					ORDER BY
						mc.company_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertiesDataByPropertyIdsByOccupancyTypeIdByCid( $arrintPropertyIds, $intOccupancyTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name
					FROM
						properties p
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND ' . ( int ) $intOccupancyTypeId . ' = ANY( p.occupancy_type_ids ) 
						AND p.id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						ORDER BY p.property_name';

		$arrmixPropertiesData = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixPropertiesData ) ) {
			return NULL;
		}

		$arrmixFormattedPropertiesData = [];

		foreach( $arrmixPropertiesData as $arrmixPropertyData ) {
			$arrmixFormattedPropertiesData[$arrmixPropertyData['id']] = $arrmixPropertyData['property_name'];
		}

		return $arrmixFormattedPropertiesData;
	}

	public static function fetchPropertiesNotAssociatedToSettingsTemplateBySettingTemplateTypeIdByCid( $intSettingsTemplateTypeId, $intCid, $objDatabase, $boolReturnArray = false, $objPagination = NULL ) {

		$strSql = 'SELECT
						DISTINCT ON ( lower( p.property_name ), p.id ) p.*,
						util_get_system_translated( \'name\', pt.name, pt.details ) AS property_type_name
					FROM
						properties p
						LEFT JOIN property_types pt ON ( pt.id = p.property_type_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
						AND p.property_type_id != ' . CPropertyType::SETTINGS_TEMPLATE . '
						AND p.property_type_id != ' . CPropertyType::TEMPLATE . '
						AND p.id NOT IN (
											SELECT
												stp.property_id
											FROM
												settings_template_properties stp
												JOIN settings_templates st ON ( stp.cid = st.cid AND stp.settings_template_id = st.id AND st.settings_template_type_id = ' . ( int ) $intSettingsTemplateTypeId . ' )
											WHERE
												stp.cid = ' . ( int ) $intCid . '
						)
					ORDER BY
						lower( p.property_name ), p.id';

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		if( true == $boolReturnArray ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return parent::fetchProperties( $strSql, $objDatabase );
		}
	}

	public static function fetchPropertyIdsNotAssociatedToSettingsTemplateBySettingTemplateTypeIdByCid( $intSettingsTemplateTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						array_to_string( array_agg( DISTINCT p.id ), \',\' ) as property_ids
					FROM
						properties p
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
						AND p.property_type_id != ' . CPropertyType::SETTINGS_TEMPLATE . '
						AND p.property_type_id != ' . CPropertyType::TEMPLATE . '
						AND p.id NOT IN (
											SELECT
												stp.property_id
											FROM
												settings_template_properties stp
												JOIN settings_templates st ON ( stp.cid = st.cid AND stp.settings_template_id = st.id AND st.settings_template_type_id = ' . ( int ) $intSettingsTemplateTypeId . ' )
											WHERE
												stp.cid = ' . ( int ) $intCid . '
						)';

		$arrmixData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixData ) && true == valArr( array_filter( $arrmixData[0] ) ) ) {
			return explode( ',', $arrmixData[0]['property_ids'] );
		} else {
			return [];
		}
	}

	public static function fetchPaginatedPropertiesNotAssociatedToSettingsTemplateBySettingTemplateTypeIdByCid( $intSettingsTemplateTypeId, $intCid, $objPagination, $objDatabase, $strPropertyNameSearch = NULL ) {
		$strSql = 'SELECT
						DISTINCT ON ( lower( p.property_name ), p.id ) p.*,
						util_get_system_translated( \'name\', pt.name, pt.details ) AS property_type_name
					FROM
						properties p
						LEFT JOIN property_types pt ON ( pt.id = p.property_type_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
						AND p.property_type_id != ' . CPropertyType::SETTINGS_TEMPLATE . '
						AND p.property_type_id != ' . CPropertyType::TEMPLATE . '
						AND p.id NOT IN (
											SELECT
												stp.property_id
											FROM
												settings_template_properties stp
												JOIN settings_templates st ON ( stp.cid = st.cid AND stp.settings_template_id = st.id AND st.settings_template_type_id = ' . ( int ) $intSettingsTemplateTypeId . ' )
											WHERE
												stp.cid = ' . ( int ) $intCid . '
						)';

		if( true == valStr( $strPropertyNameSearch ) ) {
			$strSql .= ' AND p.property_name Ilike \'%' . addslashes( $strPropertyNameSearch ) . '%\'';
		}

		$strSql .= '
					ORDER BY
						lower( p.property_name ), p.id';

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return parent::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchTotalPaginatedPropertiesNotAssociatedToSettingsTemplateBySettingTemplateTypeIdCountByCid( $intSettingsTemplateTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						count ( p.id )
					FROM
						properties p
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
						AND p.property_type_id != ' . CPropertyType::SETTINGS_TEMPLATE . '
						AND p.property_type_id != ' . CPropertyType::TEMPLATE . '
						AND p.id NOT IN (
											SELECT
												stp.property_id
											FROM
												settings_template_properties stp
												JOIN settings_templates st ON ( stp.cid = st.cid AND stp.settings_template_id = st.id AND st.settings_template_type_id = ' . ( int ) $intSettingsTemplateTypeId . ' )
											WHERE
												stp.cid = ' . ( int ) $intCid . '
						)';

		$arrmixData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixData ) && true == isset ( $arrmixData[0]['count'] ) ) {
			return $arrmixData[0]['count'];

		} else {
			return 0;
		}
	}

	public static function fetchPropertiesBySettingsTemplateIdByCid( $intSettingsTemplateId, $intCid, $objDatabase, $boolReturnArray = false ) {
		$strSql = 'SELECT
						p.*,
						util_get_system_translated( \'name\', pt.name, pt.details ) AS property_type_name
					FROM
						properties p
						JOIN settings_template_properties stp ON( stp.property_id= p.id AND stp.cid = p.cid )
						JOIN settings_templates st ON(st.id= stp.settings_template_id AND st.cid=stp.cid )
						LEFT JOIN property_types pt ON ( pt.id = p.property_type_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
						AND st.id = ' . ( int ) $intSettingsTemplateId . '
					ORDER BY
						lower( property_name )';

		if( true == $boolReturnArray ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return self::fetchProperties( $strSql, $objDatabase );
		}
	}

	public static function fetchPropertiesByLedgerFilterIdByPropertyIdsByCid( $intLedgerFilterId, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.*
					FROM
						properties p
						JOIN property_ledger_filters plf ON( plf.property_id = p.id AND plf.cid = p.cid )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND plf.ledger_filter_id = ' . ( int ) $intLedgerFilterId . '
						AND p.id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND plf.is_published = 1';

		return parent::fetchProperties( $strSql, $objDatabase );

	}

	public static function fetchPropertyIdsByArCodeIdByByCid( $intArCodeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						p.id
					FROM
						properties p
						JOIN property_ledger_filters plf ON( plf.property_id= p.id AND plf.cid = p.cid )
						JOIN ar_codes ac ON( ac.ledger_filter_id = plf.ledger_filter_id AND ac.cid = plf.cid )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND ac.id = ' . ( int ) $intArCodeId . '
						AND plf.is_published = 1
						AND p.is_disabled = 0';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchCustomSimplePropertiesByPropertyIds( $arrintPropertyIds, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return;
		}

		$strSql = 'SELECT
					p.id as property_id,
					p.number_of_units,
					mc.company_name,
					mc.rwx_domain,
					c.contract_datetime
				FROM
					properties p
					JOIN clients mc ON ( mc.id = p.cid )
					LEFT JOIN contracts c ON ( c.cid = p.cid AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . ' )
				WHERE
					p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleScheduledChargePostingPropertiesByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.lookup_code,
						COALESCE( art.transaction_amount, 0 ) AS transaction_amount
					FROM
						properties p
						LEFT JOIN scheduled_charges_fetch_ar_transactions( ' . ( int ) $intCid . ', ' . CPostScheduledChargesCriteria::DATA_RETURN_TYPE_PROPERTIES . ' ) art ON ( p.cid = art.cid AND p.id = art.property_id )
					WHERE
						p.id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
						lower( p.property_name )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByPropertyTypeIdByCids( $intPropertyTypeId, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM
						properties
					WHERE
						property_type_id = ' . ( int ) $intPropertyTypeId . '
						AND cid IN (' . implode( ',', $arrintCids ) . ' )
						AND is_disabled <> 1';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator = false, $boolShowEnabledOnly = true, $boolIsPmEnabledOnly = false, $boolIsExcludeTemplateProperties = false, $arrintPropertyIds = [], $boolCheckIsStudentProperty = false, $boolShowDisabledData = false ) {

		$strDisabledCondition = '';
		if( true == $boolShowDisabledData ) {
			$strDisabledCondition = ' AND p.is_disabled = 1 ';
		} elseif( false == $boolShowDisabledData && true == $boolShowEnabledOnly ) {
			$strDisabledCondition = ' AND p.is_disabled = 0 ';
		}

		$strDisabledCondition .= ( true == $boolIsPmEnabledOnly ) ? ' AND pgs.activate_standard_posting = true ' : ' ';
		$arrintPropertyTypes  = ( false == $boolIsExcludeTemplateProperties ) ? [ CPropertyType::SETTINGS_TEMPLATE ] : [ CPropertyType::SETTINGS_TEMPLATE, CPropertyType:: TEMPLATE ];
		$strDisabledCondition .= ( true == valArr( $arrintPropertyIds ) ) ? ' AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )' : '';
		$strJoinSql           = ( true == $boolCheckIsStudentProperty ? ' LEFT JOIN property_preferences pp ON ( p.cid = pp.cid AND p.id = pp.property_id AND pp.key = \'ENABLE_SEMESTER_SELECTION\') ' : '' );
		$strJoinSql           .= ( true == $boolShowDisabledData ? ' JOIN property_products pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.ps_product_id = ' . CPsProduct::HISTORICAL_ACCESS . ') ' : '' );
		$strSelectSql         = ( true == $boolCheckIsStudentProperty ? ' , CASE WHEN  ' . COccupancyType::STUDENT . ' = ANY( p.occupancy_type_ids ) AND pp.id IS NOT NULL THEN 1 ELSE 0 END as is_student_property ' : '' );

		$strSql = 'SELECT
						p.*,
						cu.id as company_user_id,
						pgs.is_ap_migration_mode,
						to_char( pgs.ap_post_month, \'MM/YYYY\') AS ap_post_month
						' . $strSelectSql . '
					FROM
						company_users cu
						JOIN properties p ON ( p.cid = cu.cid AND p.property_type_id NOT IN ( ' . implode( ',', $arrintPropertyTypes ) . ' ) )
						JOIN property_gl_settings pgs ON( p.cid = pgs.cid AND p.id = pgs.property_id )
						' . $strJoinSql . '
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						AND cu.id = ' . ( int ) $intCompanyUserId .
		                $strDisabledCondition .
		                ' AND 
		                  CASE
		                    WHEN cu.company_user_type_id IN ( ' . CCompanyUserType::ENTRATA . ' ) THEN cu.is_administrator = 1 ELSE TRUE
		                  END';

		if( true == $boolIsAdministrator ) {
			return $strSql;
		}

		$strSql = $strSql . '

					UNION

					SELECT
						p.*,
						cu.id as company_user_id,
						pgs.is_ap_migration_mode,
						to_char( pgs.ap_post_month, \'MM/YYYY\') AS ap_post_month
						' . $strSelectSql . '
					FROM
						company_users cu
						JOIN company_user_property_groups cupg ON ( cupg.cid = cu.cid AND cupg.company_user_id = cu.id AND cu.is_administrator = 0 )
						JOIN property_group_associations pga ON ( cupg.cid = pga.cid AND cupg.property_group_id = pga.property_group_id )
						JOIN property_groups pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN properties p ON ( p.cid = cu.cid AND p.id = pga.property_id AND p.property_type_id NOT IN ( ' . implode( ',', $arrintPropertyTypes ) . ' ) )
						JOIN property_gl_settings pgs ON( p.cid = pgs.cid AND p.id = pgs.property_id )
						' . $strJoinSql . '
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						AND cu.id = ' . ( int ) $intCompanyUserId .
		          $strDisabledCondition;

		return $strSql;
	}

	public static function fetchUnAssociatedPropertiesByWebsiteIdByCompanyUserIdByCid( $intWebsiteId, $intCompanyUserId, $intCid, $objDatabase, $boolIsParaentOnly = true, $boolIsAdministrator = false ) {
		$strSql = self::buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator );

		$strCondition = ( true == $boolIsParaentOnly ) ? ' AND p.property_id IS NULL ' : ' ';

		$strSql = 'SELECT
						p.*
					FROM
						( ' . $strSql . ' ) as p
						LEFT JOIN website_properties wp ON ( wp.cid = p.cid AND wp.property_id = p.id AND wp.website_id = ' . ( int ) $intWebsiteId . ' )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND wp.id IS NULL' .
		          $strCondition . '
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
					ORDER BY
						lower ( p.property_name ) ASC,
						order_num';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertyDetailsWithCompanyNameByClientIds( $arrintClientIds, $objDatabase ) {

		if( false == valArr( $arrintClientIds ) ) {
			return;
		}

		$strSql = 'SELECT
						c.id AS cid,
						p.id AS property_id,
						c.company_name,
						p.property_name
					FROM
						properties p
						JOIN clients c ON c.id = p.cid
					WHERE
						p.cid IN ( ' . implode( ',', $arrintClientIds ) . ' )
						AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertiesUnitCountForSmallAndLargePropertiesByIdsByCid( $intCid, $objDatabase, $intSmallPropertyUnitCount = 0, $strPropertySizeType = NULL, $arrintPropertyIds = [] ) {

		$strSql = 'SELECT
						*
					FROM
						(
						SELECT
							DISTINCT ON ( p.id )
							p.id,
							p.property_name,
							p.cid,
							p.property_id,
							p.property_type_id,
							p.company_region_id,
							p.time_zone_id,
							p.owner_id,
							p.occupancy_type_ids,
							p.remote_primary_key,
							p.lookup_code,
							p.vaultware_number,
							p.min_rent,
							p.max_rent,
							p.min_square_feet,
							p.max_square_feet,
							p.min_bedrooms,
							p.max_bedrooms,
							p.min_bathrooms,
							p.max_bathrooms,
							p.year_built,
							p.year_remodeled,
							p.short_description,
							p.full_description,
							p.driving_directions,
							p.details,
							p.allows_cats,
							p.allows_dogs,
							p.has_availability,
							p.is_test,
							p.is_managerial,
							p.order_num,
							p.imported_on,
							p.account_id,
							p.number_of_units,
							pa.street_line1,
							pa.street_line2,
							pa.city,
							pa.state_code,
							pa.country_code,
							pa.postal_code
						FROM
							properties p
							JOIN property_addresses pa ON ( p.id = pa.property_id AND p.cid = pa.cid AND pa.is_alternate = false )
						WHERE
							p.is_disabled = 0
							AND pa.address_type_id = ' . CAddressType::PRIMARY . '
							AND p.cid = ' . ( int ) $intCid;
		if( false != valArr( $arrintPropertyIds ) ) {
			$strSql .= ' AND p.id IN ( ' . trim( implode( ',', array_filter( $arrintPropertyIds ) ), ',' ) . ' )';
		}
		if( 0 < $intSmallPropertyUnitCount && false == is_null( $strPropertySizeType ) ) {
			if( 'small' == \Psi\CStringService::singleton()->strtolower( $strPropertySizeType ) ) {
				$strSql .= ' AND ( p.number_of_units IS NULL OR p.number_of_units <= ' . ( int ) $intSmallPropertyUnitCount . ' ) ';
			} elseif( 'large' == \Psi\CStringService::singleton()->strtolower( $strPropertySizeType ) ) {
				$strSql .= ' AND p.number_of_units > ' . ( int ) $intSmallPropertyUnitCount;
			}
		}
		$strSql .= ' ) as sq ORDER BY property_name';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertyNamesWithoutGlExportSettingsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name
					FROM
						properties p
						LEFT JOIN property_preferences pp ON ( p.cid = pp.cid AND p.id = pp.property_id AND pp.key = \'PROPERTY_GL_BU\' AND pp.value IS NOT NULL )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN	( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND pp.id IS NULL
					ORDER BY
						lower ( p.property_name )';

		$arrmixProperties = ( array ) fetchData( $strSql, $objDatabase );

		$arrstrProperties = [];

		foreach( $arrmixProperties as $arrmixProperty ) {
			$arrstrProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
		}

		return $arrstrProperties;
	}

	public static function fetchCustomPropertiesByCids( $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		return fetchData( 'SELECT id, property_name FROM properties WHERE cid IN ( ' . implode( ',', $arrintCids ) . ' ) AND property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' ) AND is_disabled <> 1 ORDER BY cid,property_name', $objDatabase );
	}

	public static function fetchSimplePropertiesByIds( $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		return fetchData( 'SELECT id, property_name FROM properties WHERE id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND is_disabled <> 1', $objDatabase );
	}

	public static function fetchPropertiesByOwnerIdsByCid( $arrintOwnerIds, $intCid, $objDatabase, $boolIsExcludeDisabledProperties = false ) {

		if( false == valArr( $arrintOwnerIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						properties
					WHERE
						owner_id IN ( ' . implode( ', ', $arrintOwnerIds ) . ' )
						AND cid = ' . ( int ) $intCid;
		if( false != $boolIsExcludeDisabledProperties ) {
			$strSql .= ' AND is_disabled = 0';
		}

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchSimplePropertiesByQuickResponseIdByCid( $intQuickResponseId, $intCid, $arrintPropertyIds, $objDatabase ) {

		if( false == valId( $intQuickResponseId ) && false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		if( true == valId( $intQuickResponseId ) ) {
			$arrstrQuery['join']  = 'JOIN property_quick_responses AS pqr ON ( p.id = pqr.property_id AND p.cid = pqr.cid )';
			$arrstrQuery['where'] = 'pqr.cid =' . ( int ) $intCid . ' AND pqr.quick_response_id =' . ( int ) $intQuickResponseId . '';
		}

		if( true == valArr( $arrintPropertyIds ) ) {
			$arrstrQuery['join']  = '';
			$arrstrQuery['where'] = 'p.cid = ' . ( int ) $intCid . ' AND p.id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )';
		}

		$strSql = 'SELECT
						p.property_name
					FROM
						properties AS p '
		          . $arrstrQuery['join'] . '
					WHERE '
		          . $arrstrQuery['where'] .
		          ' AND p.is_disabled = 0';

		$arrstrPropertyNames = fetchData( $strSql, $objDatabase );

		return $arrstrPropertyNames;
	}

	public static function fetchCustomPaginatedPropertyDetailsByCidByContractId( $intPageNo, $intPageSize, $intCid, $intContractId, $objDatabase, $boolCountFlag = NULL, $strSearchVal = NULL ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;
		$strSql    = 'WITH FCP AS (
								SELECT
									cp.property_id,
									cp.contract_termination_request_id AS contract_termination_request_id,
									cp.termination_date AS contract_property_termination_date,
									ctr.termination_date AS contract_termination_request_date,
									ROW_NUMBER ( ) OVER ( PARTITION BY cp.property_id
								ORDER BY
									cp.contract_termination_request_id DESC ) AS RANK,
									p.id,
									p.property_name,
									p.number_of_units,
									p.termination_date,
									p.is_disabled,
									p.is_managerial,
									p.country_code,
									pa.street_line1,
									pa.street_line2,
									pa.city,
									pa.state_code,
									pa.postal_code,
									cp.created_on AS created_on';

		if( true == $boolCountFlag ) {
			$strSql = 'SELECT COUNT( DISTINCT cp.property_id ) ';
		}

		$strSql .= ' FROM
						contract_properties cp
						JOIN contract_properties cp1 ON ( cp1.property_id = cp.property_id AND cp1.contract_id = cp.contract_id )
						JOIN properties p ON ( p.id = cp.property_id AND p.cid = cp.cid )
						LEFT JOIN property_addresses pa ON ( pa.cid = p.cid AND pa.property_id = p.id AND pa.is_alternate = false )
						LEFT JOIN contract_termination_requests ctr ON ctr.id = cp.contract_termination_request_id
					WHERE
						cp.cid = ' . ( int ) $intCid . '
						AND cp.contract_id = ' . ( int ) $intContractId . '
						AND ( ( cp.contract_termination_request_id IS NULL
						AND cp.termination_date IS NULL )
						OR ( cp1.termination_date IS NULL
						AND cp1.contract_termination_request_id IS NOT NULL )
						OR ( cp1.termination_date IS NOT NULL ) )';

		if( false == is_null( $strSearchVal ) ) {
			$strSql .= ' AND p.property_name ILIKE \'%' . addslashes( trim( $strSearchVal ) ) . '%\'';
		}

		if( true == $boolCountFlag ) {
			$arrintPropertyCount = fetchData( $strSql, $objDatabase );

			if( true == isset( $arrintPropertyCount[0]['count'] ) ) {
				return $arrintPropertyCount[0]['count'];
			}
		} else {
			$strSql .= ' ORDER BY p.property_name ) SELECT
									*
								FROM
									FCP
								WHERE
									RANK = 1' . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

			return self::fetchProperties( $strSql, $objDatabase );
		}
	}

	public static function fetchAllPropertiesByContractId( $intContractId, $objAdminDatabase, $boolFetchAllDisabled = false, $intCid = NULL ) {

		$strJoin           = 'JOIN';
		$strWhereCondition = 'cp.contract_id = ' . ( int ) $intContractId;

		// Fetch all disabled properties of the client along with properties associated with the contract.
		if( true == $boolFetchAllDisabled ) {
			$strJoin           = 'LEFT JOIN';
			$strWhereCondition .= ' OR ( ( p.cid = ' . ( int ) $intCid . ' AND p.is_disabled = 1 ) AND p.property_type_id NOT IN ( ' . implode( ', ', CPropertyType::$c_arrintExcludePropertyTypeIdsForContracts ) . ' ) AND p.termination_date IS NULL AND p.is_test = 0 )';
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.is_managerial,
						p.is_disabled,
						p.country_code,
						p.termination_date
					 FROM
						properties p
						' . $strJoin . ' contract_properties cp ON ( cp.property_id = p.id AND cp.cid = p.cid )
					WHERE
						' . $strWhereCondition . '
					GROUP BY
						p.id,
						p.property_name,
						p.is_managerial
					ORDER BY
						p.property_name';

		return parent::fetchProperties( $strSql, $objAdminDatabase );
	}

	public static function fetchPropertiesByIds( $arrintPropertyIds, $objAdminDatabase, $boolDisabled = false ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strWhereCondition = '';
		if( true === $boolDisabled ) {
			$strWhereCondition = ' AND p.is_disabled=1';
		}

		$strSql = ' SELECT
						p.id,
						p.cid,
						p.property_name,
						p.disabled_on,
						p.is_disabled,
						p.property_type_id,
						details->>\'email_relay_address\' as email_relay_address
					FROM
						properties p
					WHERE
						p.id IN (' . implode( ',', $arrintPropertyIds ) . ')' . $strWhereCondition;

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchSimplePropertiesDetailsByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					p.id,
					pa.street_line1,
					pa.street_line2,
					pa.street_line3,
					pa.city,
					pa.state_code,
					pa.postal_code,
					pn.phone_number,
					pn.phone_number_type_id,
					pd.community_website_uri
				FROM properties p
					JOIN property_phone_numbers pn ON p.cid = pn.cid AND pn.property_id = p.id
					JOIN property_details pd ON p.cid = pd.cid AND pd.property_id = p.id
					JOIN property_addresses pa ON p.cid = pa.cid AND pa.property_id = p.id AND pa.is_alternate = false
				WHERE
					pn.phone_number_type_id = ' . CPhoneNumberType::OFFICE . '
					AND pa.address_type_id = ' . CAddressType::PRIMARY . '
					AND p.cid = ' . ( int ) $intCid . '
					AND p.id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyIdsByInspectionFormIdByCid( $intInspectionFormId, $intCid, $objDatabase ) {
		if( false == valId( $intInspectionFormId ) || false == valId( $intCid ) ) {
			return [];
		}

		$strSql = 'SELECT
						p.id
					 FROM
						properties p
						JOIN property_inspection_forms pif on ( pif.property_id = p.id and pif.cid = p.cid )
					WHERE
						pif.inspection_form_id = ' . ( int ) $intInspectionFormId . '
						AND pif.cid = ' . ( int ) $intCid . '
				 ORDER BY
						p.property_name';

		$arrintPropertyIds = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		} else {
			return rekeyArray( 'id', $arrintPropertyIds, true );
		}
	}

	public static function fetchPropertyNamesByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT string_agg( property_name , \', \') property_names FROM properties WHERE id IN ( ' . implode( ',', $arrintPropertyIds ) . ') AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByAccountingExportBatchIdByCid( $intAccountingExportBatchId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT ON ( p.id )
						p.property_name,
						p.lookup_code
					FROM
						accounting_export_batches aheb
						JOIN ap_headers ah ON ( ah.accounting_export_batch_id = aheb.id AND ah.cid = aheb.cid )
						JOIN ap_payments ap ON ( ah.cid = ap.cid AND ah.ap_payment_id = ap.id )
						JOIN ap_details ad ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ad.gl_transaction_type_id = ah.gl_transaction_type_id AND ad.post_month = ah.post_month )
						JOIN properties p ON ( p.id = ad.property_id AND p.cid = ad.cid )
					WHERE
					aheb.id = ' . ( int ) $intAccountingExportBatchId . '
					AND aheb.cid = ' . ( int ) $intCid .
		          ' AND ah.is_template IS FALSE ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyIdsByPropertyGroupIdsByPsProductIdsByCid( $arrintPropertyGroupIds, $arrintPsProductIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyGroupIds ) ) {
			return [];
		}

		$arrintPsProductIds = explode( ',', $arrintPsProductIds );

		$strPsProductField = '';
		if( true == valArr( $arrintPsProductIds ) ) {
			$strPsProductField = ', ARRAY[ ' . implode( ',', ( array ) $arrintPsProductIds ) . ' ]::INTEGER[]';
		}

		$strSql            = 'SELECT * FROM load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $arrintPropertyGroupIds ) . ' ] ' . $strPsProductField . ' ) WHERE is_disabled = 0 AND is_test = 0';
		$arrmixPropertyIds = fetchData( $strSql, $objDatabase );

		$arrintPropertyIds = ( true == valArr( $arrmixPropertyIds ) ) ? rekeyArray( 'property_id', $arrmixPropertyIds ) : [];

		return array_keys( $arrintPropertyIds );
	}

	public static function fetchActiveEntrataCoreUnitsByCid( $intCid, $objDatabase ) {

		$strSql = ' SELECT
						SUM (p.number_of_units) as ecore_units
					FROM
						properties p
						JOIN clients c on c.id = p.cid AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
						JOIN contract_properties cp ON cp.property_id = p.id AND cp.renewal_contract_property_id IS NULL AND cp.is_last_contract_record = true
					WHERE
						c.id = ' . ( int ) $intCid . '
						AND p.disabled_on IS NULL
						AND cp.termination_date IS NULL
						AND cp.ps_product_id = ' . CPsProduct::ENTRATA;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertiesCountByCidByCustomerRelationshipGroupIds( $intCid, $arrintCustomerRelationshipGroupIds, $objDatabase ) {
		if( false == valArr( $arrintCustomerRelationshipGroupIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						pcrg.customer_relationship_group_id,
						count( p.id ) as properties_count
					FROM
						properties p
						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = p.cid AND pcrg.property_id = p.id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
						AND pcrg.customer_relationship_group_id IN (' . sqlIntImplode( $arrintCustomerRelationshipGroupIds ) . ')
					GROUP BY pcrg.customer_relationship_group_id';

		$arrintResponses = fetchData( $strSql, $objDatabase );

		$arrmixPropertiesCount = [];

		if( true == valArr( $arrintResponses ) ) {
			foreach( $arrintResponses as $arrintResponse ) {
				$arrmixPropertiesCount[$arrintResponse['customer_relationship_group_id']] = $arrintResponse['properties_count'];
			}
		}

		return $arrmixPropertiesCount;
	}

	public static function fetchParentPropertiesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT p.id,p.property_id FROM
						properties p
					WHERE
						p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND property_id IS NOT NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyIdsByCustomerRelationshipGroupIdByCid( $intCustomerRelationshipGroupId, $intCid, $objDatabase ) {

		if( true == is_null( $intCustomerRelationshipGroupId ) || true == is_null( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						pcrg.property_id
					FROM
						properties p
						JOIN property_customer_relationship_groups pcrg ON ( pcrg.cid = p.cid AND pcrg.property_id = p.id AND pcrg.customer_relationship_group_id = ' . ( int ) $intCustomerRelationshipGroupId . ' )
					WHERE
						pcrg.cid = ' . ( int ) $intCid . '
					ORDER BY
						p.property_name	';

		$arrintResponses = fetchData( $strSql, $objDatabase );

		$arrintPropertyIds = [];

		if( true == valArr( $arrintResponses ) ) {
			foreach( $arrintResponses as $arrintResponse ) {
				$arrintPropertyIds[$arrintResponse['property_id']] = $arrintResponse['property_id'];
			}
		}

		return $arrintPropertyIds;
	}

	public static function fetchdelinquencyInterestsEligiblePropertiesByCid( $intCid, $objDatabase, $intPageNo = NULL, $intPageSize = NULL, $arrintPropertyIds ) {

		$strSubSql = '';

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		} else {
			$strSubSql = ' AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}

		$boolIsPaginationRequired = false;

		if( false == is_null( $intPageSize ) && false == is_null( $intPageNo ) ) {
			$intOffset                = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit                 = ( int ) $intPageSize;
			$boolIsPaginationRequired = true;
		}

		$strSql = 'SELECT
						DISTINCT p.id AS property_id,
						p.property_name
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id AND p.is_disabled = 0 AND pgs.activate_standard_posting = true )
						JOIN property_products pp ON ( pgs.cid = pp.cid AND ( p.id = pp.property_id OR pp.property_id IS NULL ) )
						JOIN property_interest_formulas pif ON ( p.cid = pif.cid AND p.id = pif.property_id )
					WHERE
						pif.cid = ' . ( int ) $intCid . '
						AND pp.ps_product_id = ' . CPsProduct::ENTRATA . '
						AND pif.calculate_interest = \'t\'
						AND pif.deleted_on IS NULL
						AND pif.interest_formula_type_id = ' . CInterestFormulaType::DELINQUENCY_INTEREST . $strSubSql;

		if( true == $boolIsPaginationRequired ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDepositInterestsEligiblePropertiesByCid( $intCid, $objDatabase, $intPageNo = NULL, $intPageSize = NULL, $boolIsManagerial = false, $arrintPropertyIds = NULL, $strPostWindowEndDate = NULL ) {

		$strSubSql = '';

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		} else {
			$strSubSql = ' AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}
		$boolIsPaginationRequired = false;

		if( false == is_null( $intPageSize ) && false == is_null( $intPageNo ) ) {
			$intOffset                = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit                 = ( int ) $intPageSize;
			$boolIsPaginationRequired = true;
		}

		// @FIXME: Use load_properties condition and remove other joins.

		$strSql = 'SELECT
						p.id
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id AND p.is_disabled = 0 AND pgs.activate_standard_posting = true )
						JOIN property_products pp ON ( pgs.cid = pp.cid AND ( p.id = pp.property_id OR pp.property_id IS NULL ) )
						JOIN property_interest_formulas pif ON ( p.cid = pif.cid AND p.id = pif.property_id )
					WHERE
						pif.cid = ' . ( int ) $intCid . '
						AND pp.ps_product_id = ' . CPsProduct::ENTRATA . '
						AND pif.interest_formula_type_id = ' . CInterestFormulaType::DEPOSIT_INTEREST . '
						AND pif.calculate_interest = \'t\'
						AND pif.deleted_on IS NULL
						AND ( pif.effective_through_date >=DATE( now() ) OR pif.effective_through_date IS NULL )
						' . $strSubSql;

		$strSql .= ( true == $boolIsManagerial ) ? 'AND p.is_managerial = 1' : '';

		$strSql .= ' GROUP BY p.id';

		$arrintPropertyIds = fetchData( $strSql, $objDatabase );

		$arrintPropertyIds = ( true == valArr( $arrintPropertyIds ) ) ? array_keys( rekeyArray( 'id', $arrintPropertyIds ) ) : NULL;

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strPostWindowEndDate = ( false == valStr( $strPostWindowEndDate ) ? 'NOW()' : $strPostWindowEndDate );

		$strSql = ' WITH 
						earlist_lease as(
										SELECT
										   MIN ( li.lease_start_date ) AS lease_start_date,
										   MAX ( li.lease_end_date ) AS lease_end_date,
										   li.cid,
										   li.property_id,
										   li.lease_id
										FROM
										   lease_intervals li
										WHERE
											li.cid = ' . ( int ) $intCid . '
											AND li.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
											AND li.lease_status_type_id NOT IN ( ' . CLeaseStatusType::FUTURE . ', ' . CLeaseStatusType::CANCELLED . ' )
											AND li.lease_interval_type_id <> ' . CLeaseIntervalType::LEASE_MODIFICATION . '
										GROUP BY
										   li.cid,
										   li.property_id,
										   li.lease_id
						),		
						last_accrual as (
							   SELECT
								   MAX ( aa.end_date ) AS last_accrual_end_date,
								   aa.cid,
								   aa.lease_id,
								   aa.property_id
							   FROM
								   ar_accruals aa
							   WHERE
									aa.cid = ' . ( int ) $intCid . '
									AND aa.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
									AND aa.is_deleted = \'f\'
							   GROUP BY
								   aa.cid,
								   aa.lease_id,
								   aa.property_id
						),
						at_trans as (
								SELECT
									art.cid,
									art.property_id,
									art.lease_id,
									art.ar_code_id,
									art.ar_code_type_id,
									art.is_deposit_credit,
									CASE
									WHEN art.transaction_amount > 0 THEN SUM ( aa.allocation_amount ) * - 1
									ELSE SUM ( aa.allocation_amount )
									END AS allocated_amount,
										CASE WHEN allocated_charge_transaction.ar_code_type_id = ' . CArCodeType::REFUND . ' THEN
										allocated_charge_transaction.post_date
									END AS refund_date,
									aa.post_date AS post_date
								FROM
									ar_transactions art
									JOIN ar_codes ac ON ( art.cid = ac.cid AND art.ar_code_id = ac.id AND ac.gl_account_type_id = ' . CGlAccountType::LIABILITIES . ' )
									JOIN ar_allocations aa ON ( art.cid = aa.cid AND ( art.id = aa.charge_ar_transaction_id OR art.id = aa.credit_ar_transaction_id ) AND aa.is_deleted = FALSE )
									JOIN ar_transactions allocated_charge_transaction ON ( allocated_charge_transaction.cid = aa.cid AND aa.charge_ar_transaction_id = allocated_charge_transaction.id )
									JOIN ledger_filters lf ON ( ac.cid = lf.cid AND ac.ledger_filter_id = lf.id )
								WHERE
									art.cid = ' . ( int ) $intCid . '
										AND art.ar_code_type_id = ' . CArCodeType::DEPOSIT . '
									AND art.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
									AND art.is_deleted = FALSE
									AND ( lf.default_ledger_filter_id IS NULL OR lf.default_ledger_filter_id <> ' . CDefaultLedgerFilter::GROUP . ' )
									GROUP BY
										art.id,
										art.cid,
										art.property_id,
										art.lease_id,
										art.ar_code_id,
										art.ar_code_type_id,
										art.is_deposit_credit,
										art.transaction_amount,
										aa.post_date,
										allocated_charge_transaction.ar_code_type_id,
										allocated_charge_transaction.post_date
						),
				deposit_interest_details AS (
					SELECT
						sub_query.lease_id,
						sub_query.unit_number_cache,

						CASE WHEN sub_query.interest_period_start_date::date > NOW() THEN 0
						ELSE
							CASE WHEN sub_query.last_accrual_end_date IS NULL THEN
								CASE WHEN sub_query.pay_at_tenacy_period = \'f \'
									THEN (sub_query.allocated_amount * sub_query.interest_percent * ( ( sub_query.interest_period_end_date::DATE -( greatest( property_preference.posting_begin_date_from, ( sub_query.tenancy_period_start_date::date + concat( sub_query.interest_qualification_days, \'days\')::INTERVAL )::DATE, sub_query.post_date )::DATE ) ) + 1 ) / 365.25 )
								ELSE
									CASE WHEN ( sub_query.pay_at_tenacy_period = \'t \' AND ( ( sub_query.interest_period_end_date::DATE - greatest ( sub_query.interest_period_start_date::DATE, sub_query.post_date )::DATE ) + 1 ) >= sub_query.interest_qualification_days )
										THEN ( sub_query.allocated_amount * sub_query.interest_percent * ( ( sub_query.interest_period_end_date::date - greatest( property_preference.posting_begin_date_from, sub_query.tenancy_period_start_date::date, sub_query.post_date )::DATE ) + 1 ) / 365.25 )
									ELSE 0
									END
								END
							ELSE
								( sub_query.allocated_amount * sub_query.interest_percent * ( ( sub_query.interest_period_end_date::date - greatest( property_preference.posting_begin_date_from, sub_query.tenancy_period_start_date::date, sub_query.post_date )::DATE ) + 1 ) / 365.25 )
							END
						END AS accrued_interest,

						sub_query.customer_name,
						sub_query.interest_period_end_date::date,

						CASE WHEN sub_query.pay_at_tenacy_period = \'f\' AND sub_query.last_accrual_end_date IS NULL
							THEN greatest( property_preference.posting_begin_date_from, ( sub_query.tenancy_period_start_date::date + concat( sub_query.interest_qualification_days, \'days\')::INTERVAL )::DATE )::DATE
						ELSE
							greatest( property_preference.posting_begin_date_from, sub_query.tenancy_period_start_date::date )
						END AS tenancy_period_start_date,

						sub_query.property_id,
						sub_query.cid,
						prop.property_name,
						sub_query.post_date
					FROM
					(
						SELECT
							CASE pif.ar_trigger_id
								WHEN ' . CArTrigger::ANNIVERSARY_OF_MOVE_IN . ' THEN
										greatest ( last_accrual.last_accrual_end_date + \'1 days\' :: INTERVAL, at_trans.post_date, vl.move_in_date )
								WHEN ' . CArTrigger::END_OF_LEASE_TERM . ' THEN
										CASE
											WHEN vl.lease_interval_type_id IN ( ' . CLeaseIntervalType::MONTH_TO_MONTH . ' ,' . CLeaseIntervalType::RENEWAL . ') THEN
												greatest ( last_accrual.last_accrual_end_date + \'1 days\' :: INTERVAL, least ( earlist_lease.lease_start_date, vl.move_in_date ), at_trans.post_date)
											ELSE
												greatest ( least ( vl.lease_start_date, vl.move_in_date ), last_accrual.last_accrual_end_date + \'1 days\' :: INTERVAL, at_trans.post_date )
										END
								WHEN ' . CArTrigger::END_OF_CALENDAR_YEAR . ' THEN
										CASE
											WHEN vl.lease_interval_type_id IN ( ' . CLeaseIntervalType::MONTH_TO_MONTH . ' ,' . CLeaseIntervalType::RENEWAL . ') THEN
												greatest( least( vl.move_in_date, earlist_lease.lease_start_date ), last_accrual.last_accrual_end_date + \' 1 day\'::INTERVAL, at_trans.post_date)
											ELSE
												greatest( least( vl.move_in_date, vl.lease_start_date ), last_accrual.last_accrual_end_date + \' 1 day\'::INTERVAL, at_trans.post_date)
										END
								WHEN ' . CArTrigger::FINAL_STATEMENT . ' THEN
										CASE
											WHEN vl.lease_interval_type_id IN ( ' . CLeaseIntervalType::MONTH_TO_MONTH . ' ,' . CLeaseIntervalType::RENEWAL . ') THEN
												greatest( vl.move_in_date, earlist_lease.lease_start_date, last_accrual.last_accrual_end_date + \' 1 day\'::INTERVAL, at_trans.post_date )::date
											ELSE
												greatest( vl.move_in_date, vl.lease_start_date, last_accrual.last_accrual_end_date + \' 1 day\'::INTERVAL, at_trans.post_date )::date
										END
							END AS interest_period_start_date,

							CASE pif.ar_trigger_id
								WHEN ' . CArTrigger::ANNIVERSARY_OF_MOVE_IN . ' THEN
										CASE
											WHEN vl.lease_interval_type_id IN ( ' . CLeaseIntervalType::MONTH_TO_MONTH . ' ,' . CLeaseIntervalType::RENEWAL . ') THEN
												greatest ( least ( vl.move_out_date, vl.lease_end_date, COALESCE ( at_trans.refund_date, NULL ), func_get_latest_move_in_anniversary_date( vl.move_in_date ) ), last_accrual.last_accrual_end_date + \' 1 year\' :: INTERVAL )
											ELSE
												least ( vl.move_out_date, vl.lease_end_date, COALESCE ( at_trans.refund_date, NULL ), func_get_latest_move_in_anniversary_date( vl.move_in_date ) )
										END
								WHEN ' . CArTrigger::END_OF_LEASE_TERM . ' THEN
										CASE
											WHEN vl.lease_interval_type_id IN ( ' . CLeaseIntervalType::MONTH_TO_MONTH . ' ,' . CLeaseIntervalType::RENEWAL . ') THEN
												least ( vl.move_out_date, earlist_lease.lease_end_date, COALESCE ( at_trans.refund_date, NULL ) )
											ELSE
												least ( vl.move_out_date, vl.lease_end_date, COALESCE ( at_trans.refund_date, NULL ) )
										END
								WHEN ' . CArTrigger::END_OF_CALENDAR_YEAR . ' THEN least((( \'12/31/\' || TO_CHAR( NOW() , \'YYYY\' ) )::date + \'-1 years\' ::INTERVAL), COALESCE( at_trans.refund_date, NULL ) )
								WHEN ' . CArTrigger::FINAL_STATEMENT . ' THEN least( NOW(), COALESCE( at_trans.refund_date, NULL ) )::date
							END AS interest_period_end_date,

							CASE pif.ar_trigger_id
								WHEN ' . CArTrigger::ANNIVERSARY_OF_MOVE_IN . ' THEN
										greatest ( last_accrual.last_accrual_end_date + \'1 days\' :: INTERVAL, vl.move_in_date )
								WHEN ' . CArTrigger::END_OF_LEASE_TERM . ' THEN
										CASE
											WHEN vl.lease_interval_type_id IN ( ' . CLeaseIntervalType::MONTH_TO_MONTH . ' ,' . CLeaseIntervalType::RENEWAL . ') THEN
												greatest( last_accrual.last_accrual_end_date + \'1 days\' :: INTERVAL, least ( earlist_lease.lease_start_date, vl.move_in_date ) )
											ELSE
												greatest( least ( vl.lease_start_date, vl.move_in_date ), last_accrual.last_accrual_end_date + \'1 days\' :: INTERVAL )
										END
								WHEN ' . CArTrigger::END_OF_CALENDAR_YEAR . ' THEN
										CASE
											WHEN vl.lease_interval_type_id IN ( ' . CLeaseIntervalType::MONTH_TO_MONTH . ' ,' . CLeaseIntervalType::RENEWAL . ') THEN
												greatest( least( vl.move_in_date, earlist_lease.lease_start_date ), last_accrual.last_accrual_end_date + \' 1 day\'::INTERVAL )
											ELSE
												greatest( least( vl.move_in_date, vl.lease_start_date ), last_accrual.last_accrual_end_date + \' 1 day\'::INTERVAL)
										END
								WHEN ' . CArTrigger::FINAL_STATEMENT . ' THEN
										CASE
											WHEN vl.lease_interval_type_id IN ( ' . CLeaseIntervalType::MONTH_TO_MONTH . ' ,' . CLeaseIntervalType::RENEWAL . ') THEN
												greatest( vl.move_in_date, earlist_lease.lease_start_date, last_accrual.last_accrual_end_date + \' 1 day\'::INTERVAL )::date
											ELSE
												greatest( vl.move_in_date, vl.lease_start_date, last_accrual.last_accrual_end_date + \' 1 day\'::INTERVAL)::date
										END
							END AS tenancy_period_start_date,

							vl.cid,
							vl.property_id,
							vl.id as lease_id,
							func_format_customer_name ( vl.name_first, vl.name_last ) AS customer_name,
							vl.unit_number_cache,
							at_trans.allocated_amount,
							pif.interest_percent,
							pif.interest_qualification_days,
							pif.accrue_interest_during_qualification_period AS pay_at_tenacy_period,
							pif.ar_code_id,
							pif.id as interest_formula_id,
							last_accrual.last_accrual_end_date,
							at_trans.post_date
						FROM
							view_leases vl
							LEFT JOIN earlist_lease ON ( vl.cid = earlist_lease.cid AND vl.property_id = earlist_lease.property_id AND vl.id = earlist_lease.lease_id AND vl.lease_start_date - \'1 year\' ::INTERVAL < earlist_lease.lease_end_date )
							JOIN property_interest_formulas pif ON ( vl.property_id = pif.property_id AND vl.cid = pif.cid AND pif.effective_through_date IS NULL AND pif.deleted_on IS NULL AND pif.ar_trigger_id <> ' . CArTrigger ::FINAL_STATEMENT . ' AND pif.interest_formula_type_id = ' . CInterestFormulaType::DEPOSIT_INTEREST . ' )
							LEFT JOIN at_trans ON ( at_trans.cid = vl.cid AND at_trans.property_id = vl.property_id AND at_trans.lease_id = vl.id )
							LEFT JOIN last_accrual ON ( last_accrual.cid = vl.cid AND last_accrual.property_id = vl.property_id AND last_accrual.lease_id = vl.id )
						WHERE
							vl.cid = ' . ( int ) $intCid . '
							AND vl.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
							AND vl.lease_status_type_id IN ( ' . implode( ',', CLeaseStatusType::$c_arrintActivatedLeaseStatusTypes ) . ')
							AND vl.fmo_started_on IS NULL
							AND vl.occupancy_type_id IN ( ' . implode( ',', COccupancyType::$c_arrintIncludedOccupancyTypes ) . ' )
						)as sub_query
						LEFT JOIN (
								SELECT
									ppf.cid,
									ppf.property_id,
									ppf.value::date as posting_begin_date_from
								FROM
									property_preferences ppf
								WHERE
									ppf.cid = ' . ( int ) $intCid . '
									AND ppf.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
									AND ppf.key = \'DEPOSIT_INTEREST_BEGIN_DATE\'
									AND ppf.value IS NOT NULL
						) AS property_preference ON ( property_preference.cid = sub_query.cid AND property_preference.property_id = sub_query.property_id )
						JOIN properties prop ON ( prop.cid= sub_query.cid AND prop.id=sub_query.property_id )
					WHERE
						sub_query.interest_period_start_date < NOW()
						AND sub_query.interest_period_start_date < sub_query.interest_period_end_date
						AND sub_query.interest_period_end_date > tenancy_period_start_date
						AND sub_query.interest_period_end_date <= \'' . $strPostWindowEndDate . '\'
						AND sub_query.cid = ' . ( int ) $intCid . '
						AND sub_query.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
				)';

		$strSql .= 'SELECT
						ROUND( ROUND( SUM( sub.amount ), 6 ), 2 ) AS total_amount,
						COUNT( sub.lease_id ) AS lease_count,
						sub.property_id,
						sub.property_name
					FROM
						( SELECT
								DISTINCT did.lease_id,
								SUM ( did.accrued_interest ) AS amount,
								did.property_id,
								did.property_name
							FROM
								deposit_interest_details did
							WHERE
								did.cid = ' . ( int ) $intCid . '
								AND did.accrued_interest <> 0
								AND did.tenancy_period_start_date < did.interest_period_end_date
							GROUP BY
								did.lease_id,
								did.property_id,
								did.property_name
							HAVING
								ROUND( ROUND( SUM( did.accrued_interest ), 6 ), 2 ) > 0
							ORDER BY
								did.property_name
							) AS sub
					GROUP BY
							sub.property_id,
							sub.property_name';

		if( true == $boolIsPaginationRequired ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByTrackInventoryQuantitiesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						p.*
					FROM
						properties p
						JOIN property_preferences pf ON ( p.cid = pf.cid AND p.id = pf.property_id AND pf.key = ' . '\'TRACK_INVENTORY_QUANTITIES\'' . ' AND pf.value = ' . '\'1\'' . ' )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchBulkTransferInventoryPropertiesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == ( $arrintPropertyIds = getIntValuesFromArr( $arrintPropertyIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM 
						properties
					WHERE ( id, cid ) IN (
						 SELECT
						    p.id,
						    p.cid 
						FROM
						
						asset_locations al
						JOIN property_group_asset_locations pgal ON ( al.cid = pgal.cid AND al.id = pgal.asset_location_id )
						JOIN property_groups pg ON ( pg.cid = pgal.cid AND pgal.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pgal.cid = pga.cid AND pg.id = pga.property_group_id )
						JOIN properties p ON ( p.cid = pgal.cid AND p.id = pga.property_id AND p.is_disabled = 0 ) 
                        JOIN property_preferences pp ON ( p.cid = pp.cid AND pp.property_id = p.id AND pp.key = ' . '\'TRACK_INVENTORY_QUANTITIES\'' . ' AND pp.value = ' . '\'1\'' . ' )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND pga.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
						GROUP BY p.id, p.cid HAVING count(al.id) > 1 
						)';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByPropertyNamesByCid( $arrstrPropertyNames, $intCid, $objDatabase ) {

		if( false == valArr( $arrstrPropertyNames ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						properties
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_disabled = 0
						AND LOWER( property_name ) IN ( \'' . implode( '\',\'', $arrstrPropertyNames ) . '\' ) ';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchSimplePropertiesWithStatesByCidByPropertyIds( $intCid, $arrintPropertyIds, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.*,
						s.name as state_name
					FROM
						properties p
						LEFT JOIN property_addresses pa ON ( p.cid = pa.cid AND p.id = pa.property_id AND pa.is_alternate = false )
						LEFT JOIN states s ON ( pa.state_code = s.code AND s.is_published = 1 )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					ORDER BY
						lower ( p.property_name ) ASC';

		$arrmixProperties = fetchData( $strSql, $objDatabase );
		if( false == valArr( $arrmixProperties ) ) {
			return [];
		}

		foreach( $arrmixProperties as $arrmixPropertyData ) {
			$arrstrProperties[$arrmixPropertyData['id']] = $arrmixPropertyData;
		}

		return $arrstrProperties;
	}

	public static function fetchPropertiesCountByOccupancyTypeIdByCid( $intOccupancyTypeId, $intCid, $objClientDatabase ) {

		if( false == valId( $intOccupancyTypeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						COUNT( id )
					FROM
						properties p 
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND ' . ( int ) $intOccupancyTypeId . ' = ANY( p.occupancy_type_ids )';

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchPropertiesAvailableForCopySettingsByPropertyIdByCids( $intCid, $intCompanyUserId, $boolIsAdmin, $objDatabase ) {

		$intGreyStarClientId        = 11189; // This is an id of client GreyStar Advantage
		$intGreyStarPropertyId      = 140104; // This property is belonging to client GreyStar Advantage
		$intVanillaSettingsClientId = 12048; // This is an id of client Vanilla - Settings Do Not Touch
		$intRiverstoneClientId      = 1106;        // This is an id of client Riverstone/HSC
		$intRiverstonePropertyId    = 193706; // This property is belonging to client Riverstone/HSC

		$strCondition = ( true == $boolIsAdmin ) ? ' AND property_type_id NOT IN ( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )' : ' AND company_user_id = ' . ( int ) $intCompanyUserId;

		$strSql = '
					SELECT
						p.id,
						p.cid,
						p.property_name,
						c.company_name
					FROM
						properties p
						JOIN clients c ON ( p.cid = c.id )
					WHERE
						cid = ' . ( int ) $intVanillaSettingsClientId . '
						AND is_disabled = 0 ' . $strCondition;

		if( $intCid != $intGreyStarClientId ) {
			$strSql .= ' UNION ALL
						SELECT
							p.id,
							p.cid,
							p.property_name,
							c.company_name
						FROM
							properties p
							JOIN clients c ON ( p.cid = c.id )
						WHERE
							p.id = ' . ( int ) $intGreyStarPropertyId;
		}

		if( $intCid != $intRiverstoneClientId ) {
			$strSql .= ' UNION ALL
						SELECT
							p.id,
							p.cid,
							p.property_name,
							c.company_name
						FROM
							properties p
							JOIN clients c ON ( p.cid = c.id )
						WHERE
							p.id = ' . ( int ) $intRiverstonePropertyId;
		}

		if( $intCid != $intVanillaSettingsClientId ) {
			$strSql .= 'UNION ALL
						SELECT
							p.id,
							p.cid,
							p.property_name,
							c.company_name
						FROM
							properties p
							JOIN clients c ON ( p.cid = c.id )
						WHERE
							cid = ' . ( int ) $intCid . '
							AND is_disabled = 0 ' . $strCondition;
		}

		$arrmixProperties = ( array ) fetchData( $strSql, $objDatabase );

		return $arrmixProperties;
	}

	public static function fetchPropertiesWithIntegrationStatusByPropertyIdsByCidByCompanyUserId( $arrintPropertyIds, $intCid, $intCompanyUserId, $objDatabase, $boolIsAdministrator = false ) {

		if( true != valArr( $arrintPropertyIds ) ) {
			return true;
		}
		$strSql = self::buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator );

		$strSql = 'SELECT
					gcup.id,
					gcup.property_name,
					CASE
					WHEN ( p.remote_primary_key IS NOT NULL
						AND (pp.value IS NULL OR pp.value = \'0\')
						AND id.integration_client_type_id NOT IN ( ' . implode( ',', CIntegrationClientType::$c_arrintMigrateIntegrationClientTypeIds ) . ' )
						AND id.integration_client_status_type_id <> ' . CIntegrationClientStatusType::DISABLED . ' )
						THEN CAST(1 AS BIT)
						ELSE CAST(0 AS BIT)
						END AS is_integrated
					FROM
						( ' . $strSql . ' ) as gcup
						LEFT JOIN properties p ON gcup.id = p.id
						LEFT JOIN property_integration_databases pid ON (p.id = pid.property_id AND p.cid = pid.cid)
						LEFT JOIN integration_databases id ON (id.cid = pid.cid AND id.id = pid.integration_database_id)
						LEFT JOIN property_preferences pp ON (p.id = pp.property_id AND p.cid = pp.cid AND pp.key = \'BLOCK_ALL_WORK_ORDER_CALLS_RELATED_INTEGRATION\')
					WHERE p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.cid = ' . ( int ) $intCid . '
						ORDER BY lower( gcup.property_name )';

		$arrmixPropertyIds = fetchData( $strSql, $objDatabase );

		return $arrmixPropertyIds;
	}

	public static function fetchCustomPropertiesCountByCids( $arrintCids, $objClientDatabase ) {

		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						COUNT( DISTINCT id ) AS property_count, cid
					FROM
						properties
					WHERE
						cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND is_disabled <> 1
						GROUP BY cid';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyNamesWithOwnerDataByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ( p.id ) as property_id,
						p.property_name,
						ft.fixed_amount,
						pgs.ap_bank_account_id as bank_account_id,
						(
							SELECT
								ah.post_date
							FROM
								ap_headers ah
								JOIN ap_details ad ON ( p.cid = ad.cid AND p.id = ad.property_id )
							WHERE
								ah.cid = ad.cid
								AND ah.id = ad.ap_header_id
								AND ah.ap_header_type_id = ' . ( int ) CApHeaderType::INVOICE . '
								AND ah.is_template = false
							ORDER BY
								ah.id DESC
							LIMIT
								1
						) AS post_date
					FROM
						properties p
						JOIN property_gl_settings AS pgs ON ( pgs.cid = p.cid AND pgs.property_id = p.id )
			            JOIN property_group_associations pga ON ( pga.cid = p.cid AND pga.property_id = p.id )
			            JOIN property_groups pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN fee_template_property_groups ftpg ON ( pg.cid = ftpg.cid AND pg.id = ftpg.property_group_id )
						JOIN fee_templates as ft ON ( ftpg.cid = ft.cid AND ftpg.fee_template_id = ft.id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ft.fee_type_id =' . ( int ) CFeeType::OWNER_DISTRIBUTIONS . '
					ORDER BY
						p.property_name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActivePropertiesByPsProductIds( $arrintPsProductIds, $objDatabase, $arrintPropertyIds = NULL, $arrintCids = NULL, $boolIsFetchTerminatedProperties = true, $boolAllowTestDatabase = false, $strSortBy = '' ) {

		if( false == valArr( $arrintPsProductIds ) ) {
			return NULL;
		}

		$strAdditionalField = ( 1 == \Psi\Libraries\UtilFunctions\count( $arrintPsProductIds ) && true == in_array( CPsProduct::ENTRATAMATION, $arrintPsProductIds ) ) ? 'UPPER( p.property_name ) AS property_name_upper, ' : '';

		$arrstrCompanyStatusType = [ CCompanyStatusType::CLIENT ];
		if( true === $boolAllowTestDatabase ) {
			$arrstrCompanyStatusType = [ CCompanyStatusType::CLIENT, CCompanyStatusType::TEST_DATABASE ];
		}

		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrintPsProductIds ) && true == in_array( CPsProduct::ENTRATAMATION, $arrintPsProductIds ) ) {
			$arrstrCompanyStatusType[] = CCompanyStatusType::SALES_DEMO;
		}

		$strOrderByClause = '';
		if( true == valStr( $strSortBy ) ) {
			$strOrderByClause = ' ORDER BY ' . $strSortBy;
		} else {
			$strOrderByClause = ' ORDER BY updated_on DESC';
		}

		$strClientsWhereClause         = ( true == valArr( $arrintCids ) ) ? ' AND c.id = ANY( ARRAY[' . implode( ',', $arrintCids ) . '] )' : '';
		$strPropertiesWhereClause      = ( true == valArr( $arrintPropertyIds ) ) ? ' AND p.id = ANY( ARRAY[' . implode( ',', $arrintPropertyIds ) . '] )' : '';
		$strTerminationDateWhereClause = ( false == $boolIsFetchTerminatedProperties ) ? ' ( cp.termination_date IS NULL OR cp.termination_date > NOW() ) ' : ' cp.termination_date IS NULL ';

		$strSql = ' (
						SELECT
							p.id,
							p.property_name, ' . $strAdditionalField . '
							p.cid,
							cp.ps_product_id,
							cp.updated_on,
							c.company_name
						FROM
							clients c
							JOIN properties p	ON ( c.id = p.cid AND c.company_status_type_id IN ( ' . implode( ',', $arrstrCompanyStatusType ) . ' ) AND p.disabled_on IS NULL )
							JOIN contract_properties cp ON ( cp.property_id = p.id AND cp.cid = p.cid )
							JOIN contracts cts ON ( cts.id = cp.contract_id AND cts.cid = cp.cid AND cts.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . ' )
						WHERE
							' . $strTerminationDateWhereClause . '
							AND cp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' ) ' .
		          $strClientsWhereClause .
		          $strPropertiesWhereClause .
		          ')
						UNION
						(
							SELECT
								p.id,
								p.property_name, ' . $strAdditionalField . '
								p.cid,
								sc.ps_product_id,
								sc.updated_on,
								c.company_name
							FROM
								clients c
								JOIN properties p ON ( c.id = p.cid AND c.company_status_type_id IN ( ' . implode( ',', $arrstrCompanyStatusType ) . ' ) AND p.disabled_on IS NULL )
								JOIN simple_contracts sc ON( p.cid = sc.cid AND ( p.id = sc.property_id OR sc.property_id IS NULL ) )
							WHERE
								( sc.expire_on > NOW() OR sc.expire_on IS NULL )
								AND sc.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' ) ' .
		          $strClientsWhereClause .
		          $strPropertiesWhereClause .
		          ' )' . $strOrderByClause;

		$arrstrTempPropertiesProductsData = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrstrTempPropertiesProductsData ) ) ? $arrstrTempPropertiesProductsData : [];
	}

	public static function fetchSimpleProperties( $objDatabase, $arrintCids = NULL, $arrintPropertyIds = NULL ) {

		$strSql = 'SELECT id, property_name FROM properties WHERE is_disabled <> 1';

		$strSql .= ( true == valArr( $arrintCids ) ) ? ' AND cid IN ( ' . implode( ',', $arrintCids ) . ' )' : '';
		$strSql .= ( true == valArr( $arrintPropertyIds ) ) ? ' AND id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )' : '';

		$arrmixProperties = ( array ) fetchData( $strSql, $objDatabase );

		$arrstrProperties = [];

		foreach( $arrmixProperties as $arrmixProperty ) {
			$arrstrProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
		}

		return $arrstrProperties;
	}

	public static function fetchEntrataPropertiesByIds( $intCid, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						properties
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_type_id NOT IN ( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ')
						AND is_disabled = 0
					ORDER BY property_name';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function checkStudentPropertiesByPropertyGroupIdsByPsProductIds( $arrintPropertyGroupIds, $arrintPsProductIds, $intCid, $objDatabase, $boolIsReturnPropertyIds = false ) {

		if( false == valArr( $arrintPropertyGroupIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						lp.property_id
					FROM
					load_properties ( ARRAY [ ' . ( int ) $intCid . ' ], ARRAY [ ' . implode( ', ', $arrintPropertyGroupIds ) . ' ], ARRAY [ ' . implode( ', ', $arrintPsProductIds ) . ' ] ) lp
					WHERE
						lp.is_student_property = 1 ';

		$arrmixPropertyIds = fetchData( $strSql, $objDatabase );

		if( true == $boolIsReturnPropertyIds ) {
			return $arrmixPropertyIds;
		}

		if( true == valArr( $arrmixPropertyIds ) ) {
			return true;
		}

		return false;
	}

	public static function fetchBluemoonIntegratedPropertiesByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						p.id,
						p.property_name
					FROM
						properties p
						JOIN ps_lead_remote_properties plrp ON( p.id = plrp.property_id AND p.cid = plrp.cid )
					WHERE
						p.cid = \'' . ( int ) $intCid . '\'
						AND plrp.is_disabled = 0
						AND p.is_disabled = 0
						AND plrp.bluemoon_remote_primary_key IS NOT NULL
						AND plrp.bluemoon_license_number IS NOT NULL
					ORDER BY p.property_name';

		$arrmixPropertiesData = fetchData( $strSql, $objDatabase );

		$arrintResult = [];

		if( true == valArr( $arrmixPropertiesData ) ) {
			foreach( $arrmixPropertiesData as $arrmixPropertyData ) {
				$arrintResult[$arrmixPropertyData['id']] = $arrmixPropertyData['property_name'];
			}
		}

		return $arrintResult;

	}

	public static function fetchNumberOfUnitsByCidsByPropertyIds( $arrintCids, $arrintPropertyIds, $strGroupBy, $objDatabase ) {
		if( false == valArr( $arrintCids ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strWhere         = '';
		$strSelect        = ' sum( number_of_units )  AS contracted_units, cid ';
		$strGroupByClause = ' cid ';

		if( 'property' == $strGroupBy ) {
			if( false == valArr( $arrintPropertyIds ) ) {
				return;
			}

			$strWhere         = ' AND id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
			$strSelect        = ' sum( number_of_units )  AS contracted_units, cid || \'_\' || id AS cid  ';
			$strGroupByClause .= ', id ';
		}

		$strSql = 'SELECT
						' . $strSelect . '
						
					FROM
						properties p
					WHERE
						cid IN ( ' . implode( ',', $arrintCids ) . ' )
						' . $strWhere . '
						AND is_disabled <> 1
						AND id in ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
					GROUP BY
						
						' . $strGroupByClause . '
					ORDER BY
						cid';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSelectedRenewalPropertiesByPropertyIdsOrPropertyGroupIdsByCid( $arrmixPropertiesData, $intCid, $objDatabase ) {

		if( false == valArr( $arrmixPropertiesData ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON ( p.id ) p.id
					FROM
						properties p JOIN property_group_associations pga ON ( p.cid = pga.cid AND p.id = pga.property_id )
						JOIN property_groups pg ON ( pga.cid = pg.cid AND pga.property_group_id = pg.id )
						JOIN property_products pp ON ( 	p.cid = pp.cid
														AND ( p.id = pp.property_id OR pp.property_id IS NULL )
														AND pp.ps_product_id IN ( ' . CPsProduct::ENTRATA . ', ' . CPsProduct::LEASE_EXECUTION . ' )
													)
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND (
								pg.id IN ( ' . sqlIntImplode( $arrmixPropertiesData ) . ' )
								OR p.id IN ( ' . sqlIntImplode( $arrmixPropertiesData ) . ' )
							)
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL';

		$arrmixPropertiesData = fetchData( $strSql, $objDatabase );

		$arrintPropertyIds = [];

		if( true == valArr( $arrmixPropertiesData ) ) {
			foreach( $arrmixPropertiesData as $arrmixPropertyData ) {
				$arrintPropertyIds[] = $arrmixPropertyData['id'];
			}
		}

		return $arrintPropertyIds;
	}

	public static function fetchPropertyIdsByInvoiceIds( $arrintInvoiceIds, $objDatabase ) {
		if( false == valArr( $arrintInvoiceIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id AS property_id
					FROM
						properties p
						JOIN contract_properties cp ON ( p.id = cp.property_id )
						JOIN transactions t ON ( cp.id = t.contract_property_id )
						JOIN invoices i ON ( i.id = t.invoice_id )
					WHERE
						i.id IN ( ' . implode( ', ', $arrintInvoiceIds ) . ' )
					GROUP BY
						p.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertiesIdsByCids( $arrintPropertyIds, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					id,
					property_name
				FROM
					properties
				WHERE
					id IN( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
					AND cid IN( ' . sqlIntImplode( $arrintCids ) . ' )';

		$arrmixRecords = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixRecords ) ) {
			return NULL;
		}

		$arrmixProperties = [];

		foreach( $arrmixRecords as $arrmixProperty ) {
			$arrmixProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
		}

		return $arrmixProperties;
	}

	public static function fetchPropertyWithTimezoneByIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						p.id,
						p.property_name,
						tz.time_zone_name
					FROM
						properties p
						JOIN time_zones tz ON( p.time_zone_id = tz.id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id = ' . ( int ) $intPropertyId . '';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimplePropertiesByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objDatabase, $arrintPsProductIds = NULL ) {
		if( false == valArr( $arrintPropertyGroupIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $arrintPropertyGroupIds ) . ' ] ';

		if( true == valArr( $arrintPsProductIds ) ) {
			$strSql .= ' , ARRAY[ ' . implode( ',', $arrintPsProductIds ) . ' ] ';
		}

		$strSql .= ' )';

		$arrmixPropertyIds = fetchData( $strSql, $objDatabase );

		$arrintPropertyIds = ( true == valArr( $arrmixPropertyIds ) ) ? rekeyArray( 'property_id', $arrmixPropertyIds ) : [];

		return $arrintPropertyIds;
	}

	/**
	 * @param $intCustomerId
	 * @param $intCid
	 * @param $objClientDatabase
	 *
	 * @return array
	 *
	 * Fetch property id's from Property by customer id and client id.
	 */
	public static function fetchCustomPropertyIdsByCustomerIdByCid( $intCustomerId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( p.id ) p.id
					FROM
						customers cc,
						lease_customers clc,
						leases cl,
						properties p
					WHERE
						cc.id = clc.customer_id AND cc.cid = clc.cid
						AND cl.id = clc.lease_id AND cl.cid = clc.cid
						AND p.id = cl.property_id AND p.cid = cl.cid
						AND cc.id = ' . ( int ) $intCustomerId . '
						AND p.cid = ' . ( int ) $intCid;

		$arrintPropertyIdsByCustomerIds = ( array ) fetchData( $strSql, $objClientDatabase );

		$arrintPropertyIds = [];
		if( true === valArr( $arrintPropertyIdsByCustomerIds ) ) {
			foreach( $arrintPropertyIdsByCustomerIds as $arrintPropertyIdsByCustomerId ) {
				$arrintPropertyIds[] = $arrintPropertyIdsByCustomerId['id'];
			}
		}

		return $arrintPropertyIds;
	}

	public static function fetchPropertiesByNameOrByAddress( $intCid, $strSearchString, $boolIsPropertyName, $arrintPropertyIds, $objClientDatabase ) {

		if( true == empty( $strSearchString ) && true == empty( $strAddress ) ) {
			return NULL;
		}

		if( true == $boolIsPropertyName ) {
			$strWhereCondition = 'p.property_name ilike \'%' . $strSearchString . '%\' ';
		} else {
			$strWhereCondition = 'pa.street_line1 ilike \'%' . $strSearchString . '%\' ';
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.number_of_units,
						p.termination_date,
						p.is_disabled,
						p.is_managerial,
						p.country_code,
						pa.street_line1,
						pa.street_line2,
						pa.city,
						pa.state_code,
						pa.postal_code
					FROM
						properties p
						JOIN property_addresses pa ON ( p.cid = pa.cid AND p.id = pa.property_id AND pa.address_type_id = ' . CAddressType::PRIMARY . ' AND pa.is_alternate = false )
					WHERE
					' . $strWhereCondition . '
					AND p.cid = ' . ( int ) $intCid . '
					AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPropertiesByApExportTypeByCompanyUserIdByCid( $intApExportTypeId, $intCompanyUserId, $intCid, $boolIsAdministrator, $objDatabase ) {
		$strJoin         = '';
		$strAndCondition = '';

		if( false == $boolIsAdministrator ) {
			$strJoin .= 'JOIN view_company_user_properties vcup ON ( vcup.company_user_id = ' . ( int ) $intCompanyUserId . ' AND vcup.property_id = p.id AND vcup.cid = p.cid ) ';
		}

		if( false != is_numeric( $intApExportTypeId ) ) {
			$strJoin         .= ' JOIN property_preferences ppr ON ( p.id = ppr.property_id AND p.cid = ppr.cid ) ';
			$strAndCondition .= ' AND ppr.key = \'AP_EXPORT_BATCH_TYPE_SELECT\' AND ppr.value =\'' . $intApExportTypeId . '\'';
		}

		$strSql = 'SELECT
						DISTINCT ON( lower( p.property_name ), order_num, p.id )
						p.*,
						pgs.activate_standard_posting
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id AND p.is_disabled = 0 AND pgs.activate_standard_posting = true )
						JOIN property_products pp ON ( pgs.cid = pp.cid AND ( p.id = pp.property_id OR pp.property_id IS NULL ) )
						' . $strJoin . '
					WHERE
						p.cid = ' . ( int ) $intCid . '

						' . $strAndCondition . '
					ORDER BY
						lower( p.property_name ) ASC,
						order_num';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchIntegratedPropertyIdAndNameByCids( $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ( p.id ),
						p.*
					FROM
						properties p
						JOIN property_integration_databases pid ON( pid.cid = p.cid AND pid.property_id = p.id )
					WHERE
						p.cid IN (' . implode( ',', $arrintCids ) . ')
						AND p.is_disabled = 0
					ORDER BY
						p.property_name';

		return self::fetchProperties( $strSql, $objDatabase );

	}

	public static function fetchPropertiesByIntegrationDatabaseIdByCids( $intIntegrationDatabaseId, $arrintCids, $objClientDatabase ) {
		if( false == is_numeric( $intIntegrationDatabaseId ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON ( p.property_name, p.id )
						p.*
					FROM
						properties p,
						property_integration_databases pid,
						integration_databases id
					WHERE
						p.id = pid.property_id
						AND p.cid = pid.cid
						AND pid.integration_database_id = id.id
						AND pid.cid = id.cid
						AND id.id = ' . ( int ) $intIntegrationDatabaseId . '
						ANd id.cid IN ( ' . implode( ',', $arrintCids ) . ' )
					ORDER BY
						p.property_name, p.id';

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyIdsByPropertyGroupIdByCid( $intPropertyGroupId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . ( int ) $intPropertyGroupId . ' ] ' . ' )';

		$arrmixPropertyIds = fetchData( $strSql, $objDatabase );

		$arrintPropertyIds = ( true == valArr( $arrmixPropertyIds ) ) ? rekeyArray( 'property_id', $arrmixPropertyIds ) : [];

		return array_keys( $arrintPropertyIds );
	}

	public static function fetchPropertyContactInfoByPropertyIdsByCids( $arrintPropertyIds, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						pea.email_address as property_email_address,
						ppn.phone_number as property_phone_number
					FROM
						properties p
						LEFT JOIN property_email_addresses pea ON ( pea.cid = p.cid AND pea.property_id = p.id AND email_address_type_id = ' . CEmailAddressType::PRIMARY . ' )
						LEFT JOIN property_phone_numbers ppn ON ( ppn.cid = p.cid AND ppn.property_id = p.id AND ppn.phone_number_type_id = ' . CPhoneNumberType::OFFICE . ' )
					WHERE
						p.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchCorporatePropertiesForApplicationByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						p.id,
						p.property_name
					FROM
						properties p
						JOIN property_applications pa ON ( p.cid = pa.cid AND p.id = pa.property_id )
						JOIN company_applications ca ON ( pa.company_application_id = ca.id AND pa.cid = ca.cid )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.is_disabled = 0
						AND ca.is_published = 1
						AND ca.deleted_on IS NULL
					ORDER BY lower ( p.property_name )';

		$arrmixRecords = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixRecords ) ) {
			return NULL;
		}

		foreach( $arrmixRecords as $arrmixProperty ) {
			$arrmixProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
		}

		return $arrmixProperties;
	}

	// TODO: remove this function once done refactoring for all invoce architectures.

	public static function fetchCustomPropertiesWithBankAccountDetailsByTransactionsFilterByCid( $intCid, $objDatabase, $objCompanyUser, $objTransactionsFilter = NULL ) : array {

		$strWhereCondition = ( true == valObj( $objTransactionsFilter, 'CApTransactionsFilter' ) && true == valArr( $objTransactionsFilter->getPropertyIds() ) ) ? 'AND pgs.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )' : 'AND props.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )';
		$strSql            = 'SELECT
						props.id AS property_id,
						CASE
							WHEN props.lookup_code IS NULL THEN props.property_name
							ELSE props.lookup_code || \' - \' || props.property_name
						END AS property_name,
						to_char( pgs.ap_post_month, \'MM/YYYY\') AS property_ap_post_month,
						0 as is_allocation,
						0 as is_gl_account_associated,
						ba.bank_account_type_id,
						ba.reimbursed_property_id,
						pgs.is_ap_migration_mode
					FROM
						( ' . CProperties::buildCompanyUserPropertiesSql( $intCid, $objCompanyUser->getId(), $objCompanyUser->getIsAdministrator() ) . ' ) AS props
						JOIN property_gl_settings pgs ON ( props.cid = pgs.cid AND props.id = pgs.property_id )
						LEFT JOIN bank_accounts ba ON ( pgs.cid = ba.cid AND pgs.ap_bank_account_id = ba.id )
					WHERE
						props.cid = ' . ( int ) $intCid . ' ' . $strWhereCondition . '
					ORDER BY
						props.property_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchNonAssociatedPropertiesByCid( $intCid, $objDatabase ) {

		if( false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id
					FROM
						Properties p
						LEFT JOIN contract_properties cp ON ( p.id = cp.property_id AND cp.close_date IS NOT NULL AND cp.renewal_contract_property_id IS NULL AND cp.is_last_contract_record = true AND cp.termination_date IS NULL AND cp.contract_termination_request_id IS NULL )
					WHERE
						cp.id IS NULL
						AND p.cid = ' . ( int ) $intCid;

		$arrmixProperties = ( array ) fetchData( $strSql, $objDatabase );

		return $arrmixProperties;
	}

	public static function fetchAllPropertiesWithDefaultGlAccountByCid( $intCid, $objDatabase ) {

		if( false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						apl.ap_payee_id,
						apl.id AS ap_payee_location_id,
						p.id AS property_id,
						pgt.system_code,
						appg.default_gl_account_id
					FROM
						properties p
						LEFT JOIN property_group_associations pga ON ( pga.cid = p.cid AND pga.property_id = p.id )
						LEFT JOIN ap_payee_property_groups appg ON ( appg.cid = pga.cid AND appg.property_group_id = pga.property_group_id )
						JOIN property_groups pg ON ( pg.cid = appg.cid AND pg.id = appg.property_group_id )
						JOIN property_group_types pgt ON ( pgt.cid = pg.cid AND pgt.id = pg.property_group_type_id AND pgt.deleted_by IS NULL AND pgt.deleted_on IS NULL )
						JOIN ap_payee_locations apl ON ( apl.cid = appg.cid AND apl.ap_payee_id = appg.ap_payee_id AND apl.id = appg.ap_payee_location_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND appg.default_gl_account_id IS NOT NULL
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchNonIntegratedEntrataCorePropertyIdsByCids( $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.cid
					FROM
						properties p
						JOIN load_properties( ARRAY[ ' . implode( ',', $arrintCids ) . ' ], NULL, ARRAY[ ' . ( int ) CPsProduct::ENTRATA . ' ] ) lp ON( lp.cid = p.cid AND lp.property_id = p.id )
						LEFT JOIN property_integration_databases pid ON ( p.cid = pid.cid AND p.id = pid.property_id )
					WHERE
						p.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND p.is_disabled = 0
						AND pid.id IS NULL;';

		$arrmixProperties = ( array ) fetchData( $strSql, $objDatabase );

		$arrintNonintegratedPropertyIds = [];

		foreach( $arrmixProperties as $arrmixProperty ) {
			$arrintNonintegratedPropertyIds[$arrmixProperty['cid']][$arrmixProperty['id']] = $arrmixProperty['id'];
		}

		return $arrintNonintegratedPropertyIds;
	}

	public static function fetchIntegratedPropertyIdsByIdsByIntegrationClientTypeIdByCid( $arrintPropertyIds, $intIntegrationClientTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						pid.property_id
					FROM
						property_integration_databases pid
						JOIN integration_databases id ON ( pid.integration_database_id = id.id AND pid.cid = id.cid )
					WHERE
						pid.cid = ' . ( int ) $intCid . '
						AND id.integration_client_type_id = ' . ( int ) $intIntegrationClientTypeId . '
						AND pid.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		$arrmixResponseData = fetchData( $strSql, $objDatabase );

		$arrintPropertyIds = [];

		if( true == valArr( $arrmixResponseData ) ) {
			foreach( $arrmixResponseData as $arrmixResponse ) {
				$arrintPropertyIds[] = $arrmixResponse['property_id'];
			}
		}

		return $arrintPropertyIds;
	}

	public static function fetchPropertiesByCidByPsProductIds( $intCid, $arrintPsProductIds, $objDatabase ) {

		$strSql = 'SELECT
						pp.ps_product_id,
						p.id AS property_id
					FROM
						properties p
						JOIN property_products pp ON ( pp.cid = p.cid AND pp.property_id IS NULL )
					WHERE
						pp.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
						AND pp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
					GROUP BY
							pp.ps_product_id, p.id
					UNION
					SELECT
						pp.ps_product_id,
						p.id AS property_id
					FROM
						properties p
						JOIN property_products pp ON ( pp.cid = p.cid AND pp.property_id = p.id )
					WHERE
						pp.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
						AND pp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
					GROUP BY
							pp.ps_product_id, p.id';

		$arrstrData = fetchData( $strSql, $objDatabase );

		$arrstrFinalData = [];

		foreach( $arrintPsProductIds as $intProductId ) {
			$arrstrFinalData[$intProductId]['property_ids'] = [];
		}

		if( true == valArr( $arrstrData ) ) {

			foreach( $arrstrData as $arrstrChunk ) {

				$arrstrFinalData[$arrstrChunk['ps_product_id']]['property_ids'][$arrstrChunk['property_id']] = $arrstrChunk['property_id'];

				if( true == $boolFetchPsProductOptionProperties && true == is_numeric( $arrstrChunk['ps_product_option_id'] ) ) {

					if( true == is_numeric( $arrstrChunk['ps_product_id'] ) ) {
						$arrstrFinalData[$arrstrChunk['ps_product_id']]['ps_product_option_ids'][$arrstrChunk['ps_product_option_id']][] = $arrstrChunk['property_id'];
					} else {
						$arrstrFinalData[$arrstrChunk['ps_product_id']]['ps_product_option_ids'][$arrstrChunk['ps_product_option_id']] &= $arrstrFinalData[$arrstrChunk['ps_product_id']]['property_ids'];
					}
				}
			}
		}

		return $arrstrFinalData;
	}

	public static function fetchPropertiesWithBankAccountDetailsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $objCompanyUser, $boolShowDisabledData = false ) {

		if( false == valIntArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						props.id AS property_id,
						CASE
							WHEN props.lookup_code IS NULL THEN props.property_name
							ELSE props.lookup_code || \' - \' || props.property_name
						END AS property_name,
						to_char( pgs.ap_post_month, \'MM/YYYY\') AS ap_post_month,
						ba.bank_account_type_id,
						ba.reimbursed_property_id,
						pgs.is_ap_migration_mode
					FROM
						( ' . CProperties::buildCompanyUserPropertiesSql( $intCid, $objCompanyUser->getId(), $objCompanyUser->getIsAdministrator(), true, false, false, [], false, $boolShowDisabledData ) . ' ) AS props
						JOIN property_gl_settings pgs ON ( props.cid = pgs.cid AND props.id = pgs.property_id )
						LEFT JOIN bank_accounts ba ON ( pgs.cid = ba.cid AND pgs.ap_bank_account_id = ba.id )
					WHERE
						props.cid = ' . ( int ) $intCid . '
						AND pgs.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					ORDER BY
						props.property_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchIntegratedPropertiesByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $arrstrSelectFields = [] ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSelectFields = 'DISTINCT ON( p.id )
							p.*,
							ict.id AS integration_client_type_id';

		if( true == valArr( $arrstrSelectFields ) ) {
			$strSelectFields = implode( ',', $arrstrSelectFields );
		}

		$strSql = 'SELECT
						' . $strSelectFields . '
					FROM
						properties p
						JOIN property_integration_databases pid ON ( p.id = pid.property_id AND p.cid = pid.cid )
						JOIN integration_databases id ON ( id.cid = pid.cid AND id.id = pid.integration_database_id )
						JOIN integration_client_types ict ON ( ict.id = id.integration_client_type_id AND id.cid = ' . ( int ) $intCid . ' )
					WHERE
						p.remote_primary_key IS NOT NULL
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.cid = ' . ( int ) $intCid;

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPricingPropertiesByUnitSpaceStatusTypeIdsByCid( $arrintUnitSpaceStatusTypeIds, $intCid, CDatabase $objDatabase ) {

		if( false == valArr( $arrintUnitSpaceStatusTypeIds ) ) {
			return NULL;
		}

		$strIsPublishedCondition          = 'is_marketed = true ';
		$strCheckSubsidizedUnitSpacesSql1 = ( 1 == $objDatabase->getClusterId() ) ? ' AND ( us1.occupancy_type_id != ' . ( int ) CPropertyType::SUBSIDIZED . ' OR us1.occupancy_type_id IS NULL )' : '';
		$strCheckSubsidizedUnitSpacesSql2 = ( 1 == $objDatabase->getClusterId() ) ? ' AND ( us2.occupancy_type_id != ' . ( int ) CPropertyType::SUBSIDIZED . ' OR us2.occupancy_type_id IS NULL )' : '';

		$strSql = 'SELECT
						p.cid,
						p.id AS property_id,
						p.property_name,
						COUNT ( ut.id ) AS available_unit_types,
						(
							SELECT
								COUNT ( us1.id )
							FROM
								unit_spaces us1
							WHERE
								us1.cid = p.cid
								AND us1.property_id = p.id
								AND us1.' . $strIsPublishedCondition . '
								' . $strCheckSubsidizedUnitSpacesSql1 . '
								AND us1.is_marketed
								AND us1.deleted_on IS NULL
						) AS rentable_units_count,
						(
							SELECT
								COUNT ( us2.id )
							FROM
								unit_spaces us2
							WHERE
								us2.cid = p.cid
								AND us2.property_id = p.id
								AND us2.is_available
								AND us2.' . $strIsPublishedCondition . '
								' . $strCheckSubsidizedUnitSpacesSql2 . '
								AND us2.is_marketed
								AND us2.deleted_on IS NULL
						) AS available_units_count,
						(
							SELECT
								MAX ( rpr.process_datetime )
							FROM
								revenue_post_rents rpr
							WHERE
								rpr.cid = p.cid
								AND rpr.property_id = p.id
								AND rpr.process_datetime IS NOT NULL
						) AS latest_process_datetime
					FROM
						properties p
						JOIN property_products pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.ps_product_id = ' . CPsProduct::PRICING . ' )
						LEFT JOIN unit_types ut ON ( ut.cid = p.cid AND ut.property_id = p.id AND ut.is_occupiable = 1 )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
						AND p.is_test = 0
					GROUP BY
						p.cid,
						p.id,
						p.property_name
					ORDER BY
						p.cid,
						p.property_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActivePropertiesByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objDatabase, $boolIsReturnKeyedArray = true ) {
		if( false == valArr( $arrintPropertyGroupIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						pga.property_group_id as property_group_id
					FROM
						properties p
						JOIN property_group_associations pga ON ( p.id = pga.property_id AND p.cid = pga.cid )
					WHERE
						pga.property_group_id IN ( ' . implode( ',', $arrintPropertyGroupIds ) . ' )
						AND p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
						AND p.is_managerial = 0
					ORDER BY
						p.property_name';

		return self::fetchProperties( $strSql, $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchApPayeePropertiesByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objDatabase ) {

		if( false == ( $arrintPropertyGroupIds = getIntValuesFromArr( $arrintPropertyGroupIds ) ) ) {
			return NULL;
		}

		$strSql = '
					SELECT
						p.id,
						p.property_name
					FROM
						properties p
						JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INT[], ARRAY[ ' . sqlIntImplode( $arrintPropertyGroupIds ) . ' ]::INT[], ARRAY[ ' . CPsProduct::ENTRATA . ' ]::INT[], TRUE ) lp ON lp.cid = p.cid AND lp.property_id = p.id
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
					ORDER BY
						p.property_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveNonManagerialPropertiesByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false === valArr( $arrintPropertyIds ) || false === valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.*
					FROM
						properties p
					WHERE
						p.is_disabled = 0
						AND p.is_managerial = 0
						AND p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
					ORDER BY
							lower ( p.property_name )';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertiesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.remote_primary_key
					FROM
						properties p
						JOIN property_integration_databases pid ON( p.id = pid.property_id AND p.cid = pid.cid )
						JOIN integration_databases id ON( pid.cid = id.cid AND pid.integration_database_id = id.id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN (' . implode( ',', $arrintPropertyIds ) . ' )
						AND pid.cid = ' . ( int ) $intCid . '
						AND id.deleted_on IS NULL
						AND id.integration_client_status_type_id != ' . CIntegrationClientStatusType::DISABLED . '
						AND id.integration_client_type_id NOT IN ( ' . implode( ',', CIntegrationClientType::$c_arrintMigrateIntegrationClientTypeIds ) . ' );';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPmEnabledPropertiesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name
					FROM
						properties p
						JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( ',', $arrintPropertyIds ) . ']::INTEGER[], ARRAY[1]::INTEGER[] ) lp ON ( p.cid = lp.cid AND p.id = lp.property_id AND lp.is_disabled = 0 )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN (' . implode( ',', $arrintPropertyIds ) . ' )
					ORDER by p.property_name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertiesByPsProductIdByCids( $intPsProductId, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id, p.property_name, p.cid
					FROM
						properties p
						JOIN property_products AS pp ON ( pp.property_id = p.id OR pp.property_id IS NULL )
					WHERE
						pp.cid = p.cid
						AND pp.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND pp.ps_product_id = ' . ( int ) $intPsProductId . '
						AND p.is_disabled = 0';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAccessforBalancePropertyPreferencesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT pp1.property_id,
						(CASE WHEN pp1.id IS NOT NULL THEN 1 ELSE 0 END ) AS value
					FROM
						properties p
						JOIN property_preferences pp ON ( p.cid = pp.cid AND p.id = pp.property_id AND pp.key = \'SHOW_RESIDENT_BALANCE\' AND pp.value IS NOT NULL )
						JOIN property_preferences pp1 ON ( p.cid = pp1.cid AND p.id = pp1.property_id AND pp1.key = \'SHOW_CREDIT_BALANCES\' AND pp1.value IS NOT NULL )
						JOIN property_products AS ppd ON ( p.cid = ppd.cid AND p.id = ppd.property_id )
					WHERE
						( p.remote_primary_key IS NOT NULL OR ppd.ps_product_id = ' . ( int ) CPsProduct::ENTRATA . ' )
						AND p.cid = ' . ( int ) $intCid . '
						AND p.id IN	( ' . implode( ', ', $arrintPropertyIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEntrataCorePMIntegaredPropertiesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT p.id as property_id
					FROM
						properties p
						JOIN property_products AS ppd ON ( p.cid = ppd.cid AND p.id = ppd.property_id )
					WHERE
						( p.remote_primary_key IS NOT NULL OR ppd.ps_product_id = ' . ( int ) CPsProduct::ENTRATA . ' )
						AND p.cid = ' . ( int ) $intCid . '
						AND p.id IN	( ' . implode( ', ', $arrintPropertyIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertiesForMakePaymentAccessByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase, $boolCheckForAutopayment = false ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strWhereCondition = '';
		if( true == $boolCheckForAutopayment ) {
			$strWhereCondition = 'JOIN property_preferences pp1 ON (p.id = pp1.property_id AND p.cid = pp1.cid AND pp1.key = \'ALLOW_STATIC_AND_VARIABLE_RECURRING_PAYMENTS\' AND pp1.value::int != ' . ( int ) CScheduledPayment::DO_NOT_ALLOW_RECURR_PAYMENTS . ' ) ';
		}

		$strSql = 'SELECT
						DISTINCT( p.id ) as property_id
					FROM
						properties AS p
						JOIN property_products pps ON (p.id = pps .property_id AND p.cid = pps.cid )
						JOIN property_merchant_accounts pma ON (pma.property_id = p.id AND p.cid = pma.cid )
						JOIN property_preferences pp ON (p.id = pp .property_id AND p.cid = pp.cid )
						JOIN property_preferences pp2 ON ( p.id = pp2 .property_id AND p.cid = pp2.cid AND pp2.key = \'PAYMENTS_TAB_STATUS\' AND pp2.value IS NOT NULL )
						' . $strWhereCondition . '
					WHERE
						( pps.ps_product_id = ' . \CPsProduct::RESIDENT_PAY . ' OR ( pp.key = \'RESIDENT_PORTAL_3RD_PARTY_PAYMENT_URL\' AND pp.value IS NOT NULL ) )
						AND pma.ar_code_id IS NULL AND pma.disabled_by IS NULL AND pma.disabled_on IS NULL
						AND p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchSimplePropertyDetailsByIds( $arrintPropertyIds, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						id,
						property_name,
						cid
					FROM
						properties
					WHERE
						id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND is_disabled <> 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAffordablePropertiesByCid( $intCid, $objDatabase, $arrintPropertyIds = NULL, $boolIsRekeyedDataRequired = true, $arrintSubsidyTypeIds = [], $boolShowDisabledData = false, $boolIncludeIsInitialImportValidationMode = false ) {

		$strPropertyIds    = ( true == valArr( $arrintPropertyIds ) ) ? 'ARRAY[' . implode( ',', $arrintPropertyIds ) . ']' : '\'{}\'::INTEGER[]';
		$strSubsidyTypeIds = ( true == valArr( $arrintSubsidyTypeIds ) ) ? 'ARRAY[' . implode( ',', $arrintSubsidyTypeIds ) . ']' : '\'{}\'::INTEGER[]';

		$strSql = 'SELECT * FROM load_affordable_properties
			(
				' . $strPropertyIds . ',
				ARRAY[' . $intCid . '],
				' . ( false == $boolShowDisabledData ? 'TRUE' : 'FALSE' ) . ',
				' . $strSubsidyTypeIds . ',
				' . ( true == $boolIncludeIsInitialImportValidationMode ? 'TRUE' : 'FALSE' ) . '
			)';

		$arrstrPropertiesData = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrstrPropertiesData ) ) {
			return [];
		}
		if( false == $boolIsRekeyedDataRequired ) {
			return $arrstrPropertiesData;
		}
		if( true == valArr( $arrstrPropertiesData ) ) {
			foreach( $arrstrPropertiesData as $arrstrData ) {
				$arrstrProperties[$arrstrData['property_id']] = $arrstrData['property_name'];
			}
		}

		return $arrstrProperties;
	}

	public static function fetchMilitaryPropertiesByCid( $intCid, $objDatabase, $arrintPropertyIds = NULL, $boolIsRekeyedDataRequired = true, $boolCheckMilitaryInstallation = false, $boolCheckMilitaryOccupancyType = false ) {
		$strPropertyIds              = ( true == valArr( $arrintPropertyIds ) ) ? 'ARRAY[' . implode( ',', $arrintPropertyIds ) . ']' : '\'{}\'';
		$strSqlMilitaryInstallation  = ( true == $boolCheckMilitaryInstallation ) ? ' JOIN property_details pd ON ( pd.cid = p.cid AND pd.property_id = p.id AND pd.military_installation_id IS NOT NULL )' : '';
		$strSqlMilitaryOccupancyType = ( true == $boolCheckMilitaryOccupancyType ) ? ' AND ( p.occupancy_type_ids::TEXT IS NOT NULL AND ' . COccupancyType::MILITARY . ' = ANY( p.occupancy_type_ids ) )' : '';

		$strSql = ' SELECT
						p.cid,
						p.id AS property_id,
						p.property_name,
						p.is_disabled,
						p.is_test
					FROM
						properties p
						LEFT JOIN property_products pp ON ( pp.cid = p.cid AND ( pp.property_id = p.id OR pp.property_id IS NULL ) )
						' . $strSqlMilitaryInstallation . '
					WHERE
						pp.ps_product_id = ' . CPsProduct::ENTRATA_MILITARY . '
						' . $strSqlMilitaryOccupancyType . '
						AND p.termination_date IS NULL
						AND CASE
							WHEN array_length( ' . $strPropertyIds . ', 1 ) > 0
								THEN p.id = ANY ( ' . $strPropertyIds . ' )
							ELSE
								TRUE
							END
						AND p.cid = ' . ( int ) $intCid;

		$arrstrPropertiesData = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrstrPropertiesData ) ) {
			return [];
		}
		if( false == $boolIsRekeyedDataRequired ) {
			return $arrstrPropertiesData;
		}
		if( true == valArr( $arrstrPropertiesData ) ) {
			foreach( $arrstrPropertiesData as $arrstrData ) {
				$arrstrProperties[$arrstrData['property_id']] = $arrstrData['property_name'];
			}
		}

		return $arrstrProperties;
	}

	public static function fetchMilitaryPropertiesCountByProductIdByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT COUNT(pp.id) AS property_count FROM property_products pp WHERE pp.cid =' . ( int ) $intCid . ' AND pp.ps_product_id = ' . CPsProduct::ENTRATA_MILITARY;

		$arrintCount = fetchData( $strSql, $objDatabase );

		return ( true == isset( $arrintCount[0]['property_count'] ) && 0 < $arrintCount[0]['property_count'] ) ? $arrintCount[0]['property_count'] : 0;
	}

	public static function fetchIntegratedPropertiesByPropertyNameByCid( $strPropertyName, $intCid, $objDatabase ) {

		$strSql = '	SELECT
						p.*
					FROM
						properties p
						JOIN property_integration_databases pid ON ( p.id = pid.property_id AND p.cid = pid.cid )
					WHERE
						p.property_name ilike \'' . $strPropertyName . '%\'
						AND p.remote_primary_key IS NOT NULL
						AND p.is_disabled = 0
						AND p.CID =' . ( int ) $intCid;

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCalenderInformationSetPropertyPreferencesByPropertyIdsByCid( $arrstrKeys, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ( p.id ) as property_id,
						CASE
							WHEN
								pp.value = \'ENTRATA\'
							THEN pp.value
							WHEN
								pp.value IS NULL
							THEN NULL
							ELSE pp1.value
						END	as value
					FROM
						properties p
						LEFT JOIN property_products ppr ON ( ppr.cid = p.cid AND p.id = ppr.property_id AND ppr.ps_product_id = ' . CPsProduct::LEASING_CENTER . ' )
						LEFT JOIN property_preferences pp ON ( p.cid = pp.cid AND p.id = pp.property_id AND pp.key = \'LEASING_CENTER_CALENDAR_TYPE\' AND pp.value IS NOT NULL )
						LEFT JOIN property_preferences pp1 ON ( p.cid = pp1.cid AND p.id = pp1.property_id AND pp1.key IN (\'' . implode( "','", $arrstrKeys ) . '\') AND pp1.value IS NOT NULL AND pp1.cid = ' . ( int ) $intCid . ' )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN	( ' . implode( ', ', $arrintPropertyIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByPropertyTypeIdsByPropertyIdsByCid( $arrintPropertyTypeIds, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintPropertyTypeIds ) ) {
			return NULL;
		}

		$strWhereSql = ' AND ' . COccupancyType::STUDENT . ' = ANY( p.occupancy_type_ids ) AND pp.value = \'1\' ';
		$strJoinSql  = ' LEFT JOIN property_preferences pp ON ( p.cid = pp.cid AND p.id = pp.property_id AND pp.key = \'ENABLE_SEMESTER_SELECTION\' ) ';
		if( true == in_array( CPropertyType::APARTMENT, $arrintPropertyTypeIds ) ) {
			$strWhereSql = ' AND ( p.property_type_id = ' . CPropertyType::APARTMENT . ' OR ( ' . COccupancyType::STUDENT . ' = ANY( p.occupancy_type_ids ) AND pp.value = \'1\' ) ) ';
		}

		$strSql = 'SELECT
						DISTINCT( p.id ),
						p.property_type_id
					FROM
						properties p ' . $strJoinSql . '
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN (' . implode( ',', $arrintPropertyIds ) . ' ) ' . $strWhereSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchIntegratedAndNonIntegratedPropertyCountByClientIdPropertyIdByPropertyName( $intClientId, $mixPropertyNameOrPropertyId, $strWebsiteName, $strIntegrationStatus, $objDatabase ) {

		$strWhere = 'p.cid = ' . ( int ) $intClientId . '
						AND p.is_disabled <> 1
						AND p.is_test <> 1
						AND p.termination_date IS NULL';

		$strWhere .= ( true == valStr( $mixPropertyNameOrPropertyId ) ) ? ' AND ( LOWER(p.property_name) like \'%' . \Psi\CStringService::singleton()->strtolower( $mixPropertyNameOrPropertyId ) . '%\' OR p.id = ' . ( int ) $mixPropertyNameOrPropertyId . ' ) ' : '';
		$strWhere .= ( true == valStr( $strWebsiteName ) || true == valId( $strWebsiteName ) ) ? ' AND w.deleted_by IS NULL AND ( w.is_disabled = 0 OR w.is_disabled IS NULL ) AND LOWER(w.name) like \'%' . \Psi\CStringService::singleton()->strtolower( $strWebsiteName ) . '%\'' : '';
		$strWhere .= ( true == valStr( $strIntegrationStatus ) && 'Yes' == $strIntegrationStatus ) ? ' AND pid.property_id IS NOT NULL' : ( ( true == valStr( $strIntegrationStatus ) && 'No' == $strIntegrationStatus ) ? ' AND pid.property_id IS NULL' : '' );

		$strFrom = ( true == valStr( $strWebsiteName ) || true == valId( $strWebsiteName ) ) ? 'LEFT JOIN website_properties wp ON ( p.id = wp.property_id AND p.cid = wp.cid ) LEFT JOIN websites w ON ( wp.website_id = w.id and p.cid = w.cid )' : '';

		$strSql = 'SELECT
						COUNT( distinct( p.id ) ) AS property_count
					FROM
						properties p
						JOIN clients c ON ( p.cid = c.id )
						LEFT JOIN property_integration_databases pid ON ( pid.cid = p.cid AND pid.property_id = p.id )
						' . $strFrom . '
					WHERE ' . $strWhere;

		$arrintCount = fetchData( $strSql, $objDatabase );

		return ( true == isset( $arrintCount[0]['property_count'] ) && 0 < $arrintCount[0]['property_count'] ) ? $arrintCount[0]['property_count'] : 0;
	}

	public static function fetchIntegratedAndNonIntegratedPaginatedPropertiesByClientIdPropertyIdByPropertyName( $intClientId, $mixPropertyNameOrPropertyId, $strWebsiteName, $strIntegrationStatus, $intLimit, $intOffSet, $strOrderBy, $strOrderDirection, $objDatabase ) {

		$strWhere = 'p.cid = ' . ( int ) $intClientId . '
					AND p.is_disabled <> 1
					AND p.is_test <> 1
					AND p.termination_date IS NULL';

		$strWhere .= ( true == valStr( $mixPropertyNameOrPropertyId ) ) ? ' AND ( LOWER(p.property_name) like \'%' . \Psi\CStringService::singleton()->strtolower( $mixPropertyNameOrPropertyId ) . '%\' OR p.id = ' . ( int ) $mixPropertyNameOrPropertyId . ' ) ' : '';
		$strWhere .= ( true == valStr( $strWebsiteName ) || true == valId( $strWebsiteName ) ) ? ' AND w.deleted_by IS NULL AND ( w.is_disabled = 0 OR w.is_disabled IS NULL ) AND LOWER(w.name) like \'%' . \Psi\CStringService::singleton()->strtolower( $strWebsiteName ) . '%\'' : '';
		$strWhere .= ( true == valStr( $strIntegrationStatus ) && 'Yes' == $strIntegrationStatus ) ? ' AND pid.property_id IS NOT NULL' : ( ( true == valStr( $strIntegrationStatus ) && 'No' == $strIntegrationStatus ) ? ' AND pid.property_id IS NULL' : '' );

		$strSelect     = ( true == valStr( $strWebsiteName ) ) ? ' DISTINCT( p.id ), ' : 'p.id, ';
		$strFrom       = ( true == valStr( $strWebsiteName ) ) ? ' LEFT JOIN website_properties wp ON ( p.id = wp.property_id AND p.cid = wp.cid ) LEFT JOIN websites w ON ( wp.website_id = w.id AND p.cid = w.cid ) ' : '';
		$strOrderBySql = ( true == valStr( $strOrderBy ) && 'w.name' != $strOrderBy ) ? ' ORDER BY ' . $strOrderBy . ( true == valStr( $strOrderDirection ) ? ' ' . $strOrderDirection : ' ASC' ) : '';

		$strSql = 'SELECT
						' . $strSelect . '
						p.property_name,
						p.number_of_units AS unit_count,
						( CASE WHEN pid.property_id IS NULL THEN \'Non-integrated\' ELSE \'integrated\' END ) as integration_status
					FROM
						properties p
						JOIN clients c ON ( p.cid = c.id )
						LEFT JOIN property_integration_databases pid ON ( pid.cid = p.cid AND pid.property_id = p.id )
						' . $strFrom . '
					WHERE
						' . $strWhere . '
						' . $strOrderBySql . '
					OFFSET ' . ( int ) $intOffSet . ' LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByPropertyGroupIdsByOccupancyTypeIds( $arrintPropertyGroupIds, $arrintOccupancyTypesIds, $intCid, $objDatabase ) {
		if( true == empty( $arrintPropertyGroupIds ) && true == empty( $arrintOccupancyTypesIds ) ) {
			return false;
		}

		$strSql = 'SELECT
						p.id
					FROM
						properties p
						JOIN property_group_associations pga ON ( pga.property_id = p.id AND pga.cid = ' . ( int ) $intCid . ' )
						JOIN property_groups pg ON( pga.property_group_id =pg.id AND pg.cid = ' . ( int ) $intCid . ' )
					WHERE
						pg.id IN ( ' . implode( ', ', $arrintPropertyGroupIds ) . ' )
						AND p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						AND sort( p.occupancy_type_ids) = sort( ARRAY[' . implode( ', ', $arrintOccupancyTypesIds ) . '] )
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL';

		$arrintResponseData = fetchData( $strSql, $objDatabase );

		$arrintPropertyIds = [];

		if( true == valArr( $arrintResponseData ) ) {
			foreach( $arrintResponseData as $arrintResponse ) {
				$arrintPropertyIds[] = $arrintResponse['id'];
			}
		}

		return $arrintPropertyIds;
	}

	public static function fetchFirstIntegratedPropertyByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT *
					FROM
						properties
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_disabled = 0
						AND id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND remote_primary_key IS NOT NULL
					ORDER BY
						property_name ASC
						LIMIT 1';

		return self::fetchProperty( $strSql, $objDatabase );
	}

	public static function fetchActivePropertiesByCids( $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						id, property_name
					FROM
						properties
					WHERE
						cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						AND is_disabled <> 1
					ORDER BY cid,property_name';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchActivePropertiesByIds( $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		return self::fetchProperties( 'SELECT id, property_name FROM properties WHERE id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND is_disabled <> 1', $objDatabase );
	}

	public static function fetchPropertyNamesByCompanyApplicationIdByCid( $arrintCompanyApplicationIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCompanyApplicationIds ) ) {
			return;
		}

		$strSql = 'SELECT
						DISTINCT ON ( p.id, p.cid )
						p.id,
						p.cid,
						pga.property_group_id,
						p.property_name
					FROM
						properties p
						JOIN property_applications pa ON ( p.id = pa.property_id AND p.cid = pa.cid )
						JOIN property_group_associations pga ON ( pga.property_id = p.id AND pga.cid = p.cid )
					WHERE
						pga.property_group_id = p.id
						AND pa.cid = ' . ( int ) $intCid . '
						AND pa.company_application_id IN ( ' . implode( ', ', $arrintCompanyApplicationIds ) . ' )
						AND pa.is_published = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchParentChildSiblingPropertiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ps.*
					FROM
						properties p
						JOIN properties ps ON ( p.cid = ps.cid AND ( ps.id = p.property_id OR ps.property_id = p.id OR p.property_id = ps.property_id ) AND p.id != ps.id )
					WHERE
						p.id = ' . ( int ) $intPropertyId . '
						AND p.cid = ' . ( int ) $intCid . '
						AND ps.is_disabled = 0';

		return parent::fetchProperties( $strSql, $objDatabase, true );
	}

	public static function fetchPropertiesByPostMonthByPropertyIdsByCid( $strPostMonth, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						pgs.ap_post_month
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( p.id = pgs.property_id AND p.cid = pgs.cid )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND DATE_TRUNC( \'month\', pgs.ap_post_month ) <> \'' . date( 'm-d-Y', strtotime( $strPostMonth ) ) . '\'
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByIdsByCidByPsProductIds( $arrintPropertyIds, $intCid, $arrintPsProductIds, $objDatabase ) {

		$strSql = 'SELECT
						p.*
					FROM
						properties p
						JOIN property_products pp ON ( pp.cid = p.cid AND pp.property_id IS NULL )
					WHERE
						p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pp.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
						AND pp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
					UNION
					SELECT
						p.*
					FROM
						properties p
						JOIN property_products pp ON ( pp.cid = p.cid AND pp.property_id = p.id )
					WHERE
						p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pp.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
						AND pp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )';

		return parent::fetchProperties( $strSql, $objDatabase, true );
	}

	public static function fetchPropertyOwnerOrLookupCodeByPropertyIds( $arrintPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyId ) ) {
			return NULL;
		}

		// Find owner for property recursively
		$strSql = 'WITH RECURSIVE ownerTree AS
					(
						SELECT
							p.id AS property_id,
							o.id,
							p.property_name,
							o.owner_name,
							p.lookup_code,
							p.cid
						FROM
							properties p
							JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( ',', $arrintPropertyId ) . ']::INTEGER[] ) lp ON ( p.cid = lp.cid AND p.id = lp.property_id AND lp.is_disabled = 0 )
							JOIN owners o ON ( o.cid = p.cid AND o.id = p.owner_id )
						WHERE
							p.cid = ' . ( int ) $intCid . '

						UNION ALL
							SELECT
								ot.property_id,
								oa.id,
								ot.property_name,
								o.owner_name,
								ot.lookup_code,
								ot.cid
						FROM
							owner_associations oa
							JOIN owners o ON ( o.cid = oa.cid AND o.id = oa.owner_id )
							INNER JOIN ownerTree ot ON o.cid = ot.cid AND ot.id = oa.parent_owner_id
						WHERE
							o.cid = ' . ( int ) $intCid . ')

						SELECT
							property_id,
							property_name,
							string_agg ( owner_name, \',\' ) AS owner_name,
							lookup_code AS property_look_up_code
						FROM
							ownerTree
						GROUP BY
							property_id,
							property_name,
							lookup_code
						ORDER BY
							LOWER( property_name )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchChildPropertyIdsByPropertyIdByCidByStudentPropertyCheck( $intPropertyId, $intCid, $boolIsStudentSemesterSelectionEnabled, $objDatabase ) {

		if( true == $boolIsStudentSemesterSelectionEnabled ) {
			$strSqlWhere = ' AND ' . COccupancyType::STUDENT . ' = ANY( p.occupancy_type_ids )';
		} else {
			$strSqlWhere = ' AND ' . COccupancyType::STUDENT . ' <> ANY( p.occupancy_type_ids )';
		}

		$strSql = 'SELECT
						p.id
					FROM
						properties p
					WHERE
						p.property_id = ' . ( int ) $intPropertyId . '
						AND p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled <> 1 ' . $strSqlWhere;

		$arrmixProperties = fetchData( $strSql, $objDatabase );

		$arrintChildPropertyIds = [];
		if( true == valArr( $arrmixProperties ) ) {
			foreach( $arrmixProperties as $arrintPropertyIds ) {
				$arrintChildPropertyIds[$arrintPropertyIds['id']] = $arrintPropertyIds['id'];
			}
		}

		return $arrintChildPropertyIds;
	}

	public static function fetchStudentEnabledPropertyIdsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolCheckEntrataEnabled = true, $boolSkipDisabled = true ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strPsProductCondition = ( true == $boolCheckEntrataEnabled ? ', ARRAY [ ' . CPsProduct::ENTRATA . ' ]::int [ ]' : '' );
		$strWhere              = ( true == $boolSkipDisabled ? ' AND lp.is_disabled = 0 ' : '' );

		$strSql = 'SELECT property_id as id
			       FROM load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::int [ ], ARRAY[' . implode( ',', $arrintPropertyIds ) . ']' . $strPsProductCondition . ' ) lp 
				   WHERE lp.is_student_property = 1
				          ' . $strWhere;

		$arrmixProperties = fetchData( $strSql, $objDatabase );

		$arrintAllPropertyIds = [];
		if( true == valArr( $arrmixProperties ) ) {
			foreach( $arrmixProperties as $arrmixPropertyIds ) {
				$arrintAllPropertyIds[$arrmixPropertyIds['id']] = $arrmixPropertyIds['id'];
			}
		}

		return $arrintAllPropertyIds;

	}

	public static function fetchTerminatedAndDisabledPropertiesByIdsByCids( $arrintIds, $arrintCids, $objDatabase, $boolIsFromRenewal = false ) {
		if( false == valArr( $arrintIds ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strOrCondition = 'OR ( termination_date IS NOT NULL AND termination_date < NOW()::DATE ))';

		if( true == $boolIsFromRenewal ) {
			$strMonthEndDate = date( 'Y-m-t', strtotime( date( 'Y-m-d' ) ) );
			$strOrCondition  = 'OR ( termination_date IS NOT NULL AND termination_date::DATE <= \'' . $strMonthEndDate . '\' ) )';
		}

		$strSql = 'SELECT
						id
					FROM
						properties
					WHERE
						id IN ( ' . implode( ',', $arrintIds ) . ' )
						AND cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND ( is_disabled = 1 ' . $strOrCondition . '';

		return fetchData( $strSql, $objDatabase );
	}

	public static function loadCustomPropertiesByPsProductIds( $arrintPsProductIds, $objDatabase ) {

		if( false == valArr( $arrintPsProductIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id as property_id,
						p.property_name,
						c.id as cid,
						c.company_name
					FROM
						properties p
						JOIN property_products AS pp ON ( ( pp.property_id = p.id OR pp.property_id IS NULL ) AND pp.cid = p.cid )
						JOIN clients AS c ON ( c.id = p.cid )
					WHERE
						pp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
						AND p.is_disabled = 0
					ORDER BY
						p.id ';

		$arrstrProperties = ( array ) fetchData( $strSql, $objDatabase );

		return $arrstrProperties;
	}

	public static function fetchManagerialPropertiesByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						p.*
					FROM
						properties p
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						AND is_managerial = 1';

		$arrstrManagerialProperties = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrManagerialProperties ) ) {
			return reset( $arrstrManagerialProperties );
		}

		return NULL;
	}

	public static function fetchNonIntegratedPropertyIds( $objDatabase, $strScriptExecutionTime ) {

		$strSql = 'SELECT
						p.cid,
						p.id,
						( CURRENT_TIMESTAMP AT TIME ZONE tz.time_zone_name )::DATE as property_date
					FROM
						properties p
						JOIN clients c ON ( c.id = p.cid )
						JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id AND pgs.is_ar_migration_mode = false )
						JOIN property_details pd ON ( pd.cid = p.cid AND pd.property_id = p.id )
						JOIN time_zones tz ON ( tz.id = p.time_zone_id )
						LEFT JOIN property_integration_databases pid ON ( pid.cid = p.cid AND pid.property_id = p.id )
						LEFT JOIN integration_databases id ON ( pid.cid = id.cid AND pid.integration_database_id = id.id AND id.integration_client_type_id NOT IN ( ' . CIntegrationClientType::MIGRATION . ',' . CIntegrationClientType::EMH . ',' . CIntegrationClientType::EMH_ONE_WAY . ' ) )
					WHERE
						( pid.id IS NULL OR id.id IS NULL )
						AND ( ( pd.details->>\'lease_interval_script_last_auto_ran_on\' IS NULL ) OR ( ( ( pd.details->>\'lease_interval_script_last_auto_ran_on\' )::TIMESTAMPTZ AT TIME ZONE tz.time_zone_name )::DATE < ( now() AT TIME ZONE tz.time_zone_name )::DATE ) )
						AND ( \'' . $strScriptExecutionTime . '\' )::TIME <= ( to_char( now() AT TIME ZONE tz.time_zone_name, \'HH24\' ) || \':00\' )::TIME
					ORDER BY
						CASE 
							WHEN ' . CCompanyStatusType::CLIENT . ' = c.company_status_type_id THEN 0 ELSE 1
						END';

		$arrmixResponseData				= fetchData( $strSql, $objDatabase );
		$arrmixNonIntegratedProperties	= [];
		foreach( $arrmixResponseData as $intCid => $arrmixResponse ) {
			$arrmixNonIntegratedProperties[$arrmixResponse['cid']][$arrmixResponse['id']] = $arrmixResponse;
		}

		return $arrmixNonIntegratedProperties;
	}

	public static function fetchArMigrationModeEnabledNonIntegratedPropertyIds( $objDatabase ) {

		$strSql = 'SELECT
						p.cid,
						p.id
					FROM
						properties p
						JOIN clients c ON ( c.id = p.cid )
						JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id AND pgs.is_ar_migration_mode = true )
						LEFT JOIN property_integration_databases pid ON ( pid.cid = p.cid AND pid.property_id = p.id )
						LEFT JOIN integration_databases id ON ( pid.cid = id.cid AND pid.integration_database_id = id.id AND id.integration_client_type_id NOT IN ( ' . CIntegrationClientType::MIGRATION . ',' . CIntegrationClientType::EMH . ',' . CIntegrationClientType::EMH_ONE_WAY . ' ) )
					WHERE
						pid.id IS NULL
						OR id.id IS NULL
					ORDER BY
						CASE 
							WHEN ' . CCompanyStatusType::CLIENT . ' = c.company_status_type_id THEN 0 ELSE 1
						END';

		$arrmixResponseData				= fetchData( $strSql, $objDatabase );
		$arrmixNonIntegratedProperties	= [];
		foreach( $arrmixResponseData as $intCid => $arrmixResponse ) {
			$arrmixNonIntegratedProperties[$arrmixResponse['cid']][$arrmixResponse['id']] = $arrmixResponse;
		}

		return $arrmixNonIntegratedProperties;
	}

	public static function fetchPropertyDetailsByCidsByPropertyIds( $arrintCids, $arrintPropertyIds, $objDatabase, $boolIsClientStatusOnly = false ) {
		if( ( false == ( $arrintCids = getIntValuesFromArr( $arrintCids ) ) ) || ( false == ( $arrintPropertyIds = getIntValuesFromArr( $arrintPropertyIds ) ) ) ) {
			return NULL;
		}

		$strWhere = ( true == $boolIsClientStatusOnly ) ? ' AND c.company_status_type_id = ' . \CCompanyStatusType::CLIENT : '';

		$strSql = 'SELECT
						p.id as property_id,
						p.cid,
						c.company_name,
						p.property_name,
						ps.name as state_name,
						pa.city,
						p.country_code,
						CONCAT_WS(\', \',NULLIF( pa.street_line1, \'\' ), NULLIF( pa.street_line2, \'\' ), NULLIF( pa.street_line3, \'\' ), NULLIF( pa.city, \'\' ), 
						CONCAT_WS(\' \', pa.state_code, pa.postal_code ) ) AS property_address,
						( p.property_name || \'~\' || p.id || \'~\' || p.cid ) as match_key
					FROM
						properties p
						JOIN clients c ON ( p.cid = c.id )
						LEFT JOIN property_addresses pa ON ( pa.cid = p.cid AND p.id = pa.property_id AND pa.is_alternate = false )
						LEFT JOIN states ps ON ( pa.state_code = ps.code )
					WHERE
						p.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pa.address_type_id = ' . CAddressType::PRIMARY . '
						AND p.is_disabled = 0' . $strWhere;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyAddressesByPropertyIdsByAddressType( $arrintPropertyId, $intAddressTypeId, $objClientDatabase ) {

		if( true == is_null( $arrintPropertyId ) || true == is_null( $intAddressTypeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					p.id,
					p.property_name,
					p.is_disabled,
					p.property_type_id,
					pa.street_line1,
					pa.street_line2,
					pa.street_line3,
					pa.city,
					pa.postal_code,
					pa.state_code,
					pa.country_code
				FROM properties p
				LEFT JOIN property_addresses pa
					ON ( p.id = pa.property_id AND pa.is_alternate = false )
				WHERE p.id IN ( ' . implode( ', ', $arrintPropertyId ) . ' )
				AND pa.address_type_id = ' . ( int ) $intAddressTypeId;

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchPropertiesCountByProductIdByCid( $intProductId, $intCid, $objDatabase ) {

		if( ( false == is_numeric( $intCid ) ) || ( false == is_numeric( $intProductId ) ) ) {
			return false;
		}

		$strSql = 'SELECT
						COUNT( DISTINCT p.id )
					FROM
						properties p
						JOIN property_products pp ON ( p.cid = pp.cid )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND ( p.id = pp.property_id )
						AND pp.ps_product_id = ' . ( int ) $intProductId . '
						AND p.is_disabled = 0';

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchOwnerNamesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						o.owner_name AS owner_name
					FROM
						properties p
						JOIN owners o ON ( o.cid = p.cid AND o.id = p.owner_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id = ' . ( int ) $intPropertyId;

		return parent::fetchColumn( $strSql, 'owner_name', $objDatabase );

	}

	public static function fetchPropertiesByPropertyIdsByPsProductIds( $arrintPropertyIds, $arrintPsProductIds, $objDatabase, $boolShowTerminatedProperties = false, $intOffset = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strWhere = 'AND ( cp.termination_date IS NULL OR cp.termination_date > NOW ( ) ) AND p.termination_date IS NULL AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED;

		if( true == $boolShowTerminatedProperties ) {
			$strWhere = 'AND ( cp.termination_date IS NOT NULL OR ( cp.termination_date IS NULL OR cp.termination_date > NOW ( ) ) OR p.termination_date IS NOT NULL OR p.termination_date IS NULL ) AND c.contract_status_type_id IN ( ' . implode( ',', CContractStatusType::$c_arrintCompletedContractStatusTypeIds ) . ' )';
		}

		$objPagination = new CPagination();

		$intLimit  = 19;
		$intOffset = ( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) && 0 < $objPagination->getPageSize() && 0 != ( int ) $intOffset ) ? $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) : ( int ) $intOffset;

		$strSql = 'SELECT
						DISTINCT(p.*)
					FROM
						properties p
						JOIN contract_properties cp ON ( p.cid = cp.cid AND p.id = cp.property_id )
						JOIN contracts c ON ( c.id = cp.contract_id AND c.cid = cp.cid )
					WHERE
						cp.ps_product_id IN ( ' . sqlIntImplode( $arrintPsProductIds ) . ' )
						AND cp.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.is_disabled <> 1
						' . $strWhere . '
					ORDER BY
						property_name ASC';

		if( 0 != $intOffset ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		return self::fetchProperties( $strSql, $objDatabase );

	}

	public static function fetchPropertiesDetailsByPropertyIdsByPsProductIds( $arrintPropertyIds, $arrintPsProductIds, $objDatabase, $boolShowTerminatedProperties = false, $intOffset = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strWhere = 'AND ( cp.termination_date IS NULL OR cp.termination_date > NOW ( ) ) AND p.termination_date IS NULL AND c.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED;

		if( true == $boolShowTerminatedProperties ) {
			$strWhere = 'AND ( cp.termination_date IS NOT NULL OR ( cp.termination_date IS NULL OR cp.termination_date > NOW ( ) ) OR p.termination_date IS NOT NULL OR p.termination_date IS NULL ) AND c.contract_status_type_id IN ( ' . implode( ',', CContractStatusType::$c_arrintCompletedContractStatusTypeIds ) . ' )';
		}

		$objPagination = new CPagination();

		$intLimit  = 19;
		$intOffset = ( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) && 0 < $objPagination->getPageSize() && 0 != ( int ) $intOffset ) ? $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) : ( int ) $intOffset;

		$strSql = 'SELECT
						DISTINCT(p.*)
					FROM
						properties p
						JOIN contract_properties cp ON ( p.cid = cp.cid AND p.id = cp.property_id )
						JOIN contracts c ON ( c.id = cp.contract_id AND c.cid = cp.cid )
					WHERE
						cp.ps_product_id IN ( ' . sqlIntImplode( $arrintPsProductIds ) . ' )
						AND cp.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.is_disabled <> 1
						' . $strWhere . '
					ORDER BY
						property_name ASC';

		if( 0 != $intOffset ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByUserValidationModeByCid( $intUserValidationMode, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						p.*
					FROM
						properties p
						JOIN property_gl_settings pgs ON( p.cid = pgs.cid AND p.id = pgs.property_id AND p.is_disabled = 0 )
						JOIN property_products pp ON( pgs.cid = pp.cid AND pgs.property_id = pp.property_id AND pp.ps_product_id = ' . CPsProduct::ENTRATA . ' )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND pgs.user_validation_mode = ' . ( int ) $intUserValidationMode . '
					ORDER BY
						lower ( property_name ) ASC, order_num;';

		return self::fetchProperties( $strSql, $objDatabase );

	}

	public static function fetchIntegratedPropertiesByIntegrationClientTypeIdByPropertyIntegrationDatabaseId( $intCid, $intIntegrationClientTypeId, $objDatabase ) {

		$strSql = '
				SELECT
					p.id,
					p.property_name
				FROM
					properties p
					JOIN property_integration_databases pid ON ( pid.cid = p.cid AND pid.property_id = p.id AND p.is_disabled = 0 )
					JOIN integration_databases id ON ( id.cid = pid.cid AND id.id = pid.integration_database_id  AND id.integration_client_type_id < ' . ( int ) $intIntegrationClientTypeId . ' )
				WHERE
					p.cid = ' . ( int ) $intCid . '
				ORDER BY
					p.property_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertiesByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyGroupIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.*
					FROM
						properties p
						JOIN load_properties(  ARRAY[' . ( int ) $intCid . '], ARRAY[' . implode( ',', $arrintPropertyGroupIds ) . '] ) lp ON ( p.cid = lp.cid AND p.id = lp.property_id AND lp.is_disabled = 0 )
					WHERE
						p.cid = ' . ( int ) $intCid . '
					ORDER BY p.id';

		$arrobjProperties = self::fetchProperties( $strSql, $objDatabase );

		return ( true == valArr( $arrobjProperties ) ) ? rekeyObjects( 'Id', $arrobjProperties ) : [];

	}

	public static function fetchWaitListPropertiesByPropertyGroupIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolShowDisabledData = false ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT
						p.*
					FROM
						properties p
						JOIN wait_lists wl ON ( wl.cid = p.cid AND wl.property_group_id = p.id AND wl.is_active IS TRUE )
						JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( $arrintPropertyIds, ',' ) . ' ]::INTEGER[] ) lp ON ( wl.cid = lp.cid AND lp.property_id = wl.property_group_id ' . ( ( false == $boolShowDisabledData ) ? 'AND lp.is_disabled = 0' : '' ) . ' )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.property_id IS NULL
					ORDER BY
						p.property_name,
						p.id';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCustomWaitListEnabledPropertyIdsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						p.id
					FROM
						properties p
						JOIN wait_lists wl ON ( wl.cid = p.cid AND ( wl.property_group_id = p.id OR wl.property_group_id = p.property_id )AND wl.is_active IS TRUE )
						JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( $arrintPropertyIds, ',' ) . ' ]::INTEGER[] ) lp ON ( wl.cid = lp.cid AND lp.property_id = wl.property_group_id AND lp.is_disabled = 0 )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND ( p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) OR p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchWaitListPropertyIdsByPropertyGroupIdsByCid( $arrintPropertyIds = NULL, $intCid = NULL, $objDatabase ) {
		$strJoinClause  = '';
		$strWhereClause = '';
		if( true == valId( $intCid ) && true == valArr( $arrintPropertyIds ) ) {
			$strWhereClause = '
								WHERE
									p . cid = ' . ( int ) $intCid . '
									AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
			$strJoinClause  = 'JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( $arrintPropertyIds, ',' ) . ' ]::INTEGER[] ) lp ON ( wl.cid = lp.cid AND lp.property_id = wl.property_group_id AND lp.is_disabled = 0 )';
		}
		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.cid
					FROM
						properties p
						JOIN wait_lists wl ON ( wl.cid = p.cid AND wl.property_group_id = p.id AND wl.is_active IS TRUE )
						' . $strJoinClause . '
						' . $strWhereClause . '
					ORDER BY
						p.property_name ASC ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyIdByPropertyNameByCid( $strPropertyName, $intClientId, $objDatabase ) {

		$arrintData = fetchData( 'SELECT id FROM properties WHERE property_name = \'' . addslashes( $strPropertyName ) . '\' AND cid = ' . ( int ) $intClientId, $objDatabase );

		if( true == valArr( $arrintData[0] ) ) {
			return $arrintData[0][id];
		}

		return false;
	}

	public static function fetchPropertyAddressesByPropertyIdsByCIdsByAddressType( $arrintPropertyId, $arrintCIds, $intAddressTypeId, $objClientDatabase ) {

		if( true == is_null( $arrintPropertyId ) || true == is_null( $intAddressTypeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.is_disabled,
						p.property_type_id,
						pa.street_line1,
						pa.street_line2,
						pa.street_line3,
						pa.city,
						pa.postal_code
					FROM
						properties p
						LEFT JOIN property_addresses pa ON ( p.id = pa.property_id AND pa.is_alternate = false )
					WHERE
						p.id IN ( ' . implode( ', ', $arrintPropertyId ) . ' )
						AND p.cid IN ( ' . implode( ', ', $arrintCIds ) . ' )
						AND pa.address_type_id = ' . ( int ) $intAddressTypeId;

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchAllActivePropertiesByClientIds( $arrintClientIds, $objDatabase ) {

		if( false == valArr( $arrintClientIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT p.id,
						p.property_name
					FROM
						properties AS p
						JOIN contract_properties AS cp on ( cp.property_id = p.id )
						LEFT JOIN implementation_dates AS id on ( id.contract_property_id = cp.id )
					WHERE
						p.cid IN ( ' . implode( ',', $arrintClientIds ) . ' )
						AND p.is_disabled <> 1
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						AND ( id.confirmation_status IS NULL OR id.confirmation_status = \'Approved\' )
						ORDER BY
						 p.property_name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActivePropertyIdsWithOccupanciesByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.occupancy_type_ids
					FROM
						properties p
					WHERE
						p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						AND p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
					ORDER BY lower ( p.property_name ) ';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchActivePropertiesByPsProductIdByPropertyIdsByCids( $intPsProductId, $arrintPropertyIds, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintCids ) || false == valArr( $arrintPropertyIds ) || true == is_null( $intPsProductId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					p.*
				FROM
					properties p
					JOIN contract_properties cp ON( cp.property_id = p.id AND cp.cid = p.cid  )
				WHERE
					p.id IN( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
					AND p.cid IN( ' . sqlIntImplode( $arrintCids ) . ' )
					AND cp.cid IN ( ' . sqlIntImplode( $arrintCids ) . ' )
					AND p.termination_date IS NULL
					AND p.is_disabled <> 1
					AND cp.termination_date IS NULL
					AND cp.ps_product_id = ' . ( int ) $intPsProductId;

		return self::fetchProperties( $strSql, $objDatabase );
	}

	// Fetching customized charge code properties

	public static function fetchCustomizedChargeCodePropertiesByCid( $objDatabase, $intCid, $arrintExcludePropertyIds = [] ) {

		if( false == valId( $intCid ) ) {
			return NULL;
		}

		$strAdditionalCondition = '';
		if( true == valArr( $arrintExcludePropertyIds ) ) {
			$strAdditionalCondition = 'AND p.id NOT IN ( ' . implode( ',', ( array ) $arrintExcludePropertyIds ) . ' )';
		}

		$strSql = 'SELECT
						p.id,
						p.property_name
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						' . $strAdditionalCondition . '
						AND p.is_disabled = 0
						AND pgs.activate_standard_posting = TRUE
						AND pgs.use_custom_ar_codes = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertiesByUnitSpaceIdsByCid( $arrintUnitSpaceIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintUnitSpaceIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						u.id as unit_space_id,
						p.id as property_id,
						p.property_name
					FROM
						unit_spaces As u
						JOIN properties AS p ON ( p.id = u.property_id AND p.cid = u.cid )
					WHERE
						p.cid = ' . ( int ) $intCid .
		          ' AND u.id IN ( ' . implode( ',', $arrintUnitSpaceIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyDetailsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT pa.postal_code,
						pa.property_id,
						pa.cid,
						pa.city,
						pa.country_code,
						pa.state_code,
						CONCAT( pa.street_line1, \' \', pa.street_line2, \' \', pa.street_line3 ) AS address,
						posc.name_full AS contact_person,
						ppn.phone_number
					FROM properties p
						JOIN property_addresses pa ON ( pa.property_id = p.id AND pa.address_type_id = ' . CAddressType::PRIMARY . ' AND pa.is_alternate = false )
						LEFT JOIN property_on_site_contacts posc ON ( pa.cid = posc.cid AND pa.property_id = posc.property_id AND posc.on_site_contact_type_id = ' . COnSiteContactType::MANAGER . ' )
						LEFT JOIN property_phone_numbers ppn ON ( ppn.property_id = p.id AND ppn.phone_number_type_id = ' . CPhoneNumberType::OFFICE . ' )
					WHERE pa.property_id IN( ' . implode( ', ', $arrintPropertyIds ) . ' )AND p.is_disabled = 0 AND
					pa.cid =' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchAllEnabledPropertiesByCids( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						properties
					WHERE
						is_disabled <> 1
						AND cid = ' . ( int ) $intCid . '
						AND is_managerial = 0
					ORDER BY
						LOWER( property_name ) ASC';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchAllImplementedActivePropertiesByClientIds( $arrintClientIds, $objDatabase ) {

		if( false == valArr( $arrintClientIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT p.id,
						p.property_name
					FROM
						properties AS p
						JOIN contract_properties AS cp on ( cp.property_id = p.id )
						JOIN contracts co ON ( co.id = cp.contract_id )
					WHERE
						p.cid IN ( ' . implode( ',', $arrintClientIds ) . ' )
						AND cp.commission_bucket_id <> ' . CCommissionBucket::RENEWAL . '
						AND p.is_disabled <> 1
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						ORDER BY
						 p.property_name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomMigrationModeOnPropertiesByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
					  p.id,
					  p.property_name
					FROM
					  properties p
					  JOIN clients c ON ( p.cid = c.id )
					  JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id )
					WHERE
					  p.cid = ' . ( int ) $intCid . '
					  AND p.disabled_on IS NULL
					  AND ( ( ( pgs.is_ar_migration_mode = true )
					  OR ( pgs.is_ap_migration_mode = true )
					  OR ( pgs.is_asset_migration_mode = true ) )
					  AND pgs.user_validation_mode = ' . CPropertyGlSetting::VALIDATION_MODE_EDIT_INITIAL_IMPORT . ' ) ';

		$arrmixProperties = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixProperties ) ) {
			return NULL;
		}

		foreach( $arrmixProperties as $arrstrProperty ) {
			$arrstrProperties[$arrstrProperty['id']] = $arrstrProperty;
		}

		return $arrstrProperties;

	}

	public static function fetchPropertiesByPropertyIdsByCidByProduct( $arrintPsProductIds, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT p.id as property_id
					FROM
						properties p
						JOIN property_products AS ppd ON ( p.cid = ppd.cid AND p.id = ppd.property_id )
					WHERE
						( ppd.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' ) )
						AND p.cid = ' . ( int ) $intCid . '
						AND p.id = ' . ( int ) $intPropertyId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertiesByTransmissionVendorIdByTransmissionTypeByCid( $intTransmissionVendorId, $intTransmissionTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						p.id, p.cid
					FROM
						properties p
						JOIN property_transmission_vendors ptv ON ( ptv.cid = p.cid and ptv.property_id = p.id )
						JOIN company_transmission_vendors ctv ON ( ctv.id = ptv.company_transmission_vendor_id AND ctv.cid = ptv.cid )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND ctv.transmission_vendor_id = ' . ( int ) $intTransmissionVendorId . '
						AND ctv.transmission_type_id = ' . ( int ) $intTransmissionTypeId;

		$arrmixRecords = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixRecords ) ) {
			return NULL;
		}

		return $arrmixRecords;
	}

	public static function fetchPropertiesByPsProductIdsByCids( $arrintPsProductIds, $arrintClientIds, $objDatabase ) {

		if( false == valArr( $arrintClientIds ) || false == valArr( $arrintPsProductIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.*
					FROM properties p
						JOIN property_products pp ON ( pp.property_id = p.id AND pp.cid = p.cid )
						JOIN clients c ON ( c.id = p.cid )
					WHERE
						c.id IN ( ' . implode( ',', $arrintClientIds ) . ' )
						AND pp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchAllAccountPropertiesByIdsByCid( $intPageNo, $intPageSize, $arrintPropertyIds, $intCid, $objAdminDatabase, $objPropertyFilter = NULL ) {
		$intOffset = ( false == is_null( $intPageNo ) && 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		if( 0 < $intOffset && 0 < $intLimit ) {
			$strOffsetLimit = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		$arrstrAdditionalAndStatements = [];
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		if( true == isset ( $objPropertyFilter ) ) {
			if( 1 == $objPropertyFilter->getHideDisabled() ) {
				$arrstrAdditionalAndStatements[] = ' p.is_disabled <> 1 AND p.termination_date IS NULL ';
			}

			if( false != $objPropertyFilter->getExcludeZeroUnitCount() ) {
				$arrstrAdditionalAndStatements[] = ' p.number_of_units > 0 ';
			}

			if( false != $objPropertyFilter->getExcludeIsManagerial() ) {
				$arrstrAdditionalAndStatements[] = ' p.is_managerial = 0 ';
			}
		}

		$strSql = ' WITH rows AS ( SELECT
						ap.account_id AS ap_acc_id,
						a.account_type_id,
						count ( ap.property_id ) OVER ( PARTITION BY ap.property_id ) AS row_number,
						p.id,
						p.account_id,
						p.number_of_units,
						p.property_name,
						p.property_type_id
					FROM
						properties p
						LEFT JOIN account_properties ap ON ( ap.property_id = p.id AND ap.cid = p.cid AND p.account_id IS NULL )
						LEFT JOIN accounts a ON ( a.id = ap.account_id )
					WHERE
						p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.CID = ' . ( int ) $intCid . '
						AND ' . implode( ' AND ', $arrstrAdditionalAndStatements ) . '
						AND property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
					ORDER BY
						lower ( property_name ) ASC, order_num
					' . $strOffsetLimit . ' 
					)
					SELECT
						CASE
							WHEN rows.account_id IS NULL AND rows.row_number > 1 THEN COALESCE ( rows.ap_acc_id, rows.account_id )
							WHEN rows.account_id IS NULL AND rows.row_number = 1 THEN COALESCE ( rows.ap_acc_id, rows.account_id )
							WHEN rows.row_number = 0 THEN COALESCE ( rows.ap_acc_id, rows.account_id )
						END AS account_id,
						rows.id,
						rows.number_of_units,
						rows.property_name,
						rows.property_type_id
					FROM
						rows
					WHERE
						rows.row_number = 1
						OR ( rows.row_number > 1 AND rows.account_type_id IN ( ' . CAccountType::PRIMARY . ', ' . CAccountType::STANDARD . ' ) )
						OR rows.row_number = 0';

		return self::fetchProperties( $strSql, $objAdminDatabase );
	}

	public static function generateSearchPropertiesSqlForRpServiceHandler( $arrstrSearchKeywords = [], $strStateCode = NULL, $intPostalCode = NULL, $boolIsIncludeDemoClient = false, $strMobileApplicationToken = NULL ) {

		$arrintCompanyStatusTypeIds = [ CCompanyStatusType::CLIENT, CCompanyStatusType::PROSPECT ];

		if( true == $boolIsIncludeDemoClient ) {
			array_push( $arrintCompanyStatusTypeIds, CCompanyStatusType::SALES_DEMO, CCompanyStatusType::TEST_DATABASE );
		}

		if( false != valArr( $arrstrSearchKeywords ) ) {
			if( 2 < \Psi\Libraries\UtilFunctions\count( $arrstrSearchKeywords ) ) {
				return NULL;
			}
			$arrstrSearchKeywords = array_values( $arrstrSearchKeywords );// Reset array keys 0,1,2,3
		}

		$strMobileApplicationJoin = '';

		if( false == is_null( $strMobileApplicationToken ) ) {
			$strMobileApplicationJoin = 'JOIN mobile_applications ma ON ( p.cid = ma.cid AND ma.token = \'' . $strMobileApplicationToken . '\' )
										 JOIN mobile_application_properties map ON (p.cid = map.cid and ma.id = map.mobile_application_id and p.id = map.property_id )';
		}

		$strSql = 'SELECT
						DISTINCT ON(p.id)
						p.cid as cid,
						p.id as property_id,
						p.property_name,
						pa.street_line1,
						pa.street_line2,
						pa.street_line3,
						pa.city,
						pa.state_code,
						pa.postal_code,
						pa.country_code,
						mc.rwx_domain,
						mc.company_name,
						mc.company_status_type_id
					FROM
						properties p
						JOIN property_addresses pa ON( p.cid = pa.cid AND p.id = pa.property_id AND pa.is_alternate = false )
						JOIN clients mc ON( p.cid = mc.id )
						JOIN property_products pp ON( p.cid = pp.cid AND ( pp.property_id IS NULL OR pp.property_id = p.id ) )
						JOIN property_products pp2 ON( p.cid = pp2.cid AND ( pp2.property_id IS NULL OR pp2.property_id = p.id ) ) '
		          . $strMobileApplicationJoin . '
					WHERE
						p.is_disabled = 0 
						AND pa.is_published = 1
						AND p.is_test <> 1
						AND ( pa.street_line1 IS NOT NULL OR pa.street_line2 IS NOT NULL OR pa.street_line3 IS NOT NULL ) 
						AND pa.address_type_id = ' . CAddressType::PRIMARY . '
						AND ( pp.ps_product_id = ' . CPsProduct::RESIDENT_PORTAL . ' OR pp2.ps_product_id = ' . CPsProduct::RESIDENT_PAY . ' )
						AND pp.id != pp2.id 
						AND ( mc.company_status_type_id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' ) )';
		if( false != valArr( $arrstrSearchKeywords ) ) {
			foreach( $arrstrSearchKeywords as $strSearchKeyword ) {
				$strSql .= ' AND( p.property_name ILIKE \'%' . addslashes( trim( $strSearchKeyword ) ) . '%\' OR pa.city ILIKE \'' . addslashes( trim( $strSearchKeyword ) ) . '%\' )';
			}
		}
		if( false == is_null( $strStateCode ) ) {
			$strSql .= ' AND lower( pa.state_code ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strStateCode ) ) . '\'';
		}
		if( false == is_null( $intPostalCode ) ) {
			$strSql .= ' AND pa.postal_code = \'' . addslashes( $intPostalCode ) . '\'';
		}

		return $strSql;
	}

	public static function fetchPropertyByLookupCodeByCid( $strLookupCode, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM properties WHERE cid = ' . ( int ) $intCid . ' AND lookup_code = \'' . pg_escape_string( $strLookupCode ) . '\'';

		return self::fetchProperty( $strSql, $objDatabase );
	}

	public static function fetchAllPropertiesByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objDatabase, $boolIsReturnKeyedArray = true ) {
		if( false == ( $arrintPropertyGroupIds = getIntValuesFromArr( $arrintPropertyGroupIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.*,
						pga.property_group_id
					FROM
						properties p
						JOIN property_group_associations pga ON ( p.id = pga.property_id AND p.cid = pga.cid )
					WHERE
						pga.property_group_id IN ( ' . sqlIntImplode( $arrintPropertyGroupIds ) . ' )
						AND p.is_disabled = 0
						AND p.is_managerial <> 1
						AND p.cid = ' . ( int ) $intCid;

		return self::fetchProperties( $strSql, $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchPropertiesScreeningTransmissionVendorsCountByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT 1 as transmission_vendor_id
						WHERE exists (
						
						                SELECT
						                    DISTINCT ptv.property_id as property_id
						                FROM
						                    property_transmission_vendors AS ptv 
						                    JOIN company_transmission_vendors AS ctv ON ( ctv.cid = ptv.cid AND ptv.company_transmission_vendor_id = ctv.id )                             
						          JOIN properties p ON p.cid = ptv.cid AND ptv.property_id = p.id                                        
						                WHERE
						                    ptv.cid = ' . ( int ) $intCid . '
						                    AND ctv.transmission_type_id = ' . CTransmissionType::SCREENING . '
						                    AND ctv.is_published = 1
						                    AND p.is_disabled = 0
						                    AND ctv.transmission_vendor_id = ' . CTransmissionVendor::RESIDENT_VERIFY . '
						               )
						UNION
						
						SELECT 2 as transmission_vendor_id
						WHERE exists (
						
						                SELECT
						                    DISTINCT ptv.property_id as property_id
						                FROM
						                    property_transmission_vendors AS ptv 
						                    JOIN company_transmission_vendors AS ctv ON ( ctv.cid = ptv.cid AND ptv.company_transmission_vendor_id = ctv.id )                             
						          JOIN properties p ON p.cid = ptv.cid AND ptv.property_id = p.id                    
						                WHERE
						                    ptv.cid = ' . ( int ) $intCid . '
						                    AND ctv.transmission_type_id = ' . CTransmissionType::SCREENING . '
						                    AND ctv.is_published = 1
						                    AND p.is_disabled = 0
						                    AND ctv.transmission_vendor_id <> ' . CTransmissionVendor::RESIDENT_VERIFY . '
						               )';

		$arrintData = array_column( fetchData( $strSql, $objDatabase ), 'transmission_vendor_id' );

		return ( true == valArr( $arrintData ) ) ? $arrintData : [];
	}

	public static function fetchPropertiesByOccupancyTypeIds( $strOccupancyTypeIds, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.occupancy_type_ids
					FROM
						properties AS p
					WHERE
						p.cid = ' . ( int ) $intCid . '
					 	AND ( ( sort( p.occupancy_type_ids ) = sort( ARRAY[ ' . $strOccupancyTypeIds . ' ] )
							AND p.occupancy_type_ids::text IS NOT NULL ) )
						AND p.is_disabled = 0
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
					ORDER BY
						p.property_name';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCustomerSpecificPropertyByWebsiteIdByCustomerIdByCid( $intWebsiteId, $intCustomerId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						DISTINCT ON ( p.id ) p.*
					FROM
						properties p
						JOIN website_properties wp ON ( p.cid = wp.cid AND p.id = wp.property_id )
						JOIN leases l ON ( p.cid = l.cid AND p.id = l.property_id )
						JOIN lease_customers lc ON ( l.cid = lc.cid AND l.id = lc.lease_id )
						JOIN customers c ON ( c.cid = lc.cid AND c.id = lc.customer_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled != 1
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						AND c.id = ' . ( int ) $intCustomerId . ' LIMIT 1';

		return self::fetchProperty( $strSql, $objClientDatabase );

	}

	public static function fetchAllActivePropertiesCountByCid( $intCid, $objDatabase ) {

		if( false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						COUNT ( p.id ) AS count
					FROM
						properties AS p
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
						AND p.termination_date IS NULL
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )';

		$arrintResult = fetchData( $strSql, $objDatabase );

		return ( true == isset( $arrintResult[0] ) ) ? $arrintResult[0]['count'] : 0;
	}

	public static function fetchActivePropertyDetailsByCid( $intCid, $objAdminDatabase, $arrintPropertyIds ) {
		$strCondition = '';
		if( true == valArr( $arrintPropertyIds ) ) {
			$strCondition .= ' AND id IN (' . implode( ',', $arrintPropertyIds ) . ') ';
		}

		$strSql = 'SELECT
						id,
						property_name
					FROM
						properties
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_disabled = 0
						AND termination_date IS NULL
						AND property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						' . $strCondition . '
					ORDER BY
						lower ( property_name )';

		$arrstrPropertiesData = fetchData( $strSql, $objAdminDatabase );

		if( false == valArr( $arrstrPropertiesData ) ) {
			return NULL;
		}

		foreach( $arrstrPropertiesData as $arrstrPropertyData ) {
			$arrstrProperties[$arrstrPropertyData['id']] = $arrstrPropertyData['property_name'];
		}

		return $arrstrProperties;
	}

	public static function fetchQuickSearchDataForProperties( $objDatabase, $intCid, $intPropertyId, $intOffset, $intLimit ) {

		$strSql = ' SELECT
        ( \'properties\' || \'_\' || p.cid || \'_\' || p.id ) AS id,
        p.cid,
        p.id AS property_id,
        \'properties\'        AS search_type,
        0 AS lease_customer_id,
        0 AS customer_id,
        0 AS lease_id,
        0 AS applicant_application_id,
        0 AS applicant_id,
        0 AS application_id,
        \'\' AS name_first,
        \'\' AS name_last,
        p.property_name,
        \'\' AS building_name,
        \'\' AS unit_number_cache,
        \'\' AS lease_status_type,
        \'\' AS email_address,
        0 AS primary_phone_number,
        0 AS secondary_phone_number,
        \'\' AS remote_primary_key,
        1 AS order_num,
        \'\' AS application_stage_status,
        0 AS customer_type_id,
        0 AS occupancy_type_id,
        \'\' AS company_name,
        \'\' AS preferred_name,
        \'\' AS alt_name_first,
        \'\' AS alt_name_middle,
        \'\' AS alt_name_last,
        0 AS deleted_by
    FROM
        properties p
        JOIN clients cl ON (cl.id = p.cid)
    WHERE
        p.property_type_id NOT IN ( 38, 99 )
        AND p.is_disabled = 0
        AND cl.company_status_type_id NOT IN(5,12)';

		if( true == valId( $intCid ) ) {
			$strSql .= ' AND p.cid =' . ( int ) $intCid;
		}
		if( true == valId( $intPropertyId ) ) {
			$strSql .= ' AND p.id=' . ( int ) $intPropertyId;
		}
		if( true == valId( $intOffset ) ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset;
		}
		if( true == valId( $intLimit ) ) {
			$strSql .= ' LIMIT ' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPricingProperties( $objDatabase, $arrintTestCompanies = [] ) {
		$strTestCompaniesCondition = '';
		if( true == valArr( $arrintTestCompanies ) ) {
			$strTestCompaniesCondition = ' JOIN clients c ON c.id = lp.cid
										WHERE c.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' OR ( c.id IN(' . implode( ', ', $arrintTestCompanies ) . '))';
		}
		$strSql = ' SELECT p.*
					FROM properties AS p
						JOIN load_properties() AS lp ON p.cid = lp.cid AND p.id = lp.property_id AND lp.is_disabled = 0 AND lp.is_test = 0 AND lp.is_student_property = 0
						JOIN property_products AS pp ON p.cid = pp.cid AND p.id = pp.property_id AND pp.ps_product_id = ' . CPsProduct::PRICING . ' ' . $strTestCompaniesCondition;

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCompanyAppPropertiesByAppIdsByCid( $arrintAppIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintAppIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					  p.id, p.property_name
					FROM
					  properties p
					  JOIN property_apps pa ON ( pa.property_id = p.id and pa.cid = p.cid )
					  JOIN company_apps ca ON ( ca.id = pa.company_app_id and ca.cid = pa.cid )
					  JOIN apps a ON ( a.id = ca.app_id )
					WHERE
					  p.cid = ' . ( int ) $intCid . '
					  AND p.is_disabled <> 1
					  AND p.termination_date IS NULL
					  AND p.number_of_units > 0
					  AND p.is_managerial = 0
					  AND a.id IN ( ' . implode( ',', $arrintAppIds ) . ' )
					GROUP BY
					  p.id, p.property_name
					ORDER BY
					  lower ( property_name ) ASC';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchActivePropertiesDetailsByCidByPropertyIds( $intCid, $arrintPropertyIds, $objDatabase ) {
		if( false == valId( $intCid ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
 						p.id, p.property_name,
 						pa.postal_code
 					FROM
 						properties p
 						JOIN property_addresses pa ON ( pa.property_id = p.id AND pa.address_type_id = ' . CAddressType::PRIMARY . ' AND pa.is_alternate = false )
 					WHERE
 						p.cid = ' . ( int ) $intCid . '
 						AND p.id IN(' . sqlIntImplode( $arrintPropertyIds ) . ')
 						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
 						AND p.is_disabled <> 1
 					ORDER BY p.cid,p.property_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertiesDetailsByContractDraftStatusIds( $arrintContractDraftStatusIds, $objDatabase ) {

		if( false == valArr( $arrintContractDraftStatusIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name
					FROM
						properties p
						JOIN contract_properties cp ON( p.id = cp.property_id )
						JOIN contract_drafts cd ON ( cd.cid = cp.cid AND cd.contract_id = cp.contract_id )
					WHERE
						cd.contract_draft_status_id IN ( ' . implode( ',', $arrintContractDraftStatusIds ) . ' )
					ORDER BY
						p.property_name';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchIntegratedPropertiesCountByCid( $intCid, $objDatabase ) {

		$strSql = ' SELECT 
                        COUNT( p.id ) as property_count
					FROM 
						properties p
                        JOIN property_integration_databases pid ON( p.id = pid.property_id AND p.cid = pid.cid )
                        JOIN integration_databases id ON( id.cid = pid.cid AND id.id = pid.integration_database_id )
                        LEFT JOIN property_preferences pp ON( p.id = pp.property_id AND p.cid = pp.cid AND pp.key = \'BLOCK_ALL_WORK_ORDER_CALLS_RELATED_INTEGRATION\' )
					WHERE 
						p.remote_primary_key IS NOT NULL 
						AND p.cid = ' . ( int ) $intCid . ' 
						AND id.integration_client_type_id NOT IN ( ' . CIntegrationClientType::MIGRATION . ',' . CIntegrationClientType::EMH . ',' . CIntegrationClientType::EMH_ONE_WAY . ' )
						AND ( p.remote_primary_key IS NOT NULL or pp.value= \'0\' )
						AND id.integration_client_status_type_id <> ' . CIntegrationClientStatusType::DISABLED;

		$arrmixResult = fetchData( $strSql, $objDatabase );

		return $arrmixResult[0]['property_count'];
	}

	public static function fetchAccountSpecificPropertiesByPaymentTypeIdByCid( $intPaymentTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT p.id,
						p.property_name,
						a.id as associated_account_id
					FROM
						properties p
						LEFT JOIN account_properties ap ON (p.cid = ap.cid AND p.id = ap.property_id)
						LEFT JOIN accounts a ON (ap.account_id = a.id)
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
						AND a.is_disabled = 0 
						AND a.payment_type_id = ' . ( int ) $intPaymentTypeId . '
					GROUP BY
						p.id, a.id
					ORDER BY
						lower ( p.property_name )';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesWithoutAccountAssociationByContractIdByCid( $intContractId, $intCid, $objDatabase ) {

		if( false == valId( $intContractId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT p.*
					FROM properties p
						JOIN contract_properties cp ON (p.cid = cp.cid AND p.id = cp.property_id)
						LEFT JOIN account_properties ap ON (p.cid = ap.cid AND p.id =
							ap.property_id)
					WHERE p.cid = ' . ( int ) $intCid . ' AND
							p.is_disabled = 0 AND
							cp.termination_date IS NULL AND
							cp.contract_id = ' . ( int ) $intContractId . ' AND
							ap.id IS NULL
					GROUP BY p.id
					ORDER BY lower(p.property_name)';

		return self::fetchProperties( $strSql, $objDatabase );

	}

	public static function fetchPropertiesWithNoArTransactionsAssociatedByCid( $intCid, $objDatabase ) {

		if( !valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.*,
						CASE
							WHEN p.property_type_id =  ' . CPropertyType::CORPORATE . ' THEN 1
							ELSE 2
						END as property_group_type,
						ap.company_name
					FROM
						properties p
						JOIN property_details pd ON ( p.cid = pd.cid AND p.id = pd.property_id )
						JOIN ap_payees ap ON ( pd.cid = ap.cid AND pd.ap_payee_id = ap.id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.is_disabled = 0
						AND p.property_type_id NOT IN( ' . CPropertyType::CLIENT . ',' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						AND NOT EXISTS (
					        SELECT
					            1
					        FROM
					            lease_customers lc
					        WHERE
					            lc.cid = pd.cid
					            AND lc.customer_id = pd.customer_id
					    )
					ORDER BY 
						property_group_type,
						lower ( property_name )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertiesCountByIdsByPropertyTypeIdByCid( $arrintPropertyIds, $intPropertyTypeId, $intCid, $objClientDatabase ) : int {

		if( false == valIntArr( $arrintPropertyIds ) || false == valId( $intCid ) || false == valId( $intPropertyTypeId ) ) {
			return 0;
		}

		$strSql = 'SELECT
						COUNT(1)
					FROM
						properties
					WHERE
						id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						AND property_type_id IN ( ' . ( int ) $intPropertyTypeId . ' )';

		$arrintCount = fetchData( $strSql, $objClientDatabase );

		return $arrintCount[0]['count'] ?? 0;

	}

	public static function fetchPropertiesByNameByCid( $strSearchString, $intCid, $objDatabase ) {
		if( false == valStr( $strSearchString ) ) {
			return [];
		}

		$strSql = 'SELECT
						p.id,
						p.property_name	
					FROM
						properties p	
					WHERE
						p.property_name ilike \'%' . $strSearchString . '%\' ' . '
						AND p.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByContractPropertyId( $intContractPropertyId, $objAdminDatabase ) {

		if( false == valId( $intContractPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						p.id,
						p.property_name
					FROM
						properties p
						JOIN contract_properties cp ON ( cp.property_id = p.id )
					WHERE
						cp.id =' . $intContractPropertyId;

		return self::fetchProperty( $strSql, $objAdminDatabase );

	}

	public static function fetchPmEnabledPropertyIdsByCid( $intCid, $objDatabase ) {
		if( false == isset( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id
					 FROM
						properties p,
						property_gl_settings pgs
					WHERE
						( p.id = pgs.property_id AND p.cid = pgs.cid )
						AND p.cid = ' . ( int ) $intCid . '
						AND pgs.activate_standard_posting = true
					ORDER BY
						p.property_name ASC';

		$arrmixPropertyData         = ( array ) fetchData( $strSql, $objDatabase );
		$arrintPmEnabledPropertyIds = [];

		foreach( $arrmixPropertyData as $arrintPropertyIds ) {
			$arrintPmEnabledPropertyIds[$arrintPropertyIds['id']] = $arrintPropertyIds['id'];
		}

		return $arrintPmEnabledPropertyIds;
	}

	public static function fetchViolationTemplateAssociatedPropertiesByTemplateIdCid( $intViolationTemplateId, $intCid, $objDatabase ) {
		if( false == valId( $intViolationTemplateId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						pg.id,
						pg.name
					FROM 
						property_groups pg
                        JOIN violation_template_property_groups vtpg ON (vtpg.cid = pg.cid AND vtpg.property_group_id = pg.id )
                        JOIN violation_templates vt ON (vt.cid = vtpg.cid AND vt.id = vtpg.violation_template_id )
					WHERE 
						vt.cid = ' . ( int ) $intCid . '
		                AND vt.id = ' . ( int ) $intViolationTemplateId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyNameByPropertyIds( $arrintPropertyIds, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT id, property_name FROM properties WHERE id IN (' . sqlIntImplode( $arrintPropertyIds ) . ' ); ';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchPropertiesByCompanyTransmissionVendorId( $intCompanyTransmissionVendorId, $intCid, $objDatabase ) {
		if( false == valId( $intCompanyTransmissionVendorId ) ) {
			return;
		}

		if( false == valId( $intCid ) ) {
			return;
		}

		$strSql = 'SELECT 
          p.* 
          FROM properties p 
          JOIN property_transmission_vendors ptv ON ( p.cid = ptv.cid and p.id = ptv.property_id )
          WHERE ptv.cid = ' . ( int ) $intCid . ' 
          AND ptv.company_transmission_vendor_id = ' . ( int ) $intCompanyTransmissionVendorId;

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchActivePropertyCountByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) || false == valArr( $arrintIds ) ) {
			return 0;
		}

		$strSql = 'SELECT
						COUNT( DISTINCT p.id ) property_count
					FROM
						properties p
					WHERE
						p.cid = ' . ( int ) $intCid . ' 
						AND p.id IN ( ' . implode( ',', $arrintIds ) . ' )
						AND p.is_disabled = 0;';

		$arrintActivePropertyCount = ( array ) fetchData( $strSql, $objDatabase );

		$intValidPropertyCount = $arrintActivePropertyCount[0]['property_count'];

		return $intValidPropertyCount;
	}

	public static function fetchPropertyFMORequirementDetailsByIdbyCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ppe.cid,
						ppe.property_id,                        
                        ppe.value AS fmo_required,
                        pps.value AS fmo_stage_status_id
					FROM
						property_preferences ppe
   	                    JOIN property_preferences pps ON ( ppe.cid = pps.cid AND ppe.property_id = pps.property_id AND pps.key = \'FINANCIAL_MOVE_OUT_APPLICATION_STATUS\' )
					WHERE
                        ppe.cid = ' . ( int ) $intCid . '
                        AND ppe.property_id = ' . ( int ) $intPropertyId . '
                        AND ppe.key = \'ENABLE_APPLICATION_FINANCIAL_MOVE_OUT\';';

		return executeSql( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByTransmissionVendorIdByKeyByCompanyUserIdByCid( $intTransmissionVendorId, $strKey, $intCompanyUserId, $boolIsAdministrator, $intCid, $objDatabase, $intPageNo = NULL, $intPageSize = NULL, $boolIsEnabledLateFees = false, $arrintPropertyGroupIds = [], $boolIsShowDisabled = false ) {

		$boolIsPaginationRequired = false;
		if( false == is_null( $intPageSize ) && false == is_null( $intPageNo ) ) {
			$intOffset                = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit                 = ( int ) $intPageSize;
			$boolIsPaginationRequired = true;
		}

		$strJoin         = '';

		$strAndCondition  = ' AND p.is_disabled = 0';
		if( false != $boolIsShowDisabled ) {
			$strAndCondition  = ' AND p.is_disabled <> 0
			AND pp.ps_product_id = ' . \CPsProduct::HISTORICAL_ACCESS;
		}

		if( false == $boolIsAdministrator ) {
			$strJoin .= 'JOIN view_company_user_properties vcup ON ( vcup.company_user_id = ' . ( int ) $intCompanyUserId . ' AND vcup.property_id = p.id AND vcup.cid = p.cid ) ';
		}

		if( true == $boolIsEnabledLateFees ) {
			$strJoin         .= 'JOIN property_preferences ppre ON ( pgs.cid = ppre.cid AND p.id = ppre.property_id AND ppre.key = \'ENABLED_LATE_FEES\' ) ';
			$strAndCondition .= ' AND ( ppre.value = \'1\' ) ';
		}

		$strJoin         .= ' JOIN property_preferences ppr ON ( p.id = ppr.property_id AND p.cid= ppr.cid ) ';
		$strAndCondition .= ' AND ppr.key = ' . pg_escape_literal( $objDatabase->getHandle(), $strKey ) .
		                    ' AND  string_to_array(trim(ppr.value, \'[]\'), \',\') @> string_to_array( \'' . $intTransmissionVendorId . '\', \',\')';
		if( CTransmissionVendor::GL_EXPORT_PEOPLESOFT != $intTransmissionVendorId ) {
			$strAndCondition .= 'AND pp.ps_product_id = ' . CPsProduct::ENTRATA;
		}
		if( CTransmissionVendor::GL_EXPORT_YARDI_SIPP == $intTransmissionVendorId ) {
			$strAndCondition .= 'AND p.remote_primary_key IS NOT NULL';
		}

		if( false != valArr( $arrintPropertyGroupIds ) ) {
			$strJoin         .= ' LEFT JOIN property_group_associations pga ON ( p.id = pga.property_id AND p.cid= pga.cid ) ';
			$strAndCondition .= ' AND pga.property_group_id IN ( ' . implode( ', ', $arrintPropertyGroupIds ) . ' )';
		}

		$strSql = 'SELECT
				DISTINCT ON( lower( p.property_name ), order_num, p.id )
				p.*,
				pgs.activate_standard_posting
			FROM
				properties p
				JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id AND pgs.activate_standard_posting = true )
				JOIN property_products pp ON ( pgs.cid = pp.cid AND ( p.id = pp.property_id OR pp.property_id IS NULL ) )
				' . $strJoin . '
			WHERE
			p.cid = ' . ( int ) $intCid . '
            ' . $strAndCondition . '
			ORDER BY
				lower( p.property_name ) ASC,
				order_num';

		if( true == $boolIsPaginationRequired ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertyFMORequirementDetailsByPropertyIdsbyCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valArr( $arrintPropertyIds ) ) {
			return [];
		}

		$strSql = 'SELECT
						ppe.cid,
						ppe.property_id,                        
                        ppe.value AS fmo_required,
                        pps.value AS fmo_stage_status_id
					FROM
						property_preferences ppe
   	                    JOIN property_preferences pps ON ( ppe.cid = pps.cid AND ppe.property_id = pps.property_id AND pps.key = \'FINANCIAL_MOVE_OUT_APPLICATION_STATUS\' )
					WHERE
                        ppe.cid = ' . ( int ) $intCid . '
                        AND ppe.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
                        AND ppe.key = \'ENABLE_APPLICATION_FINANCIAL_MOVE_OUT\';';

		$arrmixData                  = fetchData( $strSql, $objDatabase );
		$arrmixFMORequirementDetails = [];

		if( true == valArr( $arrmixData ) ) {
			foreach( $arrmixData as $arrstrResult ) {
				$arrmixFMORequirementDetails[$arrstrResult['property_id']] = $arrstrResult;
			}
		}

		return $arrmixFMORequirementDetails;
	}

	public static function fetchAssetPropertiesByNamesByCidByCompanyUserId( $arrstrProperties, $intCid, $intCompanyUserId, $objClientDatabase, $boolIsAdministrator = false ) {
		if( false == valArr( $arrstrProperties ) ) {
			return NULL;
		}

		$strSql = CProperties::buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator, true, true );
		$strSql .= ' AND p.id IN (
								SELECT
									property_id
								FROM
									property_preferences
								WHERE
									cid = ' . ( int ) $intCid . '
									AND key = \'' . CPropertyPreference::TRACK_INVENTORY_QUANTITIES . '\'
									AND value = \'1\'
							)
							AND LOWER( p.property_name ) IN ( ' . sqlStrImplode( array_map( 'addslashes', $arrstrProperties ) ) . ' )
							AND p.is_disabled = 0';

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchStudentEnabledPricingPropertiesByPropertyIdsByCid( $intCid, $objDatabase ) {

		if( false == valId( $intCid ) ) {
			return NULL;
		}
		$strSql = sprintf( ' with student_properties AS (
                            SELECT 
                                p.id AS property_id, 
                                p.cid, 
                                p.property_name
                            FROM properties p
                            JOIN load_properties(ARRAY [ %d ]::int [ ], ARRAY [ ]::int [ ], ARRAY [ %d ]::int [ ]) lp ON lp.cid = p.cid AND lp.property_id = p.id
                            INNER JOIN property_preferences PP ON pp.property_id = lp.property_id AND pp.key = \'PRICING_MODEL\' AND pp.value = \'Student\'
                            INNER JOIN property_preferences ppr ON ppr.property_id = lp.property_id AND ppr.key = \'PRICING_LEASES\' AND ppr.value <> \'None\'
                            WHERE 
                                lp.is_student_property = 1 
                                AND lp.is_disabled = 0
							),
							student_lease_terms AS 
							( SELECT
							    cid,
							    property_id,
							    property_name,
							    json_agg(json_build_object(\'lease_term_id\', lt.lease_term_id,\'past_lease_term_id\', lt.past_lease_term_id,
							      \'current_lease_start_window_id\', lt.current_lease_start_window_id,\'past_lease_start_window_id\', lt.past_lease_start_window_id,
							      \'lease_term_name\', lt.lease_name, \'lease_name_start_year_past\',
							       extract(YEAR FROM lt.start_date)::INTEGER, \'lease_name_end_year_past\', 
							       extract(YEAR FROM lt.end_date)::INTEGER, \'lease_start_date\', lt.start_date, \'lease_end_date\',
							      lt.end_date,\'past_past_lease_start_window_id\',
							      lt.past_past_lease_start_window_id, \'current_window_start_date\', lt.current_window_start_date, \'current_window_end_date\', lt.current_window_end_date,
							      \'past_window_start_date\', lt.past_window_start_date, \'past_window_end_date\', lt.past_window_end_date,
							      \'past_past_window_start_date\', lt.past_past_window_start_date, \'past_past_window_end_date\', lt.past_past_window_end_date) ORDER BY lt.lease_name, lt.start_date, lt.end_date ) AS lease_terms
							  FROM (
									SELECT lt.cid,
									         lsw.property_id,
									         p.property_name,
									         lt.id AS lease_term_id,
									         lsw.id AS current_lease_start_window_id,
									         pre_lsw.id as past_lease_start_window_id,
									         pre_lsw.lease_term_id as past_lease_term_id,
									         lt.name AS lease_name,
									         lsw.start_date,
									         lsw.end_date,
									         lsw.start_date AS current_window_start_date,
									         lsw.end_date AS current_window_end_date,
									         pre_lsw.start_date AS past_window_start_date,
									         pre_lsw.end_date AS past_window_end_date,
									         ppre_lsw.start_date AS past_past_window_start_date,
									         ppre_lsw.end_date AS past_past_window_end_date,
									         ppre_lsw.id as past_past_lease_start_window_id
									  FROM lease_terms lt
									       JOIN lease_start_windows lsw ON (lsw.cid = lt.cid AND lsw.lease_term_id =
									         lt.id)
									       JOIN student_properties p ON (p.cid = lsw.cid AND p.property_id = lsw.property_id)
									       JOIN property_charge_settings pcs ON (pcs.cid = lt.cid AND
									         pcs.lease_term_structure_id = lt.lease_term_structure_id AND
									         lsw.property_id = pcs.property_id)
									       LEFT JOIN LATERAL
											(
												SELECT 
													p_lsw.*
													FROM 
													lease_start_windows p_lsw
												WHERE
													p_lsw.cid = lt.cid 
													AND p_lsw.property_id = lsw.property_id 
													AND p_lsw.start_date < lsw.start_date
													AND extract(MONTH FROM p_lsw.start_date)::INTEGER = extract(MONTH FROM lsw.start_date)::INTEGER
													AND extract(YEAR FROM p_lsw.start_date)::INTEGER = extract(YEAR FROM lsw.start_date)::INTEGER - 1
													ORDER BY 
													CASE 
													    WHEN p_lsw.lease_term_id = lt.id THEN TRUE
													    WHEN ( abs( (lsw.end_date::DATE - lsw.start_date::DATE) - (p_lsw.end_date::DATE - p_lsw.start_date::DATE) ) <= ' . CPricingLibrary::LEASE_TERM_DAYS_WITHIN . ' )
													        AND ( abs( extract(DAY FROM p_lsw.start_date)::INTEGER - extract(DAY FROM lsw.start_date)::INTEGER ) <= ' . CPricingLibrary::LEASE_START_DAYS_WITHIN . ' ) THEN TRUE
													END,
													p_lsw.start_date ASC
													LIMIT 1
											) AS pre_lsw ON pre_lsw.cid = lt.cid
											LEFT JOIN LATERAL
											(
											SELECT p_lsw.cid,
												p_lsw.id,
												p_lsw.start_date,
												p_lsw.end_date
											FROM lease_start_windows p_lsw
											WHERE p_lsw.cid = lt.cid AND p_lsw.property_id = lsw.property_id AND p_lsw.start_date < lsw.start_date AND
											extract(MONTH FROM p_lsw.start_date)::INTEGER = extract(MONTH FROM lsw.start_date)::INTEGER AND extract(YEAR
											FROM p_lsw.start_date)::INTEGER = extract(YEAR FROM lsw.start_date)::INTEGER - 2
											ORDER BY 
											CASE
												WHEN p_lsw.lease_term_id = lt.id THEN TRUE
												WHEN (abs((lsw.end_date::DATE - lsw.start_date::DATE) -(
												p_lsw.end_date::DATE - p_lsw.start_date::DATE)) <= ' . CPricingLibrary::LEASE_TERM_DAYS_WITHIN . ')
												AND (abs(extract(DAY
												FROM p_lsw.start_date)::INTEGER - extract(DAY
												FROM lsw.start_date)::INTEGER) <= ' . CPricingLibrary::LEASE_START_DAYS_WITHIN . ') THEN TRUE
											END,
											p_lsw.start_date ASC
											LIMIT 1
											) AS ppre_lsw ON ppre_lsw.cid = lt.cid
									  WHERE lt.cid = %d AND
									        lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND
									        ( ( lsw.start_date <= CURRENT_DATE AND ( lsw.end_date >= CURRENT_DATE OR lsw.end_date IS NULL ) ) OR ( lsw.start_date > CURRENT_DATE AND ( lsw.end_date >= CURRENT_DATE OR lsw.end_date IS NULL ) ) ) AND
									        (COALESCE(lt.default_lease_term_id, 0) <> 201 OR
									        lt.default_lease_term_id IS NULL)
									  ORDER BY (lsw.start_date, lsw.end_date) ASC) as lt 
									  GROUP by cid, property_id, property_name
									  ORDER by property_name
							        ),
							    student_space_config AS (
							    SELECT
										psc.cid,
										psc.property_id,
										json_agg(json_build_object(\'id\', sc.id, \'name\', sc.name, \'prop_id\',psc.property_id ) ) AS space_options
									FROM 
									    space_configurations sc
									    JOIN property_space_configurations psc on psc.cid = sc.cid and psc.space_configuration_id = sc.id
									    JOIN student_properties sp ON (sp.cid = psc.cid AND sp.property_id = psc.property_id)
									GROUP BY 
										psc.cid, 
										psc.property_id 
								  ),
								  student_floor_plans AS(
									SELECT
										pf.cid,
										pf.property_id,
										json_agg( json_build_object( \'id\', pf.id, \'name\', pf.floorplan_name, \'bedroom_count\', pf.number_of_bedrooms ) ) AS floorplans,
										json_agg(json_build_object( \'bedroom_count\',COALESCE( pf.number_of_bedrooms, 0 ) ) ) AS bedroom_count
									FROM 
										property_floorplans pf     
										JOIN student_properties sp ON (sp.cid = pf.cid AND sp.property_id = pf.property_id)
										WHERE pf.deleted_on is NULL 
										GROUP BY pf.cid,pf.property_id   
								  ),
								  student_unit_types AS(
									SELECT 
										ut.cid,
										ut.property_id,
										json_agg( json_build_object( \'id\', ut.id, \'name\', ut.name, \'bedroom_count\', ut.number_of_bedrooms ) ) as unit_type_names
									FROM unit_types ut
									JOIN student_properties sp ON ( ut.cid = sp.cid AND ut.property_id = sp.property_id )
									WHERE ut.deleted_on is NULL
									GROUP BY ut.cid,ut.property_id
								  )
  
								SELECT 
									slt.*,
								    spc.space_options,
								    sfp.floorplans,
								    sfp.bedroom_count,
								    sut.unit_type_names
								FROM
								    
									student_lease_terms AS slt
								    JOIN student_space_config AS spc ON slt.cid = spc.cid AND slt.property_id = spc.property_id
								    JOIN student_floor_plans AS sfp on slt.cid = sfp.cid and slt.property_id = sfp.property_id
								    JOIN student_unit_types AS sut on slt.cid = sut.cid and slt.property_id = sut.property_id 
								    ORDER by slt.property_name', $intCid, CPsProduct::PRICING_STUDENT, $intCid );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByPropertyPreferenceKeyByValuesByCompanyUserIdByCid( $strKey, $intTransmissionVendorId, $intCompanyUserId, $intCid, $boolIsAdministrator, $objDatabase, $boolIsShowDisabled = false ) {
		$strJoin         = '';

		$strAndCondition  = ' AND p.is_disabled = 0';
		if( false != $boolIsShowDisabled ) {
			$strAndCondition  = ' AND p.is_disabled <> 0';
		}

		if( false == $boolIsAdministrator ) {
			$strJoin .= 'JOIN view_company_user_properties vcup ON ( vcup.company_user_id = ' . ( int ) $intCompanyUserId . ' AND vcup.property_id = p.id AND vcup.cid = p.cid ) ';
		}

		if( false != is_numeric( $intTransmissionVendorId ) ) {
			$strJoin         .= ' JOIN property_preferences ppr ON ( p.id = ppr.property_id AND p.cid = ppr.cid ) ';
			$strAndCondition .= ' AND ppr.key = ' . pg_escape_literal( $objDatabase->getHandle(), $strKey ) . ' AND ppr.value =\'' . $intTransmissionVendorId . '\'';
			if( CTransmissionVendor::AP_EXPORT_YARDI_SIPP == $intTransmissionVendorId ) {
				$strAndCondition .= ' AND p.remote_primary_key IS NOT NULL ';
			}
		}

		$strSql = 'SELECT
						DISTINCT ON( lower( p.property_name ), order_num, p.id )
						p.*,
						pgs.activate_standard_posting
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id AND pgs.activate_standard_posting = true )
						JOIN property_products pp ON ( pgs.cid = pp.cid AND ( p.id = pp.property_id OR pp.property_id IS NULL ) )
						' . $strJoin . '
					WHERE
						p.cid = ' . ( int ) $intCid . '

						' . $strAndCondition . '
					ORDER BY
						lower( p.property_name ) ASC,
						order_num';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByKeyByValuesByCompanyUserIdByCid( $strKey, $arrstrValues, $intCompanyUserId, $intCid, $boolIsAdministrator, $objDatabase, $boolIsShowDisabled = false ) {
		$strJoin = '';
		$strAndCondition  = ' AND p.is_disabled = 0';
		if( false != $boolIsShowDisabled ) {
			$strAndCondition  = ' AND p.is_disabled <> 0
			AND pp.ps_product_id = ' . \CPsProduct::HISTORICAL_ACCESS;
		}

		if( false == $boolIsAdministrator ) {
			$strJoin .= 'JOIN view_company_user_properties vcup ON ( vcup.company_user_id = ' . ( int ) $intCompanyUserId . ' AND vcup.property_id = p.id AND vcup.cid = p.cid ) ';
		}
		$strSql = 'SELECT
						DISTINCT ON( lower( p.property_name ), order_num, p.id )
						p.*
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id AND pgs.activate_standard_posting = true )
						JOIN property_products pp ON ( pgs.cid = pp.cid AND ( p.id = pp.property_id OR pp.property_id IS NULL ) )
						JOIN property_preferences ppr ON ( p.id = ppr.property_id AND p.cid = ppr.cid )
						' . $strJoin . '
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND ppr.key = ' . pg_escape_literal( $strKey ) . ' 
						AND string_to_array ( regexp_replace( regexp_replace( ppr.value, \'[]]\', \'\'),\'[[]\', \'\'), \',\' ) && string_to_array( \'' . implode( ',', $arrstrValues ) . '\', \',\')
						' . $strAndCondition . '
					ORDER BY
						lower( p.property_name ) ASC,
						order_num';
		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByPsProductId( $intPsProductId, $objDatabase ) {

		$strSql = 'SELECT
					    p.id,
						p.property_name,
						c.id as cid,
						c.company_name,
						c.database_id,
						smad.created_on as social_media_start_date,
						smad.social_post_type_id, 
						MAX( smad.details->>\'user_profile_name\' ) as user_profile_name,
						MAX( smad.details->>\'business_profile_name\' ) as business_profile_name,
						MAX( smad.details->>\'authentication_failed_on\' ) as authentication_failed_on,
						CASE WHEN p.property_type_id = ' . CPropertyType::STUDENT . ' THEN ' . CPropertyType::STUDENT . ' ELSE ' . CPropertyType::APARTMENT . ' END AS property_type,
						MAX( CASE WHEN smad.social_post_type_id = ' . CSocialPostType:: FACEBOOK . ' THEN 1 END ) AS has_fb_account_linked,
						MAX( CASE WHEN smad.social_post_type_id = ' . CSocialPostType:: TWITTER . ' THEN 1 END ) AS has_twitter_account_linked,
						MAX( CASE WHEN smad.social_post_type_id = ' . CSocialPostType:: GOOGLE_MYBUSINESS . ' THEN 1 END ) AS has_google_account_linked,
						MAX( CASE WHEN ( smad.user_access_token IS NULL AND smad.social_post_type_id = 3 ) OR ( smad.token_secret IS NULL AND smad.social_post_type_id <> 3 ) THEN 0 ELSE 1 END ) AS account_status
					FROM
						properties p
						JOIN clients c ON ( c.id = p.cid AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' )
						JOIN property_products AS pp ON ( c.id = pp.cid AND (pp.property_id = p.id OR pp.property_id IS NULL ))
						LEFT JOIN social_media_account_details AS smad ON ( smad.property_id = pp.property_id AND smad.cid = pp.cid AND smad.deleted_by IS NULL AND smad.deleted_on IS NULL )
					WHERE
						pp.cid = p.cid
						AND pp.ps_product_id = ' . ( int ) $intPsProductId . '
						AND p.is_disabled = 0 
						AND ( smad.social_post_type_id = ' . CSocialPostType::FACEBOOK . ' OR smad.social_post_type_id = ' . CSocialPostType::TWITTER . ' OR smad.social_post_type_id = ' . CSocialPostType::GOOGLE_MYBUSINESS . ' )
					GROUP BY
						smad.social_post_type_id, smad.details->>\'authentication_failed_on\',  c.id, p.id, p.property_name, p.property_type_id, pp.created_on, smad.created_on
					ORDER BY
						smad.social_post_type_id DESC , smad.details->>\'authentication_failed_on\' ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByPsProductIdsByCidsByPropertyPreferenceKey( $arrintPsProductIds, $arrintCids, $strKey, $objDatabase, $intShowDisabledProperties = 0 ) {
		if( false == valArr( $arrintPsProductIds ) || false == valStr( $strKey ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT( p.id ),
						p.*
					FROM
						properties p
						JOIN property_products AS pp ON ( pp.cid = p.cid AND pp.property_id = p.id OR pp.property_id IS NULL )
						JOIN property_preferences ppf ON ( pp.cid = ppf.cid AND pp.property_id = ppf.property_id )
					WHERE
						p.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND pp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
						AND ppf.key = \'' . $strKey . '\'
						AND ppf.value = \'1\'
						' . ( ( 1 == $intShowDisabledProperties ) ? '' : ' AND p.is_disabled = ' . ( int ) $intShowDisabledProperties . ' ' ) . '
					ORDER BY
						p.property_name';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPaginatedPropertiesByCidByPropertiesFilterByExcludedPropertyIds( $intCid, $objPropertiesFilter, $objPagination, $objDatabase, $arrintExcludedPropertyIds = [] ) {

		$strWhereClause = '';

		if( true == valArr( $arrintExcludedPropertyIds ) ) {
			$strWhereClause = 'AND p.id NOT IN ( ' . implode( ', ', $arrintExcludedPropertyIds ) . ' )';
		}

		if( true == valArr( $objPropertiesFilter->getPropertyGroupIds() ) ) {
			$strPropertyGroupsJoin = 'JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( ',', $objPropertiesFilter->getPropertyGroupIds() ) . ']::INTEGER[] ) as lp ON lp.cid = p.cid AND lp.property_id = p.id';
		} else {
			return NULL;
		}

		$strSql = 'SELECT DISTINCT
						p.*,
						util_get_system_translated( \'name\', pt.name, pt.details ) AS property_type_name
					FROM
						properties p
						LEFT JOIN property_types pt ON ( pt.id = p.property_type_id ) ';

		$strSql .= $strPropertyGroupsJoin . ' WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' ) ';

		if( true == valStr( $objPropertiesFilter->getKeywordSearch() ) ) {
			$strSql .= ' AND ( p.property_name Ilike \'%' . addslashes( $objPropertiesFilter->getKeywordSearch() ) . '%\' OR p.id = ' . ( int ) addslashes( $objPropertiesFilter->getKeywordSearch() ) . ' OR p.lookup_code Ilike \'%' . addslashes( $objPropertiesFilter->getKeywordSearch() ) . '%\') ';
		}

		if( false == ( bool ) $objPropertiesFilter->getIsShowDisabled() ) {
			$strSql .= ' AND p.is_disabled = 0';
		}

		$strSql .= $strWhereClause . ' ORDER BY p.property_name ASC ';

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTotalPropertiesCountByCidByPropertiesFilterByExcludedPropertyIds( $intCid, $objPropertiesFilter, $objDatabase, $arrintExcludedPropertyIds = [] ) {

		$strWhereClause = '';

		if( true == valArr( $arrintExcludedPropertyIds ) ) {
			$strWhereClause = ' AND p.id NOT IN ( ' . implode( ', ', $arrintExcludedPropertyIds ) . ' )';
		}

		if( true == valArr( $objPropertiesFilter->getPropertyGroupIds() ) ) {
			$strPropertyGroupsJoin = 'JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( ',', $objPropertiesFilter->getPropertyGroupIds() ) . ']::INTEGER[] ) as lp ON lp.cid = p.cid AND lp.property_id = p.id';
		} else {
			return NULL;
		}

		$strSql = 'SELECT
						COUNT( p.id ) AS COUNT
					FROM
						properties p
						' . $strPropertyGroupsJoin . '
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )';

		if( true == valStr( $objPropertiesFilter->getKeywordSearch() ) ) {
			$strSql .= ' AND ( p.property_name Ilike \'%' . addslashes( $objPropertiesFilter->getKeywordSearch() ) . '%\' OR p.id = ' . ( int ) addslashes( $objPropertiesFilter->getKeywordSearch() ) . ' OR p.lookup_code Ilike \'%' . addslashes( $objPropertiesFilter->getKeywordSearch() ) . '%\') ';
		}

		if( false == ( bool ) $objPropertiesFilter->getIsShowDisabled() ) {
			$strSql .= 'AND p.is_disabled = 0';
		}

		$strSql .= $strWhereClause;

		return self::fetchColumn( $strSql, 'count', $objDatabase );

	}

	public static function fetchContractedPropertiesByPsProductIdsByCids( $arrintPsProductIds, $arrintCid, $objDatabase, $intShowDisabledProperties = 0 ) {
		if( false == valArr( $arrintPsProductIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT( p.id ),
						p.*
					FROM
						properties p
						JOIN property_products AS pp ON ( pp.cid = p.cid AND pp.property_id = p.id OR pp.property_id IS NULL )
					WHERE
						p.cid IN ( ' . implode( ',', $arrintCid ) . ' )
						AND pp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
						' . ( ( 1 == $intShowDisabledProperties ) ? '' : ' AND p.is_disabled = ' . ( int ) $intShowDisabledProperties . ' ' ) . '
					ORDER BY
						p.property_name';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchIntercompanyArEntityPropertiesByCid( $intCid, $objDatabase, $boolShowDisabledData = false ) {

		if( !valId( $intCid ) ) {
			return NULL;
		}

		$strJoin    = '';
		$strWhere   = ' AND p.is_disabled = 0 ';
		if( $boolShowDisabledData ) {
			$strWhere   = '';
			$strJoin    = 'JOIN property_products pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.ps_product_id = ' . \CPsProduct::HISTORICAL_ACCESS . ' AND p.is_disabled = 1 )';
		}

		$strSql = 'SELECT
						p.*,
						CASE
							WHEN p.property_type_id =  ' . CPropertyType::CORPORATE . ' THEN 1
							ELSE 2
						END as property_group_type,
						ap.company_name
					FROM
						properties p
						JOIN property_details pd ON ( p.cid = pd.cid AND p.id = pd.property_id )
						JOIN ap_payees ap ON ( pd.cid = ap.cid AND pd.ap_payee_id = ap.id )
						' . $strJoin . '
					WHERE
						p.cid = ' . ( int ) $intCid . '
						' . $strWhere . '
						AND p.property_type_id NOT IN( ' . CPropertyType::CLIENT . ',' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
					ORDER BY 
						property_group_type,
						lower ( property_name )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchIsPropertyMigratedByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) : bool {

		$strSql = 'SELECT
            p.id
          FROM
            properties p
            JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id )
          WHERE
            p.cid = ' . ( int ) $intCid . '
            AND p.id = ' . ( int ) $intPropertyId . '
            AND ( pgs.is_ap_migration_mode IS TRUE OR pgs.is_ar_migration_mode IS TRUE  )';

		$arrmixData = ( array ) fetchData( $strSql, $objDatabase );

		if( 1 > \Psi\Libraries\UtilFunctions\count( $arrmixData ) ) {
			return false;
		}

		return true;
	}

	public static function fetchPropertiesByOccupancyTypeIdsByCid( $arrintOccupancyTypeIds, $intCid, $objDatabase, $arrintPropertyIds = [] ) {

		if( false == valArr( $arrintOccupancyTypeIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strWhereCondition = '';

		if( true == valArr( $arrintPropertyIds ) ) {
			$strWhereCondition = ' AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}

		$strSql = 'SELECT
            *
          FROM
            properties p
          WHERE
            p.cid = ' . ( int ) $intCid . '
            ' . $strWhereCondition . '
            AND ( sort( p.occupancy_type_ids ) = sort( ARRAY[ ' . implode( ',', $arrintOccupancyTypeIds ) . ' ] ) AND p.occupancy_type_ids::TEXT IS NOT NULL )
            AND p.is_disabled = 0
            AND p.property_type_id NOT IN ( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )';

		return parent::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchSimplePropertiesByPsProductIdsByCids( $intPsProductId, $arrintCids, $objDatabase ) {
		$strSql = 'SELECT
						p.id,
						p.property_name,
						c.company_name
					FROM
						properties p
						JOIN property_products AS pp ON ( pp.property_id = p.id OR pp.property_id IS NULL )
						JOIN clients AS c ON ( c.id = p.cid )
					WHERE
						pp.cid = p.cid
						AND pp.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND pp.ps_product_id = ' . ( int ) $intPsProductId . '
						AND p.is_disabled = 0
					ORDER BY
						p.id';

		$arrstrProperties = ( array ) fetchData( $strSql, $objDatabase );

		return $arrstrProperties;
	}

	public static function fetchPropertiesByOccupancyTypeIdsByExcludingPropertyTypeIdsByCid( $arrintOccupancyTypeIds, $arrintExcludingPropertyTypeIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.occupancy_type_ids
					FROM
						properties p
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.property_type_id NOT IN ( ' . implode( ',', $arrintExcludingPropertyTypeIds ) . ' )
						AND p.occupancy_type_ids && ARRAY[' . implode( ',', $arrintOccupancyTypeIds ) . ']
					ORDER BY
						property_name';

		$arrmixProperties = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixProperties ) ) {
			return NULL;
		}

		foreach( $arrmixProperties as $arrstrProperty ) {
			$arrstrProperties[$arrstrProperty['id']] = $arrstrProperty;
		}

		return $arrstrProperties;
	}

	public static function fetchPropertiesByIdByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						p.*
					FROM
						properties p
					WHERE
						p.cid = ' . ( int ) $intCid . ' 
						AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.is_disabled = 0;';

		return parent::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByIdsByCidsByPsProductIds( $arrintPropertyIds, $arrintCid, $arrintPsProductIds, $objDatabase ) {

		$strSql = 'SELECT
						p.*
					FROM
						properties p
						JOIN property_products pp ON ( pp.cid = p.cid AND pp.property_id IS NULL )
					WHERE
						p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pp.cid IN ( ' . implode( ',', $arrintCid ) . ' )
						AND p.is_disabled = 0
						AND pp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
					UNION
					SELECT
						p.*
					FROM
						properties p
						JOIN property_products pp ON ( pp.cid = p.cid AND pp.property_id = p.id )
					WHERE
						p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pp.cid IN ( ' . implode( ',', $arrintCid ) . ' )
						AND p.is_disabled = 0
						AND pp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )';

		return parent::fetchProperties( $strSql, $objDatabase, true );
	}

	public static function fetchCustomPropertiesDetailsByPropertyIds( $arrintPropertyIds, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return;
		}

		$strSql = 'SELECT
						p.*,
						pa.state_code,
						 p.property_name || \' ( \' || pa.city || \', \' || pa.state_code || \' )\' as portfolio_name
					FROM
						properties p
						JOIN property_addresses pa ON ( pa.cid = p.cid AND pa.property_id = p.id AND pa.is_alternate = false )
					WHERE
						p.id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
						AND pa.address_type_id = ' . CAddressType::PRIMARY;

		return parent::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchCustomExceptionAssociatedPropertiesByExceptionIdByCid( $intExceptionId, $intCid, $objDatabase ) {
		if( false == valId( $intExceptionId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						p.id,
						p.property_name
					FROM 
						properties p
                        JOIN property_maintenance_exceptions pme ON (pme.cid = p.cid AND pme.property_id = p.id )
					WHERE 
						pme.cid = ' . ( int ) $intCid . '
		                AND pme.maintenance_exception_id = ' . ( int ) $intExceptionId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomMigrationModeOnPropertiesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		$arrintPropertyIds = array_filter( $arrintPropertyIds );

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						p.id
					FROM
						properties p
						JOIN property_gl_settings pgs ON ( pgs.cid = p.cid and pgs.property_id = p.id ) 
					WHERE
						pgs.cid = ' . ( int ) $intCid . '
						AND pgs.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.is_disabled = 0
						AND ( pgs.is_ar_migration_mode = true OR pgs.is_ap_migration_mode = true )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertyLocaleCodeByIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						locale_code
					FROM
						properties
					WHERE
						id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid;

		$arrmixValues = fetchData( $strSql, $objDatabase );
		return $arrmixValues[0]['locale_code'];
	}

	public static function fetchCustomPropertyLocaleCodesByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						id,
						locale_code
					FROM
						properties
					WHERE
						id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) 
						AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchStudentPricingProperties( $objDatabase ) {
		$strSql = ' 
                SELECT 
                    p.cid,p.id,p.details 
                FROM 
                properties p
                JOIN load_properties( ARRAY[]::int[], ARRAY[]::int[], ARRAY[ ' . CPsProduct::PRICING_STUDENT . ' ]::int [ ] ) lp ON lp.cid = p.cid and lp.property_id = p.id
                JOIN property_preferences PP ON pp.property_id = lp.property_id AND pp.key = \'PRICING_MODEL\' AND pp.value = \'Student\'
                WHERE
                lp.is_student_property = 1
                AND lp.is_disabled = 0 ';

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchAssetLocationPropertiesByPropertyIdsCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == ( $arrintPropertyIds = getIntValuesFromArr( $arrintPropertyIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM 
						properties
					WHERE ( id, cid ) IN (
						SELECT
							p.id,
							p.cid
						FROM
							asset_locations al
							JOIN property_group_asset_locations pgal ON ( al.cid = pgal.cid AND al.id = pgal.asset_location_id )
							JOIN property_groups pg ON ( pg.cid = pgal.cid AND pgal.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
							JOIN property_group_associations pga ON ( pgal.cid = pga.cid AND pg.id = pga.property_group_id )
							JOIN properties p ON ( p.cid = pgal.cid AND p.id = pga.property_id AND p.is_disabled = 0 AND p.is_managerial = 0 )
							JOIN property_gl_settings pgs ON pgs.cid = p.cid AND pgs.property_id = p.id AND activate_standard_posting = TRUE 
						WHERE
							p.cid = ' . ( int ) $intCid . '
							AND pga.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
							AND al.deleted_by IS NULL
						GROUP BY 
							p.id, p.cid
						HAVING 
							count(al.id) > 1
						ORDER BY p.property_name ASC
					)';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllStudentProperties( $objDatabase ) {
		$strSql = sprintf( 'SELECT 
						p.id,
						p.cid
					FROM
					properties p 
					JOIN load_properties(ARRAY [ ]::int [ ], ARRAY [ ]::int [ ], ARRAY [ ]::int [ ]) lp 
					ON lp.cid = p.cid AND lp.property_id = p.id 
					WHERE 
					%d = ANY ( p.occupancy_type_ids )
					AND lp.is_student_property = 1
					AND lp.is_disabled = 0
					ORDER BY lp.cid DESC', COccupancyType::STUDENT );

		return self::fetchProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByOccupancyTypeIdByCid( $intOccupancyTypeId, $intCid, $objDatabase, $arrintPropertyIds = [], $strRequiredFields = '', $strJoinSql = '' ) {
		if( false == valId( $intCid ) || false == valId( $intOccupancyTypeId ) ) {
			return NULL;
		}

		$strWhereSql  = ( true == valArr( $arrintPropertyIds ) ) ? ' AND p.id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' ) ' : '';
		$strSelectSql = ( true == valStr( $strRequiredFields ) ) ? $strRequiredFields : ' COUNT( p.id ) AS student_property_count ';
		$strJoinSql   .= ( COccupancyType::STUDENT == $intOccupancyTypeId ) ? ' JOIN property_preferences pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.key = \'ENABLE_SEMESTER_SELECTION\' AND pp.value = \'1\' ) ' : '';

		$strSql = 'SELECT
					' . $strSelectSql . '
					FROM
						properties p ' . $strJoinSql . '
					WHERE
						p.cid = ' . ( int ) $intCid . $strWhereSql . '
						AND ' . $intOccupancyTypeId . ' = ANY( p.occupancy_type_ids )';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchPropertiesByUnitSpaceIdsByCid( $arrintUnitSpaceIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintUnitSpaceIds ) ) return NULL;

		$strSql = 'SELECT
						p.id,
						p.property_id,
						u.id as unit_space_id
					FROM
						unit_spaces As u
						JOIN properties AS p ON ( p.id = u.property_id AND p.cid = u.cid )
					WHERE
						p.cid = ' . ( int ) $intCid .
		          ' AND u.id IN ( ' . implode( ',', $arrintUnitSpaceIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomActivePropertiesByActiveContractsByPsProductIdsByCid( $arrintPsProductIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPsProductIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name
					FROM
						clients c
						JOIN properties p ON ( c.id = p.cid AND p.disabled_on IS NULL )
						JOIN contract_properties cp ON ( cp.property_id = p.id AND cp.cid = p.cid )
						JOIN contracts cts ON ( cts.id = cp.contract_id AND cts.cid = cp.cid AND cts.contract_status_type_id = ' . CContractStatusType::CONTRACT_APPROVED . ' )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND cp.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' ) 
						AND ( cp.termination_date IS NULL OR cp.termination_date > NOW() )
						AND cp.renewal_contract_property_id IS NULL
						AND cp.is_last_contract_record = true
						AND cp.close_date IS NOT NULL
					ORDER BY cp.updated_on DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByIdsByCidsByCompanyStatusTypeIds( $arrintPropertyIds, $arrintCids, $arrintCompanyStatusTypeIds, $objDatabase, $boolIsReturnKeyedArray = true, $strOrderBy = NULL ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) || false == valArr( $arrintCompanyStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						p.* 
					FROM 
						properties p 
						JOIN clients c ON ( c.id = p.cid )
					WHERE 
						p.id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' ) 
						AND p.cid IN ( ' . implode( ', ', $arrintCids ) . ' ) 
						AND c.company_status_type_id IN ( ' . implode( ', ', $arrintCompanyStatusTypeIds ) . ' )';

		if( false == is_null( $strOrderBy ) && true == valStr( $strOrderBy ) ) {
			$strSql .= ' ORDER BY ' . $strOrderBy;
		}

		return self::fetchProperties( $strSql, $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchActivePropertyIdsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == ( $arrintPropertyIds = getIntValuesFromArr( $arrintPropertyIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						id
					FROM 
						properties
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
						AND is_disabled = 0';

		$arrmixRecords = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixRecords ) ) {
			return NULL;
		}

		$arrintAssignedPropertyIds = [];

		foreach( $arrmixRecords as $arrmixProperty ) {
			$arrintAssignedPropertyIds[$arrmixProperty['id']] = $arrmixProperty['id'];
		}

		return $arrintAssignedPropertyIds;
	}

	public static function fetchPendingBillbackPropertiesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolShowDisabledData = false ) {

		if( false === valArr( $arrintPropertyIds ) && false === valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name
					FROM
						properties p
						JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( ',', $arrintPropertyIds ) . ' ]::INTEGER[] ) lp ON 
						( p.cid = lp.cid AND p.id = lp.property_id ' . ( ( !$boolShowDisabledData ) ?  ' AND lp.is_disabled = 0 ' : '' ) . ' )
						JOIN ap_details ad ON ( ad.cid = p.cid AND ad.property_id = p.id )
						JOIN ap_headers ah ON ( ah.cid = ad.cid AND ad.ap_header_id = ah.id )
						LEFT JOIN ap_detail_reimbursements adr ON ( ad.cid = adr.cid AND ad.id = adr.original_ap_detail_id AND adr.is_deleted = false )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
						AND NOT ah.is_template
						AND ad.gl_header_id IS NULL
						AND ad.ap_transaction_type_id = ' . CApTransactionType::STANDARD . '
						AND ad.reimbursement_method_id = ' . CReimbursementMethod::BILLBACK . '
						AND ah.transaction_amount_due = 0
						AND adr.id IS NULL
					GROUP BY
						p.id,
						p.property_name
					ORDER BY
						LOWER( property_name )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyDetailsByByCids( $arrintCids, $objClientDatabase, $arrintPropertyIds = NULL ) {

		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strAndCondition = ( true == valArr( $arrintPropertyIds ) ) ? ' AND p.id IN( ' . sqlIntImplode( $arrintPropertyIds ) . ' ) ' : '';

		$strSql = 'SELECT
						p.*,
						pa.street_line1,
						pa.street_line2,
						pa.city,
						pa.state_code,
						pa.postal_code,
						ppn.phone_number as office_phone_number,
						pea.email_address as primary_email_address
					FROM
						properties p
						LEFT JOIN property_addresses pa ON ( p.id = pa.property_id AND p.cid = pa.cid AND pa.address_type_id = ' . CAddressType::PRIMARY . ' AND pa.is_alternate = false )
						LEFT JOIN property_email_addresses pea ON ( p.id = pea.property_id AND p.cid = pea.cid AND pea.email_address_type_id = ' . CEmailAddressType::PRIMARY . ' )
						LEFT JOIN property_phone_numbers ppn ON ( p.id = ppn.property_id AND p.cid = ppn.cid AND ppn.phone_number_type_id = ' . CPhoneNumberType::OFFICE . ' )
					WHERE
						p.is_disabled = 0
						AND p.cid IN ( ' . sqlIntImplode( $arrintCids ) . ' )
						' . $strAndCondition;

		return self::fetchProperties( $strSql, $objClientDatabase );
	}

	public static function fetchCustomPropertiesByPropertyName( $strPropertyName, $objAdminDatabase ) {
		if( false == valStr( $strPropertyName ) ) {
			return NULL;
		}

		$strSql = '
					SELECT 
						p.*,
						c.database_id
					FROM
						properties p
						JOIN clients c ON ( p.cid = c.id )
					WHERE
						p.is_disabled = 0
						AND LOWER( REGEXP_REPLACE( p.property_name, \'[^0-9a-zA-Z]+\', \'\', \'g\' ) ) = \'' . $strPropertyName . '\'
				  ';
		return self::fetchProperties( $strSql, $objAdminDatabase );
	}

	public static function fetchPropertiesWithTimezoneByIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						p.id,
						p.property_name,
						tz.time_zone_name
					FROM
						properties p
						JOIN time_zones tz ON( p.time_zone_id = tz.id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN (' . implode( ',', $arrintPropertyIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllStudentPropertiesWithActiveLeasingGoalsByCid( $intCid, $objDatabase, $boolFetchCount = false, $arrintPropertyIds = array() ) {
		if( false == valId( $intCid ) ) return NULL;

		$strWhereSql = '';

		if( true == $boolFetchCount ) {
			$strSelectParams = 'count( p.id )';
		} else {
			$strSelectParams = 'DISTINCT p.property_name, p.id';
		}

		if( true == valArr( $arrintPropertyIds ) ) {
			$strWhereSql = ' AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		}

		$strSql = 'SELECT ' . $strSelectParams . '
					FROM
						properties p
						JOIN load_properties( ARRAY[]::INT[], ARRAY[]::INT[ ' . ( int ) $intCid . ' ], ARRAY[1]::INT[] ) lp ON ( p.cid = lp.cid AND p.id = lp.property_id AND lp.is_student_property = 1 )
						LEFT JOIN leasing_goals lg ON ( lp.cid = lg.cid AND lg.property_id = lp.property_id AND lg.deleted_by IS NULL )
						LEFT JOIN leasing_goal_associations lga ON ( lga.cid = lg.cid AND lga.leasing_goal_id = lg.id )
						LEFT JOIN lease_start_windows lsw ON ( lsw.cid = lga.cid AND lsw.lease_term_id = lga.lease_term_id AND lsw.property_id = lg.property_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND lga.deleted_by IS NULL
						AND lsw.deleted_on IS NULL
						AND lsw.end_date > CURRENT_DATE
						AND lg.id IS NOT NULL
						' . $strWhereSql;

		if( true == $boolFetchCount ) {
			return fetchData( $strSql, $objDatabase )[0]['count'];
		} else {
			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchPropertyByIdTransmissionVendorIdsByPsProductIdsByCid( $intPropertyId, $arrintTransmissionVendorIds, $arrintPsProductIds, $intCid, $objDatabase, $boolAllPropertyDetails = true, $boolIsDisabledProperty = false ) {
		if( false == valArr( $arrintTransmissionVendorIds ) || false == valArr( $arrintPsProductIds ) ) {
			return NULL;
		}

		$strSelectClause = ' p.* ';
		if( false == $boolAllPropertyDetails ) {
			$strSelectClause = ' p.id, p.property_name, p.lookup_code, p.cid ';
		}

		$strWhereClause = ( true == $boolIsDisabledProperty ) ? ' AND p.is_disabled = 1' : 'AND p.is_disabled = 0';

		$strSql = 'SELECT ' .
		          $strSelectClause . '
					FROM
						properties AS p
						JOIN property_transmission_vendors AS ptv ON ( ptv.cid = p.cid AND ptv.property_id = p.id )
						JOIN company_transmission_vendors AS ctv ON ( ctv.cid = ptv.cid AND ctv.id = ptv.company_transmission_vendor_id )
						JOIN property_products AS pp ON ( pp.cid = p.cid AND pp.ps_product_id = ' . sqlIntImplode( $arrintPsProductIds ) . ' )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id = ' . ( int ) $intPropertyId . '
						AND ctv.transmission_vendor_id IN ( ' . sqlIntImplode( $arrintTransmissionVendorIds ) . ' )
						AND ( pp.property_id = p.id OR pp.property_id IS NULL )'
		          . $strWhereClause .
		          'LIMIT 1';

		return self::fetchProperty( $strSql, $objDatabase );
	}

	public static function fetchNonActivatedSimplementationPropertiesByCid( $intCid, $objDatabase ) {
		if( false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.property_type_id,
						SORT( p.occupancy_type_ids ) as occupancy_type_ids
					FROM
						properties p
						LEFT JOIN ips_portal_version_properties ipvp ON ( ipvp.property_id = p.id AND ipvp.cid = p.cid AND ipvp.status = \'' . CIpsPortalVersionProperty::STATUS_ACTIVATED . '\' )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND ipvp.id IS NULL
						AND p.is_disabled = 0
						AND property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
					ORDER BY
						LOWER ( p.property_name ) ASC,
						p.order_num';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllCommercialPropertiesByPropertyIdsByYearByCid( $arrintPropertyIds, $intYear, $intCid, $objDatabase, $boolIsIncludeTestAndDisabled = false ) {
		if( false == valArr( $arrintPropertyIds ) || false === valId( $intCid ) || false === valObj( $objDatabase, 'CDatabase' ) ) {
			return NULL;
		}

		$strSqlWhere = '';

		if( false == $boolIsIncludeTestAndDisabled ) {
			$strSqlWhere = ' AND p.is_disabled = 0 AND p.is_test = 0 ';
		}

		$strSql = 'SELECT
						p.id,
						p.property_name,
						pcp.reconciled_by
					FROM
						cam_periods cp
						JOIN property_cam_periods pcp ON ( cp.cid = pcp.cid AND cp.id = pcp.cam_period_id )
						JOIN properties p ON (p.cid = pcp.cid AND p.id = pcp.property_id)
					WHERE
						' . COccupancyType::COMMERCIAL . ' = ANY ( p.occupancy_type_ids )
						' . $strSqlWhere . '
						AND pcp.deleted_by IS NULL
						AND date_part( \'year\', cp.start_date ) = ' . ( int ) $intYear . '
						AND p.id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
						AND p.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyNameByIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		if( false === valId( $intPropertyId ) || false === valId( $intCid ) || false === valObj( $objDatabase, 'CDatabase' ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						property_name 
					FROM 
						properties 
					WHERE
						id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid;

		return parent::fetchColumn( $strSql, 'property_name', $objDatabase );
	}

	public static function fetchSimpleEnabledPropertyByPropertyIdByCompanyUserIdsByCid( $arrintCompanyUserIds, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCompanyUserIds ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}
		$strSql = 'SELECT
						p.id AS property_id,
						cup.company_user_id
					FROM
						properties p
						JOIN view_company_user_properties cup ON ( cup.cid = p.cid AND cup.property_id = p.id )
					WHERE
						p.is_disabled = 0
						AND p.id = ' . ( int ) $intPropertyId . '
						AND cup.company_user_id IN ( ' . sqlIntImplode( $arrintCompanyUserIds ) . ' )
						AND cup.cid = ' . ( int ) $intCid . '
					ORDER BY
						lower ( p.property_name ) ASC,
						order_num';

		$arrintPropertyIdCompanyUserIds = fetchData( $strSql, $objDatabase );

		$arrintUserAssignedPropertyIds = [];

		if( true == valArr( $arrintPropertyIdCompanyUserIds ) ) {
			foreach( $arrintPropertyIdCompanyUserIds as $arrmixPropertyIdCompanyUserId ) {
				$arrintUserAssignedPropertyIds[$arrmixPropertyIdCompanyUserId['property_id']][] = $arrmixPropertyIdCompanyUserId['company_user_id'];
			}
		}

		return $arrintUserAssignedPropertyIds;
	}

	public static function fetchAdvancedBudgetPropertyDetails( $intBudgetId, $intCid, $objClientDatabse ) {

		if( false == valId( $intBudgetId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
				   COUNT ( pu.id ) AS total_property_units,
				   SUM ( pu.square_feet ) AS total_square_feet
				FROM
				   budgets b
				   JOIN property_units pu ON ( b.cid = pu.cid AND b.property_id = pu.property_id )
				WHERE
				   b.cid = ' . ( int ) $intCid . '
				   AND b.id = ' . ( int ) $intBudgetId . '
				   AND b.deleted_on IS NULL
				   AND pu.deleted_on IS NULL';

		return fetchdata( $strSql, $objClientDatabse )[0];
	}

	public static function fetchActivePropertiesByPropertyIdsByTransmissionVendorIdsByPsProductIdsByCid( $arrintPropertyIds, $arrintTransmissionVendorIds, $arrintPsProductIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintTransmissionVendorIds ) || false == valArr( $arrintPsProductIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						p.id,
						p.property_name
					FROM
						properties AS p
						JOIN company_transmission_vendors AS ctv ON ( p.cid = ctv.cid )
						JOIN property_products AS pp ON ( pp.cid = p.cid AND pp.ps_product_id = ' . implode( ',', $arrintPsProductIds ) . ' )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND ctv.transmission_vendor_id IN ( ' . implode( ',', $arrintTransmissionVendorIds ) . ' )
						AND ( pp.property_id = p.id OR pp.property_id IS NULL )
						AND ctv.transmission_type_id = ' . CTransmissionType::SCREENING . '
						AND p.id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
						AND p.is_disabled <> 1';

			return fetchData( $strSql, $objDatabase );
	}

}

?>