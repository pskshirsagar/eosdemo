<?php

class CSubsidyType extends CBaseSubsidyType {

	const HUD 				= 1;
	const TAX_CREDIT		= 2;
	const HOME				= 3;

	const MIN_SUBSIDY_TYPES_FOR_LAYERED				= 2;

	public static $c_arrintAllSubsidyTypes = [
		self::HUD,
		self::TAX_CREDIT,
		self::HOME
	];

	public static $c_arrintRunTrailSubsidyTypes = [
		self::HUD,
		self::TAX_CREDIT
	];

	public static $c_arrintStudentexceptionSubsidyTypes = [
		self::HUD,
		self::HOME
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getTypeNameByTypeId( $intTypeId ) {
		$strName = NULL;
		switch( $intTypeId ) {
			case self::HUD:
				$strName = 'HUD';
				break;

			case self::TAX_CREDIT:
				$strName = 'Tax Credit';
				break;

			default:
				$strName = '';
				break;
		}
		return $strName;
	}

}
?>