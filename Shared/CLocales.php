<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CLocales
 * Do not add any new functions to this class.
 */

class CLocales extends CBaseLocales {

	/*
	 public static function fetchLocales( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CLocale::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	 }

	 public static function fetchLocale( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CLocale::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	 }
	 */

	public static function fetchPublishedNonDefaultLocales( $objDatabase ) {

		$strSql = sprintf( '
				SELECT
					*
				FROM
					locales
				WHERE
					is_published
					AND NOT is_base_locale
					AND locale_code != %s',
			pg_escape_literal( $objDatabase->getReplicaDatabase()->getHandle(), CLocale::DEFAULT_LOCALE ) );

		return parent::fetchLocales( $strSql, $objDatabase );
	}

	public function fetchLocalesByLocaleCodes( $arrstrLocaleCodes, $objDatabase ) {
		if( false == valArr( $arrstrLocaleCodes ) ) {
			return NULL;
		}
		$strSql = sprintf( '
				SELECT
					*
				FROM
					locales
				WHERE
					is_published
					AND locale_code IN (\'%s\')', implode( '\',\'', $arrstrLocaleCodes ) );

		return parent::fetchLocales( $strSql, $objDatabase );
	}

	public static function fetchAllPublishedLocales( $objDatabase ) {

		$strSql = 'SELECT
						l.locale_code,
						l.iso_language_code,
						l.iso_language_name,
						l.iso_region_name,
						l.is_base_locale,
						l.iso_region_code
					FROM
						locales AS l
					WHERE
						l.is_published IS TRUE
						AND l.iso_region_code <> \'\'
						AND l.iso_language_name <> \'\'
					ORDER BY order_num';
		return fetchData( $strSql, $objDatabase );
	}

    public static function fetchPublishedNonDefaultLocalesByVendor( $strVendorName, $objDatabase ) {

        $strSql = sprintf( '
				SELECT
					*
				FROM
					locales
				WHERE
					is_published
					AND NOT is_base_locale
					AND locale_code != %s
					AND details#>>%s = %s',
            pg_escape_literal( $objDatabase->getHandle(), CLocale::DEFAULT_LOCALE ),
            pg_escape_literal( $objDatabase->getHandle(), '{translation_vendor,name}' ),
            pg_escape_literal( $objDatabase->getHandle(), $strVendorName ) );

        return parent::fetchLocales( $strSql, $objDatabase );
    }

	public static function fetchPublishedNonBaseLocales( $objDatabase, $strLocaleCode = NULL ) {
		if( true == valStr( $strLocaleCode ) ) {
			$strCondition = ' AND l.locale_code = \'' . $strLocaleCode . '\'';
		}

		$strSql = 'SELECT DISTINCT 
							l.iso_region_name AS country_name,
							l.iso_region_code AS country_code,
							l.locale_code 
					FROM 
							locales l 
					WHERE 
							l.is_base_locale = FALSE 
							AND l.is_published = TRUE ' . $strCondition . '
							ORDER BY l.iso_region_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPublishedLocaleCount( $objDatabase ) {

		$strWhere = 'WHERE
						is_published IS TRUE
						AND iso_region_code IS NOT NULL
						AND locale_code <> \'' . CLocale::DEFAULT_LOCALE . '\'';

		return self::fetchLocaleCount( $strWhere, $objDatabase );
	}

	public static function fetchLocaleLanguageNameByLocaleCode( $strLocaleCode, $objDatabase ) {
		if( false == valStr( $strLocaleCode ) ) {
			return NULL;
		}
		$strSql = 'SELECT
						iso_language_name
					FROM
						locales
					WHERE
						is_published
						AND locale_code = \'' . $strLocaleCode . '\'';

		$arrmixData = fetchData( $strSql, $objDatabase );
		$strLanguageName = '';
		if( true == valArr( $arrmixData ) ) {
			$strLanguageName = $arrmixData[0]['iso_language_name'];
		}
		return $strLanguageName;
	}

	public static function fetchPaginatedAllPublishedLocalesBySearchCriteria( $arrmixSearchFilter, $objDatabase ) {

		$strCleanSearchString = '';
		$strWhereCondition = '';
		$strSelectClause = '';
		if( false == is_null( $arrmixSearchFilter['search_text'] ) && true == valStr( $arrmixSearchFilter['search_text'] ) ) {
			$strCleanSearchString = str_replace( ' ', '|', $arrmixSearchFilter['search_text'] );
		}

		if( false == valId( $arrmixSearchFilter['page_no'] ) && false == valId( $arrmixSearchFilter['page_size'] ) && false == valStr( $arrmixSearchFilter ) ) {
			return NULL;
		}

		$intOffset    = ( 0 < $arrmixSearchFilter['page_no'] ) ? $arrmixSearchFilter['page_size'] * ( $arrmixSearchFilter['page_no'] - 1 ) : 0;
		$intLimit     = ( int ) $arrmixSearchFilter['page_size'];

		if( valStr( $strCleanSearchString ) ) {
			$strSelectClause = ', COALESCE ( ts_rank_cd ( to_tsvector( \'english\', l.iso_language_name ), to_tsquery(\'' . $strCleanSearchString . '\') ) , 0 )  AS relevancy';
			$strWhereCondition .= ' AND ( to_tsvector ( \'english\', l.iso_language_name ) @@ to_tsquery(\'' . $strCleanSearchString . '\') OR lower( l.iso_language_name ) LIKE lower( \'%' . $strCleanSearchString . '%\' ) ) ';

		}

		$strSql = 'SELECT
						l.locale_code,
						l.iso_language_code,
						l.iso_language_name,
						array_to_json(hrr.reviewer_employee_ids) as reviewer_employee_ids,
						l.iso_region_code'
						. $strSelectClause . '
					FROM
						locales AS l
						LEFT JOIN help_resource_reviewers AS hrr ON l.locale_code = hrr.locale_code
					WHERE
						l.is_published IS TRUE
						' . $strWhereCondition . '
						AND l.iso_region_code IS NOT NULL
						ORDER BY l.iso_language_name';

		if( false == is_null( $intOffset ) && false == is_null( $intLimit ) ) {
			$strSql .= ' OFFSET  ' . ( int ) $intOffset . ' LIMIT  ' . ( int ) $intLimit;
		}
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllPublishedLocalesCountBySearchCriteria( $arrmixSearchFilter, $objDatabase ) {

		$strWhereCondition = '';
		$strCleanSearchString = '';
		if( false == is_null( $arrmixSearchFilter['search_text'] ) && true == valStr( $arrmixSearchFilter['search_text'] ) ) {
			$strCleanSearchString = str_replace( ' ', '|', $arrmixSearchFilter['search_text'] );
		}

		if( valStr( $strCleanSearchString ) ) {
			$strWhereCondition .= ' AND ( to_tsvector ( \'english\', l.iso_language_name ) @@ to_tsquery(\'' . $strCleanSearchString . '\') OR lower( l.iso_language_name ) LIKE lower( \'%' . $strCleanSearchString . '%\' ) ) ';
		}

		$strSql = 'SELECT
						COUNT(l.iso_language_name) as count
					FROM
						locales AS l
						LEFT JOIN help_resource_reviewers AS hrr ON l.locale_code = hrr.locale_code
					WHERE
						l.is_published IS TRUE
						' . $strWhereCondition . '
						AND l.iso_region_code IS NOT NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public function fetchPublishedLocaleByLocaleCode( $strLocaleCode, $objDatabase ) {
		if( false == valStr( $strLocaleCode ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						locales
					WHERE
						is_published
						AND locale_code = \'' . $strLocaleCode . '\'';

		return self::fetchCachedObject( $strSql, 'CLocale', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>
