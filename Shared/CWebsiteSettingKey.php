<?php

class CWebsiteSettingKey extends CBaseWebsiteSettingKey {

	protected $m_strToolTipKey;
	protected $m_strWebsiteSettingGroupName;

	public function __construct() {
		parent::__construct();
	}

	public function getToolTipKey() {
		return $this->m_strToolTipKey;
	}

	public function getWebsiteSettingGroupName() {
		return $this->m_strWebsiteSettingGroupName;
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['tool_tip_key'] ) ) {
			$this->setToolTipKey( $arrstrValues['tool_tip_key'] );
		}
		if( true == isset( $arrstrValues['website_setting_group_name'] ) ) {
			$this->setWebsiteSettingGroupName( $arrstrValues['website_setting_group_name'] );
		}

	}

	public function setToolTipKey( $strToolTipKey ) {
		$this->m_strToolTipKey = $strToolTipKey;
	}

	public function setWebsiteSettingGroupName( $strWebsiteSettingGroupName ) {
		$this->m_strWebsiteSettingGroupName = $strWebsiteSettingGroupName;
	}

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valWebsiteTemplateId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valToolTipId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valKey( $strAction, $objAdminDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getKey() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', 'Website Setting Key is required.' ) );
		} else {
			$objWebsiteSettingKey = \Psi\Eos\Admin\CWebsiteSettingKeys::createService()->fetchWebsiteSettingKeyByTemplateId( $this->getKey(), $this->getWebsiteTemplateId(), $objAdminDatabase );

			if( true == valObj( $objWebsiteSettingKey, 'CWebsiteSettingKey' ) ) {
				if( VALIDATE_INSERT == $strAction || ( VALIDATE_UPDATE == $strAction && ( $this->getId() != $objWebsiteSettingKey->getId() ) ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Website Setting key already exists.' ) );
					$boolIsValid = false;
				}
			}
		}

		return $boolIsValid;
	}

	public function valOldLabel() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valLabel() {
		$boolIsValid = true;
		if( true == is_null( $this->getLabel() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'label', 'Website Setting key label is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valWebsiteSettingGroupId() {
		$boolIsValid = true;

		if( true == is_null( $this->getWebsiteSettingGroupId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'website_setting_group_id', 'Website Setting Group is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valWebsiteSettingGroupId();
				$boolIsValid &= $this->valKey( $strAction, $objDatabase );
				$boolIsValid &= $this->valLabel();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function updateSequence( $objClientDatabase ) {
		$intNextSeqId = $this->getId() + 1;
		$arrmixData   = fetchData( 'SELECT setval( \'public.website_setting_keys_id_seq\', ' . ( int ) $intNextSeqId . ' , FALSE );', $objClientDatabase );

		if( 0 >= $arrmixData[0]['setval'] ) {
			return false;
		}

		return true;
	}

	public function getInsertSql( $intCurrentUserId ) {
		return $this->insert( $intCurrentUserId, NULL, true );
	}

	public function getUpdateSql( $intCurrentUserId ) {
		return $this->update( $intCurrentUserId, NULL, true );
	}

	public function getDeleteSql( $intCurrentUserId ) {
		return $this->delete( $intCurrentUserId, NULL, true );
	}

}

?>