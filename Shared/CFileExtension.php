<?php

class CFileExtension extends CBaseFileExtension {

	const IMAGE_BMP			= 12;
	const IMAGE_EPS			= 28;
	const IMAGE_GIF 		= 33;
    const IMAGE_JPEG 		= 44;
    const IMAGE_JPG 		= 46;
    const IMAGE_PNG 		= 86;
    const IMAGE_TIF			= 129;
    const APPLICATION_EXE	= 30;
    const APPLICATION_DOC	= 25;
    const APPLICATION_PDF	= 83;
    const APPLICATION_TXT	= 134;
    const APPLICATION_XLS	= 151;
    const APPLICATION_XLSX	= 165;
    const APPLICATION_DOCX	= 166;
    const APPLICATION_PPTX	= 167;
    const HTML				= 158;
    const HTM				= 159;
    const XML				= 161;
    const WWW				= 171;
    const WAV				= 139;
    const VIDEO_AVI			= 9;
    const VIDEO_FLV			= 32;
    const VIDEO_MOV			= 66;
    const AUDIO_MP3			= 69;
    const VIDEO_MP4			= 70;
    const VIDEO_MPE         = 71;
    const VIDEO_MPEG		= 72;
    const VIDEO_MPG         = 73;
    const VIDEO_WMV			= 148;
    const ZIP				= 157;
    const APPLICATION_ODT	= 168;
    const APPLICATION_ODF	= 169;
    const APPLICATION_ODS	= 173;
    const APPLICATION_ODP	= 170;
    const APPLICATION_CSV	= 172;
    const VIDEO_WEBM        = 176;
	const VIDEO_OGG			= 177;
	const VIDEO_OGV         = 178;
	const VCARD             = 179;
	const IMAGE_HEIC 		= 181;
	const IMAGE_JFIF 		= 182;
	const ASC               = 5;
	const SQL				= 180;

	public static $c_arrintLeadDocumentsDisallowedExtensionIds = [
		self::APPLICATION_XLS,
		self::APPLICATION_XLSX,
		self::APPLICATION_DOC,
		self::APPLICATION_DOCX
	];

	public static $c_arrintVideoExtensionIds = [
		self::VIDEO_AVI,
		self::VIDEO_FLV,
		self::VIDEO_MP4,
		self::VIDEO_MPE,
		self::VIDEO_MPEG,
		self::VIDEO_MPG,
		self::VIDEO_WMV,
		self::VIDEO_MOV,
		self::VIDEO_WEBM,
		self::VIDEO_OGG,
		self::VIDEO_OGV
	];

	public static $c_arrintImageExtensionIds = [
		self::IMAGE_BMP,
		self::IMAGE_GIF,
		self::IMAGE_JPEG,
		self::IMAGE_JPG,
		self::IMAGE_PNG,
		self::IMAGE_TIF,
		self::IMAGE_HEIC,
		self::IMAGE_JFIF
	];

	public static $c_arrmixImageExtensions = [
		'BMP'  => self::IMAGE_BMP,
		'GIF'  => self::IMAGE_GIF,
		'JPEG' => self::IMAGE_JPEG,
		'JPG' => self::IMAGE_JPG,
		'PNG'  => self::IMAGE_PNG,
		'TIF'  => self::IMAGE_TIF,
		'HEIC' => self::IMAGE_HEIC,
		'JFIF' => self::IMAGE_JFIF
	];

	public static $c_arrmixVideoExtensions = [
		'FLV'   => self::VIDEO_FLV,
		'MP4'   => self::VIDEO_MP4,
		'MPE'   => self::VIDEO_MPE,
		'MPEG'  => self::VIDEO_MPEG,
		'MPG'   => self::VIDEO_MPG,
		'WMV'   => self::VIDEO_WMV,
		'MOV'   => self::VIDEO_MOV,
		'WEBM'  => self::VIDEO_WEBM,
		'OGG'   => self::VIDEO_OGG,
		'OGV'   => self::VIDEO_OGV,
	];

	public static $c_arrintReleaseNoteImageExtensionIds = [
		self::IMAGE_JPEG,
		self::IMAGE_JPG,
		self::IMAGE_PNG
	];

	public static $c_arrintHTMLVideoTagSupportedExtensionIds = [
		self::VIDEO_MP4,
		self::VIDEO_OGG,
		self::VIDEO_WEBM,
		self::VIDEO_OGV
	];

	public static $c_arrintOrderFormImageExtensionIds = [
		self::APPLICATION_PDF,
		self::IMAGE_JPG,
		self::IMAGE_PNG
	];

	public static $c_arrintOrderFormImageExtensionForCustomAndTemplateLogo = [
		self::IMAGE_PNG,
		self::IMAGE_EPS,
		self::IMAGE_JPG,
	];

	public static $c_arrmixMaintenanceRequestExt = [
		'BMP'  => self::IMAGE_BMP,
		'GIF'  => self::IMAGE_GIF,
		'JPEG' => self::IMAGE_JPEG,
		'PNG'  => self::IMAGE_PNG,
		'TIF'  => self::IMAGE_TIF,
		'JPG'  => self::IMAGE_JPG,
		'TXT'  => self::APPLICATION_TXT,
		'PDF'  => self::APPLICATION_PDF,
		'DOC'  => self::APPLICATION_DOC,
		'DOCX' => self::APPLICATION_DOCX,
		'XLS'  => self::APPLICATION_XLS,
		'XLSX' => self::APPLICATION_XLSX,
		'AVI'  => self::VIDEO_AVI, // video starts from here
		'FLV'  => self::VIDEO_FLV,
		'MP4'  => self::VIDEO_MP4,
		'MPE'  => self::VIDEO_MPE,
		'MPEG' => self::VIDEO_MPEG,
		'MPG'  => self::VIDEO_MPG,
		'WMV'  => self::VIDEO_WMV,
		'MOV'  => self::VIDEO_MOV,
		'WEBM' => self::VIDEO_WEBM,
		'OGG'  => self::VIDEO_OGG,
		'OGV'  => self::VIDEO_OGV,
		'HEIC' => self::IMAGE_HEIC,
		'JFIF' => self::IMAGE_JFIF
	];

	public static $c_arrmixHrisDocumentExt = [
		self::APPLICATION_PDF		=> 'PDF'
	];

	public static $c_arrmixAllFileExtensions = [
		'ai'		=> 'application/postscript',
		'aif'		=> 'audio/x-aiff',
		'aifc'		=> 'audio/x-aiff',
		'aiff'		=> 'audio/x-aiff',
		'asc'		=> 'text/plain',
		'asf'		=> 'video/x-ms-asf',
		'asx'		=> 'video/x-ms-asx',
		'au'		=> 'audio/basic',
		'avi'		=> 'video/x-msvideo',
		'bcpio'		=> 'application/x-bcpio',
		'bin'		=> 'application/octet-stream',
		'bmp'		=> 'image/bmp',
		'cdf'		=> 'application/x-netcdf',
		'class'		=> 'application/octet-stream',
		'cpio'		=> 'application/x-cpio',
		'cpt'		=> 'application/mac-compactpro',
		'csh'		=> 'application/x-csh',
		'css'		=> 'text/css',
		'dcr'		=> 'application/x-director',
		'dir'		=> 'application/x-director',
		'djv'		=> 'image/vnd.djvu',
		'djvu'		=> 'image/vnd.djvu',
		'dll'		=> 'application/octet-stream',
		'dms'		=> 'application/octet-stream',
		'doc'		=> 'application/msword',
		'dvi'		=> 'application/x-dvi',
		'dxr'		=> 'application/x-director',
		'eps'		=> 'application/postscript',
		'etx'		=> 'text/x-setext',
		'exe'		=> 'application/octet-stream',
		'ez'		=> 'application/andrew-inset',
		'flv'		=> 'video/x-flv',
		'gif'		=> 'image/gif',
		'gtar'		=> 'application/x-gtar',
		'gz'		=> 'application/x-gzip',
		'hdf'		=> 'application/x-hdf',
		'hqx'		=> 'application/mac-binhex40',
		'ice'		=> 'x-conference/x-cooltalk',
		'ief'		=> 'image/ief',
		'iges'		=> 'model/iges',
		'igs'		=> 'model/iges',
		'jp2'		=> 'image/jp2',
		'jpe'		=> 'image/jpeg',
		'jpeg'		=> 'image/jpeg',
		'jpf'		=> 'image/jpx',
		'jpg'		=> 'image/pjpeg',
		'jpg2'		=> 'image/jp2',
		'jpgcmyk'	=> 'image/jpeg-cmyk',
		'jpgm'		=> 'image/jpgm',
		'jpm'		=> 'image/jpm',
		'jpx'		=> 'image/jpx',
		'js'		=> 'application/x-javascript',
		'kar'		=> 'audio/midi',
		'latex'		=> 'application/x-latex',
		'lha'		=> 'application/octet-stream',
		'lzh'		=> 'application/octet-stream',
		'm3u'		=> 'audio/x-mpegurl',
		'man'		=> 'application/x-troff-man',
		'me'		=> 'application/x-troff-me',
		'mesh'		=> 'model/mesh',
		'mid'		=> 'audio/midi',
		'midi'		=> 'audio/midi',
		'mif'		=> 'application/vnd.mif',
		'mj2'		=> 'video/mj2',
		'mjp2'		=> 'video/mj2',
		'mov'		=> 'video/quicktime',
		'movie'		=> 'video/x-sgi-movie',
		'mp2'		=> 'audio/mpeg',
		'mp3'		=> 'audio/mpeg',
		'mp4'		=> 'video/mp4',
		'mpe'		=> 'video/mpeg',
		'mpeg'		=> 'video/mpeg',
		'mpg'		=> 'video/mpeg',
		'mpga'		=> 'audio/mpeg',
		'ms'		=> 'application/x-troff-ms',
		'msh'		=> 'model/mesh',
		'mxu'		=> 'video/vnd.mpegurl',
		'nc'		=> 'application/x-netcdf',
		'oda'		=> 'application/oda',
		'pbm'		=> 'image/x-portable-bitmap',
		'pcd'		=> 'image/x-photo-cd',
		'pdb'		=> 'chemical/x-pdb',
		'pdf'		=> 'application/pdf',
		'pgm'		=> 'image/x-portable-graymap',
		'pgn'		=> 'application/x-chess-pgn',
		'png'		=> 'image/png',
		'pnm'		=> 'image/x-portable-anymap',
		'ppm'		=> 'image/x-portable-pixmap',
		'ppt'		=> 'application/vnd.ms-powerpoint',
		'ps'		=> 'application/postscript',
		'psd'		=> 'application/photoshop',
		'qt'		=> 'video/quicktime',
		'ra'		=> 'audio/x-realaudio',
		'ram'		=> 'audio/x-pn-realaudio',
		'ras'		=> 'image/x-cmu-raster',
		'rgb'		=> 'image/x-rgb',
		'rm'		=> 'audio/x-pn-realaudio',
		'roff'		=> 'application/x-troff',
		'rpm'		=> 'audio/x-pn-realaudio-plugin',
		'rtf'		=> 'text/rtf',
		'rtx'		=> 'text/richtext',
		'sgm'		=> 'text/sgml',
		'sgml'		=> 'text/sgml',
		'sh'		=> 'application/x-sh',
		'shar'		=> 'application/x-shar',
		'silo'		=> 'model/mesh',
		'sit'		=> 'application/x-stuffit',
		'skd'		=> 'application/x-koan',
		'skm'		=> 'application/x-koan',
		'skp'		=> 'application/x-koan',
		'skt'		=> 'application/x-koan',
		'smi'		=> 'application/smil',
		'smil'		=> 'application/smil',
		'snd'		=> 'audio/basic',
		'so'		=> 'application/octet-stream',
		'spl'		=> 'application/x-futuresplash',
		'src'		=> 'application/x-wais-source',
		'sv4cpio'	=> 'application/x-sv4cpio',
		'sv4crc'	=> 'application/x-sv4crc',
		'svg'		=> 'image/svg+xml',
		'swf'		=> 'application/x-shockwave-flash',
		't' 		=> 'application/x-troff',
		'tar'		=> 'application/x-tar',
		'tcl'		=> 'application/x-tcl',
		'tex'		=> 'application/x-tex',
		'texi'		=> 'application/x-texinfo',
		'texinfo'	=> 'application/x-texinfo',
		'tga'		=> 'image/tga',
		'tif'		=> 'image/tiff',
		'tifcmyk'	=> 'image/tiff-cmyk',
		'tiff'		=> 'image/tiff',
		'tr'		=> 'application/x-troff',
		'tsv'		=> 'text/tab-separated-values',
		'txt'		=> 'text/plain',
		'ustar'		=> 'application/x-ustar',
		'vcd'		=> 'application/x-cdlink',
		'vrml'		=> 'model/vrml',
		'vsd'		=> 'application/vnd.visio',
		'wav'		=> 'audio/x-wav',
		'wbmp'		=> 'image/vnd.wap.wbmp',
		'wbxml'		=> 'application/vnd.wap.wbxml',
		'wma'		=> 'audio/x-ms-wma',
		'wmf'		=> 'image/wmf',
		'wml'		=> 'text/vnd.wap.wml',
		'wmlc'		=> 'application/vnd.wap.wmlc',
		'wmls'		=> 'text/vnd.wap.wmlscript',
		'wmlsc'		=> 'application/vnd.wap.wmlscriptc',
		'wmv'		=> 'video/x-ms-wmv',
		'wrl'		=> 'model/vrml',
		'xbm'		=> 'image/x-xbitmap',
		'xls'		=> 'application/vnd.ms-excel',
		'xpm'		=> 'image/x-xpixmap',
		'xsl'		=> 'text/xml',
		'xwd'		=> 'image/x-xwindowdump',
		'xyz'		=> 'chemical/x-xyz',
		'z'			=> 'application/x-compress',
		'zip'		=> 'application/zip',
		'html'		=> 'text/html',
		'htm'		=> 'text/html',
		'eml'		=> 'message/rfc822',
		'xml'		=> 'text/html',
		'ico'		=> 'image/x-icon',
		'bmml'		=> 'text/xml',
		'xlsx'		=> 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
		'docx'		=> 'application/msword',
		'pptx'		=> 'application/vnd.ms-powerpoint',
		'odt'		=> 'application/vnd.oasis.opendocument.text',
		'odf'		=> 'application/vnd.oasis.opendocument.formula',
		'odp'		=> 'application/vnd.oasis.opendocument.presentation',
		'www'		=> 'video/mp4',
		'csv'		=> 'text/csv',
		'ods'		=> 'application/vnd.oasis.opendocument.spreadsheet',
		'patch'		=> 'text/plain',
		'diff'		=> 'text/plain',
		'webm'		=> 'video/webm',
		'ogg'		=> 'video/ogg',
		'ogv'		=> 'video/ogv',
		'vcf'		=> 'application/vcard',
		'sql'		=> 'text/x-sql',
		'heic'		=> 'image/heic',
		'jfif'		=> 'image/jpeg'
	];
	public static $c_arrmixPGPKeyExt = [
		'asc' => self::ASC
	];

    public function __construct() {
        parent::__construct();

         $this->m_arrintDisallowedExtensionIds = [ self::APPLICATION_EXE ];

        return;
    }

 	public function getDisallowedFileExtensionIds() {
    	return $this->m_arrintDisallowedExtensionIds;
    }

    public function valId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ));
        // }

        return $boolIsValid;
    }

    public function valExtension() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getExtension())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'extension', '' ));
        // }

        return $boolIsValid;
    }

    public function valMimeType() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getMimeType())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mime_type', '' ));
        // }

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getDescription())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', '' ));
        // }

        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getIsPublished())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', '' ));
        // }

        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getOrderNum())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', '' ));
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
                break;

            default:
            	// default case
            	$boolIsValid = true;
                break;
        }

        return $boolIsValid;
    }

}
?>