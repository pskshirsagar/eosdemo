<?php

class CPropertyAddress extends CBasePropertyAddress {

	protected $m_strRegion;
	protected $m_strArea;
	protected $m_intTransactionId;
	protected $m_intContractPropertyId;

    const CITY_DETROIT = 'Detroit';

	/**
	 * Get Functions
	 */

	public function createSemPropertyDetail() {
		$objSemPropertyDetail = new CSemPropertyDetail();

		$objSemPropertyDetail->setCid( $this->getCid() );
		$objSemPropertyDetail->setPropertyId( $this->getPropertyId() );
		$objSemPropertyDetail->setStreetLine1( $this->getStreetLine1() );
		$objSemPropertyDetail->setStreetLine2( $this->getStreetLine2() );
		$objSemPropertyDetail->setStreetLine3( $this->getStreetLine3() );
		$objSemPropertyDetail->setCity( $this->getCity() );
		$objSemPropertyDetail->setCounty( $this->getCounty() );
		$objSemPropertyDetail->setStateCode( $this->getStateCode() );
		$objSemPropertyDetail->setProvince( $this->getProvince() );
		$objSemPropertyDetail->setPostalCode( $this->getPostalCode() );
		$objSemPropertyDetail->setCountryCode( $this->getCountryCode() );
		$objSemPropertyDetail->setLongitude( $this->getLongitude() );
		$objSemPropertyDetail->setLatitude( $this->getLatitude() );

		return $objSemPropertyDetail;
	}

	public function getRegion() {
		return $this->m_strRegion;
	}

	public function getArea() {
		return $this->m_strArea;
	}

	public function getSeoCity() {
		return getSeoCityName( $this->getCity() );
	}

	public function getSeoState() {
		if( true == is_null( $this->getStateCode() ) ) {
			return;
		}

		return \Psi\CStringService::singleton()->strtolower( \Psi\CStringService::singleton()->preg_replace( [ '/[^a-zA-Z0-9\s_\-]+/', '/(\s|-|_)+/' ], [ '', '-' ], \Psi\Eos\Admin\CStates::createService()->determineStateNameByStateCode( $this->getStateCode() ) ) );
	}

	public function getTransactionId() {
		return $this->m_intTransactionId;
	}

	public function getContractPropertyId() {
		return $this->m_intContractPropertyId;
	}

	/**
	 * Set Functions
	 */

	public function setRegion( $strRegion ) {
		return $this->m_strRegion = $strRegion;
	}

	public function setArea( $strArea ) {
		return $this->m_strArea = $strArea;
	}

	public function setTransactionId( $intTransactionId ) {
		$this->m_intTransactionId = $intTransactionId;
	}

	public function setContractPropertyId( $intContractPropertyId ) {
		$this->m_intContractPropertyId = $intContractPropertyId;
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['region'] ) ) {
			$this->setRegion( $arrstrValues['region'] );
		}
		if( true == isset( $arrstrValues['area'] ) ) {
			$this->setArea( $arrstrValues['area'] );
		}

		if( true == isset( $arrstrValues['transaction_id'] ) ) {
			$this->setTransactionId( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['transaction_id'] ) : $arrstrValues['transaction_id'] );
		}
		if( true == isset( $arrstrValues['contract_property_id'] ) ) {
			$this->setContractPropertyId( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['contract_property_id'] ) : $arrstrValues['contract_property_id'] );
		}

		return;
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intId ) || ( 1 > $this->m_intId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Address id does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || ( 1 > $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Address client does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId( $boolIsValidatePropertyId = true ) {
		$boolIsValid = true;

		if( false == $boolIsValidatePropertyId ) {
			return $boolIsValid;
		}

		if( false == isset( $this->m_intPropertyId ) || ( 1 > $this->m_intPropertyId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Address property id does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAddressTypeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intAddressTypeId ) || ( 1 > $this->m_intAddressTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'address_type_id', __( 'Address type does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valTimeZoneId() {
		$boolIsValid = true;

		if( false == is_null( $this->m_intTimeZoneId ) && ( 1 > $this->m_intTimeZoneId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'time_zone_id', __( 'Time zone does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valStreetLine1( $strFieldName = '', $boolIsDuplicatePropertyAddress = false, $objDatabase = NULL ) {
		$boolIsValid = true;

		$strFieldName = ( false == empty( $strFieldName ) ) ? $strFieldName : __( 'Street line {%d, 0}', [ 1 ] );
		if( false == isset( $this->m_strStreetLine1 ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', __( '{%s, 0} is required.', [ $strFieldName ] ) ) );
		}

		if( true == $boolIsDuplicatePropertyAddress && false == $this->valCheckDuplicationByPropertyAddress( $objDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', __( 'Property address already exists.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;

		if( false == isset( $this->m_strCity ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', __( 'City is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPostalCode( $strFieldName = '' ) {
		$boolIsValid = true;

		$strFieldName = ( false == empty( $strFieldName ) ) ? $strFieldName : __( 'postal code' );

		if( false == isset( $this->m_strPostalCode ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( '{%s, 0} is required.', [ \Psi\CStringService::singleton()->ucfirst( $strFieldName ) ] ) ) );
		}

		return $boolIsValid;
	}

	public function valCheckDuplicationByPropertyAddress( $objDatabase = NULL ) {
		$strSqlCondition = '';

		if( true == isset( $this->m_intPropertyId ) || ( 1 < $this->m_intPropertyId ) ) {
			$strSqlCondition = ' AND property_id != ' . $this->m_intPropertyId;
		}

		$strSqlWhere = 'WHERE
							cid = ' . $this->getCid() . '
							AND lower( street_line1 ) = \'' . trim( \Psi\CStringService::singleton()->strtolower( addslashes( $this->getStreetLine1() ) ) . '\' ' . $strSqlCondition . ' ' );

		$intPropertyAddressCount = \Psi\Eos\Entrata\CPropertyAddresses::createService()->fetchPropertyAddressCount( $strSqlWhere, $objDatabase );

		return ( 0 < $intPropertyAddressCount ) ? false : true;

	}

	public function valReturnAddress( $objDatabase ) {

		$intPropertyPreferenceCount = \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferenceCountByKeyByValueByPropertyIdByCid( 'RETURN_ADDRESS', $this->getAddressTypeId(), $this->getPropertyId(), $this->getCid(), $objDatabase );

		if( 0 < $intPropertyPreferenceCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'address_type_id', __( 'This address type is associated as Return address under FMO statement settings' ) ) );

			return false;
		}

		return true;

	}

	public function validate( $strAction, $boolIsValidatePropertyId = true, $boolIsDuplicatePropertyAddress = false, $objDatabase = NULL ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valAddressTypeId();
				$boolIsValid &= $this->valStreetLine1();
				$boolIsValid &= $this->valCity();
				$boolIsValid &= $this->valTimeZoneId();
				break;

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valAddressTypeId();
				$boolIsValid &= $this->valStreetLine1();
				$boolIsValid &= $this->valCity();
				$boolIsValid &= $this->valTimeZoneId();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valReturnAddress( $objDatabase );
				break;

			case 'contract_property':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId( $boolIsValidatePropertyId );
				$boolIsValid &= $this->valAddressTypeId();
				$boolIsValid &= $this->valStreetLine1( 'Address line 1', $boolIsDuplicatePropertyAddress, $objDatabase );
				$boolIsValid &= $this->valCity();
				$boolIsValid &= $this->valPostalCode( 'zip code' );
				$boolIsValid &= $this->valTimeZoneId();
				break;

			case 'contract_property_update':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valAddressTypeId();
				$boolIsValid &= $this->valStreetLine1();
				$boolIsValid &= $this->valCity();
				$boolIsValid &= $this->valPostalCode( 'zip code' );
				$boolIsValid &= $this->valTimeZoneId();
				break;

			case 'property_add_wizard_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valAddressTypeId();
				$boolIsValid &= $this->valTimeZoneId();
				$boolIsValid &= $this->valStreetLine1();
				$boolIsValid &= $this->valCity();
				break;

			case 'property_add_wizard_update':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valAddressTypeId();
				$boolIsValid &= $this->valTimeZoneId();
				$boolIsValid &= $this->valStreetLine1();
				$boolIsValid &= $this->valCity();
				break;

			case 'vacany_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valAddressTypeId();
				$boolIsValid &= $this->valStreetLine1();
				$boolIsValid &= $this->valCity();
				$boolIsValid &= $this->valPostalCode();
				$boolIsValid &= $this->valTimeZoneId();
				break;

			case 'vacany_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valAddressTypeId();
				$boolIsValid &= $this->valTimeZoneId();
				break;

			case 'validate_bulk_insert_address':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPostalCode( 'zip code' );
				$boolIsValid &= $this->valAddressTypeId();
				break;

			case 'check_scanner_order_form_property_address':
				$boolIsValid &= $this->valStreetLine1( 'Property Address Line NO 1', true, $objDatabase );
				$boolIsValid &= $this->valPropertyId( $boolIsValidatePropertyId );
				$boolIsValid &= $this->valCity();
				$boolIsValid &= $this->valPostalCode();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function loadLongitudeLatitude() {

		$strUrl                            = str_replace( ' ', '+', str_replace( '#', '', $this->getStreetLine1() ) ) . '+' . str_replace( ' ', '+', $this->getCity() ) . '+' . $this->getStateCode() . '+' . $this->getPostalCode();
		$arrintPropertyLonitudeAndLatitude = $this->processFromGoogle( $strUrl );

		if( true == valArr( $arrintPropertyLonitudeAndLatitude ) && false == is_null( $arrintPropertyLonitudeAndLatitude[0] ) && false == is_null( $arrintPropertyLonitudeAndLatitude[1] ) ) {
			$this->setLongitude( $arrintPropertyLonitudeAndLatitude[0] );
			$this->setLatitude( $arrintPropertyLonitudeAndLatitude[1] );
		}
	}

	public function insertOrUpdate( $intCurrentUserId, $objClientDatabase, $objAdminDatabase = NULL ) {
		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objClientDatabase, $objAdminDatabase );
		} else {
			return $this->update( $intCurrentUserId, $objClientDatabase, $objAdminDatabase );
		}
	}

	public function insert( $intCurrentUserId, $objClientDatabase, $objAdminDatabase = NULL, $boolPerformAdminPropertyInsert = true ) {
		if( false == is_numeric( $this->getId() ) ) {
			$this->fetchNextId( $objAdminDatabase );
		}

		if( true == $boolPerformAdminPropertyInsert ) {
			$strSql = parent::insert( $intCurrentUserId, $objAdminDatabase, $boolReturnSqlOnly = true );

			if( false === $strSql ) {
				return true;
			}

			if( false == $this->executeSql( $strSql, $this, $objAdminDatabase ) ) {
				return false;
			}
		}

		if( !valObj( $objClientDatabase, 'CDatabase' ) ) {
			return true;
		}

		if( true == is_null( $objClientDatabase ) || false == valObj( $objClientDatabase, 'CDatabase' ) || CDatabaseType::CLIENT != $objClientDatabase->getDatabaseTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to load database object.' ) );

			return false;
		}

		$strSql = parent::insert( $intCurrentUserId, $objClientDatabase, $boolReturnSqlOnly = true );

		if( false == $this->executeSql( $strSql, $this, $objClientDatabase ) ) {
			return false;
		}

		return true;
	}

	public function update( $intCurrentUserId, $objClientDatabase, $objAdminDatabase = NULL ) {

		if( true == is_null( $objClientDatabase ) || false == valObj( $objClientDatabase, 'CDatabase' ) || CDatabaseType::CLIENT != $objClientDatabase->getDatabaseTypeId() || true == is_null( $objAdminDatabase ) || false == valObj( $objAdminDatabase, 'CDatabase' ) || CDatabaseType::ADMIN != $objAdminDatabase->getDatabaseTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to load database object.' ) );

			return false;
		}

		$strSql = parent::update( $intCurrentUserId, $objAdminDatabase, $boolReturnSqlOnly = true );

		if( false === $strSql ) {
			return true;
		}

		if( false == $this->executeSql( $strSql, $this, $objAdminDatabase ) ) {
			return false;
		}

		if( false == $this->executeSql( $strSql, $this, $objClientDatabase ) ) {
			return false;
		}

		return true;

	}

	public function delete( $intCurrentUserId, $objClientDatabase, $objAdminDatabase = NULL ) {

		if( true == is_null( $objClientDatabase ) || false == valObj( $objClientDatabase, 'CDatabase' ) || CDatabaseType::CLIENT != $objClientDatabase->getDatabaseTypeId() || true == is_null( $objAdminDatabase ) || false == valObj( $objAdminDatabase, 'CDatabase' ) || CDatabaseType::ADMIN != $objAdminDatabase->getDatabaseTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to load database object.' ) );

			return false;
		}

		if( false == parent::delete( $intCurrentUserId, $objAdminDatabase ) ) {
			return false;
		}

		if( false == parent::delete( $intCurrentUserId, $objClientDatabase ) ) {
			return false;
		}

		return true;
	}

	private function processFromGoogle( $strUrlRequestAddress ) {
		// Construct the request
		$boolIsValid = false;

		$strEnvironment = CConfig::get( 'environment' );
		$strEnvironment = ( true == valStr( $strEnvironment ) ) ? $strEnvironment : 'production';

		if( 'production' != $strEnvironment ) {
			return true;
		}

		$strUrlRequestAddress = str_replace( ' ', '+', $strUrlRequestAddress );

		// defaulting to an unrestricted api key that will work across all of our environments
		// this should be swapped out for a restricted key once our dev environments are migrated away from .lcl domains
		$strMapApiKey = CConfig::get( 'geo_code_api_key' );
		$strMapApiKey = ( true == valStr( $strMapApiKey ) ) ? $strMapApiKey : 'AIzaSyCmB4NXGdQTjYBvX4qgxLXjx3p-UoDaJVs';

		$strUrlRequest = 'https://maps.googleapis.com/maps/api/geocode/xml?address=' . $strUrlRequestAddress . '&sensor=true&key=' . $strMapApiKey;

		$strLatitude  = '';
		$strLongitude = '';

		$strContent = '';

		$objHandle = CFileIo::fileOpen( $strUrlRequest, 'r' );
		if( false === $objHandle ) {
			return false;
		}
		while( !feof( $objHandle ) ) {
			$strContent .= fread( $objHandle, 30000 );
		}

		fclose( $objHandle );

		$arrstrSimpleXmlContent = [];
		$arrstrSimpleXmlContent = xmlToArray( $strContent );

		if( true == isset( $arrstrSimpleXmlContent['GeocodeResponse']['result']['geometry']['location']['lat'] ) && true == isset( $arrstrSimpleXmlContent['GeocodeResponse']['result']['geometry']['location']['lng'] ) ) {
			$arrstrCoordinates = [];

			$strLongitude = $arrstrSimpleXmlContent['GeocodeResponse']['result']['geometry']['location']['lng']['value'];
			$strLatitude  = $arrstrSimpleXmlContent['GeocodeResponse']['result']['geometry']['location']['lat']['value'];

			array_push( $arrstrCoordinates, $strLongitude );
			array_push( $arrstrCoordinates, $strLatitude );

			return $arrstrCoordinates;
		}

		return $boolIsValid;
	}

	public function buildDisplayAddress( $boolMultiLine = false ) {
		$strSeparator = $boolMultiLine ? "\n" : ', ';

		$strDisplayAddress = $this->getStreetLine1() . $strSeparator;

		if( false == is_null( $this->getStreetLine2() ) ) {
			$strDisplayAddress .= $this->getStreetLine2() . $strSeparator;
		}

		if( false == is_null( $this->getStreetLine3() ) ) {
			$strDisplayAddress .= $this->getStreetLine3() . $strSeparator;
		}

		$strDisplayAddress .= $this->getCity() . ', ';

		if( true == in_array( $this->getCountryCode(), [ 'US', 'CA' ] ) || true == is_null( $this->getCountryCode() ) ) {
			$strDisplayAddress .= $this->getStateCode() . ' ';
		} elseif( false == is_null( $this->getProvince() ) ) {
			$strDisplayAddress .= $this->getProvince() . ' ';
		}

		$strDisplayAddress .= $this->getPostalCode();

		if( false == is_null( $this->getCountryCode() ) ) {
			$strDisplayAddress .= ' ' . $this->getCountryCode();
		}

		return $strDisplayAddress;
	}

}

?>
