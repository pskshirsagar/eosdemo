<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackagesProperties
 * Do not add any new functions to this class.
 */

class CScreeningPackagesProperties extends CBaseScreeningPackagesProperties {

	public static function fetchPropertyIdsAndPackageByCids( $arrintCids, $objDatabase ) {

		$strSql = 'SELECT
					    screening_package_id,
					    property_id
					FROM
					    screening_packages_properties
					WHERE
					    cid IN ( ' . implode( ',', $arrintCids ) . ' )
					    AND is_active = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyIdsByScreeningPackageIdsByCid( $arrintScreeningPackageIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintScreeningPackageIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					    spp.screening_package_id,
					    spp.property_id,
					    p.property_name
					FROM
					    screening_packages_properties spp
						JOIN properties p ON spp.property_id = p.id AND spp.cid = p.cid
					WHERE
					    spp.cid = ' . ( int ) $intCid . '
					    AND screening_package_id IN ( ' . implode( ',', $arrintScreeningPackageIds ) . ' )
					    AND is_active = 1
					ORDER BY p.property_name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackagesPropertiesByScreeningPackageIdByCid( $intScreeningPackageId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
					    screening_packages_properties
					WHERE
					    cid = ' . ( int ) $intCid . '
					    AND screening_package_id = ' . ( int ) $intScreeningPackageId . '
					    AND is_active = 1';

		return self::fetchScreeningPackagesProperties( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningPackageByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		if( true == empty( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						spp.*
					FROM
					    screening_packages_properties spp
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
					    AND cid = ' . ( int ) $intCid . '
					    AND is_active = 1';

		return self::fetchScreeningPackagesProperties( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningPackageByScreeningPackageIdsByPropertyIdByCid( $arrintScreeningPackageIds, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintScreeningPackageIds ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT 
						spp.*
					FROM
					    screening_packages_properties spp
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND screening_package_id IN ( ' . implode( ',', $arrintScreeningPackageIds ) . ' )
					    AND is_active = 1';

		return self::fetchScreeningPackagesProperties( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningPackagesByScreeningPackageIdsByLeaseTypeIdByPropertyIdByCid( $arrintScreeningPackageIds, $intLeaseTypeId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintScreeningPackageIds ) || false == valId( $intLeaseTypeId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
					    spp.*, ' .
		          ( int ) $intLeaseTypeId . ' AS lease_type_id,
						spctm.customer_type_id,
						spctm.customer_relationship_id,
					    spctm.screening_applicant_requirement_type_id
					FROM
					    screening_packages_properties spp
					    LEFT JOIN screening_package_customer_types_mappings spctm ON ( spp.screening_package_id = spctm.default_screening_package_id
					                                                                   AND spp.property_id = spctm.property_id
					                                                                   AND spp.cid = spctm.cid
					                                                                   AND spctm.lease_type_id = ' . ( int ) $intLeaseTypeId . '
					                                                                   AND spctm.is_active = 1 )
					WHERE
					    spp.cid = ' . ( int ) $intCid . '
					    AND spp.property_id = ' . ( int ) $intPropertyId . '
					    AND spp.screening_package_id IN ( ' . implode( ',', $arrintScreeningPackageIds ) . ' )
					    AND spp.is_active = 1';

		return self::fetchScreeningPackagesProperties( $strSql, $objDatabase, true );
	}

	public static function fetchActiveScreeningPackagePropertiesByScreeningPackageIdsByCid( $arrintScreeningPackageIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintScreeningPackageIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					    spp.screening_package_id,
					    spp.property_id,
					    p.property_name
				   FROM
					    screening_packages_properties spp
				   		INNER JOIN properties p ON ( p.cid = spp.cid AND p.id = spp.property_id )
				   WHERE
				       spp.cid = ' . ( int ) $intCid . '
				       AND spp.screening_package_id IN ( ' . implode( ',', $arrintScreeningPackageIds ) . ' )
				       AND spp.is_active = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningPackagePropertiesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					    spp.screening_package_id,
					    spp.property_id,
					    p.property_name,
					    spp.screening_package_name as name
					FROM
					    screening_packages_properties spp
				   		INNER JOIN properties p ON ( p.cid = spp.cid AND p.id = spp.property_id )
				   WHERE
				       spp.cid = ' . ( int ) $intCid . '
				       AND spp.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
				       AND spp.is_active = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDefaultScreeningPackagesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						spc.property_id,
						spc.default_screening_package_id
					FROM
						screening_package_customer_types_mappings spc
					WHERE
						spc.cid = ' . ( int ) $intCid . '
						AND spc.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND spc.is_active = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningPackagePropertiesByScreeningPackageIdsByCid( $arrintScreeningPackageIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT 
						* 
					FROM 
						screening_packages_properties 
					WHERE 
						is_active = 1 
						AND cid=' . ( int ) $intCid . ' 
						AND screening_package_id in (' . sqlIntImplode( $arrintScreeningPackageIds ) . ')';

		return CScreeningPackagesProperties::fetchScreeningPackagesProperties( $strSql, $objDatabase );
	}

	public static function fetchPropertyByScreeningPackageIdsByCid( $arrintScreeningPackageIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintScreeningPackageIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					    spp.screening_package_id,
					    p.id as property_id
					FROM
					    screening_packages_properties spp
						JOIN properties p ON spp.property_id = p.id AND spp.cid = p.cid
					WHERE
					    spp.cid = ' . ( int ) $intCid . '
					    AND screening_package_id IN ( ' . implode( ',', $arrintScreeningPackageIds ) . ' )
					    AND is_active = 1';

		return fetchData( $strSql, $objDatabase );
	}

}
?>