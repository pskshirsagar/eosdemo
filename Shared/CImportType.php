<?php

class CImportType extends CBaseImportType {

	const CSV						= 1;
	const E2E	 					= 2;
	const UTILITY					= 3;
	const UDS						= 4;
	const AFFORDABLE_HUD		    = 5;
	const AFFORDABLE_TAX_CREDIT		= 6;
	const LEASE_UP					= 7;
	const UTILITY_SETTING_TEMPLATE	= 8;
	const WORKBOOK					= 9;
	const DATA_CLEANUP_TOOL			= 10;
	const PROPERTY_DATA_MIGRATION_TOOL = 11;
	const OTHER						= 12;
	const SIM						= 13;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static $c_arrmixCustomImportTypes = [
		self::CSV							=> 'CSV',
		self::E2E							=> 'E2E',
		self::OTHER							=> 'OTHER',
		self::DATA_CLEANUP_TOOL				=> 'Data Cleanup Tool',
		self::UTILITY						=> 'Utility',
		self::UDS							=> 'UDS',
		self::AFFORDABLE_HUD				=> 'Affordable Hud',
		self::AFFORDABLE_TAX_CREDIT			=> 'Affordable Tax Credit',
		self::LEASE_UP						=> 'Lease Up',
		self::UTILITY_SETTING_TEMPLATE		=> 'Utility Setting Template',
		self::WORKBOOK						=> 'Workbook',
		self::PROPERTY_DATA_MIGRATION_TOOL	=> 'Property Data Migration Tool',
		self::SIM	=> 'SIM'
	];
}
?>