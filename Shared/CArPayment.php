<?php
require_once( PATH_APPLICATION_ENTRATA . '/Library/Tools/CLateFeeLibrary.class.php' );
require_once( PATH_APPLICATION_ENTRATA . '/Library/Tools/CDelinquencyInterestLibrary.class.php' );

if( false == defined( 'INTEGRATION_CALL_STATUS_SUSPEND_ALL_SEND_UPDATE' ) ) define( 'INTEGRATION_CALL_STATUS_SUSPEND_ALL_SEND_UPDATE', 1 );

use Psi\Core\Payment\AcceptedPaymentTypes\CAcceptedPaymentTypesService;
use Psi\Core\Payment\CPaymentBinFlags;
use Psi\Eos\Entrata\CCustomerAddresses;
use Psi\Eos\Entrata\CUnitAddresses;
use Psi\Eos\Entrata\CPropertyAddresses;
use Psi\Eos\Entrata\CPropertyProducts;
use Psi\Libraries\I18n\PostalAddress\Enum\CPostalAddressErrorCode;
use Psi\Libraries\UtilDates\CDate;


class CArPayment extends CBaseArPayment {

	use \TPostalAddressHelper;

	const PROCESS_METHOD_AUTHORIZE 						= 'authorize';
	const PROCESS_METHOD_AUTHCAPTURE 					= 'authcapture';
	const PROCESS_METHOD_AUTHREVERSE 					= 'authreverse';
	const PROCESS_METHOD_CAPTURE 						= 'capture';
	const PROCESS_METHOD_FINALIZE_QUEUED_CAPTURE        = 'finalize_queued_capture';
	const PROCESS_METHOD_VOID 							= 'void';
	const PROCESS_METHOD_MARK_VOIDED                    = 'mark_voided';
	const PROCESS_METHOD_POST 							= 'post';
	const PROCESS_METHOD_RETURN							= 'return';
	const PROCESS_METHOD_RETURN_REPRESENTED				= 'return_represented';
	const PROCESS_METHOD_REMOVE							= 'remove';
	const PROCESS_METHOD_INITIATE_CHARGE_BACK			= 'initiate_charge_back';
	const PROCESS_METHOD_CHARGE_BACK_FAILED				= 'charge_back_failed';
	const PROCESS_METHOD_CHARGED_BACK					= 'charged_back';
	const PROCESS_METHOD_REVERSE 						= 'reverse';
	const PROCESS_METHOD_INITIATE_REVERSAL				= 'initiate_reverse';
	const PROCESS_METHOD_CANCEL							= 'cancel';
	const PROCESS_METHOD_BATCH							= 'batch'; // used for check21 payments
	const PROCESS_METHOD_CHECK21_AMOUNT_ADJUSTMENT		= 'check21_adjustment';
	const PROCESS_METHOD_CHARGE_BACK_ADJUSTMENT			= 'charge_back_adjustment';
	const PROCESS_METHOD_CREDIT							= 'credit';
	const PROCESS_METHOD_SETTLED_REVERSAL_CANCELLATION	= 'reversal_cancellation';
	const PROCESS_METHOD_APPROVE_PAYMENT_REVERSAL		= 'approve_payment_reversal';
	const PROCESS_METHOD_MANUAL_PAYMENT					= 'manual_payment';
	const PROCESS_METHOD_MANUAL_RETURN					= 'manual_return';
	const PROCESS_METHOD_MANUAL_REVERSE					= 'manual_reverse';
	const PROCESS_METHOD_REASSOCIATE					= 'reassociate';
	const PROCESS_METHOD_HARD_EDIT						= 'hard_edit';
	const PROCESS_METHOD_EDIT_PAYMENT					= 'edit_payment';
	const PROCESS_METHOD_FINALIZE_3DSECURE				= 'finalize_3dsecure';

	const SYSTEM_USER_ID								= 1;

	const VISA_AND_MASTERCARD_CUT_OFF_HOUR				= 23;
	const VISA_AND_MASTERCARD_CUT_OFF_MINUTE			= 59;

	const DISCOVER_CUT_OFF_HOUR							= 22;
	const DISCOVER_CUT_OFF_MINUTE						= 59;

	const AMEX_CUT_OFF_HOUR								= 15;
	const AMEX_CUT_OFF_MINUTE							= 59;

	const ALLOWED_PAYMENT_REQUEST_RETRY_LIMIT			= 3;
	const PHONE_AUTHORIZATION_WAITING_TIME				= 300;
	const ALLOWED_PAYMENT_DECLIENED_ATTEMPTS			= 5;

	const NEW_RELIC_EVENT_TYPE							= 'ResidentPayment';

	const ERROR_TYPE_PAYMENT_NONE_FOUND					= -1000;
	const ERROR_TYPE_PAYMENT_BLOCK_ALL					= -1001;
	const ERROR_TYPE_PAYMENT_LEASE_BLOCK_ALL			= -1002;
	const ERROR_TYPE_PAYMENT_CUSTOMER_BLOCK_ALL			= -1003;
	const ERROR_TYPE_PAYMENT_CASH_EQUIVALENT			= -1004;
	const ERROR_TYPE_PAYMENT_PROPERTY_PAYMENT_TYPES		= -1005;
	const ERROR_TYPE_PAYMENT_MERCHANT_ACCOUNT			= -1006;
	const ERROR_TYPE_PAYMENT_MERCHANT_ACCOUNT_MISSING	= -1007;
	const ERROR_TYPE_PAYMENT_INVALID_PRODUCT			= -1008;
	const ERROR_TYPE_PAYMENT_INVALID_PLATFORM			= -1009;

	const BAD_ID_RANGE1_START = 616934568;
	const BAD_ID_RANGE1_STOP = 616988464;
	const BAD_ID_RANGE2_START = 1423736233;
	const BAD_ID_RANGE2_STOP = 1435950000;

	const MAX_ID_LINDON = 9999999999;  // 10 billion - 1
	const MIN_ID_IRELAND = 10000001000; // 10 billion
	const MAX_ID_IRELAND = 19999999999; // 20 billion - 1

	const ADDRESS_POSTAL_CODE_FIELD     = 'postalCode';

	protected $m_objArPaymentImageFront;
	protected $m_objArPaymentImageReverse;
	protected $m_objArPaymentImageFrontHash;
	protected $m_objArPaymentImageReverseHash;
	protected $m_objArPaymentMagstrip;
	protected $m_objCompanyMerchantAccount;
	protected $m_objMerchantAccount; // Cached copy of Company Merchant Account in works database
	protected $m_objProperty;
	protected $m_objLease;
	protected $m_objCustomer;
	protected $m_objClient;
	protected $m_objLeaseCustomer;
	protected $m_objArPaymentBilling;
	protected $m_objArPaymentDetail;
	protected $m_objIntermediaryAccount;
	protected $m_objSystemEmail;
	protected $m_objProcessingBankAccount;
	protected $m_objPropertyGlSetting;

	/** @var \CArTransaction $m_objPaymentArTransaction */
	protected $m_objPaymentArTransaction;
	protected $m_objPropertyChargeSetting;
	protected $m_objCompanyUser;
	protected $m_objPostalAddress;

	protected $m_boolIsShowIntegrationError;
	protected $m_boolIsPhoneCapturePending;


	protected $m_arrobjArPaymentImages;
	protected $m_arrobjArPaymentNotes;
	protected $m_arrobjPaymentTypes;
	protected $m_arrobjCheckAccountTypes;
	protected $m_arrobjArPaymentSplits;
	protected $m_arrobjAllocatedArTransactions;
	protected $m_arrobjActivePaymentArTransactions;

	protected $m_arrmixRequiredParameters;
	protected $m_arrmixArAllocations;
	protected $m_arrintArTransactionIds;

	protected $m_strLeaseStatusType;
	protected $m_strAccountName;
	protected $m_strAccountNickname;
	protected $m_strBilltoNameLastMatronymic;
	protected $m_strCheckAccountNumberBlindIndex;

	protected $m_boolSendChargeBackEmail;

	// This is used to store the confirm check account number
	protected $m_strConfirmCheckAccountNumber;

	// This is used to store the allocations that are manually created in PsAdmin
	protected $m_arrobjArAllocations;

	// This is used to store the charges that a new payment is covering in the ResidnetPortal/ProspectPortal
	protected $m_arrobjArTransactions;

	// This is used to see if we should pass fee to client instead of residents.
	protected $m_boolWaiveFee;

	protected $m_boolIsMultiLease;

	// This is used to see if the payment can be processed without logging in (we call a payment not associated to a customer record a floating payment)
	protected $m_boolIsFloating;

	// This is the variable that is used to determine whether the positive balance on a lease should be used or not.
	protected $m_boolUseForwardBalance;

	// This identifies this payment as a payment made during the online application process.
	protected $m_boolIsApplicationPayment;

	// This identifies this payment is portal payment or not PBY.
	protected $m_boolIsResidentPortalPayment;

	// This identifies this payment is entrata payment or not.
	protected $m_boolIsEntrataPayment;

	// This identifies this payment is Group payment or not.
	protected $m_boolIsGroupPayment;

	// This identifies if the payment is part of the Visa Pilot program
	protected $m_boolIsVisaPilot;

	// This allows the resident to specify they don't have a unit number so unit number doesn't have to be required to allow online payment (even though a setting specifies that the resident has to enter a unit number).
	protected $m_boolDoesntHaveUnitNumber;

	// This is used in the Check21 terminal application
	protected $m_boolDoesntHaveCheckNumber;

	// This is used to indicate on the payment step of the online application that the data from the application has been mapped.
	protected $m_boolCustomerMapped;

	// This variable is used to hold the payment amount that we are adjusting to on a check21 transaction or charge back adjustment.
	protected $m_fltAdjustingPaymentAmount;

	// This is used as the double entered payment amount confirmation.
	protected $m_fltConfirmPaymentAmount;

	// This is used to store the amount due.  If in Resident Portal, and resident tries to pay less than the amount due, there is a setting
	// that will prevent the resident from doing so during the validation.   This value is set in the Payment Controller class when creating the payment objects.
	protected $m_fltAmountDue;

	// Used to identify the type of payment (Rent, Deposit, etc).  This is set in the Payment Controller and is related to the Charge Code Merchant Accounts.
	protected $m_strPaymentDescription;

	// Variable is used to store that the user agreed to pay the convenience fee.
	protected $m_boolAgreesToPayConvenienceFees;

	// Variable is used to store that the user agreed to promise to pay.
	protected $m_boolPromiseToPay;

	// Variable is used to store that the user agreed to pay the return fee.
	protected $m_boolAgreesToPayReturnFees;

	// Used to collect data on whether resident agreed to terms and conditions.
	protected $m_boolAgreesToTerms;

	// Variable is used to store that the user agreed to reverse the payment.
	protected $m_boolAgreesToReversePayment;

	// This Order Number is used to store the index of Ar Payment Object in Resident Protal Inrerface. This is getting set when we create customer payment
	// objects from company merchant accounts that are associated to company charge codes
	protected $m_intOrderNum;
	protected $m_intBankAccountId;

	// This is a flag to identify whether to store billing indformation or not.Is used in Resident Portal
	protected $m_boolBillingInfoIsStored;

	// This is a flag to identify whether to use billing indformation or not.Is used in Resident Portal
	protected $m_boolUseStoredBillingInfo;

	// This is a flag to identify whether to update billing indformation or not.Is used in Resident Portal
	// Not sure why we need this.  If there is a customer payment id, we obviously need to apply it.
	protected $m_boolIsUpdateStoredBillingInfo;

	// This is a flag to identify whether to store billing indformation or not.Is used in Resident Portal
	protected $m_intCustomerPaymentAccountId;

	// Duplicate counter used in Bulk Capture Payments
	protected  $m_intDuplicateCounter;

	// Used to collect data on while makeing payment in entrata.
	protected $m_boolUseUnallocatedAmount;

	// Used to indentify that a batch is closing so that we can determine whether payment should be exported.  Relates to property preference DONT_EXPORT_REAL_TIME_PAYMENTS
	protected $m_boolBatchIsClosing;

	// used for nsf charge amout.
	protected $m_fltNsfChargeAmount;

	// used to override the visa phone auth requirement within entrata
	protected $m_boolOverrideVisaPhoneAuthorization;

	// We have some old transactions that we didn't validate routing numbers on a long time ago that we may need to do transaction adjustments on.
	// this variable gets set so we don't validate routing numbers on adjustments.  Someday, we can probably remove this. DJB 2009.02.10
	protected $m_boolIsCheck21Adjustment;

	//  This field helps us know that all convenience fees are the same, consequently we don't need to do Visa Phone Auth process
	protected $m_boolHasFlatConvenienceFees;

	//  This field tells us that we need to recredit convenience fees during the reversal.  Otherwise, resident gets hosed and doesn't get their convenience fees back.
	protected $m_boolRecreditConvenienceFees;

	protected $m_boolIsLeadPayment;

	// This variable stores the amount of funds already reversed on the current payment.
	protected $m_fltReversedPaymentAmount;

	// This variable stores the amount of convenience fees already reversed on the current payment.
	protected $m_fltReversedConvenienceFeeAmount;

	// This variable stores the charge back amount on the current payment.
	protected $m_intChargeBackAmount;

	// used to allow international credit cards
	protected $m_boolIsAllowInternationalCard;

	//  This field tells us that we need to waive reversal fees during the reversal. Otherwise, resident gets charged for their payment reversal.
	protected $m_boolWaiveReversalFees;

	//  This field tells whether client is reimbursed for convenience fee on their distribution account or not.
	protected $m_boolEatConvenienceFees;

	//  This field tells whether we should reverse it immediatly or not.
	protected $m_boolIsReverseImmediately;

	// This is used to get deposit number of the payment
	protected $m_intArDepositNumber;

	// Tells us this is a test transaction so we know not to allow it if the company is live.
	protected $m_boolIsTest;

	// This variable tells us the ar post month this transaction should allocate to.
	protected $m_strPostMonth;

	// Stores the date time for an transaction
	protected $m_strTransactionDatetime;

	// Store the post memo
	protected $m_strPostMemo;

	// This variable stores the allocations posted from entrata while making an electronic payment.  It gets used upon submission.
	protected $m_arrfltPostArAllocations;

	// This variable allows us to override auto allocations on electronic payments (electronic payments by default try to auto allocate which we don't want in entrata).
	protected $m_boolOverrideAutoAllocations;

	// Stores scheduled payment object related to this payment.  Should only be set if we are creating a scheduled payment (not while processing the scheduled payment).
	protected $m_objScheduledPayment;

	// Stores date difference in days with current date
	protected $m_intPaymentDateDiff;

	// Charity donation flag
	protected $m_boolApplyDonationFlag;

	// This will hold a flag, in case of split payment whether we should apply donation flag
	protected $m_boolIsMapPostDonationData;

	// Variable is used to store that the user agreed to pay the donation amount.
	protected $m_boolAgreesToPayDonationAmount;

	// Variable is used to indicate that request is from RP Premium make payment.
	protected $m_boolRpPremiumMakePayment;

	// Variable is used to indicate that resident has a rent charge due.
	protected $m_boolRentChargeDue;

	// Do not auto write off when payment gets added on past resident. Because in auto write off we are auto allocating the lease so allocations are not retaining properly.
	protected $m_boolExcludeAutoWriteOff;

	// Stores the delete post month for an offsetting transaction
	protected $m_strDeletePostMonth;

	// Stores the delete date time for an offsetting transaction
	protected $m_strDeleteDatetime;

	// Stores the delete memo
	protected $m_strDeleteMemo;

	// Variable is used to store the cvv code.
	protected $m_strCvvCode;

	// Stores the permissions for ps products
	protected $m_arrintPsProductIds;

	protected $m_arrfltTotalLeaseBalancesByLeaseId;

	protected $m_intApplicantApplicationId;
	protected $m_intApplicationId;
	protected $m_intApplicantId;
	protected $m_intRepaymentId;
	protected $m_intDataBlobTypeId;
	protected $m_intPaymentArCodeId;
	protected $m_intIsTerminal;
	protected $m_intPastPostMonth;
	protected $m_intFuturePostMonth;
	protected $m_intAccountVerificationLogId;
	protected $m_intDebitPricing;
	protected $m_intSummarisedPaymentTypeId;
	protected $m_intArTransactionId;
	protected $m_intCreatedCustomerId;
	protected $m_intUpdatedCustomerId;

	// This field will hold the ar_trgger for payment ar_transactions.
	protected $m_intArTriggerId;

	protected $m_objApplicantApplication;
	protected $m_objApplication;
	protected $m_objApplicant;

	protected $m_objAccount;

	protected $m_strSecureReferenceNumberError;

	protected $m_strAuthToken;

	// This field will hold internal_memo, this will be saved in ar_transactions.
	protected $m_strPaymentInternalMemo;

	// Creating this to help with a one-off migration script where we're fixing bugs with payment processing
	protected $m_intNachaEntryDetailRecordId;

	protected $m_strRemotePaymentNumber;
	protected $m_strSpaceNumber;
	protected $m_strUnitNumber;
	protected $m_strDeletePostDate;
	protected $m_strPropertyName;
	protected $m_strAllocateArTransactionIds;
	protected $m_objSuccessMsg;
	protected $m_boolIsDuplicate;

	protected $m_fltReturnItemFee;

	protected $m_boolValidatePastOrFuturePostMonth;
	protected $m_boolAllowAllocationSelection;
	protected $m_boolRequireManualPostMonths;
	protected $m_boolActivateStandardPosting;
	protected $m_boolAutoAllocations;
	protected $m_boolCheck21ConversionAmountValidation = false;
	protected $m_boolUseExternalDatabaseTransaction;
	protected $m_boolSendEmailPaymentReceipts;
	protected $m_boolIsPaymentCapturedBySms = false;
	protected $m_boolAutoCorrectTransactionDates;
	protected $m_boolOverridePostMonthPermission;
	protected $m_boolUseTransactionDateForAutoAllocation;

	protected $m_boolIsCreditCard;
	protected $m_boolIsGiftCard;
	protected $m_boolIsPrepaidCard;
	protected $m_boolIsInternationalCard;
	protected $m_boolIsCommercialCard;
	protected $m_boolIsCheckCard;
	protected $m_boolIsTranslate;

	protected $m_strTokenexToken;

	// the following fields are used with the Worldpay international payment gateway
	protected $m_strUserAgent;
	protected $m_strWorldPayCookie;
	protected $m_strThreeDSecureResponse;
	protected $m_strDFReferenceId;
	protected $m_boolRequires3dsFlexChallenge;
	protected $m_objCurrency;
	protected $m_boolForceWorldpayMoto;

    protected $m_boolIsQueueReprocessPayment;
    protected $m_boolShouldAttemptRetries = false;

	// The following fields are used with the SEPA international payment gateway
	protected $m_strPaymentBankAccountId;
	protected $m_intSepaFileStoredObjectId;
	protected $m_objCustomerSepaPaymentBankAccount;

	protected $m_boolSkipCommercialInternationalFeeFlow;
	protected $m_boolIsDeclinePayment;
	protected $m_strCreatedCustomerEmail;
	protected $m_strCreatedCustomerName;
	protected $m_strPaymentGroupId;

	public function __construct() {
		parent::__construct();

		$this->m_fltConvenienceFeeAmount 			= 0;
		$this->m_boolHasFlatConvenienceFees			= 0;
		$this->m_boolBillingInfoIsStored 			= 1;
		$this->m_boolBatchIsClosing 				= false;
		$this->m_boolIsTest			 				= false;
		$this->m_boolApplyDonationFlag				= false;
		$this->m_boolIsMapPostDonationData			= false;
		$this->m_boolOverrideVisaPhoneAuthorization = false;
		$this->m_boolOverrideAutoAllocations 		= false;
		$this->m_boolIsShowIntegrationError			= false;

		$this->m_boolAllowAllocationSelection		= false;
		$this->m_boolRequireManualPostMonths		= false;
		$this->m_boolActivateStandardPosting		= false;
		$this->m_boolRpPremiumMakePayment			= false;
		$this->m_boolRentChargeDue					= false;
		$this->m_boolAutoAllocations				= true;
		$this->m_boolUseExternalDatabaseTransaction = false;
		$this->m_boolIsPhoneCapturePending			= false;
		$this->m_intIsTerminal						= 0;
		$this->m_boolValidatePastOrFuturePostMonth  = false;
		$this->m_boolIsMultiLease					= false;
		$this->m_boolForceWorldpayMoto 				= false;
		$this->m_boolSkipCommercialInternationalFeeFlow = false;
		$this->m_strPaymentGroupId                  = '';
		return;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createArPaymentDetail( $intCurrentUserId, $objClientDatabase ) {

		$objArPaymentDetail = new CArPaymentDetail();
		$objArPaymentDetail->setId( $this->m_intId );
		$objArPaymentDetail->setCid( $this->m_intCid );
		$objArPaymentDetail->setArPaymentId( $this->m_intId );
		$objArPaymentDetail->setAllocationTypeId( $this->m_intAllocationTypeId );
		$objArPaymentDetail->setPaymentMemo( $this->m_strPaymentMemo );
		$objArPaymentDetail->setBilltoIpAddress( $this->m_strBilltoIpAddress );
		$objArPaymentDetail->setBilltoAccountNumber( $this->m_strBilltoAccountNumber );
		$objArPaymentDetail->setBilltoCompanyName( $this->m_strBilltoCompanyName );
		$objArPaymentDetail->setBilltoStreetLine1( $this->m_strBilltoStreetLine1 );
		$objArPaymentDetail->setBilltoStreetLine2( $this->m_strBilltoStreetLine2 );
		$objArPaymentDetail->setBilltoStreetLine3( $this->m_strBilltoStreetLine3 );
		$objArPaymentDetail->setBilltoCity( $this->m_strBilltoCity );
		$objArPaymentDetail->setBilltoStateCode( $this->m_strBilltoStateCode );
		$objArPaymentDetail->setBilltoProvince( $this->m_strBilltoProvince );
		$objArPaymentDetail->setBilltoPostalCode( $this->m_strBilltoPostalCode );
		$objArPaymentDetail->setBilltoCountryCode( $this->m_strBilltoCountryCode );
		$objArPaymentDetail->setBilltoPhoneNumber( $this->m_strBilltoPhoneNumber );
		$objArPaymentDetail->setCarLarAmount( $this->m_fltCarLarAmount );
		$objArPaymentDetail->setCarLarConfidence( $this->m_intCarLarConfidence );
		$objArPaymentDetail->setCarLarIsMoneyOrder( $this->m_intCarLarIsMoneyOrder );
		$objArPaymentDetail->setCarLarIsPerformed( $this->m_intCarLarIsPerformed );
		$objArPaymentDetail->setFeePostedOn( $this->m_strFeePostedOn );
		$objArPaymentDetail->setReturnFeePostedOn( $this->m_strReturnFeePostedOn );
		$objArPaymentDetail->setSecureReferenceNumber( $this->m_intSecureReferenceNumber );
		$objArPaymentDetail->setBilltoEmailAddress( $this->m_strBilltoEmailAddress );
		$objArPaymentDetail->setPhoneAuthRequired( $this->m_intPhoneAuthRequired );
		$objArPaymentDetail->setPhoneAuthBy( $this->m_intPhoneAuthBy );
		$objArPaymentDetail->setPhoneAuthOn( $this->m_strPhoneAuthOn );
		$objArPaymentDetail->setApprovedBy( $this->m_intApprovedBy );
		$objArPaymentDetail->setApprovedOn( $this->m_strApprovedOn );
		$objArPaymentDetail->setExportedOn( $this->m_strExportedOn );
		$objArPaymentDetail->setReturnExportedOn( $this->m_strReturnExportedOn );
		$objArPaymentDetail->setAllocateArTransactionIds( $this->getAllocateArTransactionIds() );
		$objArPaymentDetail->setAccountVerificationLogId( $this->getAccountVerificationLogId() );
		$objArPaymentDetail->setIsCreditCard( $this->m_boolIsCreditCard );
		$objArPaymentDetail->setIsDebitCard( $this->m_boolIsDebitCard );
		$objArPaymentDetail->setIsCheckCard( $this->m_boolIsCheckCard );
		$objArPaymentDetail->setIsGiftCard( $this->m_boolIsGiftCard );
		$objArPaymentDetail->setIsPrepaidCard( $this->m_boolIsPrepaidCard );
		$objArPaymentDetail->setIsInternationalCard( $this->m_boolIsInternationalCard );
		$objArPaymentDetail->setIsCommercialCard( $this->m_boolIsCommercialCard );
		$objArPaymentDetail->setRemotePaymentNumber( $this->m_strRemotePaymentNumber );

		if( false == is_null( $this->getSepaFileStoredObjectId() ) ) {
			$objArPaymentDetail->setSepaFileStoredObjectId( $this->getSepaFileStoredObjectId() );
		}

		if( false == is_null( $this->getPostalAddresses() ) ) {
			$objPostalAddressService                           = \Psi\Libraries\I18n\PostalAddress\CPostalAddressService::createService();
			$arrmixPostalAddress                               = $this->getPostalAddresses()['billto'];
			$objArPaymentBillToAddress                         = $objPostalAddressService->createPostalAddress( $arrmixPostalAddress );
			$objArPaymentDetail->setBilltoStreetLine1( $arrmixPostalAddress['addressLine1'] );
			$objArPaymentDetail->setBilltoStreetLine2( $arrmixPostalAddress['addressLine2'] );
			$objArPaymentDetail->setBilltoStreetLine3( $arrmixPostalAddress['addressLine3'] );
			$objArPaymentDetail->setBilltoCity( $arrmixPostalAddress['locality'] );
			$objArPaymentDetail->setBilltoStateCode( $arrmixPostalAddress['administrativeArea'] );
			$objArPaymentDetail->setBilltoPostalCode( $arrmixPostalAddress['postalCode'] );
			$objArPaymentDetail->setBilltoCountryCode( $arrmixPostalAddress['country'] );
		}

		if( true == valId( $this->getSecureReferenceNumber() ) ) {
			$arrmixBinFlags = CPaymentBinFlags::createService()->getAdditionalBinFlags( $this->getSecureReferenceNumber() );

			if( true == valArr( $arrmixBinFlags ) ) {
				$arrstrDetails = json_decode( json_encode( $objArPaymentDetail->getDetails() ), true );

				$arrstrDetails['bin_flags'] = $arrmixBinFlags;

				$objArPaymentDetail->setDetails( json_encode( $arrstrDetails ) );
			}

			// update bin flags in customer_payment_account
			$arrboolArPaymentBinFlags['is_debit_card']           = $this->m_boolIsDebitCard;
			$arrboolArPaymentBinFlags['is_credit_card']          = $this->m_boolIsCreditCard;
			$arrboolArPaymentBinFlags['is_check_card']           = $this->m_boolIsCheckCard;
			$arrboolArPaymentBinFlags['is_gift_card']            = $this->m_boolIsGiftCard;
			$arrboolArPaymentBinFlags['is_prepaid_card']         = $this->m_boolIsPrepaidCard;
			$arrboolArPaymentBinFlags['is_international_card']   = $this->m_boolIsInternationalCard;
			$arrboolArPaymentBinFlags['is_commercial_card']      = $this->m_boolIsCommercialCard;

			CCustomerPaymentAccount::updateCustomerPaymentAccountBinFlagsAndDetails( $this->getCustomerPaymentAccountId(), $this->getCid(), $intCurrentUserId, $objClientDatabase, $arrmixBinFlags, $arrboolArPaymentBinFlags );

		}

		if( true == valId( $this->getCreatedCustomerId() ) ) {
			$arrstrDetails = json_decode( json_encode( $objArPaymentDetail->getDetails() ), true );
			$arrstrDetails['created_customer_id'] = $this->getCreatedCustomerId();
			$objArPaymentDetail->setDetails( json_encode( $arrstrDetails ) );
		}

		if( true == \Psi\Libraries\UtilFunctions\valStr( $this->getPaymentGroupId() ) ) {
			$objArPaymentDetail->setDetailsField( 'payment_group_id', $this->getPaymentGroupId() );
		}

		return $objArPaymentDetail;
	}

	public function updateArPaymentDetail( $objPaymentDatabase ) {
		$objArPaymentDetail = $this->fetchArPaymentDetail( $objPaymentDatabase );
		if( true == is_object( $objArPaymentDetail->getDetails() ) ) {
			$arrstrDetails = json_decode( json_encode( $objArPaymentDetail->getDetails() ), true );
			$arrstrDetails['updated_customer_id'] = $this->getUpdatedCustomerId();
			$objArPaymentDetail->setDetails( json_encode( $arrstrDetails ) );
		}

		return $objArPaymentDetail;
	}

	public function createChargeBack( $boolIsRefund = false ) {

		$objChargeBack = new CChargeBack();
		$objChargeBack->setCid( $this->getCid() );
		$objChargeBack->setArPaymentId( $this->getId() );
		$objChargeBack->setProcessDatetime( date( 'm/d/Y H:i:s' ) );
		$objChargeBack->setAmount( $this->getChargeBackAmount() );
		$objChargeBack->setIsRefund( $boolIsRefund );

		return $objChargeBack;
	}

	public function createInternalTransfer( $intChargeBackId, $intDebitProcessingBankAccountId, $intCreditProcessingBankAccountId, $strPaymentMemo, $objPaymentDatabase ) {

		$intMerchantGatewayId = CInternalTransferProcesses::determineAchMerchantGatewayId( $intDebitProcessingBankAccountId, $objPaymentDatabase );

		$objInternalTransfer = new CInternalTransfer();
		$intPaymentAmount = $this->getChargeBackAmount();
		$objInternalTransfer->setPaymentAmount( $intPaymentAmount );
		$objInternalTransfer->setDebitProcessingBankAccountId( $intDebitProcessingBankAccountId );
		$objInternalTransfer->setCreditProcessingBankAccountId( $intCreditProcessingBankAccountId );
		$objInternalTransfer->setPaymentStatusTypeId( CPaymentStatusType::CAPTURING );
		$objInternalTransfer->setPaymentTypeId( $this->getPaymentTypeId() );
		$objInternalTransfer->setMerchantGatewayId( $intMerchantGatewayId );
		$objInternalTransfer->setInternalTransferTypeId( CInternalTransferType::CHARGEBACK );
		$objInternalTransfer->setRemoteReferenceNumber( $intChargeBackId );
		$objInternalTransfer->setPaymentDatetime( 'NOW()' );
		$objInternalTransfer->setEffectiveOn( date( 'm/d/Y' ) );
		$objInternalTransfer->setPaymentMemo( $strPaymentMemo );

		return $objInternalTransfer;
	}

	public function createArPaymentBilling() {

		$objArPaymentBilling = new CArPaymentBilling();
		$objArPaymentBilling->setId( $this->m_intId );
		$objArPaymentBilling->setCid( $this->m_intCid );
		$objArPaymentBilling->setArPaymentId( $this->m_intId );
		$objArPaymentBilling->setCcCardNumberEncrypted( $this->m_strCcCardNumberEncrypted );
		$objArPaymentBilling->setCheckAuxillaryOnUs( $this->m_strCheckAuxillaryOnUs );
		$objArPaymentBilling->setCheckEpc( $this->m_strCheckEpc );
		$objArPaymentBilling->setCcExpDateMonth( $this->m_strCcExpDateMonth );
		$objArPaymentBilling->setCcExpDateYear( $this->m_strCcExpDateYear );
		$objArPaymentBilling->setCcNameOnCard( $this->m_strCcNameOnCard );
		$objArPaymentBilling->setCcBinNumber( $this->m_strCcBinNumber );
		$objArPaymentBilling->setCheckDate( $this->m_strCheckDate );
		$objArPaymentBilling->setCheckPayableTo( $this->m_strCheckPayableTo );
		$objArPaymentBilling->setCheckBankName( $this->m_strCheckBankName );
		$objArPaymentBilling->setCheckNameOnAccount( $this->m_strCheckNameOnAccount );
		$objArPaymentBilling->setCheckAccountTypeId( $this->m_intCheckAccountTypeId );
		$objArPaymentBilling->setCheckRoutingNumber( $this->m_strCheckRoutingNumber );
		$objArPaymentBilling->setCheckAccountNumberEncrypted( $this->m_strCheckAccountNumberEncrypted );
		$objArPaymentBilling->setCheckIsMoneyOrder( $this->m_intCheckIsMoneyOrder );
		$objArPaymentBilling->setCheckIsConverted( $this->m_intCheckIsConverted );
		$objArPaymentBilling->setPaymentBankAccountId( $this->getPaymentBankAccountId() );
		$objArPaymentBilling->setCheckAccountNumberLastFour( \Psi\CStringService::singleton()->substr( $this->getCheckAccountNumber(), -4 ) );

		return $objArPaymentBilling;
	}

	public function createArPaymentImage( $intPaymentImageTypeId ) {

		$objArPaymentImage = new CArPaymentImage();
		$objArPaymentImage->setCid( $this->m_intCid );
		$objArPaymentImage->setArPaymentId( $this->m_intId );
		$objArPaymentImage->setFileExtensionId( CFileExtension::IMAGE_TIF );
		$objArPaymentImage->setPaymentImageTypeId( $intPaymentImageTypeId );

		$strYear 	= date( 'Y' );
		$strMonth 	= date( 'm' );
		$strDay 	= date( 'd' );

		$objArPaymentImage->setImagePath( $this->m_intCid . '/' . $strYear . '/' . $strMonth . '/' . $strDay . '/' );

		if( CPaymentImageType::FRONT == $intPaymentImageTypeId ) {
			$objArPaymentImage->setImageHash( $this->m_objArPaymentImageFrontHash );
			$this->m_objArPaymentImageFront = $objArPaymentImage;

		} elseif( CPaymentImageType::REVERSE == $intPaymentImageTypeId ) {
			$objArPaymentImage->setImageHash( $this->m_objArPaymentImageReverseHash );
			$this->m_objArPaymentImageReverse = $objArPaymentImage;
		}

		return $objArPaymentImage;
	}

	public function createArPaymentMagstrip() {

		$objArPaymentMagstrip = new CArPaymentMagstrip();
		$objArPaymentMagstrip->setCid( $this->m_intCid );
		$objArPaymentMagstrip->setArPaymentId( $this->m_intId );

		$this->m_objArPaymentMagstrip = $objArPaymentMagstrip;

		return $this->m_objArPaymentMagstrip;
	}

	public function createArPaymentDistribution( $intDistributionTypeId ) {

		$objArPaymentDistribution = new CArPaymentDistribution();
		$objArPaymentDistribution->setCid( $this->m_intCid );
		$objArPaymentDistribution->setArPaymentId( $this->m_intId );
		$objArPaymentDistribution->setPaymentDistributionTypeId( $intDistributionTypeId );
		$objArPaymentDistribution->setProcessingBankAccountId( $this->m_intProcessingBankAccountId );
		$objArPaymentDistribution->setProcessingBankId( 1 );

		switch( $intDistributionTypeId ) {
			case CPaymentDistributionType::SETTLEMENT:
			case CPaymentDistributionType::REVERSAL:
				$objArPaymentDistribution->setDistributionAmount( $this->m_fltPaymentAmount );
				break;

			case CPaymentDistributionType::REVERSAL_RETURN:
			case CPaymentDistributionType::RECALL:
				$objArPaymentDistribution->setDistributionAmount( round( ( -1 * $this->m_fltPaymentAmount ), 2 ) );
				break;

			default:
				return NULL;
				break;
		}

		return $objArPaymentDistribution;
	}

	public function createArPaymentTransaction( $objGateway = NULL ) {

		$objArPaymentTransaction = new CArPaymentTransaction();
		$objArPaymentTransaction->setCid( $this->getCid() );
		$objArPaymentTransaction->setCompanyMerchantAccountId( $this->getCompanyMerchantAccountId() );
		$objArPaymentTransaction->setArPaymentId( $this->getId() );
		$objArPaymentTransaction->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );
		$objArPaymentTransaction->setTransactionAmount( $this->m_fltTotalAmount );

		if( CPaymentType::CHECK_21 == $this->m_intPaymentTypeId || CMerchantGateway::FIRST_REGIONAL_ACH == $this->getMerchantGatewayId() || CMerchantGateway::ZIONS_ACH == $this->getMerchantGatewayId() ) {
			if( true == $this->getIsDeclinePayment() && \CPaymentStatusType::DECLINED == $this->getPaymentStatusTypeId() ) {
				$objArPaymentTransaction->setGatewayResponseCode( '2' );
				$objArPaymentTransaction->setGatewayResponseText( 'Insufficient Funds' );
			} else {
				// Since we don't go to the gateway for a check21 trans, just put -1 (kind of a hack).
				$objArPaymentTransaction->setGatewayResponseCode( -1 );
				$objArPaymentTransaction->setGatewayResponseText( 'ACH or Check21 Event' );
			}

		} elseif( false == is_null( $objGateway ) ) {

			$objArPaymentTransaction->setGatewayResponseCode( $objGateway->getGatewayResponseCode() );
			$objArPaymentTransaction->setGatewayResponseText( $objGateway->getGatewayResponseText() );
			$objArPaymentTransaction->setGatewayResponseReasonCode( $objGateway->getGatewayResponseReasonCode() );
			$objArPaymentTransaction->setGatewayResponseReasonText( $objGateway->getGatewayResponseReasonText() );
			$objArPaymentTransaction->setGatewayTransactionNumber( $objGateway->getGatewayTransactionNumber() );
			$objArPaymentTransaction->setGatewayApprovalCode( $objGateway->getGatewayApprovalCode() );
			$objArPaymentTransaction->setGatewayAvsResultCode( $objGateway->getGatewayAvsResultCode() );
			$objArPaymentTransaction->setGatewayAvsResultText( $objGateway->getGatewayAvsResultText() );
			$objArPaymentTransaction->setGatewayRawData( $objGateway->getGatewayRawData() );
		}

		return $objArPaymentTransaction;
	}

	public function createPaymentArTransaction( $objClientDatabase, $boolIsTemporary = false ) {

		$objArTransaction = new CArTransaction();
		$objArTransaction->setCid( $this->getCid() );
		$objArTransaction->setPropertyId( $this->getPropertyId() );
		$objArTransaction->setLeaseId( $this->getLeaseId() );
		$objArTransaction->setRepaymentId( $this->getRepaymentId() );

		if( true == valObj( $this->getOrFetchLease( $objClientDatabase ), 'CLease' ) ) {
			$objArTransaction->setLeaseIntervalId( $this->m_objLease->getActiveLeaseIntervalId() );
		}

		$objArTransaction->setArCodeTypeId( CArCodeType::PAYMENT );

		$objClient = $this->getOrFetchClient( $objClientDatabase );

		if( true == valId( $this->getPaymentArCodeId() ) ) {
			$objArCode = $objClient->fetchArCodeById( $this->getPaymentArCodeId(), $objClientDatabase );
		} else {
			if( true == $this->getIsGroupPayment() ) {
				$intLedgerFilterId   = reset( array_keys( ( array ) \Psi\Eos\Entrata\CLedgerFilters::createService()->fetchLedgerFiltersByDefaultLedgerFilterIdsByCid( [ \CDefaultLedgerFilter::GROUP ], $objClient->getId(), $objClientDatabase ) ) );
				$objArCode = \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByLedgerFilterIdByArCodeTypeIdByCid( $intLedgerFilterId, \CArCodeType::PAYMENT, $objClient->getId(), $objClientDatabase );
			} else {
				$objArCode = $objClient->fetchArCodeByDefaultArCodeId( \CDefaultArCode::PAYMENT, $objClientDatabase );
			}
		}

		$objArTransaction->setArOriginId( CArOrigin::BASE );
		$objArTransaction->setArTriggerId( CArTrigger::UNKNOWN );
		if( true == valObj( $objArCode, 'CArCode' ) ) {
			$objArTransaction->setArCodeId( $objArCode->getId() );
			if( false == is_null( $objArCode->getArOriginId() ) ) {
				$objArTransaction->setArOriginId( $objArCode->getArOriginId() );
			}
			if( false == is_null( $objArCode->getArTriggerId() ) ) {
				$objArTransaction->setArTriggerId( $objArCode->getArTriggerId() );
			}
		}

		if( true == valId( $this->getArTriggerId() ) ) {
			$objArTransaction->setArTriggerId( $this->getArTriggerId() );
		}
		if( true == $boolIsTemporary && \CPaymentStatusType::CAPTURED == $this->getPaymentStatusTypeId() ) {
			trigger_error( 'createPaymentArTransaction module. Ar Payment Id :' . $this->getId(), E_USER_WARNING );
		}
		$objArTransaction->setIsTemporary( $boolIsTemporary );
		$objArTransaction->setUseUnallocatedAmount( $this->getUseUnallocatedAmount() );
		$objArTransaction->setArCodeName( 'Payment Pending Approval' );
		$objArTransaction->setArPaymentId( $this->getId() );

		$strPostDate = $this->getTransactionDatetime() ?? $this->getReceiptDatetime();
		$objArTransaction->setPostDate( $strPostDate );

		if( true == $this->getAutoCorrectTransactionDates() ) {
			$strPostDate = getConvertedDateTime( $strPostDate, 'm/d/Y', date_default_timezone_get(), $this->m_objProperty->getOrfetchTimezone( $objClientDatabase )->getTimeZoneName() );

			$objArTransaction->setPostDate( $strPostDate );

			$this->m_objPropertyGlSetting   = $this->getOrFetchPropertyGlSetting( $objClientDatabase );
			$strAutoCorrectedPostMonth      = ( strtotime( date( 'm/1/Y', strtotime( $objArTransaction->getPostDate() ) ) ) > strtotime( $this->m_objPropertyGlSetting->getArPostMonth() ) ) ? date( 'm/1/Y', strtotime( $objArTransaction->getPostDate() ) ) : $this->m_objPropertyGlSetting->getArPostMonth();

			$objArTransaction->setPostMonth( $strAutoCorrectedPostMonth );
			$objArTransaction->setOverridePostMonthPermission( true );
		} else {
			$objArTransaction->setPostMonth( $this->getOrFetchPostMonth( $objClientDatabase ) );
		}

		$objArTransaction->setUseTransactionDateForAutoAllocation( $this->getUseTransactionDateForAutoAllocation() );

		$objArTransaction->setPaymentAmount( $this->getPaymentAmount() );
		$objArTransaction->setTransactionAmountDue( -1 * $this->getPaymentAmount() );
		$objArTransaction->setMemo( ( ( false == is_null( $this->getPostMemo() ) ) ? $this->getPostMemo() : $this->getPaymentMemo() ) );
		$objArTransaction->setInternalMemo( $this->getPaymentInternalMemo() );

		return $objArTransaction;
	}

	public function createArPaymentNote() {

		$objArPaymentNote = new CArPaymentNote();
		$objArPaymentNote->setCid( $this->getCid() );
		$objArPaymentNote->setArPaymentId( $this->getId() );
		$objArPaymentNote->setNoteDatetime( date( 'Y-m-d H:i:s' ) );

		return $objArPaymentNote;
	}

	public function createPhoneAuthorizationPendingSystemEmail( $objDatabase, $strMessage = NULL, $objPropertyEmailRule = NULL ) {

		$this->generateBlankSystemEmail( $objDatabase, $strMessage, $objPropertyEmailRule );
		$this->m_objSystemEmail->setSubject( 'YOUR ONLINE RENT PAYMENT IS NOT COMPLETE.' );
		$arrobjPaymentTypes 	= CPaymentTypes::fetchAllPaymentTypes( $objDatabase );

		$strCustomerFullName = CResidentProcesses::fetchCustomerFullNameWithMaternalLastName( $this->getCustomerId(), $this->getCid(), $objDatabase );
		$this->m_objSmarty->assign( 'customer_full_name', $strCustomerFullName );
		$this->m_objSmarty->assign_by_ref( 'payment_types', $arrobjPaymentTypes );
		$this->m_objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL', CONFIG_COMPANY_NAME_FULL );

		$strHtmlEmailOutput = $this->m_objSmarty->nestedFetch( PATH_INTERFACES_COMMON . 'system_emails/auto_send_phone_authorization_pending_email_warnings/auto_send_phone_authorization_pending_email_warnings.tpl' );
		$this->m_objSystemEmail->setHtmlContent( $strHtmlEmailOutput );

		return $this->m_objSystemEmail;
	}

	public function createEftCharge() {

		$objEftCharge = new CEftCharge();
		$objEftCharge->setCid( $this->getCid() );
		$objEftCharge->setArPaymentId( $this->getId() );
		$objEftCharge->setChargeDatetime( date( 'Y-m-d H:i:s' ) );
		$objEftCharge->setPostedOn( NULL );
		$objEftCharge->setTransactionId( NULL );

		return $objEftCharge;
	}

	public function createArPaymentSplit() {

		$objArPaymentSplit = new CArPaymentSplit();
		$objArPaymentSplit->setCid( $this->getCid() );
		$objArPaymentSplit->setArPaymentId( $this->getId() );
		$objArPaymentSplit->setRemotePrimaryKey( $this->getRemotePrimaryKey() );

		return $objArPaymentSplit;
	}

	public function createChargeArTransaction() {

		$objChargeArTransaction = new CArtransaction();
		$objChargeArTransaction->setCid( $this->getCid() );
		$objChargeArTransaction->setTransactionAmount( $this->getPaymentAmount() );
		$objChargeArTransaction->setPropertyId( $this->getPropertyId() );
		$objChargeArTransaction->setLeaseId( $this->getLeaseId() );
		$objChargeArTransaction->setPostDate( $this->getPaymentDatetime() );
		$objChargeArTransaction->setPostMonth( $this->getPostMonth() );

		return $objChargeArTransaction;
	}

	/**
	 * Add Functions
	 *
	 */

	public function addErrorMsgs( $arrobjErrorMsgs ) {
		if( true == valArr( $arrobjErrorMsgs ) ) {
			foreach( $arrobjErrorMsgs as $objErrorMsg ) {
				if( true == valObj( $objErrorMsg, 'CErrorMsg' ) || true == valObj( $objErrorMsg, \Psi\Libraries\UtilErrorMsg\CErrorMsg::class ) ) {
					$this->addErrorMsg( $objErrorMsg );
				}
			}
		}
	}

	public function addSecureErrorMsgs() {

		$boolIsValid = true;

		if( isset( $_REQUEST['errors'] ) && true == valArr( $_REQUEST['errors'] ) ) {
			foreach( $_REQUEST['errors'] as $strErrorMsg ) {
				if( true == isset( $strErrorMsg ) && 0 < strlen( $strErrorMsg ) ) {
					$this->addErrorMsg( new CErrorMsg( '-3', 'cc_card_number', $strErrorMsg ) );
					$boolIsValid &= false;
				}
			}
		}

		return $boolIsValid;
	}

	public function addArPaymentImage( $objArPaymentImage ) {

		$this->m_arrobjArPaymentImages[$objArPaymentImage->getId()] = $objArPaymentImage;

		if( CPaymentImageType::FRONT == $objArPaymentImage->getPaymentImageTypeId() ) {
			$this->m_objArPaymentImageFront = $objArPaymentImage;
		} elseif( CPaymentImageType::REVERSE ) {
			$this->m_objArPaymentImageReverse = $objArPaymentImage;
		}
	}

	/**
	 * Get or Fetch Functions
	 *
	 */

	public function getOrFetchArPaymentSplits( $objClientDatabase ) {

		if( true == valArr( $this->m_arrobjArPaymentSplits ) ) {
			return $this->m_arrobjArPaymentSplits;
		} else {

			$arrobjArPaymentSplits = ( array ) $this->fetchArPaymentSplits( $objClientDatabase );
			$this->setArPaymentSplits( $arrobjArPaymentSplits );

			return $this->getArPaymentSplits();
		}
	}

	public function getOrFetchArPaymentDetail( $objPaymentDatabase ) {

		if( true == valObj( $this->m_objArPaymentDetail, 'CArPaymentDetail' ) ) {
			return $this->m_objArPaymentDetail;
		} else {
			return $this->fetchArPaymentDetail( $objPaymentDatabase );
		}
	}

	public function getOrFetchMerchantAccount( $objClientDatabase ) {

		if( true == valObj( $this->m_objMerchantAccount, 'CMerchantAccount' ) ) {
			return $this->m_objMerchantAccount;
		} else {
			return $this->fetchMerchantAccount( $objClientDatabase );
		}
	}

	public function getOrFetchCompanyMerchantAccount( $objPaymentDatabase ) {

		if( true == valObj( $this->m_objCompanyMerchantAccount, 'CCompanyMerchantAccount' ) ) {
			return $this->m_objCompanyMerchantAccount;
		} else {
			return $this->fetchCompanyMerchantAccount( $objPaymentDatabase );
		}
	}

	public function getOrFetchClient( $objDatabase ) {

		if( true == valObj( $this->m_objClient, 'CClient' ) ) {
			return $this->m_objClient;
		} else {
			$this->m_objClient = $this->fetchClient( $objDatabase );
			return $this->m_objClient;
		}
	}

	public function getOrFetchProperty( $objDatabase ) {

		if( true == valObj( $this->m_objProperty, 'CProperty' ) ) {
			return $this->m_objProperty;
		} else {
			return $this->fetchProperty( $objDatabase );
		}
	}

	public function getOrFetchCustomer( $objDatabase ) {

		if( true == valObj( $this->m_objCustomer, 'CCustomer' ) ) {
			return $this->m_objCustomer;
		} else {
			$this->m_objCustomer = $this->fetchCustomer( $objDatabase );
			return $this->m_objCustomer;
		}
	}

	public function getOrFetchLease( $objDatabase ) {

		if( true == valObj( $this->m_objLease, 'CLease' ) ) {
			return $this->m_objLease;
		} else {
			$this->m_objLease = $this->fetchLease( $objDatabase );
			return $this->m_objLease;
		}
	}

	public function getOrFetchLeaseCustomer( $objDatabase ) {

		if( false == valObj( $this->m_objLeaseCustomer, 'CLeaseCustomer' ) ) {
			$this->m_objLeaseCustomer = $this->fetchLeaseCustomer( $objDatabase );
		}
		return $this->m_objLeaseCustomer;
	}

	public function getOrFetchPaymentTypes( $objClientDatabase, $boolIsOverrideCertifiedPayments = false, $strConsumer = NULL ) {
		if( NULL !== $this->m_arrobjPaymentTypes && true == valArr( $this->m_arrobjPaymentTypes ) ) {
			return $this->m_arrobjPaymentTypes;
		} else {
			return $this->fetchPaymentTypes( $objClientDatabase, $boolIsOverrideCertifiedPayments, $strConsumer );
		}
	}

	public function getOrFetchPropertyGlSetting( $objClientDatabase ) {
		if( true == valObj( $this->m_objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
			return $this->m_objPropertyGlSetting;
		} else {
			return $this->fetchPropertyGlSetting( $objClientDatabase );
		}
	}

	public function getOrFetchPostMonth( $objDatabase ) {

		if( true == is_null( $this->getPostMonth() ) ) {
			$this->m_objPropertyGlSetting = $this->getOrFetchPropertyGlSetting( $objDatabase );
			if( true == valObj( $this->m_objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
				$this->setPostMonth( $this->m_objPropertyGlSetting->getArPostMonth() );
			}
		}

		return $this->m_strPostMonth;
	}

	public function getOrFetchApplicantApplicationId( $objDatabase ) {

		if( true == is_null( $this->getApplicantApplicationId() ) ) {
			$objApplicantApplication = $this->fetchApplicantApplication( $objDatabase );
			$this->setApplicantApplicationId( ( true == valObj( $objApplicantApplication, 'CApplicantApplication' ) ) ? $objApplicantApplication->getId() : NULL );
		}

		return $this->getApplicantApplicationId();
	}

	public function getOrFetchPostMonthValidationData( $intCompanyUserId, $objDatabase ) {

		// check if user is admin or not
		$arrstrCompanyUser = ( array ) \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserDetailsByIdByCid( $intCompanyUserId, $this->getCid(), [ 'is_administrator' ], $objDatabase );
		if( true == isset( $arrstrCompanyUser[0] ) && 1 == getArrayElementByKey( 'is_administrator', $arrstrCompanyUser[0] ) ) return;

		if( false == isset( $this->m_intPastPostMonth ) && false == isset( $this->m_intFuturePostMonth ) ) {
			$this->setValidatePastOrFuturePostMonth( true );
			$this->setPastPostMonthAndFuturePostMonth( $intCompanyUserId, $objDatabase );
		}

		return [ 'past_post_month' => $this->getPastPostMonth(), 'future_post_month' => $this->getFuturePostMonth(), 'validate_post_month' => $this->getValidatePastOrFuturePostMonth() ];
	}

	public function getOrFetchCurrency( $objDatabase ) {

		if( false == valObj( $this->m_objCurrency, 'CCurrency' ) ) {

			if( CDatabaseType::PAYMENT == $objDatabase->getDatabaseTypeId() ) {
				$this->m_objCurrency = \Psi\Eos\Payment\CCurrencies::createService()->fetchCurrencyByCurrencyCode( $this->getCurrencyCode(), $objDatabase );
			} else {
				$this->m_objCurrency = \Psi\Eos\Entrata\CCurrencies::createService()->fetchCurrencyByCurrencyCode( $this->getCurrencyCode(), $objDatabase );
			}
		}
	}

	public function getApplicantApplicationId() {
		return $this->m_intApplicantApplicationId;
	}

	public function getApplicantApplication() {
		return $this->m_objApplicantApplication;
	}

	public function getFormattedBilltoPhoneNumber() {

		$strPhoneNumber = $this->getBilltoPhoneNumber();

		if( 0 < strlen( $strPhoneNumber ) ) {

			$strPhoneNumber = preg_replace( '/[^0-9]+/', '', $strPhoneNumber );

			if( 0 == strlen( $strPhoneNumber ) ) {
				return NULL;
			}

			return  \Psi\CStringService::singleton()->substr( $strPhoneNumber, 0, 3 ) . '-' . \Psi\CStringService::singleton()->substr( $strPhoneNumber, 3, 3 ) . '-' . \Psi\CStringService::singleton()->substr( $strPhoneNumber, 6, strlen( $strPhoneNumber ) - 6 );
		}

		return NULL;
	}

	public function getIsTerminal() {
		return $this->m_intIsTerminal;
	}

	public function getAllowAllocationSelection() {
		return $this->m_boolAllowAllocationSelection;
	}

	public function getActivateStandardPosting() {
		return $this->m_boolActivateStandardPosting;
	}

	public function getAccount() {
		return $this->m_objAccount;
	}

	public function getSecureReferenceNumberError() {
		return $this->m_strSecureReferenceNumberError;
	}

	public function getAuthToken() {
		return $this->m_strAuthToken;
	}

	public function getAutoAllocation() {
		return $this->m_boolAutoAllocations;
	}

	public function getPropertyChargeSetting() {
		return $this->m_objPropertyChargeSetting;
	}

	public function setTotalLeaseBalancesByLeaseId( $arrfltTotalLeaseBalancesByLeaseId ) {
		$this->m_arrfltTotalLeaseBalancesByLeaseId = $arrfltTotalLeaseBalancesByLeaseId;
	}

	public function getTotalLeaseBalancesByLeaseId() {
		return $this->m_arrfltTotalLeaseBalancesByLeaseId;
	}

	public function setIsMultiLease( $boolIsMultiLease ) {
		$this->m_boolIsMultiLease = $boolIsMultiLease;
	}

	public function getIsMultiLease() {
		return $this->m_boolIsMultiLease;
	}

	public function getPaymentInternalMemo() {
		return $this->m_strPaymentInternalMemo;
	}

	/*
	 * custom getter and setter
	 */

	public function getIsCreditCard() {
		return $this->m_boolIsCreditCard;
	}

	public function setIsCreditCard( $boolIsCreditCard ) {
		$this->m_boolIsCreditCard = $boolIsCreditCard;
	}

	public function getIsGiftCard() {
		return $this->m_boolIsGiftCard;
	}

	public function setIsGiftCard( $boolIsGiftCard ) {
		$this->m_boolIsGiftCard = $boolIsGiftCard;
	}

	public function getIsPrepaidCard() {
		return $this->m_boolIsPrepaidCard;
	}

	public function setIsPrepaidCard( $boolIsPrepaidCard ) {
		$this->m_boolIsPrepaidCard = $boolIsPrepaidCard;
	}

	public function getIsInternationalCard() {
		return $this->m_boolIsInternationalCard;
	}

	public function setIsInternationalCard( $boolIsInternationalCard ) {
		$this->m_boolIsInternationalCard = $boolIsInternationalCard;
	}

	public function getIsCommercialCard() {
		return $this->m_boolIsCommercialCard;
	}

	public function setIsCommercialCard( $boolIsCommercialCard ) {
		$this->m_boolIsCommercialCard = $boolIsCommercialCard;
	}

	public function getIsCheckCard() {
		return $this->m_boolIsCheckCard;
	}

	public function setIsCheckCard( $boolIsCheckCard ) {
		$this->m_boolIsCheckCard = $boolIsCheckCard;
	}

	public function getArTriggerId() {
		return $this->m_intArTriggerId;
	}

	public function setArTriggerId( $intArTriggerId ) {
		$this->m_intArTriggerId = $intArTriggerId;
	}

	public function getRemotePaymentNumber() {
		return $this->m_strRemotePaymentNumber;
	}

	public function setRemotePaymentNumber( $strRemotePaymentNumber ) {
		$this->m_strRemotePaymentNumber = CStrings::strTrimDef( $strRemotePaymentNumber, 64, NULL, true );
	}

	public function getTokenexToken() {
		return $this->m_strTokenexToken;
	}

	public function setTokenexToken( $strTokenexToken ) {
		$this->m_strTokenexToken = $strTokenexToken;
	}

	public function getUserAgent() {
		return $this->m_strUserAgent;
	}

	public function setUserAgent( $strUserAgent ) {
		$this->m_strUserAgent = $strUserAgent;
	}

	public function getWorldPayCookie() {
		return $this->m_strWorldPayCookie;
	}

	public function setWorldPayCookie( $strWorldPayCookie ) {
		$this->m_strWorldPayCookie = $strWorldPayCookie;
	}

	public function get3dSecureResponse() {
		return $this->m_strThreeDSecureResponse;
	}

	public function set3dSecureResponse( $strThreeDSecureResponse ) {
		$this->m_strThreeDSecureResponse = $strThreeDSecureResponse;
	}

	public function setDFReferenceId( $strDFReferenceId ) {
		$this->m_strDFReferenceId = $strDFReferenceId;
	}

	public function getDFReferenceId() {
		return $this->m_strDFReferenceId;
	}

	public function setRequires3dsFlexChallenge( $boolRequires3dsFlexChallenge ) {
		$this->m_boolRequires3dsFlexChallenge = $boolRequires3dsFlexChallenge;
	}

	public function requires3dsFlexChallenge() {
		return $this->m_boolRequires3dsFlexChallenge;
	}

	public function getCurrency() : CCurrency {
		return $this->m_objCurrency;
	}

	public function setCurrency( $objCurrency ) {
		$this->m_objCurrency = $objCurrency;
	}

	public function getForceWorldpayMoto() {
		return $this->m_boolForceWorldpayMoto;
	}

	public function setForceWorldpayMoto( bool $boolForceWorldpayMoto ) {
		$this->m_boolForceWorldpayMoto = $boolForceWorldpayMoto;
	}

	public function getPaymentBankAccountId() {
		return $this->m_strPaymentBankAccountId;
	}

	public function setPaymentBankAccountId( $strPaymentBankAccountId ) {
		$this->m_strPaymentBankAccountId = $strPaymentBankAccountId;
	}

	public function getPostalAddresses() {
		return $this->m_objPostalAddress;
	}

	public function setPostalAddresses( $objPostalAddress ) {
		$this->m_objPostalAddress = $objPostalAddress;
	}

	public function setCustomerSepaPaymentBankAccount( $objCustomerSepaPaymentBankAccount ) {
		$this->m_objCustomerSepaPaymentBankAccount = $objCustomerSepaPaymentBankAccount;
	}

	public function getCustomerSepaPaymentBankAccount() {
		return $this->m_objCustomerSepaPaymentBankAccount;
	}

	public function getSepaFileStoredObjectId() {
		return $this->m_intSepaFileStoredObjectId;
	}

	public function setSepaFileStoredObjectId( $intSepaFileStoredObjectId ) {
		$this->m_intSepaFileStoredObjectId = $intSepaFileStoredObjectId;
	}

	public function getCreatedCustomerId() {
		return $this->m_intCreatedCustomerId;
	}

	public function getUpdatedCustomerId() {
		return $this->m_intUpdatedCustomerId;
	}

	public function setCreatedCustomerId( $intCreatedCustomerId ) {
		$this->m_intCreatedCustomerId = $intCreatedCustomerId;
	}

	public function setUpdatedCustomerId( $intUpdatedCustomerId ) {
		$this->m_intUpdatedCustomerId = $intUpdatedCustomerId;
	}

	public function getCreatedCustomerEmail() {
		return $this->m_strCreatedCustomerEmail;
	}

	public function setCreatedCustomerEmail( $strCreatedCustomerEmail ) {
		$this->m_strCreatedCustomerEmail = $strCreatedCustomerEmail;
	}

	public function getCreatedCustomerName() {
		return $this->m_strCreatedCustomerName;
	}

	public function setCreatedCustomerName( $strCreatedCustomerName ) {
		$this->m_strCreatedCustomerName = $strCreatedCustomerName;
	}

	public function getFormattedPaymentAmount() {
		CLocaleContainer::createService()->setCurrencyCode( $this->getCurrencyCode() );
		return __( '{%m,0}', [ $this->getPaymentAmount() ] );
	}

	public function getFormattedConvenienceFeeAmount() {
		CLocaleContainer::createService()->setCurrencyCode( $this->getCurrencyCode() );
		return __( '{%m,0}', [ $this->getConvenienceFeeAmount() ] );
	}

	public function getFormattedDonationAmount() {
		CLocaleContainer::createService()->setCurrencyCode( $this->getCurrencyCode() );
		return __( '{%m,0}', [ $this->getDonationAmount() ] );
	}

	public function getFormattedTotalAmount() {
		CLocaleContainer::createService()->setCurrencyCode( $this->getCurrencyCode() );
		return __( '{%m,0}', [ $this->getTotalAmount() ] );
	}

	public function getFormattedPaymentCost() {
		CLocaleContainer::createService()->setCurrencyCode( $this->getCurrencyCode() );
		return __( '{%m,0}', [ $this->getPaymentCost() ] );
	}

	public function getFormattedCompanyChargeAmount() {
		CLocaleContainer::createService()->setCurrencyCode( $this->getCurrencyCode() );
		return __( '{%m,0}', [ $this->getCompanyChargeAmount() ] );
	}

	public function getSkipCommercialInternationalFeeFlow() {
		return $this->m_boolSkipCommercialInternationalFeeFlow;
	}

	public function setSkipCommercialInternationalFeeFlow( bool $boolSkipCommercialInternationalFeeFlow ) {
		$this->m_boolSkipCommercialInternationalFeeFlow = $boolSkipCommercialInternationalFeeFlow;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchArDeposits( $objDatabase ) {
		return \Psi\Eos\Entrata\CArDeposits::createService()->fetchUnreversedArDepositsByArPaymentIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchApplicantApplication( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationByArPaymentIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchReturnType( $objClientDatabase ) {
		return CReturnTypes::fetchReturnTypeById( $this->getReturnTypeId(), $objClientDatabase );
	}

	public function fetchReturnTypeOption( $objClientDatabase ) {
		return CReturnTypeOptions::fetchReturnTypeOptionByReturnTypeIdByCid( $this->getReturnTypeId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchArPaymentTransmission( $objDatabase ) {
		return CArPaymentTransmissions::fetchArPaymentTransmissionByIdByCid( $this->m_intArPaymentTransmissionId, $this->m_intCid, $objDatabase );
	}

	public function fetchArPayment( $objPaymentDatabase ) {
		return CArPayments::fetchArPaymentByIdByCid( $this->getArPaymentId(), $this->getCid(), $objPaymentDatabase );
	}

	public function fetchArPaymentBilling( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CArPaymentBillings::createService()->fetchArPaymentBillingByCidByArPaymentId( $this->m_intCid, $this->m_intId, $objPaymentDatabase );
	}

	public function fetchAssociatedReversingArPayments( $objPaymentDatabase ) {

		return CArPayments::fetchAssociatedReversingArPaymentsByArPaymentIdByCid( $this->getId(), $this->getCid(), $objPaymentDatabase );
	}

	public function fetchReversedPaymentAmount( $objDatabase ) {

		$this->m_fltReversedPaymentAmount = CArPayments::fetchReversedPaymentAmountByOriginalArPaymentIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		return $this->m_fltReversedPaymentAmount;
	}

	public function fetchReversedConvenienceFeeAmount( $objPaymentDatabase ) {

		$this->m_fltReversedConvenienceFeeAmount = CArPayments::fetchReversedConvenienceFeeAmountByOriginalArPaymentIdByCid( $this->getId(), $this->getCid(), $objPaymentDatabase );

		return $this->m_fltReversedConvenienceFeeAmount;
	}

	public function fetchAllocationType( $objDatabase ) {
		return CAllocationTypes::fetchAllocationTypeById( $this->m_intAllocationTypeId, $objDatabase );
	}

	public function fetchArPaymentDetail( $objPaymentDatabase ) {
		$this->m_objArPaymentDetail = \Psi\Eos\Payment\CArPaymentDetails::createService()->fetchArPaymentDetailById( $this->m_intId, $objPaymentDatabase );
		return $this->m_objArPaymentDetail;
	}

	public function fetchRelatedArPayments( $objPaymentDatabase ) {
		return CArPayments::fetchRelatedArPaymentsByIdByArPaymentIdByCid( $this->m_intId, $this->m_intArPaymentId, $this->getPropertyId(), $this->getCid(), $objPaymentDatabase );
	}

	public function fetchNachaEntryDetailRecords( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CNachaEntryDetailRecords::createService()->fetchNachaEntryDetailRecordsByArPaymentId( $this->getId(), $objPaymentDatabase );
	}

	public function fetchArPaymentNotes( $objPaymentDatabase ) {
		$this->m_arrobjArPaymentNotes = \Psi\Eos\Payment\CArPaymentNotes::createService()->fetchArPaymentNotesByArPaymentId( $this->getId(), $objPaymentDatabase );
		return $this->m_arrobjArPaymentNotes;
	}

	public function fetchPaymentTypes( $objClientDatabase, $boolIsOverrideCertifiedPayments = false, $strConsumer = NULL ) {

		$this->m_arrobjPaymentTypes = CPaymentController::fetchAcceptedPaymentTypes( $this, $objClientDatabase, $boolIsOverrideCertifiedPayments, $strConsumer );
		$this->m_arrobjPaymentTypes = CAcceptedPaymentTypesService::createService()->fetchAcceptedPaymentTypesComparison( $this->m_arrobjPaymentTypes, CPaymentController::getPaymentTypeAvailabilityMessages(), $this, $objClientDatabase, $boolIsOverrideCertifiedPayments, $strConsumer );

		return $this->m_arrobjPaymentTypes;
	}

	public function fetchCheckAccountTypes( $objDatabase ) {
		$this->m_arrobjCheckAccountTypes = \Psi\Eos\Entrata\CCheckAccountTypes::createService()->fetchStandardCheckAccountTypes( $objDatabase );
		return $this->m_arrobjCheckAccountTypes;
	}

	public function fetchPropertyGlSetting( $objDatabase ) {
		return CPropertyGlSettings::fetchCustomPropertyGlSettingsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMerchantAccount( $objPaymentDatabase ) {

		if( false == valId( $this->m_intCompanyMerchantAccountId ) ) return NULL;
		return CCompanyMerchantAccounts::fetchCompanyMerchantAccountByCompanyMerchantAccountIdByCidByPaymentMediumId( $this->m_intCompanyMerchantAccountId, $this->m_intCid, $this->m_intPaymentMediumId, $objPaymentDatabase );
	}

	public function fetchMerchantAccount( $objClientDatabase ) {

		if( false == is_int( $this->m_intCompanyMerchantAccountId ) ) return NULL;
		return CMerchantAccounts::fetchMerchantAccountByMerchantAccountIdByPaymentMediumIdByCid( $this->m_intCompanyMerchantAccountId, $this->m_intPaymentMediumId, $this->m_intCid, $objClientDatabase );
	}

	public function fetchTerminalMerchantAccount( $objClientDatabase ) {

		if( false == is_int( $this->m_intCompanyMerchantAccountId ) ) return NULL;

		return CMerchantAccounts::fetchMerchantAccountByMerchantAccountIdByPaymentMediumIdByCid( $this->m_intCompanyMerchantAccountId, CPaymentMedium::TERMINAL, $this->m_intCid, $objClientDatabase );
	}

	public function fetchDefaultMerchantAccount( $objClientDatabase, $boolSetCompanyMerchantAccountId = false ) {

		if( false == is_int( $this->m_intCid ) ) return NULL;
		if( false == is_int( $this->m_intPropertyId ) ) return NULL;

		$this->m_objMerchantAccount	= CMerchantAccounts::fetchDefaultMerchantAccountByCidByPropertyId( $this->getCid(), $this->getPropertyId(), $objClientDatabase, $this->m_intPaymentMediumId );

		if( true == valObj( $this->m_objMerchantAccount, 'CMerchantAccount' ) && true == $boolSetCompanyMerchantAccountId ) {
			$this->setCompanyMerchantAccountId( $this->m_objMerchantAccount->getId() );
		}

		return $this->m_objMerchantAccount;
	}

	public function fetchClient( $objDatabase ) {
		if( false == is_int( $this->m_intCid ) ) return NULL;
		return CClients::fetchClientById( $this->m_intCid, $objDatabase );
	}

	public function fetchProperty( $objDatabase ) {

		if( false == is_int( $this->m_intCid ) ) return NULL;
		if( false == is_int( $this->m_intPropertyId ) ) return NULL;

		$this->m_objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->m_intPropertyId, $this->m_intCid, $objDatabase );

		return $this->m_objProperty;
	}

	public function fetchCustomer( $objDatabase ) {
		if( false == is_int( $this->m_intCid ) ) return NULL;
		if( false == is_int( $this->m_intCustomerId ) ) return NULL;
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $this->m_intCustomerId, $this->m_intCid, $objDatabase );
	}

	public function fetchViewCustomer( $objDatabase ) {
		if( false == is_int( $this->m_intCid ) ) return NULL;
		if( false == is_int( $this->m_intCustomerId ) ) return NULL;
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $this->getCustomerId(), $this->getCid(), $objDatabase );
	}

	public function fetchLease( $objDatabase ) {
		if( false == is_int( $this->m_intCid ) ) return NULL;
		if( false == is_int( $this->m_intLeaseId ) ) return NULL;
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCidBySchema( $this->m_intLeaseId, $this->m_intCid, $objDatabase );
	}

	public function fetchLeaseWithLeaseStatusTypeId( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseWithLeaseStatusTypeIdByIdByCustomerIdByCid( $this->m_intLeaseId, $this->m_intCustomerId, $this->m_intCid, $objDatabase );
	}

	public function fetchArPaymentImages( $objPaymentDatabase ) {
		return CArPaymentImages::fetchArPaymentImagesByArPaymentId( $this->m_intId, $objPaymentDatabase );
	}

	public function fetchArPaymentMagstrip( $objPaymentDatabase ) {
		return  CArPaymentMagstrips::fetchArPaymentMagstripByArPaymentId( $this->m_intId, $objPaymentDatabase );
	}

	// Seems like this might be poorly programmed.

	public function fetchNextCheck21ArPayment( $objPaymentDatabase ) {
		return  CArPayments::fetchNextCheck21ArPaymentByCid( $this->getCid(), $objPaymentDatabase );
	}

	// This function determines whether we should be authorizing or capturing the payment during the first run.

	public function fetchProcessMethod( $objDatabase, $boolOverrideVisaPhoneAuthorization = false ) {

		$this->m_boolOverrideVisaPhoneAuthorization = $boolOverrideVisaPhoneAuthorization;

		$boolAuthorizeOnly = false;

		$objPaymentsAuthOnlyPropertyPreference = \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'PAYMENTS_AUTH_ONLY', $this->m_intPropertyId, $this->getCid(), $objDatabase );
		$objManualCaptureDaysPropertyPreference = \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'MANUAL_CAPTURE_DAYS', $this->m_intPropertyId, $this->getCid(), $objDatabase );
		$arrobjMigratedPropertyGLSettings = CPropertyGlSettings::fetchMigratedPropertyGlSettingsByPropertyIdsByCid( [ $this->m_intPropertyId ], $this->getCid(), $objDatabase );

		if( true == valObj( $objPaymentsAuthOnlyPropertyPreference, 'CPropertyPreference' ) ) {
			$boolAuthorizeOnly = $objPaymentsAuthOnlyPropertyPreference->getValue();
		}

		if( true == valArr( $arrobjMigratedPropertyGLSettings ) ) {
			$boolAuthorizeOnly = true;
		}

		// Find out if today is in the group of days with a manual capture designation.  If not, capture.
		if( true == valObj( $objManualCaptureDaysPropertyPreference, 'CPropertyPreference' ) ) {
			$arrintManualCaptureDaysTemp = explode( ',', $objManualCaptureDaysPropertyPreference->getValue() );
			$arrintManualCaptureDays = [];

			if( true == valArr( $arrintManualCaptureDaysTemp ) ) {
				foreach( $arrintManualCaptureDaysTemp as $intManualCaptureDay ) {
					$intManualCaptureDay = ( int ) trim( $intManualCaptureDay );
					if( 0 != ( int ) $intManualCaptureDay && 31 >= $intManualCaptureDay ) {
						$arrintManualCaptureDays[] = $intManualCaptureDay;
					}
				}
			}

			if( true == in_array( ( int ) date( 'j' ), $arrintManualCaptureDays ) ) {
				$boolAuthorizeOnly = true;
			}
		}

		switch( $this->m_intPaymentTypeId ) {
			case CPaymentType::CHECK_21:
			case CPaymentType::ACH:
			case CPaymentType::PAD:
			case CPaymentType::SEPA_DIRECT_DEBIT:
				$strProcessMethod = ( false == $this->getIsPhoneCapturePending() && false == $boolAuthorizeOnly ) ? self::PROCESS_METHOD_AUTHCAPTURE : self::PROCESS_METHOD_AUTHORIZE;
				break;

			case CPaymentType::VISA:
				$boolIsVisa = true;
			case CPaymentType::DISCOVER:
			case CPaymentType::AMEX:
			case CPaymentType::MASTERCARD:

				$boolOverridePhoneAuthorization = $boolOverrideVisaPhoneAuthorization;
				$boolPhoneAuthConvenienceFees = false;
				$boolPhoneAuthDebit = false;

				if( false == $this->getHasFlatConvenienceFees() && 0 < ( float ) $this->getConvenienceFeeAmount() ) {
					$boolPhoneAuthConvenienceFees = true;
				}

				if( true == valObj( $this->m_objMerchantAccount, 'CMerchantAccount' ) && false == $this->m_objMerchantAccount->getEnableSeparateDebitPricing() && true == $boolIsVisa ) {
					$boolPhoneAuthDebit = true;
				} elseif( true == valObj( $this->m_objMerchantAccount, 'CMerchantAccount' ) && true == $this->m_objMerchantAccount->getEnableSeparateDebitPricing() && false == $this->m_objMerchantAccount->getEnableVisaPilot() && false == ( bool ) $this->m_boolIsDebitCard ) {
					$boolPhoneAuthDebit = true;
					if( false == is_null( $this->getScheduledPaymentId() ) && false == $boolIsVisa ) {
						$boolPhoneAuthDebit = false;
					}
				}

				if( ( true == $this->getIsPhoneCapturePending() ) || ( false == $boolOverridePhoneAuthorization && true == $boolPhoneAuthConvenienceFees && true == $boolPhoneAuthDebit ) ) {
					$strProcessMethod = self::PROCESS_METHOD_AUTHORIZE;
				} else {
					$strProcessMethod = ( false == $boolAuthorizeOnly ) ? self::PROCESS_METHOD_AUTHCAPTURE : self::PROCESS_METHOD_AUTHORIZE;
				}
				break;

			case CPaymentType::CHECK:
			case CPaymentType::MONEY_ORDER:
			case CPaymentType::CASH:
			case CPaymentType::HAP:
			case CPaymentType::BAH:
				$strProcessMethod = self::PROCESS_METHOD_MANUAL_PAYMENT;
				break;

			default:
				$strProcessMethod = self::PROCESS_METHOD_POST;
				break;
		}

		return $strProcessMethod;
	}

	public function fetchLeaseCustomer( $objDatabase ) {
		if( false == is_int( $this->m_intCustomerId ) ) return NULL;
		if( false == is_int( $this->m_intLeaseId ) ) return NULL;
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchCustomLeaseCustomerByCustomerIdByLeaseIdByCid( $this->m_intCustomerId, $this->m_intLeaseId, $this->m_intCid, $objDatabase );
	}

	public function fetchPaymentArTransaction( $objDatabase, $boolIncludeDeleted = true ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionByLeaseIdByArPaymentIdByCid( $this->m_intLeaseId, $this->m_intId, $this->m_intCid, $objDatabase, $boolIncludeDeleted );
	}

	public function fetchAllPaymentArTransactions( $objDatabase, $intLeaseId = NULL ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchAllPaymentArTransactionsByArPaymentIdByCid( $this->m_intId, $this->m_intCid, $objDatabase, $intLeaseId );
	}

	public function fetchAllArTransactionsDetails( $objDatabase ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchAllArTransactionsDetailsByArPaymentIdByCid( $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchUnreversedPaymentArTransactions( $objDatabase, $boolExcludeRepaymentTransactions = false ) {

		$arrintArPaymentSplitPropertyIds	= \Psi\Eos\Entrata\CArPaymentSplits::createService()->fetchSimpleAssociatedPropertyIdsByArPaymentIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		$arrintPropertyIds 					= ( true == valArr( $arrintArPaymentSplitPropertyIds ) ) ? $arrintArPaymentSplitPropertyIds : [ $this->m_intPropertyId ];

		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchUnreversedArTransactionsByArPaymentIdByPropertyIdsByCid( $this->m_intId, $arrintPropertyIds, $this->m_intCid, $objDatabase, $boolIsCheckTransactionAmount = false, $boolExcludeRepaymentTransactions );
	}

	public function fetchArPaymentDistributions( $objPaymentDatabase ) {
		return CArPaymentDistributions::fetchArPaymentDistributionsByArPaymentId( $this->m_intId, $objPaymentDatabase );
	}

	public function fetchUnexportedSettlementDistributionByDistributionTypeIdByDistributionStatusTypeId( $intDistributionTypeId, $intDistributionStatusTypeId, $objPaymentDatabase ) {
		return CSettlementDistributions::fetchUnexportedSettlementDistributionByDistributionTypeIdByDistributionStatusTypeIdByArPaymentId( $intDistributionTypeId, $intDistributionStatusTypeId, $this->m_intId, $objPaymentDatabase );
	}

	public function fetchSettlementDistributions( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CSettlementDistributions::createService()->fetchSettlementDistributionsByArPaymentIdByCid( $this->m_intId, $this->m_intCid, $objPaymentDatabase );
	}

	public function fetchSuccessfulSettlementDistributions( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CSettlementDistributions::createService()->fetchSuccessfulSettlementDistributionsByArPaymentIdByCid( $this->m_intId, $this->m_intCid, $objPaymentDatabase );
	}

	public function fetchBatchedArPayments( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CBatchedArPayments::createService()->fetchBatchedArPaymentsByArPaymentId( $this->m_intId, $objPaymentDatabase );
	}

	public function fetchArPaymentTransactions( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CArPaymentTransactions::createService()->fetchArPaymentTransactionsbyArPaymentId( $this->m_intId, $objPaymentDatabase );
	}

	public function fetchAuthCode( $arrintPaymentTransactionTypeIds, $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CArPaymentTransactions::createService()->fetchArPaymentAuthCodeByArPaymentIdPaymentTransactionTypeIds( $this->getId(), $arrintPaymentTransactionTypeIds, $objPaymentDatabase );
	}

	public function fetchArPaymentTransactionByArPaymentTransactionTypeId( $intArPaymentTransactionTypeId, $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CArPaymentTransactions::createService()->fetchArPaymentTransactionByArPaymentIdByArPaymentTransactionTypeId( $this->getId(), $intArPaymentTransactionTypeId, $objPaymentDatabase );
	}

	public function fetchIntegrationResults( $objDatabase ) {

		$arrintIntegrationServiceIds = [
			CIntegrationService::MODIFY_AR_PAYMENT,
			CIntegrationService::MODIFY_AR_PAYMENTS,
			CIntegrationService::REVERSE_AR_PAYMENT,
			CIntegrationService::REVERSE_AR_PAYMENTS,
			CIntegrationService::SEND_AR_PAYMENT,
			CIntegrationService::SEND_AR_PAYMENTS,
			CIntegrationService::MODIFY_REVERSED_AR_PAYMENT,
			CIntegrationService::MODIFY_REVERSED_AR_PAYMENTS,
			CIntegrationService::CLOSE_PAYMENT_BATCH,
			CIntegrationService::CLOSE_RETURNED_PAYMENT_BATCH,
			CIntegrationService::RETURN_AR_PAYMENT,
			CIntegrationService::RETURN_AR_PAYMENTS,
			CIntegrationService::MODIFY_RETURNED_AR_PAYMENT,
			CIntegrationService::MODIFY_RETURNED_AR_PAYMENTS,
			CIntegrationService::RETRIEVE_AR_PAYMENT,
			CIntegrationService::RETRIEVE_AR_PAYMENTS
		];

		return CIntegrationResults::fetchIntegrationResultsByIntegrationServiceIdsByReferenceNumberByCid( $arrintIntegrationServiceIds, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationQueues( $objDatabase ) {

		$arrintIntegrationServiceIds = [
			CIntegrationService::MODIFY_AR_PAYMENT,
			CIntegrationService::MODIFY_AR_PAYMENTS,
			CIntegrationService::REVERSE_AR_PAYMENT,
			CIntegrationService::REVERSE_AR_PAYMENTS,
			CIntegrationService::SEND_AR_PAYMENT,
			CIntegrationService::SEND_AR_PAYMENTS,
			CIntegrationService::MODIFY_REVERSED_AR_PAYMENT,
			CIntegrationService::MODIFY_REVERSED_AR_PAYMENTS,
			CIntegrationService::CLOSE_PAYMENT_BATCH,
			CIntegrationService::CLOSE_RETURNED_PAYMENT_BATCH,
			CIntegrationService::RETURN_AR_PAYMENT,
			CIntegrationService::RETURN_AR_PAYMENTS,
			CIntegrationService::MODIFY_RETURNED_AR_PAYMENT,
			CIntegrationService::MODIFY_RETURNED_AR_PAYMENTS,
			CIntegrationService::RETRIEVE_AR_PAYMENT,
			CIntegrationService::RETRIEVE_AR_PAYMENTS
		];

		return \Psi\Eos\Entrata\CIntegrationQueues::createService()->fetchIntegrationQueuesByIntegrationServiceIdsByReferenceNumbersByCid( $arrintIntegrationServiceIds, [ $this->getId() ], $this->getCid(), $objDatabase );
	}

	public function fetchArPaymentTransactionsWithUsername( $objPaymentDatabase, $objClientDatabase ) {

		$arrobjArPaymentTransactions = \Psi\Eos\Payment\CArPaymentTransactions::createService()->fetchArPaymentTransactionsbyArPaymentId( $this->m_intId, $objPaymentDatabase );

		if( false == valArr( $arrobjArPaymentTransactions ) ) return NULL;

		$arrintCompanyUserIds 	= [];

		foreach( $arrobjArPaymentTransactions as $objArPaymentTransaction ) {
			$arrintCompanyUserIds[$objArPaymentTransaction->getCreatedBy()] = $objArPaymentTransaction->getCreatedBy();
		}

		$arrobjCompanyUsers	= \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUsersWithCompanyEmployeesDetailByIdsByCid( $arrintCompanyUserIds, $this->getCid(), $objClientDatabase );

		rsort( $arrobjArPaymentTransactions, SORT_REGULAR );

		foreach( $arrobjArPaymentTransactions as $objArPaymentTransaction ) {
			$strUsername = NULL;
			$strNameFull = NULL;

			$objCompanyUser = getArrayElementByKey( $objArPaymentTransaction->getCreatedBy(), $arrobjCompanyUsers );

			if( false != valObj( $objCompanyUser, 'CCompanyUser' ) ) {
				$strNameFull  = $objCompanyUser->getNameFull();
				$strUsername  = ( ( CPaymentTransactionType::VOID < $objArPaymentTransaction->getPaymentTransactionTypeId() && CPaymentType::CHECK_21 == $this->getPaymentTypeId() ) ? 'Resident Pay Desktop - ' : 'entrata - ' ) . $objCompanyUser->getUsername();
			}

			$objArPaymentTransaction->setUsername( $strUsername );
			$objArPaymentTransaction->setNameFull( $strNameFull );
		}

		return $arrobjArPaymentTransactions;
	}

	public function fetchApplicationPaymentByArPaymentId( $objDatabase ) {

		return CApplicationPayments::fetchApplicationPaymentsByArPaymentIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchEftCharges( $objDatabase ) {
		return \Psi\Eos\Payment\CEftCharges::createService()->fetchEftChargesByArPaymentId( $this->getId(), $objDatabase );
	}

	/**
	 * Get Functions
	 *
	 */

	public function getArPaymentSplits() {
		return $this->m_arrobjArPaymentSplits;
	}

	public function getMerchantAccount() {
		return $this->m_objMerchantAccount;
	}

	public function getNachaEntryDetailRecordId() {
		return $this->m_intNachaEntryDetailRecordId;
	}

	public function getPsProductIds() {
		return $this->m_arrintPsProductIds;
	}

	public function getCheckAccountTypes() {
		return $this->m_arrobjCheckAccountTypes;
	}

	public function getPaymentArTransaction() {
		return $this->m_objPaymentArTransaction;
	}

	public function getCompanyUser() {
		return $this->m_objCompanyUser;
	}

	public function getDeletePostMonth() {
		return $this->m_strDeletePostMonth;
	}

	public function getDeleteDatetime() {
		return $this->m_strDeleteDatetime;
	}

	public function getCvvCode() {
		return $this->m_strCvvCode;
	}

	public function getDeleteMemo() {
		return $this->m_strDeleteMemo;
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getPostMemo() {
		return $this->m_strPostMemo;
	}

	public function getSuccessMsg() {
		return $this->m_objSuccessMsg;
	}

	public function getReversedPaymentAmount() {
		return $this->m_fltReversedPaymentAmount;
	}

	public function getOverrideAutoAllocations() {
		return $this->m_boolOverrideAutoAllocations;
	}

	public function getProcessingBankAccount() {
		return $this->m_objProcessingBankAccount;
	}

	public function getReversedConvenienceFeeAmount() {
		return $this->m_fltReversedConvenienceFeeAmount;
	}

	public function getChargeBackAmount() {
		return $this->m_intChargeBackAmount;
	}

	public function getRecreditConvenienceFees() {
		return $this->m_boolRecreditConvenienceFees;
	}

	public function getIsLeadPayment() {
		return $this->m_boolIsLeadPayment;
	}

	public function getWaiveReversalFees() {
		return $this->m_boolWaiveReversalFees;
	}

	public function getIsTest() {
		return $this->m_boolIsTest;
	}

	public function getEatConvenienceFees() {
		return $this->m_boolEatConvenienceFees;
	}

	public function getIsReverseImmediately() {
		return $this->m_boolIsReverseImmediately;
	}

	public function getHasFlatConvenienceFees() {
		return $this->m_boolHasFlatConvenienceFees;
	}

	public function getIsCheck21Adjustment() {
		return $this->m_boolIsCheck21Adjustment;
	}

	public function getAdjustingPaymentAmount() {
		return $this->m_fltAdjustingPaymentAmount;
	}

	public function getPropertyGlSetting() {
		return $this->m_objPropertyGlSetting;
	}

	public function getPostArAllocations() {
		return $this->m_arrfltPostArAllocations;
	}

	public function getSystemEmail() {
		return $this->m_objSystemEmail;
	}

	public function getOverrideVisaPhoneAuthorization() {
		return $this->m_boolOverrideVisaPhoneAuthorization;
	}

	public function getBilltoNameFull() {
		if( false == valStr( $this->m_strBilltoNameFirst ) && false == valStr( $this->m_strBilltoNameLast ) ) {
			return trim( $this->m_strBilltoCompanyName );
		} else {
			return trim( $this->m_strBilltoNameFirst ) . ' ' . trim( $this->m_strBilltoNameLast );
		}
	}

	public function getArPaymentImageFront() {
		return $this->m_objArPaymentImageFront;
	}

	public function getArPaymentImageFrontHash() {
		return $this->m_objArPaymentImageFrontHash;
	}

	public function getArPaymentNotes() {
		return $this->m_arrobjArPaymentNotes;
	}

	public function getDoesntHaveCheckNumber() {
		return $this->m_boolDoesntHaveCheckNumber;
	}

	public function getArPaymentImageReverse() {
		return $this->m_objArPaymentImageReverse;
	}

	public function getArPaymentImageReverseHash() {
		return $this->m_objArPaymentImageReverseHash;
	}

	public function getArPaymentMagstrip() {
		return $this->m_objArPaymentMagstrip;
	}

	public function getCompanyMerchantAccount() {
		return $this->m_objCompanyMerchantAccount;
	}

	public function getProperty() {
		return $this->m_objProperty;
	}

	public function getClient() {
		return $this->m_objClient;
	}

	public function getCustomer() {
		return $this->m_objCustomer;
	}

	/**
	 * @return \CLease
	 */
	public function getLease() {
		return $this->m_objLease;
	}

	public function getArPaymentImages() {
		return $this->m_arrobjArPaymentImages;
	}

	public function getWaiveFee() {
		return $this->m_boolWaiveFee;
	}

	public function getAgreesToTerms() {
		return $this->m_boolAgreesToTerms;
	}

	public function getAgreesToReversePayment() {
		return $this->m_boolAgreesToReversePayment;
	}

	public function getIsFloating() {
		return $this->m_boolIsFloating;
	}

	public function getUseForwardBalance() {
		return $this->m_boolUseForwardBalance;
	}

	public function getIntermediaryAccount() {
		return $this->m_objIntermediaryAccount;
	}

	public function getIsApplicationPayment() {
		return $this->m_boolIsApplicationPayment;
	}

	public function getIsResidentPortalPayment() {
		return $this->m_boolIsResidentPortalPayment;
	}

	public function getIsEntrataPayment() {
		return $this->m_boolIsEntrataPayment;
	}

	public function getIsGroupPayment() {
		return $this->m_boolIsGroupPayment;
	}

	public function getIsVisaPilot() {
		return $this->m_boolIsVisaPilot;
	}

	public function getDoesntHaveUnitNumber() {
		return $this->m_boolDoesntHaveUnitNumber;
	}

	public function getConfirmPaymentAmount() {
		return $this->m_fltConfirmPaymentAmount;
	}

	public function getArPaymentDetail() {
		return $this->m_objArPaymentDetail;
	}

	public function getArPaymentBilling() {
		return $this->m_objArPaymentBilling;
	}

	public function getAmountDue() {
		return $this->m_fltAmountDue;
	}

	public function getArTransactions() {
		return $this->m_arrobjArTransactions;
	}

	public function getPaymentDescription() {
		return $this->m_strPaymentDescription;
	}

	public function getAgreesToPayConvenienceFees() {
		return $this->m_boolAgreesToPayConvenienceFees;
	}

	public function getPromiseToPay() {
		return $this->m_boolPromiseToPay;
	}

	public function getAgreesToPayReturnFees() {
		return $this->m_boolAgreesToPayReturnFees;
	}

	public function getCcCardNumber() {
		if( !valStr( $this->getCcCardNumberEncrypted() ) ) {
			return '';
		}

		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCcCardNumberEncrypted(), CONFIG_SODIUM_KEY_CC_CARD_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_CC_CARD_NUMBER ] );
	}

	/**
	 * @return \CPaymentType[]
	 */
	public function getPaymentTypes() {
		return $this->m_arrobjPaymentTypes;
	}

	public function getDisplayPaymentStatusType() {
		return CPaymentStatusTypes::paymentStatusTypeIdToStr( $this->getPaymentStatusTypeId() );
	}

	public function getDisplayPaymentType() {
		return CPaymentTypes::paymentTypeIdToStr( $this->getPaymentTypeId() );
	}

	public function getCcCardNumberLastFour() {
		return \Psi\CStringService::singleton()->substr( $this->getCcCardNumber(), -4 );
	}

	public function getCcCardNumberMasked() {
		$strCcCardNumber = $this->getCcCardNumber();
		$intStringLength = strlen( $strCcCardNumber );
		$strLastFour = \Psi\CStringService::singleton()->substr( $strCcCardNumber, -4 );
		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function getCheckAccountNumber() {
		if( !valStr( $this->getCheckAccountNumberEncrypted() ) ) {
			return '';
		}

		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCheckAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
	}

	public function getCheckAccountNumberMasked() {
		$strCheckAccountNumber = $this->getCheckAccountNumber();
		$intStringLength = strlen( $strCheckAccountNumber );
		$strLastFour = \Psi\CStringService::singleton()->substr( $strCheckAccountNumber, -4 );
		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function getArAllocations() {
		return $this->m_arrmixArAllocations;
	}

	public function getCustomerMapped() {
		return $this->m_boolCustomerMapped;
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function getBankAccountId() {
		return $this->m_intBankAccountId;
	}

	public function getBillingInfoIsStored() {
		return $this->m_boolBillingInfoIsStored;
	}

	public function getCustomerPaymentAccountId() {
		return $this->m_intCustomerPaymentAccountId;
	}

	public function getUseStoredBillingInfo() {
		return $this->m_boolUseStoredBillingInfo;
	}

	public function getIsUpdateStoredBillingInfo() {
		return $this->m_boolIsUpdateStoredBillingInfo;
	}

	public function getBatchIsClosing() {
		return $this->m_boolBatchIsClosing;
	}

	public function getDuplicateCounter() {
		return $this->m_intDuplicateCounter;
	}

	public function getUseUnallocatedAmount() {
		return $this->m_boolUseUnallocatedAmount;
	}

	public function getNsfChargeAmount() {
		return $this->m_floatNsfChargeAmount;
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function getAccountNickname() {
		return $this->m_strAccountNickname;
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function getSpaceNumber() {
		return $this->m_strSpaceNumber;
	}

	public function getTransactionDatetime() {
		return $this->m_strTransactionDatetime;
	}

	public function getCheckRoutingNumberMasked( $intNumberOfDigitsToMask = 2 ) {
		$intStringLength = strlen( $this->m_strCheckRoutingNumber );
		$strLastFour = \Psi\CStringService::singleton()->substr( $this->m_strCheckRoutingNumber, -$intNumberOfDigitsToMask );
		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function getBilltoAccountNumberMasked() {
		$intStringLength = strlen( $this->m_strBilltoAccountNumber );
		$strLastFour = \Psi\CStringService::singleton()->substr( $this->m_strBilltoAccountNumber, -4 );
		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function getConfirmCheckAccountNumber() {
		return $this->m_strConfirmCheckAccountNumber;
	}

	public function getIsAllowInternationalCard() {
		return $this->m_boolIsAllowInternationalCard;
	}

	public function getFormattedPostMonth() {
		if( true == is_null( $this->m_strPostMonth ) ) return NULL;
		return preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', $this->m_strPostMonth );
	}

	public function getFormattedDeletePostMonth() {
		if( true == is_null( $this->m_strDeletePostMonth ) ) return NULL;
		return preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', $this->m_strDeletePostMonth );
	}

	public function getArDepositNumber() {
		return $this->m_intArDepositNumber;
	}

	public function getScheduledPayment() {
		return $this->m_objScheduledPayment;
	}

	public function getPaymentDateDiff() {
		return $this->m_intPaymentDateDiff;
	}

	public function getIsDuplicate() {
		return $this->m_boolIsDuplicate;
	}

	public function getApplyDonationFlag() {
		return $this->m_boolApplyDonationFlag;
	}

	public function getIsMapPostDonationData() {
		return $this->m_boolIsMapPostDonationData;
	}

	public function getAgreesToPayDonationAmount() {
		return $this->m_boolAgreesToPayDonationAmount;
	}

	public function getPaymentDatetimeWithTimeZone( $objProperty, $objDatabase, $strFormat = 'M d Y H:i a T' ) {

		if( false == valObj( $objProperty, 'CProperty' ) ) return false;

		$objDateTime = new DateTime( $this->getPaymentDatetime() );

		$objDateTime->setTimezone( new DateTimeZone( $objProperty->fetchTimezone( $objDatabase )->getTimeZoneName() ) );

		return $objDateTime->format( $strFormat );
	}

	public function getLeaseStatusType() {
		return $this->m_strLeaseStatusType;
	}

	public function getIsShowIntegrationError() {
		return $this->m_boolIsShowIntegrationError;
	}

	public function getIsPhoneCapturePending() {
		return $this->m_boolIsPhoneCapturePending;
	}

	public function getDeletePostDate() {
		return $this->m_strDeletePostDate;
	}

	public function getReturnItemFee() {
		return $this->m_fltReturnItemFee;
	}

	public function getSendChargeBackEmail() {
		return $this->m_boolSendChargeBackEmail;
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function getApplication() {
		return $this->m_objApplication;
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function getApplicant() {
		return $this->m_objApplicant;
	}

	public function getDataBlobTypeId() {
		return $this->m_intDataBlobTypeId;
	}

	public function getDistributeOnTimestamp() {
		return strtotime( $this->getDistributeOn() . ' 15:30' );
	}

	public function getRpPremiumMakePayment() {
		return $this->m_boolRpPremiumMakePayment;
	}

	public function getRentChargeDue() {
		return $this->m_boolRentChargeDue;
	}

	public function getActivePaymentArTransactions() {
		return $this->m_arrobjActivePaymentArTransactions;
	}

	public function getPaymentArCodeId() {
		return $this->m_intPaymentArCodeId;
	}

	public function getSendEmailPaymentReceipts() {
		return $this->m_boolSendEmailPaymentReceipts;
	}

	public function getBilltoNameFirstEncrypted() {
		return $this->m_strBilltoNameFirst;
	}

	public function getBilltoNameMiddleEncrypted() {
		return $this->m_strBilltoNameMiddle;
	}

	public function getBilltoNameLastEncrypted() {
		return $this->m_strBilltoNameLast;
	}

	public function getBilltoNameLastMatronymic() {
		return $this->m_strBilltoNameLastMatronymic;
	}

	public function getArTransactionIds() {
		return $this->m_arrintArTransactionIds;
	}

	public function getCheck21ConversionAmountValidation() {
		return $this->m_boolCheck21ConversionAmountValidation;
	}

	public function getUseExternalDatabaseTransaction() {
		return $this->m_boolUseExternalDatabaseTransaction;
	}

	public function getRepaymentId() {
		return $this->m_intRepaymentId;
	}

	public function getAllocateArTransactionIds() {
		return json_decode( $this->m_strAllocateArTransactionIds );
	}

	public function getPastPostMonth() {
		return $this->m_intPastPostMonth;
	}

	public function getFuturePostMonth() {
		return $this->m_intFuturePostMonth;
	}

	public function getAutoCorrectTransactionDates() {
		return $this->m_boolAutoCorrectTransactionDates;
	}

	public function getValidatePastOrFuturePostMonth() {
		return $this->m_boolValidatePastOrFuturePostMonth;
	}

	public function getOverridePostMonthPermission() {
		return $this->m_boolOverridePostMonthPermission;
	}

	public function getUseTransactionDateForAutoAllocation() {
		return $this->m_boolUseTransactionDateForAutoAllocation;
	}

	public function getExcludeAutoWriteOff() {
		return $this->m_boolExcludeAutoWriteOff;
	}

	public function getAccountVerificationLogId() {
		return $this->m_intAccountVerificationLogId;
	}

	public function getDebitPricing() {
		return $this->m_intDebitPricing;
	}

	public function getSummarisedPaymentTypeId() {
		return $this->m_intSummarisedPaymentTypeId;
	}

	public function getCheckAccountNumberBlindIndex() {
		return $this->m_strCheckAccountNumberBlindIndex;
	}

	public function getPaymentGroupId() {
		return $this->m_strPaymentGroupId;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setCustomerPaymentAccountId( $intCustomerPaymentAccountId ) {
		$this->m_intCustomerPaymentAccountId = CStrings::strToIntDef( $intCustomerPaymentAccountId, NULL, true );
	}

	public function setIsShowIntegrationError( $boolIsShowIntegrationError = false ) {
		$this->m_boolIsShowIntegrationError = $boolIsShowIntegrationError;
	}

	public function setIsPhoneCapturePending( $boolIsPhoneCapturePending = false ) {
		$this->m_boolIsPhoneCapturePending = $boolIsPhoneCapturePending;
	}

	public function setDefaults() {

		$this->setPaymentStatusTypeId( CPaymentStatusType::PENDING );

		$this->setBilltoIpAddress( getRemoteIpAddress() );

		$this->setPaymentCost( 0.00 );
		$this->setCompanyChargeAmount( 0.00 );
		$this->setConvenienceFeeAmount( 0.00 );

		$this->setReceiptDatetime( date( 'm/d/Y H:i:s' ) );

		$this->setCheckAccountTypeId( CCheckAccountType::PERSONAL_CHECKING );

		$this->setIsDuplicate( false );

		return;
	}

	public function setIsTerminal( $intIsTerminal ) {
		$this->m_intIsTerminal = $intIsTerminal;
	}

	public function setPaymentArCodeId( $intPaymentArCodeId ) {
		$this->m_intPaymentArCodeId = $intPaymentArCodeId;
	}

	public function setSendEmailPaymentReceipt( $boolSendEmailPaymentReceipt ) {
		$this->m_boolSendEmailPaymentReceipts = $boolSendEmailPaymentReceipt;
	}

	public function setPropertyChargeSetting( $objPropertyChargeSetting ) {
		$this->m_objPropertyChargeSetting = $objPropertyChargeSetting;
	}

	public function setRepaymentId( $intRepaymentId ) {
		$this->m_intRepaymentId = $intRepaymentId;
	}

	public function setAllocateArTransactionIds( $arrintAllocateArTransactionIds ) {
		$this->m_strAllocateArTransactionIds = json_encode( $arrintAllocateArTransactionIds );
	}

	public function setOverridePostMonthPermission( $boolOverridePostMonthPermission ) {
		$this->m_boolOverridePostMonthPermission = $boolOverridePostMonthPermission;
	}

	public function setUseTransactionDateForAutoAllocation( $boolUseTransactionDateForAutoAllocation ) {
		$this->m_boolUseTransactionDateForAutoAllocation = $boolUseTransactionDateForAutoAllocation;
	}

	public function setExcludeAutoWriteOff( $boolExcludeAutoWriteOff ) {
		$this->m_boolExcludeAutoWriteOff = $boolExcludeAutoWriteOff;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['secure_reference_number_error'] ) )		$this->setSecureReferenceNumberError( $arrmixValues['secure_reference_number_error'] );
		if( true == isset( $arrmixValues['auth_token'] ) )		                    $this->setAuthToken( $arrmixValues['auth_token'] );
		if( true == isset( $arrmixValues['applicant_application_id'] ) )			$this->setApplicantApplicationId( $arrmixValues['applicant_application_id'] );
		if( true == isset( $arrmixValues['blob'] ) ) 								$this->setBlob( $arrmixValues['blob'] );
		if( true == isset( $arrmixValues['agrees_to_terms'] ) )						$this->setAgreesToTerms( $arrmixValues['agrees_to_terms'] );
		if( true == isset( $arrmixValues['agrees_to_reverse_payment'] ) )			$this->setAgreesToReversePayment( $arrmixValues['agrees_to_reverse_payment'] );
		if( true == isset( $arrmixValues['use_unallocated_amount'] ) )				$this->setUseUnallocatedAmount( $arrmixValues['use_unallocated_amount'] );
		if( true == isset( $arrmixValues['agrees_to_pay_convenience_fees'] ) ) 		$this->setAgreesToPayConvenienceFees( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['agrees_to_pay_convenience_fees'] ) : $arrmixValues['agrees_to_pay_convenience_fees'] );
		if( true == isset( $arrmixValues['promise_to_pay'] ) ) 						$this->setPromiseToPay( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['promise_to_pay'] ) : $arrmixValues['promise_to_pay'] );
		if( true == isset( $arrmixValues['agrees_to_pay_return_fees'] ) ) 			$this->setAgreesToPayReturnFees( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['agrees_to_pay_return_fees'] ) : $arrmixValues['agrees_to_pay_return_fees'] );
		if( true == isset( $arrmixValues['waive_fee'] ) ) 							$this->setWaiveFee( $arrmixValues['waive_fee'] );
		if( true == isset( $arrmixValues['is_floating'] ) ) 						$this->setIsFloating( $arrmixValues['is_floating'] );
		if( true == isset( $arrmixValues['use_forward_balance'] ) ) 				$this->setUseForwardBalance( $arrmixValues['use_forward_balance'] );
		if( true == isset( $arrmixValues['confirm_payment_amount'] ) ) 				$this->setConfirmPaymentAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['confirm_payment_amount'] ) : $arrmixValues['confirm_payment_amount'] );
		if( true == isset( $arrmixValues['doesnt_have_unit_number'] ) ) 			$this->setDoesntHaveUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['doesnt_have_unit_number'] ) : $arrmixValues['doesnt_have_unit_number'] );
		if( true == isset( $arrmixValues['check_account_number'] ) ) 				$this->setCheckAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['check_account_number'] ) : $arrmixValues['check_account_number'] );
		if( true == isset( $arrmixValues['confirm_check_account_number'] ) ) 		$this->setConfirmCheckAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['confirm_check_account_number'] ) : $arrmixValues['confirm_check_account_number'] );
		if( true == isset( $arrmixValues['is_terminal'] ) ) 						$this->setIsTerminal( $arrmixValues['is_terminal'] );
		if( true == isset( $arrmixValues['cc_card_number'] ) ) 						$this->setCcCardNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['cc_card_number'] ) : $arrmixValues['cc_card_number'] );
		if( true == isset( $arrmixValues['billing_info_is_stored'] ) ) 				$this->setBillingInfoIsStored( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['billing_info_is_stored'] ) : $arrmixValues['billing_info_is_stored'] );
		if( true == isset( $arrmixValues['is_update_billing_info'] ) ) 				$this->setIsUpdateStoredBillingInfo( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_update_billing_info'] ) : $arrmixValues['is_update_billing_info'] );
		if( true == isset( $arrmixValues['customer_payment_account_id'] ) ) 		$this->setCustomerPaymentAccountId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['customer_payment_account_id'] ) : $arrmixValues['customer_payment_account_id'] );
		if( true == isset( $arrmixValues['use_stored_billing_info'] ) ) 			$this->setUseStoredBillingInfo( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['use_stored_billing_info'] ) : $arrmixValues['use_stored_billing_info'] );
		if( true == isset( $arrmixValues['nsf_charge_amount'] ) ) 					$this->setNsfChargeAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['nsf_charge_amount'] ) : $arrmixValues['nsf_charge_amount'] );
		if( true == isset( $arrmixValues['account_name'] ) ) 						$this->setAccountName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['account_name'] ) : $arrmixValues['account_name'] );
		if( true == isset( $arrmixValues['unit_number'] ) ) 						$this->setUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['unit_number'] ) : $arrmixValues['unit_number'] );
		if( true == isset( $arrmixValues['space_number'] ) ) 						$this->setSpaceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['space_number'] ) : $arrmixValues['space_number'] );
		if( true == isset( $arrmixValues['transaction_datetime'] ) ) 				$this->setTransactionDatetime( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['transaction_datetime'] ) : $arrmixValues['transaction_datetime'] );
		if( true == isset( $arrmixValues['has_flat_convenience_fees'] ) ) 			$this->setHasFlatConvenienceFees( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['has_flat_convenience_fees'] ) : $arrmixValues['has_flat_convenience_fees'] );
		if( true == isset( $arrmixValues['recredit_convenience_fees'] ) ) 			$this->setRecreditConvenienceFees( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['recredit_convenience_fees'] ) : $arrmixValues['recredit_convenience_fees'] );
		if( true == isset( $arrmixValues['waive_reversal_fees'] ) ) 				$this->setWaiveReversalFees( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['waive_reversal_fees'] ) : $arrmixValues['waive_reversal_fees'] );
		if( true == isset( $arrmixValues['eat_convenience_fees'] ) ) 				$this->setEatConvenienceFees( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['eat_convenience_fees'] ) : $arrmixValues['eat_convenience_fees'] );
		if( true == isset( $arrmixValues['is_reverse_immediately'] ) ) 				$this->setIsReverseImmediately( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_reverse_immediately'] ) : $arrmixValues['is_reverse_immediately'] );
		if( true == isset( $arrmixValues['adjusting_payment_amount'] ) ) 			$this->setAdjustingPaymentAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['adjusting_payment_amount'] ) : $arrmixValues['adjusting_payment_amount'] );
		if( true == isset( $arrmixValues['is_allow_international_card'] ) ) 		$this->setIsAllowInternationalCard( $arrmixValues['is_allow_international_card'] );
		if( true == isset( $arrmixValues['post_month'] ) ) 							$this->setPostMonth( $arrmixValues['post_month'] );
		if( true == isset( $arrmixValues['activate_standard_posting'] ) )			$this->setActivateStandardPosting( $arrmixValues['activate_standard_posting'] );
		if( true == isset( $arrmixValues['property_name'] ) ) 						$this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['post_memo'] ) ) 							$this->setPostMemo( $arrmixValues['post_memo'] );
		if( true == isset( $arrmixValues['delete_post_month'] ) ) 					$this->setDeletePostMonth( $arrmixValues['delete_post_month'] );
		if( true == isset( $arrmixValues['delete_datetime'] ) ) 					$this->setDeleteDatetime( $arrmixValues['delete_datetime'] );
		if( true == isset( $arrmixValues['delete_memo'] ) ) 						$this->setDeleteMemo( $arrmixValues['delete_memo'] );
		if( true == isset( $arrmixValues['delete_post_date'] ) ) 					$this->setDeletePostDate( $arrmixValues['delete_post_date'] );
		if( true == isset( $arrmixValues['return_item_fee'] ) ) 					$this->setReturnItemFee( $arrmixValues['return_item_fee'] );
		if( true == isset( $arrmixValues['account_nickname'] ) ) 					$this->setAccountNickname( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['account_nickname'] ) : $arrmixValues['account_nickname'] );
		if( true == isset( $arrmixValues['payment_internal_memo'] ) ) 				$this->setPaymentInternalMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['payment_internal_memo'] ) : $arrmixValues['payment_internal_memo'] );

		// This has to be called after processing_bank_account_id setter is called.
		if( true == isset( $arrmixValues['processing_bank_account_id_alt'] ) )		$this->setProcessingBankAccountIdAlt( $arrmixValues['processing_bank_account_id_alt'] );
		if( true == isset( $arrmixValues['merchant_gateway_id_alt'] ) )	    		$this->setMerchantGatewayIdAlt( $arrmixValues['merchant_gateway_id_alt'] );

		if( true == isset( $arrmixValues['payment_date_diff'] ) )					$this->setPaymentDateDiff( $arrmixValues['payment_date_diff'] );

		if( true == isset( $arrmixValues['agrees_to_pay_donation_amount'] ) )		$this->setAgreesToPayDonationAmount( $arrmixValues['agrees_to_pay_donation_amount'] );
		if( true == isset( $arrmixValues['apply_donation_flag'] ) )					$this->setApplyDonationFlag( $arrmixValues['apply_donation_flag'] );
		if( true == isset( $arrmixValues['applicant_application_id'] ) )			$this->setApplicantApplicationId( $arrmixValues['applicant_application_id'] );
		if( true == isset( $arrmixValues['nacha_entry_detail_record_id'] ) )		$this->setNachaEntryDetailRecordId( $arrmixValues['nacha_entry_detail_record_id'] );
		if( true == isset( $arrmixValues['send_charge_back_email'] ) )				$this->setSendChargeBackEmail( $arrmixValues['send_charge_back_email'] );
		if( true == isset( $arrmixValues['application_id'] ) )						$this->setApplicationId( $arrmixValues['application_id'] );
		if( true == isset( $arrmixValues['applicant_id'] ) )						$this->setApplicantId( $arrmixValues['applicant_id'] );
		if( true == isset( $arrmixValues['data_blob_type_id'] ) )					$this->setDataBlobTypeId( $arrmixValues['data_blob_type_id'] );
		if( true == isset( $arrmixValues['payment_ar_code_id'] ) )					$this->setPaymentArCodeId( $arrmixValues['payment_ar_code_id'] );
		if( true == isset( $arrmixValues['send_email_payment_receipts'] ) )			$this->setSendEmailPaymentReceipt( $arrmixValues['send_email_payment_receipts'] );
		if( true == isset( $arrmixValues['cvv_code'] ) )							$this->setCvvCode( $arrmixValues['cvv_code'] );
		if( true == isset( $arrmixValues['bank_account_id'] ) )						$this->setBankAccountId( $arrmixValues['bank_account_id'] );
		if( true == isset( $arrmixValues['debit_pricing'] ) )                       $this->setDebitPricing( $arrmixValues['debit_pricing'] );
		if( true == isset( $arrmixValues['summarised_payment_type_id'] ) )          $this->setSummarisedPaymentTypeId( $arrmixValues['summarised_payment_type_id'] );
		if( true == isset( $arrmixValues['ar_transaction_id'] ) )                   $this->setArTransactionId( $arrmixValues['ar_transaction_id'] );
		if( true == isset( $arrmixValues['tokenex_token'] ) ) 						$this->setTokenexToken( $arrmixValues['tokenex_token'] );
		if( true == isset( $arrmixValues['payment_bank_account_id'] ) ) 			$this->setPaymentBankAccountId( $arrmixValues['payment_bank_account_id'] );
		if( true == isset( $arrmixValues['billto_name_last_matronymic'] ) ) 		$this->setBilltoNameLastMatronymic( $arrmixValues['billto_name_last_matronymic'] );
		if( true == isset( $arrmixValues['postal_addresses'] ) )                  	$this->setPostalAddresses( $arrmixValues['postal_addresses'] );

		return;
	}

	// This field comes from the data blobs table, and is used for fast searches.  We apply the string of data from data blobs to this object in this function

	public function setBlob( $strBlob ) {

		if( 0 < strlen( trim( $strBlob ) ) ) {
			$arrstrBlobData = explode( '~..$', $strBlob );

			$this->m_strBilltoNameFirst 		= ( true == isset ( $arrstrBlobData[0] ) ) ? $arrstrBlobData[0] : NULL;
			$this->m_strBilltoNameLast 			= ( true == isset ( $arrstrBlobData[1] ) ) ? $arrstrBlobData[1] : NULL;

			if( true == is_null( $this->m_strBilltoUnitNumber ) ) $this->m_strBilltoUnitNumber	= ( true == isset ( $arrstrBlobData[4] ) ) ? $arrstrBlobData[4] : NULL;

			$this->m_strLeaseStatusType 		= ( true == isset ( $arrstrBlobData[6] ) ) ? $arrstrBlobData[6] : NULL;
		}
	}

	public function loadCustomerPaymentAccount( $objClientDatabase ) {

		if( true == is_numeric( $this->getCustomerPaymentAccountId() ) && 1 == $this->getUseStoredBillingInfo() ) {
			$objCustomer = $this->getOrFetchCustomer( $objClientDatabase );

			if( true == valObj( $objCustomer, 'CCustomer' ) ) {
				$arrobjCustomerPaymentAccounts = $objCustomer->getOrFetchCustomerPaymentAccounts( $objClientDatabase );

				if( true == valArr( $arrobjCustomerPaymentAccounts ) && true == array_key_exists( $this->getCustomerPaymentAccountId(), $arrobjCustomerPaymentAccounts ) ) {
					$this->setPaymentInformationByCustomerPaymentAccount( $arrobjCustomerPaymentAccounts[$this->getCustomerPaymentAccountId()], $objClientDatabase );
				}
			}
		}
	}

	public function indicateCustomerPaymentAccountUsage( $intCompanyUserId, $objClientDatabase ) {

		if( true == is_numeric( $this->getCustomerPaymentAccountId() ) && 1 == $this->getUseStoredBillingInfo() ) {
			$objCustomer = $this->getOrFetchCustomer( $objClientDatabase );

			if( true == valObj( $objCustomer, 'CCustomer' ) ) {
				$arrobjCustomerPaymentAccounts = $objCustomer->getOrFetchCustomerPaymentAccounts( $objClientDatabase );

				if( true == valArr( $arrobjCustomerPaymentAccounts ) && true == array_key_exists( $this->getCustomerPaymentAccountId(), $arrobjCustomerPaymentAccounts ) ) {
					$arrobjCustomerPaymentAccounts[$this->getCustomerPaymentAccountId()]->setLastUsedOn( date( 'm/d/Y H:i:s' ) );
					return $arrobjCustomerPaymentAccounts[$this->getCustomerPaymentAccountId()]->update( $intCompanyUserId, $objClientDatabase );
				}
			}
		}

		return true;
	}

	public function setPaymentInformationByCustomerPaymentAccount( $objCustomerPaymentAccount, $objClientDatabase = NULL ) {
		if( false == valObj( $objCustomerPaymentAccount, 'CCustomerPaymentAccount' ) ) return NULL;

		$this->setPaymentTypeId( $objCustomerPaymentAccount->getPaymentTypeId() );

		if( false == is_null( $objCustomerPaymentAccount->getCheckAccountNumber() ) ) 		$this->setCheckAccountNumber( $objCustomerPaymentAccount->getCheckAccountNumber() );
		if( false == is_null( $objCustomerPaymentAccount->getCheckAccountTypeId() ) ) 		$this->setCheckAccountTypeId( $objCustomerPaymentAccount->getCheckAccountTypeId() );
		if( false == is_null( $objCustomerPaymentAccount->getCheckBankName() ) ) 			$this->setCheckBankName( $objCustomerPaymentAccount->getCheckBankName() );
		if( false == is_null( $objCustomerPaymentAccount->getCheckNameOnAccount() ) ) 		$this->setCheckNameOnAccount( $objCustomerPaymentAccount->getCheckNameOnAccount() );
		if( false == is_null( $objCustomerPaymentAccount->getCheckRoutingNumber() ) ) 		$this->setCheckRoutingNumber( $objCustomerPaymentAccount->getCheckRoutingNumber() );

		if( false == is_null( $objCustomerPaymentAccount->getCcCardNumber() ) ) 			$this->setCcCardNumber( $objCustomerPaymentAccount->getCcCardNumber() );
		if( false == is_null( $objCustomerPaymentAccount->getSecureReferenceNumber() ) ) 	$this->setSecureReferenceNumber( $objCustomerPaymentAccount->getSecureReferenceNumber() );
		if( false == is_null( $objCustomerPaymentAccount->getCcExpDateMonth() ) ) 			$this->setCcExpDateMonth( $objCustomerPaymentAccount->getCcExpDateMonth() );
		if( false == is_null( $objCustomerPaymentAccount->getCcExpDateYear() ) ) 			$this->setCcExpDateYear( $objCustomerPaymentAccount->getCcExpDateYear() );
		if( false == is_null( $objCustomerPaymentAccount->getCcNameOnCard() ) ) 			$this->setCcNameOnCard( $objCustomerPaymentAccount->getCcNameOnCard() );
		if( false == is_null( $objCustomerPaymentAccount->getCcBinNumber() ) ) 				$this->setCcBinNumber( $objCustomerPaymentAccount->getCcBinNumber() );
		if( false == is_null( $objCustomerPaymentAccount->getIsDebitCard() ) ) 				$this->setIsDebitCard( $objCustomerPaymentAccount->getIsDebitCard() );

		if( false == is_null( $objCustomerPaymentAccount->getBilltoNameFirst() ) ) 		$this->setBilltoNameFirst( $objCustomerPaymentAccount->getBilltoNameFirst() );
		if( false == is_null( $objCustomerPaymentAccount->getBilltoNameLast() ) ) 		$this->setBilltoNameLast( $objCustomerPaymentAccount->getBilltoNameLast() );

		if( false == is_null( $objCustomerPaymentAccount->getBilltoPhoneNumber() ) ) 	$this->setBilltoPhoneNumber( $objCustomerPaymentAccount->getBilltoPhoneNumber() );
		if( true == is_null( $this->getBilltoEmailAddress() ) && false == is_null( $objCustomerPaymentAccount->getBilltoEmailAddress() ) ) 	$this->setBilltoEmailAddress( $objCustomerPaymentAccount->getBilltoEmailAddress() );

		if( true == is_null( $this->getBilltoStreetLine1() ) && true == is_null( $this->getBilltoStreetLine2() ) && true == is_null( $this->getBilltoStreetLine3() ) && true == is_null( $this->getBilltoCity() ) && true == is_null( $this->getBilltoPostalCode() ) && true == is_null( $this->getBilltoCountryCode() ) ) {
			if( true == is_null( $this->getBilltoStreetLine1() ) && false == is_null( $objCustomerPaymentAccount->getBilltoStreetLine1() ) ) 	$this->setBilltoStreetLine1( $objCustomerPaymentAccount->getBilltoStreetLine1() );
			if( true == is_null( $this->getBilltoStreetLine2() ) && false == is_null( $objCustomerPaymentAccount->getBilltoStreetLine2() ) ) 	$this->setBilltoStreetLine2( $objCustomerPaymentAccount->getBilltoStreetLine2() );
			if( true == is_null( $this->getBilltoStreetLine3() ) && false == is_null( $objCustomerPaymentAccount->getBilltoStreetLine3() ) ) 	$this->setBilltoStreetLine3( $objCustomerPaymentAccount->getBilltoStreetLine3() );
			if( true == is_null( $this->getBilltoStreetLine1() ) && false == is_null( $objCustomerPaymentAccount->getBilltoCity() ) ) 			$this->setBilltoCity( $objCustomerPaymentAccount->getBilltoCity() );
			if( true == is_null( $this->getBilltoCity() ) && false == is_null( $objCustomerPaymentAccount->getBilltoStateCode() ) ) 		    $this->setBilltoStateCode( $objCustomerPaymentAccount->getBilltoStateCode() );
			if( true == is_null( $this->getBilltoPostalCode() ) && false == is_null( $objCustomerPaymentAccount->getBilltoPostalCode() ) ) 	    $this->setBilltoPostalCode( $objCustomerPaymentAccount->getBilltoPostalCode() );
			if( true == is_null( $this->getBilltoCountryCode() ) && false == is_null( $objCustomerPaymentAccount->getBilltoCountryCode() ) ) 	$this->setBilltoCountryCode( $objCustomerPaymentAccount->getBilltoCountryCode() );
		}

		$this->setIsAllowInternationalCard( $objCustomerPaymentAccount->getIsInternational() );

		if( true == valObj( $objClientDatabase, 'CDatabase' ) ) {
			if( false == is_null( $objCustomerPaymentAccount->getCheckAccountNumber() ) ) {
				$objCustomerPaymentAccount->setConfirmCheckAccountNumber( $objCustomerPaymentAccount->getCheckAccountNumber() );
			}
			$objCustomerPaymentAccount->setLastUsedOn( date( 'Y-m-d H:i:s' ) );
			if( false == $objCustomerPaymentAccount->validate( VALIDATE_UPDATE, $objClientDatabase ) || ( false == $objCustomerPaymentAccount->update( SYSTEM_USER_ID, $objClientDatabase ) ) ) {
				return false;
			}
		}

		return true;
	}

	public function setPaymentInformationUsingCustomerPaymentAccount( \CCustomerPaymentAccount $objCustomerPaymentAccount, $objPaymentDatabase = NULL ) {
		if( \CPaymentType::ACH == $objCustomerPaymentAccount->getPaymentTypeId() || \CPaymentType::PAD == $objCustomerPaymentAccount->getPaymentTypeId() ) {
			$this->setAchAndPadPaymentInfoUsingPaymentAccount( $objCustomerPaymentAccount );
		} elseif( true == in_array( $objCustomerPaymentAccount->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) ) {
			$this->setCardPaymentInfoUsingPaymentAccount( $objCustomerPaymentAccount );
		} elseif( \CPaymentType::SEPA_DIRECT_DEBIT == $objCustomerPaymentAccount->getPaymentTypeId() ) {
			$this->setSepaPaymentInfoUsingPaymentAccount( $objCustomerPaymentAccount, $objPaymentDatabase );
		}
	}

	public function setSepaPaymentInfoUsingPaymentAccount( $objCustomerPaymentAccount, $objPaymentDatabase ) {
		$objAccountService = \Psi\Core\Payment\BankAccountService\CAccountService::createService( $objPaymentDatabase );
		if( true == valStr( $objCustomerPaymentAccount->getPaymentBankAccountId() ) ) {
			$objSepaAccount = $objAccountService->load( $objCustomerPaymentAccount->getPaymentBankAccountId() );
			if( true == valObj( $objSepaAccount, \Psi\Core\Payment\BankAccountService\Types\CSepa::class ) ) {
				$this->setCustomerSepaPaymentBankAccount( $objSepaAccount );
			}
		}
	}

	public function setAchAndPadPaymentInfoUsingPaymentAccount( \CCustomerPaymentAccount $objCustomerPaymentAccount ) {
		$this->setCheckBankName( stripslashes( $objCustomerPaymentAccount->getCheckBankName() ) );
		$this->setCheckAccountTypeId( $objCustomerPaymentAccount->getCheckAccountTypeId() );
		$this->setCheckAccountNumberEncrypted( stripslashes( $objCustomerPaymentAccount->getCheckAccountNumberEncrypted() ) );
		$this->setCheckRoutingNumber( stripslashes( $objCustomerPaymentAccount->getCheckRoutingNumber() ) );
		$this->setCheckNameOnAccount( stripslashes( $objCustomerPaymentAccount->getCheckNameOnAccount() ) );
	}

	public function setCardPaymentInfoUsingPaymentAccount( $objCustomerPaymentAccount ) {

		$this->setCcNameOnCard( stripslashes( $objCustomerPaymentAccount->getCcNameOnCard() ) );
		$this->setCcCardNumberEncrypted( stripslashes( $objCustomerPaymentAccount->getCcCardNumberEncrypted() ) );
		$this->setSecureReferenceNumber( stripslashes( $objCustomerPaymentAccount->getSecureReferenceNumber() ) );
		$this->setCcExpDateMonth( stripslashes( $objCustomerPaymentAccount->getCcExpDateMonth() ) );
		$this->setCcExpDateYear( stripslashes( $objCustomerPaymentAccount->getCcExpDateYear() ) );
		$this->setCcBinNumber( stripslashes( $objCustomerPaymentAccount->getCcBinNumber() ) );
		$this->setIsDebitCard( $objCustomerPaymentAccount->getIsDebitCard() );
		$this->setIsCheckCard( $objCustomerPaymentAccount->getIsCheckCard() );
		$this->setIsGiftCard( $objCustomerPaymentAccount->getIsGiftCard() );
		$this->setIsPrepaidCard( $objCustomerPaymentAccount->getIsPrepaidCard() );
		$this->setIsCreditCard( $objCustomerPaymentAccount->getIsCreditCard() );
		$this->setIsInternationalCard( $objCustomerPaymentAccount->getIsInternationalCard() );
		$this->setIsCommercialCard( $objCustomerPaymentAccount->getIsCommercialCard() );
	}

	public function setNachaEntryDetailRecordId( $intNachaEntryDetailRecordId ) {
		$this->m_intNachaEntryDetailRecordId = $intNachaEntryDetailRecordId;
	}

	public function setArPaymentSplits( $arrobjArPaymentSplits ) {
		$this->m_arrobjArPaymentSplits = $arrobjArPaymentSplits;
	}

	public function setMerchantAccount( $objMerchantAccount ) {
		$this->m_objMerchantAccount = $objMerchantAccount;
	}

	public function setPsProductIds( $arrintPsProductIds ) {
		$this->m_arrintPsProductIds = $arrintPsProductIds;
	}

	public function setApplicantApplicationId( $intApplicantApplicationId ) {
		$this->m_intApplicantApplicationId = CStrings::strToIntDef( $intApplicantApplicationId, NULL, false );
	}

	public function setApplicantApplication( $objApplicantApplication ) {
		$this->m_objApplicantApplication = $objApplicantApplication;
	}

	public function setDeletePostMonth( $strDeletePostMonth ) {

		$arrstrPostMonth = explode( '/', $strDeletePostMonth );
		if( true == valArr( $arrstrPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
			$strDeletePostMonth = $arrstrPostMonth[0] . '/1/' . $arrstrPostMonth[1];
		}

		$this->m_strDeletePostMonth = $strDeletePostMonth;
	}

	public function setDeleteDatetime( $strDeleteDatetime ) {
		$this->m_strDeleteDatetime = CStrings::strTrimDef( $strDeleteDatetime, -1, NULL, true );
	}

	public function setCvvCode( $strCvvCode ) {
		$this->m_strCvvCode = CStrings::strTrimDef( $strCvvCode, -1, NULL, true );
	}

	public function setDeleteMemo( $strDeleteMemo ) {
		$this->m_strDeleteMemo = CStrings::strTrimDef( $strDeleteMemo, 2000, NULL, true );
	}

	public function setProcessingBankAccountIdAlt( $intProcessingBankAccountIdAlt ) {
		if( true == is_numeric( $intProcessingBankAccountIdAlt ) ) {
			$this->m_intProcessingBankAccountId = $intProcessingBankAccountIdAlt;
		}
	}

	public function setPropertyGlSetting( $objPropertyGlSetting ) {
		$this->m_objPropertyGlSetting = $objPropertyGlSetting;
	}

	public function setCompanyUser( $objCompanyUser ) {
		$this->m_objCompanyUser = $objCompanyUser;
	}

	public function setMerchantGatewayIdAlt( $intMerchantGatewayIdAlt ) {
		if( true == is_numeric( $intMerchantGatewayIdAlt ) ) {
			$this->m_intMerchantGatewayId = $intMerchantGatewayIdAlt;
		}
	}

	public function setProcessingBankAccount( $objProcessingBankAccount ) {
		$this->m_objProcessingBankAccount = $objProcessingBankAccount;
	}

	public function setSuccessMsg( $objSuccessMsg ) {
		$this->m_objSuccessMsg = $objSuccessMsg;
	}

	public function setScheduledPayment( $objScheduledPayment ) {
		$this->m_objScheduledPayment = $objScheduledPayment;
	}

	public function setRecreditConvenienceFees( $boolRecreditConvenienceFees ) {
		$this->m_boolRecreditConvenienceFees = $boolRecreditConvenienceFees;
	}

	public function setIsLeadPayment( $boolIsLeadPayment ) {
		$this->m_boolIsLeadPayment = $boolIsLeadPayment;
	}

	public function setPostMonth( $strPostMonth ) {

		$arrstrPostMonth = explode( '/', $strPostMonth );

		if( true == valArr( $arrstrPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
			$strPostMonth = $arrstrPostMonth[0] . '/1/' . $arrstrPostMonth[1];
		}

		$this->m_strPostMonth = CStrings::strTrimDef( $strPostMonth, -1, NULL, true );
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setPostMemo( $strPostMemo ) {
		$this->m_strPostMemo = CStrings::strTrimDef( $strPostMemo, 2000, NULL, true );
	}

	public function setWaiveReversalFees( $boolWaiveReversalFees ) {
		$this->m_boolWaiveReversalFees = $boolWaiveReversalFees;
	}

	public function setIsTest( $boolIsTest ) {
		$this->m_boolIsTest = $boolIsTest;
	}

	public function setEatConvenienceFees( $boolEatConvenienceFees ) {
		$this->m_boolEatConvenienceFees = $boolEatConvenienceFees;
	}

	public function setOverrideAutoAllocations( $boolOverrideAutoAllocations ) {
		$this->m_boolOverrideAutoAllocations = $boolOverrideAutoAllocations;
	}

	public function setIsReverseImmediately( $boolIsReverseImmediately ) {
		$this->m_boolIsReverseImmediately = $boolIsReverseImmediately;
	}

	public function setHasFlatConvenienceFees( $boolHasFlatConvenienceFees ) {
		$this->m_boolHasFlatConvenienceFees = $boolHasFlatConvenienceFees;
	}

	public function setIsCheck21Adjustment( $boolIsCheck21Adjustment ) {
		$this->m_boolIsCheck21Adjustment = $boolIsCheck21Adjustment;
	}

	public function setAdjustingPaymentAmount( $fltAdjustingPaymentAmount ) {
		$this->m_fltAdjustingPaymentAmount = CStrings::strToFloatDef( $fltAdjustingPaymentAmount, NULL, false, 2 );
	}

	public function setOverrideVisaPhoneAuthorization( $boolOverrideVisaPhoneAuthorization ) {
		$this->m_boolOverrideVisaPhoneAuthorization = $boolOverrideVisaPhoneAuthorization;
	}

	public function setConvenienceFeeAmount( $fltConvenienceFeeAmount ) {
		if( false == isset( $fltConvenienceFeeAmount ) ) $fltConvenienceFeeAmount = 0;
		$this->m_fltConvenienceFeeAmount = CStrings::strToFloatDef( $fltConvenienceFeeAmount, NULL, false, 2 );
	}

	public function setCustomerMapped( $boolCustomerMapped ) {
		$this->m_boolCustomerMapped = $boolCustomerMapped;
	}

	public function setAmountDue( $fltAmountDue ) {
		$this->m_fltAmountDue = $fltAmountDue;
	}

	public function setPostArAllocations( $arrfltArAllocations ) {
		$this->m_arrfltPostArAllocations = $arrfltArAllocations;
	}

	public function setPaymentTypes( $arrobjPaymentTypes ) {
		$this->m_arrobjPaymentTypes = $arrobjPaymentTypes;
	}

	public function setDoesntHaveCheckNumber( $boolDoesntHaveCheckNumber ) {
		$this->m_boolDoesntHaveCheckNumber = $boolDoesntHaveCheckNumber;
	}

	public function setArTransactions( $arrobjArTransactions ) {
		$this->m_arrobjArTransactions = $arrobjArTransactions;
	}

	public function setPaymentDescription( $strPaymentDescription ) {
		$this->m_strPaymentDescription = $strPaymentDescription;
	}

	public function setArPaymentDetail( $objArPaymentDetail ) {
		$this->m_objArPaymentDetail = $objArPaymentDetail;
	}

	public function setArPaymentBilling( $objArPaymentBilling ) {
		$this->m_objArPaymentBilling = $objArPaymentBilling;
	}

	public function setIntermediaryAccount( $objIntermediaryAccount ) {
		$this->m_objIntermediaryAccount = $objIntermediaryAccount;
	}

	public function setAccount( $objAccount ) {
		$this->m_objAccount = $objAccount;
	}

	public function setBilltoPhoneNumber( $strBilltoPhoneNumber, $intIsNoHyphen = 0 ) {
		if( false == is_null( $strBilltoPhoneNumber ) ) {
			if( 1 == $intIsNoHyphen ) {
				$this->m_strBilltoPhoneNumber = CStrings::strTrimDef( $strBilltoPhoneNumber, 30, NULL, true );
			} else {
				// We need to reformat phone number to get rid of everything but numbers
				if( 0 < strlen( $strBilltoPhoneNumber ) ) {
					$strBilltoPhoneNumber = preg_replace( '/[^a-zA-Z0-9]|\s/', '', trim( $strBilltoPhoneNumber ) );
					$strBilltoPhoneNumber = \Psi\CStringService::singleton()->substr( $strBilltoPhoneNumber, 0, 3 ) . '-' . \Psi\CStringService::singleton()->substr( $strBilltoPhoneNumber, 3, 3 ) . '-' . \Psi\CStringService::singleton()->substr( $strBilltoPhoneNumber, 6, 10 );
					$this->m_strBilltoPhoneNumber = CStrings::strTrimDef( $strBilltoPhoneNumber, 30, NULL, true );
				}
			}
		}
	}

	public function setAgreesToTerms( $boolAgreesToTerms ) {
		$this->m_boolAgreesToTerms = $boolAgreesToTerms;
	}

	public function setAgreesToReversePayment( $boolAgreesToReversePayment ) {
		$this->m_boolAgreesToReversePayment  = $boolAgreesToReversePayment;
	}

	public function setArAllocations( $arrmixArAllocations ) {
		$this->m_arrmixArAllocations = $arrmixArAllocations;
	}

	public function setCheckAccountNumber( $strCheckAccountNumber ) {
		$strCheckAccountNumber = trim( $strCheckAccountNumber );
		if( true == \Psi\CStringService::singleton()->stristr( $strCheckAccountNumber, 'X' ) ) return;
		$strCheckAccountNumber = CStrings::strTrimDef( $strCheckAccountNumber, 20, NULL, true );
		if( true == \valStr( $strCheckAccountNumber ) ) {
			$this->setCheckAccountNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strCheckAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
		}
	}

	public function setCcCardNumber( $strCcCardNumber ) {
		$strCcCardNumber = CStrings::strTrimDef( $strCcCardNumber, 20, NULL, true );
		if( true == \valStr( $strCcCardNumber ) ) {
			$this->setCcCardNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strCcCardNumber, CONFIG_SODIUM_KEY_CC_CARD_NUMBER ) );
		}
	}

	public function setCcNameOnCard( $strCcNameOnCard ) {
		// remove backtick(`) from name on card
		$strCcNameOnCard = str_replace( '`', '', $strCcNameOnCard );
		$this->m_strCcNameOnCard = CStrings::strTrimDef( $strCcNameOnCard, 50, NULL, true );
	}

	public function setArPaymentImageFront( $objArPaymentImageFront ) {
		$this->m_objArPaymentImageFront = $objArPaymentImageFront;
	}

	public function setArPaymentImageReverse( $objArPaymentImageReverse ) {
		$this->m_objArPaymentImageReverse = $objArPaymentImageReverse;
	}

	public function setArPaymentMagstrip( $objArPaymentMagstrip ) {
		$this->m_objArPaymentMagstrip = $objArPaymentMagstrip;
	}

	public function setCompanyMerchantAccount( $objCompanyMerchantAccount ) {
		$this->m_objCompanyMerchantAccount = $objCompanyMerchantAccount;
	}

	public function setProperty( $objProperty ) {
		$this->m_objProperty = $objProperty;
	}

	public function setClient( $objClient ) {
		$this->m_objClient = $objClient;
	}

	public function setCustomer( $objCustomer ) {
		$this->m_objCustomer = $objCustomer;
	}

	public function setLease( $objLease ) {
		$this->m_objLease = $objLease;
	}

	public function setArPaymentImages( $arrobjArPaymentImages ) {
		$this->m_arrobjArPaymentImages = $arrobjArPaymentImages;
	}

	public function setWaiveFee( $boolWaiveFee ) {
		$this->m_boolWaiveFee = $boolWaiveFee;
	}

	public function setIsFloating( $boolIsFloating ) {
		$this->m_boolIsFloating = $boolIsFloating;
	}

	public function setUseForwardBalance( $boolUseForwardBalance ) {
		$this->m_boolUseForwardBalance = $boolUseForwardBalance;
	}

	public function setAgreesToPayConvenienceFees( $boolAgreesToPayConvenienceFees ) {
		$this->m_boolAgreesToPayConvenienceFees = $boolAgreesToPayConvenienceFees;
	}

	public function setPromiseToPay( $boolPromiseToPay ) {
		$this->m_boolPromiseToPay = $boolPromiseToPay;
	}

	public function setAgreesToPayReturnFees( $boolAgreesToPayReturnFees ) {
		$this->m_boolAgreesToPayReturnFees = $boolAgreesToPayReturnFees;
	}

	public function setIsApplicationPayment( $boolIsApplicationPayment ) {
		$this->m_boolIsApplicationPayment = $boolIsApplicationPayment;
	}

	public function setIsResidentPortalPayment( $boolIsResidentPortalPayment ) {
		$this->m_boolIsResidentPortalPayment = $boolIsResidentPortalPayment;
	}

	public function setIsEntrataPayment( $boolIsEntrataPayment ) {
		$this->m_boolIsEntrataPayment = $boolIsEntrataPayment;
	}

	public function setIsVisaPilot( $boolIsVisaPilot ) {
		$this->m_boolIsVisaPilot = $boolIsVisaPilot;
	}

	public function setDoesntHaveUnitNumber( $boolDoesntHaveUnitNumber ) {
		$this->m_boolDoesntHaveUnitNumber = $boolDoesntHaveUnitNumber;
	}

	public function setConfirmPaymentAmount( $fltConfirmPaymentAmount ) {
		$this->m_fltConfirmPaymentAmount = str_replace( '$', '', $fltConfirmPaymentAmount );
	}

	public function setIsGroupPayment( $boolIsGroupPayment ) {
		$this->m_boolIsGroupPayment = $boolIsGroupPayment;
	}

	// We need to trim off leading zeros from the check number (e.g. 009282 = 9282)

	public function setCheckNumber( $strCheckNumber ) {

		while( '0' === \Psi\CStringService::singleton()->substr( $strCheckNumber, 0, 1 ) ) {
			$strCheckNumber = \Psi\CStringService::singleton()->substr( $strCheckNumber, 1 );
		}

		$this->m_strCheckNumber = CStrings::strTrimDef( $strCheckNumber, 40, NULL, false, false, true );

		return;
	}

	public function setOrderNum( $intOrderNum ) {
		$this->m_intOrderNum = $intOrderNum;
	}

	public function setBankAccountId( $intBankAccountId ) {
		$this->m_intBankAccountId = $intBankAccountId;
	}

	public function setBillingInfoIsStored( $boolBillingInfoIsStored ) {
		$this->m_boolBillingInfoIsStored = $boolBillingInfoIsStored;
	}

	public function setUseStoredBillingInfo( $boolUseStoredBillingInfo ) {
		$this->m_boolUseStoredBillingInfo = $boolUseStoredBillingInfo;
	}

	public function setIsUpdateStoredBillingInfo( $boolIsUpdateStoredBillingInfo ) {
		$this->m_boolIsUpdateStoredBillingInfo = $boolIsUpdateStoredBillingInfo;
	}

	public function setBatchIsClosing( $boolBatchIsClosing ) {
		$this->m_boolBatchIsClosing = $boolBatchIsClosing;
	}

	public function setDuplicateCounter( $intDuplicateCounter ) {
		$this->m_intDuplicateCounter = $intDuplicateCounter;
	}

	public function setUseUnallocatedAmount( $boolUseUnallocatedAmount ) {
		$this->m_boolUseUnallocatedAmount = $boolUseUnallocatedAmount;
	}

	public function setNsfChargeAmount( $fltNsfChargeAmount ) {
		$this->m_floatNsfChargeAmount = CStrings::strToFloatDef( $fltNsfChargeAmount, NULL, false, 2 );
	}

	public function setAccountName( $strAccountName ) {
		$this->m_strAccountName = $strAccountName;
	}

	public function setAccountNickname( $strAccountNickname ) {
		$this->m_strAccountNickname = CStrings::strTrimDef( $strAccountNickname, 10, NULL, true );
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->m_strUnitNumber = $strUnitNumber;
	}

	public function setSpaceNumber( $strSpaceNumber ) {
		$this->m_strSpaceNumber = $strSpaceNumber;
	}

	public function setTransactionDatetime( $strTransactionDatetime ) {
		$this->m_strTransactionDatetime = CStrings::strTrimDef( $strTransactionDatetime, -1, NULL, true );
	}

	public function setConfirmCheckAccountNumber( $strConfirmCheckAccountNumber ) {
		return $this->m_strConfirmCheckAccountNumber = $strConfirmCheckAccountNumber;
	}

	public function setChargeBackAmount( $intChargeBackAmount ) {
		$this->m_intChargeBackAmount = CStrings::strToIntDef( $intChargeBackAmount, NULL, false );
	}

	public function setIsAllowInternationalCard( $boolIsAllowInternationalCard ) {
		return $this->m_boolIsAllowInternationalCard = $boolIsAllowInternationalCard;
	}

	public function setArDepositNumber( $intArDepositNumber ) {
		$this->m_intArDepositNumber = $intArDepositNumber;
	}

	public function setBilltoUnitNumber( $strBilltoUnitNumber ) {
		$this->m_strBilltoUnitNumber = CStrings::strTrimDef( $strBilltoUnitNumber, 50, NULL, true, false, true );
	}

	public function setBilltoNameFirst( $strBilltoNameFirst ) {
		$this->m_strBilltoNameFirst = ( 0 == strlen( trim( $strBilltoNameFirst ) ) ? NULL : CStrings::getFormattedName( $strBilltoNameFirst, 50, true ) );
	}

	public function setBilltoNameMiddle( $strBilltoNameMiddle ) {
		$this->m_strBilltoNameMiddle = ( 0 == strlen( trim( $strBilltoNameMiddle ) ) ? NULL : CStrings::getFormattedName( $strBilltoNameMiddle, 50, true ) );
	}

	public function setBilltoNameLast( $strBilltoNameLast ) {
		$this->m_strBilltoNameLast = ( 0 == strlen( trim( $strBilltoNameLast ) ) ? NULL : CStrings::getFormattedName( $strBilltoNameLast, 50, true ) );
	}

	public function setBilltoNameLastMatronymic( $strBilltoNameLastMatronymic ) {
		$this->m_strBilltoNameLastMatronymic = $strBilltoNameLastMatronymic;
	}

	public function setPaymentDateDiff( $intPaymentDateDiff ) {
		$this->m_intPaymentDateDiff = $intPaymentDateDiff;
	}

	public function setIsDuplicate( $boolDuplicate ) {
		$this->m_boolIsDuplicate = $boolDuplicate;
	}

	public function setApplyDonationFlag( $boolApplyDonationFlag ) {
		$this->m_boolApplyDonationFlag = $boolApplyDonationFlag;
	}

	public function setIsMapPostDonationData( $boolIsMapPostDonationDataToArPayment ) {
		$this->m_boolIsMapPostDonationData = ( bool ) $boolIsMapPostDonationDataToArPayment;
	}

	public function setAgreesToPayDonationAmount( $boolAgreesToPayDonationAmount ) {
		$this->m_boolAgreesToPayDonationAmount = $boolAgreesToPayDonationAmount;
	}

	public function setLeaseStatusType( $strLeaseStatusType ) {
		$this->m_strLeaseStatusType = $strLeaseStatusType;
	}

	public function setDeletePostDate( $strDeletePostDate ) {
		$this->m_strDeletePostDate = $strDeletePostDate;
	}

	public function setReturnItemFee( $fltReturnItemFee ) {
		$this->m_fltReturnItemFee = ( float ) str_replace( '$', '', $fltReturnItemFee );
	}

	public function setAllowAllocationSelection( $boolAllowAllocationSelection ) {
		$this->m_boolAllowAllocationSelection = $boolAllowAllocationSelection;
	}

	public function setRequireManualPostMonths( $boolRequireManualPostMonths ) {
		$this->m_boolRequireManualPostMonths = $boolRequireManualPostMonths;
	}

	public function setActivateStandardPosting( $boolActivateStandardPosting ) {
		$this->m_boolActivateStandardPosting = $boolActivateStandardPosting;
	}

	public function setSecureReferenceNumberError( $strSecureReferenceNumberError ) {
		$this->m_strSecureReferenceNumberError = $strSecureReferenceNumberError;
	}

	public function setAuthToken( $strAuthToken ) {
		$this->m_strAuthToken = $strAuthToken;
	}

	public function setSendChargeBackEmail( $boolSendChargeBackEmail ) {
		$this->m_boolSendChargeBackEmail = $boolSendChargeBackEmail;
	}

	public function setApplicationId( $intApplicationId ) {
		$this->m_intApplicationId = $intApplicationId;
	}

	public function setApplication( $objApplication ) {
		$this->m_objApplication = $objApplication;
	}

	public function setApplicantId( $intApplicantId ) {
		$this->m_intApplicantId = $intApplicantId;
	}

	public function setApplicant( $objApplicant ) {
		$this->m_objApplicant = $objApplicant;
	}

	public function setDataBlobTypeId( $intDataBlobTypeId ) {
		$this->m_intDataBlobTypeId = $intDataBlobTypeId;
	}

	public function setRpPremiumMakePayment( $boolRpPremiumMakePayment ) {
		$this->m_boolRpPremiumMakePayment = $boolRpPremiumMakePayment;
	}

	public function setRentChargeDue( $boolRentChargeDue ) {
		$this->m_boolRentChargeDue = $boolRentChargeDue;
	}

	public function setAutoAllocation( $boolAllocations ) {
		$this->m_boolAutoAllocations = $boolAllocations;
	}

	public function setActivePaymentArTransactions( $arrobjActivePaymentArTransactions ) {
		$this->m_arrobjActivePaymentArTransactions = $arrobjActivePaymentArTransactions;
	}

	public function setArTransactionIds( $arrintArTransactionIds ) {
		return $this->m_arrintArTransactionIds = $arrintArTransactionIds;
	}

	public function setCheck21ConversionAmountValidation( $boolCheck21ConversionAmountValidation = false ) {
		$this->m_boolCheck21ConversionAmountValidation = $boolCheck21ConversionAmountValidation;
	}

	public function setUseExternalDatabaseTransaction( $boolUseExternalDatabaseTransaction ) {
		$this->m_boolUseExternalDatabaseTransaction = $boolUseExternalDatabaseTransaction;
	}

	public function setPastPostMonth( $intPastPostMonth ) {
		$this->m_intPastPostMonth = $intPastPostMonth;
	}

	public function setFuturePostMonth( $intFuturePostMonth ) {
		$this->m_intFuturePostMonth = $intFuturePostMonth;
	}

	public function setPastPostMonthAndFuturePostMonth( $intCompanyUserId, $objDatabase ) {
		$arrstrCompanyUserPreferences = \Psi\Eos\Entrata\CCompanyUserPreferences::createService()->fetchCompanyUserPreferencesByCompanyUserIdByCidByModuleIdByKeys( $intCompanyUserId, $this->getCid(), CModule::AR_POST_MONTH, [ CCompanyUserPreference::AR_FUTURE_POST_MONTH, CCompanyUserPreference::AR_PAST_POST_MONTH ], $objDatabase );

		if( true == valArr( $arrstrCompanyUserPreferences, 2 ) ) {
			$this->setPastPostMonth( $arrstrCompanyUserPreferences[CCompanyUserPreference::AR_PAST_POST_MONTH] );
			$this->setFuturePostMonth( $arrstrCompanyUserPreferences[CCompanyUserPreference::AR_FUTURE_POST_MONTH] );
		}
	}

	public function setValidatePastOrFuturePostMonth( $boolValidatePastOrFuturePostMonth ) {
		$this->m_boolValidatePastOrFuturePostMonth = ( bool ) $boolValidatePastOrFuturePostMonth;
	}

	public function setIsPaymentCapturedBySms() {
		$this->m_boolIsPaymentCapturedBySms = true;
	}

	public function setAutoCorrectTransactionDates( $boolAutoCorrectTransactionDates ) {
		$this->m_boolAutoCorrectTransactionDates = ( bool ) $boolAutoCorrectTransactionDates;
	}

	public function setPaymentInternalMemo( $strPaymentInternalMemo ) {
		$this->m_strPaymentInternalMemo = CStrings::strTrimDef( $strPaymentInternalMemo, 2000, NULL, true );
	}

	public function setAccountVerificationLogId( $intAccountVerificationLogId ) {
		$this->m_intAccountVerificationLogId = $intAccountVerificationLogId;
	}

	public function setDebitPricing( $intDebitPricing ) {
		$this->m_intDebitPricing = $intDebitPricing;
	}

	public function setSummarisedPaymentTypeId( $intSummarisedPaymentTypeId ) {
		$this->m_intSummarisedPaymentTypeId = $intSummarisedPaymentTypeId;
	}

	public function setCheckAccountNumberBlindIndex( $strCheckAccountNumberBlindIndex ) {
		$this->m_strCheckAccountNumberBlindIndex = CStrings::strTrimDef( $strCheckAccountNumberBlindIndex, 240, NULL, true );
	}

	public function setPaymentGroupId( $strPaymentGroupId ) {
		$this->m_strPaymentGroupId = $strPaymentGroupId;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valId() {
		$boolIsValid = true;
		if( false == isset( $this->m_intId ) ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Ar Payment Request:  Id required - CArPayment::valId()', E_USER_ERROR );
		}
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		if( true == is_null( $this->m_intCid ) || 0 >= ( int ) $this->m_intCid ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Ar Payment Request:  Management Id required - CArPayment::valCid()', E_USER_ERROR );
		}
		return $boolIsValid;
	}

	public function valCompanyMerchantAccountId() {
		$boolIsValid = true;

		if( true == isset( $this->m_intCompanyMerchantAccountId ) && 0 >= ( int ) $this->m_intCompanyMerchantAccountId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Ar Payment Request:  Company Merchant Account Id required - CArPayment::valCompanyMerchantAccountId()', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function validateMerchantAccountForArPayment() {

		$boolIsValid = true;

		if( CPaymentType::CASH != $this->getPaymentTypeId() && CPaymentType::CHECK != $this->getPaymentTypeId() && CPaymentType::MONEY_ORDER != $this->getPaymentTypeId() ) {

			$intMerchantAccountId = $this->getCompanyMerchantAccountId();
			if( true == is_null( $intMerchantAccountId ) || '' == $intMerchantAccountId ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Merchant Account is Required.' ) );
				$boolIsValid = false;
			}
		}
		return $boolIsValid;
	}

	public function valAccountNickName() {
		$boolIsValid = true;

		if( true == valStr( $this->m_strAccountNickname ) && false == preg_match( "/^[a-z0-9\s\-.',&\/\\\]+$/i", $this->m_strAccountNickname ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_nickname', __( 'Account Nick Name not valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCheckProperAccountNickName() {
		$boolIsValid = true;

		if( true == valStr( $this->m_strAccountNickname ) && false == preg_match( '/^[\p{L}\s\-_`\',.0\-9]+$/ui', $this->m_strCheckNameOnAccount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_nickname', __( 'Account Nick Name not valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCheckNameOnAccount() {

		$boolIsValid = true;
		if( false == isset( $this->m_strCheckNameOnAccount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_name_on_account', __( "Account Holder's Name is required." ), 604 ) );
		} elseif( true == valStr( $this->m_strCheckNameOnAccount ) && false == preg_match( "/^[a-z0-9\s\-.',&\/\\\]+$/i", $this->m_strCheckNameOnAccount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_name_on_account', __( "Account Holder's Name not valid." ) ) );
		}
		return $boolIsValid;
	}

	public function valCheckProperNameOnAccount() {
		$boolIsValid = true;

		if( false == isset( $this->m_strCheckNameOnAccount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_name_on_account', __( 'Account Holder\'s Name is required.' ), 604 ) );
		} elseif( true == valStr( $this->m_strCheckNameOnAccount ) && false == preg_match( '/^[\p{L}\s\-_`\',.0-9]+$/ui', $this->m_strCheckNameOnAccount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_name_on_account', __( 'Account Holder\'s Name not valid.' ) ) );

		}
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		if( false == isset( $this->m_intPropertyId ) || 0 >= ( int ) $this->m_intPropertyId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_medium_id', 'A property is required.', NULL ) );
		}
		return $boolIsValid;
	}

	public function valIsInMigrationMode( $objClientDatabase ) {
		$boolIsValid = true;
		$objMigratedProperty = CPropertyGlSettings::fetchMigratedPropertyGlSettingByPropertyIdByCid( $this->m_intPropertyId, $this->m_intCid, $objClientDatabase );

		if( true == valObj( $objMigratedProperty, 'CPropertyGlSetting' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_type_id', 'Payment cannot be captured for this property because it is in migration mode.', NULL ) );
		}
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		if( false == is_null( $this->m_intCustomerId ) && 0 >= ( int ) $this->m_intCustomerId && 1 != $this->getIsFloating() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_medium_id', __( 'A resident is required.' ), NULL ) );
		}
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		if( false == isset( $this->m_intLeaseId ) && 0 >= ( int ) $this->m_intLeaseId && true != $this->m_boolIsFloating ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', __( 'A lease is required for Payment ID: {%d, 0, nots}', [ $this->getId() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valPaymentTypeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPaymentTypeId ) || 0 >= ( int ) $this->m_intPaymentTypeId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_type_id', __( 'A payment method is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valReversePaymentTypeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPaymentTypeId ) || false == in_array( $this->m_intPaymentTypeId, [ CPaymentType::ACH, CPaymentType::PAD, CPaymentType::MASTERCARD, CPaymentType::AMEX, CPaymentType::DISCOVER, CPaymentType::VISA, CPaymentType::EMONEY_ORDER, CPaymentType::SEPA_DIRECT_DEBIT ] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_type_id', 'A payment method is required.' ) );
		}

		return $boolIsValid;
	}

	public function valElectronicPaymentTypeId() {
		$boolIsValid = true;

		if( false == CPaymentTypes::isElectronicPayment( $this->m_intPaymentTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_type_id', 'Only electronic payments can be made through this medium.' ) );
		}

		return $boolIsValid;
	}

	public function valPaymentMediumId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPaymentMediumId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_medium_id', 'Payment medium id is required.', NULL ) );
		}

		return $boolIsValid;
	}

	public function valTerminalPaymentMediumId() {

		$boolIsValid = true;

		if( true == isset( $this->m_intPaymentMediumId ) && CPaymentMedium::TERMINAL != $this->m_intPaymentMediumId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_medium_id', 'Only terminal payment types are allowed through this medium.', NULL ) );
		}

		return $boolIsValid;
	}

	public function valPaymentDatetime() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strPaymentDatetime ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_datetime', __( 'Payment date is required.' ) ) );
		} elseif( false == preg_match( '/(((^[0-9]{1})|0[0-9]|1[0-9]|2[0-3]))/', $this->m_strPaymentDatetime ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_datetime', __( 'Payment date is not valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDeleteDatetime() {
		$boolIsValid = true;

		if( false == valStr( $this->m_strDeleteDatetime ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delete_datetime', 'Reverse Post Date is required. ' ) );
		} elseif( false == CValidation::isValidDate( $this->m_strDeleteDatetime ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delete_datetime', 'Reverse Post Date is not valid. ' ) );
		} elseif( strtotime( date( 'm/d/y', strtotime( $this->getReceiptDatetime() ) ) ) > strtotime( $this->getDeleteDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delete_post_date', 'Reverse Post Date must be greater than or equal to original deposit post date ' . date( 'm/d/Y', strtotime( $this->getReceiptDatetime() ) ) . '. ' ) );
		}

		return $boolIsValid;
	}

	public function valReceiptDatetime( $boolIsManualPayment = false ) {
		$boolIsValid = true;

		if( true == $boolIsManualPayment ) {
			$boolIsValid = $this->valManualPaymentReceiptDatetime();

			return $boolIsValid;
		}

		if( false == is_null( $this->m_strReceiptDatetime ) ) {

			if( false == preg_match( '/^(\d{1,2})\/(\d{1,2})\/(\d{4})/', $this->m_strReceiptDatetime, $arrstrMatches ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'receipt_datetime', 'Received date is not valid.' ) );

			} else {
				if( true == isset( $arrstrMatches[1] ) && true == isset( $arrstrMatches[2] ) && true == isset( $arrstrMatches[3] ) ) {
					$arrstrMatches[1] = \Psi\CStringService::singleton()->str_pad( $arrstrMatches[1], 2, '0', STR_PAD_LEFT );
					$arrstrMatches[2] = \Psi\CStringService::singleton()->str_pad( $arrstrMatches[2], 2, '0', STR_PAD_LEFT );

					if( false == checkdate( $arrstrMatches[1], $arrstrMatches[2], $arrstrMatches[3] ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'receipt_datetime', 'Received date is invalid.' ) );
					} else {
						// If Year is passed as 4 digits and its value if less than or equal to 100, mktime will have issues with it

						// Check if given date validates as valid timestamp.
						$intReceiptDateTimestamp = mktime( 0, 0, 0, $arrstrMatches[1], $arrstrMatches[2], $arrstrMatches[3] );

						if( 4 == strlen( $arrstrMatches[3] ) && 2012 > intval( $arrstrMatches[3] ) ) {
							$boolIsValid = false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'receipt_datetime', 'Received date must be greater than 01/01/2012.' ) );
						} elseif( 4 == strlen( $arrstrMatches[3] ) && 2012 == intval( $arrstrMatches[3] ) ) {
							$intTotalNumberOfDays = date( 't', $intReceiptDateTimestamp );
							if( ( 1 >= intval( $arrstrMatches[1] ) || 12 < intval( $arrstrMatches[1] ) ) && ( 1 >= intval( $arrstrMatches[3] ) || $intTotalNumberOfDays < intval( $arrstrMatches[3] ) ) ) {
								$boolIsValid = false;
								$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'receipt_datetime', 'Received date must be greater than 01/01/2012.' ) );
							}
						}

						// For invalid parameters
						// On Windows: it returns -1 or false
						// On others : Negative timestamp
						if( false == $intReceiptDateTimestamp || 0 > $intReceiptDateTimestamp ) {
							$boolIsValid = false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'receipt_datetime', 'Received date is invalid.' ) );
						}
					}
				}
			}
		}

		return $boolIsValid;
	}

	// @TODO: Will optimize and remove duplicate function code once we start to use ISO date format (yyyy-mm-dd) everywhere in the system

	public function valManualPaymentReceiptDatetime() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strReceiptDatetime ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'receipt_datetime', 'Please enter valid receipt date.' ) );
			return $boolIsValid;
		}

		if( false == is_null( $this->m_strReceiptDatetime ) ) {

			if( false == preg_match( '/^(\d{4})[\/|\-](\d{1,2})[\/|\-](\d{1,2})$/', $this->m_strReceiptDatetime, $arrstrMatches ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'receipt_datetime', 'Received date is not valid.' ) );

			} else {
				if( true == isset( $arrstrMatches[2] ) && true == isset( $arrstrMatches[3] ) && true == isset( $arrstrMatches[1] ) ) {
					$arrstrMatches[2] = \Psi\CStringService::singleton()->str_pad( $arrstrMatches[2], 2, '0', STR_PAD_LEFT );
					$arrstrMatches[3] = \Psi\CStringService::singleton()->str_pad( $arrstrMatches[3], 2, '0', STR_PAD_LEFT );

					if( false == checkdate( $arrstrMatches[2], $arrstrMatches[3], $arrstrMatches[1] ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'receipt_datetime', 'Received date is invalid.' ) );
					} else {
						// If Year is passed as 4 digits and its value if less than or equal to 100, mktime will have issues with it

						// Check if given date validates as valid timestamp.
						$intReceiptDateTimestamp = mktime( 0, 0, 0, $arrstrMatches[2], $arrstrMatches[3], $arrstrMatches[1] );

						if( 4 == strlen( $arrstrMatches[1] ) && 2012 > intval( $arrstrMatches[1] ) ) {
							$boolIsValid = false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'receipt_datetime', 'Received date must be greater than 01/01/2012.' ) );
						} elseif( 4 == strlen( $arrstrMatches[1] ) && 2012 == intval( $arrstrMatches[1] ) ) {
							$intTotalNumberOfDays = date( 't', $intReceiptDateTimestamp );
							if( ( 1 >= intval( $arrstrMatches[2] ) || 12 < intval( $arrstrMatches[2] ) ) && ( 1 >= intval( $arrstrMatches[1] ) || $intTotalNumberOfDays < intval( $arrstrMatches[1] ) ) ) {
								$boolIsValid = false;
								$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'receipt_datetime', 'Received date must be greater than 01/01/2012.' ) );
							}
						}

						// For invalid parameters
						// On Windows: it returns -1 or false
						// On others : Negative timestamp
						if( false == $intReceiptDateTimestamp || 0 > $intReceiptDateTimestamp ) {
							$boolIsValid = false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'receipt_datetime', 'Received date is invalid.' ) );
						}
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function valTransactionDatetime() {
		$boolIsValid = true;
		if( true == valStr( $this->m_strTransactionDatetime ) ) {
			$strTransactionDatetime = $this->m_strTransactionDatetime;

			if( true == preg_match( '/^(\d{4})[\/|\-](\d{2})[\/|\-](\d{2})$/', $strTransactionDatetime ) ) {
				$strTransactionDatetime = date( 'm/d/Y', strtotime( $strTransactionDatetime ) );
			}
			$mixValidatedDate = CValidation::checkDateFormat( $strTransactionDatetime, $boolFormat = true );

			if( false === $mixValidatedDate ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_datetime', __( 'Receipt date is not valid.' ) ) );
			} else {
				$this->setTransactionDatetime( $mixValidatedDate );
			}
		} else {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_datetime', __( 'Receipt date is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPostMonth() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strPostMonth ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month is required.' ) ) );

		} elseif( false == preg_match( '#^(((0?[1-9])|(1[012]))(/0?1)/([12]\d)?\d\d)$#', $this->m_strPostMonth ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month of format mm/yyyy is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDeletePostMonth() {
		$boolIsValid = true;

		if( false == valStr( $this->m_strDeletePostMonth ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delete_post_month', __( 'Reverse Post Month is required. ' ) ) );

		} elseif( false == preg_match( '#^(((0?[1-9])|(1[012]))(/0?1)/([12]\d)?\d\d)$#', $this->m_strDeletePostMonth ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delete_post_month', 'Reverse Post Month of format mm/yyyy is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valPastResidentOverPayment( $arrobjPropertyPreferences, $intPaymentAmount, $objClientDatabase, $boolIsFromEntrata = false ) {

		if( false == valArr( $arrobjPropertyPreferences ) || false == valObj( $this->m_objLease, 'CLease' ) ) return true;

		if( CLeaseStatusType::PAST == $this->m_objLease->getLeaseStatusTypeId() && true == array_key_exists( 'PREVENT_PAST_RESIDENT_OVERPAYMENT', $arrobjPropertyPreferences ) ) {

			$objLeaseProcess 			= \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchLeaseProcessByLeaseIdByCid( $this->m_objLease->getId(), $this->m_objLease->getCid(), $objClientDatabase );
			$objArTransactionsFilter 	= new CArTransactionsFilter();
			$boolIsResidentPortal = ( true == $boolIsFromEntrata ) ? false : true;
			$objArTransactionsFilter->setDefaults( $boolIsResidentPortal );
			$this->m_objLease->fetchBalance( $objArTransactionsFilter, $objClientDatabase, true, $boolIsResidentPortal );

			$fltTotalBalance = ( true == valStr( $objLeaseProcess->getFmoProcessedOn() ) ) ? $this->m_objLease->getBalance() + $this->m_objLease->getWriteOffBalance(): $this->m_objLease->getBalance();

			if( $fltTotalBalance < $intPaymentAmount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'past resident', 'Overpayments are not allowed at this time. You may not pay more than your current balance: $' . $fltTotalBalance ) );
				return false;
			}
		}

		return true;
	}

	public function valArPostMonth( $objPropertyGlSetting, $intUserValidationMode = CPropertyGlSetting::VALIDATION_MODE_STRICT ) {

		$boolIsValid = true;

		// @TODO: If date is not valid, should we return early with proper validation message?
		if( true == valStr( $this->getPostMonth() && true == CValidation::validateDate( $this->getPostMonth() ) ) ) {
			$strPostMonth = date( 'm/1/Y', strtotime( $this->getPostMonth() ) );
		} else {
			$strPostMonth = '';
		}

		if( false == $this->m_boolValidatePastOrFuturePostMonth ) return $boolIsValid;

		if( true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) && CPropertyGlSetting::VALIDATION_MODE_STRICT == $intUserValidationMode ) {
			$strLockPeriod = date( 'm/1/Y', strtotime( $objPropertyGlSetting->getArLockMonth() ) );
			if( true == valStr( $strPostMonth ) && strtotime( $strLockPeriod ) >= strtotime( $strPostMonth ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Warning: Period locked. You cannot post a transaction on or before ' . date( 'm/Y', strtotime( $strLockPeriod ) ) . '.' ) );
				$boolIsValid &= false;
			}
		}

		$intPastPostMonth 	= strtotime( $objPropertyGlSetting->getArPostMonth() );
		$intFuturePostMonth = $intPastPostMonth;

		if( false == is_null( $this->getPastPostMonth() ) && false == is_null( $this->getFuturePostMonth() ) ) {
			$intPastPostMonth 	= strtotime( date( 'm/d/Y', $intPastPostMonth ) . ' - ' . $this->m_intPastPostMonth . ' month' );
			$intFuturePostMonth = strtotime( date( 'm/d/Y', $intFuturePostMonth ) . ' + ' . $this->m_intFuturePostMonth . ' month' );
		}

		if( strtotime( $strPostMonth ) < $intPastPostMonth || strtotime( $strPostMonth ) > $intFuturePostMonth ) {
			if( $intPastPostMonth != $intFuturePostMonth ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month should be between {%t, past_post_month, DATE_NUMERIC_POSTMONTH} and {%t, future_post_month, DATE_NUMERIC_POSTMONTH} for this transaction.', [ 'past_post_month' => $intPastPostMonth,  'future_post_month' => $intFuturePostMonth ] ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month should be {%t, 0, DATE_NUMERIC_POSTMONTH} for this transaction.', [ $intFuturePostMonth ] ) ) );
			}
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valPaymentAmount( $boolRequireConfirmationAmount = false, $boolValNegativePaymentAmount = true ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_fltPaymentAmount ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', __( 'Payments amount is required.' ) ) );
		} elseif( true == $boolValNegativePaymentAmount && false == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintNonElectronicPaymentTypeIds ) && 0 >= ( float ) $this->m_fltPaymentAmount ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', __( 'Payment amount can not be negative or zero.' ) ) );
		}

		if( true == $boolIsValid && true == $boolRequireConfirmationAmount ) {
			if( $this->m_fltPaymentAmount != $this->m_fltConfirmPaymentAmount ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', __( 'Confirmation amount does not match the payment amount.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valChargebackPaymentAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->m_fltPaymentAmount ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', __( 'Payments amount is required.' ) ) );
		} elseif( false == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintNonElectronicPaymentTypeIds ) && 0 == ( float ) $this->m_fltPaymentAmount ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', __( 'Payment amount can not be zero.' ) ) );
		}

		return $boolIsValid;
	}

	public function valReversePaymentAmount( $objOriginalArPayment ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_fltPaymentAmount ) || 0 <= ( float ) $this->m_fltPaymentAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', __( 'Payment reversals require a valid payment amount.' ) ) );
		}

		if( $objOriginalArPayment->getPaymentAmount() < ( abs( $objOriginalArPayment->getReversedPaymentAmount() ) + abs( $this->getPaymentAmount() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', __( 'Total payment reversal amount can not exceed the original payment amount.' ) ) );
		}

		return $boolIsValid;
	}

	public function valModifiedPaymentAmount( $objDatabase, $objPaymentDatabase ) {
		$boolIsValid = true;

		$objOriginalArPayment = CArPayments::fetchArPaymentByIdByCid( $this->getId(), $this->getCid(), $objPaymentDatabase );

		if( false == valObj( $objOriginalArPayment, 'CArPayment' ) ) {
			trigger_error( __( 'Customer payment failed to load.' ), E_USER_ERROR );
			exit;
		}

		if( $objOriginalArPayment->getPaymentAmount() == $this->getAdjustingPaymentAmount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', __( 'Original payment amount cannot be the same as the new payment amount.' ) ) );
		}

		if( true == is_null( $this->m_fltPaymentAmount ) || 0 >= ( float ) $this->m_fltPaymentAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', __( 'Payment amount is required.' ) ) );
		}

		$objMerchantAccount = $this->getOrFetchMerchantAccount( $objDatabase );

		if( false == valObj( $objMerchantAccount, 'CMerchantAccount' ) ) {
			trigger_error( __( 'Merchant account failed to load.' ), E_USER_ERROR );
			exit;
		}

		if( $this->getAdjustingPaymentAmount() > $objMerchantAccount->getCh21MaxPaymentAmount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', __( 'Payment amount exceeds maximum amount allowed on the associated merchant account.' ) ) );
		}

		return $boolIsValid;
	}

	public function valConvenienceFeeAmount() {
		$boolIsValid = true;

		if( false == isset( $this->m_fltConvenienceFeeAmount ) || 0 > ( int ) $this->m_fltConvenienceFeeAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'convenience_fee_amount', __( 'Invalid Ar Payment Request: Convenience fee amount must be positive.' ) ) );
		}

		if( true == isset( $this->m_intPaymentTypeId ) && $this->m_intPaymentTypeId == CPaymentType::CHECK_21 && 0 < $this->m_fltConvenienceFeeAmount && $this->m_intPaymentMediumId == CPaymentMedium::TERMINAL ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', __( 'Configuration Error: Check 21 transactions cannot carry a convenience fee.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDonationAmount() {
		$boolIsValid = true;

		if( 0 >= $this->m_fltDonationAmount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Donation amount is required.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valCompanyCharity() {
		$boolIsValid = true;

		if( 0 >= $this->m_intCompanyCharityId ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Charity is required.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function validatePaymentCost() {
		$boolIsValid = true;

		if( false == is_numeric( $this->m_fltPaymentCost ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_cost', __( 'Payment cost is required.' ), NULL ) );
		}

		return $boolIsValid;
	}

	public function validateNsfChargeAmount() {
		$boolIsValid = true;

		if( 0 > $this->m_floatNsfChargeAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'nsf_charge_amount', __( 'Zero or a positive nsf charge amount is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valBilltoIpAddress() {
		$boolIsValid = true;

		if( false == isset( $this->m_strBilltoIpAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_ip_address', __( 'Billto ip address is required.' ), NULL ) );
		}

		return $boolIsValid;
	}

	public function valCompanyChargeAmount() {
		$boolIsValid = true;

		if( false == isset( $this->m_fltCompanyChargeAmount ) ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Ar Payment Request:  Company Charge Amount required - CArPayment::valCompanyChargeAmount()', E_USER_ERROR );
		}
		return $boolIsValid;
	}

	public function valBilltoNameFirst() {
		$boolIsValid = true;

		if( ( true == is_null( $this->m_strBilltoCompanyName ) || 0 == strlen( trim( $this->m_strBilltoCompanyName ) ) ) && ( true == is_null( $this->m_strBilltoNameFirst ) || 0 == strlen( trim( $this->m_strBilltoNameFirst ) ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_name_first', __( 'First name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valBilltoNameLast() {
		$boolIsValid = true;
		if( ( true == is_null( $this->m_strBilltoCompanyName ) || 0 == strlen( trim( $this->m_strBilltoCompanyName ) ) ) && ( true == is_null( $this->m_strBilltoNameLast ) || 0 == strlen( trim( $this->m_strBilltoNameLast ) ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_name_last', __( 'Last name is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valBilltoUnitNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strBilltoUnitNumber ) || 0 == strlen( trim( $this->m_strBilltoUnitNumber ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_unit_number', __( 'Unit Number is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valReturnPaymentStatusTypeId( $objPaymentDatabase ) {
		$boolIsValid = true;

		$objOriginalArPayment = CArPayments::fetchArPaymentByIdByCid( $this->m_intId, $this->getCid(), $objPaymentDatabase );

		if( false == valObj( $objOriginalArPayment, 'CArPayment' ) ) {
			trigger_error( 'original payment failed to load.', E_USER_ERROR );
		}

		$arrintAcceptablePaymentStatusTypeIds = [ CPaymentStatusType::CAPTURED, CPaymentStatusType::CAPTURING, CPaymentStatusType::RECEIVED ];

		if( false == isset( $this->m_intPaymentStatusTypeId ) || false == in_array( $objOriginalArPayment->getPaymentStatusTypeId(), $arrintAcceptablePaymentStatusTypeIds ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_status_type_id', __( 'You can only return a payment that is of status type captured, capturing, or received.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPaymentStatusTypeId( $strAction = NULL ) {
		$boolIsValid = true;

		if( false == isset( $this->m_intPaymentStatusTypeId ) || 0 >= ( int ) $this->m_intPaymentStatusTypeId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Ar Payment Request:  Payment Status Type Id required - CArPayment::valPaymentStatusTypeId()', E_USER_ERROR );

		} else {
			switch( $strAction ) {
				case 'void':
					if( CPaymentStatusType::VOIDED != $this->m_intPaymentStatusTypeId ) {
						trigger_error( 'Invalid payment status type for voiding payment.', E_USER_ERROR );
						$boolIsValid = false;
					}
					break;

				case 'post_return':
					if( CPaymentStatusType::RECALL != $this->m_intPaymentStatusTypeId ) {
						trigger_error( 'Invalid payment status type for posting payment as return.', E_USER_ERROR );
						$boolIsValid = false;
					}
					break;

				default:
			}
		}

		return $boolIsValid;
	}

	public function valBilltoStreetLine1() {
		$boolIsValid = true;
		if( false == isset( $this->m_strBilltoStreetLine1 ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_street_line1', __( 'Billing street is required.' ), 612 ) );
		} elseif( false == mb_detect_encoding( $this->m_strBilltoStreetLine1, 'ASCII', 'true' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_street_line1', __( 'Only standard address characters are allowed.(Note- copy/pasting sometimes introduces special characters that are not allowed. Please type the address carefully into the field.)' ), 612 ) );
		} elseif( 'ASCII' == mb_detect_encoding( $this->m_strBilltoStreetLine1, 'ASCII', 'true' ) && true == isset( $this->m_strBilltoStreetLine2 ) && false == mb_detect_encoding( $this->m_strBilltoStreetLine2, 'ASCII', 'true' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_street_line2', __( 'Only standard address characters are allowed.(Note- copy/pasting sometimes introduces special characters that are not allowed. Please type the address carefully into the field.)' ), 612 ) );
		}
		return $boolIsValid;
	}

	public function valBilltoCity() {
		$boolIsValid = true;
		if( false == isset( $this->m_strBilltoCity ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_city', __( 'Billing city is required.' ), 613 ) );
		}
		return $boolIsValid;
	}

	public function valBilltoStateCode( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( false == isset( $this->m_strBilltoStateCode ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_state_code', __( 'Billing state/province code is required.' ), 614 ) );

		} else {

			if( true == isset( $objDatabase ) ) {
				// This query is completely unnecessary DJB
				$arrobjStates = \Psi\Eos\Admin\CStates::createService()->fetchAllStatesKeyedByCode( $objDatabase );

				if( false == array_key_exists( $this->m_strBilltoStateCode, $arrobjStates ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_state_code', __( 'Billing state/province code is not valid.' ), 614 ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valBilltoPostalCode() {
		$boolIsValid = true;
		if( false == isset( $this->m_strBilltoPostalCode ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_postal_code', 'Billing zip/postal code is required.', 615 ) );

		} elseif( false == CApplicationUtils::validateZipCode( $this->m_strBilltoPostalCode, NULL ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_postal_code',  ' Billing address zip code must be 5 to 10 characters.', NULL ) );
		} elseif( false == CValidation::validateMilitaryPostalCode( $this->m_strBilltoPostalCode, $this->m_strBilltoStateCode ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_postal_code',  'Billing military state zip code must start with 0 or 9.', NULL ) );
		}
		return $boolIsValid;
	}

	public function valBilltoCountryCode( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( false == is_null( $this->m_strBilltoCountryCode ) && true == isset( $objDatabase ) ) {
			$arrobjCountries = \Psi\Eos\Entrata\CCountries::createService()->fetchAllCountriesKeyedByCode( $objDatabase );

			if( false == array_key_exists( $this->m_strBilltoCountryCode, $arrobjCountries ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_country_code', __( 'Billing country code is not valid.' ), 614 ) );
			}
		}

		return $boolIsValid;
	}

	public function valBilltoPhoneNumber( $boolRequirePhoneNumber = false ) {
		$boolIsValid = true;

		$intLength = ( false == is_null( $this->m_strBilltoPhoneNumber ) && 0 < strlen( $this->m_strBilltoPhoneNumber ) ) ? strlen( $this->m_strBilltoPhoneNumber ) : 0;

		if( 0 == $intLength ) return $boolIsValid;

		if( 10 > $intLength || 15 < $intLength ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_phone_number', 'Billing phone number must be between 10 and 15 characters.', 611 ) );
		} elseif( true == isset( $this->m_strBilltoPhoneNumber ) && false == ( CValidation::validateFullPhoneNumber( $this->m_strBilltoPhoneNumber, false ) ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_phone_number', 'Valid billing phone number is required.', 504 ) );
			$boolIsValid = false;
		}

		if( true == $boolRequirePhoneNumber && false == isset( $this->m_strBilltoPhoneNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_phone_number', 'Phone number is required.', 611 ) );
		}

		return $boolIsValid;
	}

	public function valBilltoEmailAddress( $boolRequireEmailAddress = false ) {
		$boolIsValid = true;

		if( true == $boolRequireEmailAddress && true == is_null( $this->m_strBilltoEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_email_address', __( 'Email address is required.' ), 400 ) );

		}

		if( true == isset( $this->m_strBilltoEmailAddress ) && 1 != CValidation::validateEmailAddresses( $this->m_strBilltoEmailAddress, false ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_email_address', __( 'Email address is invalid.' ), 400 ) );
		}

		return $boolIsValid;
	}

	public function valCcCardNumber() {

		$boolIsValid = true;

		if( 0 < \Psi\Libraries\UtilFunctions\count( $this->getErrorMsgs() ) ) {
			return true;
		}

		$strCardNumber = $this->getCcCardNumber();

		if( true == is_null( $strCardNumber ) || 0 == strlen( $strCardNumber ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_card_number', __( 'Credit card number is required.' ), 606 ) );
			return false;
		}

		return $boolIsValid;
	}

	public function valPciSecureReferenceNumber() {
		$boolIsValid = true;
		$boolValidAuthToken = true;
		$boolValidReferenceNumber = true;

		if( false == valStr( $this->m_strAuthToken ) || ( true == valStr( $this->m_strAuthToken ) && 32 != strlen( $this->m_strAuthToken ) ) ) {
			$boolValidAuthToken = false;
		}

		if( true == is_null( $this->m_intSecureReferenceNumber ) || ( false == is_null( $this->m_intSecureReferenceNumber ) && false == valId( $this->m_intSecureReferenceNumber ) ) ) {
			$boolValidReferenceNumber = false;
		}

		if( false == $boolValidAuthToken && false == $boolValidReferenceNumber ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_card_number', __( 'Credit card number is required.' ), 621 ) );
			$strMessage = 'REF NUMBER: ' . $this->m_intSecureReferenceNumber . "\n";
			$strMessage .= 'AUTH TOKEN: ' . $this->m_strAuthToken . "\n";
			$strMessage .= 'PROPERTY_ID: ' . $this->m_intPropertyId . "\n";
			$strMessage .= 'CID: ' . $this->m_intCid . "\n";
			$strMessage .= 'PAYMENT_DATE_TIME: ' . $this->m_strPaymentDatetime . "\n";
			$strMessage .= 'FIRST_NAME: ' . $this->m_strBilltoNameFirst . "\n";
			$strMessage .= 'LAST_NAME: ' . $this->m_strBilltoNameLast . "\n";
			$strMessage .= 'APPLICANT_APPLICATION_ID: ' . $this->m_intApplicantApplicationId . "\n";
			$strMessage .= 'USER AGENT: ' . $_SERVER['HTTP_USER_AGENT'] . "\n";
			$objException = new Exception();
			$strMessage .= 'BACKTRACE: ' . $objException->getTraceAsString() . "\n\n";
			$objPaymentEmailer = new CPaymentEmailer();
			$objPaymentEmailer->emailEmergencyTeam( $strMessage, 'Credit card number is required', [ CSystemEmail::RESIDENT_PORTAL_ALERT_EMAIL_ADDRESS ] );
		}

		return $boolIsValid;
	}

	public function valSecureReferenceNumber() {
		$boolIsValid = true;

		if( 0 < \Psi\Libraries\UtilFunctions\count( $this->getErrorMsgs() ) ) {
			return true;
		}

		if( false == is_numeric( $this->m_intSecureReferenceNumber ) ) {
			$boolIsValid = false;
			if( false == is_null( $this->getSecureReferenceNumberError() ) && '' != trim( $this->getSecureReferenceNumberError() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_card_number', $this->getSecureReferenceNumberError(), 621 ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_card_number', __( 'Error in communicating processing gateway.' ), 621 ) );
			}
		}

		return $boolIsValid;
	}

	public function valCcExpDateMonth() {
		$boolIsValid = true;

		if( false == isset( $this->m_strCcExpDateMonth ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_date_month', __( 'Expiration month is required.' ), 607 ) );
		}

		if( true == isset( $this->m_strCcExpDateMonth ) && true == isset( $this->m_strCcExpDateYear ) && $this->m_strCcExpDateYear == date( 'Y' ) && $this->m_strCcExpDateMonth < date( 'm' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_date_month', __( 'Expiration date is invalid or expired.' ), 607 ) );
		}

		return $boolIsValid;
	}

	public function valCcExpDateYear() {
		$boolIsValid = true;

		if( false == isset( $this->m_strCcExpDateYear ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_date_year', __( 'Expiration year is required.' ), 608 ) );
		}

		if( true == isset( $this->m_strCcExpDateYear ) && $this->m_strCcExpDateYear < date( 'Y' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_exp_date_month', __( 'Expiration year is invalid or expired.' ), 607 ) );
		}

		return $boolIsValid;
	}

	public function valCcNameOnCard() {
		$boolIsValid = true;
		if( false == isset( $this->m_strCcNameOnCard ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cc_name_on_card', __( 'Name on the credit card is required.' ), 609 ) );
		}
		return $boolIsValid;
	}

	public function valCvvCode() {
		$boolIsValid = true;
		if( false == isset( $this->m_strCvvCode ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cvv_code', 'Card verification value (CVV/CCV) is required.', 609 ) );
			return false;
		}

		if( CPaymentType::AMEX == $this->getPaymentTypeId() && 4 <> strlen( $this->getCvvCode() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Card verification value (CVV/CCV) must be 4 digits long for American Express cards.' ) );
			$boolIsValid &= false;
		} elseif( CPaymentType::AMEX <> $this->getPaymentTypeId() && 3 <> strlen( $this->getCvvCode() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Card verification value (CVV/CCV) must be 3 digits long for Visa, Discover, and Mastercard cards.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valCustomerPaymentAccountId() {
		$boolIsValid = true;
		if( false == isset( $this->m_intCustomerPaymentAccountId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_payment_account_id', __( 'Please select account.' ) ) );
		}
		return $boolIsValid;
	}

	public function valCheckNumber( $boolIsCheckNumberMandatory = false ) {
		$boolIsValid = true;

		if( true == valStr( $this->m_strCheckNumber ) && false == preg_match( '/^([A-Za-z0-9\-]+$)/', $this->m_strCheckNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_or_money_order_number', __( 'Check / Money Order Number can be numeric or alphanumeric only.' ) ) );
		}

		if( true == is_null( $this->m_strCheckNumber ) || ( true == $boolIsCheckNumberMandatory && false == valStr( $this->m_strCheckNumber ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_or_money_order_number', __( 'Check / Money Order Number is required.' ), 622 ) );
		} elseif( 40 < strlen( $this->m_strCheckNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_or_money_order_number', __( 'Check / Money Order Number cannot exceed 40 characters.' ) ) );
		}
		return $boolIsValid;
	}

	public function valCheckNumberForCheck21( $boolIsCheckNumberMandatory = false ) {
		$boolIsValid = true;

		if( true == valStr( $this->m_strCheckNumber ) && false == preg_match( '/^([A-Za-z0-9\-]+$)/', $this->m_strCheckNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_or_money_order_number', __( 'Check / Money Order Number can be numeric or alphanumeric only.' ) ) );
		}

		if( true == is_null( $this->m_strCheckNumber ) || ( true == $boolIsCheckNumberMandatory && false == valStr( $this->m_strCheckNumber ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_or_money_order_number', __( 'Check / Money Order Number is required.' ), 622 ) );
		} elseif( 20 < strlen( $this->m_strCheckNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_or_money_order_number', __( 'Check / Money Order Number cannot exceed 20 characters.' ) ) );
		}
		return $boolIsValid;
	}

	public function valCheckDate() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strCheckDate ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_date', __( 'Check date cannot be empty.' ) ) );
			$boolIsValid &= false;
			return $boolIsValid;
		}

		if( false == CValidation::validateDate( $this->getCheckDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_date', __( 'Check date is not a valid date.' ) ) );
			$boolIsValid &= false;
		}

		if( strtotime( $this->getCheckDate() ) > strtotime( date( 'm/d/Y' ) ) && false == is_null( $this->getCheckDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_date', __( 'Post dated checks are not permitted.  Wait to process check until after check date.  Checks with inaccurate dates will be voided.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCheckBankName() {
		$boolIsValid = true;
		if( false == isset( $this->m_strCheckBankName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_bank_name', __( 'Bank name is required.' ), 601 ) );
		}
		return $boolIsValid;
	}

	public function valCheckAccountTypeId() {
		$boolIsValid = true;
		if( false == is_numeric( $this->m_intCheckAccountTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_type_id', __( 'Bank account type is required.' ), 603 ) );
		}
		return $boolIsValid;
	}

	public function valCheckRoutingNumber( $objClientDatabase = NULL ) {

		$boolIsValid = true;

		if( true == is_null( $this->getCheckRoutingNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', __( 'Routing number is required.' ), 605 ) );
			return false;
		}

		if( 0 !== preg_match( '/[^0-9]/', $this->getCheckRoutingNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', __( 'Routing number must be a number.' ), 622 ) );
			return false;
		}

		if( CPaymentType::PAD === $this->getPaymentTypeId() ) {
			// Skip remaining checks for Canadian PAD
			return true;
		}

		if( 9 != strlen( $this->getCheckRoutingNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', __( 'Routing number must be 9 digits long.' ), 623 ) );
			return false;
		}

		if( false == is_null( $objClientDatabase ) && CPaymentType::CHECK_21 != $this->getPaymentTypeId() && 1 != $this->getCheckIsMoneyOrder() && false == $this->getIsCheck21Adjustment() ) {
			// If it is 9 numeric characters see if it is a fed ach participant
			$objFedAchParticipant = \Psi\Eos\Payment\CFedAchParticipants::createService()->fetchFedAchParticipantByRoutingNumber( $this->getCheckRoutingNumber(), $objClientDatabase );

			if( false == valObj( $objFedAchParticipant, 'CFedAchParticipant' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', __( 'Routing number is not valid.' ), 624 ) );

			} elseif( true == valObj( $objFedAchParticipant, 'CFedAchParticipant' ) && true == is_null( $this->getCheckBankName() ) ) {
				$this->setCheckBankName( $objFedAchParticipant->getCustomerName() );
			}
		}

		return $boolIsValid;
	}

	public function valCheckAccountAndRoutingNumber( $boolSendEmail = false ) {
		$boolIsValid = true;
		if( CPaymentType::ACH == $this->m_intPaymentTypeId && ( true == is_null( $this->getCheckAccountNumber() ) || true == is_null( $this->getCheckRoutingNumber() ) ) ) {

			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Check Account Number or Routing Number is NULL' ) ) );

			if( true == $boolSendEmail ) {

				$strMessage = 'Check Account Number or Routing Number is NULL For Payment Id : ' . $this->getId();
				trigger_error( 'ArPayment :: ' . $strMessage, E_USER_WARNING );

				$arrstrEmailAddresses = [ CSystemEmail::XENTO_RPARDESHI_EMAIL_ADDRESS, CSystemEmail::AREID_EMAIL_ADDRESS, CSystemEmail::RJENSEN_EMAIL_ADDRESS ];
				// Try to send an email to the Emergency team
				$objPaymentEmailer = new CPaymentEmailer();
				$strSubject = 'ArPayment :: Check Account Number / Routing Number is NULL';

				$objPaymentEmailer->emailEmergencyTeam( $strMessage, $strSubject, $arrstrEmailAddresses );
			}
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valCheckAccountNumberEncrypted() {
		$boolIsValid = true;

		if( true == is_null( $this->getCheckAccountNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number', __( 'Account number is required.' ), 602 ) );
		}

		$intMinLength = ( $this->getPaymentTypeId() == CPaymentType::PAD ) ? 3 : 4;
		$intMaxLength = ( $this->getPaymentTypeId() == CPaymentType::PAD ) ? 12 : 17;

		if( $intMinLength > strlen( $this->getCheckAccountNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number', __( 'Account number should be minimum {%d, 0, nots} digits long.', $intMinLength ), 623 ) );
		}

		if( $intMaxLength < strlen( $this->getCheckAccountNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number', __( 'Account number should not be more than {%d, 0, nots} digits long.', $intMaxLength ), 623 ) );
		}

		return $boolIsValid;
	}

	public function valCheckAccountNumberEncryptedForCheck21() {
		$boolIsValid = true;

		if( true == is_null( $this->getCheckAccountNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number', __( 'Account number is required.' ), 602 ) );
		}

		if( 4 > strlen( $this->getCheckAccountNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number', __( 'Account number should be minimum 4 digits long.' ), 623 ) );
		}

		if( 20 < strlen( $this->getCheckAccountNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number', __( 'Account number should not be more than 20 digits long.' ), 623 ) );
		}

		return $boolIsValid;
	}

	public function valCheckBlacklist( $objClientDatabase ) {
		$boolIsValid = true;

		$objPaymentBlacklistEntry = new CPaymentBlacklistEntry();
		$objBlacklistedEntry = $objPaymentBlacklistEntry->checkBlacklistedBankAccount( $objClientDatabase, $this->getCheckRoutingNumber(), $this->getCheckAccountNumber(), $this->getCheckAccountTypeId(), $this->m_strCheckNameOnAccount );

		if( NULL == $objBlacklistedEntry ) {
			return true;
		}

		switch( $objBlacklistedEntry->getPaymentBlacklistTypeId() ) {
			case CPaymentBlacklistType::NONE:
				return true;

			case CPaymentBlacklistType::ROUTING_NUMBER:
				// TODO: this is for debugging on production, when ready, remove this line and uncomment code lines below
				// @mail( CSystemEmail::RJENSEN_EMAIL_ADDRESS, 'Payment Blacklist Validation', 'Found blacklist entry: ' . $objBlacklistedEntry->getId() . ', ar payment id: ' . $this->getId() . ', client id: ' . $this->getCid() . ', customer id: ' . $this->getCustomerId(), 'from: ' . CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );

				// swap out the routing number
				$this->setCheckRoutingNumber( $objBlacklistedEntry->getLookupStringCorrectedDecrypted() );

				// notify the user that we swapped the routing number
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, $objBlacklistedEntry->getBlacklistReason() ) );
				$boolIsValid = false;
				break;

			default:
				// TODO: this is for debugging on production, when ready, remove this line and uncomment code lines below
				// @mail( CSystemEmail::RJENSEN_EMAIL_ADDRESS, 'Payment Blacklist Validation', 'Found blacklist entry: ' . $objBlacklistedEntry->getId() . ', ar payment id: ' . $this->getId() . ', client id: ' . $this->getCid() . ', customer id: ' . $this->getCustomerId(), 'from: ' . CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );

				$this->addErrorMsg( new CErrorMsg( NULL, NULL, $objBlacklistedEntry->getBlacklistReason() ) );
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function validateCheck21Duplicity( $objPaymentDatabase ) {

		$boolIsValid = true;

		$objDuplicateArPayment = CArPayments::fetchDuplicateTerminalArPayment( $this, $objPaymentDatabase );

		if( true == valObj( $objDuplicateArPayment, 'CArPayment' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'A duplicate transaction request was detected ( {%d, 0, nots} ). It is probable that this payment was already processed successfully.', [ $objDuplicateArPayment->getId() ] ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valNsfPaymentStatusTypeId( $objPaymentDatabase ) {
		$boolIsValid = true;

		$objOriginalArPayment = CArPayments::fetchArPaymentByIdByCid( $this->m_intId, $this->getCid(), $objPaymentDatabase );

		if( false == valObj( $objOriginalArPayment, 'CArPayment' ) ) {
			trigger_error( 'original payment failed to load.', E_USER_ERROR );
		}

		$arrintAcceptablePaymentStatusTypeIds = [ CPaymentStatusType::CAPTURED, CPaymentStatusType::CAPTURING, CPaymentStatusType::RECEIVED ];

		if( false == isset( $this->m_intPaymentStatusTypeId ) || false == in_array( $objOriginalArPayment->getPaymentStatusTypeId(), $arrintAcceptablePaymentStatusTypeIds ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_status_type_id', __( 'You can only nsf a payment that is of status type captured, capturing, or received.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCheckAccountNumber() {
		return $this->valCheckAccountNumberEncrypted();
	}

	public function valCheckAccountNumberForCheck21() {
		return $this->valCheckAccountNumberEncryptedForCheck21();
	}

	public function valConfirmCheckAccountNumber() {
		$boolIsValid = true;

		$strCheckAccountNumber = $this->getCheckAccountNumber();

		if( 0 != strlen( $strCheckAccountNumber ) && 0 == strlen( $this->m_strConfirmCheckAccountNumber ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'confrim_check_account_number', __( 'Confirm account number is required.' ), 602 ) );
			$boolIsValid = false;
		} elseif( 0 != strlen( $strCheckAccountNumber ) && $strCheckAccountNumber != $this->m_strConfirmCheckAccountNumber ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'verify_check_account_number', __( 'Confirm account number and account number do not match.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valAgreesToTerms() {
		$boolIsValid = true;

		if( false == $this->m_boolAgreesToTerms ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Agreeing to terms is required.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valAgreesToReversePayment() {
		$boolIsValid = true;

		if( false == $this->m_boolAgreesToReversePayment ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Agreeing to Reverse payment is required.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valAgreesToPayConvenienceFees() {
		$boolIsValid = true;

		if( 0 < $this->m_fltConvenienceFeeAmount && false == $this->m_boolAgreesToPayConvenienceFees ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Agreeing to pay convenience fee is required.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valAgreesToPaymentTerms() {
		$boolIsValid = true;

		if( true == $this->isElectronicPayment() ) {
			$boolIsValid &= $this->valAgreesToPayConvenienceFees();
			$boolIsValid &= $this->valAgreesToTerms();
		}

		return $boolIsValid;
	}

	public function valPromiseToPay() {
		$boolIsValid = true;

		if( false == $this->m_boolPromiseToPay ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'promise_to_pay', __( 'Agreeing to promise to pay is required.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valReverseBatchedOn( $objOriginalArPayment ) {
		$boolIsValid = true;

		if( true == is_null( $objOriginalArPayment->getBatchedOn() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Payment has not yet been batched.  Please void instead of reversing.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function validateForeignKeys( $boolRequireLeaseId = true ) {
		$boolIsValid = true;

		$boolIsValid &= $this->valCid();
		$boolIsValid &= $this->valCompanyMerchantAccountId();
		$boolIsValid &= $this->valPropertyId();
		$boolIsValid &= $this->valCustomerId();
		if( true == $boolRequireLeaseId && false == $this->getIsFloating() ) {
			$boolIsValid &= $this->valLeaseId();
		}

		return $boolIsValid;
	}

    public function validatePaymentSettings( $objClientDatabase, $boolIsPaymentFromRpPremium = false, $arrfltTotalPaymentAmount = [], $boolIsNegativeBalance = false, $arrobjPropertyPreferences = NULL, $objPaymentDatabase = NULL, $boolIsAmenityReservationPayment = false, $boolIsReservationHub = false ) {

		if( false == valArr( $arrobjPropertyPreferences ) ) {
			$arrobjPropertyPreferences = CPropertyPreferences::fetchPropertyPreferencesByPropertyIdsByKeysByCid( [ $this->m_intPropertyId ], [ 'CURRENT_RESIDENT_MUST_PAY_BALANCE_IN_FULL', 'PARTIAL_PAYMENT_OVERRIDES_DIFFERENT_LAST_NAMES_EXIST', 'PARTIAL_PAYMENT_ALLOWANCE_OVERRIDE', 'SHOW_RESIDENT_BALANCE', 'BALANCE_PAYMENT_REQUIREMENT', 'LATE_PAYMENT_ACCEPTANCE_THRESHOLD', 'DISALLOW_LOGIN_FROM_PAST_RESIDENTS', 'DISALLOW_LAST_LEASE_MONTH_PAYMENTS', 'AR_PAYMENTS_DAYS', 'RENT_LATE_DAY', 'DISALLOW_LATE_ONLINE_PAYMENTS', 'REQUIRE_RECURRING_CHARGE_MULTIPLE_ON_PREPAYMENTS', 'PREPAYMENT_MONTH_LIMIT', 'PREPAYMENT_MONTH_LIMIT_FOR_FUTURE', 'RENT_CYCLE_BEGIN_DAY', 'ALLOW_LATE_CREDIT_CARD_PAYMENTS', 'BLOCK_ONLINE_PAYMENTS_FOR_LEASE_STATUS', 'HIDE_RESIDENT_BALANCE_FOR_PAST_STATUS', 'MAX_RPORTAL_PAYMENTS_PER_DAY', 'PREVENT_PAST_RESIDENT_OVERPAYMENT', 'PROMISE_TO_PAY', 'SHOW_CREDIT_BALANCES', 'WAIVE_PAYMENT_FROM_BEGIN_TILL_LATE_DAY' ], $this->getCid(), $objClientDatabase );
		}

		$arrobjPropertyPreferences	= ( true == valArr( $arrobjPropertyPreferences ) ) ? rekeyObjects( 'Key', $arrobjPropertyPreferences ) :[];
		$objProperty 				= $this->getOrFetchProperty( $objClientDatabase );
		$objPropertyPhoneNumber 	= ( true == valObj( $objProperty, 'CProperty' ) ) ? $objProperty->getOrFetchPrimaryPropertyPhoneNumber( $objClientDatabase ): NULL;
		$strPhoneNumberAddition 	= ( true == valObj( $objPropertyPhoneNumber, 'CPropertyPhoneNumber' ) && false == is_null( $objPropertyPhoneNumber->getPhoneNumber() ) ) ? ' at ' . $objPropertyPhoneNumber->getPhoneNumber() : '';

		// get timezone specific timestamp
		$intTimestamp = ( true == valObj( $objProperty, 'CProperty' ) ) ? $objProperty->getTimezoneSpecificTimestamp( date( 'm/d/Y H:i:s' ), $objClientDatabase ): NULL;
		if( false == is_numeric( $intTimestamp ) ) {
			$intTimestamp = time();
		}

		$objLeaseCustomer	= $this->getOrFetchLeaseCustomer( $objClientDatabase );
		$objCustomer		= $this->getOrFetchCustomer( $objClientDatabase );
		$objLease			= $this->getOrFetchLease( $objClientDatabase );

		if( COccupancyType::COMMERCIAL == $objLease->getOccupancyTypeId() ) {
			$arrstrResetPropertySettingsKey  = [
				'MAX_RPORTAL_PAYMENTS_PER_DAY',
				'RENT_CYCLE_BEGIN_DAY',
				'BALANCE_PAYMENT_REQUIREMENT',
				'WAIVE_PAYMENT_FROM_BEGIN_TILL_LATE_DAY',
				'PARTIAL_PAYMENT_ALLOWANCE_OVERRIDE',
				'CURRENT_RESIDENT_MUST_PAY_BALANCE_IN_FULL',
				'HIDE_RESIDENT_BALANCE_FOR_PAST_STATUS',
				'PROMISE_TO_PAY',
				'AR_PAYMENTS_DAYS',
				'BLOCK_ONLINE_PAYMENTS_FOR_LEASE_STATUS',
				'DISALLOW_LAST_LEASE_MONTH_PAYMENTS',
				'ALLOW_LATE_CREDIT_CARD_PAYMENTS',
				'RENT_LATE_DAY',
				'DISALLOW_LATE_ONLINE_PAYMENTS',
				'LATE_PAYMENT_ACCEPTANCE_THRESHOLD',
				'PREPAYMENT_MONTH_LIMIT',
				'PREPAYMENT_MONTH_LIMIT_FOR_FUTURE',
				'REQUIRE_RECURRING_CHARGE_MULTIPLE_ON_PREPAYMENTS',
				'PREPAYMENT_MONTH_LIMIT'
			];
			$arrobjPropertyPreferences = $objProperty->getDefaultPropertySettings( $arrobjPropertyPreferences, $arrstrResetPropertySettingsKey );
		}

		$boolLeaseHasMultipleMerchantAccounts = ( true == valObj( $objLease->getProperty(), 'CProperty' ) && 1 < \Psi\Libraries\UtilFunctions\count( $objLease->getProperty()->getMerchantAccounts() ) ) ? true : false;

		if( true == valObj( $objLease, 'CLease' ) && false == valObj( $objLease->getPropertyGlSetting(), 'CPropertyGlSetting' ) && true == valObj( $this->getPropertyGlSetting(), 'CPropertyGlSetting' ) ) {
			$objLease->setPropertyGlSetting( $this->getPropertyGlSetting() );
		}

		$fltPaymentAmount = ( true == valArr( $arrfltTotalPaymentAmount ) && true == valObj( $objLease, 'CLease' ) ) ? $arrfltTotalPaymentAmount[$objLease->getId()] : $this->getPaymentAmount();

		if( 1 < \Psi\Libraries\UtilFunctions\count( $arrfltTotalPaymentAmount ) ) {
			$fltPaymentAmount = array_sum( $arrfltTotalPaymentAmount );
		}

		if( true == $boolIsPaymentFromRpPremium && true == valObj( $objLease, 'CLease' ) ) {
			$intCountOfPayments = CArPayments::fetchCountOfPaymentsByCustomerIdByPropertyIdByCid( $this->getCustomerId(), $this->getPropertyId(), $objLease->getId(), $this->getCid(), $objClientDatabase );

			if( true == isset( $intCountOfPayments ) ) {
				$intAllowedPaymentPerDayimit = 0;

				if( true == array_key_exists( 'MAX_RPORTAL_PAYMENTS_PER_DAY', $arrobjPropertyPreferences ) && true == valObj( $arrobjPropertyPreferences['MAX_RPORTAL_PAYMENTS_PER_DAY'], 'CPropertyPreference' ) && false == is_null( $arrobjPropertyPreferences['MAX_RPORTAL_PAYMENTS_PER_DAY']->getValue() ) ) {
					$intAllowedPaymentPerDayimit = $arrobjPropertyPreferences['MAX_RPORTAL_PAYMENTS_PER_DAY']->getValue();
				}

				$strUnitNumber = '';
				if( '' != $objLease->getUnitNumberCache() ) {
					$strUnitNumber = ' for Unit-' . $objLease->getUnitNumberCache();
				}

				if( 0 != $intAllowedPaymentPerDayimit && $intAllowedPaymentPerDayimit <= $intCountOfPayments ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'You have made {%d, 0} payment(s) within the last {%d, 1} hours {%s, 2}. Additional payments cannot be accepted at this time. Please contact your property office.', [ $intCountOfPayments, 24, $strUnitNumber ] ) ) );
					return false;
				}
			}
		}

		$boolIsRentLate = false;
		$intPaymentReactivationDay = ( true == array_key_exists( 'RENT_CYCLE_BEGIN_DAY', $arrobjPropertyPreferences ) && true == valObj( $arrobjPropertyPreferences['RENT_CYCLE_BEGIN_DAY'], 'CPropertyPreference' ) && true == is_numeric( $arrobjPropertyPreferences['RENT_CYCLE_BEGIN_DAY']->getValue() ) ) ? ( int ) $arrobjPropertyPreferences['RENT_CYCLE_BEGIN_DAY']->getValue() : 25;
		$intRentLateDay = ( int ) checkPreferenceIsSet( $arrobjPropertyPreferences, 'RENT_LATE_DAY', NULL, true );

		if( true == valObj( $objLease, 'CLease' ) && 0 < $objLease->getBalance() && 0 < $intRentLateDay ) {
			$boolIsCheckRentCycleBeginDayIsGreater = ( $intRentLateDay <= $intPaymentReactivationDay && date( 'j', $intTimestamp ) >= $intRentLateDay && date( 'j', $intTimestamp ) < $intPaymentReactivationDay );
			$boolIsCheckRentCycleBeginDayIsLess = ( $intRentLateDay >= $intPaymentReactivationDay && ( date( 'j', $intTimestamp ) >= $intRentLateDay || date( 'j', $intTimestamp ) < $intPaymentReactivationDay ) );

			if( true == $boolIsCheckRentCycleBeginDayIsGreater || true == $boolIsCheckRentCycleBeginDayIsLess ) {
				$boolIsRentLate = true;
			}
		}

		$boolPartialPaymentOverride = false;
		$fltMinimumAmoundResidentShouldPay = NULL;
		if( 'POST' === $_SERVER['REQUEST_METHOD'] ) {
			if( true == $this->getIsMultiLease() && true == valArr( $this->getTotalLeaseBalancesByLeaseId() ) ) {
				$arrfltMinimumLeaseBalances = $this->getTotalLeaseBalancesByLeaseId();

				foreach( $arrfltMinimumLeaseBalances as $fltLeaseBalance ) {
					$fltMinimumAmoundResidentShouldPay += $fltLeaseBalance;
				}
			} else {
				$fltMinimumAmoundResidentShouldPay = isset( $_POST['minimum_amount_to_pay'] ) ? $_POST['minimum_amount_to_pay'] : $objCustomer->getMiniminumPaymentAmount();
			}
		}

		if( true == valObj( $objCustomer, 'CCustomer' ) && true == valObj( $objLease, 'CLease' ) ) {
			$objCustomer->calculateMinimumPaymentAmount( $objLease, $objClientDatabase, $objLeaseCustomer, $arrobjPropertyPreferences, NULL, $boolIsPaymentFromRpPremium );
		}

		$boolIsBalancePaymentRequirment = ( true == isset( $arrobjPropertyPreferences['BALANCE_PAYMENT_REQUIREMENT'] ) && 0 < $arrobjPropertyPreferences['BALANCE_PAYMENT_REQUIREMENT']->getValue() ) ? true : false;
		$boolIsWaivePaymentFromBeginTillLateDay = ( true == $boolIsBalancePaymentRequirment && true == isset( $arrobjPropertyPreferences['WAIVE_PAYMENT_FROM_BEGIN_TILL_LATE_DAY'] ) && 1 == $arrobjPropertyPreferences['WAIVE_PAYMENT_FROM_BEGIN_TILL_LATE_DAY']->getValue() )? true : false;

		if( true == $boolIsPaymentFromRpPremium
			&& true == valObj( $objLease, 'CLease' )
			&& CListType::EVICTION == $objLease->getTerminationListTypeId()
			&& true == $boolIsWaivePaymentFromBeginTillLateDay
			&& 1 == bccomp( $objCustomer->getMiniminumPaymentAmount(), $fltPaymentAmount, 2 ) ) {
			$this->addErrorMsg( new CErrorMsg( '', 'payment_amount', __( 'You are required to pay rent in full. Contact the property office for more information.' ) ) );
			return false;
		}

		$boolRentLatePayInFull = false;
		if( true == $boolIsWaivePaymentFromBeginTillLateDay && true == $boolIsPaymentFromRpPremium && true == $boolIsRentLate && 1 == bccomp( $objCustomer->getMiniminumPaymentAmount(), $fltPaymentAmount, 2 ) ) {
			$boolRentLatePayInFull = true;
		}

		$intPartialPaymentAllowanceOverride = ( true == isset( $arrobjPropertyPreferences['PARTIAL_PAYMENT_ALLOWANCE_OVERRIDE'] ) && 0 < $arrobjPropertyPreferences['PARTIAL_PAYMENT_ALLOWANCE_OVERRIDE']->getValue() ) ? $arrobjPropertyPreferences['PARTIAL_PAYMENT_ALLOWANCE_OVERRIDE']->getValue() : 0;

		if( true == valObj( $objCustomer, 'CCustomer' )
			&& true == $boolIsBalancePaymentRequirment
			&& 1 == bccomp( $objCustomer->getMiniminumPaymentAmount(), $fltPaymentAmount, 2 )
			&& false == $boolIsWaivePaymentFromBeginTillLateDay
			&& ( false == isset( $arrobjPropertyPreferences['PARTIAL_PAYMENT_ALLOWANCE_OVERRIDE'] ) || 1 == $intPartialPaymentAllowanceOverride || 2 == $intPartialPaymentAllowanceOverride ) ) {
			$boolPartialPaymentOverride = true;

		}

		if( false == $boolIsPaymentFromRpPremium ) {
			if( true == valObj( $objCustomer, 'CCustomer' )
				&& true == $boolIsBalancePaymentRequirment
				&& 1 == bccomp( $objCustomer->getMiniminumPaymentAmount(), $fltPaymentAmount, 2 ) ) {
				$boolPartialPaymentOverride = true;

			}
		}

		// Waive payments for Applicant, Future and Past
		$boolIsWaivePaymentDisabled = true;

		if( true == valObj( $objCustomer, 'CCustomer' )
			&& true == valObj( $objLeaseCustomer, 'CLeaseCustomer' )
			&& true == isset( $arrobjPropertyPreferences['BALANCE_PAYMENT_REQUIREMENT'] )
			&& 0 < $arrobjPropertyPreferences['BALANCE_PAYMENT_REQUIREMENT']->getValue()
			&& true == isset( $arrobjPropertyPreferences['CURRENT_RESIDENT_MUST_PAY_BALANCE_IN_FULL'] )
			&& 0 < $arrobjPropertyPreferences['CURRENT_RESIDENT_MUST_PAY_BALANCE_IN_FULL']->getValue()
			&& ( CLeaseStatusType::APPLICANT == $objLeaseCustomer->getLeaseStatusTypeId() || CLeaseStatusType::FUTURE == $objLeaseCustomer->getLeaseStatusTypeId() || CLeaseStatusType::PAST == $objLeaseCustomer->getLeaseStatusTypeId() ) ) {
			$boolIsWaivePaymentDisabled = false;
		}

		$fltFormattedLeaseBalance = ( 0 > $objCustomer->getMiniminumPaymentAmount() ) ? ( -1 * $objCustomer->getMiniminumPaymentAmount() ) : $objCustomer->getMiniminumPaymentAmount();

		// MINIMUM PAYMENT AMOUNT REQUIREMENT
		if( 'POST' === $_SERVER['REQUEST_METHOD'] && false == $boolIsAmenityReservationPayment ) {
			// These should only be checked on POST when resident is submitting a payment amount
			if( $fltPaymentAmount < $fltFormattedLeaseBalance && ( true == $boolRentLatePayInFull || true == $boolPartialPaymentOverride ) && true == $boolIsWaivePaymentDisabled ) {
				// Resident Must Pay Balance in Full
				if( true == isset( $arrobjPropertyPreferences['SHOW_RESIDENT_BALANCE'] )
					&& true == valObj( $arrobjPropertyPreferences['SHOW_RESIDENT_BALANCE'], 'CPropertyPreference' )
					&& 0 < $arrobjPropertyPreferences['SHOW_RESIDENT_BALANCE']->getValue() ) {

					$strFormattedMinimumPaymentAmount = ', which is ' . $objCustomer->getFormattedMiniminumPaymentAmount();

					if( true == isset( $arrobjPropertyPreferences['HIDE_RESIDENT_BALANCE_FOR_PAST_STATUS'] ) && ( $objLease->getLeaseStatusTypeId() == CLeaseStatusType::PAST ) ) {
						$strFormattedMinimumPaymentAmount = ' ';
					}
					if( true == isset( $arrobjPropertyPreferences['BALANCE_PAYMENT_REQUIREMENT'] ) && true == valObj( $arrobjPropertyPreferences['BALANCE_PAYMENT_REQUIREMENT'], 'CPropertyPreference' ) && 1 == $arrobjPropertyPreferences['BALANCE_PAYMENT_REQUIREMENT']->getValue() ) {
						// Resident Must Pay Balance in Full
						$this->addErrorMsg( new CErrorMsg( '', 'payment_amount', __( 'You are required to pay the full charges due' ) . $strFormattedMinimumPaymentAmount . __( '. Please check the amount you have entered.' ) ) );
					} elseif( true == isset( $arrobjPropertyPreferences['BALANCE_PAYMENT_REQUIREMENT'] )
						&& true == valObj( $arrobjPropertyPreferences['BALANCE_PAYMENT_REQUIREMENT'], 'CPropertyPreference' )
						&& 3 == $arrobjPropertyPreferences['BALANCE_PAYMENT_REQUIREMENT']->getValue() ) {
						$this->addErrorMsg( new CErrorMsg( '', 'payment_amount', __( 'You are required to pay the full balance due' ) . $strFormattedMinimumPaymentAmount . __( '. Please check the amount you have entered.' ) ) );
					} else {
						// Resident Must Pay Rent Balance in Full
						$this->addErrorMsg( new CErrorMsg( '', 'payment_amount', __( 'You are required to pay the full rent balance due' ) . $strFormattedMinimumPaymentAmount . __( '. Please check the amount you have entered.' ) ) );
					}

				} else {

					if( true == isset( $arrobjPropertyPreferences['BALANCE_PAYMENT_REQUIREMENT'] )
						&& true == valObj( $arrobjPropertyPreferences['BALANCE_PAYMENT_REQUIREMENT'], 'CPropertyPreference' )
						&& 1 == $arrobjPropertyPreferences['BALANCE_PAYMENT_REQUIREMENT']->getValue() ) {
						// Resident Must Pay Balance in Full
						$this->addErrorMsg( new CErrorMsg( '', 'payment_amount', __( 'You are required to pay the full amount due on your balance. Contact the property office for more information.' ) ) );
					} else {
						// Resident Must Pay Rent Balance in Full
						$this->addErrorMsg( new CErrorMsg( '', 'payment_amount', __( 'You are required to pay rent in full. Contact the property office for more information.' ) ) );
					}
				}

				return false;
			}

			if( false == $boolLeaseHasMultipleMerchantAccounts && true == array_key_exists( 'PROMISE_TO_PAY', $arrobjPropertyPreferences ) && true == valObj( $objLease, 'CLease' ) && 1 == bccomp( $objLease->getBalance(), $fltPaymentAmount, 2 ) && false == $this->validate( 'validate_promise_to_pay' ) ) {
				return false;
			}
		}

		// AR_PAYMENTS_DAYS
		if( true == array_key_exists( 'AR_PAYMENTS_DAYS', $arrobjPropertyPreferences ) && true == valObj( $arrobjPropertyPreferences['AR_PAYMENTS_DAYS'], 'CPropertyPreference' ) && false == is_null( $arrobjPropertyPreferences['AR_PAYMENTS_DAYS']->getValue() ) ) {

			$arrintArPaymentDays	= explode( ',', $arrobjPropertyPreferences['AR_PAYMENTS_DAYS']->getValue() );

			if( true == valArr( $arrintArPaymentDays ) ) {
				foreach( $arrintArPaymentDays as $intKey => $intArPaymentDay ) {
					if( false == is_numeric( $intArPaymentDay ) || $intArPaymentDay > 31 || $intArPaymentDay < 1 ) {
						unset( $arrintArPaymentDays[$intKey] );
					}
				}
			}

			$intBlockOnlinePaymentForLeaseStatus = ( true == array_key_exists( 'BLOCK_ONLINE_PAYMENTS_FOR_LEASE_STATUS', $arrobjPropertyPreferences ) ) ? ( int ) $arrobjPropertyPreferences['BLOCK_ONLINE_PAYMENTS_FOR_LEASE_STATUS']->getValue() : NULL;
			$arrintArPaymentDays = ( true == isset( $arrintArPaymentDays ) && true == valArr( $arrintArPaymentDays ) ) ? array_unique( $arrintArPaymentDays ) : [];
			$boolAllowArPaymentDays = true;

			if( true == valObj( $objLeaseCustomer, 'CLeaseCustomer' ) ) {

				switch( ( int ) $intBlockOnlinePaymentForLeaseStatus ) {

					case 3:
						if( true == in_array( $objLeaseCustomer->getLeaseStatusTypeId(), [ CLeaseStatusType::CURRENT ] ) && false == in_array( date( 'j', $intTimestamp ), $arrintArPaymentDays ) ) {
							$boolAllowArPaymentDays = false;
						}
						break;

					case NULL:
					case 1:
						if( true == in_array( $objLeaseCustomer->getLeaseStatusTypeId(), [ CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE ] ) && false == in_array( date( 'j', $intTimestamp ), $arrintArPaymentDays ) ) {
							$boolAllowArPaymentDays = false;
						}
						break;

					case 2:
						if( false == in_array( date( 'j', $intTimestamp ), $arrintArPaymentDays ) ) {
							$boolAllowArPaymentDays = false;
						}
						break;

					default:
				}
			}

			if( false == $boolAllowArPaymentDays ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Online payments are available only on these days ' ) . ' : ' . implode( ',', $arrintArPaymentDays ) . '.' ) );
				return false;
			}
		}

		// DISALLOW_LAST_LEASE_MONTH_PAYMENTS
		$strLeaseEndDate = NULL;

		if( true == valObj( $objLeaseCustomer, 'CLeaseCustomer' ) && true == in_array( $objLeaseCustomer->getLeaseStatusTypeId(), [ CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE ] ) && true == valObj( $objLease, 'CLease' ) && true == valObj( $arrobjPropertyPreferences['DISALLOW_LAST_LEASE_MONTH_PAYMENTS'], 'CPropertyPreference' ) && 1 == $arrobjPropertyPreferences['DISALLOW_LAST_LEASE_MONTH_PAYMENTS']->getValue() ) {
			$strLeaseEndDate = ( CLeaseIntervalType::MONTH_TO_MONTH == $objLease->getLeaseIntervalTypeId() && NULL == $objLease->getMoveOutDate() ) ? NULL : ( false == is_null( $objLease->getMoveOutDate() ) ? $objLease->getMoveOutDate() : $objLease->getLeaseEndDate() );
		}

		if( NULL != $strLeaseEndDate && 0 < strlen( $strLeaseEndDate ) && ( date( 'Y' ) > \Psi\CStringService::singleton()->substr( $strLeaseEndDate, 6, 4 ) || ( date( 'Y' ) == \Psi\CStringService::singleton()->substr( $strLeaseEndDate, 6, 4 ) && date( 'm' ) >= \Psi\CStringService::singleton()->substr( $strLeaseEndDate, 0, 2 ) ) ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'You cannot make online payments at this time because your lease ends in the current month. Please contact the leasing office for assistance' ) . $strPhoneNumberAddition . '.' ) );
			return false;
		}

		// Here we will check to see if the lease on the current payment has a dont_accept_payments setting = 1
		if( false == $boolIsReservationHub && true == valObj( $objLease, 'CLease' ) && CPaymentAllowanceType::BLOCK_ALL == $objLease->getPaymentAllowanceTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Payments are not available at this time.  Please remit payment directly to the leasing office.' ) ) );
			return false;
		}

		$objCustomer = $this->getOrFetchCustomer( $objClientDatabase );

		// Here we will check to see if the customer on the current payment has a dont_accept_payments setting = 1
		if( true == valObj( $objCustomer, 'CCustomer' ) && CPaymentAllowanceType::BLOCK_ALL == $objCustomer->getPaymentAllowanceTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Payments are not available at this time.  Please remit payment directly to the leasing office.' ) ) );
			return false;
		}

		// RENT_LATE_DAY & DISALLOW_LATE_ONLINE_PAYMENTS
		// RENT_CYCLE_BEGIN_DAY
		// ALLOW_LATE_CREDIT_CARD_PAYMENTS

		$arrintRestrictedStatus = [ CLeaseStatusType::PAST, CLeaseStatusType::FUTURE, CLeaseStatusType::APPLICANT ];

		$boolIsMoveInCurrentMonth = \Psi\Eos\Entrata\CLeaseProcesses::createService()->checkMoveInDateAsCurrentDate( $objLease->getId(), $objLease->getCid(), $objClientDatabase );
		if( true == $boolIsMoveInCurrentMonth ) {
			$arrintRestrictedStatus[] = CLeaseStatusType::CURRENT;
		}
		if( true == valObj( $objLeaseCustomer, 'CLeaseCustomer' ) && false == $this->checkIsAllFutureDateArTransactions()
			&& false == in_array( $objLeaseCustomer->getLeaseStatusTypeId(), $arrintRestrictedStatus )
			&& ( false == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes )
				|| false == array_key_exists( 'ALLOW_LATE_CREDIT_CARD_PAYMENTS', $arrobjPropertyPreferences )
				|| false == valObj( $arrobjPropertyPreferences['ALLOW_LATE_CREDIT_CARD_PAYMENTS'], 'CPropertyPreference' )
				|| 1 != $arrobjPropertyPreferences['ALLOW_LATE_CREDIT_CARD_PAYMENTS']->getValue() ) ) {

			if( true == valObj( $objLease, 'CLease' ) && 0 < $objLease->getBalance() && false == $this->checkIsAllFutureDateArTransactions() && true == array_key_exists( 'RENT_LATE_DAY', $arrobjPropertyPreferences )
				&& true == array_key_exists( 'DISALLOW_LATE_ONLINE_PAYMENTS', $arrobjPropertyPreferences )
				&& 1 == $arrobjPropertyPreferences['DISALLOW_LATE_ONLINE_PAYMENTS']->getValue()
				&& ( ( $arrobjPropertyPreferences['RENT_LATE_DAY']->getValue() <= $intPaymentReactivationDay
						&& date( 'j', $intTimestamp ) >= $arrobjPropertyPreferences['RENT_LATE_DAY']->getValue()
						&& date( 'j', $intTimestamp ) < $intPaymentReactivationDay )
					|| ( $arrobjPropertyPreferences['RENT_LATE_DAY']->getValue() >= $intPaymentReactivationDay
						&& ( date( 'j', $intTimestamp ) >= $arrobjPropertyPreferences['RENT_LATE_DAY']->getValue() || date( 'j', $intTimestamp ) < $intPaymentReactivationDay ) ) ) ) {

				// Entering here means that this is a late payment.  We have a new setting, LATE_PAYMENT_ACCEPTANCE_THRESHOLD, that will allow payments under a certain amount.
				// If this value is set and the payment amount is less than this setting, don't reject validation.

				if( false == isset( $arrobjPropertyPreferences['LATE_PAYMENT_ACCEPTANCE_THRESHOLD'] )
					|| false == valObj( $arrobjPropertyPreferences['LATE_PAYMENT_ACCEPTANCE_THRESHOLD'], 'CPropertyPreference' )
					|| $fltPaymentAmount > ( float ) $arrobjPropertyPreferences['LATE_PAYMENT_ACCEPTANCE_THRESHOLD']->getValue() ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Payments made against overdue balances are not allowed online.  Please remit payment directly to the leasing office or contact the leasing office' ) . $strPhoneNumberAddition . '.' ) );

					return false;
				}
			}
		}

		if( true == valObj( $objLease, 'CLease' ) ) {
			// Calculate the balance of the current lease
			$objLease->fetchScheduledChargeTotal( $objClientDatabase, CArTrigger::$c_arrintRecurringArTriggers );
		}

		if( true == valObj( $objLeaseCustomer, 'CLeaseCustomer' ) && false == in_array( $objLeaseCustomer->getLeaseStatusTypeId(), [ CLeaseStatusType::APPLICANT ] ) ) {

			// PREPAYMENT_MONTH_LIMIT
			$arrintRestrictedStatus = [ CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE ];

			$boolIsRepaymentAgreement = false;

			if( true == valObj( $objLease, 'CLease' ) && true == $objLease->getHasRepaymentAgreement() ) {
				$boolIsRepaymentAgreement = true;
			}

			if( true == $boolIsRepaymentAgreement && CLeaseStatusType::PAST == $objLease->getLeaseStatusTypeId() ) {
				array_push( $arrintRestrictedStatus,  CLeaseStatusType::PAST );
			}

			if( true == isset( $arrobjPropertyPreferences['PREPAYMENT_MONTH_LIMIT'] ) && true == in_array( $objLeaseCustomer->getLeaseStatusTypeId(), $arrintRestrictedStatus ) ) {
				$intPrepaylimit = $arrobjPropertyPreferences['PREPAYMENT_MONTH_LIMIT']->getValue();
			}

			if( true == isset( $arrobjPropertyPreferences['PREPAYMENT_MONTH_LIMIT_FOR_FUTURE'] ) && $objLeaseCustomer->getLeaseStatusTypeId() == CLeaseStatusType::FUTURE ) {
				$intPrepaylimit = $arrobjPropertyPreferences['PREPAYMENT_MONTH_LIMIT_FOR_FUTURE']->getValue();
			}

			array_push( $arrintRestrictedStatus,  CLeaseStatusType::FUTURE );

			$fltPaymentAmountOfPrepayment = ( true == isset( $arrfltTotalPaymentAmount[$objLease->getId()] ) ) ? $arrfltTotalPaymentAmount[$objLease->getId()] : $fltPaymentAmount;

			// Return error if prepayments are not allowed.
			if( true == valObj( $objLease, 'CLease' ) && 1 == bccomp( $fltPaymentAmountOfPrepayment, $objLease->getBalance(), 2 )
				&& true == isset( $intPrepaylimit )
				&& 0 == ( int ) $intPrepaylimit
				&& true == in_array( $objLeaseCustomer->getLeaseStatusTypeId(), $arrintRestrictedStatus ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, \Psi\CStringService::singleton()->htmlspecialchars( __( 'Pre-payments are not allowed at this property.  Please wait until the next months\' billing has been performed.' ), ENT_QUOTES, 'UTF-8', false ) ) );
				return false;
			}

			if( true == valObj( $objLease, 'CLease' ) && 1 == bccomp( $fltPaymentAmountOfPrepayment, $objLease->getBalance(), 2 )
				&& true == isset( $intPrepaylimit )
				&& ( 0 < ( int ) $intPrepaylimit
					&& round( $fltPaymentAmountOfPrepayment, 2 ) > round( ( $objLease->getBalance() + ( ( int ) $intPrepaylimit * ( float ) $objLease->getScheduledChargeTotal() ) ), 2 )
					&& true == in_array( $objLeaseCustomer->getLeaseStatusTypeId(), $arrintRestrictedStatus ) ) ) {

				// If credit already is at or in excess of pre-paid limit, show this error.
				if( 0 > ( $objLease->getBalance() + ( ( int ) $intPrepaylimit * ( float ) $objLease->getScheduledChargeTotal() ) ) ) {
					$strMessage = __( 'You can only pre-pay up to{%s,0}', [ ' ' ] ) . ( string ) $intPrepaylimit . ' ' . __( ' months\' charges.  Your maximum pre-payment amount has already been reached.  Please try back at a later time.' );

				} elseif( 0 > $objLease->getBalance() ) {
					$strMessage = __( 'You can only pre-pay up to{%s,0}', [ ' ' ] ) . ( string ) $intPrepaylimit . ' ' . __( ' months\' charges.  This means your balance, after your payment, must not exceed $' ) . __( '{%f,0}', [ number_format( ( float ) ( $intPrepaylimit * $objLease->getScheduledChargeTotal() ), 2 ) ] ) . __( '.  You already show a credit balance of $' ) . __( '{%f,0}', [ number_format( ( float ) $objLease->getBalance(), 2 ) ] ) . __( ', so your maximum possible payment is $' ) . __( '{%f,0}', [ number_format( ( float ) ( $objLease->getBalance() + ( ( int ) $intPrepaylimit * ( float ) $objLease->getScheduledChargeTotal() ) ), 2 ) ] ) . '.';

				} elseif( 0 == $objLease->getBalance() ) {
					$strMessage = __( 'You can only pre-pay up to{%s,0}', [ ' ' ] ) . ( string ) $intPrepaylimit . ' ' . __( ' months\' charges.  Your monthly charges total $' ) . __( '{%f,0}', [ number_format( ( float ) $objLease->getScheduledChargeTotal(), 2 ) ] ) . __( ' so your maximum payment must not exceed $' ) . __( '{%f,0}', [ number_format( ( float ) ( $intPrepaylimit * $objLease->getScheduledChargeTotal() ), 2 ) ] ) . '.';

				} else {
					$strMessage = __( 'You can only pre-pay up to{%s,0}', [ ' ' ] ) . ( string ) $intPrepaylimit . ' ' . __( ' months\' charges.  Your monthly charges total $' ) . __( '{%f,0}', [ number_format( ( float ) $objLease->getScheduledChargeTotal(), 2 ) ] ) . __( ', and your current balance is $' ) . __( '{%f,0}', [ number_format( ( float ) $objLease->getBalance(), 2 ) ] ) . __( ', so your maximum payment must not exceed $' ) . __( '{%f,0}', [ number_format( ( float ) ( $objLease->getBalance() + ( ( int ) $intPrepaylimit * ( float ) $objLease->getScheduledChargeTotal() ) ), 2 ) ] ) . '.';
				}

				$this->addErrorMsg( new CErrorMsg( NULL, NULL, \Psi\CStringService::singleton()->htmlspecialchars( $strMessage, ENT_QUOTES, 'UTF-8', false ) ) );
				return false;
			}

			// REQUIRE_RECURRING_CHARGE_MULTIPLE_ON_PREPAYMENTS

			if( true == valObj( $objLease, 'CLease' ) ) {
				$fltBalance = $objLease->getBalance();
				if( 0 > $fltBalance && true == $boolIsNegativeBalance ) {
					$fltBalance = - 1 * $fltBalance;
				}

				if( 0 < $objLease->getScheduledChargeTotal() && $fltPaymentAmount > $fltBalance
					&& true == array_key_exists( 'REQUIRE_RECURRING_CHARGE_MULTIPLE_ON_PREPAYMENTS', $arrobjPropertyPreferences )
					&& 1 == $arrobjPropertyPreferences['REQUIRE_RECURRING_CHARGE_MULTIPLE_ON_PREPAYMENTS']->getValue()
					&& ( 0 != ( ( $fltPaymentAmount - $objLease->getBalance() ) % $objLease->getScheduledChargeTotal() ) )
					&& true == in_array( $objLeaseCustomer->getLeaseStatusTypeId(), $arrintRestrictedStatus ) ) {

					// Figure out the smallest pre-payment amount that can be paid within the given restraints.
					$fltBalanceMultiple = $objLease->getBalance();

					while( round( $fltBalanceMultiple ) <= 0 ) {
						$fltBalanceMultiple += $objLease->getScheduledChargeTotal();
					}

					$intCurrentCount = 1;

					$arrstrValueInclude = [];
					array_push( $arrstrValueInclude, '$' . __( '{%f,0}', [ number_format( ( float ) $fltBalanceMultiple, 2 ) ] ) );

					while( $intCurrentCount < 13 && ( false == isset( $arrobjPropertyPreferences['PREPAYMENT_MONTH_LIMIT'] ) || ( $intCurrentCount ) <= ( int ) $intPrepaylimit ) ) {
						$intCurrentCount++;
						$fltBalanceMultiple += $objLease->getScheduledChargeTotal();

						// This if statement will break out of the while loop if the amount is invalid.
						if( true == isset( $arrobjPropertyPreferences['PREPAYMENT_MONTH_LIMIT'] ) && ( $fltBalanceMultiple - $objLease->getBalance() ) > ( $intPrepaylimit * $objLease->getScheduledChargeTotal() ) ) {
							break;
						}

						// Handle output of payment amount for PREPAYMENT_MONTH_LIMIT preference
						if( true == isset( $arrobjPropertyPreferences['PREPAYMENT_MONTH_LIMIT'] ) && ( $objLease->getScheduledChargeTotal() * $intPrepaylimit ) < $fltBalanceMultiple ) {
							break;
						}

						array_push( $arrstrValueInclude, '$' . __( '{%f,0}', [ number_format( ( float ) $fltBalanceMultiple, 2 ) ] ) );
					}

					if( in_array( '$' . __( '{%f,0}', [ number_format( ( float ) $fltPaymentAmount, 2 ) ] ), $arrstrValueInclude ) ) {
						return true;
					}

					if( 0 == $objLease->getBalance() ) {
						$strErrorMsg = __( 'When pre-paying, the payment amount must be a multiple of your monthly charges. For example, your monthly charges total $' ) . __( '{%f,0}', [ number_format( ( float ) $objLease->getScheduledChargeTotal(), 2 ) ] ) . __( ', so a valid payment amount would be any of the following value(s): ' ) . implode( ', ', $arrstrValueInclude ) . '.';

					} elseif( 0 > $objLease->getBalance() ) {
						$strErrorMsg = __( 'When pre-paying, the payment amount entered must be a multiple of your monthly charges "$' ) . __( '{%f,0}', [ number_format( ( float ) $objLease->getScheduledChargeTotal(), 2 ) ] ) . __( '."  Your account currently has a credit of $' ) . __( '{%f,0}', [ number_format( ( float ) ( -1 * $objLease->getBalance() ), 2 ) ] ) . __( '; consequently, acceptable payment amounts include the following value(s): ' ) . implode( ', ', $arrstrValueInclude ) . '.';
					} elseif( 0 < $objLease->getBalance() ) {
						$strErrorMsg = __( 'When pre-paying, the payment amount must be your existing balance "$' ) . __( '{%f,0}', [ number_format( ( float ) $objLease->getBalance(), 2 ) ] ) . __( '," plus a multiple of your monthly charges.  For example, your monthly charges total $' ) . __( '{%f,0}', [ number_format( ( float ) $objLease->getScheduledChargeTotal(), 2 ) ] ) . __( '; consequently, a valid payment amount would be any of the following value(s): ' ) . implode( ', ', $arrstrValueInclude ) . '.';
					}

					$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strErrorMsg ) );
					return false;
				}
			}
		}

		if( true == $boolIsPaymentFromRpPremium ) {

			$arrobjDeclinedPayments = CArPayments::fetchDeclinedPaymentsByCustomerIdByPropertyIdByCid( $this->getCustomerId(), $this->getPropertyId(), $this->getCid(), $objClientDatabase );
			$arrobjDeclinedGiactRequestLogs = \Psi\Eos\Payment\CGiactRequestLogs::createService()->fetchDeclinedGiactRequestLogsByCustomerIdByPropertyIdByCid( $this->getCustomerId(), $this->getPropertyId(), $this->getCid(), $objPaymentDatabase );
			if( true == valArr( $arrobjDeclinedPayments ) || true == valArr( $arrobjDeclinedGiactRequestLogs ) ) {
				if( true == valArr( $arrobjDeclinedPayments ) ) {
					$objCompanyPreference = \Psi\Eos\Entrata\CCompanyPreferences::createService()->fetchCompanyPreferenceByKeyByCid( $strKey = 'MAX_ALLOWED_PAYMENT_ATTEMPTS', $this->getCid(), $objClientDatabase );
					$intAllowedPaymentRequestRetryLimit = ( true == valObj( $objCompanyPreference, 'CCompanyPreference' ) && false == is_null( $objCompanyPreference->getValue() ) ) ? $objCompanyPreference->getValue() : CArPayment::ALLOWED_PAYMENT_REQUEST_RETRY_LIMIT;
					$intDeclinedCount = \Psi\Libraries\UtilFunctions\count( $arrobjDeclinedPayments );
					$objDeclineArPayment = array_shift( array_slice( $arrobjDeclinedPayments, ( $intAllowedPaymentRequestRetryLimit - 1 ), 1 ) );
					$objCurrentDate = new CDate( date( 'm/d/Y H:i:s' ) );
					$strDate = $objCurrentDate->getDate();
					if( true == valObj( $objDeclineArPayment, 'CArPayment' ) ) {
						$strDate = $objDeclineArPayment->getPaymentDateTime();
					}
				} else {
					$intDeclinedCount = \Psi\Libraries\UtilFunctions\count( $arrobjDeclinedGiactRequestLogs );
					$objDeclinedGiactRequestLog = array_shift( $arrobjDeclinedGiactRequestLogs );
					$strDate = $objDeclinedGiactRequestLog->getRequestTimestamp();
					$intAllowedPaymentRequestRetryLimit = CGiactRequestLog::ALLOWEDRETRYLIMIT;
				}

				if( $intAllowedPaymentRequestRetryLimit <= $intDeclinedCount ) {
					$strPaymentDateTime = new DateTime( $strDate );
					$intTimeDifference 	= ( 24 - ( time() - $strPaymentDateTime->getTimestamp() ) / ( 60 * 60 ) );
					$intHours 			= floor( $intTimeDifference );
					$intMinutes 		= floor( ( $intTimeDifference - $intHours ) * 60 );

					$strHealTime		.= ( 0 != $intHours ) ? ( ' ' . ( int ) $intHours . ' hour(s)' ) : '';
					$strHealTime		.= ( 0 != $intMinutes ) ? ( ' ' . ( int ) $intMinutes . ' minutes' ) : '';
					$strHealTime		= ( '' != $strHealTime ) ? ' for next' . $strHealTime . '.' : '';

					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'You have exceeded the maximum number of allowed payment attempts' ) . '(' . $intAllowedPaymentRequestRetryLimit . ')' . __( 'and are restricted from making payment' ) . $strHealTime ) );
					return false;
				}
			}
		}

		if( false == $this->valPastResidentOverPayment( $arrobjPropertyPreferences, $fltPaymentAmount, $objClientDatabase ) ) return false;

		return true;
	}

	public function validateCreditCardPaymentSettings( $objClientDatabase ) {

		$arrobjPropertyPreferences = ( array ) CPropertyPreferences::fetchPropertyPreferencesByPropertyIdsByKeysByCid( [ $this->m_intPropertyId ], [ 'ALLOW_LATE_CREDIT_CARD_PAYMENTS', 'RENT_CYCLE_BEGIN_DAY', 'RENT_LATE_DAY', 'DISALLOW_LATE_ONLINE_PAYMENTS', 'LATE_PAYMENT_ACCEPTANCE_THRESHOLD' ], $this->getCid(), $objClientDatabase );
		$arrobjPropertyPreferences = ( array ) rekeyObjects( 'key', $arrobjPropertyPreferences );

		$objProperty = $this->getOrFetchProperty( $objClientDatabase );
		$objPropertyPhoneNumber = $objProperty->getOrFetchPrimaryPropertyPhoneNumber( $objClientDatabase );

		$strPhoneNumberAddition = ( true == valObj( $objPropertyPhoneNumber, 'CPropertyPhoneNumber' ) && false == is_null( $objPropertyPhoneNumber->getPhoneNumber() ) ) ? ' at ' . $objPropertyPhoneNumber->getPhoneNumber() : '';

		// get timezone specific timestamp
		$intTimestamp = ( true == valObj( $objProperty, 'CProperty' ) ) ? $objProperty->getTimezoneSpecificTimestamp( date( 'm/d/Y H:i:s' ), $objClientDatabase ): NULL;
		if( false == is_numeric( $intTimestamp ) ) {
			$intTimestamp = time();
		}

		$objLease = $this->getOrFetchLease( $objClientDatabase );
		$objLeaseCustomer = $this->getOrFetchLeaseCustomer( $objClientDatabase );

		// RENT_LATE_DAY & DISALLOW_LATE_ONLINE_PAYMENTS
		// RENT_CYCLE_BEGIN_DAY
		// ALLOW_LATE_CREDIT_CARD_PAYMENTS

		$arrintRestrictedStatus = [ CLeaseStatusType::FUTURE, CLeaseStatusType::APPLICANT, CLeaseStatusType::PAST ];

		$boolIsMoveInCurrentMonth = \Psi\Eos\Entrata\CLeaseProcesses::createService()->checkMoveInDateAsCurrentDate( $objLease->getId(), $objLease->getCid(), $objClientDatabase );
		if( true == $boolIsMoveInCurrentMonth ) {
			$arrintRestrictedStatus[] = CLeaseStatusType::CURRENT;
		}

		if( true == valObj( $objLeaseCustomer, 'CLeaseCustomer' ) && false == in_array( $objLeaseCustomer->getLeaseStatusTypeId(), $arrintRestrictedStatus ) && ( false == array_key_exists( 'ALLOW_LATE_CREDIT_CARD_PAYMENTS', $arrobjPropertyPreferences ) || false == valObj( $arrobjPropertyPreferences['ALLOW_LATE_CREDIT_CARD_PAYMENTS'], 'CPropertyPreference' ) || 1 != $arrobjPropertyPreferences['ALLOW_LATE_CREDIT_CARD_PAYMENTS']->getValue() ) ) {

			if( true == valObj( $objLease, 'CLease' ) ) {
				// Calculate the balance of the current lease

				$objArTransactionsFilter = new CArTransactionsFilter();
				$objArTransactionsFilter->setDefaults( $boolIsResidentPortal = true );
				$objArTransactionsFilter->setShowOutstandingOnly( true );
				$objArTransactionsFilter->setHideCreditBalances( false );

				$objLease->fetchAndRollbackCustomerDisplayArTransactions( $objArTransactionsFilter, NULL, NULL, SYSTEM_USER_ID, $objClientDatabase );
			}

			$intPaymentReactivationDay = ( true == array_key_exists( 'RENT_CYCLE_BEGIN_DAY', $arrobjPropertyPreferences ) && true == is_numeric( $arrobjPropertyPreferences['RENT_CYCLE_BEGIN_DAY']->getValue() ) ) ? ( int ) $arrobjPropertyPreferences['RENT_CYCLE_BEGIN_DAY']->getValue() : 25;

			if( true == valObj( $objLease, 'CLease' ) && 0 < $objLease->getBalance() && true == array_key_exists( 'RENT_LATE_DAY', $arrobjPropertyPreferences ) && true == array_key_exists( 'DISALLOW_LATE_ONLINE_PAYMENTS', $arrobjPropertyPreferences ) && 1 == $arrobjPropertyPreferences['DISALLOW_LATE_ONLINE_PAYMENTS']->getValue() && date( 'j', $intTimestamp ) >= $arrobjPropertyPreferences['RENT_LATE_DAY']->getValue() && date( 'j', $intTimestamp ) <= $intPaymentReactivationDay ) {

				// Entering here means that this is a late payment.  We have a new setting, LATE_PAYMENT_ACCEPTANCE_THRESHOLD, that will allow payments under a certain amount.
				// If this value is set and the payment amount is less than this setting, don't reject validation.

				if( false == valObj( $arrobjPropertyPreferences['LATE_PAYMENT_ACCEPTANCE_THRESHOLD'], 'CPropertyPreference' ) || $this->getPaymentAmount() > ( float ) $arrobjPropertyPreferences['LATE_PAYMENT_ACCEPTANCE_THRESHOLD']->getValue() ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Payments made against overdue balances are not allowed online.  Please remit payment directly to the leasing office or contact the leasing office' . $strPhoneNumberAddition . '.' ) );
					return false;
				}
			}
		}

		return true;
	}

	public function validateCheckInformation( $objClientDatabase = NULL, $boolValidateBankName = true, $boolValidateCheckNumber = true ) {
		$boolIsValid = true;

		$boolIsValid &= $this->valCheckAccountNumber();
		$boolIsValid &= $this->valCheckRoutingNumber( $objClientDatabase );
		$boolIsValid &= $this->valCheckBlacklist( $objClientDatabase );
		$boolIsValid &= $this->valCheckProperNameOnAccount();

		if( true == $boolValidateCheckNumber ) {
			$boolIsValid &= $this->valCheckNumber();
		}

		if( true == $boolValidateBankName ) {
			$boolIsValid &= $this->valCheckBankName();
		}

		$boolIsValid &= $this->valCheckAccountTypeId();

		return $boolIsValid;
	}

	public function valForImproperTest( $objDatabase ) {
		$boolIsValid = true;

		if( true == isset ( $objDatabase ) && true == $this->m_boolIsTest ) {
			$objClient = $this->getOrFetchClient( $objDatabase );

			if( CCompanyStatusType::CLIENT == $objClient->getCompanyStatusTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'You cannot process a test transaction on a live account.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function validateCreditCardInformation( $objDatabase, $boolIsProspectPortalTerminal = false ) {
		$boolIsValid = true;

		$boolIsValid &= $this->valCcCardNumber();
		if( true == $boolIsProspectPortalTerminal ) {
			$boolIsValid &= $this->valSecureReferenceNumber();
		} else {
			$boolIsValid &= $this->valPciSecureReferenceNumber();
		}
		$boolIsValid &= $this->valCcNameOnCard();
		$boolIsValid &= $this->valCcExpDateYear();
		$boolIsValid &= $this->valCcExpDateMonth();
		$boolIsValid &= $this->valForImproperTest( $objDatabase );

		return $boolIsValid;
	}

	public function validateConfirmCreditCardInformation( $boolIsProspectPortalTerminal = false ) {
		$boolIsValid = true;

		$boolIsValid &= $this->valCcCardNumber();
		if( true == $boolIsProspectPortalTerminal ) {
			$boolIsValid &= $this->valSecureReferenceNumber();
		} else {
			$boolIsValid &= $this->valPciSecureReferenceNumber();
		}
		$boolIsValid &= $this->valCcNameOnCard();
		$boolIsValid &= $this->valCcExpDateMonth();
		$boolIsValid &= $this->valCcExpDateYear();

		return $boolIsValid;
	}

	public function validateBilltoInformation( $boolRequirePhoneNumber = false, $boolRequireEmailAddress = false ) {
		$boolIsValid = true;

		$boolIsValid &= $this->valBilltoNameFirst();
		$boolIsValid &= $this->valBilltoNameLast();
		$boolIsValid &= $this->valBilltoPhoneNumber( $boolRequirePhoneNumber );
		$boolIsValid &= $this->valBilltoEmailAddress( $boolRequireEmailAddress );
		$boolIsValid &= $this->valBilltoStreetLine1();

		if( 1 != $this->m_boolIsAllowInternationalCard ) {
			$boolIsValid &= $this->valBilltoCity();
			$boolIsValid &= $this->valBilltoStateCode();
			$boolIsValid &= $this->valBilltoPostalCode();
		}

		return $boolIsValid;
	}

	public function validateApplicationBilltoInformation( $boolRequirePhoneNumber = false, $boolRequireEmailAddress = false, $strClientCountryCode ='US' ) {
		$boolIsValid = true;
		// only validate billing information if the payment is non-terminal
		if( true == $this->getIsTerminal() || CPaymentType::SEPA_DIRECT_DEBIT == $this->getPaymentTypeId() ) return $boolIsValid;

		$boolIsValid &= $this->valBilltoNameFirst();
		$boolIsValid &= $this->valBilltoNameLast();
		$boolIsValid &= $this->valBilltoPhoneNumber( $boolRequirePhoneNumber );
		$boolIsValid &= $this->valBilltoEmailAddress( $boolRequireEmailAddress );

		if( false == isset( $this->m_strBilltoStreetLine1 ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_street_line1', __( 'Billing street is required.' ), 612 ) );
		} elseif( false == mb_detect_encoding( $this->m_strBilltoStreetLine1, 'ASCII', 'true' ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_street_line1', __( 'Only standard address characters are allowed.(Note- copy/pasting sometimes introduces special characters that are not allowed. Please type the address carefully into the field.)' ), 612 ) );
		} elseif( 'ASCII' == mb_detect_encoding( $this->m_strBilltoStreetLine1, 'ASCII', 'true' ) && true == isset( $this->m_strBilltoStreetLine2 ) && false == mb_detect_encoding( $this->m_strBilltoStreetLine2, 'ASCII', 'true' ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billto_street_line2', __( 'Only standard address characters are allowed.(Note- copy/pasting sometimes introduces special characters that are not allowed. Please type the address carefully into the field.)' ), 612 ) );
		}

		if( false == \valObj( $this->getMerchantAccount(), 'CMerchantAccount' ) ) {
			$objPaymentDatabase = $this->loadPaymentDataBaseForContactPoints();
			if( false == \valObj( $objPaymentDatabase, 'CDatabase' ) ) {
				return '';
			}
			$objPaymentDatabase->open();
			$objMerchantAccount = $this->fetchCompanyMerchantAccount( $objPaymentDatabase );
		} else {
			$objMerchantAccount = $this->getMerchantAccount();
		}

		if( true == \valObj( $objMerchantAccount, 'CCompanyMerchantAccount' ) && CMerchantProcessingType::INTERNATIONAL == $objMerchantAccount->getMerchantProcessingTypeId() && CPaymentTypes::isCreditCardPayment( $this->getPaymentTypeId() ) ) {
			return $boolIsValid;
		}

		if( 1 != $this->m_boolIsAllowInternationalCard ) {
			$boolIsValid &= $this->valBilltoCity();
			$boolIsValid &= $this->valBilltoStateCode();
			if( 'US' == $strClientCountryCode ) {
				$boolIsValid &= $this->valBilltoPostalCode();
			}
		}

		return $boolIsValid;
	}

	public function validateProspectPortalArPayment( $strAction, $objDatabase = NULL, $boolIsRequiredCvv = false, $boolIsResponsive = true, $objClientDatabase = NULL, $strClientCountryCode = 'US', $boolIsPostalAddress = false ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );

		if( CPaymentType::CHECK == $this->m_intPaymentTypeId ) {
			return true;
		}

		$boolIsValid = true;

		switch( $strAction ) {
			case 'validate_prospect_portal_summary_info':
			case 'validate_prospect_portal_billing_info':

				$boolIsValid &= $this->validateBilltoInformation( false, false );

				switch( $this->getPaymentTypeId() ) {
					case CPaymentType::ACH:
						$boolIsValid &= $this->validateCheckInformation( $objDatabase );
						break;

					case CPaymentType::PAD:
						$boolIsValid &= $this->valCheckRoutingNumber();
						$boolIsValid &= $this->validateCheckInformation( $objDatabase, false );
						break;

					case CPaymentType::VISA:
					case CPaymentType::MASTERCARD:
					case CPaymentType::DISCOVER:
					case CPaymentType::AMEX:
						$boolIsFromProspectPortalTerminal = false;
						if( true == $this->getIsTerminal() ) {
							$boolIsFromProspectPortalTerminal = true;
						}
						$boolIsValid &= $this->validateCreditCardInformation( $objDatabase, $boolIsFromProspectPortalTerminal );
						break;

					default:
						$boolIsValid = false;
						break;
				}

			case 'validate_prospect_portal_resident_info':
				$boolIsValid &= $this->validateForeignKeys( false );
				$boolIsValid &= $this->valPaymentDatetime();
				$boolIsValid &= $this->valPaymentAmount();
				$boolIsValid &= $this->valBilltoNameFirst();
				$boolIsValid &= $this->valBilltoNameLast();
				break;

			case 'validate_application_portal_billing_info':
				if( true == $boolIsPostalAddress ) {
					$boolIsValid &= $this->validateApplicationPostalAddress( true, true, $strClientCountryCode );
				} else {
					$boolIsValid &= $this->validateApplicationBilltoInformation( true, true, $strClientCountryCode );
				}
				if( true == $boolIsResponsive ) {
					$boolIsValid &= $this->valAgreesToPayConvenienceFees();
					$boolIsValid &= $this->valAgreesToTerms();
				}
				switch( $this->getPaymentTypeId() ) {
					case CPaymentType::ACH:
					case CPaymentType::PAD:
						$boolIsValid &= $this->validateCheckInformation( $objDatabase, false, false );

						if( 0 == $this->getUseStoredBillingInfo() ) {
							$boolIsValid &= $this->valConfirmCheckAccountNumber();
						}
						break;

					case CPaymentType::VISA:
					case CPaymentType::MASTERCARD:
					case CPaymentType::DISCOVER:
					case CPaymentType::AMEX:
						$boolIsFromProspectPortalTerminal = false;
						if( true == $this->getIsTerminal() ) {
							$boolIsFromProspectPortalTerminal = true;
						}
						$boolIsValid &= $this->validateCreditCardInformation( $objDatabase, $boolIsFromProspectPortalTerminal );
						if( true == $boolIsRequiredCvv ) {
							$boolIsValid &= $this->valCvvCode();
						}
						break;

					case CPaymentType::SEPA_DIRECT_DEBIT:
						$boolIsValid &= true;
						break;

					default:
						$boolIsValid = false;
						break;
				}

				$boolIsValid &= $this->validateForeignKeys( false );
				$boolIsValid &= $this->valPaymentDatetime();
				$boolIsValid &= $this->valPaymentAmount();
				break;

			case 'validate_confrim_application_portal_billing_info':
				if( true == $boolIsPostalAddress ) {
					$boolIsValid &= $this->validateApplicationPostalAddress( true, true, $strClientCountryCode );
				} else {
					$boolIsValid &= $this->validateApplicationBilltoInformation( true, true, $strClientCountryCode );
				}
				switch( $this->getPaymentTypeId() ) {
					case CPaymentType::ACH:
						$boolIsValid &= $this->validateCheckInformation( $objDatabase, true, false );
						break;

					case CPaymentType::PAD:
						$boolIsValid &= $this->validateCheckInformation( $objDatabase, false, false );
						break;

					case CPaymentType::VISA:
					case CPaymentType::MASTERCARD:
					case CPaymentType::DISCOVER:
					case CPaymentType::AMEX:
						$boolIsFromProspectPortalTerminal = false;
						if( true == $this->getIsTerminal() ) {
							$boolIsFromProspectPortalTerminal = true;
						}
						$boolIsValid &= $this->validateConfirmCreditCardInformation( $boolIsFromProspectPortalTerminal );
						break;

					default:
						$boolIsValid = false;
						break;
				}

				$boolIsValid &= $this->validateForeignKeys( false );
				$boolIsValid &= $this->valPaymentDatetime();
				$boolIsValid &= $this->valPaymentAmount();
				break;

			default:
		}

		return $boolIsValid;
	}

	public function validateApplicationPostalAddress( $boolRequirePhoneNumber = false, $boolRequireEmailAddress = false, $strClientCountryCode ='US' ) {
		$boolIsValid = true;
		// only validate billing information if the payment is non-terminal

		if( true == $this->getIsTerminal() || CPaymentType::SEPA_DIRECT_DEBIT == $this->getPaymentTypeId() || true == $this->m_boolIsAllowInternationalCard ) return $boolIsValid;

		$objPostalAddressService = \Psi\Libraries\I18n\PostalAddress\CPostalAddressService::createService();
		$strSectionName = __( 'Billing Address' );

		if( false == $objPostalAddressService->isValid( $this->getPostalAddresses()['billto'] ) ) {
			$arrobjErrorMsgs = $objPostalAddressService->getErrorMsgs();
			foreach( $arrobjErrorMsgs as $intKey => $objErrorMsg ) {
				if( ( CPostalAddressErrorCode::MISSING == $objErrorMsg->getCode() ) || ( $objErrorMsg->getField() == self::ADDRESS_POSTAL_CODE_FIELD && false == valStr( $this->getPostalAddresses()['billto']['postalCode'] ) ) ) {
					$arrmixExistingData = $objErrorMsg->getData();
					$arrmixNewData = [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ];
					$objErrorMsg->setData( array_merge( $arrmixExistingData, $arrmixNewData ) );
					continue;
				}
				unset( $arrobjErrorMsgs[$intKey] );
			}
			if( true == valArr( $arrobjErrorMsgs ) ) {
				$this->addPostalAddressErrorMsgs( $arrobjErrorMsgs, NULL, $strSectionName );
				foreach( $this->getErrorMsgs() as $objError ) {
					$objError->setMessage( __( 'Billing {%s,0}', [ $objError->getMessage() ] ) );
				}
				$boolIsValid = false;
			}
			$boolIsValid &= false;
		}

		if( CMerchantProcessingType::INTERNATIONAL == $this->getMerchantAccount()->getMerchantProcessingTypeId() && CPaymentTypes::isCreditCardPayment( $this->getPaymentTypeId() ) ) {
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function validateResidentPortalArPayment( $strAction, $objClientDatabase = NULL, $boolIsPaymentFromRpPremium = false, $arrfltTotalPaymentAmount = [], $boolIsRequiredCvv = false, $boolIsValidateUnitNumber = false, $objPaymentDatabase = NULL, $boolIsAmenityReservationPayment = false, $strClientCountryCode = 'US', $boolValidatePostalCode = true ) {

		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );

		if( true == $boolIsValidateUnitNumber && true == valObj( $this->getLease(), 'CLease' ) && ( CLeaseStatusType::FUTURE == $this->getLease()->getLeaseStatusTypeId() || CLeaseStatusType::APPLICANT == $this->getLease()->getLeaseStatusTypeId() ) ) {
			$boolIsValidateUnitNumber = false;
		}
		$boolIsValid = true;

		switch( $strAction ) {
			case 'validate_resident_portal_ar_payment':
				$boolIsValid &= $this->valPaymentDatetime();
				$boolIsValid &= $this->valPaymentAmount();
				$boolIsValid &= $this->valBilltoNameFirst();
				$boolIsValid &= $this->valBilltoNameLast();

				// Validate based on payment type
				switch( $this->getPaymentTypeId() ) {
					case CPaymentType::ACH:
					case CPaymentType::PAD:
						$boolIsValid &= $this->validateCheckInformation( $objClientDatabase, false, false );
						$boolIsValid &= $this->valCheckProperAccountNickName();
						if( false == $this->getUseStoredBillingInfo() ) {
							$boolIsValid &= $this->valConfirmCheckAccountNumber();
						}
						break;

					case CPaymentType::VISA:
					case CPaymentType::MASTERCARD:
					case CPaymentType::AMEX:
					case CPaymentType::DISCOVER:
						$boolIsValid &= $this->validateConfirmCreditCardInformation();
						if( true == $boolValidatePostalCode ) {
							$boolIsValid &= $this->valBilltoPostalCode();
						}

						if( true == $boolIsRequiredCvv ) {
							$boolIsValid &= $this->valCvvCode();
						}
						break;

					case CPaymentType::SEPA_DIRECT_DEBIT:
						$boolIsValid &= $this->validatePaymentBankAccountId();
						break;

					default:
				}

				if( true == $boolIsValid ) {
                    $boolIsValid &= $this->validatePaymentSettings( $objClientDatabase, $boolIsPaymentFromRpPremium, $arrfltTotalPaymentAmount, false, NULL, $objPaymentDatabase, $boolIsAmenityReservationPayment );
				}

				if( 0 < $this->m_objMerchantAccount->getDonationAccountId() ) {

					if( true == $this->m_boolApplyDonationFlag ) {
						$boolIsValid &= $this->valDonationAmount();
						$boolIsValid &= $this->valCompanyCharity();
					}
				}

				if( true == $boolIsValidateUnitNumber ) {
					$boolIsValid &= $this->valBilltoUnitNumber();
				}

				$boolIsValid &= $this->valAgreesToTerms();
				$boolIsValid &= $this->valAgreesToPayConvenienceFees();
				break;

			default:
		}

		return $boolIsValid;
	}

	public function validatePsAdminArPayment( $strAction, $objDatabase = NULL ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );

		$boolIsValid = true;

		switch( $strAction ) {
			case 'validate_psadmin_summary_info':
				$boolIsValid &= $this->valAgreesToTerms();
				$boolIsValid &= $this->valAgreesToPayConvenienceFees();

			case 'validate_psadmin_billing_info':

				$boolIsValid &= $this->validateBilltoInformation( false, false );

				switch( $this->getPaymentTypeId() ) {
					case CPaymentType::ACH:
						$boolIsValid &= $this->validateCheckInformation( $objDatabase );
						$boolIsValid &= $this->valConfirmCheckAccountNumber();
						break;

					case CPaymentType::VISA:
					case CPaymentType::MASTERCARD:
					case CPaymentType::DISCOVER:
					case CPaymentType::AMEX:

						$boolIsValid &= $this->validateCreditCardInformation( $objDatabase );
						break;

					default:
						$boolIsValid = false;
						break;
				}

			case 'validate_psadmin_resident_info':
				$boolIsValid &= $this->validateForeignKeys();
				$boolIsValid &= $this->valPaymentDatetime();
				$boolIsValid &= $this->valPaymentAmount( true );

				if( true == $this->getIsFloating() ) {
					$boolIsValid &= $this->valBilltoNameFirst();
					$boolIsValid &= $this->valBilltoNameLast();
				}

				$boolIsValid &= $this->valPostMonth();
				break;

			default:
		}

		return $boolIsValid;
	}

	// As of now this is used or floating payment screen of new entrata.
	// This can be made common function for validation of payments From entrata

	public function validateResidentWorksArPayment( $objDatabase = NULL ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );

		$boolIsValid = true;

		if( true == $this->getIsFloating() ) {
			$boolIsValid &= $this->valBilltoNameFirst();

			$boolIsValid &= $this->valBilltoNameLast();

		}

		$boolIsValid &= $this->validateForeignKeys();
		$boolIsValid &= $this->valPaymentDatetime();
		$boolIsValid &= $this->valPaymentAmount( false );

		// Make sure the payment type chosen is available.
		if( false == valArr( $this->m_arrobjPaymentTypes ) || false == array_key_exists( $this->getPaymentTypeId(), $this->m_arrobjPaymentTypes ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'payment_amount', 'Selected payment type is not allowed for given amount. Please select different payment type.' ) );
		}

		switch( $this->getPaymentTypeId() ) {
			case CPaymentType::ACH:
			case CPaymentType::PAD:
				$boolIsValid &= $this->validateCheckInformation( $objDatabase, false );
				if( false == is_null( $this->m_strConfirmCheckAccountNumber ) ) {
					$boolIsValid &= $this->valConfirmCheckAccountNumber();
				}
				break;

			case CPaymentType::VISA:
			case CPaymentType::MASTERCARD:
			case CPaymentType::DISCOVER:
			case CPaymentType::AMEX:
				$boolIsValid &= $this->validateCreditCardInformation( $objDatabase );
				if( false == is_null( $this->m_intCompanyMerchantAccountId ) ) {
					$objMerchantAccount = $this->getOrFetchMerchantAccount( $objDatabase );

					if( false == valObj( $objMerchantAccount, 'CMerchantAccount' ) ) {
						trigger_error( 'Merchant account failed to load.', E_USER_ERROR );
						exit;
					}

					if( false == $objMerchantAccount->getAllowInternationalCards() ) {
						$boolIsValid &= $this->valBilltoPostalCode();
					}

					if( true == $objMerchantAccount->getRequireCvv() ) {
						$boolIsValid &= $this->valCvvCode();
					}
				}
				break;

			case CPaymentType::CHECK:
			case CPaymentType::MONEY_ORDER:
				$boolIsValid &= $this->valCheckNumber( true );
				break;

			default:
		}

		$boolIsValid &= $this->valAgreesToPaymentTerms();

		return $boolIsValid;
	}

	public function validateResidentWorksPostArPayment( $arrobjChargeArTransactions, $objClientDatabase, $arrintPaymentChargeCodeIds = NULL ) {

		$boolIsValid = true;

		// ~We used to use these vars to allow unallocated payments to be lumped in with the current one being submitted to
		// ~fix problems where payments weren't getting allocated.  We'll leave these here for now, even though it's not used. DJB
		// $boolIsAddANewPayment 		= ( 0 == strlen( $this->getPaymentAmount() ) || 0.00 == $this->getPaymentAmount() ) ? false : true;
		$boolIsAddANewPayment 		= true;
		// $boolUseUnallocatedFunds	= ( false == is_null( $this->getUseUnallocatedAmount() ) ) ? true : false;
		$boolUseUnallocatedFunds	= false;

		if( ( false == $boolIsAddANewPayment && false == $boolUseUnallocatedFunds ) ) {
			$boolIsValid &= false;
			if( true == valArr( $arrobjChargeArTransactions ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, 'payment_amount', __( 'Please enter a payment amount or apply unallocated funds or both.' ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( NULL, 'payment_amount', __( 'Please enter a payment amount.' ) ) );
			}
		}

		// Make sure the payment type chosen is available.
		if( false == valArr( $this->m_arrobjPaymentTypes ) || false == array_key_exists( $this->getPaymentTypeId(), $this->m_arrobjPaymentTypes ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'payment_amount', __( 'Selected payment type is not allowed for given amount. Please select different payment type.' ) ) );
		}

		if( true == $boolIsAddANewPayment ) {

			$boolIsValid &= $this->valCid();
			$boolIsValid &= $this->valCompanyMerchantAccountId();
			$boolIsValid &= $this->valPropertyId();
			$boolIsValid &= $this->valPaymentChargeCodes( $arrintPaymentChargeCodeIds );
			if( 1 != is_null( $this->getIsFloating() ) ) {
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valLeaseId();
			}

			$boolIsValid &= $this->valPaymentTypeId();
			$boolIsValid &= $this->valPaymentStatusTypeId();
			$boolIsValid &= $this->valPaymentAmount();
			$boolIsValid &= $this->valConvenienceFeeAmount();
			$boolIsValid &= $this->valPaymentDatetime();
		}

		$boolIsValid &= $this->valPostMonth();

		// Make sure that none of the allocations are over the amount due and that the total of them all is not greater than the unallocated amount
		$fltAllocationsTotal = 0.00;
		if( true == valArr( $arrobjChargeArTransactions ) ) {
			foreach( $arrobjChargeArTransactions as $objChargeArTransaction ) {
				if( true == isset( $_REQUEST['ar_allocations'][$objChargeArTransaction->getId()]['selected'] ) && 1 == $_REQUEST['ar_allocations'][$objChargeArTransaction->getId()]['selected'] ) {
					$fltAllocationsTotal += abs( $_REQUEST['ar_allocations'][$objChargeArTransaction->getId()]['allocation_amount'] );
				}
			}
		}

		$fltCeilingAllocationAmount = ( float ) abs( $this->getPaymentAmount() );

		if( true == $boolUseUnallocatedFunds ) {
			// Load the allocations for this payment
			$objLease = $this->getOrFetchLease( $objClientDatabase );
			$fltUnallocatedAmount = $objLease->fetchUnallocatedPaymentsAndConcessionsTotal( $objClientDatabase );

			$fltCeilingAllocationAmount += ( float ) abs( $fltUnallocatedAmount );
		}

		if( round( $fltCeilingAllocationAmount, 2 ) < round( $fltAllocationsTotal, 2 ) ) {
			$boolIsValid &= false;
			if( true == $boolUseUnallocatedFunds ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Allocation amount cannot exceed the payment amount plus the unallocated amount.' ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Allocation amount cannot exceed the payment amount.' ) ) );
			}
		}

		if( true == valArr( $this->getArPaymentSplits() ) ) {
			$fltTotalSplitPayments = 0;
			foreach( $this->getArPaymentSplits() as $objArPaymentSplit ) {
				$fltTotalSplitPayments += $objArPaymentSplit->getPaymentAmount();
				if( true == $boolIsAddANewPayment ) {
					$boolIsValid &= $objArPaymentSplit->validate( VALIDATE_INSERT );
				} else {
					$boolIsValid &= $objArPaymentSplit->validate( VALIDATE_UPDATE );
				}

				if( false == $boolIsValid ) {
					$this->addErrorMsgs( $objArPaymentSplit->getErrorMsgs() );
				}
			}
		}

		// Validate based on payment type
		switch( $this->getPaymentTypeId() ) {
			case CPaymentType::ACH:
			case CPaymentType::PAD:
				if( 0 == $this->getUseStoredBillingInfo() ) {
					$boolIsValid &= $this->validateCheckInformation( $objClientDatabase, false, false );
					if( false == is_null( $this->m_strConfirmCheckAccountNumber ) ) {
						$boolIsValid &= $this->valConfirmCheckAccountNumber();
					}
				} else {
					$boolIsValid &= $this->valCheckBlacklist( $objClientDatabase );
					$boolIsValid &= $this->valCustomerPaymentAccountId();
				}
				break;

			case CPaymentType::VISA:
			case CPaymentType::MASTERCARD:
			case CPaymentType::AMEX:
			case CPaymentType::DISCOVER:
				if( 0 == $this->getUseStoredBillingInfo() ) {
					$boolIsValid &= $this->validateConfirmCreditCardInformation();
				} else {
					$boolIsValid &= $this->valCustomerPaymentAccountId();
				}
				if( false == is_null( $this->m_intCompanyMerchantAccountId ) ) {

					$objMerchantAccount = $this->getOrFetchMerchantAccount( $objClientDatabase );

					if( false == valObj( $objMerchantAccount, 'CMerchantAccount' ) ) {
						trigger_error( 'Merchant account failed to load.', E_USER_ERROR );
						exit;
					}

					if( false == $objMerchantAccount->getAllowInternationalCards() ) {
						$boolIsValid &= $this->valBilltoPostalCode();
					}

				}
				break;

			case CPaymentType::CHECK:
			case CPaymentType::MONEY_ORDER:
				$boolIsValid &= $this->valCheckNumber( true );
				break;

			default:
		}

		return $boolIsValid;
	}

	public function validatePaymentWarningOverride( $objClientDatabase, $objPropertyGlSetting = NULL, $boolIsCheck21 = false ) {

		$arrobjPropertyPreferences = ( array ) CPropertyPreferences::fetchPropertyPreferencesByPropertyIdsByKeysByCid( [ $this->m_intPropertyId ], [ 'ALLOW_PAYMENTS_FOR_RESIDENTS_IN_COLLECTION', 'PREVENT_PAST_RESIDENT_OVERPAYMENT', 'BALANCE_PAYMENT_REQUIREMENT', 'PARTIAL_PAYMENT_ALLOWANCE_OVERRIDE' ], $this->getCid(), $objClientDatabase );

		$arrobjPropertyPreferences	= rekeyObjects( 'Key', $arrobjPropertyPreferences );

		// 1 . override payment
		if( false == $this->valPastResidentOverPayment( $arrobjPropertyPreferences, $this->getPaymentAmount(), $objClientDatabase, $boolIsFromEntrata = true ) ) {
			return false;
		}

		// 2 lease in collection
		$objLease = $this->getOrFetchLease( $objClientDatabase );
		if( false == valObj( $objLease, 'CLease' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Lease not found for payment id: {%d, 0, nots}', [ $this->getId() ] ) ) );
			return false;
		}

		if( true == $objLease->checkLeaseInCollection() && false == isset( $arrobjPropertyPreferences['ALLOW_PAYMENTS_FOR_RESIDENTS_IN_COLLECTION'] ) && true == valObj( $this->m_objCompanyUser, 'CCompanyUser' ) && false == $this->m_objCompanyUser->getIsAdministrator() ) {

			$boolIsAllowedCompanyUserPermission = \Psi\Eos\Entrata\CCompanyUserPermissions::createService()->fetchCompanyUserPermissionsIsAllowedByCompanyUserIdByModuleIdByCid( $this->m_objCompanyUser->getId(), CModule::ALLOW_PAYMENT_WARNING_OVERRIDE, $this->getCid(), $objClientDatabase );
			if( NULL !== $boolIsAllowedCompanyUserPermission && true != $boolIsAllowedCompanyUserPermission ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Residents in Collection are not permitted to make payment.' ) ) );
				return false;
			}
		}

		// 3 negative amount not allowed
		if( true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) && true == $objPropertyGlSetting->getActivateStandardPosting() && false == $this->isElectronicPayment() && 0 > $this->getPaymentAmount() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Payment amount should not be negative.' ) ) );
			return false;
		}

		if( false == $boolIsCheck21 || 1 == $this->getCheckIsMoneyOrder() ) return true;

		// 4  partial payment amount not allowed
		if( ( true == isset( $arrobjPropertyPreferences['BALANCE_PAYMENT_REQUIREMENT'] ) && true == valObj( $arrobjPropertyPreferences['BALANCE_PAYMENT_REQUIREMENT'], 'CPropertyPreference' ) ) && ( CProperty::CHARGES_MUST_BE_PAID_FULL == $arrobjPropertyPreferences['BALANCE_PAYMENT_REQUIREMENT']->getValue() || CProperty::CURRENT_BALANCE_MUST_BE_PAID_IN_FULL == $arrobjPropertyPreferences['BALANCE_PAYMENT_REQUIREMENT']->getValue() ) ) {
			$objArTransactionsFilter = new CArTransactionsFilter();
			$objArTransactionsFilter->setDefaults();
			$objLease->fetchBalance( $objArTransactionsFilter, $objClientDatabase, true );
			$objPropertyPreferencePPAO = getArrayElementByKey( 'PARTIAL_PAYMENT_ALLOWANCE_OVERRIDE', $arrobjPropertyPreferences );
			$fltFormattedMinimumPaymentAmount = ( float ) $objLease->getBalance();
			$arrintCustomerIds 		= [];
			$arrobjLeaseCustomers	= [];

			$arrobjLeaseCustomers = ( array ) $objLease->fetchLeaseCustomers( $objClientDatabase );
			if( true == valArr( $arrobjLeaseCustomers ) ) {
				foreach( $arrobjLeaseCustomers as $objTempLeaseCustomer ) {
					if( CCustomerType::PRIMARY == $objTempLeaseCustomer->getCustomerTypeId() || CCustomerType::RESPONSIBLE == $objTempLeaseCustomer->getCustomerTypeId() ) {
						$arrintCustomerIds[] = $objTempLeaseCustomer->getCustomerId();
					}
				}
			}
			if( true == valObj( $objPropertyPreferencePPAO, 'CPropertyPreference' ) && CProperty::WAIVE_PAYMENT_REQUIREMENT_FOR_ROOMATES == $objPropertyPreferencePPAO->getValue() && 1 < \Psi\Libraries\UtilFunctions\count( $arrintCustomerIds ) ) {
				$fltFormattedMinimumPaymentAmount = 0;
			} elseif( true == valObj( $objPropertyPreferencePPAO, 'CPropertyPreference' ) && CProperty::DIVIDE_PAYMENT_REQUIREMENT_BY_NUMBER_OF_RESPONSIBLE_ROOMATES == $objPropertyPreferencePPAO->getValue() && 1 < \Psi\Libraries\UtilFunctions\count( $arrintCustomerIds ) ) {
				if( 0 < \Psi\Libraries\UtilFunctions\count( $arrintCustomerIds ) ) {
					$fltFormattedMinimumPaymentAmount = ( float ) ( $objLease->getBalance() / \Psi\Libraries\UtilFunctions\count( $arrintCustomerIds ) );
				}
			}
			if( $this->getPaymentAmount() < $fltFormattedMinimumPaymentAmount ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Partial payments are not allowed.' ) ) );
				return false;
			}
		}

		return true;
	}

	public function validateResidentWorksPostCheck21ArPayment( $strAction, $objPaymentDatabase = NULL, $objClientDatabase = NULL, $boolIsAllowPaymentWarningOverride = true ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );

		$boolIsValid = true;

		$arrobjPropertyPreferences = ( array ) CPropertyPreferences::fetchPropertyPreferencesKeysAndValuesByPropertyIdsByKeysByCid( [ $this->m_intPropertyId ], [ 'MAX_SPLIT_PAYMENT_AMOUNT' ], $this->getCid(), $objClientDatabase );
		$arrobjPropertyPreferences	= rekeyObjects( 'Key', $arrobjPropertyPreferences );

		// Make sure settings on the merchant account allow for check 21 payments
		$objMerchantAccount = $this->getOrFetchMerchantAccount( $objClientDatabase );

		if( true == \Psi\Libraries\UtilFunctions\valObj( $objMerchantAccount, CMerchantAccount::class ) && true == \Psi\Libraries\UtilFunctions\valId( $objMerchantAccount->getCommercialMerchantAccountId() ) ) {
			$objCommercialMerchantAccount = \Psi\Eos\Entrata\CMerchantAccounts::createService()->fetchMerchantAccountByIdByCid( $objMerchantAccount->getCommercialMerchantAccountId(), $this->getCid(), $objClientDatabase );
			$objCh21MerchantMethod = $objCommercialMerchantAccount->getOrFetchMethod( \CPaymentType::CHECK_21 );
			$fltMaxPaymentAmount = $objCh21MerchantMethod->getMaxPaymentAmount();

			if( 0 < $fltMaxPaymentAmount ) {
				$objMerchantAccount->setCh21MaxPaymentAmount( $fltMaxPaymentAmount );
			}
		}

		if( true == valArr( $this->getArPaymentSplits() ) ) {
			$fltTotalSplitPayments = 0;
			foreach( $this->getArPaymentSplits() as $objArPaymentSplit ) {
				$objCustomer = $objArPaymentSplit->getOrFetchCustomer( $objClientDatabase );
				if( $objMerchantAccount->getCh21MaxPaymentAmount() < $objArPaymentSplit->getPaymentAmount() ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Split {%s, 0} amount is greater than the max allowed payment amount for {%s, 1}.', [ $objMerchantAccount->getCh21MaxPaymentAmount(), $objCustomer->getNameFull() ] ) ) );
					$boolIsValid &= false;
				} elseif( true == isset( $arrobjPropertyPreferences['MAX_SPLIT_PAYMENT_AMOUNT'] ) && ( int ) str_replace( ',', '', $arrobjPropertyPreferences['MAX_SPLIT_PAYMENT_AMOUNT']->getValue() ) < $objArPaymentSplit->getPaymentAmount() ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Split payment amount is greater than the max allowed split payment amount for {%s, 0}.' . [ $objCustomer->getNameFull() ] ) ) );
					$boolIsValid &= false;
				}

				$fltTotalSplitPayments += $objArPaymentSplit->getPaymentAmount();

				if( 1 == $this->getCheckIsMoneyOrder() ) {
					$objArPaymentSplit->setCheckIsMoneyOrder( 1 );
				}

				$boolIsValid &= $objArPaymentSplit->validate( VALIDATE_INSERT, $objClientDatabase, $boolIsAllowPaymentWarningOverride );

				if( false == $boolIsValid ) {
					$this->addErrorMsgs( $objArPaymentSplit->getErrorMsgs() );
					break;
				}
			}

			if( trim( $fltTotalSplitPayments ) != trim( $this->getPaymentAmount() ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Total of the payment amount of all the line items must be equal to the payment amount of their corresponding split payment.' ) ) );
			}
		}

		switch( $strAction ) {
			case 'customer_specific_validation':
				if( true == is_null( $this->getIsFloating() ) || false == $this->getIsFloating() ) {
					$boolIsValid &= $this->valCustomerId();
					$boolIsValid &= $this->valLeaseId();
					$boolIsOverrideCertifiedPayments = $this->getIsModulePermission( CModule::OVERRIDE_CERTIFIED_PAYMENTS, $objClientDatabase );
					$boolIsValid &= $this->valCustomerPayStatus( $objClientDatabase, $boolIsOverrideCertifiedPayments, $boolIsAllowPaymentWarningOverride );
				}
				break;

			case 'post_payment':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCompanyMerchantAccountId();
				$boolIsValid &= $this->valPropertyId();
				$objPropertyGlSetting = $this->getOrFetchPropertyGlSetting( $objClientDatabase );

				if( true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) && true == $objPropertyGlSetting->getActivateStandardPosting() ) {
					$boolIsValid &= $this->valPostMonth();
				}

				$boolIsValid &= $this->valIsInMigrationMode( $objClientDatabase );

				if( CDataBlobType::LEADS != $this->getDataBlobTypeId() ) {
					if( true == is_null( $this->getIsFloating() ) || false == $this->getIsFloating() ) {
						$boolIsValid &= $this->valCustomerId();
						$boolIsValid &= $this->valLeaseId();

						if( false == valArr( $this->getArPaymentSplits() ) ) {
							$boolIsOverrideCertifiedPayments = $this->getIsModulePermission( CModule::OVERRIDE_CERTIFIED_PAYMENTS, $objClientDatabase );
							$boolIsValid &= $this->valCustomerPayStatus( $objClientDatabase, $boolIsOverrideCertifiedPayments, $boolIsAllowPaymentWarningOverride );
						}

					} else {
						$boolIsValid &= $this->valBilltoNameFirst();
						if( false == ( 1 == $this->getIsFloating() ) && ( true == is_null( $this->getBilltoUnitNumber() ) || 0 >= strlen( $this->getBilltoUnitNumber() ) ) ) {
							$this->addErrorMsg( new CErrorMsg( '', 'bill_to_unit_number', __( 'A unit number is required.' ) ) );
							$boolIsValid &= false;
						}
					}
				}

				$boolIsValid &= $this->valPaymentTypeId();
				$boolIsValid &= $this->valElectronicPaymentTypeId();
				$boolIsValid &= $this->valPaymentMediumId();
				$boolIsValid &= $this->valTerminalPaymentMediumId();
				$boolIsValid &= $this->valPaymentStatusTypeId();
				$boolIsValid &= $this->valPaymentDatetime();
				$boolIsValid &= $this->valPaymentAmount();
				$boolIsValid &= $this->valConvenienceFeeAmount();
				$boolIsValid &= $this->valCompanyChargeAmount();
				$boolIsValid &= $this->valBilltoIpAddress();

				if( false == $objMerchantAccount->validateTerminal( 'validate_terminal_check_21_payment_insert' ) ) {
					$this->addErrorMsgs( $objMerchantAccount->getErrorMsgs() );
					$boolIsValid = false;
					break;
				}

				if( $objMerchantAccount->getCh21MaxPaymentAmount() < $this->getPaymentAmount() ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Check amount is greater than the max allowed payment amount.' ) );
					$boolIsValid &= false;
				}

				$boolIsValid &= $this->valCheckAccountNumberForCheck21();
				$boolIsValid &= $this->valCheckRoutingNumber( $objClientDatabase );
				$boolIsValid &= $this->valCheckNumberForCheck21();
				$boolIsValid &= $this->validateCheck21Duplicity( $objPaymentDatabase );

				if( false == $boolIsAllowPaymentWarningOverride ) {
					$boolIsValid &= $this->validatePaymentWarningOverride( $objClientDatabase, $objPropertyGlSetting, true );
				}

			default:
		}

		return $boolIsValid;
	}

	public function valCustomerPayStatus( $objClientDatabase, $boolIsOverrideCertifiedPayments = false, $boolIsAllowPaymentWarningOverride = false ) {

		$boolIsValid = true;

		switch( $this->getPaymentTypeId() ) {
			case CPaymentType::ACH:
			case CPaymentType::CHECK_21:
			case CPaymentType::CHECK:
			case CPaymentType::HAP:
			case CPaymentType::PAD:

				if( true == valArr( $this->getArPaymentSplits() ) && true == $this->getIsSplit() ) {
					foreach( $this->getArPaymentSplits() as $objArPaymentSplit ) {
						$objCustomer 	= $objArPaymentSplit->getOrFetchCustomer( $objClientDatabase );
						$objLease 		= $objArPaymentSplit->getOrFetchLease( $objClientDatabase );
						$boolIsValid 	&= $this->valPayStatus( $objCustomer, $objLease, $objClientDatabase, $boolIsOverrideCertifiedPayments, $boolIsAllowPaymentWarningOverride );
					}
				} else {
					$objCustomer 	= $this->getOrFetchCustomer( $objClientDatabase );
					$objLease 		= $this->getOrFetchLease( $objClientDatabase );
					$boolIsValid 	= $this->valPayStatus( $objCustomer, $objLease, $objClientDatabase, $boolIsOverrideCertifiedPayments, $boolIsAllowPaymentWarningOverride );
				}

			default:
		}

		return $boolIsValid;
	}

	public function valPayStatus( $objCustomer, $objLease, $objClientDatabase, $boolIsOverrideCertifiedPayments = false, $boolIsAllowPaymentWarningOverride = false ) {

		if( false == valObj( $objCustomer, 'CCustomer' ) || false == valObj( $objLease, 'CLease' ) ) return false;

		$boolIsValid 				= true;
		$arrobjPropertyPreferences 	= CPropertyPreferences::fetchPropertyPreferencesByKeysByPropertyIdByCid( [ 'BLOCK_ALL_PAYMENTS_FROM_EVICTING', 'ALLOW_PAYMENTS_FOR_RESIDENTS_IN_COLLECTION' ], $objLease->getPropertyId(), $objLease->getCid(), $objClientDatabase );
		$arrobjPropertyPreferences	= rekeyObjects( 'Key', $arrobjPropertyPreferences );

		if( CPaymentAllowanceType::BLOCK_ALL == $objCustomer->getPaymentAllowanceTypeId()
			|| CPaymentAllowanceType::BLOCK_ALL == $objLease->getPaymentAllowanceTypeId() ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, $objCustomer->getNameFull() . __( ' is blocked from making any payments on this lease.' ) ) );
		} elseif( ( CPaymentAllowanceType::CASH_EQUIVALENT == $objCustomer->getPaymentAllowanceTypeId() || CPaymentAllowanceType::CASH_EQUIVALENT == $objLease->getPaymentAllowanceTypeId() )
			&& 1 != $this->getCheckIsMoneyOrder() && false == $boolIsOverrideCertifiedPayments ) {

			if( true == valObj( $this->m_objProperty, 'CProperty' ) ) {
				$arrmixNonCertifiedPaymentTypes = CPropertyPaymentTypes::fetchPaymentIdsByCertifiedFundsByPropertyId( $this->m_objProperty->getId(), $objClientDatabase );
				if( false == in_array( $this->getPaymentTypeId(), $arrmixNonCertifiedPaymentTypes ) ) {
					return $boolIsValid;
				}
			}

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( '{%s, 0} is restricted to make payments by certified funds only on this lease.', [ $objCustomer->getNameFull() ] ) ) );

		} elseif( false == $objLease->getIsPaymentAllowedForEvicting() && true == isset( $arrobjPropertyPreferences['BLOCK_ALL_PAYMENTS_FROM_EVICTING'] ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Evicting status resident {%s, 0} are not permitted to make a payment', [ $objCustomer->getNameFull() ] ) ) );

		} elseif( true == $objLease->checkLeaseInCollection() && false == isset( $arrobjPropertyPreferences['ALLOW_PAYMENTS_FOR_RESIDENTS_IN_COLLECTION'] ) && true == valObj( $this->m_objCompanyUser, 'CCompanyUser' ) && false == $this->m_objCompanyUser->getIsAdministrator() ) {
			if( false == $boolIsAllowPaymentWarningOverride ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( ' In Collection status resident {%s, 0} is not permitted to make a payment.', [ $objCustomer->getNameFull() ] ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function validatePaymentReversal( $objOriginalArPayment, $objClientDatabase ) {

		if( CPaymentType::PAD === $this->getPaymentTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, _( 'Canadian PAD payments are not able to be reversed at this time.' ) ) );
			return false;
		}

		$boolIsValid = true;
		$boolIsValid &= $this->valId();
		$boolIsValid &= $this->valCid();
		$boolIsValid &= $this->valCompanyMerchantAccountId();
		$boolIsValid &= $this->valPropertyId();
		$boolIsValid &= $this->valPaymentDatetime();
		$boolIsValid &= $this->valReversePaymentTypeId();
		$boolIsValid &= $this->valReversePaymentAmount( $objOriginalArPayment );
		$boolIsValid &= $this->valReverseBatchedOn( $objOriginalArPayment );

		switch( $this->getPaymentTypeId() ) {
			case CPaymentType::ACH:
				$boolIsValid &= $this->valCheckAccountNumber();
				$boolIsValid &= $this->valCheckRoutingNumber( $objClientDatabase );
				$boolIsValid &= $this->valCheckNameOnAccount();
				$boolIsValid &= $this->valCheckBankName();
				$boolIsValid &= $this->valCheckAccountTypeId();
				break;

			case CPaymentType::VISA:
			case CPaymentType::MASTERCARD:
			case CPaymentType::AMEX:
			case CPaymentType::DISCOVER:
				$boolIsValid &= $this->valSecureReferenceNumber();
				break;

			default:
		}

		return $boolIsValid;
	}

	public function validateEmoDuplicity( $objPaymentDatabase ) {

		$boolIsValid = true;

		$objDuplicateArPayment = CArPayments::fetchDuplicateTerminalArPayment( $this, $objPaymentDatabase );

		if( true == valObj( $objDuplicateArPayment, 'CArPayment' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Duplicate EMO payment was found with Id: {%d, 0, nots} and BillToAccountNumber: {%d, 1, nots}', [ $objDuplicateArPayment->getId(), $objDuplicateArPayment->getBilltoAccountNumber() ] ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valLeaseStatus( $objClientDatabase ) {
		$boolIsValid = true;

		if( true == isset( $objClientDatabase ) ) {
			$this->getOrFetchLease( $objClientDatabase );

			if( true == valObj( $this->m_objLease, 'CLease' ) ) {

				if( CLeaseStatusType::CANCELLED == $this->m_objLease->getLeaseStatusTypeId() ) {
					$this->addErrorMsg( new CErrorMsg( NULL, 'lease_id', __( 'You cannot make a payment on a cancelled lease.' ), NULL ) );
					$boolIsValid &= false;
				}

				if( CListType::EVICTION == $this->m_objLease->getTerminationListTypeId() ) {
					$this->addErrorMsg( new CErrorMsg( NULL, 'lease_id', __( 'You cannot make a payment on an lease in the eviction process.' ), NULL ) );
					$boolIsValid &= false;
				}
			}
		}

		return $boolIsValid;
	}

	public function valIsInPaymentStatusTypeIds( $arrintApprovedPaymentStatusTypeIds ) {
		$boolIsValid = ( true == valArr( $arrintApprovedPaymentStatusTypeIds ) && true == in_array( $this->getPaymentStatusTypeId(), $arrintApprovedPaymentStatusTypeIds ) ) ? true : false;

		if( false == $boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'payment_status_type_id', 'Invalid payment status.  Only the following payment status types allow this function:' . implode( ', ', $arrintApprovedPaymentStatusTypeIds ) . '.' ) );
		}

		return $boolIsValid;
	}

	public function valIsElectronicPayment() {
		if( false == $this->isElectronicPayment() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'payment_type_id', __( 'Only electronic payment types allow this function.' ) ) );
			return false;
		}

		return true;
	}

	public function valIsManual() {
		if( true == $this->isElectronicPayment() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'payment_type_id', __( 'Electronic payments cannot be processed as manual payments.' ) ) );
			return false;
		}

		return true;
	}

	public function valIsInPaymentTypeIds( $arrintApprovedPaymentTypeIds ) {
		$boolIsValid = ( true == valArr( $arrintApprovedPaymentTypeIds ) && true == in_array( $this->getPaymentTypeId(), $arrintApprovedPaymentTypeIds ) ) ? true : false;

		if( false == $boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'payment_type_id', 'Invalid payment type.  Only the following payment types allow this function:' . implode( ', ', $arrintApprovedPaymentTypeIds ) . '.' ) );
		}

		return $boolIsValid;
	}

	public function valAssertBatchedOn( $boolRequireBatchedOn = true ) {

		if( true == $boolRequireBatchedOn && true == is_null( $this->getBatchedOn() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'payment_type_id', __( 'This function requires that the payment is batched.' ) ) );
			return false;

		} elseif( false == $boolRequireBatchedOn && false == is_null( $this->getBatchedOn() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'payment_type_id', __( 'This function cannot be performed once the payment is batched.' ) ) );
			return false;
		}

		return true;
	}

	public function valIsControlledDistribution( $objClientDatabase ) {

		if( in_array( $this->getMerchantGatewayId(), CMerchantGateway::$c_arrintSkipControlledDistributionCheck ) ) {
			return true;
		}

		$objMerchantAccount = $this->getOrFetchMerchantAccount( $objClientDatabase );

		if( false == valObj( $objMerchantAccount, 'CMerchantAccount' ) || 1 != $objMerchantAccount->getIsControlledDistribution() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'company_merchant_account_id', __( 'Distribution must be controlled to perform this function.' ) ) );
			return false;
		}

		return true;
	}

	public function validateReversability() {

		$boolIsValid = true;

		if( 1 == $this->getIsSplit() ) {
			if( $this->getAdjustingPaymentAmount() < $this->getPaymentAmount() ) {
				$this->addErrorMsg( new CErrorMsg( NULL, '', __( 'You can not reverse partial amount for splited payment.' ) ) );
				$boolIsValid = false;
			}
		}

		if( false == $this->getRecreditConvenienceFees() && $this->getAdjustingPaymentAmount() > $this->getPaymentAmount() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, '', __( 'Reimbursement amount should not greater than payment amount.' ) ) );
			$boolIsValid = false;

		} elseif( true == $this->getRecreditConvenienceFees() && $this->getAdjustingPaymentAmount() > $this->getTotalAmount() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, '', __( 'Reimbursement amount should not greater than total payment amount.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPaymentForWeakValidationMode( $strAction, $objDatabase, $objPropertyGlSetting, $intUserValidationMode ) {

		$strAction = ( 'hard_edit_payment' == $strAction ) ? 'edit' : 'delete';

		if( 'edit' == $strAction && 0 == ( float ) $this->getPaymentAmount() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'payment_amount', __( 'Payment amount is required.' ) ) );
			return false;
		}

		$arrobjArTransactions = ( array ) $this->fetchUnreversedPaymentArTransactions( $objDatabase );

		if( CPropertyGlSetting::VALIDATION_MODE_EDIT_INITIAL_IMPORT != $objPropertyGlSetting->getUserValidationMode() ) {
			foreach( $arrobjArTransactions as $objArTransaction ) {
				if( true == $objArTransaction->getIsInitialImport() ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'You cannot ' . $strAction . ' initial imported payment, unless user validation mode is Edit Initial Import.' ) );
					return false;
				}
			}
		}

		$this->m_arrobjArTransactions 			= ( array ) $this->fetchUnreversedPaymentArTransactions( $objDatabase );
		$this->m_arrobjAppliedArTransactions	= ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchAppliedArTransactionsByArTransactionIdsByCid( array_keys( $this->m_arrobjArTransactions ), $this->getCid(), $objDatabase );

		foreach( $this->m_arrobjAppliedArTransactions as $objArTransaction ) {
			if( CArCodeType::REFUND == $objArTransaction->getArCodeTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'You cannot ' . $strAction . ' this payment, as refund is already created for this.' ) );
				return false;
			}
		}

		$arrobjArPaymentTransactionGlDetails = ( array ) CGlDetails::fetchGlDetailsForUserValidationModeByArPaymentIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		foreach( $arrobjArPaymentTransactionGlDetails as $objArPaymentTransactionGlDetail ) {

			if( false == is_null( $objArPaymentTransactionGlDetail->getGlReconciliationId() ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'You cannot ' . $strAction . ' this payment, as related GL is reconciled.' ) );
				return false;
			}

			if( false == is_null( $objArPaymentTransactionGlDetail->getAccountingExportBatchId() ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'You cannot ' . $strAction . ' this payment, as related GL is exported.' ) );
				return false;
			}

			if( date( 'Y-m-d H:i:s', strtotime( $objArPaymentTransactionGlDetail->getPostMonth() ) ) <= date( 'Y-m-d H:i:s', strtotime( $objPropertyGlSetting->getArLockMonth() ) ) && true == in_array( $intUserValidationMode, [ CPropertyGlSetting::VALIDATION_MODE_STRICT, CPropertyGlSetting::VALIDATION_MODE_OPEN_PERIODS ] ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'You cannot ' . $strAction . ' this payment, as transaction is in close period.' ) );
				return false;
			}
		}

		if( 1 == $this->getIsSplit() ) {

			$arrmixPropertyGlSettings = ( array ) CPropertyGlSettings::fetchPropertyGlSettingsByArPaymentIdByCid( $this->getId(), $this->getCid(), $objDatabase );
			$intCount = \Psi\Libraries\UtilFunctions\count( array_keys( rekeyArray( 'user_validation_mode', $arrmixPropertyGlSettings ) ) );

			if( 1 < $intCount ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'This payment is split among multiple leases from different properties. You cannot ' . $strAction . ' this payment as user validation mode for all properties must be same.' ) );
				return false;
			}
		}

		return true;
	}

	public function valActiveBankAccount( $arrintActiveBankAccountsIds ) {
		$boolIsValid = true;

		if( false == in_array( $this->getBankAccountId(), $arrintActiveBankAccountsIds ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_account', __( 'The bank account selected is not allowed for this property' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valPaymentTypes( $arrintPaymentTypeIds ) {
		$boolIsValid = true;

		if( false == in_array( $this->getPaymentTypeId(), $arrintPaymentTypeIds ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_type', __( 'The payment type selected is not allowed for this property' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valPaymentType( $objClientDatabase ) {
		$boolIsValid = true;
		$objLease = $this->getOrFetchLease( $objClientDatabase );

		if( CPaymentType::HAP == $this->getPaymentTypeId() && COccupancyType::AFFORDABLE != $objLease->getOccupancyTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resident', __( 'HAP Payments are not allowed for non-Affordable leases.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valPaymentChargeCodes( $arrintPaymentChargeCodeIds ) {
		$boolIsValid = true;
		$intArCodeId = ( true == valId( $this->getPaymentArCodeId() ) ) ? $this->getPaymentArCodeId() : $this->getArCodeId();
		if( false == valId( $intArCodeId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_charge_code', __( 'The payment charge code is required.' ) ) );
			$boolIsValid &= false;
		} elseif( false == in_array( $intArCodeId, $arrintPaymentChargeCodeIds ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_charge_code', __( 'The charge code selected is not allowed for this property.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase = NULL, $objPaymentDatabase = NULL, $boolValPaymentDuplicity = true, $objPropertyGlSetting = NULL, $intUserValidationMode = CPropertyGlSetting::VALIDATION_MODE_STRICT, $boolIsOverrideCertifiedPayments = false, $boolValNegativePaymentAmount = true, $arrintActiveBankAccountsIds = [], $arrintPaymentTypeIds = [], $arrintPaymentChargeCodeIds = [] ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );

		$boolIsValid = true;

		switch( $strAction ) {

			case 'void_payment':
				// WE CAN NEVER USE THE VOID ACTION ON A PAYMENT THAT ISN'T ALREADY CAPTURED.
				// THE REASON IS BECAUSE VOIDING WILL AFFECT INTEGRATION.  UNCAPTURED PAYMENTS SHOULD BE CANCELLED WHICH WILL NOT
				// AFFECT INTEGRATION WITH EXTERNAL SYSTEMS.
				if( CPaymentStatusType::CAPTURED != $this->getPaymentStatusTypeId() ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'You cannot void a payment that is not of status type "Captured."' ) ) );
					$boolIsValid = false;
				} elseif( false == is_null( $this->getBatchedOn() ) && false == in_array( $this->getPaymentTypeId(), [ CPaymentType::VISA, CPaymentType::MASTERCARD, CPaymentType::DISCOVER, CPaymentType::AMEX ] ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'You cannot void a transaction that has been processed (has been batched to the bank).' ) ) );
					$boolIsValid = false;
				} elseif( time() >= $this->getDistributeOnTimestamp() ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'You cannot void a transaction that has been settled to the property.' ) ) );
					$boolIsValid = false;
				} elseif( false == in_array( $this->getPaymentTypeId(), [ CPaymentType::ACH, CPaymentType::CHECK_21, CPaymentType::VISA, CPaymentType::MASTERCARD, CPaymentType::DISCOVER, CPaymentType::AMEX, CPaymentType::PAD ] ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'You cannot void a payment that is not a ACH, Check21 or Credit Card' ) ) );
					$boolIsValid = false;
				}
				break;

			case 'cancel_payment':
				if( false == $this->isElectronicPayment() ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'You cannot cancel a payment that is a non-electronic transaction.' ) ) );
					$boolIsValid = false;
				} elseif( false == in_array( $this->getPaymentStatusTypeId(), [ CPaymentStatusType::AUTHORIZED, CPaymentStatusType::AUTHORIZING, CPaymentStatusType::CAPTURING, CPaymentStatusType::PHONE_CAPTURE_PENDING, CPaymentStatusType::BATCHING, CPaymentStatusType::REVERSAL_PENDING ] ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'You cannot cancel a payment that is not of status type authorizing, authorized, phone capture pending or reversal pending.' ) ) );
					$boolIsValid = false;
				}
				break;

			case 'resident_works_reversability_full':
				$boolIsValid &= $this->validateReversability( $objPaymentDatabase );

			case 'resident_works_reversability':

				$arrintPaymentStatusTypeIds = ( true == $this->getIsDeclinePayment() ) ? [ CPaymentStatusType::CAPTURED, CPaymentStatusType::DECLINED ] : [ CPaymentStatusType::CAPTURED ];
				$boolIsValid                &= $this->valIsInPaymentStatusTypeIds( $arrintPaymentStatusTypeIds );

				$boolIsValid &= $this->valIsInPaymentTypeIds( CPaymentType::$c_arrintWebAllowedPaymentTypes );
				$boolIsValid &= $this->valAssertBatchedOn( $boolRequireBatchedOn = true );
				$boolIsValid &= $this->valIsControlledDistribution( $objClientDatabase );
				break;

			case 'validate_lease_status':
				$boolIsValid &= $this->valLeaseStatus( $objClientDatabase );
				break;

			case 'validate_post_nsf':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCompanyMerchantAccountId();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPaymentTypeId();
				$boolIsValid &= $this->valNsfPaymentStatusTypeId( $objPaymentDatabase );
				$boolIsValid &= $this->valPaymentDatetime();
				$boolIsValid &= $this->valPaymentAmount();
				$boolIsValid &= $this->valConvenienceFeeAmount();
				$boolIsValid &= $this->validateNsfChargeAmount();
				break;

			case 'process_terminal_payment':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCompanyMerchantAccountId();
				$boolIsValid &= $this->valPropertyId();

				if( true == is_null( $this->getIsFloating() ) ) {
					$boolIsValid &= $this->valCustomerId();
					$boolIsValid &= $this->valLeaseId();
				}

				$boolIsValid &= $this->valPaymentTypeId();

				if( CPaymentStatusType::BATCHING == $this->getPaymentStatusTypeId() ) {
					$boolIsValid &= $this->valBilltoNameFirst();
					$boolIsValid &= $this->valBilltoNameLast();
				}

				$boolIsValid &= $this->valElectronicPaymentTypeId();
				$boolIsValid &= $this->valPaymentMediumId();
				$boolIsValid &= $this->valTerminalPaymentMediumId();
				$boolIsValid &= $this->valPaymentStatusTypeId();
				$boolIsValid &= $this->valPaymentDatetime();
				$boolIsValid &= $this->valPaymentAmount();
				$boolIsValid &= $this->valConvenienceFeeAmount();
				$boolIsValid &= $this->valCompanyChargeAmount();
				$boolIsValid &= $this->valBilltoIpAddress();
				$boolIsValid &= $this->valReceiptDatetime();

				$objMerchantAccount = $this->getOrFetchMerchantAccount( $objClientDatabase );

				if( false == valObj( $this->m_objArPaymentMagstrip, 'CArPaymentMagstrip' ) ) {
					trigger_error( 'Magstrip not loaded when it should have been.', E_USER_ERROR );
					exit;
				}

				$boolIsValid &= $this->m_objArPaymentMagstrip->validate( VALIDATE_INSERT );

				switch( $this->getPaymentTypeId() ) {
					case CPaymentType::PAD:
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'You cannot process a PAD transaction through a terminal.' ) ) );
						break;

					case CPaymentType::ACH:
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'You cannot process an ACH transaction through a terminal.' ) ) );
						break;

					// It's a credit card payment.  validate the credit card info
					case CPaymentType::VISA:
					case CPaymentType::MASTERCARD:
					case CPaymentType::AMEX:
					case CPaymentType::DISCOVER:

						// $boolIsValid &= $this->valSecureReferenceNumber();
						$boolIsValid &= $this->valCcExpDateMonth();
						$boolIsValid &= $this->valCcNameOnCard();
						break;

					case CPaymentType::CHECK_21:
						// Make sure settings on the merchant account allow for check 21 payments
						if( false == $objMerchantAccount->validateTerminal( 'validate_terminal_check_21_payment_insert' ) ) {
							$this->addErrorMsgs( $objMerchantAccount->getErrorMsgs() );
							$boolIsValid = false;
							break;
						}

						$boolIsValid &= $this->valCheckAccountNumberForCheck21();
						$boolIsValid &= $this->valCheckRoutingNumber( $objClientDatabase );
						$boolIsValid &= $this->valCheckNumberForCheck21();

						if( true == $boolValPaymentDuplicity ) {
							$boolIsValid &= $this->validateCheck21Duplicity( $objPaymentDatabase );
						}

						if( false == $this->m_objArPaymentImageFront->validate( VALIDATE_INSERT ) ) {
							$this->addErrorMsgs( $this->m_objArPaymentImageFront->getErrorMsgs() );
							$boolIsValid &= false;
						}

						if( false == $this->m_objArPaymentImageReverse->validate( VALIDATE_INSERT ) ) {
							$this->addErrorMsgs( $this->m_objArPaymentImageReverse->getErrorMsgs() );
							$boolIsValid &= false;
						}
						break;

					default:
				}
				break;

			case 'process_emo_terminal_payment':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCompanyMerchantAccountId();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPaymentTypeId();
				$boolIsValid &= $this->valElectronicPaymentTypeId();
				$boolIsValid &= $this->valPaymentMediumId();
				$boolIsValid &= $this->valTerminalPaymentMediumId();
				$boolIsValid &= $this->valPaymentStatusTypeId();
				$boolIsValid &= $this->valPaymentDatetime();
				$boolIsValid &= $this->valPaymentAmount();

				$boolIsValid &= $this->validateEmoDuplicity( $objPaymentDatabase );
				break;

			case 'process_clearing_terminal_payment':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCompanyMerchantAccountId();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPaymentTypeId();
				$boolIsValid &= $this->valElectronicPaymentTypeId();
				$boolIsValid &= $this->valPaymentMediumId();
				$boolIsValid &= $this->valTerminalPaymentMediumId();
				$boolIsValid &= $this->valPaymentStatusTypeId();
				$boolIsValid &= $this->valPaymentDatetime();
				$boolIsValid &= $this->valPaymentAmount();

				$boolIsValid &= $this->validateBilltoInformation( false, false );

				switch( $this->getPaymentTypeId() ) {
					case CPaymentType::ACH:
					case CPaymentType::PAD:
						$boolIsValid &= $this->validateCheckInformation( $objClientDatabase );
						break;

					case CPaymentType::VISA:
					case CPaymentType::MASTERCARD:
					case CPaymentType::DISCOVER:
					case CPaymentType::AMEX:
						$boolIsValid &= $this->validateCreditCardPaymentSettings( $objClientDatabase );

						if( true == $boolIsValid ) {
							$boolIsValid &= $this->validateCreditCardInformation( $objClientDatabase );
						}
						break;

					default:
						$boolIsValid = false;
						break;
				}
				break;

			case 'update_no_floating':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCompanyMerchantAccountId();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valPaymentTypeId();
				$boolIsValid &= $this->valPaymentStatusTypeId();
				$boolIsValid &= $this->valPaymentDatetime();
				$boolIsValid &= $this->valPaymentAmount( false, $boolValNegativePaymentAmount );
				$boolIsValid &= $this->valConvenienceFeeAmount();
				$boolIsValid &= $this->valPostMonth();

				if( false == is_null( $objClientDatabase ) ) {
					$boolIsValid &= $this->valCustomerPayStatus( $objClientDatabase );
				}
				break;

			case 'validate_admin_update_payment_amount':
				$boolIsValid &= $this->valModifiedPaymentAmount( $objClientDatabase, $objPaymentDatabase );
				break;

			case 'post_payment':
				$boolIsValid &= $this->valCheckNumber();
				$boolIsValid &= $this->valPaymentDatetime();
				break;

			case 'post_manual_payment':
				$boolIsValid &= $this->valCheckNumber();
				$boolIsValid &= $this->valPaymentDatetime();
				$boolIsValid &= $this->valPaymentAmount();
				$boolIsValid &= $this->valPostMonth();
				$boolIsValid &= $this->valIsManual();
				$boolIsValid &= $this->valPaymentType( $objClientDatabase );
				break;

			case 'disassociate':
				$boolIsValid &= $this->valCheckNumber();
				$boolIsValid &= $this->valPaymentDatetime();
				break;

			case 'capture_payment':
				$boolIsValid &= $this->valPostMonth();
				$boolIsValid &= $this->valTransactionDatetime();
				break;

			case 'validate_agrees_to_payment_terms':
				$boolIsValid &= $this->valAgreesToPaymentTerms();
				break;

			case 'validate_post_month':
				$boolIsValid &= $this->valPostMonth();
				break;

			case 'validate_payment_amount':
				$boolIsValid &= $this->valPaymentAmount();
				break;

			case 'validate_promise_to_pay':
				$boolIsValid &= $this->valPromiseToPay();
				break;

			case 'validate_agrees_to_reverse_payment':
				$boolIsValid &= $this->valAgreesToReversePayment();
				break;

			case 'post_outside_of_ar_post_month':
				$boolIsValid &= $this->valArPostMonth( $objPropertyGlSetting, $intUserValidationMode );
				break;

			case 'edit_payment':
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valPaymentAmount();
				break;

			case 'bulk_payment_validation':
				$boolIsValid &= $this->valPaymentAmount();
				$boolIsValid &= $this->valCustomerPayStatus( $objClientDatabase, $boolIsOverrideCertifiedPayments );
				$boolIsValid &= $this->valReceiptDatetime( $boolIsManualPayment = true );
				$boolIsValid &= $this->valPaymentType( $objClientDatabase );
				break;

			case 'hard_edit_payment':
			case 'hard_delete_payment';
				$boolIsValid &= $this->valPaymentForWeakValidationMode( $strAction, $objClientDatabase, $objPropertyGlSetting, $intUserValidationMode );
				break;

			case 'post_hap_payment':
				$boolIsValid &= $this->valIsManual();
				$boolIsValid &= $this->valPaymentDatetime();
				$boolIsValid &= $this->valPaymentAmount( false, false );
				$boolIsValid &= $this->valPostMonth();
				break;

			case 'post_bulk_csv_payments':
				$boolIsValid &= $this->valActiveBankAccount( $arrintActiveBankAccountsIds );
				$boolIsValid &= $this->valPaymentTypes( $arrintPaymentTypeIds );
				$boolIsValid &= $this->valPaymentChargeCodes( $arrintPaymentChargeCodeIds );
				$boolIsValid &= $this->valPaymentType( $objClientDatabase );
				break;

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valChargebackPaymentAmount();
				break;

			case 'create_customer_payment_account':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPaymentTypeId();
				$boolIsValid &= $this->valElectronicPaymentTypeId();
				$boolIsValid &= $this->valPaymentMediumId();
				$boolIsValid &= $this->valPaymentDatetime();

				switch( $this->getPaymentTypeId() ) {
					case CPaymentType::ACH:
					case CPaymentType::PAD:
						$boolIsValid &= $this->validateCheckInformation( $objClientDatabase, false, false );
						break;

					case CPaymentType::VISA:
					case CPaymentType::MASTERCARD:
					case CPaymentType::DISCOVER:
					case CPaymentType::AMEX:
						$boolIsValid &= $this->validateBilltoInformation( false, false );
						$boolIsValid &= $this->validateCreditCardInformation( $objClientDatabase );
						break;

					default:
						$boolIsValid = false;
						break;
				}

			case 'validate_reverse_post_month_and_date':
				$boolIsValid &= $this->valDeletePostMonth();
				$boolIsValid &= $this->valDeleteDatetime();
				break;

			case 'validate_collection_payment':
				$boolIsValid &= $this->valPaymentChargeCodes( $arrintPaymentChargeCodeIds );

				switch( $this->getPaymentTypeId() ) {
					case CPaymentType::CHECK:
					case CPaymentType::MONEY_ORDER:
						$boolIsValid &= $this->valCheckNumber( true );
						break;

					default:
						// Default case
						break;
				}
				$boolIsValid &= $this->valPostMonth();
				break;

			default:
				// default case
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Process Functions
	 *
	 */

	public function validateProductPermissions( $strProcessMethod, $objClientDatabase ) {
		$boolIsValid = true;

		$intPsProductId = ( CPaymentType::CHECK_21 == $this->getPaymentTypeId() ) ? CPsProduct::RESIDENT_PAY_DESKTOP : CPsProduct::RESIDENT_PAY;

		if( true == CPaymentTypes::isElectronicPayment( $this->getPaymentTypeId() ) ) {
			switch( $strProcessMethod ) {
				case self::PROCESS_METHOD_AUTHORIZE:
				case self::PROCESS_METHOD_AUTHCAPTURE:
				case self::PROCESS_METHOD_CAPTURE:
				case self::PROCESS_METHOD_AUTHREVERSE:

					$boolIsValid = CPropertyProducts::createService()->fetchHasPermissions( $this->getCid(), $this->getPropertyId(), $intPsProductId, $objClientDatabase );

					if( false == $boolIsValid ) {
						$strPsProductName = ( CPaymentType::CHECK_21 == $this->getPaymentTypeId() ) ? 'Check scanning' : 'Resident Pay';
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'The property does not have permissions granted to the ' . $strPsProductName . ' product.' ) );
					}
					break;

				case self::PROCESS_METHOD_REVERSE:
				case self::PROCESS_METHOD_CREDIT:
				default:
					// default case
					break;
			}
		}

		return $boolIsValid;
	}

	/**
	 * @param $objPaymentDatabase
	 * @return bool
	 */
	public function validatePaymentBankAccountId() {
		$boolIsValid = true;

		if( CPaymentType::SEPA_DIRECT_DEBIT == $this->getPaymentTypeId() && ( false == valId( $this->getCustomerPaymentAccountId() ) || false == valStr( $this->getPaymentBankAccountId() ) ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'A valid {%s, 0} Direct Debit Mandate is required to process this payment.', [ 'SEPA' ] ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function processPayment( $strProcessMethod, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase = NULL, $boolForceIntegration = true, $boolSendEmails = false, $strReturnReasonCode = '', $intIntegrationSyncTypeId = NULL, $intUserValidationMode = CPropertyGlSetting::VALIDATION_MODE_STRICT, $boolIsInitialImport = false, $objLease = NULL, $boolIsAutoAllocateLatefees = false ) {

		$boolIsValid = true;

		if( false == $this->validateProductPermissions( $strProcessMethod, $objClientDatabase ) ) {
			return false;
		}

		if( true == is_null( $this->getIsDebitCard() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Card type could not be verified. Please try again.' ) ) );
			return false;
		}

		switch( $strProcessMethod ) {
			case self::PROCESS_METHOD_BATCH:
				$boolIsValid &= CPaymentProcesses::batchPayment( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase );
				break;

			case self::PROCESS_METHOD_AUTHORIZE:
				/** @var \Psi\Core\Payment\Operations\ArPayments\CAuthorize $objOperation */
				$objOperation = \Psi\Core\Payment\Operations\ArPayments\COperationFactory::createService()->get( CPaymentTransactionType::AUTHORIZE );
				$boolIsValid &= $objOperation->inject( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase )
					->execute();
				break;

			case self::PROCESS_METHOD_AUTHCAPTURE:
			    $this->setShouldAttemptRetries( $objClientDatabase );

			    if( $this->shouldAttemptRetries() ) {
                    $objOperation = \Psi\Core\Payment\Operations\ArPayments\CQueuedAuthCapture::create();
                    $boolIsValid &= $objOperation->inject( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase )
                        ->setForceIntegration( $boolForceIntegration )
                        ->execute();

                    break;
                }

				/** @var \Psi\Core\Payment\Operations\ArPayments\CAuthCapture $objOperation */
				$objOperation = \Psi\Core\Payment\Operations\ArPayments\COperationFactory::createService()->get( CPaymentTransactionType::AUTH_CAPTURE );
				$boolIsValid &= $objOperation->inject( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase )
					->execute();

				if( true == $boolIsValid && true == $boolForceIntegration ) {
					$this->exportArPayment( $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $intIntegrationSyncTypeId );
				}
				break;

			case self::PROCESS_METHOD_AUTHREVERSE:
				/** @var \Psi\Core\Payment\Operations\ArPayments\CAuthReverse $objOperation */
				$objOperation = \Psi\Core\Payment\Operations\ArPayments\COperationFactory::createService()->get( CPaymentTransactionType::AUTH_REVERSAL );
				$boolIsValid &= $objOperation->inject( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase )
					->execute();
				break;

			case self::PROCESS_METHOD_CAPTURE:
				/** @var \Psi\Core\Payment\Operations\ArPayments\CCapture $objOperation */
				$objOperation = \Psi\Core\Payment\Operations\ArPayments\COperationFactory::createService()->get( CPaymentTransactionType::CAPTURE );
				$boolIsValid &= $objOperation->inject( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase )
					->execute();

				if( true == $boolIsValid ) {
					$objLatefeeLibreary = new CLateFeeLibrary();
					$objLatefeeLibreary->autoAdjustLatefee( $this, $intCurrentUserId, $objClientDatabase );

					$objDelinquencyInterestLibrary = new CDelinquencyInterestLibrary();
					$objDelinquencyInterestLibrary->autoAdjustDelinquencyInterest( $this, $intCurrentUserId, $objClientDatabase );
				}

				if( true == $boolIsValid && true == $boolForceIntegration ) {
					$this->exportArPayment( $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $intIntegrationSyncTypeId );
				}
				break;

            case self::PROCESS_METHOD_FINALIZE_QUEUED_CAPTURE:
                $objOperation = \Psi\Core\Payment\Operations\ArPayments\CFinalizeQueuedCapture::create();
                $boolIsValid = $objOperation->inject( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase )
                                    ->execute();

                if( true == $boolIsValid ) {
                    $objLatefeeLibreary = new CLateFeeLibrary();
                    $objLatefeeLibreary->autoAdjustLatefee( $this, $intCurrentUserId, $objClientDatabase );

                    $objDelinquencyInterestLibrary = new CDelinquencyInterestLibrary();
                    $objDelinquencyInterestLibrary->autoAdjustDelinquencyInterest( $this, $intCurrentUserId, $objClientDatabase );
                }

                if( true == $boolIsValid && true == $boolForceIntegration ) {
                    $this->exportArPayment( $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $intIntegrationSyncTypeId );
                }
                break;

			case self::PROCESS_METHOD_REVERSE:
				$objMerchantAccount = $this->fetchMerchantAccount( $objClientDatabase );

				if( \Psi\Eos\Payment\CMerchantGateways::createService()->isAsyncGateway( $this->getMerchantGatewayId() ) ) {
					/** @var $objOperation \Psi\Core\Payment\Operations\ArPayments\CAsyncReverse */
					$objOperation = \Psi\Core\Payment\Operations\ArPayments\CAsyncReverse::createService();

					$boolIsValid &= $objOperation->inject( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase )
						->setArPayment( $this )
						->setRequiresReversalApproval( ( bool ) $this->getRequiresApproval() )
						->setIsForceIntegration( $boolForceIntegration )
						->setSendEmails( $boolSendEmails )
						->setIntegrationTypeId( $intIntegrationSyncTypeId )
						->execute();
				} else {
					$boolIsValid &= CPaymentProcesses::reversePayment( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase, $boolForceIntegration, $boolSendEmails, $intIntegrationSyncTypeId );
				}

				if( true == $boolIsValid ) {
					$objLatefeeLibreary = new CLateFeeLibrary();
					$objLatefeeLibreary->autoAdjustLatefee( $this, $intCurrentUserId, $objClientDatabase );

					$objDelinquencyInterestLibrary = new CDelinquencyInterestLibrary();
					$objDelinquencyInterestLibrary->autoAdjustDelinquencyInterest( $this, $intCurrentUserId, $objClientDatabase );
				}
				break;

			case self::PROCESS_METHOD_SETTLED_REVERSAL_CANCELLATION:
				$boolIsValid &= CPaymentProcesses::cancelSettledReversedPayment( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase );
				break;

			case self::PROCESS_METHOD_CREDIT:
				/** @var \Psi\Core\Payment\Operations\ArPayments\CCredit $objOperation */
				$objOperation = \Psi\Core\Payment\Operations\ArPayments\COperationFactory::createService()->get( CPaymentTransactionType::CREDIT );
				$boolIsValid &= $objOperation->inject( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase )
					->execute();
				break;

            case self::PROCESS_METHOD_MARK_VOIDED:
                // A payment can only be voided if it is controlled distribution and hasn't been settled.
                $arrobjArTransactions	= ( array ) $this->fetchUnreversedPaymentArTransactions( $objClientDatabase );
                /** @var \Psi\Core\Payment\Operations\ArPayments\CMarkVoided $objOperation */
                $objOperation = \Psi\Core\Payment\Operations\ArPayments\CMarkVoided::createService();
                $boolIsValid &= $objOperation->inject( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase )
                    ->execute();

                if( true == $boolIsValid ) {
                    $objLatefeeLibreary = new CLateFeeLibrary();
                    $objLatefeeLibreary->autoAdjustLatefee( $this, $intCurrentUserId, $objClientDatabase, $boolIsAutoAllocateLatefees = true, $arrobjArTransactions );

                    $objDelinquencyInterestLibrary = new CDelinquencyInterestLibrary();
                    $objDelinquencyInterestLibrary->autoAdjustDelinquencyInterest( $this, $intCurrentUserId, $objClientDatabase, $arrobjArTransactions );
                }

                if( true == $boolIsValid && true == $boolForceIntegration ) {
                    $this->exportReversedArPayment( $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, false, false, $intIntegrationSyncTypeId );
                }
                break;

			case self::PROCESS_METHOD_VOID:
				// A payment can only be voided if it is controlled distribution and hasn't been settled.
				$arrobjArTransactions	= ( array ) $this->fetchUnreversedPaymentArTransactions( $objClientDatabase );
				/** @var \Psi\Core\Payment\Operations\ArPayments\CVoid $objOperation */
				$objOperation = \Psi\Core\Payment\Operations\ArPayments\COperationFactory::createService()->get( CPaymentTransactionType::VOID );
				$boolIsValid &= $objOperation->inject( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase )
					->execute();

				if( true == $boolIsValid ) {
					$objLatefeeLibreary = new CLateFeeLibrary();
					$objLatefeeLibreary->autoAdjustLatefee( $this, $intCurrentUserId, $objClientDatabase, $boolIsAutoAllocateLatefees = true, $arrobjArTransactions );

					$objDelinquencyInterestLibrary = new CDelinquencyInterestLibrary();
					$objDelinquencyInterestLibrary->autoAdjustDelinquencyInterest( $this, $intCurrentUserId, $objClientDatabase, $arrobjArTransactions );
				}

				if( true == $boolIsValid && true == $boolForceIntegration ) {
					$this->exportReversedArPayment( $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, false, false, $intIntegrationSyncTypeId );
				}
				break;

			case self::PROCESS_METHOD_INITIATE_CHARGE_BACK:
				$boolIsValid &= CPaymentProcesses::initiateChargeback( $this, $intCurrentUserId, $boolSendEmails, $objClientDatabase, $objPaymentDatabase, $strReturnReasonCode, $objEmailDatabase );
				break;

			case self::PROCESS_METHOD_CHARGE_BACK_FAILED:
				// When this is called, we actually resettle a new payment to the client.  Integration is called down in the method.
				$boolIsValid &= CPaymentProcesses::processPaymentChargeBackFailed( $this, $intCurrentUserId, $boolSendEmails, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase, $boolForceIntegration );
				break;

			case self::PROCESS_METHOD_CHARGED_BACK:
				$boolIsValid &= CPaymentProcesses::processPaymentAsChargedBack( $this, $intCurrentUserId, $boolSendEmails, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase );
				break;

			case self::PROCESS_METHOD_CHARGE_BACK_ADJUSTMENT:
				$boolIsValid &= CPaymentProcesses::postChargeBackAdjustment( $this, $intCurrentUserId, $boolSendEmails, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase );
				break;

			case self::PROCESS_METHOD_CANCEL:
				$arrobjArTransactions	= ( array ) $this->fetchUnreversedPaymentArTransactions( $objClientDatabase );
				/** @var \Psi\Core\Payment\Operations\ArPayments\CCancel $objOperation */
				$objOperation = \Psi\Core\Payment\Operations\ArPayments\COperationFactory::createService()->get( CPaymentTransactionType::CANCEL );
				$boolIsValid &= $objOperation->inject( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase )
					->setSendEmails( $boolSendEmails )
					->execute();

				if( true == $boolIsValid ) {
					$objLatefeeLibreary = new CLateFeeLibrary();
					$objLatefeeLibreary->autoAdjustLatefee( $this, $intCurrentUserId, $objClientDatabase, $boolIsAutoAllocateLatefees = true, $arrobjArTransactions );

					$objDelinquencyInterestLibrary = new CDelinquencyInterestLibrary();
					$objDelinquencyInterestLibrary->autoAdjustDelinquencyInterest( $this, $intCurrentUserId, $objClientDatabase, $arrobjArTransactions );
				}
				break;

			case self::PROCESS_METHOD_RETURN:
				$boolIsValid &= CPaymentProcesses::processReturn( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase, $boolSendEmails, $strReturnReasonCode );

				if( true == $boolIsValid && true == $boolForceIntegration ) {
					$this->exportReturnedArPayment( $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, false, $intIntegrationSyncTypeId );
				}
				break;

			case self::PROCESS_METHOD_RETURN_REPRESENTED:
				// When this is called, we actually resettle a new payment to the client.  Integration is called down in the method.
				$boolIsValid &= CPaymentProcesses::processReturnRepresented( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $boolForceIntegration );
				break;

			case self::PROCESS_METHOD_APPROVE_PAYMENT_REVERSAL:
				if( \Psi\Eos\Payment\CMerchantGateways::createService()->isAsyncGateway( $this->getMerchantGatewayId() ) ) {
					/** @var \Psi\Core\Payment\Operations\ArPayments\CApproveAsyncReversal $objOperation */
					$objOperation = \Psi\Core\Payment\Operations\ArPayments\CApproveAsyncReversal::createService();
					$objOperation->inject( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase )
						->setArPayment( $this )
						->setIsForceIntegration( $boolForceIntegration )
						->execute();
				} else {
					$boolIsValid &= CPaymentProcesses::approvePaymentReversal( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase, $boolForceIntegration );
				}
				break;

			case self::PROCESS_METHOD_CHECK21_AMOUNT_ADJUSTMENT:
				$boolIsValid &= CPaymentProcesses::adjustCheck21Amount( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase );
				break;

			case self::PROCESS_METHOD_MANUAL_PAYMENT:
				$boolIsValid &= CPaymentProcesses::postManualPayment( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase );
				break;

			case self::PROCESS_METHOD_MANUAL_RETURN:
				$boolIsValid &= CPaymentProcesses::postManualReturn( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase );
				break;

			case self::PROCESS_METHOD_MANUAL_REVERSE:
				$arrobjArTransactions	= ( array ) $this->fetchUnreversedPaymentArTransactions( $objClientDatabase );
				$boolIsValid &= CPaymentProcesses::postManualReverse( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $intUserValidationMode );

				if( true == $boolIsValid ) {
					$objLatefeeLibreary = new CLateFeeLibrary();
					$objLatefeeLibreary->autoAdjustLatefee( $this, $intCurrentUserId, $objClientDatabase, $boolIsAutoAllocateLatefees = true, $arrobjArTransactions );

					$objDelinquencyInterestLibrary = new CDelinquencyInterestLibrary();
					$objDelinquencyInterestLibrary->autoAdjustDelinquencyInterest( $this, $intCurrentUserId, $objClientDatabase, $arrobjArTransactions );
				}
				break;

			case self::PROCESS_METHOD_EDIT_PAYMENT:
				$boolIsValid &= CPaymentProcesses::editArPayment( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase );
				break;

			case self::PROCESS_METHOD_REASSOCIATE:
				$boolIsValid &= CPaymentProcesses::reassociateArPayment( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objLease );
				break;

			case self::PROCESS_METHOD_HARD_EDIT:
				return CPaymentProcesses::hardEditArPayment( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase, $intUserValidationMode, $boolIsInitialImport );
				break;

			case self::PROCESS_METHOD_FINALIZE_3DSECURE:
				/** @var \Psi\Core\Payment\Operations\ArPayments\CFinalize3dSecure $objOperation */
				$objOperation = \Psi\Core\Payment\Operations\ArPayments\COperationFactory::createService()->get( CPaymentTransactionType::FINALIZE_3DSECURE );
				$boolIsValid &= $objOperation->inject( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase )
					->execute();
				break;

			case self::PROCESS_METHOD_POST:
			default:
				$boolIsValid &= $this->postPayment( $intCurrentUserId, $objClientDatabase, $objPaymentDatabase );
				break;
		}

		$arrstrProcessMethods = [ self::PROCESS_METHOD_REVERSE, self::PROCESS_METHOD_VOID, self::PROCESS_METHOD_MANUAL_RETURN, self::PROCESS_METHOD_MANUAL_REVERSE, self::PROCESS_METHOD_REASSOCIATE, self::PROCESS_METHOD_RETURN, self::PROCESS_METHOD_EDIT_PAYMENT ];

		if( true == $boolIsValid && true == in_array( $strProcessMethod, $arrstrProcessMethods ) ) {
			$boolIsValid &= $this->createArDeposit( $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $strProcessMethod );
		}

		return $boolIsValid;
	}

	/**
	 * This is to be used when a reversal is triggered on an international payment
	 *
	 * @param $intCurrentUserId
	 * @param $objClientDatabase
	 * @param $objPaymentDatabas
	 * @param $objEmailDatabase
	 * @param $boolForceIntegration
	 * @param $boolSendEmails
	 * @param $intIntegrationSyncTypeId
	 * @return bool
	 */
	public function handleInternationalCreditOperation( $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase, $boolForceIntegration, $boolSendEmails, $intIntegrationSyncTypeId ) {
		/** @var $objClientDatabase CDatabase */
		$objClientDatabase->begin();
		/** @var $objPaymentDatabase CDatabase */
		$objPaymentDatabase->begin();

		// Create the reverse payment.  Override the database transaction
		$boolIsValid = CPaymentProcesses::reversePayment( $this, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase, $boolForceIntegration, $boolSendEmails, $intIntegrationSyncTypeId, false, true );

		if( !$boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'An an unexpected error occurred while reversing this payment.' ) ) );
			$objPaymentDatabase->rollback();
			$objClientDatabase->rollback();

			return $boolIsValid;
		}

		// Fetch the new reversal payment so we can perform credit operation
		$objReversalArPayment = \Psi\Eos\Payment\CArPayments::createService()->fetchLastArPaymentByArPaymentIdByCidByPaymentStatusTypeId( $this->getId(), $this->getCid(), CPaymentStatusType::REVERSED, $objPaymentDatabase );

		if( !valObj( $objReversalArPayment, CArPayment::class ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'An an unexpected error occurred while reversing this payment.' ) ) );

			$objPaymentDatabase->rollback();
			$objClientDatabase->rollback();

			return false;
		}

		/** @var \Psi\Core\Payment\Operations\ArPayments\CCredit $objOperation */
		$objOperation = \Psi\Core\Payment\Operations\ArPayments\COperationFactory::createService()->get( CPaymentTransactionType::CREDIT );
		$boolIsValid &= $objOperation->inject( $objReversalArPayment, $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $objEmailDatabase )
			->execute();

		if( !$boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'An an unexpected error occurred while reversing this payment.' ) ) );
			$objPaymentDatabase->rollback();
			$objClientDatabase->rollback();

			return $boolIsValid;
		}

		// If all went well, commit the transaction
		$objPaymentDatabase->commit();
		$objClientDatabase->commit();

		return $boolIsValid;
	}

	public function reApplyArPayment( $intCompanyUserId, $objClientDatabase, $objPaymentDatabase, $intIntegrationSyncTypeId = NULL ) {

		$this->refreshField( 'remote_primary_key', 'ar_payments', $objPaymentDatabase );
		// Try Exporting for integrated lease only PBY
		$objLease = $this->fetchLease( $objClientDatabase );

		if( true == is_null( $objLease ) || true == is_null( $objLease->getRemotePrimaryKey() ) ) {
			$objProperty 					= $this->getOrFetchProperty( $objClientDatabase );
			$objIntegrationDatabase 	= ( true == valObj( $objProperty, 'CProperty' ) ) ? $objProperty->fetchIntegrationDatabase( $objClientDatabase ) : NULL;

			// For realpagebatch integration we don't need to check lease, it is optional.
			if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) || false == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), [ CIntegrationClientType::REAL_PAGE ] ) ) {
				return true;
			}
		}
		// Ideally we should export only captured payments but some times payment get return, void,, charge back before it get export,
		// in this case we need to first export it and then return it into external system.
		if( false == $this->isElectronicPayment() || false == in_array( $this->getPaymentStatusTypeId(), [ CPaymentStatusType::CAPTURED, CPaymentStatusType::RETURN_REPRESENTED, CPaymentStatusType::CHARGED_BACK, CPaymentStatusType::CHARGE_BACK_PENDING, CPaymentStatusType::VOIDED ] ) ) {
			return true;
		}
		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->m_intCid, CIntegrationService::REAPPLY_AR_PAYMENT, $intCompanyUserId, $objClientDatabase, $objPaymentDatabase );
		$objGenericWorker->setClient( $this->getOrFetchClient( $objClientDatabase ) );
		$objGenericWorker->setProperty( $this->getOrFetchProperty( $objClientDatabase ) );
		$objGenericWorker->setArPayment( $this );

		if( NULL != $intIntegrationSyncTypeId ) {
			$objGenericWorker->setIntegrationSyncTypeId( $intIntegrationSyncTypeId );
		}
		if( true == valObj( $objGenericWorker, 'CYardiClientWorker' ) || true == valObj( $objGenericWorker, 'CMriClientWorker' ) || true == valObj( $objGenericWorker, 'CAmsiClientWorker' ) ) {
			$objGenericWorker->setCallTimeLimit( 300 );
		}

		// fetching integration queue //
		// $objGenericWorker->setQueueSyncOverride( true );

		$boolIsValid = $objGenericWorker->process();

		if( true == valArr( $objGenericWorker->getErrorMsgs() ) ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}
		return $boolIsValid;
	}

	/**
	 * Export Functions
	 *
	 */

	public function exportArPayment( $intCompanyUserId, $objClientDatabase, $objPaymentDatabase, $intIntegrationSyncTypeId = NULL, $boolQueueSyncOverride = false, $boolIsScript = false ) {

		$this->refreshField( 'remote_primary_key', 'ar_payments', $objPaymentDatabase );

		if( false == is_null( $this->getRemotePrimaryKey() ) ) return true;

		// Try Exporting for integrated lease only PBY
		$objLease = $this->fetchLease( $objClientDatabase );

		$objProperty            = $this->getOrFetchProperty( $objClientDatabase );
		$objIntegrationDatabase = ( true == valObj( $objProperty, 'CProperty' ) ) ? $objProperty->fetchIntegrationDatabase( $objClientDatabase ) : NULL;

		if( true == is_null( $objLease ) || true == is_null( $objLease->getRemotePrimaryKey() ) ) {

			// For realpagebatch integration we don't need to check lease, it is optional.
			if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) || false == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), [ CIntegrationClientType::REAL_PAGE ] ) ) {
				return true;
			}
		}

		if( true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) ) {
			$objIntegrationClient = \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchIntegrationClientByIntegrationServiceIdByIntegrationDatabaseIdByCid( CIntegrationService::SEND_AR_PAYMENT, $objIntegrationDatabase->getId(), $objIntegrationDatabase->getCid(), $objClientDatabase );
		}

		$objPropertyIntegrationDatabase = $objProperty->fetchPropertyIntegrationDatabase( $objClientDatabase );

		// If payment is integrated before property integration date then payment should not be integrate.
		if( false == valObj( $objPropertyIntegrationDatabase, 'CPropertyIntegrationDatabase' ) || ( strtotime( $objPropertyIntegrationDatabase->getCreatedOn() ) > strtotime( $this->getCapturedOn() ) && ( true == valObj( $objIntegrationClient, 'CIntegrationClient' ) && true == valStr( $objIntegrationClient->getSyncFromDate() ) && strtotime( $objIntegrationClient->getSyncFromDate() ) > strtotime( $this->getCapturedOn() ) ) ) ) {
			return false;
		}

		// Ideally we should export only captured payments but some times payment get return, void,, charge back before it get export,
		// in this case we need to first export it and then return it into external system.
		if( false == $this->isElectronicPayment() || false == in_array( $this->getPaymentStatusTypeId(), [ CPaymentStatusType::CAPTURED, CPaymentStatusType::RECALL, CPaymentStatusType::CHARGED_BACK, CPaymentStatusType::CHARGE_BACK_PENDING, CPaymentStatusType::VOIDED ] ) ) {
			return true;
		}

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->m_intCid, CIntegrationService::SEND_AR_PAYMENT, $intCompanyUserId, $objClientDatabase, $objPaymentDatabase );
		$objGenericWorker->setClient( $this->getOrFetchClient( $objClientDatabase ) );
		$objGenericWorker->setProperty( $this->getOrFetchProperty( $objClientDatabase ) );
		$objGenericWorker->setArPayment( $this );
		$objGenericWorker->setTransportRules( [ 'is_script' => $boolIsScript ] );

		if( NULL != $intIntegrationSyncTypeId ) {
			$objGenericWorker->setIntegrationSyncTypeId( $intIntegrationSyncTypeId );
		}

		if( true == valObj( $objGenericWorker, 'CYardiClientWorker' ) || true == valObj( $objGenericWorker, 'CMriClientWorker' ) || true == valObj( $objGenericWorker, 'CAmsiClientWorker' ) ) {
			$objGenericWorker->setCallTimeLimit( 300 );
		}

		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );

		$boolIsValid = $objGenericWorker->process();

		if( true == $this->getIsShowIntegrationError() && true == valArr( $objGenericWorker->getErrorMsgs() ) ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		return $boolIsValid;
	}

	// only for Real Page payments

	public function reattemptRealPageArPayment( $intCompanyUserId, $objClientDatabase, $objPaymentDatabase, $boolUseMiscellaneousAccount = true ) {

		$this->refreshField( 'remote_primary_key', 'ar_payments', $objPaymentDatabase );

		if( true == is_null( $this->getRemotePrimaryKey() ) ) return true;

		// Ideally we should export only captured payments but some times payment get return, void,, charge back before it get export,
		// in this case we need to first export it and then return it into external system.
		if( false == $this->isElectronicPayment() || false == in_array( $this->getPaymentStatusTypeId(), [ CPaymentStatusType::CAPTURED, CPaymentStatusType::RECALL, CPaymentStatusType::CHARGED_BACK, CPaymentStatusType::CHARGE_BACK_PENDING, CPaymentStatusType::VOIDED ] ) ) {
			return true;
		}

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->m_intCid, CIntegrationService::SEND_AR_PAYMENT, $intCompanyUserId, $objClientDatabase, $objPaymentDatabase );
		$objGenericWorker->setClient( $this->getOrFetchClient( $objClientDatabase ) );
		$objGenericWorker->setProperty( $this->getOrFetchProperty( $objClientDatabase ) );
		$objGenericWorker->setArPayment( $this );
		$objGenericWorker->setIntegrationSyncTypeId( CIntegrationSyncType::NOW );

		$arrboolTransportRules['reintegrate']				= true;
		$arrboolTransportRules['use_miscellaneous_account']	= $boolUseMiscellaneousAccount;

		$objGenericWorker->setTransportRules( $arrboolTransportRules );

		$boolIsValid = $objGenericWorker->process();

		if( true == $this->getIsShowIntegrationError() && true == valArr( $objGenericWorker->getErrorMsgs() ) ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		return $boolIsValid;
	}

	// This function should now be called as soon as a payment is exported.
	// For now may, 24, 2007, only amsi needs this function to be called.

	public function exportUpdatedArPayment( $intCompanyUserId, $objClientDatabase, $objPaymentDatabase, $boolQueueSyncOverride = false ) {

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->m_intCid, CIntegrationService::MODIFY_AR_PAYMENT, $intCompanyUserId, $objClientDatabase );
		$objGenericWorker->setClient( $this->fetchClient( $objClientDatabase ) );
		$objGenericWorker->setProperty( $this->fetchProperty( $objClientDatabase ) );
		$objGenericWorker->setArPayment( $this );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );
		$objGenericWorker->setPaymentDatabase( $objPaymentDatabase );

		return $objGenericWorker->process();
	}

	// This function should not be private because it needs to be called from the PADE script.

	public function exportReturnedArPayment( $intCompanyUserId, $objClientDatabase, $objPaymentDatabase, $boolQueueSyncOverride = false, $intIntegrationSyncTypeId = NULL, $boolForcePaymentReturn = false, $boolIsApplicationPaymentTransfer = false ) {

		if( 0 < strlen( trim( $this->getReturnRemotePrimaryKey() ) ) ) return true;

		$objLease = $this->getOrFetchLease( $objClientDatabase );

		$objProperty 					= $this->getOrFetchProperty( $objClientDatabase );
		$objIntegrationDatabase 	= ( true == valObj( $objProperty, 'CProperty' ) ) ? $objProperty->fetchIntegrationDatabase( $objClientDatabase ) : NULL;

		// For realpagebatch integration we don't need to check lease, it is optional.
		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) || false == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), [ CIntegrationClientType::REAL_PAGE ] ) ) {
			if( false == valObj( $objLease, 'CLease', 'RemotePrimaryKey' ) ) return true;
		}

		// If there is no remote primary key, wouldn't we just want to return true?
		if( true == is_null( $this->getRemotePrimaryKey() ) ) {
			$this->exportArPayment( $intCompanyUserId, $objClientDatabase, $objPaymentDatabase, $boolQueueSyncOverride );
		}

		$objClient = $this->getOrFetchClient( $objClientDatabase );

		if( false == valObj( $objClient, 'CClient' ) ) {
			trigger_error( 'Failed to load client object.', E_USER_ERROR );
		}

		$arrintPaymentStatusTypeIds = [
			CPaymentStatusType::CHARGED_BACK,
			CPaymentStatusType::CHARGE_BACK_PENDING,
			CPaymentStatusType::CAPTURED,
			CPaymentStatusType::REVERSED,
			CPaymentStatusType::RECALL,
			CPaymentStatusType::TRANSFERRED
		];

		if( false == $boolForcePaymentReturn && false == in_array( $this->getPaymentStatusTypeId(), $arrintPaymentStatusTypeIds ) ) return true;

		$boolIsReturnIntegrationCall 	= false;
		$arrobjAssignedReturnTypes		= ( true == in_array( $this->getPaymentStatusTypeId(), [ CPaymentStatusType::RECALL, CPaymentStatusType::CHARGED_BACK, CPaymentStatusType::CHARGE_BACK_PENDING ] ) ) ? ( array ) $objClient->fetchAssignedReturnTypes( $objClientDatabase ) : [];
		$objCompanyPreference			= $objClient->fetchCompanyPreferenceByKey( 'USE_REVERSE_INTEGRATION_CALL_ONLY_FOR_VOID', $objClientDatabase );
		$objIntegrationDatabase 		= \Psi\Eos\Entrata\CIntegrationDatabases::createService()->fetchIntegrationDatabaseByPropertyIdByCid( $this->getPropertyId(), $objClient->getId(), $objClientDatabase );

		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) ) return true;

		if( ( true == valObj( $objCompanyPreference, 'CCompanyPreference' ) && CIntegrationClientType::YARDI == $objIntegrationDatabase->getIntegrationClientTypeId() ) || ( true == is_numeric( $this->getReturnTypeId() ) && true == array_key_exists( $this->getReturnTypeId(), $arrobjAssignedReturnTypes ) ) ) {
			$boolIsReturnIntegrationCall = true;
		} else {

			if( CPaymentStatusType::CAPTURED != $this->getPaymentStatusTypeId() ) {
				$this->alertIncorrectPaymentCall( $objClient, $arrintPaymentStatusTypeIds, $arrobjAssignedReturnTypes, $objClientDatabase );
			}
		}

		if( true == $boolIsReturnIntegrationCall ) {

			if( CPaymentStatusType::CAPTURED == $this->getPaymentStatusTypeId() && 1 == $this->getIsReversed() && false == $boolIsApplicationPaymentTransfer ) {
				$objArPaymentReversed = $objClient->fetchArPaymentReversedByArPaymentId( $this->getId(), $objPaymentDatabase );

				if( abs( $objArPaymentReversed->getPaymentAmount() ) != $this->getPaymentAmount() ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'You have to manually adjust partial payment reverse into third party system.' ) ) );
					return false;
				}
			}

			$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->m_intCid, CIntegrationService::RETURN_AR_PAYMENT, $intCompanyUserId, $objClientDatabase );
			$objGenericWorker->setClient( $objClient );
			$objGenericWorker->setProperty( $this->getOrFetchProperty( $objClientDatabase ) );
			$objGenericWorker->setArPayment( $this );
			$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );
			$objGenericWorker->setPaymentDatabase( $objPaymentDatabase );
			$objGenericWorker->setDontEnqueueService( $boolForcePaymentReturn );
			$objGenericWorker->setForcePaymentReverse( $boolForcePaymentReturn );

			if( NULL != $intIntegrationSyncTypeId ) {
				$objGenericWorker->setIntegrationSyncTypeId( $intIntegrationSyncTypeId );
			}

			$boolIsValid = $objGenericWorker->process();

			if( true == $this->getIsShowIntegrationError() && true == valArr( $objGenericWorker->getErrorMsgs() ) ) {
				$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
			}

			return $boolIsValid;

		} else {
			return $this->exportReversedArPayment( $intCompanyUserId, $objClientDatabase, $objPaymentDatabase, $boolQueueSyncOverride, $boolForcePaymentReturn, $intIntegrationSyncTypeId, $boolIsApplicationPaymentTransfer );
		}
	}

	// This function should not be private because it needs to be called from the PADE script.
	// This function allocates the payment to a reversal batch in the 3rd party accounting system.

	public function exportUpdatedReturnedArPayment( $intCompanyUserId, $objClientDatabase, $objPaymentDatabase, $boolQueueSyncOverride = false ) {

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->m_intCid, CIntegrationService::MODIFY_RETURNED_AR_PAYMENT, $intCompanyUserId, $objClientDatabase );
		$objGenericWorker->setClient( $this->fetchClient( $objClientDatabase ) );
		$objGenericWorker->setProperty( $this->fetchProperty( $objClientDatabase ) );
		$objGenericWorker->setArPayment( $this );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );
		$objGenericWorker->setPaymentDatabase( $objPaymentDatabase );

		return $objGenericWorker->process();
	}

	// This function should not be private because it needs to be called from the PADE script.

	public function exportReversedArPayment( $intCompanyUserId, $objClientDatabase, $objPaymentDatabase, $boolQueueSyncOverride = false, $boolForcePaymentReverse = false, $intIntegrationSyncTypeId = NULL, $boolIsApplicationPaymentTransfer = false ) {

		if( 0 < strlen( trim( $this->getReturnRemotePrimaryKey() ) ) ) return true;

		$objLease = $this->getOrFetchLease( $objClientDatabase );

		if( false == valObj( $objLease, 'CLease', 'RemotePrimaryKey' ) ) return true;

		if( false == in_array( $this->getPaymentStatusTypeId(), [ CPaymentStatusType::CAPTURED, CPaymentStatusType::RECALL, CPaymentStatusType::CHARGED_BACK, CPaymentStatusType::CHARGE_BACK_PENDING, CPaymentStatusType::VOIDED ] ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Invalid payment status type.' ) ) );
			return false;
		}

		if( false == $this->isElectronicPayment() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'You can not be exported unless it is an electronic payment.' ) ) );
			return false;
		}

		if( true == is_null( $this->getRemotePrimaryKey() ) ) {
			$this->exportArPayment( $intCompanyUserId, $objClientDatabase, $objPaymentDatabase, $boolQueueSyncOverride );
		}

		$objClient = $this->getOrFetchClient( $objClientDatabase );

		if( CPaymentStatusType::CAPTURED == $this->getPaymentStatusTypeId() && 1 == $this->getIsReversed() && false == $boolIsApplicationPaymentTransfer ) {
			$objArPaymentReversed = $objClient->fetchArPaymentReversedByArPaymentId( $this->getId(), $objPaymentDatabase );

			if( false == valObj( $objArPaymentReversed, 'CArPayment' ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to load reversed payment.' ) ) );
				return false;
			}

			if( abs( $objArPaymentReversed->getPaymentAmount() ) != $this->getPaymentAmount() ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, '__( You have to manually adjust partial payment reverse into third party system.' ) );
				return false;
			}
		}

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->m_intCid, CIntegrationService::REVERSE_AR_PAYMENT, $intCompanyUserId, $objClientDatabase );
		$objGenericWorker->setClient( $objClient );
		$objGenericWorker->setProperty( $this->fetchProperty( $objClientDatabase ) );
		$objGenericWorker->setArPayment( $this );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );
		$objGenericWorker->setDontEnqueueService( $boolForcePaymentReverse );
		$objGenericWorker->setForcePaymentReverse( $boolForcePaymentReverse );
		$objGenericWorker->setPaymentDatabase( $objPaymentDatabase );

		if( NULL != $intIntegrationSyncTypeId ) {
			$objGenericWorker->setIntegrationSyncTypeId( $intIntegrationSyncTypeId );
		}

		$boolIsValid = $objGenericWorker->process();

		if( true == $this->getIsShowIntegrationError() && true == valArr( $objGenericWorker->getErrorMsgs() ) ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		return $boolIsValid;
	}

	// This function should not be private because it needs to be called from the PADE script.
	// This function allocates the payment to a reversal batch in the 3rd party accounting system.

	public function exportUpdatedReversedArPayment( $intCompanyUserId, $objClientDatabase, $objPaymentDatabase, $boolQueueSyncOverride = false, $boolForcePaymentReverse = false ) {

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->m_intCid, CIntegrationService::MODIFY_REVERSED_AR_PAYMENT, $intCompanyUserId, $objClientDatabase );
		$objGenericWorker->setClient( $this->fetchClient( $objClientDatabase ) );
		$objGenericWorker->setProperty( $this->fetchProperty( $objClientDatabase ) );
		$objGenericWorker->setArPayment( $this );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );
		$objGenericWorker->setForcePaymentReverse( $boolForcePaymentReverse );
		$objGenericWorker->setPaymentDatabase( $objPaymentDatabase );

		return $objGenericWorker->process();
	}

	// This function close the return payment batch in the 3rd party accounting system. AMSI

	public function exportCloseReturnedPaymentBatch( $intCompanyUserId, $objClientDatabase, $objPaymentDatabase, $boolQueueSyncOverride = false, $boolForcePaymentReverse = false ) {

		$objGenericWorker = CIntegrationFactory::createWorker( $this->m_intPropertyId, $this->m_intCid, CIntegrationService::CLOSE_RETURNED_PAYMENT_BATCH, $intCompanyUserId, $objClientDatabase );
		$objGenericWorker->setClient( $this->fetchClient( $objClientDatabase ) );
		$objGenericWorker->setProperty( $this->fetchProperty( $objClientDatabase ) );
		$objGenericWorker->setArPayment( $this );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );
		$objGenericWorker->setForcePaymentReverse( $boolForcePaymentReverse );
		$objGenericWorker->setPaymentDatabase( $objPaymentDatabase );

		return $objGenericWorker->process();
	}

	/**
	 * Database Functions
	 *
	 */

	public function insert( $intCurrentUserId, $objClientDatabase, $objPaymentDatabase = NULL, $boolOverrideTransaction = false ) {
		if( false == valObj( $objClientDatabase, 'CDatabase' ) || CDatabaseType::CLIENT != $objClientDatabase->getDatabaseTypeId()
			|| false == valObj( $objPaymentDatabase, 'CDatabase' ) || CDatabaseType::PAYMENT != $objPaymentDatabase->getDatabaseTypeId() ) {
			trigger_error( 'Failed to load database object', E_USER_ERROR );
			return false;
		}

		if( in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintECheckPaymentTypeIds, false ) ) {

			$this->setCcNameOnCard( NULL );
			$this->setCcCardNumber( NULL );
			$this->setCcExpDateYear( NULL );
			$this->setCcExpDateMonth( NULL );

		} elseif( ( CPaymentType::DISCOVER == $this->getPaymentTypeId() ) || ( CPaymentType::AMEX == $this->getPaymentTypeId() ) || ( CPaymentType::VISA == $this->getPaymentTypeId() ) || ( CPaymentType::MASTERCARD == $this->getPaymentTypeId() ) ) {

			$this->setCheckAccountNumber( NULL );
			$this->setCheckRoutingNumber( NULL );
			$this->setCheckNameOnAccount( NULL );
			$this->setCheckAccountTypeId( NULL );
			$this->setCheckBankName( NULL );
		}

		if( true == in_array( $this->m_intPaymentTypeId, [ CPaymentType::ACH, CPaymentType::CHECK_21, CPaymentType::PAD ] ) ) {
			$this->setCreditCardFieldsNull();
		} else {
			$this->setCheckFieldsNull();
		}

		// Override the user input and set the date to today
		if( true == $this->isElectronicPayment() ) {
			$this->setPaymentDatetime( date( 'm/d/Y H:i:s' ) );
			if( false == $this->valCheckAccountAndRoutingNumber() ) {
				return false;
			}
		}

		// Set receipt_datetime as payment_datetime (if null )
		if( true == is_null( $this->getReceiptDatetime() ) ) {
			$this->setReceiptDatetime( $this->m_strPaymentDatetime );
		}

		// We should never be including a database transaction on customer payment from a module.
		// We are inserting at least three records here (and maybe more in case of check21 images or mag strips), so if anything
		// fails, we roll back the transaction so we don't have incomplete data in the datbase.
		// We never go to the gateway until after the insert suceeds entirely so we don't have to worry about rolling
		// back a successful transaction.  This will cause a problem if someone attempts to use a transaction in a module that
		// invokes this insert request.

		// Any non-electronic payments should have this value set to true so we don't prematurely commit an external transaction.
		if( false == $boolOverrideTransaction ) {
			$objClientDatabase->begin();
			$objPaymentDatabase->begin();
		}

		$boolAdminTransaction = false;

		// We have some code here that forces an electronic transaction to commit explicitly in this insert function.
		// Electronic transactions can never rollback.  This code centralizes things so we're sure that electronic transactions will never rollback.
		if( true == $boolOverrideTransaction && false == $objPaymentDatabase->getIsTransaction() && true == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintElectronicPaymentTypeIds ) ) {
			$objPaymentDatabase->begin();
			$boolAdminTransaction = true;
		}

		// Insert the customer payment
		$objPaymentDataset = $objPaymentDatabase->createDataset();

		$intId = $this->getId();

		if( true == is_numeric( $this->m_intLeaseId ) && true == is_numeric( $this->m_intCustomerId ) ) {
			$objLease = $this->fetchLeaseWithLeaseStatusTypeId( $objClientDatabase );
			if( true == valObj( $objLease, 'CLease' ) ) {
				$this->setLeaseStatusTypeId( $objLease->getLeaseStatusTypeId() );
			}
		} else {
			$this->setLeaseStatusTypeId( NULL );
		}

		if( false == valId( $intId ) ) {
			$intId = $this->fetchNextId( $objPaymentDatabase );

			if( false == valId( $intId ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert customer payment record. Failed to fetch next payment id.' ) );
				return false;
			} else {
				$this->setId( $intId );
			}
		}

		$strSql = 'SELECT * ' .
			'FROM ar_payments_insert( row_to_json ( ROW ( ' .
			$this->sqlId() . ', ' .
			$this->sqlCid() . ', ' .
			$this->sqlCompanyMerchantAccountId() . ', ' .
			$this->sqlProcessingBankAccountId() . ', ' .
			$this->sqlMerchantGatewayId() . ', ' .
			$this->sqlPropertyId() . ', ' .
			$this->sqlCustomerId() . ', ' .
			$this->sqlLeaseId() . ', ' .
			$this->sqlPaymentTypeId() . ', ' .
			$this->sqlPaymentMediumId() . ', ' .
			$this->sqlPaymentStatusTypeId() . ', ' .
			$this->sqlReturnTypeId() . ', ' .
			$this->sqlArPaymentId() . ', ' .
			$this->sqlScheduledPaymentId() . ', ' .
			$this->sqlArCodeId() . ', ' .
			$this->sqlCustomerPaymentAccountId() . ', ' .
			$this->sqlArPaymentTransmissionId() . ', ' .
			$this->sqlArPaymentExportId() . ', ' .
			$this->sqlPsProductId() . ', ' .
			$this->sqlPsProductOptionId() . ', ' .
			$this->sqlCompanyCharityId() . ', ' .
			$this->sqlLeaseStatusTypeId() . ', ' .
			$this->sqlRemotePrimaryKey() . ', ' .
			$this->sqlReturnRemotePrimaryKey() . ', ' .
			$this->sqlPaymentDatetime() . ', ' .
			$this->sqlReceiptDatetime() . ', ' .
			$this->sqlPaymentAmount() . ', ' .
			$this->sqlConvenienceFeeAmount() . ', ' .
			$this->sqlDonationAmount() . ', ' .
			$this->sqlTotalAmount() . ', ' .
			$this->sqlPaymentCost() . ', ' .
			$this->sqlCompanyChargeAmount() . ', ' .
			$this->sqlIncentiveCompanyChargeAmount() . ', ' .
			$this->sqlBilltoUnitNumber() . ', ' .
			$this->sqlBilltoNameFirst() . ', ' .
			$this->sqlBilltoNameFirstEncrypted() . ', ' .
			$this->sqlBilltoNameMiddle() . ', ' .
			$this->sqlBilltoNameMiddleEncrypted() . ', ' .
			$this->sqlBilltoNameLast() . ', ' .
			$this->sqlBilltoNameLastEncrypted() . ', ' .
			$this->sqlCheckNumber() . ', ' .
			$this->sqlFedTrackingNumber() . ', ' .
			$this->sqlIsSplit() . ', ' .
			$this->sqlIsReversed() . ', ' .
			$this->sqlIsReconPrepped() . ', ' .
			$this->sqlIsDebitCard() . ', ' .
			$this->sqlBlockAssociation() . ', ' .
			$this->sqlOrderNum() . ', ' .
			$this->sqlLockSequence() . ', ' .
			$this->sqlTermsAcceptedOn() . ', ' .
			$this->sqlDistributionBlockedOn() . ', ' .
			$this->sqlAutoCaptureOn() . ', ' .
			$this->sqlDistributeOn() . ', ' .
			$this->sqlDistributeReturnOn() . ', ' .
			$this->sqlReturnedOn() . ', ' .
			$this->sqlBatchedOn() . ', ' .
			$this->sqlEftBatchedOn() . ', ' .
			$this->sqlCapturedOn() . ', ' .
			( int ) $intCurrentUserId . ', ' .
			$this->sqlUpdatedOn() . ', ' .
			( int ) $intCurrentUserId . ', ' .
			$this->sqlCreatedOn() . ', ' .
			$this->sqlCurrencyCode() . ', ' .
			$this->sqlCardTypeId() . ' ) ) ) AS result;';

		if( false == $objPaymentDataset->execute( $strSql ) ) {
			// Reset id on error
			$this->setId( $intId );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to insert customer payment record.' ) ) );

			$objPaymentDataset->cleanup();
			return false;
		}

		if( 0 < $objPaymentDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to insert customer payment record.' ) ) );
			// Reset id on error
			$this->setId( $intId );

			$objPaymentDataset->cleanup();
			return false;
		}

		$objPaymentDataset->cleanup();

		// Insert the customer payment
		$objClientDataset = $objClientDatabase->createDataset();

		$strSql = 'SELECT * ' .
			'FROM ar_payments_insert( row_to_json ( ROW ( ' .
			$this->sqlId() . ', ' .
			$this->sqlCid() . ', ' .
			$this->sqlCompanyMerchantAccountId() . ', ' .
			$this->sqlProcessingBankAccountId() . ', ' .
			$this->sqlMerchantGatewayId() . ', ' .
			$this->sqlPropertyId() . ', ' .
			$this->sqlCustomerId() . ', ' .
			$this->sqlLeaseId() . ', ' .
			$this->sqlPaymentTypeId() . ', ' .
			$this->sqlPaymentMediumId() . ', ' .
			$this->sqlPaymentStatusTypeId() . ', ' .
			$this->sqlReturnTypeId() . ', ' .
			$this->sqlArPaymentId() . ', ' .
			$this->sqlScheduledPaymentId() . ', ' .
			$this->sqlArCodeId() . ', ' .
			$this->sqlCustomerPaymentAccountId() . ', ' .
			$this->sqlArPaymentTransmissionId() . ', ' .
			$this->sqlArPaymentExportId() . ', ' .
			$this->sqlPsProductId() . ', ' .
			$this->sqlPsProductOptionId() . ', ' .
			$this->sqlCompanyCharityId() . ', ' .
			$this->sqlLeaseStatusTypeId() . ', ' .
			$this->sqlRemotePrimaryKey() . ', ' .
			$this->sqlReturnRemotePrimaryKey() . ', ' .
			$this->sqlPaymentDatetime() . ', ' .
			$this->sqlReceiptDatetime() . ', ' .
			$this->sqlPaymentAmount() . ', ' .
			$this->sqlConvenienceFeeAmount() . ', ' .
			$this->sqlDonationAmount() . ', ' .
			$this->sqlTotalAmount() . ', ' .
			$this->sqlPaymentCost() . ', ' .
			$this->sqlCompanyChargeAmount() . ', ' .
			$this->sqlIncentiveCompanyChargeAmount() . ', ' .
			$this->sqlBilltoUnitNumber() . ', ' .
			$this->sqlBilltoNameFirst() . ', ' .
			$this->sqlBilltoNameFirstEncrypted() . ', ' .
			$this->sqlBilltoNameMiddle() . ', ' .
			$this->sqlBilltoNameMiddleEncrypted() . ', ' .
			$this->sqlBilltoNameLast() . ', ' .
			$this->sqlBilltoNameLastEncrypted() . ', ' .
			$this->sqlCheckNumber() . ', ' .
			$this->sqlFedTrackingNumber() . ', ' .
			$this->sqlIsSplit() . ', ' .
			$this->sqlIsReversed() . ', ' .
			$this->sqlIsReconPrepped() . ', ' .
			$this->sqlIsDebitCard() . ', ' .
			$this->sqlBlockAssociation() . ', ' .
			$this->sqlOrderNum() . ', ' .
			$this->sqlLockSequence() . ', ' .
			$this->sqlTermsAcceptedOn() . ', ' .
			$this->sqlDistributionBlockedOn() . ', ' .
			$this->sqlAutoCaptureOn() . ', ' .
			$this->sqlDistributeOn() . ', ' .
			$this->sqlDistributeReturnOn() . ', ' .
			$this->sqlReturnedOn() . ', ' .
			$this->sqlBatchedOn() . ', ' .
			$this->sqlEftBatchedOn() . ', ' .
			$this->sqlCapturedOn() . ', ' .
			( int ) $intCurrentUserId . ', ' .
			$this->sqlUpdatedOn() . ', ' .
			$this->sqlCreatedOn() . ', ' .
			( int ) $intCurrentUserId . ', ' .
			$this->sqlCurrencyCode() . ', ' .
			$this->sqlCardTypeId() . ' ) ) ) AS result;';

		if( false == $objClientDataset->execute( $strSql ) ) {
			// Reset id on error
			$this->setId( $intId );

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to insert customer payment record.' ) ) );
			$objClientDataset->cleanup();

			return false;
		}

		if( 0 < $objClientDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to insert customer payment record.' ) ) );
			// Reset id on error
			$this->setId( $intId );

			$objClientDataset->cleanup();
			return false;
		}

		$objClientDataset->cleanup();

		// Insert the customer payment detail record
		$this->m_objArPaymentDetail = $this->createArPaymentDetail( $intCurrentUserId, $objClientDatabase );

		if( true == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) && true == is_null( $this->m_objArPaymentDetail->getSecureReferenceNumber() ) ) {

			$objPaymentEmailer = new CPaymentEmailer();
			$strSubject = 'CC ArPayment with NULL secure reference number';
			$strMessage = 'CC Ar Payment was processed with a null secure reference number.  Payment id: ' . $this->getId() . "\n";
			$strMessage .= print_r( debug_backtrace( DEBUG_BACKTRACE_IGNORE_ARGS, 10 ), true );
			$objPaymentEmailer->emailEmergencyTeam( $strMessage, $strSubject, CSystemEmail::$c_arrstrRPayRelatedEmailAddress );
		}

		if( false == $this->m_objArPaymentDetail->insert( $intCurrentUserId, $objPaymentDatabase ) ) {
			if( false == $boolOverrideTransaction ) {
				$objClientDatabase->rollback();
				$objPaymentDatabase->rollback();
			}
			$this->addErrorMsgs( $this->m_objArPaymentDetail->getErrorMsgs() );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to insert customer payment record. Failed to insert payment details.' ) ) );
			return false;
		}

		if( false == $this->m_objArPaymentDetail->insert( $intCurrentUserId, $objClientDatabase ) ) {
			if( false == $boolOverrideTransaction ) {
				$objClientDatabase->rollback();
				$objPaymentDatabase->rollback();
			}
			$this->addErrorMsgs( $this->m_objArPaymentDetail->getErrorMsgs() );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to insert customer payment record. Failed to insert payment details.' ) ) );
			return false;
		}

		if( CPaymentMedium::TERMINAL == $this->m_intPaymentMediumId && CPaymentType::CHECK_21 == $this->m_intPaymentTypeId ) {

			$this->m_objArPaymentMagstrip->setArPaymentId( $this->m_intId );

			if( false == $this->m_objArPaymentMagstrip->insert( $intCurrentUserId, $objPaymentDatabase ) ) {
				if( false == $boolOverrideTransaction ) {
					$objClientDatabase->rollback();
					$objPaymentDatabase->rollback();
				}
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to insert customer payment record. Failed to insert payment magstrip.' ) ) );
				return false;
			}

			// If this is a check 21 payment, insert the 2 CArPaymentImage objects
			if( $this->m_intPaymentTypeId == CPaymentType::CHECK_21 ) {

				$this->m_objArPaymentImageFront->setArPaymentId( $this->m_intId );
				$this->m_objArPaymentImageFront->setImageName( 'image_front_ma' . $this->m_intCompanyMerchantAccountId . '_id' . $this->m_intId . '.tif' );
				$this->m_objArPaymentImageFront->setPaymentDatabase( $objPaymentDatabase );
				$this->m_objArPaymentImageReverse->setPaymentDatabase( $objPaymentDatabase );

				$strReverseImageName = 'image_back_ma' . $this->m_intCompanyMerchantAccountId . '_id' . $this->m_intId . '.tif';
				$strEndorsedReverseImageName = 'endorsed_' . $strReverseImageName;

				$boolEndorsedImageExists = false;
				if( true == file_exists( $this->m_objArPaymentImageReverse->getImagePath() . $strEndorsedReverseImageName ) || false == is_null( $this->m_objArPaymentImageReverse->getEndorsedOn() ) ) {
					$boolEndorsedImageExists = true;
				}

				$this->m_objArPaymentImageReverse->setArPaymentId( $this->m_intId );

				if( true == $boolEndorsedImageExists ) {
					$this->m_objArPaymentImageReverse->setImageName( $strEndorsedReverseImageName );
					if( false != is_null( $this->m_objArPaymentImageReverse->getEndorsedOn() ) ) {
						$this->m_objArPaymentImageReverse->setEndorsedOn( date( 'm/d/Y H:i:s' ) );
					}
				} else {
					$this->m_objArPaymentImageReverse->setImageName( $strReverseImageName );
				}

				$this->m_objArPaymentImageFront->setId( $this->m_objArPaymentImageFront->fetchNextId( $objPaymentDatabase ) );
				$this->m_objArPaymentImageFront->setUserId( $intCurrentUserId );
				if( false == $this->m_objArPaymentImageFront->writeImageContentToFileSystem() ) {
					if( false == $boolOverrideTransaction ) {
						$objClientDatabase->rollback();
						$objPaymentDatabase->rollback();
					}
					$this->addErrorMsgs( $this->m_objArPaymentImageFront->getErrorMsgs() );
					return false;
				}
				$this->m_objArPaymentImageFront->calculateImageSizeAndResolution();

				// If validation of check images has not been performed, then insert will fail
				if( false == $this->m_objArPaymentImageFront->insert( $intCurrentUserId, $objPaymentDatabase ) ) {
					if( false == $boolOverrideTransaction ) {
						$objClientDatabase->rollback();
						$objPaymentDatabase->rollback();
					}
					$this->addErrorMsgs( $this->m_objArPaymentImageFront->getErrorMsgs() );
					return false;
				}

				$this->m_objArPaymentImageReverse->setId( $this->m_objArPaymentImageReverse->fetchNextId( $objPaymentDatabase ) );
				$this->m_objArPaymentImageReverse->setUserId( $intCurrentUserId );
				if( false == $this->m_objArPaymentImageReverse->writeImageContentToFileSystem() ) {
					if( false == $boolOverrideTransaction ) {
						$objClientDatabase->rollback();
						$objPaymentDatabase->rollback();
					}
					$this->addErrorMsgs( $this->m_objArPaymentImageReverse->getErrorMsgs() );
					return false;
				}
				$this->m_objArPaymentImageReverse->calculateImageSizeAndResolution();

				// If validation of check images has not been performed, then insert will fail
				if( false == $this->m_objArPaymentImageReverse->insert( $intCurrentUserId, $objPaymentDatabase ) ) {
					if( false == $boolOverrideTransaction ) {
						$objClientDatabase->rollback();
						$objPaymentDatabase->rollback();
					}
					$this->addErrorMsgs( $this->m_objArPaymentImageReverse->getErrorMsgs() );
					return false;
				}

				if( false == $boolEndorsedImageExists ) {
					// Set to unendorsed image name so that the unendorsed version gets written (If exists, endorsed version has already been written)
					$this->m_objArPaymentImageReverse->setImageName( $strReverseImageName );
				} else {
					$this->m_objArPaymentImageReverse->setImageName( $strEndorsedReverseImageName );
				}

				if( false == $this->m_objArPaymentImageFront->writePreRenderedFrontCacheFiles( $this->m_objArPaymentImageFront->getCheckFrontGifImageChild( 'front' ), $this->m_objArPaymentImageFront->getCheckFrontGifImageChild( 'frontThumb' ), $this->m_objArPaymentImageFront->getCheckFrontGifImageChild( 'frontMasked' ), $this->m_objArPaymentImageFront->getCheckFrontGifImageChild( 'frontThumbMasked' ) ) ) {
					if( false == $boolOverrideTransaction ) {
						$objClientDatabase->rollback();
						$objPaymentDatabase->rollback();
					}
					$this->addErrorMsgs( $this->m_objArPaymentImageFront->getErrorMsgs() );
					return false;
				}

				if( false == $this->m_objArPaymentImageReverse->writePreRenderedReverseCacheFiles( $this->m_objArPaymentImageReverse->getCheckReverseGifImageChild( 'reverse' ), $this->m_objArPaymentImageReverse->getCheckReverseGifImageChild( 'reverseThumb' ), $this->m_objArPaymentImageReverse->getCheckReverseGifImageChild( 'reverseEndorsed' ), $this->m_objArPaymentImageReverse->getCheckReverseGifImageChild( 'reverseThumbEndorsed' ) ) ) {
					if( false == $boolOverrideTransaction ) {
						$objClientDatabase->rollback();
						$objPaymentDatabase->rollback();
					}
					$this->addErrorMsgs( $this->m_objArPaymentImageReverse->getErrorMsgs() );
					return false;
				}
			}
		}

		// Insert the payment billing record
		$this->m_objArPaymentBilling = $this->createArPaymentBilling();

		if( $this->m_intPaymentTypeId == CPaymentType::CHECK_21 ) {

			$objMerchantAccount = $this->getOrFetchMerchantAccount( $objClientDatabase );

			if( false == valObj( $objMerchantAccount, 'CMerchantAccount' ) ) {
				trigger_error( 'Merchant account failed to load.', E_USER_ERROR );
				exit;
			}

			if( 1 == $objMerchantAccount->getConvertCheck21ToAch() ) {
				$this->m_objArPaymentBilling->calculateAndSetCheckIsConverted( $this->m_objArPaymentImageFront, $this->m_intCompanyMerchantAccountId, $this->m_intCustomerId, $this->m_intCid, $objPaymentDatabase, $objClientDatabase );
			}
		}

		if( false == $this->m_objArPaymentBilling->insert( $intCurrentUserId, $objPaymentDatabase ) ) {
			if( false == $boolOverrideTransaction ) {
				$objClientDatabase->rollback();
				$objPaymentDatabase->rollback();
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to insert customer payment record. Failed to insert payment billing.' ) ) );
			return false;
		}

		$arrstrData = fetchData( 'SELECT updated_on FROM ar_payments WHERE id = ' . $this->getId() . ' AND cid = ' . $this->getCid(), $objPaymentDatabase );

		if( true == isset( $arrstrData[0]['updated_on'] ) ) {
			$this->setUpdatedOn( $arrstrData[0]['updated_on'] );
		}

		if( true == valArr( $this->getArPaymentSplits() ) ) {
			foreach( $this->getArPaymentSplits() as $objArPaymentSplit ) {
				$objArPaymentSplit->setArPaymentId( $this->getId() );
				if( true == valArr( $objArPaymentSplit->getErrorMsgs() || false == $objArPaymentSplit->insert( $intCurrentUserId, $objClientDatabase ) ) ) {
					$this->setPaymentStatusTypeId( CPaymentStatusType::PENDING );
					return false;
				}
			}
		}

		if( true == $boolAdminTransaction ) {
			$objPaymentDatabase->commit();
		}

		if( false == $boolOverrideTransaction ) {
			$objClientDatabase->commit();
			$objPaymentDatabase->commit();
		}

		if( true == $this->isElectronicPayment() ) {
			$this->valCheckAccountAndRoutingNumber( true );
		}

		return true;
	}

	public function update( $intCurrentUserId, $objClientDatabase, $objPaymentDatabase = NULL ) {

		/**
		 * Checking Entrata Module write permission, if not then insert/update/delete would be restricted.
		 */

		if( false == checkEntrataModuleWritePermission() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'You have not been granted write access to this module.' ) ) );
			return false;
		}

		if( false == valObj( $objClientDatabase, 'CDatabase' ) || CDatabaseType::CLIENT != $objClientDatabase->getDatabaseTypeId()
			|| false == valObj( $objPaymentDatabase, 'CDatabase' ) || CDatabaseType::PAYMENT != $objPaymentDatabase->getDatabaseTypeId() ) {
			trigger_error( 'Failed to load database object', E_USER_ERROR );
			return false;
		}

		$boolIsValid = true;

		// We have an issue that happens when a lease, unit, property, etc gets deleted in works without admin knowing.  If lease gets
		// deleted in works, the lease_id stays on the ar_payment object in admin.  If later, the payment needs to be updated, the lease id will
		// attempt to be updated in works, causing an fkey violation.  This is a big problem, so everytime we attempt to update an ar_payment,
		// we need to validate that the fkeys are still valid.

		// Check for blank payment id
		if( true == is_null( $this->getId() ) ) {
			trigger_error( 'AR Payment Id is passed as NULL.' . print_r( debug_backtrace( DEBUG_BACKTRACE_IGNORE_ARGS ), true ), E_USER_WARNING );
		}

		// Check for lease id
		if( true == is_numeric( $this->getLeaseId() ) ) {
			$arrstrData = fetchData( 'SELECT id FROM leases WHERE id = ' . ( int ) $this->getLeaseId() . ' AND cid = ' . ( int ) $this->getCid(), $objClientDatabase );
			if( false == isset( $arrstrData[0]['id'] ) || false == is_numeric( $arrstrData[0]['id'] ) ) $this->setLeaseId( NULL );
		}

		// Check for customer id
		if( true == is_numeric( $this->getCustomerId() ) ) {
			$arrstrData = fetchData( 'SELECT id FROM customers WHERE id = ' . ( int ) $this->getCustomerId() . ' AND cid = ' . ( int ) $this->getCid(), $objClientDatabase );
			if( false == isset( $arrstrData[0]['id'] ) || false == is_numeric( $arrstrData[0]['id'] ) ) $this->setCustomerId( NULL );
		}

		// Check for ar code?
		if( true == is_numeric( $this->getArCodeId() ) ) {
			$arrstrData = fetchData( 'SELECT id FROM ar_codes WHERE id = ' . ( int ) $this->getArCodeId() . ' AND cid = ' . ( int ) $this->getCid(), $objClientDatabase );
			if( false == isset( $arrstrData[0]['id'] ) || false == is_numeric( $arrstrData[0]['id'] ) ) $this->setArCodeId( NULL );
		}

		// Customer payment account
		if( true == is_numeric( $this->getCustomerPaymentAccountId() ) ) {
			$arrstrData = fetchData( 'SELECT id FROM customer_payment_accounts WHERE id = ' . ( int ) $this->getCustomerPaymentAccountId() . ' AND cid = ' . ( int ) $this->getCid(), $objClientDatabase );
			if( false == isset( $arrstrData[0]['id'] ) || false == is_numeric( $arrstrData[0]['id'] ) ) $this->setCustomerPaymentAccountId( NULL );
		}

		// Ar payment transmission
		if( true == is_numeric( $this->getArPaymentTransmissionId() ) ) {
			$arrstrData = fetchData( 'SELECT id FROM ar_payment_transmissions WHERE id = ' . ( int ) $this->getArPaymentTransmissionId() . ' AND cid = ' . ( int ) $this->getCid(), $objClientDatabase );
			if( false == isset( $arrstrData[0]['id'] ) || false == is_numeric( $arrstrData[0]['id'] ) ) $this->setArPaymentTransmissionId( NULL );
		}

		// Ar payment export
		if( true == is_numeric( $this->getArPaymentExportId() ) ) {
			$arrstrData = fetchData( 'SELECT id FROM ar_payment_exports WHERE id = ' . ( int ) $this->getArPaymentExportId() . ' AND cid = ' . ( int ) $this->getCid(), $objClientDatabase );
			if( false == isset( $arrstrData[0]['id'] ) || false == is_numeric( $arrstrData[0]['id'] ) ) $this->setArPaymentExportId( NULL );
		}

		// Check for ar deposit

		// ********************************************************************************************************************************************************
		// Concurrency Protection
		//  Check to assure we are in a transaction, if not start one
		//  Check Admin db ar_payments to see if row has changed since object was selected. Use FOR UPDATE to lock out other concurrent writers
		//  Use string comparison to include fractional second differences. e.g. '03/14/2010 20:50:25.146343 MDT' should not match '03/14/2010 20:50:25.146344 MDT'
		// ********************************************************************************************************************************************************

		$boolAdminTransaction = false;

		if( false == $objPaymentDatabase->getIsTransaction() ) {
			$objPaymentDatabase->begin();
			$boolAdminTransaction = true;
		}

		$arrstrData = fetchData( 'SELECT updated_on FROM ar_payments WHERE id = ' . ( int ) $this->getId() . ' AND cid = ' . ( int ) $this->getCid() . ' FOR UPDATE;', $objPaymentDatabase );
		if( true == isset( $arrstrData[0]['updated_on'] ) && ( strtotime( $arrstrData[0]['updated_on'] ) != strtotime( $this->getUpdatedOn() ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Concurrency error.  Please resubmit your request. Current Value: ' . strtotime( $arrstrData[0]['updated_on'] ) . ', New Value: ' . strtotime( $this->getUpdatedOn() ) ) );
			trigger_error( 'Concurrency error.  Please resubmit your request:' . $this->getId(), E_USER_WARNING );
			if( true == $boolAdminTransaction ) {
				$objPaymentDatabase->rollback();
			}

			return false;
		}
		if( true == is_numeric( $this->m_intLeaseId ) && true == is_numeric( $this->m_intCustomerId ) ) {
			$objLease = $this->fetchLeaseWithLeaseStatusTypeId( $objClientDatabase );
			if( true == valObj( $objLease, 'CLease' ) ) {
				$this->setLeaseStatusTypeId( $objLease->getLeaseStatusTypeId() );
			}
		} else {
			$this->setLeaseStatusTypeId( NULL );
		}
		$objPaymentDataset = $objPaymentDatabase->createDataset();

		$strSql = 'SELECT * ' .
			'FROM ar_payments_update( row_to_json ( ROW ( ' .
			$this->sqlId() . ', ' .
			$this->sqlCid() . ', ' .
			$this->sqlCompanyMerchantAccountId() . ', ' .
			$this->sqlProcessingBankAccountId() . ', ' .
			$this->sqlMerchantGatewayId() . ', ' .
			$this->sqlPropertyId() . ', ' .
			$this->sqlCustomerId() . ', ' .
			$this->sqlLeaseId() . ', ' .
			$this->sqlPaymentTypeId() . ', ' .
			$this->sqlPaymentMediumId() . ', ' .
			$this->sqlPaymentStatusTypeId() . ', ' .
			$this->sqlReturnTypeId() . ', ' .
			$this->sqlArPaymentId() . ', ' .
			$this->sqlScheduledPaymentId() . ', ' .
			$this->sqlArCodeId() . ', ' .
			$this->sqlCustomerPaymentAccountId() . ', ' .
			$this->sqlArPaymentTransmissionId() . ', ' .
			$this->sqlArPaymentExportId() . ', ' .
			$this->sqlPsProductId() . ', ' .
			$this->sqlPsProductOptionId() . ', ' .
			$this->sqlCompanyCharityId() . ', ' .
			$this->sqlLeaseStatusTypeId() . ', ' .
			$this->sqlRemotePrimaryKey() . ', ' .
			$this->sqlReturnRemotePrimaryKey() . ', ' .
			$this->sqlPaymentDatetime() . ', ' .
			$this->sqlReceiptDatetime() . ', ' .
			$this->sqlPaymentAmount() . ', ' .
			$this->sqlConvenienceFeeAmount() . ', ' .
			$this->sqlDonationAmount() . ', ' .
			$this->sqlTotalAmount() . ', ' .
			$this->sqlPaymentCost() . ', ' .
			$this->sqlCompanyChargeAmount() . ', ' .
			$this->sqlIncentiveCompanyChargeAmount() . ', ' .
			$this->sqlBilltoUnitNumber() . ', ' .
			$this->sqlBilltoNameFirst() . ', ' .
			$this->sqlBilltoNameFirstEncrypted() . ', ' .
			$this->sqlBilltoNameMiddle() . ', ' .
			$this->sqlBilltoNameMiddleEncrypted() . ', ' .
			$this->sqlBilltoNameLast() . ', ' .
			$this->sqlBilltoNameLastEncrypted() . ', ' .
			$this->sqlCheckNumber() . ', ' .
			$this->sqlFedTrackingNumber() . ', ' .
			$this->sqlIsSplit() . ', ' .
			$this->sqlIsReversed() . ', ' .
			$this->sqlIsReconPrepped() . ', ' .
			$this->sqlIsDebitCard() . ', ' .
			$this->sqlBlockAssociation() . ', ' .
			$this->sqlOrderNum() . ', ' .
			$this->sqlLockSequence() . ', ' .
			$this->sqlTermsAcceptedOn() . ', ' .
			$this->sqlDistributionBlockedOn() . ', ' .
			$this->sqlAutoCaptureOn() . ', ' .
			$this->sqlDistributeOn() . ', ' .
			$this->sqlDistributeReturnOn() . ', ' .
			$this->sqlReturnedOn() . ', ' .
			$this->sqlBatchedOn() . ', ' .
			$this->sqlEftBatchedOn() . ', ' .
			$this->sqlCapturedOn() . ', ' .
			( int ) $intCurrentUserId . ', ' .
			$this->sqlUpdatedOn() . ', ' .
			$this->sqlCurrencyCode() . ', ' .
			$this->sqlCardTypeId() . ' ) ) ) AS result;';

		if( false == $objPaymentDataset->execute( $strSql ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to update customer payment record. The following error was reported.' ) ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objPaymentDatabase->errorMsg() ) );

			$objPaymentDataset->cleanup();

			if( true == $boolAdminTransaction ) {
				$objPaymentDatabase->rollback();
			}

			return false;
		}

		if( 0 < $objPaymentDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to update customer payment record. The following error was reported.' ) ) );
			while( false == $objPaymentDataset->eof() ) {
				$arrmixValues = $objPaymentDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
				$objPaymentDataset->next();
			}

			$objPaymentDataset->cleanup();

			if( true == $boolAdminTransaction ) {
				$objPaymentDatabase->rollback();
			}

			return false;
		}

		$objPaymentDataset->cleanup();

		$arrstrData = fetchData( 'SELECT updated_on FROM ar_payments WHERE id = ' . ( int ) $this->getId() . ' AND cid = ' . ( int ) $this->getCid(), $objPaymentDatabase );

		if( true == isset( $arrstrData[0]['updated_on'] ) ) {
			$this->setUpdatedOn( $arrstrData[0]['updated_on'] );
		}

		if( true == $boolAdminTransaction ) {
			$objPaymentDatabase->commit();
		}

		$objClientDataset = $objClientDatabase->createDataset();

		$strSql = 'SELECT * ' .
			'FROM ar_payments_update( row_to_json ( ROW ( ' .
			$this->sqlId() . ', ' .
			$this->sqlCid() . ', ' .
			$this->sqlCompanyMerchantAccountId() . ', ' .
			$this->sqlProcessingBankAccountId() . ', ' .
			$this->sqlMerchantGatewayId() . ', ' .
			$this->sqlPropertyId() . ', ' .
			$this->sqlCustomerId() . ', ' .
			$this->sqlLeaseId() . ', ' .
			$this->sqlPaymentTypeId() . ', ' .
			$this->sqlPaymentMediumId() . ', ' .
			$this->sqlPaymentStatusTypeId() . ', ' .
			$this->sqlReturnTypeId() . ', ' .
			$this->sqlArPaymentId() . ', ' .
			$this->sqlScheduledPaymentId() . ', ' .
			$this->sqlArCodeId() . ', ' .
			$this->sqlCustomerPaymentAccountId() . ', ' .
			$this->sqlArPaymentTransmissionId() . ', ' .
			$this->sqlArPaymentExportId() . ', ' .
			$this->sqlPsProductId() . ', ' .
			$this->sqlPsProductOptionId() . ', ' .
			$this->sqlCompanyCharityId() . ', ' .
			$this->sqlLeaseStatusTypeId() . ', ' .
			$this->sqlRemotePrimaryKey() . ', ' .
			$this->sqlReturnRemotePrimaryKey() . ', ' .
			$this->sqlPaymentDatetime() . ', ' .
			$this->sqlReceiptDatetime() . ', ' .
			$this->sqlPaymentAmount() . ', ' .
			$this->sqlConvenienceFeeAmount() . ', ' .
			$this->sqlDonationAmount() . ', ' .
			$this->sqlTotalAmount() . ', ' .
			$this->sqlPaymentCost() . ', ' .
			$this->sqlCompanyChargeAmount() . ', ' .
			$this->sqlIncentiveCompanyChargeAmount() . ', ' .
			$this->sqlBilltoUnitNumber() . ', ' .
			$this->sqlBilltoNameFirst() . ', ' .
			$this->sqlBilltoNameFirstEncrypted() . ', ' .
			$this->sqlBilltoNameMiddle() . ', ' .
			$this->sqlBilltoNameMiddleEncrypted() . ', ' .
			$this->sqlBilltoNameLast() . ', ' .
			$this->sqlBilltoNameLastEncrypted() . ', ' .
			$this->sqlCheckNumber() . ', ' .
			$this->sqlFedTrackingNumber() . ', ' .
			$this->sqlIsSplit() . ', ' .
			$this->sqlIsReversed() . ', ' .
			$this->sqlIsReconPrepped() . ', ' .
			$this->sqlIsDebitCard() . ', ' .
			$this->sqlBlockAssociation() . ', ' .
			$this->sqlOrderNum() . ', ' .
			$this->sqlLockSequence() . ', ' .
			$this->sqlTermsAcceptedOn() . ', ' .
			$this->sqlDistributionBlockedOn() . ', ' .
			$this->sqlAutoCaptureOn() . ', ' .
			$this->sqlDistributeOn() . ', ' .
			$this->sqlDistributeReturnOn() . ', ' .
			$this->sqlReturnedOn() . ', ' .
			$this->sqlBatchedOn() . ', ' .
			$this->sqlEftBatchedOn() . ', ' .
			$this->sqlCapturedOn() . ', ' .
			( int ) $intCurrentUserId . ', ' .
			$this->sqlUpdatedOn() . ', ' .
			$this->sqlCurrencyCode() . ', ' .
			$this->sqlCardTypeId() . ' ) ) ) AS result;';

		if( false == $objClientDataset->execute( $strSql ) ) {
			trigger_error( 'Ar Payment ID [' . $this->getId() . '] failed to update.' . $objClientDatabase->errorMsg(), E_USER_WARNING );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to update customer payment record. The following error was reported.' ) ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objClientDatabase->errorMsg() ) );

			$objClientDataset->cleanup();

			return false;
		}

		if( 0 < $objClientDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to update customer payment record. The following error was reported.' ) ) );
			while( false == $objClientDataset->eof() ) {
				$arrmixValues = $objClientDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
				$objClientDataset->next();
			}

			$objClientDataset->cleanup();

			return false;
		}

		$objClientDataset->cleanup();

		// Update ArPaymentDetail details field with updated_customer_id
		if( true == \Psi\Libraries\UtilFunctions\valId( $this->getUpdatedCustomerId() ) ) {
			$this->m_objArPaymentDetail = $this->updateArPaymentDetail( $objPaymentDatabase );
			if ( true == \Psi\Libraries\UtilFunctions\valObj( $this->m_objArPaymentDetail, \CArPaymentDetail::class ) ) {
				$this->m_objArPaymentDetail->update( $intCurrentUserId, $objClientDatabase );
			}
		}

		if( CPaymentMedium::TERMINAL == $this->m_intPaymentMediumId ) {

			if( true == valObj( $this->m_objArPaymentMagstrip, 'CArPaymentMagstrip' ) && false == $this->m_objArPaymentMagstrip->update( $intCurrentUserId, $objPaymentDatabase ) ) {
				$this->addErrorMsgs( $this->m_objArPaymentMagstrip->getErrorMsgs() );
				return false;
			}

			// If this is a check 21 payment, insert the 2 CArPaymentImage objects
			if( $this->m_intPaymentTypeId == CPaymentType::CHECK_21 ) {

				if( true == valObj( $this->m_objArPaymentImageFront, 'CArPaymentImage' ) && false == $this->m_objArPaymentImageFront->update( $intCurrentUserId, $objPaymentDatabase ) ) {
					$this->addErrorMsgs( $this->m_objArPaymentImageFront->getErrorMsgs() );
					return false;
				}

				if( true == valObj( $this->m_objArPaymentImageReverse, 'CArPaymentImage' ) && false == $this->m_objArPaymentImageReverse->update( $intCurrentUserId, $objPaymentDatabase ) ) {
					$this->addErrorMsgs( $this->m_objArPaymentImageReverse->getErrorMsgs() );
					return false;
				}

				$intCustomerSettingCount = CCustomerSettings::fetchCustomerSettingCountByKeyByCustomerIdByCid( $this->m_intCustomerId, $this->m_intCid, $objClientDatabase );
				$objMerchantAccount = $this->getOrFetchMerchantAccount( $objClientDatabase );

				if( false == valObj( $objMerchantAccount, 'CMerchantAccount' ) ) {
					trigger_error( 'Merchant account failed to load.', E_USER_ERROR );
					exit;
				}

				$boolCheckIsConverted = true;
				if( ( ( 1 == $objMerchantAccount->getConvertCheck21ToAch() && 0 < $intCustomerSettingCount ) || false == $this->getCheck21ConversionAmountValidation() ) || 0 == $objMerchantAccount->getConvertCheck21ToAch() || true == $this->getCheckIsMoneyOrder() ) {
					$boolCheckIsConverted = false;
				}

				$this->m_objArPaymentBilling = \Psi\Eos\Payment\CArPaymentBillings::createService()->fetchArPaymentBillingByCidByArPaymentId( $this->m_intCid, $this->m_intId, $objPaymentDatabase );
				if( false == valObj( $this->m_objArPaymentBilling, 'CArPaymentBilling' ) ) {
					trigger_error( 'Ar Payment Billing failed to load.', E_USER_ERROR );
					exit;
				}

				if( false == $boolCheckIsConverted ) {
					$this->m_objArPaymentBilling->setCheckIsConverted( 0 );
				} else {
					$this->m_objArPaymentBilling->setCheckIsConverted( 1 );
				}

				if( false == $this->m_objArPaymentBilling->update( $intCurrentUserId, $objPaymentDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to update ar payment billing. Following error was reported.' ) ) );
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objPaymentDatabase->errorMsg() ) );
					return false;
				}
			}
		}

		// This will set PAT(payment_allowance_type_id) in customers table based on NSF settings
		if( true == valStr( $this->getCustomerId() ) && true == valStr( $this->getLeaseId() ) && false == is_null( $this->getPaymentStatusTypeId() ) && CPaymentStatusType::RECALL == $this->getPaymentStatusTypeId() ) {

			$strSql = 'SELECT * FROM func_set_customers_payment_allowance_type( ' . ( int ) $this->getCustomerId() . ', ' . ( int ) $this->getCid() . ', ' . ( int ) $intCurrentUserId . ' ) AS result;';

			if( false == $objClientDataset->execute( $strSql ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update customer payment allowance type id. Following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objClientDatabase->errorMsg() ) );
				return false;
			}
		}

		return $boolIsValid;
	}

	/**
	 * Updates the distribute_on field ONLY
	 *
	 * @param $intUserId
	 * @param $objClientDatabase
	 * @param $objPaymentDatabase
	 * @return bool
	 */
	public function updateDistributeOnDate( $intUserId, $objClientDatabase, $objPaymentDatabase ) {
		if( !valStr( $this->getDistributeOn() ) ) {
			return false;
		}

		$strSql = sprintf( "UPDATE ar_payments SET distribute_on = '%s', updated_by = %d, updated_on = NOW() WHERE id = %d", $this->getDistributeOn(), ( int ) $intUserId, ( int ) $this->getId() );

		/** Update payments record */
		if( false === fetchData( $strSql, $objPaymentDatabase ) ) {
			return false;
		}

		/** Update entrata record */
		if( false === fetchData( $strSql, $objClientDatabase ) ) {
			return false;
		}

		return true;
	}

	public function delete( $intCurrentUserId, $objClientDatabase, $objPaymentDatabase = NULL ) {

		if( false == valObj( $objClientDatabase, 'CDatabase' ) || CDatabaseType::CLIENT != $objClientDatabase->getDatabaseTypeId() || false == valObj( $objPaymentDatabase, 'CDatabase' ) || CDatabaseType::PAYMENT != $objPaymentDatabase->getDatabaseTypeId() ) {
			trigger_error( 'Failed to load database object', E_USER_ERROR );
			return false;
		}

		if( false == in_array( $this->getPaymentTypeId(), [ CPaymentType::CASH, CPaymentType::CHECK, CPaymentType::MONEY_ORDER, CPaymentType::CHECK_21 ] ) ) {
			trigger_error( 'This is not the allowed payment type for deletion. Payment Id [' . $this->getId() . '] & Payment type [' . $this->getPaymentTypeId() . ']', E_USER_WARNING );
			return false;
		}

		if( CPaymentType::CHECK_21 == $this->getPaymentTypeId() ) {

			$arrobjArPaymentImages 	= $this->loadArPaymentImages( $objPaymentDatabase );
			$objArPaymentMagstrip	= $this->loadArPaymentMagstrip( $objPaymentDatabase );

			if( true == valArr( $arrobjArPaymentImages ) ) {
				foreach( $arrobjArPaymentImages as $objArPaymentImage ) {
					if( false == $objArPaymentImage->delete( $intCurrentUserId, $objPaymentDatabase ) ) {
						$this->addErrorMsg( new CErrorMsg( '', '', 'Failed to delete customer payment image.' ) );
						trigger_error( 'Failed to delete customer payment image. Payment Id [' . $this->getId() . '].', E_USER_WARNING );
						return false;
					}
				}
			}

			if( true == valObj( $objArPaymentMagstrip, 'CArPaymentMagstrip' ) ) {
				if( false == $objArPaymentMagstrip->delete( $intCurrentUserId, $objPaymentDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( '', '', 'Failed to delete customer payment magstrip.' ) );
					trigger_error( 'Failed to delete customer payment image. Payment Id [' . $this->getId() . '].', E_USER_WARNING );
					return false;
				}
			}
		}

		if( false == parent::delete( $intCurrentUserId, $objPaymentDatabase ) ) return false;
		return parent::delete( $intCurrentUserId, $objClientDatabase );
	}

	/**
	 * Other Functions
	 *
	 */

	public function setCheckFieldsNull() {
		$this->m_strCheckBankName	 				= NULL;
		$this->m_strCheckDate 						= NULL;
		$this->m_strCheckPayableTo 					= NULL;
		$this->m_strCheckRoutingNumber				= NULL;
		$this->m_strCheckAccountNumberEncrypted 	= NULL;
		$this->m_strCheckNameOnAccount				= NULL;
		$this->m_intCheckAccountTypeId				= NULL;
	}

	public function setCreditCardFieldsNull() {
		$this->m_strCcCardNumberEncrypted 	= NULL;
		$this->m_strCcExpDateMonth	 		= NULL;
		$this->m_strCcExpDateYear 			= NULL;
		$this->m_strCcNameOnCard 			= NULL;
	}

	public function refreshRemotePrimaryKey( $objPaymentDatabase ) {

		$strSql = 'SELECT remote_primary_key FROM ar_payments WHERE id = ' . $this->getId() . ' AND cid = ' . $this->getCid();
		$arrstrRemotePrimaryKey = fetchData( $strSql, $objPaymentDatabase );

		if( true == isset( $arrstrRemotePrimaryKey[0]['remote_primary_key'] ) ) {
			$this->setRemotePrimaryKey( $arrstrRemotePrimaryKey[0]['remote_primary_key'] );
		}
	}

	public function refreshReturnRemotePrimaryKey( $objPaymentDatabase ) {

		$strSql = 'SELECT return_remote_primary_key FROM ar_payments WHERE id = ' . $this->getId() . ' AND cid = ' . $this->getCid();
		$arrstrRemotePrimaryKey = fetchData( $strSql, $objPaymentDatabase );

		if( true == isset( $arrstrRemotePrimaryKey[0]['return_remote_primary_key'] ) ) {
			$this->setReturnRemotePrimaryKey( $arrstrRemotePrimaryKey[0]['return_remote_primary_key'] );
		}
	}

	public function refreshReturnBatchedOn( $objPaymentDatabase ) {

		$strSql = 'SELECT return_batched_on FROM ar_payment_details WHERE ar_Payment_id = ' . $this->getId() . ' AND cid = ' . $this->getCid();
		$arrstrReturnBatchedOn = fetchData( $strSql, $objPaymentDatabase );

		if( true == isset( $arrstrReturnBatchedOn[0]['return_batched_on'] ) ) {
			$this->setReturnBatchedOn( $arrstrReturnBatchedOn[0]['return_batched_on'] );
		}
	}

	// Set billto name first, name last, and unit number

	public function loadDefaultBillingInfo( $objDatabase ) {

		$objPropertyUnit = ( true == isset( $this->m_objLease ) ) ? $this->m_objLease->fetchPropertyUnit( $objDatabase ) : NULL;

		if( true == valObj( $objPropertyUnit, 'CPropertyUnit' ) ) {
			$arrobjUnitSpaces = $objPropertyUnit->fetchUnitSpaces( $objDatabase );

			$strUnitNumber = $objPropertyUnit->getUnitNumber();

			if( true == valArr( $arrobjUnitSpaces, 2 ) && true == is_numeric( $this->m_objLease->getUnitSpaceId() ) && true == array_key_exists( $this->m_objLease->getUnitSpaceId(), $arrobjUnitSpaces ) ) {
				$strUnitNumber .= '-' . $arrobjUnitSpaces[$this->m_objLease->getUnitSpaceId()]->getSpaceNumber();
			}

			if( true == is_null( $this->getBilltoUnitNumber() ) ) $this->setBilltoUnitNumber( $strUnitNumber );
		}

		if( true == valObj( $this->m_objCustomer, 'CCustomer' ) ) {
			if( true == is_null( $this->getBilltoNameFirst() ) )	$this->setBilltoNameFirst( $this->m_objCustomer->getNameFirst() );
			if( true == is_null( $this->getBilltoNameFirst() ) )	$this->setBilltoNameFirstEncrypted( $this->m_objCustomer->getNameFirst() );
			if( true == is_null( $this->getBilltoNameLast() ) )		$this->setBilltoNameLast( $this->m_objCustomer->getNameLast() );
			if( true == is_null( $this->getBilltoNameLast() ) )		$this->setBilltoNameLastEncrypted( $this->m_objCustomer->getNameLast() );
			if( true == is_null( $this->getBilltoPhoneNumber() ) ) 	$this->setBilltoPhoneNumber( $this->m_objCustomer->getAvailableCustomerPhoneNumber( $objDatabase ) );
			if( true == is_null( $this->getBilltoStreetLine1() ) ) 	$this->setBilltoStreetLine1( $this->m_objCustomer->getPrimaryStreetLine1() );
			if( true == is_null( $this->getBilltoStreetLine2() ) ) 	$this->setBilltoStreetLine2( $this->m_objCustomer->getPrimaryStreetLine2() );
			if( true == is_null( $this->getBilltoCity() ) ) 			$this->setBilltoCity( $this->m_objCustomer->getPrimaryCity() );
			if( true == is_null( $this->getBilltoStateCode() ) ) 	$this->setBilltoStateCode( $this->m_objCustomer->getPrimaryStateCode() );
			if( true == is_null( $this->getBilltoPostalCode() ) ) 	$this->setBilltoPostalCode( $this->m_objCustomer->getPrimaryPostalCode() );
		}

		if( true == is_null( $this->getCcNameOnCard() ) ) 			$this->setCcNameOnCard( $this->getBilltoNameFull() );
		if( true == is_null( $this->getCheckNameOnAccount() ) ) 		$this->setCheckNameOnAccount( $this->getBilltoNameFull() );

		return;
	}

	public function forceReversalExport( $intCompanyUserId, $objDatabase, $objPaymentDatabase ) {

		$this->m_objProperty	= $this->getOrFetchProperty( $objDatabase );
		$objIntegrationDatabase	= $this->m_objProperty->fetchIntegrationDatabase( $objDatabase );

		$boolIsValid = true;

		$arrintIntegrationClientTypeIds = [
			CIntegrationClientType::AMSI,
			CIntegrationClientType::YARDI,
			CIntegrationClientType::YARDI_RPORTAL,
			CIntegrationClientType::MRI,
			CIntegrationClientType::TIMBERLINE,
			CIntegrationClientType::TENANT_PRO,
			CIntegrationClientType::REAL_PAGE_API
		];

		// if the payment is integrated then we need to make the call for return payment.
		if( true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' )
			&& true == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), $arrintIntegrationClientTypeIds )
			&& CPaymentStatusType::CAPTURED == $this->getPaymentStatusTypeId()
			&& true == valObj( $this->m_objProperty, 'CProperty', 'RemotePrimaryKey' )
			&& true == valStr( $this->getRemotePrimaryKey() )
			&& false == valStr( $this->getReturnRemotePrimaryKey() ) ) {

			$objIntegrationClient = \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchIntegrationClientByPropertyIdByIntegrationServiceIdByCid( $this->m_objProperty->getId(), CIntegrationService::REVERSE_AR_PAYMENT, $this->m_intCid, $objDatabase );

			if( true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) ) {
				$objIntegrationClientKeyValue = \Psi\Eos\Entrata\CIntegrationClientKeyValues::createService()->fetchIntegrationClientKeyValueByKeyByIntegrationDatabaseIdByCid( 'INTEGRATION_CLIENT_STATUS', $objIntegrationDatabase->getId(), $this->m_intCid, $objDatabase );
			}

			if( true == valObj( $objIntegrationClientKeyValue, 'CIntegrationClientKeyValue' ) && INTEGRATION_CALL_STATUS_SUSPEND_ALL_SEND_UPDATE == $objIntegrationClientKeyValue->getValue() ) {
				return false;
			}

			if( false == valObj( $objIntegrationClient, 'CIntegrationClient' )
				|| CIntegrationClientStatusType::ACTIVE != $objIntegrationClient->getIntegrationClientStatusTypeId()
				|| CIntegrationClientStatusType::ACTIVE != $objIntegrationClient->getIntegrationDatabaseIntegrationClientStatusTypeId() ) return false;

			$boolIsValid &= $this->exportReturnedArPayment( $intCompanyUserId, $objDatabase, $objPaymentDatabase, $boolQueueSyncOverride = false, CIntegrationSyncType::NOW, $boolForcePaymentReturn = true );
		}

		return $boolIsValid;
	}

	public function resetAndReIntegrate( $intCompanyUserId, $objDatabase, $objPaymentDatabase, $boolNullRemotePrimaryKey = false ) {

		$boolResult = true;

		$this->getOrFetchProperty( $objDatabase );
		$objIntegrationClientType = $this->m_objProperty->fetchIntegrationClientType( $objDatabase );

		// AMSI doesnt allow same payment to be integrated twice, Hence, we should not update the RPK to NULL.
		// However if payment was never integrated then we should try to integrate.
		if( CPaymentStatusType::CAPTURED == $this->getPaymentStatusTypeId()
			&& 0 < strlen( trim( $this->m_objProperty->getRemotePrimaryKey() ) )
			&& true == valObj( $objIntegrationClientType, 'CIntegrationClientType' ) ) {

			if( true == $boolNullRemotePrimaryKey && CIntegrationClientType::AMSI != $objIntegrationClientType->getId() ) {
				$this->setRemotePrimaryKey( NULL );
				$this->setExportedOn( NULL );

				if( false == $this->validate( VALIDATE_UPDATE ) || false == $this->update( $intCompanyUserId, $objDatabase, $objPaymentDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to update original ar payment RPK.' ) );
					return false;
				}
			}

			$boolResult = $this->exportArPayment( $intCompanyUserId, $objDatabase, $objPaymentDatabase, NULL, true );
		}

		return $boolResult;
	}

	public function loadArPaymentImages( $objPaymentDatabase ) {

		$arrobjArPaymentImages = CArPaymentImages::fetchArPaymentImagesByArPaymentId( $this->m_intId, $objPaymentDatabase );

		if( false == valArr( $arrobjArPaymentImages ) || 2 != \Psi\Libraries\UtilFunctions\count( $arrobjArPaymentImages ) ) return false;

		foreach( $arrobjArPaymentImages as $objArPaymentImage ) {
			if( CPaymentImageType::FRONT == $objArPaymentImage->getPaymentImageTypeId() ) {
				$this->m_objArPaymentImageFront = $objArPaymentImage;
			} elseif( CPaymentImageType::REVERSE ) {
				$this->m_objArPaymentImageReverse = $objArPaymentImage;
			} else {
				return false;
			}
		}

		if( true == valObj( $this->m_objArPaymentImageFront, 'CArPaymentImage' ) && true == valObj( $this->m_objArPaymentImageReverse, 'CArPaymentImage' ) ) {

			return $arrobjArPaymentImages;
		}

		return false;
	}

	public function loadArPaymentMagstrip( $objPaymentDatabase ) {

		$this->m_objArPaymentMagstrip = CArPaymentMagstrips::fetchArPaymentMagstripByArPaymentId( $this->m_intId, $objPaymentDatabase );

		if( false == valObj( $this->m_objArPaymentMagstrip, 'CArPaymentMagstrip' ) ) {
			return false;
		}

		return $this->m_objArPaymentMagstrip;
	}

	public function postPayment( $intCurrentUserId, $objClientDatabase, $objPaymentDatabase ) {

		// For electronic payments no more processing required. Return immediately.
		if( true == $this->isElectronicPayment() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to post customer payment. Invalid payment type.' ) );
			return false;
		}
		// Check payment status
		if( CPaymentStatusType::PENDING != $this->getPaymentStatusTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to post customer payment. Invalid payment status.' ) );
			return false;
		}

		$this->setPaymentStatusTypeId( CPaymentStatusType::RECEIVED );
		// Save customer payment
		if( false == $this->validate( 'post_payment' ) || false == $this->insert( $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $boolOverrideTransaction = true ) ) {
			$this->setPaymentStatusTypeId( CPaymentStatusType::PENDING );
			return false;
		}

		return true;
	}

	public function applyAuthFailureFee( $intCurrentUserId, $objPaymentDatabase, $objClientDatabase ) {

		$objMerchantAccount = $this->getOrFetchMerchantAccount( $objClientDatabase );

		if( false == CPaymentTypes::isElectronicPayment( $this->getPaymentTypeId() ) ) {
			return false;
		}

		if( false == valObj( $objMerchantAccount, 'CMerchantAccount' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to load company merchant account.', NULL ) );
			return false;
		}

		// Load client to get entity_id
		$objClient = $objMerchantAccount->fetchClient( $objClientDatabase );

		if( false == valObj( $objClient, 'CClient' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to load client.', NULL ) );
			return false;
		}

		$objMerchantMethod = $objMerchantAccount->getMethodByAmount( $this->getPaymentTypeId(), $this->getPaymentAmount() );
		$fltCompanyReturnFee = $objMerchantMethod->getAuthFailureFee();

		if( true == is_null( $fltCompanyReturnFee ) || 0 == $fltCompanyReturnFee ) {
			return false;
		}

		$intChargeCodeId = NULL;

		if( CPaymentTypes::isCreditCardPayment( $this->getPaymentTypeId() ) ) {
			$intChargeCodeId = CChargeCode::CC_MANAGEMENT_FAILED_FEE;
		}

		if( CPaymentTypes::isAchPayment( $this->getPaymentTypeId() ) ) {
			$intChargeCodeId = CChargeCode::ACH_MANAGEMENT_FAILED_FEE;
		}

		if( CPaymentTypes::isCheck21Payment( $this->getPaymentTypeId() ) ) {
			$intChargeCodeId = CChargeCode::CHECK_21_FEE;
		}

		$objEftCharge = $this->createEftCharge();
		$objEftCharge->setChargeCodeId( $intChargeCodeId );
		$objEftCharge->setCompanyMerchantAccountId( $objMerchantAccount->getId() );
		$objEftCharge->setEftChargeTypeId( CEftChargeType::AUTH_FAILURE_FEE );
		$objEftCharge->setAccountId( $objMerchantAccount->getBillingAccountId() );
		$objEftCharge->setChargeAmount( $fltCompanyReturnFee );
		$objEftCharge->setMemo( 'Company return fee for $' . __( '{%f,0}', [ number_format( ( float ) $this->getPaymentAmount(), 2 ) ] ) . ' ' . CPaymentTypes::paymentTypeIdToStr( $this->getPaymentTypeId() ) . ' payment( ' . $this->getId() . ' ) from ' . addslashes( $this->getBilltoNameFull() ) . ' on ' . $this->getPaymentDatetime() );

		if( false == $objEftCharge->validate( VALIDATE_INSERT ) || false == $objEftCharge->insert( $intCurrentUserId, $objPaymentDatabase ) ) {
			$strErrorMessage = $objEftCharge->getConsolidatedErrorMsg();
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strErrorMessage, NULL ) );
			trigger_error( $strErrorMessage, E_USER_WARNING );
			return false;
		}

		return true;
	}

	public function fetchArPaymentSplits( $objClientDatabase ) {
		if( 1 != $this->m_intIsSplit ) return NULL;
		return \Psi\Eos\Entrata\CArPaymentSplits::createService()->fetchArPaymentSplitsByArPaymentIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchActiveArPaymentSplits( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CArPaymentSplits::createService()->fetchActiveArPaymentSplitsByArPaymetIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function postArTransactionsToResidentLedgers( $intCurrentUserId, $objClientDatabase, $boolAutoAllocate = true, $boolIsTemporary = false, $boolIsApplicationPaymentTransfer = false ) {

		$arrobjArPaymentSplits = $this->getOrFetchArPaymentSplits( $objClientDatabase );

		if( false == valId( $this->getLeaseId() ) && false == valArr( $arrobjArPaymentSplits ) ) return true;

		$arrobjArTransactions = $this->fetchUnreversedPaymentArTransactions( $objClientDatabase );
		$objPropertyGlSetting = $this->getOrFetchPropertyGlSetting( $objClientDatabase );
		$arrstrPostMonthValidationData = $this->getOrFetchPostMonthValidationData( $intCurrentUserId, $objClientDatabase );

		$arrintArTransactionIds = [];
		$arrintAllocateArTransactionIds = $this->getAllocateArTransactionIds();
		if( true == valArr( $arrintAllocateArTransactionIds ) ) {
			$arrintArTransactionIds = $arrintAllocateArTransactionIds;
		}

		if( true == valArr( $arrobjArTransactions ) ) {

			/** @var \CArTransaction $objArTransaction */
			foreach( $arrobjArTransactions as $objArTransaction ) {
				if( true == $objArTransaction->getIsTemporary() ) {
					$objArTransaction->setIsTemporary( false );

					if( true == valStr( $this->getPostMonth() ) ) {
						$objArTransaction->setPostMonth( $this->getPostMonth() );
					} elseif( true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) && strtotime( $objArTransaction->getPostMonth() ) < strtotime( $objPropertyGlSetting->getArPostMonth() ) ) {
						$objArTransaction->setPostMonth( $objPropertyGlSetting->getArPostMonth() );
					}

					if( false == $objArTransaction->postUpdate( $intCurrentUserId, $objClientDatabase ) ) {
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Could not post payment to resident ledger.', NULL ) );
						return false;
					}

					if( true == $boolAutoAllocate && false == $this->getOverrideAutoAllocations() && true == $this->getAutoAllocation() ) {
						$objArTransaction->autoAllocate( $intCurrentUserId, $objClientDatabase, NULL, $arrintArTransactionIds );
					}
				}
			}

		} elseif( true == valArr( $arrobjArPaymentSplits ) ) {

			$arrobjLeases = ( array ) \Psi\Eos\Entrata\CLeases::createService()->fetchLeasesByIdsByCid( array_keys( rekeyObjects( 'LeaseId', $arrobjArPaymentSplits ) ), $this->getCid(), $objClientDatabase );

			foreach( $arrobjArPaymentSplits as $objArPaymentSplit ) {

				$objLease = $arrobjLeases[$objArPaymentSplit->getLeaseId()];

				/** @var \CArTransaction $objPaymentArTransaction */
				$objPaymentArTransaction = $objArPaymentSplit->createPaymentArTransaction( $this, $boolIsTemporary, $objClientDatabase );
				$objPaymentArTransaction->setLeaseIntervalId( $objLease->getActiveLeaseIntervalId() );
				$objPaymentArTransaction->setPostMonth( $this->getOrFetchPostMonth( $objClientDatabase ) );

				$strValidateAction = 'post_payment_ar_transaction';

				if( ( true == $boolIsApplicationPaymentTransfer && $this->getPaymentStatusTypeId() == CPaymentStatusType::TRANSFERRED ) || $this->getPaymentStatusTypeId() == CPaymentStatusType::REVERSED || $this->getPaymentStatusTypeId() == CPaymentStatusType::REVERSAL_PENDING ) {
					$strValidateAction = 'post_payment_reverse_ar_transaction';
				}

				if( true == valArr( $arrstrPostMonthValidationData ) ) {
					$objPaymentArTransaction->setPastPostMonth( $arrstrPostMonthValidationData['past_post_month'] );
					$objPaymentArTransaction->setFuturePostMonth( $arrstrPostMonthValidationData['future_post_month'] );
					$objPaymentArTransaction->setValidatePastOrFuturePostMonth( $arrstrPostMonthValidationData['validate_post_month'] );
				}

				if( false == $objPaymentArTransaction->validate( $strValidateAction, $objClientDatabase ) || false == $objPaymentArTransaction->postPayment( $intCurrentUserId, $objClientDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Could not post a credit to resident ledger.' ), NULL ) );
					$this->addErrorMsgs( $objPaymentArTransaction->getErrorMsgs() );
					$this->addErrorMsgs( $objClientDatabase->getErrorMsgs() );

					return false;
				}

				if( true == $boolAutoAllocate && false == $this->getOverrideAutoAllocations() && true == $objArPaymentSplit->getAutoAllocation() ) {
					$objPaymentArTransaction->autoAllocate( $intCurrentUserId, $objClientDatabase );
				}
			}
		} else {
			$this->m_objPaymentArTransaction = $this->createPaymentArTransaction( $objClientDatabase, $boolIsTemporary );
			$strValidateAction = 'post_payment_ar_transaction';
			if( ( true == $boolIsApplicationPaymentTransfer && $this->getPaymentStatusTypeId() == CPaymentStatusType::TRANSFERRED ) || $this->getPaymentStatusTypeId() == CPaymentStatusType::REVERSED || $this->getPaymentStatusTypeId() == CPaymentStatusType::REVERSAL_PENDING ) {
				$strValidateAction = 'post_payment_reverse_ar_transaction';
			}

			if( true == valArr( $arrstrPostMonthValidationData ) ) {
				$this->m_objPaymentArTransaction->setPastPostMonth( $arrstrPostMonthValidationData['past_post_month'] );
				$this->m_objPaymentArTransaction->setFuturePostMonth( $arrstrPostMonthValidationData['future_post_month'] );
				$this->m_objPaymentArTransaction->setValidatePastOrFuturePostMonth( $arrstrPostMonthValidationData['validate_post_month'] );
			}

			if( false == $this->m_objPaymentArTransaction->validate( $strValidateAction, $objClientDatabase ) || false == $this->m_objPaymentArTransaction->postPayment( $intCurrentUserId, $objClientDatabase ) ) {

				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Could not post a credit to resident ledger.' ), NULL ) );
				$this->addErrorMsgs( $this->m_objPaymentArTransaction->getErrorMsgs() );

				return false;
			}

			// When electronic payments post in entrata -> Payments, we map the desired allocations to the Post Ar Allocations Variable.  This field is null for every other condition.
			// Perhaps we can load Allocations into the AR Payment from new customer financial module
			if( false == $intIsTemporary && true == valArr( $this->getPostArAllocations() ) ) {

				if( true == valObj( $this->m_objLease, 'CLease' ) ) {
					// Load the current unallocated charges, with their respective allocated totals.
					$arrobjArAllocations = $this->m_objLease->fetchArAllocationsForUnallocatedCharges( $objClientDatabase );
					$this->m_arrobjArAllocations = CAllocationEngineController::generateFormPostArAllocations( $this->getPostArAllocations(), $this->m_objPaymentArTransaction, $arrobjArAllocations, $this->m_objPropertyGlSetting, $objClientDatabase );
				}
			}

			if( true == valArr( $this->m_arrobjArAllocations ) ) {

				$this->m_objPropertyGlSetting = $this->getOrFetchPropertyGlSetting( $objClientDatabase );

				foreach( $this->m_arrobjArAllocations as $objArAllocation ) {

					if( false == is_numeric( $objArAllocation->getCreditArTransactionId() ) ) {
						$objArAllocation->setCreditArTransactionId( $this->m_objPaymentArTransaction->getId() );
					}

					if( false == valStr( $objArAllocation->getPostMonth() ) ) $objArAllocation->setPostMonth( $this->getPostMonth() );
					if( false == valStr( $objArAllocation->getPostDate() ) ) $objArAllocation->setPostDate( date( 'm/d/Y' ) );
					$objArAllocation->setReportingPostDate( $objArAllocation->getPostDate() );

					if( true == valArr( $arrstrPostMonthValidationData ) ) {
						$objArAllocation->setPastPostMonth( $arrstrPostMonthValidationData['past_post_month'] );
						$objArAllocation->setFuturePostMonth( $arrstrPostMonthValidationData['future_post_month'] );
						$objArAllocation->setValidatePastOrFuturePostMonth( $arrstrPostMonthValidationData['validate_post_month'] );
					}

					if( false == $objArAllocation->validate( 'post_ar_allocation', $objClientDatabase, $this->m_objPropertyGlSetting ) || false == $objArAllocation->postAllocation( $intCurrentUserId, $objClientDatabase ) ) {
						$this->addErrorMsg( new CErrorMsg( 'Payment processed successfully but allocations failed to post.' ) );
						trigger_error( 'Payment ID# ' . $this->getId() . ' processed successfully but allocations failed to post.', E_USER_WARNING );
						return false;
					}
				}

			} elseif( true == $boolAutoAllocate && false == $this->getOverrideAutoAllocations() && true == $this->getAutoAllocation() ) {
				$this->m_objPaymentArTransaction->autoAllocate( $intCurrentUserId, $objClientDatabase, NULL, $arrintArTransactionIds );

			}
		}

		return true;
	}

	public function recreditConvienienceFees( $intCurrentUserId, $objPaymentDatabase ) {

		$boolIsValid = true;

		// Find all convenience fees associated to this customer payment.
		$arrobjEftCharges = \Psi\Eos\Payment\CEftCharges::createService()->fetchEftChargesByChargeCodeIdsByArPaymentId( [ CChargeCode::ACH_CONVENIENCE_FEE, CChargeCode::CC_CONVENIENCE_FEE, CChargeCode::ACH_CONVENIENCE_FEE_CLEARING, CChargeCode::CC_CONVENIENCE_FEE_CLEARING ], $this->getId(), $objPaymentDatabase );

		if( true == valArr( $arrobjEftCharges ) ) {
			foreach( $arrobjEftCharges as $objEftCharge ) {

				$objReversalEftCharge = clone $objEftCharge;
				$objReversalEftCharge->setId( NULL );
				$objReversalEftCharge->setEftChargeId( $objReversalEftCharge->getId() );
				$objReversalEftCharge->setChargeAmount( -1 * $objReversalEftCharge->getChargeAmount() );
				$objReversalEftCharge->setMemo( 'Convenience Fee Reversal Credit for payment ' . $objReversalEftCharge->getArPaymentId() . ' from ' . $this->getBilltoNameFull() );

				if( false == $objReversalEftCharge->validate( VALIDATE_INSERT ) || false == $objReversalEftCharge->insert( $intCurrentUserId, $objPaymentDatabase ) ) {
					$strErrorMessage = $objReversalEftCharge->getConsolidatedErrorMsg();
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strErrorMessage, NULL ) );
					trigger_error( $strErrorMessage, E_USER_WARNING );

					$boolIsValid = false;

					break;
				}
			}
		}

		return $boolIsValid;
	}

	public function recreditCharityDonations( $intCurrentUserId, $objPaymentDatabase ) {

		$boolIsValid = true;

		$arrobjEftCharges = \Psi\Eos\Payment\CEftCharges::createService()->fetchEftChargesByChargeCodeIdsByArPaymentId( [ CChargeCode::CHARITY_DONATION ], $this->getId(), $objPaymentDatabase );

		if( true == valArr( $arrobjEftCharges ) ) {
			foreach( $arrobjEftCharges as $objEftCharge ) {
				$objReversalEftCharge = clone $objEftCharge;
				$objReversalEftCharge->setId( NULL );
				$objReversalEftCharge->setEftChargeId( $objReversalEftCharge->getId() );
				$objReversalEftCharge->setChargeAmount( -1 * $objReversalEftCharge->getChargeAmount() );
				$objReversalEftCharge->setMemo( 'Donation Amount Reversal Credit for payment ' . $objReversalEftCharge->getArPaymentId() . ' from ' . $this->getBilltoNameFull() );

				if( false == $objReversalEftCharge->validate( VALIDATE_INSERT ) || false == $objReversalEftCharge->insert( $intCurrentUserId, $objPaymentDatabase ) ) {
					$strErrorMessage = $objReversalEftCharge->getConsolidatedErrorMsg();
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strErrorMessage, NULL ) );
					trigger_error( $strErrorMessage, E_USER_WARNING );

					$boolIsValid = false;

					break;
				}
			}
		}

		return $boolIsValid;
	}

	public function recreditReturnFees( $intCurrentUserId, $objPaymentDatabase ) {

		$boolIsValid = true;

		$arrobjEftCharges = \Psi\Eos\Payment\CEftCharges::createService()->fetchEftChargesByChargeCodeIdsByArPaymentId( [ CChargeCode::RETURN_FEE ], $this->getId(), $objPaymentDatabase );

		if( true == valArr( $arrobjEftCharges ) ) {
			foreach( $arrobjEftCharges as $objEftCharge ) {
				$objReversalEftCharge = clone $objEftCharge;
				$objReversalEftCharge->setId( NULL );
				$objReversalEftCharge->setEftChargeId( $objReversalEftCharge->getId() );
				$objReversalEftCharge->setChargeAmount( -1 * $objReversalEftCharge->getChargeAmount() );
				$objReversalEftCharge->setMemo( 'Return Fee Reversal Credit for payment ' . $objReversalEftCharge->getArPaymentId() . ' from ' . $this->getBilltoNameFull() );

				if( false == $objReversalEftCharge->validate( VALIDATE_INSERT ) || false == $objReversalEftCharge->insert( $intCurrentUserId, $objPaymentDatabase ) ) {
					$strErrorMessage = $objReversalEftCharge->getConsolidatedErrorMsg();
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strErrorMessage, NULL ) );
					trigger_error( $strErrorMessage, E_USER_WARNING );

					$boolIsValid = false;

					break;
				}
			}
		}

		return $boolIsValid;
	}

	public function recreditCompanyFees( $intCurrentUserId, $objPaymentDatabase ) {

		$boolIsValid = true;

		$arrobjEftCharges = \Psi\Eos\Payment\CEftCharges::createService()->fetchEftChargesByChargeCodeIdsByArPaymentId( [ CChargeCode::ACH_MANAGEMENT_FEE, CChargeCode::CC_MANAGEMENT_FEE ], $this->getId(), $objPaymentDatabase );

		if( true == valArr( $arrobjEftCharges ) ) {
			foreach( $arrobjEftCharges as $objEftCharge ) {
				$objReversalEftCharge = clone $objEftCharge;
				$objReversalEftCharge->setId( NULL );
				$objReversalEftCharge->setEftChargeId( $objReversalEftCharge->getId() );
				$objReversalEftCharge->setChargeAmount( -1 * $objReversalEftCharge->getChargeAmount() );
				$objReversalEftCharge->setMemo( 'Company Fee Reversal for payment ' . $objReversalEftCharge->getArPaymentId() . ' from ' . $this->getBilltoNameFull() );

				if( false == $objReversalEftCharge->validate( VALIDATE_INSERT ) || false == $objReversalEftCharge->insert( $intCurrentUserId, $objPaymentDatabase ) ) {
					$strErrorMessage = $objReversalEftCharge->getConsolidatedErrorMsg();
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strErrorMessage, NULL ) );
					trigger_error( $strErrorMessage, E_USER_WARNING );

					$boolIsValid = false;

					break;
				}
			}
		}

		return $boolIsValid;
	}

	public function recreditRebateFees( $intCurrentUserId, $objPaymentDatabase ) {

		$boolIsValid = true;

		$arrintChargeCodeIds = [];

		if( CPaymentType::ACH == $this->getPaymentTypeId() ) {
			$arrintChargeCodeIds = [ CChargeCode::ACH_REBATE_FEE_CLEARING ];
		} elseif( true == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) ) {
			$arrintChargeCodeIds = [ CChargeCode::CC_REBATE_FEE_CLEARING ];
		}

		$arrobjEftCharges = \Psi\Eos\Payment\CEftCharges::createService()->fetchEftChargesByChargeCodeIdsByArPaymentId( $arrintChargeCodeIds, $this->getId(), $objPaymentDatabase );

		if( true == valArr( $arrobjEftCharges ) ) {
			foreach( $arrobjEftCharges as $objEftCharge ) {
				$objRebateEftCharge = clone $objEftCharge;
				$objRebateEftCharge->setId( NULL );
				$objRebateEftCharge->setEftChargeId( $objRebateEftCharge->getId() );
				$objRebateEftCharge->setChargeAmount( -1 * $objRebateEftCharge->getChargeAmount() );
				$objRebateEftCharge->setMemo( 'Rebate reversal for PSID for payment ' . $objRebateEftCharge->getArPaymentId() );

				if( false == $objRebateEftCharge->validate( VALIDATE_INSERT ) || false == $objRebateEftCharge->insert( $intCurrentUserId, $objPaymentDatabase ) ) {
					$strErrorMessage = $objRebateEftCharge->getConsolidatedErrorMsg();
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strErrorMessage, NULL ) );
					trigger_error( $strErrorMessage, E_USER_WARNING );

					$boolIsValid = false;

					break;
				}
			}
		}

		return $boolIsValid;
	}

	public function migrateConvienienceFeesToCompany( $intCurrentUserId, $objPaymentDatabase ) {
		$boolIsValid = true;

		// Find all convenience fees associated to this customer payment.
		//  NOTE: We purposely do not include "clearing" convenience fees. These are the company's revenue. PSI's revenu is already represented by the management fee on this payment
		$arrobjEftCharges = \Psi\Eos\Payment\CEftCharges::createService()->fetchEftChargesByChargeCodeIdsByArPaymentId( [ CChargeCode::ACH_CONVENIENCE_FEE, CChargeCode::CC_CONVENIENCE_FEE ], $this->getId(), $objPaymentDatabase );

		if( true == valArr( $arrobjEftCharges ) ) {
			foreach( $arrobjEftCharges as $objEftCharge ) {
				$objReversalEftCharge = clone $objEftCharge;
				$objReversalEftCharge->setId( NULL );
				$objReversalEftCharge->setEftChargeId( $objReversalEftCharge->getId() );
				$objReversalEftCharge->setChargeAmount( -1 * $objReversalEftCharge->getChargeAmount() );
				$objReversalEftCharge->setMemo( 'Convenience Fee Reversal Credit for payment ' . $objReversalEftCharge->getArPaymentId() . ' from ' . $this->getBilltoNameFull() );

				if( false == $objReversalEftCharge->validate( VALIDATE_INSERT ) || false == $objReversalEftCharge->insert( $intCurrentUserId, $objPaymentDatabase ) ) {
					$strErrorMessage = $objReversalEftCharge->getConsolidatedErrorMsg();
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strErrorMessage, NULL ) );
					trigger_error( $strErrorMessage, E_USER_WARNING );

					$boolIsValid = false;

					break;
				}
			}
		}

		return $boolIsValid;

		if( 1 != $this->getEatConvenienceFees() ) {

			if( true == valArr( $arrobjEftCharges ) ) {
				foreach( $arrobjEftCharges as $objEftCharge ) {
					$objMigrateEftCharge = clone $objEftCharge;
					$objMigrateEftCharge->setId( NULL );
					$objMigrateEftCharge->setEftChargeId( $objMigrateEftCharge->getId() );
					$objMigrateEftCharge->setMemo( 'Convenience Fee Migration for payment ' . $objMigrateEftCharge->getArPaymentId() . ' from ' . $this->getBilltoNameFull() );

					if( false == $objMigrateEftCharge->validate( VALIDATE_INSERT ) || false == $objMigrateEftCharge->insert( $intCurrentUserId, $objPaymentDatabase ) ) {
						$strErrorMessage = $objMigrateEftCharge->getConsolidatedErrorMsg();
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strErrorMessage, NULL ) );
						trigger_error( $strErrorMessage, E_USER_WARNING );

						$boolIsValid = false;

						break;
					}
				}
			}
		}

		return true;
	}

	public function offsetPaymentArTransactions( $intCurrentUserId, $objClientDatabase, $boolPostReturnFees = false, $boolHasIncrementReturnCount = false, $intUserValidationMode = CPropertyGlSetting::VALIDATION_MODE_STRICT ) {

		$boolIsValid = true;
		$arrintLeaseIds = [];
		$arrobjLateFeeArTransactions = [];

		// This varaible holds one single CArTransaction per lease (eliminates duplicate leases)
		$arrobjArTransactionsKeyedByLeaseIds = [];

		/** @var CArTransaction[] $arrobjArTransactions */
		$arrobjArTransactions = ( array ) $this->fetchUnreversedPaymentArTransactions( $objClientDatabase );

		// I'm not sure why we are reversing allocations here.  Aren't we doing this in the CArTransaction->delete()
		if( true == valArr( $arrobjArTransactions ) ) {
			$arrobjAllocatedArTransactions = ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchAppliedArTransactionsByArTransactionIdsByCid( array_keys( $arrobjArTransactions ), $this->getCid(), $objClientDatabase );
			foreach( $arrobjAllocatedArTransactions as $objAllocatedArTransaction ) {
				if( CArCodeType::DEPOSIT == $objAllocatedArTransaction->getArCodeTypeId() && false == \Psi\Eos\Entrata\CArTransactions::createService()->deleteChargeArTransactionsAppliedToDeposits( $objAllocatedArTransaction, $intCurrentUserId, $this->getCid(), $objClientDatabase ) ) {
					$this->addErrorMsgs( $objAllocatedArTransaction->getErrorMsgs() );
					return false;
				}
			}
		}

		if( false == is_null( $this->getLeaseId() ) ) {

			$boolIsShowErrorMessage = false;

			if( true == valArr( $arrobjArTransactions ) ) {
				foreach( $arrobjArTransactions as $objArTransaction ) {

					$arrintLeaseIds[$objArTransaction->getLeaseId()] = $objArTransaction->getLeaseId();

					if( CPropertyGlSetting::VALIDATION_MODE_STRICT == $intUserValidationMode ) {

						// skip return fees if the property is disabled
						if( true == $boolPostReturnFees && true == valId( $objArTransaction->getPropertyId() ) ) {
							$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objArTransaction->getPropertyId(), $objArTransaction->getCid(), $objClientDatabase );
							if( true == valObj( $objProperty, CProperty::class ) && 1 == $objProperty->getIsDisabled() ) {
								$boolPostReturnFees = false;
							}
						}

						$objArTransaction->loadDefaultDeleteData( $objClientDatabase, $boolLoadReturnItemFee = $boolPostReturnFees, $intCurrentUserId );
						$objArTransaction->setArPayment( $this );

						if( false == is_null( $this->getDeletePostMonth() ) )	$objArTransaction->setDeletePostMonth( $this->getDeletePostMonth() );
						if( false == is_null( $this->getDeleteMemo() ) )		$objArTransaction->setDeleteMemo( $this->getDeleteMemo() );
						if( false == is_null( $this->getReturnItemFee() ) )		$objArTransaction->setReturnItemFee( $this->getReturnItemFee() );

						if( true == $boolPostReturnFees ) {
							if( false == $objArTransaction->postReturnItemFee( $intCurrentUserId, $objClientDatabase ) ) {
								$this->addErrorMsgs( $objArTransaction->getErrorMsgs() );
								return;
							}
						}

						if( true == $boolHasIncrementReturnCount ) {
							if( false == $objArTransaction->incrementReturnedPaymentsCount( $intCurrentUserId, $objClientDatabase ) ) {
								$this->addErrorMsgs( $objArTransaction->getErrorMsgs() );
								return;
							}
						}
					}

					// We do not need to validate post_month permission for SYSTEM, RESIDENT_PORTAL and PROSPECT_PORTAL users.
					$arrintCompanyUserIds = [ CUser::ID_SYSTEM, CUser::ID_RESIDENT_PORTAL, CUser::ID_PROSPECT_PORTAL ];

					if( true == in_array( $intCurrentUserId, $arrintCompanyUserIds ) ) $objArTransaction->setOverridePostMonthPermission( true );

					if( false == $objArTransaction->delete( $intCurrentUserId, $objClientDatabase, $objPaymentDatabase = NULL, $boolDisassociateArPayment = false, $boolPeformRapidDelete = false, $boolReturnSqlOnly = false, $intUserValidationMode ) ) {

						$this->addErrorMsg( new CErrorMsg( NULL, '', 'Original payment ar transaction failed to offset.' ) );

						$arrobjArTransactionErrorMsgs = rekeyObjects( 'Type', $objArTransaction->getErrorMsgs() );
						unset( $arrobjArTransactionErrorMsgs[ERROR_TYPE_DATABASE] );

						$this->addErrorMsgs( $arrobjArTransactionErrorMsgs );
						return false;
					}

					// Ensure return fees post once only.
					$boolPostReturnFees = false;
				}

				$this->setActivePaymentArTransactions( $arrobjArTransactions );
			}
		}

		if( true == valArr( $arrintLeaseIds ) ) {

			require_once( PATH_APPLICATION_ENTRATA . 'Library/Tools/CLateFeeLibrary.class.php' );

			$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCustomCompanyUserByIdByCid( $intCurrentUserId, $this->getCid(), $objClientDatabase );

			$arrobjLeases = ( array ) \Psi\Eos\Entrata\CLeases::createService()->fetchViewLeasesByIdsByCid( $arrintLeaseIds, $this->getCid(), $objClientDatabase );

			foreach( $arrintLeaseIds as $intLeaseId ) {

				if( false == array_key_exists( $intLeaseId, $arrobjLeases ) ) continue;
				$objLease = $arrobjLeases[$intLeaseId];
				$objEventLibrary 	= $objLease->logActivityForLeaseInformation( CEventType::LEDGER_INFO, $objClientDatabase, NULL, $this, [ 'employee_name' => $objCompanyUser->getUsername(), 'event_type' => 'Payment', 'event' => 'Reversed' ], true );

				if( false == valObj( $objEventLibrary, 'CEventLibrary' ) ) {
					$this->addSessionErrorMsg( 'Could not log activity for lease : ' . $objLease->getId() );
					break;
				}

				if( false == ( CPaymentStatusType::RECALL == $this->getPaymentStatusTypeId() && true == in_array( $this->getPaymentTypeId(), [ CPaymentType::CHECK_21, CPaymentType::ACH, CPaymentType::PAD ] ) ) ) {
					if( false == $objEventLibrary->insertEvent( $objCompanyUser->getId(), $objClientDatabase ) ) {
						$this->addErrorMsgs( $objEventLibrary->getErrorMsgs() );
						break;
					}
				}
			}

			if( false == valArr( $arrobjAllocatedArTransactions ) ) {
				return true;
			}

			$arrintArTransactionIds = [];

			foreach( $arrobjAllocatedArTransactions as $objAllocatedArTransaction ) {
				$arrintArTransactionIds[$objAllocatedArTransaction->getLeaseId()][$objAllocatedArTransaction->getId()] = $objAllocatedArTransaction->getId();
			}

			$arrobjLeaseDetails = ( array ) \Psi\Eos\Entrata\CLeaseDetails::createService()->fetchLeaseDetailsByLeaseIdsByCid( $arrintLeaseIds, $this->getCid(), $objClientDatabase );
			$arrobjLeaseDetails	= rekeyObjects( 'LeaseId', $arrobjLeaseDetails );

			if( CPaymentStatusType::RECALL == $this->getPaymentStatusTypeId() && CPropertyGlSetting::VALIDATION_MODE_STRICT == $intUserValidationMode ) {

				// Post Late fees to the leases which has NSF Payments
				try {

					$arrmixMessageProperties 			= [ 'priority' => 0 ];
					$objArWorkflowsAmqpSenderLibrary 	= new CArWorkflowsAmqpSenderLibrary( $strBasicQueueName = 'arworkflows.autoadjustlatefees' );
					$objArWorkflowsAmqpSenderLibrary->setMessageProperties( $arrmixMessageProperties );
					$objArWorkflowsAmqpSenderLibrary->setAutoAdjustLateFeeMessage( $arrobjArTransactions, $this->getCid(), $this->getId(), $intCurrentUserId, $boolIsFlagAsNSF = true );
					$objArWorkflowsAmqpSenderLibrary->sendMessage();

				} catch( Exception $objException ) {

					$objLateFeeLibrary	= new CLateFeeLibrary();
					if( false == $objLateFeeLibrary->autoAdjustLatefee( $this, $intCurrentUserId, $objClientDatabase, $boolIsAutoAllocateLatefees = true, $arrobjArTransactions, $boolIsFlagAsNSF = true ) ) {
						$this->addErrorMsgs( $objEventLibrary->getErrorMsgs() );
					}
				}

				// Send Late notices for the leases which has NSF Payments
				$this->sendNsfReturnedLateNoticesMessage( $arrobjArTransactions, $intCurrentUserId );
			}
		}

		return true;
	}

	public function sendNsfReturnedLateNoticesMessage( $arrobjArTransactions, $intCurrentUserId ) {

		// @FIXME Probably we can merge this function with late fee message block and put out some common LOC
		try {
			$arrmixMessageProperties 			= [ 'priority' => 0 ];
			$objArWorkflowsAmqpSenderLibrary 	= new CArWorkflowsAmqpSenderLibrary( CArWorkflowsSendNsfReturnedLateNoticesMessage::MESSAGE_TYPE );
			$objArWorkflowsAmqpSenderLibrary->setMessageProperties( $arrmixMessageProperties );
			$objArWorkflowsAmqpSenderLibrary->createNsfReturnedLateNoticesMessage( $arrobjArTransactions, $this->getCid(), $this->getId(), $intCurrentUserId );
			$objArWorkflowsAmqpSenderLibrary->sendMessage();

		} catch( Exception $objException ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to insert message to queue for cid:' . $this->getCid() . ' due to error ' . $objException->getMessage() ) );
		}
	}

	public function valManualPaymentReturn( $objClientDatabase, $intCurrentUserId ) {

		if( false == is_null( $this->getLeaseId() ) ) {

			$arrobjArTransactions = $this->fetchUnreversedPaymentArTransactions( $objClientDatabase );

			if( true == valArr( $arrobjArTransactions ) ) {
				$objArTransaction = reset( $arrobjArTransactions );
				$arrstrPostMonthValidationData = $objArTransaction->getOrFetchPostMonthValidationData( $intCurrentUserId, $objClientDatabase );

				foreach( $arrobjArTransactions as $objArTransaction ) {
					$objArTransaction->setArPayment( $this );
					$objArTransaction->setDeletePostMonth( $this->getFormattedDeletePostMonth() );
					$objArTransaction->setDeletePostDate( $objArTransaction->getPostDate() );
					if( true == valArr( $arrstrPostMonthValidationData ) ) {
						$objArTransaction->setPastPostMonth( $arrstrPostMonthValidationData['past_post_month'] );
						$objArTransaction->setFuturePostMonth( $arrstrPostMonthValidationData['future_post_month'] );
						$objArTransaction->setValidatePastOrFuturePostMonth( $arrstrPostMonthValidationData['validate_post_month'] );
					}
					if( false == $objArTransaction->validate( 'resident_works_return', $objClientDatabase ) ) {
						$this->addErrorMsgs( $objArTransaction->getErrorMsgs() );
						return false;
					}
				}
			}
		}

		return true;
	}

	public function valManualPaymentReverse( $objClientDatabase, $intUserValidationMode = CPropertyGlSetting::VALIDATION_MODE_STRICT, $intCurrentUserId ) {

		if( false == is_null( $this->getLeaseId() ) ) {

			$arrobjArTransactions = $this->fetchUnreversedPaymentArTransactions( $objClientDatabase );

			if( true == valArr( $arrobjArTransactions ) ) {
				$objArTransaction = reset( $arrobjArTransactions );
				$arrstrPostMonthValidationData = $objArTransaction->getOrFetchPostMonthValidationData( $intCurrentUserId, $objClientDatabase );
				foreach( $arrobjArTransactions as $objArTransaction ) {

					$objArTransaction->setArPayment( $this );

					if( false == $intUserValidationMode ) {
						$objArTransaction->setDeletePostMonth( $this->getFormattedDeletePostMonth() );
						$objArTransaction->setDeletePostDate( $objArTransaction->getPostDate() );
					}

					if( true == valArr( $arrstrPostMonthValidationData ) ) {
						$objArTransaction->setPastPostMonth( $arrstrPostMonthValidationData['past_post_month'] );
						$objArTransaction->setFuturePostMonth( $arrstrPostMonthValidationData['future_post_month'] );
						$objArTransaction->setValidatePastOrFuturePostMonth( $arrstrPostMonthValidationData['validate_post_month'] );
					}

					if( false == $objArTransaction->validate( 'resident_works_delete', $objClientDatabase, $objPaymentDatabase = NULL, $boolIsValPostMonth = false, $boolIsIgnoreChargeAmount = false, $intUserValidationMode ) ) {
						$this->addErrorMsgs( $objArTransaction->getErrorMsgs() );
						return false;
					}
				}
			}
		}

		return true;
	}

	public function insertArPaymentTransaction( $intPaymentTransactionTypeId, $intCurrentUserId, $objPaymentDatabase, $objGateway = NULL ) {

		$objArPaymentTransaction = $this->createArPaymentTransaction( $objGateway );
		$objArPaymentTransaction->setPaymentTransactionTypeId( $intPaymentTransactionTypeId );

		if( false == is_null( $this->getRemotePrimaryKey() ) ) {
			$objArPaymentTransaction->setTransactionMemo( 'Original Remote Primary Key:' . $this->getRemotePrimaryKey() );
		}

		if( false == $objArPaymentTransaction->insert( $intCurrentUserId, $objPaymentDatabase ) ) {
			$this->addErrorMsgs( $objArPaymentTransaction->getErrorMsgs() );
			return false;
		}

		return true;
	}

	public function isElectronicPayment() {

		if( true == is_null( $this->m_intPaymentTypeId ) ) {
			return false;
		}

		return CPaymentTypes::isElectronicPayment( $this->m_intPaymentTypeId );
	}

    /**
     * Here we inspect the Merchant Account setting
     * to determine if we should reattempt gateway operations
     * in the case of request timeouts
     *
     * @param $objClientDatabase
     */
    public function setShouldAttemptRetries( $objClientDatabase ) {
        // We'll only attempt retries for CC payment types
        if( false == CPaymentTypes::isCreditCardPayment( $this->getPaymentTypeId() ) ) {
            $this->m_boolShouldAttemptRetries = false;
            return;
        }

        if( \Psi\Libraries\UtilFunctions\valObj( $this->getMerchantAccount(), CMerchantAccount::class ) ) {
            $objMerchantAccount = $this->getMerchantAccount();
        } else {
            $objMerchantAccount = $this->getOrFetchMerchantAccount( $objClientDatabase );

            if( !\Psi\Libraries\UtilFunctions\valObj( $objMerchantAccount, CMerchantAccount::class ) ) {
               $this->m_boolShouldAttemptRetries = false;
               return;
            }
        }

        $this->m_boolShouldAttemptRetries = ( bool ) $objMerchantAccount->getEnableGatewayTimeoutRetries();

    }

    /**
     * @return bool
     */
    public function shouldAttemptRetries() {
        return $this->m_boolShouldAttemptRetries;
    }

    public function setIsQueueReprocessPayment( $boolIsQueueReprocessPayment ) {
        $this->m_boolIsQueueReprocessPayment = $boolIsQueueReprocessPayment;
    }

    public function isQueueReprocessPayment() {
        return $this->m_boolIsQueueReprocessPayment;
    }

	public function isVoidAllowed( $objPaymentDatabase, $boolCanVoidCheck21 = true ) {

		$boolVoidAllowed = false;
		if( CPaymentStatusType::CAPTURED == $this->getPaymentStatusTypeId() && true == is_null( $this->getBatchedOn() ) ) {
			if( CPaymentType::ACH == $this->getPaymentTypeId() ) {

				$intCapturedOn = strtotime( $this->getCapturedOn() );
				$strCutOffTime = '1530';
				$intCutoffDate = date( 'm/d/Y' );
				$intCurrentDateTime = strtotime( date( 'm/d/Y H:i' ) );
				$intCutoffDateTime = strtotime( $intCutoffDate . ' ' . $strCutOffTime );

				$boolIsBankHoliday = $this->checkIsBankingHoliday( $intCutoffDate, $objPaymentDatabase );

				if( true == $boolIsBankHoliday ) {
					$boolVoidAllowed = true;
				} elseif( false == $boolIsBankHoliday && $intCapturedOn <= $intCutoffDateTime && date( 'Hi', $intCutoffDateTime ) < date( 'Hi', $intCurrentDateTime ) ) {
					$boolVoidAllowed = false;
				} else {
					$boolVoidAllowed = true;
				}
			} elseif( CPaymentType::PAD == $this->getPaymentTypeId() ) {
				// TODO: implement logic for this. Currently disabling
				return false;
			} elseif( CPaymentType::CHECK_21 == $this->getPaymentTypeId() && true === $boolCanVoidCheck21 ) {
				$boolVoidAllowed = true;
			}
		}
		return $boolVoidAllowed;
	}

	public function checkIsBankingHoliday( $strDate, $objPaymentDatabase ) {

		// Skip this check in development environment
		if( CONFIG_ENVIRONMENT == 'development' ) {
			return false;
		}

		$objTransactionHoliday = \Psi\Eos\Payment\CTransactionHolidays::createService()->fetchTransactionHolidayByDate( $strDate, $objPaymentDatabase );

		if( true == valObj( $objTransactionHoliday, 'CTransactionHoliday' ) || 'Sun' == date( 'D', strtotime( $strDate ) ) || 'Sat' == date( 'D', strtotime( $strDate ) ) ) {
			return true;
		}

		return false;
	}

	public function regenerateConvienienceFeesForChargeBack( $intCurrentUserId, $objPaymentDatabase ) {

		if( false == CPaymentTypes::isCreditCardPayment( $this->getPaymentTypeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'This action is not permissiable on a transaction that is not a credit card transaction.', NULL ) );
			return false;
		}

		$objCompanyMerchantAccount = $this->getOrFetchCompanyMerchantAccount( $objPaymentDatabase );

		if( false == valObj( $objCompanyMerchantAccount, 'CCompanyMerchantAccount' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'The merchant account object could not be retrieved from the database.', NULL ) );
			return false;
		}

		$objIntermediaryAccount = $this->getIntermediaryAccount();

		if( false == valObj( $objIntermediaryAccount, 'CAccount' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'The account object could not be retrieved from the database.', NULL ) );
			return false;
		}

		$objEftCharge = $this->createEftCharge();
		$objEftCharge->setChargeCodeId( CChargeCode::CC_CONVENIENCE_FEE );
		$objEftCharge->setEftChargeTypeId( CEftChargeType::CONVENIENCE_FEES );
		$objEftCharge->setCompanyMerchantAccountId( $objCompanyMerchantAccount->getId() );
		$objEftCharge->setAccountId( $objIntermediaryAccount->getId() );
		$objEftCharge->setChargeAmount( $this->getConvenienceFeeAmount() );
		$objEftCharge->setMemo( 'Regenerated Convenience Fee for Charged Back Payment Id ' . $this->getId() . ' from ' . $this->getBilltoNameFull() );

		if( false == $objEftCharge->validate( VALIDATE_INSERT ) || false == $objEftCharge->insert( $intCurrentUserId, $objPaymentDatabase ) ) {
			$strErrorMessage = $objEftCharge->getConsolidatedErrorMsg();
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strErrorMessage, NULL ) );
			trigger_error( $strErrorMessage, E_USER_WARNING );
			return false;
		}

		return true;
	}

	public function determineReturnOn( $objDatabase, $objPaymentDatabase ) {

		$objMerchantAccount = $this->getOrFetchMerchantAccount( $objDatabase );

		if( false == valObj( $objMerchantAccount, 'CMerchantAccount' ) ) {
			trigger_error( 'Merchant Account object failed to load.', E_USER_WARNING );
			return NULL;
		}

		$intReturnDistributionDelayDays	= 0;
		$intCutOffHour 					= 15;
		$intCutOffMinutes 				= 30;
		$strCutOffTime					= NULL;

		switch( $this->m_intPaymentTypeId ) {

			case CPaymentType::ACH:
				$strCutOffTime = $objMerchantAccount->getAchCutOffTime();
				break;

			case CPaymentType::VISA:
				$strCutOffTime = $objMerchantAccount->getViCutOffTime();
				break;

			case CPaymentType::MASTERCARD:
				$strCutOffTime = $objMerchantAccount->getMcCutOffTime();
				break;

			case CPaymentType::AMEX:
				$strCutOffTime = $objMerchantAccount->getAeCutOffTime();
				break;

			case CPaymentType::DISCOVER:
				$strCutOffTime = $objMerchantAccount->getDiCutOffTime();
				break;

			case CPaymentType::CHECK_21:
				$strCutOffTime = $objMerchantAccount->getCh21CutOffTime();
				break;

			case CPaymentType::EMONEY_ORDER:
				$strCutOffTime = $objMerchantAccount->getEmoCutOffTime();
				break;

			case CPaymentType::PAD:
				$strCutOffTime = $objMerchantAccount->getOrFetchMethod( CPaymentType::PAD )->getCutOffTime();
				break;

			default:
				return NULL;
				break;
		}

		if( false == is_null( $strCutOffTime ) ) {
			$intCutOffHour		= ( int ) \Psi\CStringService::singleton()->substr( $strCutOffTime, 0, 2 );
			$intCutOffMinutes 	= ( int ) \Psi\CStringService::singleton()->substr( $strCutOffTime, 3, 2 );

			if( 0 == $intCutOffHour && 0 == $intCutOffMinutes ) {
				$intCutOffHour = 24;
			}
		}

		// If the current hour of the day is greater than 3:30pm, then bump the distribution date forward one day.
		// We run the settlement distibution scripts at 3:30pm every day.  So if it falls after that time, we put it in tomorrow or next bus day settlement distribution.

		if( $intCutOffHour < ( int ) date( 'G' ) || ( $intCutOffHour == ( int ) date( 'G' ) && $intCutOffMinutes <= ( int ) date( 'i' ) ) ) {
			$intReturnDistributionDelayDays++;
		}

		if( 0 == $intReturnDistributionDelayDays ) {
			$intReturnDate = strtotime( date( 'm/d/Y' ) );

		} else {
			// Determine the exact timestamp for distribution delay.
			$intReturnDate = time() + ( 3600 * 24 );
			$intReturnDate = strtotime( date( 'm/d/Y', $intReturnDate ) );
		}

		// Make sure that the return date is not in the transaction_holidays table.
		// Here we load all transaction holidays in the next 3 weeks.  We loop until the next day
		// that is not a holiday.
		$arrobjTransactionHolidays = \Psi\Eos\Payment\CTransactionHolidays::createService()->fetchTransactionHolidaysOnOrAfterDateAndBeforeDate( date( 'm/d/Y', $intReturnDate ), date( 'm/d/Y', ( $intReturnDate + ( 21 * 24 * 3600 ) ) ), $objPaymentDatabase );

		// Create an array of all prohibited transaction holiday settlement dates
		$arrintProhibitedTransactionHolidays = [];

		if( true == valArr( $arrobjTransactionHolidays ) ) {
			foreach( $arrobjTransactionHolidays as $objTransactionHoliday ) {
				array_push( $arrintProhibitedTransactionHolidays, strtotime( $objTransactionHoliday->getHolidayDate() ) );
			}
		}

		// Make sure that we aren't choosing an return date that is Saturday or Sunday, and the return date isn't a holiday.
		while( 'Sun' == date( 'D', $intReturnDate ) || 'Sat' == date( 'D', $intReturnDate ) || true == in_array( strtotime( date( 'm/d/Y', $intReturnDate ) ), $arrintProhibitedTransactionHolidays ) ) {
			$intReturnDate = strtotime( '+1 day', $intReturnDate );
		}

		// Functionality to make sure we don't pull funds back before they settle.
		if( $intReturnDate < strtotime( $this->m_strDistributeOn ) ) {
			$intReturnDate = strtotime( $this->m_strDistributeOn );
		}

		return date( 'm/d/Y', $intReturnDate );
	}

	// This function determines the date our transactions will actually appear on the clients bank statement.  Funds are funded one business day after the distribute_on date.

	public function determineFundingDate( $objPaymentDatabase ) {

		if( true == is_null( $this->getDistributeOn() ) ) {
			return NULL;
		}

		$intDistributeOn = strtotime( $this->getDistributeOn() );

		// Make sure that the settlement date is not in the transaction_holidays table.
		// Here we load all transaction holidays in the next 3 weeks.  We loop until the next day
		// that is not a holiday.
		$arrobjTransactionHolidays = \Psi\Eos\Payment\CTransactionHolidays::createService()->fetchTransactionHolidaysOnOrAfterDateAndBeforeDate( '1/1/2002', date( 'm/d/Y', ( time() + ( 30 * 24 * 3600 ) ) ), $objPaymentDatabase );

		// Create an array of all prohibited transaction holiday settlement dates
		$arrintProhibitedTransactionHolidays = [];

		if( true == valArr( $arrobjTransactionHolidays ) ) {
			foreach( $arrobjTransactionHolidays as $objTransactionHoliday ) {
				array_push( $arrintProhibitedTransactionHolidays, strtotime( $objTransactionHoliday->getHolidayDate() ) );
			}
		}

		$intFundOnDate = $intDistributeOn + ( 3600 * 24 );

		// Now we loop on the distribution delay days
		while( 'Sun' == date( 'D', $intFundOnDate ) || 'Sat' == date( 'D', $intFundOnDate ) || true == in_array( strtotime( date( 'm/d/Y', $intFundOnDate ) ), $arrintProhibitedTransactionHolidays ) ) {
			$intFundOnDate += ( 3600 * 24 );
		}

		// Trim the date down to the date only (no hours or seconds)
		$intFundOnDate = strtotime( date( 'm/d/Y', $intFundOnDate ) );

		return date( 'm/d/Y', $intFundOnDate );
	}

	// TODO: This function seems to be a copy of the determineFundingDate function.  Can we get rid of it?  Both functions are being used.

	public function determineDepositDate( $objPaymentDatabase ) {

		if( true == is_null( $this->getDistributeOn() ) ) {
			trigger_error( 'Distibute on date is null.', E_USER_ERROR );
			exit;
		}

		// Make sure that the settlement date is not in the transaction_holidays table.
		// Here we load all transaction holidays in the next 3 weeks.  We loop until the next day
		// that is not a holiday.
		$arrobjTransactionHolidays = \Psi\Eos\Payment\CTransactionHolidays::createService()->fetchTransactionHolidaysOnOrAfterDateAndBeforeDate( $this->getDistributeOn(), date( 'm/d/Y', ( strtotime( $this->getDistributeOn() ) + ( 30 * 24 * 3600 ) ) ), $objPaymentDatabase );

		// Create an array of all prohibited transaction holiday settlement dates
		$arrintProhibitedTransactionHolidays = [];

		if( true == valArr( $arrobjTransactionHolidays ) ) {
			foreach( $arrobjTransactionHolidays as $objTransactionHoliday ) {
				array_push( $arrintProhibitedTransactionHolidays, strtotime( $objTransactionHoliday->getHolidayDate() ) );
			}
		}

		$intDepositDatetime = strtotime( $this->getDistributeOn() );

		while( true ) {
			$intDepositDatetime += ( 3600 * 24 );
			if( 'Sun' != date( 'D', $intDepositDatetime ) && 'Sat' != date( 'D', $intDepositDatetime ) && false == in_array( strtotime( date( 'm/d/Y', $intDepositDatetime ) ), $arrintProhibitedTransactionHolidays ) ) {
				$strDepositDate = date( 'm/d/Y', $intDepositDatetime );
				break;
			}
		}

		return $strDepositDate;
	}

	public function determineDistributeOnMinusOneBuissnessDay( $objPaymentDatabase ) {

		if( false == valStr( $this->getDistributeOn() ) ) {
			trigger_error( 'Distibute on date is null.', E_USER_WARNING );
			return NULL;
		}

		// Make sure that the settlement date is not in the transaction_holidays table.
		// Here we load all transaction holidays in the previous 3 weeks.  We loop until the previous day
		// that is not a holiday.
		$arrobjTransactionHolidays = \Psi\Eos\Payment\CTransactionHolidays::createService()->fetchTransactionHolidaysOnOrAfterDateAndBeforeDate( date( 'm/d/Y', ( strtotime( $this->getDistributeOn() ) - ( 30 * 24 * 3600 ) ) ), $this->getDistributeOn(), $objPaymentDatabase );

		// Create an array of all prohibited transaction holiday settlement dates
		$arrintProhibitedTransactionHolidays = [];

		if( true == valArr( $arrobjTransactionHolidays ) ) {
			foreach( $arrobjTransactionHolidays as $objTransactionHoliday ) {
				array_push( $arrintProhibitedTransactionHolidays, strtotime( $objTransactionHoliday->getHolidayDate() ) );
			}
		}

		$intDepositDatetime = strtotime( $this->getDistributeOn() );

		while( true ) {
			$intDepositDatetime -= ( 3600 * 24 );
			if( 'Sun' != date( 'D', $intDepositDatetime ) && 'Sat' != date( 'D', $intDepositDatetime ) && false == in_array( strtotime( date( 'm/d/Y', $intDepositDatetime ) ), $arrintProhibitedTransactionHolidays ) ) {
				$strDepositDate = date( 'm/d/Y', $intDepositDatetime );
				break;
			}
		}

		return $strDepositDate;
	}

	// This function determines the date our system should credit funds on a reversal

	public function determineReverseOn( $objClientDatabase, $objPaymentDatabase ) {

		// Look at merchant account and see what the settlement_delay_days are.
		$objMerchantAccount = $this->getOrFetchMerchantAccount( $objClientDatabase );

		if( false == valObj( $objMerchantAccount, 'CMerchantAccount' ) ) {
			trigger_error( 'Merchant Account object failed to load.', E_USER_WARNING );
			return NULL;
		}

		$intReversalDelayDays 	= 0;
		$intCutOffHour			= 15;
		$intCutOffMinutes		= 30;
		$strCutOffTime			= NULL;

		switch( $this->m_intPaymentTypeId ) {

			case CPaymentType::ACH:
				$intReversalDelayDays += ( int ) $objMerchantAccount->getAchReversalDelayDays();
				$strCutOffTime = $objMerchantAccount->getAchCutOffTime();
				break;

			case CPaymentType::VISA:
				$intReversalDelayDays += ( int ) $objMerchantAccount->getViReversalDelayDays();
				$strCutOffTime = $objMerchantAccount->getViCutOffTime();
				break;

			case CPaymentType::MASTERCARD:
				$intReversalDelayDays += ( int ) $objMerchantAccount->getMcReversalDelayDays();
				$strCutOffTime = $objMerchantAccount->getMcCutOffTime();
				break;

			case CPaymentType::AMEX:
				$intReversalDelayDays += ( int ) $objMerchantAccount->getAeReversalDelayDays();
				$strCutOffTime = $objMerchantAccount->getAeCutOffTime();
				break;

			case CPaymentType::DISCOVER:
				$intReversalDelayDays += ( int ) $objMerchantAccount->getDiReversalDelayDays();
				$strCutOffTime = $objMerchantAccount->getDiCutOffTime();
				break;

			case CPaymentType::CHECK_21:
				$intReversalDelayDays += ( int ) $objMerchantAccount->getCh21ReversalDelayDays();
				$strCutOffTime = $objMerchantAccount->getCh21CutOffTime();
				break;

			case CPaymentType::PAD:
				$intReversalDelayDays += ( int ) $objMerchantAccount->getOrFetchMethod( CPaymentType::PAD )->getReversalDelayDays();
				$strCutOffTime = $objMerchantAccount->getOrFetchMethod( CPaymentType::PAD )->getCutOffTime();
				break;

			default:
				return NULL;
				break;
		}

		if( false == is_null( $strCutOffTime ) ) {
			$intCutOffHour		= ( int ) \Psi\CStringService::singleton()->substr( $strCutOffTime, 0, 2 );
			$intCutOffMinutes 	= ( int ) \Psi\CStringService::singleton()->substr( $strCutOffTime, 3, 2 );

			if( 0 == $intCutOffHour && 0 == $intCutOffMinutes ) {
				$intCutOffHour = 24;
			}
		}

		// If the current hour of the day is greater than 15 (3pm), or it is saturday or sunday, then bump the Reversal date forward one day.
		if( 7 == date( 'N' ) || 6 == date( 'N' ) || $intCutOffHour < ( int ) date( 'G' ) || ( $intCutOffHour == ( int ) date( 'G' ) && $intCutOffMinutes <= ( int ) date( 'i' ) ) ) {
			$intReversalDelayDays++;
		}

		// Make sure that the settlement date is not in the transaction_holidays table.
		// Here we load all transaction holidays in the next 3 weeks.  We loop until the next day
		// that is not a holiday.

		$arrobjTransactionHolidays = \Psi\Eos\Payment\CTransactionHolidays::createService()->fetchTransactionHolidaysOnOrAfterDateAndBeforeDate( date( 'm/d/Y' ), date( 'm/d/Y', ( time() + ( 30 * 24 * 3600 ) ) ), $objPaymentDatabase );

		// Create an array of all prohibited transaction holiday settlement dates
		$arrintProhibitedTransactionHolidays = [];

		if( true == valArr( $arrobjTransactionHolidays ) ) {
			foreach( $arrobjTransactionHolidays as $objTransactionHoliday ) {
				array_push( $arrintProhibitedTransactionHolidays, strtotime( $objTransactionHoliday->getHolidayDate() ) );
			}
		}

		$intSettlementDate = time();

		// Now we loop on the Reversal delay days.
		while( $intReversalDelayDays > 0 ) {
			$intSettlementDate += ( 3600 * 24 );

			if( 'Sun' != date( 'D', $intSettlementDate ) && 'Sat' != date( 'D', $intSettlementDate ) && false == in_array( strtotime( date( 'm/d/Y', $intSettlementDate ) ), $arrintProhibitedTransactionHolidays ) ) {
				$intReversalDelayDays--;
			}
		}

		// Now make certain that we advance to a date that is not a weekend or a holiday
		// We might need this if reversal delay days is set to zero in the database.
		while( 'Sun' == date( 'D', $intSettlementDate ) || 'Sat' == date( 'D', $intSettlementDate ) || true == in_array( strtotime( date( 'm/d/Y', $intSettlementDate ) ), $arrintProhibitedTransactionHolidays ) ) {
			$intSettlementDate += ( 3600 * 24 );
		}

		// Trim the date down to the date only (no hours or seconds)
		$intSettlementDate = strtotime( date( 'm/d/Y', $intSettlementDate ) );

		return date( 'm/d/Y', $intSettlementDate );
	}

	// This function determines the date our system should settle the funds

	public function determineDistributeOn( $objClientDatabase, $objPaymentDatabase, $boolIsReversal = false ) {

		// Look at merchant account and see what the settlement_delay_days are.
		$objMerchantAccount = $this->getOrFetchMerchantAccount( $objClientDatabase );

		if( false == valObj( $objMerchantAccount, 'CMerchantAccount' ) ) {
			trigger_error( 'Merchant Account object failed to load.', E_USER_WARNING );
			return NULL;
		}

		$intDistributionDelayDays 	= 0;
		$intCutOffHour 				= 15;
		$intCutOffMinutes 			= 30;
		$strCutOffTime				= NULL;

		if( false == $boolIsReversal ) {
			switch( $this->m_intPaymentTypeId ) {

				case CPaymentType::ACH:
					$intDistributionDelayDays += ( int ) $objMerchantAccount->getAchDistributionDelayDays();
					$strCutOffTime = $objMerchantAccount->getAchCutOffTime();
					break;

				case CPaymentType::VISA:
					$intDistributionDelayDays += ( int ) $objMerchantAccount->getViDistributionDelayDays();
					$strCutOffTime = $objMerchantAccount->getViCutOffTime();
					break;

				case CPaymentType::MASTERCARD:
					$intDistributionDelayDays += ( int ) $objMerchantAccount->getMcDistributionDelayDays();
					$strCutOffTime = $objMerchantAccount->getMcCutOffTime();
					break;

				case CPaymentType::AMEX:
					$intDistributionDelayDays += ( int ) $objMerchantAccount->getAeDistributionDelayDays();
					$strCutOffTime = $objMerchantAccount->getAeCutOffTime();
					break;

				case CPaymentType::DISCOVER:
					$intDistributionDelayDays += ( int ) $objMerchantAccount->getDiDistributionDelayDays();
					$strCutOffTime = $objMerchantAccount->getDiCutOffTime();
					break;

				case CPaymentType::CHECK_21:
					$intDistributionDelayDays += ( int ) $objMerchantAccount->getCh21DistributionDelayDays();
					$strCutOffTime = $objMerchantAccount->getCh21CutOffTime();
					break;

				case CPaymentType::EMONEY_ORDER:
					$intDistributionDelayDays += ( int ) $objMerchantAccount->getEmoDistributionDelayDays();
					$strCutOffTime = $objMerchantAccount->getEmoCutOffTime();
					break;

				case CPaymentType::PAD:
					$intDistributionDelayDays += ( int ) $objMerchantAccount->getOrFetchMethod( CPaymentType::PAD )->getDistributionDelayDays();
					$strCutOffTime = $objMerchantAccount->getOrFetchMethod( CPaymentType::PAD )->getCutOffTime();
					break;

				default:
					return NULL;
					break;
			}
		}

		if( false == is_null( $strCutOffTime ) ) {
			$intCutOffHour		= ( int ) \Psi\CStringService::singleton()->substr( $strCutOffTime, 0, 2 );
			$intCutOffMinutes 	= ( int ) \Psi\CStringService::singleton()->substr( $strCutOffTime, 3, 2 );

			if( 0 == $intCutOffHour && 0 == $intCutOffMinutes ) {
				$intCutOffHour = 24;
			}
		}

		// Make sure that the settlement date is not in the transaction_holidays table.
		// Here we load all transaction holidays in the next 3 weeks.  We loop until the next day
		// that is not a holiday.

		$arrobjTransactionHolidays = \Psi\Eos\Payment\CTransactionHolidays::createService()->fetchTransactionHolidaysOnOrAfterDateAndBeforeDate( date( 'm/d/Y' ), date( 'm/d/Y', ( time() + ( 30 * 24 * 3600 ) ) ), $objPaymentDatabase );

		// Create an array of all prohibited transaction holiday settlement dates
		$arrintProhibitedTransactionHolidays = [];

		if( true == valArr( $arrobjTransactionHolidays ) ) {
			foreach( $arrobjTransactionHolidays as $objTransactionHoliday ) {
				array_push( $arrintProhibitedTransactionHolidays, strtotime( $objTransactionHoliday->getHolidayDate() ) );
			}
		}

		$intSettlementDate = time();

		// If the current hour of the day is greater than 15 (3pm), or it is saturday or sunday, or it is holiday then bump the distribution date forward one day.
		if( 7 == date( 'N' ) || 6 == date( 'N' ) || $intCutOffHour < ( int ) date( 'G' ) || ( $intCutOffHour == ( int ) date( 'G' ) && $intCutOffMinutes <= ( int ) date( 'i' ) )
			|| true == in_array( strtotime( date( 'm/d/Y', $intSettlementDate ) ), $arrintProhibitedTransactionHolidays ) ) {
			$intDistributionDelayDays++;
		}

		// Add 3 days to the settlment delay days if payment amount in excess of MA ceiling
		if( ( false == is_null( $objMerchantAccount->getUndelayedSettlementCeiling() ) && 0 < $objMerchantAccount->getUndelayedSettlementCeiling() && $this->getTotalAmount() > $objMerchantAccount->getUndelayedSettlementCeiling() ) && ( CPaymentType::ACH != $this->m_intPaymentTypeId || ( CPaymentType::ACH == $this->m_intPaymentTypeId && true == valId( $this->getCheckAccountTypeId() ) && false == in_array( $this->getCheckAccountTypeId(), [ CCheckAccountType::BUSINESS_CHECKING, CCheckAccountType::BUSINESS_SAVINGS ] ) ) ) ) {
			$intDistributionDelayDays += 3;
		}

		/**
		 * Determine distribution return prediction
		 */
		if( true == $objMerchantAccount->getDelayPredictedPaymentReturns() && in_array( $this->getPaymentTypeId(), [ CPaymentType::ACH, CPaymentType::CHECK_21 ] ) && $intDistributionDelayDays < 2 ) {
			$this->sendPaymentReturnPredictionRequest();
		}

		// Now we loop on the distribution delay days.
		while( $intDistributionDelayDays > 0 ) {
			$intSettlementDate += ( 3600 * 24 );

			if( 'Sun' != date( 'D', $intSettlementDate ) && 'Sat' != date( 'D', $intSettlementDate ) && false == in_array( strtotime( date( 'm/d/Y', $intSettlementDate ) ), $arrintProhibitedTransactionHolidays ) ) {
				$intDistributionDelayDays--;
			}
		}

		// Now make certain that we advance to a date that is not a weekend or a holiday
		// We might need this if distribution delay days is set to zero in the database.
		while( 'Sun' == date( 'D', $intSettlementDate ) || 'Sat' == date( 'D', $intSettlementDate ) || true == in_array( strtotime( date( 'm/d/Y', $intSettlementDate ) ), $arrintProhibitedTransactionHolidays ) ) {
			$intSettlementDate += ( 3600 * 24 );
		}

		// Trim the date down to the date only (no hours or seconds)
		$intSettlementDate = strtotime( date( 'm/d/Y', $intSettlementDate ) );

		return date( 'm/d/Y', $intSettlementDate );
	}

	/**
	 *
	 */
	public function sendPaymentReturnPredictionRequest() {
		$objMessage = \Psi\Libraries\PaymentQueue\Message\Payments\Info\CDistributionReturnPrediction::create();
		$objMessage->setArPaymentId( $this->getId() );

		$objAmqpMessageFactory = \Psi\Libraries\Queue\Factory\CAmqpMessageFactory::createService();

		$objMessageSender = \Psi\Libraries\Queue\Factory\CMessageSenderFactory::createService()->createSimpleSender( $objAmqpMessageFactory, $objMessage::QUEUE_NAME, [], NULL, \Psi\Libraries\Queue\CAmqpConnection::PAYMENTS_VHOST );

		try {
			$objMessageSender->send( $objMessage, [],  30 );
		} catch( \Exception $objException ) {
			$objLogger = \Psi\Libraries\Logger\Remote\CRemoteLogger::createService( $objMessage::QUEUE_NAME );
			$objLogger->error( $objException->getMessage() );
		}
	}

	public function determineIsPaymentPendingPhoneAuthorization() {

		if( $this->m_intPaymentStatusTypeId == CPaymentStatusType::PHONE_CAPTURE_PENDING ) {
			return true;
		}

		return false;
	}

	public function determineIsReversal() {

		if( true == array_key_exists( $this->m_intPaymentStatusTypeId, [ CPaymentStatusType::REVERSAL_CANCELLED, CPaymentStatusType::REVERSAL_PENDING, CPaymentStatusType::REVERSAL_RETURN, CPaymentStatusType::REVERSED ] ) ) {
			return true;
		}

		return false;
	}

	public function determineIsPaymentDeclined() {

		if( true == in_array( $this->m_intPaymentStatusTypeId, [ CPaymentStatusType::DECLINED, CPaymentStatusType::FAILED ] ) ) {
			return true;
		}

		return false;
	}

	// This function maps the data from a seed payment onto a payment that was split into several payments
	// becuase of property merchant account settings.  Specifically this is used while posting recurring payments
	// because no form post exists to map the data back to the regenerated objects.

	public function mapCorrespondingArPaymentData( $objArPayment ) {

		$this->setCid( $objArPayment->getCid() );
		$this->setPropertyId( $objArPayment->getPropertyId() );
		$this->setCustomerId( $objArPayment->getCustomerId() );
		$this->setLeaseId( $objArPayment->getLeaseId() );
		$this->setPaymentMediumId( $objArPayment->getPaymentMediumId() );
		$this->setScheduledPaymentId( $objArPayment->getScheduledPaymentId() );
		$this->setPaymentDatetime( $objArPayment->getPaymentDatetime() );
		$this->setIsFloating( $objArPayment->getIsFloating() );
		$this->setWaiveFee( $objArPayment->getWaiveFee() );
		$this->setPaymentStatusTypeId( $objArPayment->getPaymentStatusTypeId() );
		$this->setPsProductId( $objArPayment->getPsProductId() );
		$this->setPaymentTypeId( $objArPayment->getPaymentTypeId() );
		$this->setBilltoUnitNumber( stripslashes( $objArPayment->getBilltoUnitNumber() ) );
		$this->setBilltoAccountNumber( stripslashes( $objArPayment->getBilltoAccountNumber() ) );
		$this->setBilltoCompanyName( stripslashes( $objArPayment->getBilltoCompanyName() ) );
		$this->setBilltoNameFirst( stripslashes( $objArPayment->getBilltoNameFirst() ) );
		$this->setBilltoNameMiddle( stripslashes( $objArPayment->getBilltoNameMiddle() ) );
		$this->setBilltoNameLast( stripslashes( $objArPayment->getBilltoNameLast() ) );
		$this->setBilltoStreetLine1( stripslashes( $objArPayment->getBilltoStreetLine1() ) );
		$this->setBilltoStreetLine2( stripslashes( $objArPayment->getBilltoStreetLine2() ) );
		$this->setBilltoStreetLine3( stripslashes( $objArPayment->getBilltoStreetLine3() ) );
		$this->setBilltoCity( stripslashes( $objArPayment->getBilltoCity() ) );
		$this->setBilltoStateCode( stripslashes( $objArPayment->getBilltoStateCode() ) );
		$this->setBilltoProvince( stripslashes( $objArPayment->getBilltoProvince() ) );
		$this->setBilltoPostalCode( stripslashes( $objArPayment->getBilltoPostalCode() ) );
		$this->setBilltoCountryCode( stripslashes( $objArPayment->getBilltoCountryCode() ) );
		$this->setBilltoPhoneNumber( stripslashes( $objArPayment->getBilltoPhoneNumber() ) );
		$this->setBilltoEmailAddress( stripslashes( $objArPayment->getBilltoEmailAddress() ) );

		$this->setCcNameOnCard( stripslashes( $objArPayment->getCcNameOnCard() ) );
		$this->setCcCardNumberEncrypted( stripslashes( $objArPayment->getCcCardNumberEncrypted() ) );
		$this->setCcBinNumber( $objArPayment->getCcBinNumber() );
		$this->setCcExpDateMonth( stripslashes( $objArPayment->getCcExpDateMonth() ) );
		$this->setCcExpDateYear( stripslashes( $objArPayment->getCcExpDateYear() ) );

		$this->setIsDebitCard( $objArPayment->getIsDebitCard() );
		$this->setIsCheckCard( $objArPayment->getIsCheckCard() );
		$this->setIsGiftCard( $objArPayment->getIsGiftCard() );
		$this->setIsPrepaidCard( $objArPayment->getIsPrepaidCard() );
		$this->setIsCreditCard( $objArPayment->getIsCreditCard() );
		$this->setIsInternationalCard( $objArPayment->getIsInternationalCard() );
		$this->setIsCommercialCard( $objArPayment->getIsCommercialCard() );

		$this->setCheckBankName( stripslashes( $objArPayment->getCheckBankName() ) );
		$this->setCheckAccountTypeId( stripslashes( $objArPayment->getCheckAccountTypeId() ) );
		$this->setCheckAccountNumberEncrypted( stripslashes( $objArPayment->getCheckAccountNumberEncrypted() ) );
		$this->setCheckRoutingNumber( stripslashes( $objArPayment->getCheckRoutingNumber() ) );
		$this->setCheckNameOnAccount( stripslashes( $objArPayment->getCheckNameOnAccount() ) );
		$this->setPaymentMemo( stripslashes( $objArPayment->getPaymentMemo() ) );
		$this->setSecureReferenceNumber( stripslashes( $objArPayment->getSecureReferenceNumber() ) );
		$this->setTermsAcceptedOn( $objArPayment->getTermsAcceptedOn() );
		$this->setCardTypeId( $objArPayment->getCardTypeId() );

		return true;
	}

	public function generateBlankSystemEmail( $objDatabase, $strMessage = NULL, $objPropertyEmailRule = NULL ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$objClient	= $this->getOrFetchClient( $objDatabase );
		$objCustomer 			= $this->getOrFetchCustomer( $objDatabase );
		$objProperty			= $this->getOrFetchProperty( $objDatabase );

		if( true == valObj( $objProperty, 'CProperty' ) ) {
			$objMarketingMediaAssociation = $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objDatabase, true );
		}

		if( true == valObj( $objClient, 'CClient' ) ) {
			$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objDatabase );
		}
		$objMerchantAccount = $this->getOrFetchMerchantAccount( $objDatabase );

		$this->m_objSystemEmail = new CSystemEmail();
		$this->m_objSystemEmail->setDefaults();

		$this->m_objSmarty = new CPsSmarty( PATH_INTERFACES_COMMON, false );

		$this->m_objSmarty->assign_by_ref( 'ar_payment', 					$this );
		$this->m_objSmarty->assign_by_ref( 'property', 					$objProperty );
		$this->m_objSmarty->assign_by_ref( 'merchant_account', 			$objMerchantAccount );
		$this->m_objSmarty->assign_by_ref( 'client', 						$objClient );
		$this->m_objSmarty->assign_by_ref( 'message', 						$strMessage );
		$this->m_objSmarty->assign_by_ref( 'marketing_media_association', 	$objMarketingMediaAssociation );
		$this->m_objSmarty->assign_by_ref( 'default_company_media_file', 	$objLogoMarketingMediaAssociation );

		$this->m_objSmarty->assign( 'property_email_rule', 					$objPropertyEmailRule );
		$this->m_objSmarty->assign( 'ar_payment_future_cancellation_date',  date( 'M d, Y', strtotime( $this->getPaymentDatetime() ) + ( 60 * 60 * 24 * 7 ) ) );
		$this->m_objSmarty->assign( 'terms_and_condition_path', 			CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/Common/legal/resident_center_terms_conditions.html' );
		$this->m_objSmarty->assign( 'server_name', 							CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$this->m_objSmarty->assign( 'media_library_uri', 					CONFIG_MEDIA_LIBRARY_PATH );

		$this->m_objSystemEmail->setToEmailAddress( CSystemEmail::SUPPORT_EMAIL_ADDRESS );

		if( false == is_null( $this->getBilltoEmailAddress() ) ) {
			$this->m_objSystemEmail->setToEmailAddress( $this->getBilltoEmailAddress() );
		} elseif( true == valObj( $objCustomer, 'CCustomer' ) ) {
			$this->m_objSystemEmail->setToEmailAddress( $objCustomer->getUsername() );
		}

		$this->m_objSystemEmail->setSystemEmailTypeId( CSystemEmailType::SCHEDULED_PAYMENT_WARNING );
		$this->m_objSystemEmail->setPropertyId( $this->getPropertyId() );
		$this->m_objSystemEmail->setCid( $this->getCid() );
		$this->m_objSystemEmail->setCustomerId( $this->getCustomerId() );
		$this->m_objSystemEmail->setScheduledSendDatetime( date( 'm/d/Y H:i:s' ) );

		return true;
	}

	public function createX937ManualReturnReconciliationEntry( $objPaymentDatabase ) {

		$objReconciliationEntry = new CReconciliationEntry();
		$objReconciliationEntry->setProcessingBankAccountId( $this->getProcessingBankAccountId() );
		$objReconciliationEntry->setReconciliationEntryTypeId( CReconciliationEntryType::RECONCILIATION_ENTRY_MANUAL_AR_PAYMENT_X937_RETURN_OR_ADJUSTMENT );
		$objReconciliationEntry->setArPaymentId( $this->getId() );

		// First we need to find out if this is return is an amount adjustment.
		if( CReturnType::CHECK21_AMOUNT_ADJUSTMENT == $this->getReturnTypeId() ) {
			$objAdjustingArPayment = CArPayments::fetchArPaymentByAssociatedArPaymentIdByCid( $this->getId(), $this->getCid(), $objPaymentDatabase );

			if( false == valObj( $objAdjustingArPayment, 'CArPayment' ) ) {
				trigger_error( 'Adjusting AR payment did not load. (ARPaymentID:' . $this->getId() . ')', E_USER_WARNING );
				return false;
				exit;
			}

			$fltTotal = ( $objAdjustingArPayment->getTotalAmount() - $this->getTotalAmount() );

		} else {
			$fltTotal = ( -1 ) * $this->getTotalAmount();
		}

		$objReconciliationEntry->setTotal( $fltTotal );

		// Make sure that the return date is not in the common.transaction_holidays table.
		// Here we load all transaction holidays in the next 3 weeks.  We loop until the next day
		// that is not a holiday.

		if( false == is_null( $this->getReturnedOn() ) ) {
			$intReturnedOn = strtotime( $this->getReturnedOn() );
		} else {
			$intReturnedOn = time();
		}

		$arrobjTransactionHolidays = \Psi\Eos\Payment\CTransactionHolidays::createService()->fetchTransactionHolidaysOnOrAfterDateAndBeforeDate( date( 'm/d/Y', $intReturnedOn ), date( 'm/d/Y', ( $intReturnedOn + ( 21 * 24 * 3600 ) ) ), $objPaymentDatabase );

		// Create an array of all prohibited transaction holiday settlement dates
		$arrintProhibitedTransactionHolidays = [];

		if( true == valArr( $arrobjTransactionHolidays ) ) {
			foreach( $arrobjTransactionHolidays as $objTransactionHoliday ) {
				array_push( $arrintProhibitedTransactionHolidays, strtotime( $objTransactionHoliday->getHolidayDate() ) );
			}
		}

		$intReturnedOn = strtotime( '+1 day', $intReturnedOn );

		while( 'Sun' == date( 'D', $intReturnedOn ) || 'Sat' == date( 'D', $intReturnedOn ) || true == in_array( $intReturnedOn, $arrintProhibitedTransactionHolidays ) ) {
			$intReturnedOn = strtotime( '+1 day', $intReturnedOn );
		}

		$objReconciliationEntry->setMovementDatetime( date( 'm/d/Y', $intReturnedOn ) );

		return $objReconciliationEntry;
	}

	public function createNachaManualReturnReconciliationEntry( $objPaymentDatabase ) {

		$objReconciliationEntry = new CReconciliationEntry();
		$objReconciliationEntry->setProcessingBankAccountId( $this->getProcessingBankAccountId() );
		$objReconciliationEntry->setReconciliationEntryTypeId( CReconciliationEntryType::RECONCILIATION_ENTRY_MANUAL_AR_PAYMENT_NACHA_RETURN );
		$objReconciliationEntry->setArPaymentId( $this->getId() );

		$objReconciliationEntry->setTotal( -1 * $this->getTotalAmount() );

		// Make sure that the return date is not in the common.transaction_holidays table.
		// Here we load all transaction holidays in the next 3 weeks.  We loop until the next day
		// that is not a holiday.

		if( false == is_null( $this->getReturnedOn() ) ) {
			$intReturnedOn = strtotime( $this->getReturnedOn() );
		} else {
			$intReturnedOn = time();
		}

		$arrobjTransactionHolidays = \Psi\Eos\Payment\CTransactionHolidays::createService()->fetchTransactionHolidaysOnOrAfterDateAndBeforeDate( date( 'm/d/Y', $intReturnedOn ), date( 'm/d/Y', ( $intReturnedOn + ( 21 * 24 * 3600 ) ) ), $objPaymentDatabase );

		// Create an array of all prohibited transaction holiday settlement dates
		$arrintProhibitedTransactionHolidays = [];

		if( true == valArr( $arrobjTransactionHolidays ) ) {
			foreach( $arrobjTransactionHolidays as $objTransactionHoliday ) {
				array_push( $arrintProhibitedTransactionHolidays, strtotime( $objTransactionHoliday->getHolidayDate() ) );
			}
		}

		$intReturnedOn = strtotime( '+1 day', $intReturnedOn );

		while( 'Sun' == date( 'D', $intReturnedOn ) || 'Sat' == date( 'D', $intReturnedOn ) || true == in_array( $intReturnedOn, $arrintProhibitedTransactionHolidays ) ) {
			$intReturnedOn = strtotime( '+1 day', $intReturnedOn );
		}

		$objReconciliationEntry->setMovementDatetime( date( 'm/d/Y', $intReturnedOn ) );

		return $objReconciliationEntry;
	}

	public function fetchDuplicatePaymentWithInHours( $objClientDatabase, $intHours = 48 ) {

		$strSql = 'SELECT * FROM ar_payments
					WHERE property_id = ' . ( int ) $this->getPropertyId() . '
						AND customer_id = ' . ( int ) $this->getCustomerId() . '
						AND payment_amount = ' . ( float ) $this->getPaymentAmount() . '
						AND lease_id		= ' . ( int ) $this->getLeaseId() . '
						AND cid = ' . ( int ) $this->getCid() . '
						AND ( payment_status_type_id = ' . CPaymentStatusType::AUTHORIZED . ' OR payment_status_type_id = ' . CPaymentStatusType::CAPTURED . ')
						AND created_on >= ( NOW() - INTERVAL \'' . $intHours . ' hours\' )';

		return CArPayments::fetchArPayments( $strSql, $objClientDatabase );
	}

	public function fetchArPaymentDetailByArPaymentIdWithoutPaymentMemo( $intArPaymentId, $objPaymentDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ar_payment_details
					WHERE
						payment_memo IS NULL
						AND cid = ' . ( int ) $this->getCid() . '
						AND ar_payment_id = ' . ( int ) $intArPaymentId;

		return \Psi\Eos\Payment\CArPaymentDetails::createService()->fetchArPaymentDetail( $strSql, $objPaymentDatabase );
	}

	public function setCustomBatchedOnBulk( $arrintArPaymentIds, $strBatchedOn, $intUserId, &$objPaymentDatabase, &$arrobjClientDatabases ) {

		$strSql = '
			UPDATE
				ar_payments
			SET
				batched_on = \'' . $strBatchedOn . '\',
				updated_on = NOW(),
				updated_by = ' . ( int ) $intUserId . '
			WHERE
				id IN ( ' . implode( ',', $arrintArPaymentIds ) . ' ) AND cid = cid
			;';

		if( false == $this->executeSql( $strSql, $this, $objPaymentDatabase ) ) {
			return false;
		}

		$boolUpdateSuccess = true;

		foreach( $arrobjClientDatabases as $objClientDatabase ) {
			if( false == $this->executeSql( $strSql, $this, $objClientDatabase ) ) {
				$boolUpdateSuccess = false;
				break;
			}
		}

		return $boolUpdateSuccess;
	}

	public function loadDefaultDeleteData( $objDatabase, $boolLoadReturnItemFee = false, $intCompanyUserId = NULL ) {

		$this->m_objPropertyGlSetting 	= $this->getOrFetchPropertyGlSetting( $objDatabase );
		$objPaymentArTransaction 		= $this->fetchPaymentArTransaction( $objDatabase );

		if( true == valObj( $objPaymentArTransaction, 'CArTransaction' ) ) {
			$this->setDeletePostDate( $objPaymentArTransaction->getPostDate() );
		}

		if( true == valObj( $this->m_objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
			// If this is a future charge, we don't want to reverse this charge into a previous period.
			if( strtotime( $this->getPostMonth() ) > strtotime( $this->m_objPropertyGlSetting->getArPostMonth() ) ) {

				$strDeletePostMonth = $this->getPostMonth();
			} else {

				$strDeletePostMonth = $this->m_objPropertyGlSetting->getArPostMonth();
			}

			$this->setDeletePostMonth( $strDeletePostMonth );
			$this->setDeleteMemo( 'Reversal of transaction ID:' . $this->getId() );

			if( true == $boolLoadReturnItemFee ) {
				$this->fetchDefaultReturnItemFee( $objDatabase, $intCompanyUserId );
			}
		}

		return;
	}

	public function fetchDefaultReturnItemFee( $objDatabase, $intCompanyUserId ) {

		$objPostScheduledChargesLibrary 	= CPostScheduledChargeFactory::createObject( CScheduledChargePostingMode::RETURN_ITEM_FEE, $objDatabase );
		$objPostScheduledChargesCriteria	= $objPostScheduledChargesLibrary->loadPostScheduledChargesCriteria( $this->getCid(), [ $this->getPropertyId() ], $intCompanyUserId );

		$objPostScheduledChargesCriteria->setIsDryRun( true )
			->setLeaseIds( [ $this->getLeaseId() ] )
			->setPostPostedScheduledCharges( true );

		$objDatabase->begin();
		$objPostScheduledChargesLibrary->postScheduledCharges( $objPostScheduledChargesCriteria );
		$arrobjArTransactions = $objPostScheduledChargesLibrary->getArTransactions();
		$objDatabase->rollback();

		$fltReturnItemFee = 0;

		foreach( $arrobjArTransactions as $objArTransaction ) {
			$fltReturnItemFee += $objArTransaction->getTransactionAmount();
		}

		$this->setReturnItemFee( $fltReturnItemFee );

		return;
	}

	public function disassociateArPayment( $objClient, $objProperty, $objCompanyUser, $objDatabase, $objPaymentDatabase ) {

		$boolIsValid = true;

		$objIntegrationDatabase	 = NULL;

		if( true == valObj( $objProperty, 'CProperty' ) ) {
			$objIntegrationDatabase	 = $objProperty->fetchIntegrationDatabase( $objDatabase );
		}

		$objPaymentArTransaction 	= $this->fetchPaymentArTransaction( $objDatabase );
		$objArPaymentDetail 		= $this->getOrFetchArPaymentDetail( $objPaymentDatabase );

		$arrintNonEditableDefaultArCodeIds 	= [ CDefaultArCode::REFUND_PAYABLE, CDefaultArCode::SUBSIDY_REFUND_PAYABLE ];
		$arrobjArCodes 						= ( array ) $objClient->fetchArCodes( $objDatabase );

		if( false == valObj( $objPaymentArTransaction, 'CArTransaction' ) ) {
			switch( NULL ) {
				default:

					// Before we disassociate, we should try to export a reversal of this payment to offset the original.
					if( false == $this->forceReversalExport( $objCompanyUser->getId(), $objDatabase, $objPaymentDatabase ) ) {
						$boolIsValid = false;
						break;
					}

					if( false == $this->getUseExternalDatabaseTransaction() ) {
						$objDatabase->begin();
						$objPaymentDatabase->begin();
					}

					if( false == $this->insertArPaymentTransaction( CPaymentTransactionType::DISASSOCIATE, $objCompanyUser->getId(), $objPaymentDatabase ) ) {
						if( false == $this->getUseExternalDatabaseTransaction() ) {
							$objDatabase->rollback();
							$objPaymentDatabase->rollback();
						}
						$boolIsValid = false;
						break;
					}

					$this->setLeaseId( NULL );
					$this->setCustomerId( NULL );
					$this->setRemotePrimaryKey( NULL );

					if( false == $this->update( $objCompanyUser->getId(), $objDatabase, $objPaymentDatabase ) ) {
						if( false == $this->getUseExternalDatabaseTransaction() ) {
							$objDatabase->rollback();
							$objPaymentDatabase->rollback();
						}
						$boolIsValid = false;
						break;
					}

					// set exported on to null.
					if( true == valObj( $objArPaymentDetail, 'CArPaymentDetail' )
						&& ( ( true == valObj( $objProperty, 'CProperty' ) && false == valStr( $objProperty->getRemotePrimaryKey() ) )
							|| ( true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) && ( CIntegrationClientType::JENARK != $objIntegrationDatabase->getIntegrationClientTypeId() ) ) ) ) {
						$objArPaymentDetail->setExportedOn( NULL );

						// If there is no payment ar transaction, set le
						if( false == $objArPaymentDetail->update( $objCompanyUser->getId(), $objPaymentDatabase ) ) {
							$boolIsValid = false;
							break;
						}
					}

					if( false == $this->getUseExternalDatabaseTransaction() ) {
						$objDatabase->commit();
						$objPaymentDatabase->commit();
					}
					return true;
			}
			return false;
		}

		$arrobjAllocatedArTransactions 	= ( array ) $objPaymentArTransaction->fetchAppliedArTransactions( $objDatabase );

		if( true == valArr( $arrobjAllocatedArTransactions ) ) {
			foreach( $arrobjAllocatedArTransactions as $objAllocatedArTransaction ) {
				if( true == array_key_exists( $objAllocatedArTransaction->getArCodeId(), $arrobjArCodes ) && true == valObj( $arrobjArCodes[$objAllocatedArTransaction->getArCodeId()], 'CArCode' ) && ( true == in_array( $arrobjArCodes[$objAllocatedArTransaction->getArCodeId()]->getDefaultArCodeId(), $arrintNonEditableDefaultArCodeIds ) || true == $objAllocatedArTransaction->getIsDepositCredit() ) ) {
					$this->addSessionErrorMsg( 'This payment is allocated to a ' . $arrobjArCodes[$objAllocatedArTransaction->getArCodeId()]->getName() . '.You cannot disassociate this payment from this resident manually.' );
					return false;
				}
			}
		}

		$objPaymentArTransaction->setArPayment( $this );

		$objLease 				= $objPaymentArTransaction->getOrFetchLease( $objDatabase );
		$objPropertyGlSetting 	= $objPaymentArTransaction->getOrFetchPropertyGlSetting( $objDatabase );

		$objPaymentArTransaction->loadDefaultDeleteData( $objDatabase, $boolLoadReturnItemFee = true, $objCompanyUser->getId() );

		$objPaymentArTransaction->setDeletePostMonth( $objPropertyGlSetting->getArPostMonth() );
		$objPaymentArTransaction->setDeletePostDate( date( 'm/d/Y', time() ) );
		$objPaymentArTransaction->setDeleteMemo( 'Handled during cancel transfer.' );
		$objPaymentArTransaction->setReturnItemFee( NULL );// what value should be here??

		if( false == $objCompanyUser->getIsAdministrator() ) {
			$objPaymentArTransaction->setValidatePastOrFuturePostMonth( true );
			$objPaymentArTransaction->setPastPostMonthAndFuturePostMonth( $objCompanyUser->getId(), $objDatabase );
		}

		switch( NULL ) {
			default:
				$boolIsValid = true;

				if( false == $objPaymentArTransaction->validate( 'resident_works_disassociate', $objDatabase, $objPaymentDatabase ) ) {
					$boolIsValid = false;
					break;
				}

				// This is a hack to prevent dissociation of non-electronic payments without company merchant account id.
				if( false == valStr( $this->getCompanyMerchantAccountId() ) ) {
					$boolIsValid = false;
					break;
				}

				// Before we disassociate, we should try to export a reversal of this payment to offset the original.
				if( false == $this->forceReversalExport( $objCompanyUser->getId(), $objDatabase, $objPaymentDatabase ) ) {
					$boolIsValid = false;
					break;
				}

				if( false == $this->getUseExternalDatabaseTransaction() ) {
					$objDatabase->begin();
					$objPaymentDatabase->begin();
				}

				if( false == $objPaymentArTransaction->delete( $objCompanyUser->getId(), $objDatabase, $objPaymentDatabase, $boolDisassociateArPayment = true ) ) {
					$boolIsValid = false;
					break;
				}

				if( true == valObj( $objLease, 'CLease' ) ) {
					$objEventLibrary = $objLease->logActivityForLeaseInformation( CEventType::LEDGER_INFO, $objDatabase, NULL, $this, [ 'employee_name' => $objCompanyUser->getUsername(), 'event_type' => 'Payment', 'event' => 'Disassociated from resident' ], true );

					if( true == valObj( $objEventLibrary, 'CEventLibrary' ) ) {

						if( false == $objEventLibrary->insertEvent( $objCompanyUser->getId(), $objDatabase ) ) {
							$boolIsValid = false;
							break;
						}
					}
				}

				// set exported on to null.
				if( true == valObj( $objArPaymentDetail, 'CArPaymentDetail' )
					&& ( ( true == valObj( $objProperty, 'CProperty' ) && false == valStr( $objProperty->getRemotePrimaryKey() ) )
						|| ( true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) && ( CIntegrationClientType::JENARK != $objIntegrationDatabase->getIntegrationClientTypeId() ) ) ) ) {

					$objArPaymentDetail->setExportedOn( NULL );
					if( false == $objArPaymentDetail->update( $objCompanyUser->getId(), $objPaymentDatabase ) ) {
						$boolIsValid = false;
						break;
					}
				}

				if( false == $this->getUseExternalDatabaseTransaction() ) {
					$objDatabase->commit();
					$objPaymentDatabase->commit();
				}
				return $boolIsValid;
		}

		if( false == $this->getUseExternalDatabaseTransaction() ) {
			$objDatabase->rollback();
			$objPaymentDatabase->rollback();
		}

		return $boolIsValid;
	}

	public function associateArPayment( $objNewLeaseToAssociate, $objClient, $objProperty, $objCompanyUser, $objDatabase, $objPaymentDatabase ) {

		$boolIsValid = true;

		if( 1 != $this->getIsFloating() ) {
			$boolIsValid = $this->disassociateArPayment( $objClient, $objProperty, $objCompanyUser, $objDatabase, $objPaymentDatabase );
			if( false == $boolIsValid ) {
				return $boolIsValid;
			}
		}

		$objCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $objNewLeaseToAssociate->getPrimaryCustomerId(), $objClient->getId(), $objDatabase );
		$objPropertyGlSetting = $objProperty->fetchPropertyGlSetting( $objDatabase );

		$this->setPostMonth( $objPropertyGlSetting->getArPostMonth() );
		$this->setTransactionDatetime( date( 'm/d/Y' ) );
		$this->setPostMemo( 'Operated In Canceling Transfer.' );
		$this->setCustomerId( $objCustomer->getId() );
		$this->setLeaseId( $objNewLeaseToAssociate->getId() );
		$this->setBilltoUnitNumber( $objNewLeaseToAssociate->getUnitNumberCache() );

		switch( NULL ) {
			default:

				$boolIsValid &= $this->validate( 'update_no_floating', $objDatabase );

				if( false == $boolIsValid ) {
					break;
				}

				if( false == $this->getUseExternalDatabaseTransaction() ) {
					$objDatabase->begin();
					$objPaymentDatabase->begin();
				}

				$this->setBilltoNameFirst( $objCustomer->getNameFirst() );
				$this->setBilltoNameMiddle( $objCustomer->getNameMiddle() );
				$this->setBilltoNameLast( $objCustomer->getNameLast() );

				if( false == $this->insertArPaymentTransaction( CPaymentTransactionType::ASSOCIATE, $objCompanyUser->getId(), $objPaymentDatabase ) ) {
					if( false == $this->getUseExternalDatabaseTransaction() ) {
						$objDatabase->rollback();
						$objPaymentDatabase->rollback();
					}
					$boolIsValid &= false;
					break;
				}

				if( true == valObj( $objNewLeaseToAssociate, 'CLease' ) ) {
					$objEventLibrary = $objNewLeaseToAssociate->logActivityForLeaseInformation( CEventType::LEDGER_INFO, $objDatabase, NULL, $this, [ 'employee_name' => $objCompanyUser->getUsername(), 'event_type' => 'Payment', 'event' => 'Associated' ], true );

					if( true == valObj( $objEventLibrary, 'CEventLibrary' ) ) {
						if( false == $objEventLibrary->insertEvent( $objCompanyUser->getId(), $objDatabase ) ) {
							$boolIsValid = false;
							break;
						}
					}
				}

				if( false == $this->update( $objCompanyUser->getId(), $objDatabase, $objPaymentDatabase ) ) {
					if( false == $this->getUseExternalDatabaseTransaction() ) {
						$objDatabase->rollback();
						$objPaymentDatabase->rollback();
					}
					$boolIsValid = false;
					break;
				}

				if( false == in_array( $this->getPaymentStatusTypeId(), [ CPaymentStatusType::RECEIVED, CPaymentStatusType::CAPTURED ] ) ) {
					if( false == $this->getUseExternalDatabaseTransaction() ) {
						$objDatabase->commit();
						$objPaymentDatabase->commit();
					}
					// If the payment is authorized, or visa phone auth pending, we have to bail here.  Authorized transactions only post to the ledger at the point of capture.
					return $boolIsValid;
				}

				$objArTransaction = $this->fetchPaymentArTransaction( $objDatabase );

				if( false == valObj( $objArTransaction, 'CArTransaction' ) ) {
					$objArTransaction = $objNewLeaseToAssociate->createPaymentArTransaction( $this, $objDatabase );
					if( false == $objCompanyUser->getIsAdministrator() ) {
						$objArTransaction->setValidatePastOrFuturePostMonth( true );
						$objArTransaction->setPastPostMonthAndFuturePostMonth( $objCompanyUser->getId(), $objDatabase );
					}
					if( false == $objArTransaction->validate( 'post_payment_ar_transaction', $objDatabase ) || false == $objArTransaction->postPayment( $objCompanyUser->getId(), $objDatabase ) ) {

						if( false == $this->getUseExternalDatabaseTransaction() ) {
							$objDatabase->rollback();
							$objPaymentDatabase->rollback();
						}
						$boolIsValid = false;
						break;
					}
				}

				if( false == $this->getUseExternalDatabaseTransaction() ) {
					$objDatabase->commit();
					$objPaymentDatabase->commit();
				}

				$this->resetAndReIntegrate( $objCompanyUser->getId(), $objDatabase, $objPaymentDatabase );
				return $boolIsValid;
		}

		return $boolIsValid;
	}

	// written this function here on ar payment instead of ar transaction because, Now we have split payments, and it will be easy to implement deposit logic for splited trabsaction

	public function createArDeposit( $intCurrentUserId, $objClientDatabase, $objPaymentDatabase, $strProcessMethod = NULL ) {

		$arrobjOriginalArTransactions = $this->getActivePaymentArTransactions();
		if( false == valArr( $arrobjOriginalArTransactions ) ) return true;

		$arrobjOffsetArTransactions = ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchOffsetArTransactionsByArPaymentIdByArTransactionIdsByCid( $this->getId(), array_keys( $arrobjOriginalArTransactions ), $this->getCid(), $objClientDatabase );
		if( false == valArr( $arrobjOffsetArTransactions ) || \Psi\Libraries\UtilFunctions\count( $arrobjOffsetArTransactions ) != \Psi\Libraries\UtilFunctions\count( $arrobjOriginalArTransactions ) ) return true;

		$arrobjNewArTransactions = [];

		if( self::PROCESS_METHOD_REASSOCIATE == $strProcessMethod || self::PROCESS_METHOD_EDIT_PAYMENT == $strProcessMethod ) {
			$arrobjNewArTransactions = ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchUndepositedUnreversedArTransactionsByArPaymentIdsByCid( [ $this->getId() ], $this->getCid(), $objClientDatabase );

			if( false == valArr( $arrobjNewArTransactions ) ) return true;
		}

		// find out existing ar deposits
		$arrobjArDeposits = ( array ) \Psi\Eos\Entrata\CArDeposits::createService()->fetchArDepositsByArTransactionIdsByCid( array_keys( $arrobjOriginalArTransactions ), $this->getCid(), $objClientDatabase );

		if( true == valArr( $arrobjArDeposits ) ) {

			// if we found multiple active ar deposits for a payment we return
			if( 1 < \Psi\Libraries\UtilFunctions\count( $arrobjArDeposits ) ) return true;

			// fetch active ar deposit transactions
			$arrobjActiveArDepositTransactions = ( array ) \Psi\Eos\Entrata\CArDepositTransactions::createService()->fetchArDepositTransactionsByArTransactionIdsByCid( array_keys( $arrobjOriginalArTransactions ), $this->getCid(), $objClientDatabase );
			if( \Psi\Libraries\UtilFunctions\count( $arrobjActiveArDepositTransactions ) != \Psi\Libraries\UtilFunctions\count( $arrobjOriginalArTransactions ) ) return true;
		}

		$objActiveBankAccount 	= NULL;
		$objArDepositReference	= NULL;

		$boolIsDeposited = ( false == valArr( $arrobjArDeposits ) ) ? false : true;

		// we should not create zero amount auto-deposit if the payment is being returned and if it is a part of settlement distribution. Because settlemnt distribution script should always take care of it.
		if( false == $boolIsDeposited && self::PROCESS_METHOD_RETURN == $strProcessMethod && false == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintNonElectronicPaymentTypeIds ) ) {
			$arrobjArPaymentDistributions = ( array ) $this->fetchArPaymentDistributions( $objPaymentDatabase );
			if( true == valArr( $arrobjArPaymentDistributions ) ) return true;
		}

		// If original payment is deposited then use original bank account otherwise use rent bank account respective to the property for creating AR deposit
		if( false == valArr( $arrobjArDeposits ) ) {
			if( false == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintNonElectronicPaymentTypeIds ) && false == in_array( $this->getPaymentStatusTypeId(), [ CPaymentStatusType::VOIDED, CPaymentStatusType::RECALL ] ) && self::PROCESS_METHOD_REASSOCIATE != $strProcessMethod && self::PROCESS_METHOD_EDIT_PAYMENT != $strProcessMethod ) return true;

			$objActiveBankAccount = CBankAccounts::fetchActiveOperatingBankAccountByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objClientDatabase );
		} else {
			$objArDepositReference = array_shift( $arrobjArDeposits );
		}

		if( false == valObj( $objActiveBankAccount, 'CBankAccount' ) && false == valObj( $objArDepositReference, 'CArDeposit' ) ) return true;

		// Prepare array of undeposited ar transactions
		$arrobjUnDepositedArTransactions = [];

		$arrobjUnDepositedArTransactions = $arrobjOffsetArTransactions;
		if( false == $boolIsDeposited ) {
			$arrobjUnDepositedArTransactions = $arrobjUnDepositedArTransactions + $arrobjOriginalArTransactions;
		} elseif( true == valArr( $arrobjNewArTransactions ) ) {
			$arrobjUnDepositedArTransactions = $arrobjUnDepositedArTransactions + $arrobjNewArTransactions;
		}

		// all properties should have shared bank account in order to create ar deposit
		$intTempBankAccountId = ( true == valObj( $objArDepositReference, 'CArDeposit' ) ) ? $objArDepositReference->getBankAccountId() : $objActiveBankAccount->getId();

		$arrobjTempUndepositedArTransactions = ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchUndepositedArTransactionsByBankAccountIdByIdsByCid( $intTempBankAccountId, array_keys( $arrobjUnDepositedArTransactions ), $this->getCid(), $objClientDatabase );
		if( false == valArr( $arrobjTempUndepositedArTransactions ) || true == array_key_exists( 0, rekeyObjects( 'HasBankAccount', $arrobjTempUndepositedArTransactions ) ) ) return true;

		$arrobjArDepositTransactions = [];
		$arrstrPostMonths = $arrstrPostDates = [];
		$fltTotalDepositAmount = 0;

		foreach( $arrobjUnDepositedArTransactions as $objArTransaction ) {
			$fltTotalDepositAmount = bcadd( $fltTotalDepositAmount, $objArTransaction->getTransactionAmount(), 2 );

			$arrstrPostMonths[strtotime( $objArTransaction->getPostMonth() )] = $objArTransaction->getPostMonth();
			$arrstrPostDates[strtotime( $objArTransaction->getPostDate() )] = $objArTransaction->getPostDate();
		}

		$arrstrProcessMethods = [ self::PROCESS_METHOD_MANUAL_RETURN, self::PROCESS_METHOD_MANUAL_REVERSE, self::PROCESS_METHOD_REASSOCIATE, self::PROCESS_METHOD_EDIT_PAYMENT ];

		if( false == in_array( $strProcessMethod, $arrstrProcessMethods ) ) {
			// include ar_post_month of all properties for comparison
			$arrobjPropertyGlSettings = ( array ) CPropertyGlSettings::fetchCustomPropertyGlSettingsByPropertyIdsByCid( array_keys( rekeyObjects( 'PropertyId', $arrobjUnDepositedArTransactions ) ), $this->getCid(), $objClientDatabase );

			foreach( $arrobjPropertyGlSettings as $objPropertyGlSetting ) {
				$arrstrPostMonths[strtotime( $objPropertyGlSetting->getArPostMonth() )] = $objPropertyGlSetting->getArPostMonth();
			}

			$arrstrPostDates[strtotime( date( 'm/d/Y' ) )] = date( 'm/d/Y' );
		}

		if( false == valArr( $arrstrPostMonths ) || false == valArr( $arrstrPostDates ) )	return true;

		$objArDeposit = ( true == valObj( $objArDepositReference, 'CArDeposit' ) ) ? clone $objArDepositReference : new CArDeposit();
		$objArDeposit->setId( NULL );
		$objArDeposit->setCid( $this->getCid() );
		$objArDeposit->setIsInitialImport( 0 );
		$objArDeposit->setTransactionDatetime( 'NOW()' );
		$objArDeposit->setDepositAmount( - 1 * $fltTotalDepositAmount );
		$objArDeposit->setIsPosted( 0 );
		$objArDeposit->setGlTransactionTypeId( CGlTransactionType::AR_DEPOSIT );
		$objArDeposit->setSettlementDistributionId( NULL );
		$objArDeposit->setRemotePrimaryKey( NULL );
		$objArDeposit->setDepositMemo( NULL );
		$objArDeposit->setUpdatedBy( $intCurrentUserId );
		$objArDeposit->setCreatedBy( $intCurrentUserId );
		$objArDeposit->setUpdatedOn( NULL );
		$objArDeposit->setCreatedOn( NULL );
		$objArDeposit->setArTransactionIds( array_keys( $arrobjUnDepositedArTransactions ) );
		$objArDeposit->setDetailsInserted( 0 );
		$objArDeposit->setIsDeleted( 0 );

		// to check if we are reversing deposit from job costing then need to set requested post date for reversed ar_deposit record.
		if( true == valObj( $this->getOrFetchLease( $objClientDatabase ), 'CLease' ) && true == in_array( $this->m_objLease->getOccupancyTypeId(), array( COccupancyType::OWNER, COccupancyType::LENDER ) ) ) {
			$objArDeposit->setPostDate( $this->getDeleteDatetime() );
			$objArDeposit->setPostMonth( $this->getDeletePostMonth() );
		} else {
			$objArDeposit->setPostDate( $arrstrPostDates[max( array_keys( $arrstrPostDates ) )] );
			$objArDeposit->setPostMonth( $arrstrPostMonths[max( array_keys( $arrstrPostMonths ) )] );
		}

		if( false == valObj( $objArDepositReference, 'CArDeposit' ) ) {
			$objArDeposit->setBankAccountId( $objActiveBankAccount->getId() );
			$objArDeposit->setArDepositTypeId( CArDepositType::MANUAL );
		}

		if( false == $objArDeposit->insert( $intCurrentUserId, $objClientDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to insert ar deposit.' ) );
			return false;
		}

		return true;

	}

	public function createActivityLogForReturnPayment( $intCurrentUserId, $objClientDatabase ) {

		$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCustomCompanyUserByIdByCid( $intCurrentUserId, $this->getCid(), $objClientDatabase );

		if( true == $this->getIsSplit() ) {
			$arrobjLeases = ( array ) \Psi\Eos\Entrata\CLeases::createService()->fetchLeasesForSplitPaymentsByArPaymentIdByCid( $this->getId(), $this->m_intCid, $objClientDatabase );
		} else {
			$arrobjLeases[] = $this->getOrFetchLease( $objClientDatabase );
		}

		foreach( $arrobjLeases as $objLease ) {
			if( false == valObj( $objLease, 'CLease' ) ) {
				continue;
			}

			$objEventLibrary 	= $objLease->logActivityForLeaseInformation( CEventType::LEDGER_INFO, $objClientDatabase, NULL, $this, [ 'employee_name' => $objCompanyUser->getUsername(), 'event_type' => 'Payment', 'event' => 'Returned' ], true );

			if( false == valObj( $objEventLibrary, 'CEventLibrary' ) ) {
				$this->addSessionErrorMsg( 'Could not log activity for lease : ' . $objLease->getId() );
				break;
			}
			if( false == $objEventLibrary->insertEvent( $objCompanyUser->getId(), $objClientDatabase ) ) {
				$this->addErrorMsgs( $objEventLibrary->getErrorMsgs() );
				break;
			}
		}
		return true;
	}

	public function checkIsAllFutureDateArTransactions() {
		$arrobjArTransactions	= ( array ) $this->getArTransactions();
		$strTodaysDate			= strtotime( date( 'm/d/Y' ) );

		foreach( $arrobjArTransactions as $objArTransaction ) {
			if( $strTodaysDate > strtotime( $objArTransaction->getPostDate() ) && 0 < $objArTransaction->getTransactionAmountDue() ) {
				return false;
			}
		}

		return true;
	}

	// This function is intentionally overridden in order to jump some range of ids from payments.ar_payments table.
	// Do not move this function from here.

	public function fetchNextId( $objDatabase, $strTableName = NULL ) {

		$this->updateArPaymentSequence( $objDatabase );

		return parent::fetchNextId( $objDatabase );
	}

	public function updateArPaymentSequence( $objDatabase, $intSequenceIncrementValue = 1 ) {
		$strSql = 'SELECT last_value AS id FROM ar_payments_id_seq';

		$arrmixData = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixData ) || false == isset( $arrmixData[0]['id'] ) ) {
			return;
		}

		$intStartingId = $arrmixData[0]['id'];
		$intEndingId = $intStartingId + $intSequenceIncrementValue;

		if( CONFIG_CLOUD_ID == CCloud::LINDON_ID && $intEndingId > self::MAX_ID_LINDON ) {
			// we're in the Lindon cloud and the payment id is not between 0 and 10 billion
			$strSql = 'SELECT id FROM ar_payments WHERE id <  ' . self::MAX_ID_LINDON . ' ORDER BY id DESC LIMIT 1';
			$intDefaultValue = NULL;
		} elseif( CONFIG_CLOUD_ID == CCloud::IRELAND_ID && ( $intEndingId > self::MAX_ID_IRELAND || $intStartingId < self::MIN_ID_IRELAND ) ) {
			// we're in the Ireland cloud and the payment id is not between 10 and 20 billion
			$strSql = 'SELECT id FROM ar_payments WHERE id BETWEEN ' . self::MIN_ID_IRELAND . ' AND ' . self::MAX_ID_IRELAND . ' ORDER BY id DESC LIMIT 1';
			$intDefaultValue = self::MIN_ID_IRELAND;
		} else {
			return;
		}

		$arrmixResult = fetchData( $strSql, $objDatabase );
		if( false == valArr( $arrmixResult ) ) {
			$intNewStartingId = $intDefaultValue;
		} else {
			$intNewStartingId = $arrmixResult[0]['id'];
		}

		$objRemoteLog = \Psi\Libraries\Logger\Remote\CRemoteLogger::createService( 'ar_payment_sequence' );
		if( is_null( $intNewStartingId ) ) {
			$objRemoteLog->error( 'Failed to get current max value!' );
			trigger_error( 'Failed to generate a valid payment ID.', E_USER_ERROR );
		}

		$objRemoteLog->notice( 'Updating ar_payment_id sequence! New Starting ID: ' . ( int ) $intNewStartingId . ' Original Value: ' . ( int ) $intStartingId . ' DB User: ' . ( int ) $objDatabase->getDatabaseUserTypeId() );

		$intNextId = $intNewStartingId + 1;
		$strSql = 'SELECT setval( \'ar_payments_id_seq\', ' . ( int ) $intNextId . ', FALSE )';
		fetchData( $strSql, $objDatabase );
	}

	protected function validateAndCorrectSequenceByCloud( $intStartingId, $intEndingId, $objDatabase ) {
		if( CONFIG_CLOUD_ID == CCloud::LINDON_ID && self::MAX_ID_LINDON < $intEndingId ) {
			// we're in the Lindon cloud and the payment id is greater than 10 billion
			// $strSql = 'SELECT id FROM ar_payments WHERE id < ' . self::MAX_ID_LINDON . ' AND id NOT BETWEEN ' . self::BAD_ID_RANGE1_START . ' AND ' . self::BAD_ID_RANGE1_STOP . ' AND id NOT BETWEEN ' . self::BAD_ID_RANGE2_START . ' AND ' . self::BAD_ID_RANGE2_STOP . ' ORDER BY id DESC LIMIT 1';
			$strSql = 'SELECT id FROM ar_payments WHERE id < ' . self::MAX_ID_LINDON . ' AND id NOT BETWEEN ' . self::BAD_ID_RANGE1_START . ' AND ' . self::BAD_ID_RANGE1_STOP . ' AND id < ' . self::BAD_ID_RANGE2_START . ' ORDER BY id DESC LIMIT 1';
		} elseif( CONFIG_CLOUD_ID == CCloud::IRELAND_ID && ( self::MAX_ID_IRELAND < $intEndingId || self::MIN_ID_IRELAND > $intStartingId ) ) {
			// we're in the Ireland cloud and the payment id is not between 10 and 20 billion
			$strSql = 'SELECT id FROM ar_payments WHERE id BETWEEN ' . self::MIN_ID_IRELAND . ' AND ' . self::MAX_ID_IRELAND . ' ORDER BY id DESC LIMIT 1';
			$intMinIdByCloud = self::MIN_ID_IRELAND;
		} else {
			// no adjustment necessary
			return $intStartingId;
		}

		$arrmixResult = fetchData( $strSql, $objDatabase );
		if( false == valArr( $arrmixResult ) ) {
			$intMaxId = $intMinIdByCloud;
		} else {
			$intMaxId = $arrmixResult[0]['id'];
		}

		$objRemoteLog = \Psi\Libraries\Logger\Remote\CRemoteLogger::createService( 'ar_payment_sequence' );
		$objRemoteLog->notice( 'Updating ar_payment_id sequence! Max ID: ' . ( int ) $intMaxId . ' Starting ID: ' . ( int ) $intStartingId . ' Ending ID: ' . ( int ) $intEndingId . ' DB User: ' . $objDatabase->getDatabaseUserTypeId() );

		$intNextId = $intMaxId + 1;
		$strSql = 'SELECT setval( \'ar_payments_id_seq\', ' . ( int ) $intNextId . ', FALSE )';
		fetchData( $strSql, $objDatabase );

		return $intNextId;
	}

	// the api key can vary depending on who is sending the data to new relic, it is used for security purposes

	public function logPaymentToNewRelic( $strApiKey, $strOrigin, $strClientName, $strPropertyName ) {
		$arrmixAttributes = [
			'origin' => $strOrigin,
			'client' => $strClientName,
			'property' => $strPropertyName,
			'paymentId' => $this->getId(),
			'paymentType' => CPaymentTypes::paymentTypeIdToStr( $this->getPaymentTypeId() ),
			'paymentAmount' => $this->getTotalAmount()
		];
		$objNewRelic = \Psi\Libraries\NewRelic\CNewRelic::createService();
		$objNewRelic->postCustomEvent( $strApiKey, self::NEW_RELIC_EVENT_TYPE, $arrmixAttributes );
	}

	public function checkIsManualCaptureDay( $objPropertyPreference ) {

		if( false == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {
			return false;
		}

		$intTimestamp = time();
		$arrintManualCaptureDays = explode( ',', $objPropertyPreference->getValue() );

		if( false == valArr( $arrintManualCaptureDays ) ) {
			return false;
		}

		foreach( $arrintManualCaptureDays as $intKey => $intManualCaptureDay ) {
			if( false == is_numeric( $intManualCaptureDay ) || 31 < $intManualCaptureDay || 1 > $intManualCaptureDay ) {
				unset( $arrintManualCaptureDays[$intKey] );
			}
		}

		$arrintManualCaptureDays = ( true == valArr( $arrintManualCaptureDays ) ) ? array_unique( $arrintManualCaptureDays ) : [];

		return ( 0 < \Psi\Libraries\UtilFunctions\count( $arrintManualCaptureDays ) && in_array( date( 'j', $intTimestamp ), $arrintManualCaptureDays ) );
	}

	public function getOrFetchPropertyChargeSetting( $objClientDatabase ) {

		if( false == valObj( $this->m_objPropertyChargeSetting, 'CPropertyChargeSetting' ) ) {
			$this->m_objPropertyChargeSetting = \Psi\Eos\Entrata\CPropertyChargeSettings::createService()->fetchPropertyChargeSettingByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objClientDatabase );
		}

		return $this->m_objPropertyChargeSetting;
	}

	public function getIsPaymentCapturedBySms() {
		return $this->m_boolIsPaymentCapturedBySms;
	}

	public function autoAllocate( $intCurrentUserId, $objClientDatabase ) {
		$arrobjPaymentArTransactions    = ( array ) $this->fetchUnreversedPaymentArTransactions( $objClientDatabase );

		/** @var \CArTransaction $objArTransaction */
		foreach( $arrobjPaymentArTransactions as $objArTransaction ) {
			$objArTransaction->autoAllocate( $intCurrentUserId, $objClientDatabase );
		}
	}

	/**
	 * @param \CArPayment|\CScheduledPayment $objPayment
	 */
	public function addEditCustomerPaymentAccount( $intCurrentUserId, $objPayment, $objClientDatabase ) {
		$objCustomer = $objPayment->getOrFetchCustomer( $objClientDatabase );
		// Store Billing Info
		if( true === valId( $objPayment->getCustomerPaymentAccountId() ) ) {
			$arrmixStoredBillingInfo = [];
			$arrmixStoredBillingInfo['customer_payment_account_id'] = $objPayment->getCustomerPaymentAccountId();
			$arrmixStoredBillingInfo['customer_id'] = $objPayment->getCustomerId();
			$arrmixStoredBillingInfo['billto_name_first'] = $objCustomer->getNameFirst();
			$arrmixStoredBillingInfo['billto_name_last'] = $objCustomer->getNameLast();
			$arrmixStoredBillingInfo['billto_email_address'] = $objCustomer->getEmailAddress();

			$this->updateCustomerPaymentAccount( $arrmixStoredBillingInfo, $objPayment, $intCurrentUserId, $objClientDatabase );
		}
	}

	public function updateCustomerPaymentAccount( array $arrmixStoredBillingInfo, \CArPayment $objArPayment = NULL, $intCurrentUserId, $objClientDatabase ) {
		/** @var CPublishPaymentCenterSystem $this */
		$intCustomerId = $arrmixStoredBillingInfo['customer_id'];
		$intCustomerPaymentAccountId = $arrmixStoredBillingInfo['customer_payment_account_id'];
		$objCustomerPaymentAccount = \CCustomerPaymentAccounts::fetchCustomerPaymentAccountByIdByCustomerIdByCid( $intCustomerPaymentAccountId, $intCustomerId, $this->m_objClient->getId(), $objClientDatabase );
		if( false === valObj( $objCustomerPaymentAccount, 'CCustomerPaymentAccount' ) ) {
			return false;

		}

		$objCustomerPaymentAccount->setBilltoNameFirst( $arrmixStoredBillingInfo['billto_name_first'] );
		$objCustomerPaymentAccount->setBilltoNameLast( $arrmixStoredBillingInfo['billto_name_last'] );
		$objCustomerPaymentAccount->setBilltoEmailAddress( $arrmixStoredBillingInfo['billto_email_address'] );
		$objCustomerPaymentAccount->setLastUsedOn( date( 'm/d/Y H:i:s' ) );

		if( true === valObj( $objArPayment, 'CArPayment' ) ) {
			$objCustomerPaymentAccount->setBilltoStreetLine1( $objArPayment->getBilltoStreetLine1() );
			$objCustomerPaymentAccount->setBilltoStreetLine2( $objArPayment->getBilltoStreetLine2() );
			$objCustomerPaymentAccount->setBilltoStreetLine3( $objArPayment->getBilltoStreetLine3() );
			$objCustomerPaymentAccount->setBilltoCity( $objArPayment->getBilltoCity() );
			$objCustomerPaymentAccount->setBilltoStateCode( $objArPayment->getBilltoStateCode() );
			$objCustomerPaymentAccount->setBilltoPostalCode( $objArPayment->getBilltoPostalCode() );
			$objCustomerPaymentAccount->setBilltoCountryCode( $objArPayment->getBilltoCountryCode() );
			$objCustomerPaymentAccount->setIsCreditCard( $objArPayment->getIsCreditCard() );
			$objCustomerPaymentAccount->setIsDebitCard( $objArPayment->getIsDebitCard() );
			$objCustomerPaymentAccount->setIsCheckCard( $objArPayment->getIsCheckCard() );
			$objCustomerPaymentAccount->setIsGiftCard( $objArPayment->getIsGiftCard() );
			$objCustomerPaymentAccount->setIsPrepaidCard( $objArPayment->getIsPrepaidCard() );
			$objCustomerPaymentAccount->setIsInternationalCard( $objArPayment->getIsInternationalCard() );
			$objCustomerPaymentAccount->setIsCommercialCard( $objArPayment->getIsCommercialCard() );
		}

		if( false === $objCustomerPaymentAccount->update( $intCurrentUserId, $objClientDatabase ) ) {
			trigger_error( 'Failed to update Customer Payment Account ' . $arrmixStoredBillingInfo['customer_payment_account_id'], E_USER_WARNING );

			return false;
		}

		return true;
	}

	public function determinLastPaymentDate() {
		$strLastPaymentDate = date( 'm/d/Y', strtotime( $this->getPaymentDatetime() ) );
		if( CPaymentType::CHECK_21 === $this->getPaymentTypeId() && true === valStr( $this->getReceiptDatetime() ) ) {
			$strLastPaymentDate = date( 'm/d/Y', strtotime( $this->getReceiptDatetime() ) );
		}

		return $strLastPaymentDate;
	}

	public function validateDeclinedPayments( $intCompanyUserId, $objClientDatabase ) {
		$arrobjDeclinedPayments = ( array ) CArPayments::fetchDeclinedPaymentsByEntrataUserIdByCid( $intCompanyUserId, $this->getCid(), $objClientDatabase );
		if( true == valArr( $arrobjDeclinedPayments ) ) {
			$intDeclinedCount    = \Psi\Libraries\UtilFunctions\count( $arrobjDeclinedPayments );
			$objDeclineArPayment = array_shift( array_slice( $arrobjDeclinedPayments, ( CArPayment::ALLOWED_PAYMENT_DECLIENED_ATTEMPTS - 1 ), 1 ) );
			$objCurrentDate      = new CDate( date( 'm/d/Y H:i:s' ) );
			$strDate             = $objCurrentDate->getDate();
			if( true == valObj( $objDeclineArPayment, 'CArPayment' ) ) {
				$strDate = $objDeclineArPayment->getPaymentDateTime();
			}
			if( CArPayment::ALLOWED_PAYMENT_DECLIENED_ATTEMPTS <= $intDeclinedCount ) {
				$strPaymentDateTime = new DateTime( $strDate );
				$intTimeDifference  = ( 24 - ( time() - $strPaymentDateTime->getTimestamp() ) / ( 60 * 60 ) );
				$intHours           = floor( $intTimeDifference );
				$intMinutes         = floor( ( $intTimeDifference - $intHours ) * 60 );

				$strHealTime .= ( 0 != $intHours ) ? ( ' ' . ( int ) $intHours ) . ' hour(s) ' : '';
				$strHealTime .= ( 0 != $intMinutes ) ? ( ' ' . ( int ) $intMinutes . ' minute(s)' ) : '';
				$strHealTime = ( '' != $strHealTime ) ? $strHealTime : '';

				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'You have reached the maximum number of declined card payment attempts. You will be able to submit card payments again in ' . $strHealTime ) );
				return false;
			}

		}

		return true;
	}

	public function loadEmailDatabase() {
		if( false == valObj( $this->m_objEmailDatabase, 'CDatabase' ) ) {
			$this->m_objEmailDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::EMAIL, CDatabaseUserType::PS_DEVELOPER, CDatabaseServerType::POSTGRES, false );
		}
		return $this->m_objEmailDatabase;
	}

	public function alertIncorrectPaymentCall( $objClient, $arrintPaymentStatusTypeIds, $arrobjAssignedReturnTypes, $objClientDatabase ) {
		$arrstrPaymentStatuses = rekeyArray( 'id', ( array ) CPaymentStatusTypes::fetchPaymentStatusNamesByIds( $arrintPaymentStatusTypeIds, $objClientDatabase ) );

		$objSmarty = new CPsSmarty( PATH_INTERFACES_RESIDENT_WORKS, false );
		$objSmarty->assign( 'image_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );
		$objSmarty->assign_by_ref( 'payment', $this );
		$objSmarty->assign_by_ref( 'payment_statuses', $arrstrPaymentStatuses );
		$objSmarty->assign_by_ref( 'client', $objClient );
		$objSmarty->assign_by_ref( 'payment_return_types', $arrobjAssignedReturnTypes );
		$strHtmlEmailOutput = $objSmarty->fetch( PATH_PHP_INTERFACES . 'Templates/Common/system_emails/integration/triggering_incorrect_payment_call.tpl' );

		if( true == valStr( $strHtmlEmailOutput ) ) {

			$objSystemEmail = new CSystemEmail();
			$objSystemEmail->setHtmlContent( addslashes( $strHtmlEmailOutput ) );
			$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::PS_TO_COMPANY_ACCOUNTING );
			$objSystemEmail->setSubject( 'Incorrect payment call for payment id# ' . $this->getId() );
			$objSystemEmail->setToEmailAddress( CSystemEmail::XENTO_INTEGRATION_EMAIL_ADDRESS );
			$objSystemEmail->insert( SCRIPT_USER_ID, $this->loadEmailDatabase() );
			$this->m_objEmailDatabase->close();
		}
	}

	/**
	 * @param $objClientDatabase
	 * @return null
	 */
	public function getIsModulePermission( $intModuleId, $objClientDatabase ) {
		if( true == $this->m_objCompanyUser->getIsAdministrator() ) {
			return true;
		}

		$arrintModulePermissions = $this->m_objCompanyUser->fetchPermissionsByModuleId( $intModuleId, $objClientDatabase );
		if( true == valArr( $arrintModulePermissions ) && true == in_array( $intModuleId, $arrintModulePermissions ) ) {
			return true;
		}

		return false;
	}

	public function setIsTranslateEmail( $boolIsTranslate = false ) {
		$this->m_boolIsTranslate = $boolIsTranslate;
	}

	public function getIsTranslateEmail() {
		return $this->m_boolIsTranslate;
	}

	public function setArTransactionId( $intArTransactionId ) {
		$this->m_intArTransactionId = $intArTransactionId;
	}

	public function getArTransactionId() {
		return $this->m_intArTransactionId;
	}

	public function getArPaymentDate() {
		return date( 'M j, Y', strtotime( $this->getArPaymentDateTime() ) );
	}

	public function getArPaymentTime() {
		return date( 'h:i A', strtotime( $this->getArPaymentDateTime() ) );
	}

	public function getArPaymentDateTime() {
		if( false == valObj( $this->getProperty(), 'CProperty' ) ) {
			$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $this->m_objDatabase );
		} else {
			$objProperty = $this->getProperty();
		}
		$strPropertyTimeZoneName = 'America/Denver';
		if( true == valObj( $objProperty, 'CProperty' ) ) {
			$objPropertyTimeZone     = $objProperty->getOrfetchTimezone( $this->m_objDatabase );
			if( true == valObj( $objPropertyTimeZone, 'CTimeZone' ) ) {
				$strPropertyTimeZoneName = $objPropertyTimeZone->getTimeZoneName();
			}
		}
		return __( '{%t, 0, DATETIME_ALPHA_SHORT}', [ CInternationalDateTime::create( $this->getPaymentDatetime(), $strPropertyTimeZoneName ) ] );
	}

	public function getArPaymentAuthorizationCode() {
		$arrmixRequiredParameters = $this->getRequiredParameters();
		if( true == valArr( $arrmixRequiredParameters['split_ar_payment_ids'] ) ) {
			return '';
		}
		$objPaymentDatabase = $this->loadPaymentDataBaseForContactPoints();
		if( false == valObj( $objPaymentDatabase, 'CDatabase' ) ) {
			return '';
		}
		$strHtmlContent = '';
		$objPaymentDatabase->open();
		$intAuthCaptureArPaymentTransactionId = \Psi\Eos\Payment\CArPaymentTransactions::createService()->fetchTransactionIdByArPaymentIdByArPaymentTransactionTypeId( $this->getId(), CPaymentTransactionType::AUTH_CAPTURE, $objPaymentDatabase );
		$objPaymentDatabase->close();
		if( true == valId( $intAuthCaptureArPaymentTransactionId ) ) {
			$strHtmlContent = '<span class="header_text" style="font-size: 11px;font-weight: bold;font-family: Helvetica, sans-serif;line-height: 1.5;color: #868686;"> ' . __( 'AUTHORIZATION   CODE' ) . '</span><br/>' . $intAuthCaptureArPaymentTransactionId;
		}
		return $strHtmlContent;
	}

	public function getArPaymentNote() {
		$strHtmlContent = '';
		if( true == valId( $this->getId() ) ) {
			$objArPaymentDetail = \Psi\Eos\Entrata\CArPaymentDetails::createService()->fetchArPaymentDetailByArPaymentId( $this->getId(), $this->m_objDatabase );
			if( true == valObj( $objArPaymentDetail, 'CArPaymentDetail' ) && true == valStr( $objArPaymentDetail->getPaymentMemo() ) ) {
				$strHtmlContent = __( 'Memo:' ) . $objArPaymentDetail->getPaymentMemo();
			}
		}
		return $strHtmlContent;
	}

	public function getArPaymentIdForMergeField() {
		$arrmixRequiredParameters = $this->getRequiredParameters();
		if( true == valArr( $arrmixRequiredParameters['split_ar_payment_ids'] ) ) {
			return '';
		}
		$strHtmlContent = '';
		if( true == valId( $this->getId() ) ) {
			$strHtmlContent = '<span class="header_text" style="font-size: 11px;font-weight: bold;font-family: Helvetica, sans-serif;line-height: 1.5;color: #868686;">' . __( 'PAYMENT NUMBER' ) . ' </span><br/>' . $this->getId();
		}
		return $strHtmlContent;
	}

	public function getArPaymentAccountNumberMasked() {
		if( false == valId( $this->getPaymentTypeId() ) ) {
			return NULL;
		}
		$strLastFour = '';
		$objPaymentDatabase = $this->loadPaymentDataBaseForContactPoints();

		if( false == valObj( $objPaymentDatabase, 'CDatabase' ) ) {
			return NULL;
		}
		$objPaymentDatabase->open();
		$objArPaymentBilling = \Psi\Eos\Payment\CArPaymentBillings::createService()->fetchArPaymentBillingByCidByArPaymentId( $this->getCid(), $this->getId(), $objPaymentDatabase );
		$strCcCardNumber = '';
		if( true == valObj( $objArPaymentBilling, 'CArPaymentBilling' ) && \CPaymentType::SEPA_DIRECT_DEBIT != $this->getPaymentTypeId() ) {
			if( true == in_array( $this->getPaymentTypeId(), \CPaymentType::$c_arrintCreditCardPaymentTypes ) && true == valStr( $objArPaymentBilling->getCcCardNumberEncrypted() ) ) {
				$strCcCardNumber = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $objArPaymentBilling->getCcCardNumberEncrypted(), CONFIG_SODIUM_KEY_CC_CARD_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_CC_CARD_NUMBER ] );
			} else {
				if( true == valStr( $objArPaymentBilling->getCheckAccountNumberEncrypted() ) ) {
					$strCcCardNumber = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $objArPaymentBilling->getCheckAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
				}
			}
			if( true == valStr( $strCcCardNumber ) ) {
				$strLastFour = ' x' . \Psi\CStringService::singleton()->substr( $strCcCardNumber, -4 );
			}
		}

		if( \CPaymentType::SEPA_DIRECT_DEBIT == $this->getPaymentTypeId() ) {
			$objClientDatabase = $this->loadClientDatabaseForContactPoints( $this->getCid() );
			$objClientDatabase->open();
			$objPaymentEmailer = new CPaymentEmailer();
			$strLastFour = $objPaymentEmailer->getSepaLastFourDigitNumber( $this, $objClientDatabase, $objPaymentDatabase );
			$objClientDatabase->close();
		}

		$objPaymentDatabase->close();
		return $strLastFour;

	}

	public function getArPaymentSummary() {
		$arrmixRequiredParameters = $this->getRequiredParameters();
		if( true == valArr( $arrmixRequiredParameters['split_ar_payment_ids'] ) ) {
			$objPaymentDatabase = $this->loadPaymentDataBaseForContactPoints();

			if( false == valObj( $objPaymentDatabase, 'CDatabase' ) ) {
				return '';
			}
			$objPaymentDatabase->open();
			$arrobjArPayments = \Psi\Eos\Payment\CArPayments::createService()->fetchArPaymentsByIdsByCid( $arrmixRequiredParameters['split_ar_payment_ids'], $this->getCid(), $objPaymentDatabase );
			$strSplitArPaymentSummary = $this->getSplitArPaymentSummary( $arrobjArPayments, $objPaymentDatabase );

			if( true == valObj( $objPaymentDatabase, 'CDatabase' ) && false != $objPaymentDatabase->getIsOpen() ) {
				$objPaymentDatabase->close();
			}
			return $strSplitArPaymentSummary;

		} else {
			return $this->getSingleArPaymentSummary();
		}
	}

	public function getSplitArPaymentSummary( $arrobjArPayments, $objPaymentDatabase ) {
		$objPaymentDatabase->open();
		$strHtmlContent = '<tr style="width:100%; background: #F3F3F3;"><td colspan="3"><table style="padding-left: 25px;padding-top: 25px;padding-right: 25px;padding-bottom: 25px;font-size: 14px;width: 100%;"><tbody style="font-size: 14px;font-weight: lighter;font-family: "Helvetica", sans-serif;line-height: 1.5;color: #414141">';
		$fltTotalAmount = 0;
		foreach( $arrobjArPayments as $objArPayment ) {
			$strCurrencySymbol                     = $this->retrieveCurrencySymbol( $objArPayment->getCurrencyCode() );
			$intAuthCaptureArPaymentTransactionId = \Psi\Eos\Payment\CArPaymentTransactions::createService()->fetchTransactionIdByArPaymentIdByArPaymentTransactionTypeIds( $objArPayment->getId(), [ \CPaymentTransactionType::CAPTURE, \CPaymentTransactionType::AUTH_CAPTURE ], $objPaymentDatabase );
			$objMerchantAccount = $objArPayment->fetchCompanyMerchantAccount( $objPaymentDatabase );
			$fltPaymentAmount                     = round( $objArPayment->getPaymentAmount(), 2 );
			$fltConvFeeAmount                     = round( $objArPayment->getConvenienceFeeAmount(), 2 );
			$fltDonationAmount                    = round( $objArPayment->getDonationAmount(), 2 );
			$fltConDonationFeeAmount              = bcadd( $fltConvFeeAmount, $fltDonationAmount, 2 );
			$fltTotalAmount                       += bcadd( $fltPaymentAmount, $fltConDonationFeeAmount, 2 );

			$strHtmlContent .= '<tr>
									<td> ' . __( 'Payment Amount' ) . '</td>
									<td align="right"> ' . $strCurrencySymbol . __( '{%f,0,p:2}', [ $fltPaymentAmount ] ) . '</td>
								</tr>';
			if( 0 < $fltConvFeeAmount ) {
				$strCurrencyCode = ( true == valStr( $this->getCurrencyCode() ) ) ? $this->getCurrencyCode() : \CCurrency::CURRENCY_CODE_USD;
				if( \CCurrency::CURRENCY_CODE_CAD == $strCurrencyCode ) {
					$strConFeeType = __( 'Service Fee' );
				} else {
					$strConFeeType = __( 'Convenience Fee' );
				}
				$strHtmlContent .= '<tr>
										<td > ' . $strConFeeType . '</td >
										<td align="right"> ' . $strCurrencySymbol . __( '{%f,0,p:2}', [ $fltConvFeeAmount ] ) . '</td >
									</tr>';
			}
			if( 0 < $fltDonationAmount ) {
				$strHtmlContent .= '<tr >
										<td> ' . __( 'Donation Amount' ) . '</td >
										<td align="right"> ' . $strCurrencySymbol . __( '{%f,0,p:2}', [ $fltDonationAmount ] ) . '</td >
									</tr>';
			}
			if( true == valId( $intAuthCaptureArPaymentTransactionId ) ) {
				$strHtmlContent .= '<tr>
										<td> ' . __( 'Authorization Code' ) . '</td>
										<td align="right"> ' . $intAuthCaptureArPaymentTransactionId . '</td>
									</tr>';
			}

			if( true == valObj( $objMerchantAccount, 'CCompanyMerchantAccount' ) && true == valStr( $objMerchantAccount->getCountryCode() ) && ( \CCountry::CODE_CANADA == $objMerchantAccount->getCountryCode() || \CCountry::CODE_USA == $objMerchantAccount->getCountryCode() ) ) {
				$strPaymentNumber = __( 'Payment Number' );
			} else {
				$strPaymentNumber = __( 'Receipt Number' );
			}

			$strHtmlContent .= '<tr>
									<td>' . $strPaymentNumber . '</td>
									<td align="right">' . $objArPayment->getId() . '
									</td>
								</tr>';

			if( true == valObj( $objMerchantAccount, 'CCompanyMerchantAccount' ) ) {
				$strHtmlContent .= '<tr>
										<td>' . __( 'Shown on Statement as: {%s,0}', [ $objMerchantAccount->getMerchantName() ] ) . '</td>
									</tr>';
			}

			if( true == valStr( $objArPayment->getAutoCaptureOn() ) ) {
				$strHtmlContent .= '<tr style="color:red; font-style: italic;">
										<td> ' . __( 'Expected Post Date' ) . ' </td>
										<td align="right">' . date( 'm/d/Y H:i T', strtotime( $objArPayment->getAutoCaptureOn() ) ) . '</td>
									</tr>';
			}
			$strHtmlContent .= '<tr>
									<td colspan="2"><hr style="border-top: 1px dashed #414141;"></td>
								</tr>';
		}
		$strHtmlContent .= '<tr>
								<td><strong>' . __( 'Payment Total' ) . '</strong></td>
								<td align="right"><strong>' . $strCurrencySymbol . __( '{%f,0,p:2}', [ $fltTotalAmount ] ) . '</strong></td>
							</tr>
						</tbody></table>';

		if( true == valObj( $objPaymentDatabase, 'CDatabase' ) && false != $objPaymentDatabase->getIsOpen() ) {
			$objPaymentDatabase->close();
		}
		return $strHtmlContent;
	}

	public function getSingleArPaymentSummary() {
		$fltTotalAmount             = 0;
		if( false == valObj( $this->m_objClient, 'CClient' ) ) {
			$this->getOrFetchClient( $this->m_objDatabase );
		}
		if( true == valId( $this->getCompanyMerchantAccountId() ) ) {
			$strCurrencySymbol = $this->getCurrencyCode();
		} else {
			$strCurrencySymbol = ( true == valStr( $this->m_objClient->getCurrencyCode() ) ? $this->m_objClient->getCurrencyCode() : CCurrency::CURRENCY_CODE_USD );
		}
		$strCurrencySymbol          = $this->retrieveCurrencySymbol( $strCurrencySymbol );
		$fltPaymentAmount           = round( $this->getPaymentAmount(), 2 );
		$fltConvFeeAmount           = round( $this->getConvenienceFeeAmount(), 2 );
		$fltDonationAmount          = round( $this->getDonationAmount(), 2 );
		$fltConDonationFeeAmount    = bcadd( $fltConvFeeAmount, $fltDonationAmount, 2 );
		$fltTotalAmount             += bcadd( $fltPaymentAmount, $fltConDonationFeeAmount, 2 );

		$strHtmlContent = '<table style="padding-left: 25px;padding-top: 25px;padding-right: 25px;padding-bottom: 25px;font-size: 14px;width: 100%;"><tbody style="font-size: 14px;font-weight: lighter;font-family: "Helvetica", sans-serif;line-height: 1.5;color: #414141">';
		$strHtmlContent .= '<tr>
									<td > ' . __( 'Payment Amount' ) . '  </td>
									<td align="right"> ' . $strCurrencySymbol . __( '{%f,0,p:2}', [ $fltPaymentAmount ] ) . '</td>
							</tr>';

		if( 0 < $fltConvFeeAmount ) {
			$strCurrencyCode = ( true == valStr( $this->getCurrencyCode() ) ) ? $this->getCurrencyCode() : \CCurrency::CURRENCY_CODE_USD;
			if( \CCurrency::CURRENCY_CODE_CAD == $strCurrencyCode ) {
				$strConFeeType = __( 'Service Fee' );
			} else {
				$strConFeeType = __( 'Convenience Fee' );
			}

			$strHtmlContent .= '<tr>
										<td> ' . $strConFeeType . '</td >
										<td align="right"> ' . $strCurrencySymbol . __( '{%f,0,p:2}', [ $fltConvFeeAmount ] ) . '</td >
								</tr >';
		}

		if( 0 < $fltDonationAmount ) {
			$strHtmlContent .= '<tr >
										<td>' . __( 'Donation Amount' ) . '</td >
										<td align="right">' . $strCurrencySymbol . __( '{%f,0,p:2}', [ $fltDonationAmount ] ) . '</td >
								</tr >';
		}

		if( 0 < $fltConvFeeAmount || 0 < $fltDonationAmount ) {
			$strHtmlContent .= '<tr>
					<td colspan="2"><hr/></td>
				</tr
				<tr>
					<td ><strong> ' . __( 'Amount Paid' ) . ' </strong></td >
					<td align="right"><strong> ' . $strCurrencySymbol . __( '{%f,0,p:2}', [ $fltTotalAmount ] ) . ' </strong></td >
				</tr >';
		}

		$strHtmlContent .= '</tbody></table>';
		return $strHtmlContent;
	}

	public function getArPaymentLegalText() {
		if( false == valObj( $this->getClient(), 'CClient' ) ) {
			$objClient = $this->getOrFetchClient( $this->m_objDatabase );
		} else {
			$objClient = $this->getClient();
		}

		if( false == valObj( $this->getMerchantAccount(), 'CMerchantAccount' ) ) {
			$objPaymentDatabase = $this->loadPaymentDataBaseForContactPoints();
			if( false == valObj( $objPaymentDatabase, 'CDatabase' ) ) {
				return '';
			}
			$objPaymentDatabase->open();
			$objMerchantAccount = $this->fetchCompanyMerchantAccount( $objPaymentDatabase );
		} else {
			$objMerchantAccount = $this->getMerchantAccount();
		}

		$strLegalText = '';
		if( false == valObj( $this->getProperty(), 'CProperty' ) ) {
			$objProperty = $this->getOrFetchProperty( $this->m_objDatabase );
		} else {
			$objProperty = $this->getProperty();
		}

		$arrmixRequiredParameters = $this->getRequiredParameters();
		if( true == valObj( $objMerchantAccount, 'CCompanyMerchantAccount' ) && \CMerchantProcessingType::INTERNATIONAL != $objMerchantAccount->getMerchantProcessingTypeId() ) {
			if( false == valArr( $arrmixRequiredParameters['split_ar_payment_ids'] ) ) {
				$strLegalText = '<tr><td colspan="3"><br/></td></tr>
							 <tr>
									<td colspan="3" style="font-size: 14px;font-weight: lighter;font-family: "Helvetica" sans-serif;line-height: 1.5;color: #414141">' . __( 'Shown on Statement as: {%s,0}', [ $objMerchantAccount->getMerchantName() ] ) . '</td>
							 </tr>';
			}

			if( true == valObj( $objProperty, 'CProperty' ) ) {
				$strLegalText .= '<tr>
										<td colspan = "3" style="font-size: 11px;font-weight: lighter;font-family: "Helvetica" sans-serif;line-height: 1.5;color: #868686;" valign = "top" >' . __( 'This payment was processed by Entrata, Inc. on behalf of {%s,0}. ', [ $objProperty->getPropertyName() ] ) . '</td >
									</tr >';
			}

			if( true == valStr( $this->getPaymentMemo() ) ) {
				$strLegalText .= '<tr>
										<td colspan="3" style="color:#474747; font:normal normal normal 11px/18px arial;" valign="top">' .
					__( 'Memo: {%s,0}', [ $this->getPaymentMemo() ] ) . '<br/>
										</td>
									</tr>';
			}
		}

		if( true == valObj( $objPaymentDatabase, 'CDatabase' ) && false != $objPaymentDatabase->getIsOpen() ) {
			$objPaymentDatabase->close();
		}
		return $strLegalText;

	}

	public function loadPaymentDataBaseForContactPoints() {
		return \Psi\Eos\Connect\CDatabases::createService()->loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::PAYMENT, CDatabaseUserType::PS_DEVELOPER );
	}

	public function setRequiredParameters( $arrmixRequiredParameters = NULL ) {
		$this->m_arrmixRequiredParameters = $arrmixRequiredParameters;
	}

	public function getRequiredParameters() {
		return $this->m_arrmixRequiredParameters;
	}

	public function getCustomerAddresses() {

		$objCustomerAddress    = NULL;
		$arrstrAddressInfo     = [];
		$objLease              = $this->getOrFetchLease( $this->m_objDatabase );
		$arrintAddressTypeIds = [ \CAddressType::PRIMARY, \CAddressType::CURRENT, \CAddressType::BILLING, \CAddressType::PAYMENT, \CAddressType::PERMANENT, \CAddressType::PHYSICAL, \CAddressType::MAILING, \CAddressType::PO_BOX, \CAddressType::SHIPPING, \CAddressType::OTHER ];

		$arrobjCustomerAddress = ( array ) CCustomerAddresses::createService()->fetchCustomerAddressesByCustomerIdByAddressTypeIdsByCid( $this->getCustomerId(), $arrintAddressTypeIds, $this->getCid(), $this->m_objDatabase );
		$arrobjCustomerAddresses = rekeyObjects( 'AddressTypeId', $arrobjCustomerAddress );

		$boolIsPrimaryAddress = ( true == \valObj( $arrobjCustomerAddresses[\CAddressType::PRIMARY], 'CCustomerAddress' ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::PRIMARY]->getStreetLine1() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::PRIMARY]->getCity() && true == \valStr( $arrobjCustomerAddresses[\CAddressType::PRIMARY]->getStateCode() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::PRIMARY]->getPostalCode() ) ) );
		$boolIsCurrentAddress = ( false == $boolIsPrimaryAddress && true == \valObj( $arrobjCustomerAddresses[\CAddressType::CURRENT], 'CCustomerAddress' ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::CURRENT]->getStreetLine1() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::CURRENT]->getCity() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::CURRENT]->getStateCode() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::CURRENT]->getPostalCode() ) );
		$boolIsBillingAddress = ( false == $boolIsCurrentAddress && true == \valObj( $arrobjCustomerAddresses[\CAddressType::BILLING], 'CCustomerAddress' ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::BILLING]->getStreetLine1() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::BILLING]->getCity() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::BILLING]->getStateCode() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::BILLING]->getPostalCode() ) );
		$boolIsPaymentAddress = ( false == $boolIsBillingAddress && true == \valObj( $arrobjCustomerAddresses[\CAddressType::PAYMENT], 'CCustomerAddress' ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::PAYMENT]->getStreetLine1() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::PAYMENT]->getCity() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::PAYMENT]->getStateCode() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::PAYMENT]->getPostalCode() ) );
		$boolIsPermanentAddress = ( false == $boolIsPaymentAddress && true == \valObj( $arrobjCustomerAddresses[\CAddressType::PERMANENT], 'CCustomerAddress' ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::PERMANENT]->getStreetLine1() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::PERMANENT]->getCity() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::PERMANENT]->getStateCode() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::PERMANENT]->getPostalCode() ) );
		$boolIsPhysicalAddress = ( false == $boolIsPermanentAddress && true == \valObj( $arrobjCustomerAddresses[\CAddressType::PHYSICAL], 'CCustomerAddress' ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::PHYSICAL]->getStreetLine1() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::PHYSICAL]->getCity() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::PHYSICAL]->getStateCode() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::PHYSICAL]->getPostalCode() ) );
		$boolIsMailingAddress = ( false == $boolIsPhysicalAddress && true == \valObj( $arrobjCustomerAddresses[\CAddressType::MAILING], 'CCustomerAddress' ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::MAILING]->getStreetLine1() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::MAILING]->getCity() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::MAILING]->getStateCode() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::MAILING]->getPostalCode() ) );
		$boolIsPoBoxAddress = ( false == $boolIsMailingAddress && true == \valObj( $arrobjCustomerAddresses[\CAddressType::PO_BOX], 'CCustomerAddress' ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::PO_BOX]->getStreetLine1() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::PO_BOX]->getCity() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::PO_BOX]->getStateCode() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::PO_BOX]->getPostalCode() ) );
		$boolIsShippingAddress = ( false == $boolIsPoBoxAddress && true == \valObj( $arrobjCustomerAddresses[\CAddressType::SHIPPING], 'CCustomerAddress' ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::SHIPPING]->getStreetLine1() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::SHIPPING]->getCity() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::SHIPPING]->getStateCode() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::SHIPPING]->getPostalCode() ) );
		$boolIsOtherAddress = ( false == $boolIsShippingAddress && true == \valObj( $arrobjCustomerAddresses[\CAddressType::OTHER], 'CCustomerAddress' ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::OTHER]->getStreetLine1() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::OTHER]->getCity() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::OTHER]->getStateCode() ) && true == \valStr( $arrobjCustomerAddresses[\CAddressType::OTHER]->getPostalCode() ) );

		$arrstrAddressInfo['streetLine1'] = '';
		$arrstrAddressInfo['streetLine2'] = '';
		$arrstrAddressInfo['city']        = '';
		$arrstrAddressInfo['stateCode']   = '';
		$arrstrAddressInfo['postalCode']  = '';

		switch( $arrobjCustomerAddresses ) {
			case ( true == $boolIsPrimaryAddress ):
				$objCustomerAddress = $arrobjCustomerAddresses[\CAddressType::PRIMARY];
				break;

			case ( true == $boolIsCurrentAddress ):
				$objCustomerAddress = $arrobjCustomerAddresses[\CAddressType::CURRENT];
				break;

			case ( true == $boolIsBillingAddress ):
				$objCustomerAddress = $arrobjCustomerAddresses[\CAddressType::BILLING];
				break;

			case ( true == $boolIsPaymentAddress ):
				$objCustomerAddress = $arrobjCustomerAddresses[\CAddressType::PAYMENT];
				break;

			case ( true == $boolIsPermanentAddress ):
				$objCustomerAddress = $arrobjCustomerAddresses[\CAddressType::PERMANENT];
				break;

			case ( true == $boolIsPhysicalAddress ):
				$objCustomerAddress = $arrobjCustomerAddresses[\CAddressType::PHYSICAL];
				break;

			case ( true == $boolIsMailingAddress ):
				$objCustomerAddress = $arrobjCustomerAddresses[\CAddressType::MAILING];
				break;

			case ( true == $boolIsPoBoxAddress ):
				$objCustomerAddress = $arrobjCustomerAddresses[\CAddressType::PO_BOX];
				break;

			case ( true == $boolIsShippingAddress ):
				$objCustomerAddress = $arrobjCustomerAddresses[\CAddressType::SHIPPING];
				break;

			case ( true == $boolIsOtherAddress ):
				$objCustomerAddress = $arrobjCustomerAddresses[\CAddressType::OTHER];
				break;

			default:
		}

		if( true === \valObj( $objCustomerAddress, \CCustomerAddress::class ) ) {
			$arrstrAddressInfo['streetLine1'] = ( true == \valStr( $objCustomerAddress->getStreetLine1() ) ) ? $objCustomerAddress->getStreetLine1() : '';
			$arrstrAddressInfo['streetLine2'] = ( true == \valStr( $objCustomerAddress->getStreetLine2() ) ) ? $objCustomerAddress->getStreetLine2() : '';
			$arrstrAddressInfo['city']        = ( true == \valStr( $objCustomerAddress->getCity() ) ) ? $objCustomerAddress->getCity() : '';
			$arrstrAddressInfo['stateCode']   = ( true == \valStr( $objCustomerAddress->getStateCode() ) ) ? $objCustomerAddress->getStateCode() : '';
			$arrstrAddressInfo['postalCode']  = ( true == \valStr( $objCustomerAddress->getPostalCode() ) ) ? $objCustomerAddress->getPostalCode() : '';
		} else {
			$objUnitAddress = NULL;
			$objUnitOrPropertyAddress = NULL;
			if( true == \valId( $objLease->getPropertyUnitId() ) ) {
				$objUnitAddress = \Psi\Eos\Entrata\CUnitAddresses::createService()->fetchUnitPrimaryAddressByPropertyUnitIdByCid( $objLease->getPropertyUnitId(), $this->getCid(), $this->m_objDatabase );
			}

			if( true === \valObj( $objUnitAddress, \CUnitAddress::class ) && ( true == \valStr( $objUnitAddress->getStreetLine1() ) || true == \valStr( $objUnitAddress->getCity() ) || true == \valStr( $objUnitAddress->getStateCode() ) || true == \valStr( $objUnitAddress->getPostalCode() ) ) ) {
				$arrstrAddressInfo['streetLine1'] = ( true == \valStr( $objUnitAddress->getStreetLine1() ) ) ? $objUnitAddress->getStreetLine1() : '';
				$arrstrAddressInfo['streetLine2'] = ( true == \valStr( $objUnitAddress->getStreetLine2() ) ) ? $objUnitAddress->getStreetLine2() : '';
				$arrstrAddressInfo['city']        = ( true == \valStr( $objUnitAddress->getCity() ) ) ? $objUnitAddress->getCity() : '';
				$arrstrAddressInfo['stateCode']   = ( true == \valStr( $objUnitAddress->getStateCode() ) ) ? $objUnitAddress->getStateCode() : '';
				$arrstrAddressInfo['postalCode']  = ( true == \valStr( $objUnitAddress->getPostalCode() ) ) ? $objUnitAddress->getPostalCode() : '';
			} else {
				if( true == \valId( $objLease->getPropertyId() ) ) {
					$objUnitOrPropertyAddress = CPropertyAddresses::createService()->fetchPropertyAddressByPropertyIdByAddressTypeIdByCid( $objLease->getPropertyId(), ( array ) CAddressType::PRIMARY, $this->getCid(), $this->m_objDatabase );
					if( true === \valObj( $objUnitOrPropertyAddress, \CPropertyAddress::class ) ) {
						$arrstrAddressInfo['streetLine1'] = ( true == \valStr( $objUnitOrPropertyAddress->getStreetLine1() ) ) ? $objUnitOrPropertyAddress->getStreetLine1() : '';
						$arrstrAddressInfo['streetLine2'] = ( true == \valStr( $objUnitOrPropertyAddress->getStreetLine2() ) ) ? $objUnitOrPropertyAddress->getStreetLine2() : '';
						$arrstrAddressInfo['city']        = ( true == \valStr( $objUnitOrPropertyAddress->getCity() ) ) ? $objUnitOrPropertyAddress->getCity() : '';
						$arrstrAddressInfo['stateCode']   = ( true == \valStr( $objUnitOrPropertyAddress->getStateCode() ) ) ? $objUnitOrPropertyAddress->getStateCode() : '';
						$arrstrAddressInfo['postalCode']  = ( true == \valStr( $objUnitOrPropertyAddress->getPostalCode() ) ) ? $objUnitOrPropertyAddress->getPostalCode() : '';
					}
				}
			}
		}

		return $arrstrAddressInfo;

	}

	public function getStreetLine1() {

		$arrstrAddressInfo = $this->getCustomerAddresses();
		return $arrstrAddressInfo['streetLine1'];

	}

	public function getStreetLine2() {

		$arrstrAddressInfo = $this->getCustomerAddresses();
		if( true == \valStr( $arrstrAddressInfo['streetLine2'] ) ) {
			return $arrstrAddressInfo['streetLine2'] . '<br />';
		}
		return NULL;

	}

	public function getCity() {

		$arrstrAddressInfo = $this->getCustomerAddresses();
		return $arrstrAddressInfo['city'];

	}

	public function getStateCode() {

		$arrstrAddressInfo = $this->getCustomerAddresses();
		return $arrstrAddressInfo['stateCode'];

	}

	public function getPostalCode() {

		$arrstrAddressInfo = $this->getCustomerAddresses();
		return $arrstrAddressInfo['postalCode'];

	}

	public function retrieveCurrencySymbol( $strArCurrencyCode ) {
		$strCurrencyCode = ( true == \valStr( $strArCurrencyCode ) ) ? $strArCurrencyCode : \CCurrency::CURRENCY_CODE_USD;
		return \CCurrency::getCurrencySymbol( $strCurrencyCode );
	}

	public function getArPaymentChargesSummary() {
		$arrmixRequiredParameters = $this->getRequiredParameters();
		if( true == valArr( $arrmixRequiredParameters['split_ar_payment_ids'] ) ) {
			$objPaymentDatabase = $this->loadPaymentDataBaseForContactPoints();

			if( false == valObj( $objPaymentDatabase, 'CDatabase' ) ) {
				return '';
			}
			$objPaymentDatabase->open();
			$arrobjArPayments = \Psi\Eos\Payment\CArPayments::createService()->fetchArPaymentsByIdsByCid( $arrmixRequiredParameters['split_ar_payment_ids'], $this->getCid(), $objPaymentDatabase );
			$strSplitArPaymentSummary = $this->getSplitArPaymentChargesSummary( $arrobjArPayments );

			if( true == valObj( $objPaymentDatabase, 'CDatabase' ) && false != $objPaymentDatabase->getIsOpen() ) {
				$objPaymentDatabase->close();
			}
			return $strSplitArPaymentSummary;

		} else {
			return $this->getSingleArPaymentChargesSummary();
		}
	}

	public function getSingleArPaymentChargesSummary() {
		$strHtmlContent     = '';
		$fltTotalAmount     = 0;

		if( false == valObj( $this->m_objClient, 'CClient' ) ) {
			$this->getOrFetchClient( $this->m_objDatabase );
		}
		if( true == valId( $this->getCompanyMerchantAccountId() ) ) {
			$strCurrencySymbol = $this->getCurrencyCode();
		} else {
			$strCurrencySymbol = ( true == valStr( $this->m_objClient->getCurrencyCode() ) ? $this->m_objClient->getCurrencyCode() : CCurrency::CURRENCY_CODE_USD );
		}
		$strCurrencySymbol  = $this->retrieveCurrencySymbol( $strCurrencySymbol );
		$arrobjExistingArTransactions 	= ( array ) $this->fetchUnreversedPaymentArTransactions( $this->m_objDatabase );
		$arrobjArAllocatedTransactions 	= ( array ) rekeyObjects( 'LeaseId', \Psi\Eos\Entrata\CArTransactions::createService()->fetchAllocatedTransactionsByArCodeGroupIdCidByArTransactionIds( $this->m_objClient->getId(), array_keys( $arrobjExistingArTransactions ), $this->m_objDatabase ), true );
		$arrobjAllArCodes		        = ( array ) \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodesByCidByArCodesFilter( $this->m_objClient->getId(), new \CArCodesFilter( $boolExcludeReservedCodes = false, $boolShowDisabled = true ), $this->m_objDatabase );

		if( true == valArr( $arrobjArAllocatedTransactions ) && true == valId( $this->getLeaseId() ) && true == valArr( $arrobjArAllocatedTransactions[$this->getLeaseId()] ) ) {
			$strHtmlContent = '<tr>
									<td colspan="3"><span class="fontContentWapper" style="font-family:arial,helvetica,sans-serif;"><span class="header_text" style="font-size: 11px;font-weight: bold;font-family: Helvetica, sans-serif;line-height: 1.5;color: #868686;">' . __( 'CHARGES PAID' ) . '</span></span></td>
								</tr>
								<tr>
									<td colspan="3"><span class="fontContentWapper" style="font-family:arial,helvetica,sans-serif;">&nbsp;</span></td>
								</tr>
								<tr style="width:100%; background: #F3F3F3;">
									<td colspan="3">
									<table style="padding-left: 25px;padding-top: 25px;padding-right: 25px;padding-bottom: 25px;font-size: 14px;width: 100%;">
										<tbody style="font-size: 14px;font-weight: lighter;font-family: "Helvetica", sans-serif;line-height: 1.5;color: #414141">';

			foreach( $arrobjArAllocatedTransactions[$this->getLeaseId()] as $objArAllocatedTransaction ) {
				$strHtmlContent .= '<tr>
									<td> ' . __( 'Date' ) . '</td>
									<td align="right"> ' . __( '{%t,0,DATE_ALPHA_SHORT}', [ $objArAllocatedTransaction->getPostDate() ] ) . '</td>
								</tr>';

				if( true == valArr( $arrobjAllArCodes ) && true == valId( $objArAllocatedTransaction->getArCodeId() ) ) {
					$strDescription = $arrobjAllArCodes[$objArAllocatedTransaction->getArCodeId()]->getName();
				} else {
					$strDescription = $objArAllocatedTransaction->getArCodeName();
				}

				$strHtmlContent .= '<tr>
									<td> ' . __( 'Description' ) . '</td>
									<td align="right"> ' . $strDescription . '</td>
								</tr>';

				$strHtmlContent .= '<tr>
									<td> ' . __( 'Amount' ) . '</td>
									<td align="right"> ' . $strCurrencySymbol . __( '{%f,0,p:2}', [ round( abs( $objArAllocatedTransaction->getTransactionAmount() ), 2 ) ] ) . '</td>
								</tr>';

				$strHtmlContent .= '<tr>
									<td> ' . __( 'Amount Paid' ) . '</td>
									<td align="right"> ' . $strCurrencySymbol . __( '{%f,0,p:2}', [ round( abs( $objArAllocatedTransaction->getAllocationAmount() ), 2 ) ] ) . '</td>
								</tr>';

				$strHtmlContent .= '<tr>
									<td colspan="2"><hr style="border-top: 1px dashed #414141;"></td>
								</tr>';
				$fltTotalAmount += $objArAllocatedTransaction->getAllocationAmount();
			}

			$strHtmlContent .= '<tr>
									<td><strong>' . __( 'Total' ) . '</strong></td>
									<td align="right"><strong>' . $strCurrencySymbol . __( '{%f,0,p:2}', [ round( abs( $fltTotalAmount ), 2 ) ] ) . '</strong></td>
								</tr>
								</tbody>
							</table>';
		}
		return $strHtmlContent;
	}

	public function getSplitArPaymentChargesSummary( $arrobjArPayments ) {

		$strHtmlContent = '';
		$strHtmlContentBody = '';
		$fltTotalAmount = 0;
		$arrmixResult = [];
		if( false == valObj( $this->m_objClient, 'CClient' ) ) {
			$this->getOrFetchClient( $this->m_objDatabase );
		}
		$strCurrencySymbol  = $this->retrieveCurrencySymbol( $this->getCurrencyCode() );
		$arrobjAllArCodes   = ( array ) \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodesByCidByArCodesFilter( $this->m_objClient->getId(), new \CArCodesFilter( $boolExcludeReservedCodes = false, $boolShowDisabled = true ), $this->m_objDatabase );

		foreach( $arrobjArPayments as $objArPayment ) {
			$arrobjExistingArTransactions 	= ( array ) $objArPayment->fetchUnreversedPaymentArTransactions( $this->m_objDatabase );
			$arrobjArAllocatedTransactions 	= ( array ) rekeyObjects( 'LeaseId', \Psi\Eos\Entrata\CArTransactions::createService()->fetchAllocatedTransactionsByArCodeGroupIdCidByArTransactionIds( $this->m_objClient->getId(), array_keys( $arrobjExistingArTransactions ), $this->m_objDatabase ), true );
			if( true == valArr( $arrobjArAllocatedTransactions ) && true == valId( $objArPayment->getLeaseId() ) && true == valArr( $arrobjArAllocatedTransactions[$objArPayment->getLeaseId()] ) ) {
				foreach( $arrobjArAllocatedTransactions[$objArPayment->getLeaseId()] as $objArAllocatedTransaction ) {
					$fltPreAmountPaidAgainstSameCharge = 0;
					$intArCodeId = $objArAllocatedTransaction->getArCodeId();
					if( true == valArr( $arrmixResult[$intArCodeId] ) ) {
						$fltPreAmountPaidAgainstSameCharge = $arrmixResult[$intArCodeId]['amount_paid'];
						$fltPreAmountPaidAgainstSameCharge = isset( $arrmixResult[$intArCodeId]['amount_paid'] ) ? $arrmixResult[$intArCodeId]['amount_paid'] : 0;
					}
					$arrmixResult[$intArCodeId]['date'] = $objArAllocatedTransaction->getPostDate();
					if( true == valArr( $arrobjAllArCodes ) && true == valId( $objArAllocatedTransaction->getArCodeId() ) ) {
						$strDescription = $arrobjAllArCodes[$objArAllocatedTransaction->getArCodeId()]->getName();
					} else {
						$strDescription = $objArAllocatedTransaction->getArCodeName();
					}
					$arrmixResult[$intArCodeId]['description'] = $strDescription;
					$arrmixResult[$intArCodeId]['amount'] = $objArAllocatedTransaction->getTransactionAmount();
					$arrmixResult[$intArCodeId]['amount_paid'] = $objArAllocatedTransaction->getAllocationAmount() + $fltPreAmountPaidAgainstSameCharge;
					$fltTotalAmount += $objArAllocatedTransaction->getAllocationAmount();
				}
			}
		}

		if( true == valArr( $arrmixResult ) ) {
			$strHtmlContent = '<tr>
										<td colspan="3"><span class="fontContentWapper" style="font-family:arial,helvetica,sans-serif;"><span class="header_text" style="font-size: 11px;font-weight: bold;font-family: Helvetica, sans-serif;line-height: 1.5;color: #868686;">' . __( 'CHARGES PAID' ) . '</span></span></td>
									</tr>
									<tr>
										<td colspan="3"><span class="fontContentWapper" style="font-family:arial,helvetica,sans-serif;">&nbsp;</span></td>
									</tr>
									<tr style="width:100%; background: #F3F3F3;">
										<td colspan="3">
										<table style="padding-left: 25px;padding-top: 25px;padding-right: 25px;padding-bottom: 25px;font-size: 14px;width: 100%;">
											<tbody style="font-size: 14px;font-weight: lighter;font-family: "Helvetica", sans-serif;line-height: 1.5;color: #414141">';
			foreach( $arrmixResult as $arrmixCharge ) {
				$strHtmlContent .= '<tr>
										<td> ' . __( 'Date' ) . '</td>
										<td align="right"> ' . __( '{%t,0,DATE_ALPHA_SHORT}', [ $arrmixCharge['date'] ] ) . '</td>
									</tr>';

				$strHtmlContent .= '<tr>
										<td> ' . __( 'Description' ) . '</td>
										<td align="right"> ' . $arrmixCharge['description'] . '</td>
									</tr>';

				$strHtmlContent .= '<tr>
										<td> ' . __( 'Amount' ) . '</td>
										<td align="right"> ' . $strCurrencySymbol . __( '{%f,0,p:2}', [ round( abs( $arrmixCharge['amount'] ), 2 ) ] ) . '</td>
									</tr>';

				$strHtmlContent .= '<tr>
										<td> ' . __( 'Amount Paid' ) . '</td>
										<td align="right"> ' . $strCurrencySymbol . __( '{%f,0,p:2}', [ round( abs( $arrmixCharge['amount_paid'] ), 2 ) ] ) . '</td>
									</tr>';

				$strHtmlContent .= '<tr>
										<td colspan="2"><hr style="border-top: 1px dashed #414141;"></td>
									</tr>';
			}
			$strHtmlContent .= '<tr>
										<td><strong>' . __( 'Total' ) . '</strong></td>
										<td align="right"><strong>' . $strCurrencySymbol . __( '{%f,0,p:2}', [ round( abs( $fltTotalAmount ), 2 ) ] ) . '</strong></td>
									</tr>
								</tbody>
							</table>';
		}
		return $strHtmlContent;
	}

	public function getIsDeclinePayment() {
		return $this->m_boolIsDeclinePayment;
	}

	public function setIsDeclinePayment( $boolIsDeclinePayment ) {
		$this->m_boolIsDeclinePayment = $boolIsDeclinePayment;
	}

	public function getPaymentCreatorFullName( $objDatabase ) {

		if( false == \Psi\Libraries\UtilFunctions\valId( $this->getCustomerId() ) || false == \Psi\Libraries\UtilFunctions\valobj( $objDatabase, \CDatabase::class ) ) {
			return NULL;
		}

		$strPaymentCreatorFullName = '';
		$arrmixCustomerNames = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerFullNameAndCompanyNameByIdAndCid( $this->getCustomerId(), $this->getCid(), $objDatabase );

		if( true == \Psi\Libraries\UtilFunctions\valArr( $arrmixCustomerNames ) ) {
			if( true == \Psi\Libraries\UtilFunctions\valStr( $arrmixCustomerNames[0]['name_first'] ) || true == \Psi\Libraries\UtilFunctions\valStr( $arrmixCustomerNames[0]['name_last'] ) || true == \Psi\Libraries\UtilFunctions\valStr( $arrmixCustomerNames[0]['name_last_matronymic'] ) ) {
				$strPaymentCreatorFullName = $arrmixCustomerNames[0]['name_first'] . ' ' . $arrmixCustomerNames[0]['name_last'] . ' ' . $arrmixCustomerNames[0]['name_last_matronymic'];
			} elseif( true == \Psi\Libraries\UtilFunctions\valStr( $arrmixCustomerNames[0]['company_name'] ) ) {
				$strPaymentCreatorFullName = $arrmixCustomerNames[0]['company_name'];
			}
		}

		return $strPaymentCreatorFullName;
	}

	public function getArPaymentTypeMergeField() {
		$objPaymentType = \Psi\Eos\Entrata\CPaymentTypes::createService()->fetchPaymentTypeById( $this->getPaymentTypeId(), $this->m_objDatabase );
		if( true == valObj( $objPaymentType, 'CPaymentType' ) ) {
			return  $objPaymentType->getName();
		}
	}

	/**
	 * @param $intCid
	 * @return \CDatabase|mixed
	 */
	public function loadClientDatabaseForContactPoints( $intCid ) {
		$objClientDatabase = NULL;
		$objConnectDatabase		 = \Psi\Eos\Connect\CDatabases::createService()->createConnectDatabase( false );
		$arrobjClientDatabases	 = \Psi\Eos\Connect\CDatabases::createService()->fetchClientDatabasesByCIdByDatabaseUserTypeIdByDatabaseTypeId( $intCid, \CDatabaseUserType::RESIDENTPORTAL, \CDatabaseType::CLIENT, $objConnectDatabase );
		if( true == valArr( $arrobjClientDatabases ) ) {
			$objClientDatabase = current( $arrobjClientDatabases );
		}

		return $objClientDatabase;
	}

}
?>