<?php

class CComplianceItem extends CBaseComplianceItem {

	const OWNER_CRIMINAL			= 1;
	const CREDIT					= 2;
	const GENERAL_LIABILITY			= 7;
	const AUTO						= 8;
	const WORKERS_COMP				= 9;
	const MISCELLANEOUS_INSURANCE	= 10;
	const MISCELLANEOUS_DOCUMENT	= 6;
	const UMBRELLA 					= 25;
	const CRIMINAL 					= 30;
	const W9                        = 3;
	const CONTRACT                  = 32;
	const BUSINESS_LICENSE          = 4;
	const EXCESS_POLICY             = 33;

	protected $m_strComplianceRuleName;

	protected $m_intComplianceLevelId;

	public static $c_arrintCustomerSpecificSharigComplianceItemIds = [
		CComplianceItem::MISCELLANEOUS_DOCUMENT => CComplianceItem::MISCELLANEOUS_DOCUMENT,
		CComplianceItem::CONTRACT => CComplianceItem::CONTRACT
	];

	public static $c_arrintNonPropertyAssociatedComplianceItemIds = [
		CComplianceItem::W9 => CComplianceItem::W9,
		CComplianceItem::BUSINESS_LICENSE => CComplianceItem::BUSINESS_LICENSE
	];

	public function valId() {
		return true;
	}

	public function valComplianceTypeId() {
		return true;
	}

	public function valName() {
		return true;
	}

	public function valDescription() {
		return true;
	}

	public function valIsPublished() {
		return true;
	}

	public function valOrderNum() {
		return true;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function setComplianceRuleName( $strComplianceRuleName ) {
		$this->m_strComplianceRuleName = CStrings::strTrimDef( $strComplianceRuleName, 50, NULL, true );
	}

	public function getComplianceRuleName() {
		return $this->m_strComplianceRuleName;
	}

	public function setComplianceLevelId( $intComplianceLevelId ) {
		$this->m_intComplianceLevelId = ( int ) $intComplianceLevelId;
	}

	public function getComplianceLevelId() {
		return $this->m_intComplianceLevelId;
	}

	public static function getComplianceChildItemIdForDocLimits( $intParentComplianceItemId ) {

		switch( $intParentComplianceItemId ) {

			case self::GENERAL_LIABILITY:
				$intParentComplianceChildItemId = 16;
				break;

			case self::AUTO:
				$intParentComplianceChildItemId = 18;
				break;

			case self::WORKERS_COMP:
				$intParentComplianceChildItemId = 24;
				break;

			case self::MISCELLANEOUS_INSURANCE:
				$intParentComplianceChildItemId = 29;
				break;

			default:
				$intParentComplianceChildItemId = $intParentComplianceItemId;
				break;
		}

		return $intParentComplianceChildItemId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( isset( $arrmixValues['compliance_rule_name'] ) && $boolDirectSet ) {
			$this->m_strComplianceRuleName = trim( stripcslashes( $arrmixValues['compliance_rule_name'] ) );
		} elseif( isset( $arrmixValues['compliance_rule_name'] ) ) {
			$this->setComplianceRuleName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['compliance_rule_name'] ) : $arrmixValues['compliance_rule_name'] );
		}

		if( isset( $arrmixValues['compliance_level_id'] ) && $boolDirectSet ) {
			$this->m_intComplianceLevelId = trim( $arrmixValues['compliance_level_id'] );
		} elseif( isset( $arrmixValues['compliance_level_id'] ) ) {
			$this->setComplianceLevelId( $arrmixValues['compliance_level_id'] );
		}
	}

	public static function getComplianceItemName( $intComplianceItemId ) {

		$strComplianceItemName = '';

		switch( $intComplianceItemId ) {

			case self::OWNER_CRIMINAL:
				$strComplianceItemName = 'Owner Criminal';
				break;

			case self::CREDIT:
				$strComplianceItemName = 'Credit ';
				break;

			case self::GENERAL_LIABILITY:
				$strComplianceItemName = 'General Liability';
				break;

			case self::AUTO:
				$strComplianceItemName = 'Auto';
				break;

			case self::WORKERS_COMP:
				$strComplianceItemName = 'Workers Compensation';
				break;

			case self::MISCELLANEOUS_INSURANCE:
				$strComplianceItemName = 'Miscellaneous Insurance';
				break;

			case self::MISCELLANEOUS_DOCUMENT:
				$strComplianceItemName = 'Miscellaneous';
				break;

			case self::UMBRELLA:
				$strComplianceItemName = 'Umbrella';
				break;

			case self::CRIMINAL:
				$strComplianceItemName = 'Criminal';
				break;

			case self::W9:
				$strComplianceItemName = 'W-9';
				break;

			case self::CONTRACT:
				$strComplianceItemName = 'Contract';
				break;

			case self::BUSINESS_LICENSE:
				$strComplianceItemName = 'Business License';
				break;

			default:
		}
		return $strComplianceItemName;
	}

}
?>