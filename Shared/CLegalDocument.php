<?php

class CLegalDocument extends CBaseLegalDocument {

	protected $m_strDocumentType;
	protected $m_strDocumentContent;
	protected $m_intOriginalVersion;
	protected $m_intIsHtmlDocument;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLegalDocumentTypeId() {

		$boolIsValid = true;

		if( false == \valId( $this->getLegalDocumentTypeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'legal_document_type_id', 'Legal document type id is required.' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valEffectiveDatetime() {

		$boolIsValid			= true;
		$strEffectiveDatetime	= $this->getEffectiveDatetime();

		if( false == \valStr( $strEffectiveDatetime ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_datetime', 'Effective datetime is required.' ) );
			return $boolIsValid;
		}

		if( false === strtotime( $strEffectiveDatetime ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_datetime', 'Effective datetime is not a valid date.' ) );
			return $boolIsValid;
		}

		if( false == CValidation::validateDate( date( 'm/d/Y', $strEffectiveDatetime ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_datetime', 'Effective datetime is not formatted properly. (' . $strDateFormat . ')' ) );
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valCountryCode() {

		$boolIsValid = true;

		if( false == \valstr( $this->getCountryCode(), 2 ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'country_code', 'Country code is required.' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valLanguageCode() {

		$boolIsValid = true;

		if( false == \valstr( $this->getLanguageCode(), 2 ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Language_code', 'Language code is required.' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valVersion() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileName() {

		$boolIsValid = true;

		if( false == \valstr( $this->getFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', 'Document file is required.' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valIsMaterial() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPayment() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valLegalDocumentTypeId();
				$boolIsValid &= $this->valCountryCode();
				$boolIsValid &= $this->valLanguageCode();
				$boolIsValid &= $this->valEffectiveDatetime();
				$boolIsValid &= $this->valFileName();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valEffectiveDatetime();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setDocumentType( $strDocumentType ) {
		$this->set( 'm_strDocumentType', CStrings::strTrimDef( $strDocumentType, 100, NULL, true ) );
	}

	public function getDocumentType() {
		return $this->m_strDocumentType;
	}

	public function setOriginalVersion( $strOriginalVersion ) {
		$this->set( 'm_strOriginalVersion', CStrings::strToIntDef( $strOriginalVersion, NULL, false ) );
	}

	public function getOriginalVersion() {
		return $this->m_strOriginalVersion;
	}

	public function setIsHtmlDocument( $boolIsHtmlDocument ) {
		$this->set( 'm_intIsHtmlDocument', CStrings::strToBool( $boolIsHtmlDocument ) );
	}

	public function getIsHtmlDocument() {
		return $this->m_intIsHtmlDocument;
	}

	public function setDocumentContent( $strDocumentContent ) {
		$this->set( 'm_strDocumentContent', CStrings::strTrimDef( $strDocumentContent, -1, NULL, true ) );
	}

	public function getDocumentContent() {
		return $this->m_strDocumentContent;
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['document_type'] ) && false == is_null( $arrstrValues['document_type'] ) ) {
			$this->setDocumentType( $arrstrValues['document_type'] );
		}

		if( true == isset( $arrstrValues['original_version'] ) && false == is_null( $arrstrValues['original_version'] ) ) {
			$this->setOriginalVersion( $arrstrValues['original_version'] );
		}

		if( true == isset( $arrstrValues['is_html_document'] ) && false == is_null( $arrstrValues['is_html_document'] ) ) {
			$this->setIsHtmlDocument( $arrstrValues['is_html_document'] );
		}

		if( true == isset( $arrstrValues['document_content'] ) && false == is_null( $arrstrValues['document_content'] ) ) {
			$this->setDocumentContent( $arrstrValues['document_content'] );
		}

	}

}
?>