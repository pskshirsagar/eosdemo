<?php

class CSubsidyIncomeLevelType extends CBaseSubsidyIncomeLevelType {

	const EXTREMELY_LOW	= 1;
	const VERY_LOW		= 3;
	const SIXTY_PERCENT	= 4;
	const LOW			= 6;
	const BMIR 			= 7;

	const INCOME_LIMIT_EXTREMELY_LOW	= 0.30;
	const INCOME_LIMIT_VERY_LOW			= 0.50;
	const INCOME_LIMIT_SIXTY_PERCENT	= 0.60;
	const INCOME_LIMIT_LOW				= 0.80;
	const INCOME_LIMIT_BMIR				= 0.95;

	public static $c_arrstrCustomSubsidyIncomeLevelTypes = [
		self::VERY_LOW,
		self::SIXTY_PERCENT
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHudCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPercent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getLevelPercentById( $intSubsidyIncomeLevelTypeId ) {
		$fltPercent = NULL;

		switch( $intSubsidyIncomeLevelTypeId ) {
			case self::EXTREMELY_LOW:
				$fltPercent = self::INCOME_LIMIT_EXTREMELY_LOW;
				break;

			case self::VERY_LOW:
				$fltPercent = self::INCOME_LIMIT_VERY_LOW;
				break;

			case self::SIXTY_PERCENT:
				$fltPercent = self::INCOME_LIMIT_SIXTY_PERCENT;
				break;

			case self::LOW:
				$fltPercent = self::INCOME_LIMIT_LOW;
				break;

			case self::BMIR:
				$fltPercent = self::INCOME_LIMIT_BMIR;
				break;

			default:
				// do nothing
		}

		return $fltPercent;
	}

}
?>