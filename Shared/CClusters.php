<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CClusters
 * Do not add any new functions to this class.
 */

class CClusters extends CBaseClusters {

	public static function fetchAllClusters( $objDatabase, $strField = NULL ) {

		$strOrderBy = ( false == is_null( $strField ) ) ? ' ORDER BY ' . addslashes( $strField ) . '' : '';
		$strSql = 'SELECT * FROM clusters ' . $strOrderBy;

		return parent::fetchClusters( $strSql, $objDatabase );
	}

	public static function fetchAllPublishedClusters( $objDatabase, $strField = NULL ) {

		$strOrderBy = ( false == is_null( $strField ) ) ? ' ORDER BY ' . addslashes( $strField ) . '' : '';
		$strSql = 'SELECT * FROM clusters WHERE is_published = 1 AND id != ' . CCluster::MOBILE . $strOrderBy;

		return parent::fetchClusters( $strSql, $objDatabase );
	}

	public static function fetchClustersByIds( $arrintClustersIds, $objDatabase ) {
		if( false == valArr( $arrintClustersIds ) ) return NULL;
		$strSql = 'SELECT * FROM clusters WHERE id IN ( ' . implode( ',', $arrintClustersIds ) . ' )';

		return parent::fetchClusters( $strSql, $objDatabase );
	}

	public static function fetchClusterById( $intClustersId, $objDatabase ) {

		$strSql = 'SELECT * FROM clusters WHERE id = ' . ( int ) $intClustersId;
		return parent::fetchCluster( $strSql, $objDatabase );
	}

	public static function fetchClusterNameByDatabaseId( $intDatabaseId, $objDatabase ) {
		if( true == is_null( $intDatabaseId ) ) return;

		$strSql = 'SELECT
						c.name as cluster_name,
						d.id
					FROM
						clusters c
						RIGHT JOIN databases d ON ( c.id = d.cluster_id )
					WHERE
						d.id = ' . ( int ) $intDatabaseId . '
						AND d.deleted_by IS NULL
						AND d.deleted_on IS NULL ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllPublishedNonVirtualClusters( $objDatabase, $strField = NULL ) {

		$strOrderBy = ( false == is_null( $strField ) ) ? ' ORDER BY ' . addslashes( $strField ) . '' : '';
		$strSql = 'SELECT * FROM clusters WHERE is_published = 1 AND is_virtual = false ' . $strOrderBy;

		return self::fetchClusters( $strSql, $objDatabase );
	}

}
?>