<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CContactTypes
 * Do not add any new functions to this class.
 */

class CContactTypes extends CBaseContactTypes {

	const PRIMARY			= 1;
	const MANAGER			= 2;
	const OWNER				= 3;
	const MAINTENENCE		= 4;
	const BILLING			= 5;
	const IMPLEMENTATION	= 6;
	const FAILED_SETTLEMENT	= 7;
	const MISCELLANEOUS		= 8;

	public static function fetchContactTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CContactType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchContactType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CContactType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>