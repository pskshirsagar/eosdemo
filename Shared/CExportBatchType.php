<?php

class CExportBatchType extends CBaseExportBatchType {

	const  PROPERTY_SOLUTIONS 		= 1;
	const  RESIDENT_INSURE_MARKEL 	= 2;
	const  RESIDENT_INSURE_KEMPER 	= 3;
	const  RESIDENT_INSURE_QBE 		= 4;
	const  RESIDENT_INSURE_IDTHEFT	= 5;
	const  INTERNAL_TRANSFERS 		= 6;
	const  MASTER_POLICY			= 7;

	public static $c_arrintBatchTypeInsurance = [ self::RESIDENT_INSURE_MARKEL, self::RESIDENT_INSURE_KEMPER, self::RESIDENT_INSURE_QBE, self::RESIDENT_INSURE_IDTHEFT, self::MASTER_POLICY ];
}
?>