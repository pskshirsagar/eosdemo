<?php

class CPropertyCharities extends CBasePropertyCharities {

	public static function fetchAssociatedPropertyIdsByCompanyCharityIdByCid( $intCompanyCharityId, $intCid, $objDatabase, $boolIsDisabled = false ) {
		$strSql = 'SELECT
						property_id
					FROM
						property_charities
					WHERE
						company_charity_id = ' . ( int ) $intCompanyCharityId . '
						AND	cid = ' . ( int ) $intCid;

		if( false == $boolIsDisabled ) {
			$strSql .= ' AND disabled_by IS NULL AND disabled_on IS NULL';
		}

		$arrintPropertyIds = fetchData( $strSql, $objDatabase );

		return rekeyArray( 'property_id', $arrintPropertyIds );
	}

	public static function fetchPropertyCharitiesByPropertyIdsByCompanyCharityIdByCid( $arrintPropertyIds, $intCompanyCharityId, $intCid, $objDatabase, $boolIsDisabled = false ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return false;
		}

		$strSql = 'SELECT
						*
					FROM
						property_charities
					WHERE
						property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND	company_charity_id = ' . ( int ) $intCompanyCharityId . '
						AND	cid = ' . ( int ) $intCid;

		if( false == $boolIsDisabled ) {
			$strSql .= ' AND disabled_by IS NULL AND disabled_on IS NULL';
		}

		return self::fetchPropertyCharities( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );

	}

}
?>