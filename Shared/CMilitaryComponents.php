<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CMilitaryComponents
 * Do not add any new functions to this class.
 */

class CMilitaryComponents extends CBaseMilitaryComponents {

	public static function fetchAllMilitaryComponents( $objDatabase ) {

		$strSql = 'SELECT * from military_components WHERE is_published = TRUE';

		return self::fetchMilitaryComponents( $strSql, $objDatabase );
	}

	public static function fetchMilitaryComponents( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMilitaryComponent', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMilitaryComponent( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMilitaryComponent', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedMilitaryComponents( $objDatabase ) {
		$strSql = 'SELECT * FROM military_components WHERE is_published=true order by order_num';
		return self::fetchMilitaryComponents( $strSql, $objDatabase );
	}

	public static function fetchMilitaryComponentByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						mc.*
					FROM
						military_components mc
						JOIN military_installations mi ON ( mi.military_component_id = mc.id )
						JOIN property_details pd ON pd.military_installation_id = mi.id
                    WHERE
						pd.property_id = ' . ( int ) $intPropertyId . '
						AND pd.cid = ' . ( int ) $intCid;

		return parent::fetchMilitaryComponent( $strSql, $objDatabase );
	}

	public static function fetchCustomMilitaryComponents( $objDatabase ) {

		$strSql = 'SELECT
						mc.id, 
						mc.name, 
						mr.id as rank_id, 
						mr.name as rank_name,
						mr.order_num as rank_order_num,
						mpg.id military_pay_grade_id,
						mpg.name military_pay_grade_name
					FROM
						military_ranks mr
						JOIN military_components mc ON ( mc.id = mr.military_component_id )
						JOIN military_pay_grade_ranks mpr ON ( mpr.military_rank_id = mr.id )
						JOIN military_pay_grades mpg ON ( mpr.military_pay_grade_id = mpg.id )
						WHERE mr.is_published=true AND mc.is_published=true order by mc.order_num';

		return fetchData( $strSql, $objDatabase );
	}

}
?>