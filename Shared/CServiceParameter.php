<?php

class CServiceParameter extends CBaseServiceParameter {

    public function __construct() {
        parent::__construct();
	    // FIXME remove this static declaration once UI implemented at CA which allows user to select specific version.
	    $this->m_intMinorApiVersionId = CApiVersion::TMP_DEFAULT_MINOR_API_VERSION;

        return;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valServiceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName( $objDatabase ) {
        $boolIsValid = true;
        if( false == valStr( $this->getName() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Parameter Name is required.' ) );
        } elseif( false != \Psi\CStringService::singleton()->strstr( $this->getName(), ' ' ) ) {
	       	$boolIsValid = false;
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Space is not allowed in parameter name.' ) );

        } elseif( false != isset( $objDatabase ) && false != is_numeric( $this->getServiceId() ) ) {
        	$strWhere = ' WHERE lower(name) = \'' . \Psi\CStringService::singleton()->strtolower( $this->getName() ) . '\' AND service_id = ' . $this->getServiceId() . ' AND minor_api_version_id = ' . $this->getMinorApiVersionId() . ' AND is_response = ' . $this->sqlIsResponse();
        	$strWhere .= ( 0 < $this->getId() ) ? ' AND id NOT IN (' . $this->getId() . ')' : '';
        	$intCount = \Psi\Eos\Entrata\CServiceParameters::createService()->fetchServiceParameterCount( $strWhere, $objDatabase );

        	if( 0 < $intCount ) {
        		$boolIsValid = false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', '- This parameter is already exists.' ) );
        	}
        }

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valType() {
        $boolIsValid = true;
        if( false == valStr( $this->getType() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', 'Please select parameter type.' ) );
        }
        return $boolIsValid;
    }

    public function valMaxOccurance() {
        $boolIsValid = true;
        if( false == is_numeric( $this->getMaxOccurance() ) || 0 >= $this->getMaxOccurance() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_occurance', 'Maximum occurrence should be numeric & greater than 0.' ) );
        }
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsRequired() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsMultiple() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        if( false == is_numeric( $this->getOrderNum() ) || 1 > $this->getOrderNum() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', 'Order number should be numeric & greater than 0.' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;
        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName( $objDatabase );
            	$boolIsValid &= $this->valType();
            	$boolIsValid &= $this->valMaxOccurance();
            	$boolIsValid &= $this->valOrderNum();
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid = true;
                break;

            default:
            	// default case
            	$boolIsValid = true;
                break;
        }

        return $boolIsValid;
    }

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$strSql = parent::update( $intCurrentUserId, $objDatabase, true );
		if( false == valStr( $strSql ) ) {
			return false;
		}
		$strSql = rtrim( $strSql, ';' );
		$strSql .= ' AND minor_api_version_id = ' . ( int ) $this->sqlMinorApiVersionId() . ';';
		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$strSql = rtrim( parent::delete( $intCurrentUserId, $objDatabase, true ), ';' );
		$strSql .= ' AND minor_api_version_id = ' . ( int ) $this->sqlMinorApiVersionId() . ';';
		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

}
?>