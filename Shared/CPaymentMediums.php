<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CPaymentMediums
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CPaymentMediums extends CBasePaymentMediums {

	public static function fetchPaymentMediums( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CPaymentMedium', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchPaymentMedium( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CPaymentMedium', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function paymentMediumIdToStr( $intPaymentMediumId ) {
		switch( $intPaymentMediumId ) {
			case CPaymentMedium::WEB:
				return 'Web';
			case CPaymentMedium::RECURRING:
				return 'Recurring';
			case CPaymentMedium::TERMINAL:
				return 'Terminal';
			default:
				trigger_error( 'Invalid payment medium ID.', E_USER_WARNING );
				break;
		}
	}

}
?>