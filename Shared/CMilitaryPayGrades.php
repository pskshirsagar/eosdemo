<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CMilitaryPayGrades
 * Do not add any new functions to this class.
 */

class CMilitaryPayGrades extends CBaseMilitaryPayGrades {

	public static function fetchAllMilitaryPayGrades( $objDatabase ) {

		$strSql = 'SELECT * from military_pay_grades WHERE is_published = TRUE order by order_num';

		return self::fetchMilitaryPayGrades( $strSql, $objDatabase );
	}

	public static function fetchMilitaryPayGrades( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMilitaryPayGrade', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMilitaryPayGrade( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMilitaryPayGrade', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedMilitaryPayGrades( $objDatabase ) {

		$strSql = 'SELECT * FROM military_pay_grades WHERE is_published=true order by order_num';
		return self::fetchMilitaryPayGrades( $strSql, $objDatabase );
	}

	public static function fetchMilitaryPayGradesWithUnitTypesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT mpg.*
					FROM
						unit_type_military_pay_grades utmpg
					JOIN military_pay_grades mpg ON ( utmpg.military_pay_grade_id = mpg.id )
					JOIN unit_types ut ON ( ut.id = utmpg.unit_type_id )
					WHERE
						utmpg.cid = ' . ( int ) $intCid . '
						AND mpg.is_published = TRUE
						AND ut.property_id in ( ' . implode( ',', $arrintPropertyIds ) . ' )
					GROUP BY mpg.id
					HAVING count(mpg.id) > 0
					ORDER BY mpg.name
				  ';

		return parent::fetchMilitaryPayGrades( $strSql, $objDatabase );
	}

	public static function fetchMilitaryPayGradesByMilitaryPayGardeNames( $arrstrMilitaryPayGradeNames, $objDatabase ) {

		if( false == valArr( $arrstrMilitaryPayGradeNames ) ) {
			return NULL;
		}

		$strSql = 'select
						*
					FROM
						military_pay_grades
					WHERE
						name IN ( \'' . implode( "','", $arrstrMilitaryPayGradeNames ) . '\' )';

		return parent::fetchMilitaryPayGrades( $strSql, $objDatabase );
	}

	public static function fetchPublishedMilitaryPayGradesByMilitaryComponentIdByMilitaryRankId( $intMilitaryComponentId, $intMilitaryRankId, $objDatabase, $intLimit = NULL ) {

		if( false == valId( $intMilitaryComponentId ) || false == valId( $intMilitaryRankId ) ) {
			return NULL;
		}

		$strSqlLimit = '';

		if( true == valId( $intLimit ) ) {
			$strSqlLimit = ' LIMIT ' . $intLimit;
		}

		$strSql = 'SELECT
						mpg.id,
						mpg.name,
						mr.id military_rank_id
					FROM
						military_pay_grades mpg
						JOIN military_pay_grade_ranks mpr ON ( mpr.military_pay_grade_id = mpg.id )
						JOIN military_ranks mr ON ( mpr.military_rank_id = mr.id )
					WHERE
						mpg.is_published=true
						AND mr.military_component_id = ' . ( int ) $intMilitaryComponentId . '
						AND mpr.military_rank_id = ' . ( int ) $intMilitaryRankId . '
					ORDER BY
						mpg.order_num ' . ( string ) $strSqlLimit;

	return self::fetchMilitaryPayGrades( $strSql, $objDatabase );

	}

	public static function fetchMilitaryPayGradesByPayGradeMacIds( $arrintPayGradeMacIds, $objDatabase ) {

		if( false == valArr( $arrintPayGradeMacIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						military_pay_grades
					WHERE
						pay_grade_mac_id IN ( \'' . implode( "','", $arrintPayGradeMacIds ) . '\' )';

		return parent::fetchMilitaryPayGrades( $strSql, $objDatabase );
	}

	public static function fetchPublishedMilitaryPayGradesIds( $objDatabase ) {

		$strSql = 'SELECT
						pay_grade_mac_id
					FROM
						military_pay_grades
					WHERE
						is_published = true';

		return fetchData( $strSql, $objDatabase );
	}

}
?>