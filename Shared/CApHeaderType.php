<?php

class CApHeaderType extends CBaseApHeaderType {

	const TYPE_DEFAULT				= 1;
// 	const MANAGEMENT_FEE			= 2;
//  const OWNER_DISTRIBUTION		= 3;
// 	const REIMBURSEMENT				= 4;
// 	const JOB_COST					= 5;
// 	const ASSET						= 6;

	const RFP				= 1;
	const JOB				= 2;
	const CONTRACT			= 3;
	const PURCHASE_ORDER	= 4;
	const INVOICE			= 5;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public static function assignSmartyConstants( $objSmarty ) {
		$objSmarty->assign( 'AP_HEADER_TYPE_RFP',				self::RFP );
		$objSmarty->assign( 'AP_HEADER_TYPE_JOB',				self::JOB );
		$objSmarty->assign( 'AP_HEADER_TYPE_CONTRACT',			self::CONTRACT );
		$objSmarty->assign( 'AP_HEADER_TYPE_PURCHASE_ORDER',	self::PURCHASE_ORDER );
		$objSmarty->assign( 'AP_HEADER_TYPE_INVOICE',			self::INVOICE );
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
			default:
				// default case
				break;
		}

		return $boolIsValid;
	}

}
?>