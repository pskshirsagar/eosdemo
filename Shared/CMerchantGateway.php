<?php

class CMerchantGateway extends CBaseMerchantGateway {

    const ECHO_CREDIT_CARD 			= 1;
    const FIRST_REGIONAL_CHECK21	= 2;
    const VITAL 					= 3;
    const ECHO_ACH					= 5;
    const FIRST_REGIONAL_ACH		= 6;
    const FIRST_REGIONAL_WU_EMO		= 7;
    const ZIONS_ACH		       	 	= 8;
    const ZIONS_CHECK21				= 9;
    const EPX_CREDIT_CARD	        = 10;
    const PAY_NEAR_ME	        	= 11;
    const FIFTH_THIRD_ACH		   	= 12;
    const FIFTH_THIRD_CHECK21	    = 13;
	const MONEY_GRAM				= 14;
	const VANTIV_CREDIT_CARD		= 15;
	const LITLE_CREDIT_CARD			= 16;
	const TABAPAY_CREDIT_CARD		= 17;
	const BANK_OF_MONTREAL_ECHECK	= 18;
	const WORLDPAY_DIRECT_DEBIT		= 19;
	const WORLDPAY_INTL_CREDIT_CARD	= 20;
	const SEPA_DIRECT_DEBIT			= 21;

	static $c_arrintRequiresGatewayCredentials = [ self::VANTIV_CREDIT_CARD, self::LITLE_CREDIT_CARD, self::WORLDPAY_DIRECT_DEBIT ];
	static $c_arrintRequiresLoginCredentials = [];

	static $c_arrintSkipControlledDistributionCheck = [ self::WORLDPAY_INTL_CREDIT_CARD, self::SEPA_DIRECT_DEBIT ];

	static $c_arrintAsyncGateways = [ \CMerchantGateway::WORLDPAY_INTL_CREDIT_CARD, \CMerchantGateway::SEPA_DIRECT_DEBIT ];

	/**
	 * Other Functions
	 *
	 */

    public static function merchantGatewayIdToShortStr( $intMerchantGatewayId ) {
        switch( $intMerchantGatewayId ) {
            case self::ECHO_CREDIT_CARD:
        		return 'ECC';
            case self::FIRST_REGIONAL_CHECK21:
        		return 'FRCH21';
            case self::VITAL:
        		return 'VCC';
            case self::ECHO_ACH:
        		return 'EACH';
            case self::FIRST_REGIONAL_ACH:
        		return 'FRACH';
            case self::FIRST_REGIONAL_WU_EMO:
        		return 'WU';
            case self::MONEY_GRAM:
        		return 'MG';
            case self::ZIONS_ACH:
        		return 'ZACH';
            case self::ZIONS_CHECK21:
        		return 'ZCH21';
            case self::EPX_CREDIT_CARD:
        		return 'EPXCC';
            case self::PAY_NEAR_ME:
        		return 'PNM';
            case self::FIFTH_THIRD_ACH:
        		return 'FTACH';
            case self::FIFTH_THIRD_CHECK21:
        		return 'FTCH21';
            case self::VANTIV_CREDIT_CARD:
        		return 'VANTIV';
			case self::LITLE_CREDIT_CARD:
				return 'LITLE';
			case self::TABAPAY_CREDIT_CARD:
				return 'TABAPAY';
			case self::BANK_OF_MONTREAL_ECHECK:
				return 'BOMECHECK';
			case self::WORLDPAY_DIRECT_DEBIT:
				return 'LTILEDD';
			case self::WORLDPAY_INTL_CREDIT_CARD:
				return 'WINTLCC';
			case self::SEPA_DIRECT_DEBIT:
				return 'SEPA';
            default:
        	    trigger_error( 'Invalid merchant geteway id.', E_USER_WARNING );
                break;
        }
    }

}

?>