<?php

class CDistributionStatusType extends CBaseDistributionStatusType {

	const PENDING 			= 1;
	const DEBITING 			= 2;
	const DEBITED 			= 3;
	const DEBIT_FAILED 		= 4;
	const DEBIT_RETURN		= 5;
	const CREDITING 		= 6;
	const CREDITED 			= 7;
	const CREDIT_FAILED 	= 8;
	const CREDIT_RETURN		= 9;

	public static function distributionStatusTypeIdToStr( $intDistributionStatusTypeId ) {
		switch( $intDistributionStatusTypeId ) {
		    case self::PENDING:
				return 'Pending';
		    case self::DEBITING:
				return 'Debiting';
		    case self::DEBITED:
				return 'Debited';
		    case self::DEBIT_FAILED:
				return 'Debit Failed';
		    case self::DEBIT_RETURN:
				return 'Debit Return';
		    case self::CREDITING:
				return 'Crediting';
		    case self::CREDITED:
				return 'Credited';
		    case self::CREDIT_FAILED:
				return 'Credit Failed';
		    case self::CREDIT_RETURN:
				return 'Credit Return';
		    default:
				trigger_error( 'Invalid distribution status type ID.', E_USER_WARNING );
		        break;
		}
	}
}
?>