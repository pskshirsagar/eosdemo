<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPhoneNumberCountryCodes
 * Do not add any new functions to this class.
 */

class CPhoneNumberCountryCodes extends CBasePhoneNumberCountryCodes {

	public static function fetchPhoneNumberCountryCodes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CPhoneNumberCountryCode::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPhoneNumberCountryCode( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CPhoneNumberCountryCode::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public function fetchDialCodeByIsoAlpha2Code( $strIsoAlpha2Code, $objDatabase ) {
		if( false == $strIsoAlpha2Code ) {
			return NULL;
		}

		$strSql = 'SELECT 
						dial_code
					FROM 
						phone_number_country_codes
					WHERE
						iso_alpha2_code = \'' . $strIsoAlpha2Code . '\'
					LIMIT 1';

		return parent::fetchColumn( $strSql, 'dial_code', $objDatabase );
	}

	public static function fetchPublishedCountryCodes( $objDatabase, bool $boolLoadOnlySupportedCountryCodes = true ) {
		$strSql = 'SELECT * FROM phone_number_country_codes WHERE is_published = TRUE';

		if( true == $boolLoadOnlySupportedCountryCodes ) {
			$strSql .= ' AND has_i18n_support = true';
		}

		$strSql .= '
			ORDER BY
				order_num;';

		return self::fetchPhoneNumberCountryCodes( $strSql, $objDatabase );
	}

}

?>