<?php

class CWebsiteTemplateSlot extends CBaseWebsiteTemplateSlot {

	const STATUS_EMPTY   = 0;
	const STATUS_CUSTOM  = 1;
	const STATUS_DEFAULT = 2;

	protected $m_arrobjTemplateSlotImages;
	protected $m_intWebsiteTemplateSlotStatusId;
	protected $m_strOpenLink;
	protected $m_strCloseLink;
	protected $m_strLink;
	protected $m_strTarget;
	protected $m_strLinkText;
	protected $m_strAlt;
	protected $m_strCaption;

	/**
	 * Set Functions
	 */
	public function __construct() {
		parent::__construct();
		$this->setAllowDifferentialUpdate( true );
	}

	public function setValues( $arrintValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrintValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrintValues['website_template_slot_status_id'] ) ) {
			$this->setWebsiteTemplateSlotStatusId( $arrintValues['website_template_slot_status_id'] );
		}
		if( true == isset( $arrintValues['open_link'] ) ) {
			$this->setOpenLink( $arrintValues['open_link'] );
		}
		if( true == isset( $arrintValues['close_link'] ) ) {
			$this->setCloseLink( $arrintValues['close_link'] );
		}
		if( true == isset( $arrintValues['link'] ) ) {
			$this->setLink( $arrintValues['link'] );
		}
		if( true == isset( $arrintValues['target'] ) ) {
			$this->setTarget( $arrintValues['target'] );
		}
		if( true == isset( $arrintValues['link_text'] ) ) {
			$this->setLinkText( $arrintValues['link_text'] );
		}

		return;
	}

	/**
	 * Create Functions
	 */

	public function createTemplateSlotImage( $objWebsite = NULL ) {
		$objTemplateSlotImage = new CTemplateSlotImage();

		if( true == valObj( $objWebsite, 'CWebsite' ) ) {
			$objTemplateSlotImage->setCid( $objWebsite->getCid() );
			$objTemplateSlotImage->setWebsiteId( $objWebsite->getId() );
		}

		$objTemplateSlotImage->setWebsiteTemplateSlotId( $this->m_intId );

		return $objTemplateSlotImage;
	}

	/**
	 * Get Functions
	 */

	public function getTemplateSlotImages() {
		return $this->m_arrobjTemplateSlotImages;
	}

	public function getWebsiteTemplateSlotStatusId() {
		return $this->m_intWebsiteTemplateSlotStatusId;
	}

	public function getOpenLink() {
		return $this->m_strOpenLink;
	}

	public function getCloseLink() {
		return $this->m_strCloseLink;
	}

	public function getLink() {
		return $this->m_strLink;
	}

	public function getTarget() {
		return $this->m_strTarget;
	}

	public function getLinkText() {
		return $this->m_strLinkText;
	}

	public function getAlt() {
		return $this->m_strAlt;
	}

	public function getCaption() {
		return $this->m_strCaption;
	}

	/**
	 * Add Functions
	 */

	public function addTemplateSlotImage( $objTemplateSlotImage ) {
		$this->m_arrobjTemplateSlotImages[] = $objTemplateSlotImage;
	}

	/**
	 * Set Functions
	 */

	public function setWebsiteTemplateSlotStatusId( $intWebsiteTemplateSlotStatusId ) {
		$this->m_intWebsiteTemplateSlotStatusId = $intWebsiteTemplateSlotStatusId;
	}

	public function setOpenLink( $strOpenLink ) {
		$this->m_strOpenLink = $strOpenLink;
	}

	public function setCloseLink( $strCloseLink ) {
		$this->m_strCloseLink = $strCloseLink;
	}

	public function setLink( $strLink ) {
		$this->m_strLink = $strLink;
	}

	public function setTarget( $strTarget ) {
		$this->m_strTarget = $strTarget;
	}

	public function setLinkText( $strLinkText ) {
		$this->m_strLinkText = $strLinkText;
	}

	public function setAlt( $strAlt ) {
		$this->m_strAlt = $strAlt;
	}

	public function setCaption( $strCaption ) {
		$this->m_strCaption = $strCaption;
	}

	/**
	 * Validate Functions
	 */

	public function valKey() {
		$boolIsValid = true;

		if( true == is_null( $this->getKey() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', 'Key is required' ) );
		}

		return $boolIsValid;
	}

	public function valDuplicateKey( $objAdminDatabase ) {

		$boolIsValid = true;

		$intPreexistingWebsiteTemplateSlotCount = \Psi\Eos\Entrata\CWebsiteTemplateSlots::createService()->fetchCompetingWebsiteTemplateSlotCountByKeyByWebsiteTemplateIdById( $this->getKey(), $this->getWebsiteTemplateId(), $this->m_intId, $objAdminDatabase );

		if( 0 < $intPreexistingWebsiteTemplateSlotCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Key is already in use by this template.' ) );
		}

		return $boolIsValid;
	}

	public function valTemplateSlotTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTemplateSlotTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'template_slot_type_id', 'Slot type is required' ) );
		}

		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;

		if( true == is_null( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Title is required' ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDefaultImageName() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valExampleImageName() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valBgcolor() {
		$boolIsValid = true;

		if( true == is_null( $this->getBgcolor() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bgcolor', 'Bgcolor is required' ) );
		}

		return $boolIsValid;
	}

	public function valMediaWidth() {
		$boolIsValid = true;

		if( true == is_null( $this->getMediaWidth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'media_width', 'Media width is required' ) );
		}

		return $boolIsValid;
	}

	public function valMediaHeight() {
		$boolIsValid = true;

		if( true == is_null( $this->getMediaHeight() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'media_height', 'Media height is required' ) );
		}

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTemplateSlotTypeId();
				$boolIsValid &= $this->valKey();
				$boolIsValid &= $this->valDuplicateKey( $objAdminDatabase );
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valBgcolor();
				$boolIsValid &= $this->valMediaWidth();
				$boolIsValid &= $this->valMediaHeight();
				$boolIsValid &= $this->valOrderNum();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function updateSequence( $objClientDatabase ) {
		$intNextSeqId = $this->getId() + 1;
		$arrmixData   = fetchData( 'SELECT setval( \'public.website_template_slots_id_seq\', ' . ( int ) $intNextSeqId . ' , FALSE );', $objClientDatabase );

		if( 0 >= $arrmixData[0]['setval'] ) {
			return false;
		}

		return true;
	}

	public function getInsertSql( $intCurrentUserId ) {
		return $this->insert( $intCurrentUserId, NULL, true );
	}

	public function getUpdateSql( $intCurrentUserId ) {
		return $this->update( $intCurrentUserId, NULL, true );
	}

	public function getDeleteSql( $intCurrentUserId ) {
		return $this->delete( $intCurrentUserId, NULL, true );
	}

}

?>