<?php

class CScreeningRecommendationType extends CBaseScreeningRecommendationType {

    const PASS                    = 1; // Order Completed and was Passed
    const PASSWITHCONDITIONS      = 2; // Order Completed and Passed With Conditions
    const FAIL                    = 3; // Order Completed and Fail
    const PENDING_UNKNOWN         = 4; // Order Inprogress Recommendation Pending
    const ERROR                   = 5; // Order Had an Error
    const CANCELLED               = 6; // Order was Cancelled
    const COMPLETED               = 7; // Order Completed.. used in case of user not having appropriate permission to view the Screening Recommendation
    const PENDING_REVIEW          = 8;
	const RV_SUPPORT_EMAIL_ADDRESS = 'rvsupport@entrata.com';

	public static $c_arrintScreeningRecommendationTypeIds = [ self::PASS, self::PASSWITHCONDITIONS, self::FAIL ];

	public static $c_arrintScreeningCompletedRecommendationTypeIds = [ self::PASS, self::PASSWITHCONDITIONS, self::FAIL ];

    public static function getScreeningRecommendationCssClass( $intScreeningRecommendationTypeId ) {
        $strScreeningRecommendationCssClass = '';

        switch( $intScreeningRecommendationTypeId ) {
            case self::PASS:
                $strScreeningRecommendationCssClass = 'greenlight';
                break;

            case self::PASSWITHCONDITIONS:
                $strScreeningRecommendationCssClass = 'yellowlight';
                break;

            case self::FAIL:
                $strScreeningRecommendationCssClass = 'redlight';
                break;

            case self::PENDING_UNKNOWN:
                $strScreeningRecommendationCssClass = 'alert-in-progress';
                break;

            case self::ERROR:
                $strScreeningRecommendationCssClass = 'alert-red-sm';
                break;

            case self::PENDING_REVIEW:
                $strScreeningRecommendationCssClass = 'alert-in-progress';
                break;

            default:
                // added default case
        }

        return $strScreeningRecommendationCssClass;
    }

    public static function getScreeningRecommendationTypeColor( $intScreeningRecommendationTypeId ) {

    	$strScreeningRecommendationTypeColor = '';

    	switch( $intScreeningRecommendationTypeId ) {
    		case self::PASS:
    			$strScreeningRecommendationTypeColor = 'green';
    			break;

    		case self::PASSWITHCONDITIONS:
    			$strScreeningRecommendationTypeColor = 'yellow';
    			break;

    		case self::FAIL:
    			$strScreeningRecommendationTypeColor = 'red';
    			break;

    		case self::PENDING_UNKNOWN:
    			$strScreeningRecommendationTypeColor = 'blue';
    			break;

    		case self::ERROR:
    			$strScreeningRecommendationTypeColor = 'dark-red';
    			break;

            case self::PENDING_REVIEW:
                $strScreeningRecommendationTypeColor = 'blue';
                break;

    		default:
    			// added default case
    	}

    	return $strScreeningRecommendationTypeColor;
    }

    public static function getScreeningRecommendationTypeText( $intScreeningRecommendationTypeId ) {
    	$strScreeningRecommendationTypeText = '';

    	switch( $intScreeningRecommendationTypeId ) {
    		case self::PASS:
    			$strScreeningRecommendationTypeText = __( 'Pass' );
    			break;

    		case self::PASSWITHCONDITIONS:
    			$strScreeningRecommendationTypeText = __( 'Pass With Conditions' );
    			break;

    		case self::FAIL:
    			$strScreeningRecommendationTypeText = __( 'Fail' );
    			break;

    		case self::PENDING_UNKNOWN:
    			$strScreeningRecommendationTypeText = __( 'In Progress' );
    			break;

    		case self::ERROR:
    			$strScreeningRecommendationTypeText = __( 'Error - Contact Support' );
    			break;

    		case self::CANCELLED:
    			$strScreeningRecommendationTypeText = __( 'Pending' );
    			break;

    		case self::COMPLETED:
    			$strScreeningRecommendationTypeText = __( 'Completed' );
    			break;

    		default:
    			// added default case
    	}

    	return $strScreeningRecommendationTypeText;
    }

	public static function getErrorScreeningRecommendationText() {
    	$strScreeningRecommendationTypeText = __( 'See Error Below: Correct and Resubmit' );

		return $strScreeningRecommendationTypeText;
	}

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>
