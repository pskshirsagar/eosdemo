<?php

class CPaymentBlacklistEntry extends CBasePaymentBlacklistEntry {

	const PAYMENT_BLACKLIST_TYPE_BANK_ACCOUNT = 'BankAccount';
	const PAYMENT_BLACKLIST_TYPE_BANK_ACCOUNT_ROUTING_NUMBER = 'BankAccountRoutingNumber';

	const DELIMITER = '||';

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPaymentBlacklistTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSystemEmailId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLookupStringHashed() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLookupStringEncrypted() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLookupStringCorrectedEncrypted() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBlacklistReason() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
			default:
				// Nothing to do
				break;
        }

        return $boolIsValid;
    }

    public static function hashLookupString( $mixedLookupString ) {
    	if( false == \valArr( $mixedLookupString ) ) {
    		$mixedLookupString = [ $mixedLookupString ];
    	}

    	$strLookupString = implode( self::DELIMITER, $mixedLookupString );

    	$strHashedInfo = \Psi\Libraries\Cryptography\CCrypto::createService()->blindIndex( $strLookupString, CONFIG_SODIUM_KEY_BLIND_INDEX );

    	return $strHashedInfo;
    }

    public static function encryptLookupString( $mixedLookupString ) {
    	if( false == \valArr( $mixedLookupString ) && false == \valStr( $mixedLookupString ) ) {
    		return NULL;
    	}

    	if( false == \valArr( $mixedLookupString ) ) {
    		$mixedLookupString = [ $mixedLookupString ];
    	}

    	$strLookupString = implode( self::DELIMITER, $mixedLookupString );

    	$strEncryptedInfo = \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strLookupString, CONFIG_SODIUM_KEY_PAYMENT_BLACKLIST_LOOKUP );

    	return $strEncryptedInfo;
    }

    public function getLookupStringDecrypted() {
	    if( false == \valStr( $this->getLookupStringEncrypted() ) ) {
		    return NULL;
	    }
	    $strDecrypted = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getLookupStringEncrypted(), CONFIG_SODIUM_KEY_PAYMENT_BLACKLIST_LOOKUP, [ 'legacy_secret_key' => CONFIG_KEY_PAYMENT_BLACKLIST_LOOKUP ] );

    	$arrDecrypted = explode( self::DELIMITER, $strDecrypted );

    	if( \Psi\Libraries\UtilFunctions\count( $arrDecrypted ) == 1 ) {
    		return $arrDecrypted[0];
    	}

    	return $arrDecrypted;
    }

    public function getLookupStringCorrectedDecrypted() {
	    if( false == \valStr( $this->getLookupStringCorrectedEncrypted() ) ) {
	    	return NULL;
	    }
	    $strDecrypted = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getLookupStringCorrectedEncrypted(), CONFIG_SODIUM_KEY_PAYMENT_BLACKLIST_LOOKUP, [ 'legacy_secret_key' => CONFIG_KEY_PAYMENT_BLACKLIST_LOOKUP ] );

    	return $strDecrypted;
    }

    public static function factory( $arrLookupString, $strCorrectedString, $strBlacklistReason, $intSystemEmailId, $intPaymentBlacklistEntryTypeId ) {

    	$strHashedLookupString = self::hashLookupString( $arrLookupString );
    	$strEncryptedLookupString = self::encryptLookupString( $arrLookupString );
    	$strEncryptedCorrectedString = self::encryptLookupString( $strCorrectedString );

    	$objBlacklistEntry = new CPaymentBlacklistEntry();

    	$objBlacklistEntry->setBlacklistReason( $strBlacklistReason );
    	$objBlacklistEntry->setLookupStringEncrypted( $strEncryptedLookupString );
    	$objBlacklistEntry->setLookupStringHashed( $strHashedLookupString );
    	$objBlacklistEntry->setLookupStringCorrectedEncrypted( $strEncryptedCorrectedString );
		$objBlacklistEntry->setSystemEmailId( $intSystemEmailId );
    	$objBlacklistEntry->setPaymentBlacklistTypeId( $intPaymentBlacklistEntryTypeId );

    	return $objBlacklistEntry;
    }

   	public function checkBlacklistedBankAccount( $objClientDatabase, $strRoutingNumber, $strCheckAccountNumber = NULL, $intCheckAccountTypeId = NULL, $strNameOnAccount = NULL ) {
		$boolIsBlacklisted = false;

		if( is_null( $objClientDatabase ) ) {
			return NULL;
		}

		if( empty( $strRoutingNumber ) ) {
		    return NULL;
		}

		$arrLookupStrings = [ self::hashLookupString( [ $strRoutingNumber ] ) ];
		$arrPaymentTypes = [ CPaymentBlacklistType::ROUTING_NUMBER ];

		if( false == is_null( $strCheckAccountNumber ) ) {
		    array_push( $arrLookupStrings, self::hashLookupString( [ $strRoutingNumber, $strCheckAccountNumber ] ) );
		    array_push( $arrPaymentTypes, CPaymentBlacklistType::BANK_ACCOUNT_ROUTING_NUMBER );

		    if( false == is_null( $intCheckAccountTypeId ) ) {
		        array_push( $arrLookupStrings, self::hashLookupString( [ $strRoutingNumber, $strCheckAccountNumber, $intCheckAccountTypeId ] ) );
		        array_push( $arrPaymentTypes, CPaymentBlacklistType::BANK_ACCOUNT_ROUTING_NUMBER_TYPE );
		    }

		    if( false == is_null( $strNameOnAccount ) ) {
		        array_push( $arrLookupStrings, self::hashLookupString( [ $strRoutingNumber, $strCheckAccountNumber, $strNameOnAccount ] ) );
		        array_push( $arrPaymentTypes, CPaymentBlacklistType::BANK_ACCOUNT_ROUTING_NUMBER_NAME );
		    }
		}

		$arrPaymentBlacklistEntries = CPaymentBlacklistEntries::fetchPaymentBlacklistEntriesByPaymentBlacklistTypesAndByLookupStringHashed( $arrPaymentTypes, $arrLookupStrings, $objClientDatabase );

		if( \Psi\Libraries\UtilFunctions\count( $arrPaymentBlacklistEntries ) == 0 ) {
			return NULL;
		}

		$objPaymentBlacklistEntryRoutingNumber = NULL;

		// if the hash matches, unencrypt and check to ensure a match
		foreach( $arrPaymentBlacklistEntries as $objPaymentBlacklistEntry ) {
			$arrLookupString = $objPaymentBlacklistEntry->getLookupStringDecrypted();

			switch( $objPaymentBlacklistEntry->getPaymentBlacklistTypeId() ) {
				case CPaymentBlacklistType::ROUTING_NUMBER:
					$strBlacklistedRoutingNumber = true == valArr( $arrLookupString ) ? $arrLookupString[0] : $arrLookupString;

					if( $strBlacklistedRoutingNumber == $strRoutingNumber ) {
				        // we only want to return corrected routing number if there is no other blacklist entry for the given
				        // account information, so continue checking.
				   		$objPaymentBlacklistEntryRoutingNumber = $objPaymentBlacklistEntry;
					}
					break;

				case CPaymentBlacklistType::BANK_ACCOUNT_ROUTING_NUMBER:
					// Verify that routing and account number matches
					if( false == valArr( $arrLookupString ) || \Psi\Libraries\UtilFunctions\count( $arrLookupString ) < 2 ) {
						// this isn't an account number, since account number will always be at least 2 parts: routing number & account number
						continue 2;
					}

					if( $strRoutingNumber == $arrLookupString[0] && $strCheckAccountNumber == $arrLookupString[1] ) {
					    return $objPaymentBlacklistEntry;
					}
					break;

				case CPaymentBlacklistType::BANK_ACCOUNT_ROUTING_NUMBER_NAME:
					// Verify that routing and account number matches
					if( false == valArr( $arrLookupString ) || \Psi\Libraries\UtilFunctions\count( $arrLookupString ) < 3 ) {
						continue 2;
					}

					if( $strRoutingNumber == $arrLookupString[0] &&
						$strCheckAccountNumber == $arrLookupString[1] &&
						$strNameOnAccount == $arrLookupString[2] ) {
					    return $objPaymentBlacklistEntry;
					}
					break;

				case CPaymentBlacklistType::BANK_ACCOUNT_ROUTING_NUMBER_TYPE:
					if( false == valArr( $arrLookupString ) || \Psi\Libraries\UtilFunctions\count( $arrLookupString ) < 3 ) {
						continue 2;
					}

					if( $strRoutingNumber == $arrLookupString[0] &&
						$strCheckAccountNumber == $arrLookupString[1] &&
						$intCheckAccountTypeId == ( int ) $arrLookupString[2] ) {
						return $objPaymentBlacklistEntry;
					}
					break;

				default:
					// Continue
					break;
			}
		}

		return $objPaymentBlacklistEntryRoutingNumber;
   	}

}
?>