<?php

class CTemplateLoadFunction extends CBaseTemplateLoadFunction {

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valWebsiteTemplateId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valFunctionKey() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valArguments() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getInsertSql( $intCurrentUserId ) {
		return $this->insert( $intCurrentUserId, NULL, true );
	}

	public function getDeleteSql( $intCurrentUserId ) {
		return $this->delete( $intCurrentUserId, NULL, true );
	}

}

?>