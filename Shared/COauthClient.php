<?php

class COauthClient extends CBaseOauthClient {

	const TYPE_APPS		= 'APPS';
	const TYPE_ILS		= 'ILS';

	private $m_boolIsAuthEmailRequired;
	private $m_strOauthClientType;

	public function getIsAuthEmailRequired() {
		return $this->m_boolIsAuthEmailRequired;
	}

	public function setIsAuthEmailRequired( $boolIsAuthEmailRequired ) {
		$this->m_boolIsAuthEmailRequired = CStrings::strToBool( $boolIsAuthEmailRequired );
	}

	public function getOauthClientType() {
		return $this->m_strOauthClientType;
	}

	public function setOauthClientType( $strOauthClientType ) {
		$this->m_strOauthClientType = $strOauthClientType;
	}

	public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valClientId() {
        $boolIsValid = true;

        if( false == isset( $this->m_strClientId ) || false == valStr( $this->m_strClientId ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'client_id', 'Client Id is required.' ) );
        }

        return $boolIsValid;
    }

    public function valClientSecret() {
        $boolIsValid = true;

        if( false == isset( $this->m_strClientSecret ) || false == valStr( $this->m_strClientSecret ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'client_secret', 'Client Secret is required.' ) );
        }

        return $boolIsValid;
    }

    public function valAuthUrl() {
        $boolIsValid = true;

		if( false == $this->getIsAuthEmailRequired() && false == valStr( $this->getAuthUrl() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'auth_url', 'Auth url is required.' ) );
		} elseif( true == valStr( $this->getAuthUrl() ) && false == CValidation::checkUrl( $this->getAuthUrl() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'auth_url', 'Auth Url is not in correct format.' ) );
		}

		return $boolIsValid;
    }

    public function valOauthClientTypeId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intOauthClientTypeId ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'oauth_client_type_id', 'Oauth client type is not set.' ) );
        } elseif( false == in_array( $this->m_intOauthClientTypeId,  COauthClientType::$c_arrintAllOauthClientTypeIds ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'client_type', 'Oauth client type is not valid.' ) );
        }

        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAuthEmail() {
	    $boolIsValid = true;
	    if( true == $this->getIsAuthEmailRequired() && false == valStr( $this->getAuthEmail() ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'auth_email', 'Auth Email is required.' ) );
	    } elseif( true == valStr( $this->getAuthEmail() ) && false == CValidation::validateEmailAddress( $this->getAuthEmail() ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'auth_email', 'Auth Email is not in correct format.' ) );
	    }
	    return $boolIsValid;
    }

    public function valName( $objDatabase ) {
	    $boolIsValid = true;
	    if( false != valStr( $this->getName() ) ) {
		    $strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
		    $intCount = \Psi\Eos\Admin\COauthClients::createService()->fetchOauthClientCount( ' WHERE lower( name )= \'' . pg_escape_string( \Psi\CStringService::singleton()->strtolower( $this->getName() ) ) . '\'' . $strSqlCondition, $objDatabase );
		    if( 0 < $intCount ) {
			    $boolIsValid = false;
			    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'auth_email', 'Name already exists. Duplicate names are not allowed.' ) );
		    }
	    }
	    return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
	            $boolIsValid &= $this->valName( $objDatabase );
            	$boolIsValid &= $this->valAuthUrl();
            	$boolIsValid &= $this->valOauthClientTypeId();
            	$boolIsValid &= $this->valAuthEmail();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function generateClientId( $strAppName ) {
    	if( false == valStr( $strAppName ) ) {
    		throw new Exception( 'Can\'t generate a client id when app name is not set.' );
    	}

    	$strHash = md5( $strAppName );

    	return \Psi\CStringService::singleton()->substr( $strHash, 0, 20 ) . '.' . \Psi\CStringService::singleton()->substr( \Psi\CStringService::singleton()->strtolower( str_replace( ' ', '_', $strAppName ) ), 0, 8 );
    }

    public function generateClientSecret() {
    	$intSecretLength = 32;

    	if( false != file_exists( '/dev/urandom' ) ) {
    		// Get 100 bytes of random data
    		$strRandomData = file_get_contents( '/dev/urandom', false, NULL, 0, 100 ) . uniqid( mt_rand(), true );
    	} else {
    		$strRandomData = mt_rand() . mt_rand() . mt_rand() . mt_rand() . microtime( true ) . uniqid( mt_rand(), true );
    	}

    	return \Psi\CStringService::singleton()->substr( hash( 'sha512', $strRandomData ), 0, $intSecretLength );
    }

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['is_auth_email_required'] ) ) $this->setIsAuthEmailRequired( $arrmixValues['is_auth_email_required'] );
		if( true == isset( $arrmixValues['oauth_client_type'] ) ) $this->setOauthClientType( $arrmixValues['oauth_client_type'] );
		return;
	}

}
?>