<?php

class CAddressType extends CBaseAddressType {

    const PRIMARY 		= 1;
    const PHYSICAL 		= 2;
    const PO_BOX 		= 3;
	const MAILING 		= 4;
    const SHIPPING 		= 5;
    const BILLING 		= 6;
    const PAYMENT 		= 7;
    const PERMANENT 	= 8;
    const PREVIOUS 		= 9;
    const OTHER 		= 10;
    const MARKETING 	= 11;
    const CURRENT 		= 12;
    const FORWARDING 	= 13;
	const RETURN 		    = 14;
	const POINT_OF_INTEREST = 15;

	public static $c_arrintActiveLeaseAdresssTypesPriority = [
		self::PRIMARY,
		self::PHYSICAL,
		self::BILLING,
		self::PERMANENT,
		self::MAILING,
		self::PO_BOX,
		self::CURRENT,
		self::FORWARDING,
		self::MARKETING,
		self::OTHER,
		self::PAYMENT,
		self::PREVIOUS,
		self::RETURN,
		self::SHIPPING
	];

	public static $c_arrintGroupAddressTypes = [
		self::BILLING,
		self::PRIMARY,
		self::CURRENT,
		self::MAILING,
		self::PERMANENT
	];

	public static $c_arrintPastLeaseAdresssTypesPriority = [
		self::FORWARDING,
		self::PRIMARY,
		self::PHYSICAL,
		self::BILLING,
		self::PERMANENT,
		self::MAILING,
		self::PO_BOX,
		self::CURRENT,
		self::MARKETING,
		self::OTHER,
		self::PAYMENT,
		self::PREVIOUS,
		self::RETURN,
		self::SHIPPING
	];

	public static $c_arrintAllAddressTypes = [
		self::PRIMARY,
		self::PHYSICAL,
		self::PO_BOX,
		self::MAILING,
		self::SHIPPING,
		self::BILLING,
		self::PAYMENT,
		self::PERMANENT,
		self::PREVIOUS,
		self::OTHER,
		self::MARKETING,
		self::CURRENT,
		self::FORWARDING,
		self::RETURN,
		self::POINT_OF_INTEREST
	];

	public static $c_arrintAddressTypesPreferencePriority = [
		self::CURRENT => 'Current',
		self::PREVIOUS => 'Previous',
		self::BILLING => 'Billing',
		self::FORWARDING => 'Forwarding',
		self::MAILING => 'Mailing',
		self::MARKETING => 'Marketing',
		self::OTHER =>'Other',
		self::PAYMENT => 'Payment',
		self::PERMANENT => 'Permanent',
		self::PHYSICAL => 'Physical',
		self::PO_BOX => 'PO Box',
		self::PRIMARY => 'Primary',
		self::SHIPPING => 'Shipping',
		self::RETURN => 'Return',
		self::POINT_OF_INTEREST => 'Point of Interest'
	];

    public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

    public function getAddressTypeNameByAddressTypeId( $intAddressTypeId ) {
    	$strAddressTypeName = NULL;

    	switch( $intAddressTypeId ) {

    		case self::PRIMARY:
    			$strAddressTypeName = __( 'Primary' );
    			break;

    		case self::PERMANENT:
    			$strAddressTypeName = __( 'Permanent' );
    			break;

    		case self::PREVIOUS:
    			$strAddressTypeName = __( 'Previous' );
    			break;

    		case self::FORWARDING:
    			$strAddressTypeName = __( 'Forwarding' );
    			break;

    		case self::BILLING:
    			$strAddressTypeName = __( 'Billing' );
    			break;

    		case self::SHIPPING:
    			$strAddressTypeName = __( 'Shipping' );
    			break;

    		case self::PHYSICAL:
    			$strAddressTypeName = __( 'Physical' );
    			break;

    		case self::PO_BOX:
    			$strAddressTypeName = __( 'PO_BOX' );
    			break;

    		case self::MAILING:
    			$strAddressTypeName = __( 'Mailing' );
    			break;

    		case self::PAYMENT:
    			$strAddressTypeName = __( 'Payment' );
    			break;

    		case self::OTHER:
    			$strAddressTypeName = __( 'Other' );
    			break;

    		case self::MARKETING:
    			$strAddressTypeName = __( 'Marketing' );
    			break;

    		case self::CURRENT:
    			$strAddressTypeName = __( 'Current' );
    			break;

    		case self::RETURN:
    			$strAddressTypeName = __( 'Return' );
    			break;

		    case self::POINT_OF_INTEREST:
			    $strAddressTypeName = __( 'Point of Interest' );
			    break;

    		default:
    			// default case
    			break;
    	}
    	return $strAddressTypeName;
    }

}
?>