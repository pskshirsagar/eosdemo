<?php

class CBasePhoneNumberCountryCode extends CEosSingularBase {

	const TABLE_NAME = 'public.phone_number_country_codes';

	protected $m_intId;
	protected $m_strIsoAlpha2Code;
	protected $m_strCountry;
	protected $m_strDialCode;
	protected $m_boolIsPublished;
	protected $m_boolHasI18nSupport;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = false;
		$this->m_boolHasI18nSupport = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['iso_alpha2_code'] ) && $boolDirectSet ) $this->set( 'm_strIsoAlpha2Code', trim( stripcslashes( $arrValues['iso_alpha2_code'] ) ) ); elseif( isset( $arrValues['iso_alpha2_code'] ) ) $this->setIsoAlpha2Code( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['iso_alpha2_code'] ) : $arrValues['iso_alpha2_code'] );
		if( isset( $arrValues['country'] ) && $boolDirectSet ) $this->set( 'm_strCountry', trim( stripcslashes( $arrValues['country'] ) ) ); elseif( isset( $arrValues['country'] ) ) $this->setCountry( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country'] ) : $arrValues['country'] );
		if( isset( $arrValues['dial_code'] ) && $boolDirectSet ) $this->set( 'm_strDialCode', trim( stripcslashes( $arrValues['dial_code'] ) ) ); elseif( isset( $arrValues['dial_code'] ) ) $this->setDialCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dial_code'] ) : $arrValues['dial_code'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['has_i18n_support'] ) && $boolDirectSet ) $this->set( 'm_boolHasI18nSupport', trim( stripcslashes( $arrValues['has_i18n_support'] ) ) ); elseif( isset( $arrValues['has_i18n_support'] ) ) $this->setHasI18nSupport( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_i18n_support'] ) : $arrValues['has_i18n_support'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setIsoAlpha2Code( $strIsoAlpha2Code ) {
		$this->set( 'm_strIsoAlpha2Code', CStrings::strTrimDef( $strIsoAlpha2Code, 2, NULL, true ) );
	}

	public function getIsoAlpha2Code() {
		return $this->m_strIsoAlpha2Code;
	}

	public function sqlIsoAlpha2Code() {
		return ( true == isset( $this->m_strIsoAlpha2Code ) ) ? '\'' . addslashes( $this->m_strIsoAlpha2Code ) . '\'' : 'NULL';
	}

	public function setCountry( $strCountry ) {
		$this->set( 'm_strCountry', CStrings::strTrimDef( $strCountry, 50, NULL, true ) );
	}

	public function getCountry() {
		return $this->m_strCountry;
	}

	public function sqlCountry() {
		return ( true == isset( $this->m_strCountry ) ) ? '\'' . addslashes( $this->m_strCountry ) . '\'' : 'NULL';
	}

	public function setDialCode( $strDialCode ) {
		$this->set( 'm_strDialCode', CStrings::strTrimDef( $strDialCode, 8, NULL, true ) );
	}

	public function getDialCode() {
		return $this->m_strDialCode;
	}

	public function sqlDialCode() {
		return ( true == isset( $this->m_strDialCode ) ) ? '\'' . addslashes( $this->m_strDialCode ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasI18nSupport( $boolHasI18nSupport ) {
		$this->set( 'm_boolHasI18nSupport', CStrings::strToBool( $boolHasI18nSupport ) );
	}

	public function getHasI18nSupport() {
		return $this->m_boolHasI18nSupport;
	}

	public function sqlHasI18nSupport() {
		return ( true == isset( $this->m_boolHasI18nSupport ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasI18nSupport ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'iso_alpha2_code' => $this->getIsoAlpha2Code(),
			'country' => $this->getCountry(),
			'dial_code' => $this->getDialCode(),
			'is_published' => $this->getIsPublished(),
			'has_i18n_support' => $this->getHasI18nSupport(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>