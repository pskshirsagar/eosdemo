<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CPropertyScreeningConfigurations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyScreeningConfigurations extends CEosPluralBase {

	/**
	 * @return CPropertyScreeningConfiguration[]
	 */
	public static function fetchPropertyScreeningConfigurations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyScreeningConfiguration', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyScreeningConfiguration
	 */
	public static function fetchPropertyScreeningConfiguration( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyScreeningConfiguration', $objDatabase );
	}

	public static function fetchPropertyScreeningConfigurationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_screening_configurations', $objDatabase );
	}

	public static function fetchPropertyScreeningConfigurationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyScreeningConfiguration( sprintf( 'SELECT * FROM property_screening_configurations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyScreeningConfigurationsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyScreeningConfigurations( sprintf( 'SELECT * FROM property_screening_configurations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyScreeningConfigurationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyScreeningConfigurations( sprintf( 'SELECT * FROM property_screening_configurations WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyScreeningConfigurationsByScreeningConfigurationIdByCid( $intScreeningConfigurationId, $intCid, $objDatabase ) {
		return self::fetchPropertyScreeningConfigurations( sprintf( 'SELECT * FROM property_screening_configurations WHERE screening_configuration_id = %d AND cid = %d', ( int ) $intScreeningConfigurationId, ( int ) $intCid ), $objDatabase );
	}

}
?>