<?php

class CBasePaymentBlacklistEntry extends CEosSingularBase {

	const TABLE_NAME = 'public.payment_blacklist_entries';

	protected $m_intId;
	protected $m_intPaymentBlacklistTypeId;
	protected $m_intSystemEmailId;
	protected $m_strLookupStringHashed;
	protected $m_strLookupStringEncrypted;
	protected $m_strLookupStringCorrectedEncrypted;
	protected $m_strBlacklistReason;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['payment_blacklist_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentBlacklistTypeId', trim( $arrValues['payment_blacklist_type_id'] ) ); elseif( isset( $arrValues['payment_blacklist_type_id'] ) ) $this->setPaymentBlacklistTypeId( $arrValues['payment_blacklist_type_id'] );
		if( isset( $arrValues['system_email_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemEmailId', trim( $arrValues['system_email_id'] ) ); elseif( isset( $arrValues['system_email_id'] ) ) $this->setSystemEmailId( $arrValues['system_email_id'] );
		if( isset( $arrValues['lookup_string_hashed'] ) && $boolDirectSet ) $this->set( 'm_strLookupStringHashed', trim( stripcslashes( $arrValues['lookup_string_hashed'] ) ) ); elseif( isset( $arrValues['lookup_string_hashed'] ) ) $this->setLookupStringHashed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['lookup_string_hashed'] ) : $arrValues['lookup_string_hashed'] );
		if( isset( $arrValues['lookup_string_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strLookupStringEncrypted', trim( stripcslashes( $arrValues['lookup_string_encrypted'] ) ) ); elseif( isset( $arrValues['lookup_string_encrypted'] ) ) $this->setLookupStringEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['lookup_string_encrypted'] ) : $arrValues['lookup_string_encrypted'] );
		if( isset( $arrValues['lookup_string_corrected_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strLookupStringCorrectedEncrypted', trim( stripcslashes( $arrValues['lookup_string_corrected_encrypted'] ) ) ); elseif( isset( $arrValues['lookup_string_corrected_encrypted'] ) ) $this->setLookupStringCorrectedEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['lookup_string_corrected_encrypted'] ) : $arrValues['lookup_string_corrected_encrypted'] );
		if( isset( $arrValues['blacklist_reason'] ) && $boolDirectSet ) $this->set( 'm_strBlacklistReason', trim( stripcslashes( $arrValues['blacklist_reason'] ) ) ); elseif( isset( $arrValues['blacklist_reason'] ) ) $this->setBlacklistReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['blacklist_reason'] ) : $arrValues['blacklist_reason'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPaymentBlacklistTypeId( $intPaymentBlacklistTypeId ) {
		$this->set( 'm_intPaymentBlacklistTypeId', CStrings::strToIntDef( $intPaymentBlacklistTypeId, NULL, false ) );
	}

	public function getPaymentBlacklistTypeId() {
		return $this->m_intPaymentBlacklistTypeId;
	}

	public function sqlPaymentBlacklistTypeId() {
		return ( true == isset( $this->m_intPaymentBlacklistTypeId ) ) ? ( string ) $this->m_intPaymentBlacklistTypeId : 'NULL';
	}

	public function setSystemEmailId( $intSystemEmailId ) {
		$this->set( 'm_intSystemEmailId', CStrings::strToIntDef( $intSystemEmailId, NULL, false ) );
	}

	public function getSystemEmailId() {
		return $this->m_intSystemEmailId;
	}

	public function sqlSystemEmailId() {
		return ( true == isset( $this->m_intSystemEmailId ) ) ? ( string ) $this->m_intSystemEmailId : 'NULL';
	}

	public function setLookupStringHashed( $strLookupStringHashed ) {
		$this->set( 'm_strLookupStringHashed', CStrings::strTrimDef( $strLookupStringHashed, 255, NULL, true ) );
	}

	public function getLookupStringHashed() {
		return $this->m_strLookupStringHashed;
	}

	public function sqlLookupStringHashed() {
		return ( true == isset( $this->m_strLookupStringHashed ) ) ? '\'' . addslashes( $this->m_strLookupStringHashed ) . '\'' : 'NULL';
	}

	public function setLookupStringEncrypted( $strLookupStringEncrypted ) {
		$this->set( 'm_strLookupStringEncrypted', CStrings::strTrimDef( $strLookupStringEncrypted, 255, NULL, true ) );
	}

	public function getLookupStringEncrypted() {
		return $this->m_strLookupStringEncrypted;
	}

	public function sqlLookupStringEncrypted() {
		return ( true == isset( $this->m_strLookupStringEncrypted ) ) ? '\'' . addslashes( $this->m_strLookupStringEncrypted ) . '\'' : 'NULL';
	}

	public function setLookupStringCorrectedEncrypted( $strLookupStringCorrectedEncrypted ) {
		$this->set( 'm_strLookupStringCorrectedEncrypted', CStrings::strTrimDef( $strLookupStringCorrectedEncrypted, 255, NULL, true ) );
	}

	public function getLookupStringCorrectedEncrypted() {
		return $this->m_strLookupStringCorrectedEncrypted;
	}

	public function sqlLookupStringCorrectedEncrypted() {
		return ( true == isset( $this->m_strLookupStringCorrectedEncrypted ) ) ? '\'' . addslashes( $this->m_strLookupStringCorrectedEncrypted ) . '\'' : 'NULL';
	}

	public function setBlacklistReason( $strBlacklistReason ) {
		$this->set( 'm_strBlacklistReason', CStrings::strTrimDef( $strBlacklistReason, -1, NULL, true ) );
	}

	public function getBlacklistReason() {
		return $this->m_strBlacklistReason;
	}

	public function sqlBlacklistReason() {
		return ( true == isset( $this->m_strBlacklistReason ) ) ? '\'' . addslashes( $this->m_strBlacklistReason ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, payment_blacklist_type_id, system_email_id, lookup_string_hashed, lookup_string_encrypted, lookup_string_corrected_encrypted, blacklist_reason, deleted_by, deleted_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPaymentBlacklistTypeId() . ', ' .
 						$this->sqlSystemEmailId() . ', ' .
 						$this->sqlLookupStringHashed() . ', ' .
 						$this->sqlLookupStringEncrypted() . ', ' .
 						$this->sqlLookupStringCorrectedEncrypted() . ', ' .
 						$this->sqlBlacklistReason() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_blacklist_type_id = ' . $this->sqlPaymentBlacklistTypeId() . ','; } elseif( true == array_key_exists( 'PaymentBlacklistTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_blacklist_type_id = ' . $this->sqlPaymentBlacklistTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId() . ','; } elseif( true == array_key_exists( 'SystemEmailId', $this->getChangedColumns() ) ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lookup_string_hashed = ' . $this->sqlLookupStringHashed() . ','; } elseif( true == array_key_exists( 'LookupStringHashed', $this->getChangedColumns() ) ) { $strSql .= ' lookup_string_hashed = ' . $this->sqlLookupStringHashed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lookup_string_encrypted = ' . $this->sqlLookupStringEncrypted() . ','; } elseif( true == array_key_exists( 'LookupStringEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' lookup_string_encrypted = ' . $this->sqlLookupStringEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lookup_string_corrected_encrypted = ' . $this->sqlLookupStringCorrectedEncrypted() . ','; } elseif( true == array_key_exists( 'LookupStringCorrectedEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' lookup_string_corrected_encrypted = ' . $this->sqlLookupStringCorrectedEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' blacklist_reason = ' . $this->sqlBlacklistReason() . ','; } elseif( true == array_key_exists( 'BlacklistReason', $this->getChangedColumns() ) ) { $strSql .= ' blacklist_reason = ' . $this->sqlBlacklistReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'payment_blacklist_type_id' => $this->getPaymentBlacklistTypeId(),
			'system_email_id' => $this->getSystemEmailId(),
			'lookup_string_hashed' => $this->getLookupStringHashed(),
			'lookup_string_encrypted' => $this->getLookupStringEncrypted(),
			'lookup_string_corrected_encrypted' => $this->getLookupStringCorrectedEncrypted(),
			'blacklist_reason' => $this->getBlacklistReason(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>