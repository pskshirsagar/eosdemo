<?php

class CBaseToolTip extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.tool_tips';

	protected $m_intId;
	protected $m_intToolTipTypeId;
	protected $m_arrintHelpResourceIds;
	protected $m_strKey;
	protected $m_strTitle;
	protected $m_strClientDescription;
	protected $m_strClientDescriptionEdited;
	protected $m_strInternalDescription;
	protected $m_intIsPublished;
	protected $m_intIsApproved;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intRequiresSync;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strPathway;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '0';
		$this->m_intIsApproved = '0';
		$this->m_intRequiresSync = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['tool_tip_type_id'] ) && $boolDirectSet ) $this->set( 'm_intToolTipTypeId', trim( $arrValues['tool_tip_type_id'] ) ); elseif( isset( $arrValues['tool_tip_type_id'] ) ) $this->setToolTipTypeId( $arrValues['tool_tip_type_id'] );
		if( isset( $arrValues['help_resource_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintHelpResourceIds', trim( $arrValues['help_resource_ids'] ) ); elseif( isset( $arrValues['help_resource_ids'] ) ) $this->setHelpResourceIds( $arrValues['help_resource_ids'] );
		if( isset( $arrValues['key'] ) && $boolDirectSet ) $this->set( 'm_strKey', trim( stripcslashes( $arrValues['key'] ) ) ); elseif( isset( $arrValues['key'] ) ) $this->setKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['key'] ) : $arrValues['key'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['client_description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strClientDescription', trim( stripcslashes( $arrValues['client_description'] ) ) ); elseif( isset( $arrValues['client_description'] ) ) $this->setClientDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['client_description'] ) : $arrValues['client_description'] );
		if( isset( $arrValues['client_description_edited'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strClientDescriptionEdited', trim( stripcslashes( $arrValues['client_description_edited'] ) ) ); elseif( isset( $arrValues['client_description_edited'] ) ) $this->setClientDescriptionEdited( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['client_description_edited'] ) : $arrValues['client_description_edited'] );
		if( isset( $arrValues['internal_description'] ) && $boolDirectSet ) $this->set( 'm_strInternalDescription', trim( stripcslashes( $arrValues['internal_description'] ) ) ); elseif( isset( $arrValues['internal_description'] ) ) $this->setInternalDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['internal_description'] ) : $arrValues['internal_description'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_approved'] ) && $boolDirectSet ) $this->set( 'm_intIsApproved', trim( $arrValues['is_approved'] ) ); elseif( isset( $arrValues['is_approved'] ) ) $this->setIsApproved( $arrValues['is_approved'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['requires_sync'] ) && $boolDirectSet ) $this->set( 'm_intRequiresSync', trim( $arrValues['requires_sync'] ) ); elseif( isset( $arrValues['requires_sync'] ) ) $this->setRequiresSync( $arrValues['requires_sync'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['pathway'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strPathway', trim( stripcslashes( $arrValues['pathway'] ) ) ); elseif( isset( $arrValues['pathway'] ) ) $this->setPathway( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['pathway'] ) : $arrValues['pathway'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setToolTipTypeId( $intToolTipTypeId ) {
		$this->set( 'm_intToolTipTypeId', CStrings::strToIntDef( $intToolTipTypeId, NULL, false ) );
	}

	public function getToolTipTypeId() {
		return $this->m_intToolTipTypeId;
	}

	public function sqlToolTipTypeId() {
		return ( true == isset( $this->m_intToolTipTypeId ) ) ? ( string ) $this->m_intToolTipTypeId : 'NULL';
	}

	public function setHelpResourceIds( $arrintHelpResourceIds ) {
		$this->set( 'm_arrintHelpResourceIds', CStrings::strToArrIntDef( $arrintHelpResourceIds, NULL ) );
	}

	public function getHelpResourceIds() {
		return $this->m_arrintHelpResourceIds;
	}

	public function sqlHelpResourceIds() {
		return ( true == isset( $this->m_arrintHelpResourceIds ) && true == valArr( $this->m_arrintHelpResourceIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintHelpResourceIds, NULL ) . '\'' : 'NULL';
	}

	public function setKey( $strKey ) {
		$this->set( 'm_strKey', CStrings::strTrimDef( $strKey, 256, NULL, true ) );
	}

	public function getKey() {
		return $this->m_strKey;
	}

	public function sqlKey() {
		return ( true == isset( $this->m_strKey ) ) ? '\'' . addslashes( $this->m_strKey ) . '\'' : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 240, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setClientDescription( $strClientDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strClientDescription', CStrings::strTrimDef( $strClientDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getClientDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strClientDescription', $strLocaleCode );
	}

	public function sqlClientDescription() {
		return ( true == isset( $this->m_strClientDescription ) ) ? '\'' . addslashes( $this->m_strClientDescription ) . '\'' : 'NULL';
	}

	public function setClientDescriptionEdited( $strClientDescriptionEdited, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strClientDescriptionEdited', CStrings::strTrimDef( $strClientDescriptionEdited, -1, NULL, true ), $strLocaleCode );
	}

	public function getClientDescriptionEdited( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strClientDescriptionEdited', $strLocaleCode );
	}

	public function sqlClientDescriptionEdited() {
		return ( true == isset( $this->m_strClientDescriptionEdited ) ) ? '\'' . addslashes( $this->m_strClientDescriptionEdited ) . '\'' : 'NULL';
	}

	public function setInternalDescription( $strInternalDescription ) {
		$this->set( 'm_strInternalDescription', CStrings::strTrimDef( $strInternalDescription, -1, NULL, true ) );
	}

	public function getInternalDescription() {
		return $this->m_strInternalDescription;
	}

	public function sqlInternalDescription() {
		return ( true == isset( $this->m_strInternalDescription ) ) ? '\'' . addslashes( $this->m_strInternalDescription ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '0';
	}

	public function setIsApproved( $intIsApproved ) {
		$this->set( 'm_intIsApproved', CStrings::strToIntDef( $intIsApproved, NULL, false ) );
	}

	public function getIsApproved() {
		return $this->m_intIsApproved;
	}

	public function sqlIsApproved() {
		return ( true == isset( $this->m_intIsApproved ) ) ? ( string ) $this->m_intIsApproved : '0';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setRequiresSync( $intRequiresSync ) {
		$this->set( 'm_intRequiresSync', CStrings::strToIntDef( $intRequiresSync, NULL, false ) );
	}

	public function getRequiresSync() {
		return $this->m_intRequiresSync;
	}

	public function sqlRequiresSync() {
		return ( true == isset( $this->m_intRequiresSync ) ) ? ( string ) $this->m_intRequiresSync : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setPathway( $strPathway, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strPathway', CStrings::strTrimDef( $strPathway, -1, NULL, true ), $strLocaleCode );
	}

	public function getPathway( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strPathway', $strLocaleCode );
	}

	public function sqlPathway() {
		return ( true == isset( $this->m_strPathway ) ) ? '\'' . addslashes( $this->m_strPathway ) . '\'' : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, tool_tip_type_id, help_resource_ids, key, title, client_description, client_description_edited, internal_description, is_published, is_approved, approved_by, approved_on, requires_sync, updated_by, updated_on, created_by, created_on, details, pathway )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlToolTipTypeId() . ', ' .
						$this->sqlHelpResourceIds() . ', ' .
						$this->sqlKey() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlClientDescription() . ', ' .
						$this->sqlClientDescriptionEdited() . ', ' .
						$this->sqlInternalDescription() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsApproved() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						$this->sqlRequiresSync() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlPathway() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tool_tip_type_id = ' . $this->sqlToolTipTypeId(). ',' ; } elseif( true == array_key_exists( 'ToolTipTypeId', $this->getChangedColumns() ) ) { $strSql .= ' tool_tip_type_id = ' . $this->sqlToolTipTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' help_resource_ids = ' . $this->sqlHelpResourceIds(). ',' ; } elseif( true == array_key_exists( 'HelpResourceIds', $this->getChangedColumns() ) ) { $strSql .= ' help_resource_ids = ' . $this->sqlHelpResourceIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' key = ' . $this->sqlKey(). ',' ; } elseif( true == array_key_exists( 'Key', $this->getChangedColumns() ) ) { $strSql .= ' key = ' . $this->sqlKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_description = ' . $this->sqlClientDescription(). ',' ; } elseif( true == array_key_exists( 'ClientDescription', $this->getChangedColumns() ) ) { $strSql .= ' client_description = ' . $this->sqlClientDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_description_edited = ' . $this->sqlClientDescriptionEdited(). ',' ; } elseif( true == array_key_exists( 'ClientDescriptionEdited', $this->getChangedColumns() ) ) { $strSql .= ' client_description_edited = ' . $this->sqlClientDescriptionEdited() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internal_description = ' . $this->sqlInternalDescription(). ',' ; } elseif( true == array_key_exists( 'InternalDescription', $this->getChangedColumns() ) ) { $strSql .= ' internal_description = ' . $this->sqlInternalDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_approved = ' . $this->sqlIsApproved(). ',' ; } elseif( true == array_key_exists( 'IsApproved', $this->getChangedColumns() ) ) { $strSql .= ' is_approved = ' . $this->sqlIsApproved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requires_sync = ' . $this->sqlRequiresSync(). ',' ; } elseif( true == array_key_exists( 'RequiresSync', $this->getChangedColumns() ) ) { $strSql .= ' requires_sync = ' . $this->sqlRequiresSync() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pathway = ' . $this->sqlPathway(). ',' ; } elseif( true == array_key_exists( 'Pathway', $this->getChangedColumns() ) ) { $strSql .= ' pathway = ' . $this->sqlPathway() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'tool_tip_type_id' => $this->getToolTipTypeId(),
			'help_resource_ids' => $this->getHelpResourceIds(),
			'key' => $this->getKey(),
			'title' => $this->getTitle(),
			'client_description' => $this->getClientDescription(),
			'client_description_edited' => $this->getClientDescriptionEdited(),
			'internal_description' => $this->getInternalDescription(),
			'is_published' => $this->getIsPublished(),
			'is_approved' => $this->getIsApproved(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'requires_sync' => $this->getRequiresSync(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'pathway' => $this->getPathway()
		);
	}

}
?>