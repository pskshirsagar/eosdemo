<?php

class CBaseTimeZone extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.time_zones';

	protected $m_intId;
	protected $m_strName;
	protected $m_strTimeZoneName;
	protected $m_strAbbr;
	protected $m_intGmtOffset;
	protected $m_intIsDaylightSavings;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strCloudReferenceId;
	protected $m_strMarketingTimeZoneName;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intIsDaylightSavings = '0';
		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['time_zone_name'] ) && $boolDirectSet ) $this->set( 'm_strTimeZoneName', trim( stripcslashes( $arrValues['time_zone_name'] ) ) ); elseif( isset( $arrValues['time_zone_name'] ) ) $this->setTimeZoneName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['time_zone_name'] ) : $arrValues['time_zone_name'] );
		if( isset( $arrValues['abbr'] ) && $boolDirectSet ) $this->set( 'm_strAbbr', trim( stripcslashes( $arrValues['abbr'] ) ) ); elseif( isset( $arrValues['abbr'] ) ) $this->setAbbr( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['abbr'] ) : $arrValues['abbr'] );
		if( isset( $arrValues['gmt_offset'] ) && $boolDirectSet ) $this->set( 'm_intGmtOffset', trim( $arrValues['gmt_offset'] ) ); elseif( isset( $arrValues['gmt_offset'] ) ) $this->setGmtOffset( $arrValues['gmt_offset'] );
		if( isset( $arrValues['is_daylight_savings'] ) && $boolDirectSet ) $this->set( 'm_intIsDaylightSavings', trim( $arrValues['is_daylight_savings'] ) ); elseif( isset( $arrValues['is_daylight_savings'] ) ) $this->setIsDaylightSavings( $arrValues['is_daylight_savings'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['cloud_reference_id'] ) && $boolDirectSet ) $this->set( 'm_strCloudReferenceId', trim( stripcslashes( $arrValues['cloud_reference_id'] ) ) ); elseif( isset( $arrValues['cloud_reference_id'] ) ) $this->setCloudReferenceId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cloud_reference_id'] ) : $arrValues['cloud_reference_id'] );
		if( isset( $arrValues['marketing_time_zone_name'] ) && $boolDirectSet ) $this->set( 'm_strMarketingTimeZoneName', trim( stripcslashes( $arrValues['marketing_time_zone_name'] ) ) ); elseif( isset( $arrValues['marketing_time_zone_name'] ) ) $this->setMarketingTimeZoneName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['marketing_time_zone_name'] ) : $arrValues['marketing_time_zone_name'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setTimeZoneName( $strTimeZoneName ) {
		$this->set( 'm_strTimeZoneName', CStrings::strTrimDef( $strTimeZoneName, 100, NULL, true ) );
	}

	public function getTimeZoneName() {
		return $this->m_strTimeZoneName;
	}

	public function sqlTimeZoneName() {
		return ( true == isset( $this->m_strTimeZoneName ) ) ? '\'' . addslashes( $this->m_strTimeZoneName ) . '\'' : 'NULL';
	}

	public function setAbbr( $strAbbr ) {
		$this->set( 'm_strAbbr', CStrings::strTrimDef( $strAbbr, 4, NULL, true ) );
	}

	public function getAbbr() {
		return $this->m_strAbbr;
	}

	public function sqlAbbr() {
		return ( true == isset( $this->m_strAbbr ) ) ? '\'' . addslashes( $this->m_strAbbr ) . '\'' : 'NULL';
	}

	public function setGmtOffset( $intGmtOffset ) {
		$this->set( 'm_intGmtOffset', CStrings::strToIntDef( $intGmtOffset, NULL, false ) );
	}

	public function getGmtOffset() {
		return $this->m_intGmtOffset;
	}

	public function sqlGmtOffset() {
		return ( true == isset( $this->m_intGmtOffset ) ) ? ( string ) $this->m_intGmtOffset : 'NULL';
	}

	public function setIsDaylightSavings( $intIsDaylightSavings ) {
		$this->set( 'm_intIsDaylightSavings', CStrings::strToIntDef( $intIsDaylightSavings, NULL, false ) );
	}

	public function getIsDaylightSavings() {
		return $this->m_intIsDaylightSavings;
	}

	public function sqlIsDaylightSavings() {
		return ( true == isset( $this->m_intIsDaylightSavings ) ) ? ( string ) $this->m_intIsDaylightSavings : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setCloudReferenceId( $strCloudReferenceId ) {
		$this->set( 'm_strCloudReferenceId', CStrings::strTrimDef( $strCloudReferenceId, 255, NULL, true ) );
	}

	public function getCloudReferenceId() {
		return $this->m_strCloudReferenceId;
	}

	public function sqlCloudReferenceId() {
		return ( true == isset( $this->m_strCloudReferenceId ) ) ? '\'' . addslashes( $this->m_strCloudReferenceId ) . '\'' : 'NULL';
	}

	public function setMarketingTimeZoneName( $strMarketingTimeZoneName ) {
		$this->set( 'm_strMarketingTimeZoneName', CStrings::strTrimDef( $strMarketingTimeZoneName, 100, NULL, true ) );
	}

	public function getMarketingTimeZoneName() {
		return $this->m_strMarketingTimeZoneName;
	}

	public function sqlMarketingTimeZoneName() {
		return ( true == isset( $this->m_strMarketingTimeZoneName ) ) ? '\'' . addslashes( $this->m_strMarketingTimeZoneName ) . '\'' : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'time_zone_name' => $this->getTimeZoneName(),
			'abbr' => $this->getAbbr(),
			'gmt_offset' => $this->getGmtOffset(),
			'is_daylight_savings' => $this->getIsDaylightSavings(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails(),
			'cloud_reference_id' => $this->getCloudReferenceId(),
			'marketing_time_zone_name' => $this->getMarketingTimeZoneName()
		);
	}

}
?>