<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CProperties
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseProperties extends CEosPluralBase {

	/**
	 * @return CProperty[]
	 */
	public static function fetchProperties( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CProperty::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CProperty
	 */
	public static function fetchProperty( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CProperty::class, $objDatabase );
	}

	public static function fetchPropertyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'properties', $objDatabase );
	}

	public static function fetchPropertyByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchProperty( sprintf( 'SELECT * FROM properties WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchPropertiesByCid( $intCid, $objDatabase ) {
		return self::fetchProperties( sprintf( 'SELECT * FROM properties WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchPropertiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchProperties( sprintf( 'SELECT * FROM properties WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchPropertiesByPropertyTypeIdByCid( $intPropertyTypeId, $intCid, $objDatabase ) {
		return self::fetchProperties( sprintf( 'SELECT * FROM properties WHERE property_type_id = %d AND cid = %d', $intPropertyTypeId, $intCid ), $objDatabase );
	}

	public static function fetchPropertiesByCompanyRegionIdByCid( $intCompanyRegionId, $intCid, $objDatabase ) {
		return self::fetchProperties( sprintf( 'SELECT * FROM properties WHERE company_region_id = %d AND cid = %d', $intCompanyRegionId, $intCid ), $objDatabase );
	}

	public static function fetchPropertiesByTimeZoneIdByCid( $intTimeZoneId, $intCid, $objDatabase ) {
		return self::fetchProperties( sprintf( 'SELECT * FROM properties WHERE time_zone_id = %d AND cid = %d', $intTimeZoneId, $intCid ), $objDatabase );
	}

	public static function fetchPropertiesByOwnerIdByCid( $intOwnerId, $intCid, $objDatabase ) {
		return self::fetchProperties( sprintf( 'SELECT * FROM properties WHERE owner_id = %d AND cid = %d', $intOwnerId, $intCid ), $objDatabase );
	}

	public static function fetchPropertiesByAccountIdByCid( $intAccountId, $intCid, $objDatabase ) {
		return self::fetchProperties( sprintf( 'SELECT * FROM properties WHERE account_id = %d AND cid = %d', $intAccountId, $intCid ), $objDatabase );
	}

	public static function fetchPropertiesByDefaultOccupancyTypeIdByCid( $intDefaultOccupancyTypeId, $intCid, $objDatabase ) {
		return self::fetchProperties( sprintf( 'SELECT * FROM properties WHERE default_occupancy_type_id = %d AND cid = %d', $intDefaultOccupancyTypeId, $intCid ), $objDatabase );
	}

}
?>