<?php

class CBaseInternetListingService extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.internet_listing_services';

	protected $m_intId;
	protected $m_intOauthClientId;
	protected $m_intParentInternetListingServiceId;
	protected $m_strName;
	protected $m_strDirectoryName;
	protected $m_strDescription;
	protected $m_strUrl;
	protected $m_strSignupUrl;
	protected $m_strLogoUri;
	protected $m_strIconUri;
	protected $m_intFtpPortNumber;
	protected $m_strFtpServerAddress;
	protected $m_strFtpUsernameEncrypted;
	protected $m_strFtpPasswordEncrypted;
	protected $m_strCheckAvailabilityPageTemplate;
	protected $m_strNameFirstEncrypted;
	protected $m_strNameLastEncrypted;
	protected $m_strEmailAddressEncrypted;
	protected $m_strInternalNotes;
	protected $m_strIlsSubtitle;
	protected $m_strIlsActivationUrl;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intIsPublished;
	protected $m_intIsGuestCardEnabled;
	protected $m_intIsAllowPhoneExtension;
	protected $m_intIsSupportsFreeBasicListings;
	protected $m_intIsExpress;
	protected $m_intIsNational;
	protected $m_intIsFeedEnabled;
	protected $m_intIsLimited;
	protected $m_boolIsGuestCardApi;
	protected $m_boolIsIlsApi;
	protected $m_boolIsDirectActivation;
	protected $m_boolIsForEntrataUser;
	protected $m_boolIsFeedChangeApprovalNotRequired;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_arrintOccupancyTypeIds;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_strIconUri = NULL;
		$this->m_intIsPublished = '1';
		$this->m_intIsGuestCardEnabled = '0';
		$this->m_intIsAllowPhoneExtension = '1';
		$this->m_intIsSupportsFreeBasicListings = '0';
		$this->m_intIsExpress = '0';
		$this->m_intIsNational = '0';
		$this->m_intIsFeedEnabled = '0';
		$this->m_intIsLimited = '0';
		$this->m_boolIsGuestCardApi = false;
		$this->m_boolIsIlsApi = false;
		$this->m_boolIsDirectActivation = false;
		$this->m_boolIsForEntrataUser = false;
		$this->m_boolIsFeedChangeApprovalNotRequired = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['oauth_client_id'] ) && $boolDirectSet ) $this->set( 'm_intOauthClientId', trim( $arrValues['oauth_client_id'] ) ); elseif( isset( $arrValues['oauth_client_id'] ) ) $this->setOauthClientId( $arrValues['oauth_client_id'] );
		if( isset( $arrValues['parent_internet_listing_service_id'] ) && $boolDirectSet ) $this->set( 'm_intParentInternetListingServiceId', trim( $arrValues['parent_internet_listing_service_id'] ) ); elseif( isset( $arrValues['parent_internet_listing_service_id'] ) ) $this->setParentInternetListingServiceId( $arrValues['parent_internet_listing_service_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['directory_name'] ) && $boolDirectSet ) $this->set( 'm_strDirectoryName', trim( stripcslashes( $arrValues['directory_name'] ) ) ); elseif( isset( $arrValues['directory_name'] ) ) $this->setDirectoryName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['directory_name'] ) : $arrValues['directory_name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['url'] ) && $boolDirectSet ) $this->set( 'm_strUrl', trim( stripcslashes( $arrValues['url'] ) ) ); elseif( isset( $arrValues['url'] ) ) $this->setUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['url'] ) : $arrValues['url'] );
		if( isset( $arrValues['signup_url'] ) && $boolDirectSet ) $this->set( 'm_strSignupUrl', trim( stripcslashes( $arrValues['signup_url'] ) ) ); elseif( isset( $arrValues['signup_url'] ) ) $this->setSignupUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['signup_url'] ) : $arrValues['signup_url'] );
		if( isset( $arrValues['logo_uri'] ) && $boolDirectSet ) $this->set( 'm_strLogoUri', trim( stripcslashes( $arrValues['logo_uri'] ) ) ); elseif( isset( $arrValues['logo_uri'] ) ) $this->setLogoUri( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['logo_uri'] ) : $arrValues['logo_uri'] );
		if( isset( $arrValues['icon_uri'] ) && $boolDirectSet ) $this->set( 'm_strIconUri', trim( stripcslashes( $arrValues['icon_uri'] ) ) ); elseif( isset( $arrValues['icon_uri'] ) ) $this->setIconUri( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['icon_uri'] ) : $arrValues['icon_uri'] );
		if( isset( $arrValues['ftp_port_number'] ) && $boolDirectSet ) $this->set( 'm_intFtpPortNumber', trim( $arrValues['ftp_port_number'] ) ); elseif( isset( $arrValues['ftp_port_number'] ) ) $this->setFtpPortNumber( $arrValues['ftp_port_number'] );
		if( isset( $arrValues['ftp_server_address'] ) && $boolDirectSet ) $this->set( 'm_strFtpServerAddress', trim( stripcslashes( $arrValues['ftp_server_address'] ) ) ); elseif( isset( $arrValues['ftp_server_address'] ) ) $this->setFtpServerAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ftp_server_address'] ) : $arrValues['ftp_server_address'] );
		if( isset( $arrValues['ftp_username_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strFtpUsernameEncrypted', trim( stripcslashes( $arrValues['ftp_username_encrypted'] ) ) ); elseif( isset( $arrValues['ftp_username_encrypted'] ) ) $this->setFtpUsernameEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ftp_username_encrypted'] ) : $arrValues['ftp_username_encrypted'] );
		if( isset( $arrValues['ftp_password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strFtpPasswordEncrypted', trim( stripcslashes( $arrValues['ftp_password_encrypted'] ) ) ); elseif( isset( $arrValues['ftp_password_encrypted'] ) ) $this->setFtpPasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ftp_password_encrypted'] ) : $arrValues['ftp_password_encrypted'] );
		if( isset( $arrValues['check_availability_page_template'] ) && $boolDirectSet ) $this->set( 'm_strCheckAvailabilityPageTemplate', trim( stripcslashes( $arrValues['check_availability_page_template'] ) ) ); elseif( isset( $arrValues['check_availability_page_template'] ) ) $this->setCheckAvailabilityPageTemplate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_availability_page_template'] ) : $arrValues['check_availability_page_template'] );
		if( isset( $arrValues['name_first_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strNameFirstEncrypted', trim( stripcslashes( $arrValues['name_first_encrypted'] ) ) ); elseif( isset( $arrValues['name_first_encrypted'] ) ) $this->setNameFirstEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_first_encrypted'] ) : $arrValues['name_first_encrypted'] );
		if( isset( $arrValues['name_last_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strNameLastEncrypted', trim( stripcslashes( $arrValues['name_last_encrypted'] ) ) ); elseif( isset( $arrValues['name_last_encrypted'] ) ) $this->setNameLastEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_last_encrypted'] ) : $arrValues['name_last_encrypted'] );
		if( isset( $arrValues['email_address_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddressEncrypted', trim( stripcslashes( $arrValues['email_address_encrypted'] ) ) ); elseif( isset( $arrValues['email_address_encrypted'] ) ) $this->setEmailAddressEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_address_encrypted'] ) : $arrValues['email_address_encrypted'] );
		if( isset( $arrValues['internal_notes'] ) && $boolDirectSet ) $this->set( 'm_strInternalNotes', trim( stripcslashes( $arrValues['internal_notes'] ) ) ); elseif( isset( $arrValues['internal_notes'] ) ) $this->setInternalNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['internal_notes'] ) : $arrValues['internal_notes'] );
		if( isset( $arrValues['ils_subtitle'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strIlsSubtitle', trim( stripcslashes( $arrValues['ils_subtitle'] ) ) ); elseif( isset( $arrValues['ils_subtitle'] ) ) $this->setIlsSubtitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ils_subtitle'] ) : $arrValues['ils_subtitle'] );
		if( isset( $arrValues['ils_activation_url'] ) && $boolDirectSet ) $this->set( 'm_strIlsActivationUrl', trim( stripcslashes( $arrValues['ils_activation_url'] ) ) ); elseif( isset( $arrValues['ils_activation_url'] ) ) $this->setIlsActivationUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ils_activation_url'] ) : $arrValues['ils_activation_url'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_guest_card_enabled'] ) && $boolDirectSet ) $this->set( 'm_intIsGuestCardEnabled', trim( $arrValues['is_guest_card_enabled'] ) ); elseif( isset( $arrValues['is_guest_card_enabled'] ) ) $this->setIsGuestCardEnabled( $arrValues['is_guest_card_enabled'] );
		if( isset( $arrValues['is_allow_phone_extension'] ) && $boolDirectSet ) $this->set( 'm_intIsAllowPhoneExtension', trim( $arrValues['is_allow_phone_extension'] ) ); elseif( isset( $arrValues['is_allow_phone_extension'] ) ) $this->setIsAllowPhoneExtension( $arrValues['is_allow_phone_extension'] );
		if( isset( $arrValues['is_supports_free_basic_listings'] ) && $boolDirectSet ) $this->set( 'm_intIsSupportsFreeBasicListings', trim( $arrValues['is_supports_free_basic_listings'] ) ); elseif( isset( $arrValues['is_supports_free_basic_listings'] ) ) $this->setIsSupportsFreeBasicListings( $arrValues['is_supports_free_basic_listings'] );
		if( isset( $arrValues['is_express'] ) && $boolDirectSet ) $this->set( 'm_intIsExpress', trim( $arrValues['is_express'] ) ); elseif( isset( $arrValues['is_express'] ) ) $this->setIsExpress( $arrValues['is_express'] );
		if( isset( $arrValues['is_national'] ) && $boolDirectSet ) $this->set( 'm_intIsNational', trim( $arrValues['is_national'] ) ); elseif( isset( $arrValues['is_national'] ) ) $this->setIsNational( $arrValues['is_national'] );
		if( isset( $arrValues['is_feed_enabled'] ) && $boolDirectSet ) $this->set( 'm_intIsFeedEnabled', trim( $arrValues['is_feed_enabled'] ) ); elseif( isset( $arrValues['is_feed_enabled'] ) ) $this->setIsFeedEnabled( $arrValues['is_feed_enabled'] );
		if( isset( $arrValues['is_limited'] ) && $boolDirectSet ) $this->set( 'm_intIsLimited', trim( $arrValues['is_limited'] ) ); elseif( isset( $arrValues['is_limited'] ) ) $this->setIsLimited( $arrValues['is_limited'] );
		if( isset( $arrValues['is_guest_card_api'] ) && $boolDirectSet ) $this->set( 'm_boolIsGuestCardApi', trim( stripcslashes( $arrValues['is_guest_card_api'] ) ) ); elseif( isset( $arrValues['is_guest_card_api'] ) ) $this->setIsGuestCardApi( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_guest_card_api'] ) : $arrValues['is_guest_card_api'] );
		if( isset( $arrValues['is_ils_api'] ) && $boolDirectSet ) $this->set( 'm_boolIsIlsApi', trim( stripcslashes( $arrValues['is_ils_api'] ) ) ); elseif( isset( $arrValues['is_ils_api'] ) ) $this->setIsIlsApi( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_ils_api'] ) : $arrValues['is_ils_api'] );
		if( isset( $arrValues['is_direct_activation'] ) && $boolDirectSet ) $this->set( 'm_boolIsDirectActivation', trim( stripcslashes( $arrValues['is_direct_activation'] ) ) ); elseif( isset( $arrValues['is_direct_activation'] ) ) $this->setIsDirectActivation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_direct_activation'] ) : $arrValues['is_direct_activation'] );
		if( isset( $arrValues['is_for_entrata_user'] ) && $boolDirectSet ) $this->set( 'm_boolIsForEntrataUser', trim( stripcslashes( $arrValues['is_for_entrata_user'] ) ) ); elseif( isset( $arrValues['is_for_entrata_user'] ) ) $this->setIsForEntrataUser( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_for_entrata_user'] ) : $arrValues['is_for_entrata_user'] );
		if( isset( $arrValues['is_feed_change_approval_not_required'] ) && $boolDirectSet ) $this->set( 'm_boolIsFeedChangeApprovalNotRequired', trim( stripcslashes( $arrValues['is_feed_change_approval_not_required'] ) ) ); elseif( isset( $arrValues['is_feed_change_approval_not_required'] ) ) $this->setIsFeedChangeApprovalNotRequired( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_feed_change_approval_not_required'] ) : $arrValues['is_feed_change_approval_not_required'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['occupancy_type_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintOccupancyTypeIds', trim( $arrValues['occupancy_type_ids'] ) ); elseif( isset( $arrValues['occupancy_type_ids'] ) ) $this->setOccupancyTypeIds( $arrValues['occupancy_type_ids'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setOauthClientId( $intOauthClientId ) {
		$this->set( 'm_intOauthClientId', CStrings::strToIntDef( $intOauthClientId, NULL, false ) );
	}

	public function getOauthClientId() {
		return $this->m_intOauthClientId;
	}

	public function sqlOauthClientId() {
		return ( true == isset( $this->m_intOauthClientId ) ) ? ( string ) $this->m_intOauthClientId : 'NULL';
	}

	public function setParentInternetListingServiceId( $intParentInternetListingServiceId ) {
		$this->set( 'm_intParentInternetListingServiceId', CStrings::strToIntDef( $intParentInternetListingServiceId, NULL, false ) );
	}

	public function getParentInternetListingServiceId() {
		return $this->m_intParentInternetListingServiceId;
	}

	public function sqlParentInternetListingServiceId() {
		return ( true == isset( $this->m_intParentInternetListingServiceId ) ) ? ( string ) $this->m_intParentInternetListingServiceId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDirectoryName( $strDirectoryName ) {
		$this->set( 'm_strDirectoryName', CStrings::strTrimDef( $strDirectoryName, 50, NULL, true ) );
	}

	public function getDirectoryName() {
		return $this->m_strDirectoryName;
	}

	public function sqlDirectoryName() {
		return ( true == isset( $this->m_strDirectoryName ) ) ? '\'' . addslashes( $this->m_strDirectoryName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setUrl( $strUrl ) {
		$this->set( 'm_strUrl', CStrings::strTrimDef( $strUrl, 4096, NULL, true ) );
	}

	public function getUrl() {
		return $this->m_strUrl;
	}

	public function sqlUrl() {
		return ( true == isset( $this->m_strUrl ) ) ? '\'' . addslashes( $this->m_strUrl ) . '\'' : 'NULL';
	}

	public function setSignupUrl( $strSignupUrl ) {
		$this->set( 'm_strSignupUrl', CStrings::strTrimDef( $strSignupUrl, 4096, NULL, true ) );
	}

	public function getSignupUrl() {
		return $this->m_strSignupUrl;
	}

	public function sqlSignupUrl() {
		return ( true == isset( $this->m_strSignupUrl ) ) ? '\'' . addslashes( $this->m_strSignupUrl ) . '\'' : 'NULL';
	}

	public function setLogoUri( $strLogoUri ) {
		$this->set( 'm_strLogoUri', CStrings::strTrimDef( $strLogoUri, 4096, NULL, true ) );
	}

	public function getLogoUri() {
		return $this->m_strLogoUri;
	}

	public function sqlLogoUri() {
		return ( true == isset( $this->m_strLogoUri ) ) ? '\'' . addslashes( $this->m_strLogoUri ) . '\'' : 'NULL';
	}

	public function setIconUri( $strIconUri ) {
		$this->set( 'm_strIconUri', CStrings::strTrimDef( $strIconUri, 4096, NULL, true ) );
	}

	public function getIconUri() {
		return $this->m_strIconUri;
	}

	public function sqlIconUri() {
		return ( true == isset( $this->m_strIconUri ) ) ? '\'' . addslashes( $this->m_strIconUri ) . '\'' : '\'NULL\'';
	}

	public function setFtpPortNumber( $intFtpPortNumber ) {
		$this->set( 'm_intFtpPortNumber', CStrings::strToIntDef( $intFtpPortNumber, NULL, false ) );
	}

	public function getFtpPortNumber() {
		return $this->m_intFtpPortNumber;
	}

	public function sqlFtpPortNumber() {
		return ( true == isset( $this->m_intFtpPortNumber ) ) ? ( string ) $this->m_intFtpPortNumber : 'NULL';
	}

	public function setFtpServerAddress( $strFtpServerAddress ) {
		$this->set( 'm_strFtpServerAddress', CStrings::strTrimDef( $strFtpServerAddress, 50, NULL, true ) );
	}

	public function getFtpServerAddress() {
		return $this->m_strFtpServerAddress;
	}

	public function sqlFtpServerAddress() {
		return ( true == isset( $this->m_strFtpServerAddress ) ) ? '\'' . addslashes( $this->m_strFtpServerAddress ) . '\'' : 'NULL';
	}

	public function setFtpUsernameEncrypted( $strFtpUsernameEncrypted ) {
		$this->set( 'm_strFtpUsernameEncrypted', CStrings::strTrimDef( $strFtpUsernameEncrypted, 240, NULL, true ) );
	}

	public function getFtpUsernameEncrypted() {
		return $this->m_strFtpUsernameEncrypted;
	}

	public function sqlFtpUsernameEncrypted() {
		return ( true == isset( $this->m_strFtpUsernameEncrypted ) ) ? '\'' . addslashes( $this->m_strFtpUsernameEncrypted ) . '\'' : 'NULL';
	}

	public function setFtpPasswordEncrypted( $strFtpPasswordEncrypted ) {
		$this->set( 'm_strFtpPasswordEncrypted', CStrings::strTrimDef( $strFtpPasswordEncrypted, 240, NULL, true ) );
	}

	public function getFtpPasswordEncrypted() {
		return $this->m_strFtpPasswordEncrypted;
	}

	public function sqlFtpPasswordEncrypted() {
		return ( true == isset( $this->m_strFtpPasswordEncrypted ) ) ? '\'' . addslashes( $this->m_strFtpPasswordEncrypted ) . '\'' : 'NULL';
	}

	public function setCheckAvailabilityPageTemplate( $strCheckAvailabilityPageTemplate ) {
		$this->set( 'm_strCheckAvailabilityPageTemplate', CStrings::strTrimDef( $strCheckAvailabilityPageTemplate, -1, NULL, true ) );
	}

	public function getCheckAvailabilityPageTemplate() {
		return $this->m_strCheckAvailabilityPageTemplate;
	}

	public function sqlCheckAvailabilityPageTemplate() {
		return ( true == isset( $this->m_strCheckAvailabilityPageTemplate ) ) ? '\'' . addslashes( $this->m_strCheckAvailabilityPageTemplate ) . '\'' : 'NULL';
	}

	public function setNameFirstEncrypted( $strNameFirstEncrypted ) {
		$this->set( 'm_strNameFirstEncrypted', CStrings::strTrimDef( $strNameFirstEncrypted, 240, NULL, true ) );
	}

	public function getNameFirstEncrypted() {
		return $this->m_strNameFirstEncrypted;
	}

	public function sqlNameFirstEncrypted() {
		return ( true == isset( $this->m_strNameFirstEncrypted ) ) ? '\'' . addslashes( $this->m_strNameFirstEncrypted ) . '\'' : 'NULL';
	}

	public function setNameLastEncrypted( $strNameLastEncrypted ) {
		$this->set( 'm_strNameLastEncrypted', CStrings::strTrimDef( $strNameLastEncrypted, 240, NULL, true ) );
	}

	public function getNameLastEncrypted() {
		return $this->m_strNameLastEncrypted;
	}

	public function sqlNameLastEncrypted() {
		return ( true == isset( $this->m_strNameLastEncrypted ) ) ? '\'' . addslashes( $this->m_strNameLastEncrypted ) . '\'' : 'NULL';
	}

	public function setEmailAddressEncrypted( $strEmailAddressEncrypted ) {
		$this->set( 'm_strEmailAddressEncrypted', CStrings::strTrimDef( $strEmailAddressEncrypted, -1, NULL, true ) );
	}

	public function getEmailAddressEncrypted() {
		return $this->m_strEmailAddressEncrypted;
	}

	public function sqlEmailAddressEncrypted() {
		return ( true == isset( $this->m_strEmailAddressEncrypted ) ) ? '\'' . addslashes( $this->m_strEmailAddressEncrypted ) . '\'' : 'NULL';
	}

	public function setInternalNotes( $strInternalNotes ) {
		$this->set( 'm_strInternalNotes', CStrings::strTrimDef( $strInternalNotes, -1, NULL, true ) );
	}

	public function getInternalNotes() {
		return $this->m_strInternalNotes;
	}

	public function sqlInternalNotes() {
		return ( true == isset( $this->m_strInternalNotes ) ) ? '\'' . addslashes( $this->m_strInternalNotes ) . '\'' : 'NULL';
	}

	public function setIlsSubtitle( $strIlsSubtitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strIlsSubtitle', CStrings::strTrimDef( $strIlsSubtitle, 250, NULL, true ), $strLocaleCode );
	}

	public function getIlsSubtitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strIlsSubtitle', $strLocaleCode );
	}

	public function sqlIlsSubtitle() {
		return ( true == isset( $this->m_strIlsSubtitle ) ) ? '\'' . addslashes( $this->m_strIlsSubtitle ) . '\'' : 'NULL';
	}

	public function setIlsActivationUrl( $strIlsActivationUrl ) {
		$this->set( 'm_strIlsActivationUrl', CStrings::strTrimDef( $strIlsActivationUrl, 4096, NULL, true ) );
	}

	public function getIlsActivationUrl() {
		return $this->m_strIlsActivationUrl;
	}

	public function sqlIlsActivationUrl() {
		return ( true == isset( $this->m_strIlsActivationUrl ) ) ? '\'' . addslashes( $this->m_strIlsActivationUrl ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsGuestCardEnabled( $intIsGuestCardEnabled ) {
		$this->set( 'm_intIsGuestCardEnabled', CStrings::strToIntDef( $intIsGuestCardEnabled, NULL, false ) );
	}

	public function getIsGuestCardEnabled() {
		return $this->m_intIsGuestCardEnabled;
	}

	public function sqlIsGuestCardEnabled() {
		return ( true == isset( $this->m_intIsGuestCardEnabled ) ) ? ( string ) $this->m_intIsGuestCardEnabled : '0';
	}

	public function setIsAllowPhoneExtension( $intIsAllowPhoneExtension ) {
		$this->set( 'm_intIsAllowPhoneExtension', CStrings::strToIntDef( $intIsAllowPhoneExtension, NULL, false ) );
	}

	public function getIsAllowPhoneExtension() {
		return $this->m_intIsAllowPhoneExtension;
	}

	public function sqlIsAllowPhoneExtension() {
		return ( true == isset( $this->m_intIsAllowPhoneExtension ) ) ? ( string ) $this->m_intIsAllowPhoneExtension : '1';
	}

	public function setIsSupportsFreeBasicListings( $intIsSupportsFreeBasicListings ) {
		$this->set( 'm_intIsSupportsFreeBasicListings', CStrings::strToIntDef( $intIsSupportsFreeBasicListings, NULL, false ) );
	}

	public function getIsSupportsFreeBasicListings() {
		return $this->m_intIsSupportsFreeBasicListings;
	}

	public function sqlIsSupportsFreeBasicListings() {
		return ( true == isset( $this->m_intIsSupportsFreeBasicListings ) ) ? ( string ) $this->m_intIsSupportsFreeBasicListings : '0';
	}

	public function setIsExpress( $intIsExpress ) {
		$this->set( 'm_intIsExpress', CStrings::strToIntDef( $intIsExpress, NULL, false ) );
	}

	public function getIsExpress() {
		return $this->m_intIsExpress;
	}

	public function sqlIsExpress() {
		return ( true == isset( $this->m_intIsExpress ) ) ? ( string ) $this->m_intIsExpress : '0';
	}

	public function setIsNational( $intIsNational ) {
		$this->set( 'm_intIsNational', CStrings::strToIntDef( $intIsNational, NULL, false ) );
	}

	public function getIsNational() {
		return $this->m_intIsNational;
	}

	public function sqlIsNational() {
		return ( true == isset( $this->m_intIsNational ) ) ? ( string ) $this->m_intIsNational : '0';
	}

	public function setIsFeedEnabled( $intIsFeedEnabled ) {
		$this->set( 'm_intIsFeedEnabled', CStrings::strToIntDef( $intIsFeedEnabled, NULL, false ) );
	}

	public function getIsFeedEnabled() {
		return $this->m_intIsFeedEnabled;
	}

	public function sqlIsFeedEnabled() {
		return ( true == isset( $this->m_intIsFeedEnabled ) ) ? ( string ) $this->m_intIsFeedEnabled : '0';
	}

	public function setIsLimited( $intIsLimited ) {
		$this->set( 'm_intIsLimited', CStrings::strToIntDef( $intIsLimited, NULL, false ) );
	}

	public function getIsLimited() {
		return $this->m_intIsLimited;
	}

	public function sqlIsLimited() {
		return ( true == isset( $this->m_intIsLimited ) ) ? ( string ) $this->m_intIsLimited : '0';
	}

	public function setIsGuestCardApi( $boolIsGuestCardApi ) {
		$this->set( 'm_boolIsGuestCardApi', CStrings::strToBool( $boolIsGuestCardApi ) );
	}

	public function getIsGuestCardApi() {
		return $this->m_boolIsGuestCardApi;
	}

	public function sqlIsGuestCardApi() {
		return ( true == isset( $this->m_boolIsGuestCardApi ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsGuestCardApi ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsIlsApi( $boolIsIlsApi ) {
		$this->set( 'm_boolIsIlsApi', CStrings::strToBool( $boolIsIlsApi ) );
	}

	public function getIsIlsApi() {
		return $this->m_boolIsIlsApi;
	}

	public function sqlIsIlsApi() {
		return ( true == isset( $this->m_boolIsIlsApi ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsIlsApi ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDirectActivation( $boolIsDirectActivation ) {
		$this->set( 'm_boolIsDirectActivation', CStrings::strToBool( $boolIsDirectActivation ) );
	}

	public function getIsDirectActivation() {
		return $this->m_boolIsDirectActivation;
	}

	public function sqlIsDirectActivation() {
		return ( true == isset( $this->m_boolIsDirectActivation ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDirectActivation ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsForEntrataUser( $boolIsForEntrataUser ) {
		$this->set( 'm_boolIsForEntrataUser', CStrings::strToBool( $boolIsForEntrataUser ) );
	}

	public function getIsForEntrataUser() {
		return $this->m_boolIsForEntrataUser;
	}

	public function sqlIsForEntrataUser() {
		return ( true == isset( $this->m_boolIsForEntrataUser ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsForEntrataUser ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsFeedChangeApprovalNotRequired( $boolIsFeedChangeApprovalNotRequired ) {
		$this->set( 'm_boolIsFeedChangeApprovalNotRequired', CStrings::strToBool( $boolIsFeedChangeApprovalNotRequired ) );
	}

	public function getIsFeedChangeApprovalNotRequired() {
		return $this->m_boolIsFeedChangeApprovalNotRequired;
	}

	public function sqlIsFeedChangeApprovalNotRequired() {
		return ( true == isset( $this->m_boolIsFeedChangeApprovalNotRequired ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFeedChangeApprovalNotRequired ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setOccupancyTypeIds( $arrintOccupancyTypeIds ) {
		$this->set( 'm_arrintOccupancyTypeIds', CStrings::strToArrIntDef( $arrintOccupancyTypeIds, NULL ) );
	}

	public function getOccupancyTypeIds() {
		return $this->m_arrintOccupancyTypeIds;
	}

	public function sqlOccupancyTypeIds() {
		return ( true == isset( $this->m_arrintOccupancyTypeIds ) && true == valArr( $this->m_arrintOccupancyTypeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintOccupancyTypeIds, NULL ) . '\'' : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, oauth_client_id, parent_internet_listing_service_id, name, directory_name, description, url, signup_url, logo_uri, icon_uri, ftp_port_number, ftp_server_address, ftp_username_encrypted, ftp_password_encrypted, check_availability_page_template, name_first_encrypted, name_last_encrypted, email_address_encrypted, internal_notes, ils_subtitle, ils_activation_url, details, is_published, is_guest_card_enabled, is_allow_phone_extension, is_supports_free_basic_listings, is_express, is_national, is_feed_enabled, is_limited, is_guest_card_api, is_ils_api, is_direct_activation, is_for_entrata_user, is_feed_change_approval_not_required, order_num, updated_by, updated_on, created_by, created_on, occupancy_type_ids )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlOauthClientId() . ', ' .
						$this->sqlParentInternetListingServiceId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDirectoryName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlUrl() . ', ' .
						$this->sqlSignupUrl() . ', ' .
						$this->sqlLogoUri() . ', ' .
						$this->sqlIconUri() . ', ' .
						$this->sqlFtpPortNumber() . ', ' .
						$this->sqlFtpServerAddress() . ', ' .
						$this->sqlFtpUsernameEncrypted() . ', ' .
						$this->sqlFtpPasswordEncrypted() . ', ' .
						$this->sqlCheckAvailabilityPageTemplate() . ', ' .
						$this->sqlNameFirstEncrypted() . ', ' .
						$this->sqlNameLastEncrypted() . ', ' .
						$this->sqlEmailAddressEncrypted() . ', ' .
						$this->sqlInternalNotes() . ', ' .
						$this->sqlIlsSubtitle() . ', ' .
						$this->sqlIlsActivationUrl() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsGuestCardEnabled() . ', ' .
						$this->sqlIsAllowPhoneExtension() . ', ' .
						$this->sqlIsSupportsFreeBasicListings() . ', ' .
						$this->sqlIsExpress() . ', ' .
						$this->sqlIsNational() . ', ' .
						$this->sqlIsFeedEnabled() . ', ' .
						$this->sqlIsLimited() . ', ' .
						$this->sqlIsGuestCardApi() . ', ' .
						$this->sqlIsIlsApi() . ', ' .
						$this->sqlIsDirectActivation() . ', ' .
						$this->sqlIsForEntrataUser() . ', ' .
						$this->sqlIsFeedChangeApprovalNotRequired() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlOccupancyTypeIds() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' oauth_client_id = ' . $this->sqlOauthClientId(). ',' ; } elseif( true == array_key_exists( 'OauthClientId', $this->getChangedColumns() ) ) { $strSql .= ' oauth_client_id = ' . $this->sqlOauthClientId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_internet_listing_service_id = ' . $this->sqlParentInternetListingServiceId(). ',' ; } elseif( true == array_key_exists( 'ParentInternetListingServiceId', $this->getChangedColumns() ) ) { $strSql .= ' parent_internet_listing_service_id = ' . $this->sqlParentInternetListingServiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' directory_name = ' . $this->sqlDirectoryName(). ',' ; } elseif( true == array_key_exists( 'DirectoryName', $this->getChangedColumns() ) ) { $strSql .= ' directory_name = ' . $this->sqlDirectoryName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' url = ' . $this->sqlUrl(). ',' ; } elseif( true == array_key_exists( 'Url', $this->getChangedColumns() ) ) { $strSql .= ' url = ' . $this->sqlUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signup_url = ' . $this->sqlSignupUrl(). ',' ; } elseif( true == array_key_exists( 'SignupUrl', $this->getChangedColumns() ) ) { $strSql .= ' signup_url = ' . $this->sqlSignupUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' logo_uri = ' . $this->sqlLogoUri(). ',' ; } elseif( true == array_key_exists( 'LogoUri', $this->getChangedColumns() ) ) { $strSql .= ' logo_uri = ' . $this->sqlLogoUri() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' icon_uri = ' . $this->sqlIconUri(). ',' ; } elseif( true == array_key_exists( 'IconUri', $this->getChangedColumns() ) ) { $strSql .= ' icon_uri = ' . $this->sqlIconUri() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ftp_port_number = ' . $this->sqlFtpPortNumber(). ',' ; } elseif( true == array_key_exists( 'FtpPortNumber', $this->getChangedColumns() ) ) { $strSql .= ' ftp_port_number = ' . $this->sqlFtpPortNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ftp_server_address = ' . $this->sqlFtpServerAddress(). ',' ; } elseif( true == array_key_exists( 'FtpServerAddress', $this->getChangedColumns() ) ) { $strSql .= ' ftp_server_address = ' . $this->sqlFtpServerAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ftp_username_encrypted = ' . $this->sqlFtpUsernameEncrypted(). ',' ; } elseif( true == array_key_exists( 'FtpUsernameEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' ftp_username_encrypted = ' . $this->sqlFtpUsernameEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ftp_password_encrypted = ' . $this->sqlFtpPasswordEncrypted(). ',' ; } elseif( true == array_key_exists( 'FtpPasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' ftp_password_encrypted = ' . $this->sqlFtpPasswordEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_availability_page_template = ' . $this->sqlCheckAvailabilityPageTemplate(). ',' ; } elseif( true == array_key_exists( 'CheckAvailabilityPageTemplate', $this->getChangedColumns() ) ) { $strSql .= ' check_availability_page_template = ' . $this->sqlCheckAvailabilityPageTemplate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first_encrypted = ' . $this->sqlNameFirstEncrypted(). ',' ; } elseif( true == array_key_exists( 'NameFirstEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' name_first_encrypted = ' . $this->sqlNameFirstEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last_encrypted = ' . $this->sqlNameLastEncrypted(). ',' ; } elseif( true == array_key_exists( 'NameLastEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' name_last_encrypted = ' . $this->sqlNameLastEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address_encrypted = ' . $this->sqlEmailAddressEncrypted(). ',' ; } elseif( true == array_key_exists( 'EmailAddressEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' email_address_encrypted = ' . $this->sqlEmailAddressEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internal_notes = ' . $this->sqlInternalNotes(). ',' ; } elseif( true == array_key_exists( 'InternalNotes', $this->getChangedColumns() ) ) { $strSql .= ' internal_notes = ' . $this->sqlInternalNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ils_subtitle = ' . $this->sqlIlsSubtitle(). ',' ; } elseif( true == array_key_exists( 'IlsSubtitle', $this->getChangedColumns() ) ) { $strSql .= ' ils_subtitle = ' . $this->sqlIlsSubtitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ils_activation_url = ' . $this->sqlIlsActivationUrl(). ',' ; } elseif( true == array_key_exists( 'IlsActivationUrl', $this->getChangedColumns() ) ) { $strSql .= ' ils_activation_url = ' . $this->sqlIlsActivationUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_guest_card_enabled = ' . $this->sqlIsGuestCardEnabled(). ',' ; } elseif( true == array_key_exists( 'IsGuestCardEnabled', $this->getChangedColumns() ) ) { $strSql .= ' is_guest_card_enabled = ' . $this->sqlIsGuestCardEnabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_allow_phone_extension = ' . $this->sqlIsAllowPhoneExtension(). ',' ; } elseif( true == array_key_exists( 'IsAllowPhoneExtension', $this->getChangedColumns() ) ) { $strSql .= ' is_allow_phone_extension = ' . $this->sqlIsAllowPhoneExtension() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_supports_free_basic_listings = ' . $this->sqlIsSupportsFreeBasicListings(). ',' ; } elseif( true == array_key_exists( 'IsSupportsFreeBasicListings', $this->getChangedColumns() ) ) { $strSql .= ' is_supports_free_basic_listings = ' . $this->sqlIsSupportsFreeBasicListings() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_express = ' . $this->sqlIsExpress(). ',' ; } elseif( true == array_key_exists( 'IsExpress', $this->getChangedColumns() ) ) { $strSql .= ' is_express = ' . $this->sqlIsExpress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_national = ' . $this->sqlIsNational(). ',' ; } elseif( true == array_key_exists( 'IsNational', $this->getChangedColumns() ) ) { $strSql .= ' is_national = ' . $this->sqlIsNational() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_feed_enabled = ' . $this->sqlIsFeedEnabled(). ',' ; } elseif( true == array_key_exists( 'IsFeedEnabled', $this->getChangedColumns() ) ) { $strSql .= ' is_feed_enabled = ' . $this->sqlIsFeedEnabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_limited = ' . $this->sqlIsLimited(). ',' ; } elseif( true == array_key_exists( 'IsLimited', $this->getChangedColumns() ) ) { $strSql .= ' is_limited = ' . $this->sqlIsLimited() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_guest_card_api = ' . $this->sqlIsGuestCardApi(). ',' ; } elseif( true == array_key_exists( 'IsGuestCardApi', $this->getChangedColumns() ) ) { $strSql .= ' is_guest_card_api = ' . $this->sqlIsGuestCardApi() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ils_api = ' . $this->sqlIsIlsApi(). ',' ; } elseif( true == array_key_exists( 'IsIlsApi', $this->getChangedColumns() ) ) { $strSql .= ' is_ils_api = ' . $this->sqlIsIlsApi() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_direct_activation = ' . $this->sqlIsDirectActivation(). ',' ; } elseif( true == array_key_exists( 'IsDirectActivation', $this->getChangedColumns() ) ) { $strSql .= ' is_direct_activation = ' . $this->sqlIsDirectActivation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_for_entrata_user = ' . $this->sqlIsForEntrataUser(). ',' ; } elseif( true == array_key_exists( 'IsForEntrataUser', $this->getChangedColumns() ) ) { $strSql .= ' is_for_entrata_user = ' . $this->sqlIsForEntrataUser() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_feed_change_approval_not_required = ' . $this->sqlIsFeedChangeApprovalNotRequired(). ',' ; } elseif( true == array_key_exists( 'IsFeedChangeApprovalNotRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_feed_change_approval_not_required = ' . $this->sqlIsFeedChangeApprovalNotRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_ids = ' . $this->sqlOccupancyTypeIds(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeIds', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_ids = ' . $this->sqlOccupancyTypeIds() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'oauth_client_id' => $this->getOauthClientId(),
			'parent_internet_listing_service_id' => $this->getParentInternetListingServiceId(),
			'name' => $this->getName(),
			'directory_name' => $this->getDirectoryName(),
			'description' => $this->getDescription(),
			'url' => $this->getUrl(),
			'signup_url' => $this->getSignupUrl(),
			'logo_uri' => $this->getLogoUri(),
			'icon_uri' => $this->getIconUri(),
			'ftp_port_number' => $this->getFtpPortNumber(),
			'ftp_server_address' => $this->getFtpServerAddress(),
			'ftp_username_encrypted' => $this->getFtpUsernameEncrypted(),
			'ftp_password_encrypted' => $this->getFtpPasswordEncrypted(),
			'check_availability_page_template' => $this->getCheckAvailabilityPageTemplate(),
			'name_first_encrypted' => $this->getNameFirstEncrypted(),
			'name_last_encrypted' => $this->getNameLastEncrypted(),
			'email_address_encrypted' => $this->getEmailAddressEncrypted(),
			'internal_notes' => $this->getInternalNotes(),
			'ils_subtitle' => $this->getIlsSubtitle(),
			'ils_activation_url' => $this->getIlsActivationUrl(),
			'details' => $this->getDetails(),
			'is_published' => $this->getIsPublished(),
			'is_guest_card_enabled' => $this->getIsGuestCardEnabled(),
			'is_allow_phone_extension' => $this->getIsAllowPhoneExtension(),
			'is_supports_free_basic_listings' => $this->getIsSupportsFreeBasicListings(),
			'is_express' => $this->getIsExpress(),
			'is_national' => $this->getIsNational(),
			'is_feed_enabled' => $this->getIsFeedEnabled(),
			'is_limited' => $this->getIsLimited(),
			'is_guest_card_api' => $this->getIsGuestCardApi(),
			'is_ils_api' => $this->getIsIlsApi(),
			'is_direct_activation' => $this->getIsDirectActivation(),
			'is_for_entrata_user' => $this->getIsForEntrataUser(),
			'is_feed_change_approval_not_required' => $this->getIsFeedChangeApprovalNotRequired(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'occupancy_type_ids' => $this->getOccupancyTypeIds()
		);
	}

}
?>