<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CPaymentBlacklistTypes
 * Do not add any new functions to this class.
 */

class CBasePaymentBlacklistTypes extends CEosPluralBase {

	/**
	 * @return CPaymentBlacklistType[]
	 */
	public static function fetchPaymentBlacklistTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPaymentBlacklistType::class, $objDatabase );
	}

	/**
	 * @return CPaymentBlacklistType
	 */
	public static function fetchPaymentBlacklistType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPaymentBlacklistType::class, $objDatabase );
	}

	public static function fetchPaymentBlacklistTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'payment_blacklist_types', $objDatabase );
	}

	public static function fetchPaymentBlacklistTypeById( $intId, $objDatabase ) {
		return self::fetchPaymentBlacklistType( sprintf( 'SELECT * FROM payment_blacklist_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>