<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CPaymentDistributionTypes
 * Do not add any new functions to this class.
 */

class CBasePaymentDistributionTypes extends CEosPluralBase {

	/**
	 * @return CPaymentDistributionType[]
	 */
	public static function fetchPaymentDistributionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPaymentDistributionType::class, $objDatabase );
	}

	/**
	 * @return CPaymentDistributionType
	 */
	public static function fetchPaymentDistributionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPaymentDistributionType::class, $objDatabase );
	}

	public static function fetchPaymentDistributionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'payment_distribution_types', $objDatabase );
	}

	public static function fetchPaymentDistributionTypeById( $intId, $objDatabase ) {
		return self::fetchPaymentDistributionType( sprintf( 'SELECT * FROM payment_distribution_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>