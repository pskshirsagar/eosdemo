<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CMilitaryPayGrades
 * Do not add any new functions to this class.
 */

class CBaseMilitaryPayGrades extends CEosPluralBase {

	/**
	 * @return CMilitaryPayGrade[]
	 */
	public static function fetchMilitaryPayGrades( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMilitaryPayGrade::class, $objDatabase );
	}

	/**
	 * @return CMilitaryPayGrade
	 */
	public static function fetchMilitaryPayGrade( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMilitaryPayGrade::class, $objDatabase );
	}

	public static function fetchMilitaryPayGradeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'military_pay_grades', $objDatabase );
	}

	public static function fetchMilitaryPayGradeById( $intId, $objDatabase ) {
		return self::fetchMilitaryPayGrade( sprintf( 'SELECT * FROM military_pay_grades WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchMilitaryPayGradesByPayGradeMacId( $strPayGradeMacId, $objDatabase ) {
		return self::fetchMilitaryPayGrades( sprintf( 'SELECT * FROM military_pay_grades WHERE pay_grade_mac_id = \'%s\'', $strPayGradeMacId ), $objDatabase );
	}

	public static function fetchMilitaryPayGradesByMilitaryOfficerStructureId( $intMilitaryOfficerStructureId, $objDatabase ) {
		return self::fetchMilitaryPayGrades( sprintf( 'SELECT * FROM military_pay_grades WHERE military_officer_structure_id = %d', $intMilitaryOfficerStructureId ), $objDatabase );
	}

}
?>