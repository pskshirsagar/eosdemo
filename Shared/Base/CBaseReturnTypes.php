<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReturnTypes
 * Do not add any new functions to this class.
 */

class CBaseReturnTypes extends CEosPluralBase {

	/**
	 * @return CReturnType[]
	 */
	public static function fetchReturnTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReturnType::class, $objDatabase );
	}

	/**
	 * @return CReturnType
	 */
	public static function fetchReturnType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReturnType::class, $objDatabase );
	}

	public static function fetchReturnTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'return_types', $objDatabase );
	}

	public static function fetchReturnTypeById( $intId, $objDatabase ) {
		return self::fetchReturnType( sprintf( 'SELECT * FROM return_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>