<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CLocales
 * Do not add any new functions to this class.
 */

class CBaseLocales extends CEosPluralBase {

	/**
	 * @return CLocale[]
	 */
	public static function fetchLocales( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CLocale::class, $objDatabase );
	}

	/**
	 * @return CLocale
	 */
	public static function fetchLocale( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CLocale::class, $objDatabase );
	}

	public static function fetchLocaleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'locales', $objDatabase );
	}

	public static function fetchLocaleByLocaleCode( $strLocaleCode, $objDatabase ) {
		return self::fetchLocale( sprintf( 'SELECT * FROM locales WHERE locale_code = %s', pg_escape_literal( $objDatabase->getHandle(), $strLocaleCode ) ), $objDatabase );
	}

}
?>