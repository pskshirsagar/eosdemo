<?php

class CBaseComplianceItemLevel extends CEosSingularBase {

	const TABLE_NAME = 'public.compliance_item_levels';

	protected $m_intId;
	protected $m_intComplianceItemId;
	protected $m_intComplianceLevelId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['compliance_item_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceItemId', trim( $arrValues['compliance_item_id'] ) ); elseif( isset( $arrValues['compliance_item_id'] ) ) $this->setComplianceItemId( $arrValues['compliance_item_id'] );
		if( isset( $arrValues['compliance_level_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceLevelId', trim( $arrValues['compliance_level_id'] ) ); elseif( isset( $arrValues['compliance_level_id'] ) ) $this->setComplianceLevelId( $arrValues['compliance_level_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setComplianceItemId( $intComplianceItemId ) {
		$this->set( 'm_intComplianceItemId', CStrings::strToIntDef( $intComplianceItemId, NULL, false ) );
	}

	public function getComplianceItemId() {
		return $this->m_intComplianceItemId;
	}

	public function sqlComplianceItemId() {
		return ( true == isset( $this->m_intComplianceItemId ) ) ? ( string ) $this->m_intComplianceItemId : 'NULL';
	}

	public function setComplianceLevelId( $intComplianceLevelId ) {
		$this->set( 'm_intComplianceLevelId', CStrings::strToIntDef( $intComplianceLevelId, NULL, false ) );
	}

	public function getComplianceLevelId() {
		return $this->m_intComplianceLevelId;
	}

	public function sqlComplianceLevelId() {
		return ( true == isset( $this->m_intComplianceLevelId ) ) ? ( string ) $this->m_intComplianceLevelId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, compliance_item_id, compliance_level_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlComplianceItemId() . ', ' .
						$this->sqlComplianceLevelId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_item_id = ' . $this->sqlComplianceItemId(). ',' ; } elseif( true == array_key_exists( 'ComplianceItemId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_item_id = ' . $this->sqlComplianceItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_level_id = ' . $this->sqlComplianceLevelId() ; } elseif( true == array_key_exists( 'ComplianceLevelId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_level_id = ' . $this->sqlComplianceLevelId() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'compliance_item_id' => $this->getComplianceItemId(),
			'compliance_level_id' => $this->getComplianceLevelId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>