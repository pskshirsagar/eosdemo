<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseHelpQuizQuestion extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.help_quiz_questions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDefaultCid;
	protected $m_intHelpQuizId;
	protected $m_intParentQuestionId;
	protected $m_strQuestion;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_intIsRequiresSync;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intHelpQuestionTypeId;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = '1';
		$this->m_intOrderNum = '0';
		$this->m_intIsRequiresSync = '1';
		$this->m_intHelpQuestionTypeId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['default_cid'] ) && $boolDirectSet ) $this->set( 'm_intDefaultCid', trim( $arrValues['default_cid'] ) ); elseif( isset( $arrValues['default_cid'] ) ) $this->setDefaultCid( $arrValues['default_cid'] );
		if( isset( $arrValues['help_quiz_id'] ) && $boolDirectSet ) $this->set( 'm_intHelpQuizId', trim( $arrValues['help_quiz_id'] ) ); elseif( isset( $arrValues['help_quiz_id'] ) ) $this->setHelpQuizId( $arrValues['help_quiz_id'] );
		if( isset( $arrValues['parent_question_id'] ) && $boolDirectSet ) $this->set( 'm_intParentQuestionId', trim( $arrValues['parent_question_id'] ) ); elseif( isset( $arrValues['parent_question_id'] ) ) $this->setParentQuestionId( $arrValues['parent_question_id'] );
		if( isset( $arrValues['question'] ) && $boolDirectSet ) $this->set( 'm_strQuestion', trim( stripcslashes( $arrValues['question'] ) ) ); elseif( isset( $arrValues['question'] ) ) $this->setQuestion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['question'] ) : $arrValues['question'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['is_requires_sync'] ) && $boolDirectSet ) $this->set( 'm_intIsRequiresSync', trim( $arrValues['is_requires_sync'] ) ); elseif( isset( $arrValues['is_requires_sync'] ) ) $this->setIsRequiresSync( $arrValues['is_requires_sync'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['help_question_type_id'] ) && $boolDirectSet ) $this->set( 'm_intHelpQuestionTypeId', trim( $arrValues['help_question_type_id'] ) ); elseif( isset( $arrValues['help_question_type_id'] ) ) $this->setHelpQuestionTypeId( $arrValues['help_question_type_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : '1';
	}

	public function setDefaultCid( $intDefaultCid ) {
		$this->set( 'm_intDefaultCid', CStrings::strToIntDef( $intDefaultCid, NULL, false ) );
	}

	public function getDefaultCid() {
		return $this->m_intDefaultCid;
	}

	public function sqlDefaultCid() {
		return ( true == isset( $this->m_intDefaultCid ) ) ? ( string ) $this->m_intDefaultCid : 'NULL';
	}

	public function setHelpQuizId( $intHelpQuizId ) {
		$this->set( 'm_intHelpQuizId', CStrings::strToIntDef( $intHelpQuizId, NULL, false ) );
	}

	public function getHelpQuizId() {
		return $this->m_intHelpQuizId;
	}

	public function sqlHelpQuizId() {
		return ( true == isset( $this->m_intHelpQuizId ) ) ? ( string ) $this->m_intHelpQuizId : 'NULL';
	}

	public function setParentQuestionId( $intParentQuestionId ) {
		$this->set( 'm_intParentQuestionId', CStrings::strToIntDef( $intParentQuestionId, NULL, false ) );
	}

	public function getParentQuestionId() {
		return $this->m_intParentQuestionId;
	}

	public function sqlParentQuestionId() {
		return ( true == isset( $this->m_intParentQuestionId ) ) ? ( string ) $this->m_intParentQuestionId : 'NULL';
	}

	public function setQuestion( $strQuestion ) {
		$this->set( 'm_strQuestion', CStrings::strTrimDef( $strQuestion, 240, NULL, true ) );
	}

	public function getQuestion() {
		return $this->m_strQuestion;
	}

	public function sqlQuestion() {
		return ( true == isset( $this->m_strQuestion ) ) ? '\'' . addslashes( $this->m_strQuestion ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setIsRequiresSync( $intIsRequiresSync ) {
		$this->set( 'm_intIsRequiresSync', CStrings::strToIntDef( $intIsRequiresSync, NULL, false ) );
	}

	public function getIsRequiresSync() {
		return $this->m_intIsRequiresSync;
	}

	public function sqlIsRequiresSync() {
		return ( true == isset( $this->m_intIsRequiresSync ) ) ? ( string ) $this->m_intIsRequiresSync : '1';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setHelpQuestionTypeId( $intHelpQuestionTypeId ) {
		$this->set( 'm_intHelpQuestionTypeId', CStrings::strToIntDef( $intHelpQuestionTypeId, NULL, false ) );
	}

	public function getHelpQuestionTypeId() {
		return $this->m_intHelpQuestionTypeId;
	}

	public function sqlHelpQuestionTypeId() {
		return ( true == isset( $this->m_intHelpQuestionTypeId ) ) ? ( string ) $this->m_intHelpQuestionTypeId : '1';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, default_cid, help_quiz_id, parent_question_id, question, is_published, order_num, is_requires_sync, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, help_question_type_id, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlDefaultCid() . ', ' .
						$this->sqlHelpQuizId() . ', ' .
						$this->sqlParentQuestionId() . ', ' .
						$this->sqlQuestion() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlIsRequiresSync() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlHelpQuestionTypeId() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_cid = ' . $this->sqlDefaultCid(). ',' ; } elseif( true == array_key_exists( 'DefaultCid', $this->getChangedColumns() ) ) { $strSql .= ' default_cid = ' . $this->sqlDefaultCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' help_quiz_id = ' . $this->sqlHelpQuizId(). ',' ; } elseif( true == array_key_exists( 'HelpQuizId', $this->getChangedColumns() ) ) { $strSql .= ' help_quiz_id = ' . $this->sqlHelpQuizId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_question_id = ' . $this->sqlParentQuestionId(). ',' ; } elseif( true == array_key_exists( 'ParentQuestionId', $this->getChangedColumns() ) ) { $strSql .= ' parent_question_id = ' . $this->sqlParentQuestionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' question = ' . $this->sqlQuestion(). ',' ; } elseif( true == array_key_exists( 'Question', $this->getChangedColumns() ) ) { $strSql .= ' question = ' . $this->sqlQuestion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_requires_sync = ' . $this->sqlIsRequiresSync(). ',' ; } elseif( true == array_key_exists( 'IsRequiresSync', $this->getChangedColumns() ) ) { $strSql .= ' is_requires_sync = ' . $this->sqlIsRequiresSync() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' help_question_type_id = ' . $this->sqlHelpQuestionTypeId(). ',' ; } elseif( true == array_key_exists( 'HelpQuestionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' help_question_type_id = ' . $this->sqlHelpQuestionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'default_cid' => $this->getDefaultCid(),
			'help_quiz_id' => $this->getHelpQuizId(),
			'parent_question_id' => $this->getParentQuestionId(),
			'question' => $this->getQuestion(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'is_requires_sync' => $this->getIsRequiresSync(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'help_question_type_id' => $this->getHelpQuestionTypeId(),
			'details' => $this->getDetails()
		);
	}

}
?>