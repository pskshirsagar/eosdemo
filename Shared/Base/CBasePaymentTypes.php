<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPaymentTypes
 * Do not add any new functions to this class.
 */

class CBasePaymentTypes extends CEosPluralBase {

	/**
	 * @return CPaymentType[]
	 */
	public static function fetchPaymentTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPaymentType::class, $objDatabase );
	}

	/**
	 * @return CPaymentType
	 */
	public static function fetchPaymentType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPaymentType::class, $objDatabase );
	}

	public static function fetchPaymentTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'payment_types', $objDatabase );
	}

	public static function fetchPaymentTypeById( $intId, $objDatabase ) {
		return self::fetchPaymentType( sprintf( 'SELECT * FROM payment_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>