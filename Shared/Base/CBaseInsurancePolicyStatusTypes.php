<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePolicyStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseInsurancePolicyStatusTypes extends CEosPluralBase {

	/**
	 * @return CInsurancePolicyStatusType[]
	 */
	public static function fetchInsurancePolicyStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsurancePolicyStatusType::class, $objDatabase );
	}

	/**
	 * @return CInsurancePolicyStatusType
	 */
	public static function fetchInsurancePolicyStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsurancePolicyStatusType::class, $objDatabase );
	}

	public static function fetchInsurancePolicyStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_policy_status_types', $objDatabase );
	}

	public static function fetchInsurancePolicyStatusTypeById( $intId, $objDatabase ) {
		return self::fetchInsurancePolicyStatusType( sprintf( 'SELECT * FROM insurance_policy_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>