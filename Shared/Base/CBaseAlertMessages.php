<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CAlertMessages
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAlertMessages extends CEosPluralBase {

	/**
	 * @return CAlertMessage[]
	 */
	public static function fetchAlertMessages( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CAlertMessage', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CAlertMessage
	 */
	public static function fetchAlertMessage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAlertMessage', $objDatabase );
	}

	public static function fetchAlertMessageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'alert_messages', $objDatabase );
	}

	public static function fetchAlertMessageByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchAlertMessage( sprintf( 'SELECT * FROM alert_messages WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAlertMessagesByCid( $intCid, $objDatabase ) {
		return self::fetchAlertMessages( sprintf( 'SELECT * FROM alert_messages WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAlertMessagesByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchAlertMessages( sprintf( 'SELECT * FROM alert_messages WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAlertMessagesByCallIdByCid( $intCallId, $intCid, $objDatabase ) {
		return self::fetchAlertMessages( sprintf( 'SELECT * FROM alert_messages WHERE call_id = %d AND cid = %d', ( int ) $intCallId, ( int ) $intCid ), $objDatabase );
	}

}
?>