<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CMilitaryInstallations
 * Do not add any new functions to this class.
 */

class CBaseMilitaryInstallations extends CEosPluralBase {

	/**
	 * @return CMilitaryInstallation[]
	 */
	public static function fetchMilitaryInstallations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMilitaryInstallation::class, $objDatabase );
	}

	/**
	 * @return CMilitaryInstallation
	 */
	public static function fetchMilitaryInstallation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMilitaryInstallation::class, $objDatabase );
	}

	public static function fetchMilitaryInstallationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'military_installations', $objDatabase );
	}

	public static function fetchMilitaryInstallationById( $intId, $objDatabase ) {
		return self::fetchMilitaryInstallation( sprintf( 'SELECT * FROM military_installations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMilitaryInstallationsByMilitaryComponentId( $intMilitaryComponentId, $objDatabase ) {
		return self::fetchMilitaryInstallations( sprintf( 'SELECT * FROM military_installations WHERE military_component_id = %d', ( int ) $intMilitaryComponentId ), $objDatabase );
	}

}
?>