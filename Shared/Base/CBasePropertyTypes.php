<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPropertyTypes
 * Do not add any new functions to this class.
 */

class CBasePropertyTypes extends CEosPluralBase {

	/**
	 * @return CPropertyType[]
	 */
	public static function fetchPropertyTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPropertyType::class, $objDatabase );
	}

	/**
	 * @return CPropertyType
	 */
	public static function fetchPropertyType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyType::class, $objDatabase );
	}

	public static function fetchPropertyTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_types', $objDatabase );
	}

	public static function fetchPropertyTypeById( $intId, $objDatabase ) {
		return self::fetchPropertyType( sprintf( 'SELECT * FROM property_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchPropertyTypesByOccupancyTypeId( $intOccupancyTypeId, $objDatabase ) {
		return self::fetchPropertyTypes( sprintf( 'SELECT * FROM property_types WHERE occupancy_type_id = %d', $intOccupancyTypeId ), $objDatabase );
	}

}
?>