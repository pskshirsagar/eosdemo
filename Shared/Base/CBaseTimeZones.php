<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTimeZones
 * Do not add any new functions to this class.
 */

class CBaseTimeZones extends CEosPluralBase {

	/**
	 * @return CTimeZone[]
	 */
	public static function fetchTimeZones( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTimeZone::class, $objDatabase );
	}

	/**
	 * @return CTimeZone
	 */
	public static function fetchTimeZone( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTimeZone::class, $objDatabase );
	}

	public static function fetchTimeZoneCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'time_zones', $objDatabase );
	}

	public static function fetchTimeZoneById( $intId, $objDatabase ) {
		return self::fetchTimeZone( sprintf( 'SELECT * FROM time_zones WHERE id = %d', $intId ), $objDatabase );
	}

}
?>