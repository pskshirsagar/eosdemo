<?php

class CBaseLocale extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.locales';

	protected $m_strLocaleCode;
	protected $m_strIsoLanguageCode;
	protected $m_strIsoLanguageName;
	protected $m_strIsoRegionCode;
	protected $m_strIsoRegionName;
	protected $m_strPluralForms;
	protected $m_boolIsBaseLocale;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsBaseLocale = false;
		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['locale_code'] ) && $boolDirectSet ) $this->set( 'm_strLocaleCode', trim( stripcslashes( $arrValues['locale_code'] ) ) ); elseif( isset( $arrValues['locale_code'] ) ) $this->setLocaleCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['locale_code'] ) : $arrValues['locale_code'] );
		if( isset( $arrValues['iso_language_code'] ) && $boolDirectSet ) $this->set( 'm_strIsoLanguageCode', trim( stripcslashes( $arrValues['iso_language_code'] ) ) ); elseif( isset( $arrValues['iso_language_code'] ) ) $this->setIsoLanguageCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['iso_language_code'] ) : $arrValues['iso_language_code'] );
		if( isset( $arrValues['iso_language_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strIsoLanguageName', trim( stripcslashes( $arrValues['iso_language_name'] ) ) ); elseif( isset( $arrValues['iso_language_name'] ) ) $this->setIsoLanguageName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['iso_language_name'] ) : $arrValues['iso_language_name'] );
		if( isset( $arrValues['iso_region_code'] ) && $boolDirectSet ) $this->set( 'm_strIsoRegionCode', trim( stripcslashes( $arrValues['iso_region_code'] ) ) ); elseif( isset( $arrValues['iso_region_code'] ) ) $this->setIsoRegionCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['iso_region_code'] ) : $arrValues['iso_region_code'] );
		if( isset( $arrValues['iso_region_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strIsoRegionName', trim( stripcslashes( $arrValues['iso_region_name'] ) ) ); elseif( isset( $arrValues['iso_region_name'] ) ) $this->setIsoRegionName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['iso_region_name'] ) : $arrValues['iso_region_name'] );
		if( isset( $arrValues['plural_forms'] ) && $boolDirectSet ) $this->set( 'm_strPluralForms', trim( stripcslashes( $arrValues['plural_forms'] ) ) ); elseif( isset( $arrValues['plural_forms'] ) ) $this->setPluralForms( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['plural_forms'] ) : $arrValues['plural_forms'] );
		if( isset( $arrValues['is_base_locale'] ) && $boolDirectSet ) $this->set( 'm_boolIsBaseLocale', trim( stripcslashes( $arrValues['is_base_locale'] ) ) ); elseif( isset( $arrValues['is_base_locale'] ) ) $this->setIsBaseLocale( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_base_locale'] ) : $arrValues['is_base_locale'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setLocaleCode( $strLocaleCode ) {
		$this->set( 'm_strLocaleCode', CStrings::strTrimDef( $strLocaleCode, 100, NULL, true ) );
	}

	public function getLocaleCode() {
		return $this->m_strLocaleCode;
	}

	public function sqlLocaleCode() {
		return ( true == isset( $this->m_strLocaleCode ) ) ? '\'' . addslashes( $this->m_strLocaleCode ) . '\'' : 'NULL';
	}

	public function setIsoLanguageCode( $strIsoLanguageCode ) {
		$this->set( 'm_strIsoLanguageCode', CStrings::strTrimDef( $strIsoLanguageCode, 3, NULL, true ) );
	}

	public function getIsoLanguageCode() {
		return $this->m_strIsoLanguageCode;
	}

	public function sqlIsoLanguageCode() {
		return ( true == isset( $this->m_strIsoLanguageCode ) ) ? '\'' . addslashes( $this->m_strIsoLanguageCode ) . '\'' : 'NULL';
	}

	public function setIsoLanguageName( $strIsoLanguageName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strIsoLanguageName', CStrings::strTrimDef( $strIsoLanguageName, 100, NULL, true ), $strLocaleCode );
	}

	public function getIsoLanguageName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strIsoLanguageName', $strLocaleCode );
	}

	public function sqlIsoLanguageName() {
		return ( true == isset( $this->m_strIsoLanguageName ) ) ? '\'' . addslashes( $this->m_strIsoLanguageName ) . '\'' : 'NULL';
	}

	public function setIsoRegionCode( $strIsoRegionCode ) {
		$this->set( 'm_strIsoRegionCode', CStrings::strTrimDef( $strIsoRegionCode, 3, NULL, true ) );
	}

	public function getIsoRegionCode() {
		return $this->m_strIsoRegionCode;
	}

	public function sqlIsoRegionCode() {
		return ( true == isset( $this->m_strIsoRegionCode ) ) ? '\'' . addslashes( $this->m_strIsoRegionCode ) . '\'' : 'NULL';
	}

	public function setIsoRegionName( $strIsoRegionName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strIsoRegionName', CStrings::strTrimDef( $strIsoRegionName, 100, NULL, true ), $strLocaleCode );
	}

	public function getIsoRegionName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strIsoRegionName', $strLocaleCode );
	}

	public function sqlIsoRegionName() {
		return ( true == isset( $this->m_strIsoRegionName ) ) ? '\'' . addslashes( $this->m_strIsoRegionName ) . '\'' : 'NULL';
	}

	public function setPluralForms( $strPluralForms ) {
		$this->set( 'm_strPluralForms', CStrings::strTrimDef( $strPluralForms, 400, NULL, true ) );
	}

	public function getPluralForms() {
		return $this->m_strPluralForms;
	}

	public function sqlPluralForms() {
		return ( true == isset( $this->m_strPluralForms ) ) ? '\'' . addslashes( $this->m_strPluralForms ) . '\'' : 'NULL';
	}

	public function setIsBaseLocale( $boolIsBaseLocale ) {
		$this->set( 'm_boolIsBaseLocale', CStrings::strToBool( $boolIsBaseLocale ) );
	}

	public function getIsBaseLocale() {
		return $this->m_boolIsBaseLocale;
	}

	public function sqlIsBaseLocale() {
		return ( true == isset( $this->m_boolIsBaseLocale ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsBaseLocale ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'locale_code' => $this->getLocaleCode(),
			'iso_language_code' => $this->getIsoLanguageCode(),
			'iso_language_name' => $this->getIsoLanguageName(),
			'iso_region_code' => $this->getIsoRegionCode(),
			'iso_region_name' => $this->getIsoRegionName(),
			'plural_forms' => $this->getPluralForms(),
			'is_base_locale' => $this->getIsBaseLocale(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>