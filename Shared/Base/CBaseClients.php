<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CClients
 * Do not add any new functions to this class.
 */

class CBaseClients extends CEosPluralBase {

	/**
	 * @return CClient[]
	 */
	public static function fetchClients( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CClient::class, $objDatabase );
	}

	/**
	 * @return CClient
	 */
	public static function fetchClient( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CClient::class, $objDatabase );
	}

	public static function fetchClientCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'clients', $objDatabase );
	}

	public static function fetchClientById( $intId, $objDatabase ) {
		return self::fetchClient( sprintf( 'SELECT * FROM clients WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchClientsByPsLeadId( $intPsLeadId, $objDatabase ) {
		return self::fetchClients( sprintf( 'SELECT * FROM clients WHERE ps_lead_id = %d', ( int ) $intPsLeadId ), $objDatabase );
	}

	public static function fetchClientsByEntityId( $intEntityId, $objDatabase ) {
		return self::fetchClients( sprintf( 'SELECT * FROM clients WHERE entity_id = %d', ( int ) $intEntityId ), $objDatabase );
	}

	public static function fetchClientsByCompanyStatusTypeId( $intCompanyStatusTypeId, $objDatabase ) {
		return self::fetchClients( sprintf( 'SELECT * FROM clients WHERE company_status_type_id = %d', ( int ) $intCompanyStatusTypeId ), $objDatabase );
	}

	public static function fetchClientsByDatabaseId( $intDatabaseId, $objDatabase ) {
		return self::fetchClients( sprintf( 'SELECT * FROM clients WHERE database_id = %d', ( int ) $intDatabaseId ), $objDatabase );
	}

}
?>