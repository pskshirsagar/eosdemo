<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSharedFilter extends CEosSingularBase {

	const TABLE_NAME = 'public.shared_filters';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intFilterTypeId;
	protected $m_intReportFilterId;
	protected $m_intCompanyGroupId;
	protected $m_intCompanyUserId;
	protected $m_strActions;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intSharedBy;
	protected $m_strSharedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['filter_type_id'] ) && $boolDirectSet ) $this->set( 'm_intFilterTypeId', trim( $arrValues['filter_type_id'] ) ); elseif( isset( $arrValues['filter_type_id'] ) ) $this->setFilterTypeId( $arrValues['filter_type_id'] );
		if( isset( $arrValues['report_filter_id'] ) && $boolDirectSet ) $this->set( 'm_intReportFilterId', trim( $arrValues['report_filter_id'] ) ); elseif( isset( $arrValues['report_filter_id'] ) ) $this->setReportFilterId( $arrValues['report_filter_id'] );
		if( isset( $arrValues['company_group_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyGroupId', trim( $arrValues['company_group_id'] ) ); elseif( isset( $arrValues['company_group_id'] ) ) $this->setCompanyGroupId( $arrValues['company_group_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['actions'] ) && $boolDirectSet ) $this->set( 'm_strActions', trim( stripcslashes( $arrValues['actions'] ) ) ); elseif( isset( $arrValues['actions'] ) ) $this->setActions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['actions'] ) : $arrValues['actions'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['shared_by'] ) && $boolDirectSet ) $this->set( 'm_intSharedBy', trim( $arrValues['shared_by'] ) ); elseif( isset( $arrValues['shared_by'] ) ) $this->setSharedBy( $arrValues['shared_by'] );
		if( isset( $arrValues['shared_on'] ) && $boolDirectSet ) $this->set( 'm_strSharedOn', trim( $arrValues['shared_on'] ) ); elseif( isset( $arrValues['shared_on'] ) ) $this->setSharedOn( $arrValues['shared_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setFilterTypeId( $intFilterTypeId ) {
		$this->set( 'm_intFilterTypeId', CStrings::strToIntDef( $intFilterTypeId, NULL, false ) );
	}

	public function getFilterTypeId() {
		return $this->m_intFilterTypeId;
	}

	public function sqlFilterTypeId() {
		return ( true == isset( $this->m_intFilterTypeId ) ) ? ( string ) $this->m_intFilterTypeId : 'NULL';
	}

	public function setReportFilterId( $intReportFilterId ) {
		$this->set( 'm_intReportFilterId', CStrings::strToIntDef( $intReportFilterId, NULL, false ) );
	}

	public function getReportFilterId() {
		return $this->m_intReportFilterId;
	}

	public function sqlReportFilterId() {
		return ( true == isset( $this->m_intReportFilterId ) ) ? ( string ) $this->m_intReportFilterId : 'NULL';
	}

	public function setCompanyGroupId( $intCompanyGroupId ) {
		$this->set( 'm_intCompanyGroupId', CStrings::strToIntDef( $intCompanyGroupId, NULL, false ) );
	}

	public function getCompanyGroupId() {
		return $this->m_intCompanyGroupId;
	}

	public function sqlCompanyGroupId() {
		return ( true == isset( $this->m_intCompanyGroupId ) ) ? ( string ) $this->m_intCompanyGroupId : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setActions( $strActions ) {
		$this->set( 'm_strActions', CStrings::strTrimDef( $strActions, -1, NULL, true ) );
	}

	public function getActions() {
		return $this->m_strActions;
	}

	public function sqlActions() {
		return ( true == isset( $this->m_strActions ) ) ? '\'' . addslashes( $this->m_strActions ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setSharedBy( $intSharedBy ) {
		$this->set( 'm_intSharedBy', CStrings::strToIntDef( $intSharedBy, NULL, false ) );
	}

	public function getSharedBy() {
		return $this->m_intSharedBy;
	}

	public function sqlSharedBy() {
		return ( true == isset( $this->m_intSharedBy ) ) ? ( string ) $this->m_intSharedBy : 'NULL';
	}

	public function setSharedOn( $strSharedOn ) {
		$this->set( 'm_strSharedOn', CStrings::strTrimDef( $strSharedOn, -1, NULL, true ) );
	}

	public function getSharedOn() {
		return $this->m_strSharedOn;
	}

	public function sqlSharedOn() {
		return ( true == isset( $this->m_strSharedOn ) ) ? '\'' . $this->m_strSharedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, filter_type_id, report_filter_id, company_group_id, company_user_id, actions, deleted_by, deleted_on, updated_by, updated_on, shared_by, shared_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlFilterTypeId() . ', ' .
 						$this->sqlReportFilterId() . ', ' .
 						$this->sqlCompanyGroupId() . ', ' .
 						$this->sqlCompanyUserId() . ', ' .
 						$this->sqlActions() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
 						$this->sqlSharedBy() . ', ' .
 						$this->sqlSharedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' filter_type_id = ' . $this->sqlFilterTypeId() . ','; } elseif( true == array_key_exists( 'FilterTypeId', $this->getChangedColumns() ) ) { $strSql .= ' filter_type_id = ' . $this->sqlFilterTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_filter_id = ' . $this->sqlReportFilterId() . ','; } elseif( true == array_key_exists( 'ReportFilterId', $this->getChangedColumns() ) ) { $strSql .= ' report_filter_id = ' . $this->sqlReportFilterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_group_id = ' . $this->sqlCompanyGroupId() . ','; } elseif( true == array_key_exists( 'CompanyGroupId', $this->getChangedColumns() ) ) { $strSql .= ' company_group_id = ' . $this->sqlCompanyGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actions = ' . $this->sqlActions() . ','; } elseif( true == array_key_exists( 'Actions', $this->getChangedColumns() ) ) { $strSql .= ' actions = ' . $this->sqlActions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' shared_by = ' . $this->sqlSharedBy() . ','; } elseif( true == array_key_exists( 'SharedBy', $this->getChangedColumns() ) ) { $strSql .= ' shared_by = ' . $this->sqlSharedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' shared_on = ' . $this->sqlSharedOn() . ','; } elseif( true == array_key_exists( 'SharedOn', $this->getChangedColumns() ) ) { $strSql .= ' shared_on = ' . $this->sqlSharedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'filter_type_id' => $this->getFilterTypeId(),
			'report_filter_id' => $this->getReportFilterId(),
			'company_group_id' => $this->getCompanyGroupId(),
			'company_user_id' => $this->getCompanyUserId(),
			'actions' => $this->getActions(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'shared_by' => $this->getSharedBy(),
			'shared_on' => $this->getSharedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>