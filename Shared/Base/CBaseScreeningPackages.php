<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackages
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningPackages extends CEosPluralBase {

	/**
	 * @return CScreeningPackage[]
	 */
	public static function fetchScreeningPackages( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningPackage::class, $objDatabase );
	}

	/**
	 * @return CScreeningPackage
	 */
	public static function fetchScreeningPackage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningPackage::class, $objDatabase );
	}

	public static function fetchScreeningPackageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_packages', $objDatabase );
	}

	public static function fetchScreeningPackageByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackage( sprintf( 'SELECT * FROM screening_packages WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackagesByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningPackages( sprintf( 'SELECT * FROM screening_packages WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackagesByParentPackageIdByCid( $intParentPackageId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackages( sprintf( 'SELECT * FROM screening_packages WHERE parent_package_id = %d AND cid = %d', ( int ) $intParentPackageId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackagesByDelinquentAccountTimeframeTypeIdByCid( $intDelinquentAccountTimeframeTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackages( sprintf( 'SELECT * FROM screening_packages WHERE delinquent_account_timeframe_type_id = %d AND cid = %d', ( int ) $intDelinquentAccountTimeframeTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackagesByScreeningScoringModelIdByCid( $intScreeningScoringModelId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackages( sprintf( 'SELECT * FROM screening_packages WHERE screening_scoring_model_id = %d AND cid = %d', ( int ) $intScreeningScoringModelId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackagesByScreeningPackageTypeIdByCid( $intScreeningPackageTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackages( sprintf( 'SELECT * FROM screening_packages WHERE screening_package_type_id = %d AND cid = %d', ( int ) $intScreeningPackageTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackagesByScreeningCumulationTypeIdByCid( $intScreeningCumulationTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackages( sprintf( 'SELECT * FROM screening_packages WHERE screening_cumulation_type_id = %d AND cid = %d', ( int ) $intScreeningCumulationTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>