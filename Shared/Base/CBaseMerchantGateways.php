<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CMerchantGateways
 * Do not add any new functions to this class.
 */

class CBaseMerchantGateways extends CEosPluralBase {

	/**
	 * @return CMerchantGateway[]
	 */
	public static function fetchMerchantGateways( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMerchantGateway::class, $objDatabase );
	}

	/**
	 * @return CMerchantGateway
	 */
	public static function fetchMerchantGateway( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMerchantGateway::class, $objDatabase );
	}

	public static function fetchMerchantGatewayCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'merchant_gateways', $objDatabase );
	}

	public static function fetchMerchantGatewayById( $intId, $objDatabase ) {
		return self::fetchMerchantGateway( sprintf( 'SELECT * FROM merchant_gateways WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMerchantGatewaysByGatewayTypeId( $intGatewayTypeId, $objDatabase ) {
		return self::fetchMerchantGateways( sprintf( 'SELECT * FROM merchant_gateways WHERE gateway_type_id = %d', ( int ) $intGatewayTypeId ), $objDatabase );
	}

}
?>