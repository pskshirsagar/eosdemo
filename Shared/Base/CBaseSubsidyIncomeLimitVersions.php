<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSubsidyIncomeLimitVersions
 * Do not add any new functions to this class.
 */

class CBaseSubsidyIncomeLimitVersions extends CEosPluralBase {

	/**
	 * @return CSubsidyIncomeLimitVersion[]
	 */
	public static function fetchSubsidyIncomeLimitVersions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSubsidyIncomeLimitVersion', $objDatabase );
	}

	/**
	 * @return CSubsidyIncomeLimitVersion
	 */
	public static function fetchSubsidyIncomeLimitVersion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSubsidyIncomeLimitVersion', $objDatabase );
	}

	public static function fetchSubsidyIncomeLimitVersionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_income_limit_versions', $objDatabase );
	}

	public static function fetchSubsidyIncomeLimitVersionById( $intId, $objDatabase ) {
		return self::fetchSubsidyIncomeLimitVersion( sprintf( 'SELECT * FROM subsidy_income_limit_versions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>