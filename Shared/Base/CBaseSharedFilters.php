<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSharedFilters
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSharedFilters extends CEosPluralBase {

	/**
	 * @return CSharedFilter[]
	 */
	public static function fetchSharedFilters( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CSharedFilter', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSharedFilter
	 */
	public static function fetchSharedFilter( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSharedFilter', $objDatabase );
	}

	public static function fetchSharedFilterCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'shared_filters', $objDatabase );
	}

	public static function fetchSharedFilterByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSharedFilter( sprintf( 'SELECT * FROM shared_filters WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSharedFiltersByCid( $intCid, $objDatabase ) {
		return self::fetchSharedFilters( sprintf( 'SELECT * FROM shared_filters WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSharedFiltersByFilterTypeIdByCid( $intFilterTypeId, $intCid, $objDatabase ) {
		return self::fetchSharedFilters( sprintf( 'SELECT * FROM shared_filters WHERE filter_type_id = %d AND cid = %d', ( int ) $intFilterTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSharedFiltersByReportFilterIdByCid( $intReportFilterId, $intCid, $objDatabase ) {
		return self::fetchSharedFilters( sprintf( 'SELECT * FROM shared_filters WHERE report_filter_id = %d AND cid = %d', ( int ) $intReportFilterId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSharedFiltersByCompanyGroupIdByCid( $intCompanyGroupId, $intCid, $objDatabase ) {
		return self::fetchSharedFilters( sprintf( 'SELECT * FROM shared_filters WHERE company_group_id = %d AND cid = %d', ( int ) $intCompanyGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSharedFiltersByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchSharedFilters( sprintf( 'SELECT * FROM shared_filters WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

}
?>