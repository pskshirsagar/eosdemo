<?php

class CBaseCompanySettingKey extends CEosSingularBase {

	const TABLE_NAME = 'public.company_setting_keys';

	protected $m_intId;
	protected $m_intCompanySettingGroupId;
	protected $m_intDatabaseTypeId;
	protected $m_intToolTipId;
	protected $m_strKey;
	protected $m_strLabel;
	protected $m_strTableName;
	protected $m_strColumnName;
	protected $m_boolRequiresSync;
	protected $m_boolIsDisabled;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intDatabaseTypeId = '0';
		$this->m_boolRequiresSync = false;
		$this->m_boolIsDisabled = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['company_setting_group_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanySettingGroupId', trim( $arrValues['company_setting_group_id'] ) ); elseif( isset( $arrValues['company_setting_group_id'] ) ) $this->setCompanySettingGroupId( $arrValues['company_setting_group_id'] );
		if( isset( $arrValues['database_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseTypeId', trim( $arrValues['database_type_id'] ) ); elseif( isset( $arrValues['database_type_id'] ) ) $this->setDatabaseTypeId( $arrValues['database_type_id'] );
		if( isset( $arrValues['tool_tip_id'] ) && $boolDirectSet ) $this->set( 'm_intToolTipId', trim( $arrValues['tool_tip_id'] ) ); elseif( isset( $arrValues['tool_tip_id'] ) ) $this->setToolTipId( $arrValues['tool_tip_id'] );
		if( isset( $arrValues['key'] ) && $boolDirectSet ) $this->set( 'm_strKey', trim( stripcslashes( $arrValues['key'] ) ) ); elseif( isset( $arrValues['key'] ) ) $this->setKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['key'] ) : $arrValues['key'] );
		if( isset( $arrValues['label'] ) && $boolDirectSet ) $this->set( 'm_strLabel', trim( stripcslashes( $arrValues['label'] ) ) ); elseif( isset( $arrValues['label'] ) ) $this->setLabel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['label'] ) : $arrValues['label'] );
		if( isset( $arrValues['table_name'] ) && $boolDirectSet ) $this->set( 'm_strTableName', trim( stripcslashes( $arrValues['table_name'] ) ) ); elseif( isset( $arrValues['table_name'] ) ) $this->setTableName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['table_name'] ) : $arrValues['table_name'] );
		if( isset( $arrValues['column_name'] ) && $boolDirectSet ) $this->set( 'm_strColumnName', trim( stripcslashes( $arrValues['column_name'] ) ) ); elseif( isset( $arrValues['column_name'] ) ) $this->setColumnName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['column_name'] ) : $arrValues['column_name'] );
		if( isset( $arrValues['requires_sync'] ) && $boolDirectSet ) $this->set( 'm_boolRequiresSync', trim( stripcslashes( $arrValues['requires_sync'] ) ) ); elseif( isset( $arrValues['requires_sync'] ) ) $this->setRequiresSync( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['requires_sync'] ) : $arrValues['requires_sync'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCompanySettingGroupId( $intCompanySettingGroupId ) {
		$this->set( 'm_intCompanySettingGroupId', CStrings::strToIntDef( $intCompanySettingGroupId, NULL, false ) );
	}

	public function getCompanySettingGroupId() {
		return $this->m_intCompanySettingGroupId;
	}

	public function sqlCompanySettingGroupId() {
		return ( true == isset( $this->m_intCompanySettingGroupId ) ) ? ( string ) $this->m_intCompanySettingGroupId : 'NULL';
	}

	public function setDatabaseTypeId( $intDatabaseTypeId ) {
		$this->set( 'm_intDatabaseTypeId', CStrings::strToIntDef( $intDatabaseTypeId, NULL, false ) );
	}

	public function getDatabaseTypeId() {
		return $this->m_intDatabaseTypeId;
	}

	public function sqlDatabaseTypeId() {
		return ( true == isset( $this->m_intDatabaseTypeId ) ) ? ( string ) $this->m_intDatabaseTypeId : '0';
	}

	public function setToolTipId( $intToolTipId ) {
		$this->set( 'm_intToolTipId', CStrings::strToIntDef( $intToolTipId, NULL, false ) );
	}

	public function getToolTipId() {
		return $this->m_intToolTipId;
	}

	public function sqlToolTipId() {
		return ( true == isset( $this->m_intToolTipId ) ) ? ( string ) $this->m_intToolTipId : 'NULL';
	}

	public function setKey( $strKey ) {
		$this->set( 'm_strKey', CStrings::strTrimDef( $strKey, 80, NULL, true ) );
	}

	public function getKey() {
		return $this->m_strKey;
	}

	public function sqlKey() {
		return ( true == isset( $this->m_strKey ) ) ? '\'' . addslashes( $this->m_strKey ) . '\'' : 'NULL';
	}

	public function setLabel( $strLabel ) {
		$this->set( 'm_strLabel', CStrings::strTrimDef( $strLabel, 240, NULL, true ) );
	}

	public function getLabel() {
		return $this->m_strLabel;
	}

	public function sqlLabel() {
		return ( true == isset( $this->m_strLabel ) ) ? '\'' . addslashes( $this->m_strLabel ) . '\'' : 'NULL';
	}

	public function setTableName( $strTableName ) {
		$this->set( 'm_strTableName', CStrings::strTrimDef( $strTableName, 80, NULL, true ) );
	}

	public function getTableName() {
		return $this->m_strTableName;
	}

	public function sqlTableName() {
		return ( true == isset( $this->m_strTableName ) ) ? '\'' . addslashes( $this->m_strTableName ) . '\'' : 'NULL';
	}

	public function setColumnName( $strColumnName ) {
		$this->set( 'm_strColumnName', CStrings::strTrimDef( $strColumnName, 80, NULL, true ) );
	}

	public function getColumnName() {
		return $this->m_strColumnName;
	}

	public function sqlColumnName() {
		return ( true == isset( $this->m_strColumnName ) ) ? '\'' . addslashes( $this->m_strColumnName ) . '\'' : 'NULL';
	}

	public function setRequiresSync( $boolRequiresSync ) {
		$this->set( 'm_boolRequiresSync', CStrings::strToBool( $boolRequiresSync ) );
	}

	public function getRequiresSync() {
		return $this->m_boolRequiresSync;
	}

	public function sqlRequiresSync() {
		return ( true == isset( $this->m_boolRequiresSync ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequiresSync ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, company_setting_group_id, database_type_id, tool_tip_id, key, label, table_name, column_name, requires_sync, is_disabled, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCompanySettingGroupId() . ', ' .
 						$this->sqlDatabaseTypeId() . ', ' .
 						$this->sqlToolTipId() . ', ' .
 						$this->sqlKey() . ', ' .
 						$this->sqlLabel() . ', ' .
 						$this->sqlTableName() . ', ' .
 						$this->sqlColumnName() . ', ' .
 						$this->sqlRequiresSync() . ', ' .
 						$this->sqlIsDisabled() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_setting_group_id = ' . $this->sqlCompanySettingGroupId() . ','; } elseif( true == array_key_exists( 'CompanySettingGroupId', $this->getChangedColumns() ) ) { $strSql .= ' company_setting_group_id = ' . $this->sqlCompanySettingGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_type_id = ' . $this->sqlDatabaseTypeId() . ','; } elseif( true == array_key_exists( 'DatabaseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' database_type_id = ' . $this->sqlDatabaseTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tool_tip_id = ' . $this->sqlToolTipId() . ','; } elseif( true == array_key_exists( 'ToolTipId', $this->getChangedColumns() ) ) { $strSql .= ' tool_tip_id = ' . $this->sqlToolTipId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' key = ' . $this->sqlKey() . ','; } elseif( true == array_key_exists( 'Key', $this->getChangedColumns() ) ) { $strSql .= ' key = ' . $this->sqlKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' label = ' . $this->sqlLabel() . ','; } elseif( true == array_key_exists( 'Label', $this->getChangedColumns() ) ) { $strSql .= ' label = ' . $this->sqlLabel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' table_name = ' . $this->sqlTableName() . ','; } elseif( true == array_key_exists( 'TableName', $this->getChangedColumns() ) ) { $strSql .= ' table_name = ' . $this->sqlTableName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' column_name = ' . $this->sqlColumnName() . ','; } elseif( true == array_key_exists( 'ColumnName', $this->getChangedColumns() ) ) { $strSql .= ' column_name = ' . $this->sqlColumnName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requires_sync = ' . $this->sqlRequiresSync() . ','; } elseif( true == array_key_exists( 'RequiresSync', $this->getChangedColumns() ) ) { $strSql .= ' requires_sync = ' . $this->sqlRequiresSync() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'company_setting_group_id' => $this->getCompanySettingGroupId(),
			'database_type_id' => $this->getDatabaseTypeId(),
			'tool_tip_id' => $this->getToolTipId(),
			'key' => $this->getKey(),
			'label' => $this->getLabel(),
			'table_name' => $this->getTableName(),
			'column_name' => $this->getColumnName(),
			'requires_sync' => $this->getRequiresSync(),
			'is_disabled' => $this->getIsDisabled(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>