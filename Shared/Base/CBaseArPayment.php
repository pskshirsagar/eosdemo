<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseArPayment extends CEosSingularBase {

	const TABLE_NAME = 'public.ar_payments';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCompanyMerchantAccountId;
	protected $m_intProcessingBankAccountId;
	protected $m_intMerchantGatewayId;
	protected $m_intPropertyId;
	protected $m_intCustomerId;
	protected $m_intLeaseId;
	protected $m_intPaymentTypeId;
	protected $m_intPaymentMediumId;
	protected $m_intPaymentStatusTypeId;
	protected $m_intReturnTypeId;
	protected $m_intArPaymentId;
	protected $m_intScheduledPaymentId;
	protected $m_intArCodeId;
	protected $m_intCustomerPaymentAccountId;
	protected $m_intArPaymentTransmissionId;
	protected $m_intArPaymentExportId;
	protected $m_intPsProductId;
	protected $m_intPsProductOptionId;
	protected $m_intAccountVerificationLogId;
	protected $m_intCompanyCharityId;
	protected $m_intLeaseStatusTypeId;
	protected $m_intAllocationTypeId;
	protected $m_strRemotePrimaryKey;
	protected $m_strReturnRemotePrimaryKey;
	protected $m_strPaymentDatetime;
	protected $m_strReceiptDatetime;
	protected $m_fltPaymentAmount;
	protected $m_fltConvenienceFeeAmount;
	protected $m_fltDonationAmount;
	protected $m_fltTotalAmount;
	protected $m_fltPaymentCost;
	protected $m_fltCompanyChargeAmount;
	protected $m_fltIncentiveCompanyChargeAmount;
	protected $m_intSecureReferenceNumber;
	protected $m_strRemotePaymentNumber;
	protected $m_strPaymentMemo;
	protected $m_strBilltoIpAddress;
	protected $m_strBilltoIpAddressEncrypted;
	protected $m_strBilltoAccountNumber;
	protected $m_strBilltoCompanyName;
	protected $m_strBilltoUnitNumber;
	protected $m_strBilltoNameFirst;
	protected $m_strBilltoNameFirstEncrypted;
	protected $m_strBilltoNameMiddle;
	protected $m_strBilltoNameMiddleEncrypted;
	protected $m_strBilltoNameLast;
	protected $m_strBilltoNameLastEncrypted;
	protected $m_strBilltoStreetLine1;
	protected $m_strBilltoStreetLine2;
	protected $m_strBilltoStreetLine3;
	protected $m_strBilltoCity;
	protected $m_strBilltoStateCode;
	protected $m_strBilltoProvince;
	protected $m_strBilltoPostalCode;
	protected $m_strBilltoCountryCode;
	protected $m_strBilltoPhoneNumber;
	protected $m_strBilltoPhoneNumberEncrypted;
	protected $m_strBilltoEmailAddress;
	protected $m_strBilltoEmailAddressEncrypted;
	protected $m_fltCarLarAmount;
	protected $m_intCarLarConfidence;
	protected $m_intCarLarIsMoneyOrder;
	protected $m_intCarLarIsPerformed;
	protected $m_intPhoneAuthRequired;
	protected $m_intPhoneAuthBy;
	protected $m_strPhoneAuthOn;
	protected $m_intRequiresApproval;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_strExportedOn;
	protected $m_strReturnExportedOn;
	protected $m_strReturnBatchedOn;
	protected $m_strFeePostedOn;
	protected $m_strReturnFeePostedOn;
	protected $m_strReverseFeePostedOn;
	protected $m_intIsApplicationPayment;
	protected $m_strAllocateArTransactionIds;
	protected $m_strCcCardNumberEncrypted;
	protected $m_strCcExpDateMonth;
	protected $m_strCcExpDateYear;
	protected $m_strCcNameOnCard;
	protected $m_strCcBinNumber;
	protected $m_strCheckNumber;
	protected $m_strCheckDate;
	protected $m_strCheckPayableTo;
	protected $m_strCheckBankName;
	protected $m_strCheckNameOnAccount;
	protected $m_intCheckAccountTypeId;
	protected $m_strCheckRoutingNumber;
	protected $m_strCheckAccountNumberEncrypted;
	protected $m_strCheckAccountNumberLastfourEncrypted;
	protected $m_strCheckAuxillaryOnUs;
	protected $m_strCheckEpc;
	protected $m_intCheckIsMoneyOrder;
	protected $m_intCheckIsConverted;
	protected $m_intBlockAutoAssociation;
	protected $m_strFedTrackingNumber;
	protected $m_intIsSplit;
	protected $m_intIsReversed;
	protected $m_intIsReconPrepped;
	protected $m_boolIsDebitCard;
	protected $m_boolIsCheckCard;
	protected $m_boolIsGiftCard;
	protected $m_boolIsPrepaidCard;
	protected $m_boolIsCreditCard;
	protected $m_boolIsInternationalCard;
	protected $m_boolIsCommercialCard;
	protected $m_boolIsRiskReview;
	protected $m_intBlockAssociation;
	protected $m_intOrderNum;
	protected $m_intLockSequence;
	protected $m_strTermsAcceptedOn;
	protected $m_strDistributionBlockedOn;
	protected $m_strAutoCaptureOn;
	protected $m_strDistributeOn;
	protected $m_strDistributeReturnOn;
	protected $m_strReturnedOn;
	protected $m_strBatchedOn;
	protected $m_strEftBatchedOn;
	protected $m_strCapturedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strCurrencyCode;
	protected $m_intCardTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_fltDonationAmount = '0';
		$this->m_intIsSplit = '0';
		$this->m_intIsReversed = '0';
		$this->m_intIsReconPrepped = '0';
		$this->m_boolIsDebitCard = false;
		$this->m_intBlockAssociation = '0';
		$this->m_strCurrencyCode = 'USD';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_merchant_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMerchantAccountId', trim( $arrValues['company_merchant_account_id'] ) ); elseif( isset( $arrValues['company_merchant_account_id'] ) ) $this->setCompanyMerchantAccountId( $arrValues['company_merchant_account_id'] );
		if( isset( $arrValues['processing_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankAccountId', trim( $arrValues['processing_bank_account_id'] ) ); elseif( isset( $arrValues['processing_bank_account_id'] ) ) $this->setProcessingBankAccountId( $arrValues['processing_bank_account_id'] );
		if( isset( $arrValues['merchant_gateway_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantGatewayId', trim( $arrValues['merchant_gateway_id'] ) ); elseif( isset( $arrValues['merchant_gateway_id'] ) ) $this->setMerchantGatewayId( $arrValues['merchant_gateway_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['payment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentTypeId', trim( $arrValues['payment_type_id'] ) ); elseif( isset( $arrValues['payment_type_id'] ) ) $this->setPaymentTypeId( $arrValues['payment_type_id'] );
		if( isset( $arrValues['payment_medium_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentMediumId', trim( $arrValues['payment_medium_id'] ) ); elseif( isset( $arrValues['payment_medium_id'] ) ) $this->setPaymentMediumId( $arrValues['payment_medium_id'] );
		if( isset( $arrValues['payment_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentStatusTypeId', trim( $arrValues['payment_status_type_id'] ) ); elseif( isset( $arrValues['payment_status_type_id'] ) ) $this->setPaymentStatusTypeId( $arrValues['payment_status_type_id'] );
		if( isset( $arrValues['return_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReturnTypeId', trim( $arrValues['return_type_id'] ) ); elseif( isset( $arrValues['return_type_id'] ) ) $this->setReturnTypeId( $arrValues['return_type_id'] );
		if( isset( $arrValues['ar_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentId', trim( $arrValues['ar_payment_id'] ) ); elseif( isset( $arrValues['ar_payment_id'] ) ) $this->setArPaymentId( $arrValues['ar_payment_id'] );
		if( isset( $arrValues['scheduled_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledPaymentId', trim( $arrValues['scheduled_payment_id'] ) ); elseif( isset( $arrValues['scheduled_payment_id'] ) ) $this->setScheduledPaymentId( $arrValues['scheduled_payment_id'] );
		if( isset( $arrValues['ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeId', trim( $arrValues['ar_code_id'] ) ); elseif( isset( $arrValues['ar_code_id'] ) ) $this->setArCodeId( $arrValues['ar_code_id'] );
		if( isset( $arrValues['customer_payment_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerPaymentAccountId', trim( $arrValues['customer_payment_account_id'] ) ); elseif( isset( $arrValues['customer_payment_account_id'] ) ) $this->setCustomerPaymentAccountId( $arrValues['customer_payment_account_id'] );
		if( isset( $arrValues['ar_payment_transmission_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentTransmissionId', trim( $arrValues['ar_payment_transmission_id'] ) ); elseif( isset( $arrValues['ar_payment_transmission_id'] ) ) $this->setArPaymentTransmissionId( $arrValues['ar_payment_transmission_id'] );
		if( isset( $arrValues['ar_payment_export_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentExportId', trim( $arrValues['ar_payment_export_id'] ) ); elseif( isset( $arrValues['ar_payment_export_id'] ) ) $this->setArPaymentExportId( $arrValues['ar_payment_export_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['ps_product_option_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductOptionId', trim( $arrValues['ps_product_option_id'] ) ); elseif( isset( $arrValues['ps_product_option_id'] ) ) $this->setPsProductOptionId( $arrValues['ps_product_option_id'] );
		if( isset( $arrValues['account_verification_log_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountVerificationLogId', trim( $arrValues['account_verification_log_id'] ) ); elseif( isset( $arrValues['account_verification_log_id'] ) ) $this->setAccountVerificationLogId( $arrValues['account_verification_log_id'] );
		if( isset( $arrValues['company_charity_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyCharityId', trim( $arrValues['company_charity_id'] ) ); elseif( isset( $arrValues['company_charity_id'] ) ) $this->setCompanyCharityId( $arrValues['company_charity_id'] );
		if( isset( $arrValues['lease_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStatusTypeId', trim( $arrValues['lease_status_type_id'] ) ); elseif( isset( $arrValues['lease_status_type_id'] ) ) $this->setLeaseStatusTypeId( $arrValues['lease_status_type_id'] );
		if( isset( $arrValues['allocation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAllocationTypeId', trim( $arrValues['allocation_type_id'] ) ); elseif( isset( $arrValues['allocation_type_id'] ) ) $this->setAllocationTypeId( $arrValues['allocation_type_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['return_remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strReturnRemotePrimaryKey', trim( stripcslashes( $arrValues['return_remote_primary_key'] ) ) ); elseif( isset( $arrValues['return_remote_primary_key'] ) ) $this->setReturnRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['return_remote_primary_key'] ) : $arrValues['return_remote_primary_key'] );
		if( isset( $arrValues['payment_datetime'] ) && $boolDirectSet ) $this->set( 'm_strPaymentDatetime', trim( $arrValues['payment_datetime'] ) ); elseif( isset( $arrValues['payment_datetime'] ) ) $this->setPaymentDatetime( $arrValues['payment_datetime'] );
		if( isset( $arrValues['receipt_datetime'] ) && $boolDirectSet ) $this->set( 'm_strReceiptDatetime', trim( $arrValues['receipt_datetime'] ) ); elseif( isset( $arrValues['receipt_datetime'] ) ) $this->setReceiptDatetime( $arrValues['receipt_datetime'] );
		if( isset( $arrValues['payment_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPaymentAmount', trim( $arrValues['payment_amount'] ) ); elseif( isset( $arrValues['payment_amount'] ) ) $this->setPaymentAmount( $arrValues['payment_amount'] );
		if( isset( $arrValues['convenience_fee_amount'] ) && $boolDirectSet ) $this->set( 'm_fltConvenienceFeeAmount', trim( $arrValues['convenience_fee_amount'] ) ); elseif( isset( $arrValues['convenience_fee_amount'] ) ) $this->setConvenienceFeeAmount( $arrValues['convenience_fee_amount'] );
		if( isset( $arrValues['donation_amount'] ) && $boolDirectSet ) $this->set( 'm_fltDonationAmount', trim( $arrValues['donation_amount'] ) ); elseif( isset( $arrValues['donation_amount'] ) ) $this->setDonationAmount( $arrValues['donation_amount'] );
		if( isset( $arrValues['total_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalAmount', trim( $arrValues['total_amount'] ) ); elseif( isset( $arrValues['total_amount'] ) ) $this->setTotalAmount( $arrValues['total_amount'] );
		if( isset( $arrValues['payment_cost'] ) && $boolDirectSet ) $this->set( 'm_fltPaymentCost', trim( $arrValues['payment_cost'] ) ); elseif( isset( $arrValues['payment_cost'] ) ) $this->setPaymentCost( $arrValues['payment_cost'] );
		if( isset( $arrValues['company_charge_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCompanyChargeAmount', trim( $arrValues['company_charge_amount'] ) ); elseif( isset( $arrValues['company_charge_amount'] ) ) $this->setCompanyChargeAmount( $arrValues['company_charge_amount'] );
		if( isset( $arrValues['incentive_company_charge_amount'] ) && $boolDirectSet ) $this->set( 'm_fltIncentiveCompanyChargeAmount', trim( $arrValues['incentive_company_charge_amount'] ) ); elseif( isset( $arrValues['incentive_company_charge_amount'] ) ) $this->setIncentiveCompanyChargeAmount( $arrValues['incentive_company_charge_amount'] );
		if( isset( $arrValues['secure_reference_number'] ) && $boolDirectSet ) $this->set( 'm_intSecureReferenceNumber', trim( $arrValues['secure_reference_number'] ) ); elseif( isset( $arrValues['secure_reference_number'] ) ) $this->setSecureReferenceNumber( $arrValues['secure_reference_number'] );
		if( isset( $arrValues['remote_payment_number'] ) && $boolDirectSet ) $this->set( 'm_strRemotePaymentNumber', trim( stripcslashes( $arrValues['remote_payment_number'] ) ) ); elseif( isset( $arrValues['remote_payment_number'] ) ) $this->setRemotePaymentNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_payment_number'] ) : $arrValues['remote_payment_number'] );
		if( isset( $arrValues['payment_memo'] ) && $boolDirectSet ) $this->set( 'm_strPaymentMemo', trim( stripcslashes( $arrValues['payment_memo'] ) ) ); elseif( isset( $arrValues['payment_memo'] ) ) $this->setPaymentMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payment_memo'] ) : $arrValues['payment_memo'] );
		if( isset( $arrValues['billto_ip_address'] ) && $boolDirectSet ) $this->set( 'm_strBilltoIpAddress', trim( stripcslashes( $arrValues['billto_ip_address'] ) ) ); elseif( isset( $arrValues['billto_ip_address'] ) ) $this->setBilltoIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_ip_address'] ) : $arrValues['billto_ip_address'] );
		if( isset( $arrValues['billto_ip_address_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBilltoIpAddressEncrypted', trim( stripcslashes( $arrValues['billto_ip_address_encrypted'] ) ) ); elseif( isset( $arrValues['billto_ip_address_encrypted'] ) ) $this->setBilltoIpAddressEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_ip_address_encrypted'] ) : $arrValues['billto_ip_address_encrypted'] );
		if( isset( $arrValues['billto_account_number'] ) && $boolDirectSet ) $this->set( 'm_strBilltoAccountNumber', trim( stripcslashes( $arrValues['billto_account_number'] ) ) ); elseif( isset( $arrValues['billto_account_number'] ) ) $this->setBilltoAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_account_number'] ) : $arrValues['billto_account_number'] );
		if( isset( $arrValues['billto_company_name'] ) && $boolDirectSet ) $this->set( 'm_strBilltoCompanyName', trim( stripcslashes( $arrValues['billto_company_name'] ) ) ); elseif( isset( $arrValues['billto_company_name'] ) ) $this->setBilltoCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_company_name'] ) : $arrValues['billto_company_name'] );
		if( isset( $arrValues['billto_unit_number'] ) && $boolDirectSet ) $this->set( 'm_strBilltoUnitNumber', trim( stripcslashes( $arrValues['billto_unit_number'] ) ) ); elseif( isset( $arrValues['billto_unit_number'] ) ) $this->setBilltoUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_unit_number'] ) : $arrValues['billto_unit_number'] );
		if( isset( $arrValues['billto_name_first'] ) && $boolDirectSet ) $this->set( 'm_strBilltoNameFirst', trim( stripcslashes( $arrValues['billto_name_first'] ) ) ); elseif( isset( $arrValues['billto_name_first'] ) ) $this->setBilltoNameFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_name_first'] ) : $arrValues['billto_name_first'] );
		if( isset( $arrValues['billto_name_first_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBilltoNameFirstEncrypted', trim( stripcslashes( $arrValues['billto_name_first_encrypted'] ) ) ); elseif( isset( $arrValues['billto_name_first_encrypted'] ) ) $this->setBilltoNameFirstEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_name_first_encrypted'] ) : $arrValues['billto_name_first_encrypted'] );
		if( isset( $arrValues['billto_name_middle'] ) && $boolDirectSet ) $this->set( 'm_strBilltoNameMiddle', trim( stripcslashes( $arrValues['billto_name_middle'] ) ) ); elseif( isset( $arrValues['billto_name_middle'] ) ) $this->setBilltoNameMiddle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_name_middle'] ) : $arrValues['billto_name_middle'] );
		if( isset( $arrValues['billto_name_middle_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBilltoNameMiddleEncrypted', trim( stripcslashes( $arrValues['billto_name_middle_encrypted'] ) ) ); elseif( isset( $arrValues['billto_name_middle_encrypted'] ) ) $this->setBilltoNameMiddleEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_name_middle_encrypted'] ) : $arrValues['billto_name_middle_encrypted'] );
		if( isset( $arrValues['billto_name_last'] ) && $boolDirectSet ) $this->set( 'm_strBilltoNameLast', trim( stripcslashes( $arrValues['billto_name_last'] ) ) ); elseif( isset( $arrValues['billto_name_last'] ) ) $this->setBilltoNameLast( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_name_last'] ) : $arrValues['billto_name_last'] );
		if( isset( $arrValues['billto_name_last_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBilltoNameLastEncrypted', trim( stripcslashes( $arrValues['billto_name_last_encrypted'] ) ) ); elseif( isset( $arrValues['billto_name_last_encrypted'] ) ) $this->setBilltoNameLastEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_name_last_encrypted'] ) : $arrValues['billto_name_last_encrypted'] );
		if( isset( $arrValues['billto_street_line1'] ) && $boolDirectSet ) $this->set( 'm_strBilltoStreetLine1', trim( stripcslashes( $arrValues['billto_street_line1'] ) ) ); elseif( isset( $arrValues['billto_street_line1'] ) ) $this->setBilltoStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_street_line1'] ) : $arrValues['billto_street_line1'] );
		if( isset( $arrValues['billto_street_line2'] ) && $boolDirectSet ) $this->set( 'm_strBilltoStreetLine2', trim( stripcslashes( $arrValues['billto_street_line2'] ) ) ); elseif( isset( $arrValues['billto_street_line2'] ) ) $this->setBilltoStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_street_line2'] ) : $arrValues['billto_street_line2'] );
		if( isset( $arrValues['billto_street_line3'] ) && $boolDirectSet ) $this->set( 'm_strBilltoStreetLine3', trim( stripcslashes( $arrValues['billto_street_line3'] ) ) ); elseif( isset( $arrValues['billto_street_line3'] ) ) $this->setBilltoStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_street_line3'] ) : $arrValues['billto_street_line3'] );
		if( isset( $arrValues['billto_city'] ) && $boolDirectSet ) $this->set( 'm_strBilltoCity', trim( stripcslashes( $arrValues['billto_city'] ) ) ); elseif( isset( $arrValues['billto_city'] ) ) $this->setBilltoCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_city'] ) : $arrValues['billto_city'] );
		if( isset( $arrValues['billto_state_code'] ) && $boolDirectSet ) $this->set( 'm_strBilltoStateCode', trim( stripcslashes( $arrValues['billto_state_code'] ) ) ); elseif( isset( $arrValues['billto_state_code'] ) ) $this->setBilltoStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_state_code'] ) : $arrValues['billto_state_code'] );
		if( isset( $arrValues['billto_province'] ) && $boolDirectSet ) $this->set( 'm_strBilltoProvince', trim( stripcslashes( $arrValues['billto_province'] ) ) ); elseif( isset( $arrValues['billto_province'] ) ) $this->setBilltoProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_province'] ) : $arrValues['billto_province'] );
		if( isset( $arrValues['billto_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strBilltoPostalCode', trim( stripcslashes( $arrValues['billto_postal_code'] ) ) ); elseif( isset( $arrValues['billto_postal_code'] ) ) $this->setBilltoPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_postal_code'] ) : $arrValues['billto_postal_code'] );
		if( isset( $arrValues['billto_country_code'] ) && $boolDirectSet ) $this->set( 'm_strBilltoCountryCode', trim( stripcslashes( $arrValues['billto_country_code'] ) ) ); elseif( isset( $arrValues['billto_country_code'] ) ) $this->setBilltoCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_country_code'] ) : $arrValues['billto_country_code'] );
		if( isset( $arrValues['billto_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strBilltoPhoneNumber', trim( stripcslashes( $arrValues['billto_phone_number'] ) ) ); elseif( isset( $arrValues['billto_phone_number'] ) ) $this->setBilltoPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_phone_number'] ) : $arrValues['billto_phone_number'] );
		if( isset( $arrValues['billto_phone_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBilltoPhoneNumberEncrypted', trim( stripcslashes( $arrValues['billto_phone_number_encrypted'] ) ) ); elseif( isset( $arrValues['billto_phone_number_encrypted'] ) ) $this->setBilltoPhoneNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_phone_number_encrypted'] ) : $arrValues['billto_phone_number_encrypted'] );
		if( isset( $arrValues['billto_email_address'] ) && $boolDirectSet ) $this->set( 'm_strBilltoEmailAddress', trim( stripcslashes( $arrValues['billto_email_address'] ) ) ); elseif( isset( $arrValues['billto_email_address'] ) ) $this->setBilltoEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_email_address'] ) : $arrValues['billto_email_address'] );
		if( isset( $arrValues['billto_email_address_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBilltoEmailAddressEncrypted', trim( stripcslashes( $arrValues['billto_email_address_encrypted'] ) ) ); elseif( isset( $arrValues['billto_email_address_encrypted'] ) ) $this->setBilltoEmailAddressEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_email_address_encrypted'] ) : $arrValues['billto_email_address_encrypted'] );
		if( isset( $arrValues['car_lar_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCarLarAmount', trim( $arrValues['car_lar_amount'] ) ); elseif( isset( $arrValues['car_lar_amount'] ) ) $this->setCarLarAmount( $arrValues['car_lar_amount'] );
		if( isset( $arrValues['car_lar_confidence'] ) && $boolDirectSet ) $this->set( 'm_intCarLarConfidence', trim( $arrValues['car_lar_confidence'] ) ); elseif( isset( $arrValues['car_lar_confidence'] ) ) $this->setCarLarConfidence( $arrValues['car_lar_confidence'] );
		if( isset( $arrValues['car_lar_is_money_order'] ) && $boolDirectSet ) $this->set( 'm_intCarLarIsMoneyOrder', trim( $arrValues['car_lar_is_money_order'] ) ); elseif( isset( $arrValues['car_lar_is_money_order'] ) ) $this->setCarLarIsMoneyOrder( $arrValues['car_lar_is_money_order'] );
		if( isset( $arrValues['car_lar_is_performed'] ) && $boolDirectSet ) $this->set( 'm_intCarLarIsPerformed', trim( $arrValues['car_lar_is_performed'] ) ); elseif( isset( $arrValues['car_lar_is_performed'] ) ) $this->setCarLarIsPerformed( $arrValues['car_lar_is_performed'] );
		if( isset( $arrValues['phone_auth_required'] ) && $boolDirectSet ) $this->set( 'm_intPhoneAuthRequired', trim( $arrValues['phone_auth_required'] ) ); elseif( isset( $arrValues['phone_auth_required'] ) ) $this->setPhoneAuthRequired( $arrValues['phone_auth_required'] );
		if( isset( $arrValues['phone_auth_by'] ) && $boolDirectSet ) $this->set( 'm_intPhoneAuthBy', trim( $arrValues['phone_auth_by'] ) ); elseif( isset( $arrValues['phone_auth_by'] ) ) $this->setPhoneAuthBy( $arrValues['phone_auth_by'] );
		if( isset( $arrValues['phone_auth_on'] ) && $boolDirectSet ) $this->set( 'm_strPhoneAuthOn', trim( $arrValues['phone_auth_on'] ) ); elseif( isset( $arrValues['phone_auth_on'] ) ) $this->setPhoneAuthOn( $arrValues['phone_auth_on'] );
		if( isset( $arrValues['requires_approval'] ) && $boolDirectSet ) $this->set( 'm_intRequiresApproval', trim( $arrValues['requires_approval'] ) ); elseif( isset( $arrValues['requires_approval'] ) ) $this->setRequiresApproval( $arrValues['requires_approval'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['return_exported_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnExportedOn', trim( $arrValues['return_exported_on'] ) ); elseif( isset( $arrValues['return_exported_on'] ) ) $this->setReturnExportedOn( $arrValues['return_exported_on'] );
		if( isset( $arrValues['return_batched_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnBatchedOn', trim( $arrValues['return_batched_on'] ) ); elseif( isset( $arrValues['return_batched_on'] ) ) $this->setReturnBatchedOn( $arrValues['return_batched_on'] );
		if( isset( $arrValues['fee_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strFeePostedOn', trim( $arrValues['fee_posted_on'] ) ); elseif( isset( $arrValues['fee_posted_on'] ) ) $this->setFeePostedOn( $arrValues['fee_posted_on'] );
		if( isset( $arrValues['return_fee_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnFeePostedOn', trim( $arrValues['return_fee_posted_on'] ) ); elseif( isset( $arrValues['return_fee_posted_on'] ) ) $this->setReturnFeePostedOn( $arrValues['return_fee_posted_on'] );
		if( isset( $arrValues['reverse_fee_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strReverseFeePostedOn', trim( $arrValues['reverse_fee_posted_on'] ) ); elseif( isset( $arrValues['reverse_fee_posted_on'] ) ) $this->setReverseFeePostedOn( $arrValues['reverse_fee_posted_on'] );
		if( isset( $arrValues['is_application_payment'] ) && $boolDirectSet ) $this->set( 'm_intIsApplicationPayment', trim( $arrValues['is_application_payment'] ) ); elseif( isset( $arrValues['is_application_payment'] ) ) $this->setIsApplicationPayment( $arrValues['is_application_payment'] );
		if( isset( $arrValues['allocate_ar_transaction_ids'] ) && $boolDirectSet ) $this->set( 'm_strAllocateArTransactionIds', trim( stripcslashes( $arrValues['allocate_ar_transaction_ids'] ) ) ); elseif( isset( $arrValues['allocate_ar_transaction_ids'] ) ) $this->setAllocateArTransactionIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allocate_ar_transaction_ids'] ) : $arrValues['allocate_ar_transaction_ids'] );
		if( isset( $arrValues['cc_card_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCcCardNumberEncrypted', trim( stripcslashes( $arrValues['cc_card_number_encrypted'] ) ) ); elseif( isset( $arrValues['cc_card_number_encrypted'] ) ) $this->setCcCardNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cc_card_number_encrypted'] ) : $arrValues['cc_card_number_encrypted'] );
		if( isset( $arrValues['cc_exp_date_month'] ) && $boolDirectSet ) $this->set( 'm_strCcExpDateMonth', trim( stripcslashes( $arrValues['cc_exp_date_month'] ) ) ); elseif( isset( $arrValues['cc_exp_date_month'] ) ) $this->setCcExpDateMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cc_exp_date_month'] ) : $arrValues['cc_exp_date_month'] );
		if( isset( $arrValues['cc_exp_date_year'] ) && $boolDirectSet ) $this->set( 'm_strCcExpDateYear', trim( stripcslashes( $arrValues['cc_exp_date_year'] ) ) ); elseif( isset( $arrValues['cc_exp_date_year'] ) ) $this->setCcExpDateYear( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cc_exp_date_year'] ) : $arrValues['cc_exp_date_year'] );
		if( isset( $arrValues['cc_name_on_card'] ) && $boolDirectSet ) $this->set( 'm_strCcNameOnCard', trim( stripcslashes( $arrValues['cc_name_on_card'] ) ) ); elseif( isset( $arrValues['cc_name_on_card'] ) ) $this->setCcNameOnCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cc_name_on_card'] ) : $arrValues['cc_name_on_card'] );
		if( isset( $arrValues['cc_bin_number'] ) && $boolDirectSet ) $this->set( 'm_strCcBinNumber', trim( stripcslashes( $arrValues['cc_bin_number'] ) ) ); elseif( isset( $arrValues['cc_bin_number'] ) ) $this->setCcBinNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cc_bin_number'] ) : $arrValues['cc_bin_number'] );
		if( isset( $arrValues['check_number'] ) && $boolDirectSet ) $this->set( 'm_strCheckNumber', trim( stripcslashes( $arrValues['check_number'] ) ) ); elseif( isset( $arrValues['check_number'] ) ) $this->setCheckNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_number'] ) : $arrValues['check_number'] );
		if( isset( $arrValues['check_date'] ) && $boolDirectSet ) $this->set( 'm_strCheckDate', trim( $arrValues['check_date'] ) ); elseif( isset( $arrValues['check_date'] ) ) $this->setCheckDate( $arrValues['check_date'] );
		if( isset( $arrValues['check_payable_to'] ) && $boolDirectSet ) $this->set( 'm_strCheckPayableTo', trim( stripcslashes( $arrValues['check_payable_to'] ) ) ); elseif( isset( $arrValues['check_payable_to'] ) ) $this->setCheckPayableTo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_payable_to'] ) : $arrValues['check_payable_to'] );
		if( isset( $arrValues['check_bank_name'] ) && $boolDirectSet ) $this->set( 'm_strCheckBankName', trim( stripcslashes( $arrValues['check_bank_name'] ) ) ); elseif( isset( $arrValues['check_bank_name'] ) ) $this->setCheckBankName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_bank_name'] ) : $arrValues['check_bank_name'] );
		if( isset( $arrValues['check_name_on_account'] ) && $boolDirectSet ) $this->set( 'm_strCheckNameOnAccount', trim( stripcslashes( $arrValues['check_name_on_account'] ) ) ); elseif( isset( $arrValues['check_name_on_account'] ) ) $this->setCheckNameOnAccount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_name_on_account'] ) : $arrValues['check_name_on_account'] );
		if( isset( $arrValues['check_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCheckAccountTypeId', trim( $arrValues['check_account_type_id'] ) ); elseif( isset( $arrValues['check_account_type_id'] ) ) $this->setCheckAccountTypeId( $arrValues['check_account_type_id'] );
		if( isset( $arrValues['check_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strCheckRoutingNumber', trim( stripcslashes( $arrValues['check_routing_number'] ) ) ); elseif( isset( $arrValues['check_routing_number'] ) ) $this->setCheckRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_routing_number'] ) : $arrValues['check_routing_number'] );
		if( isset( $arrValues['check_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCheckAccountNumberEncrypted', trim( stripcslashes( $arrValues['check_account_number_encrypted'] ) ) ); elseif( isset( $arrValues['check_account_number_encrypted'] ) ) $this->setCheckAccountNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_account_number_encrypted'] ) : $arrValues['check_account_number_encrypted'] );
		if( isset( $arrValues['check_account_number_lastfour_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCheckAccountNumberLastfourEncrypted', trim( stripcslashes( $arrValues['check_account_number_lastfour_encrypted'] ) ) ); elseif( isset( $arrValues['check_account_number_lastfour_encrypted'] ) ) $this->setCheckAccountNumberLastfourEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_account_number_lastfour_encrypted'] ) : $arrValues['check_account_number_lastfour_encrypted'] );
		if( isset( $arrValues['check_auxillary_on_us'] ) && $boolDirectSet ) $this->set( 'm_strCheckAuxillaryOnUs', trim( stripcslashes( $arrValues['check_auxillary_on_us'] ) ) ); elseif( isset( $arrValues['check_auxillary_on_us'] ) ) $this->setCheckAuxillaryOnUs( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_auxillary_on_us'] ) : $arrValues['check_auxillary_on_us'] );
		if( isset( $arrValues['check_epc'] ) && $boolDirectSet ) $this->set( 'm_strCheckEpc', trim( stripcslashes( $arrValues['check_epc'] ) ) ); elseif( isset( $arrValues['check_epc'] ) ) $this->setCheckEpc( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_epc'] ) : $arrValues['check_epc'] );
		if( isset( $arrValues['check_is_money_order'] ) && $boolDirectSet ) $this->set( 'm_intCheckIsMoneyOrder', trim( $arrValues['check_is_money_order'] ) ); elseif( isset( $arrValues['check_is_money_order'] ) ) $this->setCheckIsMoneyOrder( $arrValues['check_is_money_order'] );
		if( isset( $arrValues['check_is_converted'] ) && $boolDirectSet ) $this->set( 'm_intCheckIsConverted', trim( $arrValues['check_is_converted'] ) ); elseif( isset( $arrValues['check_is_converted'] ) ) $this->setCheckIsConverted( $arrValues['check_is_converted'] );
		if( isset( $arrValues['block_auto_association'] ) && $boolDirectSet ) $this->set( 'm_intBlockAutoAssociation', trim( $arrValues['block_auto_association'] ) ); elseif( isset( $arrValues['block_auto_association'] ) ) $this->setBlockAutoAssociation( $arrValues['block_auto_association'] );
		if( isset( $arrValues['fed_tracking_number'] ) && $boolDirectSet ) $this->set( 'm_strFedTrackingNumber', trim( stripcslashes( $arrValues['fed_tracking_number'] ) ) ); elseif( isset( $arrValues['fed_tracking_number'] ) ) $this->setFedTrackingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['fed_tracking_number'] ) : $arrValues['fed_tracking_number'] );
		if( isset( $arrValues['is_split'] ) && $boolDirectSet ) $this->set( 'm_intIsSplit', trim( $arrValues['is_split'] ) ); elseif( isset( $arrValues['is_split'] ) ) $this->setIsSplit( $arrValues['is_split'] );
		if( isset( $arrValues['is_reversed'] ) && $boolDirectSet ) $this->set( 'm_intIsReversed', trim( $arrValues['is_reversed'] ) ); elseif( isset( $arrValues['is_reversed'] ) ) $this->setIsReversed( $arrValues['is_reversed'] );
		if( isset( $arrValues['is_recon_prepped'] ) && $boolDirectSet ) $this->set( 'm_intIsReconPrepped', trim( $arrValues['is_recon_prepped'] ) ); elseif( isset( $arrValues['is_recon_prepped'] ) ) $this->setIsReconPrepped( $arrValues['is_recon_prepped'] );
		if( isset( $arrValues['is_debit_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsDebitCard', trim( stripcslashes( $arrValues['is_debit_card'] ) ) ); elseif( isset( $arrValues['is_debit_card'] ) ) $this->setIsDebitCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_debit_card'] ) : $arrValues['is_debit_card'] );
		if( isset( $arrValues['is_check_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsCheckCard', trim( stripcslashes( $arrValues['is_check_card'] ) ) ); elseif( isset( $arrValues['is_check_card'] ) ) $this->setIsCheckCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_check_card'] ) : $arrValues['is_check_card'] );
		if( isset( $arrValues['is_gift_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsGiftCard', trim( stripcslashes( $arrValues['is_gift_card'] ) ) ); elseif( isset( $arrValues['is_gift_card'] ) ) $this->setIsGiftCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_gift_card'] ) : $arrValues['is_gift_card'] );
		if( isset( $arrValues['is_prepaid_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsPrepaidCard', trim( stripcslashes( $arrValues['is_prepaid_card'] ) ) ); elseif( isset( $arrValues['is_prepaid_card'] ) ) $this->setIsPrepaidCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_prepaid_card'] ) : $arrValues['is_prepaid_card'] );
		if( isset( $arrValues['is_credit_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsCreditCard', trim( stripcslashes( $arrValues['is_credit_card'] ) ) ); elseif( isset( $arrValues['is_credit_card'] ) ) $this->setIsCreditCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_credit_card'] ) : $arrValues['is_credit_card'] );
		if( isset( $arrValues['is_international_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsInternationalCard', trim( stripcslashes( $arrValues['is_international_card'] ) ) ); elseif( isset( $arrValues['is_international_card'] ) ) $this->setIsInternationalCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_international_card'] ) : $arrValues['is_international_card'] );
		if( isset( $arrValues['is_commercial_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsCommercialCard', trim( stripcslashes( $arrValues['is_commercial_card'] ) ) ); elseif( isset( $arrValues['is_commercial_card'] ) ) $this->setIsCommercialCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_commercial_card'] ) : $arrValues['is_commercial_card'] );
		if( isset( $arrValues['is_risk_review'] ) && $boolDirectSet ) $this->set( 'm_boolIsRiskReview', trim( stripcslashes( $arrValues['is_risk_review'] ) ) ); elseif( isset( $arrValues['is_risk_review'] ) ) $this->setIsRiskReview( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_risk_review'] ) : $arrValues['is_risk_review'] );
		if( isset( $arrValues['block_association'] ) && $boolDirectSet ) $this->set( 'm_intBlockAssociation', trim( $arrValues['block_association'] ) ); elseif( isset( $arrValues['block_association'] ) ) $this->setBlockAssociation( $arrValues['block_association'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['lock_sequence'] ) && $boolDirectSet ) $this->set( 'm_intLockSequence', trim( $arrValues['lock_sequence'] ) ); elseif( isset( $arrValues['lock_sequence'] ) ) $this->setLockSequence( $arrValues['lock_sequence'] );
		if( isset( $arrValues['terms_accepted_on'] ) && $boolDirectSet ) $this->set( 'm_strTermsAcceptedOn', trim( $arrValues['terms_accepted_on'] ) ); elseif( isset( $arrValues['terms_accepted_on'] ) ) $this->setTermsAcceptedOn( $arrValues['terms_accepted_on'] );
		if( isset( $arrValues['distribution_blocked_on'] ) && $boolDirectSet ) $this->set( 'm_strDistributionBlockedOn', trim( $arrValues['distribution_blocked_on'] ) ); elseif( isset( $arrValues['distribution_blocked_on'] ) ) $this->setDistributionBlockedOn( $arrValues['distribution_blocked_on'] );
		if( isset( $arrValues['auto_capture_on'] ) && $boolDirectSet ) $this->set( 'm_strAutoCaptureOn', trim( $arrValues['auto_capture_on'] ) ); elseif( isset( $arrValues['auto_capture_on'] ) ) $this->setAutoCaptureOn( $arrValues['auto_capture_on'] );
		if( isset( $arrValues['distribute_on'] ) && $boolDirectSet ) $this->set( 'm_strDistributeOn', trim( $arrValues['distribute_on'] ) ); elseif( isset( $arrValues['distribute_on'] ) ) $this->setDistributeOn( $arrValues['distribute_on'] );
		if( isset( $arrValues['distribute_return_on'] ) && $boolDirectSet ) $this->set( 'm_strDistributeReturnOn', trim( $arrValues['distribute_return_on'] ) ); elseif( isset( $arrValues['distribute_return_on'] ) ) $this->setDistributeReturnOn( $arrValues['distribute_return_on'] );
		if( isset( $arrValues['returned_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnedOn', trim( $arrValues['returned_on'] ) ); elseif( isset( $arrValues['returned_on'] ) ) $this->setReturnedOn( $arrValues['returned_on'] );
		if( isset( $arrValues['batched_on'] ) && $boolDirectSet ) $this->set( 'm_strBatchedOn', trim( $arrValues['batched_on'] ) ); elseif( isset( $arrValues['batched_on'] ) ) $this->setBatchedOn( $arrValues['batched_on'] );
		if( isset( $arrValues['eft_batched_on'] ) && $boolDirectSet ) $this->set( 'm_strEftBatchedOn', trim( $arrValues['eft_batched_on'] ) ); elseif( isset( $arrValues['eft_batched_on'] ) ) $this->setEftBatchedOn( $arrValues['eft_batched_on'] );
		if( isset( $arrValues['captured_on'] ) && $boolDirectSet ) $this->set( 'm_strCapturedOn', trim( $arrValues['captured_on'] ) ); elseif( isset( $arrValues['captured_on'] ) ) $this->setCapturedOn( $arrValues['captured_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( stripcslashes( $arrValues['currency_code'] ) ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['currency_code'] ) : $arrValues['currency_code'] );
		if( isset( $arrValues['card_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCardTypeId', trim( $arrValues['card_type_id'] ) ); elseif( isset( $arrValues['card_type_id'] ) ) $this->setCardTypeId( $arrValues['card_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCompanyMerchantAccountId( $intCompanyMerchantAccountId ) {
		$this->set( 'm_intCompanyMerchantAccountId', CStrings::strToIntDef( $intCompanyMerchantAccountId, NULL, false ) );
	}

	public function getCompanyMerchantAccountId() {
		return $this->m_intCompanyMerchantAccountId;
	}

	public function sqlCompanyMerchantAccountId() {
		return ( true == isset( $this->m_intCompanyMerchantAccountId ) ) ? ( string ) $this->m_intCompanyMerchantAccountId : 'NULL';
	}

	public function setProcessingBankAccountId( $intProcessingBankAccountId ) {
		$this->set( 'm_intProcessingBankAccountId', CStrings::strToIntDef( $intProcessingBankAccountId, NULL, false ) );
	}

	public function getProcessingBankAccountId() {
		return $this->m_intProcessingBankAccountId;
	}

	public function sqlProcessingBankAccountId() {
		return ( true == isset( $this->m_intProcessingBankAccountId ) ) ? ( string ) $this->m_intProcessingBankAccountId : 'NULL';
	}

	public function setMerchantGatewayId( $intMerchantGatewayId ) {
		$this->set( 'm_intMerchantGatewayId', CStrings::strToIntDef( $intMerchantGatewayId, NULL, false ) );
	}

	public function getMerchantGatewayId() {
		return $this->m_intMerchantGatewayId;
	}

	public function sqlMerchantGatewayId() {
		return ( true == isset( $this->m_intMerchantGatewayId ) ) ? ( string ) $this->m_intMerchantGatewayId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setPaymentTypeId( $intPaymentTypeId ) {
		$this->set( 'm_intPaymentTypeId', CStrings::strToIntDef( $intPaymentTypeId, NULL, false ) );
	}

	public function getPaymentTypeId() {
		return $this->m_intPaymentTypeId;
	}

	public function sqlPaymentTypeId() {
		return ( true == isset( $this->m_intPaymentTypeId ) ) ? ( string ) $this->m_intPaymentTypeId : 'NULL';
	}

	public function setPaymentMediumId( $intPaymentMediumId ) {
		$this->set( 'm_intPaymentMediumId', CStrings::strToIntDef( $intPaymentMediumId, NULL, false ) );
	}

	public function getPaymentMediumId() {
		return $this->m_intPaymentMediumId;
	}

	public function sqlPaymentMediumId() {
		return ( true == isset( $this->m_intPaymentMediumId ) ) ? ( string ) $this->m_intPaymentMediumId : 'NULL';
	}

	public function setPaymentStatusTypeId( $intPaymentStatusTypeId ) {
		$this->set( 'm_intPaymentStatusTypeId', CStrings::strToIntDef( $intPaymentStatusTypeId, NULL, false ) );
	}

	public function getPaymentStatusTypeId() {
		return $this->m_intPaymentStatusTypeId;
	}

	public function sqlPaymentStatusTypeId() {
		return ( true == isset( $this->m_intPaymentStatusTypeId ) ) ? ( string ) $this->m_intPaymentStatusTypeId : 'NULL';
	}

	public function setReturnTypeId( $intReturnTypeId ) {
		$this->set( 'm_intReturnTypeId', CStrings::strToIntDef( $intReturnTypeId, NULL, false ) );
	}

	public function getReturnTypeId() {
		return $this->m_intReturnTypeId;
	}

	public function sqlReturnTypeId() {
		return ( true == isset( $this->m_intReturnTypeId ) ) ? ( string ) $this->m_intReturnTypeId : 'NULL';
	}

	public function setArPaymentId( $intArPaymentId ) {
		$this->set( 'm_intArPaymentId', CStrings::strToIntDef( $intArPaymentId, NULL, false ) );
	}

	public function getArPaymentId() {
		return $this->m_intArPaymentId;
	}

	public function sqlArPaymentId() {
		return ( true == isset( $this->m_intArPaymentId ) ) ? ( string ) $this->m_intArPaymentId : 'NULL';
	}

	public function setScheduledPaymentId( $intScheduledPaymentId ) {
		$this->set( 'm_intScheduledPaymentId', CStrings::strToIntDef( $intScheduledPaymentId, NULL, false ) );
	}

	public function getScheduledPaymentId() {
		return $this->m_intScheduledPaymentId;
	}

	public function sqlScheduledPaymentId() {
		return ( true == isset( $this->m_intScheduledPaymentId ) ) ? ( string ) $this->m_intScheduledPaymentId : 'NULL';
	}

	public function setArCodeId( $intArCodeId ) {
		$this->set( 'm_intArCodeId', CStrings::strToIntDef( $intArCodeId, NULL, false ) );
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function sqlArCodeId() {
		return ( true == isset( $this->m_intArCodeId ) ) ? ( string ) $this->m_intArCodeId : 'NULL';
	}

	public function setCustomerPaymentAccountId( $intCustomerPaymentAccountId ) {
		$this->set( 'm_intCustomerPaymentAccountId', CStrings::strToIntDef( $intCustomerPaymentAccountId, NULL, false ) );
	}

	public function getCustomerPaymentAccountId() {
		return $this->m_intCustomerPaymentAccountId;
	}

	public function sqlCustomerPaymentAccountId() {
		return ( true == isset( $this->m_intCustomerPaymentAccountId ) ) ? ( string ) $this->m_intCustomerPaymentAccountId : 'NULL';
	}

	public function setArPaymentTransmissionId( $intArPaymentTransmissionId ) {
		$this->set( 'm_intArPaymentTransmissionId', CStrings::strToIntDef( $intArPaymentTransmissionId, NULL, false ) );
	}

	public function getArPaymentTransmissionId() {
		return $this->m_intArPaymentTransmissionId;
	}

	public function sqlArPaymentTransmissionId() {
		return ( true == isset( $this->m_intArPaymentTransmissionId ) ) ? ( string ) $this->m_intArPaymentTransmissionId : 'NULL';
	}

	public function setArPaymentExportId( $intArPaymentExportId ) {
		$this->set( 'm_intArPaymentExportId', CStrings::strToIntDef( $intArPaymentExportId, NULL, false ) );
	}

	public function getArPaymentExportId() {
		return $this->m_intArPaymentExportId;
	}

	public function sqlArPaymentExportId() {
		return ( true == isset( $this->m_intArPaymentExportId ) ) ? ( string ) $this->m_intArPaymentExportId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setPsProductOptionId( $intPsProductOptionId ) {
		$this->set( 'm_intPsProductOptionId', CStrings::strToIntDef( $intPsProductOptionId, NULL, false ) );
	}

	public function getPsProductOptionId() {
		return $this->m_intPsProductOptionId;
	}

	public function sqlPsProductOptionId() {
		return ( true == isset( $this->m_intPsProductOptionId ) ) ? ( string ) $this->m_intPsProductOptionId : 'NULL';
	}

	public function setAccountVerificationLogId( $intAccountVerificationLogId ) {
		$this->set( 'm_intAccountVerificationLogId', CStrings::strToIntDef( $intAccountVerificationLogId, NULL, false ) );
	}

	public function getAccountVerificationLogId() {
		return $this->m_intAccountVerificationLogId;
	}

	public function sqlAccountVerificationLogId() {
		return ( true == isset( $this->m_intAccountVerificationLogId ) ) ? ( string ) $this->m_intAccountVerificationLogId : 'NULL';
	}

	public function setCompanyCharityId( $intCompanyCharityId ) {
		$this->set( 'm_intCompanyCharityId', CStrings::strToIntDef( $intCompanyCharityId, NULL, false ) );
	}

	public function getCompanyCharityId() {
		return $this->m_intCompanyCharityId;
	}

	public function sqlCompanyCharityId() {
		return ( true == isset( $this->m_intCompanyCharityId ) ) ? ( string ) $this->m_intCompanyCharityId : 'NULL';
	}

	public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$this->set( 'm_intLeaseStatusTypeId', CStrings::strToIntDef( $intLeaseStatusTypeId, NULL, false ) );
	}

	public function getLeaseStatusTypeId() {
		return $this->m_intLeaseStatusTypeId;
	}

	public function sqlLeaseStatusTypeId() {
		return ( true == isset( $this->m_intLeaseStatusTypeId ) ) ? ( string ) $this->m_intLeaseStatusTypeId : 'NULL';
	}

	public function setAllocationTypeId( $intAllocationTypeId ) {
		$this->set( 'm_intAllocationTypeId', CStrings::strToIntDef( $intAllocationTypeId, NULL, false ) );
	}

	public function getAllocationTypeId() {
		return $this->m_intAllocationTypeId;
	}

	public function sqlAllocationTypeId() {
		return ( true == isset( $this->m_intAllocationTypeId ) ) ? ( string ) $this->m_intAllocationTypeId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setReturnRemotePrimaryKey( $strReturnRemotePrimaryKey ) {
		$this->set( 'm_strReturnRemotePrimaryKey', CStrings::strTrimDef( $strReturnRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getReturnRemotePrimaryKey() {
		return $this->m_strReturnRemotePrimaryKey;
	}

	public function sqlReturnRemotePrimaryKey() {
		return ( true == isset( $this->m_strReturnRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strReturnRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setPaymentDatetime( $strPaymentDatetime ) {
		$this->set( 'm_strPaymentDatetime', CStrings::strTrimDef( $strPaymentDatetime, -1, NULL, true ) );
	}

	public function getPaymentDatetime() {
		return $this->m_strPaymentDatetime;
	}

	public function sqlPaymentDatetime() {
		return ( true == isset( $this->m_strPaymentDatetime ) ) ? '\'' . $this->m_strPaymentDatetime . '\'' : 'NOW()';
	}

	public function setReceiptDatetime( $strReceiptDatetime ) {
		$this->set( 'm_strReceiptDatetime', CStrings::strTrimDef( $strReceiptDatetime, -1, NULL, true ) );
	}

	public function getReceiptDatetime() {
		return $this->m_strReceiptDatetime;
	}

	public function sqlReceiptDatetime() {
		return ( true == isset( $this->m_strReceiptDatetime ) ) ? '\'' . $this->m_strReceiptDatetime . '\'' : 'NOW()';
	}

	public function setPaymentAmount( $fltPaymentAmount ) {
		$this->set( 'm_fltPaymentAmount', CStrings::strToFloatDef( $fltPaymentAmount, NULL, false, 2 ) );
	}

	public function getPaymentAmount() {
		return $this->m_fltPaymentAmount;
	}

	public function sqlPaymentAmount() {
		return ( true == isset( $this->m_fltPaymentAmount ) ) ? ( string ) $this->m_fltPaymentAmount : 'NULL';
	}

	public function setConvenienceFeeAmount( $fltConvenienceFeeAmount ) {
		$this->set( 'm_fltConvenienceFeeAmount', CStrings::strToFloatDef( $fltConvenienceFeeAmount, NULL, false, 2 ) );
	}

	public function getConvenienceFeeAmount() {
		return $this->m_fltConvenienceFeeAmount;
	}

	public function sqlConvenienceFeeAmount() {
		return ( true == isset( $this->m_fltConvenienceFeeAmount ) ) ? ( string ) $this->m_fltConvenienceFeeAmount : 'NULL';
	}

	public function setDonationAmount( $fltDonationAmount ) {
		$this->set( 'm_fltDonationAmount', CStrings::strToFloatDef( $fltDonationAmount, NULL, false, 2 ) );
	}

	public function getDonationAmount() {
		return $this->m_fltDonationAmount;
	}

	public function sqlDonationAmount() {
		return ( true == isset( $this->m_fltDonationAmount ) ) ? ( string ) $this->m_fltDonationAmount : '0';
	}

	public function setTotalAmount( $fltTotalAmount ) {
		$this->set( 'm_fltTotalAmount', CStrings::strToFloatDef( $fltTotalAmount, NULL, false, 2 ) );
	}

	public function getTotalAmount() {
		return $this->m_fltTotalAmount;
	}

	public function sqlTotalAmount() {
		return ( true == isset( $this->m_fltTotalAmount ) ) ? ( string ) $this->m_fltTotalAmount : 'NULL';
	}

	public function setPaymentCost( $fltPaymentCost ) {
		$this->set( 'm_fltPaymentCost', CStrings::strToFloatDef( $fltPaymentCost, NULL, false, 2 ) );
	}

	public function getPaymentCost() {
		return $this->m_fltPaymentCost;
	}

	public function sqlPaymentCost() {
		return ( true == isset( $this->m_fltPaymentCost ) ) ? ( string ) $this->m_fltPaymentCost : 'NULL';
	}

	public function setCompanyChargeAmount( $fltCompanyChargeAmount ) {
		$this->set( 'm_fltCompanyChargeAmount', CStrings::strToFloatDef( $fltCompanyChargeAmount, NULL, false, 2 ) );
	}

	public function getCompanyChargeAmount() {
		return $this->m_fltCompanyChargeAmount;
	}

	public function sqlCompanyChargeAmount() {
		return ( true == isset( $this->m_fltCompanyChargeAmount ) ) ? ( string ) $this->m_fltCompanyChargeAmount : 'NULL';
	}

	public function setIncentiveCompanyChargeAmount( $fltIncentiveCompanyChargeAmount ) {
		$this->set( 'm_fltIncentiveCompanyChargeAmount', CStrings::strToFloatDef( $fltIncentiveCompanyChargeAmount, NULL, false, 2 ) );
	}

	public function getIncentiveCompanyChargeAmount() {
		return $this->m_fltIncentiveCompanyChargeAmount;
	}

	public function sqlIncentiveCompanyChargeAmount() {
		return ( true == isset( $this->m_fltIncentiveCompanyChargeAmount ) ) ? ( string ) $this->m_fltIncentiveCompanyChargeAmount : 'NULL';
	}

	public function setSecureReferenceNumber( $intSecureReferenceNumber ) {
		$this->set( 'm_intSecureReferenceNumber', CStrings::strToIntDef( $intSecureReferenceNumber, NULL, false ) );
	}

	public function getSecureReferenceNumber() {
		return $this->m_intSecureReferenceNumber;
	}

	public function sqlSecureReferenceNumber() {
		return ( true == isset( $this->m_intSecureReferenceNumber ) ) ? ( string ) $this->m_intSecureReferenceNumber : 'NULL';
	}

	public function setRemotePaymentNumber( $strRemotePaymentNumber ) {
		$this->set( 'm_strRemotePaymentNumber', CStrings::strTrimDef( $strRemotePaymentNumber, 64, NULL, true ) );
	}

	public function getRemotePaymentNumber() {
		return $this->m_strRemotePaymentNumber;
	}

	public function sqlRemotePaymentNumber() {
		return ( true == isset( $this->m_strRemotePaymentNumber ) ) ? '\'' . addslashes( $this->m_strRemotePaymentNumber ) . '\'' : 'NULL';
	}

	public function setPaymentMemo( $strPaymentMemo ) {
		$this->set( 'm_strPaymentMemo', CStrings::strTrimDef( $strPaymentMemo, 2000, NULL, true ) );
	}

	public function getPaymentMemo() {
		return $this->m_strPaymentMemo;
	}

	public function sqlPaymentMemo() {
		return ( true == isset( $this->m_strPaymentMemo ) ) ? '\'' . addslashes( $this->m_strPaymentMemo ) . '\'' : 'NULL';
	}

	public function setBilltoIpAddress( $strBilltoIpAddress ) {
		$this->set( 'm_strBilltoIpAddress', CStrings::strTrimDef( $strBilltoIpAddress, 23, NULL, true ) );
	}

	public function getBilltoIpAddress() {
		return $this->m_strBilltoIpAddress;
	}

	public function sqlBilltoIpAddress() {
		return ( true == isset( $this->m_strBilltoIpAddress ) ) ? '\'' . addslashes( $this->m_strBilltoIpAddress ) . '\'' : 'NULL';
	}

	public function setBilltoIpAddressEncrypted( $strBilltoIpAddressEncrypted ) {
		$this->set( 'm_strBilltoIpAddressEncrypted', CStrings::strTrimDef( $strBilltoIpAddressEncrypted, 240, NULL, true ) );
	}

	public function getBilltoIpAddressEncrypted() {
		return $this->m_strBilltoIpAddressEncrypted;
	}

	public function sqlBilltoIpAddressEncrypted() {
		return ( true == isset( $this->m_strBilltoIpAddressEncrypted ) ) ? '\'' . addslashes( $this->m_strBilltoIpAddressEncrypted ) . '\'' : 'NULL';
	}

	public function setBilltoAccountNumber( $strBilltoAccountNumber ) {
		$this->set( 'm_strBilltoAccountNumber', CStrings::strTrimDef( $strBilltoAccountNumber, 50, NULL, true ) );
	}

	public function getBilltoAccountNumber() {
		return $this->m_strBilltoAccountNumber;
	}

	public function sqlBilltoAccountNumber() {
		return ( true == isset( $this->m_strBilltoAccountNumber ) ) ? '\'' . addslashes( $this->m_strBilltoAccountNumber ) . '\'' : 'NULL';
	}

	public function setBilltoCompanyName( $strBilltoCompanyName ) {
		$this->set( 'm_strBilltoCompanyName', CStrings::strTrimDef( $strBilltoCompanyName, 100, NULL, true ) );
	}

	public function getBilltoCompanyName() {
		return $this->m_strBilltoCompanyName;
	}

	public function sqlBilltoCompanyName() {
		return ( true == isset( $this->m_strBilltoCompanyName ) ) ? '\'' . addslashes( $this->m_strBilltoCompanyName ) . '\'' : 'NULL';
	}

	public function setBilltoUnitNumber( $strBilltoUnitNumber ) {
		$this->set( 'm_strBilltoUnitNumber', CStrings::strTrimDef( $strBilltoUnitNumber, 50, NULL, true ) );
	}

	public function getBilltoUnitNumber() {
		return $this->m_strBilltoUnitNumber;
	}

	public function sqlBilltoUnitNumber() {
		return ( true == isset( $this->m_strBilltoUnitNumber ) ) ? '\'' . addslashes( $this->m_strBilltoUnitNumber ) . '\'' : 'NULL';
	}

	public function setBilltoNameFirst( $strBilltoNameFirst ) {
		$this->set( 'm_strBilltoNameFirst', CStrings::strTrimDef( $strBilltoNameFirst, 50, NULL, true ) );
	}

	public function getBilltoNameFirst() {
		return $this->m_strBilltoNameFirst;
	}

	public function sqlBilltoNameFirst() {
		return ( true == isset( $this->m_strBilltoNameFirst ) ) ? '\'' . addslashes( $this->m_strBilltoNameFirst ) . '\'' : 'NULL';
	}

	public function setBilltoNameFirstEncrypted( $strBilltoNameFirstEncrypted ) {
		$this->set( 'm_strBilltoNameFirstEncrypted', CStrings::strTrimDef( $strBilltoNameFirstEncrypted, 240, NULL, true ) );
	}

	public function getBilltoNameFirstEncrypted() {
		return $this->m_strBilltoNameFirstEncrypted;
	}

	public function sqlBilltoNameFirstEncrypted() {
		return ( true == isset( $this->m_strBilltoNameFirstEncrypted ) ) ? '\'' . addslashes( $this->m_strBilltoNameFirstEncrypted ) . '\'' : 'NULL';
	}

	public function setBilltoNameMiddle( $strBilltoNameMiddle ) {
		$this->set( 'm_strBilltoNameMiddle', CStrings::strTrimDef( $strBilltoNameMiddle, 50, NULL, true ) );
	}

	public function getBilltoNameMiddle() {
		return $this->m_strBilltoNameMiddle;
	}

	public function sqlBilltoNameMiddle() {
		return ( true == isset( $this->m_strBilltoNameMiddle ) ) ? '\'' . addslashes( $this->m_strBilltoNameMiddle ) . '\'' : 'NULL';
	}

	public function setBilltoNameMiddleEncrypted( $strBilltoNameMiddleEncrypted ) {
		$this->set( 'm_strBilltoNameMiddleEncrypted', CStrings::strTrimDef( $strBilltoNameMiddleEncrypted, 240, NULL, true ) );
	}

	public function getBilltoNameMiddleEncrypted() {
		return $this->m_strBilltoNameMiddleEncrypted;
	}

	public function sqlBilltoNameMiddleEncrypted() {
		return ( true == isset( $this->m_strBilltoNameMiddleEncrypted ) ) ? '\'' . addslashes( $this->m_strBilltoNameMiddleEncrypted ) . '\'' : 'NULL';
	}

	public function setBilltoNameLast( $strBilltoNameLast ) {
		$this->set( 'm_strBilltoNameLast', CStrings::strTrimDef( $strBilltoNameLast, 50, NULL, true ) );
	}

	public function getBilltoNameLast() {
		return $this->m_strBilltoNameLast;
	}

	public function sqlBilltoNameLast() {
		return ( true == isset( $this->m_strBilltoNameLast ) ) ? '\'' . addslashes( $this->m_strBilltoNameLast ) . '\'' : 'NULL';
	}

	public function setBilltoNameLastEncrypted( $strBilltoNameLastEncrypted ) {
		$this->set( 'm_strBilltoNameLastEncrypted', CStrings::strTrimDef( $strBilltoNameLastEncrypted, 240, NULL, true ) );
	}

	public function getBilltoNameLastEncrypted() {
		return $this->m_strBilltoNameLastEncrypted;
	}

	public function sqlBilltoNameLastEncrypted() {
		return ( true == isset( $this->m_strBilltoNameLastEncrypted ) ) ? '\'' . addslashes( $this->m_strBilltoNameLastEncrypted ) . '\'' : 'NULL';
	}

	public function setBilltoStreetLine1( $strBilltoStreetLine1 ) {
		$this->set( 'm_strBilltoStreetLine1', CStrings::strTrimDef( $strBilltoStreetLine1, 100, NULL, true ) );
	}

	public function getBilltoStreetLine1() {
		return $this->m_strBilltoStreetLine1;
	}

	public function sqlBilltoStreetLine1() {
		return ( true == isset( $this->m_strBilltoStreetLine1 ) ) ? '\'' . addslashes( $this->m_strBilltoStreetLine1 ) . '\'' : 'NULL';
	}

	public function setBilltoStreetLine2( $strBilltoStreetLine2 ) {
		$this->set( 'm_strBilltoStreetLine2', CStrings::strTrimDef( $strBilltoStreetLine2, 100, NULL, true ) );
	}

	public function getBilltoStreetLine2() {
		return $this->m_strBilltoStreetLine2;
	}

	public function sqlBilltoStreetLine2() {
		return ( true == isset( $this->m_strBilltoStreetLine2 ) ) ? '\'' . addslashes( $this->m_strBilltoStreetLine2 ) . '\'' : 'NULL';
	}

	public function setBilltoStreetLine3( $strBilltoStreetLine3 ) {
		$this->set( 'm_strBilltoStreetLine3', CStrings::strTrimDef( $strBilltoStreetLine3, 100, NULL, true ) );
	}

	public function getBilltoStreetLine3() {
		return $this->m_strBilltoStreetLine3;
	}

	public function sqlBilltoStreetLine3() {
		return ( true == isset( $this->m_strBilltoStreetLine3 ) ) ? '\'' . addslashes( $this->m_strBilltoStreetLine3 ) . '\'' : 'NULL';
	}

	public function setBilltoCity( $strBilltoCity ) {
		$this->set( 'm_strBilltoCity', CStrings::strTrimDef( $strBilltoCity, 50, NULL, true ) );
	}

	public function getBilltoCity() {
		return $this->m_strBilltoCity;
	}

	public function sqlBilltoCity() {
		return ( true == isset( $this->m_strBilltoCity ) ) ? '\'' . addslashes( $this->m_strBilltoCity ) . '\'' : 'NULL';
	}

	public function setBilltoStateCode( $strBilltoStateCode ) {
		$this->set( 'm_strBilltoStateCode', CStrings::strTrimDef( $strBilltoStateCode, 2, NULL, true ) );
	}

	public function getBilltoStateCode() {
		return $this->m_strBilltoStateCode;
	}

	public function sqlBilltoStateCode() {
		return ( true == isset( $this->m_strBilltoStateCode ) ) ? '\'' . addslashes( $this->m_strBilltoStateCode ) . '\'' : 'NULL';
	}

	public function setBilltoProvince( $strBilltoProvince ) {
		$this->set( 'm_strBilltoProvince', CStrings::strTrimDef( $strBilltoProvince, 50, NULL, true ) );
	}

	public function getBilltoProvince() {
		return $this->m_strBilltoProvince;
	}

	public function sqlBilltoProvince() {
		return ( true == isset( $this->m_strBilltoProvince ) ) ? '\'' . addslashes( $this->m_strBilltoProvince ) . '\'' : 'NULL';
	}

	public function setBilltoPostalCode( $strBilltoPostalCode ) {
		$this->set( 'm_strBilltoPostalCode', CStrings::strTrimDef( $strBilltoPostalCode, 20, NULL, true ) );
	}

	public function getBilltoPostalCode() {
		return $this->m_strBilltoPostalCode;
	}

	public function sqlBilltoPostalCode() {
		return ( true == isset( $this->m_strBilltoPostalCode ) ) ? '\'' . addslashes( $this->m_strBilltoPostalCode ) . '\'' : 'NULL';
	}

	public function setBilltoCountryCode( $strBilltoCountryCode ) {
		$this->set( 'm_strBilltoCountryCode', CStrings::strTrimDef( $strBilltoCountryCode, 2, NULL, true ) );
	}

	public function getBilltoCountryCode() {
		return $this->m_strBilltoCountryCode;
	}

	public function sqlBilltoCountryCode() {
		return ( true == isset( $this->m_strBilltoCountryCode ) ) ? '\'' . addslashes( $this->m_strBilltoCountryCode ) . '\'' : 'NULL';
	}

	public function setBilltoPhoneNumber( $strBilltoPhoneNumber ) {
		$this->set( 'm_strBilltoPhoneNumber', CStrings::strTrimDef( $strBilltoPhoneNumber, 30, NULL, true ) );
	}

	public function getBilltoPhoneNumber() {
		return $this->m_strBilltoPhoneNumber;
	}

	public function sqlBilltoPhoneNumber() {
		return ( true == isset( $this->m_strBilltoPhoneNumber ) ) ? '\'' . addslashes( $this->m_strBilltoPhoneNumber ) . '\'' : 'NULL';
	}

	public function setBilltoPhoneNumberEncrypted( $strBilltoPhoneNumberEncrypted ) {
		$this->set( 'm_strBilltoPhoneNumberEncrypted', CStrings::strTrimDef( $strBilltoPhoneNumberEncrypted, 240, NULL, true ) );
	}

	public function getBilltoPhoneNumberEncrypted() {
		return $this->m_strBilltoPhoneNumberEncrypted;
	}

	public function sqlBilltoPhoneNumberEncrypted() {
		return ( true == isset( $this->m_strBilltoPhoneNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strBilltoPhoneNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setBilltoEmailAddress( $strBilltoEmailAddress ) {
		$this->set( 'm_strBilltoEmailAddress', CStrings::strTrimDef( $strBilltoEmailAddress, 240, NULL, true ) );
	}

	public function getBilltoEmailAddress() {
		return $this->m_strBilltoEmailAddress;
	}

	public function sqlBilltoEmailAddress() {
		return ( true == isset( $this->m_strBilltoEmailAddress ) ) ? '\'' . addslashes( $this->m_strBilltoEmailAddress ) . '\'' : 'NULL';
	}

	public function setBilltoEmailAddressEncrypted( $strBilltoEmailAddressEncrypted ) {
		$this->set( 'm_strBilltoEmailAddressEncrypted', CStrings::strTrimDef( $strBilltoEmailAddressEncrypted, 550, NULL, true ) );
	}

	public function getBilltoEmailAddressEncrypted() {
		return $this->m_strBilltoEmailAddressEncrypted;
	}

	public function sqlBilltoEmailAddressEncrypted() {
		return ( true == isset( $this->m_strBilltoEmailAddressEncrypted ) ) ? '\'' . addslashes( $this->m_strBilltoEmailAddressEncrypted ) . '\'' : 'NULL';
	}

	public function setCarLarAmount( $fltCarLarAmount ) {
		$this->set( 'm_fltCarLarAmount', CStrings::strToFloatDef( $fltCarLarAmount, NULL, false, 2 ) );
	}

	public function getCarLarAmount() {
		return $this->m_fltCarLarAmount;
	}

	public function sqlCarLarAmount() {
		return ( true == isset( $this->m_fltCarLarAmount ) ) ? ( string ) $this->m_fltCarLarAmount : 'NULL';
	}

	public function setCarLarConfidence( $intCarLarConfidence ) {
		$this->set( 'm_intCarLarConfidence', CStrings::strToIntDef( $intCarLarConfidence, NULL, false ) );
	}

	public function getCarLarConfidence() {
		return $this->m_intCarLarConfidence;
	}

	public function sqlCarLarConfidence() {
		return ( true == isset( $this->m_intCarLarConfidence ) ) ? ( string ) $this->m_intCarLarConfidence : 'NULL';
	}

	public function setCarLarIsMoneyOrder( $intCarLarIsMoneyOrder ) {
		$this->set( 'm_intCarLarIsMoneyOrder', CStrings::strToIntDef( $intCarLarIsMoneyOrder, NULL, false ) );
	}

	public function getCarLarIsMoneyOrder() {
		return $this->m_intCarLarIsMoneyOrder;
	}

	public function sqlCarLarIsMoneyOrder() {
		return ( true == isset( $this->m_intCarLarIsMoneyOrder ) ) ? ( string ) $this->m_intCarLarIsMoneyOrder : 'NULL';
	}

	public function setCarLarIsPerformed( $intCarLarIsPerformed ) {
		$this->set( 'm_intCarLarIsPerformed', CStrings::strToIntDef( $intCarLarIsPerformed, NULL, false ) );
	}

	public function getCarLarIsPerformed() {
		return $this->m_intCarLarIsPerformed;
	}

	public function sqlCarLarIsPerformed() {
		return ( true == isset( $this->m_intCarLarIsPerformed ) ) ? ( string ) $this->m_intCarLarIsPerformed : 'NULL';
	}

	public function setPhoneAuthRequired( $intPhoneAuthRequired ) {
		$this->set( 'm_intPhoneAuthRequired', CStrings::strToIntDef( $intPhoneAuthRequired, NULL, false ) );
	}

	public function getPhoneAuthRequired() {
		return $this->m_intPhoneAuthRequired;
	}

	public function sqlPhoneAuthRequired() {
		return ( true == isset( $this->m_intPhoneAuthRequired ) ) ? ( string ) $this->m_intPhoneAuthRequired : 'NULL';
	}

	public function setPhoneAuthBy( $intPhoneAuthBy ) {
		$this->set( 'm_intPhoneAuthBy', CStrings::strToIntDef( $intPhoneAuthBy, NULL, false ) );
	}

	public function getPhoneAuthBy() {
		return $this->m_intPhoneAuthBy;
	}

	public function sqlPhoneAuthBy() {
		return ( true == isset( $this->m_intPhoneAuthBy ) ) ? ( string ) $this->m_intPhoneAuthBy : 'NULL';
	}

	public function setPhoneAuthOn( $strPhoneAuthOn ) {
		$this->set( 'm_strPhoneAuthOn', CStrings::strTrimDef( $strPhoneAuthOn, -1, NULL, true ) );
	}

	public function getPhoneAuthOn() {
		return $this->m_strPhoneAuthOn;
	}

	public function sqlPhoneAuthOn() {
		return ( true == isset( $this->m_strPhoneAuthOn ) ) ? '\'' . $this->m_strPhoneAuthOn . '\'' : 'NULL';
	}

	public function setRequiresApproval( $intRequiresApproval ) {
		$this->set( 'm_intRequiresApproval', CStrings::strToIntDef( $intRequiresApproval, NULL, false ) );
	}

	public function getRequiresApproval() {
		return $this->m_intRequiresApproval;
	}

	public function sqlRequiresApproval() {
		return ( true == isset( $this->m_intRequiresApproval ) ) ? ( string ) $this->m_intRequiresApproval : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setReturnExportedOn( $strReturnExportedOn ) {
		$this->set( 'm_strReturnExportedOn', CStrings::strTrimDef( $strReturnExportedOn, -1, NULL, true ) );
	}

	public function getReturnExportedOn() {
		return $this->m_strReturnExportedOn;
	}

	public function sqlReturnExportedOn() {
		return ( true == isset( $this->m_strReturnExportedOn ) ) ? '\'' . $this->m_strReturnExportedOn . '\'' : 'NULL';
	}

	public function setReturnBatchedOn( $strReturnBatchedOn ) {
		$this->set( 'm_strReturnBatchedOn', CStrings::strTrimDef( $strReturnBatchedOn, -1, NULL, true ) );
	}

	public function getReturnBatchedOn() {
		return $this->m_strReturnBatchedOn;
	}

	public function sqlReturnBatchedOn() {
		return ( true == isset( $this->m_strReturnBatchedOn ) ) ? '\'' . $this->m_strReturnBatchedOn . '\'' : 'NULL';
	}

	public function setFeePostedOn( $strFeePostedOn ) {
		$this->set( 'm_strFeePostedOn', CStrings::strTrimDef( $strFeePostedOn, -1, NULL, true ) );
	}

	public function getFeePostedOn() {
		return $this->m_strFeePostedOn;
	}

	public function sqlFeePostedOn() {
		return ( true == isset( $this->m_strFeePostedOn ) ) ? '\'' . $this->m_strFeePostedOn . '\'' : 'NULL';
	}

	public function setReturnFeePostedOn( $strReturnFeePostedOn ) {
		$this->set( 'm_strReturnFeePostedOn', CStrings::strTrimDef( $strReturnFeePostedOn, -1, NULL, true ) );
	}

	public function getReturnFeePostedOn() {
		return $this->m_strReturnFeePostedOn;
	}

	public function sqlReturnFeePostedOn() {
		return ( true == isset( $this->m_strReturnFeePostedOn ) ) ? '\'' . $this->m_strReturnFeePostedOn . '\'' : 'NULL';
	}

	public function setReverseFeePostedOn( $strReverseFeePostedOn ) {
		$this->set( 'm_strReverseFeePostedOn', CStrings::strTrimDef( $strReverseFeePostedOn, -1, NULL, true ) );
	}

	public function getReverseFeePostedOn() {
		return $this->m_strReverseFeePostedOn;
	}

	public function sqlReverseFeePostedOn() {
		return ( true == isset( $this->m_strReverseFeePostedOn ) ) ? '\'' . $this->m_strReverseFeePostedOn . '\'' : 'NULL';
	}

	public function setIsApplicationPayment( $intIsApplicationPayment ) {
		$this->set( 'm_intIsApplicationPayment', CStrings::strToIntDef( $intIsApplicationPayment, NULL, false ) );
	}

	public function getIsApplicationPayment() {
		return $this->m_intIsApplicationPayment;
	}

	public function sqlIsApplicationPayment() {
		return ( true == isset( $this->m_intIsApplicationPayment ) ) ? ( string ) $this->m_intIsApplicationPayment : 'NULL';
	}

	public function setAllocateArTransactionIds( $strAllocateArTransactionIds ) {
		$this->set( 'm_strAllocateArTransactionIds', CStrings::strTrimDef( $strAllocateArTransactionIds, -1, NULL, true ) );
	}

	public function getAllocateArTransactionIds() {
		return $this->m_strAllocateArTransactionIds;
	}

	public function sqlAllocateArTransactionIds() {
		return ( true == isset( $this->m_strAllocateArTransactionIds ) ) ? '\'' . addslashes( $this->m_strAllocateArTransactionIds ) . '\'' : 'NULL';
	}

	public function setCcCardNumberEncrypted( $strCcCardNumberEncrypted ) {
		$this->set( 'm_strCcCardNumberEncrypted', CStrings::strTrimDef( $strCcCardNumberEncrypted, 240, NULL, true ) );
	}

	public function getCcCardNumberEncrypted() {
		return $this->m_strCcCardNumberEncrypted;
	}

	public function sqlCcCardNumberEncrypted() {
		return ( true == isset( $this->m_strCcCardNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strCcCardNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setCcExpDateMonth( $strCcExpDateMonth ) {
		$this->set( 'm_strCcExpDateMonth', CStrings::strTrimDef( $strCcExpDateMonth, 2, NULL, true ) );
	}

	public function getCcExpDateMonth() {
		return $this->m_strCcExpDateMonth;
	}

	public function sqlCcExpDateMonth() {
		return ( true == isset( $this->m_strCcExpDateMonth ) ) ? '\'' . addslashes( $this->m_strCcExpDateMonth ) . '\'' : 'NULL';
	}

	public function setCcExpDateYear( $strCcExpDateYear ) {
		$this->set( 'm_strCcExpDateYear', CStrings::strTrimDef( $strCcExpDateYear, 4, NULL, true ) );
	}

	public function getCcExpDateYear() {
		return $this->m_strCcExpDateYear;
	}

	public function sqlCcExpDateYear() {
		return ( true == isset( $this->m_strCcExpDateYear ) ) ? '\'' . addslashes( $this->m_strCcExpDateYear ) . '\'' : 'NULL';
	}

	public function setCcNameOnCard( $strCcNameOnCard ) {
		$this->set( 'm_strCcNameOnCard', CStrings::strTrimDef( $strCcNameOnCard, 50, NULL, true ) );
	}

	public function getCcNameOnCard() {
		return $this->m_strCcNameOnCard;
	}

	public function sqlCcNameOnCard() {
		return ( true == isset( $this->m_strCcNameOnCard ) ) ? '\'' . addslashes( $this->m_strCcNameOnCard ) . '\'' : 'NULL';
	}

	public function setCcBinNumber( $strCcBinNumber ) {
		$this->set( 'm_strCcBinNumber', CStrings::strTrimDef( $strCcBinNumber, 6, NULL, true ) );
	}

	public function getCcBinNumber() {
		return $this->m_strCcBinNumber;
	}

	public function sqlCcBinNumber() {
		return ( true == isset( $this->m_strCcBinNumber ) ) ? '\'' . addslashes( $this->m_strCcBinNumber ) . '\'' : 'NULL';
	}

	public function setCheckNumber( $strCheckNumber ) {
		$this->set( 'm_strCheckNumber', CStrings::strTrimDef( $strCheckNumber, 40, NULL, true ) );
	}

	public function getCheckNumber() {
		return $this->m_strCheckNumber;
	}

	public function sqlCheckNumber() {
		return ( true == isset( $this->m_strCheckNumber ) ) ? '\'' . addslashes( $this->m_strCheckNumber ) . '\'' : 'NULL';
	}

	public function setCheckDate( $strCheckDate ) {
		$this->set( 'm_strCheckDate', CStrings::strTrimDef( $strCheckDate, -1, NULL, true ) );
	}

	public function getCheckDate() {
		return $this->m_strCheckDate;
	}

	public function sqlCheckDate() {
		return ( true == isset( $this->m_strCheckDate ) ) ? '\'' . $this->m_strCheckDate . '\'' : 'NULL';
	}

	public function setCheckPayableTo( $strCheckPayableTo ) {
		$this->set( 'm_strCheckPayableTo', CStrings::strTrimDef( $strCheckPayableTo, 240, NULL, true ) );
	}

	public function getCheckPayableTo() {
		return $this->m_strCheckPayableTo;
	}

	public function sqlCheckPayableTo() {
		return ( true == isset( $this->m_strCheckPayableTo ) ) ? '\'' . addslashes( $this->m_strCheckPayableTo ) . '\'' : 'NULL';
	}

	public function setCheckBankName( $strCheckBankName ) {
		$this->set( 'm_strCheckBankName', CStrings::strTrimDef( $strCheckBankName, 100, NULL, true ) );
	}

	public function getCheckBankName() {
		return $this->m_strCheckBankName;
	}

	public function sqlCheckBankName() {
		return ( true == isset( $this->m_strCheckBankName ) ) ? '\'' . addslashes( $this->m_strCheckBankName ) . '\'' : 'NULL';
	}

	public function setCheckNameOnAccount( $strCheckNameOnAccount ) {
		$this->set( 'm_strCheckNameOnAccount', CStrings::strTrimDef( $strCheckNameOnAccount, 50, NULL, true ) );
	}

	public function getCheckNameOnAccount() {
		return $this->m_strCheckNameOnAccount;
	}

	public function sqlCheckNameOnAccount() {
		return ( true == isset( $this->m_strCheckNameOnAccount ) ) ? '\'' . addslashes( $this->m_strCheckNameOnAccount ) . '\'' : 'NULL';
	}

	public function setCheckAccountTypeId( $intCheckAccountTypeId ) {
		$this->set( 'm_intCheckAccountTypeId', CStrings::strToIntDef( $intCheckAccountTypeId, NULL, false ) );
	}

	public function getCheckAccountTypeId() {
		return $this->m_intCheckAccountTypeId;
	}

	public function sqlCheckAccountTypeId() {
		return ( true == isset( $this->m_intCheckAccountTypeId ) ) ? ( string ) $this->m_intCheckAccountTypeId : 'NULL';
	}

	public function setCheckRoutingNumber( $strCheckRoutingNumber ) {
		$this->set( 'm_strCheckRoutingNumber', CStrings::strTrimDef( $strCheckRoutingNumber, 240, NULL, true ) );
	}

	public function getCheckRoutingNumber() {
		return $this->m_strCheckRoutingNumber;
	}

	public function sqlCheckRoutingNumber() {
		return ( true == isset( $this->m_strCheckRoutingNumber ) ) ? '\'' . addslashes( $this->m_strCheckRoutingNumber ) . '\'' : 'NULL';
	}

	public function setCheckAccountNumberEncrypted( $strCheckAccountNumberEncrypted ) {
		$this->set( 'm_strCheckAccountNumberEncrypted', CStrings::strTrimDef( $strCheckAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getCheckAccountNumberEncrypted() {
		return $this->m_strCheckAccountNumberEncrypted;
	}

	public function sqlCheckAccountNumberEncrypted() {
		return ( true == isset( $this->m_strCheckAccountNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strCheckAccountNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setCheckAccountNumberLastfourEncrypted( $strCheckAccountNumberLastfourEncrypted ) {
		$this->set( 'm_strCheckAccountNumberLastfourEncrypted', CStrings::strTrimDef( $strCheckAccountNumberLastfourEncrypted, 240, NULL, true ) );
	}

	public function getCheckAccountNumberLastfourEncrypted() {
		return $this->m_strCheckAccountNumberLastfourEncrypted;
	}

	public function sqlCheckAccountNumberLastfourEncrypted() {
		return ( true == isset( $this->m_strCheckAccountNumberLastfourEncrypted ) ) ? '\'' . addslashes( $this->m_strCheckAccountNumberLastfourEncrypted ) . '\'' : 'NULL';
	}

	public function setCheckAuxillaryOnUs( $strCheckAuxillaryOnUs ) {
		$this->set( 'm_strCheckAuxillaryOnUs', CStrings::strTrimDef( $strCheckAuxillaryOnUs, 240, NULL, true ) );
	}

	public function getCheckAuxillaryOnUs() {
		return $this->m_strCheckAuxillaryOnUs;
	}

	public function sqlCheckAuxillaryOnUs() {
		return ( true == isset( $this->m_strCheckAuxillaryOnUs ) ) ? '\'' . addslashes( $this->m_strCheckAuxillaryOnUs ) . '\'' : 'NULL';
	}

	public function setCheckEpc( $strCheckEpc ) {
		$this->set( 'm_strCheckEpc', CStrings::strTrimDef( $strCheckEpc, 240, NULL, true ) );
	}

	public function getCheckEpc() {
		return $this->m_strCheckEpc;
	}

	public function sqlCheckEpc() {
		return ( true == isset( $this->m_strCheckEpc ) ) ? '\'' . addslashes( $this->m_strCheckEpc ) . '\'' : 'NULL';
	}

	public function setCheckIsMoneyOrder( $intCheckIsMoneyOrder ) {
		$this->set( 'm_intCheckIsMoneyOrder', CStrings::strToIntDef( $intCheckIsMoneyOrder, NULL, false ) );
	}

	public function getCheckIsMoneyOrder() {
		return $this->m_intCheckIsMoneyOrder;
	}

	public function sqlCheckIsMoneyOrder() {
		return ( true == isset( $this->m_intCheckIsMoneyOrder ) ) ? ( string ) $this->m_intCheckIsMoneyOrder : 'NULL';
	}

	public function setCheckIsConverted( $intCheckIsConverted ) {
		$this->set( 'm_intCheckIsConverted', CStrings::strToIntDef( $intCheckIsConverted, NULL, false ) );
	}

	public function getCheckIsConverted() {
		return $this->m_intCheckIsConverted;
	}

	public function sqlCheckIsConverted() {
		return ( true == isset( $this->m_intCheckIsConverted ) ) ? ( string ) $this->m_intCheckIsConverted : 'NULL';
	}

	public function setBlockAutoAssociation( $intBlockAutoAssociation ) {
		$this->set( 'm_intBlockAutoAssociation', CStrings::strToIntDef( $intBlockAutoAssociation, NULL, false ) );
	}

	public function getBlockAutoAssociation() {
		return $this->m_intBlockAutoAssociation;
	}

	public function sqlBlockAutoAssociation() {
		return ( true == isset( $this->m_intBlockAutoAssociation ) ) ? ( string ) $this->m_intBlockAutoAssociation : 'NULL';
	}

	public function setFedTrackingNumber( $strFedTrackingNumber ) {
		$this->set( 'm_strFedTrackingNumber', CStrings::strTrimDef( $strFedTrackingNumber, 64, NULL, true ) );
	}

	public function getFedTrackingNumber() {
		return $this->m_strFedTrackingNumber;
	}

	public function sqlFedTrackingNumber() {
		return ( true == isset( $this->m_strFedTrackingNumber ) ) ? '\'' . addslashes( $this->m_strFedTrackingNumber ) . '\'' : 'NULL';
	}

	public function setIsSplit( $intIsSplit ) {
		$this->set( 'm_intIsSplit', CStrings::strToIntDef( $intIsSplit, NULL, false ) );
	}

	public function getIsSplit() {
		return $this->m_intIsSplit;
	}

	public function sqlIsSplit() {
		return ( true == isset( $this->m_intIsSplit ) ) ? ( string ) $this->m_intIsSplit : '0';
	}

	public function setIsReversed( $intIsReversed ) {
		$this->set( 'm_intIsReversed', CStrings::strToIntDef( $intIsReversed, NULL, false ) );
	}

	public function getIsReversed() {
		return $this->m_intIsReversed;
	}

	public function sqlIsReversed() {
		return ( true == isset( $this->m_intIsReversed ) ) ? ( string ) $this->m_intIsReversed : '0';
	}

	public function setIsReconPrepped( $intIsReconPrepped ) {
		$this->set( 'm_intIsReconPrepped', CStrings::strToIntDef( $intIsReconPrepped, NULL, false ) );
	}

	public function getIsReconPrepped() {
		return $this->m_intIsReconPrepped;
	}

	public function sqlIsReconPrepped() {
		return ( true == isset( $this->m_intIsReconPrepped ) ) ? ( string ) $this->m_intIsReconPrepped : '0';
	}

	public function setIsDebitCard( $boolIsDebitCard ) {
		$this->set( 'm_boolIsDebitCard', CStrings::strToBool( $boolIsDebitCard ) );
	}

	public function getIsDebitCard() {
		return $this->m_boolIsDebitCard;
	}

	public function sqlIsDebitCard() {
		return ( true == isset( $this->m_boolIsDebitCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDebitCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCheckCard( $boolIsCheckCard ) {
		$this->set( 'm_boolIsCheckCard', CStrings::strToBool( $boolIsCheckCard ) );
	}

	public function getIsCheckCard() {
		return $this->m_boolIsCheckCard;
	}

	public function sqlIsCheckCard() {
		return ( true == isset( $this->m_boolIsCheckCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCheckCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsGiftCard( $boolIsGiftCard ) {
		$this->set( 'm_boolIsGiftCard', CStrings::strToBool( $boolIsGiftCard ) );
	}

	public function getIsGiftCard() {
		return $this->m_boolIsGiftCard;
	}

	public function sqlIsGiftCard() {
		return ( true == isset( $this->m_boolIsGiftCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsGiftCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPrepaidCard( $boolIsPrepaidCard ) {
		$this->set( 'm_boolIsPrepaidCard', CStrings::strToBool( $boolIsPrepaidCard ) );
	}

	public function getIsPrepaidCard() {
		return $this->m_boolIsPrepaidCard;
	}

	public function sqlIsPrepaidCard() {
		return ( true == isset( $this->m_boolIsPrepaidCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPrepaidCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCreditCard( $boolIsCreditCard ) {
		$this->set( 'm_boolIsCreditCard', CStrings::strToBool( $boolIsCreditCard ) );
	}

	public function getIsCreditCard() {
		return $this->m_boolIsCreditCard;
	}

	public function sqlIsCreditCard() {
		return ( true == isset( $this->m_boolIsCreditCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCreditCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsInternationalCard( $boolIsInternationalCard ) {
		$this->set( 'm_boolIsInternationalCard', CStrings::strToBool( $boolIsInternationalCard ) );
	}

	public function getIsInternationalCard() {
		return $this->m_boolIsInternationalCard;
	}

	public function sqlIsInternationalCard() {
		return ( true == isset( $this->m_boolIsInternationalCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInternationalCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCommercialCard( $boolIsCommercialCard ) {
		$this->set( 'm_boolIsCommercialCard', CStrings::strToBool( $boolIsCommercialCard ) );
	}

	public function getIsCommercialCard() {
		return $this->m_boolIsCommercialCard;
	}

	public function sqlIsCommercialCard() {
		return ( true == isset( $this->m_boolIsCommercialCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCommercialCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsRiskReview( $boolIsRiskReview ) {
		$this->set( 'm_boolIsRiskReview', CStrings::strToBool( $boolIsRiskReview ) );
	}

	public function getIsRiskReview() {
		return $this->m_boolIsRiskReview;
	}

	public function sqlIsRiskReview() {
		return ( true == isset( $this->m_boolIsRiskReview ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRiskReview ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setBlockAssociation( $intBlockAssociation ) {
		$this->set( 'm_intBlockAssociation', CStrings::strToIntDef( $intBlockAssociation, NULL, false ) );
	}

	public function getBlockAssociation() {
		return $this->m_intBlockAssociation;
	}

	public function sqlBlockAssociation() {
		return ( true == isset( $this->m_intBlockAssociation ) ) ? ( string ) $this->m_intBlockAssociation : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setLockSequence( $intLockSequence ) {
		$this->set( 'm_intLockSequence', CStrings::strToIntDef( $intLockSequence, NULL, false ) );
	}

	public function getLockSequence() {
		return $this->m_intLockSequence;
	}

	public function sqlLockSequence() {
		return ( true == isset( $this->m_intLockSequence ) ) ? ( string ) $this->m_intLockSequence : 'NULL';
	}

	public function setTermsAcceptedOn( $strTermsAcceptedOn ) {
		$this->set( 'm_strTermsAcceptedOn', CStrings::strTrimDef( $strTermsAcceptedOn, -1, NULL, true ) );
	}

	public function getTermsAcceptedOn() {
		return $this->m_strTermsAcceptedOn;
	}

	public function sqlTermsAcceptedOn() {
		return ( true == isset( $this->m_strTermsAcceptedOn ) ) ? '\'' . $this->m_strTermsAcceptedOn . '\'' : 'NULL';
	}

	public function setDistributionBlockedOn( $strDistributionBlockedOn ) {
		$this->set( 'm_strDistributionBlockedOn', CStrings::strTrimDef( $strDistributionBlockedOn, -1, NULL, true ) );
	}

	public function getDistributionBlockedOn() {
		return $this->m_strDistributionBlockedOn;
	}

	public function sqlDistributionBlockedOn() {
		return ( true == isset( $this->m_strDistributionBlockedOn ) ) ? '\'' . $this->m_strDistributionBlockedOn . '\'' : 'NULL';
	}

	public function setAutoCaptureOn( $strAutoCaptureOn ) {
		$this->set( 'm_strAutoCaptureOn', CStrings::strTrimDef( $strAutoCaptureOn, -1, NULL, true ) );
	}

	public function getAutoCaptureOn() {
		return $this->m_strAutoCaptureOn;
	}

	public function sqlAutoCaptureOn() {
		return ( true == isset( $this->m_strAutoCaptureOn ) ) ? '\'' . $this->m_strAutoCaptureOn . '\'' : 'NULL';
	}

	public function setDistributeOn( $strDistributeOn ) {
		$this->set( 'm_strDistributeOn', CStrings::strTrimDef( $strDistributeOn, -1, NULL, true ) );
	}

	public function getDistributeOn() {
		return $this->m_strDistributeOn;
	}

	public function sqlDistributeOn() {
		return ( true == isset( $this->m_strDistributeOn ) ) ? '\'' . $this->m_strDistributeOn . '\'' : 'NULL';
	}

	public function setDistributeReturnOn( $strDistributeReturnOn ) {
		$this->set( 'm_strDistributeReturnOn', CStrings::strTrimDef( $strDistributeReturnOn, -1, NULL, true ) );
	}

	public function getDistributeReturnOn() {
		return $this->m_strDistributeReturnOn;
	}

	public function sqlDistributeReturnOn() {
		return ( true == isset( $this->m_strDistributeReturnOn ) ) ? '\'' . $this->m_strDistributeReturnOn . '\'' : 'NULL';
	}

	public function setReturnedOn( $strReturnedOn ) {
		$this->set( 'm_strReturnedOn', CStrings::strTrimDef( $strReturnedOn, -1, NULL, true ) );
	}

	public function getReturnedOn() {
		return $this->m_strReturnedOn;
	}

	public function sqlReturnedOn() {
		return ( true == isset( $this->m_strReturnedOn ) ) ? '\'' . $this->m_strReturnedOn . '\'' : 'NULL';
	}

	public function setBatchedOn( $strBatchedOn ) {
		$this->set( 'm_strBatchedOn', CStrings::strTrimDef( $strBatchedOn, -1, NULL, true ) );
	}

	public function getBatchedOn() {
		return $this->m_strBatchedOn;
	}

	public function sqlBatchedOn() {
		return ( true == isset( $this->m_strBatchedOn ) ) ? '\'' . $this->m_strBatchedOn . '\'' : 'NULL';
	}

	public function setEftBatchedOn( $strEftBatchedOn ) {
		$this->set( 'm_strEftBatchedOn', CStrings::strTrimDef( $strEftBatchedOn, -1, NULL, true ) );
	}

	public function getEftBatchedOn() {
		return $this->m_strEftBatchedOn;
	}

	public function sqlEftBatchedOn() {
		return ( true == isset( $this->m_strEftBatchedOn ) ) ? '\'' . $this->m_strEftBatchedOn . '\'' : 'NULL';
	}

	public function setCapturedOn( $strCapturedOn ) {
		$this->set( 'm_strCapturedOn', CStrings::strTrimDef( $strCapturedOn, -1, NULL, true ) );
	}

	public function getCapturedOn() {
		return $this->m_strCapturedOn;
	}

	public function sqlCapturedOn() {
		return ( true == isset( $this->m_strCapturedOn ) ) ? '\'' . $this->m_strCapturedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' : '\'USD\'';
	}

	public function setCardTypeId( $intCardTypeId ) {
		$this->set( 'm_intCardTypeId', CStrings::strTrimDef( $intCardTypeId, 3, NULL, true ) );
	}

	public function getCardTypeId() {
		return $this->m_intCardTypeId;
	}

	public function sqlCardTypeId() {
		return ( true == isset( $this->m_intCardTypeId ) ) ? ( string ) $this->m_intCardTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$objDataset = $objDatabase->createDataset();

		$intId = $this->getId();

		if( true == is_null( $intId ) ) {
			$strSql = 'SELECT nextval( \'' . $this->getSequenceName() . '\'::text ) AS id';

			if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert ar payment record. The following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

				$objDataset->cleanup();
				return false;
			}

			$arrValues = $objDataset->fetchArray();
			$this->setId( $arrValues['id'] );

			$objDataset->cleanup();
		}

		$strSql = 'SELECT * ' .
					'FROM ' . static::TABLE_NAME . '_insert( row_to_json ( ROW ( ' .
						$this->sqlId() . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCompanyMerchantAccountId() . ', ' .
 						$this->sqlProcessingBankAccountId() . ', ' .
 						$this->sqlMerchantGatewayId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlPaymentTypeId() . ', ' .
 						$this->sqlPaymentMediumId() . ', ' .
 						$this->sqlPaymentStatusTypeId() . ', ' .
 						$this->sqlReturnTypeId() . ', ' .
 						$this->sqlArPaymentId() . ', ' .
 						$this->sqlScheduledPaymentId() . ', ' .
 						$this->sqlArCodeId() . ', ' .
 						$this->sqlCustomerPaymentAccountId() . ', ' .
 						$this->sqlArPaymentTransmissionId() . ', ' .
 						$this->sqlArPaymentExportId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlPsProductOptionId() . ', ' .
 						$this->sqlAccountVerificationLogId() . ', ' .
 						$this->sqlCompanyCharityId() . ', ' .
 						$this->sqlLeaseStatusTypeId() . ', ' .
 						$this->sqlAllocationTypeId() . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlReturnRemotePrimaryKey() . ', ' .
 						$this->sqlPaymentDatetime() . ', ' .
 						$this->sqlReceiptDatetime() . ', ' .
 						$this->sqlPaymentAmount() . ', ' .
 						$this->sqlConvenienceFeeAmount() . ', ' .
 						$this->sqlDonationAmount() . ', ' .
 						$this->sqlTotalAmount() . ', ' .
 						$this->sqlPaymentCost() . ', ' .
 						$this->sqlCompanyChargeAmount() . ', ' .
 						$this->sqlIncentiveCompanyChargeAmount() . ', ' .
 						$this->sqlSecureReferenceNumber() . ', ' .
 						$this->sqlRemotePaymentNumber() . ', ' .
 						$this->sqlPaymentMemo() . ', ' .
 						$this->sqlBilltoIpAddress() . ', ' .
 						$this->sqlBilltoIpAddressEncrypted() . ', ' .
 						$this->sqlBilltoAccountNumber() . ', ' .
 						$this->sqlBilltoCompanyName() . ', ' .
 						$this->sqlBilltoUnitNumber() . ', ' .
 						$this->sqlBilltoNameFirst() . ', ' .
 						$this->sqlBilltoNameFirstEncrypted() . ', ' .
 						$this->sqlBilltoNameMiddle() . ', ' .
 						$this->sqlBilltoNameMiddleEncrypted() . ', ' .
 						$this->sqlBilltoNameLast() . ', ' .
 						$this->sqlBilltoNameLastEncrypted() . ', ' .
 						$this->sqlBilltoStreetLine1() . ', ' .
 						$this->sqlBilltoStreetLine2() . ', ' .
 						$this->sqlBilltoStreetLine3() . ', ' .
 						$this->sqlBilltoCity() . ', ' .
 						$this->sqlBilltoStateCode() . ', ' .
 						$this->sqlBilltoProvince() . ', ' .
 						$this->sqlBilltoPostalCode() . ', ' .
 						$this->sqlBilltoCountryCode() . ', ' .
 						$this->sqlBilltoPhoneNumber() . ', ' .
 						$this->sqlBilltoPhoneNumberEncrypted() . ', ' .
 						$this->sqlBilltoEmailAddress() . ', ' .
 						$this->sqlBilltoEmailAddressEncrypted() . ', ' .
 						$this->sqlCarLarAmount() . ', ' .
 						$this->sqlCarLarConfidence() . ', ' .
 						$this->sqlCarLarIsMoneyOrder() . ', ' .
 						$this->sqlCarLarIsPerformed() . ', ' .
 						$this->sqlPhoneAuthRequired() . ', ' .
 						$this->sqlPhoneAuthBy() . ', ' .
 						$this->sqlPhoneAuthOn() . ', ' .
 						$this->sqlRequiresApproval() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
 						$this->sqlExportedOn() . ', ' .
 						$this->sqlReturnExportedOn() . ', ' .
 						$this->sqlReturnBatchedOn() . ', ' .
 						$this->sqlFeePostedOn() . ', ' .
 						$this->sqlReturnFeePostedOn() . ', ' .
 						$this->sqlReverseFeePostedOn() . ', ' .
 						$this->sqlIsApplicationPayment() . ', ' .
 						$this->sqlAllocateArTransactionIds() . ', ' .
 						$this->sqlCcCardNumberEncrypted() . ', ' .
 						$this->sqlCcExpDateMonth() . ', ' .
 						$this->sqlCcExpDateYear() . ', ' .
 						$this->sqlCcNameOnCard() . ', ' .
 						$this->sqlCcBinNumber() . ', ' .
 						$this->sqlCheckNumber() . ', ' .
 						$this->sqlCheckDate() . ', ' .
 						$this->sqlCheckPayableTo() . ', ' .
 						$this->sqlCheckBankName() . ', ' .
 						$this->sqlCheckNameOnAccount() . ', ' .
 						$this->sqlCheckAccountTypeId() . ', ' .
 						$this->sqlCheckRoutingNumber() . ', ' .
 						$this->sqlCheckAccountNumberEncrypted() . ', ' .
 						$this->sqlCheckAccountNumberLastfourEncrypted() . ', ' .
 						$this->sqlCheckAuxillaryOnUs() . ', ' .
 						$this->sqlCheckEpc() . ', ' .
 						$this->sqlCheckIsMoneyOrder() . ', ' .
 						$this->sqlCheckIsConverted() . ', ' .
 						$this->sqlBlockAutoAssociation() . ', ' .
 						$this->sqlFedTrackingNumber() . ', ' .
 						$this->sqlIsSplit() . ', ' .
 						$this->sqlIsReversed() . ', ' .
 						$this->sqlIsReconPrepped() . ', ' .
 						$this->sqlIsDebitCard() . ', ' .
 						$this->sqlIsCheckCard() . ', ' .
 						$this->sqlIsGiftCard() . ', ' .
 						$this->sqlIsPrepaidCard() . ', ' .
 						$this->sqlIsCreditCard() . ', ' .
 						$this->sqlIsInternationalCard() . ', ' .
 						$this->sqlIsCommercialCard() . ', ' .
 						$this->sqlIsRiskReview() . ', ' .
 						$this->sqlBlockAssociation() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlLockSequence() . ', ' .
 						$this->sqlTermsAcceptedOn() . ', ' .
 						$this->sqlDistributionBlockedOn() . ', ' .
 						$this->sqlAutoCaptureOn() . ', ' .
 						$this->sqlDistributeOn() . ', ' .
 						$this->sqlDistributeReturnOn() . ', ' .
 						$this->sqlReturnedOn() . ', ' .
 						$this->sqlBatchedOn() . ', ' .
 						$this->sqlEftBatchedOn() . ', ' .
 						$this->sqlCapturedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlCurrencyCode() .
						$this->sqlCardTypeId() . ' ) ) ) AS result;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {

			//Reset id on error
			$this->setId( $intId );

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert ar payment record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();
			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert ar payment record. The following error was reported.' ) );
			//Reset id on error
			$this->setId( $intId );

			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * ' .
					'FROM ' . static::TABLE_NAME . '_update( row_to_json ( ROW ( ' .
						$this->sqlId() . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCompanyMerchantAccountId() . ', ' .
 						$this->sqlProcessingBankAccountId() . ', ' .
 						$this->sqlMerchantGatewayId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlPaymentTypeId() . ', ' .
 						$this->sqlPaymentMediumId() . ', ' .
 						$this->sqlPaymentStatusTypeId() . ', ' .
 						$this->sqlReturnTypeId() . ', ' .
 						$this->sqlArPaymentId() . ', ' .
 						$this->sqlScheduledPaymentId() . ', ' .
 						$this->sqlArCodeId() . ', ' .
 						$this->sqlCustomerPaymentAccountId() . ', ' .
 						$this->sqlArPaymentTransmissionId() . ', ' .
 						$this->sqlArPaymentExportId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlPsProductOptionId() . ', ' .
 						$this->sqlAccountVerificationLogId() . ', ' .
 						$this->sqlCompanyCharityId() . ', ' .
 						$this->sqlLeaseStatusTypeId() . ', ' .
 						$this->sqlAllocationTypeId() . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlReturnRemotePrimaryKey() . ', ' .
 						$this->sqlPaymentDatetime() . ', ' .
 						$this->sqlReceiptDatetime() . ', ' .
 						$this->sqlPaymentAmount() . ', ' .
 						$this->sqlConvenienceFeeAmount() . ', ' .
 						$this->sqlDonationAmount() . ', ' .
 						$this->sqlTotalAmount() . ', ' .
 						$this->sqlPaymentCost() . ', ' .
 						$this->sqlCompanyChargeAmount() . ', ' .
 						$this->sqlIncentiveCompanyChargeAmount() . ', ' .
 						$this->sqlSecureReferenceNumber() . ', ' .
 						$this->sqlRemotePaymentNumber() . ', ' .
 						$this->sqlPaymentMemo() . ', ' .
 						$this->sqlBilltoIpAddress() . ', ' .
 						$this->sqlBilltoIpAddressEncrypted() . ', ' .
 						$this->sqlBilltoAccountNumber() . ', ' .
 						$this->sqlBilltoCompanyName() . ', ' .
 						$this->sqlBilltoUnitNumber() . ', ' .
 						$this->sqlBilltoNameFirst() . ', ' .
 						$this->sqlBilltoNameFirstEncrypted() . ', ' .
 						$this->sqlBilltoNameMiddle() . ', ' .
 						$this->sqlBilltoNameMiddleEncrypted() . ', ' .
 						$this->sqlBilltoNameLast() . ', ' .
 						$this->sqlBilltoNameLastEncrypted() . ', ' .
 						$this->sqlBilltoStreetLine1() . ', ' .
 						$this->sqlBilltoStreetLine2() . ', ' .
 						$this->sqlBilltoStreetLine3() . ', ' .
 						$this->sqlBilltoCity() . ', ' .
 						$this->sqlBilltoStateCode() . ', ' .
 						$this->sqlBilltoProvince() . ', ' .
 						$this->sqlBilltoPostalCode() . ', ' .
 						$this->sqlBilltoCountryCode() . ', ' .
 						$this->sqlBilltoPhoneNumber() . ', ' .
 						$this->sqlBilltoPhoneNumberEncrypted() . ', ' .
 						$this->sqlBilltoEmailAddress() . ', ' .
 						$this->sqlBilltoEmailAddressEncrypted() . ', ' .
 						$this->sqlCarLarAmount() . ', ' .
 						$this->sqlCarLarConfidence() . ', ' .
 						$this->sqlCarLarIsMoneyOrder() . ', ' .
 						$this->sqlCarLarIsPerformed() . ', ' .
 						$this->sqlPhoneAuthRequired() . ', ' .
 						$this->sqlPhoneAuthBy() . ', ' .
 						$this->sqlPhoneAuthOn() . ', ' .
 						$this->sqlRequiresApproval() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
 						$this->sqlExportedOn() . ', ' .
 						$this->sqlReturnExportedOn() . ', ' .
 						$this->sqlReturnBatchedOn() . ', ' .
 						$this->sqlFeePostedOn() . ', ' .
 						$this->sqlReturnFeePostedOn() . ', ' .
 						$this->sqlReverseFeePostedOn() . ', ' .
 						$this->sqlIsApplicationPayment() . ', ' .
 						$this->sqlAllocateArTransactionIds() . ', ' .
 						$this->sqlCcCardNumberEncrypted() . ', ' .
 						$this->sqlCcExpDateMonth() . ', ' .
 						$this->sqlCcExpDateYear() . ', ' .
 						$this->sqlCcNameOnCard() . ', ' .
 						$this->sqlCcBinNumber() . ', ' .
 						$this->sqlCheckNumber() . ', ' .
 						$this->sqlCheckDate() . ', ' .
 						$this->sqlCheckPayableTo() . ', ' .
 						$this->sqlCheckBankName() . ', ' .
 						$this->sqlCheckNameOnAccount() . ', ' .
 						$this->sqlCheckAccountTypeId() . ', ' .
 						$this->sqlCheckRoutingNumber() . ', ' .
 						$this->sqlCheckAccountNumberEncrypted() . ', ' .
 						$this->sqlCheckAccountNumberLastfourEncrypted() . ', ' .
 						$this->sqlCheckAuxillaryOnUs() . ', ' .
 						$this->sqlCheckEpc() . ', ' .
 						$this->sqlCheckIsMoneyOrder() . ', ' .
 						$this->sqlCheckIsConverted() . ', ' .
 						$this->sqlBlockAutoAssociation() . ', ' .
 						$this->sqlFedTrackingNumber() . ', ' .
 						$this->sqlIsSplit() . ', ' .
 						$this->sqlIsReversed() . ', ' .
 						$this->sqlIsReconPrepped() . ', ' .
 						$this->sqlIsDebitCard() . ', ' .
 						$this->sqlIsCheckCard() . ', ' .
 						$this->sqlIsGiftCard() . ', ' .
 						$this->sqlIsPrepaidCard() . ', ' .
 						$this->sqlIsCreditCard() . ', ' .
 						$this->sqlIsInternationalCard() . ', ' .
 						$this->sqlIsCommercialCard() . ', ' .
 						$this->sqlIsRiskReview() . ', ' .
 						$this->sqlBlockAssociation() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlLockSequence() . ', ' .
 						$this->sqlTermsAcceptedOn() . ', ' .
 						$this->sqlDistributionBlockedOn() . ', ' .
 						$this->sqlAutoCaptureOn() . ', ' .
 						$this->sqlDistributeOn() . ', ' .
 						$this->sqlDistributeReturnOn() . ', ' .
 						$this->sqlReturnedOn() . ', ' .
 						$this->sqlBatchedOn() . ', ' .
 						$this->sqlEftBatchedOn() . ', ' .
 						$this->sqlCapturedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$this->sqlCurrencyCode() .
						$this->sqlCardTypeId() . ' ) ) ) AS result;';

		if( true == $boolReturnSqlOnly ) {
			 return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update ar payment record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update ar payment record. The following error was reported.' ) );

			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * ' .
					'FROM ' . static::TABLE_NAME . '_delete( ' .
		$this->sqlId() . ', ' .
		$this->sqlCid() . ', ' .
					 ( int ) $intCurrentUserId . ' ) AS result;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to delete ar payment record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to delete ar payment record. The following error was reported.' ) );

			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'company_merchant_account_id' => $this->getCompanyMerchantAccountId(),
			'processing_bank_account_id' => $this->getProcessingBankAccountId(),
			'merchant_gateway_id' => $this->getMerchantGatewayId(),
			'property_id' => $this->getPropertyId(),
			'customer_id' => $this->getCustomerId(),
			'lease_id' => $this->getLeaseId(),
			'payment_type_id' => $this->getPaymentTypeId(),
			'payment_medium_id' => $this->getPaymentMediumId(),
			'payment_status_type_id' => $this->getPaymentStatusTypeId(),
			'return_type_id' => $this->getReturnTypeId(),
			'ar_payment_id' => $this->getArPaymentId(),
			'scheduled_payment_id' => $this->getScheduledPaymentId(),
			'ar_code_id' => $this->getArCodeId(),
			'customer_payment_account_id' => $this->getCustomerPaymentAccountId(),
			'ar_payment_transmission_id' => $this->getArPaymentTransmissionId(),
			'ar_payment_export_id' => $this->getArPaymentExportId(),
			'ps_product_id' => $this->getPsProductId(),
			'ps_product_option_id' => $this->getPsProductOptionId(),
			'account_verification_log_id' => $this->getAccountVerificationLogId(),
			'company_charity_id' => $this->getCompanyCharityId(),
			'lease_status_type_id' => $this->getLeaseStatusTypeId(),
			'allocation_type_id' => $this->getAllocationTypeId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'return_remote_primary_key' => $this->getReturnRemotePrimaryKey(),
			'payment_datetime' => $this->getPaymentDatetime(),
			'receipt_datetime' => $this->getReceiptDatetime(),
			'payment_amount' => $this->getPaymentAmount(),
			'convenience_fee_amount' => $this->getConvenienceFeeAmount(),
			'donation_amount' => $this->getDonationAmount(),
			'total_amount' => $this->getTotalAmount(),
			'payment_cost' => $this->getPaymentCost(),
			'company_charge_amount' => $this->getCompanyChargeAmount(),
			'incentive_company_charge_amount' => $this->getIncentiveCompanyChargeAmount(),
			'secure_reference_number' => $this->getSecureReferenceNumber(),
			'remote_payment_number' => $this->getRemotePaymentNumber(),
			'payment_memo' => $this->getPaymentMemo(),
			'billto_ip_address' => $this->getBilltoIpAddress(),
			'billto_ip_address_encrypted' => $this->getBilltoIpAddressEncrypted(),
			'billto_account_number' => $this->getBilltoAccountNumber(),
			'billto_company_name' => $this->getBilltoCompanyName(),
			'billto_unit_number' => $this->getBilltoUnitNumber(),
			'billto_name_first' => $this->getBilltoNameFirst(),
			'billto_name_first_encrypted' => $this->getBilltoNameFirstEncrypted(),
			'billto_name_middle' => $this->getBilltoNameMiddle(),
			'billto_name_middle_encrypted' => $this->getBilltoNameMiddleEncrypted(),
			'billto_name_last' => $this->getBilltoNameLast(),
			'billto_name_last_encrypted' => $this->getBilltoNameLastEncrypted(),
			'billto_street_line1' => $this->getBilltoStreetLine1(),
			'billto_street_line2' => $this->getBilltoStreetLine2(),
			'billto_street_line3' => $this->getBilltoStreetLine3(),
			'billto_city' => $this->getBilltoCity(),
			'billto_state_code' => $this->getBilltoStateCode(),
			'billto_province' => $this->getBilltoProvince(),
			'billto_postal_code' => $this->getBilltoPostalCode(),
			'billto_country_code' => $this->getBilltoCountryCode(),
			'billto_phone_number' => $this->getBilltoPhoneNumber(),
			'billto_phone_number_encrypted' => $this->getBilltoPhoneNumberEncrypted(),
			'billto_email_address' => $this->getBilltoEmailAddress(),
			'billto_email_address_encrypted' => $this->getBilltoEmailAddressEncrypted(),
			'car_lar_amount' => $this->getCarLarAmount(),
			'car_lar_confidence' => $this->getCarLarConfidence(),
			'car_lar_is_money_order' => $this->getCarLarIsMoneyOrder(),
			'car_lar_is_performed' => $this->getCarLarIsPerformed(),
			'phone_auth_required' => $this->getPhoneAuthRequired(),
			'phone_auth_by' => $this->getPhoneAuthBy(),
			'phone_auth_on' => $this->getPhoneAuthOn(),
			'requires_approval' => $this->getRequiresApproval(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'exported_on' => $this->getExportedOn(),
			'return_exported_on' => $this->getReturnExportedOn(),
			'return_batched_on' => $this->getReturnBatchedOn(),
			'fee_posted_on' => $this->getFeePostedOn(),
			'return_fee_posted_on' => $this->getReturnFeePostedOn(),
			'reverse_fee_posted_on' => $this->getReverseFeePostedOn(),
			'is_application_payment' => $this->getIsApplicationPayment(),
			'allocate_ar_transaction_ids' => $this->getAllocateArTransactionIds(),
			'cc_card_number_encrypted' => $this->getCcCardNumberEncrypted(),
			'cc_exp_date_month' => $this->getCcExpDateMonth(),
			'cc_exp_date_year' => $this->getCcExpDateYear(),
			'cc_name_on_card' => $this->getCcNameOnCard(),
			'cc_bin_number' => $this->getCcBinNumber(),
			'check_number' => $this->getCheckNumber(),
			'check_date' => $this->getCheckDate(),
			'check_payable_to' => $this->getCheckPayableTo(),
			'check_bank_name' => $this->getCheckBankName(),
			'check_name_on_account' => $this->getCheckNameOnAccount(),
			'check_account_type_id' => $this->getCheckAccountTypeId(),
			'check_routing_number' => $this->getCheckRoutingNumber(),
			'check_account_number_encrypted' => $this->getCheckAccountNumberEncrypted(),
			'check_account_number_lastfour_encrypted' => $this->getCheckAccountNumberLastfourEncrypted(),
			'check_auxillary_on_us' => $this->getCheckAuxillaryOnUs(),
			'check_epc' => $this->getCheckEpc(),
			'check_is_money_order' => $this->getCheckIsMoneyOrder(),
			'check_is_converted' => $this->getCheckIsConverted(),
			'block_auto_association' => $this->getBlockAutoAssociation(),
			'fed_tracking_number' => $this->getFedTrackingNumber(),
			'is_split' => $this->getIsSplit(),
			'is_reversed' => $this->getIsReversed(),
			'is_recon_prepped' => $this->getIsReconPrepped(),
			'is_debit_card' => $this->getIsDebitCard(),
			'is_check_card' => $this->getIsCheckCard(),
			'is_gift_card' => $this->getIsGiftCard(),
			'is_prepaid_card' => $this->getIsPrepaidCard(),
			'is_credit_card' => $this->getIsCreditCard(),
			'is_international_card' => $this->getIsInternationalCard(),
			'is_commercial_card' => $this->getIsCommercialCard(),
			'is_risk_review' => $this->getIsRiskReview(),
			'block_association' => $this->getBlockAssociation(),
			'order_num' => $this->getOrderNum(),
			'lock_sequence' => $this->getLockSequence(),
			'terms_accepted_on' => $this->getTermsAcceptedOn(),
			'distribution_blocked_on' => $this->getDistributionBlockedOn(),
			'auto_capture_on' => $this->getAutoCaptureOn(),
			'distribute_on' => $this->getDistributeOn(),
			'distribute_return_on' => $this->getDistributeReturnOn(),
			'returned_on' => $this->getReturnedOn(),
			'batched_on' => $this->getBatchedOn(),
			'eft_batched_on' => $this->getEftBatchedOn(),
			'captured_on' => $this->getCapturedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'currency_code' => $this->getCurrencyCode(),
			'card_type_id' => $this->getCardTypeId()
		);
	}

}
?>