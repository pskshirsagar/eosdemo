<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CFilterTypes
 * Do not add any new functions to this class.
 */

class CBaseFilterTypes extends CEosPluralBase {

	/**
	 * @return CFilterType[]
	 */
	public static function fetchFilterTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CFilterType', $objDatabase );
	}

	/**
	 * @return CFilterType
	 */
	public static function fetchFilterType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CFilterType', $objDatabase );
	}

	public static function fetchFilterTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'filter_types', $objDatabase );
	}

	public static function fetchFilterTypeById( $intId, $objDatabase ) {
		return self::fetchFilterType( sprintf( 'SELECT * FROM filter_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>