<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTableLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseTableLogs extends CEosPluralBase {

	/**
	 * @return CTableLog[]
	 */
	public static function fetchTableLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CTableLog::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CTableLog
	 */
	public static function fetchTableLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTableLog::class, $objDatabase );
	}

	public static function fetchTableLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'table_logs', $objDatabase );
	}

	public static function fetchTableLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchTableLog( sprintf( 'SELECT * FROM table_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTableLogsByCid( $intCid, $objDatabase ) {
		return self::fetchTableLogs( sprintf( 'SELECT * FROM table_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>