<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackageApplicantTypeAssociations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningPackageApplicantTypeAssociations extends CEosPluralBase {

	/**
	 * @return CScreeningPackageApplicantTypeAssociation[]
	 */
	public static function fetchScreeningPackageApplicantTypeAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CScreeningPackageApplicantTypeAssociation', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScreeningPackageApplicantTypeAssociation
	 */
	public static function fetchScreeningPackageApplicantTypeAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningPackageApplicantTypeAssociation', $objDatabase );
	}

	public static function fetchScreeningPackageApplicantTypeAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_package_applicant_type_associations', $objDatabase );
	}

	public static function fetchScreeningPackageApplicantTypeAssociationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageApplicantTypeAssociation( sprintf( 'SELECT * FROM screening_package_applicant_type_associations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageApplicantTypeAssociationsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningPackageApplicantTypeAssociations( sprintf( 'SELECT * FROM screening_package_applicant_type_associations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageApplicantTypeAssociationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageApplicantTypeAssociations( sprintf( 'SELECT * FROM screening_package_applicant_type_associations WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageApplicantTypeAssociationsByScreeningPackageIdByCid( $intScreeningPackageId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageApplicantTypeAssociations( sprintf( 'SELECT * FROM screening_package_applicant_type_associations WHERE screening_package_id = %d AND cid = %d', ( int ) $intScreeningPackageId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageApplicantTypeAssociationsByScreeningApplicantTypeIdByCid( $intScreeningApplicantTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageApplicantTypeAssociations( sprintf( 'SELECT * FROM screening_package_applicant_type_associations WHERE screening_applicant_type_id = %d AND cid = %d', ( int ) $intScreeningApplicantTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>