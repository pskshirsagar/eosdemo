<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningPackagesProperties
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningPackagesProperties extends CEosPluralBase {

	/**
	 * @return CScreeningPackagesProperty[]
	 */
	public static function fetchScreeningPackagesProperties( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CScreeningPackagesProperty', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScreeningPackagesProperty
	 */
	public static function fetchScreeningPackagesProperty( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningPackagesProperty', $objDatabase );
	}

	public static function fetchScreeningPackagesPropertyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_packages_properties', $objDatabase );
	}

	public static function fetchScreeningPackagesPropertyByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackagesProperty( sprintf( 'SELECT * FROM screening_packages_properties WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackagesPropertiesByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningPackagesProperties( sprintf( 'SELECT * FROM screening_packages_properties WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackagesPropertiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackagesProperties( sprintf( 'SELECT * FROM screening_packages_properties WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackagesPropertiesByScreeningPackageIdByCid( $intScreeningPackageId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackagesProperties( sprintf( 'SELECT * FROM screening_packages_properties WHERE screening_package_id = %d AND cid = %d', ( int ) $intScreeningPackageId, ( int ) $intCid ), $objDatabase );
	}

}
?>