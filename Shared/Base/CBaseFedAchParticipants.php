<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CFedAchParticipants
 * Do not add any new functions to this class.
 */

class CBaseFedAchParticipants extends CEosPluralBase {

	/**
	 * @return CFedAchParticipant[]
	 */
	public static function fetchFedAchParticipants( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CFedAchParticipant::class, $objDatabase );
	}

	/**
	 * @return CFedAchParticipant
	 */
	public static function fetchFedAchParticipant( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFedAchParticipant::class, $objDatabase );
	}

	public static function fetchFedAchParticipantCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'fed_ach_participants', $objDatabase );
	}

	public static function fetchFedAchParticipantById( $intId, $objDatabase ) {
		return self::fetchFedAchParticipant( sprintf( 'SELECT * FROM fed_ach_participants WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>