<?php

class CBaseDefaultTemplateWidget extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_template_widgets';

	protected $m_intId;
	protected $m_intWebsiteTemplateId;
	protected $m_intWidgetSpaceId;
	protected $m_strWidgetNamespace;
	protected $m_strWidgetName;
	protected $m_intSupportedType;
	protected $m_strTitle;
	protected $m_boolShowDescription;
	protected $m_strDescription;
	protected $m_boolShowLink;
	protected $m_strLink;
	protected $m_strLinkText;
	protected $m_intTarget;
	protected $m_strAlt;
	protected $m_boolShowImage;
	protected $m_strMediaName;
	protected $m_intMediaWidth;
	protected $m_intMediaHeight;
	protected $m_strWidgetParameters;
	protected $m_boolIsDefault;
	protected $m_boolIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strWidgetLabel;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolShowDescription = true;
		$this->m_boolShowLink = true;
		$this->m_intTarget = '1';
		$this->m_boolShowImage = true;
		$this->m_boolIsDefault = false;
		$this->m_boolIsPublished = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['website_template_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteTemplateId', trim( $arrValues['website_template_id'] ) ); elseif( isset( $arrValues['website_template_id'] ) ) $this->setWebsiteTemplateId( $arrValues['website_template_id'] );
		if( isset( $arrValues['widget_space_id'] ) && $boolDirectSet ) $this->set( 'm_intWidgetSpaceId', trim( $arrValues['widget_space_id'] ) ); elseif( isset( $arrValues['widget_space_id'] ) ) $this->setWidgetSpaceId( $arrValues['widget_space_id'] );
		if( isset( $arrValues['widget_namespace'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strWidgetNamespace', trim( stripcslashes( $arrValues['widget_namespace'] ) ) ); elseif( isset( $arrValues['widget_namespace'] ) ) $this->setWidgetNamespace( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['widget_namespace'] ) : $arrValues['widget_namespace'] );
		if( isset( $arrValues['widget_name'] ) && $boolDirectSet ) $this->set( 'm_strWidgetName', trim( stripcslashes( $arrValues['widget_name'] ) ) ); elseif( isset( $arrValues['widget_name'] ) ) $this->setWidgetName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['widget_name'] ) : $arrValues['widget_name'] );
		if( isset( $arrValues['supported_type'] ) && $boolDirectSet ) $this->set( 'm_intSupportedType', trim( $arrValues['supported_type'] ) ); elseif( isset( $arrValues['supported_type'] ) ) $this->setSupportedType( $arrValues['supported_type'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['show_description'] ) && $boolDirectSet ) $this->set( 'm_boolShowDescription', trim( stripcslashes( $arrValues['show_description'] ) ) ); elseif( isset( $arrValues['show_description'] ) ) $this->setShowDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_description'] ) : $arrValues['show_description'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['show_link'] ) && $boolDirectSet ) $this->set( 'm_boolShowLink', trim( stripcslashes( $arrValues['show_link'] ) ) ); elseif( isset( $arrValues['show_link'] ) ) $this->setShowLink( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_link'] ) : $arrValues['show_link'] );
		if( isset( $arrValues['link'] ) && $boolDirectSet ) $this->set( 'm_strLink', trim( stripcslashes( $arrValues['link'] ) ) ); elseif( isset( $arrValues['link'] ) ) $this->setLink( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['link'] ) : $arrValues['link'] );
		if( isset( $arrValues['link_text'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strLinkText', trim( stripcslashes( $arrValues['link_text'] ) ) ); elseif( isset( $arrValues['link_text'] ) ) $this->setLinkText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['link_text'] ) : $arrValues['link_text'] );
		if( isset( $arrValues['target'] ) && $boolDirectSet ) $this->set( 'm_intTarget', trim( $arrValues['target'] ) ); elseif( isset( $arrValues['target'] ) ) $this->setTarget( $arrValues['target'] );
		if( isset( $arrValues['alt'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strAlt', trim( stripcslashes( $arrValues['alt'] ) ) ); elseif( isset( $arrValues['alt'] ) ) $this->setAlt( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['alt'] ) : $arrValues['alt'] );
		if( isset( $arrValues['show_image'] ) && $boolDirectSet ) $this->set( 'm_boolShowImage', trim( stripcslashes( $arrValues['show_image'] ) ) ); elseif( isset( $arrValues['show_image'] ) ) $this->setShowImage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_image'] ) : $arrValues['show_image'] );
		if( isset( $arrValues['media_name'] ) && $boolDirectSet ) $this->set( 'm_strMediaName', trim( stripcslashes( $arrValues['media_name'] ) ) ); elseif( isset( $arrValues['media_name'] ) ) $this->setMediaName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['media_name'] ) : $arrValues['media_name'] );
		if( isset( $arrValues['media_width'] ) && $boolDirectSet ) $this->set( 'm_intMediaWidth', trim( $arrValues['media_width'] ) ); elseif( isset( $arrValues['media_width'] ) ) $this->setMediaWidth( $arrValues['media_width'] );
		if( isset( $arrValues['media_height'] ) && $boolDirectSet ) $this->set( 'm_intMediaHeight', trim( $arrValues['media_height'] ) ); elseif( isset( $arrValues['media_height'] ) ) $this->setMediaHeight( $arrValues['media_height'] );
		if( isset( $arrValues['widget_parameters'] ) && $boolDirectSet ) $this->set( 'm_strWidgetParameters', trim( stripcslashes( $arrValues['widget_parameters'] ) ) ); elseif( isset( $arrValues['widget_parameters'] ) ) $this->setWidgetParameters( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['widget_parameters'] ) : $arrValues['widget_parameters'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_boolIsDefault', trim( stripcslashes( $arrValues['is_default'] ) ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_default'] ) : $arrValues['is_default'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['widget_label'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strWidgetLabel', trim( stripcslashes( $arrValues['widget_label'] ) ) ); elseif( isset( $arrValues['widget_label'] ) ) $this->setWidgetLabel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['widget_label'] ) : $arrValues['widget_label'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setWebsiteTemplateId( $intWebsiteTemplateId ) {
		$this->set( 'm_intWebsiteTemplateId', CStrings::strToIntDef( $intWebsiteTemplateId, NULL, false ) );
	}

	public function getWebsiteTemplateId() {
		return $this->m_intWebsiteTemplateId;
	}

	public function sqlWebsiteTemplateId() {
		return ( true == isset( $this->m_intWebsiteTemplateId ) ) ? ( string ) $this->m_intWebsiteTemplateId : 'NULL';
	}

	public function setWidgetSpaceId( $intWidgetSpaceId ) {
		$this->set( 'm_intWidgetSpaceId', CStrings::strToIntDef( $intWidgetSpaceId, NULL, false ) );
	}

	public function getWidgetSpaceId() {
		return $this->m_intWidgetSpaceId;
	}

	public function sqlWidgetSpaceId() {
		return ( true == isset( $this->m_intWidgetSpaceId ) ) ? ( string ) $this->m_intWidgetSpaceId : 'NULL';
	}

	public function setWidgetNamespace( $strWidgetNamespace, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strWidgetNamespace', CStrings::strTrimDef( $strWidgetNamespace, 250, NULL, true ), $strLocaleCode );
	}

	public function getWidgetNamespace( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strWidgetNamespace', $strLocaleCode );
	}

	public function sqlWidgetNamespace() {
		return ( true == isset( $this->m_strWidgetNamespace ) ) ? '\'' . addslashes( $this->m_strWidgetNamespace ) . '\'' : 'NULL';
	}

	public function setWidgetName( $strWidgetName ) {
		$this->set( 'm_strWidgetName', CStrings::strTrimDef( $strWidgetName, 250, NULL, true ) );
	}

	public function getWidgetName() {
		return $this->m_strWidgetName;
	}

	public function sqlWidgetName() {
		return ( true == isset( $this->m_strWidgetName ) ) ? '\'' . addslashes( $this->m_strWidgetName ) . '\'' : 'NULL';
	}

	public function setSupportedType( $intSupportedType ) {
		$this->set( 'm_intSupportedType', CStrings::strToIntDef( $intSupportedType, NULL, false ) );
	}

	public function getSupportedType() {
		return $this->m_intSupportedType;
	}

	public function sqlSupportedType() {
		return ( true == isset( $this->m_intSupportedType ) ) ? ( string ) $this->m_intSupportedType : 'NULL';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 250, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setShowDescription( $boolShowDescription ) {
		$this->set( 'm_boolShowDescription', CStrings::strToBool( $boolShowDescription ) );
	}

	public function getShowDescription() {
		return $this->m_boolShowDescription;
	}

	public function sqlShowDescription() {
		return ( true == isset( $this->m_boolShowDescription ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowDescription ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setShowLink( $boolShowLink ) {
		$this->set( 'm_boolShowLink', CStrings::strToBool( $boolShowLink ) );
	}

	public function getShowLink() {
		return $this->m_boolShowLink;
	}

	public function sqlShowLink() {
		return ( true == isset( $this->m_boolShowLink ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowLink ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setLink( $strLink ) {
		$this->set( 'm_strLink', CStrings::strTrimDef( $strLink, -1, NULL, true ) );
	}

	public function getLink() {
		return $this->m_strLink;
	}

	public function sqlLink() {
		return ( true == isset( $this->m_strLink ) ) ? '\'' . addslashes( $this->m_strLink ) . '\'' : 'NULL';
	}

	public function setLinkText( $strLinkText, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strLinkText', CStrings::strTrimDef( $strLinkText, 50, NULL, true ), $strLocaleCode );
	}

	public function getLinkText( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strLinkText', $strLocaleCode );
	}

	public function sqlLinkText() {
		return ( true == isset( $this->m_strLinkText ) ) ? '\'' . addslashes( $this->m_strLinkText ) . '\'' : 'NULL';
	}

	public function setTarget( $intTarget ) {
		$this->set( 'm_intTarget', CStrings::strToIntDef( $intTarget, NULL, false ) );
	}

	public function getTarget() {
		return $this->m_intTarget;
	}

	public function sqlTarget() {
		return ( true == isset( $this->m_intTarget ) ) ? ( string ) $this->m_intTarget : '1';
	}

	public function setAlt( $strAlt, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strAlt', CStrings::strTrimDef( $strAlt, 250, NULL, true ), $strLocaleCode );
	}

	public function getAlt( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strAlt', $strLocaleCode );
	}

	public function sqlAlt() {
		return ( true == isset( $this->m_strAlt ) ) ? '\'' . addslashes( $this->m_strAlt ) . '\'' : 'NULL';
	}

	public function setShowImage( $boolShowImage ) {
		$this->set( 'm_boolShowImage', CStrings::strToBool( $boolShowImage ) );
	}

	public function getShowImage() {
		return $this->m_boolShowImage;
	}

	public function sqlShowImage() {
		return ( true == isset( $this->m_boolShowImage ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowImage ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setMediaName( $strMediaName ) {
		$this->set( 'm_strMediaName', CStrings::strTrimDef( $strMediaName, 250, NULL, true ) );
	}

	public function getMediaName() {
		return $this->m_strMediaName;
	}

	public function sqlMediaName() {
		return ( true == isset( $this->m_strMediaName ) ) ? '\'' . addslashes( $this->m_strMediaName ) . '\'' : 'NULL';
	}

	public function setMediaWidth( $intMediaWidth ) {
		$this->set( 'm_intMediaWidth', CStrings::strToIntDef( $intMediaWidth, NULL, false ) );
	}

	public function getMediaWidth() {
		return $this->m_intMediaWidth;
	}

	public function sqlMediaWidth() {
		return ( true == isset( $this->m_intMediaWidth ) ) ? ( string ) $this->m_intMediaWidth : 'NULL';
	}

	public function setMediaHeight( $intMediaHeight ) {
		$this->set( 'm_intMediaHeight', CStrings::strToIntDef( $intMediaHeight, NULL, false ) );
	}

	public function getMediaHeight() {
		return $this->m_intMediaHeight;
	}

	public function sqlMediaHeight() {
		return ( true == isset( $this->m_intMediaHeight ) ) ? ( string ) $this->m_intMediaHeight : 'NULL';
	}

	public function setWidgetParameters( $strWidgetParameters ) {
		$this->set( 'm_strWidgetParameters', CStrings::strTrimDef( $strWidgetParameters, -1, NULL, true ) );
	}

	public function getWidgetParameters() {
		return $this->m_strWidgetParameters;
	}

	public function sqlWidgetParameters() {
		return ( true == isset( $this->m_strWidgetParameters ) ) ? '\'' . addslashes( $this->m_strWidgetParameters ) . '\'' : 'NULL';
	}

	public function setIsDefault( $boolIsDefault ) {
		$this->set( 'm_boolIsDefault', CStrings::strToBool( $boolIsDefault ) );
	}

	public function getIsDefault() {
		return $this->m_boolIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_boolIsDefault ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDefault ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setWidgetLabel( $strWidgetLabel, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strWidgetLabel', CStrings::strTrimDef( $strWidgetLabel, 250, NULL, true ), $strLocaleCode );
	}

	public function getWidgetLabel( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strWidgetLabel', $strLocaleCode );
	}

	public function sqlWidgetLabel() {
		return ( true == isset( $this->m_strWidgetLabel ) ) ? '\'' . addslashes( $this->m_strWidgetLabel ) . '\'' : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, website_template_id, widget_space_id, widget_namespace, widget_name, supported_type, title, show_description, description, show_link, link, link_text, target, alt, show_image, media_name, media_width, media_height, widget_parameters, is_default, is_published, updated_by, updated_on, created_by, created_on, details, widget_label )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlWebsiteTemplateId() . ', ' .
						$this->sqlWidgetSpaceId() . ', ' .
						$this->sqlWidgetNamespace() . ', ' .
						$this->sqlWidgetName() . ', ' .
						$this->sqlSupportedType() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlShowDescription() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlShowLink() . ', ' .
						$this->sqlLink() . ', ' .
						$this->sqlLinkText() . ', ' .
						$this->sqlTarget() . ', ' .
						$this->sqlAlt() . ', ' .
						$this->sqlShowImage() . ', ' .
						$this->sqlMediaName() . ', ' .
						$this->sqlMediaWidth() . ', ' .
						$this->sqlMediaHeight() . ', ' .
						$this->sqlWidgetParameters() . ', ' .
						$this->sqlIsDefault() . ', ' .
						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlWidgetLabel() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_template_id = ' . $this->sqlWebsiteTemplateId(). ',' ; } elseif( true == array_key_exists( 'WebsiteTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' website_template_id = ' . $this->sqlWebsiteTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' widget_space_id = ' . $this->sqlWidgetSpaceId(). ',' ; } elseif( true == array_key_exists( 'WidgetSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' widget_space_id = ' . $this->sqlWidgetSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' widget_namespace = ' . $this->sqlWidgetNamespace(). ',' ; } elseif( true == array_key_exists( 'WidgetNamespace', $this->getChangedColumns() ) ) { $strSql .= ' widget_namespace = ' . $this->sqlWidgetNamespace() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' widget_name = ' . $this->sqlWidgetName(). ',' ; } elseif( true == array_key_exists( 'WidgetName', $this->getChangedColumns() ) ) { $strSql .= ' widget_name = ' . $this->sqlWidgetName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' supported_type = ' . $this->sqlSupportedType(). ',' ; } elseif( true == array_key_exists( 'SupportedType', $this->getChangedColumns() ) ) { $strSql .= ' supported_type = ' . $this->sqlSupportedType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_description = ' . $this->sqlShowDescription(). ',' ; } elseif( true == array_key_exists( 'ShowDescription', $this->getChangedColumns() ) ) { $strSql .= ' show_description = ' . $this->sqlShowDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_link = ' . $this->sqlShowLink(). ',' ; } elseif( true == array_key_exists( 'ShowLink', $this->getChangedColumns() ) ) { $strSql .= ' show_link = ' . $this->sqlShowLink() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' link = ' . $this->sqlLink(). ',' ; } elseif( true == array_key_exists( 'Link', $this->getChangedColumns() ) ) { $strSql .= ' link = ' . $this->sqlLink() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' link_text = ' . $this->sqlLinkText(). ',' ; } elseif( true == array_key_exists( 'LinkText', $this->getChangedColumns() ) ) { $strSql .= ' link_text = ' . $this->sqlLinkText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' target = ' . $this->sqlTarget(). ',' ; } elseif( true == array_key_exists( 'Target', $this->getChangedColumns() ) ) { $strSql .= ' target = ' . $this->sqlTarget() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alt = ' . $this->sqlAlt(). ',' ; } elseif( true == array_key_exists( 'Alt', $this->getChangedColumns() ) ) { $strSql .= ' alt = ' . $this->sqlAlt() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_image = ' . $this->sqlShowImage(). ',' ; } elseif( true == array_key_exists( 'ShowImage', $this->getChangedColumns() ) ) { $strSql .= ' show_image = ' . $this->sqlShowImage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_name = ' . $this->sqlMediaName(). ',' ; } elseif( true == array_key_exists( 'MediaName', $this->getChangedColumns() ) ) { $strSql .= ' media_name = ' . $this->sqlMediaName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_width = ' . $this->sqlMediaWidth(). ',' ; } elseif( true == array_key_exists( 'MediaWidth', $this->getChangedColumns() ) ) { $strSql .= ' media_width = ' . $this->sqlMediaWidth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_height = ' . $this->sqlMediaHeight(). ',' ; } elseif( true == array_key_exists( 'MediaHeight', $this->getChangedColumns() ) ) { $strSql .= ' media_height = ' . $this->sqlMediaHeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' widget_parameters = ' . $this->sqlWidgetParameters(). ',' ; } elseif( true == array_key_exists( 'WidgetParameters', $this->getChangedColumns() ) ) { $strSql .= ' widget_parameters = ' . $this->sqlWidgetParameters() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault(). ',' ; } elseif( true == array_key_exists( 'IsDefault', $this->getChangedColumns() ) ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' widget_label = ' . $this->sqlWidgetLabel(). ',' ; } elseif( true == array_key_exists( 'WidgetLabel', $this->getChangedColumns() ) ) { $strSql .= ' widget_label = ' . $this->sqlWidgetLabel() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {

				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'website_template_id' => $this->getWebsiteTemplateId(),
			'widget_space_id' => $this->getWidgetSpaceId(),
			'widget_namespace' => $this->getWidgetNamespace(),
			'widget_name' => $this->getWidgetName(),
			'supported_type' => $this->getSupportedType(),
			'title' => $this->getTitle(),
			'show_description' => $this->getShowDescription(),
			'description' => $this->getDescription(),
			'show_link' => $this->getShowLink(),
			'link' => $this->getLink(),
			'link_text' => $this->getLinkText(),
			'target' => $this->getTarget(),
			'alt' => $this->getAlt(),
			'show_image' => $this->getShowImage(),
			'media_name' => $this->getMediaName(),
			'media_width' => $this->getMediaWidth(),
			'media_height' => $this->getMediaHeight(),
			'widget_parameters' => $this->getWidgetParameters(),
			'is_default' => $this->getIsDefault(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'widget_label' => $this->getWidgetLabel()
		);
	}

}
?>