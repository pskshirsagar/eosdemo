<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseArPaymentDetail extends CEosSingularBase {

	use TEosDetails;

	use TEosPostalAddresses;

	const TABLE_NAME = 'public.ar_payment_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intArPaymentId;
	protected $m_intAllocationTypeId;
	protected $m_intAccountVerificationLogId;
	protected $m_strAllocateArTransactionIds;
	protected $m_intSecureReferenceNumber;
	protected $m_strPaymentMemo;
	protected $m_strBilltoIpAddress;
	protected $m_strBilltoIpAddressEncrypted;
	protected $m_strBilltoAccountNumber;
	protected $m_strBilltoCompanyName;
	protected $m_strBilltoStreetLine1;
	protected $m_strBilltoStreetLine2;
	protected $m_strBilltoStreetLine3;
	protected $m_strBilltoCity;
	protected $m_strBilltoStateCode;
	protected $m_strBilltoProvince;
	protected $m_strBilltoPostalCode;
	protected $m_strBilltoCountryCode;
	protected $m_strBilltoPhoneNumber;
	protected $m_strBilltoPhoneNumberEncrypted;
	protected $m_strBilltoEmailAddress;
	protected $m_strBilltoEmailAddressEncrypted;
	protected $m_fltCarLarAmount;
	protected $m_intCarLarConfidence;
	protected $m_intCarLarIsMoneyOrder;
	protected $m_intCarLarIsPerformed;
	protected $m_intPhoneAuthRequired;
	protected $m_intPhoneAuthBy;
	protected $m_strPhoneAuthOn;
	protected $m_intRequiresApproval;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_strExportedOn;
	protected $m_strReturnExportedOn;
	protected $m_strReturnBatchedOn;
	protected $m_strFeePostedOn;
	protected $m_strReturnFeePostedOn;
	protected $m_strReverseFeePostedOn;
	protected $m_strChargeBackFeePostedOn;
	protected $m_intIsApplicationPayment;
	protected $m_boolIsDebitCard;
	protected $m_boolIsCheckCard;
	protected $m_boolIsGiftCard;
	protected $m_boolIsPrepaidCard;
	protected $m_boolIsCreditCard;
	protected $m_boolIsInternationalCard;
	protected $m_boolIsCommercialCard;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strRemotePaymentNumber;
	protected $m_boolIsRiskReview;
	protected $m_strUnauthorizedReturnFeePostedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_arrstrPostalAddressFields;

	public function __construct() {
		parent::__construct();

		$this->m_intAllocationTypeId = '1';
		$this->m_intCarLarIsMoneyOrder = '0';
		$this->m_intCarLarIsPerformed = '0';
		$this->m_intPhoneAuthRequired = '0';
		$this->m_intRequiresApproval = '0';
		$this->m_intIsApplicationPayment = '0';
		$this->m_arrstrPostalAddressFields = [
			 'billto' => [
				'm_strBilltoStreetLine1' => 'addressLine1',
				'm_strBilltoStreetLine2' => 'addressLine2',
				'm_strBilltoStreetLine3' => 'addressLine3',
				'm_strBilltoCity' => 'locality',
				'm_strBilltoStateCode' => 'administrativeArea',
				'm_strBilltoPostalCode' => 'postalCode',
				'm_strBilltoCountryCode' => 'country'
			]
		 ];

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ar_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentId', trim( $arrValues['ar_payment_id'] ) ); elseif( isset( $arrValues['ar_payment_id'] ) ) $this->setArPaymentId( $arrValues['ar_payment_id'] );
		if( isset( $arrValues['allocation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAllocationTypeId', trim( $arrValues['allocation_type_id'] ) ); elseif( isset( $arrValues['allocation_type_id'] ) ) $this->setAllocationTypeId( $arrValues['allocation_type_id'] );
		if( isset( $arrValues['account_verification_log_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountVerificationLogId', trim( $arrValues['account_verification_log_id'] ) ); elseif( isset( $arrValues['account_verification_log_id'] ) ) $this->setAccountVerificationLogId( $arrValues['account_verification_log_id'] );
		if( isset( $arrValues['allocate_ar_transaction_ids'] ) && $boolDirectSet ) $this->set( 'm_strAllocateArTransactionIds', trim( stripcslashes( $arrValues['allocate_ar_transaction_ids'] ) ) ); elseif( isset( $arrValues['allocate_ar_transaction_ids'] ) ) $this->setAllocateArTransactionIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allocate_ar_transaction_ids'] ) : $arrValues['allocate_ar_transaction_ids'] );
		if( isset( $arrValues['secure_reference_number'] ) && $boolDirectSet ) $this->set( 'm_intSecureReferenceNumber', trim( $arrValues['secure_reference_number'] ) ); elseif( isset( $arrValues['secure_reference_number'] ) ) $this->setSecureReferenceNumber( $arrValues['secure_reference_number'] );
		if( isset( $arrValues['payment_memo'] ) && $boolDirectSet ) $this->set( 'm_strPaymentMemo', trim( stripcslashes( $arrValues['payment_memo'] ) ) ); elseif( isset( $arrValues['payment_memo'] ) ) $this->setPaymentMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payment_memo'] ) : $arrValues['payment_memo'] );
		if( isset( $arrValues['billto_ip_address'] ) && $boolDirectSet ) $this->set( 'm_strBilltoIpAddress', trim( stripcslashes( $arrValues['billto_ip_address'] ) ) ); elseif( isset( $arrValues['billto_ip_address'] ) ) $this->setBilltoIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_ip_address'] ) : $arrValues['billto_ip_address'] );
		if( isset( $arrValues['billto_ip_address_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBilltoIpAddressEncrypted', trim( stripcslashes( $arrValues['billto_ip_address_encrypted'] ) ) ); elseif( isset( $arrValues['billto_ip_address_encrypted'] ) ) $this->setBilltoIpAddressEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_ip_address_encrypted'] ) : $arrValues['billto_ip_address_encrypted'] );
		if( isset( $arrValues['billto_account_number'] ) && $boolDirectSet ) $this->set( 'm_strBilltoAccountNumber', trim( stripcslashes( $arrValues['billto_account_number'] ) ) ); elseif( isset( $arrValues['billto_account_number'] ) ) $this->setBilltoAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_account_number'] ) : $arrValues['billto_account_number'] );
		if( isset( $arrValues['billto_company_name'] ) && $boolDirectSet ) $this->set( 'm_strBilltoCompanyName', trim( stripcslashes( $arrValues['billto_company_name'] ) ) ); elseif( isset( $arrValues['billto_company_name'] ) ) $this->setBilltoCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_company_name'] ) : $arrValues['billto_company_name'] );
		if( isset( $arrValues['billto_street_line1'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBilltoStreetLine1', trim( stripcslashes( $arrValues['billto_street_line1'] ) ) ); elseif( isset( $arrValues['billto_street_line1'] ) ) $this->setBilltoStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_street_line1'] ) : $arrValues['billto_street_line1'] );
		if( isset( $arrValues['billto_street_line2'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBilltoStreetLine2', trim( stripcslashes( $arrValues['billto_street_line2'] ) ) ); elseif( isset( $arrValues['billto_street_line2'] ) ) $this->setBilltoStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_street_line2'] ) : $arrValues['billto_street_line2'] );
		if( isset( $arrValues['billto_street_line3'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBilltoStreetLine3', trim( stripcslashes( $arrValues['billto_street_line3'] ) ) ); elseif( isset( $arrValues['billto_street_line3'] ) ) $this->setBilltoStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_street_line3'] ) : $arrValues['billto_street_line3'] );
		if( isset( $arrValues['billto_city'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBilltoCity', trim( stripcslashes( $arrValues['billto_city'] ) ) ); elseif( isset( $arrValues['billto_city'] ) ) $this->setBilltoCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_city'] ) : $arrValues['billto_city'] );
		if( isset( $arrValues['billto_state_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBilltoStateCode', trim( stripcslashes( $arrValues['billto_state_code'] ) ) ); elseif( isset( $arrValues['billto_state_code'] ) ) $this->setBilltoStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_state_code'] ) : $arrValues['billto_state_code'] );
		if( isset( $arrValues['billto_province'] ) && $boolDirectSet ) $this->set( 'm_strBilltoProvince', trim( stripcslashes( $arrValues['billto_province'] ) ) ); elseif( isset( $arrValues['billto_province'] ) ) $this->setBilltoProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_province'] ) : $arrValues['billto_province'] );
		if( isset( $arrValues['billto_postal_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBilltoPostalCode', trim( stripcslashes( $arrValues['billto_postal_code'] ) ) ); elseif( isset( $arrValues['billto_postal_code'] ) ) $this->setBilltoPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_postal_code'] ) : $arrValues['billto_postal_code'] );
		if( isset( $arrValues['billto_country_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBilltoCountryCode', trim( stripcslashes( $arrValues['billto_country_code'] ) ) ); elseif( isset( $arrValues['billto_country_code'] ) ) $this->setBilltoCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_country_code'] ) : $arrValues['billto_country_code'] );
		if( isset( $arrValues['billto_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strBilltoPhoneNumber', trim( stripcslashes( $arrValues['billto_phone_number'] ) ) ); elseif( isset( $arrValues['billto_phone_number'] ) ) $this->setBilltoPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_phone_number'] ) : $arrValues['billto_phone_number'] );
		if( isset( $arrValues['billto_phone_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBilltoPhoneNumberEncrypted', trim( stripcslashes( $arrValues['billto_phone_number_encrypted'] ) ) ); elseif( isset( $arrValues['billto_phone_number_encrypted'] ) ) $this->setBilltoPhoneNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_phone_number_encrypted'] ) : $arrValues['billto_phone_number_encrypted'] );
		if( isset( $arrValues['billto_email_address'] ) && $boolDirectSet ) $this->set( 'm_strBilltoEmailAddress', trim( stripcslashes( $arrValues['billto_email_address'] ) ) ); elseif( isset( $arrValues['billto_email_address'] ) ) $this->setBilltoEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_email_address'] ) : $arrValues['billto_email_address'] );
		if( isset( $arrValues['billto_email_address_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBilltoEmailAddressEncrypted', trim( stripcslashes( $arrValues['billto_email_address_encrypted'] ) ) ); elseif( isset( $arrValues['billto_email_address_encrypted'] ) ) $this->setBilltoEmailAddressEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['billto_email_address_encrypted'] ) : $arrValues['billto_email_address_encrypted'] );
		if( isset( $arrValues['car_lar_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCarLarAmount', trim( $arrValues['car_lar_amount'] ) ); elseif( isset( $arrValues['car_lar_amount'] ) ) $this->setCarLarAmount( $arrValues['car_lar_amount'] );
		if( isset( $arrValues['car_lar_confidence'] ) && $boolDirectSet ) $this->set( 'm_intCarLarConfidence', trim( $arrValues['car_lar_confidence'] ) ); elseif( isset( $arrValues['car_lar_confidence'] ) ) $this->setCarLarConfidence( $arrValues['car_lar_confidence'] );
		if( isset( $arrValues['car_lar_is_money_order'] ) && $boolDirectSet ) $this->set( 'm_intCarLarIsMoneyOrder', trim( $arrValues['car_lar_is_money_order'] ) ); elseif( isset( $arrValues['car_lar_is_money_order'] ) ) $this->setCarLarIsMoneyOrder( $arrValues['car_lar_is_money_order'] );
		if( isset( $arrValues['car_lar_is_performed'] ) && $boolDirectSet ) $this->set( 'm_intCarLarIsPerformed', trim( $arrValues['car_lar_is_performed'] ) ); elseif( isset( $arrValues['car_lar_is_performed'] ) ) $this->setCarLarIsPerformed( $arrValues['car_lar_is_performed'] );
		if( isset( $arrValues['phone_auth_required'] ) && $boolDirectSet ) $this->set( 'm_intPhoneAuthRequired', trim( $arrValues['phone_auth_required'] ) ); elseif( isset( $arrValues['phone_auth_required'] ) ) $this->setPhoneAuthRequired( $arrValues['phone_auth_required'] );
		if( isset( $arrValues['phone_auth_by'] ) && $boolDirectSet ) $this->set( 'm_intPhoneAuthBy', trim( $arrValues['phone_auth_by'] ) ); elseif( isset( $arrValues['phone_auth_by'] ) ) $this->setPhoneAuthBy( $arrValues['phone_auth_by'] );
		if( isset( $arrValues['phone_auth_on'] ) && $boolDirectSet ) $this->set( 'm_strPhoneAuthOn', trim( $arrValues['phone_auth_on'] ) ); elseif( isset( $arrValues['phone_auth_on'] ) ) $this->setPhoneAuthOn( $arrValues['phone_auth_on'] );
		if( isset( $arrValues['requires_approval'] ) && $boolDirectSet ) $this->set( 'm_intRequiresApproval', trim( $arrValues['requires_approval'] ) ); elseif( isset( $arrValues['requires_approval'] ) ) $this->setRequiresApproval( $arrValues['requires_approval'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['return_exported_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnExportedOn', trim( $arrValues['return_exported_on'] ) ); elseif( isset( $arrValues['return_exported_on'] ) ) $this->setReturnExportedOn( $arrValues['return_exported_on'] );
		if( isset( $arrValues['return_batched_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnBatchedOn', trim( $arrValues['return_batched_on'] ) ); elseif( isset( $arrValues['return_batched_on'] ) ) $this->setReturnBatchedOn( $arrValues['return_batched_on'] );
		if( isset( $arrValues['fee_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strFeePostedOn', trim( $arrValues['fee_posted_on'] ) ); elseif( isset( $arrValues['fee_posted_on'] ) ) $this->setFeePostedOn( $arrValues['fee_posted_on'] );
		if( isset( $arrValues['return_fee_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnFeePostedOn', trim( $arrValues['return_fee_posted_on'] ) ); elseif( isset( $arrValues['return_fee_posted_on'] ) ) $this->setReturnFeePostedOn( $arrValues['return_fee_posted_on'] );
		if( isset( $arrValues['reverse_fee_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strReverseFeePostedOn', trim( $arrValues['reverse_fee_posted_on'] ) ); elseif( isset( $arrValues['reverse_fee_posted_on'] ) ) $this->setReverseFeePostedOn( $arrValues['reverse_fee_posted_on'] );
		if( isset( $arrValues['charge_back_fee_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strChargeBackFeePostedOn', trim( $arrValues['charge_back_fee_posted_on'] ) ); elseif( isset( $arrValues['charge_back_fee_posted_on'] ) ) $this->setChargeBackFeePostedOn( $arrValues['charge_back_fee_posted_on'] );
		if( isset( $arrValues['is_application_payment'] ) && $boolDirectSet ) $this->set( 'm_intIsApplicationPayment', trim( $arrValues['is_application_payment'] ) ); elseif( isset( $arrValues['is_application_payment'] ) ) $this->setIsApplicationPayment( $arrValues['is_application_payment'] );
		if( isset( $arrValues['is_debit_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsDebitCard', trim( stripcslashes( $arrValues['is_debit_card'] ) ) ); elseif( isset( $arrValues['is_debit_card'] ) ) $this->setIsDebitCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_debit_card'] ) : $arrValues['is_debit_card'] );
		if( isset( $arrValues['is_check_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsCheckCard', trim( stripcslashes( $arrValues['is_check_card'] ) ) ); elseif( isset( $arrValues['is_check_card'] ) ) $this->setIsCheckCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_check_card'] ) : $arrValues['is_check_card'] );
		if( isset( $arrValues['is_gift_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsGiftCard', trim( stripcslashes( $arrValues['is_gift_card'] ) ) ); elseif( isset( $arrValues['is_gift_card'] ) ) $this->setIsGiftCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_gift_card'] ) : $arrValues['is_gift_card'] );
		if( isset( $arrValues['is_prepaid_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsPrepaidCard', trim( stripcslashes( $arrValues['is_prepaid_card'] ) ) ); elseif( isset( $arrValues['is_prepaid_card'] ) ) $this->setIsPrepaidCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_prepaid_card'] ) : $arrValues['is_prepaid_card'] );
		if( isset( $arrValues['is_credit_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsCreditCard', trim( stripcslashes( $arrValues['is_credit_card'] ) ) ); elseif( isset( $arrValues['is_credit_card'] ) ) $this->setIsCreditCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_credit_card'] ) : $arrValues['is_credit_card'] );
		if( isset( $arrValues['is_international_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsInternationalCard', trim( stripcslashes( $arrValues['is_international_card'] ) ) ); elseif( isset( $arrValues['is_international_card'] ) ) $this->setIsInternationalCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_international_card'] ) : $arrValues['is_international_card'] );
		if( isset( $arrValues['is_commercial_card'] ) && $boolDirectSet ) $this->set( 'm_boolIsCommercialCard', trim( stripcslashes( $arrValues['is_commercial_card'] ) ) ); elseif( isset( $arrValues['is_commercial_card'] ) ) $this->setIsCommercialCard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_commercial_card'] ) : $arrValues['is_commercial_card'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['remote_payment_number'] ) && $boolDirectSet ) $this->set( 'm_strRemotePaymentNumber', trim( stripcslashes( $arrValues['remote_payment_number'] ) ) ); elseif( isset( $arrValues['remote_payment_number'] ) ) $this->setRemotePaymentNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_payment_number'] ) : $arrValues['remote_payment_number'] );
		if( isset( $arrValues['is_risk_review'] ) && $boolDirectSet ) $this->set( 'm_boolIsRiskReview', trim( stripcslashes( $arrValues['is_risk_review'] ) ) ); elseif( isset( $arrValues['is_risk_review'] ) ) $this->setIsRiskReview( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_risk_review'] ) : $arrValues['is_risk_review'] );
		if( isset( $arrValues['unauthorized_return_fee_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strUnauthorizedReturnFeePostedOn', trim( $arrValues['unauthorized_return_fee_posted_on'] ) ); elseif( isset( $arrValues['unauthorized_return_fee_posted_on'] ) ) $this->setUnauthorizedReturnFeePostedOn( $arrValues['unauthorized_return_fee_posted_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( $this->m_boolInitialized ) {
			$this->setPostalAddressValues( $arrValues, $this->m_arrstrPostalAddressFields );
		}

		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setArPaymentId( $intArPaymentId ) {
		$this->set( 'm_intArPaymentId', CStrings::strToIntDef( $intArPaymentId, NULL, false ) );
	}

	public function getArPaymentId() {
		return $this->m_intArPaymentId;
	}

	public function sqlArPaymentId() {
		return ( true == isset( $this->m_intArPaymentId ) ) ? ( string ) $this->m_intArPaymentId : 'NULL';
	}

	public function setAllocationTypeId( $intAllocationTypeId ) {
		$this->set( 'm_intAllocationTypeId', CStrings::strToIntDef( $intAllocationTypeId, NULL, false ) );
	}

	public function getAllocationTypeId() {
		return $this->m_intAllocationTypeId;
	}

	public function sqlAllocationTypeId() {
		return ( true == isset( $this->m_intAllocationTypeId ) ) ? ( string ) $this->m_intAllocationTypeId : '1';
	}

	public function setAccountVerificationLogId( $intAccountVerificationLogId ) {
		$this->set( 'm_intAccountVerificationLogId', CStrings::strToIntDef( $intAccountVerificationLogId, NULL, false ) );
	}

	public function getAccountVerificationLogId() {
		return $this->m_intAccountVerificationLogId;
	}

	public function sqlAccountVerificationLogId() {
		return ( true == isset( $this->m_intAccountVerificationLogId ) ) ? ( string ) $this->m_intAccountVerificationLogId : 'NULL';
	}

	public function setAllocateArTransactionIds( $strAllocateArTransactionIds ) {
		$this->set( 'm_strAllocateArTransactionIds', CStrings::strTrimDef( $strAllocateArTransactionIds, -1, NULL, true ) );
	}

	public function getAllocateArTransactionIds() {
		return $this->m_strAllocateArTransactionIds;
	}

	public function sqlAllocateArTransactionIds() {
		return ( true == isset( $this->m_strAllocateArTransactionIds ) ) ? '\'' . addslashes( $this->m_strAllocateArTransactionIds ) . '\'' : 'NULL';
	}

	public function setSecureReferenceNumber( $intSecureReferenceNumber ) {
		$this->set( 'm_intSecureReferenceNumber', CStrings::strToIntDef( $intSecureReferenceNumber, NULL, false ) );
	}

	public function getSecureReferenceNumber() {
		return $this->m_intSecureReferenceNumber;
	}

	public function sqlSecureReferenceNumber() {
		return ( true == isset( $this->m_intSecureReferenceNumber ) ) ? ( string ) $this->m_intSecureReferenceNumber : 'NULL';
	}

	public function setPaymentMemo( $strPaymentMemo ) {
		$this->set( 'm_strPaymentMemo', CStrings::strTrimDef( $strPaymentMemo, 2000, NULL, true ) );
	}

	public function getPaymentMemo() {
		return $this->m_strPaymentMemo;
	}

	public function sqlPaymentMemo() {
		return ( true == isset( $this->m_strPaymentMemo ) ) ? '\'' . addslashes( $this->m_strPaymentMemo ) . '\'' : 'NULL';
	}

	public function setBilltoIpAddress( $strBilltoIpAddress ) {
		$this->set( 'm_strBilltoIpAddress', CStrings::strTrimDef( $strBilltoIpAddress, 23, NULL, true ) );
	}

	public function getBilltoIpAddress() {
		return $this->m_strBilltoIpAddress;
	}

	public function sqlBilltoIpAddress() {
		return ( true == isset( $this->m_strBilltoIpAddress ) ) ? '\'' . addslashes( $this->m_strBilltoIpAddress ) . '\'' : 'NULL';
	}

	public function setBilltoIpAddressEncrypted( $strBilltoIpAddressEncrypted ) {
		$this->set( 'm_strBilltoIpAddressEncrypted', CStrings::strTrimDef( $strBilltoIpAddressEncrypted, 240, NULL, true ) );
	}

	public function getBilltoIpAddressEncrypted() {
		return $this->m_strBilltoIpAddressEncrypted;
	}

	public function sqlBilltoIpAddressEncrypted() {
		return ( true == isset( $this->m_strBilltoIpAddressEncrypted ) ) ? '\'' . addslashes( $this->m_strBilltoIpAddressEncrypted ) . '\'' : 'NULL';
	}

	public function setBilltoAccountNumber( $strBilltoAccountNumber ) {
		$this->set( 'm_strBilltoAccountNumber', CStrings::strTrimDef( $strBilltoAccountNumber, 50, NULL, true ) );
	}

	public function getBilltoAccountNumber() {
		return $this->m_strBilltoAccountNumber;
	}

	public function sqlBilltoAccountNumber() {
		return ( true == isset( $this->m_strBilltoAccountNumber ) ) ? '\'' . addslashes( $this->m_strBilltoAccountNumber ) . '\'' : 'NULL';
	}

	public function setBilltoCompanyName( $strBilltoCompanyName ) {
		$this->set( 'm_strBilltoCompanyName', CStrings::strTrimDef( $strBilltoCompanyName, 100, NULL, true ) );
	}

	public function getBilltoCompanyName() {
		return $this->m_strBilltoCompanyName;
	}

	public function sqlBilltoCompanyName() {
		return ( true == isset( $this->m_strBilltoCompanyName ) ) ? '\'' . addslashes( $this->m_strBilltoCompanyName ) . '\'' : 'NULL';
	}

	public function setBilltoStreetLine1( $strBilltoStreetLine1 ) {
		$this->setPostalAddressField( 'addressLine1', $strBilltoStreetLine1, $strAddressKey = 'billto', 'm_strBilltoStreetLine1' );
	}

	public function getBilltoStreetLine1() {
		return $this->getPostalAddressField( 'addressLine1', 'billto', 'm_strBilltoStreetLine1' );
	}

	public function sqlBilltoStreetLine1() {
		return ( true == isset( $this->m_strBilltoStreetLine1 ) ) ? '\'' . addslashes( $this->m_strBilltoStreetLine1 ) . '\'' : 'NULL';
	}

	public function setBilltoStreetLine2( $strBilltoStreetLine2 ) {
		$this->setPostalAddressField( 'addressLine2', $strBilltoStreetLine2, $strAddressKey = 'billto', 'm_strBilltoStreetLine2' );
	}

	public function getBilltoStreetLine2() {
		return $this->getPostalAddressField( 'addressLine2', 'billto', 'm_strBilltoStreetLine2' );
	}

	public function sqlBilltoStreetLine2() {
		return ( true == isset( $this->m_strBilltoStreetLine2 ) ) ? '\'' . addslashes( $this->m_strBilltoStreetLine2 ) . '\'' : 'NULL';
	}

	public function setBilltoStreetLine3( $strBilltoStreetLine3 ) {
		$this->setPostalAddressField( 'addressLine3', $strBilltoStreetLine3, $strAddressKey = 'billto', 'm_strBilltoStreetLine3' );
	}

	public function getBilltoStreetLine3() {
		return $this->getPostalAddressField( 'addressLine3', 'billto', 'm_strBilltoStreetLine3' );
	}

	public function sqlBilltoStreetLine3() {
		return ( true == isset( $this->m_strBilltoStreetLine3 ) ) ? '\'' . addslashes( $this->m_strBilltoStreetLine3 ) . '\'' : 'NULL';
	}

	public function setBilltoCity( $strBilltoCity ) {
		$this->setPostalAddressField( 'locality', $strBilltoCity, $strAddressKey = 'billto', 'm_strBilltoCity' );
	}

	public function getBilltoCity() {
		return $this->getPostalAddressField( 'locality', 'billto', 'm_strBilltoCity' );
	}

	public function sqlBilltoCity() {
		return ( true == isset( $this->m_strBilltoCity ) ) ? '\'' . addslashes( $this->m_strBilltoCity ) . '\'' : 'NULL';
	}

	public function setBilltoStateCode( $strBilltoStateCode ) {
		$this->setPostalAddressField( 'administrativeArea', $strBilltoStateCode, $strAddressKey = 'billto', 'm_strBilltoStateCode' );
	}

	public function getBilltoStateCode() {
		return $this->getPostalAddressField( 'administrativeArea', 'billto', 'm_strBilltoStateCode' );
	}

	public function sqlBilltoStateCode() {
		return ( true == isset( $this->m_strBilltoStateCode ) ) ? '\'' . addslashes( $this->m_strBilltoStateCode ) . '\'' : 'NULL';
	}

	public function setBilltoProvince( $strBilltoProvince ) {
		$this->set( 'm_strBilltoProvince', CStrings::strTrimDef( $strBilltoProvince, 50, NULL, true ) );
	}

	public function getBilltoProvince() {
		return $this->m_strBilltoProvince;
	}

	public function sqlBilltoProvince() {
		return ( true == isset( $this->m_strBilltoProvince ) ) ? '\'' . addslashes( $this->m_strBilltoProvince ) . '\'' : 'NULL';
	}

	public function setBilltoPostalCode( $strBilltoPostalCode ) {
		$this->setPostalAddressField( 'postalCode', $strBilltoPostalCode, $strAddressKey = 'billto', 'm_strBilltoPostalCode' );
	}

	public function getBilltoPostalCode() {
		return $this->getPostalAddressField( 'postalCode', 'billto', 'm_strBilltoPostalCode' );
	}

	public function sqlBilltoPostalCode() {
		return ( true == isset( $this->m_strBilltoPostalCode ) ) ? '\'' . addslashes( $this->m_strBilltoPostalCode ) . '\'' : 'NULL';
	}

	public function setBilltoCountryCode( $strBilltoCountryCode ) {
		$this->setPostalAddressField( 'country', $strBilltoCountryCode, $strAddressKey = 'billto', 'm_strBilltoCountryCode' );
	}

	public function getBilltoCountryCode() {
		return $this->getPostalAddressField( 'country', 'billto', 'm_strBilltoCountryCode' );
	}

	public function sqlBilltoCountryCode() {
		return ( true == isset( $this->m_strBilltoCountryCode ) ) ? '\'' . addslashes( $this->m_strBilltoCountryCode ) . '\'' : 'NULL';
	}

	public function setBilltoPhoneNumber( $strBilltoPhoneNumber ) {
		$this->set( 'm_strBilltoPhoneNumber', CStrings::strTrimDef( $strBilltoPhoneNumber, 30, NULL, true ) );
	}

	public function getBilltoPhoneNumber() {
		return $this->m_strBilltoPhoneNumber;
	}

	public function sqlBilltoPhoneNumber() {
		return ( true == isset( $this->m_strBilltoPhoneNumber ) ) ? '\'' . addslashes( $this->m_strBilltoPhoneNumber ) . '\'' : 'NULL';
	}

	public function setBilltoPhoneNumberEncrypted( $strBilltoPhoneNumberEncrypted ) {
		$this->set( 'm_strBilltoPhoneNumberEncrypted', CStrings::strTrimDef( $strBilltoPhoneNumberEncrypted, 240, NULL, true ) );
	}

	public function getBilltoPhoneNumberEncrypted() {
		return $this->m_strBilltoPhoneNumberEncrypted;
	}

	public function sqlBilltoPhoneNumberEncrypted() {
		return ( true == isset( $this->m_strBilltoPhoneNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strBilltoPhoneNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setBilltoEmailAddress( $strBilltoEmailAddress ) {
		$this->set( 'm_strBilltoEmailAddress', CStrings::strTrimDef( $strBilltoEmailAddress, 240, NULL, true ) );
	}

	public function getBilltoEmailAddress() {
		return $this->m_strBilltoEmailAddress;
	}

	public function sqlBilltoEmailAddress() {
		return ( true == isset( $this->m_strBilltoEmailAddress ) ) ? '\'' . addslashes( $this->m_strBilltoEmailAddress ) . '\'' : 'NULL';
	}

	public function setBilltoEmailAddressEncrypted( $strBilltoEmailAddressEncrypted ) {
		$this->set( 'm_strBilltoEmailAddressEncrypted', CStrings::strTrimDef( $strBilltoEmailAddressEncrypted, 550, NULL, true ) );
	}

	public function getBilltoEmailAddressEncrypted() {
		return $this->m_strBilltoEmailAddressEncrypted;
	}

	public function sqlBilltoEmailAddressEncrypted() {
		return ( true == isset( $this->m_strBilltoEmailAddressEncrypted ) ) ? '\'' . addslashes( $this->m_strBilltoEmailAddressEncrypted ) . '\'' : 'NULL';
	}

	public function setCarLarAmount( $fltCarLarAmount ) {
		$this->set( 'm_fltCarLarAmount', CStrings::strToFloatDef( $fltCarLarAmount, NULL, false, 2 ) );
	}

	public function getCarLarAmount() {
		return $this->m_fltCarLarAmount;
	}

	public function sqlCarLarAmount() {
		return ( true == isset( $this->m_fltCarLarAmount ) ) ? ( string ) $this->m_fltCarLarAmount : 'NULL';
	}

	public function setCarLarConfidence( $intCarLarConfidence ) {
		$this->set( 'm_intCarLarConfidence', CStrings::strToIntDef( $intCarLarConfidence, NULL, false ) );
	}

	public function getCarLarConfidence() {
		return $this->m_intCarLarConfidence;
	}

	public function sqlCarLarConfidence() {
		return ( true == isset( $this->m_intCarLarConfidence ) ) ? ( string ) $this->m_intCarLarConfidence : 'NULL';
	}

	public function setCarLarIsMoneyOrder( $intCarLarIsMoneyOrder ) {
		$this->set( 'm_intCarLarIsMoneyOrder', CStrings::strToIntDef( $intCarLarIsMoneyOrder, NULL, false ) );
	}

	public function getCarLarIsMoneyOrder() {
		return $this->m_intCarLarIsMoneyOrder;
	}

	public function sqlCarLarIsMoneyOrder() {
		return ( true == isset( $this->m_intCarLarIsMoneyOrder ) ) ? ( string ) $this->m_intCarLarIsMoneyOrder : '0';
	}

	public function setCarLarIsPerformed( $intCarLarIsPerformed ) {
		$this->set( 'm_intCarLarIsPerformed', CStrings::strToIntDef( $intCarLarIsPerformed, NULL, false ) );
	}

	public function getCarLarIsPerformed() {
		return $this->m_intCarLarIsPerformed;
	}

	public function sqlCarLarIsPerformed() {
		return ( true == isset( $this->m_intCarLarIsPerformed ) ) ? ( string ) $this->m_intCarLarIsPerformed : '0';
	}

	public function setPhoneAuthRequired( $intPhoneAuthRequired ) {
		$this->set( 'm_intPhoneAuthRequired', CStrings::strToIntDef( $intPhoneAuthRequired, NULL, false ) );
	}

	public function getPhoneAuthRequired() {
		return $this->m_intPhoneAuthRequired;
	}

	public function sqlPhoneAuthRequired() {
		return ( true == isset( $this->m_intPhoneAuthRequired ) ) ? ( string ) $this->m_intPhoneAuthRequired : '0';
	}

	public function setPhoneAuthBy( $intPhoneAuthBy ) {
		$this->set( 'm_intPhoneAuthBy', CStrings::strToIntDef( $intPhoneAuthBy, NULL, false ) );
	}

	public function getPhoneAuthBy() {
		return $this->m_intPhoneAuthBy;
	}

	public function sqlPhoneAuthBy() {
		return ( true == isset( $this->m_intPhoneAuthBy ) ) ? ( string ) $this->m_intPhoneAuthBy : 'NULL';
	}

	public function setPhoneAuthOn( $strPhoneAuthOn ) {
		$this->set( 'm_strPhoneAuthOn', CStrings::strTrimDef( $strPhoneAuthOn, -1, NULL, true ) );
	}

	public function getPhoneAuthOn() {
		return $this->m_strPhoneAuthOn;
	}

	public function sqlPhoneAuthOn() {
		return ( true == isset( $this->m_strPhoneAuthOn ) ) ? '\'' . $this->m_strPhoneAuthOn . '\'' : 'NULL';
	}

	public function setRequiresApproval( $intRequiresApproval ) {
		$this->set( 'm_intRequiresApproval', CStrings::strToIntDef( $intRequiresApproval, NULL, false ) );
	}

	public function getRequiresApproval() {
		return $this->m_intRequiresApproval;
	}

	public function sqlRequiresApproval() {
		return ( true == isset( $this->m_intRequiresApproval ) ) ? ( string ) $this->m_intRequiresApproval : '0';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setReturnExportedOn( $strReturnExportedOn ) {
		$this->set( 'm_strReturnExportedOn', CStrings::strTrimDef( $strReturnExportedOn, -1, NULL, true ) );
	}

	public function getReturnExportedOn() {
		return $this->m_strReturnExportedOn;
	}

	public function sqlReturnExportedOn() {
		return ( true == isset( $this->m_strReturnExportedOn ) ) ? '\'' . $this->m_strReturnExportedOn . '\'' : 'NULL';
	}

	public function setReturnBatchedOn( $strReturnBatchedOn ) {
		$this->set( 'm_strReturnBatchedOn', CStrings::strTrimDef( $strReturnBatchedOn, -1, NULL, true ) );
	}

	public function getReturnBatchedOn() {
		return $this->m_strReturnBatchedOn;
	}

	public function sqlReturnBatchedOn() {
		return ( true == isset( $this->m_strReturnBatchedOn ) ) ? '\'' . $this->m_strReturnBatchedOn . '\'' : 'NULL';
	}

	public function setFeePostedOn( $strFeePostedOn ) {
		$this->set( 'm_strFeePostedOn', CStrings::strTrimDef( $strFeePostedOn, -1, NULL, true ) );
	}

	public function getFeePostedOn() {
		return $this->m_strFeePostedOn;
	}

	public function sqlFeePostedOn() {
		return ( true == isset( $this->m_strFeePostedOn ) ) ? '\'' . $this->m_strFeePostedOn . '\'' : 'NULL';
	}

	public function setReturnFeePostedOn( $strReturnFeePostedOn ) {
		$this->set( 'm_strReturnFeePostedOn', CStrings::strTrimDef( $strReturnFeePostedOn, -1, NULL, true ) );
	}

	public function getReturnFeePostedOn() {
		return $this->m_strReturnFeePostedOn;
	}

	public function sqlReturnFeePostedOn() {
		return ( true == isset( $this->m_strReturnFeePostedOn ) ) ? '\'' . $this->m_strReturnFeePostedOn . '\'' : 'NULL';
	}

	public function setReverseFeePostedOn( $strReverseFeePostedOn ) {
		$this->set( 'm_strReverseFeePostedOn', CStrings::strTrimDef( $strReverseFeePostedOn, -1, NULL, true ) );
	}

	public function getReverseFeePostedOn() {
		return $this->m_strReverseFeePostedOn;
	}

	public function sqlReverseFeePostedOn() {
		return ( true == isset( $this->m_strReverseFeePostedOn ) ) ? '\'' . $this->m_strReverseFeePostedOn . '\'' : 'NULL';
	}

	public function setChargeBackFeePostedOn( $strChargeBackFeePostedOn ) {
		$this->set( 'm_strChargeBackFeePostedOn', CStrings::strTrimDef( $strChargeBackFeePostedOn, -1, NULL, true ) );
	}

	public function getChargeBackFeePostedOn() {
		return $this->m_strChargeBackFeePostedOn;
	}

	public function sqlChargeBackFeePostedOn() {
		return ( true == isset( $this->m_strChargeBackFeePostedOn ) ) ? '\'' . $this->m_strChargeBackFeePostedOn . '\'' : 'NULL';
	}

	public function setIsApplicationPayment( $intIsApplicationPayment ) {
		$this->set( 'm_intIsApplicationPayment', CStrings::strToIntDef( $intIsApplicationPayment, NULL, false ) );
	}

	public function getIsApplicationPayment() {
		return $this->m_intIsApplicationPayment;
	}

	public function sqlIsApplicationPayment() {
		return ( true == isset( $this->m_intIsApplicationPayment ) ) ? ( string ) $this->m_intIsApplicationPayment : '0';
	}

	public function setIsDebitCard( $boolIsDebitCard ) {
		$this->set( 'm_boolIsDebitCard', CStrings::strToBool( $boolIsDebitCard ) );
	}

	public function getIsDebitCard() {
		return $this->m_boolIsDebitCard;
	}

	public function sqlIsDebitCard() {
		return ( true == isset( $this->m_boolIsDebitCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDebitCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCheckCard( $boolIsCheckCard ) {
		$this->set( 'm_boolIsCheckCard', CStrings::strToBool( $boolIsCheckCard ) );
	}

	public function getIsCheckCard() {
		return $this->m_boolIsCheckCard;
	}

	public function sqlIsCheckCard() {
		return ( true == isset( $this->m_boolIsCheckCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCheckCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsGiftCard( $boolIsGiftCard ) {
		$this->set( 'm_boolIsGiftCard', CStrings::strToBool( $boolIsGiftCard ) );
	}

	public function getIsGiftCard() {
		return $this->m_boolIsGiftCard;
	}

	public function sqlIsGiftCard() {
		return ( true == isset( $this->m_boolIsGiftCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsGiftCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPrepaidCard( $boolIsPrepaidCard ) {
		$this->set( 'm_boolIsPrepaidCard', CStrings::strToBool( $boolIsPrepaidCard ) );
	}

	public function getIsPrepaidCard() {
		return $this->m_boolIsPrepaidCard;
	}

	public function sqlIsPrepaidCard() {
		return ( true == isset( $this->m_boolIsPrepaidCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPrepaidCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCreditCard( $boolIsCreditCard ) {
		$this->set( 'm_boolIsCreditCard', CStrings::strToBool( $boolIsCreditCard ) );
	}

	public function getIsCreditCard() {
		return $this->m_boolIsCreditCard;
	}

	public function sqlIsCreditCard() {
		return ( true == isset( $this->m_boolIsCreditCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCreditCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsInternationalCard( $boolIsInternationalCard ) {
		$this->set( 'm_boolIsInternationalCard', CStrings::strToBool( $boolIsInternationalCard ) );
	}

	public function getIsInternationalCard() {
		return $this->m_boolIsInternationalCard;
	}

	public function sqlIsInternationalCard() {
		return ( true == isset( $this->m_boolIsInternationalCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInternationalCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCommercialCard( $boolIsCommercialCard ) {
		$this->set( 'm_boolIsCommercialCard', CStrings::strToBool( $boolIsCommercialCard ) );
	}

	public function getIsCommercialCard() {
		return $this->m_boolIsCommercialCard;
	}

	public function sqlIsCommercialCard() {
		return ( true == isset( $this->m_boolIsCommercialCard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCommercialCard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setRemotePaymentNumber( $strRemotePaymentNumber ) {
		$this->set( 'm_strRemotePaymentNumber', CStrings::strTrimDef( $strRemotePaymentNumber, 64, NULL, true ) );
	}

	public function getRemotePaymentNumber() {
		return $this->m_strRemotePaymentNumber;
	}

	public function sqlRemotePaymentNumber() {
		return ( true == isset( $this->m_strRemotePaymentNumber ) ) ? '\'' . addslashes( $this->m_strRemotePaymentNumber ) . '\'' : 'NULL';
	}

	public function setIsRiskReview( $boolIsRiskReview ) {
		$this->set( 'm_boolIsRiskReview', CStrings::strToBool( $boolIsRiskReview ) );
	}

	public function getIsRiskReview() {
		return $this->m_boolIsRiskReview;
	}

	public function sqlIsRiskReview() {
		return ( true == isset( $this->m_boolIsRiskReview ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRiskReview ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUnauthorizedReturnFeePostedOn( $strUnauthorizedReturnFeePostedOn ) {
		$this->set( 'm_strUnauthorizedReturnFeePostedOn', CStrings::strTrimDef( $strUnauthorizedReturnFeePostedOn, -1, NULL, true ) );
	}

	public function getUnauthorizedReturnFeePostedOn() {
		return $this->m_strUnauthorizedReturnFeePostedOn;
	}

	public function sqlUnauthorizedReturnFeePostedOn() {
		return ( true == isset( $this->m_strUnauthorizedReturnFeePostedOn ) ) ? '\'' . $this->m_strUnauthorizedReturnFeePostedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ar_payment_id, allocation_type_id, account_verification_log_id, allocate_ar_transaction_ids, secure_reference_number, payment_memo, billto_ip_address, billto_ip_address_encrypted, billto_account_number, billto_company_name, billto_street_line1, billto_street_line2, billto_street_line3, billto_city, billto_state_code, billto_province, billto_postal_code, billto_country_code, billto_phone_number, billto_phone_number_encrypted, billto_email_address, billto_email_address_encrypted, car_lar_amount, car_lar_confidence, car_lar_is_money_order, car_lar_is_performed, phone_auth_required, phone_auth_by, phone_auth_on, requires_approval, approved_by, approved_on, exported_on, return_exported_on, return_batched_on, fee_posted_on, return_fee_posted_on, reverse_fee_posted_on, charge_back_fee_posted_on, is_application_payment, is_debit_card, is_check_card, is_gift_card, is_prepaid_card, is_credit_card, is_international_card, is_commercial_card, updated_by, updated_on, created_by, created_on, remote_payment_number, is_risk_review, unauthorized_return_fee_posted_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlArPaymentId() . ', ' .
						$this->sqlAllocationTypeId() . ', ' .
						$this->sqlAccountVerificationLogId() . ', ' .
						$this->sqlAllocateArTransactionIds() . ', ' .
						$this->sqlSecureReferenceNumber() . ', ' .
						$this->sqlPaymentMemo() . ', ' .
						$this->sqlBilltoIpAddress() . ', ' .
						$this->sqlBilltoIpAddressEncrypted() . ', ' .
						$this->sqlBilltoAccountNumber() . ', ' .
						$this->sqlBilltoCompanyName() . ', ' .
						$this->sqlBilltoStreetLine1() . ', ' .
						$this->sqlBilltoStreetLine2() . ', ' .
						$this->sqlBilltoStreetLine3() . ', ' .
						$this->sqlBilltoCity() . ', ' .
						$this->sqlBilltoStateCode() . ', ' .
						$this->sqlBilltoProvince() . ', ' .
						$this->sqlBilltoPostalCode() . ', ' .
						$this->sqlBilltoCountryCode() . ', ' .
						$this->sqlBilltoPhoneNumber() . ', ' .
						$this->sqlBilltoPhoneNumberEncrypted() . ', ' .
						$this->sqlBilltoEmailAddress() . ', ' .
						$this->sqlBilltoEmailAddressEncrypted() . ', ' .
						$this->sqlCarLarAmount() . ', ' .
						$this->sqlCarLarConfidence() . ', ' .
						$this->sqlCarLarIsMoneyOrder() . ', ' .
						$this->sqlCarLarIsPerformed() . ', ' .
						$this->sqlPhoneAuthRequired() . ', ' .
						$this->sqlPhoneAuthBy() . ', ' .
						$this->sqlPhoneAuthOn() . ', ' .
						$this->sqlRequiresApproval() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						$this->sqlExportedOn() . ', ' .
						$this->sqlReturnExportedOn() . ', ' .
						$this->sqlReturnBatchedOn() . ', ' .
						$this->sqlFeePostedOn() . ', ' .
						$this->sqlReturnFeePostedOn() . ', ' .
						$this->sqlReverseFeePostedOn() . ', ' .
						$this->sqlChargeBackFeePostedOn() . ', ' .
						$this->sqlIsApplicationPayment() . ', ' .
						$this->sqlIsDebitCard() . ', ' .
						$this->sqlIsCheckCard() . ', ' .
						$this->sqlIsGiftCard() . ', ' .
						$this->sqlIsPrepaidCard() . ', ' .
						$this->sqlIsCreditCard() . ', ' .
						$this->sqlIsInternationalCard() . ', ' .
						$this->sqlIsCommercialCard() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlRemotePaymentNumber() . ', ' .
						$this->sqlIsRiskReview() . ', ' .
						$this->sqlUnauthorizedReturnFeePostedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId(). ',' ; } elseif( true == array_key_exists( 'ArPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allocation_type_id = ' . $this->sqlAllocationTypeId(). ',' ; } elseif( true == array_key_exists( 'AllocationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' allocation_type_id = ' . $this->sqlAllocationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_verification_log_id = ' . $this->sqlAccountVerificationLogId(). ',' ; } elseif( true == array_key_exists( 'AccountVerificationLogId', $this->getChangedColumns() ) ) { $strSql .= ' account_verification_log_id = ' . $this->sqlAccountVerificationLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allocate_ar_transaction_ids = ' . $this->sqlAllocateArTransactionIds(). ',' ; } elseif( true == array_key_exists( 'AllocateArTransactionIds', $this->getChangedColumns() ) ) { $strSql .= ' allocate_ar_transaction_ids = ' . $this->sqlAllocateArTransactionIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' secure_reference_number = ' . $this->sqlSecureReferenceNumber(). ',' ; } elseif( true == array_key_exists( 'SecureReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' secure_reference_number = ' . $this->sqlSecureReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_memo = ' . $this->sqlPaymentMemo(). ',' ; } elseif( true == array_key_exists( 'PaymentMemo', $this->getChangedColumns() ) ) { $strSql .= ' payment_memo = ' . $this->sqlPaymentMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_ip_address = ' . $this->sqlBilltoIpAddress(). ',' ; } elseif( true == array_key_exists( 'BilltoIpAddress', $this->getChangedColumns() ) ) { $strSql .= ' billto_ip_address = ' . $this->sqlBilltoIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_ip_address_encrypted = ' . $this->sqlBilltoIpAddressEncrypted(). ',' ; } elseif( true == array_key_exists( 'BilltoIpAddressEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' billto_ip_address_encrypted = ' . $this->sqlBilltoIpAddressEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_account_number = ' . $this->sqlBilltoAccountNumber(). ',' ; } elseif( true == array_key_exists( 'BilltoAccountNumber', $this->getChangedColumns() ) ) { $strSql .= ' billto_account_number = ' . $this->sqlBilltoAccountNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_company_name = ' . $this->sqlBilltoCompanyName(). ',' ; } elseif( true == array_key_exists( 'BilltoCompanyName', $this->getChangedColumns() ) ) { $strSql .= ' billto_company_name = ' . $this->sqlBilltoCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_street_line1 = ' . $this->sqlBilltoStreetLine1(). ',' ; } elseif( true == array_key_exists( 'BilltoStreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' billto_street_line1 = ' . $this->sqlBilltoStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_street_line2 = ' . $this->sqlBilltoStreetLine2(). ',' ; } elseif( true == array_key_exists( 'BilltoStreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' billto_street_line2 = ' . $this->sqlBilltoStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_street_line3 = ' . $this->sqlBilltoStreetLine3(). ',' ; } elseif( true == array_key_exists( 'BilltoStreetLine3', $this->getChangedColumns() ) ) { $strSql .= ' billto_street_line3 = ' . $this->sqlBilltoStreetLine3() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_city = ' . $this->sqlBilltoCity(). ',' ; } elseif( true == array_key_exists( 'BilltoCity', $this->getChangedColumns() ) ) { $strSql .= ' billto_city = ' . $this->sqlBilltoCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_state_code = ' . $this->sqlBilltoStateCode(). ',' ; } elseif( true == array_key_exists( 'BilltoStateCode', $this->getChangedColumns() ) ) { $strSql .= ' billto_state_code = ' . $this->sqlBilltoStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_province = ' . $this->sqlBilltoProvince(). ',' ; } elseif( true == array_key_exists( 'BilltoProvince', $this->getChangedColumns() ) ) { $strSql .= ' billto_province = ' . $this->sqlBilltoProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_postal_code = ' . $this->sqlBilltoPostalCode(). ',' ; } elseif( true == array_key_exists( 'BilltoPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' billto_postal_code = ' . $this->sqlBilltoPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_country_code = ' . $this->sqlBilltoCountryCode(). ',' ; } elseif( true == array_key_exists( 'BilltoCountryCode', $this->getChangedColumns() ) ) { $strSql .= ' billto_country_code = ' . $this->sqlBilltoCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_phone_number = ' . $this->sqlBilltoPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'BilltoPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' billto_phone_number = ' . $this->sqlBilltoPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_phone_number_encrypted = ' . $this->sqlBilltoPhoneNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'BilltoPhoneNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' billto_phone_number_encrypted = ' . $this->sqlBilltoPhoneNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_email_address = ' . $this->sqlBilltoEmailAddress(). ',' ; } elseif( true == array_key_exists( 'BilltoEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' billto_email_address = ' . $this->sqlBilltoEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_email_address_encrypted = ' . $this->sqlBilltoEmailAddressEncrypted(). ',' ; } elseif( true == array_key_exists( 'BilltoEmailAddressEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' billto_email_address_encrypted = ' . $this->sqlBilltoEmailAddressEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' car_lar_amount = ' . $this->sqlCarLarAmount(). ',' ; } elseif( true == array_key_exists( 'CarLarAmount', $this->getChangedColumns() ) ) { $strSql .= ' car_lar_amount = ' . $this->sqlCarLarAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' car_lar_confidence = ' . $this->sqlCarLarConfidence(). ',' ; } elseif( true == array_key_exists( 'CarLarConfidence', $this->getChangedColumns() ) ) { $strSql .= ' car_lar_confidence = ' . $this->sqlCarLarConfidence() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' car_lar_is_money_order = ' . $this->sqlCarLarIsMoneyOrder(). ',' ; } elseif( true == array_key_exists( 'CarLarIsMoneyOrder', $this->getChangedColumns() ) ) { $strSql .= ' car_lar_is_money_order = ' . $this->sqlCarLarIsMoneyOrder() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' car_lar_is_performed = ' . $this->sqlCarLarIsPerformed(). ',' ; } elseif( true == array_key_exists( 'CarLarIsPerformed', $this->getChangedColumns() ) ) { $strSql .= ' car_lar_is_performed = ' . $this->sqlCarLarIsPerformed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_auth_required = ' . $this->sqlPhoneAuthRequired(). ',' ; } elseif( true == array_key_exists( 'PhoneAuthRequired', $this->getChangedColumns() ) ) { $strSql .= ' phone_auth_required = ' . $this->sqlPhoneAuthRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_auth_by = ' . $this->sqlPhoneAuthBy(). ',' ; } elseif( true == array_key_exists( 'PhoneAuthBy', $this->getChangedColumns() ) ) { $strSql .= ' phone_auth_by = ' . $this->sqlPhoneAuthBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_auth_on = ' . $this->sqlPhoneAuthOn(). ',' ; } elseif( true == array_key_exists( 'PhoneAuthOn', $this->getChangedColumns() ) ) { $strSql .= ' phone_auth_on = ' . $this->sqlPhoneAuthOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requires_approval = ' . $this->sqlRequiresApproval(). ',' ; } elseif( true == array_key_exists( 'RequiresApproval', $this->getChangedColumns() ) ) { $strSql .= ' requires_approval = ' . $this->sqlRequiresApproval() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn(). ',' ; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_exported_on = ' . $this->sqlReturnExportedOn(). ',' ; } elseif( true == array_key_exists( 'ReturnExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' return_exported_on = ' . $this->sqlReturnExportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_batched_on = ' . $this->sqlReturnBatchedOn(). ',' ; } elseif( true == array_key_exists( 'ReturnBatchedOn', $this->getChangedColumns() ) ) { $strSql .= ' return_batched_on = ' . $this->sqlReturnBatchedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fee_posted_on = ' . $this->sqlFeePostedOn(). ',' ; } elseif( true == array_key_exists( 'FeePostedOn', $this->getChangedColumns() ) ) { $strSql .= ' fee_posted_on = ' . $this->sqlFeePostedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_fee_posted_on = ' . $this->sqlReturnFeePostedOn(). ',' ; } elseif( true == array_key_exists( 'ReturnFeePostedOn', $this->getChangedColumns() ) ) { $strSql .= ' return_fee_posted_on = ' . $this->sqlReturnFeePostedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reverse_fee_posted_on = ' . $this->sqlReverseFeePostedOn(). ',' ; } elseif( true == array_key_exists( 'ReverseFeePostedOn', $this->getChangedColumns() ) ) { $strSql .= ' reverse_fee_posted_on = ' . $this->sqlReverseFeePostedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_back_fee_posted_on = ' . $this->sqlChargeBackFeePostedOn(). ',' ; } elseif( true == array_key_exists( 'ChargeBackFeePostedOn', $this->getChangedColumns() ) ) { $strSql .= ' charge_back_fee_posted_on = ' . $this->sqlChargeBackFeePostedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_application_payment = ' . $this->sqlIsApplicationPayment(). ',' ; } elseif( true == array_key_exists( 'IsApplicationPayment', $this->getChangedColumns() ) ) { $strSql .= ' is_application_payment = ' . $this->sqlIsApplicationPayment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_debit_card = ' . $this->sqlIsDebitCard(). ',' ; } elseif( true == array_key_exists( 'IsDebitCard', $this->getChangedColumns() ) ) { $strSql .= ' is_debit_card = ' . $this->sqlIsDebitCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_check_card = ' . $this->sqlIsCheckCard(). ',' ; } elseif( true == array_key_exists( 'IsCheckCard', $this->getChangedColumns() ) ) { $strSql .= ' is_check_card = ' . $this->sqlIsCheckCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_gift_card = ' . $this->sqlIsGiftCard(). ',' ; } elseif( true == array_key_exists( 'IsGiftCard', $this->getChangedColumns() ) ) { $strSql .= ' is_gift_card = ' . $this->sqlIsGiftCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_prepaid_card = ' . $this->sqlIsPrepaidCard(). ',' ; } elseif( true == array_key_exists( 'IsPrepaidCard', $this->getChangedColumns() ) ) { $strSql .= ' is_prepaid_card = ' . $this->sqlIsPrepaidCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_credit_card = ' . $this->sqlIsCreditCard(). ',' ; } elseif( true == array_key_exists( 'IsCreditCard', $this->getChangedColumns() ) ) { $strSql .= ' is_credit_card = ' . $this->sqlIsCreditCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_international_card = ' . $this->sqlIsInternationalCard(). ',' ; } elseif( true == array_key_exists( 'IsInternationalCard', $this->getChangedColumns() ) ) { $strSql .= ' is_international_card = ' . $this->sqlIsInternationalCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_commercial_card = ' . $this->sqlIsCommercialCard(). ',' ; } elseif( true == array_key_exists( 'IsCommercialCard', $this->getChangedColumns() ) ) { $strSql .= ' is_commercial_card = ' . $this->sqlIsCommercialCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_payment_number = ' . $this->sqlRemotePaymentNumber(). ',' ; } elseif( true == array_key_exists( 'RemotePaymentNumber', $this->getChangedColumns() ) ) { $strSql .= ' remote_payment_number = ' . $this->sqlRemotePaymentNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_risk_review = ' . $this->sqlIsRiskReview(). ',' ; } elseif( true == array_key_exists( 'IsRiskReview', $this->getChangedColumns() ) ) { $strSql .= ' is_risk_review = ' . $this->sqlIsRiskReview() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unauthorized_return_fee_posted_on = ' . $this->sqlUnauthorizedReturnFeePostedOn(). ',' ; } elseif( true == array_key_exists( 'UnauthorizedReturnFeePostedOn', $this->getChangedColumns() ) ) { $strSql .= ' unauthorized_return_fee_posted_on = ' . $this->sqlUnauthorizedReturnFeePostedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ar_payment_id' => $this->getArPaymentId(),
			'allocation_type_id' => $this->getAllocationTypeId(),
			'account_verification_log_id' => $this->getAccountVerificationLogId(),
			'allocate_ar_transaction_ids' => $this->getAllocateArTransactionIds(),
			'secure_reference_number' => $this->getSecureReferenceNumber(),
			'payment_memo' => $this->getPaymentMemo(),
			'billto_ip_address' => $this->getBilltoIpAddress(),
			'billto_ip_address_encrypted' => $this->getBilltoIpAddressEncrypted(),
			'billto_account_number' => $this->getBilltoAccountNumber(),
			'billto_company_name' => $this->getBilltoCompanyName(),
			'billto_street_line1' => $this->getBilltoStreetLine1(),
			'billto_street_line2' => $this->getBilltoStreetLine2(),
			'billto_street_line3' => $this->getBilltoStreetLine3(),
			'billto_city' => $this->getBilltoCity(),
			'billto_state_code' => $this->getBilltoStateCode(),
			'billto_province' => $this->getBilltoProvince(),
			'billto_postal_code' => $this->getBilltoPostalCode(),
			'billto_country_code' => $this->getBilltoCountryCode(),
			'billto_phone_number' => $this->getBilltoPhoneNumber(),
			'billto_phone_number_encrypted' => $this->getBilltoPhoneNumberEncrypted(),
			'billto_email_address' => $this->getBilltoEmailAddress(),
			'billto_email_address_encrypted' => $this->getBilltoEmailAddressEncrypted(),
			'car_lar_amount' => $this->getCarLarAmount(),
			'car_lar_confidence' => $this->getCarLarConfidence(),
			'car_lar_is_money_order' => $this->getCarLarIsMoneyOrder(),
			'car_lar_is_performed' => $this->getCarLarIsPerformed(),
			'phone_auth_required' => $this->getPhoneAuthRequired(),
			'phone_auth_by' => $this->getPhoneAuthBy(),
			'phone_auth_on' => $this->getPhoneAuthOn(),
			'requires_approval' => $this->getRequiresApproval(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'exported_on' => $this->getExportedOn(),
			'return_exported_on' => $this->getReturnExportedOn(),
			'return_batched_on' => $this->getReturnBatchedOn(),
			'fee_posted_on' => $this->getFeePostedOn(),
			'return_fee_posted_on' => $this->getReturnFeePostedOn(),
			'reverse_fee_posted_on' => $this->getReverseFeePostedOn(),
			'charge_back_fee_posted_on' => $this->getChargeBackFeePostedOn(),
			'is_application_payment' => $this->getIsApplicationPayment(),
			'is_debit_card' => $this->getIsDebitCard(),
			'is_check_card' => $this->getIsCheckCard(),
			'is_gift_card' => $this->getIsGiftCard(),
			'is_prepaid_card' => $this->getIsPrepaidCard(),
			'is_credit_card' => $this->getIsCreditCard(),
			'is_international_card' => $this->getIsInternationalCard(),
			'is_commercial_card' => $this->getIsCommercialCard(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'remote_payment_number' => $this->getRemotePaymentNumber(),
			'is_risk_review' => $this->getIsRiskReview(),
			'unauthorized_return_fee_posted_on' => $this->getUnauthorizedReturnFeePostedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>