<?php

class CBaseSubsidyIncomeLimitArea extends CEosSingularBase {

	const TABLE_NAME = 'public.subsidy_income_limit_areas';

	protected $m_intId;
	protected $m_intSubsidyIncomeLimitVersionId;
	protected $m_strHmfaCode;
	protected $m_strMetroAreaName;
	protected $m_intMedianIncome;
	protected $m_boolIsMetro;
	protected $m_boolIsHera;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intMedianIncome = '0';
		$this->m_boolIsMetro = false;
		$this->m_boolIsHera = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['subsidy_income_limit_version_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyIncomeLimitVersionId', trim( $arrValues['subsidy_income_limit_version_id'] ) ); elseif( isset( $arrValues['subsidy_income_limit_version_id'] ) ) $this->setSubsidyIncomeLimitVersionId( $arrValues['subsidy_income_limit_version_id'] );
		if( isset( $arrValues['hmfa_code'] ) && $boolDirectSet ) $this->set( 'm_strHmfaCode', trim( stripcslashes( $arrValues['hmfa_code'] ) ) ); elseif( isset( $arrValues['hmfa_code'] ) ) $this->setHmfaCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hmfa_code'] ) : $arrValues['hmfa_code'] );
		if( isset( $arrValues['metro_area_name'] ) && $boolDirectSet ) $this->set( 'm_strMetroAreaName', trim( stripcslashes( $arrValues['metro_area_name'] ) ) ); elseif( isset( $arrValues['metro_area_name'] ) ) $this->setMetroAreaName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['metro_area_name'] ) : $arrValues['metro_area_name'] );
		if( isset( $arrValues['median_income'] ) && $boolDirectSet ) $this->set( 'm_intMedianIncome', trim( $arrValues['median_income'] ) ); elseif( isset( $arrValues['median_income'] ) ) $this->setMedianIncome( $arrValues['median_income'] );
		if( isset( $arrValues['is_metro'] ) && $boolDirectSet ) $this->set( 'm_boolIsMetro', trim( stripcslashes( $arrValues['is_metro'] ) ) ); elseif( isset( $arrValues['is_metro'] ) ) $this->setIsMetro( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_metro'] ) : $arrValues['is_metro'] );
		if( isset( $arrValues['is_hera'] ) && $boolDirectSet ) $this->set( 'm_boolIsHera', trim( stripcslashes( $arrValues['is_hera'] ) ) ); elseif( isset( $arrValues['is_hera'] ) ) $this->setIsHera( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_hera'] ) : $arrValues['is_hera'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSubsidyIncomeLimitVersionId( $intSubsidyIncomeLimitVersionId ) {
		$this->set( 'm_intSubsidyIncomeLimitVersionId', CStrings::strToIntDef( $intSubsidyIncomeLimitVersionId, NULL, false ) );
	}

	public function getSubsidyIncomeLimitVersionId() {
		return $this->m_intSubsidyIncomeLimitVersionId;
	}

	public function sqlSubsidyIncomeLimitVersionId() {
		return ( true == isset( $this->m_intSubsidyIncomeLimitVersionId ) ) ? ( string ) $this->m_intSubsidyIncomeLimitVersionId : 'NULL';
	}

	public function setHmfaCode( $strHmfaCode ) {
		$this->set( 'm_strHmfaCode', CStrings::strTrimDef( $strHmfaCode, 16, NULL, true ) );
	}

	public function getHmfaCode() {
		return $this->m_strHmfaCode;
	}

	public function sqlHmfaCode() {
		return ( true == isset( $this->m_strHmfaCode ) ) ? '\'' . addslashes( $this->m_strHmfaCode ) . '\'' : 'NULL';
	}

	public function setMetroAreaName( $strMetroAreaName ) {
		$this->set( 'm_strMetroAreaName', CStrings::strTrimDef( $strMetroAreaName, 100, NULL, true ) );
	}

	public function getMetroAreaName() {
		return $this->m_strMetroAreaName;
	}

	public function sqlMetroAreaName() {
		return ( true == isset( $this->m_strMetroAreaName ) ) ? '\'' . addslashes( $this->m_strMetroAreaName ) . '\'' : 'NULL';
	}

	public function setMedianIncome( $intMedianIncome ) {
		$this->set( 'm_intMedianIncome', CStrings::strToIntDef( $intMedianIncome, NULL, false ) );
	}

	public function getMedianIncome() {
		return $this->m_intMedianIncome;
	}

	public function sqlMedianIncome() {
		return ( true == isset( $this->m_intMedianIncome ) ) ? ( string ) $this->m_intMedianIncome : '0';
	}

	public function setIsMetro( $boolIsMetro ) {
		$this->set( 'm_boolIsMetro', CStrings::strToBool( $boolIsMetro ) );
	}

	public function getIsMetro() {
		return $this->m_boolIsMetro;
	}

	public function sqlIsMetro() {
		return ( true == isset( $this->m_boolIsMetro ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsMetro ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsHera( $boolIsHera ) {
		$this->set( 'm_boolIsHera', CStrings::strToBool( $boolIsHera ) );
	}

	public function getIsHera() {
		return $this->m_boolIsHera;
	}

	public function sqlIsHera() {
		return ( true == isset( $this->m_boolIsHera ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHera ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, subsidy_income_limit_version_id, hmfa_code, metro_area_name, median_income, is_metro, is_hera, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlSubsidyIncomeLimitVersionId() . ', ' .
 						$this->sqlHmfaCode() . ', ' .
 						$this->sqlMetroAreaName() . ', ' .
 						$this->sqlMedianIncome() . ', ' .
 						$this->sqlIsMetro() . ', ' .
 						$this->sqlIsHera() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_income_limit_version_id = ' . $this->sqlSubsidyIncomeLimitVersionId() . ','; } elseif( true == array_key_exists( 'SubsidyIncomeLimitVersionId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_income_limit_version_id = ' . $this->sqlSubsidyIncomeLimitVersionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hmfa_code = ' . $this->sqlHmfaCode() . ','; } elseif( true == array_key_exists( 'HmfaCode', $this->getChangedColumns() ) ) { $strSql .= ' hmfa_code = ' . $this->sqlHmfaCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' metro_area_name = ' . $this->sqlMetroAreaName() . ','; } elseif( true == array_key_exists( 'MetroAreaName', $this->getChangedColumns() ) ) { $strSql .= ' metro_area_name = ' . $this->sqlMetroAreaName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' median_income = ' . $this->sqlMedianIncome() . ','; } elseif( true == array_key_exists( 'MedianIncome', $this->getChangedColumns() ) ) { $strSql .= ' median_income = ' . $this->sqlMedianIncome() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_metro = ' . $this->sqlIsMetro() . ','; } elseif( true == array_key_exists( 'IsMetro', $this->getChangedColumns() ) ) { $strSql .= ' is_metro = ' . $this->sqlIsMetro() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_hera = ' . $this->sqlIsHera() . ','; } elseif( true == array_key_exists( 'IsHera', $this->getChangedColumns() ) ) { $strSql .= ' is_hera = ' . $this->sqlIsHera() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'subsidy_income_limit_version_id' => $this->getSubsidyIncomeLimitVersionId(),
			'hmfa_code' => $this->getHmfaCode(),
			'metro_area_name' => $this->getMetroAreaName(),
			'median_income' => $this->getMedianIncome(),
			'is_metro' => $this->getIsMetro(),
			'is_hera' => $this->getIsHera(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>