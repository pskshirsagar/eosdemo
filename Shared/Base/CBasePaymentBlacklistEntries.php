<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CPaymentBlacklistEntries
 * Do not add any new functions to this class.
 */

class CBasePaymentBlacklistEntries extends CEosPluralBase {

	/**
	 * @return CPaymentBlacklistEntry[]
	 */
	public static function fetchPaymentBlacklistEntries( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPaymentBlacklistEntry::class, $objDatabase );
	}

	/**
	 * @return CPaymentBlacklistEntry
	 */
	public static function fetchPaymentBlacklistEntry( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPaymentBlacklistEntry::class, $objDatabase );
	}

	public static function fetchPaymentBlacklistEntryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'payment_blacklist_entries', $objDatabase );
	}

	public static function fetchPaymentBlacklistEntryById( $intId, $objDatabase ) {
		return self::fetchPaymentBlacklistEntry( sprintf( 'SELECT * FROM payment_blacklist_entries WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPaymentBlacklistEntriesByPaymentBlacklistTypeId( $intPaymentBlacklistTypeId, $objDatabase ) {
		return self::fetchPaymentBlacklistEntries( sprintf( 'SELECT * FROM payment_blacklist_entries WHERE payment_blacklist_type_id = %d', ( int ) $intPaymentBlacklistTypeId ), $objDatabase );
	}

	public static function fetchPaymentBlacklistEntriesBySystemEmailId( $intSystemEmailId, $objDatabase ) {
		return self::fetchPaymentBlacklistEntries( sprintf( 'SELECT * FROM payment_blacklist_entries WHERE system_email_id = %d', ( int ) $intSystemEmailId ), $objDatabase );
	}

}
?>