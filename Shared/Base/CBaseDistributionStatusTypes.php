<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CDistributionStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseDistributionStatusTypes extends CEosPluralBase {

	/**
	 * @return CDistributionStatusType[]
	 */
	public static function fetchDistributionStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDistributionStatusType::class, $objDatabase );
	}

	/**
	 * @return CDistributionStatusType
	 */
	public static function fetchDistributionStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDistributionStatusType::class, $objDatabase );
	}

	public static function fetchDistributionStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'distribution_status_types', $objDatabase );
	}

	public static function fetchDistributionStatusTypeById( $intId, $objDatabase ) {
		return self::fetchDistributionStatusType( sprintf( 'SELECT * FROM distribution_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>