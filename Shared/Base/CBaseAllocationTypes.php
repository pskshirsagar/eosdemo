<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CAllocationTypes
 * Do not add any new functions to this class.
 */

class CBaseAllocationTypes extends CEosPluralBase {

	/**
	 * @return CAllocationType[]
	 */
	public static function fetchAllocationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CAllocationType::class, $objDatabase );
	}

	/**
	 * @return CAllocationType
	 */
	public static function fetchAllocationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CAllocationType::class, $objDatabase );
	}

	public static function fetchAllocationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'allocation_types', $objDatabase );
	}

	public static function fetchAllocationTypeById( $intId, $objDatabase ) {
		return self::fetchAllocationType( sprintf( 'SELECT * FROM allocation_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>