<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CMilitaryRanks
 * Do not add any new functions to this class.
 */

class CBaseMilitaryRanks extends CEosPluralBase {

	/**
	 * @return CMilitaryRank[]
	 */
	public static function fetchMilitaryRanks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMilitaryRank::class, $objDatabase );
	}

	/**
	 * @return CMilitaryRank
	 */
	public static function fetchMilitaryRank( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMilitaryRank::class, $objDatabase );
	}

	public static function fetchMilitaryRankCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'military_ranks', $objDatabase );
	}

	public static function fetchMilitaryRankById( $intId, $objDatabase ) {
		return self::fetchMilitaryRank( sprintf( 'SELECT * FROM military_ranks WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchMilitaryRanksByMilitaryComponentId( $intMilitaryComponentId, $objDatabase ) {
		return self::fetchMilitaryRanks( sprintf( 'SELECT * FROM military_ranks WHERE military_component_id = %d', $intMilitaryComponentId ), $objDatabase );
	}

	public static function fetchMilitaryRanksByPayGradeId( $intPayGradeId, $objDatabase ) {
		return self::fetchMilitaryRanks( sprintf( 'SELECT * FROM military_ranks WHERE pay_grade_id = %d', $intPayGradeId ), $objDatabase );
	}

}
?>