<?php

class CBaseIpsChecklistQuestion extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.ips_checklist_questions';

	protected $m_intId;
	protected $m_intIpsChecklistId;
	protected $m_intReferenceKeyId;
	protected $m_strQuestion;
	protected $m_boolIsRequiresSync;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intChecklistQuestionId;
	protected $m_boolIsClientVisible;
	protected $m_boolIsPublished;
	protected $m_intIpsChecklistQuestionTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsRequiresSync = true;
		$this->m_intOrderNum = '0';
		$this->m_boolIsClientVisible = false;
		$this->m_boolIsPublished = true;
		$this->m_intIpsChecklistQuestionTypeId = '4';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ips_checklist_id'] ) && $boolDirectSet ) $this->set( 'm_intIpsChecklistId', trim( $arrValues['ips_checklist_id'] ) ); elseif( isset( $arrValues['ips_checklist_id'] ) ) $this->setIpsChecklistId( $arrValues['ips_checklist_id'] );
		if( isset( $arrValues['reference_key_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceKeyId', trim( $arrValues['reference_key_id'] ) ); elseif( isset( $arrValues['reference_key_id'] ) ) $this->setReferenceKeyId( $arrValues['reference_key_id'] );
		if( isset( $arrValues['question'] ) && $boolDirectSet ) $this->set( 'm_strQuestion', trim( $arrValues['question'] ) ); elseif( isset( $arrValues['question'] ) ) $this->setQuestion( $arrValues['question'] );
		if( isset( $arrValues['is_requires_sync'] ) && $boolDirectSet ) $this->set( 'm_boolIsRequiresSync', trim( stripcslashes( $arrValues['is_requires_sync'] ) ) ); elseif( isset( $arrValues['is_requires_sync'] ) ) $this->setIsRequiresSync( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_requires_sync'] ) : $arrValues['is_requires_sync'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['checklist_question_id'] ) && $boolDirectSet ) $this->set( 'm_intChecklistQuestionId', trim( $arrValues['checklist_question_id'] ) ); elseif( isset( $arrValues['checklist_question_id'] ) ) $this->setChecklistQuestionId( $arrValues['checklist_question_id'] );
		if( isset( $arrValues['is_client_visible'] ) && $boolDirectSet ) $this->set( 'm_boolIsClientVisible', trim( stripcslashes( $arrValues['is_client_visible'] ) ) ); elseif( isset( $arrValues['is_client_visible'] ) ) $this->setIsClientVisible( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_client_visible'] ) : $arrValues['is_client_visible'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['ips_checklist_question_type_id'] ) && $boolDirectSet ) $this->set( 'm_intIpsChecklistQuestionTypeId', trim( $arrValues['ips_checklist_question_type_id'] ) ); elseif( isset( $arrValues['ips_checklist_question_type_id'] ) ) $this->setIpsChecklistQuestionTypeId( $arrValues['ips_checklist_question_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setIpsChecklistId( $intIpsChecklistId ) {
		$this->set( 'm_intIpsChecklistId', CStrings::strToIntDef( $intIpsChecklistId, NULL, false ) );
	}

	public function getIpsChecklistId() {
		return $this->m_intIpsChecklistId;
	}

	public function sqlIpsChecklistId() {
		return ( true == isset( $this->m_intIpsChecklistId ) ) ? ( string ) $this->m_intIpsChecklistId : 'NULL';
	}

	public function setReferenceKeyId( $intReferenceKeyId ) {
		$this->set( 'm_intReferenceKeyId', CStrings::strToIntDef( $intReferenceKeyId, NULL, false ) );
	}

	public function getReferenceKeyId() {
		return $this->m_intReferenceKeyId;
	}

	public function sqlReferenceKeyId() {
		return ( true == isset( $this->m_intReferenceKeyId ) ) ? ( string ) $this->m_intReferenceKeyId : 'NULL';
	}

	public function setQuestion( $strQuestion ) {
		$this->set( 'm_strQuestion', CStrings::strTrimDef( $strQuestion, -1, NULL, true ) );
	}

	public function getQuestion() {
		return $this->m_strQuestion;
	}

	public function sqlQuestion() {
		return ( true == isset( $this->m_strQuestion ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strQuestion ) : '\'' . addslashes( $this->m_strQuestion ) . '\'' ) : 'NULL';
	}

	public function setIsRequiresSync( $boolIsRequiresSync ) {
		$this->set( 'm_boolIsRequiresSync', CStrings::strToBool( $boolIsRequiresSync ) );
	}

	public function getIsRequiresSync() {
		return $this->m_boolIsRequiresSync;
	}

	public function sqlIsRequiresSync() {
		return ( true == isset( $this->m_boolIsRequiresSync ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRequiresSync ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setChecklistQuestionId( $intChecklistQuestionId ) {
		$this->set( 'm_intChecklistQuestionId', CStrings::strToIntDef( $intChecklistQuestionId, NULL, false ) );
	}

	public function getChecklistQuestionId() {
		return $this->m_intChecklistQuestionId;
	}

	public function sqlChecklistQuestionId() {
		return ( true == isset( $this->m_intChecklistQuestionId ) ) ? ( string ) $this->m_intChecklistQuestionId : 'NULL';
	}

	public function setIsClientVisible( $boolIsClientVisible ) {
		$this->set( 'm_boolIsClientVisible', CStrings::strToBool( $boolIsClientVisible ) );
	}

	public function getIsClientVisible() {
		return $this->m_boolIsClientVisible;
	}

	public function sqlIsClientVisible() {
		return ( true == isset( $this->m_boolIsClientVisible ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsClientVisible ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIpsChecklistQuestionTypeId( $intIpsChecklistQuestionTypeId ) {
		$this->set( 'm_intIpsChecklistQuestionTypeId', CStrings::strToIntDef( $intIpsChecklistQuestionTypeId, NULL, false ) );
	}

	public function getIpsChecklistQuestionTypeId() {
		return $this->m_intIpsChecklistQuestionTypeId;
	}

	public function sqlIpsChecklistQuestionTypeId() {
		return ( true == isset( $this->m_intIpsChecklistQuestionTypeId ) ) ? ( string ) $this->m_intIpsChecklistQuestionTypeId : '4';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ips_checklist_id, reference_key_id, question, is_requires_sync, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details, checklist_question_id, is_client_visible, is_published, ips_checklist_question_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlIpsChecklistId() . ', ' .
						$this->sqlReferenceKeyId() . ', ' .
						$this->sqlQuestion() . ', ' .
						$this->sqlIsRequiresSync() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlChecklistQuestionId() . ', ' .
						$this->sqlIsClientVisible() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIpsChecklistQuestionTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ips_checklist_id = ' . $this->sqlIpsChecklistId(). ',' ; } elseif( true == array_key_exists( 'IpsChecklistId', $this->getChangedColumns() ) ) { $strSql .= ' ips_checklist_id = ' . $this->sqlIpsChecklistId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_key_id = ' . $this->sqlReferenceKeyId(). ',' ; } elseif( true == array_key_exists( 'ReferenceKeyId', $this->getChangedColumns() ) ) { $strSql .= ' reference_key_id = ' . $this->sqlReferenceKeyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' question = ' . $this->sqlQuestion(). ',' ; } elseif( true == array_key_exists( 'Question', $this->getChangedColumns() ) ) { $strSql .= ' question = ' . $this->sqlQuestion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_requires_sync = ' . $this->sqlIsRequiresSync(). ',' ; } elseif( true == array_key_exists( 'IsRequiresSync', $this->getChangedColumns() ) ) { $strSql .= ' is_requires_sync = ' . $this->sqlIsRequiresSync() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' checklist_question_id = ' . $this->sqlChecklistQuestionId(). ',' ; } elseif( true == array_key_exists( 'ChecklistQuestionId', $this->getChangedColumns() ) ) { $strSql .= ' checklist_question_id = ' . $this->sqlChecklistQuestionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_client_visible = ' . $this->sqlIsClientVisible(). ',' ; } elseif( true == array_key_exists( 'IsClientVisible', $this->getChangedColumns() ) ) { $strSql .= ' is_client_visible = ' . $this->sqlIsClientVisible() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ips_checklist_question_type_id = ' . $this->sqlIpsChecklistQuestionTypeId(). ',' ; } elseif( true == array_key_exists( 'IpsChecklistQuestionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ips_checklist_question_type_id = ' . $this->sqlIpsChecklistQuestionTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ips_checklist_id' => $this->getIpsChecklistId(),
			'reference_key_id' => $this->getReferenceKeyId(),
			'question' => $this->getQuestion(),
			'is_requires_sync' => $this->getIsRequiresSync(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'checklist_question_id' => $this->getChecklistQuestionId(),
			'is_client_visible' => $this->getIsClientVisible(),
			'is_published' => $this->getIsPublished(),
			'ips_checklist_question_type_id' => $this->getIpsChecklistQuestionTypeId()
		);
	}

}
?>