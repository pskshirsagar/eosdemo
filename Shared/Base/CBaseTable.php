<?php

class CBaseTable extends CEosSingularBase {

	const TABLE_NAME = 'public.tables';

	protected $m_intId;
	protected $m_intDatabaseId;
	protected $m_intPsProductId;
	protected $m_intTeamId;
	protected $m_strTableName;
	protected $m_strDescription;
	protected $m_strKeywords;
	protected $m_strLastOptimized;
	protected $m_intHasFilter;
	protected $m_boolIsCloudSync;
	protected $m_intIsComposite;
	protected $m_intIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intHasFilter = '0';
		$this->m_boolIsCloudSync = false;
		$this->m_intIsComposite = '0';
		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['database_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseId', trim( $arrValues['database_id'] ) ); elseif( isset( $arrValues['database_id'] ) ) $this->setDatabaseId( $arrValues['database_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['team_id'] ) && $boolDirectSet ) $this->set( 'm_intTeamId', trim( $arrValues['team_id'] ) ); elseif( isset( $arrValues['team_id'] ) ) $this->setTeamId( $arrValues['team_id'] );
		if( isset( $arrValues['table_name'] ) && $boolDirectSet ) $this->set( 'm_strTableName', trim( stripcslashes( $arrValues['table_name'] ) ) ); elseif( isset( $arrValues['table_name'] ) ) $this->setTableName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['table_name'] ) : $arrValues['table_name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['keywords'] ) && $boolDirectSet ) $this->set( 'm_strKeywords', trim( stripcslashes( $arrValues['keywords'] ) ) ); elseif( isset( $arrValues['keywords'] ) ) $this->setKeywords( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['keywords'] ) : $arrValues['keywords'] );
		if( isset( $arrValues['last_optimized'] ) && $boolDirectSet ) $this->set( 'm_strLastOptimized', trim( $arrValues['last_optimized'] ) ); elseif( isset( $arrValues['last_optimized'] ) ) $this->setLastOptimized( $arrValues['last_optimized'] );
		if( isset( $arrValues['has_filter'] ) && $boolDirectSet ) $this->set( 'm_intHasFilter', trim( $arrValues['has_filter'] ) ); elseif( isset( $arrValues['has_filter'] ) ) $this->setHasFilter( $arrValues['has_filter'] );
		if( isset( $arrValues['is_cloud_sync'] ) && $boolDirectSet ) $this->set( 'm_boolIsCloudSync', trim( stripcslashes( $arrValues['is_cloud_sync'] ) ) ); elseif( isset( $arrValues['is_cloud_sync'] ) ) $this->setIsCloudSync( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_cloud_sync'] ) : $arrValues['is_cloud_sync'] );
		if( isset( $arrValues['is_composite'] ) && $boolDirectSet ) $this->set( 'm_intIsComposite', trim( $arrValues['is_composite'] ) ); elseif( isset( $arrValues['is_composite'] ) ) $this->setIsComposite( $arrValues['is_composite'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDatabaseId( $intDatabaseId ) {
		$this->set( 'm_intDatabaseId', CStrings::strToIntDef( $intDatabaseId, NULL, false ) );
	}

	public function getDatabaseId() {
		return $this->m_intDatabaseId;
	}

	public function sqlDatabaseId() {
		return ( true == isset( $this->m_intDatabaseId ) ) ? ( string ) $this->m_intDatabaseId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setTeamId( $intTeamId ) {
		$this->set( 'm_intTeamId', CStrings::strToIntDef( $intTeamId, NULL, false ) );
	}

	public function getTeamId() {
		return $this->m_intTeamId;
	}

	public function sqlTeamId() {
		return ( true == isset( $this->m_intTeamId ) ) ? ( string ) $this->m_intTeamId : 'NULL';
	}

	public function setTableName( $strTableName ) {
		$this->set( 'm_strTableName', CStrings::strTrimDef( $strTableName, -1, NULL, true ) );
	}

	public function getTableName() {
		return $this->m_strTableName;
	}

	public function sqlTableName() {
		return ( true == isset( $this->m_strTableName ) ) ? '\'' . addslashes( $this->m_strTableName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setKeywords( $strKeywords ) {
		$this->set( 'm_strKeywords', CStrings::strTrimDef( $strKeywords, 255, NULL, true ) );
	}

	public function getKeywords() {
		return $this->m_strKeywords;
	}

	public function sqlKeywords() {
		return ( true == isset( $this->m_strKeywords ) ) ? '\'' . addslashes( $this->m_strKeywords ) . '\'' : 'NULL';
	}

	public function setLastOptimized( $strLastOptimized ) {
		$this->set( 'm_strLastOptimized', CStrings::strTrimDef( $strLastOptimized, -1, NULL, true ) );
	}

	public function getLastOptimized() {
		return $this->m_strLastOptimized;
	}

	public function sqlLastOptimized() {
		return ( true == isset( $this->m_strLastOptimized ) ) ? '\'' . $this->m_strLastOptimized . '\'' : 'NULL';
	}

	public function setHasFilter( $intHasFilter ) {
		$this->set( 'm_intHasFilter', CStrings::strToIntDef( $intHasFilter, NULL, false ) );
	}

	public function getHasFilter() {
		return $this->m_intHasFilter;
	}

	public function sqlHasFilter() {
		return ( true == isset( $this->m_intHasFilter ) ) ? ( string ) $this->m_intHasFilter : '0';
	}

	public function setIsCloudSync( $boolIsCloudSync ) {
		$this->set( 'm_boolIsCloudSync', CStrings::strToBool( $boolIsCloudSync ) );
	}

	public function getIsCloudSync() {
		return $this->m_boolIsCloudSync;
	}

	public function sqlIsCloudSync() {
		return ( true == isset( $this->m_boolIsCloudSync ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCloudSync ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsComposite( $intIsComposite ) {
		$this->set( 'm_intIsComposite', CStrings::strToIntDef( $intIsComposite, NULL, false ) );
	}

	public function getIsComposite() {
		return $this->m_intIsComposite;
	}

	public function sqlIsComposite() {
		return ( true == isset( $this->m_intIsComposite ) ) ? ( string ) $this->m_intIsComposite : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, database_id, ps_product_id, team_id, table_name, description, keywords, last_optimized, has_filter, is_cloud_sync, is_composite, is_published, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlDatabaseId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlTeamId() . ', ' .
 						$this->sqlTableName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlKeywords() . ', ' .
 						$this->sqlLastOptimized() . ', ' .
 						$this->sqlHasFilter() . ', ' .
 						$this->sqlIsCloudSync() . ', ' .
 						$this->sqlIsComposite() . ', ' .
 						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId() . ','; } elseif( true == array_key_exists( 'DatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' team_id = ' . $this->sqlTeamId() . ','; } elseif( true == array_key_exists( 'TeamId', $this->getChangedColumns() ) ) { $strSql .= ' team_id = ' . $this->sqlTeamId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' table_name = ' . $this->sqlTableName() . ','; } elseif( true == array_key_exists( 'TableName', $this->getChangedColumns() ) ) { $strSql .= ' table_name = ' . $this->sqlTableName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' keywords = ' . $this->sqlKeywords() . ','; } elseif( true == array_key_exists( 'Keywords', $this->getChangedColumns() ) ) { $strSql .= ' keywords = ' . $this->sqlKeywords() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_optimized = ' . $this->sqlLastOptimized() . ','; } elseif( true == array_key_exists( 'LastOptimized', $this->getChangedColumns() ) ) { $strSql .= ' last_optimized = ' . $this->sqlLastOptimized() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_filter = ' . $this->sqlHasFilter() . ','; } elseif( true == array_key_exists( 'HasFilter', $this->getChangedColumns() ) ) { $strSql .= ' has_filter = ' . $this->sqlHasFilter() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_cloud_sync = ' . $this->sqlIsCloudSync() . ','; } elseif( true == array_key_exists( 'IsCloudSync', $this->getChangedColumns() ) ) { $strSql .= ' is_cloud_sync = ' . $this->sqlIsCloudSync() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_composite = ' . $this->sqlIsComposite() . ','; } elseif( true == array_key_exists( 'IsComposite', $this->getChangedColumns() ) ) { $strSql .= ' is_composite = ' . $this->sqlIsComposite() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'database_id' => $this->getDatabaseId(),
			'ps_product_id' => $this->getPsProductId(),
			'team_id' => $this->getTeamId(),
			'table_name' => $this->getTableName(),
			'description' => $this->getDescription(),
			'keywords' => $this->getKeywords(),
			'last_optimized' => $this->getLastOptimized(),
			'has_filter' => $this->getHasFilter(),
			'is_cloud_sync' => $this->getIsCloudSync(),
			'is_composite' => $this->getIsComposite(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>