<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CMessageOperators
 * Do not add any new functions to this class.
 */

class CBaseMessageOperators extends CEosPluralBase {

	/**
	 * @return CMessageOperator[]
	 */
	public static function fetchMessageOperators( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMessageOperator', $objDatabase );
	}

	/**
	 * @return CMessageOperator
	 */
	public static function fetchMessageOperator( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMessageOperator', $objDatabase );
	}

	public static function fetchMessageOperatorCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'message_operators', $objDatabase );
	}

	public static function fetchMessageOperatorById( $intId, $objDatabase ) {
		return self::fetchMessageOperator( sprintf( 'SELECT * FROM message_operators WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>