<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmailAddressTypes
 * Do not add any new functions to this class.
 */

class CBaseEmailAddressTypes extends CEosPluralBase {

	/**
	 * @return CEmailAddressType[]
	 */
	public static function fetchEmailAddressTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEmailAddressType::class, $objDatabase );
	}

	/**
	 * @return CEmailAddressType
	 */
	public static function fetchEmailAddressType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEmailAddressType::class, $objDatabase );
	}

	public static function fetchEmailAddressTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'email_address_types', $objDatabase );
	}

	public static function fetchEmailAddressTypeById( $intId, $objDatabase ) {
		return self::fetchEmailAddressType( sprintf( 'SELECT * FROM email_address_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>