<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSubsidyIncomeLevelTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyIncomeLevelTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyIncomeLevelType[]
	 */
	public static function fetchSubsidyIncomeLevelTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSubsidyIncomeLevelType', $objDatabase );
	}

	/**
	 * @return CSubsidyIncomeLevelType
	 */
	public static function fetchSubsidyIncomeLevelType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSubsidyIncomeLevelType', $objDatabase );
	}

	public static function fetchSubsidyIncomeLevelTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_income_level_types', $objDatabase );
	}

	public static function fetchSubsidyIncomeLevelTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyIncomeLevelType( sprintf( 'SELECT * FROM subsidy_income_level_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>