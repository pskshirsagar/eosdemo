<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CMessageTypes
 * Do not add any new functions to this class.
 */

class CBaseMessageTypes extends CEosPluralBase {

	/**
	 * @return CMessageType[]
	 */
	public static function fetchMessageTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMessageType::class, $objDatabase );
	}

	/**
	 * @return CMessageType
	 */
	public static function fetchMessageType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMessageType::class, $objDatabase );
	}

	public static function fetchMessageTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'message_types', $objDatabase );
	}

	public static function fetchMessageTypeById( $intId, $objDatabase ) {
		return self::fetchMessageType( sprintf( 'SELECT * FROM message_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>