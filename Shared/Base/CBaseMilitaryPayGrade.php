<?php

class CBaseMilitaryPayGrade extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.military_pay_grades';

	protected $m_intId;
	protected $m_strName;
	protected $m_strPayGradeMacId;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intMilitaryOfficerStructureId;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['pay_grade_mac_id'] ) && $boolDirectSet ) $this->set( 'm_strPayGradeMacId', trim( stripcslashes( $arrValues['pay_grade_mac_id'] ) ) ); elseif( isset( $arrValues['pay_grade_mac_id'] ) ) $this->setPayGradeMacId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['pay_grade_mac_id'] ) : $arrValues['pay_grade_mac_id'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['military_officer_structure_id'] ) && $boolDirectSet ) $this->set( 'm_intMilitaryOfficerStructureId', trim( $arrValues['military_officer_structure_id'] ) ); elseif( isset( $arrValues['military_officer_structure_id'] ) ) $this->setMilitaryOfficerStructureId( $arrValues['military_officer_structure_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 5, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setPayGradeMacId( $strPayGradeMacId ) {
		$this->set( 'm_strPayGradeMacId', CStrings::strTrimDef( $strPayGradeMacId, 5, NULL, true ) );
	}

	public function getPayGradeMacId() {
		return $this->m_strPayGradeMacId;
	}

	public function sqlPayGradeMacId() {
		return ( true == isset( $this->m_strPayGradeMacId ) ) ? '\'' . addslashes( $this->m_strPayGradeMacId ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setMilitaryOfficerStructureId( $intMilitaryOfficerStructureId ) {
		$this->set( 'm_intMilitaryOfficerStructureId', CStrings::strToIntDef( $intMilitaryOfficerStructureId, NULL, false ) );
	}

	public function getMilitaryOfficerStructureId() {
		return $this->m_intMilitaryOfficerStructureId;
	}

	public function sqlMilitaryOfficerStructureId() {
		return ( true == isset( $this->m_intMilitaryOfficerStructureId ) ) ? ( string ) $this->m_intMilitaryOfficerStructureId : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'pay_grade_mac_id' => $this->getPayGradeMacId(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails(),
			'military_officer_structure_id' => $this->getMilitaryOfficerStructureId()
		);
	}

}
?>