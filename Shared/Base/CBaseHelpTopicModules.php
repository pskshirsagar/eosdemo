<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CHelpTopicModules
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseHelpTopicModules extends CEosPluralBase {

	/**
	 * @return CHelpTopicModule[]
	 */
	public static function fetchHelpTopicModules( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CHelpTopicModule', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CHelpTopicModule
	 */
	public static function fetchHelpTopicModule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CHelpTopicModule', $objDatabase );
	}

	public static function fetchHelpTopicModuleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'help_topic_modules', $objDatabase );
	}

	public static function fetchHelpTopicModuleByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchHelpTopicModule( sprintf( 'SELECT * FROM help_topic_modules WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchHelpTopicModulesByCid( $intCid, $objDatabase ) {
		return self::fetchHelpTopicModules( sprintf( 'SELECT * FROM help_topic_modules WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchHelpTopicModulesByHelpTopicIdByCid( $intHelpTopicId, $intCid, $objDatabase ) {
		return self::fetchHelpTopicModules( sprintf( 'SELECT * FROM help_topic_modules WHERE help_topic_id = %d AND cid = %d', ( int ) $intHelpTopicId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchHelpTopicModulesByModuleIdByCid( $intModuleId, $intCid, $objDatabase ) {
		return self::fetchHelpTopicModules( sprintf( 'SELECT * FROM help_topic_modules WHERE module_id = %d AND cid = %d', ( int ) $intModuleId, ( int ) $intCid ), $objDatabase );
	}

}
?>