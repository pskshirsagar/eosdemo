<?php

class CBaseServiceGroupType extends CEosSingularBase {

	const TABLE_NAME = 'public.service_group_types';

	protected $m_intId;
	protected $m_intMajorApiVersionId;
	protected $m_strName;
	protected $m_strSystemCode;
	protected $m_strProposedDeprecationDate;
	protected $m_strDeprecationDescription;
	protected $m_boolIsPublished;
	protected $m_boolIsDefaultApiVersion;
	protected $m_intOrderNum;
	protected $m_intDeprecatedBy;
	protected $m_strDeprecatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;
		$this->m_boolIsDefaultApiVersion = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['major_api_version_id'] ) && $boolDirectSet ) $this->set( 'm_intMajorApiVersionId', trim( $arrValues['major_api_version_id'] ) ); elseif( isset( $arrValues['major_api_version_id'] ) ) $this->setMajorApiVersionId( $arrValues['major_api_version_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['system_code'] ) && $boolDirectSet ) $this->set( 'm_strSystemCode', trim( stripcslashes( $arrValues['system_code'] ) ) ); elseif( isset( $arrValues['system_code'] ) ) $this->setSystemCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['system_code'] ) : $arrValues['system_code'] );
		if( isset( $arrValues['proposed_deprecation_date'] ) && $boolDirectSet ) $this->set( 'm_strProposedDeprecationDate', trim( $arrValues['proposed_deprecation_date'] ) ); elseif( isset( $arrValues['proposed_deprecation_date'] ) ) $this->setProposedDeprecationDate( $arrValues['proposed_deprecation_date'] );
		if( isset( $arrValues['deprecation_description'] ) && $boolDirectSet ) $this->set( 'm_strDeprecationDescription', trim( stripcslashes( $arrValues['deprecation_description'] ) ) ); elseif( isset( $arrValues['deprecation_description'] ) ) $this->setDeprecationDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['deprecation_description'] ) : $arrValues['deprecation_description'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['is_default_api_version'] ) && $boolDirectSet ) $this->set( 'm_boolIsDefaultApiVersion', trim( stripcslashes( $arrValues['is_default_api_version'] ) ) ); elseif( isset( $arrValues['is_default_api_version'] ) ) $this->setIsDefaultApiVersion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_default_api_version'] ) : $arrValues['is_default_api_version'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deprecated_by'] ) && $boolDirectSet ) $this->set( 'm_intDeprecatedBy', trim( $arrValues['deprecated_by'] ) ); elseif( isset( $arrValues['deprecated_by'] ) ) $this->setDeprecatedBy( $arrValues['deprecated_by'] );
		if( isset( $arrValues['deprecated_on'] ) && $boolDirectSet ) $this->set( 'm_strDeprecatedOn', trim( $arrValues['deprecated_on'] ) ); elseif( isset( $arrValues['deprecated_on'] ) ) $this->setDeprecatedOn( $arrValues['deprecated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setMajorApiVersionId( $intMajorApiVersionId ) {
		$this->set( 'm_intMajorApiVersionId', CStrings::strToIntDef( $intMajorApiVersionId, NULL, false ) );
	}

	public function getMajorApiVersionId() {
		return $this->m_intMajorApiVersionId;
	}

	public function sqlMajorApiVersionId() {
		return ( true == isset( $this->m_intMajorApiVersionId ) ) ? ( string ) $this->m_intMajorApiVersionId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setSystemCode( $strSystemCode ) {
		$this->set( 'm_strSystemCode', CStrings::strTrimDef( $strSystemCode, 50, NULL, true ) );
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	public function sqlSystemCode() {
		return ( true == isset( $this->m_strSystemCode ) ) ? '\'' . addslashes( $this->m_strSystemCode ) . '\'' : 'NULL';
	}

	public function setProposedDeprecationDate( $strProposedDeprecationDate ) {
		$this->set( 'm_strProposedDeprecationDate', CStrings::strTrimDef( $strProposedDeprecationDate, -1, NULL, true ) );
	}

	public function getProposedDeprecationDate() {
		return $this->m_strProposedDeprecationDate;
	}

	public function sqlProposedDeprecationDate() {
		return ( true == isset( $this->m_strProposedDeprecationDate ) ) ? '\'' . $this->m_strProposedDeprecationDate . '\'' : 'NULL';
	}

	public function setDeprecationDescription( $strDeprecationDescription ) {
		$this->set( 'm_strDeprecationDescription', CStrings::strTrimDef( $strDeprecationDescription, -1, NULL, true ) );
	}

	public function getDeprecationDescription() {
		return $this->m_strDeprecationDescription;
	}

	public function sqlDeprecationDescription() {
		return ( true == isset( $this->m_strDeprecationDescription ) ) ? '\'' . addslashes( $this->m_strDeprecationDescription ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDefaultApiVersion( $boolIsDefaultApiVersion ) {
		$this->set( 'm_boolIsDefaultApiVersion', CStrings::strToBool( $boolIsDefaultApiVersion ) );
	}

	public function getIsDefaultApiVersion() {
		return $this->m_boolIsDefaultApiVersion;
	}

	public function sqlIsDefaultApiVersion() {
		return ( true == isset( $this->m_boolIsDefaultApiVersion ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDefaultApiVersion ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeprecatedBy( $intDeprecatedBy ) {
		$this->set( 'm_intDeprecatedBy', CStrings::strToIntDef( $intDeprecatedBy, NULL, false ) );
	}

	public function getDeprecatedBy() {
		return $this->m_intDeprecatedBy;
	}

	public function sqlDeprecatedBy() {
		return ( true == isset( $this->m_intDeprecatedBy ) ) ? ( string ) $this->m_intDeprecatedBy : 'NULL';
	}

	public function setDeprecatedOn( $strDeprecatedOn ) {
		$this->set( 'm_strDeprecatedOn', CStrings::strTrimDef( $strDeprecatedOn, -1, NULL, true ) );
	}

	public function getDeprecatedOn() {
		return $this->m_strDeprecatedOn;
	}

	public function sqlDeprecatedOn() {
		return ( true == isset( $this->m_strDeprecatedOn ) ) ? '\'' . $this->m_strDeprecatedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, major_api_version_id, name, system_code, proposed_deprecation_date, deprecation_description, is_published, is_default_api_version, order_num, deprecated_by, deprecated_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlMajorApiVersionId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlSystemCode() . ', ' .
 						$this->sqlProposedDeprecationDate() . ', ' .
 						$this->sqlDeprecationDescription() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlIsDefaultApiVersion() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlDeprecatedBy() . ', ' .
 						$this->sqlDeprecatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' major_api_version_id = ' . $this->sqlMajorApiVersionId() . ','; } elseif( true == array_key_exists( 'MajorApiVersionId', $this->getChangedColumns() ) ) { $strSql .= ' major_api_version_id = ' . $this->sqlMajorApiVersionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_code = ' . $this->sqlSystemCode() . ','; } elseif( true == array_key_exists( 'SystemCode', $this->getChangedColumns() ) ) { $strSql .= ' system_code = ' . $this->sqlSystemCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proposed_deprecation_date = ' . $this->sqlProposedDeprecationDate() . ','; } elseif( true == array_key_exists( 'ProposedDeprecationDate', $this->getChangedColumns() ) ) { $strSql .= ' proposed_deprecation_date = ' . $this->sqlProposedDeprecationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deprecation_description = ' . $this->sqlDeprecationDescription() . ','; } elseif( true == array_key_exists( 'DeprecationDescription', $this->getChangedColumns() ) ) { $strSql .= ' deprecation_description = ' . $this->sqlDeprecationDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default_api_version = ' . $this->sqlIsDefaultApiVersion() . ','; } elseif( true == array_key_exists( 'IsDefaultApiVersion', $this->getChangedColumns() ) ) { $strSql .= ' is_default_api_version = ' . $this->sqlIsDefaultApiVersion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deprecated_by = ' . $this->sqlDeprecatedBy() . ','; } elseif( true == array_key_exists( 'DeprecatedBy', $this->getChangedColumns() ) ) { $strSql .= ' deprecated_by = ' . $this->sqlDeprecatedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deprecated_on = ' . $this->sqlDeprecatedOn() . ','; } elseif( true == array_key_exists( 'DeprecatedOn', $this->getChangedColumns() ) ) { $strSql .= ' deprecated_on = ' . $this->sqlDeprecatedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'major_api_version_id' => $this->getMajorApiVersionId(),
			'name' => $this->getName(),
			'system_code' => $this->getSystemCode(),
			'proposed_deprecation_date' => $this->getProposedDeprecationDate(),
			'deprecation_description' => $this->getDeprecationDescription(),
			'is_published' => $this->getIsPublished(),
			'is_default_api_version' => $this->getIsDefaultApiVersion(),
			'order_num' => $this->getOrderNum(),
			'deprecated_by' => $this->getDeprecatedBy(),
			'deprecated_on' => $this->getDeprecatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>