<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReservedSubdomains
 * Do not add any new functions to this class.
 */

class CBaseReservedSubdomains extends CEosPluralBase {

	/**
	 * @return CReservedSubdomain[]
	 */
	public static function fetchReservedSubdomains( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CReservedSubdomain', $objDatabase );
	}

	/**
	 * @return CReservedSubdomain
	 */
	public static function fetchReservedSubdomain( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CReservedSubdomain', $objDatabase );
	}

	public static function fetchReservedSubdomainCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'reserved_subdomains', $objDatabase );
	}

	public static function fetchReservedSubdomainById( $intId, $objDatabase ) {
		return self::fetchReservedSubdomain( sprintf( 'SELECT * FROM reserved_subdomains WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>