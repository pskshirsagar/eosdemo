<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPhoneNumberCountryCodes
 * Do not add any new functions to this class.
 */

class CBasePhoneNumberCountryCodes extends CEosPluralBase {

	/**
	 * @return CPhoneNumberCountryCode[]
	 */
	public static function fetchPhoneNumberCountryCodes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPhoneNumberCountryCode::class, $objDatabase );
	}

	/**
	 * @return CPhoneNumberCountryCode
	 */
	public static function fetchPhoneNumberCountryCode( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPhoneNumberCountryCode::class, $objDatabase );
	}

	public static function fetchPhoneNumberCountryCodeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'phone_number_country_codes', $objDatabase );
	}

	public static function fetchPhoneNumberCountryCodeById( $intId, $objDatabase ) {
		return self::fetchPhoneNumberCountryCode( sprintf( 'SELECT * FROM phone_number_country_codes WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>