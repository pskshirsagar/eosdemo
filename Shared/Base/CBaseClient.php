<?php

class CBaseClient extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.clients';

	protected $m_intId;
	protected $m_intPsLeadId;
	protected $m_intEntityId;
	protected $m_intCompanyStatusTypeId;
	protected $m_intDatabaseId;
	protected $m_strCompanyName;
	protected $m_strRwxDomain;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strCurrencyCode;
	protected $m_strLocaleCode;
	protected $m_strCountryCode;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intPsLeadId = '0';
		$this->m_intDatabaseId = '2';
		$this->m_strCurrencyCode = 'USD';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId', trim( $arrValues['ps_lead_id'] ) ); elseif( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['entity_id'] ) && $boolDirectSet ) $this->set( 'm_intEntityId', trim( $arrValues['entity_id'] ) ); elseif( isset( $arrValues['entity_id'] ) ) $this->setEntityId( $arrValues['entity_id'] );
		if( isset( $arrValues['company_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyStatusTypeId', trim( $arrValues['company_status_type_id'] ) ); elseif( isset( $arrValues['company_status_type_id'] ) ) $this->setCompanyStatusTypeId( $arrValues['company_status_type_id'] );
		if( isset( $arrValues['database_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseId', trim( $arrValues['database_id'] ) ); elseif( isset( $arrValues['database_id'] ) ) $this->setDatabaseId( $arrValues['database_id'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( stripcslashes( $arrValues['company_name'] ) ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_name'] ) : $arrValues['company_name'] );
		if( isset( $arrValues['rwx_domain'] ) && $boolDirectSet ) $this->set( 'm_strRwxDomain', trim( stripcslashes( $arrValues['rwx_domain'] ) ) ); elseif( isset( $arrValues['rwx_domain'] ) ) $this->setRwxDomain( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['rwx_domain'] ) : $arrValues['rwx_domain'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( stripcslashes( $arrValues['currency_code'] ) ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['currency_code'] ) : $arrValues['currency_code'] );
		if( isset( $arrValues['locale_code'] ) && $boolDirectSet ) $this->set( 'm_strLocaleCode', trim( stripcslashes( $arrValues['locale_code'] ) ) ); elseif( isset( $arrValues['locale_code'] ) ) $this->setLocaleCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['locale_code'] ) : $arrValues['locale_code'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->set( 'm_intPsLeadId', CStrings::strToIntDef( $intPsLeadId, NULL, false ) );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : '0';
	}

	public function setEntityId( $intEntityId ) {
		$this->set( 'm_intEntityId', CStrings::strToIntDef( $intEntityId, NULL, false ) );
	}

	public function getEntityId() {
		return $this->m_intEntityId;
	}

	public function sqlEntityId() {
		return ( true == isset( $this->m_intEntityId ) ) ? ( string ) $this->m_intEntityId : 'NULL';
	}

	public function setCompanyStatusTypeId( $intCompanyStatusTypeId ) {
		$this->set( 'm_intCompanyStatusTypeId', CStrings::strToIntDef( $intCompanyStatusTypeId, NULL, false ) );
	}

	public function getCompanyStatusTypeId() {
		return $this->m_intCompanyStatusTypeId;
	}

	public function sqlCompanyStatusTypeId() {
		return ( true == isset( $this->m_intCompanyStatusTypeId ) ) ? ( string ) $this->m_intCompanyStatusTypeId : 'NULL';
	}

	public function setDatabaseId( $intDatabaseId ) {
		$this->set( 'm_intDatabaseId', CStrings::strToIntDef( $intDatabaseId, NULL, false ) );
	}

	public function getDatabaseId() {
		return $this->m_intDatabaseId;
	}

	public function sqlDatabaseId() {
		return ( true == isset( $this->m_intDatabaseId ) ) ? ( string ) $this->m_intDatabaseId : '2';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 100, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? '\'' . addslashes( $this->m_strCompanyName ) . '\'' : 'NULL';
	}

	public function setRwxDomain( $strRwxDomain ) {
		$this->set( 'm_strRwxDomain', CStrings::strTrimDef( $strRwxDomain, 240, NULL, true ) );
	}

	public function getRwxDomain() {
		return $this->m_strRwxDomain;
	}

	public function sqlRwxDomain() {
		return ( true == isset( $this->m_strRwxDomain ) ) ? '\'' . addslashes( $this->m_strRwxDomain ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' : '\'USD\'';
	}

	public function setLocaleCode( $strLocaleCode ) {
		$this->set( 'm_strLocaleCode', CStrings::strTrimDef( $strLocaleCode, 100, NULL, true ) );
	}

	public function getLocaleCode() {
		return $this->m_strLocaleCode;
	}

	public function sqlLocaleCode() {
		return ( true == isset( $this->m_strLocaleCode ) ) ? '\'' . addslashes( $this->m_strLocaleCode ) . '\'' : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ps_lead_id, entity_id, company_status_type_id, database_id, company_name, rwx_domain, updated_by, updated_on, created_by, created_on, currency_code, locale_code, country_code, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlPsLeadId() . ', ' .
						$this->sqlEntityId() . ', ' .
						$this->sqlCompanyStatusTypeId() . ', ' .
						$this->sqlDatabaseId() . ', ' .
						$this->sqlCompanyName() . ', ' .
						$this->sqlRwxDomain() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlCurrencyCode() . ', ' .
						$this->sqlLocaleCode() . ', ' .
						$this->sqlCountryCode() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId(). ',' ; } elseif( true == array_key_exists( 'PsLeadId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entity_id = ' . $this->sqlEntityId(). ',' ; } elseif( true == array_key_exists( 'EntityId', $this->getChangedColumns() ) ) { $strSql .= ' entity_id = ' . $this->sqlEntityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_status_type_id = ' . $this->sqlCompanyStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'CompanyStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' company_status_type_id = ' . $this->sqlCompanyStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId(). ',' ; } elseif( true == array_key_exists( 'DatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName(). ',' ; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rwx_domain = ' . $this->sqlRwxDomain(). ',' ; } elseif( true == array_key_exists( 'RwxDomain', $this->getChangedColumns() ) ) { $strSql .= ' rwx_domain = ' . $this->sqlRwxDomain() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode(). ',' ; } elseif( true == array_key_exists( 'CurrencyCode', $this->getChangedColumns() ) ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locale_code = ' . $this->sqlLocaleCode(). ',' ; } elseif( true == array_key_exists( 'LocaleCode', $this->getChangedColumns() ) ) { $strSql .= ' locale_code = ' . $this->sqlLocaleCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode(). ',' ; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_lead_id' => $this->getPsLeadId(),
			'entity_id' => $this->getEntityId(),
			'company_status_type_id' => $this->getCompanyStatusTypeId(),
			'database_id' => $this->getDatabaseId(),
			'company_name' => $this->getCompanyName(),
			'rwx_domain' => $this->getRwxDomain(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'currency_code' => $this->getCurrencyCode(),
			'locale_code' => $this->getLocaleCode(),
			'country_code' => $this->getCountryCode(),
			'details' => $this->getDetails()
		);
	}

}
?>