<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CFilters
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFilters extends CEosPluralBase {

	/**
	 * @return CFilter[]
	 */
	public static function fetchFilters( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CFilter', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFilter
	 */
	public static function fetchFilter( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CFilter', $objDatabase );
	}

	public static function fetchFilterCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'filters', $objDatabase );
	}

	public static function fetchFilterByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFilter( sprintf( 'SELECT * FROM filters WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFiltersByCid( $intCid, $objDatabase ) {
		return self::fetchFilters( sprintf( 'SELECT * FROM filters WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFiltersByFilterTypeIdByCid( $intFilterTypeId, $intCid, $objDatabase ) {
		return self::fetchFilters( sprintf( 'SELECT * FROM filters WHERE filter_type_id = %d AND cid = %d', ( int ) $intFilterTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>