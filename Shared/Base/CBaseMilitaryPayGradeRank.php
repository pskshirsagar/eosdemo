<?php

class CBaseMilitaryPayGradeRank extends CEosSingularBase {

	const TABLE_NAME = 'public.military_pay_grade_ranks';

	protected $m_intId;
	protected $m_intMilitaryPayGradeId;
	protected $m_intMilitaryRankId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['military_pay_grade_id'] ) && $boolDirectSet ) $this->set( 'm_intMilitaryPayGradeId', trim( $arrValues['military_pay_grade_id'] ) ); elseif( isset( $arrValues['military_pay_grade_id'] ) ) $this->setMilitaryPayGradeId( $arrValues['military_pay_grade_id'] );
		if( isset( $arrValues['military_rank_id'] ) && $boolDirectSet ) $this->set( 'm_intMilitaryRankId', trim( $arrValues['military_rank_id'] ) ); elseif( isset( $arrValues['military_rank_id'] ) ) $this->setMilitaryRankId( $arrValues['military_rank_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setMilitaryPayGradeId( $intMilitaryPayGradeId ) {
		$this->set( 'm_intMilitaryPayGradeId', CStrings::strToIntDef( $intMilitaryPayGradeId, NULL, false ) );
	}

	public function getMilitaryPayGradeId() {
		return $this->m_intMilitaryPayGradeId;
	}

	public function sqlMilitaryPayGradeId() {
		return ( true == isset( $this->m_intMilitaryPayGradeId ) ) ? ( string ) $this->m_intMilitaryPayGradeId : 'NULL';
	}

	public function setMilitaryRankId( $intMilitaryRankId ) {
		$this->set( 'm_intMilitaryRankId', CStrings::strToIntDef( $intMilitaryRankId, NULL, false ) );
	}

	public function getMilitaryRankId() {
		return $this->m_intMilitaryRankId;
	}

	public function sqlMilitaryRankId() {
		return ( true == isset( $this->m_intMilitaryRankId ) ) ? ( string ) $this->m_intMilitaryRankId : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'military_pay_grade_id' => $this->getMilitaryPayGradeId(),
			'military_rank_id' => $this->getMilitaryRankId()
		);
	}

}
?>