<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyIncomeLimit extends CEosSingularBase {

	const TABLE_NAME = 'public.subsidy_income_limits';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSubsidyIncomeLimitVersionId;
	protected $m_intSubsidyIncomeLimitAreaId;
	protected $m_intSubsidyIncomeLevelTypeId;
	protected $m_intFamilySize;
	protected $m_intIncomeLimit;
	protected $m_intSubsidyTypeId;
	protected $m_boolIsHera;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = '1';
		$this->m_intFamilySize = '1';
		$this->m_boolIsHera = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['subsidy_income_limit_version_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyIncomeLimitVersionId', trim( $arrValues['subsidy_income_limit_version_id'] ) ); elseif( isset( $arrValues['subsidy_income_limit_version_id'] ) ) $this->setSubsidyIncomeLimitVersionId( $arrValues['subsidy_income_limit_version_id'] );
		if( isset( $arrValues['subsidy_income_limit_area_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyIncomeLimitAreaId', trim( $arrValues['subsidy_income_limit_area_id'] ) ); elseif( isset( $arrValues['subsidy_income_limit_area_id'] ) ) $this->setSubsidyIncomeLimitAreaId( $arrValues['subsidy_income_limit_area_id'] );
		if( isset( $arrValues['subsidy_income_level_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyIncomeLevelTypeId', trim( $arrValues['subsidy_income_level_type_id'] ) ); elseif( isset( $arrValues['subsidy_income_level_type_id'] ) ) $this->setSubsidyIncomeLevelTypeId( $arrValues['subsidy_income_level_type_id'] );
		if( isset( $arrValues['family_size'] ) && $boolDirectSet ) $this->set( 'm_intFamilySize', trim( $arrValues['family_size'] ) ); elseif( isset( $arrValues['family_size'] ) ) $this->setFamilySize( $arrValues['family_size'] );
		if( isset( $arrValues['income_limit'] ) && $boolDirectSet ) $this->set( 'm_intIncomeLimit', trim( $arrValues['income_limit'] ) ); elseif( isset( $arrValues['income_limit'] ) ) $this->setIncomeLimit( $arrValues['income_limit'] );
		if( isset( $arrValues['subsidy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyTypeId', trim( $arrValues['subsidy_type_id'] ) ); elseif( isset( $arrValues['subsidy_type_id'] ) ) $this->setSubsidyTypeId( $arrValues['subsidy_type_id'] );
		if( isset( $arrValues['is_hera'] ) && $boolDirectSet ) $this->set( 'm_boolIsHera', trim( stripcslashes( $arrValues['is_hera'] ) ) ); elseif( isset( $arrValues['is_hera'] ) ) $this->setIsHera( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_hera'] ) : $arrValues['is_hera'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : '1';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSubsidyIncomeLimitVersionId( $intSubsidyIncomeLimitVersionId ) {
		$this->set( 'm_intSubsidyIncomeLimitVersionId', CStrings::strToIntDef( $intSubsidyIncomeLimitVersionId, NULL, false ) );
	}

	public function getSubsidyIncomeLimitVersionId() {
		return $this->m_intSubsidyIncomeLimitVersionId;
	}

	public function sqlSubsidyIncomeLimitVersionId() {
		return ( true == isset( $this->m_intSubsidyIncomeLimitVersionId ) ) ? ( string ) $this->m_intSubsidyIncomeLimitVersionId : 'NULL';
	}

	public function setSubsidyIncomeLimitAreaId( $intSubsidyIncomeLimitAreaId ) {
		$this->set( 'm_intSubsidyIncomeLimitAreaId', CStrings::strToIntDef( $intSubsidyIncomeLimitAreaId, NULL, false ) );
	}

	public function getSubsidyIncomeLimitAreaId() {
		return $this->m_intSubsidyIncomeLimitAreaId;
	}

	public function sqlSubsidyIncomeLimitAreaId() {
		return ( true == isset( $this->m_intSubsidyIncomeLimitAreaId ) ) ? ( string ) $this->m_intSubsidyIncomeLimitAreaId : 'NULL';
	}

	public function setSubsidyIncomeLevelTypeId( $intSubsidyIncomeLevelTypeId ) {
		$this->set( 'm_intSubsidyIncomeLevelTypeId', CStrings::strToIntDef( $intSubsidyIncomeLevelTypeId, NULL, false ) );
	}

	public function getSubsidyIncomeLevelTypeId() {
		return $this->m_intSubsidyIncomeLevelTypeId;
	}

	public function sqlSubsidyIncomeLevelTypeId() {
		return ( true == isset( $this->m_intSubsidyIncomeLevelTypeId ) ) ? ( string ) $this->m_intSubsidyIncomeLevelTypeId : 'NULL';
	}

	public function setFamilySize( $intFamilySize ) {
		$this->set( 'm_intFamilySize', CStrings::strToIntDef( $intFamilySize, NULL, false ) );
	}

	public function getFamilySize() {
		return $this->m_intFamilySize;
	}

	public function sqlFamilySize() {
		return ( true == isset( $this->m_intFamilySize ) ) ? ( string ) $this->m_intFamilySize : '1';
	}

	public function setIncomeLimit( $intIncomeLimit ) {
		$this->set( 'm_intIncomeLimit', CStrings::strToIntDef( $intIncomeLimit, NULL, false ) );
	}

	public function getIncomeLimit() {
		return $this->m_intIncomeLimit;
	}

	public function sqlIncomeLimit() {
		return ( true == isset( $this->m_intIncomeLimit ) ) ? ( string ) $this->m_intIncomeLimit : 'NULL';
	}

	public function setSubsidyTypeId( $intSubsidyTypeId ) {
		$this->set( 'm_intSubsidyTypeId', CStrings::strToIntDef( $intSubsidyTypeId, NULL, false ) );
	}

	public function getSubsidyTypeId() {
		return $this->m_intSubsidyTypeId;
	}

	public function sqlSubsidyTypeId() {
		return ( true == isset( $this->m_intSubsidyTypeId ) ) ? ( string ) $this->m_intSubsidyTypeId : 'NULL';
	}

	public function setIsHera( $boolIsHera ) {
		$this->set( 'm_boolIsHera', CStrings::strToBool( $boolIsHera ) );
	}

	public function getIsHera() {
		return $this->m_boolIsHera;
	}

	public function sqlIsHera() {
		return ( true == isset( $this->m_boolIsHera ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHera ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, subsidy_income_limit_version_id, subsidy_income_limit_area_id, subsidy_income_level_type_id, family_size, income_limit, subsidy_type_id, is_hera, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlSubsidyIncomeLimitVersionId() . ', ' .
 						$this->sqlSubsidyIncomeLimitAreaId() . ', ' .
 						$this->sqlSubsidyIncomeLevelTypeId() . ', ' .
 						$this->sqlFamilySize() . ', ' .
 						$this->sqlIncomeLimit() . ', ' .
 						$this->sqlSubsidyTypeId() . ', ' .
 						$this->sqlIsHera() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_income_limit_version_id = ' . $this->sqlSubsidyIncomeLimitVersionId() . ','; } elseif( true == array_key_exists( 'SubsidyIncomeLimitVersionId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_income_limit_version_id = ' . $this->sqlSubsidyIncomeLimitVersionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_income_limit_area_id = ' . $this->sqlSubsidyIncomeLimitAreaId() . ','; } elseif( true == array_key_exists( 'SubsidyIncomeLimitAreaId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_income_limit_area_id = ' . $this->sqlSubsidyIncomeLimitAreaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_income_level_type_id = ' . $this->sqlSubsidyIncomeLevelTypeId() . ','; } elseif( true == array_key_exists( 'SubsidyIncomeLevelTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_income_level_type_id = ' . $this->sqlSubsidyIncomeLevelTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' family_size = ' . $this->sqlFamilySize() . ','; } elseif( true == array_key_exists( 'FamilySize', $this->getChangedColumns() ) ) { $strSql .= ' family_size = ' . $this->sqlFamilySize() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' income_limit = ' . $this->sqlIncomeLimit() . ','; } elseif( true == array_key_exists( 'IncomeLimit', $this->getChangedColumns() ) ) { $strSql .= ' income_limit = ' . $this->sqlIncomeLimit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_type_id = ' . $this->sqlSubsidyTypeId() . ','; } elseif( true == array_key_exists( 'SubsidyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_type_id = ' . $this->sqlSubsidyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_hera = ' . $this->sqlIsHera() . ','; } elseif( true == array_key_exists( 'IsHera', $this->getChangedColumns() ) ) { $strSql .= ' is_hera = ' . $this->sqlIsHera() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'subsidy_income_limit_version_id' => $this->getSubsidyIncomeLimitVersionId(),
			'subsidy_income_limit_area_id' => $this->getSubsidyIncomeLimitAreaId(),
			'subsidy_income_level_type_id' => $this->getSubsidyIncomeLevelTypeId(),
			'family_size' => $this->getFamilySize(),
			'income_limit' => $this->getIncomeLimit(),
			'subsidy_type_id' => $this->getSubsidyTypeId(),
			'is_hera' => $this->getIsHera(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>