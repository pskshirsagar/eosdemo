<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPaymentTransactionTypes
 * Do not add any new functions to this class.
 */

class CBasePaymentTransactionTypes extends CEosPluralBase {

	/**
	 * @return CPaymentTransactionType[]
	 */
	public static function fetchPaymentTransactionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPaymentTransactionType::class, $objDatabase );
	}

	/**
	 * @return CPaymentTransactionType
	 */
	public static function fetchPaymentTransactionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPaymentTransactionType::class, $objDatabase );
	}

	public static function fetchPaymentTransactionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'payment_transaction_types', $objDatabase );
	}

	public static function fetchPaymentTransactionTypeById( $intId, $objDatabase ) {
		return self::fetchPaymentTransactionType( sprintf( 'SELECT * FROM payment_transaction_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>