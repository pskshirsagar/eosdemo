<?php

class CBasePropertySettingGroup extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.property_setting_groups';

	protected $m_intId;
	protected $m_intModuleId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strTableName;
	protected $m_strColumnName;
	protected $m_boolIsList;
	protected $m_intToolTipId;
	protected $m_boolIsPublished;
	protected $m_boolRequiresSync;
	protected $m_boolCanMarkEmptyCompleted;
	protected $m_strImplementationDescription;
	protected $m_boolIsImplementationRequired;
	protected $m_boolIsTemplatable;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intModuleId = '0';
		$this->m_boolIsList = false;
		$this->m_boolIsPublished = true;
		$this->m_boolRequiresSync = false;
		$this->m_boolCanMarkEmptyCompleted = true;
		$this->m_boolIsImplementationRequired = false;
		$this->m_boolIsTemplatable = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['module_id'] ) && $boolDirectSet ) $this->set( 'm_intModuleId', trim( $arrValues['module_id'] ) ); elseif( isset( $arrValues['module_id'] ) ) $this->setModuleId( $arrValues['module_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['table_name'] ) && $boolDirectSet ) $this->set( 'm_strTableName', trim( stripcslashes( $arrValues['table_name'] ) ) ); elseif( isset( $arrValues['table_name'] ) ) $this->setTableName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['table_name'] ) : $arrValues['table_name'] );
		if( isset( $arrValues['column_name'] ) && $boolDirectSet ) $this->set( 'm_strColumnName', trim( stripcslashes( $arrValues['column_name'] ) ) ); elseif( isset( $arrValues['column_name'] ) ) $this->setColumnName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['column_name'] ) : $arrValues['column_name'] );
		if( isset( $arrValues['is_list'] ) && $boolDirectSet ) $this->set( 'm_boolIsList', trim( stripcslashes( $arrValues['is_list'] ) ) ); elseif( isset( $arrValues['is_list'] ) ) $this->setIsList( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_list'] ) : $arrValues['is_list'] );
		if( isset( $arrValues['tool_tip_id'] ) && $boolDirectSet ) $this->set( 'm_intToolTipId', trim( $arrValues['tool_tip_id'] ) ); elseif( isset( $arrValues['tool_tip_id'] ) ) $this->setToolTipId( $arrValues['tool_tip_id'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['requires_sync'] ) && $boolDirectSet ) $this->set( 'm_boolRequiresSync', trim( stripcslashes( $arrValues['requires_sync'] ) ) ); elseif( isset( $arrValues['requires_sync'] ) ) $this->setRequiresSync( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['requires_sync'] ) : $arrValues['requires_sync'] );
		if( isset( $arrValues['can_mark_empty_completed'] ) && $boolDirectSet ) $this->set( 'm_boolCanMarkEmptyCompleted', trim( stripcslashes( $arrValues['can_mark_empty_completed'] ) ) ); elseif( isset( $arrValues['can_mark_empty_completed'] ) ) $this->setCanMarkEmptyCompleted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['can_mark_empty_completed'] ) : $arrValues['can_mark_empty_completed'] );
		if( isset( $arrValues['implementation_description'] ) && $boolDirectSet ) $this->set( 'm_strImplementationDescription', trim( stripcslashes( $arrValues['implementation_description'] ) ) ); elseif( isset( $arrValues['implementation_description'] ) ) $this->setImplementationDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['implementation_description'] ) : $arrValues['implementation_description'] );
		if( isset( $arrValues['is_implementation_required'] ) && $boolDirectSet ) $this->set( 'm_boolIsImplementationRequired', trim( stripcslashes( $arrValues['is_implementation_required'] ) ) ); elseif( isset( $arrValues['is_implementation_required'] ) ) $this->setIsImplementationRequired( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_implementation_required'] ) : $arrValues['is_implementation_required'] );
		if( isset( $arrValues['is_templatable'] ) && $boolDirectSet ) $this->set( 'm_boolIsTemplatable', trim( stripcslashes( $arrValues['is_templatable'] ) ) ); elseif( isset( $arrValues['is_templatable'] ) ) $this->setIsTemplatable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_templatable'] ) : $arrValues['is_templatable'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setModuleId( $intModuleId ) {
		$this->set( 'm_intModuleId', CStrings::strToIntDef( $intModuleId, NULL, false ) );
	}

	public function getModuleId() {
		return $this->m_intModuleId;
	}

	public function sqlModuleId() {
		return ( true == isset( $this->m_intModuleId ) ) ? ( string ) $this->m_intModuleId : '0';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setTableName( $strTableName ) {
		$this->set( 'm_strTableName', CStrings::strTrimDef( $strTableName, 255, NULL, true ) );
	}

	public function getTableName() {
		return $this->m_strTableName;
	}

	public function sqlTableName() {
		return ( true == isset( $this->m_strTableName ) ) ? '\'' . addslashes( $this->m_strTableName ) . '\'' : 'NULL';
	}

	public function setColumnName( $strColumnName ) {
		$this->set( 'm_strColumnName', CStrings::strTrimDef( $strColumnName, 255, NULL, true ) );
	}

	public function getColumnName() {
		return $this->m_strColumnName;
	}

	public function sqlColumnName() {
		return ( true == isset( $this->m_strColumnName ) ) ? '\'' . addslashes( $this->m_strColumnName ) . '\'' : 'NULL';
	}

	public function setIsList( $boolIsList ) {
		$this->set( 'm_boolIsList', CStrings::strToBool( $boolIsList ) );
	}

	public function getIsList() {
		return $this->m_boolIsList;
	}

	public function sqlIsList() {
		return ( true == isset( $this->m_boolIsList ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsList ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setToolTipId( $intToolTipId ) {
		$this->set( 'm_intToolTipId', CStrings::strToIntDef( $intToolTipId, NULL, false ) );
	}

	public function getToolTipId() {
		return $this->m_intToolTipId;
	}

	public function sqlToolTipId() {
		return ( true == isset( $this->m_intToolTipId ) ) ? ( string ) $this->m_intToolTipId : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequiresSync( $boolRequiresSync ) {
		$this->set( 'm_boolRequiresSync', CStrings::strToBool( $boolRequiresSync ) );
	}

	public function getRequiresSync() {
		return $this->m_boolRequiresSync;
	}

	public function sqlRequiresSync() {
		return ( true == isset( $this->m_boolRequiresSync ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequiresSync ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCanMarkEmptyCompleted( $boolCanMarkEmptyCompleted ) {
		$this->set( 'm_boolCanMarkEmptyCompleted', CStrings::strToBool( $boolCanMarkEmptyCompleted ) );
	}

	public function getCanMarkEmptyCompleted() {
		return $this->m_boolCanMarkEmptyCompleted;
	}

	public function sqlCanMarkEmptyCompleted() {
		return ( true == isset( $this->m_boolCanMarkEmptyCompleted ) ) ? '\'' . ( true == ( bool ) $this->m_boolCanMarkEmptyCompleted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setImplementationDescription( $strImplementationDescription ) {
		$this->set( 'm_strImplementationDescription', CStrings::strTrimDef( $strImplementationDescription, -1, NULL, true ) );
	}

	public function getImplementationDescription() {
		return $this->m_strImplementationDescription;
	}

	public function sqlImplementationDescription() {
		return ( true == isset( $this->m_strImplementationDescription ) ) ? '\'' . addslashes( $this->m_strImplementationDescription ) . '\'' : 'NULL';
	}

	public function setIsImplementationRequired( $boolIsImplementationRequired ) {
		$this->set( 'm_boolIsImplementationRequired', CStrings::strToBool( $boolIsImplementationRequired ) );
	}

	public function getIsImplementationRequired() {
		return $this->m_boolIsImplementationRequired;
	}

	public function sqlIsImplementationRequired() {
		return ( true == isset( $this->m_boolIsImplementationRequired ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsImplementationRequired ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTemplatable( $boolIsTemplatable ) {
		$this->set( 'm_boolIsTemplatable', CStrings::strToBool( $boolIsTemplatable ) );
	}

	public function getIsTemplatable() {
		return $this->m_boolIsTemplatable;
	}

	public function sqlIsTemplatable() {
		return ( true == isset( $this->m_boolIsTemplatable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTemplatable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, module_id, name, description, table_name, column_name, is_list, tool_tip_id, is_published, requires_sync, can_mark_empty_completed, implementation_description, is_implementation_required, is_templatable, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlModuleId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlTableName() . ', ' .
						$this->sqlColumnName() . ', ' .
						$this->sqlIsList() . ', ' .
						$this->sqlToolTipId() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlRequiresSync() . ', ' .
						$this->sqlCanMarkEmptyCompleted() . ', ' .
						$this->sqlImplementationDescription() . ', ' .
						$this->sqlIsImplementationRequired() . ', ' .
						$this->sqlIsTemplatable() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' module_id = ' . $this->sqlModuleId(). ',' ; } elseif( true == array_key_exists( 'ModuleId', $this->getChangedColumns() ) ) { $strSql .= ' module_id = ' . $this->sqlModuleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' table_name = ' . $this->sqlTableName(). ',' ; } elseif( true == array_key_exists( 'TableName', $this->getChangedColumns() ) ) { $strSql .= ' table_name = ' . $this->sqlTableName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' column_name = ' . $this->sqlColumnName(). ',' ; } elseif( true == array_key_exists( 'ColumnName', $this->getChangedColumns() ) ) { $strSql .= ' column_name = ' . $this->sqlColumnName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_list = ' . $this->sqlIsList(). ',' ; } elseif( true == array_key_exists( 'IsList', $this->getChangedColumns() ) ) { $strSql .= ' is_list = ' . $this->sqlIsList() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tool_tip_id = ' . $this->sqlToolTipId(). ',' ; } elseif( true == array_key_exists( 'ToolTipId', $this->getChangedColumns() ) ) { $strSql .= ' tool_tip_id = ' . $this->sqlToolTipId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requires_sync = ' . $this->sqlRequiresSync(). ',' ; } elseif( true == array_key_exists( 'RequiresSync', $this->getChangedColumns() ) ) { $strSql .= ' requires_sync = ' . $this->sqlRequiresSync() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' can_mark_empty_completed = ' . $this->sqlCanMarkEmptyCompleted(). ',' ; } elseif( true == array_key_exists( 'CanMarkEmptyCompleted', $this->getChangedColumns() ) ) { $strSql .= ' can_mark_empty_completed = ' . $this->sqlCanMarkEmptyCompleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_description = ' . $this->sqlImplementationDescription(). ',' ; } elseif( true == array_key_exists( 'ImplementationDescription', $this->getChangedColumns() ) ) { $strSql .= ' implementation_description = ' . $this->sqlImplementationDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_implementation_required = ' . $this->sqlIsImplementationRequired(). ',' ; } elseif( true == array_key_exists( 'IsImplementationRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_implementation_required = ' . $this->sqlIsImplementationRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_templatable = ' . $this->sqlIsTemplatable(). ',' ; } elseif( true == array_key_exists( 'IsTemplatable', $this->getChangedColumns() ) ) { $strSql .= ' is_templatable = ' . $this->sqlIsTemplatable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'module_id' => $this->getModuleId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'table_name' => $this->getTableName(),
			'column_name' => $this->getColumnName(),
			'is_list' => $this->getIsList(),
			'tool_tip_id' => $this->getToolTipId(),
			'is_published' => $this->getIsPublished(),
			'requires_sync' => $this->getRequiresSync(),
			'can_mark_empty_completed' => $this->getCanMarkEmptyCompleted(),
			'implementation_description' => $this->getImplementationDescription(),
			'is_implementation_required' => $this->getIsImplementationRequired(),
			'is_templatable' => $this->getIsTemplatable(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>