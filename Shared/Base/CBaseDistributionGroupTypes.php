<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CDistributionGroupTypes
 * Do not add any new functions to this class.
 */

class CBaseDistributionGroupTypes extends CEosPluralBase {

	/**
	 * @return CDistributionGroupType[]
	 */
	public static function fetchDistributionGroupTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDistributionGroupType::class, $objDatabase );
	}

	/**
	 * @return CDistributionGroupType
	 */
	public static function fetchDistributionGroupType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDistributionGroupType::class, $objDatabase );
	}

	public static function fetchDistributionGroupTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'distribution_group_types', $objDatabase );
	}

	public static function fetchDistributionGroupTypeById( $intId, $objDatabase ) {
		return self::fetchDistributionGroupType( sprintf( 'SELECT * FROM distribution_group_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>