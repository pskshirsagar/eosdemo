<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseEmployeeStatusTypes extends CEosPluralBase {

	/**
	 * @return CEmployeeStatusType[]
	 */
	public static function fetchEmployeeStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEmployeeStatusType::class, $objDatabase );
	}

	/**
	 * @return CEmployeeStatusType
	 */
	public static function fetchEmployeeStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEmployeeStatusType::class, $objDatabase );
	}

	public static function fetchEmployeeStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_status_types', $objDatabase );
	}

	public static function fetchEmployeeStatusTypeById( $intId, $objDatabase ) {
		return self::fetchEmployeeStatusType( sprintf( 'SELECT * FROM employee_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>