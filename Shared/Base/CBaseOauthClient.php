<?php

class CBaseOauthClient extends CEosSingularBase {

	const TABLE_NAME = 'public.oauth_clients';

	protected $m_intId;
	protected $m_strClientId;
	protected $m_strClientSecret;
	protected $m_strAuthUrl;
	protected $m_intOauthClientTypeId;
	protected $m_boolUseApiThrottling;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsPublished;
	protected $m_strGrantType;
	protected $m_strAuthEmail;
	protected $m_strName;

	public function __construct() {
		parent::__construct();

		$this->m_intOauthClientTypeId = '1';
		$this->m_boolUseApiThrottling = false;
		$this->m_intOrderNum = '0';
		$this->m_boolIsPublished = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['client_id'] ) && $boolDirectSet ) $this->set( 'm_strClientId', trim( stripcslashes( $arrValues['client_id'] ) ) ); elseif( isset( $arrValues['client_id'] ) ) $this->setClientId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['client_id'] ) : $arrValues['client_id'] );
		if( isset( $arrValues['client_secret'] ) && $boolDirectSet ) $this->set( 'm_strClientSecret', trim( stripcslashes( $arrValues['client_secret'] ) ) ); elseif( isset( $arrValues['client_secret'] ) ) $this->setClientSecret( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['client_secret'] ) : $arrValues['client_secret'] );
		if( isset( $arrValues['auth_url'] ) && $boolDirectSet ) $this->set( 'm_strAuthUrl', trim( stripcslashes( $arrValues['auth_url'] ) ) ); elseif( isset( $arrValues['auth_url'] ) ) $this->setAuthUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['auth_url'] ) : $arrValues['auth_url'] );
		if( isset( $arrValues['oauth_client_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOauthClientTypeId', trim( $arrValues['oauth_client_type_id'] ) ); elseif( isset( $arrValues['oauth_client_type_id'] ) ) $this->setOauthClientTypeId( $arrValues['oauth_client_type_id'] );
		if( isset( $arrValues['use_api_throttling'] ) && $boolDirectSet ) $this->set( 'm_boolUseApiThrottling', trim( stripcslashes( $arrValues['use_api_throttling'] ) ) ); elseif( isset( $arrValues['use_api_throttling'] ) ) $this->setUseApiThrottling( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_api_throttling'] ) : $arrValues['use_api_throttling'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['grant_type'] ) && $boolDirectSet ) $this->set( 'm_strGrantType', trim( stripcslashes( $arrValues['grant_type'] ) ) ); elseif( isset( $arrValues['grant_type'] ) ) $this->setGrantType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['grant_type'] ) : $arrValues['grant_type'] );
		if( isset( $arrValues['auth_email'] ) && $boolDirectSet ) $this->set( 'm_strAuthEmail', trim( stripcslashes( $arrValues['auth_email'] ) ) ); elseif( isset( $arrValues['auth_email'] ) ) $this->setAuthEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['auth_email'] ) : $arrValues['auth_email'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setClientId( $strClientId ) {
		$this->set( 'm_strClientId', CStrings::strTrimDef( $strClientId, 250, NULL, true ) );
	}

	public function getClientId() {
		return $this->m_strClientId;
	}

	public function sqlClientId() {
		return ( true == isset( $this->m_strClientId ) ) ? '\'' . addslashes( $this->m_strClientId ) . '\'' : 'NULL';
	}

	public function setClientSecret( $strClientSecret ) {
		$this->set( 'm_strClientSecret', CStrings::strTrimDef( $strClientSecret, 250, NULL, true ) );
	}

	public function getClientSecret() {
		return $this->m_strClientSecret;
	}

	public function sqlClientSecret() {
		return ( true == isset( $this->m_strClientSecret ) ) ? '\'' . addslashes( $this->m_strClientSecret ) . '\'' : 'NULL';
	}

	public function setAuthUrl( $strAuthUrl ) {
		$this->set( 'm_strAuthUrl', CStrings::strTrimDef( $strAuthUrl, 250, NULL, true ) );
	}

	public function getAuthUrl() {
		return $this->m_strAuthUrl;
	}

	public function sqlAuthUrl() {
		return ( true == isset( $this->m_strAuthUrl ) ) ? '\'' . addslashes( $this->m_strAuthUrl ) . '\'' : 'NULL';
	}

	public function setOauthClientTypeId( $intOauthClientTypeId ) {
		$this->set( 'm_intOauthClientTypeId', CStrings::strToIntDef( $intOauthClientTypeId, NULL, false ) );
	}

	public function getOauthClientTypeId() {
		return $this->m_intOauthClientTypeId;
	}

	public function sqlOauthClientTypeId() {
		return ( true == isset( $this->m_intOauthClientTypeId ) ) ? ( string ) $this->m_intOauthClientTypeId : '1';
	}

	public function setUseApiThrottling( $boolUseApiThrottling ) {
		$this->set( 'm_boolUseApiThrottling', CStrings::strToBool( $boolUseApiThrottling ) );
	}

	public function getUseApiThrottling() {
		return $this->m_boolUseApiThrottling;
	}

	public function sqlUseApiThrottling() {
		return ( true == isset( $this->m_boolUseApiThrottling ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseApiThrottling ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setGrantType( $strGrantType ) {
		$this->set( 'm_strGrantType', CStrings::strTrimDef( $strGrantType, 240, NULL, true ) );
	}

	public function getGrantType() {
		return $this->m_strGrantType;
	}

	public function sqlGrantType() {
		return ( true == isset( $this->m_strGrantType ) ) ? '\'' . addslashes( $this->m_strGrantType ) . '\'' : 'NULL';
	}

	public function setAuthEmail( $strAuthEmail ) {
		$this->set( 'm_strAuthEmail', CStrings::strTrimDef( $strAuthEmail, 240, NULL, true ) );
	}

	public function getAuthEmail() {
		return $this->m_strAuthEmail;
	}

	public function sqlAuthEmail() {
		return ( true == isset( $this->m_strAuthEmail ) ) ? '\'' . addslashes( $this->m_strAuthEmail ) . '\'' : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, client_id, client_secret, auth_url, oauth_client_type_id, use_api_throttling, order_num, updated_by, updated_on, created_by, created_on, is_published, grant_type, auth_email, name )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlClientId() . ', ' .
						$this->sqlClientSecret() . ', ' .
						$this->sqlAuthUrl() . ', ' .
						$this->sqlOauthClientTypeId() . ', ' .
						$this->sqlUseApiThrottling() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlGrantType() . ', ' .
						$this->sqlAuthEmail() . ', ' .
						$this->sqlName() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_id = ' . $this->sqlClientId(). ',' ; } elseif( true == array_key_exists( 'ClientId', $this->getChangedColumns() ) ) { $strSql .= ' client_id = ' . $this->sqlClientId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_secret = ' . $this->sqlClientSecret(). ',' ; } elseif( true == array_key_exists( 'ClientSecret', $this->getChangedColumns() ) ) { $strSql .= ' client_secret = ' . $this->sqlClientSecret() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auth_url = ' . $this->sqlAuthUrl(). ',' ; } elseif( true == array_key_exists( 'AuthUrl', $this->getChangedColumns() ) ) { $strSql .= ' auth_url = ' . $this->sqlAuthUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' oauth_client_type_id = ' . $this->sqlOauthClientTypeId(). ',' ; } elseif( true == array_key_exists( 'OauthClientTypeId', $this->getChangedColumns() ) ) { $strSql .= ' oauth_client_type_id = ' . $this->sqlOauthClientTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_api_throttling = ' . $this->sqlUseApiThrottling(). ',' ; } elseif( true == array_key_exists( 'UseApiThrottling', $this->getChangedColumns() ) ) { $strSql .= ' use_api_throttling = ' . $this->sqlUseApiThrottling() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' grant_type = ' . $this->sqlGrantType(). ',' ; } elseif( true == array_key_exists( 'GrantType', $this->getChangedColumns() ) ) { $strSql .= ' grant_type = ' . $this->sqlGrantType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auth_email = ' . $this->sqlAuthEmail(). ',' ; } elseif( true == array_key_exists( 'AuthEmail', $this->getChangedColumns() ) ) { $strSql .= ' auth_email = ' . $this->sqlAuthEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'client_id' => $this->getClientId(),
			'client_secret' => $this->getClientSecret(),
			'auth_url' => $this->getAuthUrl(),
			'oauth_client_type_id' => $this->getOauthClientTypeId(),
			'use_api_throttling' => $this->getUseApiThrottling(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_published' => $this->getIsPublished(),
			'grant_type' => $this->getGrantType(),
			'auth_email' => $this->getAuthEmail(),
			'name' => $this->getName()
		);
	}

}
?>