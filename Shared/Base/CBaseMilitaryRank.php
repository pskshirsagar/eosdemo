<?php

class CBaseMilitaryRank extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.military_ranks';

	protected $m_intId;
	protected $m_intMilitaryComponentId;
	protected $m_strName;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strMilitaryRanksAbbreviation;
	protected $m_intPayGradeId;
	protected $m_strRankInsignia;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['military_component_id'] ) && $boolDirectSet ) $this->set( 'm_intMilitaryComponentId', trim( $arrValues['military_component_id'] ) ); elseif( isset( $arrValues['military_component_id'] ) ) $this->setMilitaryComponentId( $arrValues['military_component_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['military_ranks_abbreviation'] ) && $boolDirectSet ) $this->set( 'm_strMilitaryRanksAbbreviation', trim( stripcslashes( $arrValues['military_ranks_abbreviation'] ) ) ); elseif( isset( $arrValues['military_ranks_abbreviation'] ) ) $this->setMilitaryRanksAbbreviation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['military_ranks_abbreviation'] ) : $arrValues['military_ranks_abbreviation'] );
		if( isset( $arrValues['pay_grade_id'] ) && $boolDirectSet ) $this->set( 'm_intPayGradeId', trim( $arrValues['pay_grade_id'] ) ); elseif( isset( $arrValues['pay_grade_id'] ) ) $this->setPayGradeId( $arrValues['pay_grade_id'] );
		if( isset( $arrValues['rank_insignia'] ) && $boolDirectSet ) $this->set( 'm_strRankInsignia', trim( stripcslashes( $arrValues['rank_insignia'] ) ) ); elseif( isset( $arrValues['rank_insignia'] ) ) $this->setRankInsignia( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['rank_insignia'] ) : $arrValues['rank_insignia'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setMilitaryComponentId( $intMilitaryComponentId ) {
		$this->set( 'm_intMilitaryComponentId', CStrings::strToIntDef( $intMilitaryComponentId, NULL, false ) );
	}

	public function getMilitaryComponentId() {
		return $this->m_intMilitaryComponentId;
	}

	public function sqlMilitaryComponentId() {
		return ( true == isset( $this->m_intMilitaryComponentId ) ) ? ( string ) $this->m_intMilitaryComponentId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 60, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setMilitaryRanksAbbreviation( $strMilitaryRanksAbbreviation ) {
		$this->set( 'm_strMilitaryRanksAbbreviation', CStrings::strTrimDef( $strMilitaryRanksAbbreviation, 256, NULL, true ) );
	}

	public function getMilitaryRanksAbbreviation() {
		return $this->m_strMilitaryRanksAbbreviation;
	}

	public function sqlMilitaryRanksAbbreviation() {
		return ( true == isset( $this->m_strMilitaryRanksAbbreviation ) ) ? '\'' . addslashes( $this->m_strMilitaryRanksAbbreviation ) . '\'' : 'NULL';
	}

	public function setPayGradeId( $intPayGradeId ) {
		$this->set( 'm_intPayGradeId', CStrings::strToIntDef( $intPayGradeId, NULL, false ) );
	}

	public function getPayGradeId() {
		return $this->m_intPayGradeId;
	}

	public function sqlPayGradeId() {
		return ( true == isset( $this->m_intPayGradeId ) ) ? ( string ) $this->m_intPayGradeId : 'NULL';
	}

	public function setRankInsignia( $strRankInsignia ) {
		$this->set( 'm_strRankInsignia', CStrings::strTrimDef( $strRankInsignia, 256, NULL, true ) );
	}

	public function getRankInsignia() {
		return $this->m_strRankInsignia;
	}

	public function sqlRankInsignia() {
		return ( true == isset( $this->m_strRankInsignia ) ) ? '\'' . addslashes( $this->m_strRankInsignia ) . '\'' : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'military_component_id' => $this->getMilitaryComponentId(),
			'name' => $this->getName(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails(),
			'military_ranks_abbreviation' => $this->getMilitaryRanksAbbreviation(),
			'pay_grade_id' => $this->getPayGradeId(),
			'rank_insignia' => $this->getRankInsignia()
		);
	}

}
?>