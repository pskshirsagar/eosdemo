<?php

class CBaseMilitaryHousingAreaZipcode extends CEosSingularBase {

	const TABLE_NAME = 'public.military_housing_area_zipcodes';

	protected $m_intId;
	protected $m_intMilitaryHousingAreaId;
	protected $m_strZipcode;
	protected $m_boolIsPublished;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['military_housing_area_id'] ) && $boolDirectSet ) $this->set( 'm_intMilitaryHousingAreaId', trim( $arrValues['military_housing_area_id'] ) ); elseif( isset( $arrValues['military_housing_area_id'] ) ) $this->setMilitaryHousingAreaId( $arrValues['military_housing_area_id'] );
		if( isset( $arrValues['zipcode'] ) && $boolDirectSet ) $this->set( 'm_strZipcode', trim( stripcslashes( $arrValues['zipcode'] ) ) ); elseif( isset( $arrValues['zipcode'] ) ) $this->setZipcode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['zipcode'] ) : $arrValues['zipcode'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setMilitaryHousingAreaId( $intMilitaryHousingAreaId ) {
		$this->set( 'm_intMilitaryHousingAreaId', CStrings::strToIntDef( $intMilitaryHousingAreaId, NULL, false ) );
	}

	public function getMilitaryHousingAreaId() {
		return $this->m_intMilitaryHousingAreaId;
	}

	public function sqlMilitaryHousingAreaId() {
		return ( true == isset( $this->m_intMilitaryHousingAreaId ) ) ? ( string ) $this->m_intMilitaryHousingAreaId : 'NULL';
	}

	public function setZipcode( $strZipcode ) {
		$this->set( 'm_strZipcode', CStrings::strTrimDef( $strZipcode, 5, NULL, true ) );
	}

	public function getZipcode() {
		return $this->m_strZipcode;
	}

	public function sqlZipcode() {
		return ( true == isset( $this->m_strZipcode ) ) ? '\'' . addslashes( $this->m_strZipcode ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'military_housing_area_id' => $this->getMilitaryHousingAreaId(),
			'zipcode' => $this->getZipcode(),
			'is_published' => $this->getIsPublished()
		);
	}

}
?>