<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CSystemEmailTypes
 * Do not add any new functions to this class.
 */

class CBaseSystemEmailTypes extends CEosPluralBase {

	/**
	 * @return CSystemEmailType[]
	 */
	public static function fetchSystemEmailTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSystemEmailType::class, $objDatabase );
	}

	/**
	 * @return CSystemEmailType
	 */
	public static function fetchSystemEmailType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSystemEmailType::class, $objDatabase );
	}

	public static function fetchSystemEmailTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_email_types', $objDatabase );
	}

	public static function fetchSystemEmailTypeById( $intId, $objDatabase ) {
		return self::fetchSystemEmailType( sprintf( 'SELECT * FROM system_email_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchSystemEmailTypesBySystemEmailCategoryId( $intSystemEmailCategoryId, $objDatabase ) {
		return self::fetchSystemEmailTypes( sprintf( 'SELECT * FROM system_email_types WHERE system_email_category_id = %d', $intSystemEmailCategoryId ), $objDatabase );
	}

	public static function fetchSystemEmailTypesBySystemEmailPriorityId( $intSystemEmailPriorityId, $objDatabase ) {
		return self::fetchSystemEmailTypes( sprintf( 'SELECT * FROM system_email_types WHERE system_email_priority_id = %d', $intSystemEmailPriorityId ), $objDatabase );
	}

	public static function fetchSystemEmailTypesByEmailServiceProviderId( $intEmailServiceProviderId, $objDatabase ) {
		return self::fetchSystemEmailTypes( sprintf( 'SELECT * FROM system_email_types WHERE email_service_provider_id = %d', $intEmailServiceProviderId ), $objDatabase );
	}

	public static function fetchSystemEmailTypesByDocumentSubTypeId( $intDocumentSubTypeId, $objDatabase ) {
		return self::fetchSystemEmailTypes( sprintf( 'SELECT * FROM system_email_types WHERE document_sub_type_id = %d', $intDocumentSubTypeId ), $objDatabase );
	}

	public static function fetchSystemEmailTypesBySampleFileId( $intSampleFileId, $objDatabase ) {
		return self::fetchSystemEmailTypes( sprintf( 'SELECT * FROM system_email_types WHERE sample_file_id = %d', $intSampleFileId ), $objDatabase );
	}

}
?>