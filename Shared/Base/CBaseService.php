<?php

class CBaseService extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.services';

	protected $m_intId;
	protected $m_intMinorApiVersionId;
	protected $m_intServiceGroupId;
	protected $m_intMajorApiVersionId;
	protected $m_intServiceTypeId;
	protected $m_arrintServiceAuthenticationTypeIds;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strServiceThresholds;
	protected $m_strProposedDeprecationDate;
	protected $m_strDeprecationDescription;
	protected $m_intIsWebVisible;
	protected $m_intIsPublished;
	protected $m_intIsTryYourselfEnabled;
	protected $m_intIsLoggingEnabled;
	protected $m_boolIsDefaultApiVersion;
	protected $m_intOrderNum;
	protected $m_intDeprecatedBy;
	protected $m_strDeprecatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strChangeLog;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intServiceTypeId = '1';
		$this->m_intIsWebVisible = '0';
		$this->m_intIsPublished = '1';
		$this->m_intIsTryYourselfEnabled = '0';
		$this->m_intIsLoggingEnabled = '0';
		$this->m_boolIsDefaultApiVersion = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['minor_api_version_id'] ) && $boolDirectSet ) $this->set( 'm_intMinorApiVersionId', trim( $arrValues['minor_api_version_id'] ) ); elseif( isset( $arrValues['minor_api_version_id'] ) ) $this->setMinorApiVersionId( $arrValues['minor_api_version_id'] );
		if( isset( $arrValues['service_group_id'] ) && $boolDirectSet ) $this->set( 'm_intServiceGroupId', trim( $arrValues['service_group_id'] ) ); elseif( isset( $arrValues['service_group_id'] ) ) $this->setServiceGroupId( $arrValues['service_group_id'] );
		if( isset( $arrValues['major_api_version_id'] ) && $boolDirectSet ) $this->set( 'm_intMajorApiVersionId', trim( $arrValues['major_api_version_id'] ) ); elseif( isset( $arrValues['major_api_version_id'] ) ) $this->setMajorApiVersionId( $arrValues['major_api_version_id'] );
		if( isset( $arrValues['service_type_id'] ) && $boolDirectSet ) $this->set( 'm_intServiceTypeId', trim( $arrValues['service_type_id'] ) ); elseif( isset( $arrValues['service_type_id'] ) ) $this->setServiceTypeId( $arrValues['service_type_id'] );
		if( isset( $arrValues['service_authentication_type_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintServiceAuthenticationTypeIds', trim( $arrValues['service_authentication_type_ids'] ) ); elseif( isset( $arrValues['service_authentication_type_ids'] ) ) $this->setServiceAuthenticationTypeIds( $arrValues['service_authentication_type_ids'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['service_thresholds'] ) && $boolDirectSet ) $this->set( 'm_strServiceThresholds', trim( stripcslashes( $arrValues['service_thresholds'] ) ) ); elseif( isset( $arrValues['service_thresholds'] ) ) $this->setServiceThresholds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['service_thresholds'] ) : $arrValues['service_thresholds'] );
		if( isset( $arrValues['proposed_deprecation_date'] ) && $boolDirectSet ) $this->set( 'm_strProposedDeprecationDate', trim( $arrValues['proposed_deprecation_date'] ) ); elseif( isset( $arrValues['proposed_deprecation_date'] ) ) $this->setProposedDeprecationDate( $arrValues['proposed_deprecation_date'] );
		if( isset( $arrValues['deprecation_description'] ) && $boolDirectSet ) $this->set( 'm_strDeprecationDescription', trim( $arrValues['deprecation_description'] ) ); elseif( isset( $arrValues['deprecation_description'] ) ) $this->setDeprecationDescription( $arrValues['deprecation_description'] );
		if( isset( $arrValues['is_web_visible'] ) && $boolDirectSet ) $this->set( 'm_intIsWebVisible', trim( $arrValues['is_web_visible'] ) ); elseif( isset( $arrValues['is_web_visible'] ) ) $this->setIsWebVisible( $arrValues['is_web_visible'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_try_yourself_enabled'] ) && $boolDirectSet ) $this->set( 'm_intIsTryYourselfEnabled', trim( $arrValues['is_try_yourself_enabled'] ) ); elseif( isset( $arrValues['is_try_yourself_enabled'] ) ) $this->setIsTryYourselfEnabled( $arrValues['is_try_yourself_enabled'] );
		if( isset( $arrValues['is_logging_enabled'] ) && $boolDirectSet ) $this->set( 'm_intIsLoggingEnabled', trim( $arrValues['is_logging_enabled'] ) ); elseif( isset( $arrValues['is_logging_enabled'] ) ) $this->setIsLoggingEnabled( $arrValues['is_logging_enabled'] );
		if( isset( $arrValues['is_default_api_version'] ) && $boolDirectSet ) $this->set( 'm_boolIsDefaultApiVersion', trim( stripcslashes( $arrValues['is_default_api_version'] ) ) ); elseif( isset( $arrValues['is_default_api_version'] ) ) $this->setIsDefaultApiVersion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_default_api_version'] ) : $arrValues['is_default_api_version'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deprecated_by'] ) && $boolDirectSet ) $this->set( 'm_intDeprecatedBy', trim( $arrValues['deprecated_by'] ) ); elseif( isset( $arrValues['deprecated_by'] ) ) $this->setDeprecatedBy( $arrValues['deprecated_by'] );
		if( isset( $arrValues['deprecated_on'] ) && $boolDirectSet ) $this->set( 'm_strDeprecatedOn', trim( $arrValues['deprecated_on'] ) ); elseif( isset( $arrValues['deprecated_on'] ) ) $this->setDeprecatedOn( $arrValues['deprecated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['change_log'] ) && $boolDirectSet ) $this->set( 'm_strChangeLog', trim( $arrValues['change_log'] ) ); elseif( isset( $arrValues['change_log'] ) ) $this->setChangeLog( $arrValues['change_log'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setMinorApiVersionId( $intMinorApiVersionId ) {
		$this->set( 'm_intMinorApiVersionId', CStrings::strToIntDef( $intMinorApiVersionId, NULL, false ) );
	}

	public function getMinorApiVersionId() {
		return $this->m_intMinorApiVersionId;
	}

	public function sqlMinorApiVersionId() {
		return ( true == isset( $this->m_intMinorApiVersionId ) ) ? ( string ) $this->m_intMinorApiVersionId : 'NULL';
	}

	public function setServiceGroupId( $intServiceGroupId ) {
		$this->set( 'm_intServiceGroupId', CStrings::strToIntDef( $intServiceGroupId, NULL, false ) );
	}

	public function getServiceGroupId() {
		return $this->m_intServiceGroupId;
	}

	public function sqlServiceGroupId() {
		return ( true == isset( $this->m_intServiceGroupId ) ) ? ( string ) $this->m_intServiceGroupId : 'NULL';
	}

	public function setMajorApiVersionId( $intMajorApiVersionId ) {
		$this->set( 'm_intMajorApiVersionId', CStrings::strToIntDef( $intMajorApiVersionId, NULL, false ) );
	}

	public function getMajorApiVersionId() {
		return $this->m_intMajorApiVersionId;
	}

	public function sqlMajorApiVersionId() {
		return ( true == isset( $this->m_intMajorApiVersionId ) ) ? ( string ) $this->m_intMajorApiVersionId : 'NULL';
	}

	public function setServiceTypeId( $intServiceTypeId ) {
		$this->set( 'm_intServiceTypeId', CStrings::strToIntDef( $intServiceTypeId, NULL, false ) );
	}

	public function getServiceTypeId() {
		return $this->m_intServiceTypeId;
	}

	public function sqlServiceTypeId() {
		return ( true == isset( $this->m_intServiceTypeId ) ) ? ( string ) $this->m_intServiceTypeId : '1';
	}

	public function setServiceAuthenticationTypeIds( $arrintServiceAuthenticationTypeIds ) {
		$this->set( 'm_arrintServiceAuthenticationTypeIds', CStrings::strToArrIntDef( $arrintServiceAuthenticationTypeIds, NULL ) );
	}

	public function getServiceAuthenticationTypeIds() {
		return $this->m_arrintServiceAuthenticationTypeIds;
	}

	public function sqlServiceAuthenticationTypeIds() {
		return ( true == isset( $this->m_arrintServiceAuthenticationTypeIds ) && true == valArr( $this->m_arrintServiceAuthenticationTypeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintServiceAuthenticationTypeIds, NULL ) . '\'' : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 2000, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setServiceThresholds( $strServiceThresholds ) {
		$this->set( 'm_strServiceThresholds', CStrings::strTrimDef( $strServiceThresholds, -1, NULL, true ) );
	}

	public function getServiceThresholds() {
		return $this->m_strServiceThresholds;
	}

	public function sqlServiceThresholds() {
		return ( true == isset( $this->m_strServiceThresholds ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strServiceThresholds ) : '\'' . addslashes( $this->m_strServiceThresholds ) . '\'' ) : 'NULL';
	}

	public function setProposedDeprecationDate( $strProposedDeprecationDate ) {
		$this->set( 'm_strProposedDeprecationDate', CStrings::strTrimDef( $strProposedDeprecationDate, -1, NULL, true ) );
	}

	public function getProposedDeprecationDate() {
		return $this->m_strProposedDeprecationDate;
	}

	public function sqlProposedDeprecationDate() {
		return ( true == isset( $this->m_strProposedDeprecationDate ) ) ? '\'' . $this->m_strProposedDeprecationDate . '\'' : 'NULL';
	}

	public function setDeprecationDescription( $strDeprecationDescription ) {
		$this->set( 'm_strDeprecationDescription', CStrings::strTrimDef( $strDeprecationDescription, -1, NULL, true ) );
	}

	public function getDeprecationDescription() {
		return $this->m_strDeprecationDescription;
	}

	public function sqlDeprecationDescription() {
		return ( true == isset( $this->m_strDeprecationDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDeprecationDescription ) : '\'' . addslashes( $this->m_strDeprecationDescription ) . '\'' ) : 'NULL';
	}

	public function setIsWebVisible( $intIsWebVisible ) {
		$this->set( 'm_intIsWebVisible', CStrings::strToIntDef( $intIsWebVisible, NULL, false ) );
	}

	public function getIsWebVisible() {
		return $this->m_intIsWebVisible;
	}

	public function sqlIsWebVisible() {
		return ( true == isset( $this->m_intIsWebVisible ) ) ? ( string ) $this->m_intIsWebVisible : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsTryYourselfEnabled( $intIsTryYourselfEnabled ) {
		$this->set( 'm_intIsTryYourselfEnabled', CStrings::strToIntDef( $intIsTryYourselfEnabled, NULL, false ) );
	}

	public function getIsTryYourselfEnabled() {
		return $this->m_intIsTryYourselfEnabled;
	}

	public function sqlIsTryYourselfEnabled() {
		return ( true == isset( $this->m_intIsTryYourselfEnabled ) ) ? ( string ) $this->m_intIsTryYourselfEnabled : '0';
	}

	public function setIsLoggingEnabled( $intIsLoggingEnabled ) {
		$this->set( 'm_intIsLoggingEnabled', CStrings::strToIntDef( $intIsLoggingEnabled, NULL, false ) );
	}

	public function getIsLoggingEnabled() {
		return $this->m_intIsLoggingEnabled;
	}

	public function sqlIsLoggingEnabled() {
		return ( true == isset( $this->m_intIsLoggingEnabled ) ) ? ( string ) $this->m_intIsLoggingEnabled : '0';
	}

	public function setIsDefaultApiVersion( $boolIsDefaultApiVersion ) {
		$this->set( 'm_boolIsDefaultApiVersion', CStrings::strToBool( $boolIsDefaultApiVersion ) );
	}

	public function getIsDefaultApiVersion() {
		return $this->m_boolIsDefaultApiVersion;
	}

	public function sqlIsDefaultApiVersion() {
		return ( true == isset( $this->m_boolIsDefaultApiVersion ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDefaultApiVersion ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeprecatedBy( $intDeprecatedBy ) {
		$this->set( 'm_intDeprecatedBy', CStrings::strToIntDef( $intDeprecatedBy, NULL, false ) );
	}

	public function getDeprecatedBy() {
		return $this->m_intDeprecatedBy;
	}

	public function sqlDeprecatedBy() {
		return ( true == isset( $this->m_intDeprecatedBy ) ) ? ( string ) $this->m_intDeprecatedBy : 'NULL';
	}

	public function setDeprecatedOn( $strDeprecatedOn ) {
		$this->set( 'm_strDeprecatedOn', CStrings::strTrimDef( $strDeprecatedOn, -1, NULL, true ) );
	}

	public function getDeprecatedOn() {
		return $this->m_strDeprecatedOn;
	}

	public function sqlDeprecatedOn() {
		return ( true == isset( $this->m_strDeprecatedOn ) ) ? '\'' . $this->m_strDeprecatedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setChangeLog( $strChangeLog ) {
		$this->set( 'm_strChangeLog', CStrings::strTrimDef( $strChangeLog, -1, NULL, true ) );
	}

	public function getChangeLog() {
		return $this->m_strChangeLog;
	}

	public function sqlChangeLog() {
		return ( true == isset( $this->m_strChangeLog ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strChangeLog ) : '\'' . addslashes( $this->m_strChangeLog ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, minor_api_version_id, service_group_id, major_api_version_id, service_type_id, service_authentication_type_ids, name, description, service_thresholds, proposed_deprecation_date, deprecation_description, is_web_visible, is_published, is_try_yourself_enabled, is_logging_enabled, is_default_api_version, order_num, deprecated_by, deprecated_on, updated_by, updated_on, created_by, created_on, change_log, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlMinorApiVersionId() . ', ' .
						$this->sqlServiceGroupId() . ', ' .
						$this->sqlMajorApiVersionId() . ', ' .
						$this->sqlServiceTypeId() . ', ' .
						$this->sqlServiceAuthenticationTypeIds() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlServiceThresholds() . ', ' .
						$this->sqlProposedDeprecationDate() . ', ' .
						$this->sqlDeprecationDescription() . ', ' .
						$this->sqlIsWebVisible() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsTryYourselfEnabled() . ', ' .
						$this->sqlIsLoggingEnabled() . ', ' .
						$this->sqlIsDefaultApiVersion() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDeprecatedBy() . ', ' .
						$this->sqlDeprecatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlChangeLog() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minor_api_version_id = ' . $this->sqlMinorApiVersionId(). ',' ; } elseif( true == array_key_exists( 'MinorApiVersionId', $this->getChangedColumns() ) ) { $strSql .= ' minor_api_version_id = ' . $this->sqlMinorApiVersionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_group_id = ' . $this->sqlServiceGroupId(). ',' ; } elseif( true == array_key_exists( 'ServiceGroupId', $this->getChangedColumns() ) ) { $strSql .= ' service_group_id = ' . $this->sqlServiceGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' major_api_version_id = ' . $this->sqlMajorApiVersionId(). ',' ; } elseif( true == array_key_exists( 'MajorApiVersionId', $this->getChangedColumns() ) ) { $strSql .= ' major_api_version_id = ' . $this->sqlMajorApiVersionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_type_id = ' . $this->sqlServiceTypeId(). ',' ; } elseif( true == array_key_exists( 'ServiceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' service_type_id = ' . $this->sqlServiceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_authentication_type_ids = ' . $this->sqlServiceAuthenticationTypeIds(). ',' ; } elseif( true == array_key_exists( 'ServiceAuthenticationTypeIds', $this->getChangedColumns() ) ) { $strSql .= ' service_authentication_type_ids = ' . $this->sqlServiceAuthenticationTypeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_thresholds = ' . $this->sqlServiceThresholds(). ',' ; } elseif( true == array_key_exists( 'ServiceThresholds', $this->getChangedColumns() ) ) { $strSql .= ' service_thresholds = ' . $this->sqlServiceThresholds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proposed_deprecation_date = ' . $this->sqlProposedDeprecationDate(). ',' ; } elseif( true == array_key_exists( 'ProposedDeprecationDate', $this->getChangedColumns() ) ) { $strSql .= ' proposed_deprecation_date = ' . $this->sqlProposedDeprecationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deprecation_description = ' . $this->sqlDeprecationDescription(). ',' ; } elseif( true == array_key_exists( 'DeprecationDescription', $this->getChangedColumns() ) ) { $strSql .= ' deprecation_description = ' . $this->sqlDeprecationDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_web_visible = ' . $this->sqlIsWebVisible(). ',' ; } elseif( true == array_key_exists( 'IsWebVisible', $this->getChangedColumns() ) ) { $strSql .= ' is_web_visible = ' . $this->sqlIsWebVisible() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_try_yourself_enabled = ' . $this->sqlIsTryYourselfEnabled(). ',' ; } elseif( true == array_key_exists( 'IsTryYourselfEnabled', $this->getChangedColumns() ) ) { $strSql .= ' is_try_yourself_enabled = ' . $this->sqlIsTryYourselfEnabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_logging_enabled = ' . $this->sqlIsLoggingEnabled(). ',' ; } elseif( true == array_key_exists( 'IsLoggingEnabled', $this->getChangedColumns() ) ) { $strSql .= ' is_logging_enabled = ' . $this->sqlIsLoggingEnabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default_api_version = ' . $this->sqlIsDefaultApiVersion(). ',' ; } elseif( true == array_key_exists( 'IsDefaultApiVersion', $this->getChangedColumns() ) ) { $strSql .= ' is_default_api_version = ' . $this->sqlIsDefaultApiVersion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deprecated_by = ' . $this->sqlDeprecatedBy(). ',' ; } elseif( true == array_key_exists( 'DeprecatedBy', $this->getChangedColumns() ) ) { $strSql .= ' deprecated_by = ' . $this->sqlDeprecatedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deprecated_on = ' . $this->sqlDeprecatedOn(). ',' ; } elseif( true == array_key_exists( 'DeprecatedOn', $this->getChangedColumns() ) ) { $strSql .= ' deprecated_on = ' . $this->sqlDeprecatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' change_log = ' . $this->sqlChangeLog(). ',' ; } elseif( true == array_key_exists( 'ChangeLog', $this->getChangedColumns() ) ) { $strSql .= ' change_log = ' . $this->sqlChangeLog() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'minor_api_version_id' => $this->getMinorApiVersionId(),
			'service_group_id' => $this->getServiceGroupId(),
			'major_api_version_id' => $this->getMajorApiVersionId(),
			'service_type_id' => $this->getServiceTypeId(),
			'service_authentication_type_ids' => $this->getServiceAuthenticationTypeIds(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'service_thresholds' => $this->getServiceThresholds(),
			'proposed_deprecation_date' => $this->getProposedDeprecationDate(),
			'deprecation_description' => $this->getDeprecationDescription(),
			'is_web_visible' => $this->getIsWebVisible(),
			'is_published' => $this->getIsPublished(),
			'is_try_yourself_enabled' => $this->getIsTryYourselfEnabled(),
			'is_logging_enabled' => $this->getIsLoggingEnabled(),
			'is_default_api_version' => $this->getIsDefaultApiVersion(),
			'order_num' => $this->getOrderNum(),
			'deprecated_by' => $this->getDeprecatedBy(),
			'deprecated_on' => $this->getDeprecatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'change_log' => $this->getChangeLog(),
			'details' => $this->getDetails()
		);
	}

}
?>