<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseHelpQuiz extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.help_quizzes';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDefaultCid;
	protected $m_intCourseHelpResourceId;
	protected $m_strName;
	protected $m_strInstruction;
	protected $m_intBankedQuestionCount;
	protected $m_intIsRequiresSync;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intSectionHelpResourceId;
	protected $m_strHelpQuizType;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = '1';
		$this->m_intIsRequiresSync = '1';
		$this->m_strHelpQuizType = 'Help Quiz';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['default_cid'] ) && $boolDirectSet ) $this->set( 'm_intDefaultCid', trim( $arrValues['default_cid'] ) ); elseif( isset( $arrValues['default_cid'] ) ) $this->setDefaultCid( $arrValues['default_cid'] );
		if( isset( $arrValues['course_help_resource_id'] ) && $boolDirectSet ) $this->set( 'm_intCourseHelpResourceId', trim( $arrValues['course_help_resource_id'] ) ); elseif( isset( $arrValues['course_help_resource_id'] ) ) $this->setCourseHelpResourceId( $arrValues['course_help_resource_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['instruction'] ) && $boolDirectSet ) $this->set( 'm_strInstruction', trim( stripcslashes( $arrValues['instruction'] ) ) ); elseif( isset( $arrValues['instruction'] ) ) $this->setInstruction( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['instruction'] ) : $arrValues['instruction'] );
		if( isset( $arrValues['banked_question_count'] ) && $boolDirectSet ) $this->set( 'm_intBankedQuestionCount', trim( $arrValues['banked_question_count'] ) ); elseif( isset( $arrValues['banked_question_count'] ) ) $this->setBankedQuestionCount( $arrValues['banked_question_count'] );
		if( isset( $arrValues['is_requires_sync'] ) && $boolDirectSet ) $this->set( 'm_intIsRequiresSync', trim( $arrValues['is_requires_sync'] ) ); elseif( isset( $arrValues['is_requires_sync'] ) ) $this->setIsRequiresSync( $arrValues['is_requires_sync'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['section_help_resource_id'] ) && $boolDirectSet ) $this->set( 'm_intSectionHelpResourceId', trim( $arrValues['section_help_resource_id'] ) ); elseif( isset( $arrValues['section_help_resource_id'] ) ) $this->setSectionHelpResourceId( $arrValues['section_help_resource_id'] );
		if( isset( $arrValues['help_quiz_type'] ) && $boolDirectSet ) $this->set( 'm_strHelpQuizType', trim( stripcslashes( $arrValues['help_quiz_type'] ) ) ); elseif( isset( $arrValues['help_quiz_type'] ) ) $this->setHelpQuizType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['help_quiz_type'] ) : $arrValues['help_quiz_type'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : '1';
	}

	public function setDefaultCid( $intDefaultCid ) {
		$this->set( 'm_intDefaultCid', CStrings::strToIntDef( $intDefaultCid, NULL, false ) );
	}

	public function getDefaultCid() {
		return $this->m_intDefaultCid;
	}

	public function sqlDefaultCid() {
		return ( true == isset( $this->m_intDefaultCid ) ) ? ( string ) $this->m_intDefaultCid : 'NULL';
	}

	public function setCourseHelpResourceId( $intCourseHelpResourceId ) {
		$this->set( 'm_intCourseHelpResourceId', CStrings::strToIntDef( $intCourseHelpResourceId, NULL, false ) );
	}

	public function getCourseHelpResourceId() {
		return $this->m_intCourseHelpResourceId;
	}

	public function sqlCourseHelpResourceId() {
		return ( true == isset( $this->m_intCourseHelpResourceId ) ) ? ( string ) $this->m_intCourseHelpResourceId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setInstruction( $strInstruction ) {
		$this->set( 'm_strInstruction', CStrings::strTrimDef( $strInstruction, 240, NULL, true ) );
	}

	public function getInstruction() {
		return $this->m_strInstruction;
	}

	public function sqlInstruction() {
		return ( true == isset( $this->m_strInstruction ) ) ? '\'' . addslashes( $this->m_strInstruction ) . '\'' : 'NULL';
	}

	public function setBankedQuestionCount( $intBankedQuestionCount ) {
		$this->set( 'm_intBankedQuestionCount', CStrings::strToIntDef( $intBankedQuestionCount, NULL, false ) );
	}

	public function getBankedQuestionCount() {
		return $this->m_intBankedQuestionCount;
	}

	public function sqlBankedQuestionCount() {
		return ( true == isset( $this->m_intBankedQuestionCount ) ) ? ( string ) $this->m_intBankedQuestionCount : 'NULL';
	}

	public function setIsRequiresSync( $intIsRequiresSync ) {
		$this->set( 'm_intIsRequiresSync', CStrings::strToIntDef( $intIsRequiresSync, NULL, false ) );
	}

	public function getIsRequiresSync() {
		return $this->m_intIsRequiresSync;
	}

	public function sqlIsRequiresSync() {
		return ( true == isset( $this->m_intIsRequiresSync ) ) ? ( string ) $this->m_intIsRequiresSync : '1';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setSectionHelpResourceId( $intSectionHelpResourceId ) {
		$this->set( 'm_intSectionHelpResourceId', CStrings::strToIntDef( $intSectionHelpResourceId, NULL, false ) );
	}

	public function getSectionHelpResourceId() {
		return $this->m_intSectionHelpResourceId;
	}

	public function sqlSectionHelpResourceId() {
		return ( true == isset( $this->m_intSectionHelpResourceId ) ) ? ( string ) $this->m_intSectionHelpResourceId : 'NULL';
	}

	public function setHelpQuizType( $strHelpQuizType ) {
		$this->set( 'm_strHelpQuizType', CStrings::strTrimDef( $strHelpQuizType, -1, NULL, true ) );
	}

	public function getHelpQuizType() {
		return $this->m_strHelpQuizType;
	}

	public function sqlHelpQuizType() {
		return ( true == isset( $this->m_strHelpQuizType ) ) ? '\'' . addslashes( $this->m_strHelpQuizType ) . '\'' : '\'Help Quiz\'';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, default_cid, course_help_resource_id, name, instruction, banked_question_count, is_requires_sync, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, section_help_resource_id, help_quiz_type, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlDefaultCid() . ', ' .
						$this->sqlCourseHelpResourceId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlInstruction() . ', ' .
						$this->sqlBankedQuestionCount() . ', ' .
						$this->sqlIsRequiresSync() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlSectionHelpResourceId() . ', ' .
						$this->sqlHelpQuizType() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_cid = ' . $this->sqlDefaultCid(). ',' ; } elseif( true == array_key_exists( 'DefaultCid', $this->getChangedColumns() ) ) { $strSql .= ' default_cid = ' . $this->sqlDefaultCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' course_help_resource_id = ' . $this->sqlCourseHelpResourceId(). ',' ; } elseif( true == array_key_exists( 'CourseHelpResourceId', $this->getChangedColumns() ) ) { $strSql .= ' course_help_resource_id = ' . $this->sqlCourseHelpResourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' instruction = ' . $this->sqlInstruction(). ',' ; } elseif( true == array_key_exists( 'Instruction', $this->getChangedColumns() ) ) { $strSql .= ' instruction = ' . $this->sqlInstruction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' banked_question_count = ' . $this->sqlBankedQuestionCount(). ',' ; } elseif( true == array_key_exists( 'BankedQuestionCount', $this->getChangedColumns() ) ) { $strSql .= ' banked_question_count = ' . $this->sqlBankedQuestionCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_requires_sync = ' . $this->sqlIsRequiresSync(). ',' ; } elseif( true == array_key_exists( 'IsRequiresSync', $this->getChangedColumns() ) ) { $strSql .= ' is_requires_sync = ' . $this->sqlIsRequiresSync() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' section_help_resource_id = ' . $this->sqlSectionHelpResourceId(). ',' ; } elseif( true == array_key_exists( 'SectionHelpResourceId', $this->getChangedColumns() ) ) { $strSql .= ' section_help_resource_id = ' . $this->sqlSectionHelpResourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' help_quiz_type = ' . $this->sqlHelpQuizType(). ',' ; } elseif( true == array_key_exists( 'HelpQuizType', $this->getChangedColumns() ) ) { $strSql .= ' help_quiz_type = ' . $this->sqlHelpQuizType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'default_cid' => $this->getDefaultCid(),
			'course_help_resource_id' => $this->getCourseHelpResourceId(),
			'name' => $this->getName(),
			'instruction' => $this->getInstruction(),
			'banked_question_count' => $this->getBankedQuestionCount(),
			'is_requires_sync' => $this->getIsRequiresSync(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'section_help_resource_id' => $this->getSectionHelpResourceId(),
			'help_quiz_type' => $this->getHelpQuizType(),
			'details' => $this->getDetails()
		);
	}

}
?>