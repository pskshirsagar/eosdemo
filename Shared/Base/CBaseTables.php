<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Logs\CTables
 * Do not add any new functions to this class.
 */

class CBaseTables extends CEosPluralBase {

	/**
	 * @return CTable[]
	 */
	public static function fetchTables( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTable', $objDatabase );
	}

	/**
	 * @return CTable
	 */
	public static function fetchTable( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTable', $objDatabase );
	}

	public static function fetchTableCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'tables', $objDatabase );
	}

	public static function fetchTableById( $intId, $objDatabase ) {
		return self::fetchTable( sprintf( 'SELECT * FROM tables WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTablesByDatabaseId( $intDatabaseId, $objDatabase ) {
		return self::fetchTables( sprintf( 'SELECT * FROM tables WHERE database_id = %d', ( int ) $intDatabaseId ), $objDatabase );
	}

	public static function fetchTablesByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchTables( sprintf( 'SELECT * FROM tables WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchTablesByTeamId( $intTeamId, $objDatabase ) {
		return self::fetchTables( sprintf( 'SELECT * FROM tables WHERE team_id = %d', ( int ) $intTeamId ), $objDatabase );
	}

}
?>