<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseCompanyStatusTypes extends CEosPluralBase {

	/**
	 * @return CCompanyStatusType[]
	 */
	public static function fetchCompanyStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCompanyStatusType', $objDatabase );
	}

	/**
	 * @return CCompanyStatusType
	 */
	public static function fetchCompanyStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyStatusType', $objDatabase );
	}

	public static function fetchCompanyStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_status_types', $objDatabase );
	}

	public static function fetchCompanyStatusTypeById( $intId, $objDatabase ) {
		return self::fetchCompanyStatusType( sprintf( 'SELECT * FROM company_status_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>