<?php

class CBaseMilitaryRate extends CEosSingularBase {

	const TABLE_NAME = 'public.military_rates';

	protected $m_intId;
	protected $m_intMilitaryHousingAreaId;
	protected $m_intMilitaryPayGradeId;
	protected $m_intYear;
	protected $m_boolIsWithDependents;
	protected $m_fltRate;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsWithDependents = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['military_housing_area_id'] ) && $boolDirectSet ) $this->set( 'm_intMilitaryHousingAreaId', trim( $arrValues['military_housing_area_id'] ) ); elseif( isset( $arrValues['military_housing_area_id'] ) ) $this->setMilitaryHousingAreaId( $arrValues['military_housing_area_id'] );
		if( isset( $arrValues['military_pay_grade_id'] ) && $boolDirectSet ) $this->set( 'm_intMilitaryPayGradeId', trim( $arrValues['military_pay_grade_id'] ) ); elseif( isset( $arrValues['military_pay_grade_id'] ) ) $this->setMilitaryPayGradeId( $arrValues['military_pay_grade_id'] );
		if( isset( $arrValues['year'] ) && $boolDirectSet ) $this->set( 'm_intYear', trim( $arrValues['year'] ) ); elseif( isset( $arrValues['year'] ) ) $this->setYear( $arrValues['year'] );
		if( isset( $arrValues['is_with_dependents'] ) && $boolDirectSet ) $this->set( 'm_boolIsWithDependents', trim( stripcslashes( $arrValues['is_with_dependents'] ) ) ); elseif( isset( $arrValues['is_with_dependents'] ) ) $this->setIsWithDependents( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_with_dependents'] ) : $arrValues['is_with_dependents'] );
		if( isset( $arrValues['rate'] ) && $boolDirectSet ) $this->set( 'm_fltRate', trim( $arrValues['rate'] ) ); elseif( isset( $arrValues['rate'] ) ) $this->setRate( $arrValues['rate'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setMilitaryHousingAreaId( $intMilitaryHousingAreaId ) {
		$this->set( 'm_intMilitaryHousingAreaId', CStrings::strToIntDef( $intMilitaryHousingAreaId, NULL, false ) );
	}

	public function getMilitaryHousingAreaId() {
		return $this->m_intMilitaryHousingAreaId;
	}

	public function sqlMilitaryHousingAreaId() {
		return ( true == isset( $this->m_intMilitaryHousingAreaId ) ) ? ( string ) $this->m_intMilitaryHousingAreaId : 'NULL';
	}

	public function setMilitaryPayGradeId( $intMilitaryPayGradeId ) {
		$this->set( 'm_intMilitaryPayGradeId', CStrings::strToIntDef( $intMilitaryPayGradeId, NULL, false ) );
	}

	public function getMilitaryPayGradeId() {
		return $this->m_intMilitaryPayGradeId;
	}

	public function sqlMilitaryPayGradeId() {
		return ( true == isset( $this->m_intMilitaryPayGradeId ) ) ? ( string ) $this->m_intMilitaryPayGradeId : 'NULL';
	}

	public function setYear( $intYear ) {
		$this->set( 'm_intYear', CStrings::strToIntDef( $intYear, NULL, false ) );
	}

	public function getYear() {
		return $this->m_intYear;
	}

	public function sqlYear() {
		return ( true == isset( $this->m_intYear ) ) ? ( string ) $this->m_intYear : 'NULL';
	}

	public function setIsWithDependents( $boolIsWithDependents ) {
		$this->set( 'm_boolIsWithDependents', CStrings::strToBool( $boolIsWithDependents ) );
	}

	public function getIsWithDependents() {
		return $this->m_boolIsWithDependents;
	}

	public function sqlIsWithDependents() {
		return ( true == isset( $this->m_boolIsWithDependents ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsWithDependents ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRate( $fltRate ) {
		$this->set( 'm_fltRate', CStrings::strToFloatDef( $fltRate, NULL, false, 2 ) );
	}

	public function getRate() {
		return $this->m_fltRate;
	}

	public function sqlRate() {
		return ( true == isset( $this->m_fltRate ) ) ? ( string ) $this->m_fltRate : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'military_housing_area_id' => $this->getMilitaryHousingAreaId(),
			'military_pay_grade_id' => $this->getMilitaryPayGradeId(),
			'year' => $this->getYear(),
			'is_with_dependents' => $this->getIsWithDependents(),
			'rate' => $this->getRate()
		);
	}

}
?>