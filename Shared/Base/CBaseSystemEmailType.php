<?php

class CBaseSystemEmailType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.system_email_types';

	protected $m_intId;
	protected $m_intSystemEmailCategoryId;
	protected $m_intSystemEmailPriorityId;
	protected $m_intEmailServiceProviderId;
	protected $m_intDocumentSubTypeId;
	protected $m_intSampleFileId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_boolSendsToResident;
	protected $m_boolSendsToProspect;
	protected $m_boolSendsToEmployee;
	protected $m_boolSendsToPsEmployee;
	protected $m_intIsPublished;
	protected $m_intIsConfidential;
	protected $m_boolAllowDisabling;
	protected $m_boolAllowCustomHeaderText;
	protected $m_boolAllowCustomFooterText;
	protected $m_intOrderNum;
	protected $m_intCleanupInterval;
	protected $m_intIsPaused;
	protected $m_strLastSyncedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intSystemEmailCategoryId = '16';
		$this->m_intSystemEmailPriorityId = '3';
		$this->m_intEmailServiceProviderId = '10';
		$this->m_boolSendsToResident = false;
		$this->m_boolSendsToProspect = false;
		$this->m_boolSendsToEmployee = false;
		$this->m_boolSendsToPsEmployee = false;
		$this->m_intIsPublished = '1';
		$this->m_intIsConfidential = '0';
		$this->m_boolAllowDisabling = false;
		$this->m_boolAllowCustomHeaderText = false;
		$this->m_boolAllowCustomFooterText = false;
		$this->m_intOrderNum = '0';
		$this->m_intIsPaused = '0';
		$this->m_intUpdatedBy = '1';
		$this->m_strUpdatedOn = 'now()';
		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['system_email_category_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemEmailCategoryId', trim( $arrValues['system_email_category_id'] ) ); elseif( isset( $arrValues['system_email_category_id'] ) ) $this->setSystemEmailCategoryId( $arrValues['system_email_category_id'] );
		if( isset( $arrValues['system_email_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemEmailPriorityId', trim( $arrValues['system_email_priority_id'] ) ); elseif( isset( $arrValues['system_email_priority_id'] ) ) $this->setSystemEmailPriorityId( $arrValues['system_email_priority_id'] );
		if( isset( $arrValues['email_service_provider_id'] ) && $boolDirectSet ) $this->set( 'm_intEmailServiceProviderId', trim( $arrValues['email_service_provider_id'] ) ); elseif( isset( $arrValues['email_service_provider_id'] ) ) $this->setEmailServiceProviderId( $arrValues['email_service_provider_id'] );
		if( isset( $arrValues['document_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentSubTypeId', trim( $arrValues['document_sub_type_id'] ) ); elseif( isset( $arrValues['document_sub_type_id'] ) ) $this->setDocumentSubTypeId( $arrValues['document_sub_type_id'] );
		if( isset( $arrValues['sample_file_id'] ) && $boolDirectSet ) $this->set( 'm_intSampleFileId', trim( $arrValues['sample_file_id'] ) ); elseif( isset( $arrValues['sample_file_id'] ) ) $this->setSampleFileId( $arrValues['sample_file_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['sends_to_resident'] ) && $boolDirectSet ) $this->set( 'm_boolSendsToResident', trim( stripcslashes( $arrValues['sends_to_resident'] ) ) ); elseif( isset( $arrValues['sends_to_resident'] ) ) $this->setSendsToResident( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sends_to_resident'] ) : $arrValues['sends_to_resident'] );
		if( isset( $arrValues['sends_to_prospect'] ) && $boolDirectSet ) $this->set( 'm_boolSendsToProspect', trim( stripcslashes( $arrValues['sends_to_prospect'] ) ) ); elseif( isset( $arrValues['sends_to_prospect'] ) ) $this->setSendsToProspect( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sends_to_prospect'] ) : $arrValues['sends_to_prospect'] );
		if( isset( $arrValues['sends_to_employee'] ) && $boolDirectSet ) $this->set( 'm_boolSendsToEmployee', trim( stripcslashes( $arrValues['sends_to_employee'] ) ) ); elseif( isset( $arrValues['sends_to_employee'] ) ) $this->setSendsToEmployee( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sends_to_employee'] ) : $arrValues['sends_to_employee'] );
		if( isset( $arrValues['sends_to_ps_employee'] ) && $boolDirectSet ) $this->set( 'm_boolSendsToPsEmployee', trim( stripcslashes( $arrValues['sends_to_ps_employee'] ) ) ); elseif( isset( $arrValues['sends_to_ps_employee'] ) ) $this->setSendsToPsEmployee( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sends_to_ps_employee'] ) : $arrValues['sends_to_ps_employee'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_confidential'] ) && $boolDirectSet ) $this->set( 'm_intIsConfidential', trim( $arrValues['is_confidential'] ) ); elseif( isset( $arrValues['is_confidential'] ) ) $this->setIsConfidential( $arrValues['is_confidential'] );
		if( isset( $arrValues['allow_disabling'] ) && $boolDirectSet ) $this->set( 'm_boolAllowDisabling', trim( stripcslashes( $arrValues['allow_disabling'] ) ) ); elseif( isset( $arrValues['allow_disabling'] ) ) $this->setAllowDisabling( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_disabling'] ) : $arrValues['allow_disabling'] );
		if( isset( $arrValues['allow_custom_header_text'] ) && $boolDirectSet ) $this->set( 'm_boolAllowCustomHeaderText', trim( stripcslashes( $arrValues['allow_custom_header_text'] ) ) ); elseif( isset( $arrValues['allow_custom_header_text'] ) ) $this->setAllowCustomHeaderText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_custom_header_text'] ) : $arrValues['allow_custom_header_text'] );
		if( isset( $arrValues['allow_custom_footer_text'] ) && $boolDirectSet ) $this->set( 'm_boolAllowCustomFooterText', trim( stripcslashes( $arrValues['allow_custom_footer_text'] ) ) ); elseif( isset( $arrValues['allow_custom_footer_text'] ) ) $this->setAllowCustomFooterText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_custom_footer_text'] ) : $arrValues['allow_custom_footer_text'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['cleanup_interval'] ) && $boolDirectSet ) $this->set( 'm_intCleanupInterval', trim( $arrValues['cleanup_interval'] ) ); elseif( isset( $arrValues['cleanup_interval'] ) ) $this->setCleanupInterval( $arrValues['cleanup_interval'] );
		if( isset( $arrValues['is_paused'] ) && $boolDirectSet ) $this->set( 'm_intIsPaused', trim( $arrValues['is_paused'] ) ); elseif( isset( $arrValues['is_paused'] ) ) $this->setIsPaused( $arrValues['is_paused'] );
		if( isset( $arrValues['last_synced_on'] ) && $boolDirectSet ) $this->set( 'm_strLastSyncedOn', trim( $arrValues['last_synced_on'] ) ); elseif( isset( $arrValues['last_synced_on'] ) ) $this->setLastSyncedOn( $arrValues['last_synced_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSystemEmailCategoryId( $intSystemEmailCategoryId ) {
		$this->set( 'm_intSystemEmailCategoryId', CStrings::strToIntDef( $intSystemEmailCategoryId, NULL, false ) );
	}

	public function getSystemEmailCategoryId() {
		return $this->m_intSystemEmailCategoryId;
	}

	public function sqlSystemEmailCategoryId() {
		return ( true == isset( $this->m_intSystemEmailCategoryId ) ) ? ( string ) $this->m_intSystemEmailCategoryId : '16';
	}

	public function setSystemEmailPriorityId( $intSystemEmailPriorityId ) {
		$this->set( 'm_intSystemEmailPriorityId', CStrings::strToIntDef( $intSystemEmailPriorityId, NULL, false ) );
	}

	public function getSystemEmailPriorityId() {
		return $this->m_intSystemEmailPriorityId;
	}

	public function sqlSystemEmailPriorityId() {
		return ( true == isset( $this->m_intSystemEmailPriorityId ) ) ? ( string ) $this->m_intSystemEmailPriorityId : '3';
	}

	public function setEmailServiceProviderId( $intEmailServiceProviderId ) {
		$this->set( 'm_intEmailServiceProviderId', CStrings::strToIntDef( $intEmailServiceProviderId, NULL, false ) );
	}

	public function getEmailServiceProviderId() {
		return $this->m_intEmailServiceProviderId;
	}

	public function sqlEmailServiceProviderId() {
		return ( true == isset( $this->m_intEmailServiceProviderId ) ) ? ( string ) $this->m_intEmailServiceProviderId : '10';
	}

	public function setDocumentSubTypeId( $intDocumentSubTypeId ) {
		$this->set( 'm_intDocumentSubTypeId', CStrings::strToIntDef( $intDocumentSubTypeId, NULL, false ) );
	}

	public function getDocumentSubTypeId() {
		return $this->m_intDocumentSubTypeId;
	}

	public function sqlDocumentSubTypeId() {
		return ( true == isset( $this->m_intDocumentSubTypeId ) ) ? ( string ) $this->m_intDocumentSubTypeId : 'NULL';
	}

	public function setSampleFileId( $intSampleFileId ) {
		$this->set( 'm_intSampleFileId', CStrings::strToIntDef( $intSampleFileId, NULL, false ) );
	}

	public function getSampleFileId() {
		return $this->m_intSampleFileId;
	}

	public function sqlSampleFileId() {
		return ( true == isset( $this->m_intSampleFileId ) ) ? ( string ) $this->m_intSampleFileId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setSendsToResident( $boolSendsToResident ) {
		$this->set( 'm_boolSendsToResident', CStrings::strToBool( $boolSendsToResident ) );
	}

	public function getSendsToResident() {
		return $this->m_boolSendsToResident;
	}

	public function sqlSendsToResident() {
		return ( true == isset( $this->m_boolSendsToResident ) ) ? '\'' . ( true == ( bool ) $this->m_boolSendsToResident ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setSendsToProspect( $boolSendsToProspect ) {
		$this->set( 'm_boolSendsToProspect', CStrings::strToBool( $boolSendsToProspect ) );
	}

	public function getSendsToProspect() {
		return $this->m_boolSendsToProspect;
	}

	public function sqlSendsToProspect() {
		return ( true == isset( $this->m_boolSendsToProspect ) ) ? '\'' . ( true == ( bool ) $this->m_boolSendsToProspect ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setSendsToEmployee( $boolSendsToEmployee ) {
		$this->set( 'm_boolSendsToEmployee', CStrings::strToBool( $boolSendsToEmployee ) );
	}

	public function getSendsToEmployee() {
		return $this->m_boolSendsToEmployee;
	}

	public function sqlSendsToEmployee() {
		return ( true == isset( $this->m_boolSendsToEmployee ) ) ? '\'' . ( true == ( bool ) $this->m_boolSendsToEmployee ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setSendsToPsEmployee( $boolSendsToPsEmployee ) {
		$this->set( 'm_boolSendsToPsEmployee', CStrings::strToBool( $boolSendsToPsEmployee ) );
	}

	public function getSendsToPsEmployee() {
		return $this->m_boolSendsToPsEmployee;
	}

	public function sqlSendsToPsEmployee() {
		return ( true == isset( $this->m_boolSendsToPsEmployee ) ) ? '\'' . ( true == ( bool ) $this->m_boolSendsToPsEmployee ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsConfidential( $intIsConfidential ) {
		$this->set( 'm_intIsConfidential', CStrings::strToIntDef( $intIsConfidential, NULL, false ) );
	}

	public function getIsConfidential() {
		return $this->m_intIsConfidential;
	}

	public function sqlIsConfidential() {
		return ( true == isset( $this->m_intIsConfidential ) ) ? ( string ) $this->m_intIsConfidential : '0';
	}

	public function setAllowDisabling( $boolAllowDisabling ) {
		$this->set( 'm_boolAllowDisabling', CStrings::strToBool( $boolAllowDisabling ) );
	}

	public function getAllowDisabling() {
		return $this->m_boolAllowDisabling;
	}

	public function sqlAllowDisabling() {
		return ( true == isset( $this->m_boolAllowDisabling ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowDisabling ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowCustomHeaderText( $boolAllowCustomHeaderText ) {
		$this->set( 'm_boolAllowCustomHeaderText', CStrings::strToBool( $boolAllowCustomHeaderText ) );
	}

	public function getAllowCustomHeaderText() {
		return $this->m_boolAllowCustomHeaderText;
	}

	public function sqlAllowCustomHeaderText() {
		return ( true == isset( $this->m_boolAllowCustomHeaderText ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowCustomHeaderText ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowCustomFooterText( $boolAllowCustomFooterText ) {
		$this->set( 'm_boolAllowCustomFooterText', CStrings::strToBool( $boolAllowCustomFooterText ) );
	}

	public function getAllowCustomFooterText() {
		return $this->m_boolAllowCustomFooterText;
	}

	public function sqlAllowCustomFooterText() {
		return ( true == isset( $this->m_boolAllowCustomFooterText ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowCustomFooterText ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setCleanupInterval( $intCleanupInterval ) {
		$this->set( 'm_intCleanupInterval', CStrings::strToIntDef( $intCleanupInterval, NULL, false ) );
	}

	public function getCleanupInterval() {
		return $this->m_intCleanupInterval;
	}

	public function sqlCleanupInterval() {
		return ( true == isset( $this->m_intCleanupInterval ) ) ? ( string ) $this->m_intCleanupInterval : 'NULL';
	}

	public function setIsPaused( $intIsPaused ) {
		$this->set( 'm_intIsPaused', CStrings::strToIntDef( $intIsPaused, NULL, false ) );
	}

	public function getIsPaused() {
		return $this->m_intIsPaused;
	}

	public function sqlIsPaused() {
		return ( true == isset( $this->m_intIsPaused ) ) ? ( string ) $this->m_intIsPaused : '0';
	}

	public function setLastSyncedOn( $strLastSyncedOn ) {
		$this->set( 'm_strLastSyncedOn', CStrings::strTrimDef( $strLastSyncedOn, -1, NULL, true ) );
	}

	public function getLastSyncedOn() {
		return $this->m_strLastSyncedOn;
	}

	public function sqlLastSyncedOn() {
		return ( true == isset( $this->m_strLastSyncedOn ) ) ? '\'' . $this->m_strLastSyncedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : '1';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, system_email_category_id, system_email_priority_id, email_service_provider_id, document_sub_type_id, sample_file_id, name, description, sends_to_resident, sends_to_prospect, sends_to_employee, sends_to_ps_employee, is_published, is_confidential, allow_disabling, allow_custom_header_text, allow_custom_footer_text, order_num, cleanup_interval, is_paused, last_synced_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlSystemEmailCategoryId() . ', ' .
						$this->sqlSystemEmailPriorityId() . ', ' .
						$this->sqlEmailServiceProviderId() . ', ' .
						$this->sqlDocumentSubTypeId() . ', ' .
						$this->sqlSampleFileId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlSendsToResident() . ', ' .
						$this->sqlSendsToProspect() . ', ' .
						$this->sqlSendsToEmployee() . ', ' .
						$this->sqlSendsToPsEmployee() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsConfidential() . ', ' .
						$this->sqlAllowDisabling() . ', ' .
						$this->sqlAllowCustomHeaderText() . ', ' .
						$this->sqlAllowCustomFooterText() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlCleanupInterval() . ', ' .
						$this->sqlIsPaused() . ', ' .
						$this->sqlLastSyncedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_email_category_id = ' . $this->sqlSystemEmailCategoryId(). ',' ; } elseif( true == array_key_exists( 'SystemEmailCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' system_email_category_id = ' . $this->sqlSystemEmailCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_email_priority_id = ' . $this->sqlSystemEmailPriorityId(). ',' ; } elseif( true == array_key_exists( 'SystemEmailPriorityId', $this->getChangedColumns() ) ) { $strSql .= ' system_email_priority_id = ' . $this->sqlSystemEmailPriorityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_service_provider_id = ' . $this->sqlEmailServiceProviderId(). ',' ; } elseif( true == array_key_exists( 'EmailServiceProviderId', $this->getChangedColumns() ) ) { $strSql .= ' email_service_provider_id = ' . $this->sqlEmailServiceProviderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_sub_type_id = ' . $this->sqlDocumentSubTypeId(). ',' ; } elseif( true == array_key_exists( 'DocumentSubTypeId', $this->getChangedColumns() ) ) { $strSql .= ' document_sub_type_id = ' . $this->sqlDocumentSubTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sample_file_id = ' . $this->sqlSampleFileId(). ',' ; } elseif( true == array_key_exists( 'SampleFileId', $this->getChangedColumns() ) ) { $strSql .= ' sample_file_id = ' . $this->sqlSampleFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sends_to_resident = ' . $this->sqlSendsToResident(). ',' ; } elseif( true == array_key_exists( 'SendsToResident', $this->getChangedColumns() ) ) { $strSql .= ' sends_to_resident = ' . $this->sqlSendsToResident() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sends_to_prospect = ' . $this->sqlSendsToProspect(). ',' ; } elseif( true == array_key_exists( 'SendsToProspect', $this->getChangedColumns() ) ) { $strSql .= ' sends_to_prospect = ' . $this->sqlSendsToProspect() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sends_to_employee = ' . $this->sqlSendsToEmployee(). ',' ; } elseif( true == array_key_exists( 'SendsToEmployee', $this->getChangedColumns() ) ) { $strSql .= ' sends_to_employee = ' . $this->sqlSendsToEmployee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sends_to_ps_employee = ' . $this->sqlSendsToPsEmployee(). ',' ; } elseif( true == array_key_exists( 'SendsToPsEmployee', $this->getChangedColumns() ) ) { $strSql .= ' sends_to_ps_employee = ' . $this->sqlSendsToPsEmployee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_confidential = ' . $this->sqlIsConfidential(). ',' ; } elseif( true == array_key_exists( 'IsConfidential', $this->getChangedColumns() ) ) { $strSql .= ' is_confidential = ' . $this->sqlIsConfidential() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_disabling = ' . $this->sqlAllowDisabling(). ',' ; } elseif( true == array_key_exists( 'AllowDisabling', $this->getChangedColumns() ) ) { $strSql .= ' allow_disabling = ' . $this->sqlAllowDisabling() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_custom_header_text = ' . $this->sqlAllowCustomHeaderText(). ',' ; } elseif( true == array_key_exists( 'AllowCustomHeaderText', $this->getChangedColumns() ) ) { $strSql .= ' allow_custom_header_text = ' . $this->sqlAllowCustomHeaderText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_custom_footer_text = ' . $this->sqlAllowCustomFooterText(). ',' ; } elseif( true == array_key_exists( 'AllowCustomFooterText', $this->getChangedColumns() ) ) { $strSql .= ' allow_custom_footer_text = ' . $this->sqlAllowCustomFooterText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cleanup_interval = ' . $this->sqlCleanupInterval(). ',' ; } elseif( true == array_key_exists( 'CleanupInterval', $this->getChangedColumns() ) ) { $strSql .= ' cleanup_interval = ' . $this->sqlCleanupInterval() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_paused = ' . $this->sqlIsPaused(). ',' ; } elseif( true == array_key_exists( 'IsPaused', $this->getChangedColumns() ) ) { $strSql .= ' is_paused = ' . $this->sqlIsPaused() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_synced_on = ' . $this->sqlLastSyncedOn(). ',' ; } elseif( true == array_key_exists( 'LastSyncedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_synced_on = ' . $this->sqlLastSyncedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'system_email_category_id' => $this->getSystemEmailCategoryId(),
			'system_email_priority_id' => $this->getSystemEmailPriorityId(),
			'email_service_provider_id' => $this->getEmailServiceProviderId(),
			'document_sub_type_id' => $this->getDocumentSubTypeId(),
			'sample_file_id' => $this->getSampleFileId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'sends_to_resident' => $this->getSendsToResident(),
			'sends_to_prospect' => $this->getSendsToProspect(),
			'sends_to_employee' => $this->getSendsToEmployee(),
			'sends_to_ps_employee' => $this->getSendsToPsEmployee(),
			'is_published' => $this->getIsPublished(),
			'is_confidential' => $this->getIsConfidential(),
			'allow_disabling' => $this->getAllowDisabling(),
			'allow_custom_header_text' => $this->getAllowCustomHeaderText(),
			'allow_custom_footer_text' => $this->getAllowCustomFooterText(),
			'order_num' => $this->getOrderNum(),
			'cleanup_interval' => $this->getCleanupInterval(),
			'is_paused' => $this->getIsPaused(),
			'last_synced_on' => $this->getLastSyncedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>