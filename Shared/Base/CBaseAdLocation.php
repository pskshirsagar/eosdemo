<?php

class CBaseAdLocation extends CEosSingularBase {

	const TABLE_NAME = 'public.ad_locations';

	protected $m_intId;
	protected $m_intAdTypeId;
	protected $m_intAdEnvironmentId;
	protected $m_intSystemEmailTypeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intIsPublished;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ad_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAdTypeId', trim( $arrValues['ad_type_id'] ) ); elseif( isset( $arrValues['ad_type_id'] ) ) $this->setAdTypeId( $arrValues['ad_type_id'] );
		if( isset( $arrValues['ad_environment_id'] ) && $boolDirectSet ) $this->set( 'm_intAdEnvironmentId', trim( $arrValues['ad_environment_id'] ) ); elseif( isset( $arrValues['ad_environment_id'] ) ) $this->setAdEnvironmentId( $arrValues['ad_environment_id'] );
		if( isset( $arrValues['system_email_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemEmailTypeId', trim( $arrValues['system_email_type_id'] ) ); elseif( isset( $arrValues['system_email_type_id'] ) ) $this->setSystemEmailTypeId( $arrValues['system_email_type_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setAdTypeId( $intAdTypeId ) {
		$this->set( 'm_intAdTypeId', CStrings::strToIntDef( $intAdTypeId, NULL, false ) );
	}

	public function getAdTypeId() {
		return $this->m_intAdTypeId;
	}

	public function sqlAdTypeId() {
		return ( true == isset( $this->m_intAdTypeId ) ) ? ( string ) $this->m_intAdTypeId : 'NULL';
	}

	public function setAdEnvironmentId( $intAdEnvironmentId ) {
		$this->set( 'm_intAdEnvironmentId', CStrings::strToIntDef( $intAdEnvironmentId, NULL, false ) );
	}

	public function getAdEnvironmentId() {
		return $this->m_intAdEnvironmentId;
	}

	public function sqlAdEnvironmentId() {
		return ( true == isset( $this->m_intAdEnvironmentId ) ) ? ( string ) $this->m_intAdEnvironmentId : 'NULL';
	}

	public function setSystemEmailTypeId( $intSystemEmailTypeId ) {
		$this->set( 'm_intSystemEmailTypeId', CStrings::strToIntDef( $intSystemEmailTypeId, NULL, false ) );
	}

	public function getSystemEmailTypeId() {
		return $this->m_intSystemEmailTypeId;
	}

	public function sqlSystemEmailTypeId() {
		return ( true == isset( $this->m_intSystemEmailTypeId ) ) ? ( string ) $this->m_intSystemEmailTypeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ad_type_id' => $this->getAdTypeId(),
			'ad_environment_id' => $this->getAdEnvironmentId(),
			'system_email_type_id' => $this->getSystemEmailTypeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>