<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSubsidyIncomeLimits
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyIncomeLimits extends CEosPluralBase {

	/**
	 * @return CSubsidyIncomeLimit[]
	 */
	public static function fetchSubsidyIncomeLimits( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CSubsidyIncomeLimit::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSubsidyIncomeLimit
	 */
	public static function fetchSubsidyIncomeLimit( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyIncomeLimit::class, $objDatabase );
	}

	public static function fetchSubsidyIncomeLimitCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_income_limits', $objDatabase );
	}

	public static function fetchSubsidyIncomeLimitByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSubsidyIncomeLimit( sprintf( 'SELECT * FROM subsidy_income_limits WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyIncomeLimitsByCid( $intCid, $objDatabase ) {
		return self::fetchSubsidyIncomeLimits( sprintf( 'SELECT * FROM subsidy_income_limits WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyIncomeLimitsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSubsidyIncomeLimits( sprintf( 'SELECT * FROM subsidy_income_limits WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyIncomeLimitsBySubsidyIncomeLimitVersionIdByCid( $intSubsidyIncomeLimitVersionId, $intCid, $objDatabase ) {
		return self::fetchSubsidyIncomeLimits( sprintf( 'SELECT * FROM subsidy_income_limits WHERE subsidy_income_limit_version_id = %d AND cid = %d', ( int ) $intSubsidyIncomeLimitVersionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyIncomeLimitsBySubsidyIncomeLimitAreaIdByCid( $intSubsidyIncomeLimitAreaId, $intCid, $objDatabase ) {
		return self::fetchSubsidyIncomeLimits( sprintf( 'SELECT * FROM subsidy_income_limits WHERE subsidy_income_limit_area_id = %d AND cid = %d', ( int ) $intSubsidyIncomeLimitAreaId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyIncomeLimitsBySubsidyIncomeLevelTypeIdByCid( $intSubsidyIncomeLevelTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyIncomeLimits( sprintf( 'SELECT * FROM subsidy_income_limits WHERE subsidy_income_level_type_id = %d AND cid = %d', ( int ) $intSubsidyIncomeLevelTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyIncomeLimitsBySubsidyTypeIdByCid( $intSubsidyTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyIncomeLimits( sprintf( 'SELECT * FROM subsidy_income_limits WHERE subsidy_type_id = %d AND cid = %d', ( int ) $intSubsidyTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>