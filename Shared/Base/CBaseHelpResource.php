<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseHelpResource extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.help_resources';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDefaultCid;
	protected $m_intHelpResourceTypeId;
	protected $m_strHelpResourceSystem;
	protected $m_strTitle;
	protected $m_strContent;
	protected $m_strKeywords;
	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_strThumbnailPath;
	protected $m_strExternalPath;
	protected $m_intIsVisibleTo;
	protected $m_intIsRequiresSync;
	protected $m_intIsSearchable;
	protected $m_intIsCourseReset;
	protected $m_intIsPublished;
	protected $m_strDescription;
	protected $m_strExpireOn;
	protected $m_intOrderNum;
	protected $m_intYesCount;
	protected $m_intNoCount;
	protected $m_intViewsCount;
	protected $m_intCommentsCount;
	protected $m_intContentUpdatedBy;
	protected $m_strContentUpdatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intParentHelpResourceId;
	protected $m_strLocaleCode;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intArchivedBy;
	protected $m_strArchivedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = '1';
		$this->m_strHelpResourceSystem = 'ENTRATA';
		$this->m_intIsVisibleTo = '1';
		$this->m_intIsRequiresSync = '0';
		$this->m_intIsSearchable = '1';
		$this->m_intIsCourseReset = '0';
		$this->m_intIsPublished = '0';
		$this->m_intOrderNum = '0';
		$this->m_intYesCount = '0';
		$this->m_intNoCount = '0';
		$this->m_intViewsCount = '0';
		$this->m_intCommentsCount = '0';
		$this->m_strLocaleCode = 'en_US';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['default_cid'] ) && $boolDirectSet ) $this->set( 'm_intDefaultCid', trim( $arrValues['default_cid'] ) ); elseif( isset( $arrValues['default_cid'] ) ) $this->setDefaultCid( $arrValues['default_cid'] );
		if( isset( $arrValues['help_resource_type_id'] ) && $boolDirectSet ) $this->set( 'm_intHelpResourceTypeId', trim( $arrValues['help_resource_type_id'] ) ); elseif( isset( $arrValues['help_resource_type_id'] ) ) $this->setHelpResourceTypeId( $arrValues['help_resource_type_id'] );
		if( isset( $arrValues['help_resource_system'] ) && $boolDirectSet ) $this->set( 'm_strHelpResourceSystem', trim( stripcslashes( $arrValues['help_resource_system'] ) ) ); elseif( isset( $arrValues['help_resource_system'] ) ) $this->setHelpResourceSystem( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['help_resource_system'] ) : $arrValues['help_resource_system'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( $arrValues['title'] ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( $arrValues['title'] );
		if( isset( $arrValues['content'] ) && $boolDirectSet ) $this->set( 'm_strContent', trim( $arrValues['content'] ) ); elseif( isset( $arrValues['content'] ) ) $this->setContent( $arrValues['content'] );
		if( isset( $arrValues['keywords'] ) && $boolDirectSet ) $this->set( 'm_strKeywords', trim( $arrValues['keywords'] ) ); elseif( isset( $arrValues['keywords'] ) ) $this->setKeywords( $arrValues['keywords'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( $arrValues['file_name'] ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( $arrValues['file_name'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( $arrValues['file_path'] ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( $arrValues['file_path'] );
		if( isset( $arrValues['thumbnail_path'] ) && $boolDirectSet ) $this->set( 'm_strThumbnailPath', trim( $arrValues['thumbnail_path'] ) ); elseif( isset( $arrValues['thumbnail_path'] ) ) $this->setThumbnailPath( $arrValues['thumbnail_path'] );
		if( isset( $arrValues['external_path'] ) && $boolDirectSet ) $this->set( 'm_strExternalPath', trim( $arrValues['external_path'] ) ); elseif( isset( $arrValues['external_path'] ) ) $this->setExternalPath( $arrValues['external_path'] );
		if( isset( $arrValues['is_visible_to'] ) && $boolDirectSet ) $this->set( 'm_intIsVisibleTo', trim( $arrValues['is_visible_to'] ) ); elseif( isset( $arrValues['is_visible_to'] ) ) $this->setIsVisibleTo( $arrValues['is_visible_to'] );
		if( isset( $arrValues['is_requires_sync'] ) && $boolDirectSet ) $this->set( 'm_intIsRequiresSync', trim( $arrValues['is_requires_sync'] ) ); elseif( isset( $arrValues['is_requires_sync'] ) ) $this->setIsRequiresSync( $arrValues['is_requires_sync'] );
		if( isset( $arrValues['is_searchable'] ) && $boolDirectSet ) $this->set( 'm_intIsSearchable', trim( $arrValues['is_searchable'] ) ); elseif( isset( $arrValues['is_searchable'] ) ) $this->setIsSearchable( $arrValues['is_searchable'] );
		if( isset( $arrValues['is_course_reset'] ) && $boolDirectSet ) $this->set( 'm_intIsCourseReset', trim( $arrValues['is_course_reset'] ) ); elseif( isset( $arrValues['is_course_reset'] ) ) $this->setIsCourseReset( $arrValues['is_course_reset'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['expire_on'] ) && $boolDirectSet ) $this->set( 'm_strExpireOn', trim( $arrValues['expire_on'] ) ); elseif( isset( $arrValues['expire_on'] ) ) $this->setExpireOn( $arrValues['expire_on'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['yes_count'] ) && $boolDirectSet ) $this->set( 'm_intYesCount', trim( $arrValues['yes_count'] ) ); elseif( isset( $arrValues['yes_count'] ) ) $this->setYesCount( $arrValues['yes_count'] );
		if( isset( $arrValues['no_count'] ) && $boolDirectSet ) $this->set( 'm_intNoCount', trim( $arrValues['no_count'] ) ); elseif( isset( $arrValues['no_count'] ) ) $this->setNoCount( $arrValues['no_count'] );
		if( isset( $arrValues['views_count'] ) && $boolDirectSet ) $this->set( 'm_intViewsCount', trim( $arrValues['views_count'] ) ); elseif( isset( $arrValues['views_count'] ) ) $this->setViewsCount( $arrValues['views_count'] );
		if( isset( $arrValues['comments_count'] ) && $boolDirectSet ) $this->set( 'm_intCommentsCount', trim( $arrValues['comments_count'] ) ); elseif( isset( $arrValues['comments_count'] ) ) $this->setCommentsCount( $arrValues['comments_count'] );
		if( isset( $arrValues['content_updated_by'] ) && $boolDirectSet ) $this->set( 'm_intContentUpdatedBy', trim( $arrValues['content_updated_by'] ) ); elseif( isset( $arrValues['content_updated_by'] ) ) $this->setContentUpdatedBy( $arrValues['content_updated_by'] );
		if( isset( $arrValues['content_updated_on'] ) && $boolDirectSet ) $this->set( 'm_strContentUpdatedOn', trim( $arrValues['content_updated_on'] ) ); elseif( isset( $arrValues['content_updated_on'] ) ) $this->setContentUpdatedOn( $arrValues['content_updated_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['parent_help_resource_id'] ) && $boolDirectSet ) $this->set( 'm_intParentHelpResourceId', trim( $arrValues['parent_help_resource_id'] ) ); elseif( isset( $arrValues['parent_help_resource_id'] ) ) $this->setParentHelpResourceId( $arrValues['parent_help_resource_id'] );
		if( isset( $arrValues['locale_code'] ) && $boolDirectSet ) $this->set( 'm_strLocaleCode', trim( $arrValues['locale_code'] ) ); elseif( isset( $arrValues['locale_code'] ) ) $this->setLocaleCode( $arrValues['locale_code'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['archived_by'] ) && $boolDirectSet ) $this->set( 'm_intArchivedBy', trim( $arrValues['archived_by'] ) ); elseif( isset( $arrValues['archived_by'] ) ) $this->setArchivedBy( $arrValues['archived_by'] );
		if( isset( $arrValues['archived_on'] ) && $boolDirectSet ) $this->set( 'm_strArchivedOn', trim( $arrValues['archived_on'] ) ); elseif( isset( $arrValues['archived_on'] ) ) $this->setArchivedOn( $arrValues['archived_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : '1';
	}

	public function setDefaultCid( $intDefaultCid ) {
		$this->set( 'm_intDefaultCid', CStrings::strToIntDef( $intDefaultCid, NULL, false ) );
	}

	public function getDefaultCid() {
		return $this->m_intDefaultCid;
	}

	public function sqlDefaultCid() {
		return ( true == isset( $this->m_intDefaultCid ) ) ? ( string ) $this->m_intDefaultCid : 'NULL';
	}

	public function setHelpResourceTypeId( $intHelpResourceTypeId ) {
		$this->set( 'm_intHelpResourceTypeId', CStrings::strToIntDef( $intHelpResourceTypeId, NULL, false ) );
	}

	public function getHelpResourceTypeId() {
		return $this->m_intHelpResourceTypeId;
	}

	public function sqlHelpResourceTypeId() {
		return ( true == isset( $this->m_intHelpResourceTypeId ) ) ? ( string ) $this->m_intHelpResourceTypeId : 'NULL';
	}

	public function setHelpResourceSystem( $strHelpResourceSystem ) {
		$this->set( 'm_strHelpResourceSystem', CStrings::strTrimDef( $strHelpResourceSystem, -1, NULL, true ) );
	}

	public function getHelpResourceSystem() {
		return $this->m_strHelpResourceSystem;
	}

	public function sqlHelpResourceSystem() {
		return ( true == isset( $this->m_strHelpResourceSystem ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHelpResourceSystem ) : '\'' . addslashes( $this->m_strHelpResourceSystem ) . '\'' ) : '\'ENTRATA\'';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 500, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTitle ) : '\'' . addslashes( $this->m_strTitle ) . '\'' ) : 'NULL';
	}

	public function setContent( $strContent ) {
		$this->set( 'm_strContent', CStrings::strTrimDef( $strContent, -1, NULL, true ) );
	}

	public function getContent() {
		return $this->m_strContent;
	}

	public function sqlContent() {
		return ( true == isset( $this->m_strContent ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strContent ) : '\'' . addslashes( $this->m_strContent ) . '\'' ) : 'NULL';
	}

	public function setKeywords( $strKeywords ) {
		$this->set( 'm_strKeywords', CStrings::strTrimDef( $strKeywords, 1000, NULL, true ) );
	}

	public function getKeywords() {
		return $this->m_strKeywords;
	}

	public function sqlKeywords() {
		return ( true == isset( $this->m_strKeywords ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strKeywords ) : '\'' . addslashes( $this->m_strKeywords ) . '\'' ) : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 500, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFileName ) : '\'' . addslashes( $this->m_strFileName ) . '\'' ) : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 500, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFilePath ) : '\'' . addslashes( $this->m_strFilePath ) . '\'' ) : 'NULL';
	}

	public function setThumbnailPath( $strThumbnailPath ) {
		$this->set( 'm_strThumbnailPath', CStrings::strTrimDef( $strThumbnailPath, 500, NULL, true ) );
	}

	public function getThumbnailPath() {
		return $this->m_strThumbnailPath;
	}

	public function sqlThumbnailPath() {
		return ( true == isset( $this->m_strThumbnailPath ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strThumbnailPath ) : '\'' . addslashes( $this->m_strThumbnailPath ) . '\'' ) : 'NULL';
	}

	public function setExternalPath( $strExternalPath ) {
		$this->set( 'm_strExternalPath', CStrings::strTrimDef( $strExternalPath, 500, NULL, true ) );
	}

	public function getExternalPath() {
		return $this->m_strExternalPath;
	}

	public function sqlExternalPath() {
		return ( true == isset( $this->m_strExternalPath ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strExternalPath ) : '\'' . addslashes( $this->m_strExternalPath ) . '\'' ) : 'NULL';
	}

	public function setIsVisibleTo( $intIsVisibleTo ) {
		$this->set( 'm_intIsVisibleTo', CStrings::strToIntDef( $intIsVisibleTo, NULL, false ) );
	}

	public function getIsVisibleTo() {
		return $this->m_intIsVisibleTo;
	}

	public function sqlIsVisibleTo() {
		return ( true == isset( $this->m_intIsVisibleTo ) ) ? ( string ) $this->m_intIsVisibleTo : '1';
	}

	public function setIsRequiresSync( $intIsRequiresSync ) {
		$this->set( 'm_intIsRequiresSync', CStrings::strToIntDef( $intIsRequiresSync, NULL, false ) );
	}

	public function getIsRequiresSync() {
		return $this->m_intIsRequiresSync;
	}

	public function sqlIsRequiresSync() {
		return ( true == isset( $this->m_intIsRequiresSync ) ) ? ( string ) $this->m_intIsRequiresSync : '0';
	}

	public function setIsSearchable( $intIsSearchable ) {
		$this->set( 'm_intIsSearchable', CStrings::strToIntDef( $intIsSearchable, NULL, false ) );
	}

	public function getIsSearchable() {
		return $this->m_intIsSearchable;
	}

	public function sqlIsSearchable() {
		return ( true == isset( $this->m_intIsSearchable ) ) ? ( string ) $this->m_intIsSearchable : '1';
	}

	public function setIsCourseReset( $intIsCourseReset ) {
		$this->set( 'm_intIsCourseReset', CStrings::strToIntDef( $intIsCourseReset, NULL, false ) );
	}

	public function getIsCourseReset() {
		return $this->m_intIsCourseReset;
	}

	public function sqlIsCourseReset() {
		return ( true == isset( $this->m_intIsCourseReset ) ) ? ( string ) $this->m_intIsCourseReset : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '0';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 1500, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setExpireOn( $strExpireOn ) {
		$this->set( 'm_strExpireOn', CStrings::strTrimDef( $strExpireOn, -1, NULL, true ) );
	}

	public function getExpireOn() {
		return $this->m_strExpireOn;
	}

	public function sqlExpireOn() {
		return ( true == isset( $this->m_strExpireOn ) ) ? '\'' . $this->m_strExpireOn . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setYesCount( $intYesCount ) {
		$this->set( 'm_intYesCount', CStrings::strToIntDef( $intYesCount, NULL, false ) );
	}

	public function getYesCount() {
		return $this->m_intYesCount;
	}

	public function sqlYesCount() {
		return ( true == isset( $this->m_intYesCount ) ) ? ( string ) $this->m_intYesCount : '0';
	}

	public function setNoCount( $intNoCount ) {
		$this->set( 'm_intNoCount', CStrings::strToIntDef( $intNoCount, NULL, false ) );
	}

	public function getNoCount() {
		return $this->m_intNoCount;
	}

	public function sqlNoCount() {
		return ( true == isset( $this->m_intNoCount ) ) ? ( string ) $this->m_intNoCount : '0';
	}

	public function setViewsCount( $intViewsCount ) {
		$this->set( 'm_intViewsCount', CStrings::strToIntDef( $intViewsCount, NULL, false ) );
	}

	public function getViewsCount() {
		return $this->m_intViewsCount;
	}

	public function sqlViewsCount() {
		return ( true == isset( $this->m_intViewsCount ) ) ? ( string ) $this->m_intViewsCount : '0';
	}

	public function setCommentsCount( $intCommentsCount ) {
		$this->set( 'm_intCommentsCount', CStrings::strToIntDef( $intCommentsCount, NULL, false ) );
	}

	public function getCommentsCount() {
		return $this->m_intCommentsCount;
	}

	public function sqlCommentsCount() {
		return ( true == isset( $this->m_intCommentsCount ) ) ? ( string ) $this->m_intCommentsCount : '0';
	}

	public function setContentUpdatedBy( $intContentUpdatedBy ) {
		$this->set( 'm_intContentUpdatedBy', CStrings::strToIntDef( $intContentUpdatedBy, NULL, false ) );
	}

	public function getContentUpdatedBy() {
		return $this->m_intContentUpdatedBy;
	}

	public function sqlContentUpdatedBy() {
		return ( true == isset( $this->m_intContentUpdatedBy ) ) ? ( string ) $this->m_intContentUpdatedBy : 'NULL';
	}

	public function setContentUpdatedOn( $strContentUpdatedOn ) {
		$this->set( 'm_strContentUpdatedOn', CStrings::strTrimDef( $strContentUpdatedOn, -1, NULL, true ) );
	}

	public function getContentUpdatedOn() {
		return $this->m_strContentUpdatedOn;
	}

	public function sqlContentUpdatedOn() {
		return ( true == isset( $this->m_strContentUpdatedOn ) ) ? '\'' . $this->m_strContentUpdatedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setParentHelpResourceId( $intParentHelpResourceId ) {
		$this->set( 'm_intParentHelpResourceId', CStrings::strToIntDef( $intParentHelpResourceId, NULL, false ) );
	}

	public function getParentHelpResourceId() {
		return $this->m_intParentHelpResourceId;
	}

	public function sqlParentHelpResourceId() {
		return ( true == isset( $this->m_intParentHelpResourceId ) ) ? ( string ) $this->m_intParentHelpResourceId : 'NULL';
	}

	public function setLocaleCode( $strLocaleCode ) {
		$this->set( 'm_strLocaleCode', CStrings::strTrimDef( $strLocaleCode, 100, NULL, true ) );
	}

	public function getLocaleCode() {
		return $this->m_strLocaleCode;
	}

	public function sqlLocaleCode() {
		return ( true == isset( $this->m_strLocaleCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLocaleCode ) : '\'' . addslashes( $this->m_strLocaleCode ) . '\'' ) : '\'en_US\'';
	}

	public function setArchivedBy( $intArchivedBy ) {
		$this->set( 'm_intArchivedBy', CStrings::strToIntDef( $intArchivedBy, NULL, false ) );
	}

	public function getArchivedBy() {
		return $this->m_intArchivedBy;
	}

	public function sqlArchivedBy() {
		return ( true == isset( $this->m_intArchivedBy ) ) ? ( string ) $this->m_intArchivedBy : 'NULL';
	}

	public function setArchivedOn( $strArchivedOn ) {
		$this->set( 'm_strArchivedOn', CStrings::strTrimDef( $strArchivedOn, -1, NULL, true ) );
	}

	public function getArchivedOn() {
		return $this->m_strArchivedOn;
	}

	public function sqlArchivedOn() {
		return ( true == isset( $this->m_strArchivedOn ) ) ? '\'' . $this->m_strArchivedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, default_cid, help_resource_type_id, help_resource_system, title, content, keywords, file_name, file_path, thumbnail_path, external_path, is_visible_to, is_requires_sync, is_searchable, is_course_reset, is_published, description, expire_on, order_num, yes_count, no_count, views_count, comments_count, content_updated_by, content_updated_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, parent_help_resource_id, locale_code, details, archived_by, archived_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlDefaultCid() . ', ' .
						$this->sqlHelpResourceTypeId() . ', ' .
						$this->sqlHelpResourceSystem() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlContent() . ', ' .
						$this->sqlKeywords() . ', ' .
						$this->sqlFileName() . ', ' .
						$this->sqlFilePath() . ', ' .
						$this->sqlThumbnailPath() . ', ' .
						$this->sqlExternalPath() . ', ' .
						$this->sqlIsVisibleTo() . ', ' .
						$this->sqlIsRequiresSync() . ', ' .
						$this->sqlIsSearchable() . ', ' .
						$this->sqlIsCourseReset() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlExpireOn() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlYesCount() . ', ' .
						$this->sqlNoCount() . ', ' .
						$this->sqlViewsCount() . ', ' .
						$this->sqlCommentsCount() . ', ' .
						$this->sqlContentUpdatedBy() . ', ' .
						$this->sqlContentUpdatedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlParentHelpResourceId() . ', ' .
						$this->sqlLocaleCode() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlArchivedBy() . ', ' .
						$this->sqlArchivedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_cid = ' . $this->sqlDefaultCid(). ',' ; } elseif( true == array_key_exists( 'DefaultCid', $this->getChangedColumns() ) ) { $strSql .= ' default_cid = ' . $this->sqlDefaultCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' help_resource_type_id = ' . $this->sqlHelpResourceTypeId(). ',' ; } elseif( true == array_key_exists( 'HelpResourceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' help_resource_type_id = ' . $this->sqlHelpResourceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' help_resource_system = ' . $this->sqlHelpResourceSystem(). ',' ; } elseif( true == array_key_exists( 'HelpResourceSystem', $this->getChangedColumns() ) ) { $strSql .= ' help_resource_system = ' . $this->sqlHelpResourceSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' content = ' . $this->sqlContent(). ',' ; } elseif( true == array_key_exists( 'Content', $this->getChangedColumns() ) ) { $strSql .= ' content = ' . $this->sqlContent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' keywords = ' . $this->sqlKeywords(). ',' ; } elseif( true == array_key_exists( 'Keywords', $this->getChangedColumns() ) ) { $strSql .= ' keywords = ' . $this->sqlKeywords() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName(). ',' ; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath(). ',' ; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' thumbnail_path = ' . $this->sqlThumbnailPath(). ',' ; } elseif( true == array_key_exists( 'ThumbnailPath', $this->getChangedColumns() ) ) { $strSql .= ' thumbnail_path = ' . $this->sqlThumbnailPath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_path = ' . $this->sqlExternalPath(). ',' ; } elseif( true == array_key_exists( 'ExternalPath', $this->getChangedColumns() ) ) { $strSql .= ' external_path = ' . $this->sqlExternalPath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_visible_to = ' . $this->sqlIsVisibleTo(). ',' ; } elseif( true == array_key_exists( 'IsVisibleTo', $this->getChangedColumns() ) ) { $strSql .= ' is_visible_to = ' . $this->sqlIsVisibleTo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_requires_sync = ' . $this->sqlIsRequiresSync(). ',' ; } elseif( true == array_key_exists( 'IsRequiresSync', $this->getChangedColumns() ) ) { $strSql .= ' is_requires_sync = ' . $this->sqlIsRequiresSync() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_searchable = ' . $this->sqlIsSearchable(). ',' ; } elseif( true == array_key_exists( 'IsSearchable', $this->getChangedColumns() ) ) { $strSql .= ' is_searchable = ' . $this->sqlIsSearchable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_course_reset = ' . $this->sqlIsCourseReset(). ',' ; } elseif( true == array_key_exists( 'IsCourseReset', $this->getChangedColumns() ) ) { $strSql .= ' is_course_reset = ' . $this->sqlIsCourseReset() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expire_on = ' . $this->sqlExpireOn(). ',' ; } elseif( true == array_key_exists( 'ExpireOn', $this->getChangedColumns() ) ) { $strSql .= ' expire_on = ' . $this->sqlExpireOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' yes_count = ' . $this->sqlYesCount(). ',' ; } elseif( true == array_key_exists( 'YesCount', $this->getChangedColumns() ) ) { $strSql .= ' yes_count = ' . $this->sqlYesCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' no_count = ' . $this->sqlNoCount(). ',' ; } elseif( true == array_key_exists( 'NoCount', $this->getChangedColumns() ) ) { $strSql .= ' no_count = ' . $this->sqlNoCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' views_count = ' . $this->sqlViewsCount(). ',' ; } elseif( true == array_key_exists( 'ViewsCount', $this->getChangedColumns() ) ) { $strSql .= ' views_count = ' . $this->sqlViewsCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' comments_count = ' . $this->sqlCommentsCount(). ',' ; } elseif( true == array_key_exists( 'CommentsCount', $this->getChangedColumns() ) ) { $strSql .= ' comments_count = ' . $this->sqlCommentsCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' content_updated_by = ' . $this->sqlContentUpdatedBy(). ',' ; } elseif( true == array_key_exists( 'ContentUpdatedBy', $this->getChangedColumns() ) ) { $strSql .= ' content_updated_by = ' . $this->sqlContentUpdatedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' content_updated_on = ' . $this->sqlContentUpdatedOn(). ',' ; } elseif( true == array_key_exists( 'ContentUpdatedOn', $this->getChangedColumns() ) ) { $strSql .= ' content_updated_on = ' . $this->sqlContentUpdatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_help_resource_id = ' . $this->sqlParentHelpResourceId(). ',' ; } elseif( true == array_key_exists( 'ParentHelpResourceId', $this->getChangedColumns() ) ) { $strSql .= ' parent_help_resource_id = ' . $this->sqlParentHelpResourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locale_code = ' . $this->sqlLocaleCode(). ',' ; } elseif( true == array_key_exists( 'LocaleCode', $this->getChangedColumns() ) ) { $strSql .= ' locale_code = ' . $this->sqlLocaleCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' archived_by = ' . $this->sqlArchivedBy(). ',' ; } elseif( true == array_key_exists( 'ArchivedBy', $this->getChangedColumns() ) ) { $strSql .= ' archived_by = ' . $this->sqlArchivedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' archived_on = ' . $this->sqlArchivedOn(). ',' ; } elseif( true == array_key_exists( 'ArchivedOn', $this->getChangedColumns() ) ) { $strSql .= ' archived_on = ' . $this->sqlArchivedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'default_cid' => $this->getDefaultCid(),
			'help_resource_type_id' => $this->getHelpResourceTypeId(),
			'help_resource_system' => $this->getHelpResourceSystem(),
			'title' => $this->getTitle(),
			'content' => $this->getContent(),
			'keywords' => $this->getKeywords(),
			'file_name' => $this->getFileName(),
			'file_path' => $this->getFilePath(),
			'thumbnail_path' => $this->getThumbnailPath(),
			'external_path' => $this->getExternalPath(),
			'is_visible_to' => $this->getIsVisibleTo(),
			'is_requires_sync' => $this->getIsRequiresSync(),
			'is_searchable' => $this->getIsSearchable(),
			'is_course_reset' => $this->getIsCourseReset(),
			'is_published' => $this->getIsPublished(),
			'description' => $this->getDescription(),
			'expire_on' => $this->getExpireOn(),
			'order_num' => $this->getOrderNum(),
			'yes_count' => $this->getYesCount(),
			'no_count' => $this->getNoCount(),
			'views_count' => $this->getViewsCount(),
			'comments_count' => $this->getCommentsCount(),
			'content_updated_by' => $this->getContentUpdatedBy(),
			'content_updated_on' => $this->getContentUpdatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'parent_help_resource_id' => $this->getParentHelpResourceId(),
			'locale_code' => $this->getLocaleCode(),
			'details' => $this->getDetails(),
			'archived_by' => $this->getArchivedBy(),
			'archived_on' => $this->getArchivedOn()
		);
	}

}
?>