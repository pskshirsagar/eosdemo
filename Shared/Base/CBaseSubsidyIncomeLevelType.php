<?php

class CBaseSubsidyIncomeLevelType extends CEosSingularBase {

	const TABLE_NAME = 'public.subsidy_income_level_types';

	protected $m_intId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strHudCode;
	protected $m_fltPercent;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		$this->m_fltPercent = '0';
		$this->m_boolIsPublished = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['hud_code'] ) && $boolDirectSet ) $this->set( 'm_strHudCode', trim( stripcslashes( $arrValues['hud_code'] ) ) ); elseif( isset( $arrValues['hud_code'] ) ) $this->setHudCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hud_code'] ) : $arrValues['hud_code'] );
		if( isset( $arrValues['percent'] ) && $boolDirectSet ) $this->set( 'm_fltPercent', trim( $arrValues['percent'] ) ); elseif( isset( $arrValues['percent'] ) ) $this->setPercent( $arrValues['percent'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setHudCode( $strHudCode ) {
		$this->set( 'm_strHudCode', CStrings::strTrimDef( $strHudCode, 1, NULL, true ) );
	}

	public function getHudCode() {
		return $this->m_strHudCode;
	}

	public function sqlHudCode() {
		return ( true == isset( $this->m_strHudCode ) ) ? '\'' . addslashes( $this->m_strHudCode ) . '\'' : 'NULL';
	}

	public function setPercent( $fltPercent ) {
		$this->set( 'm_fltPercent', CStrings::strToFloatDef( $fltPercent, NULL, false, 2 ) );
	}

	public function getPercent() {
		return $this->m_fltPercent;
	}

	public function sqlPercent() {
		return ( true == isset( $this->m_fltPercent ) ) ? ( string ) $this->m_fltPercent : '0';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'hud_code' => $this->getHudCode(),
			'percent' => $this->getPercent(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>