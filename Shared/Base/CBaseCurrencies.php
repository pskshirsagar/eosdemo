<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCurrencies
 * Do not add any new functions to this class.
 */

class CBaseCurrencies extends CEosPluralBase {

	/**
	 * @return CCurrency[]
	 */
	public static function fetchCurrencies( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCurrency::class, $objDatabase );
	}

	/**
	 * @return CCurrency
	 */
	public static function fetchCurrency( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCurrency::class, $objDatabase );
	}

	public static function fetchCurrencyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'currencies', $objDatabase );
	}

	public static function fetchCurrencyById( $intId, $objDatabase ) {
		return self::fetchCurrency( sprintf( 'SELECT * FROM currencies WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>