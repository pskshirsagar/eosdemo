<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Deploy\CClusters
 * Do not add any new functions to this class.
 */

class CBaseClusters extends CEosPluralBase {

	public static function fetchClusters( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCluster', $objDatabase );
	}

	public static function fetchCluster( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCluster', $objDatabase );
	}

	public static function fetchClusterCount( $strWhere = NULL, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'clusters', $objDatabase );
	}

	public static function fetchClusterById( $intId, $objDatabase ) {
		return self::fetchCluster( sprintf( 'SELECT * FROM clusters WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>