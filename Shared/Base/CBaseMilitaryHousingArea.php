<?php

class CBaseMilitaryHousingArea extends CEosSingularBase {

	const TABLE_NAME = 'public.military_housing_areas';

	protected $m_intId;
	protected $m_strName;
	protected $m_strExternalReference;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_intYear;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['external_reference'] ) && $boolDirectSet ) $this->set( 'm_strExternalReference', trim( $arrValues['external_reference'] ) ); elseif( isset( $arrValues['external_reference'] ) ) $this->setExternalReference( $arrValues['external_reference'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['year'] ) && $boolDirectSet ) $this->set( 'm_intYear', trim( $arrValues['year'] ) ); elseif( isset( $arrValues['year'] ) ) $this->setYear( $arrValues['year'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 60, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setExternalReference( $strExternalReference ) {
		$this->set( 'm_strExternalReference', CStrings::strTrimDef( $strExternalReference, 5, NULL, true ) );
	}

	public function getExternalReference() {
		return $this->m_strExternalReference;
	}

	public function sqlExternalReference() {
		return ( true == isset( $this->m_strExternalReference ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strExternalReference ) : '\'' . addslashes( $this->m_strExternalReference ) . '\'' ) : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setYear( $intYear ) {
		$this->set( 'm_intYear', CStrings::strToIntDef( $intYear, NULL, false ) );
	}

	public function getYear() {
		return $this->m_intYear;
	}

	public function sqlYear() {
		return ( true == isset( $this->m_intYear ) ) ? ( string ) $this->m_intYear : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'external_reference' => $this->getExternalReference(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'year' => $this->getYear()
		);
	}

}
?>