<?php

class CBaseReservedSubdomain extends CEosSingularBase {

	const TABLE_NAME = 'public.reserved_subdomains';

	protected $m_intId;
	protected $m_strSubDomain;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['sub_domain'] ) && $boolDirectSet ) $this->set( 'm_strSubDomain', trim( stripcslashes( $arrValues['sub_domain'] ) ) ); elseif( isset( $arrValues['sub_domain'] ) ) $this->setSubDomain( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sub_domain'] ) : $arrValues['sub_domain'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSubDomain( $strSubDomain ) {
		$this->set( 'm_strSubDomain', CStrings::strTrimDef( $strSubDomain, 240, NULL, true ) );
	}

	public function getSubDomain() {
		return $this->m_strSubDomain;
	}

	public function sqlSubDomain() {
		return ( true == isset( $this->m_strSubDomain ) ) ? '\'' . addslashes( $this->m_strSubDomain ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'sub_domain' => $this->getSubDomain()
		);
	}

}
?>