<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningPackage extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_packages';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intParentPackageId;
	protected $m_intDelinquentAccountTimeframeTypeId;
	protected $m_intScreeningScoringModelId;
	protected $m_intScreeningPackageTypeId;
	protected $m_intScreeningCumulationTypeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strExternalName;
	protected $m_intCombineConditionSetValue;
	protected $m_boolIsUseForPrescreening;
	protected $m_intIsUseForRvIndexScore;
	protected $m_intIsCombineIncome;
	protected $m_boolIsAllMustPassCreditAndIncome;
	protected $m_intIsPublished;
	protected $m_boolIsActive;
	protected $m_strPackageCompletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsUsedForFilteringPendingCharges;
	protected $m_arrintScreeningPackageLeaseTypeIds;
	protected $m_arrintStopProcessingOnRecommendationIds;

	public function __construct() {
		parent::__construct();

		$this->m_intScreeningCumulationTypeId = '1';
		$this->m_boolIsUseForPrescreening = false;
		$this->m_intIsUseForRvIndexScore = '0';
		$this->m_intIsCombineIncome = '0';
		$this->m_boolIsAllMustPassCreditAndIncome = true;
		$this->m_intIsPublished = '1';
		$this->m_boolIsActive = true;
		$this->m_boolIsUsedForFilteringPendingCharges = false;
		$this->m_arrintScreeningPackageLeaseTypeIds = 'ARRAY[1, 2, 3, 4, 5]';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['parent_package_id'] ) && $boolDirectSet ) $this->set( 'm_intParentPackageId', trim( $arrValues['parent_package_id'] ) ); elseif( isset( $arrValues['parent_package_id'] ) ) $this->setParentPackageId( $arrValues['parent_package_id'] );
		if( isset( $arrValues['delinquent_account_timeframe_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDelinquentAccountTimeframeTypeId', trim( $arrValues['delinquent_account_timeframe_type_id'] ) ); elseif( isset( $arrValues['delinquent_account_timeframe_type_id'] ) ) $this->setDelinquentAccountTimeframeTypeId( $arrValues['delinquent_account_timeframe_type_id'] );
		if( isset( $arrValues['screening_scoring_model_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningScoringModelId', trim( $arrValues['screening_scoring_model_id'] ) ); elseif( isset( $arrValues['screening_scoring_model_id'] ) ) $this->setScreeningScoringModelId( $arrValues['screening_scoring_model_id'] );
		if( isset( $arrValues['screening_package_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningPackageTypeId', trim( $arrValues['screening_package_type_id'] ) ); elseif( isset( $arrValues['screening_package_type_id'] ) ) $this->setScreeningPackageTypeId( $arrValues['screening_package_type_id'] );
		if( isset( $arrValues['screening_cumulation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningCumulationTypeId', trim( $arrValues['screening_cumulation_type_id'] ) ); elseif( isset( $arrValues['screening_cumulation_type_id'] ) ) $this->setScreeningCumulationTypeId( $arrValues['screening_cumulation_type_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['external_name'] ) && $boolDirectSet ) $this->set( 'm_strExternalName', trim( stripcslashes( $arrValues['external_name'] ) ) ); elseif( isset( $arrValues['external_name'] ) ) $this->setExternalName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['external_name'] ) : $arrValues['external_name'] );
		if( isset( $arrValues['combine_condition_set_value'] ) && $boolDirectSet ) $this->set( 'm_intCombineConditionSetValue', trim( $arrValues['combine_condition_set_value'] ) ); elseif( isset( $arrValues['combine_condition_set_value'] ) ) $this->setCombineConditionSetValue( $arrValues['combine_condition_set_value'] );
		if( isset( $arrValues['is_use_for_prescreening'] ) && $boolDirectSet ) $this->set( 'm_boolIsUseForPrescreening', trim( stripcslashes( $arrValues['is_use_for_prescreening'] ) ) ); elseif( isset( $arrValues['is_use_for_prescreening'] ) ) $this->setIsUseForPrescreening( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_use_for_prescreening'] ) : $arrValues['is_use_for_prescreening'] );
		if( isset( $arrValues['is_use_for_rv_index_score'] ) && $boolDirectSet ) $this->set( 'm_intIsUseForRvIndexScore', trim( $arrValues['is_use_for_rv_index_score'] ) ); elseif( isset( $arrValues['is_use_for_rv_index_score'] ) ) $this->setIsUseForRvIndexScore( $arrValues['is_use_for_rv_index_score'] );
		if( isset( $arrValues['is_combine_income'] ) && $boolDirectSet ) $this->set( 'm_intIsCombineIncome', trim( $arrValues['is_combine_income'] ) ); elseif( isset( $arrValues['is_combine_income'] ) ) $this->setIsCombineIncome( $arrValues['is_combine_income'] );
		if( isset( $arrValues['is_all_must_pass_credit_and_income'] ) && $boolDirectSet ) $this->set( 'm_boolIsAllMustPassCreditAndIncome', trim( stripcslashes( $arrValues['is_all_must_pass_credit_and_income'] ) ) ); elseif( isset( $arrValues['is_all_must_pass_credit_and_income'] ) ) $this->setIsAllMustPassCreditAndIncome( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_all_must_pass_credit_and_income'] ) : $arrValues['is_all_must_pass_credit_and_income'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['package_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strPackageCompletedOn', trim( $arrValues['package_completed_on'] ) ); elseif( isset( $arrValues['package_completed_on'] ) ) $this->setPackageCompletedOn( $arrValues['package_completed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_used_for_filtering_pending_charges'] ) && $boolDirectSet ) $this->set( 'm_boolIsUsedForFilteringPendingCharges', trim( stripcslashes( $arrValues['is_used_for_filtering_pending_charges'] ) ) ); elseif( isset( $arrValues['is_used_for_filtering_pending_charges'] ) ) $this->setIsUsedForFilteringPendingCharges( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_used_for_filtering_pending_charges'] ) : $arrValues['is_used_for_filtering_pending_charges'] );
		if( isset( $arrValues['screening_package_lease_type_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintScreeningPackageLeaseTypeIds', trim( $arrValues['screening_package_lease_type_ids'] ) ); elseif( isset( $arrValues['screening_package_lease_type_ids'] ) ) $this->setScreeningPackageLeaseTypeIds( $arrValues['screening_package_lease_type_ids'] );
		if( isset( $arrValues['stop_processing_on_recommendation_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintStopProcessingOnRecommendationIds', trim( $arrValues['stop_processing_on_recommendation_ids'] ) ); elseif( isset( $arrValues['stop_processing_on_recommendation_ids'] ) ) $this->setStopProcessingOnRecommendationIds( $arrValues['stop_processing_on_recommendation_ids'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setParentPackageId( $intParentPackageId ) {
		$this->set( 'm_intParentPackageId', CStrings::strToIntDef( $intParentPackageId, NULL, false ) );
	}

	public function getParentPackageId() {
		return $this->m_intParentPackageId;
	}

	public function sqlParentPackageId() {
		return ( true == isset( $this->m_intParentPackageId ) ) ? ( string ) $this->m_intParentPackageId : 'NULL';
	}

	public function setDelinquentAccountTimeframeTypeId( $intDelinquentAccountTimeframeTypeId ) {
		$this->set( 'm_intDelinquentAccountTimeframeTypeId', CStrings::strToIntDef( $intDelinquentAccountTimeframeTypeId, NULL, false ) );
	}

	public function getDelinquentAccountTimeframeTypeId() {
		return $this->m_intDelinquentAccountTimeframeTypeId;
	}

	public function sqlDelinquentAccountTimeframeTypeId() {
		return ( true == isset( $this->m_intDelinquentAccountTimeframeTypeId ) ) ? ( string ) $this->m_intDelinquentAccountTimeframeTypeId : 'NULL';
	}

	public function setScreeningScoringModelId( $intScreeningScoringModelId ) {
		$this->set( 'm_intScreeningScoringModelId', CStrings::strToIntDef( $intScreeningScoringModelId, NULL, false ) );
	}

	public function getScreeningScoringModelId() {
		return $this->m_intScreeningScoringModelId;
	}

	public function sqlScreeningScoringModelId() {
		return ( true == isset( $this->m_intScreeningScoringModelId ) ) ? ( string ) $this->m_intScreeningScoringModelId : 'NULL';
	}

	public function setScreeningPackageTypeId( $intScreeningPackageTypeId ) {
		$this->set( 'm_intScreeningPackageTypeId', CStrings::strToIntDef( $intScreeningPackageTypeId, NULL, false ) );
	}

	public function getScreeningPackageTypeId() {
		return $this->m_intScreeningPackageTypeId;
	}

	public function sqlScreeningPackageTypeId() {
		return ( true == isset( $this->m_intScreeningPackageTypeId ) ) ? ( string ) $this->m_intScreeningPackageTypeId : 'NULL';
	}

	public function setScreeningCumulationTypeId( $intScreeningCumulationTypeId ) {
		$this->set( 'm_intScreeningCumulationTypeId', CStrings::strToIntDef( $intScreeningCumulationTypeId, NULL, false ) );
	}

	public function getScreeningCumulationTypeId() {
		return $this->m_intScreeningCumulationTypeId;
	}

	public function sqlScreeningCumulationTypeId() {
		return ( true == isset( $this->m_intScreeningCumulationTypeId ) ) ? ( string ) $this->m_intScreeningCumulationTypeId : '1';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, -1, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setExternalName( $strExternalName ) {
		$this->set( 'm_strExternalName', CStrings::strTrimDef( $strExternalName, -1, NULL, true ) );
	}

	public function getExternalName() {
		return $this->m_strExternalName;
	}

	public function sqlExternalName() {
		return ( true == isset( $this->m_strExternalName ) ) ? '\'' . addslashes( $this->m_strExternalName ) . '\'' : 'NULL';
	}

	public function setCombineConditionSetValue( $intCombineConditionSetValue ) {
		$this->set( 'm_intCombineConditionSetValue', CStrings::strToIntDef( $intCombineConditionSetValue, NULL, false ) );
	}

	public function getCombineConditionSetValue() {
		return $this->m_intCombineConditionSetValue;
	}

	public function sqlCombineConditionSetValue() {
		return ( true == isset( $this->m_intCombineConditionSetValue ) ) ? ( string ) $this->m_intCombineConditionSetValue : 'NULL';
	}

	public function setIsUseForPrescreening( $boolIsUseForPrescreening ) {
		$this->set( 'm_boolIsUseForPrescreening', CStrings::strToBool( $boolIsUseForPrescreening ) );
	}

	public function getIsUseForPrescreening() {
		return $this->m_boolIsUseForPrescreening;
	}

	public function sqlIsUseForPrescreening() {
		return ( true == isset( $this->m_boolIsUseForPrescreening ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsUseForPrescreening ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsUseForRvIndexScore( $intIsUseForRvIndexScore ) {
		$this->set( 'm_intIsUseForRvIndexScore', CStrings::strToIntDef( $intIsUseForRvIndexScore, NULL, false ) );
	}

	public function getIsUseForRvIndexScore() {
		return $this->m_intIsUseForRvIndexScore;
	}

	public function sqlIsUseForRvIndexScore() {
		return ( true == isset( $this->m_intIsUseForRvIndexScore ) ) ? ( string ) $this->m_intIsUseForRvIndexScore : '0';
	}

	public function setIsCombineIncome( $intIsCombineIncome ) {
		$this->set( 'm_intIsCombineIncome', CStrings::strToIntDef( $intIsCombineIncome, NULL, false ) );
	}

	public function getIsCombineIncome() {
		return $this->m_intIsCombineIncome;
	}

	public function sqlIsCombineIncome() {
		return ( true == isset( $this->m_intIsCombineIncome ) ) ? ( string ) $this->m_intIsCombineIncome : '0';
	}

	public function setIsAllMustPassCreditAndIncome( $boolIsAllMustPassCreditAndIncome ) {
		$this->set( 'm_boolIsAllMustPassCreditAndIncome', CStrings::strToBool( $boolIsAllMustPassCreditAndIncome ) );
	}

	public function getIsAllMustPassCreditAndIncome() {
		return $this->m_boolIsAllMustPassCreditAndIncome;
	}

	public function sqlIsAllMustPassCreditAndIncome() {
		return ( true == isset( $this->m_boolIsAllMustPassCreditAndIncome ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAllMustPassCreditAndIncome ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPackageCompletedOn( $strPackageCompletedOn ) {
		$this->set( 'm_strPackageCompletedOn', CStrings::strTrimDef( $strPackageCompletedOn, -1, NULL, true ) );
	}

	public function getPackageCompletedOn() {
		return $this->m_strPackageCompletedOn;
	}

	public function sqlPackageCompletedOn() {
		return ( true == isset( $this->m_strPackageCompletedOn ) ) ? '\'' . $this->m_strPackageCompletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsUsedForFilteringPendingCharges( $boolIsUsedForFilteringPendingCharges ) {
		$this->set( 'm_boolIsUsedForFilteringPendingCharges', CStrings::strToBool( $boolIsUsedForFilteringPendingCharges ) );
	}

	public function getIsUsedForFilteringPendingCharges() {
		return $this->m_boolIsUsedForFilteringPendingCharges;
	}

	public function sqlIsUsedForFilteringPendingCharges() {
		return ( true == isset( $this->m_boolIsUsedForFilteringPendingCharges ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsUsedForFilteringPendingCharges ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setScreeningPackageLeaseTypeIds( $arrintScreeningPackageLeaseTypeIds ) {
		$this->set( 'm_arrintScreeningPackageLeaseTypeIds', CStrings::strToArrIntDef( $arrintScreeningPackageLeaseTypeIds, NULL ) );
	}

	public function getScreeningPackageLeaseTypeIds() {
		return $this->m_arrintScreeningPackageLeaseTypeIds;
	}

	public function sqlScreeningPackageLeaseTypeIds() {
		return ( true == isset( $this->m_arrintScreeningPackageLeaseTypeIds ) && true == valArr( $this->m_arrintScreeningPackageLeaseTypeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintScreeningPackageLeaseTypeIds, NULL ) . '\'' : 'ARRAY[1, 2, 3, 4, 5]';
	}

	public function setStopProcessingOnRecommendationIds( $arrintStopProcessingOnRecommendationIds ) {
		$this->set( 'm_arrintStopProcessingOnRecommendationIds', CStrings::strToArrIntDef( $arrintStopProcessingOnRecommendationIds, NULL ) );
	}

	public function getStopProcessingOnRecommendationIds() {
		return $this->m_arrintStopProcessingOnRecommendationIds;
	}

	public function sqlStopProcessingOnRecommendationIds() {
		return ( true == isset( $this->m_arrintStopProcessingOnRecommendationIds ) && true == valArr( $this->m_arrintStopProcessingOnRecommendationIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintStopProcessingOnRecommendationIds, NULL ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, parent_package_id, delinquent_account_timeframe_type_id, screening_scoring_model_id, screening_package_type_id, screening_cumulation_type_id, name, description, external_name, combine_condition_set_value, is_use_for_prescreening, is_use_for_rv_index_score, is_combine_income, is_all_must_pass_credit_and_income, is_published, is_active, package_completed_on, updated_by, updated_on, created_by, created_on, is_used_for_filtering_pending_charges, screening_package_lease_type_ids, stop_processing_on_recommendation_ids )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlParentPackageId() . ', ' .
 						$this->sqlDelinquentAccountTimeframeTypeId() . ', ' .
 						$this->sqlScreeningScoringModelId() . ', ' .
 						$this->sqlScreeningPackageTypeId() . ', ' .
 						$this->sqlScreeningCumulationTypeId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlExternalName() . ', ' .
 						$this->sqlCombineConditionSetValue() . ', ' .
 						$this->sqlIsUseForPrescreening() . ', ' .
 						$this->sqlIsUseForRvIndexScore() . ', ' .
 						$this->sqlIsCombineIncome() . ', ' .
 						$this->sqlIsAllMustPassCreditAndIncome() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlIsActive() . ', ' .
 						$this->sqlPackageCompletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
		                $this->sqlIsUsedForFilteringPendingCharges() . ', ' .
 						$this->sqlScreeningPackageLeaseTypeIds() . ', ' .
                        $this->sqlStopProcessingOnRecommendationIds() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_package_id = ' . $this->sqlParentPackageId() . ','; } elseif( true == array_key_exists( 'ParentPackageId', $this->getChangedColumns() ) ) { $strSql .= ' parent_package_id = ' . $this->sqlParentPackageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquent_account_timeframe_type_id = ' . $this->sqlDelinquentAccountTimeframeTypeId() . ','; } elseif( true == array_key_exists( 'DelinquentAccountTimeframeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' delinquent_account_timeframe_type_id = ' . $this->sqlDelinquentAccountTimeframeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_scoring_model_id = ' . $this->sqlScreeningScoringModelId() . ','; } elseif( true == array_key_exists( 'ScreeningScoringModelId', $this->getChangedColumns() ) ) { $strSql .= ' screening_scoring_model_id = ' . $this->sqlScreeningScoringModelId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_package_type_id = ' . $this->sqlScreeningPackageTypeId() . ','; } elseif( true == array_key_exists( 'ScreeningPackageTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_package_type_id = ' . $this->sqlScreeningPackageTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_cumulation_type_id = ' . $this->sqlScreeningCumulationTypeId() . ','; } elseif( true == array_key_exists( 'ScreeningCumulationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_cumulation_type_id = ' . $this->sqlScreeningCumulationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_name = ' . $this->sqlExternalName() . ','; } elseif( true == array_key_exists( 'ExternalName', $this->getChangedColumns() ) ) { $strSql .= ' external_name = ' . $this->sqlExternalName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' combine_condition_set_value = ' . $this->sqlCombineConditionSetValue() . ','; } elseif( true == array_key_exists( 'CombineConditionSetValue', $this->getChangedColumns() ) ) { $strSql .= ' combine_condition_set_value = ' . $this->sqlCombineConditionSetValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_use_for_prescreening = ' . $this->sqlIsUseForPrescreening() . ','; } elseif( true == array_key_exists( 'IsUseForPrescreening', $this->getChangedColumns() ) ) { $strSql .= ' is_use_for_prescreening = ' . $this->sqlIsUseForPrescreening() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_use_for_rv_index_score = ' . $this->sqlIsUseForRvIndexScore() . ','; } elseif( true == array_key_exists( 'IsUseForRvIndexScore', $this->getChangedColumns() ) ) { $strSql .= ' is_use_for_rv_index_score = ' . $this->sqlIsUseForRvIndexScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_combine_income = ' . $this->sqlIsCombineIncome() . ','; } elseif( true == array_key_exists( 'IsCombineIncome', $this->getChangedColumns() ) ) { $strSql .= ' is_combine_income = ' . $this->sqlIsCombineIncome() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_all_must_pass_credit_and_income = ' . $this->sqlIsAllMustPassCreditAndIncome() . ','; } elseif( true == array_key_exists( 'IsAllMustPassCreditAndIncome', $this->getChangedColumns() ) ) { $strSql .= ' is_all_must_pass_credit_and_income = ' . $this->sqlIsAllMustPassCreditAndIncome() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' package_completed_on = ' . $this->sqlPackageCompletedOn() . ','; } elseif( true == array_key_exists( 'PackageCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' package_completed_on = ' . $this->sqlPackageCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_used_for_filtering_pending_charges = ' . $this->sqlIsUsedForFilteringPendingCharges() . ','; } elseif( true == array_key_exists( 'IsUsedForFilteringPendingCharges', $this->getChangedColumns() ) ) { $strSql .= ' is_used_for_filtering_pending_charges = ' . $this->sqlIsUsedForFilteringPendingCharges() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_package_lease_type_ids = ' . $this->sqlScreeningPackageLeaseTypeIds() . ','; } elseif( true == array_key_exists( 'ScreeningPackageLeaseTypeIds', $this->getChangedColumns() ) ) { $strSql .= ' screening_package_lease_type_ids = ' . $this->sqlScreeningPackageLeaseTypeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' stop_processing_on_recommendation_ids = ' . $this->sqlStopProcessingOnRecommendationIds(). ',' ; } elseif( true == array_key_exists( 'StopProcessingOnRecommendationIds', $this->getChangedColumns() ) ) { $strSql .= ' stop_processing_on_recommendation_ids = ' . $this->sqlStopProcessingOnRecommendationIds() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'parent_package_id' => $this->getParentPackageId(),
			'delinquent_account_timeframe_type_id' => $this->getDelinquentAccountTimeframeTypeId(),
			'screening_scoring_model_id' => $this->getScreeningScoringModelId(),
			'screening_package_type_id' => $this->getScreeningPackageTypeId(),
			'screening_cumulation_type_id' => $this->getScreeningCumulationTypeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'external_name' => $this->getExternalName(),
			'combine_condition_set_value' => $this->getCombineConditionSetValue(),
			'is_use_for_prescreening' => $this->getIsUseForPrescreening(),
			'is_use_for_rv_index_score' => $this->getIsUseForRvIndexScore(),
			'is_combine_income' => $this->getIsCombineIncome(),
			'is_all_must_pass_credit_and_income' => $this->getIsAllMustPassCreditAndIncome(),
			'is_published' => $this->getIsPublished(),
			'is_active' => $this->getIsActive(),
			'package_completed_on' => $this->getPackageCompletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_used_for_filtering_pending_charges' => $this->getIsUsedForFilteringPendingCharges(),
			'screening_package_lease_type_ids' => $this->getScreeningPackageLeaseTypeIds(),
			'stop_processing_on_recommendation_ids' => $this->getStopProcessingOnRecommendationIds()
		);
	}

}
?>