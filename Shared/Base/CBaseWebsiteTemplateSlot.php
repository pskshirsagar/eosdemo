<?php

class CBaseWebsiteTemplateSlot extends CEosSingularBase {

	const TABLE_NAME = 'public.website_template_slots';

	protected $m_intId;
	protected $m_intTemplateSlotTypeId;
	protected $m_intWebsiteTemplateId;
	protected $m_strKey;
	protected $m_strTitle;
	protected $m_strDescription;
	protected $m_strDefaultImageName;
	protected $m_strBgcolor;
	protected $m_intMediaWidth;
	protected $m_intMediaHeight;
	protected $m_strPreviewImageName;
	protected $m_strPreviewImageDimensions;
	protected $m_intShowCaption;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intShowCaption = '0';
		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['template_slot_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTemplateSlotTypeId', trim( $arrValues['template_slot_type_id'] ) ); elseif( isset( $arrValues['template_slot_type_id'] ) ) $this->setTemplateSlotTypeId( $arrValues['template_slot_type_id'] );
		if( isset( $arrValues['website_template_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteTemplateId', trim( $arrValues['website_template_id'] ) ); elseif( isset( $arrValues['website_template_id'] ) ) $this->setWebsiteTemplateId( $arrValues['website_template_id'] );
		if( isset( $arrValues['key'] ) && $boolDirectSet ) $this->set( 'm_strKey', trim( stripcslashes( $arrValues['key'] ) ) ); elseif( isset( $arrValues['key'] ) ) $this->setKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['key'] ) : $arrValues['key'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['default_image_name'] ) && $boolDirectSet ) $this->set( 'm_strDefaultImageName', trim( stripcslashes( $arrValues['default_image_name'] ) ) ); elseif( isset( $arrValues['default_image_name'] ) ) $this->setDefaultImageName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['default_image_name'] ) : $arrValues['default_image_name'] );
		if( isset( $arrValues['bgcolor'] ) && $boolDirectSet ) $this->set( 'm_strBgcolor', trim( stripcslashes( $arrValues['bgcolor'] ) ) ); elseif( isset( $arrValues['bgcolor'] ) ) $this->setBgcolor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bgcolor'] ) : $arrValues['bgcolor'] );
		if( isset( $arrValues['media_width'] ) && $boolDirectSet ) $this->set( 'm_intMediaWidth', trim( $arrValues['media_width'] ) ); elseif( isset( $arrValues['media_width'] ) ) $this->setMediaWidth( $arrValues['media_width'] );
		if( isset( $arrValues['media_height'] ) && $boolDirectSet ) $this->set( 'm_intMediaHeight', trim( $arrValues['media_height'] ) ); elseif( isset( $arrValues['media_height'] ) ) $this->setMediaHeight( $arrValues['media_height'] );
		if( isset( $arrValues['preview_image_name'] ) && $boolDirectSet ) $this->set( 'm_strPreviewImageName', trim( stripcslashes( $arrValues['preview_image_name'] ) ) ); elseif( isset( $arrValues['preview_image_name'] ) ) $this->setPreviewImageName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['preview_image_name'] ) : $arrValues['preview_image_name'] );
		if( isset( $arrValues['preview_image_dimensions'] ) && $boolDirectSet ) $this->set( 'm_strPreviewImageDimensions', trim( stripcslashes( $arrValues['preview_image_dimensions'] ) ) ); elseif( isset( $arrValues['preview_image_dimensions'] ) ) $this->setPreviewImageDimensions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['preview_image_dimensions'] ) : $arrValues['preview_image_dimensions'] );
		if( isset( $arrValues['show_caption'] ) && $boolDirectSet ) $this->set( 'm_intShowCaption', trim( $arrValues['show_caption'] ) ); elseif( isset( $arrValues['show_caption'] ) ) $this->setShowCaption( $arrValues['show_caption'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTemplateSlotTypeId( $intTemplateSlotTypeId ) {
		$this->set( 'm_intTemplateSlotTypeId', CStrings::strToIntDef( $intTemplateSlotTypeId, NULL, false ) );
	}

	public function getTemplateSlotTypeId() {
		return $this->m_intTemplateSlotTypeId;
	}

	public function sqlTemplateSlotTypeId() {
		return ( true == isset( $this->m_intTemplateSlotTypeId ) ) ? ( string ) $this->m_intTemplateSlotTypeId : 'NULL';
	}

	public function setWebsiteTemplateId( $intWebsiteTemplateId ) {
		$this->set( 'm_intWebsiteTemplateId', CStrings::strToIntDef( $intWebsiteTemplateId, NULL, false ) );
	}

	public function getWebsiteTemplateId() {
		return $this->m_intWebsiteTemplateId;
	}

	public function sqlWebsiteTemplateId() {
		return ( true == isset( $this->m_intWebsiteTemplateId ) ) ? ( string ) $this->m_intWebsiteTemplateId : 'NULL';
	}

	public function setKey( $strKey ) {
		$this->set( 'm_strKey', CStrings::strTrimDef( $strKey, 64, NULL, true ) );
	}

	public function getKey() {
		return $this->m_strKey;
	}

	public function sqlKey() {
		return ( true == isset( $this->m_strKey ) ) ? '\'' . addslashes( $this->m_strKey ) . '\'' : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 50, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setDefaultImageName( $strDefaultImageName ) {
		$this->set( 'm_strDefaultImageName', CStrings::strTrimDef( $strDefaultImageName, 4096, NULL, true ) );
	}

	public function getDefaultImageName() {
		return $this->m_strDefaultImageName;
	}

	public function sqlDefaultImageName() {
		return ( true == isset( $this->m_strDefaultImageName ) ) ? '\'' . addslashes( $this->m_strDefaultImageName ) . '\'' : 'NULL';
	}

	public function setBgcolor( $strBgcolor ) {
		$this->set( 'm_strBgcolor', CStrings::strTrimDef( $strBgcolor, 20, NULL, true ) );
	}

	public function getBgcolor() {
		return $this->m_strBgcolor;
	}

	public function sqlBgcolor() {
		return ( true == isset( $this->m_strBgcolor ) ) ? '\'' . addslashes( $this->m_strBgcolor ) . '\'' : 'NULL';
	}

	public function setMediaWidth( $intMediaWidth ) {
		$this->set( 'm_intMediaWidth', CStrings::strToIntDef( $intMediaWidth, NULL, false ) );
	}

	public function getMediaWidth() {
		return $this->m_intMediaWidth;
	}

	public function sqlMediaWidth() {
		return ( true == isset( $this->m_intMediaWidth ) ) ? ( string ) $this->m_intMediaWidth : 'NULL';
	}

	public function setMediaHeight( $intMediaHeight ) {
		$this->set( 'm_intMediaHeight', CStrings::strToIntDef( $intMediaHeight, NULL, false ) );
	}

	public function getMediaHeight() {
		return $this->m_intMediaHeight;
	}

	public function sqlMediaHeight() {
		return ( true == isset( $this->m_intMediaHeight ) ) ? ( string ) $this->m_intMediaHeight : 'NULL';
	}

	public function setPreviewImageName( $strPreviewImageName ) {
		$this->set( 'm_strPreviewImageName', CStrings::strTrimDef( $strPreviewImageName, 250, NULL, true ) );
	}

	public function getPreviewImageName() {
		return $this->m_strPreviewImageName;
	}

	public function sqlPreviewImageName() {
		return ( true == isset( $this->m_strPreviewImageName ) ) ? '\'' . addslashes( $this->m_strPreviewImageName ) . '\'' : 'NULL';
	}

	public function setPreviewImageDimensions( $strPreviewImageDimensions ) {
		$this->set( 'm_strPreviewImageDimensions', CStrings::strTrimDef( $strPreviewImageDimensions, 20, NULL, true ) );
	}

	public function getPreviewImageDimensions() {
		return $this->m_strPreviewImageDimensions;
	}

	public function sqlPreviewImageDimensions() {
		return ( true == isset( $this->m_strPreviewImageDimensions ) ) ? '\'' . addslashes( $this->m_strPreviewImageDimensions ) . '\'' : 'NULL';
	}

	public function setShowCaption( $intShowCaption ) {
		$this->set( 'm_intShowCaption', CStrings::strToIntDef( $intShowCaption, NULL, false ) );
	}

	public function getShowCaption() {
		return $this->m_intShowCaption;
	}

	public function sqlShowCaption() {
		return ( true == isset( $this->m_intShowCaption ) ) ? ( string ) $this->m_intShowCaption : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, template_slot_type_id, website_template_id, key, title, description, default_image_name, bgcolor, media_width, media_height, preview_image_name, preview_image_dimensions, show_caption, is_published, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlTemplateSlotTypeId() . ', ' .
						$this->sqlWebsiteTemplateId() . ', ' .
						$this->sqlKey() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlDefaultImageName() . ', ' .
						$this->sqlBgcolor() . ', ' .
						$this->sqlMediaWidth() . ', ' .
						$this->sqlMediaHeight() . ', ' .
						$this->sqlPreviewImageName() . ', ' .
						$this->sqlPreviewImageDimensions() . ', ' .
						$this->sqlShowCaption() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_slot_type_id = ' . $this->sqlTemplateSlotTypeId() . ','; } elseif( true == array_key_exists( 'TemplateSlotTypeId', $this->getChangedColumns() ) ) { $strSql .= ' template_slot_type_id = ' . $this->sqlTemplateSlotTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_template_id = ' . $this->sqlWebsiteTemplateId() . ','; } elseif( true == array_key_exists( 'WebsiteTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' website_template_id = ' . $this->sqlWebsiteTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' key = ' . $this->sqlKey() . ','; } elseif( true == array_key_exists( 'Key', $this->getChangedColumns() ) ) { $strSql .= ' key = ' . $this->sqlKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_image_name = ' . $this->sqlDefaultImageName() . ','; } elseif( true == array_key_exists( 'DefaultImageName', $this->getChangedColumns() ) ) { $strSql .= ' default_image_name = ' . $this->sqlDefaultImageName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bgcolor = ' . $this->sqlBgcolor() . ','; } elseif( true == array_key_exists( 'Bgcolor', $this->getChangedColumns() ) ) { $strSql .= ' bgcolor = ' . $this->sqlBgcolor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_width = ' . $this->sqlMediaWidth() . ','; } elseif( true == array_key_exists( 'MediaWidth', $this->getChangedColumns() ) ) { $strSql .= ' media_width = ' . $this->sqlMediaWidth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_height = ' . $this->sqlMediaHeight() . ','; } elseif( true == array_key_exists( 'MediaHeight', $this->getChangedColumns() ) ) { $strSql .= ' media_height = ' . $this->sqlMediaHeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' preview_image_name = ' . $this->sqlPreviewImageName() . ','; } elseif( true == array_key_exists( 'PreviewImageName', $this->getChangedColumns() ) ) { $strSql .= ' preview_image_name = ' . $this->sqlPreviewImageName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' preview_image_dimensions = ' . $this->sqlPreviewImageDimensions() . ','; } elseif( true == array_key_exists( 'PreviewImageDimensions', $this->getChangedColumns() ) ) { $strSql .= ' preview_image_dimensions = ' . $this->sqlPreviewImageDimensions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_caption = ' . $this->sqlShowCaption() . ','; } elseif( true == array_key_exists( 'ShowCaption', $this->getChangedColumns() ) ) { $strSql .= ' show_caption = ' . $this->sqlShowCaption() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'template_slot_type_id' => $this->getTemplateSlotTypeId(),
			'website_template_id' => $this->getWebsiteTemplateId(),
			'key' => $this->getKey(),
			'title' => $this->getTitle(),
			'description' => $this->getDescription(),
			'default_image_name' => $this->getDefaultImageName(),
			'bgcolor' => $this->getBgcolor(),
			'media_width' => $this->getMediaWidth(),
			'media_height' => $this->getMediaHeight(),
			'preview_image_name' => $this->getPreviewImageName(),
			'preview_image_dimensions' => $this->getPreviewImageDimensions(),
			'show_caption' => $this->getShowCaption(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>