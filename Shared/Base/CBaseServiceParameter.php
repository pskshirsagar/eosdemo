<?php

class CBaseServiceParameter extends CEosSingularBase {

	const TABLE_NAME = 'public.service_parameters';

	protected $m_intId;
	protected $m_intMinorApiVersionId;
	protected $m_intServiceId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strType;
	protected $m_intMaxOccurance;
	protected $m_boolIsPublished;
	protected $m_boolIsRequired;
	protected $m_boolIsMultiple;
	protected $m_boolIsResponse;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;
		$this->m_boolIsRequired = false;
		$this->m_boolIsMultiple = false;
		$this->m_boolIsResponse = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['minor_api_version_id'] ) && $boolDirectSet ) $this->set( 'm_intMinorApiVersionId', trim( $arrValues['minor_api_version_id'] ) ); elseif( isset( $arrValues['minor_api_version_id'] ) ) $this->setMinorApiVersionId( $arrValues['minor_api_version_id'] );
		if( isset( $arrValues['service_id'] ) && $boolDirectSet ) $this->set( 'm_intServiceId', trim( $arrValues['service_id'] ) ); elseif( isset( $arrValues['service_id'] ) ) $this->setServiceId( $arrValues['service_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['type'] ) && $boolDirectSet ) $this->set( 'm_strType', trim( stripcslashes( $arrValues['type'] ) ) ); elseif( isset( $arrValues['type'] ) ) $this->setType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['type'] ) : $arrValues['type'] );
		if( isset( $arrValues['max_occurance'] ) && $boolDirectSet ) $this->set( 'm_intMaxOccurance', trim( $arrValues['max_occurance'] ) ); elseif( isset( $arrValues['max_occurance'] ) ) $this->setMaxOccurance( $arrValues['max_occurance'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['is_required'] ) && $boolDirectSet ) $this->set( 'm_boolIsRequired', trim( stripcslashes( $arrValues['is_required'] ) ) ); elseif( isset( $arrValues['is_required'] ) ) $this->setIsRequired( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_required'] ) : $arrValues['is_required'] );
		if( isset( $arrValues['is_multiple'] ) && $boolDirectSet ) $this->set( 'm_boolIsMultiple', trim( stripcslashes( $arrValues['is_multiple'] ) ) ); elseif( isset( $arrValues['is_multiple'] ) ) $this->setIsMultiple( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_multiple'] ) : $arrValues['is_multiple'] );
		if( isset( $arrValues['is_response'] ) && $boolDirectSet ) $this->set( 'm_boolIsResponse', trim( stripcslashes( $arrValues['is_response'] ) ) ); elseif( isset( $arrValues['is_response'] ) ) $this->setIsResponse( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_response'] ) : $arrValues['is_response'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setMinorApiVersionId( $intMinorApiVersionId ) {
		$this->set( 'm_intMinorApiVersionId', CStrings::strToIntDef( $intMinorApiVersionId, NULL, false ) );
	}

	public function getMinorApiVersionId() {
		return $this->m_intMinorApiVersionId;
	}

	public function sqlMinorApiVersionId() {
		return ( true == isset( $this->m_intMinorApiVersionId ) ) ? ( string ) $this->m_intMinorApiVersionId : 'NULL';
	}

	public function setServiceId( $intServiceId ) {
		$this->set( 'm_intServiceId', CStrings::strToIntDef( $intServiceId, NULL, false ) );
	}

	public function getServiceId() {
		return $this->m_intServiceId;
	}

	public function sqlServiceId() {
		return ( true == isset( $this->m_intServiceId ) ) ? ( string ) $this->m_intServiceId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 500, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setType( $strType ) {
		$this->set( 'm_strType', CStrings::strTrimDef( $strType, 20, NULL, true ) );
	}

	public function getType() {
		return $this->m_strType;
	}

	public function sqlType() {
		return ( true == isset( $this->m_strType ) ) ? '\'' . addslashes( $this->m_strType ) . '\'' : 'NULL';
	}

	public function setMaxOccurance( $intMaxOccurance ) {
		$this->set( 'm_intMaxOccurance', CStrings::strToIntDef( $intMaxOccurance, NULL, false ) );
	}

	public function getMaxOccurance() {
		return $this->m_intMaxOccurance;
	}

	public function sqlMaxOccurance() {
		return ( true == isset( $this->m_intMaxOccurance ) ) ? ( string ) $this->m_intMaxOccurance : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsRequired( $boolIsRequired ) {
		$this->set( 'm_boolIsRequired', CStrings::strToBool( $boolIsRequired ) );
	}

	public function getIsRequired() {
		return $this->m_boolIsRequired;
	}

	public function sqlIsRequired() {
		return ( true == isset( $this->m_boolIsRequired ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRequired ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsMultiple( $boolIsMultiple ) {
		$this->set( 'm_boolIsMultiple', CStrings::strToBool( $boolIsMultiple ) );
	}

	public function getIsMultiple() {
		return $this->m_boolIsMultiple;
	}

	public function sqlIsMultiple() {
		return ( true == isset( $this->m_boolIsMultiple ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsMultiple ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsResponse( $boolIsResponse ) {
		$this->set( 'm_boolIsResponse', CStrings::strToBool( $boolIsResponse ) );
	}

	public function getIsResponse() {
		return $this->m_boolIsResponse;
	}

	public function sqlIsResponse() {
		return ( true == isset( $this->m_boolIsResponse ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsResponse ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, minor_api_version_id, service_id, name, description, type, max_occurance, is_published, is_required, is_multiple, is_response, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlMinorApiVersionId() . ', ' .
 						$this->sqlServiceId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlType() . ', ' .
 						$this->sqlMaxOccurance() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlIsRequired() . ', ' .
 						$this->sqlIsMultiple() . ', ' .
 						$this->sqlIsResponse() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minor_api_version_id = ' . $this->sqlMinorApiVersionId() . ','; } elseif( true == array_key_exists( 'MinorApiVersionId', $this->getChangedColumns() ) ) { $strSql .= ' minor_api_version_id = ' . $this->sqlMinorApiVersionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_id = ' . $this->sqlServiceId() . ','; } elseif( true == array_key_exists( 'ServiceId', $this->getChangedColumns() ) ) { $strSql .= ' service_id = ' . $this->sqlServiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' type = ' . $this->sqlType() . ','; } elseif( true == array_key_exists( 'Type', $this->getChangedColumns() ) ) { $strSql .= ' type = ' . $this->sqlType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_occurance = ' . $this->sqlMaxOccurance() . ','; } elseif( true == array_key_exists( 'MaxOccurance', $this->getChangedColumns() ) ) { $strSql .= ' max_occurance = ' . $this->sqlMaxOccurance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_required = ' . $this->sqlIsRequired() . ','; } elseif( true == array_key_exists( 'IsRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_required = ' . $this->sqlIsRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_multiple = ' . $this->sqlIsMultiple() . ','; } elseif( true == array_key_exists( 'IsMultiple', $this->getChangedColumns() ) ) { $strSql .= ' is_multiple = ' . $this->sqlIsMultiple() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_response = ' . $this->sqlIsResponse() . ','; } elseif( true == array_key_exists( 'IsResponse', $this->getChangedColumns() ) ) { $strSql .= ' is_response = ' . $this->sqlIsResponse() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'minor_api_version_id' => $this->getMinorApiVersionId(),
			'service_id' => $this->getServiceId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'type' => $this->getType(),
			'max_occurance' => $this->getMaxOccurance(),
			'is_published' => $this->getIsPublished(),
			'is_required' => $this->getIsRequired(),
			'is_multiple' => $this->getIsMultiple(),
			'is_response' => $this->getIsResponse(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>