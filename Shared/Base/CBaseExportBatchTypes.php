<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CExportBatchTypes
 * Do not add any new functions to this class.
 */

class CBaseExportBatchTypes extends CEosPluralBase {

	/**
	 * @return CExportBatchType[]
	 */
	public static function fetchExportBatchTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CExportBatchType', $objDatabase );
	}

	/**
	 * @return CExportBatchType
	 */
	public static function fetchExportBatchType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CExportBatchType', $objDatabase );
	}

	public static function fetchExportBatchTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'export_batch_types', $objDatabase );
	}

	public static function fetchExportBatchTypeById( $intId, $objDatabase ) {
		return self::fetchExportBatchType( sprintf( 'SELECT * FROM export_batch_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>