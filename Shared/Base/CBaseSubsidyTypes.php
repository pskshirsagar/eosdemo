<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSubsidyTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyType[]
	 */
	public static function fetchSubsidyTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSubsidyType', $objDatabase );
	}

	/**
	 * @return CSubsidyType
	 */
	public static function fetchSubsidyType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSubsidyType', $objDatabase );
	}

	public static function fetchSubsidyTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_types', $objDatabase );
	}

	public static function fetchSubsidyTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyType( sprintf( 'SELECT * FROM subsidy_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>