<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CMilitaryServiceUnits
 * Do not add any new functions to this class.
 */

class CBaseMilitaryServiceUnits extends CEosPluralBase {

	/**
	 * @return CMilitaryServiceUnit[]
	 */

	public static function fetchMilitaryServiceUnits( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMilitaryServiceUnit::class, $objDatabase );
	}

	/**
	 * @return CMilitaryServiceUnit
	 */
	public static function fetchMilitaryServiceUnit( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMilitaryServiceUnit::class, $objDatabase );
	}

	public static function fetchMilitaryServiceUnitCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'military_service_units', $objDatabase );
	}

	public static function fetchMilitaryServiceUnitById( $intId, $objDatabase ) {
		return self::fetchMilitaryServiceUnit( sprintf( 'SELECT * FROM military_service_units WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMilitaryServiceUnitsByMilitaryInstallationId( $intMilitaryInstallationId, $objDatabase ) {
		return self::fetchMilitaryServiceUnits( sprintf( 'SELECT * FROM military_service_units WHERE military_installation_id = %d', ( int ) $intMilitaryInstallationId ), $objDatabase );
	}

}
?>