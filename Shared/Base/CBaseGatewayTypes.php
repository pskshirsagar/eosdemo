<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CGatewayTypes
 * Do not add any new functions to this class.
 */

class CBaseGatewayTypes extends CEosPluralBase {

	/**
	 * @return CGatewayType[]
	 */
	public static function fetchGatewayTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CGatewayType::class, $objDatabase );
	}

	/**
	 * @return CGatewayType
	 */
	public static function fetchGatewayType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGatewayType::class, $objDatabase );
	}

	public static function fetchGatewayTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gateway_types', $objDatabase );
	}

	public static function fetchGatewayTypeById( $intId, $objDatabase ) {
		return self::fetchGatewayType( sprintf( 'SELECT * FROM gateway_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>