<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallPriorities
 * Do not add any new functions to this class.
 */

class CBaseCallPriorities extends CEosPluralBase {

	/**
	 * @return CCallPriority[]
	 */
	public static function fetchCallPriorities( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallPriority::class, $objDatabase );
	}

	/**
	 * @return CCallPriority
	 */
	public static function fetchCallPriority( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallPriority::class, $objDatabase );
	}

	public static function fetchCallPriorityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_priorities', $objDatabase );
	}

	public static function fetchCallPriorityById( $intId, $objDatabase ) {
		return self::fetchCallPriority( sprintf( 'SELECT * FROM call_priorities WHERE id = %d', $intId ), $objDatabase );
	}

}
?>