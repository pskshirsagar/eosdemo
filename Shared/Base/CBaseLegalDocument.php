<?php

class CBaseLegalDocument extends CEosSingularBase {

	const TABLE_NAME = 'public.legal_documents';

	protected $m_intId;
	protected $m_intLegalDocumentTypeId;
	protected $m_strEffectiveDatetime;
	protected $m_strCountryCode;
	protected $m_strLanguageCode;
	protected $m_intVersion;
	protected $m_strFileName;
	protected $m_boolIsMaterial;
	protected $m_boolIsPayment;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsMaterial = false;
		$this->m_boolIsPayment = false;
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['legal_document_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLegalDocumentTypeId', trim( $arrValues['legal_document_type_id'] ) ); elseif( isset( $arrValues['legal_document_type_id'] ) ) $this->setLegalDocumentTypeId( $arrValues['legal_document_type_id'] );
		if( isset( $arrValues['effective_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDatetime', trim( $arrValues['effective_datetime'] ) ); elseif( isset( $arrValues['effective_datetime'] ) ) $this->setEffectiveDatetime( $arrValues['effective_datetime'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( $arrValues['country_code'] ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( $arrValues['country_code'] );
		if( isset( $arrValues['language_code'] ) && $boolDirectSet ) $this->set( 'm_strLanguageCode', trim( $arrValues['language_code'] ) ); elseif( isset( $arrValues['language_code'] ) ) $this->setLanguageCode( $arrValues['language_code'] );
		if( isset( $arrValues['version'] ) && $boolDirectSet ) $this->set( 'm_intVersion', trim( $arrValues['version'] ) ); elseif( isset( $arrValues['version'] ) ) $this->setVersion( $arrValues['version'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( $arrValues['file_name'] ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( $arrValues['file_name'] );
		if( isset( $arrValues['is_material'] ) && $boolDirectSet ) $this->set( 'm_boolIsMaterial', trim( stripcslashes( $arrValues['is_material'] ) ) ); elseif( isset( $arrValues['is_material'] ) ) $this->setIsMaterial( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_material'] ) : $arrValues['is_material'] );
		if( isset( $arrValues['is_payment'] ) && $boolDirectSet ) $this->set( 'm_boolIsPayment', trim( stripcslashes( $arrValues['is_payment'] ) ) ); elseif( isset( $arrValues['is_payment'] ) ) $this->setIsPayment( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_payment'] ) : $arrValues['is_payment'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setLegalDocumentTypeId( $intLegalDocumentTypeId ) {
		$this->set( 'm_intLegalDocumentTypeId', CStrings::strToIntDef( $intLegalDocumentTypeId, NULL, false ) );
	}

	public function getLegalDocumentTypeId() {
		return $this->m_intLegalDocumentTypeId;
	}

	public function sqlLegalDocumentTypeId() {
		return ( true == isset( $this->m_intLegalDocumentTypeId ) ) ? ( string ) $this->m_intLegalDocumentTypeId : 'NULL';
	}

	public function setEffectiveDatetime( $strEffectiveDatetime ) {
		$this->set( 'm_strEffectiveDatetime', CStrings::strTrimDef( $strEffectiveDatetime, -1, NULL, true ) );
	}

	public function getEffectiveDatetime() {
		return $this->m_strEffectiveDatetime;
	}

	public function sqlEffectiveDatetime() {
		return ( true == isset( $this->m_strEffectiveDatetime ) ) ? '\'' . $this->m_strEffectiveDatetime . '\'' : 'NOW()';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCountryCode ) : '\'' . addslashes( $this->m_strCountryCode ) . '\'' ) : 'NULL';
	}

	public function setLanguageCode( $strLanguageCode ) {
		$this->set( 'm_strLanguageCode', CStrings::strTrimDef( $strLanguageCode, 3, NULL, true ) );
	}

	public function getLanguageCode() {
		return $this->m_strLanguageCode;
	}

	public function sqlLanguageCode() {
		return ( true == isset( $this->m_strLanguageCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLanguageCode ) : '\'' . addslashes( $this->m_strLanguageCode ) . '\'' ) : 'NULL';
	}

	public function setVersion( $intVersion ) {
		$this->set( 'm_intVersion', CStrings::strToIntDef( $intVersion, NULL, false ) );
	}

	public function getVersion() {
		return $this->m_intVersion;
	}

	public function sqlVersion() {
		return ( true == isset( $this->m_intVersion ) ) ? ( string ) $this->m_intVersion : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 255, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFileName ) : '\'' . addslashes( $this->m_strFileName ) . '\'' ) : 'NULL';
	}

	public function setIsMaterial( $boolIsMaterial ) {
		$this->set( 'm_boolIsMaterial', CStrings::strToBool( $boolIsMaterial ) );
	}

	public function getIsMaterial() {
		return $this->m_boolIsMaterial;
	}

	public function sqlIsMaterial() {
		return ( true == isset( $this->m_boolIsMaterial ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsMaterial ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPayment( $boolIsPayment ) {
		$this->set( 'm_boolIsPayment', CStrings::strToBool( $boolIsPayment ) );
	}

	public function getIsPayment() {
		return $this->m_boolIsPayment;
	}

	public function sqlIsPayment() {
		return ( true == isset( $this->m_boolIsPayment ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPayment ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, legal_document_type_id, effective_datetime, country_code, language_code, version, file_name, is_material, is_payment, deleted_by, deleted_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlLegalDocumentTypeId() . ', ' .
						$this->sqlEffectiveDatetime() . ', ' .
						$this->sqlCountryCode() . ', ' .
						$this->sqlLanguageCode() . ', ' .
						$this->sqlVersion() . ', ' .
						$this->sqlFileName() . ', ' .
						$this->sqlIsMaterial() . ', ' .
						$this->sqlIsPayment() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' legal_document_type_id = ' . $this->sqlLegalDocumentTypeId(). ',' ; } elseif( true == array_key_exists( 'LegalDocumentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' legal_document_type_id = ' . $this->sqlLegalDocumentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_datetime = ' . $this->sqlEffectiveDatetime(). ',' ; } elseif( true == array_key_exists( 'EffectiveDatetime', $this->getChangedColumns() ) ) { $strSql .= ' effective_datetime = ' . $this->sqlEffectiveDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode(). ',' ; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' language_code = ' . $this->sqlLanguageCode(). ',' ; } elseif( true == array_key_exists( 'LanguageCode', $this->getChangedColumns() ) ) { $strSql .= ' language_code = ' . $this->sqlLanguageCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' version = ' . $this->sqlVersion(). ',' ; } elseif( true == array_key_exists( 'Version', $this->getChangedColumns() ) ) { $strSql .= ' version = ' . $this->sqlVersion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName(). ',' ; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_material = ' . $this->sqlIsMaterial(). ',' ; } elseif( true == array_key_exists( 'IsMaterial', $this->getChangedColumns() ) ) { $strSql .= ' is_material = ' . $this->sqlIsMaterial() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_payment = ' . $this->sqlIsPayment(). ',' ; } elseif( true == array_key_exists( 'IsPayment', $this->getChangedColumns() ) ) { $strSql .= ' is_payment = ' . $this->sqlIsPayment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'legal_document_type_id' => $this->getLegalDocumentTypeId(),
			'effective_datetime' => $this->getEffectiveDatetime(),
			'country_code' => $this->getCountryCode(),
			'language_code' => $this->getLanguageCode(),
			'version' => $this->getVersion(),
			'file_name' => $this->getFileName(),
			'is_material' => $this->getIsMaterial(),
			'is_payment' => $this->getIsPayment(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>