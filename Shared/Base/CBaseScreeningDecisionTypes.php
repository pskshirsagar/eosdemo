<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningDecisionTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningDecisionTypes extends CEosPluralBase {

	/**
	 * @return CScreeningDecisionType[]
	 */
	public static function fetchScreeningDecisionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningDecisionType::class, $objDatabase );
	}

	/**
	 * @return CScreeningDecisionType
	 */
	public static function fetchScreeningDecisionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningDecisionType::class, $objDatabase );
	}

	public static function fetchScreeningDecisionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_decision_types', $objDatabase );
	}

	public static function fetchScreeningDecisionTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningDecisionType( sprintf( 'SELECT * FROM screening_decision_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>