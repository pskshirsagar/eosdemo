<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CVacationTypes
 * Do not add any new functions to this class.
 */

class CBaseVacationTypes extends CEosPluralBase {

	/**
	 * @return CVacationType[]
	 */
	public static function fetchVacationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CVacationType', $objDatabase );
	}

	/**
	 * @return CVacationType
	 */
	public static function fetchVacationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CVacationType', $objDatabase );
	}

	public static function fetchVacationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'vacation_types', $objDatabase );
	}

	public static function fetchVacationTypeById( $intId, $objDatabase ) {
		return self::fetchVacationType( sprintf( 'SELECT * FROM vacation_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>