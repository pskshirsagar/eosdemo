<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPaymentImageTypes
 * Do not add any new functions to this class.
 */

class CBasePaymentImageTypes extends CEosPluralBase {

	/**
	 * @return CPaymentImageType[]
	 */
	public static function fetchPaymentImageTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPaymentImageType::class, $objDatabase );
	}

	/**
	 * @return CPaymentImageType
	 */
	public static function fetchPaymentImageType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPaymentImageType::class, $objDatabase );
	}

	public static function fetchPaymentImageTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'payment_image_types', $objDatabase );
	}

	public static function fetchPaymentImageTypeById( $intId, $objDatabase ) {
		return self::fetchPaymentImageType( sprintf( 'SELECT * FROM payment_image_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>