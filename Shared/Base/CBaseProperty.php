<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseProperty extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.properties';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyTypeId;
	protected $m_intCompanyRegionId;
	protected $m_intTimeZoneId;
	protected $m_intOwnerId;
	protected $m_intAccountId;
	protected $m_arrintOccupancyTypeIds;
	protected $m_strRemotePrimaryKey;
	protected $m_strPropertyName;
	protected $m_strLookupCode;
	protected $m_strCountryCode;
	protected $m_strVaultwareNumber;
	protected $m_fltMinRent;
	protected $m_fltMaxRent;
	protected $m_fltMinSquareFeet;
	protected $m_fltMaxSquareFeet;
	protected $m_intMinBedrooms;
	protected $m_intMaxBedrooms;
	protected $m_fltMinBathrooms;
	protected $m_fltMaxBathrooms;
	protected $m_intNumberOfUnits;
	protected $m_strYearBuilt;
	protected $m_strYearRemodeled;
	protected $m_strShortDescription;
	protected $m_strFullDescription;
	protected $m_strDrivingDirections;
	protected $m_strTerminationReason;
	protected $m_strTerminationDate;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intAllowsCats;
	protected $m_intAllowsDogs;
	protected $m_intHasAvailability;
	protected $m_intIsDisabled;
	protected $m_intIsTest;
	protected $m_intIsManagerial;
	protected $m_intOrderNum;
	protected $m_strImportedOn;
	protected $m_strDisabledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strLocaleCode;
	protected $m_intDefaultOccupancyTypeId;
	protected $m_strMarketingName;

	public function __construct() {
		parent::__construct();

		$this->m_arrintOccupancyTypeIds = array( 1 );
		$this->m_strCountryCode = 'US';
		$this->m_intNumberOfUnits = '1';
		$this->m_intAllowsCats = '0';
		$this->m_intAllowsDogs = '0';
		$this->m_intHasAvailability = '0';
		$this->m_intIsDisabled = '0';
		$this->m_intIsTest = '0';
		$this->m_intIsManagerial = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyTypeId', trim( $arrValues['property_type_id'] ) ); elseif( isset( $arrValues['property_type_id'] ) ) $this->setPropertyTypeId( $arrValues['property_type_id'] );
		if( isset( $arrValues['company_region_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyRegionId', trim( $arrValues['company_region_id'] ) ); elseif( isset( $arrValues['company_region_id'] ) ) $this->setCompanyRegionId( $arrValues['company_region_id'] );
		if( isset( $arrValues['time_zone_id'] ) && $boolDirectSet ) $this->set( 'm_intTimeZoneId', trim( $arrValues['time_zone_id'] ) ); elseif( isset( $arrValues['time_zone_id'] ) ) $this->setTimeZoneId( $arrValues['time_zone_id'] );
		if( isset( $arrValues['owner_id'] ) && $boolDirectSet ) $this->set( 'm_intOwnerId', trim( $arrValues['owner_id'] ) ); elseif( isset( $arrValues['owner_id'] ) ) $this->setOwnerId( $arrValues['owner_id'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['occupancy_type_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintOccupancyTypeIds', trim( $arrValues['occupancy_type_ids'] ) ); elseif( isset( $arrValues['occupancy_type_ids'] ) ) $this->setOccupancyTypeIds( $arrValues['occupancy_type_ids'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['property_name'] ) && $boolDirectSet ) $this->set( 'm_strPropertyName', trim( stripcslashes( $arrValues['property_name'] ) ) ); elseif( isset( $arrValues['property_name'] ) ) $this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_name'] ) : $arrValues['property_name'] );
		if( isset( $arrValues['lookup_code'] ) && $boolDirectSet ) $this->set( 'm_strLookupCode', trim( stripcslashes( $arrValues['lookup_code'] ) ) ); elseif( isset( $arrValues['lookup_code'] ) ) $this->setLookupCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['lookup_code'] ) : $arrValues['lookup_code'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		if( isset( $arrValues['vaultware_number'] ) && $boolDirectSet ) $this->set( 'm_strVaultwareNumber', trim( stripcslashes( $arrValues['vaultware_number'] ) ) ); elseif( isset( $arrValues['vaultware_number'] ) ) $this->setVaultwareNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vaultware_number'] ) : $arrValues['vaultware_number'] );
		if( isset( $arrValues['min_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMinRent', trim( $arrValues['min_rent'] ) ); elseif( isset( $arrValues['min_rent'] ) ) $this->setMinRent( $arrValues['min_rent'] );
		if( isset( $arrValues['max_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMaxRent', trim( $arrValues['max_rent'] ) ); elseif( isset( $arrValues['max_rent'] ) ) $this->setMaxRent( $arrValues['max_rent'] );
		if( isset( $arrValues['min_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltMinSquareFeet', trim( $arrValues['min_square_feet'] ) ); elseif( isset( $arrValues['min_square_feet'] ) ) $this->setMinSquareFeet( $arrValues['min_square_feet'] );
		if( isset( $arrValues['max_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltMaxSquareFeet', trim( $arrValues['max_square_feet'] ) ); elseif( isset( $arrValues['max_square_feet'] ) ) $this->setMaxSquareFeet( $arrValues['max_square_feet'] );
		if( isset( $arrValues['min_bedrooms'] ) && $boolDirectSet ) $this->set( 'm_intMinBedrooms', trim( $arrValues['min_bedrooms'] ) ); elseif( isset( $arrValues['min_bedrooms'] ) ) $this->setMinBedrooms( $arrValues['min_bedrooms'] );
		if( isset( $arrValues['max_bedrooms'] ) && $boolDirectSet ) $this->set( 'm_intMaxBedrooms', trim( $arrValues['max_bedrooms'] ) ); elseif( isset( $arrValues['max_bedrooms'] ) ) $this->setMaxBedrooms( $arrValues['max_bedrooms'] );
		if( isset( $arrValues['min_bathrooms'] ) && $boolDirectSet ) $this->set( 'm_fltMinBathrooms', trim( $arrValues['min_bathrooms'] ) ); elseif( isset( $arrValues['min_bathrooms'] ) ) $this->setMinBathrooms( $arrValues['min_bathrooms'] );
		if( isset( $arrValues['max_bathrooms'] ) && $boolDirectSet ) $this->set( 'm_fltMaxBathrooms', trim( $arrValues['max_bathrooms'] ) ); elseif( isset( $arrValues['max_bathrooms'] ) ) $this->setMaxBathrooms( $arrValues['max_bathrooms'] );
		if( isset( $arrValues['number_of_units'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfUnits', trim( $arrValues['number_of_units'] ) ); elseif( isset( $arrValues['number_of_units'] ) ) $this->setNumberOfUnits( $arrValues['number_of_units'] );
		if( isset( $arrValues['year_built'] ) && $boolDirectSet ) $this->set( 'm_strYearBuilt', trim( $arrValues['year_built'] ) ); elseif( isset( $arrValues['year_built'] ) ) $this->setYearBuilt( $arrValues['year_built'] );
		if( isset( $arrValues['year_remodeled'] ) && $boolDirectSet ) $this->set( 'm_strYearRemodeled', trim( $arrValues['year_remodeled'] ) ); elseif( isset( $arrValues['year_remodeled'] ) ) $this->setYearRemodeled( $arrValues['year_remodeled'] );
		if( isset( $arrValues['short_description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strShortDescription', trim( stripcslashes( $arrValues['short_description'] ) ) ); elseif( isset( $arrValues['short_description'] ) ) $this->setShortDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['short_description'] ) : $arrValues['short_description'] );
		if( isset( $arrValues['full_description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strFullDescription', trim( stripcslashes( $arrValues['full_description'] ) ) ); elseif( isset( $arrValues['full_description'] ) ) $this->setFullDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['full_description'] ) : $arrValues['full_description'] );
		if( isset( $arrValues['driving_directions'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDrivingDirections', trim( stripcslashes( $arrValues['driving_directions'] ) ) ); elseif( isset( $arrValues['driving_directions'] ) ) $this->setDrivingDirections( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['driving_directions'] ) : $arrValues['driving_directions'] );
		if( isset( $arrValues['termination_reason'] ) && $boolDirectSet ) $this->set( 'm_strTerminationReason', trim( stripcslashes( $arrValues['termination_reason'] ) ) ); elseif( isset( $arrValues['termination_reason'] ) ) $this->setTerminationReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['termination_reason'] ) : $arrValues['termination_reason'] );
		if( isset( $arrValues['termination_date'] ) && $boolDirectSet ) $this->set( 'm_strTerminationDate', trim( $arrValues['termination_date'] ) ); elseif( isset( $arrValues['termination_date'] ) ) $this->setTerminationDate( $arrValues['termination_date'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['allows_cats'] ) && $boolDirectSet ) $this->set( 'm_intAllowsCats', trim( $arrValues['allows_cats'] ) ); elseif( isset( $arrValues['allows_cats'] ) ) $this->setAllowsCats( $arrValues['allows_cats'] );
		if( isset( $arrValues['allows_dogs'] ) && $boolDirectSet ) $this->set( 'm_intAllowsDogs', trim( $arrValues['allows_dogs'] ) ); elseif( isset( $arrValues['allows_dogs'] ) ) $this->setAllowsDogs( $arrValues['allows_dogs'] );
		if( isset( $arrValues['has_availability'] ) && $boolDirectSet ) $this->set( 'm_intHasAvailability', trim( $arrValues['has_availability'] ) ); elseif( isset( $arrValues['has_availability'] ) ) $this->setHasAvailability( $arrValues['has_availability'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['is_test'] ) && $boolDirectSet ) $this->set( 'm_intIsTest', trim( $arrValues['is_test'] ) ); elseif( isset( $arrValues['is_test'] ) ) $this->setIsTest( $arrValues['is_test'] );
		if( isset( $arrValues['is_managerial'] ) && $boolDirectSet ) $this->set( 'm_intIsManagerial', trim( $arrValues['is_managerial'] ) ); elseif( isset( $arrValues['is_managerial'] ) ) $this->setIsManagerial( $arrValues['is_managerial'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['imported_on'] ) && $boolDirectSet ) $this->set( 'm_strImportedOn', trim( $arrValues['imported_on'] ) ); elseif( isset( $arrValues['imported_on'] ) ) $this->setImportedOn( $arrValues['imported_on'] );
		if( isset( $arrValues['disabled_on'] ) && $boolDirectSet ) $this->set( 'm_strDisabledOn', trim( $arrValues['disabled_on'] ) ); elseif( isset( $arrValues['disabled_on'] ) ) $this->setDisabledOn( $arrValues['disabled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['locale_code'] ) && $boolDirectSet ) $this->set( 'm_strLocaleCode', trim( stripcslashes( $arrValues['locale_code'] ) ) ); elseif( isset( $arrValues['locale_code'] ) ) $this->setLocaleCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['locale_code'] ) : $arrValues['locale_code'] );
		if( isset( $arrValues['default_occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultOccupancyTypeId', trim( $arrValues['default_occupancy_type_id'] ) ); elseif( isset( $arrValues['default_occupancy_type_id'] ) ) $this->setDefaultOccupancyTypeId( $arrValues['default_occupancy_type_id'] );
		if( isset( $arrValues['marketing_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strMarketingName', trim( $arrValues['marketing_name'] ) ); elseif( isset( $arrValues['marketing_name'] ) ) $this->setMarketingName( $arrValues['marketing_name'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyTypeId( $intPropertyTypeId ) {
		$this->set( 'm_intPropertyTypeId', CStrings::strToIntDef( $intPropertyTypeId, NULL, false ) );
	}

	public function getPropertyTypeId() {
		return $this->m_intPropertyTypeId;
	}

	public function sqlPropertyTypeId() {
		return ( true == isset( $this->m_intPropertyTypeId ) ) ? ( string ) $this->m_intPropertyTypeId : 'NULL';
	}

	public function setCompanyRegionId( $intCompanyRegionId ) {
		$this->set( 'm_intCompanyRegionId', CStrings::strToIntDef( $intCompanyRegionId, NULL, false ) );
	}

	public function getCompanyRegionId() {
		return $this->m_intCompanyRegionId;
	}

	public function sqlCompanyRegionId() {
		return ( true == isset( $this->m_intCompanyRegionId ) ) ? ( string ) $this->m_intCompanyRegionId : 'NULL';
	}

	public function setTimeZoneId( $intTimeZoneId ) {
		$this->set( 'm_intTimeZoneId', CStrings::strToIntDef( $intTimeZoneId, NULL, false ) );
	}

	public function getTimeZoneId() {
		return $this->m_intTimeZoneId;
	}

	public function sqlTimeZoneId() {
		return ( true == isset( $this->m_intTimeZoneId ) ) ? ( string ) $this->m_intTimeZoneId : 'NULL';
	}

	public function setOwnerId( $intOwnerId ) {
		$this->set( 'm_intOwnerId', CStrings::strToIntDef( $intOwnerId, NULL, false ) );
	}

	public function getOwnerId() {
		return $this->m_intOwnerId;
	}

	public function sqlOwnerId() {
		return ( true == isset( $this->m_intOwnerId ) ) ? ( string ) $this->m_intOwnerId : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setOccupancyTypeIds( $arrintOccupancyTypeIds ) {
		$this->set( 'm_arrintOccupancyTypeIds', CStrings::strToArrIntDef( $arrintOccupancyTypeIds, NULL ) );
	}

	public function getOccupancyTypeIds() {
		return $this->m_arrintOccupancyTypeIds;
	}

	public function sqlOccupancyTypeIds() {
		return ( true == isset( $this->m_arrintOccupancyTypeIds ) && true == valArr( $this->m_arrintOccupancyTypeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintOccupancyTypeIds, NULL ) . '\'' : '\'{1}\'';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setPropertyName( $strPropertyName ) {
		$this->set( 'm_strPropertyName', CStrings::strTrimDef( $strPropertyName, 50, NULL, true ) );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function sqlPropertyName() {
		return ( true == isset( $this->m_strPropertyName ) ) ? '\'' . addslashes( $this->m_strPropertyName ) . '\'' : 'NULL';
	}

	public function setLookupCode( $strLookupCode ) {
		$this->set( 'm_strLookupCode', CStrings::strTrimDef( $strLookupCode, 240, NULL, true ) );
	}

	public function getLookupCode() {
		return $this->m_strLookupCode;
	}

	public function sqlLookupCode() {
		return ( true == isset( $this->m_strLookupCode ) ) ? '\'' . addslashes( $this->m_strLookupCode ) . '\'' : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : '\'US\'';
	}

	public function setVaultwareNumber( $strVaultwareNumber ) {
		$this->set( 'm_strVaultwareNumber', CStrings::strTrimDef( $strVaultwareNumber, 50, NULL, true ) );
	}

	public function getVaultwareNumber() {
		return $this->m_strVaultwareNumber;
	}

	public function sqlVaultwareNumber() {
		return ( true == isset( $this->m_strVaultwareNumber ) ) ? '\'' . addslashes( $this->m_strVaultwareNumber ) . '\'' : 'NULL';
	}

	public function setMinRent( $fltMinRent ) {
		$this->set( 'm_fltMinRent', CStrings::strToFloatDef( $fltMinRent, NULL, false, 4 ) );
	}

	public function getMinRent() {
		return $this->m_fltMinRent;
	}

	public function sqlMinRent() {
		return ( true == isset( $this->m_fltMinRent ) ) ? ( string ) $this->m_fltMinRent : 'NULL';
	}

	public function setMaxRent( $fltMaxRent ) {
		$this->set( 'm_fltMaxRent', CStrings::strToFloatDef( $fltMaxRent, NULL, false, 4 ) );
	}

	public function getMaxRent() {
		return $this->m_fltMaxRent;
	}

	public function sqlMaxRent() {
		return ( true == isset( $this->m_fltMaxRent ) ) ? ( string ) $this->m_fltMaxRent : 'NULL';
	}

	public function setMinSquareFeet( $fltMinSquareFeet ) {
		$this->set( 'm_fltMinSquareFeet', CStrings::strToFloatDef( $fltMinSquareFeet, NULL, false, 4 ) );
	}

	public function getMinSquareFeet() {
		return $this->m_fltMinSquareFeet;
	}

	public function sqlMinSquareFeet() {
		return ( true == isset( $this->m_fltMinSquareFeet ) ) ? ( string ) $this->m_fltMinSquareFeet : 'NULL';
	}

	public function setMaxSquareFeet( $fltMaxSquareFeet ) {
		$this->set( 'm_fltMaxSquareFeet', CStrings::strToFloatDef( $fltMaxSquareFeet, NULL, false, 4 ) );
	}

	public function getMaxSquareFeet() {
		return $this->m_fltMaxSquareFeet;
	}

	public function sqlMaxSquareFeet() {
		return ( true == isset( $this->m_fltMaxSquareFeet ) ) ? ( string ) $this->m_fltMaxSquareFeet : 'NULL';
	}

	public function setMinBedrooms( $intMinBedrooms ) {
		$this->set( 'm_intMinBedrooms', CStrings::strToIntDef( $intMinBedrooms, NULL, false ) );
	}

	public function getMinBedrooms() {
		return $this->m_intMinBedrooms;
	}

	public function sqlMinBedrooms() {
		return ( true == isset( $this->m_intMinBedrooms ) ) ? ( string ) $this->m_intMinBedrooms : 'NULL';
	}

	public function setMaxBedrooms( $intMaxBedrooms ) {
		$this->set( 'm_intMaxBedrooms', CStrings::strToIntDef( $intMaxBedrooms, NULL, false ) );
	}

	public function getMaxBedrooms() {
		return $this->m_intMaxBedrooms;
	}

	public function sqlMaxBedrooms() {
		return ( true == isset( $this->m_intMaxBedrooms ) ) ? ( string ) $this->m_intMaxBedrooms : 'NULL';
	}

	public function setMinBathrooms( $fltMinBathrooms ) {
		$this->set( 'm_fltMinBathrooms', CStrings::strToFloatDef( $fltMinBathrooms, NULL, false, 4 ) );
	}

	public function getMinBathrooms() {
		return $this->m_fltMinBathrooms;
	}

	public function sqlMinBathrooms() {
		return ( true == isset( $this->m_fltMinBathrooms ) ) ? ( string ) $this->m_fltMinBathrooms : 'NULL';
	}

	public function setMaxBathrooms( $fltMaxBathrooms ) {
		$this->set( 'm_fltMaxBathrooms', CStrings::strToFloatDef( $fltMaxBathrooms, NULL, false, 4 ) );
	}

	public function getMaxBathrooms() {
		return $this->m_fltMaxBathrooms;
	}

	public function sqlMaxBathrooms() {
		return ( true == isset( $this->m_fltMaxBathrooms ) ) ? ( string ) $this->m_fltMaxBathrooms : 'NULL';
	}

	public function setNumberOfUnits( $intNumberOfUnits ) {
		$this->set( 'm_intNumberOfUnits', CStrings::strToIntDef( $intNumberOfUnits, NULL, false ) );
	}

	public function getNumberOfUnits() {
		return $this->m_intNumberOfUnits;
	}

	public function sqlNumberOfUnits() {
		return ( true == isset( $this->m_intNumberOfUnits ) ) ? ( string ) $this->m_intNumberOfUnits : '1';
	}

	public function setYearBuilt( $strYearBuilt ) {
		$this->set( 'm_strYearBuilt', CStrings::strTrimDef( $strYearBuilt, -1, NULL, true ) );
	}

	public function getYearBuilt() {
		return $this->m_strYearBuilt;
	}

	public function sqlYearBuilt() {
		return ( true == isset( $this->m_strYearBuilt ) ) ? '\'' . $this->m_strYearBuilt . '\'' : 'NULL';
	}

	public function setYearRemodeled( $strYearRemodeled ) {
		$this->set( 'm_strYearRemodeled', CStrings::strTrimDef( $strYearRemodeled, -1, NULL, true ) );
	}

	public function getYearRemodeled() {
		return $this->m_strYearRemodeled;
	}

	public function sqlYearRemodeled() {
		return ( true == isset( $this->m_strYearRemodeled ) ) ? '\'' . $this->m_strYearRemodeled . '\'' : 'NULL';
	}

	public function setShortDescription( $strShortDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strShortDescription', CStrings::strTrimDef( $strShortDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getShortDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strShortDescription', $strLocaleCode );
	}

	public function sqlShortDescription() {
		return ( true == isset( $this->m_strShortDescription ) ) ? '\'' . addslashes( $this->m_strShortDescription ) . '\'' : 'NULL';
	}

	public function setFullDescription( $strFullDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strFullDescription', CStrings::strTrimDef( $strFullDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getFullDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strFullDescription', $strLocaleCode );
	}

	public function sqlFullDescription() {
		return ( true == isset( $this->m_strFullDescription ) ) ? '\'' . addslashes( $this->m_strFullDescription ) . '\'' : 'NULL';
	}

	public function setDrivingDirections( $strDrivingDirections, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDrivingDirections', CStrings::strTrimDef( $strDrivingDirections, -1, NULL, true ), $strLocaleCode );
	}

	public function getDrivingDirections( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDrivingDirections', $strLocaleCode );
	}

	public function sqlDrivingDirections() {
		return ( true == isset( $this->m_strDrivingDirections ) ) ? '\'' . addslashes( $this->m_strDrivingDirections ) . '\'' : 'NULL';
	}

	public function setTerminationReason( $strTerminationReason ) {
		$this->set( 'm_strTerminationReason', CStrings::strTrimDef( $strTerminationReason, 240, NULL, true ) );
	}

	public function getTerminationReason() {
		return $this->m_strTerminationReason;
	}

	public function sqlTerminationReason() {
		return ( true == isset( $this->m_strTerminationReason ) ) ? '\'' . addslashes( $this->m_strTerminationReason ) . '\'' : 'NULL';
	}

	public function setTerminationDate( $strTerminationDate ) {
		$this->set( 'm_strTerminationDate', CStrings::strTrimDef( $strTerminationDate, -1, NULL, true ) );
	}

	public function getTerminationDate() {
		return $this->m_strTerminationDate;
	}

	public function sqlTerminationDate() {
		return ( true == isset( $this->m_strTerminationDate ) ) ? '\'' . $this->m_strTerminationDate . '\'' : 'NULL';
	}

	public function setAllowsCats( $intAllowsCats ) {
		$this->set( 'm_intAllowsCats', CStrings::strToIntDef( $intAllowsCats, NULL, false ) );
	}

	public function getAllowsCats() {
		return $this->m_intAllowsCats;
	}

	public function sqlAllowsCats() {
		return ( true == isset( $this->m_intAllowsCats ) ) ? ( string ) $this->m_intAllowsCats : '0';
	}

	public function setAllowsDogs( $intAllowsDogs ) {
		$this->set( 'm_intAllowsDogs', CStrings::strToIntDef( $intAllowsDogs, NULL, false ) );
	}

	public function getAllowsDogs() {
		return $this->m_intAllowsDogs;
	}

	public function sqlAllowsDogs() {
		return ( true == isset( $this->m_intAllowsDogs ) ) ? ( string ) $this->m_intAllowsDogs : '0';
	}

	public function setHasAvailability( $intHasAvailability ) {
		$this->set( 'm_intHasAvailability', CStrings::strToIntDef( $intHasAvailability, NULL, false ) );
	}

	public function getHasAvailability() {
		return $this->m_intHasAvailability;
	}

	public function sqlHasAvailability() {
		return ( true == isset( $this->m_intHasAvailability ) ) ? ( string ) $this->m_intHasAvailability : '0';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function setIsTest( $intIsTest ) {
		$this->set( 'm_intIsTest', CStrings::strToIntDef( $intIsTest, NULL, false ) );
	}

	public function getIsTest() {
		return $this->m_intIsTest;
	}

	public function sqlIsTest() {
		return ( true == isset( $this->m_intIsTest ) ) ? ( string ) $this->m_intIsTest : '0';
	}

	public function setIsManagerial( $intIsManagerial ) {
		$this->set( 'm_intIsManagerial', CStrings::strToIntDef( $intIsManagerial, NULL, false ) );
	}

	public function getIsManagerial() {
		return $this->m_intIsManagerial;
	}

	public function sqlIsManagerial() {
		return ( true == isset( $this->m_intIsManagerial ) ) ? ( string ) $this->m_intIsManagerial : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setImportedOn( $strImportedOn ) {
		$this->set( 'm_strImportedOn', CStrings::strTrimDef( $strImportedOn, -1, NULL, true ) );
	}

	public function getImportedOn() {
		return $this->m_strImportedOn;
	}

	public function sqlImportedOn() {
		return ( true == isset( $this->m_strImportedOn ) ) ? '\'' . $this->m_strImportedOn . '\'' : 'NULL';
	}

	public function setDisabledOn( $strDisabledOn ) {
		$this->set( 'm_strDisabledOn', CStrings::strTrimDef( $strDisabledOn, -1, NULL, true ) );
	}

	public function getDisabledOn() {
		return $this->m_strDisabledOn;
	}

	public function sqlDisabledOn() {
		return ( true == isset( $this->m_strDisabledOn ) ) ? '\'' . $this->m_strDisabledOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setLocaleCode( $strLocaleCode ) {
		$this->set( 'm_strLocaleCode', CStrings::strTrimDef( $strLocaleCode, 100, NULL, true ) );
	}

	public function getLocaleCode() {
		return $this->m_strLocaleCode;
	}

	public function sqlLocaleCode() {
		return ( true == isset( $this->m_strLocaleCode ) ) ? '\'' . addslashes( $this->m_strLocaleCode ) . '\'' : 'NULL';
	}

	public function setDefaultOccupancyTypeId( $intDefaultOccupancyTypeId ) {
		$this->set( 'm_intDefaultOccupancyTypeId', CStrings::strToIntDef( $intDefaultOccupancyTypeId, NULL, false ) );
	}

	public function getDefaultOccupancyTypeId() {
		return $this->m_intDefaultOccupancyTypeId;
	}

	public function sqlDefaultOccupancyTypeId() {
		return ( true == isset( $this->m_intDefaultOccupancyTypeId ) ) ? ( string ) $this->m_intDefaultOccupancyTypeId : 'NULL';
	}

	public function setMarketingName( $strMarketingName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strMarketingName', CStrings::strTrimDef( $strMarketingName, -1, NULL, true ), $strLocaleCode );
	}

	public function getMarketingName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strMarketingName', $strLocaleCode );
	}

	public function sqlMarketingName() {
		return ( true == isset( $this->m_strMarketingName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMarketingName ) : '\'' . addslashes( $this->m_strMarketingName ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_type_id, company_region_id, time_zone_id, owner_id, account_id, occupancy_type_ids, remote_primary_key, property_name, lookup_code, country_code, vaultware_number, min_rent, max_rent, min_square_feet, max_square_feet, min_bedrooms, max_bedrooms, min_bathrooms, max_bathrooms, number_of_units, year_built, year_remodeled, short_description, full_description, driving_directions, termination_reason, termination_date, details, allows_cats, allows_dogs, has_availability, is_disabled, is_test, is_managerial, order_num, imported_on, disabled_on, updated_by, updated_on, created_by, created_on, locale_code, default_occupancy_type_id, marketing_name )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyTypeId() . ', ' .
						$this->sqlCompanyRegionId() . ', ' .
						$this->sqlTimeZoneId() . ', ' .
						$this->sqlOwnerId() . ', ' .
						$this->sqlAccountId() . ', ' .
						$this->sqlOccupancyTypeIds() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlPropertyName() . ', ' .
						$this->sqlLookupCode() . ', ' .
						$this->sqlCountryCode() . ', ' .
						$this->sqlVaultwareNumber() . ', ' .
						$this->sqlMinRent() . ', ' .
						$this->sqlMaxRent() . ', ' .
						$this->sqlMinSquareFeet() . ', ' .
						$this->sqlMaxSquareFeet() . ', ' .
						$this->sqlMinBedrooms() . ', ' .
						$this->sqlMaxBedrooms() . ', ' .
						$this->sqlMinBathrooms() . ', ' .
						$this->sqlMaxBathrooms() . ', ' .
						$this->sqlNumberOfUnits() . ', ' .
						$this->sqlYearBuilt() . ', ' .
						$this->sqlYearRemodeled() . ', ' .
						$this->sqlShortDescription() . ', ' .
						$this->sqlFullDescription() . ', ' .
						$this->sqlDrivingDirections() . ', ' .
						$this->sqlTerminationReason() . ', ' .
						$this->sqlTerminationDate() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlAllowsCats() . ', ' .
						$this->sqlAllowsDogs() . ', ' .
						$this->sqlHasAvailability() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						$this->sqlIsTest() . ', ' .
						$this->sqlIsManagerial() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlImportedOn() . ', ' .
						$this->sqlDisabledOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlLocaleCode() . ', ' .
						$this->sqlDefaultOccupancyTypeId() . ', ' .
						$this->sqlMarketingName() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_type_id = ' . $this->sqlPropertyTypeId(). ',' ; } elseif( true == array_key_exists( 'PropertyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_type_id = ' . $this->sqlPropertyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_region_id = ' . $this->sqlCompanyRegionId(). ',' ; } elseif( true == array_key_exists( 'CompanyRegionId', $this->getChangedColumns() ) ) { $strSql .= ' company_region_id = ' . $this->sqlCompanyRegionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' time_zone_id = ' . $this->sqlTimeZoneId(). ',' ; } elseif( true == array_key_exists( 'TimeZoneId', $this->getChangedColumns() ) ) { $strSql .= ' time_zone_id = ' . $this->sqlTimeZoneId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_id = ' . $this->sqlOwnerId(). ',' ; } elseif( true == array_key_exists( 'OwnerId', $this->getChangedColumns() ) ) { $strSql .= ' owner_id = ' . $this->sqlOwnerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_ids = ' . $this->sqlOccupancyTypeIds(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeIds', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_ids = ' . $this->sqlOccupancyTypeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_name = ' . $this->sqlPropertyName(). ',' ; } elseif( true == array_key_exists( 'PropertyName', $this->getChangedColumns() ) ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lookup_code = ' . $this->sqlLookupCode(). ',' ; } elseif( true == array_key_exists( 'LookupCode', $this->getChangedColumns() ) ) { $strSql .= ' lookup_code = ' . $this->sqlLookupCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode(). ',' ; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vaultware_number = ' . $this->sqlVaultwareNumber(). ',' ; } elseif( true == array_key_exists( 'VaultwareNumber', $this->getChangedColumns() ) ) { $strSql .= ' vaultware_number = ' . $this->sqlVaultwareNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_rent = ' . $this->sqlMinRent(). ',' ; } elseif( true == array_key_exists( 'MinRent', $this->getChangedColumns() ) ) { $strSql .= ' min_rent = ' . $this->sqlMinRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_rent = ' . $this->sqlMaxRent(). ',' ; } elseif( true == array_key_exists( 'MaxRent', $this->getChangedColumns() ) ) { $strSql .= ' max_rent = ' . $this->sqlMaxRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_square_feet = ' . $this->sqlMinSquareFeet(). ',' ; } elseif( true == array_key_exists( 'MinSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' min_square_feet = ' . $this->sqlMinSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_square_feet = ' . $this->sqlMaxSquareFeet(). ',' ; } elseif( true == array_key_exists( 'MaxSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' max_square_feet = ' . $this->sqlMaxSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_bedrooms = ' . $this->sqlMinBedrooms(). ',' ; } elseif( true == array_key_exists( 'MinBedrooms', $this->getChangedColumns() ) ) { $strSql .= ' min_bedrooms = ' . $this->sqlMinBedrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_bedrooms = ' . $this->sqlMaxBedrooms(). ',' ; } elseif( true == array_key_exists( 'MaxBedrooms', $this->getChangedColumns() ) ) { $strSql .= ' max_bedrooms = ' . $this->sqlMaxBedrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_bathrooms = ' . $this->sqlMinBathrooms(). ',' ; } elseif( true == array_key_exists( 'MinBathrooms', $this->getChangedColumns() ) ) { $strSql .= ' min_bathrooms = ' . $this->sqlMinBathrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_bathrooms = ' . $this->sqlMaxBathrooms(). ',' ; } elseif( true == array_key_exists( 'MaxBathrooms', $this->getChangedColumns() ) ) { $strSql .= ' max_bathrooms = ' . $this->sqlMaxBathrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_units = ' . $this->sqlNumberOfUnits(). ',' ; } elseif( true == array_key_exists( 'NumberOfUnits', $this->getChangedColumns() ) ) { $strSql .= ' number_of_units = ' . $this->sqlNumberOfUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' year_built = ' . $this->sqlYearBuilt(). ',' ; } elseif( true == array_key_exists( 'YearBuilt', $this->getChangedColumns() ) ) { $strSql .= ' year_built = ' . $this->sqlYearBuilt() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' year_remodeled = ' . $this->sqlYearRemodeled(). ',' ; } elseif( true == array_key_exists( 'YearRemodeled', $this->getChangedColumns() ) ) { $strSql .= ' year_remodeled = ' . $this->sqlYearRemodeled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' short_description = ' . $this->sqlShortDescription(). ',' ; } elseif( true == array_key_exists( 'ShortDescription', $this->getChangedColumns() ) ) { $strSql .= ' short_description = ' . $this->sqlShortDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' full_description = ' . $this->sqlFullDescription(). ',' ; } elseif( true == array_key_exists( 'FullDescription', $this->getChangedColumns() ) ) { $strSql .= ' full_description = ' . $this->sqlFullDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' driving_directions = ' . $this->sqlDrivingDirections(). ',' ; } elseif( true == array_key_exists( 'DrivingDirections', $this->getChangedColumns() ) ) { $strSql .= ' driving_directions = ' . $this->sqlDrivingDirections() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_reason = ' . $this->sqlTerminationReason(). ',' ; } elseif( true == array_key_exists( 'TerminationReason', $this->getChangedColumns() ) ) { $strSql .= ' termination_reason = ' . $this->sqlTerminationReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_date = ' . $this->sqlTerminationDate(). ',' ; } elseif( true == array_key_exists( 'TerminationDate', $this->getChangedColumns() ) ) { $strSql .= ' termination_date = ' . $this->sqlTerminationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allows_cats = ' . $this->sqlAllowsCats(). ',' ; } elseif( true == array_key_exists( 'AllowsCats', $this->getChangedColumns() ) ) { $strSql .= ' allows_cats = ' . $this->sqlAllowsCats() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allows_dogs = ' . $this->sqlAllowsDogs(). ',' ; } elseif( true == array_key_exists( 'AllowsDogs', $this->getChangedColumns() ) ) { $strSql .= ' allows_dogs = ' . $this->sqlAllowsDogs() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_availability = ' . $this->sqlHasAvailability(). ',' ; } elseif( true == array_key_exists( 'HasAvailability', $this->getChangedColumns() ) ) { $strSql .= ' has_availability = ' . $this->sqlHasAvailability() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_test = ' . $this->sqlIsTest(). ',' ; } elseif( true == array_key_exists( 'IsTest', $this->getChangedColumns() ) ) { $strSql .= ' is_test = ' . $this->sqlIsTest() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_managerial = ' . $this->sqlIsManagerial(). ',' ; } elseif( true == array_key_exists( 'IsManagerial', $this->getChangedColumns() ) ) { $strSql .= ' is_managerial = ' . $this->sqlIsManagerial() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn(). ',' ; } elseif( true == array_key_exists( 'ImportedOn', $this->getChangedColumns() ) ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn(). ',' ; } elseif( true == array_key_exists( 'DisabledOn', $this->getChangedColumns() ) ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locale_code = ' . $this->sqlLocaleCode(). ',' ; } elseif( true == array_key_exists( 'LocaleCode', $this->getChangedColumns() ) ) { $strSql .= ' locale_code = ' . $this->sqlLocaleCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_occupancy_type_id = ' . $this->sqlDefaultOccupancyTypeId(). ',' ; } elseif( true == array_key_exists( 'DefaultOccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' default_occupancy_type_id = ' . $this->sqlDefaultOccupancyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_name = ' . $this->sqlMarketingName(). ',' ; } elseif( true == array_key_exists( 'MarketingName', $this->getChangedColumns() ) ) { $strSql .= ' marketing_name = ' . $this->sqlMarketingName() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_type_id' => $this->getPropertyTypeId(),
			'company_region_id' => $this->getCompanyRegionId(),
			'time_zone_id' => $this->getTimeZoneId(),
			'owner_id' => $this->getOwnerId(),
			'account_id' => $this->getAccountId(),
			'occupancy_type_ids' => $this->getOccupancyTypeIds(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'property_name' => $this->getPropertyName(),
			'lookup_code' => $this->getLookupCode(),
			'country_code' => $this->getCountryCode(),
			'vaultware_number' => $this->getVaultwareNumber(),
			'min_rent' => $this->getMinRent(),
			'max_rent' => $this->getMaxRent(),
			'min_square_feet' => $this->getMinSquareFeet(),
			'max_square_feet' => $this->getMaxSquareFeet(),
			'min_bedrooms' => $this->getMinBedrooms(),
			'max_bedrooms' => $this->getMaxBedrooms(),
			'min_bathrooms' => $this->getMinBathrooms(),
			'max_bathrooms' => $this->getMaxBathrooms(),
			'number_of_units' => $this->getNumberOfUnits(),
			'year_built' => $this->getYearBuilt(),
			'year_remodeled' => $this->getYearRemodeled(),
			'short_description' => $this->getShortDescription(),
			'full_description' => $this->getFullDescription(),
			'driving_directions' => $this->getDrivingDirections(),
			'termination_reason' => $this->getTerminationReason(),
			'termination_date' => $this->getTerminationDate(),
			'details' => $this->getDetails(),
			'allows_cats' => $this->getAllowsCats(),
			'allows_dogs' => $this->getAllowsDogs(),
			'has_availability' => $this->getHasAvailability(),
			'is_disabled' => $this->getIsDisabled(),
			'is_test' => $this->getIsTest(),
			'is_managerial' => $this->getIsManagerial(),
			'order_num' => $this->getOrderNum(),
			'imported_on' => $this->getImportedOn(),
			'disabled_on' => $this->getDisabledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'locale_code' => $this->getLocaleCode(),
			'default_occupancy_type_id' => $this->getDefaultOccupancyTypeId(),
			'marketing_name' => $this->getMarketingName()
		);
	}

}
?>