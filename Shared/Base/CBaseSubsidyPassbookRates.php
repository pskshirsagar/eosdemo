<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSubsidyPassbookRates
 * Do not add any new functions to this class.
 */

class CBaseSubsidyPassbookRates extends CEosPluralBase {

	/**
	 * @return CSubsidyPassbookRate[]
	 */
	public static function fetchSubsidyPassbookRates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSubsidyPassbookRate', $objDatabase );
	}

	/**
	 * @return CSubsidyPassbookRate
	 */
	public static function fetchSubsidyPassbookRate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSubsidyPassbookRate', $objDatabase );
	}

	public static function fetchSubsidyPassbookRateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_passbook_rates', $objDatabase );
	}

	public static function fetchSubsidyPassbookRateById( $intId, $objDatabase ) {
		return self::fetchSubsidyPassbookRate( sprintf( 'SELECT * FROM subsidy_passbook_rates WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>