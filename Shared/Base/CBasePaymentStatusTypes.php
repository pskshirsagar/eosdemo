<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPaymentStatusTypes
 * Do not add any new functions to this class.
 */

class CBasePaymentStatusTypes extends CEosPluralBase {

	/**
	 * @return CPaymentStatusType[]
	 */
	public static function fetchPaymentStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPaymentStatusType::class, $objDatabase );
	}

	/**
	 * @return CPaymentStatusType
	 */
	public static function fetchPaymentStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPaymentStatusType::class, $objDatabase );
	}

	public static function fetchPaymentStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'payment_status_types', $objDatabase );
	}

	public static function fetchPaymentStatusTypeById( $intId, $objDatabase ) {
		return self::fetchPaymentStatusType( sprintf( 'SELECT * FROM payment_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>