<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CMilitaryPayGradeRanks
 * Do not add any new functions to this class.
 */

class CBaseMilitaryPayGradeRanks extends CEosPluralBase {

	/**
	 * @return CMilitaryPayGradeRank[]
	 */
	public static function fetchMilitaryPayGradeRanks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMilitaryPayGradeRank', $objDatabase );
	}

	/**
	 * @return CMilitaryPayGradeRank
	 */
	public static function fetchMilitaryPayGradeRank( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMilitaryPayGradeRank', $objDatabase );
	}

	public static function fetchMilitaryPayGradeRankCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'military_pay_grade_ranks', $objDatabase );
	}

	public static function fetchMilitaryPayGradeRankById( $intId, $objDatabase ) {
		return self::fetchMilitaryPayGradeRank( sprintf( 'SELECT * FROM military_pay_grade_ranks WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMilitaryPayGradeRanksByMilitaryPayGradeId( $intMilitaryPayGradeId, $objDatabase ) {
		return self::fetchMilitaryPayGradeRanks( sprintf( 'SELECT * FROM military_pay_grade_ranks WHERE military_pay_grade_id = %d', ( int ) $intMilitaryPayGradeId ), $objDatabase );
	}

	public static function fetchMilitaryPayGradeRanksByMilitaryRankId( $intMilitaryRankId, $objDatabase ) {
		return self::fetchMilitaryPayGradeRanks( sprintf( 'SELECT * FROM military_pay_grade_ranks WHERE military_rank_id = %d', ( int ) $intMilitaryRankId ), $objDatabase );
	}

}
?>