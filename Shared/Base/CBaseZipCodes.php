<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CZipCodes
 * Do not add any new functions to this class.
 */

class CBaseZipCodes extends CEosPluralBase {

	/**
	 * @return CZipCode[]
	 */
	public static function fetchZipCodes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CZipCode', $objDatabase );
	}

	/**
	 * @return CZipCode
	 */
	public static function fetchZipCode( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CZipCode', $objDatabase );
	}

	public static function fetchZipCodeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'zip_codes', $objDatabase );
	}

	public static function fetchZipCodeById( $intId, $objDatabase ) {
		return self::fetchZipCode( sprintf( 'SELECT * FROM zip_codes WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchZipCodesByTimeZoneId( $intTimeZoneId, $objDatabase ) {
		return self::fetchZipCodes( sprintf( 'SELECT * FROM zip_codes WHERE time_zone_id = %d', ( int ) $intTimeZoneId ), $objDatabase );
	}

}
?>