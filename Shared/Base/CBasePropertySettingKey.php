<?php

class CBasePropertySettingKey extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.property_setting_keys';

	protected $m_intId;
	protected $m_intPropertySettingGroupId;
	protected $m_intDatabaseTypeId;
	protected $m_intToolTipId;
	protected $m_strKey;
	protected $m_strOldLabel;
	protected $m_strLabel;
	protected $m_strTableName;
	protected $m_strColumnName;
	protected $m_strDeleteValue;
	protected $m_strDefaultValue;
	protected $m_strOptions;
	protected $m_boolIsNegated;
	protected $m_boolRequiresSync;
	protected $m_boolIsImplementationRequired;
	protected $m_boolIsTemplatable;
	protected $m_boolIsDefaultCopy;
	protected $m_boolIsDisabled;
	protected $m_boolCanMarkEmptyCompleted;
	protected $m_boolIsBulkEditable;
	protected $m_boolIsClientSpecific;
	protected $m_strImplementationDescription;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsTranslated;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intDatabaseTypeId = '0';
		$this->m_boolIsNegated = false;
		$this->m_boolRequiresSync = false;
		$this->m_boolIsImplementationRequired = false;
		$this->m_boolIsTemplatable = true;
		$this->m_boolIsDefaultCopy = false;
		$this->m_boolIsDisabled = false;
		$this->m_boolCanMarkEmptyCompleted = true;
		$this->m_boolIsBulkEditable = false;
		$this->m_boolIsClientSpecific = false;
		$this->m_boolIsTranslated = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['property_setting_group_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertySettingGroupId', trim( $arrValues['property_setting_group_id'] ) ); elseif( isset( $arrValues['property_setting_group_id'] ) ) $this->setPropertySettingGroupId( $arrValues['property_setting_group_id'] );
		if( isset( $arrValues['database_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseTypeId', trim( $arrValues['database_type_id'] ) ); elseif( isset( $arrValues['database_type_id'] ) ) $this->setDatabaseTypeId( $arrValues['database_type_id'] );
		if( isset( $arrValues['tool_tip_id'] ) && $boolDirectSet ) $this->set( 'm_intToolTipId', trim( $arrValues['tool_tip_id'] ) ); elseif( isset( $arrValues['tool_tip_id'] ) ) $this->setToolTipId( $arrValues['tool_tip_id'] );
		if( isset( $arrValues['key'] ) && $boolDirectSet ) $this->set( 'm_strKey', trim( stripcslashes( $arrValues['key'] ) ) ); elseif( isset( $arrValues['key'] ) ) $this->setKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['key'] ) : $arrValues['key'] );
		if( isset( $arrValues['old_label'] ) && $boolDirectSet ) $this->set( 'm_strOldLabel', trim( stripcslashes( $arrValues['old_label'] ) ) ); elseif( isset( $arrValues['old_label'] ) ) $this->setOldLabel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['old_label'] ) : $arrValues['old_label'] );
		if( isset( $arrValues['label'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strLabel', trim( stripcslashes( $arrValues['label'] ) ) ); elseif( isset( $arrValues['label'] ) ) $this->setLabel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['label'] ) : $arrValues['label'] );
		if( isset( $arrValues['table_name'] ) && $boolDirectSet ) $this->set( 'm_strTableName', trim( stripcslashes( $arrValues['table_name'] ) ) ); elseif( isset( $arrValues['table_name'] ) ) $this->setTableName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['table_name'] ) : $arrValues['table_name'] );
		if( isset( $arrValues['column_name'] ) && $boolDirectSet ) $this->set( 'm_strColumnName', trim( stripcslashes( $arrValues['column_name'] ) ) ); elseif( isset( $arrValues['column_name'] ) ) $this->setColumnName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['column_name'] ) : $arrValues['column_name'] );
		if( isset( $arrValues['delete_value'] ) && $boolDirectSet ) $this->set( 'm_strDeleteValue', trim( stripcslashes( $arrValues['delete_value'] ) ) ); elseif( isset( $arrValues['delete_value'] ) ) $this->setDeleteValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['delete_value'] ) : $arrValues['delete_value'] );
		if( isset( $arrValues['default_value'] ) && $boolDirectSet ) $this->set( 'm_strDefaultValue', trim( stripcslashes( $arrValues['default_value'] ) ) ); elseif( isset( $arrValues['default_value'] ) ) $this->setDefaultValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['default_value'] ) : $arrValues['default_value'] );
		if( isset( $arrValues['options'] ) && $boolDirectSet ) $this->set( 'm_strOptions', trim( stripcslashes( $arrValues['options'] ) ) ); elseif( isset( $arrValues['options'] ) ) $this->setOptions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['options'] ) : $arrValues['options'] );
		if( isset( $arrValues['is_negated'] ) && $boolDirectSet ) $this->set( 'm_boolIsNegated', trim( stripcslashes( $arrValues['is_negated'] ) ) ); elseif( isset( $arrValues['is_negated'] ) ) $this->setIsNegated( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_negated'] ) : $arrValues['is_negated'] );
		if( isset( $arrValues['requires_sync'] ) && $boolDirectSet ) $this->set( 'm_boolRequiresSync', trim( stripcslashes( $arrValues['requires_sync'] ) ) ); elseif( isset( $arrValues['requires_sync'] ) ) $this->setRequiresSync( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['requires_sync'] ) : $arrValues['requires_sync'] );
		if( isset( $arrValues['is_implementation_required'] ) && $boolDirectSet ) $this->set( 'm_boolIsImplementationRequired', trim( stripcslashes( $arrValues['is_implementation_required'] ) ) ); elseif( isset( $arrValues['is_implementation_required'] ) ) $this->setIsImplementationRequired( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_implementation_required'] ) : $arrValues['is_implementation_required'] );
		if( isset( $arrValues['is_templatable'] ) && $boolDirectSet ) $this->set( 'm_boolIsTemplatable', trim( stripcslashes( $arrValues['is_templatable'] ) ) ); elseif( isset( $arrValues['is_templatable'] ) ) $this->setIsTemplatable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_templatable'] ) : $arrValues['is_templatable'] );
		if( isset( $arrValues['is_default_copy'] ) && $boolDirectSet ) $this->set( 'm_boolIsDefaultCopy', trim( stripcslashes( $arrValues['is_default_copy'] ) ) ); elseif( isset( $arrValues['is_default_copy'] ) ) $this->setIsDefaultCopy( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_default_copy'] ) : $arrValues['is_default_copy'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['can_mark_empty_completed'] ) && $boolDirectSet ) $this->set( 'm_boolCanMarkEmptyCompleted', trim( stripcslashes( $arrValues['can_mark_empty_completed'] ) ) ); elseif( isset( $arrValues['can_mark_empty_completed'] ) ) $this->setCanMarkEmptyCompleted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['can_mark_empty_completed'] ) : $arrValues['can_mark_empty_completed'] );
		if( isset( $arrValues['is_bulk_editable'] ) && $boolDirectSet ) $this->set( 'm_boolIsBulkEditable', trim( stripcslashes( $arrValues['is_bulk_editable'] ) ) ); elseif( isset( $arrValues['is_bulk_editable'] ) ) $this->setIsBulkEditable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_bulk_editable'] ) : $arrValues['is_bulk_editable'] );
		if( isset( $arrValues['is_client_specific'] ) && $boolDirectSet ) $this->set( 'm_boolIsClientSpecific', trim( stripcslashes( $arrValues['is_client_specific'] ) ) ); elseif( isset( $arrValues['is_client_specific'] ) ) $this->setIsClientSpecific( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_client_specific'] ) : $arrValues['is_client_specific'] );
		if( isset( $arrValues['implementation_description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strImplementationDescription', trim( stripcslashes( $arrValues['implementation_description'] ) ) ); elseif( isset( $arrValues['implementation_description'] ) ) $this->setImplementationDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['implementation_description'] ) : $arrValues['implementation_description'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_translated'] ) && $boolDirectSet ) $this->set( 'm_boolIsTranslated', trim( stripcslashes( $arrValues['is_translated'] ) ) ); elseif( isset( $arrValues['is_translated'] ) ) $this->setIsTranslated( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_translated'] ) : $arrValues['is_translated'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPropertySettingGroupId( $intPropertySettingGroupId ) {
		$this->set( 'm_intPropertySettingGroupId', CStrings::strToIntDef( $intPropertySettingGroupId, NULL, false ) );
	}

	public function getPropertySettingGroupId() {
		return $this->m_intPropertySettingGroupId;
	}

	public function sqlPropertySettingGroupId() {
		return ( true == isset( $this->m_intPropertySettingGroupId ) ) ? ( string ) $this->m_intPropertySettingGroupId : 'NULL';
	}

	public function setDatabaseTypeId( $intDatabaseTypeId ) {
		$this->set( 'm_intDatabaseTypeId', CStrings::strToIntDef( $intDatabaseTypeId, NULL, false ) );
	}

	public function getDatabaseTypeId() {
		return $this->m_intDatabaseTypeId;
	}

	public function sqlDatabaseTypeId() {
		return ( true == isset( $this->m_intDatabaseTypeId ) ) ? ( string ) $this->m_intDatabaseTypeId : '0';
	}

	public function setToolTipId( $intToolTipId ) {
		$this->set( 'm_intToolTipId', CStrings::strToIntDef( $intToolTipId, NULL, false ) );
	}

	public function getToolTipId() {
		return $this->m_intToolTipId;
	}

	public function sqlToolTipId() {
		return ( true == isset( $this->m_intToolTipId ) ) ? ( string ) $this->m_intToolTipId : 'NULL';
	}

	public function setKey( $strKey ) {
		$this->set( 'm_strKey', CStrings::strTrimDef( $strKey, 80, NULL, true ) );
	}

	public function getKey() {
		return $this->m_strKey;
	}

	public function sqlKey() {
		return ( true == isset( $this->m_strKey ) ) ? '\'' . addslashes( $this->m_strKey ) . '\'' : 'NULL';
	}

	public function setOldLabel( $strOldLabel ) {
		$this->set( 'm_strOldLabel', CStrings::strTrimDef( $strOldLabel, 240, NULL, true ) );
	}

	public function getOldLabel() {
		return $this->m_strOldLabel;
	}

	public function sqlOldLabel() {
		return ( true == isset( $this->m_strOldLabel ) ) ? '\'' . addslashes( $this->m_strOldLabel ) . '\'' : 'NULL';
	}

	public function setLabel( $strLabel, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strLabel', CStrings::strTrimDef( $strLabel, 240, NULL, true ), $strLocaleCode );
	}

	public function getLabel( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strLabel', $strLocaleCode );
	}

	public function sqlLabel() {
		return ( true == isset( $this->m_strLabel ) ) ? '\'' . addslashes( $this->m_strLabel ) . '\'' : 'NULL';
	}

	public function setTableName( $strTableName ) {
		$this->set( 'm_strTableName', CStrings::strTrimDef( $strTableName, 255, NULL, true ) );
	}

	public function getTableName() {
		return $this->m_strTableName;
	}

	public function sqlTableName() {
		return ( true == isset( $this->m_strTableName ) ) ? '\'' . addslashes( $this->m_strTableName ) . '\'' : 'NULL';
	}

	public function setColumnName( $strColumnName ) {
		$this->set( 'm_strColumnName', CStrings::strTrimDef( $strColumnName, 255, NULL, true ) );
	}

	public function getColumnName() {
		return $this->m_strColumnName;
	}

	public function sqlColumnName() {
		return ( true == isset( $this->m_strColumnName ) ) ? '\'' . addslashes( $this->m_strColumnName ) . '\'' : 'NULL';
	}

	public function setDeleteValue( $strDeleteValue ) {
		$this->set( 'm_strDeleteValue', CStrings::strTrimDef( $strDeleteValue, -1, NULL, true ) );
	}

	public function getDeleteValue() {
		return $this->m_strDeleteValue;
	}

	public function sqlDeleteValue() {
		return ( true == isset( $this->m_strDeleteValue ) ) ? '\'' . addslashes( $this->m_strDeleteValue ) . '\'' : 'NULL';
	}

	public function setDefaultValue( $strDefaultValue ) {
		$this->set( 'm_strDefaultValue', CStrings::strTrimDef( $strDefaultValue, -1, NULL, true ) );
	}

	public function getDefaultValue() {
		return $this->m_strDefaultValue;
	}

	public function sqlDefaultValue() {
		return ( true == isset( $this->m_strDefaultValue ) ) ? '\'' . addslashes( $this->m_strDefaultValue ) . '\'' : 'NULL';
	}

	public function setOptions( $strOptions ) {
		$this->set( 'm_strOptions', CStrings::strTrimDef( $strOptions, -1, NULL, true ) );
	}

	public function getOptions() {
		return $this->m_strOptions;
	}

	public function sqlOptions() {
		return ( true == isset( $this->m_strOptions ) ) ? '\'' . addslashes( $this->m_strOptions ) . '\'' : 'NULL';
	}

	public function setIsNegated( $boolIsNegated ) {
		$this->set( 'm_boolIsNegated', CStrings::strToBool( $boolIsNegated ) );
	}

	public function getIsNegated() {
		return $this->m_boolIsNegated;
	}

	public function sqlIsNegated() {
		return ( true == isset( $this->m_boolIsNegated ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsNegated ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequiresSync( $boolRequiresSync ) {
		$this->set( 'm_boolRequiresSync', CStrings::strToBool( $boolRequiresSync ) );
	}

	public function getRequiresSync() {
		return $this->m_boolRequiresSync;
	}

	public function sqlRequiresSync() {
		return ( true == isset( $this->m_boolRequiresSync ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequiresSync ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsImplementationRequired( $boolIsImplementationRequired ) {
		$this->set( 'm_boolIsImplementationRequired', CStrings::strToBool( $boolIsImplementationRequired ) );
	}

	public function getIsImplementationRequired() {
		return $this->m_boolIsImplementationRequired;
	}

	public function sqlIsImplementationRequired() {
		return ( true == isset( $this->m_boolIsImplementationRequired ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsImplementationRequired ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTemplatable( $boolIsTemplatable ) {
		$this->set( 'm_boolIsTemplatable', CStrings::strToBool( $boolIsTemplatable ) );
	}

	public function getIsTemplatable() {
		return $this->m_boolIsTemplatable;
	}

	public function sqlIsTemplatable() {
		return ( true == isset( $this->m_boolIsTemplatable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTemplatable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDefaultCopy( $boolIsDefaultCopy ) {
		$this->set( 'm_boolIsDefaultCopy', CStrings::strToBool( $boolIsDefaultCopy ) );
	}

	public function getIsDefaultCopy() {
		return $this->m_boolIsDefaultCopy;
	}

	public function sqlIsDefaultCopy() {
		return ( true == isset( $this->m_boolIsDefaultCopy ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDefaultCopy ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCanMarkEmptyCompleted( $boolCanMarkEmptyCompleted ) {
		$this->set( 'm_boolCanMarkEmptyCompleted', CStrings::strToBool( $boolCanMarkEmptyCompleted ) );
	}

	public function getCanMarkEmptyCompleted() {
		return $this->m_boolCanMarkEmptyCompleted;
	}

	public function sqlCanMarkEmptyCompleted() {
		return ( true == isset( $this->m_boolCanMarkEmptyCompleted ) ) ? '\'' . ( true == ( bool ) $this->m_boolCanMarkEmptyCompleted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsBulkEditable( $boolIsBulkEditable ) {
		$this->set( 'm_boolIsBulkEditable', CStrings::strToBool( $boolIsBulkEditable ) );
	}

	public function getIsBulkEditable() {
		return $this->m_boolIsBulkEditable;
	}

	public function sqlIsBulkEditable() {
		return ( true == isset( $this->m_boolIsBulkEditable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsBulkEditable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsClientSpecific( $boolIsClientSpecific ) {
		$this->set( 'm_boolIsClientSpecific', CStrings::strToBool( $boolIsClientSpecific ) );
	}

	public function getIsClientSpecific() {
		return $this->m_boolIsClientSpecific;
	}

	public function sqlIsClientSpecific() {
		return ( true == isset( $this->m_boolIsClientSpecific ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsClientSpecific ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setImplementationDescription( $strImplementationDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strImplementationDescription', CStrings::strTrimDef( $strImplementationDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getImplementationDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strImplementationDescription', $strLocaleCode );
	}

	public function sqlImplementationDescription() {
		return ( true == isset( $this->m_strImplementationDescription ) ) ? '\'' . addslashes( $this->m_strImplementationDescription ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsTranslated( $boolIsTranslated ) {
		$this->set( 'm_boolIsTranslated', CStrings::strToBool( $boolIsTranslated ) );
	}

	public function getIsTranslated() {
		return $this->m_boolIsTranslated;
	}

	public function sqlIsTranslated() {
		return ( true == isset( $this->m_boolIsTranslated ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTranslated ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, property_setting_group_id, database_type_id, tool_tip_id, key, old_label, label, table_name, column_name, delete_value, default_value, options, is_negated, requires_sync, is_implementation_required, is_templatable, is_default_copy, is_disabled, can_mark_empty_completed, is_bulk_editable, is_client_specific, implementation_description, updated_by, updated_on, created_by, created_on, is_translated, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlPropertySettingGroupId() . ', ' .
						$this->sqlDatabaseTypeId() . ', ' .
						$this->sqlToolTipId() . ', ' .
						$this->sqlKey() . ', ' .
						$this->sqlOldLabel() . ', ' .
						$this->sqlLabel() . ', ' .
						$this->sqlTableName() . ', ' .
						$this->sqlColumnName() . ', ' .
						$this->sqlDeleteValue() . ', ' .
						$this->sqlDefaultValue() . ', ' .
						$this->sqlOptions() . ', ' .
						$this->sqlIsNegated() . ', ' .
						$this->sqlRequiresSync() . ', ' .
						$this->sqlIsImplementationRequired() . ', ' .
						$this->sqlIsTemplatable() . ', ' .
						$this->sqlIsDefaultCopy() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						$this->sqlCanMarkEmptyCompleted() . ', ' .
						$this->sqlIsBulkEditable() . ', ' .
						$this->sqlIsClientSpecific() . ', ' .
						$this->sqlImplementationDescription() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsTranslated() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_setting_group_id = ' . $this->sqlPropertySettingGroupId(). ',' ; } elseif( true == array_key_exists( 'PropertySettingGroupId', $this->getChangedColumns() ) ) { $strSql .= ' property_setting_group_id = ' . $this->sqlPropertySettingGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_type_id = ' . $this->sqlDatabaseTypeId(). ',' ; } elseif( true == array_key_exists( 'DatabaseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' database_type_id = ' . $this->sqlDatabaseTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tool_tip_id = ' . $this->sqlToolTipId(). ',' ; } elseif( true == array_key_exists( 'ToolTipId', $this->getChangedColumns() ) ) { $strSql .= ' tool_tip_id = ' . $this->sqlToolTipId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' key = ' . $this->sqlKey(). ',' ; } elseif( true == array_key_exists( 'Key', $this->getChangedColumns() ) ) { $strSql .= ' key = ' . $this->sqlKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_label = ' . $this->sqlOldLabel(). ',' ; } elseif( true == array_key_exists( 'OldLabel', $this->getChangedColumns() ) ) { $strSql .= ' old_label = ' . $this->sqlOldLabel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' label = ' . $this->sqlLabel(). ',' ; } elseif( true == array_key_exists( 'Label', $this->getChangedColumns() ) ) { $strSql .= ' label = ' . $this->sqlLabel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' table_name = ' . $this->sqlTableName(). ',' ; } elseif( true == array_key_exists( 'TableName', $this->getChangedColumns() ) ) { $strSql .= ' table_name = ' . $this->sqlTableName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' column_name = ' . $this->sqlColumnName(). ',' ; } elseif( true == array_key_exists( 'ColumnName', $this->getChangedColumns() ) ) { $strSql .= ' column_name = ' . $this->sqlColumnName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delete_value = ' . $this->sqlDeleteValue(). ',' ; } elseif( true == array_key_exists( 'DeleteValue', $this->getChangedColumns() ) ) { $strSql .= ' delete_value = ' . $this->sqlDeleteValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_value = ' . $this->sqlDefaultValue(). ',' ; } elseif( true == array_key_exists( 'DefaultValue', $this->getChangedColumns() ) ) { $strSql .= ' default_value = ' . $this->sqlDefaultValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' options = ' . $this->sqlOptions(). ',' ; } elseif( true == array_key_exists( 'Options', $this->getChangedColumns() ) ) { $strSql .= ' options = ' . $this->sqlOptions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_negated = ' . $this->sqlIsNegated(). ',' ; } elseif( true == array_key_exists( 'IsNegated', $this->getChangedColumns() ) ) { $strSql .= ' is_negated = ' . $this->sqlIsNegated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requires_sync = ' . $this->sqlRequiresSync(). ',' ; } elseif( true == array_key_exists( 'RequiresSync', $this->getChangedColumns() ) ) { $strSql .= ' requires_sync = ' . $this->sqlRequiresSync() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_implementation_required = ' . $this->sqlIsImplementationRequired(). ',' ; } elseif( true == array_key_exists( 'IsImplementationRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_implementation_required = ' . $this->sqlIsImplementationRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_templatable = ' . $this->sqlIsTemplatable(). ',' ; } elseif( true == array_key_exists( 'IsTemplatable', $this->getChangedColumns() ) ) { $strSql .= ' is_templatable = ' . $this->sqlIsTemplatable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default_copy = ' . $this->sqlIsDefaultCopy(). ',' ; } elseif( true == array_key_exists( 'IsDefaultCopy', $this->getChangedColumns() ) ) { $strSql .= ' is_default_copy = ' . $this->sqlIsDefaultCopy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' can_mark_empty_completed = ' . $this->sqlCanMarkEmptyCompleted(). ',' ; } elseif( true == array_key_exists( 'CanMarkEmptyCompleted', $this->getChangedColumns() ) ) { $strSql .= ' can_mark_empty_completed = ' . $this->sqlCanMarkEmptyCompleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_bulk_editable = ' . $this->sqlIsBulkEditable(). ',' ; } elseif( true == array_key_exists( 'IsBulkEditable', $this->getChangedColumns() ) ) { $strSql .= ' is_bulk_editable = ' . $this->sqlIsBulkEditable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_client_specific = ' . $this->sqlIsClientSpecific(). ',' ; } elseif( true == array_key_exists( 'IsClientSpecific', $this->getChangedColumns() ) ) { $strSql .= ' is_client_specific = ' . $this->sqlIsClientSpecific() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_description = ' . $this->sqlImplementationDescription(). ',' ; } elseif( true == array_key_exists( 'ImplementationDescription', $this->getChangedColumns() ) ) { $strSql .= ' implementation_description = ' . $this->sqlImplementationDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_translated = ' . $this->sqlIsTranslated(). ',' ; } elseif( true == array_key_exists( 'IsTranslated', $this->getChangedColumns() ) ) { $strSql .= ' is_translated = ' . $this->sqlIsTranslated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'property_setting_group_id' => $this->getPropertySettingGroupId(),
			'database_type_id' => $this->getDatabaseTypeId(),
			'tool_tip_id' => $this->getToolTipId(),
			'key' => $this->getKey(),
			'old_label' => $this->getOldLabel(),
			'label' => $this->getLabel(),
			'table_name' => $this->getTableName(),
			'column_name' => $this->getColumnName(),
			'delete_value' => $this->getDeleteValue(),
			'default_value' => $this->getDefaultValue(),
			'options' => $this->getOptions(),
			'is_negated' => $this->getIsNegated(),
			'requires_sync' => $this->getRequiresSync(),
			'is_implementation_required' => $this->getIsImplementationRequired(),
			'is_templatable' => $this->getIsTemplatable(),
			'is_default_copy' => $this->getIsDefaultCopy(),
			'is_disabled' => $this->getIsDisabled(),
			'can_mark_empty_completed' => $this->getCanMarkEmptyCompleted(),
			'is_bulk_editable' => $this->getIsBulkEditable(),
			'is_client_specific' => $this->getIsClientSpecific(),
			'implementation_description' => $this->getImplementationDescription(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_translated' => $this->getIsTranslated(),
			'details' => $this->getDetails()
		);
	}

}
?>