<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CContactTypes
 * Do not add any new functions to this class.
 */

class CBaseContactTypes extends CEosPluralBase {

	/**
	 * @return CContactType[]
	 */
	public static function fetchContactTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CContactType', $objDatabase );
	}

	/**
	 * @return CContactType
	 */
	public static function fetchContactType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CContactType', $objDatabase );
	}

	public static function fetchContactTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'contact_types', $objDatabase );
	}

	public static function fetchContactTypeById( $intId, $objDatabase ) {
		return self::fetchContactType( sprintf( 'SELECT * FROM contact_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>