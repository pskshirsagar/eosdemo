<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CMilitaryComponents
 * Do not add any new functions to this class.
 */

class CBaseMilitaryComponents extends CEosPluralBase {

	/**
	 * @return CMilitaryComponent[]
	 */
	public static function fetchMilitaryComponents( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMilitaryComponent::class, $objDatabase );
	}

	/**
	 * @return CMilitaryComponent
	 */
	public static function fetchMilitaryComponent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMilitaryComponent::class, $objDatabase );
	}

	public static function fetchMilitaryComponentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'military_components', $objDatabase );
	}

	public static function fetchMilitaryComponentById( $intId, $objDatabase ) {
		return self::fetchMilitaryComponent( sprintf( 'SELECT * FROM military_components WHERE id = %d', $intId ), $objDatabase );
	}

}
?>