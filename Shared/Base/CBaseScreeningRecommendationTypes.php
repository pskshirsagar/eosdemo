<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningRecommendationTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningRecommendationTypes extends CEosPluralBase {

	/**
	 * @return CScreeningRecommendationType[]
	 */
	public static function fetchScreeningRecommendationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningRecommendationType::class, $objDatabase );
	}

	/**
	 * @return CScreeningRecommendationType
	 */
	public static function fetchScreeningRecommendationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningRecommendationType::class, $objDatabase );
	}

	public static function fetchScreeningRecommendationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_recommendation_types', $objDatabase );
	}

	public static function fetchScreeningRecommendationTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningRecommendationType( sprintf( 'SELECT * FROM screening_recommendation_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>