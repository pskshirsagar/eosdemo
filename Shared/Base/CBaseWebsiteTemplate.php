<?php

class CBaseWebsiteTemplate extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.website_templates';

	protected $m_intId;
	protected $m_strPrivateCompanyId;
	protected $m_intWebsiteTemplateTypeId;
	protected $m_strName;
	protected $m_strPath;
	protected $m_strDescription;
	protected $m_strFeatures;
	protected $m_strTemplateColors;
	protected $m_strTemplateFonts;
	protected $m_strTemplateThemes;
	protected $m_strTemplatePatterns;
	protected $m_strTemplateSpecificPreferences;
	protected $m_intNavBarCount;
	protected $m_intIsSingular;
	protected $m_boolIsCustom;
	protected $m_intIsResponsive;
	protected $m_intIsPublished;
	protected $m_intIsNotAvailable;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strGoogleOptimizeId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intWebsiteTemplateStatusTypeId;
	protected $m_strDeprecatedOn;
	protected $m_boolIsNew;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intWebsiteTemplateTypeId = '1';
		$this->m_intNavBarCount = '1';
		$this->m_boolIsCustom = false;
		$this->m_intIsResponsive = '0';
		$this->m_intIsPublished = '1';
		$this->m_intIsNotAvailable = '0';
		$this->m_intWebsiteTemplateStatusTypeId = '1';
		$this->m_boolIsNew = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['private_company_id'] ) && $boolDirectSet ) $this->set( 'm_strPrivateCompanyId', trim( $arrValues['private_company_id'] ) ); elseif( isset( $arrValues['private_company_id'] ) ) $this->setPrivateCompanyId( $arrValues['private_company_id'] );
		if( isset( $arrValues['website_template_type_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteTemplateTypeId', trim( $arrValues['website_template_type_id'] ) ); elseif( isset( $arrValues['website_template_type_id'] ) ) $this->setWebsiteTemplateTypeId( $arrValues['website_template_type_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['path'] ) && $boolDirectSet ) $this->set( 'm_strPath', trim( $arrValues['path'] ) ); elseif( isset( $arrValues['path'] ) ) $this->setPath( $arrValues['path'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['features'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strFeatures', trim( $arrValues['features'] ) ); elseif( isset( $arrValues['features'] ) ) $this->setFeatures( $arrValues['features'] );
		if( isset( $arrValues['template_colors'] ) && $boolDirectSet ) $this->set( 'm_strTemplateColors', trim( $arrValues['template_colors'] ) ); elseif( isset( $arrValues['template_colors'] ) ) $this->setTemplateColors( $arrValues['template_colors'] );
		if( isset( $arrValues['template_fonts'] ) && $boolDirectSet ) $this->set( 'm_strTemplateFonts', trim( $arrValues['template_fonts'] ) ); elseif( isset( $arrValues['template_fonts'] ) ) $this->setTemplateFonts( $arrValues['template_fonts'] );
		if( isset( $arrValues['template_themes'] ) && $boolDirectSet ) $this->set( 'm_strTemplateThemes', trim( $arrValues['template_themes'] ) ); elseif( isset( $arrValues['template_themes'] ) ) $this->setTemplateThemes( $arrValues['template_themes'] );
		if( isset( $arrValues['template_patterns'] ) && $boolDirectSet ) $this->set( 'm_strTemplatePatterns', trim( $arrValues['template_patterns'] ) ); elseif( isset( $arrValues['template_patterns'] ) ) $this->setTemplatePatterns( $arrValues['template_patterns'] );
		if( isset( $arrValues['template_specific_preferences'] ) && $boolDirectSet ) $this->set( 'm_strTemplateSpecificPreferences', trim( $arrValues['template_specific_preferences'] ) ); elseif( isset( $arrValues['template_specific_preferences'] ) ) $this->setTemplateSpecificPreferences( $arrValues['template_specific_preferences'] );
		if( isset( $arrValues['nav_bar_count'] ) && $boolDirectSet ) $this->set( 'm_intNavBarCount', trim( $arrValues['nav_bar_count'] ) ); elseif( isset( $arrValues['nav_bar_count'] ) ) $this->setNavBarCount( $arrValues['nav_bar_count'] );
		if( isset( $arrValues['is_singular'] ) && $boolDirectSet ) $this->set( 'm_intIsSingular', trim( $arrValues['is_singular'] ) ); elseif( isset( $arrValues['is_singular'] ) ) $this->setIsSingular( $arrValues['is_singular'] );
		if( isset( $arrValues['is_custom'] ) && $boolDirectSet ) $this->set( 'm_boolIsCustom', trim( stripcslashes( $arrValues['is_custom'] ) ) ); elseif( isset( $arrValues['is_custom'] ) ) $this->setIsCustom( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_custom'] ) : $arrValues['is_custom'] );
		if( isset( $arrValues['is_responsive'] ) && $boolDirectSet ) $this->set( 'm_intIsResponsive', trim( $arrValues['is_responsive'] ) ); elseif( isset( $arrValues['is_responsive'] ) ) $this->setIsResponsive( $arrValues['is_responsive'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_not_available'] ) && $boolDirectSet ) $this->set( 'm_intIsNotAvailable', trim( $arrValues['is_not_available'] ) ); elseif( isset( $arrValues['is_not_available'] ) ) $this->setIsNotAvailable( $arrValues['is_not_available'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['google_optimize_id'] ) && $boolDirectSet ) $this->set( 'm_strGoogleOptimizeId', trim( $arrValues['google_optimize_id'] ) ); elseif( isset( $arrValues['google_optimize_id'] ) ) $this->setGoogleOptimizeId( $arrValues['google_optimize_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['website_template_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteTemplateStatusTypeId', trim( $arrValues['website_template_status_type_id'] ) ); elseif( isset( $arrValues['website_template_status_type_id'] ) ) $this->setWebsiteTemplateStatusTypeId( $arrValues['website_template_status_type_id'] );
		if( isset( $arrValues['deprecated_on'] ) && $boolDirectSet ) $this->set( 'm_strDeprecatedOn', trim( $arrValues['deprecated_on'] ) ); elseif( isset( $arrValues['deprecated_on'] ) ) $this->setDeprecatedOn( $arrValues['deprecated_on'] );
		if( isset( $arrValues['is_new'] ) && $boolDirectSet ) $this->set( 'm_boolIsNew', trim( stripcslashes( $arrValues['is_new'] ) ) ); elseif( isset( $arrValues['is_new'] ) ) $this->setIsNew( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_new'] ) : $arrValues['is_new'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPrivateCompanyId( $strPrivateCompanyId ) {
		$this->set( 'm_strPrivateCompanyId', CStrings::strTrimDef( $strPrivateCompanyId, -1, NULL, true ) );
	}

	public function getPrivateCompanyId() {
		return $this->m_strPrivateCompanyId;
	}

	public function sqlPrivateCompanyId() {
		return ( true == isset( $this->m_strPrivateCompanyId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrivateCompanyId ) : '\'' . addslashes( $this->m_strPrivateCompanyId ) . '\'' ) : 'NULL';
	}

	public function setWebsiteTemplateTypeId( $intWebsiteTemplateTypeId ) {
		$this->set( 'm_intWebsiteTemplateTypeId', CStrings::strToIntDef( $intWebsiteTemplateTypeId, NULL, false ) );
	}

	public function getWebsiteTemplateTypeId() {
		return $this->m_intWebsiteTemplateTypeId;
	}

	public function sqlWebsiteTemplateTypeId() {
		return ( true == isset( $this->m_intWebsiteTemplateTypeId ) ) ? ( string ) $this->m_intWebsiteTemplateTypeId : '1';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setPath( $strPath ) {
		$this->set( 'm_strPath', CStrings::strTrimDef( $strPath, 4096, NULL, true ) );
	}

	public function getPath() {
		return $this->m_strPath;
	}

	public function sqlPath() {
		return ( true == isset( $this->m_strPath ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPath ) : '\'' . addslashes( $this->m_strPath ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setFeatures( $strFeatures, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strFeatures', CStrings::strTrimDef( $strFeatures, -1, NULL, true ), $strLocaleCode );
	}

	public function getFeatures( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strFeatures', $strLocaleCode );
	}

	public function sqlFeatures() {
		return ( true == isset( $this->m_strFeatures ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFeatures ) : '\'' . addslashes( $this->m_strFeatures ) . '\'' ) : 'NULL';
	}

	public function setTemplateColors( $strTemplateColors ) {
		$this->set( 'm_strTemplateColors', CStrings::strTrimDef( $strTemplateColors, 2000, NULL, true ) );
	}

	public function getTemplateColors() {
		return $this->m_strTemplateColors;
	}

	public function sqlTemplateColors() {
		return ( true == isset( $this->m_strTemplateColors ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTemplateColors ) : '\'' . addslashes( $this->m_strTemplateColors ) . '\'' ) : 'NULL';
	}

	public function setTemplateFonts( $strTemplateFonts ) {
		$this->set( 'm_strTemplateFonts', CStrings::strTrimDef( $strTemplateFonts, -1, NULL, true ) );
	}

	public function getTemplateFonts() {
		return $this->m_strTemplateFonts;
	}

	public function sqlTemplateFonts() {
		return ( true == isset( $this->m_strTemplateFonts ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTemplateFonts ) : '\'' . addslashes( $this->m_strTemplateFonts ) . '\'' ) : 'NULL';
	}

	public function setTemplateThemes( $strTemplateThemes ) {
		$this->set( 'm_strTemplateThemes', CStrings::strTrimDef( $strTemplateThemes, -1, NULL, true ) );
	}

	public function getTemplateThemes() {
		return $this->m_strTemplateThemes;
	}

	public function sqlTemplateThemes() {
		return ( true == isset( $this->m_strTemplateThemes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTemplateThemes ) : '\'' . addslashes( $this->m_strTemplateThemes ) . '\'' ) : 'NULL';
	}

	public function setTemplatePatterns( $strTemplatePatterns ) {
		$this->set( 'm_strTemplatePatterns', CStrings::strTrimDef( $strTemplatePatterns, -1, NULL, true ) );
	}

	public function getTemplatePatterns() {
		return $this->m_strTemplatePatterns;
	}

	public function sqlTemplatePatterns() {
		return ( true == isset( $this->m_strTemplatePatterns ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTemplatePatterns ) : '\'' . addslashes( $this->m_strTemplatePatterns ) . '\'' ) : 'NULL';
	}

	public function setTemplateSpecificPreferences( $strTemplateSpecificPreferences ) {
		$this->set( 'm_strTemplateSpecificPreferences', CStrings::strTrimDef( $strTemplateSpecificPreferences, -1, NULL, true ) );
	}

	public function getTemplateSpecificPreferences() {
		return $this->m_strTemplateSpecificPreferences;
	}

	public function sqlTemplateSpecificPreferences() {
		return ( true == isset( $this->m_strTemplateSpecificPreferences ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTemplateSpecificPreferences ) : '\'' . addslashes( $this->m_strTemplateSpecificPreferences ) . '\'' ) : 'NULL';
	}

	public function setNavBarCount( $intNavBarCount ) {
		$this->set( 'm_intNavBarCount', CStrings::strToIntDef( $intNavBarCount, NULL, false ) );
	}

	public function getNavBarCount() {
		return $this->m_intNavBarCount;
	}

	public function sqlNavBarCount() {
		return ( true == isset( $this->m_intNavBarCount ) ) ? ( string ) $this->m_intNavBarCount : '1';
	}

	public function setIsSingular( $intIsSingular ) {
		$this->set( 'm_intIsSingular', CStrings::strToIntDef( $intIsSingular, NULL, false ) );
	}

	public function getIsSingular() {
		return $this->m_intIsSingular;
	}

	public function sqlIsSingular() {
		return ( true == isset( $this->m_intIsSingular ) ) ? ( string ) $this->m_intIsSingular : 'NULL';
	}

	public function setIsCustom( $boolIsCustom ) {
		$this->set( 'm_boolIsCustom', CStrings::strToBool( $boolIsCustom ) );
	}

	public function getIsCustom() {
		return $this->m_boolIsCustom;
	}

	public function sqlIsCustom() {
		return ( true == isset( $this->m_boolIsCustom ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCustom ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsResponsive( $intIsResponsive ) {
		$this->set( 'm_intIsResponsive', CStrings::strToIntDef( $intIsResponsive, NULL, false ) );
	}

	public function getIsResponsive() {
		return $this->m_intIsResponsive;
	}

	public function sqlIsResponsive() {
		return ( true == isset( $this->m_intIsResponsive ) ) ? ( string ) $this->m_intIsResponsive : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsNotAvailable( $intIsNotAvailable ) {
		$this->set( 'm_intIsNotAvailable', CStrings::strToIntDef( $intIsNotAvailable, NULL, false ) );
	}

	public function getIsNotAvailable() {
		return $this->m_intIsNotAvailable;
	}

	public function sqlIsNotAvailable() {
		return ( true == isset( $this->m_intIsNotAvailable ) ) ? ( string ) $this->m_intIsNotAvailable : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setGoogleOptimizeId( $strGoogleOptimizeId ) {
		$this->set( 'm_strGoogleOptimizeId', CStrings::strTrimDef( $strGoogleOptimizeId, 25, NULL, true ) );
	}

	public function getGoogleOptimizeId() {
		return $this->m_strGoogleOptimizeId;
	}

	public function sqlGoogleOptimizeId() {
		return ( true == isset( $this->m_strGoogleOptimizeId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strGoogleOptimizeId ) : '\'' . addslashes( $this->m_strGoogleOptimizeId ) . '\'' ) : 'NULL';
	}

	public function setWebsiteTemplateStatusTypeId( $intWebsiteTemplateStatusTypeId ) {
		$this->set( 'm_intWebsiteTemplateStatusTypeId', CStrings::strToIntDef( $intWebsiteTemplateStatusTypeId, NULL, false ) );
	}

	public function getWebsiteTemplateStatusTypeId() {
		return $this->m_intWebsiteTemplateStatusTypeId;
	}

	public function sqlWebsiteTemplateStatusTypeId() {
		return ( true == isset( $this->m_intWebsiteTemplateStatusTypeId ) ) ? ( string ) $this->m_intWebsiteTemplateStatusTypeId : '1';
	}

	public function setDeprecatedOn( $strDeprecatedOn ) {
		$this->set( 'm_strDeprecatedOn', CStrings::strTrimDef( $strDeprecatedOn, -1, NULL, true ) );
	}

	public function getDeprecatedOn() {
		return $this->m_strDeprecatedOn;
	}

	public function sqlDeprecatedOn() {
		return ( true == isset( $this->m_strDeprecatedOn ) ) ? '\'' . $this->m_strDeprecatedOn . '\'' : 'NULL';
	}

	public function setIsNew( $boolIsNew ) {
		$this->set( 'm_boolIsNew', CStrings::strToBool( $boolIsNew ) );
	}

	public function getIsNew() {
		return $this->m_boolIsNew;
	}

	public function sqlIsNew() {
		return ( true == isset( $this->m_boolIsNew ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsNew ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, private_company_id, website_template_type_id, name, path, description, features, template_colors, template_fonts, template_themes, template_patterns, template_specific_preferences, nav_bar_count, is_singular, is_custom, is_responsive, is_published, is_not_available, updated_by, updated_on, created_by, created_on, google_optimize_id, details, website_template_status_type_id, deprecated_on, is_new )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlPrivateCompanyId() . ', ' .
		          $this->sqlWebsiteTemplateTypeId() . ', ' .
		          $this->sqlName() . ', ' .
		          $this->sqlPath() . ', ' .
		          $this->sqlDescription() . ', ' .
		          $this->sqlFeatures() . ', ' .
		          $this->sqlTemplateColors() . ', ' .
		          $this->sqlTemplateFonts() . ', ' .
		          $this->sqlTemplateThemes() . ', ' .
		          $this->sqlTemplatePatterns() . ', ' .
		          $this->sqlTemplateSpecificPreferences() . ', ' .
		          $this->sqlNavBarCount() . ', ' .
		          $this->sqlIsSingular() . ', ' .
		          $this->sqlIsCustom() . ', ' .
		          $this->sqlIsResponsive() . ', ' .
		          $this->sqlIsPublished() . ', ' .
		          $this->sqlIsNotAvailable() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlGoogleOptimizeId() . ', ' .
		          $this->sqlDetails() . ', ' .
		          $this->sqlWebsiteTemplateStatusTypeId() . ', ' .
		          $this->sqlDeprecatedOn() . ', ' .
		          $this->sqlIsNew() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' private_company_id = ' . $this->sqlPrivateCompanyId(). ',' ; } elseif( true == array_key_exists( 'PrivateCompanyId', $this->getChangedColumns() ) ) { $strSql .= ' private_company_id = ' . $this->sqlPrivateCompanyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_template_type_id = ' . $this->sqlWebsiteTemplateTypeId(). ',' ; } elseif( true == array_key_exists( 'WebsiteTemplateTypeId', $this->getChangedColumns() ) ) { $strSql .= ' website_template_type_id = ' . $this->sqlWebsiteTemplateTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' path = ' . $this->sqlPath(). ',' ; } elseif( true == array_key_exists( 'Path', $this->getChangedColumns() ) ) { $strSql .= ' path = ' . $this->sqlPath() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' features = ' . $this->sqlFeatures(). ',' ; } elseif( true == array_key_exists( 'Features', $this->getChangedColumns() ) ) { $strSql .= ' features = ' . $this->sqlFeatures() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_colors = ' . $this->sqlTemplateColors(). ',' ; } elseif( true == array_key_exists( 'TemplateColors', $this->getChangedColumns() ) ) { $strSql .= ' template_colors = ' . $this->sqlTemplateColors() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_fonts = ' . $this->sqlTemplateFonts(). ',' ; } elseif( true == array_key_exists( 'TemplateFonts', $this->getChangedColumns() ) ) { $strSql .= ' template_fonts = ' . $this->sqlTemplateFonts() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_themes = ' . $this->sqlTemplateThemes(). ',' ; } elseif( true == array_key_exists( 'TemplateThemes', $this->getChangedColumns() ) ) { $strSql .= ' template_themes = ' . $this->sqlTemplateThemes() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_patterns = ' . $this->sqlTemplatePatterns(). ',' ; } elseif( true == array_key_exists( 'TemplatePatterns', $this->getChangedColumns() ) ) { $strSql .= ' template_patterns = ' . $this->sqlTemplatePatterns() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_specific_preferences = ' . $this->sqlTemplateSpecificPreferences(). ',' ; } elseif( true == array_key_exists( 'TemplateSpecificPreferences', $this->getChangedColumns() ) ) { $strSql .= ' template_specific_preferences = ' . $this->sqlTemplateSpecificPreferences() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nav_bar_count = ' . $this->sqlNavBarCount(). ',' ; } elseif( true == array_key_exists( 'NavBarCount', $this->getChangedColumns() ) ) { $strSql .= ' nav_bar_count = ' . $this->sqlNavBarCount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_singular = ' . $this->sqlIsSingular(). ',' ; } elseif( true == array_key_exists( 'IsSingular', $this->getChangedColumns() ) ) { $strSql .= ' is_singular = ' . $this->sqlIsSingular() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_custom = ' . $this->sqlIsCustom(). ',' ; } elseif( true == array_key_exists( 'IsCustom', $this->getChangedColumns() ) ) { $strSql .= ' is_custom = ' . $this->sqlIsCustom() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_responsive = ' . $this->sqlIsResponsive(). ',' ; } elseif( true == array_key_exists( 'IsResponsive', $this->getChangedColumns() ) ) { $strSql .= ' is_responsive = ' . $this->sqlIsResponsive() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_not_available = ' . $this->sqlIsNotAvailable(). ',' ; } elseif( true == array_key_exists( 'IsNotAvailable', $this->getChangedColumns() ) ) { $strSql .= ' is_not_available = ' . $this->sqlIsNotAvailable() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' google_optimize_id = ' . $this->sqlGoogleOptimizeId(). ',' ; } elseif( true == array_key_exists( 'GoogleOptimizeId', $this->getChangedColumns() ) ) { $strSql .= ' google_optimize_id = ' . $this->sqlGoogleOptimizeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_template_status_type_id = ' . $this->sqlWebsiteTemplateStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'WebsiteTemplateStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' website_template_status_type_id = ' . $this->sqlWebsiteTemplateStatusTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deprecated_on = ' . $this->sqlDeprecatedOn(). ',' ; } elseif( true == array_key_exists( 'DeprecatedOn', $this->getChangedColumns() ) ) { $strSql .= ' deprecated_on = ' . $this->sqlDeprecatedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_new = ' . $this->sqlIsNew(). ',' ; } elseif( true == array_key_exists( 'IsNew', $this->getChangedColumns() ) ) { $strSql .= ' is_new = ' . $this->sqlIsNew() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'private_company_id' => $this->getPrivateCompanyId(),
			'website_template_type_id' => $this->getWebsiteTemplateTypeId(),
			'name' => $this->getName(),
			'path' => $this->getPath(),
			'description' => $this->getDescription(),
			'features' => $this->getFeatures(),
			'template_colors' => $this->getTemplateColors(),
			'template_fonts' => $this->getTemplateFonts(),
			'template_themes' => $this->getTemplateThemes(),
			'template_patterns' => $this->getTemplatePatterns(),
			'template_specific_preferences' => $this->getTemplateSpecificPreferences(),
			'nav_bar_count' => $this->getNavBarCount(),
			'is_singular' => $this->getIsSingular(),
			'is_custom' => $this->getIsCustom(),
			'is_responsive' => $this->getIsResponsive(),
			'is_published' => $this->getIsPublished(),
			'is_not_available' => $this->getIsNotAvailable(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'google_optimize_id' => $this->getGoogleOptimizeId(),
			'details' => $this->getDetails(),
			'website_template_status_type_id' => $this->getWebsiteTemplateStatusTypeId(),
			'deprecated_on' => $this->getDeprecatedOn(),
			'is_new' => $this->getIsNew()
		);
	}

}
?>