<?php

class CBasePropertySettingKeyValue extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.property_setting_key_values';

	protected $m_intId;
	protected $m_intPropertySettingKeyId;
	protected $m_intPropertySettingKeyValueTypeId;
	protected $m_strTableName;
	protected $m_strColumnName;
	protected $m_strPropertySettingValue;
	protected $m_strDisplayText;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['property_setting_key_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertySettingKeyId', trim( $arrValues['property_setting_key_id'] ) ); elseif( isset( $arrValues['property_setting_key_id'] ) ) $this->setPropertySettingKeyId( $arrValues['property_setting_key_id'] );
		if( isset( $arrValues['property_setting_key_value_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertySettingKeyValueTypeId', trim( $arrValues['property_setting_key_value_type_id'] ) ); elseif( isset( $arrValues['property_setting_key_value_type_id'] ) ) $this->setPropertySettingKeyValueTypeId( $arrValues['property_setting_key_value_type_id'] );
		if( isset( $arrValues['table_name'] ) && $boolDirectSet ) $this->set( 'm_strTableName', trim( stripcslashes( $arrValues['table_name'] ) ) ); elseif( isset( $arrValues['table_name'] ) ) $this->setTableName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['table_name'] ) : $arrValues['table_name'] );
		if( isset( $arrValues['column_name'] ) && $boolDirectSet ) $this->set( 'm_strColumnName', trim( stripcslashes( $arrValues['column_name'] ) ) ); elseif( isset( $arrValues['column_name'] ) ) $this->setColumnName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['column_name'] ) : $arrValues['column_name'] );
		if( isset( $arrValues['property_setting_value'] ) && $boolDirectSet ) $this->set( 'm_strPropertySettingValue', trim( stripcslashes( $arrValues['property_setting_value'] ) ) ); elseif( isset( $arrValues['property_setting_value'] ) ) $this->setPropertySettingValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_setting_value'] ) : $arrValues['property_setting_value'] );
		if( isset( $arrValues['display_text'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDisplayText', trim( stripcslashes( $arrValues['display_text'] ) ) ); elseif( isset( $arrValues['display_text'] ) ) $this->setDisplayText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['display_text'] ) : $arrValues['display_text'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPropertySettingKeyId( $intPropertySettingKeyId ) {
		$this->set( 'm_intPropertySettingKeyId', CStrings::strToIntDef( $intPropertySettingKeyId, NULL, false ) );
	}

	public function getPropertySettingKeyId() {
		return $this->m_intPropertySettingKeyId;
	}

	public function sqlPropertySettingKeyId() {
		return ( true == isset( $this->m_intPropertySettingKeyId ) ) ? ( string ) $this->m_intPropertySettingKeyId : 'NULL';
	}

	public function setPropertySettingKeyValueTypeId( $intPropertySettingKeyValueTypeId ) {
		$this->set( 'm_intPropertySettingKeyValueTypeId', CStrings::strToIntDef( $intPropertySettingKeyValueTypeId, NULL, false ) );
	}

	public function getPropertySettingKeyValueTypeId() {
		return $this->m_intPropertySettingKeyValueTypeId;
	}

	public function sqlPropertySettingKeyValueTypeId() {
		return ( true == isset( $this->m_intPropertySettingKeyValueTypeId ) ) ? ( string ) $this->m_intPropertySettingKeyValueTypeId : 'NULL';
	}

	public function setTableName( $strTableName ) {
		$this->set( 'm_strTableName', CStrings::strTrimDef( $strTableName, 255, NULL, true ) );
	}

	public function getTableName() {
		return $this->m_strTableName;
	}

	public function sqlTableName() {
		return ( true == isset( $this->m_strTableName ) ) ? '\'' . addslashes( $this->m_strTableName ) . '\'' : 'NULL';
	}

	public function setColumnName( $strColumnName ) {
		$this->set( 'm_strColumnName', CStrings::strTrimDef( $strColumnName, 255, NULL, true ) );
	}

	public function getColumnName() {
		return $this->m_strColumnName;
	}

	public function sqlColumnName() {
		return ( true == isset( $this->m_strColumnName ) ) ? '\'' . addslashes( $this->m_strColumnName ) . '\'' : 'NULL';
	}

	public function setPropertySettingValue( $strPropertySettingValue ) {
		$this->set( 'm_strPropertySettingValue', CStrings::strTrimDef( $strPropertySettingValue, -1, NULL, true ) );
	}

	public function getPropertySettingValue() {
		return $this->m_strPropertySettingValue;
	}

	public function sqlPropertySettingValue() {
		return ( true == isset( $this->m_strPropertySettingValue ) ) ? '\'' . addslashes( $this->m_strPropertySettingValue ) . '\'' : 'NULL';
	}

	public function setDisplayText( $strDisplayText, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDisplayText', CStrings::strTrimDef( $strDisplayText, -1, NULL, true ), $strLocaleCode );
	}

	public function getDisplayText( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDisplayText', $strLocaleCode );
	}

	public function sqlDisplayText() {
		return ( true == isset( $this->m_strDisplayText ) ) ? '\'' . addslashes( $this->m_strDisplayText ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, property_setting_key_id, property_setting_key_value_type_id, table_name, column_name, property_setting_value, display_text, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlPropertySettingKeyId() . ', ' .
						$this->sqlPropertySettingKeyValueTypeId() . ', ' .
						$this->sqlTableName() . ', ' .
						$this->sqlColumnName() . ', ' .
						$this->sqlPropertySettingValue() . ', ' .
						$this->sqlDisplayText() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_setting_key_id = ' . $this->sqlPropertySettingKeyId(). ',' ; } elseif( true == array_key_exists( 'PropertySettingKeyId', $this->getChangedColumns() ) ) { $strSql .= ' property_setting_key_id = ' . $this->sqlPropertySettingKeyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_setting_key_value_type_id = ' . $this->sqlPropertySettingKeyValueTypeId(). ',' ; } elseif( true == array_key_exists( 'PropertySettingKeyValueTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_setting_key_value_type_id = ' . $this->sqlPropertySettingKeyValueTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' table_name = ' . $this->sqlTableName(). ',' ; } elseif( true == array_key_exists( 'TableName', $this->getChangedColumns() ) ) { $strSql .= ' table_name = ' . $this->sqlTableName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' column_name = ' . $this->sqlColumnName(). ',' ; } elseif( true == array_key_exists( 'ColumnName', $this->getChangedColumns() ) ) { $strSql .= ' column_name = ' . $this->sqlColumnName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_setting_value = ' . $this->sqlPropertySettingValue(). ',' ; } elseif( true == array_key_exists( 'PropertySettingValue', $this->getChangedColumns() ) ) { $strSql .= ' property_setting_value = ' . $this->sqlPropertySettingValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' display_text = ' . $this->sqlDisplayText(). ',' ; } elseif( true == array_key_exists( 'DisplayText', $this->getChangedColumns() ) ) { $strSql .= ' display_text = ' . $this->sqlDisplayText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'property_setting_key_id' => $this->getPropertySettingKeyId(),
			'property_setting_key_value_type_id' => $this->getPropertySettingKeyValueTypeId(),
			'table_name' => $this->getTableName(),
			'column_name' => $this->getColumnName(),
			'property_setting_value' => $this->getPropertySettingValue(),
			'display_text' => $this->getDisplayText(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>