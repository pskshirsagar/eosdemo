<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CArPaymentDetails
 * Do not add any new functions to this class.
 */

class CBaseArPaymentDetails extends CEosPluralBase {

	/**
	 * @return CArPaymentDetail[]
	 */
	public static function fetchArPaymentDetails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CArPaymentDetail::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CArPaymentDetail
	 */
	public static function fetchArPaymentDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CArPaymentDetail::class, $objDatabase );
	}

	public static function fetchArPaymentDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ar_payment_details', $objDatabase );
	}

	public static function fetchArPaymentDetailById( $intId, $objDatabase ) {
		return self::fetchArPaymentDetail( sprintf( 'SELECT * FROM ar_payment_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchArPaymentDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchArPaymentDetails( sprintf( 'SELECT * FROM ar_payment_details WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentDetailsByArPaymentId( $intArPaymentId, $objDatabase ) {
		return self::fetchArPaymentDetails( sprintf( 'SELECT * FROM ar_payment_details WHERE ar_payment_id = %d', ( int ) $intArPaymentId ), $objDatabase );
	}

	public static function fetchArPaymentDetailsByAllocationTypeId( $intAllocationTypeId, $objDatabase ) {
		return self::fetchArPaymentDetails( sprintf( 'SELECT * FROM ar_payment_details WHERE allocation_type_id = %d', ( int ) $intAllocationTypeId ), $objDatabase );
	}

	public static function fetchArPaymentDetailsByAccountVerificationLogId( $intAccountVerificationLogId, $objDatabase ) {
		return self::fetchArPaymentDetails( sprintf( 'SELECT * FROM ar_payment_details WHERE account_verification_log_id = %d', ( int ) $intAccountVerificationLogId ), $objDatabase );
	}

}
?>