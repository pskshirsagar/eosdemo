<?php

class CBaseFedAchParticipant extends CEosSingularBase {

	const TABLE_NAME = 'public.fed_ach_participants';

	protected $m_intId;
	protected $m_strRoutingNumber;
	protected $m_strOfficeCode;
	protected $m_strServicingFedRoutingNumber;
	protected $m_strRecordTypeCode;
	protected $m_strChangeDate;
	protected $m_strNewRoutingNumber;
	protected $m_strCustomerName;
	protected $m_strAddress;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strPostalCode;
	protected $m_strPhoneNumber;
	protected $m_strInstitutionStatusCode;
	protected $m_strDataViewCode;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['routing_number'] ) && $boolDirectSet ) $this->set( 'm_strRoutingNumber', trim( stripcslashes( $arrValues['routing_number'] ) ) ); elseif( isset( $arrValues['routing_number'] ) ) $this->setRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['routing_number'] ) : $arrValues['routing_number'] );
		if( isset( $arrValues['office_code'] ) && $boolDirectSet ) $this->set( 'm_strOfficeCode', trim( stripcslashes( $arrValues['office_code'] ) ) ); elseif( isset( $arrValues['office_code'] ) ) $this->setOfficeCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['office_code'] ) : $arrValues['office_code'] );
		if( isset( $arrValues['servicing_fed_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strServicingFedRoutingNumber', trim( stripcslashes( $arrValues['servicing_fed_routing_number'] ) ) ); elseif( isset( $arrValues['servicing_fed_routing_number'] ) ) $this->setServicingFedRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['servicing_fed_routing_number'] ) : $arrValues['servicing_fed_routing_number'] );
		if( isset( $arrValues['record_type_code'] ) && $boolDirectSet ) $this->set( 'm_strRecordTypeCode', trim( stripcslashes( $arrValues['record_type_code'] ) ) ); elseif( isset( $arrValues['record_type_code'] ) ) $this->setRecordTypeCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['record_type_code'] ) : $arrValues['record_type_code'] );
		if( isset( $arrValues['change_date'] ) && $boolDirectSet ) $this->set( 'm_strChangeDate', trim( $arrValues['change_date'] ) ); elseif( isset( $arrValues['change_date'] ) ) $this->setChangeDate( $arrValues['change_date'] );
		if( isset( $arrValues['new_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strNewRoutingNumber', trim( stripcslashes( $arrValues['new_routing_number'] ) ) ); elseif( isset( $arrValues['new_routing_number'] ) ) $this->setNewRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['new_routing_number'] ) : $arrValues['new_routing_number'] );
		if( isset( $arrValues['customer_name'] ) && $boolDirectSet ) $this->set( 'm_strCustomerName', trim( stripcslashes( $arrValues['customer_name'] ) ) ); elseif( isset( $arrValues['customer_name'] ) ) $this->setCustomerName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_name'] ) : $arrValues['customer_name'] );
		if( isset( $arrValues['address'] ) && $boolDirectSet ) $this->set( 'm_strAddress', trim( stripcslashes( $arrValues['address'] ) ) ); elseif( isset( $arrValues['address'] ) ) $this->setAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['address'] ) : $arrValues['address'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( stripcslashes( $arrValues['postal_code'] ) ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( stripcslashes( $arrValues['phone_number'] ) ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number'] ) : $arrValues['phone_number'] );
		if( isset( $arrValues['institution_status_code'] ) && $boolDirectSet ) $this->set( 'm_strInstitutionStatusCode', trim( stripcslashes( $arrValues['institution_status_code'] ) ) ); elseif( isset( $arrValues['institution_status_code'] ) ) $this->setInstitutionStatusCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_status_code'] ) : $arrValues['institution_status_code'] );
		if( isset( $arrValues['data_view_code'] ) && $boolDirectSet ) $this->set( 'm_strDataViewCode', trim( stripcslashes( $arrValues['data_view_code'] ) ) ); elseif( isset( $arrValues['data_view_code'] ) ) $this->setDataViewCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['data_view_code'] ) : $arrValues['data_view_code'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setRoutingNumber( $strRoutingNumber ) {
		$this->set( 'm_strRoutingNumber', CStrings::strTrimDef( $strRoutingNumber, 240, NULL, true ) );
	}

	public function getRoutingNumber() {
		return $this->m_strRoutingNumber;
	}

	public function sqlRoutingNumber() {
		return ( true == isset( $this->m_strRoutingNumber ) ) ? '\'' . addslashes( $this->m_strRoutingNumber ) . '\'' : 'NULL';
	}

	public function setOfficeCode( $strOfficeCode ) {
		$this->set( 'm_strOfficeCode', CStrings::strTrimDef( $strOfficeCode, 10, NULL, true ) );
	}

	public function getOfficeCode() {
		return $this->m_strOfficeCode;
	}

	public function sqlOfficeCode() {
		return ( true == isset( $this->m_strOfficeCode ) ) ? '\'' . addslashes( $this->m_strOfficeCode ) . '\'' : 'NULL';
	}

	public function setServicingFedRoutingNumber( $strServicingFedRoutingNumber ) {
		$this->set( 'm_strServicingFedRoutingNumber', CStrings::strTrimDef( $strServicingFedRoutingNumber, 240, NULL, true ) );
	}

	public function getServicingFedRoutingNumber() {
		return $this->m_strServicingFedRoutingNumber;
	}

	public function sqlServicingFedRoutingNumber() {
		return ( true == isset( $this->m_strServicingFedRoutingNumber ) ) ? '\'' . addslashes( $this->m_strServicingFedRoutingNumber ) . '\'' : 'NULL';
	}

	public function setRecordTypeCode( $strRecordTypeCode ) {
		$this->set( 'm_strRecordTypeCode', CStrings::strTrimDef( $strRecordTypeCode, 10, NULL, true ) );
	}

	public function getRecordTypeCode() {
		return $this->m_strRecordTypeCode;
	}

	public function sqlRecordTypeCode() {
		return ( true == isset( $this->m_strRecordTypeCode ) ) ? '\'' . addslashes( $this->m_strRecordTypeCode ) . '\'' : 'NULL';
	}

	public function setChangeDate( $strChangeDate ) {
		$this->set( 'm_strChangeDate', CStrings::strTrimDef( $strChangeDate, -1, NULL, true ) );
	}

	public function getChangeDate() {
		return $this->m_strChangeDate;
	}

	public function sqlChangeDate() {
		return ( true == isset( $this->m_strChangeDate ) ) ? '\'' . $this->m_strChangeDate . '\'' : 'NULL';
	}

	public function setNewRoutingNumber( $strNewRoutingNumber ) {
		$this->set( 'm_strNewRoutingNumber', CStrings::strTrimDef( $strNewRoutingNumber, 240, NULL, true ) );
	}

	public function getNewRoutingNumber() {
		return $this->m_strNewRoutingNumber;
	}

	public function sqlNewRoutingNumber() {
		return ( true == isset( $this->m_strNewRoutingNumber ) ) ? '\'' . addslashes( $this->m_strNewRoutingNumber ) . '\'' : 'NULL';
	}

	public function setCustomerName( $strCustomerName ) {
		$this->set( 'm_strCustomerName', CStrings::strTrimDef( $strCustomerName, 100, NULL, true ) );
	}

	public function getCustomerName() {
		return $this->m_strCustomerName;
	}

	public function sqlCustomerName() {
		return ( true == isset( $this->m_strCustomerName ) ) ? '\'' . addslashes( $this->m_strCustomerName ) . '\'' : 'NULL';
	}

	public function setAddress( $strAddress ) {
		$this->set( 'm_strAddress', CStrings::strTrimDef( $strAddress, 100, NULL, true ) );
	}

	public function getAddress() {
		return $this->m_strAddress;
	}

	public function sqlAddress() {
		return ( true == isset( $this->m_strAddress ) ) ? '\'' . addslashes( $this->m_strAddress ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 20, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' : 'NULL';
	}

	public function setInstitutionStatusCode( $strInstitutionStatusCode ) {
		$this->set( 'm_strInstitutionStatusCode', CStrings::strTrimDef( $strInstitutionStatusCode, 10, NULL, true ) );
	}

	public function getInstitutionStatusCode() {
		return $this->m_strInstitutionStatusCode;
	}

	public function sqlInstitutionStatusCode() {
		return ( true == isset( $this->m_strInstitutionStatusCode ) ) ? '\'' . addslashes( $this->m_strInstitutionStatusCode ) . '\'' : 'NULL';
	}

	public function setDataViewCode( $strDataViewCode ) {
		$this->set( 'm_strDataViewCode', CStrings::strTrimDef( $strDataViewCode, 10, NULL, true ) );
	}

	public function getDataViewCode() {
		return $this->m_strDataViewCode;
	}

	public function sqlDataViewCode() {
		return ( true == isset( $this->m_strDataViewCode ) ) ? '\'' . addslashes( $this->m_strDataViewCode ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, routing_number, office_code, servicing_fed_routing_number, record_type_code, change_date, new_routing_number, customer_name, address, city, state_code, postal_code, phone_number, institution_status_code, data_view_code, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlRoutingNumber() . ', ' .
 						$this->sqlOfficeCode() . ', ' .
 						$this->sqlServicingFedRoutingNumber() . ', ' .
 						$this->sqlRecordTypeCode() . ', ' .
 						$this->sqlChangeDate() . ', ' .
 						$this->sqlNewRoutingNumber() . ', ' .
 						$this->sqlCustomerName() . ', ' .
 						$this->sqlAddress() . ', ' .
 						$this->sqlCity() . ', ' .
 						$this->sqlStateCode() . ', ' .
 						$this->sqlPostalCode() . ', ' .
 						$this->sqlPhoneNumber() . ', ' .
 						$this->sqlInstitutionStatusCode() . ', ' .
 						$this->sqlDataViewCode() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' routing_number = ' . $this->sqlRoutingNumber() . ','; } elseif( true == array_key_exists( 'RoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' routing_number = ' . $this->sqlRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' office_code = ' . $this->sqlOfficeCode() . ','; } elseif( true == array_key_exists( 'OfficeCode', $this->getChangedColumns() ) ) { $strSql .= ' office_code = ' . $this->sqlOfficeCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' servicing_fed_routing_number = ' . $this->sqlServicingFedRoutingNumber() . ','; } elseif( true == array_key_exists( 'ServicingFedRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' servicing_fed_routing_number = ' . $this->sqlServicingFedRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' record_type_code = ' . $this->sqlRecordTypeCode() . ','; } elseif( true == array_key_exists( 'RecordTypeCode', $this->getChangedColumns() ) ) { $strSql .= ' record_type_code = ' . $this->sqlRecordTypeCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' change_date = ' . $this->sqlChangeDate() . ','; } elseif( true == array_key_exists( 'ChangeDate', $this->getChangedColumns() ) ) { $strSql .= ' change_date = ' . $this->sqlChangeDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_routing_number = ' . $this->sqlNewRoutingNumber() . ','; } elseif( true == array_key_exists( 'NewRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' new_routing_number = ' . $this->sqlNewRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_name = ' . $this->sqlCustomerName() . ','; } elseif( true == array_key_exists( 'CustomerName', $this->getChangedColumns() ) ) { $strSql .= ' customer_name = ' . $this->sqlCustomerName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address = ' . $this->sqlAddress() . ','; } elseif( true == array_key_exists( 'Address', $this->getChangedColumns() ) ) { $strSql .= ' address = ' . $this->sqlAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_status_code = ' . $this->sqlInstitutionStatusCode() . ','; } elseif( true == array_key_exists( 'InstitutionStatusCode', $this->getChangedColumns() ) ) { $strSql .= ' institution_status_code = ' . $this->sqlInstitutionStatusCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_view_code = ' . $this->sqlDataViewCode() . ','; } elseif( true == array_key_exists( 'DataViewCode', $this->getChangedColumns() ) ) { $strSql .= ' data_view_code = ' . $this->sqlDataViewCode() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'routing_number' => $this->getRoutingNumber(),
			'office_code' => $this->getOfficeCode(),
			'servicing_fed_routing_number' => $this->getServicingFedRoutingNumber(),
			'record_type_code' => $this->getRecordTypeCode(),
			'change_date' => $this->getChangeDate(),
			'new_routing_number' => $this->getNewRoutingNumber(),
			'customer_name' => $this->getCustomerName(),
			'address' => $this->getAddress(),
			'city' => $this->getCity(),
			'state_code' => $this->getStateCode(),
			'postal_code' => $this->getPostalCode(),
			'phone_number' => $this->getPhoneNumber(),
			'institution_status_code' => $this->getInstitutionStatusCode(),
			'data_view_code' => $this->getDataViewCode(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>