<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPropertySettingKeys
 * Do not add any new functions to this class.
 */

class CBasePropertySettingKeys extends CEosPluralBase {

	/**
	 * @return CPropertySettingKey[]
	 */
	public static function fetchPropertySettingKeys( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPropertySettingKey::class, $objDatabase );
	}

	/**
	 * @return CPropertySettingKey
	 */
	public static function fetchPropertySettingKey( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertySettingKey::class, $objDatabase );
	}

	public static function fetchPropertySettingKeyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_setting_keys', $objDatabase );
	}

	public static function fetchPropertySettingKeyById( $intId, $objDatabase ) {
		return self::fetchPropertySettingKey( sprintf( 'SELECT * FROM property_setting_keys WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchPropertySettingKeysByPropertySettingGroupId( $intPropertySettingGroupId, $objDatabase ) {
		return self::fetchPropertySettingKeys( sprintf( 'SELECT * FROM property_setting_keys WHERE property_setting_group_id = %d', $intPropertySettingGroupId ), $objDatabase );
	}

	public static function fetchPropertySettingKeysByDatabaseTypeId( $intDatabaseTypeId, $objDatabase ) {
		return self::fetchPropertySettingKeys( sprintf( 'SELECT * FROM property_setting_keys WHERE database_type_id = %d', $intDatabaseTypeId ), $objDatabase );
	}

	public static function fetchPropertySettingKeysByToolTipId( $intToolTipId, $objDatabase ) {
		return self::fetchPropertySettingKeys( sprintf( 'SELECT * FROM property_setting_keys WHERE tool_tip_id = %d', $intToolTipId ), $objDatabase );
	}

}
?>