<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSubsidyIncomeLimitAreas
 * Do not add any new functions to this class.
 */

class CBaseSubsidyIncomeLimitAreas extends CEosPluralBase {

	/**
	 * @return CSubsidyIncomeLimitArea[]
	 */
	public static function fetchSubsidyIncomeLimitAreas( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSubsidyIncomeLimitArea', $objDatabase );
	}

	/**
	 * @return CSubsidyIncomeLimitArea
	 */
	public static function fetchSubsidyIncomeLimitArea( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSubsidyIncomeLimitArea', $objDatabase );
	}

	public static function fetchSubsidyIncomeLimitAreaCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_income_limit_areas', $objDatabase );
	}

	public static function fetchSubsidyIncomeLimitAreaById( $intId, $objDatabase ) {
		return self::fetchSubsidyIncomeLimitArea( sprintf( 'SELECT * FROM subsidy_income_limit_areas WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSubsidyIncomeLimitAreasBySubsidyIncomeLimitVersionId( $intSubsidyIncomeLimitVersionId, $objDatabase ) {
		return self::fetchSubsidyIncomeLimitAreas( sprintf( 'SELECT * FROM subsidy_income_limit_areas WHERE subsidy_income_limit_version_id = %d', ( int ) $intSubsidyIncomeLimitVersionId ), $objDatabase );
	}

}
?>