<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CArPayments
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseArPayments extends CEosPluralBase {

	/**
	 * @return CArPayment[]
	 */
	public static function fetchArPayments( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CArPayment::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CArPayment
	 */
	public static function fetchArPayment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CArPayment::class, $objDatabase );
	}

	public static function fetchArPaymentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'view_ar_payments', $objDatabase );
	}

	public static function fetchArPaymentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchArPayment( sprintf( 'SELECT * FROM view_ar_payments WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByCid( $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByCompanyMerchantAccountIdByCid( $intCompanyMerchantAccountId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE company_merchant_account_id = %d AND cid = %d', ( int ) $intCompanyMerchantAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByProcessingBankAccountIdByCid( $intProcessingBankAccountId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE processing_bank_account_id = %d AND cid = %d', ( int ) $intProcessingBankAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByMerchantGatewayIdByCid( $intMerchantGatewayId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE merchant_gateway_id = %d AND cid = %d', ( int ) $intMerchantGatewayId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByPaymentTypeIdByCid( $intPaymentTypeId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE payment_type_id = %d AND cid = %d', ( int ) $intPaymentTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByPaymentMediumIdByCid( $intPaymentMediumId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE payment_medium_id = %d AND cid = %d', ( int ) $intPaymentMediumId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByPaymentStatusTypeIdByCid( $intPaymentStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE payment_status_type_id = %d AND cid = %d', ( int ) $intPaymentStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByReturnTypeIdByCid( $intReturnTypeId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE return_type_id = %d AND cid = %d', ( int ) $intReturnTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByArPaymentIdByCid( $intArPaymentId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE ar_payment_id = %d AND cid = %d', ( int ) $intArPaymentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByScheduledPaymentIdByCid( $intScheduledPaymentId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE scheduled_payment_id = %d AND cid = %d', ( int ) $intScheduledPaymentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByArCodeIdByCid( $intArCodeId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE ar_code_id = %d AND cid = %d', ( int ) $intArCodeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByCustomerPaymentAccountIdByCid( $intCustomerPaymentAccountId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE customer_payment_account_id = %d AND cid = %d', ( int ) $intCustomerPaymentAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByArPaymentTransmissionIdByCid( $intArPaymentTransmissionId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE ar_payment_transmission_id = %d AND cid = %d', ( int ) $intArPaymentTransmissionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByArPaymentExportIdByCid( $intArPaymentExportId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE ar_payment_export_id = %d AND cid = %d', ( int ) $intArPaymentExportId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByPsProductIdByCid( $intPsProductId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE ps_product_id = %d AND cid = %d', ( int ) $intPsProductId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByPsProductOptionIdByCid( $intPsProductOptionId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE ps_product_option_id = %d AND cid = %d', ( int ) $intPsProductOptionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByAccountVerificationLogIdByCid( $intAccountVerificationLogId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE account_verification_log_id = %d AND cid = %d', ( int ) $intAccountVerificationLogId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByCompanyCharityIdByCid( $intCompanyCharityId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE company_charity_id = %d AND cid = %d', ( int ) $intCompanyCharityId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByLeaseStatusTypeIdByCid( $intLeaseStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE lease_status_type_id = %d AND cid = %d', ( int ) $intLeaseStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByAllocationTypeIdByCid( $intAllocationTypeId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE allocation_type_id = %d AND cid = %d', ( int ) $intAllocationTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchArPaymentsByCheckAccountTypeIdByCid( $intCheckAccountTypeId, $intCid, $objDatabase ) {
		return self::fetchArPayments( sprintf( 'SELECT * FROM view_ar_payments WHERE check_account_type_id = %d AND cid = %d', ( int ) $intCheckAccountTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>