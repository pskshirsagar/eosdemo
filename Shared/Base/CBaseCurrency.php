<?php

class CBaseCurrency extends CEosSingularBase {

	const TABLE_NAME = 'public.currencies';

	protected $m_strCurrencyCode;
	protected $m_strSymbol;
	protected $m_strFormatting;
	protected $m_intRounding;
	protected $m_strDescription;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( stripcslashes( $arrValues['currency_code'] ) ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['currency_code'] ) : $arrValues['currency_code'] );
		if( isset( $arrValues['symbol'] ) && $boolDirectSet ) $this->set( 'm_strSymbol', trim( stripcslashes( $arrValues['symbol'] ) ) ); elseif( isset( $arrValues['symbol'] ) ) $this->setSymbol( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['symbol'] ) : $arrValues['symbol'] );
		if( isset( $arrValues['formatting'] ) && $boolDirectSet ) $this->set( 'm_strFormatting', trim( stripcslashes( $arrValues['formatting'] ) ) ); elseif( isset( $arrValues['formatting'] ) ) $this->setFormatting( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['formatting'] ) : $arrValues['formatting'] );
		if( isset( $arrValues['rounding'] ) && $boolDirectSet ) $this->set( 'm_intRounding', trim( $arrValues['rounding'] ) ); elseif( isset( $arrValues['rounding'] ) ) $this->setRounding( $arrValues['rounding'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' : 'NULL';
	}

	public function setSymbol( $strSymbol ) {
		$this->set( 'm_strSymbol', CStrings::strTrimDef( $strSymbol, 240, NULL, true ) );
	}

	public function getSymbol() {
		return $this->m_strSymbol;
	}

	public function sqlSymbol() {
		return ( true == isset( $this->m_strSymbol ) ) ? '\'' . addslashes( $this->m_strSymbol ) . '\'' : 'NULL';
	}

	public function setFormatting( $strFormatting ) {
		$this->set( 'm_strFormatting', CStrings::strTrimDef( $strFormatting, 240, NULL, true ) );
	}

	public function getFormatting() {
		return $this->m_strFormatting;
	}

	public function sqlFormatting() {
		return ( true == isset( $this->m_strFormatting ) ) ? '\'' . addslashes( $this->m_strFormatting ) . '\'' : 'NULL';
	}

	public function setRounding( $intRounding ) {
		$this->set( 'm_intRounding', CStrings::strToIntDef( $intRounding, NULL, false ) );
	}

	public function getRounding() {
		return $this->m_intRounding;
	}

	public function sqlRounding() {
		return ( true == isset( $this->m_intRounding ) ) ? ( string ) $this->m_intRounding : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function toArray() {
		return array(
			'currency_code' => $this->getCurrencyCode(),
			'symbol' => $this->getSymbol(),
			'formatting' => $this->getFormatting(),
			'rounding' => $this->getRounding(),
			'description' => $this->getDescription(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>