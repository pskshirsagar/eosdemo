<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CPaymentMediums
 * Do not add any new functions to this class.
 */

class CBasePaymentMediums extends CEosPluralBase {

	/**
	 * @return CPaymentMedium[]
	 */
	public static function fetchPaymentMediums( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPaymentMedium::class, $objDatabase );
	}

	/**
	 * @return CPaymentMedium
	 */
	public static function fetchPaymentMedium( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPaymentMedium::class, $objDatabase );
	}

	public static function fetchPaymentMediumCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'payment_mediums', $objDatabase );
	}

	public static function fetchPaymentMediumById( $intId, $objDatabase ) {
		return self::fetchPaymentMedium( sprintf( 'SELECT * FROM payment_mediums WHERE id = %d', $intId ), $objDatabase );
	}

}
?>