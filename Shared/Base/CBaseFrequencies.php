<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CFrequencies
 * Do not add any new functions to this class.
 */

class CBaseFrequencies extends CEosPluralBase {

	/**
	 * @return CFrequency[]
	 */
	public static function fetchFrequencies( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CFrequency::class, $objDatabase );
	}

	/**
	 * @return CFrequency
	 */
	public static function fetchFrequency( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFrequency::class, $objDatabase );
	}

	public static function fetchFrequencyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'frequencies', $objDatabase );
	}

	public static function fetchFrequencyById( $intId, $objDatabase ) {
		return self::fetchFrequency( sprintf( 'SELECT * FROM frequencies WHERE id = %d', $intId ), $objDatabase );
	}

}
?>