<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultIdentificationTypes
 * Do not add any new functions to this class.
 */

class CBaseDefaultIdentificationTypes extends CEosPluralBase {

	/**
	 * @return CDefaultIdentificationType[]
	 */
	public static function fetchDefaultIdentificationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultIdentificationType::class, $objDatabase );
	}

	/**
	 * @return CDefaultIdentificationType
	 */
	public static function fetchDefaultIdentificationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultIdentificationType::class, $objDatabase );
	}

	public static function fetchDefaultIdentificationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_identification_types', $objDatabase );
	}

	public static function fetchDefaultIdentificationTypeById( $intId, $objDatabase ) {
		return self::fetchDefaultIdentificationType( sprintf( 'SELECT * FROM default_identification_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>