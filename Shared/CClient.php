<?php

use Psi\Eos\Entrata\CApBatches;
use Psi\Eos\Entrata\CCareerApplications;
use Psi\Eos\Entrata\CPressReleaseCategories;
use Psi\Eos\Entrata\CPressReleases;
use Psi\Eos\Entrata\CCompanyAreas;
use Psi\Eos\Entrata\CCompanyMediaFolders;
use Psi\Eos\Entrata\CCareers;
use Psi\Eos\Entrata\CWebsiteAdPageTypes;
use Psi\Eos\Entrata\CWebsiteRedirects;
use Psi\Eos\Entrata\CWebsites;
use Psi\Eos\Entrata\CWebsiteTemplates;
use Psi\Eos\Entrata\CCompanyMediaFiles;
use Psi\Eos\Entrata\CFiles;
use Psi\Eos\Entrata\CFileAssociations;
use Psi\Eos\Entrata\CFloorplanGroups;
use Psi\Eos\Entrata\CCareerTypes;
use Psi\Eos\Entrata\CMaintenancePriorities;
use Psi\Eos\Entrata\CStateMetaSettings;
use Psi\Eos\Entrata\CWebsiteAmenities;
use Psi\Eos\Entrata\CWebsiteProperties;
use Psi\Eos\Entrata\CGlAccounts;
use Psi\Eos\Entrata\CScheduledTasks;
use Psi\Eos\Entrata\CMaintenanceLocations;
use Psi\Eos\Entrata\CInspections;
use Psi\Eos\Entrata\CUnitTypes;
use Psi\Eos\Entrata\CMaintenanceStatuses;
use Psi\Eos\Entrata\CMaintenanceProblems;
use Psi\Eos\Entrata\CMaintenanceTemplates;
use Psi\Eos\Entrata\CPropertyMaintenanceLocations;
use Psi\Eos\Entrata\CPropertyGroups;
use Psi\Eos\Entrata\CPetTypes;
use Psi\Eos\Entrata\CPropertyFloorplans;
use Psi\Eos\Entrata\CPropertyGroupTypes;
use Psi\Eos\Entrata\CPropertyMaintenanceLocationProblems;
use Psi\Eos\Entrata\CInspectionForms;
use Psi\Eos\Entrata\CMaintenanceRequests;
use Psi\Eos\Entrata\CCompanyDepartments;
use Psi\Eos\Entrata\CInspectionNotes;
use Psi\Eos\Entrata\CPropertyBuildings;
use Psi\Eos\Entrata\CPropertyFloors;
use Psi\Eos\Entrata\CPropertyDetails;
use Psi\Eos\Entrata\CPropertyUnits;
use Psi\Eos\Entrata\CCompanyEmployees;
use Psi\Eos\Entrata\CCompanyPreferences;
use Psi\Eos\Entrata\CUnitSpaces;
use Psi\Eos\Admin\CPersons;
use Psi\Eos\Admin\CAccountRegions;
use Psi\Eos\Admin\CAccountProperties;
use Psi\Eos\Admin\CCompanyCharges;
use Psi\Eos\Admin\CContractProperties;
use Psi\Eos\Admin\CSimpleContracts;
use Psi\Eos\Admin\CInvoices;
use Psi\Eos\Entrata\CPropertyProducts;
use Psi\Eos\Admin\CPsProducts;
use Psi\Eos\Admin\CCommissionRateAssociations;
use Psi\Eos\Admin\CCommissions;
use Psi\Eos\Admin\CContracts;
use Psi\Eos\Admin\CCommissionStructures;
use Psi\Eos\Admin\CTransactions;
use Psi\Eos\Admin\CAccounts;

class CClient extends CBaseClient {

	const OVERDUE_LAST_MONTH           = 1;
	const OVERDUE_ONE_TO_TWO_MONTHS    = 2;
	const OVERDUE_TWO_TO_THREE_MONTHS  = 3;
	const OVERDUE_THREE_TO_FOUR_MONTHS = 4;
	const OVERDUE_ABOVE_FOUR_MONTHS    = 5;

	/**
	 * @deprecated Use ID_DEFAULT instead of const ID as its not named correctly. Looking at CClient::ID doesn't make any sense while CClient::ID_DEFAULT gives clear idea.
	 *
	 */
	const ID                                    = 1;
	const ID_DEFAULT                            = 1;

	const ID_PSI                                = 7;
	const ID_TRAINING_CLIENT                    = 13;
	const ID_GORDON_MANAGEMENT                  = 224;
	const ID_DEMOOLD                            = 235;
	const ID_ACCOUNT_MANAGER                    = 1668;
	const ID_RIVERSTONE                         = 1106;
	const ID_DOMINIUM_MANAGEMENT_SERVICES_LLC   = 1333;
	const ID_DEV_TEST                           = 1405;
	const ID_LYND                               = 1491;
	const ID_IMPLEMENTATION                     = 1596;
	const ID_DRAPER_AND_KRAMER                  = 1662;
	const ID_WESTOVER_COMPANIES                 = 1715;
	const ID_LINCOLN_PROPERTY_COMPANY           = 1826;
	const ID_TEST_LINCOLN                       = 1865;
	const ID_DOLBEN_COMPANY                     = 1926;
	const ID_GREYSTAR_PROPERTIES                = 1976;
	const ID_YARDIVOYAGER6DEMO                  = 1995;
	const ID_MONOGRAM_APARTMENT                 = 2014;
	const ID_WINN_RESIDENTIAL                   = 2109;
	const ID_DB_XENTO_SYSTEMS                   = 2138;
	const ID_DEMO_QA_INTEGRATIONS               = 2195;
	const ID_MADISON_APARTMENT_GROUP            = 2247;
	const ID_FAIRFIELD_RESIDENTIAL              = 2213;
	const ID_WEINDER_APARTMENTS_HOMES           = 2393;
	const ID_FOGELMAN_MGMT                      = 2540;
	const ID_CAMPUS_APARTMENTS                  = 2603;
	const ID_LERNER_ENTERPRISES                 = 2683;
	const ID_TEST_PRIMARY_SALES_DEMO            = 2695;
	const ID_FPI_MANAGEMENT                     = 2748;
	const ID_BATMAN_RENTALS                     = 2753;
	const ID_MILESTONE_MGMT                     = 2785;
	const ID_LARAMAR_GROUP_LLC                  = 2843;
	const ID_BRE                                = 3033;
	const ID_ALLIANCE_RESIDENTIAL               = 3049;
	const ID_OZONE_ENTERPRISES                  = 3100;
	const ID_LANDMARK                           = 3114;
	const ID_WINDSOR                            = 3148;
	const ID_THE_HANOVER_COMPANY                = 3156;
	const ID_TEST_ALLIANCE_RESIDENTIAL          = 3160;
	const ID_ASSET_CAMPUS_HOUSING               = 3395;
	const ID_TRINITY_PROPERTY_CONSULTANT        = 3482;
	const ID_DOBLER_CLIENT                      = 4104;
	const ID_SWAY_MANAGEMENT                    = 4282;
	const ID_UDR                                = 4319;
	const ID_HOME_PROPERTIES                    = 4396;
	const ID_CH_MANAGEMENT_SERVICES_LLC         = 4532;
	const ID_RESIDENT_PORTAL_2_0                = 4547;
	const ID_TANDEM                             = 4585;
	const ID_UDR_TEST                           = 6154;
	const ID_KETTLER                            = 6941;
	const ID_RESIDENT_VERIFY_DEMO               = 7890;
	const ID_AMLI_RESIDENTIAL                   = 8742;
	const ID_CAMDEN_PROPERTY_TRUST              = 10153;
	const ID_PARADIGM_PROPERTIES_MGMT           = 10357;
	const ID_GREYSTAR_ADVANTAGE                 = 11189;
	const ID_LIFESTYLE_COMMUNITIES              = 11806;
	const ID_LEASING_CENTER_TRAINING            = 11839;
	const ID_LINCOLN_PROPERTY_COMPANY_CORE_TEST = 11893;
	const ID_VANILLA_SETTINGS                   = 12048;
	const ID_CAMPUS_LIFE_AND_STYLE              = 12394;
	const ID_ROBIN_RENTALS                      = 12568;
	const ID_JRK_PROPERTY_HOLDINGS              = 12859;
	const ID_SCION_GROUP                        = 13531;
	const ID_MONUMENT_REAL_ESTATE               = 13659;
	const ID_AION_PARTNERS                      = 14181;
	const ID_PROPERTY_RESOURCES_GROUP           = 11999;
	const ID_MILL_CREEK_RESIDENTIAL_TRUST       = 4583;
	const ID_VENDOR_ACCESS_DEFAULT_CLIENT       = 13740;
	const ID_COTTONWOOD_CAPITAL_PROPERTY_MGMT   = 13938;
	const ID_INTEGRITY_ASSET_MGMT               = 14687;
	const ID_TREEWOODS                          = 3609;
	const ID_TRITON_INVESTMENTS_TEST            = 14177;
	const ID_PLACE_10_RESIDENTIAL_NEW           = 14368;
	const ID_TIME_GROUP_WPM						= 2485;
	const ID_APP_STORE_CERTIFICATION			= 5061;
	const ID_DINESH_RU                          = 4377;
	const ID_DO_NOT_USE_SMOKE_AUTOMATION_RAPID  = 17515;
	const ID_CORVIAS							= 14693;
	const ID_MILL_CREEK_RESIDENTIAL_TRUST_NEW	= 14034;
	const ID_WATERTON							= 1916;
	const ID_AMERISOUTH_REALTY_MANAGEMENT		= 14412;
	const ID_RMPRICING		                    = 4111;
	const ID_MOUNTS44                           = 12924;
	const ID_JAMES_HARRIS_CONSULTING            = 11695;
	const ID_MOUNTS2                            = 11358;
	const ID_TEST_SYSTEM                        = 227;
	const ID_GIG_TEST							= 15731;
	const ID_I18N								= 15705;
	const ID_RIVER_ASSET_MANAGEMENT             = 14047;
	const ID_BM_SMITH_AND_ASSOCIATES			= 14724;
	const ID_GREYSTAR_MEXICO					= 16227;
	const ID_GREYSTAR_MEXICO_TEST				= 16350;
	const ID_GREYSTAR_STUDENT_LIVING			= 15027;
	const ID_ROSS_MANAGEMENT					= 6949;
	const ID_HAMPSHIRE_ASSETS					= 13165;
	const ID_DONALDSON_GROUP					= 15458;
	const ID_W3_OWNER_LP_ENTRATA_CORE			= 16033;
	const ID_W3_OWNER_LP						= 13202;
	const ID_MATRIX_RESIDENTIAL_LCC				= 9241;
	const ID_PEAK_CAMPUS						= 15647;
	const ID_RESIDENT_VERIFY_STANDARD_DEMO  	= 2005;
	const ID_XENTO_PRODUCTS						= 16597;
	const ID_NORTH_AMERICAN_PROPERTIES_NEW  	= 16477;

	const ID_GIG_15149_TEST                     = 15149;
	const ID_GIG_15142_TEST                     = 15142;
	const ID_GREYSTAR_SPAIN_TEST                = 16534;
	const ID_GREYSTAR_IRELAND_TEST              = 16643;
	const ID_GREYSTAR_UK_TEST                   = 17022;
	const ID_GIG_16116_TEST                     = 16116;
	const ID_GREYSTAR_STUDENT_TEST              = 16331;
	const ID_GREYSTAR_STUDENT_16390_TEST        = 16390;

	const ID_RESIDENT_INSURE					= 14156;
	const ID_RI_GENERIC					        = 4668;
	const ID_SPECIALIZED_REAL_ESTATE_GROUP_INC	= 15789;
	const ID_AIMCO								= 16431;
	const ID_JIMH_CONSULTING 					= 6806;
	const ID_ACT                                = 16792;
	const ID_BMO_PROPERTIES                     = 14527;
	const ID_PRIMARY_SALES_DEMO_17032           = 17032;
	const ID_LOPM                               = 13345;
	const ID_CZ                                 = 13007;
	const ID_HWB_PROPERTIES                     = 14366;
	const ID_PBL_MANAGEMENT                     = 10054;
	const ID_JML_PROPERTIES                     = 14749;
	const ID_JESSE_HERRIN                       = 15952;
	const ID_JMILLS_MANAGEMENT                  = 14575;
	const ID_SEASONS_PROPERTIES                 = 16562;
	const ID_MJS                                = 15620;
	const ID_LC_PROPERTIES                      = 14777;
	const ID_LEGENDARY_PROPERTIES               = 16750;
	const ID_ZAYA_PROPERTIES                    = 16471;
	const ID_MKS                                = 16591;
	const ID_TRAINING                           = 15121;
	const ID_VOL_PROPERTIES                     = 15039;
	const ID_CCC_PROPERTIES                     = 16427;
	const ID_BASECAMP                           = 13009;
	const ID_XENTO_HR							= 17091;
	const ID_BLAKE_BDEMO						= 8745;
	const ID_GREYSTAR_IRELAND					= 16506;
	const ID_REPUTATION_ADVISOR					= 17006;
	const ID_GIG 								= 15077;
	const ID_KAREN_BERTELLI						= 12742;
	const ID_GREYSTAR_MANAGEMENT_SERVICES 		= 16362;
	const ID_GREYSTAR_SENIOR_HOUSING 			= 15190;
	const ID_GREYSTAR_SPAIN 					= 16480;
	const ID_GREYSTAR_UK 						= 16845;
	const ID_GREYSTAR_FRANCE 					= 16587;
    const ID_GREYSTAR_CHINA 					= 100006;
    const ID_DEV_TEST_COMPANY                   = 1405;
	const ID_LERAPID                            = 14557;
	const ID_DOC_EXECUTION_RAPID                = 17494;
	const ID_GREYSTAR_CHINA_TEST_TRAINING       = 100008;
	const ID_WILLOWICK_MANAGEMENT				= 16407;
	const ID_A_PROPSOL_TESTING					= 1557;
	const ID_LENCHECK_RENBERG					= 4581;
	const ID_PROPERTY_MANAGEMENT				= 16616;
	const ID_IRT                                = 17541;
	const ID_ISCO_LAND							= 17465;
	const ID_PSD_TEST_17818						= 17818;
	const ID_FRANKTHETANK						= 17473;
	const ID_MRT								= 17599;
	const ID_MOORE_PROPERTIES					= 16034;
	const ID_CASSTERO_COURT_APARTMENTS_TEST		= 10889;
	const ID_BMW_COMPANY_LLC					= 10740;
	const ID_STUDENT_QUARTERS					= 16593;
	const ID_VANGUARD_REALTY_GROUP				= 9370;

	public static $c_arrintTrainingClientIds = [ self::ID_TRAINING_CLIENT ];
	public static $c_arrintResidentPortalSkinEnabledCids = [ self::ID_DEMO_QA_INTEGRATIONS, self::ID_OZONE_ENTERPRISES, self::ID_RESIDENT_PORTAL_2_0 ];
	public static $c_arrintVesCids = [ CClient::ID_UDR, CClient::ID_UDR_TEST ];
	public static $c_arrintSystemEmailBlockedCids = [ self::ID_THE_HANOVER_COMPANY, self::ID_CAMPUS_LIFE_AND_STYLE, self::ID_MADISON_APARTMENT_GROUP, self::ID_LERNER_ENTERPRISES, self::ID_ROSS_MANAGEMENT, self::ID_HAMPSHIRE_ASSETS, self::ID_DONALDSON_GROUP, self::ID_LIFESTYLE_COMMUNITIES ];
	public static $c_arrintResidentInsureTestClientIds = [ self::ID_DB_XENTO_SYSTEMS, self::ID_TEST_PRIMARY_SALES_DEMO, self::ID_TREEWOODS ];
	public static $c_arrintSystemMessageConsumerClientIds = [ self::ID_DEMOOLD ];
	public static $c_arrintStaticTopACVClients = [
		CClient::ID_GREYSTAR_ADVANTAGE,
		CClient::ID_LINCOLN_PROPERTY_COMPANY,
		CClient::ID_ALLIANCE_RESIDENTIAL,
		CClient::ID_ASSET_CAMPUS_HOUSING,
		CClient::ID_WINN_RESIDENTIAL,
		CClient::ID_WEINDER_APARTMENTS_HOMES,
		CClient::ID_FPI_MANAGEMENT,
		CClient::ID_TRINITY_PROPERTY_CONSULTANT,
		CClient::ID_PARADIGM_PROPERTIES_MGMT,
		CClient::ID_MONUMENT_REAL_ESTATE,
		CClient::ID_MILESTONE_MGMT,
		CClient::ID_AION_PARTNERS,
		CClient::ID_FAIRFIELD_RESIDENTIAL,
		CClient::ID_JRK_PROPERTY_HOLDINGS,
		CClient::ID_DOLBEN_COMPANY,
		CClient::ID_MILL_CREEK_RESIDENTIAL_TRUST,
		CClient::ID_DRAPER_AND_KRAMER,
		CClient::ID_THE_HANOVER_COMPANY,
		CClient::ID_WESTOVER_COMPANIES,
		CClient::ID_INTEGRITY_ASSET_MGMT,
		CClient::ID_KETTLER,
		CClient::ID_SCION_GROUP,
		CClient::ID_FOGELMAN_MGMT,
		CClient::ID_CAMPUS_APARTMENTS,
		CClient::ID_PLACE_10_RESIDENTIAL_NEW,
		CClient::ID_COTTONWOOD_CAPITAL_PROPERTY_MGMT
	];
	public static $c_arrintDemoSalesCids = [ self::ID_DEMOOLD, self::ID_TEST_LINCOLN, self::ID_YARDIVOYAGER6DEMO, self::ID_TEST_PRIMARY_SALES_DEMO, self::ID_TEST_ALLIANCE_RESIDENTIAL, self::ID_XENTO_PRODUCTS ];

	public static $c_arrintSalesOpportunityRedesignCids = [ self::ID_TEST_SYSTEM ];

	// set client ids to restrict execution of entrata dashboard count SQLs
	public static $c_arrintDisabledDashboardCountCids = [];

	public static $c_arrintTopGoalCids = [
		self::ID_AMLI_RESIDENTIAL,
		self::ID_LINCOLN_PROPERTY_COMPANY,
		self::ID_COTTONWOOD_CAPITAL_PROPERTY_MGMT,
		self::ID_MILL_CREEK_RESIDENTIAL_TRUST,
		self::ID_MILL_CREEK_RESIDENTIAL_TRUST_NEW,
		self::ID_CORVIAS
	];

	public static $c_arrintInsuranceTestCids = [ self::ID_DEMOOLD, self::ID_DB_XENTO_SYSTEMS, self::ID_TREEWOODS, self::ID_RESIDENT_INSURE ];

	public static $c_arrintNewLcChatEnabledCids = [ self::ID_XENTO_PRODUCTS, self::ID_DEV_TEST, self::ID_YARDIVOYAGER6DEMO, self::ID_BMW_COMPANY_LLC, self::ID_STUDENT_QUARTERS, self::ID_LERNER_ENTERPRISES, self::ID_VANGUARD_REALTY_GROUP ];

	public static $c_arrintNewLcSmsChatEnabledCids = [ self::ID_XENTO_PRODUCTS, self::ID_DEV_TEST, self::ID_YARDIVOYAGER6DEMO ];

	public static $c_arrintWhiteLabelCids = [
		self::ID_GIG_15149_TEST,
		self::ID_GIG_15142_TEST,
		self::ID_GREYSTAR_SPAIN_TEST,
		self::ID_GREYSTAR_IRELAND_TEST,
		self::ID_GREYSTAR_MEXICO_TEST,
		self::ID_GIG_TEST,
		self::ID_GIG_16116_TEST,
		self::ID_GREYSTAR_STUDENT_TEST,
		self::ID_GREYSTAR_STUDENT_16390_TEST,
		self::ID_ACCOUNT_MANAGER,
		self::ID_DB_XENTO_SYSTEMS,
		self::ID_JAMES_HARRIS_CONSULTING,
		self::ID_ROBIN_RENTALS,
		self::ID_BATMAN_RENTALS
	];


	public static $c_arrintCidsForLoginException = [
		self::ID_ACT,
		self::ID_BMO_PROPERTIES,
		self::ID_PRIMARY_SALES_DEMO_17032,
		self::ID_LOPM,
		self::ID_CZ,
		self::ID_HWB_PROPERTIES,
		self::ID_PBL_MANAGEMENT,
		self::ID_JML_PROPERTIES,
		self::ID_DEMOOLD,
		self::ID_JESSE_HERRIN,
		self::ID_JMILLS_MANAGEMENT,
		self::ID_SEASONS_PROPERTIES,
		self::ID_MJS,
		self::ID_LC_PROPERTIES,
		self::ID_LEGENDARY_PROPERTIES,
		self::ID_ZAYA_PROPERTIES,
		self::ID_MKS,
		self::ID_TRAINING,
		self::ID_VOL_PROPERTIES,
		self::ID_CCC_PROPERTIES,
		self::ID_BASECAMP,
		self::ID_ISCO_LAND,
		self::ID_PSD_TEST_17818,
		self::ID_FRANKTHETANK,
		self::ID_MRT,
		self::ID_MOORE_PROPERTIES,
		self::ID_CASSTERO_COURT_APARTMENTS_TEST
	];

	public static $c_arrintContractAutomationSalesDemoCids = [ self::ID_TEST_PRIMARY_SALES_DEMO, self::ID_BLAKE_BDEMO ];

	public static $c_arrintNewServiceSetupEnabledClients = [
		self::ID_DEMOOLD,
		self::ID_DB_XENTO_SYSTEMS,
		self::ID_GIG_15149_TEST,
		self::ID_GIG_15142_TEST,
		self::ID_GREYSTAR_SPAIN_TEST,
		self::ID_GREYSTAR_MEXICO_TEST,
		self::ID_GREYSTAR_CHINA_TEST_TRAINING,
		self::ID_GREYSTAR_UK_TEST,
		self::ID_GREYSTAR_IRELAND_TEST
	];

	public static $c_arrintPunchOutEnabledClients = [
		self::ID_DEMOOLD,
		self::ID_KAREN_BERTELLI,
		self::ID_DEV_TEST_COMPANY
	];

	public static $c_arrintPropertySettingsKeysReportEnabledClients = [
		self::ID_GIG,
		self::ID_GREYSTAR_MANAGEMENT_SERVICES,
		self::ID_GREYSTAR_SENIOR_HOUSING,
		self::ID_GREYSTAR_STUDENT_LIVING,
		self::ID_GREYSTAR_MEXICO,
		self::ID_GREYSTAR_SPAIN,
		self::ID_GREYSTAR_IRELAND,
		self::ID_GREYSTAR_FRANCE,
		self::ID_GREYSTAR_UK,
		self::ID_GREYSTAR_CHINA,
		self::ID_WILLOWICK_MANAGEMENT
	];

	public static $c_arrintPlaidSandboxTestingCids = [
		self::ID_DB_XENTO_SYSTEMS,
		self::ID_JAMES_HARRIS_CONSULTING
	];

	public static $c_arrintPlaidDevelopmentCids = [
//		self::ID_BATMAN_RENTALS,
//		self::ID_ROBIN_RENTALS
	];

	public static $c_arrintStudentAvailabilityClients = [
		self::ID_LENCHECK_RENBERG,
		self::ID_PROPERTY_MANAGEMENT,
		self::ID_MOUNTS44
	];

	public static $c_intNullCid = 1;

	protected $m_objPsLeadDetail;
	protected $m_objLastCompanyPayment;
	protected $m_objBillingCompanyContact;

	protected $m_arrobjTasks;
	protected $m_arrobjStates;
	protected $m_arrobjPersons;
	protected $m_arrobjArCodes;
	protected $m_arrobjAccounts;
	protected $m_arrobjProperties;
	protected $m_arrobjPsProducts;
	protected $m_arrobjCompanyApps;
	protected $m_arrobjCompanyUsers;
	protected $m_arrobjTransactions;
	protected $m_arrobjApplications;
	protected $m_arrobjPsLeadDetails;
	protected $m_arrobjMarketingIntegrationSubscriptions;
	protected $m_arrobjIntegrationClients;
	protected $m_arrobjCompanyPreferences;
	protected $m_arrobjDataExportSchedules;
	protected $m_arrobjApplicationKeyValues;
	protected $m_arrobjIntegrationDatabases;
	protected $m_arrobjMerchantChangeRequests;
	protected $m_arrobjCompanyMerchantAccounts;
	protected $m_arrobjDocumentParameters;

	protected $m_arrmixCompanyCharges;
	protected $m_arrmixCompanyDisabledCharges;

	protected $m_arrstrPropertyEmailAddresses;

	protected $m_intSelected;
	protected $m_intClusterId;
	protected $m_intHasEntrata;
	protected $m_intIsKeyClient;
	protected $m_intPropertyCount;
	protected $m_intNumberOfUnits;
	protected $m_intNumberOfInvoices;
	protected $m_intChargePostWarning;
	protected $m_intNumberOfCompanyCharges;
	protected $m_intHasNonMonthlyCompanyCharges;
	protected $m_intPreviousCompanyStatusTypeId;
	protected $m_intDefaultResidentLedgerFilterId;
	protected $m_intPropertyId;

	protected $m_strCompanyStatusTypeName;
	protected $m_strProspectPortalPropertyAnchor;
	protected $m_strProspectPortalPropertyCaption;
	protected $m_strPropertyName;
	protected $m_strContractStartDate;

	protected $m_fltAmountDue;
	protected $m_fltAmountToPost;
	protected $m_fltReadyCommissions;
	protected $m_fltPendingCommissions;
	protected $m_fltCommissionAmountDue;
	protected $m_fltMonthlyCompanyChargeTotal;
	protected $m_fltUnexportedChargesTotalTotal;
	protected $m_fltUnexportedChargesTotal;

	public function __construct() {
		parent::__construct();

		$this->m_fltPendingCommissions          = 0;
		$this->m_fltReadyCommissions            = 0;
		$this->m_fltUnexportedChargesTotal      = 0;
		$this->m_intChargePostWarning           = 0;
		$this->m_intHasNonMonthlyCompanyCharges = 0;
		$this->m_intNumberOfCompanyCharges      = 0;
		$this->m_fltAmountToPost                = 0;

		$this->m_arrobjCompanyMerchantAccounts = [];
		$this->m_arrobjMarketingIntegrationSubscriptions        = [];
		$this->m_arrobjStates                  = [];
	}

	/**
	 * Get or Fetch Functions
	 *
	 */

	public static function getAllowedAchAndCheckTypeCids() {
		// Waterton , Sentinel
		return [ 1916, 2124 ];
	}

	public function getOrFetchArCodes( $objDatabase ) {
		if( true == valArr( $this->m_arrobjArCodes ) ) {
			return $this->m_arrobjArCodes;
		}

		$this->fetchArCodes( $objDatabase );

		if( true == is_null( $this->m_arrobjArCodes ) ) {
			$this->m_arrobjArCodes = [];
		}

		return $this->m_arrobjArCodes;
	}

	/**
	 * Add Functions
	 *
	 */

	public function addCompanyMerchantAccount( $objCompanyMerchantAccount ) {
		$this->m_arrobjCompanyMerchantAccounts[$objCompanyMerchantAccount->getId()] = $objCompanyMerchantAccount;
	}

	public function addMarketingIntegrationSubscription( $objMarketingIntegrationSubscription ) {
		$this->m_arrobjMarketingIntegrationSubscriptions[$objMarketingIntegrationSubscription->getId()] = $objMarketingIntegrationSubscription;
	}

	public function addProperty( $objProperty ) {
		$this->m_arrobjProperties[$objProperty->getId()] = $objProperty;
	}

	public function addPsLeadDetail( $objPsLeadDetail ) {
		$this->m_arrobjPsLeadDetails[$objPsLeadDetail->getId()] = $objPsLeadDetail;
	}

	public function addIntegrationClient( $objIntegrationClient ) {
		$this->m_arrobjIntegrationClients[$objIntegrationClient->getId()] = $objIntegrationClient;
	}

	public function addIntegrationDatabase( $objIntegrationDatabase ) {
		$this->m_arrobjIntegrationDatabases[$objIntegrationDatabase->getId()] = $objIntegrationDatabase;
	}

	public function addDataExportSchedule( $objDataExportSchedule ) {
		$this->m_arrobjDataExportSchedules[$objDataExportSchedule->getId()] = $objDataExportSchedule;
	}

	public function addPerson( $objPerson ) {
		$this->m_arrobjPersons[$objPerson->getId()] = $objPerson;
	}

	public function addCompanyUser( $objCompanyUser ) {
		$this->m_arrobjCompanyUsers[$objCompanyUser->getId()] = $objCompanyUser;
	}

	public function addTransaction( $objTransaction ) {
		$this->m_arrobjTransactions[] = $objTransaction;
	}

	public function addApplicationKeyValue( $objApplicationKeyValue ) {
		$this->m_arrobjApplicationKeyValues[$objApplicationKeyValue->getKey()] = $objApplicationKeyValue;
	}

	public function addAccount( $objAccount ) {
		$this->m_arrobjAccounts[$objAccount->getId()] = $objAccount;
	}

	public function addCompanyCharge( $arrmixCompanyCharge ) {
		$this->m_arrmixCompanyCharges[$arrmixCompanyCharge['charge_code_id']] = $arrmixCompanyCharge;
	}

	public function addCompanyDisabledCharge( $arrmixCompanyCharge ) {
		$this->m_arrmixCompanyDisabledCharges[$arrmixCompanyCharge['charge_code_id']] = $arrmixCompanyCharge;
	}

	public function addTask( $objTask ) {
		$this->m_arrobjTasks[$objTask->getId()] = $objTask;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getStates() {
		return $this->m_arrobjStates;
	}

	public function getCompanyMerchantAccounts() {
		return $this->m_arrobjCompanyMerchantAccounts;
	}

	public function getNumberOfCompanyCharges() {
		return $this->m_intNumberOfCompanyCharges;
	}

	public function getHasNonMonthlyCompanyCharges() {
		return $this->m_intHasNonMonthlyCompanyCharges;
	}

	public function getSelected() {
		return $this->m_intSelected;
	}

	public function getUnexportedChargesTotal() {
		return $this->m_fltUnexportedChargesTotal;
	}

	public function getAmountToPost() {
		return $this->m_fltAmountToPost;
	}

	public function getPsLeadDetail() {
		return $this->m_objPsLeadDetail;
	}

	public function getBillingCompanyContact() {
		return $this->m_objBillingCompanyContact;
	}

	public function getPsLeadDetails() {
		return $this->m_arrobjPsLeadDetails;
	}

	public function getPersons() {
		return $this->m_arrobjPersons;
	}

	public function getCompanyUsers() {
		return $this->m_arrobjCompanyUsers;
	}

	public function getTransactions() {
		return $this->m_arrobjTransactions;
	}

	public function getLastCompanyPayment() {
		return $this->m_objLastCompanyPayment;
	}

	public function getMonthlyCompanyChargeTotal() {
		return $this->m_fltMonthlyCompanyChargeTotal;
	}

	public function getApplicationKeyValues() {
		return $this->m_arrobjApplicationKeyValues;
	}

	public function getCompanyPreferences() {
		return $this->m_arrobjCompanyPreferences;
	}

	public function getChargePostWarning() {
		return $this->m_intChargePostWarning;
	}

	public function getProperties() {
		return $this->m_arrobjProperties;
	}

	public function getMarketingIntegrationSubscriptions() {
		return $this->m_arrobjMarketingIntegrationSubscriptions;
	}

	public function getApplications() {
		return $this->m_arrobjApplications;
	}

	public function getHasEntrata() {
		return $this->m_intHasEntrata;
	}

	public function getCompanyCharges() {
		return $this->m_arrmixCompanyCharges;
	}

	public function getCompanyDisabledCharges() {
		return $this->m_arrmixCompanyDisabledCharges;
	}

	public function getAmountDue() {
		return $this->m_fltAmountDue;
	}

	public function getCommissionAmountDue() {
		return $this->m_fltCommissionAmountDue;
	}

	public function getAccounts() {
		return $this->m_arrobjAccounts;
	}

	public function getIntegrationClients() {
		return $this->m_arrobjIntegrationClients;
	}

	public function getIntegrationDatabases() {
		return $this->m_arrobjIntegrationDatabases;
	}

	public function getDataExportSchedules() {
		return $this->m_arrobjDataExportSchedules;
	}

	public function getProspectPortalPropertyAnchor() {
		return $this->m_strProspectPortalPropertyAnchor;
	}

	public function getProspectPortalPropertyCaption() {
		return $this->m_strProspectPortalPropertyCaption;
	}

	public function getPreviousCompanyStatusTypeId() {
		return $this->m_intPreviousCompanyStatusTypeId;
	}

	public function getPropertyEmailAddresses() {
		return $this->m_arrstrPropertyEmailAddresses;
	}

	public function getTasks() {
		return $this->m_arrobjTasks;
	}

	public function getReadyCommissions() {
		return $this->m_fltReadyCommissions;
	}

	public function getPendingCommissions() {
		return $this->m_fltPendingCommissions;
	}

	public function getTotalCommissions() {
		return ( $this->m_fltPendingCommissions + $this->m_fltReadyCommissions );
	}

	public function getCompanyStatusTypeName() {
		return $this->m_strCompanyStatusTypeName;
	}

	public function getIsKeyClient() {
		return $this->m_intIsKeyClient;
	}

	public function getPropertyCount() {
		return $this->m_intPropertyCount;
	}

	public function getClientStatusName() {
		return CCompanyStatusType::companyStatusTypeIdToStr( $this->getCompanyStatusTypeId() );
	}

	public function getNumberOfUnits() {
		return $this->m_intNumberOfUnits;
	}

	public function getNumberOfInvoices() {
		return $this->m_intNumberOfInvoices;
	}

	public function getDefaultResidentLedgerFilterId() {
		return $this->m_intDefaultResidentLedgerFilterId;
	}

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function getEntrataUrl() {
		return CONFIG_SECURE_HOST_PREFIX . $this->getRwxDomain() . CONFIG_RWX_LOGIN_SUFFIX;
	}

	public function getContractStartDate() {
		return $this->m_strContractStartDate;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['amount_due'] ) ) {
			$this->setAmountDue( $arrmixValues['amount_due'] );
		}
		if( true == isset( $arrmixValues['commission_amount_due'] ) ) {
			$this->setCommissionAmountDue( $arrmixValues['commission_amount_due'] );
		}
		if( true == isset( $arrmixValues['rwx_domain'] ) ) {
			$this->setRwxDomain( \Psi\CStringService::singleton()->strtolower( $arrmixValues['rwx_domain'] ) );
		}
		if( true == isset( $arrmixValues['unexported_charges'] ) ) {
			$this->setUnexportedChargesTotal( \Psi\CStringService::singleton()->strtolower( $arrmixValues['unexported_charges'] ) );
		}
		if( true == isset( $arrmixValues['prospect_portal_property_anchor'] ) ) {
			$this->setProspectPortalPropertyAnchor( $arrmixValues['prospect_portal_property_anchor'] );
		}
		if( true == isset( $arrmixValues['prospect_portal_property_caption'] ) ) {
			$this->setProspectPortalPropertyCaption( $arrmixValues['prospect_portal_property_caption'] );
		}
		if( true == isset( $arrmixValues['number_of_company_charges'] ) ) {
			$this->setNumberOfCompanyCharges( $arrmixValues['number_of_company_charges'] );
		}
		if( true == isset( $arrmixValues['has_non_monthly_company_charges'] ) ) {
			$this->setHasNonMonthlyCompanyCharges( $arrmixValues['has_non_monthly_company_charges'] );
		}
		if( true == isset( $arrmixValues['company_status_type_name'] ) ) {
			$this->setCompanyStatusTypeName( $arrmixValues['company_status_type_name'] );
		}
		if( true == isset( $arrmixValues['is_key_client'] ) ) {
			$this->setIsKeyClient( $arrmixValues['is_key_client'] );
		}
		if( true == isset( $arrmixValues['property_count'] ) ) {
			$this->setPropertyCount( $arrmixValues['property_count'] );
		}
		if( true == isset( $arrmixValues['ps_lead_id'] ) ) {
			$this->setPsLeadId( $arrmixValues['ps_lead_id'] );
		}
		if( true == isset( $arrmixValues['number_of_units'] ) ) {
			$this->setNumberOfUnits( $arrmixValues['number_of_units'] );
		}
		if( true == isset( $arrmixValues['number_of_invoices'] ) ) {
			$this->setNumberOfInvoices( $arrmixValues['number_of_invoices'] );
		}
		if( true == isset( $arrmixValues['cluster_id'] ) ) {
			$this->setClusterId( $arrmixValues['cluster_id'] );
		}
		if( true == isset( $arrmixValues['has_entrata'] ) ) {
			$this->setHasEntrata( $arrmixValues['has_entrata'] );
		}
		if( true == isset( $arrmixValues['contract_start_date'] ) ) {
			$this->setContractStartDate( $arrmixValues['contract_start_date'] );
		}
		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}
	}

	public function setDefaults() {
		$this->setCompanyStatusTypeId( CCompanyStatusType::PROSPECT );
	}

	public function setReadyCommissions( $fltReadyCommissions ) {
		$this->m_fltReadyCommissions = $fltReadyCommissions;
	}

	public function setPendingCommissions( $fltPendingCommissions ) {
		$this->m_fltPendingCommissions = $fltPendingCommissions;
	}

	public function setNumberOfCompanyCharges( $intNumberOfCompanyCharges ) {
		$this->m_intNumberOfCompanyCharges = $intNumberOfCompanyCharges;
	}

	public function setTasks( $arrobjTasks ) {
		$this->m_arrobjTasks = $arrobjTasks;
	}

	public function setHasEntrata( $intHasEntrata ) {
		$this->m_intHasEntrata = $intHasEntrata;
	}

	public function setHasNonMonthlyCompanyCharges( $intHasNonMonthlyCompanyCharges ) {
		$this->m_intHasNonMonthlyCompanyCharges = $intHasNonMonthlyCompanyCharges;
	}

	public function setBillingCompanyContact( $objBillingCompanyContact ) {
		$this->m_objBillingCompanyContact = $objBillingCompanyContact;
	}

	public function setStates( $arrobjStates ) {
		$this->m_arrobjStates = $arrobjStates;
	}

	public function setMarketingIntegrationSubscriptions( $arrobjMarketingIntegrationSubscriptions ) {
		$this->m_arrobjMarketingIntegrationSubscriptions = $arrobjMarketingIntegrationSubscriptions;
	}

	public function setSelected( $intSelected ) {
		$this->m_intSelected = $intSelected;
	}

	public function setPsLeadDetail( $objPsLeadDetail ) {
		$this->m_objPsLeadDetail = $objPsLeadDetail;
	}

	public function setUnexportedChargesTotal( $fltUnexportedChargesTotal ) {
		$this->m_fltUnexportedChargesTotal = $fltUnexportedChargesTotal;
	}

	public function setChargePostWarning( $intChargePostWarning ) {
		$this->m_intChargePostWarning = $intChargePostWarning;
	}

	public function setAmountDue( $fltAmountDue ) {
		$this->m_fltAmountDue = CStrings::strToFloatDef( $fltAmountDue, NULL, true, 4 );
	}

	public function setCommissionAmountDue( $fltCommissionAmountDue ) {
		$this->m_fltCommissionAmountDue = CStrings::strToFloatDef( $fltCommissionAmountDue, NULL, true, 4 );
	}

	public function setAccounts( $arrobjAccounts ) {
		$this->m_arrobjAccounts = $arrobjAccounts;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setProspectPortalPropertyAnchor( $strProspectPortalPropertyAnchor ) {
		$this->m_strProspectPortalPropertyAnchor = $strProspectPortalPropertyAnchor;
	}

	public function setProspectPortalPropertyCaption( $strProspectPortalPropertyCaption ) {
		$this->m_strProspectPortalPropertyCaption = $strProspectPortalPropertyCaption;
	}

	public function setPreviousCompanyStatusTypeId( $intCompanyStatusTypeId ) {
		$this->m_intPreviousCompanyStatusTypeId = $intCompanyStatusTypeId;
	}

	public function setPropertyEmailAddresses( $arrstrPropertyEmailAddresses ) {
		$this->m_arrstrPropertyEmailAddresses = $arrstrPropertyEmailAddresses;
	}

	public function setProperties( $arrobjProperties ) {
		$this->m_arrobjProperties = $arrobjProperties;
	}

	public function setCompanyStatusTypeName( $strCompanyStatusTypeName ) {
		$this->m_strCompanyStatusTypeName = $strCompanyStatusTypeName;
	}

	public function setIsKeyClient( $intIsKeyClient ) {
		$this->m_intIsKeyClient = $intIsKeyClient;
	}

	public function setPropertyCount( $intPropertyCount ) {
		$this->m_intPropertyCount = $intPropertyCount;
	}

	public function setNumberOfUnits( $intNumberOfUnits ) {
		$this->m_intNumberOfUnits = $intNumberOfUnits;
	}

	public function setNumberOfInvoices( $intNumberOfInvoices ) {
		$this->m_intNumberOfInvoices = $intNumberOfInvoices;
	}

	public function setDefaultResidentLedgerFilterId( $intDefaultResidentLedgerFilterId ) {
		$this->m_intDefaultResidentLedgerFilterId = $intDefaultResidentLedgerFilterId;
	}

	public function setClusterId( $intClusterId ) {
		$this->m_intClusterId = $intClusterId;
	}

	public function loadDatabase( $intDatabaseUserTypeId, $objConnectDatabase = NULL, $boolForceOpen = true ) {

		$boolIsDatabaseOpen = false;
		$objClientDatabase  = NULL;

		if( false == is_null( $this->getDatabaseId() ) && is_numeric( $this->getDatabaseId() ) ) {
			$objClientDatabase = CCache::fetchObject( 'client_databases_' . $this->getDatabaseId() . '_' . $intDatabaseUserTypeId );
		}

		if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
			if( true == is_null( $objConnectDatabase ) ) {
				$objConnectDatabase = CDatabases::createConnectDatabase();

				if( false == valObj( $objConnectDatabase, 'CDatabase' ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Could not connect to a database.' ) );

					return false;
				}

				$boolIsDatabaseOpen = true;
			}

			if( true == is_null( $this->getDatabaseId() ) ) {
				$objClientDatabase = CDatabases::fetchCommonClientDatabaseByDatabaseUserTypeIdByDatabaseTypeId( $intDatabaseUserTypeId, CDatabaseType::CLIENT, $objConnectDatabase );
			} else {
				$objClientDatabase = $this->fetchDatabaseByDatabaseUserTypeId( $intDatabaseUserTypeId, $objConnectDatabase );
				CCache::storeObject( 'client_databases_' . $this->getDatabaseId() . '_' . $intDatabaseUserTypeId, $objClientDatabase, $intSpecificLifetime = 3600, $arrstrTags = [] );
			}
		}

		if( true == valObj( $objClientDatabase, 'CDatabase' ) && true == getIsDbOnMaintenance( $objClientDatabase->getId() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'client database is on maintenance' ) );

			return false;
		}

		if( false == valObj( $objClientDatabase, 'CDatabase' ) || false == $objClientDatabase->validateAndOpen( $boolForceOpen ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Client is no longer in use' ) );

			return false;
		}

		if( true == $boolIsDatabaseOpen ) {
			$objConnectDatabase->close();
		}

		$this->setDatabase( $objClientDatabase );

		return $objClientDatabase;
	}

	public function setContractStartDate( $strContractStartDate ) {
		$this->m_strContractStartDate = $strContractStartDate;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createProposal() {
		$objProposal = new CProposal();
		$objProposal->setCid( $this->getId() );

		return $objProposal;
	}

	public function createReport() {

		$objReport = new CReport();
		$objReport->setCid( $this->getId() );

		return $objReport;
	}

	public function createCompanyPhoneNumberByPhoneNumberTypeId( $intPhoneNumberTypeId ) {

		$objCompanyPhoneNumber = new CCompanyPhoneNumber();
		$objCompanyPhoneNumber->setCid( $this->getId() );
		$objCompanyPhoneNumber->setPhoneNumberTypeId( $intPhoneNumberTypeId );

		return $objCompanyPhoneNumber;
	}

	public function createCompanyAddressByAddressTypeId( $intAddressTypeId ) {

		$objCompanyAddress = new CCompanyAddress();
		$objCompanyAddress->setCid( $this->getId() );
		$objCompanyAddress->setAddressTypeId( $intAddressTypeId );

		return $objCompanyAddress;
	}

	public function createArPayment() {

		$objArPayment = new CArPayment();
		$objArPayment->setDefaults();
		$objArPayment->setCid( $this->getId() );
		$objArPayment->setUseUnallocatedAmount( 1 );
		$objArPayment->setPaymentDatetime( date( 'm/d/Y H:i:s' ) );

		return $objArPayment;
	}

	public function createCompanyMerchantAccount( $intPaymentMediumId ) {

		$objCompanyMerchantAccount = new CCompanyMerchantAccount();
		$objCompanyMerchantAccount->setPaymentMediumId( $intPaymentMediumId );
		$objCompanyMerchantAccount->setDefaults( $intPaymentMediumId );
		$objCompanyMerchantAccount->setCid( $this->getId() );
		$objCompanyMerchantAccount->setTargetDiscountRate( .0295 );
		$objCompanyMerchantAccount->setFixedConvenienceFee( 29.95 );
		$objCompanyMerchantAccount->setExternalLegalEntityId( NULL );

		return $objCompanyMerchantAccount;
	}

	public function createAccountProperty() {

		$objAccountProperty = new CAccountProperty();
		$objAccountProperty->setCid( $this->getId() );

		return $objAccountProperty;
	}

	public function createIntegrationResult() {

		$objIntegrationResult = new CIntegrationResult();
		$objIntegrationResult->setDefaults();
		$objIntegrationResult->setCid( $this->getId() );

		return $objIntegrationResult;
	}

	public function createIntegrationQueue() {

		$objIntegrationQueue = new CIntegrationQueue();
		$objIntegrationQueue->setCid( $this->getId() );

		return $objIntegrationQueue;
	}

	public function createLeadSource() {

		$objLeadSource = new CLeadSource();
		$objLeadSource->setCid( $this->getId() );

		return $objLeadSource;
	}

	public function createLeadSourceByInternetListingServiceId( $intInternetListingServiceId ) {

		$objLeadSource = new CLeadSource();
		$objLeadSource->setCid( $this->getId() );

		$strName = \Psi\Eos\Entrata\CLeadSources::createService()->getDefaultLeadSourceNameByInternetListingServiceId( $intInternetListingServiceId );
		$objLeadSource->setName( $strName );

		return $objLeadSource;
	}

	public function createCompanyCharge() {

		$objCompanyCharge = new CCompanyCharge();
		$objCompanyCharge->setDefaults();
		$objCompanyCharge->setCid( $this->getId() );

		return $objCompanyCharge;
	}

	public function createContract() {

		$objContract = new CContract();

		$objContract->setCid( $this->getId() );
		$objContract->setContractDatetime( 'NOW()' );

		return $objContract;
	}

	public function createSimpleContract() {

		$objSimpleContract = new CSimpleContract();

		$objSimpleContract->setCid( $this->getId() );

		return $objSimpleContract;
	}

	public function createCompanyPayment() {

		$objCompanyPayment = new CCompanyPayment();
		$objCompanyPayment->setDefaults();
		$objCompanyPayment->setCid( $this->getId() );
		$objCompanyPayment->setCheckAccountTypeId( CCheckAccountType::BUSINESS_CHECKING );

		return $objCompanyPayment;
	}

	public function createIntegrationClientKeyValue() {

		$objIntegrationClientKeyValue = new CIntegrationClientKeyValue();
		$objIntegrationClientKeyValue->setCid( $this->getId() );

		return $objIntegrationClientKeyValue;
	}

	public function createPrimaryPerson() {

		$objPrimaryPerson = new CPerson();
		$objPrimaryPerson->setCid( $this->getId() );
		$objPrimaryPerson->setIsPrimary( 1 );

		return $objPrimaryPerson;
	}

	public function createImplementationPerson() {

		$objImplementationPerson = new CPerson();

		$objImplementationPerson->setPhoneNumber1TypeId( CPhoneNumberType::OFFICE );
		$objImplementationPerson->setCid( $this->getId() );

		return $objImplementationPerson;
	}

	public function createCompanyCategory() {

		$objCompanyCategory = new CCompanyCategory();
		$objCompanyCategory->setCid( $this->getId() );
		$objCompanyCategory->setIsPublished( 1 );

		return $objCompanyCategory;
	}

	public function createFile() {

		$objFile = new CFile();
		$objFile->setCid( $this->getId() );

		return $objFile;
	}

	public function createFileMetadata( $intEntityTypeId = CEntityType::PROPERTY ) {

		$objFileMetadata = new CFileMetadata();
		$objFileMetadata->setCid( $this->getId() );
		$objFileMetadata->setEntityTypeId( $intEntityTypeId );

		return $objFileMetadata;
	}

	public function createFileType() {

		$objFileType = new CFileType();
		$objFileType->setCid( $this->getId() );
		$objFileType->setIsPublished( 1 );
		$objFileType->setIsSystem( 0 );

		return $objFileType;
	}

	public function createFileAssociation() {

		$objFileAssociation = new CFileAssociation();
		$objFileAssociation->setDefaults();
		$objFileAssociation->setCid( $this->getId() );

		return $objFileAssociation;
	}

	public function createMaintenanceProblem() {

		$objMaintenanceProblem = new CMaintenanceProblem();
		$objMaintenanceProblem->setCid( $this->getId() );
		$objMaintenanceProblem->setIsPublished( 1 );

		return $objMaintenanceProblem;
	}

	public function createMaintenanceStatus() {

		$objMaintenanceStatus = new CMaintenanceStatus();
		$objMaintenanceStatus->setCid( $this->getId() );
		$objMaintenanceStatus->setIsPublished( 1 );

		return $objMaintenanceStatus;
	}

	public function createMaintenancePriority() {

		$objMaintenancePriority = new CMaintenancePriority();
		$objMaintenancePriority->setCid( $this->getId() );
		$objMaintenancePriority->setIsPublished( 1 );

		return $objMaintenancePriority;
	}

	public function createMaintenanceRequest() {

		$objMaintenanceRequest = new CMaintenanceRequest();
		$objMaintenanceRequest->setCid( $this->getId() );

		return $objMaintenanceRequest;
	}

	public function createOwnerPerson() {

		$objOwnerPerson = new CPerson();
		$objOwnerPerson->setPhoneNumber1TypeId( CPhoneNumberType::OFFICE );
		$objOwnerPerson->setCid( $this->getId() );

		return $objOwnerPerson;
	}

	public function createBillingPerson() {

		$objBillingPerson = new CPerson();
		$objBillingPerson->setPhoneNumber1TypeId( CPhoneNumberType::OFFICE );
		$objBillingPerson->setCid( $this->getId() );
		$objBillingPerson->setCid( $this->getId() );

		return $objBillingPerson;
	}

	public function createCompanyMediaFolder() {

		$objCompanyMediaFolder = new CCompanyMediaFolder();
		$objCompanyMediaFolder->setCid( $this->getId() );

		return $objCompanyMediaFolder;
	}

	public function createPsLeadDetail() {

		$objPsLeadDetail = new CPsLeadDetail();
		$objPsLeadDetail->setDefaults();

		$objPsLeadDetail->setCid( $this->getId() );

		return $objPsLeadDetail;
	}

	public function createPsLeadTermsAgreement() {

		$objPsLeadTermsAgreement = new CPsLeadTermsAgreement();
		$objPsLeadTermsAgreement->setCid( $this->getId() );

		$objPsLeadTermsAgreement->setIpAddress( $_SERVER['REMOTE_ADDR'] );
		$objPsLeadTermsAgreement->setAgreedOn( 'NOW()' );

		return $objPsLeadTermsAgreement;
	}

	public function createCompanyMediaFile() {

		$objCompanyMediaFile = new CCompanyMediaFile();
		$objCompanyMediaFile->setCid( $this->getId() );

		return $objCompanyMediaFile;
	}

	public function createMarketingMedia() {

		$objMarketingMedia = new CMarketingMedia();
		$objMarketingMedia->setCid( $this->getId() );

		return $objMarketingMedia;
	}

	public function createMarketingMediaAssociation() {

		$objMediaAssociation = new CMarketingMediaAssociation();
		$objMediaAssociation->setCid( $this->getId() );

		return $objMediaAssociation;
	}

	public function createCustomerMedia() {

		$objCustomerMedia = new CCustomerMedia();
		$objCustomerMedia->setCid( $this->getId() );

		return $objCustomerMedia;
	}

	public function createCompanyMedia() {

		$objCompanyMedia = new CCompanyMedia();
		$objCompanyMedia->setCid( $this->getId() );

		return $objCompanyMedia;
	}

	public function createCompanyEvent() {

		$objCompanyEvent = new CCompanyEvent();
		$objCompanyEvent->setCid( $this->getId() );

		return $objCompanyEvent;
	}

	public function createCompanyEventType() {

		$objCompanyEventType = new CCompanyEventType();

		$objCompanyEventType->setCid( $this->getId() );

		return $objCompanyEventType;
	}

	public function createPackage() {

		$objPackage = new CPackage();
		$objPackage->setCid( $this->getId() );

		return $objPackage;
	}

	public function createPackageBatch() {

		$objPackageBatch = new CPackageBatch();
		$objPackageBatch->setCid( $this->getId() );

		return $objPackageBatch;
	}

	public function createAnnouncement() {

		$objAnnouncement = new CAnnouncement();
		$objAnnouncement->setCid( $this->getId() );
		$objAnnouncement->setAnnouncementTypeId( CAnnouncementType::PROPERTY );

		return $objAnnouncement;
	}

	public function createAnnouncementPsProduct() {

		$objAnnouncementPsProduct = new CAnnouncementPsProduct();
		$objAnnouncementPsProduct->setCid( $this->getId() );

		return $objAnnouncementPsProduct;
	}

	public function createLease() {

		$objLease = new CLease();
		$objLease->setCid( $this->getId() );
		$objLease->setLeaseIntervalTypeId( CLeaseIntervalType::APPLICATION );

		return $objLease;
	}

	public function createCompanyConciergeService() {

		$objCompanyConciergeService = new CCompanyConciergeService();
		$objCompanyConciergeService->setCid( $this->getId() );

		return $objCompanyConciergeService;
	}

	public function createSpecial() {

		$objSpecial = new CSpecial();
		$objSpecial->setCid( $this->getId() );

		return $objSpecial;
	}

	public function createInstallmentPlan() {
		$objInstallmentPlan = new CInstallmentPlan();
		$objInstallmentPlan->setCid( $this->getId() );

		return $objInstallmentPlan;
	}

	public function createPrimaryAccount() {

		$objPrimaryAccount = $this->createAccount();
		$objPrimaryAccount->setCid( $this->getId() );
		$objPrimaryAccount->setAccountName( PRIMARY_ACCOUNT_NAME );
		$objPrimaryAccount->setAccountTypeId( CAccountType::PRIMARY );

		return $objPrimaryAccount;
	}

	public function createContactSubmissionType() {
		$objContactSubmissionType = new CContactSubmissionType();
		$objContactSubmissionType->setCid( $this->getId() );
		$objContactSubmissionType->setIsPublished( 1 );

		return $objContactSubmissionType;
	}

	public function createIntegrationClient() {

		$objIntegrationClient = new CIntegrationClient();
		$objIntegrationClient->setCid( $this->getId() );

		return $objIntegrationClient;
	}

	public function createIntegrationDatabase() {

		$objIntegrationDatabase = new \CIntegrationDatabase();
		$objIntegrationDatabase->setCid( $this->getId() );

		return $objIntegrationDatabase;
	}

	public function createAccount( $boolIsMerchantAccount = false ) {

		$objAccount = new CAccount();
		$objAccount->setDefaults();
		$objAccount->setCid( $this->getId() );
		$objAccount->setEntityId( $this->getEntityId() );
		$objAccount->setClient( $this );
		$objAccount->setManualInvoiceFee( 0 );
		$objAccount->setCheckAccountTypeId( NULL );

		if( true == $boolIsMerchantAccount ) {
			$objAccount->setInvoiceMethodId( CInvoiceMethod::TRACK_2 );
		} else {
			$objAccount->setInvoiceMethodId( CInvoiceMethod::TRACK_1 );
		}

		return $objAccount;
	}

	public function createTransaction() {

		$objTransaction = new CTransaction();
		$objTransaction->setDefaults();

		$objTransaction->setCid( $this->getId() );
		$objTransaction->setEntityId( $this->getEntityId() );

		return $objTransaction;
	}

	public function createAllocation() {

		$objAllocation = new CAllocation();
		$objAllocation->setDefaults();

		$objAllocation->setCid( $this->getId() );

		return $objAllocation;
	}

	public function createCompanyUser() {
		$objCompanyUser = new CCompanyUser();

		$objCompanyUser->setIsAdministrator( 0 );
		$objCompanyUser->setRwxDomain( $this->getRwxDomain() );
		$objCompanyUser->setCid( $this->getId() );
		$objCompanyUser->setCompanyUserTypeId( CCompanyUserType::ENTRATA );

		return $objCompanyUser;
	}

	public function createCompanyUserToken() {
		$objCompanyUserToken = new CCompanyUserToken();

		$objCompanyUserToken->setCid( $this->getId() );

		return $objCompanyUserToken;
	}

	public function createCompanyGroup() {
		$objCompanyGroup = new CCompanyGroup();

		$objCompanyGroup->setCid( $this->getId() );

		return $objCompanyGroup;
	}

	public function createCompanyEmployee() {

		$objCompanyEmployee = new CCompanyEmployee();

		$objCompanyEmployee->setCid( $this->getId() );
		$objCompanyEmployee->setEmployeeStatusTypeId( CEmployeeStatusType::CURRENT );

		return $objCompanyEmployee;
	}

	public function createGlAccount() {

		$objGlAccount = new CGlAccount();
		$objGlAccount->setCid( $this->getId() );

		return $objGlAccount;
	}

	public function createGlAccountTree() {

		$objGlAccountTree = new CGlAccountTree();
		$objGlAccountTree->setCid( $this->getId() );

		return $objGlAccountTree;
	}

	public function createBankAccount() {

		$objBankAccount = new CBankAccount();
		$objBankAccount->setNextCheckNumber( 1 );
		$objBankAccount->setCid( $this->getId() );

		return $objBankAccount;
	}

	public function createDataExportSchedule() {

		$objDataExportSchedule = new CDataExportSchedule();
		$objDataExportSchedule->setCid( $this->getId() );

		return $objDataExportSchedule;
	}

	public function createGlHeader() {

		$objGlHeader = new CGlHeader();
		$objGlHeader->setCid( $this->getId() );
		$objGlHeader->setGlHeaderTypeId( CGlHeaderType::STANDARD );

		return $objGlHeader;
	}

	public function createArCode() {

		$objArCode = new CArCode();
		$objArCode->setCid( $this->getId() );
		$objArCode->setArCodeAllocationRulesCollection( \Psi\Core\AccountsReceivables\Collections\CArCodeAllocationRulesCollection::create() );

		return $objArCode;
	}

	public function createArCodeGroup() {

		$objArCodeGroup = new CArCodeGroup();
		$objArCodeGroup->setCid( $this->getId() );

		return $objArCodeGroup;
	}

	public function createArRuleSet() {

		$objArRuleSet = new CArRuleSet();
		$objArRuleSet->setCid( $this->getId() );

		return $objArRuleSet;
	}

	public function createApartmentAmenity() {

		$objApartmentAmenity = new CAmenity();
		$objApartmentAmenity->setCid( $this->getId() );
		$objApartmentAmenity->setAmenityTypeId( CAmenityType::APARTMENT );

		return $objApartmentAmenity;
	}

	public function createCommunityAmenity() {

		$objCommunityAmenity = new CAmenity();
		$objCommunityAmenity->setCid( $this->getId() );
		$objCommunityAmenity->setAmenityTypeId( CAmenityType::COMMUNITY );

		return $objCommunityAmenity;
	}

	public function createWebsite() {

		$objWebsite = new CWebsite();
		$objWebsite->setCid( $this->getId() );

		return $objWebsite;
	}

	public function createCompanyKeyValue( $strKey, $strValue ) {

		$objCompanyKeyValue = new CCompanyKeyValue();
		$objCompanyKeyValue->setCid( $this->getId() );
		$objCompanyKeyValue->setKey( $strKey );
		$objCompanyKeyValue->setValue( $strValue );

		return $objCompanyKeyValue;
	}

	public function createCustomer() {

		$objCustomer = new CCustomer();
		$objCustomer->setCid( $this->getId() );
		$objCustomer->setDontAllowLogin( 0 );
		$objCustomer->setPaymentAllowanceTypeId( CPaymentAllowanceType::ALLOW_ALL );
		$objCustomer->setBlockPosting( 0 );
		$objCustomer->setIsNetworkingEnrolled( 0 );
		$objCustomer->setNameFirst( NULL );

		return $objCustomer;
	}

	public function createProperty() {

		$objProperty = new CProperty();
		$objProperty->setDefaults();
		$objProperty->setCid( $this->getId() );
		$objProperty->setPropertyTypeId( CPropertyType::APARTMENT );

		return $objProperty;
	}

	public function createSettingsTemplateProperty() {

		$objProperty = new CProperty();
		$objProperty->setDefaults();
		$objProperty->setCid( $this->getId() );
		$objProperty->setPropertyTypeId( CPropertyType::SETTINGS_TEMPLATE );
		$objProperty->setIsManagerial( 1 );

		return $objProperty;
	}

	public function createPropertyGroupType() {

		$objPropertyGroupType = new CPropertyGroupType();
		$objPropertyGroupType->setCid( $this->getId() );

		return $objPropertyGroupType;
	}

	public function createPropertyGroup() {

		$objPropertyGroup = new CPropertyGroup();
		$objPropertyGroup->setCid( $this->getId() );

		return $objPropertyGroup;
	}

	public function createPropertyGroupAssociation() {

		$objPropertyGroupAssociation = new CPropertyGroupAssociation();
		$objPropertyGroupAssociation->setCid( $this->getId() );

		return $objPropertyGroupAssociation;
	}

	public function createPropertyStandardResponse() {

		$objPropertyStandardResponse = new CPropertyStandardResponse();
		$objPropertyStandardResponse->setCid( $this->getId() );

		return $objPropertyStandardResponse;
	}

	public function createRate() {

		$objRate = new CRate();
		$objRate->setCid( $this->getId() );

		return $objRate;
	}

	public function createArTransaction() {

		$objArTransaction = new CArTransaction();
		$objArTransaction->setCid( $this->getId() );
		$objArTransaction->setPostDate( date( 'm/d/Y' ) );

		return $objArTransaction;
	}

	public function createMerchantAccountApplication() {

		$objMerchantAccountApplication = new CMerchantAccountApplication();
		$objMerchantAccountApplication->setCid( $this->getId() );

		return $objMerchantAccountApplication;
	}

	public function createDocument() {

		$objDocument = new CDocument();
		$objDocument->setCid( $this->getId() );

		return $objDocument;
	}

	public function createMaintenanceLocation() {

		$objMaintenanceMaintenanceLocation = new CMaintenanceLocation();
		$objMaintenanceMaintenanceLocation->setCid( $this->getId() );
		$objMaintenanceMaintenanceLocation->setMaintenanceLocationTypeId( NULL );

		return $objMaintenanceMaintenanceLocation;
	}

	public function createCorporateClient() {

		$objCorporateClient = new CCorporateClient();
		$objCorporateClient->setCid( $this->getId() );

		return $objCorporateClient;
	}

	public function createScheduledPayment() {

		$objScheduledPayment = new CScheduledPayment();
		$objScheduledPayment->setDefaults();

		$objScheduledPayment->setCid( $this->getId() );
		$objScheduledPayment->setPaymentTypeId( CPaymentType::ACH );
		$objScheduledPayment->setScheduledPaymentFrequencyId( CScheduledPaymentFrequency::MONTHLY );
		$objScheduledPayment->setStartMonth( date( 'n' ) );
		$objScheduledPayment->setSpecifyPaymentCeiling( false );

		return $objScheduledPayment;
	}

	public function createMerchantChangeRequest() {

		$objMerchantChangeRequest = new CMerchantChangeRequest();
		$objMerchantChangeRequest->setCid( $this->getId() );

		return $objMerchantChangeRequest;
	}

	public function createSystemEmail( $intSystemEmailTypeId = NULL ) {
		$objSystemEmail = new CSystemEmail();
		$objSystemEmail->setCid( $this->getId() );

		if( true == valId( $intSystemEmailTypeId ) ) {
			$objSystemEmail->setSystemEmailTypeId( $intSystemEmailTypeId );
		}

		return $objSystemEmail;
	}

	public function createSystemEmailBlock() {
		$objSystemEmailBlock = new CSystemEmailBlock();
		$objSystemEmailBlock->setCid( $this->getId() );

		return $objSystemEmailBlock;
	}

	public function createApplicationFilter() {

		$objApplicationFilter = new CApplicationFilterNew();
		$objApplicationFilter->setDefaults();
		$objApplicationFilter->setCid( $this->getId() );

		return $objApplicationFilter;
	}

	public function createCareerType() {

		$objCareerType = new CCareerType();
		$objCareerType->setCid( $this->getId() );

		return $objCareerType;
	}

	public function createCareerSource() {

		$objCareerSource = new CCareerSource();
		$objCareerSource->setCid( $this->getId() );

		return $objCareerSource;
	}

	public function createCompanyPsDocument() {

		$objPsDocument = new CPsDocument();
		$objPsDocument->setCid( $this->getId() );

		return $objPsDocument;
	}

	public function createForm() {

		$objForm = new CForm();

		$objForm->setIpAddress( getRemoteIpAddress() );
		$objForm->setReferrerUrl( isset( $_SERVER['HTTP_REFERER'] ) ? $_SERVER['HTTP_REFERER'] : '' );
		$objForm->setCid( $this->getId() );
		$objForm->setFormStatusTypeId( CFormStatusType::VIEWED );

		return $objForm;
	}

	public function createCompanyClassifiedCategory() {

		$objCompanyClassifiedCategory = new CCompanyClassifiedCategory();

		$objCompanyClassifiedCategory->setCid( $this->getId() );

		return $objCompanyClassifiedCategory;
	}

	public function createPetType() {
		//  MEGARATES_COMMENTS :
		//  Renamed from createCompanyPetType

		$objPetType = new CPetType();

		$objPetType->setCid( $this->getId() );

		return $objPetType;
	}

	public function createCompanyHoliday() {

		$objCompanyHoliday = new CCompanyHoliday();
		$objCompanyHoliday->setCid( $this->getId() );

		return $objCompanyHoliday;
	}

	public function createCompanyApplication() {

		$objCompanyApplication = new CCompanyApplication();
		$objCompanyApplication->setCid( $this->getId() );
		$objCompanyApplication->setIsPublished( true );

		return $objCompanyApplication;
	}

	public function createApplication() {

		$objApplication = new CApplication();
		$objApplication->setCid( $this->getId() );

		if( false == valObj( $this->m_objDatabase, 'CDatabase' ) || ( true == valObj( $this->m_objDatabase, 'CDatabase' ) && false == is_resource( $this->m_objDatabase->getHandle() ) ) ) {
 			$this->loadDatabase( CDatabaseUserType::PS_PROPERTYMANAGER );
		}

		$objApplication->setDatabase( $this->m_objDatabase );

		return $objApplication;
	}

	public function createApplicant() {

		$objApplicant = new CApplicant();
		$objApplicant->setCid( $this->getId() );
		$objApplicant->setPrimaryPhoneNumberTypeId( CPhoneNumberType::HOME );

		return $objApplicant;
	}

	public function createAvailabilityAlertRequest() {

		$objAvailabilityAlertRequest = new CAvailabilityAlertRequest();
		$objAvailabilityAlertRequest->setDefaults();
		$objAvailabilityAlertRequest->setCid( $this->getId() );

		return $objAvailabilityAlertRequest;
	}

	public function createCompanyArea() {

		$objCompanyArea = new CCompanyArea();
		$objCompanyArea->setDefaults();
		$objCompanyArea->setCid( $this->getId() );

		return $objCompanyArea;
	}

	public function createCareer() {

		$objCareer = new CCareer();
		$objCareer->setCid( $this->getId() );

		return $objCareer;
	}

	public function createCareerApplication() {

		$objCareerApplication = new CCareerApplication();
		$objCareerApplication->setCid( $this->getId() );

		return $objCareerApplication;
	}

	public function createPressReleaseCategory() {

		$objPressReleaseCategory = new CPressReleaseCategory();
		$objPressReleaseCategory->setCid( $this->getId() );
		$objPressReleaseCategory->setIsPublished( 1 );

		return $objPressReleaseCategory;
	}

	public function createPressRelease() {

		$objPressRelease = new CPressRelease();
		$objPressRelease->setReleaseDatetime( date( 'm/d/Y' ) );
		$objPressRelease->setCid( $this->getId() );

		return $objPressRelease;
	}

	public function createScheduledCall() {

		$objScheduledCall = new CScheduledCall();
		$objScheduledCall->setCid( $this->getId() );

		return $objScheduledCall;
	}

	public function createEventResult() {

		$objEventResult = new CEventResult();
		$objEventResult->setCid( $this->getId() );

		return $objEventResult;
	}

	public function createAccountRegion() {

		$objAccountRegion = new CAccountRegion();
		$objAccountRegion->setCid( $this->getId() );

		return $objAccountRegion;
	}

	public function createReview() {

		$objReview = new CReview();

		$objReview->setCid( $this->getId() );

		return $objReview;
	}

	public function createWebsiteRedirect() {

		$objWebsiteRedirect = new CWebsiteRedirect();

		$objWebsiteRedirect->setCid( $this->getId() );

		return $objWebsiteRedirect;
	}

	public function createCallBlock() {

		$objCallBlock = new CCallBlock();

		$objCallBlock->setCid( $this->getId() );

		return $objCallBlock;
	}

	public function createFloorplanGroup() {

		$objFloorplanGroup = new CFloorplanGroup();
		$objFloorplanGroup->setCid( $this->getId() );
		$objFloorplanGroup->setIsPublished( 1 );

		return $objFloorplanGroup;
	}

	public function createApPayee() {

		$objApPayee = new CApPayee();
		$objApPayee->setCid( $this->getId() );
		$objApPayee->setApPayeeTypeId( CApPayeeType::STANDARD );

		return $objApPayee;
	}

	public function createCompanyPricing() {
		$objCompanyPricing = new CCompanyPricing();

		$objCompanyPricing->setDefaults();

		$objCompanyPricing->setCid( $this->getId() );

		return $objCompanyPricing;
	}

	public function createApPayeeTerm() {

		$objApPayeeTerm = new CApPayeeTerm();
		$objApPayeeTerm->setDefaults();
		$objApPayeeTerm->setCid( $this->getId() );

		return $objApPayeeTerm;
	}

	public function createApBatch() {

		$objApBatch = new CApBatch();
		$objApBatch->setCid( $this->getId() );

		return $objApBatch;
	}

	public function createPsLead() {

		$objPsLead = new CPsLead();
		$objPsLead->setCompanyStatusTypeId( $this->getCompanyStatusTypeId() );
		$objPsLead->setCompanyName( $this->getCompanyName() );
		$objPsLead->setIpAddress( getRemoteIpAddress() );
		$objPsLead->setRequestDatetime( 'NOW()' );

		return $objPsLead;
	}

	public function createMaintenanceTemplate() {

		$objMaintenanceTemplate = new CMaintenanceTemplate();
		$objMaintenanceTemplate->setCid( $this->getId() );

		return $objMaintenanceTemplate;
	}

	public function createCheckTemplate() {

		$objCheckTemplate = new CCheckTemplate();
		$objCheckTemplate->setCid( $this->getId() );

		return $objCheckTemplate;
	}

	public function createLateFeeFormula() {

		$objLateFeeFormula = new CLateFeeFormula();
		$objLateFeeFormula->setCid( $this->getId() );
		$objLateFeeFormula->setAutoPostNsfLateFees( true );
		$objLateFeeFormula->setScheduledChargesOnly( true );
		$objLateFeeFormula->setChargeOnPaymentPostDate( true );

		return $objLateFeeFormula;
	}

	public function createOccupation() {

		$objOccupation = new COccupation();
		$objOccupation->setCid( $this->getId() );

		return $objOccupation;
	}

	public function createCompanyMoveOutListItem() {

		$objCompanyMoveOutListItem = new CListItem();
		$objCompanyMoveOutListItem->setCid( $this->getId() );
		$objCompanyMoveOutListItem->setListTypeId( CListType::MOVE_OUT );

		return $objCompanyMoveOutListItem;
	}

	public function createCompanyCancelListItem() {

		$objCompanyCancelListItem = new CListItem();
		$objCompanyCancelListItem->setCid( $this->getId() );
		$objCompanyCancelListItem->setListTypeId( CListType::LEAD_CANCELLATION );

		return $objCompanyCancelListItem;
	}

	public function createEntity() {

		$objEntity = new CEntity();
		$objEntity->setCompanyName( $this->getCompanyName() );

		return $objEntity;
	}

	public function createIdentificationType() {

		$objIdentificationType = new CCompanyIdentificationType();
		$objIdentificationType->setCid( $this->getId() );

		return $objIdentificationType;
	}

	public function createUtility( $intUtilityTypeId = NULL ) {

		$objUtility = new CUtility();
		$objUtility->setCid( $this->getId() );

		if( true == valId( $intUtilityTypeId ) ) {
			$objUtility->setUtilityTypeId( $intUtilityTypeId );
		}

		return $objUtility;
	}

	public function createCallFilter() {

		$objCallFilter = new CCallFilter();
		$objCallFilter->setDefaults();
		$objCallFilter->setCid( $this->m_intId );

		return $objCallFilter;
	}

	public function createCompanyTransmissionVendor( $intTransmissionTypeId = NULL, $intTransmissionVendorId = NULL, $intTransmissionConnectionTypeId = NULL ) {

		$objCompanyTransmissionVendor = new CCompanyTransmissionVendor( $intTransmissionTypeId, $intTransmissionVendorId, $intTransmissionConnectionTypeId, $this->getRwxDomain() );
		$objCompanyTransmissionVendor->setCid( $this->getId() );
		$objCompanyTransmissionVendor->setIsPublished( 1 );

		return $objCompanyTransmissionVendor;
	}

	public function createAccountingExportBatch() {
		$objAccountingExportBatch = new CAccountingExportBatch();

		$objAccountingExportBatch->setCid( $this->getId() );

		return $objAccountingExportBatch;
	}

	public function createApExportBatch() {

		$objApExportBatch = new CApExportBatch();
		$objApExportBatch->setCid( $this->getId() );

		return $objApExportBatch;
	}

	public function createEmbeddedLoginLog() {

		$objEmbeddedLoginLog = new CEmbeddedLoginLog();
		$objEmbeddedLoginLog->setCid( $this->getId() );

		return $objEmbeddedLoginLog;
	}

	public function createDirective( $objClientDatabase ) {

		$objDirective = new CDirective();
		$objDirective->setCid( $this->getId() );
		$objDirective->setDirectiveTypeId( CDirectiveType::CLIENT );
		$objDirective->setDomain( \Psi\CStringService::singleton()->strtolower( $this->getRwxDomain() ) );
		$objDirective->setDatabaseId( $this->getDatabaseId() );
		$objDirective->setClusterId( $objClientDatabase->getClusterId() );

		return $objDirective;
	}

	public function createPerson() {

		$objPerson = new CPerson();
		$objPerson->setCid( $this->getId() );

		return $objPerson;
	}

	public function createApHeader() {

		$objApHeader = new CApHeader();
		$objApHeader->setDefaults();
		$objApHeader->setCid( $this->getId() );
		$objApHeader->setTransactionDatetime( 'NOW()' );
		$objApHeader->setGlTransactionTypeId( CGlTransactionType::AP_PAYMENT );

		return $objApHeader;
	}

	public function createCompanyRoute() {
		$objRoute = new CRoute();

		$objRoute->setCid( $this->getId() );

		return $objRoute;
	}

	public function createCompanyPreference() {

		$objCompanyPreference = new CCompanyPreference();
		$objCompanyPreference->setIsSecure( 0 );
		$objCompanyPreference->setCid( $this->getId() );

		return $objCompanyPreference;
	}

	public function createBudget() {

		$objBudget = new CBudget();

		$objBudget->setCid( $this->getId() );
		$objBudget->setBudgetStatusTypeId( CBudgetStatusType::WORKING );

		return $objBudget;
	}

	public function createBudgetGlAccountMonthNote() {

		$objBudgetGlAccountMonthNote = new CBudgetGlAccountMonthNote();

		$objBudgetGlAccountMonthNote->setCid( $this->getId() );
		$objBudgetGlAccountMonthNote->setNoteDatetime( 'NOW()' );

		return $objBudgetGlAccountMonthNote;
	}

	public function createApAllocation() {

		$objApAllocation = new CApAllocation();

		$objApAllocation->setDefaults();
		$objApAllocation->setCid( $this->getId() );
		$objApAllocation->setAllocationDatetime( 'NOW()' );

		return $objApAllocation;
	}

	public function createGlTree() {

		$objGLTree = new CGlTree();
		$objGLTree->setCid( $this->getId() );
		$objGLTree->setGlTreeTypeId( CGlTreeType::STANDARD );

		return $objGLTree;
	}

	public function createApiRequest() {

		$objApiRequest = new CApiRequest();
		$objApiRequest->setCid( $this->getId() );
		$objApiRequest->setRequestDatetime( 'NOW()' );
		$objApiRequest->setIpAddress( getRemoteIpAddress() );

		return $objApiRequest;
	}

	public function createApiIp() {
		$objApiIp = new CApiIp();
		$objApiIp->setCid( $this->getId() );

		return $objApiIp;
	}

	public function createInspectionForm() {

		$objInspectionForm = new CInspectionForm();
		$objInspectionForm->setCid( $this->getId() );

		return $objInspectionForm;
	}

	public function createPropertyInspectionForm() {

		$objPropertyInspectionForm = new CPropertyInspectionForm();
		$objPropertyInspectionForm->setCid( $this->getId() );

		return $objPropertyInspectionForm;
	}

	public function createInspectionFormLocation() {

		$objInspectionFormLocation = new CInspectionFormLocation();
		$objInspectionFormLocation->setCid( $this->getId() );

		return $objInspectionFormLocation;
	}

	public function createInspectionFormLocationProblem() {

		$objInspectionFormLocationProblem = new CInspectionFormLocationProblem();
		$objInspectionFormLocationProblem->setCid( $this->getId() );

		return $objInspectionFormLocationProblem;
	}

	public function createCriminalClassificationSetting() {

		$objCriminalClassificationSetting = new CCriminalClassificationSetting();
		$objCriminalClassificationSetting->setCid( $this->getId() );
		$objCriminalClassificationSetting->setIsActive( 1 );

		return $objCriminalClassificationSetting;
	}

	public function createScreeningPropertyAccount() {

		$objScreeningPropertyAccount = new CScreeningPropertyAccount();
		$objScreeningPropertyAccount->setCid( $this->getId() );
		$objScreeningPropertyAccount->setIsPublished( true );

		return $objScreeningPropertyAccount;
	}

	public function createCompanyScreeningConfiguration() {

		$objCompanyScreeningConfiguration = new CScreeningConfiguration();
		$objCompanyScreeningConfiguration->setCid( $this->getId() );
		$objCompanyScreeningConfiguration->setIsPublished( true );

		return $objCompanyScreeningConfiguration;
	}

	public function createScreeningApplicantConfiguration() {
		$objCompanyScreeningApplicantConfiguration = new CScreeningApplicantConfiguration();
		$objCompanyScreeningApplicantConfiguration->setCid( $this->getId() );

		return $objCompanyScreeningApplicantConfiguration;
	}

	public function createScreeningAccount() {
		$objScreeningAccount = new CScreeningAccount();
		$objScreeningAccount->setCid( $this->getId() );

		return $objScreeningAccount;
	}

	public function createScreeningAccountRate() {
		$objScreeningAccount = new CScreeningAccountRate();
		$objScreeningAccount->setCid( $this->getId() );

		return $objScreeningAccount;
	}

	public function createScreeningPackageAvailableCondition() {
		$objScreeningPackageAvailableCondition = new CScreeningPackageAvailableCondition();
		$objScreeningPackageAvailableCondition->setCid( $this->getId() );

		return $objScreeningPackageAvailableCondition;
	}

	public function createScreeningAvailableCondition() {
		$objScreeningAvailableCondition = new CScreeningAvailableCondition();
		$objScreeningAvailableCondition->setCid( $this->getId() );

		return $objScreeningAvailableCondition;
	}

	public function createTransmission() {
		$objTransmission = new CTransmission();

		$objTransmission->setDefaults();
		$objTransmission->setCid( $this->getId() );

		return $objTransmission;
	}

	public function createInspectionResponse() {
		$objInspectionResponse = new CInspectionResponse();
		$objInspectionResponse->setCid( $this->getId() );

		return $objInspectionResponse;
	}

	public function createGlHeaderSchedule() {

		$objGlHeaderSchedule = new CGlHeaderSchedule();
		$objGlHeaderSchedule->setCid( $this->getId() );

		return $objGlHeaderSchedule;
	}

	public function creatApFormula() {

		$objApInvoiceAllocation = new CApFormula();
		$objApInvoiceAllocation->setCid( $this->getId() );

		return $objApInvoiceAllocation;
	}

	public function createChecklist() {

		$objChecklist = new CChecklist();
		$objChecklist->setCid( $this->getId() );

		return $objChecklist;
	}

	public function createScheduledAccountingExportBatch() {

		$objAccountingExportScheduler = new CAccountingExportScheduler();
		$objAccountingExportScheduler->setCid( $this->getId() );

		return $objAccountingExportScheduler;
	}

	public function createScheduledApExportBatch() {

		$objScheduledApExportBatch = new CScheduledApExportBatch();
		$objScheduledApExportBatch->setCid( $this->getId() );

		return $objScheduledApExportBatch;
	}

	public function createNoticeBatch() {

		$objNoticeBatch = new CNoticeBatch();
		$objNoticeBatch->setCid( $this->getId() );
		$objNoticeBatch->setIsAutoRun( 0 );
		$objNoticeBatch->setBatchDatetime( date( 'm/d/Y H:i:s' ) );

		return $objNoticeBatch;
	}

	public function createPropertyArCode() {

		$objPropertyArCode = new CPropertyArCode();
		$objPropertyArCode->setCid( $this->getId() );

		return $objPropertyArCode;
	}

	public function createRoommateInterest() {

		$objRoommateInterest = new CRoommateInterest();
		$objRoommateInterest->setDefaults();
		$objRoommateInterest->setCid( $this->getId() );

		return $objRoommateInterest;
	}

	public function createRoommate() {

		$objRoommate = new CRoommate();
		$objRoommate->setCid( $this->getId() );

		return $objRoommate;
	}

	public function createRoommateGroup() {

		$objRoommateGroup = new CRoommateGroup();
		$objRoommateGroup->setCid( $this->getId() );

		return $objRoommateGroup;
	}

	public function createFeeTemplate() {
		$objFeeTemplate = new CFeeTemplate();

		$objFeeTemplate->setDefaults();
		$objFeeTemplate->setCid( $this->getId() );

		return $objFeeTemplate;
	}

	public function createFee() {

		$objFee = new CFee();
		$objFee->setDefaults();
		$objFee->setCid( $this->getId() );

		return $objFee;
	}

	public function createManagementFee() {
		$objManagementFee = new CFee();

		$objManagementFee->setDefaults();
		$objManagementFee->setCid( $this->getId() );
		$objManagementFee->setFeeTypeId( CFeeType::MANAGEMENT_FEES );

		return $objManagementFee;
	}

	public function createPropertyAmenityAvailabilityFileAssociation() {
		$objPropertyAmenityAvailabilityFileAssociation = new CPropertyAmenityAvailabilityFileAssociation();

		$objPropertyAmenityAvailabilityFileAssociation->setCid( $this->getId() );

		return $objPropertyAmenityAvailabilityFileAssociation;
	}

	public function createMigrationToken() {
		$objMigrationToken = new CMigrationToken();

		$objMigrationToken->setCid( $this->getId() );

		return $objMigrationToken;
	}

	public function createNotificationProperty() {

		$objNotificationProperty = new CNotificationProperty();
		$objNotificationProperty->setCid( $this->getId() );

		return $objNotificationProperty;
	}

	public function createNotificationCompanyGroup() {

		$objNotificationCompanyGroup = new CNotificationCompanyGroup();
		$objNotificationCompanyGroup->setCid( $this->getId() );

		return $objNotificationCompanyGroup;
	}

	public function createNotificationModule() {

		$objNotificationModule = new CNotificationModule();
		$objNotificationModule->setCid( $this->getId() );

		return $objNotificationModule;
	}

	public function createNotificationLog() {

		$objNotificationLog = new CNotificationLog();
		$objNotificationLog->setCid( $this->getId() );

		return $objNotificationLog;
	}

	public function createDataChange() {

		$objDataChange = new CDataChange();
		$objDataChange->setDefaults();
		$objDataChange->setCid( $this->getId() );

		return $objDataChange;
	}

	public function createChore() {

		$objChore = new CChore();
		$objChore->setCid( $this->getId() );

		return $objChore;
	}

	public function createChoreType() {

		$objChoreType = new CChoreType();
		$objChoreType->setCid( $this->getId() );

		return $objChoreType;
	}

	public function createPeriod() {

		$objPeriod = new CPeriod();
		$objPeriod->setDefaults();
		$objPeriod->setCid( $this->getId() );

		return $objPeriod;
	}

	public function createEvent() {

		$objEvent = new CEvent();
		$objEvent->setCid( $this->getId() );

		return $objEvent;
	}

	public function createDelinquencyPolicy() {

		$objDelinquencyPolicy = new CDelinquencyPolicy();
		$objDelinquencyPolicy->setCid( $this->getId() );

		return $objDelinquencyPolicy;
	}

	public function createCompanyWebsiteTag() {

		$objCompanyWebsiteTag = new CCompanyWebsiteTag();
		$objCompanyWebsiteTag->setCid( $this->getId() );

		return $objCompanyWebsiteTag;
	}

	public function createContactSubmission() {
		$objContactSubmission = new CContactSubmission();
		$objContactSubmission->setCid( $this->getId() );
		$objContactSubmission->setContactDatetime( 'NOW()' );

		return $objContactSubmission;
	}

	public function createCustomerContact() {

		$objCustomerContact = new CCustomerContact();
		$objCustomerContact->setCid( $this->getId() );

		return $objCustomerContact;
	}

	/**
	 * @return CReportGroup
	 */
	public function createReportGroup() {

		$objReportGroup = new CReportGroup();
		$objReportGroup->setCid( $this->getId() );

		return $objReportGroup;
	}

	public function createLedgerFilter() {

		$objLedgerFilter = new CLedgerFilter();
		$objLedgerFilter->setCid( $this->getId() );
		$objLedgerFilter->setIsPublished( 1 );

		return $objLedgerFilter;
	}

	public function createCompanyReviewAttribute() {
		$objCompanyReviewAttribute = new CCompanyReviewAttribute();
		$objCompanyReviewAttribute->setCid( $this->getId() );

		return $objCompanyReviewAttribute;
	}

	public function createReportInstance() {

		$objReportInstance = new CReportInstance();
		$objReportInstance->setCid( $this->getId() );

		return $objReportInstance;
	}

	public function createReportFilter() {
		$objReportFilter = new CReportFilter();
		$objReportFilter->setCid( $this->getId() );

		return $objReportFilter;
	}

	public function createReportSchedule() {
		$objReportSchedule = new CReportSchedule();
		$objReportSchedule->setCid( $this->getId() );

		return $objReportSchedule;
	}

	public function createApCodeCategory() {

		$objApCodeCategory = new CApCodeCategory();
		$objApCodeCategory->setCid( $this->getId() );

		return $objApCodeCategory;
	}

	public function createJobCategory() {

		$objJobCategory = new CJobCategory();
		$objJobCategory->setCid( $this->getId() );

		return $objJobCategory;
	}

	public function createJob() {
		$objJob = new CJob();
		$objJob->setCid( $this->getId() );
		$objJob->setHasPhases( CJob::HAS_PHASES );

		return $objJob;
	}

	public function createApCode() {

		$objApCode = new CApCode();
		$objApCode->setCid( $this->getId() );

		return $objApCode;
	}

	public function createLeaseStartStructure() {
		$objLeaseStartStructure = new CLeaseStartStructure();
		$objLeaseStartStructure->setCid( $this->getId() );

		return $objLeaseStartStructure;
	}

	public function createLeaseStartWindow() {
		$objLeaseStartWindow = new CLeaseStartWindow();
		$objLeaseStartWindow->setCid( $this->getId() );

		return $objLeaseStartWindow;
	}

	public function createAmenityFilter() {
		$objAmenityFilter = new CAmenityFilter();
		$objAmenityFilter->setCid( $this->getId() );

		return $objAmenityFilter;
	}

	public function createLeaseTermStructure() {

		$objLeaseTermStructure = new CLeaseTermStructure();
		$objLeaseTermStructure->setCid( $this->getId() );

		return $objLeaseTermStructure;
	}

	public function createLeaseTerm() {

		$objLeaseTerm = new CLeaseTerm();
		$objLeaseTerm->setCid( $this->getId() );

		return $objLeaseTerm;
	}

	public function createAmenity() {
		$objAmenity = new CAmenity();
		$objAmenity->setCid( $this->getId() );

		return $objAmenity;
	}

	public function createSpaceConfiguration() {

		$objSpaceConfiguration = new CSpaceConfiguration();
		$objSpaceConfiguration->setCid( $this->getId() );

		return $objSpaceConfiguration;
	}

	public function createAssetCondition() {

		$objAssetCondition = new CAssetCondition();
		$objAssetCondition->setCid( $this->getId() );

		return $objAssetCondition;
	}

	public function createAssetLocation() {

		$objAssetLocation = new CAssetLocation();
		$objAssetLocation->setCid( $this->getId() );

		return $objAssetLocation;
	}

	public function createUnitOfMeasure() {

		$objUnitOfMeasure = new CUnitOfMeasure();
		$objUnitOfMeasure->setCid( $this->getId() );

		return $objUnitOfMeasure;
	}

	public function createUnitOfMeasureConversion() {

		$objUnitOfMeasureConversion = new CUnitOfMeasureConversion();
		$objUnitOfMeasureConversion->setCid( $this->getId() );

		return $objUnitOfMeasureConversion;
	}

	public function createLeaseExpirationStructure() {

		$objLeaseExpirationStructure = new CLeaseExpirationStructure();
		$objLeaseExpirationStructure->setDefaults();
		$objLeaseExpirationStructure->setCid( $this->getId() );

		return $objLeaseExpirationStructure;
	}

	public function createCallAnalysisScorecard() {
		$objCallAnalysisScorecard = new CCallAnalysisScorecard();
		$objCallAnalysisScorecard->setDefaults();
		$objCallAnalysisScorecard->setCid( $this->getId() );

		return $objCallAnalysisScorecard;
	}

	public function createCompanyApPaymentTypeAssociation() {

		$objCompanyApPaymentTypeAssociation = new CCompanyApPaymentTypeAssociation();
		$objCompanyApPaymentTypeAssociation->setCid( $this->getId() );

		return $objCompanyApPaymentTypeAssociation;
	}

	public function createCompanyVehicle() {

		$objCompanyVehicle = new CVehicle();
		$objCompanyVehicle->setCid( $this->getId() );
		$objCompanyVehicle->setIsActive( 1 );

		return $objCompanyVehicle;
	}

	public function createExcludedPropertyIlsPermission() {

		$objExcludedPropertyIlsPermission = new CExcludedPropertyIlsPermission();
		$objExcludedPropertyIlsPermission->setCid( $this->getId() );

		return $objExcludedPropertyIlsPermission;
	}

	public function createCompanyDepartment() {

		$objCompanyDepartment = new CCompanyDepartment();
		$objCompanyDepartment->setCid( $this->getId() );
		$objCompanyDepartment->setDefaultCompanyDepartmentId( NULL );
		$objCompanyDepartment->setIsSystem( 0 );
		$objCompanyDepartment->setOrderNum( 0 );

		return $objCompanyDepartment;
	}

	public function createGlDimension() {

		$objGlDimension = new CGlDimension();
		$objGlDimension->setCid( $this->getId() );

		return $objGlDimension;
	}

	public function createApRoutingTag() {

		$objApRoutingTag = new CApRoutingTag();
		$objApRoutingTag->setCid( $this->getId() );

		return $objApRoutingTag;
	}

	public function createCompanyMaintenanceBoard() {

		$objCompanyMaintenanceBoard = new CMaintenanceBoard();
		$objCompanyMaintenanceBoard->setCid( $this->getId() );

		return $objCompanyMaintenanceBoard;
	}

	public function createCompanyMaintenanceBoardProblem() {

		$objCompanyMaintenanceBoardProblem = new CMaintenanceBoardProblem();
		$objCompanyMaintenanceBoardProblem->setCid( $this->getId() );

		return $objCompanyMaintenanceBoardProblem;
	}

	public function createGlBook() {

		$objGlBook = new CGlBook();
		$objGlBook->setCid( $this->getId() );

		return $objGlBook;
	}

	public function createPropertyUnitMaintenanceLocation() {

		$objPropertyUnitMaintenanceLocation = new CPropertyUnitMaintenanceLocation();
		$objPropertyUnitMaintenanceLocation->setCid( $this->getId() );

		return $objPropertyUnitMaintenanceLocation;
	}

	public function createWebsiteWebsiteAdPageType() {
		$objWebsiteAdPageType = new CWebsiteAdPageType();

		$objWebsiteAdPageType->setCid( $this->getId() );

		return $objWebsiteAdPageType;
	}

	public function createCallPhoneNumber( $intCallTypeId = NULL ) {
		$objCallPhoneNumber = new CCallPhoneNumber();
		$objCallPhoneNumber->setCid( $this->getId() );
		$objCallPhoneNumber->setIvrId( NULL );
		$objCallPhoneNumber->setCallForwardPhoneNumber( NULL );

		if( true == valId( $intCallTypeId ) ) {
			$objCallPhoneNumber->setCallTypeId( $intCallTypeId );
		}

		return $objCallPhoneNumber;
	}

	public function createCompanyNotification() {
		$objCompanyNotification = new CCompanyNotification();
		$objCompanyNotification->setCid( $this->getId() );

		return $objCompanyNotification;
	}

	public function createViolationTemplate() {
		$objViolationTemplate = new CViolationTemplate();
		$objViolationTemplate->setCid( $this->getId() );

		return $objViolationTemplate;
	}

	public function createViolationTemplatePropertyGroup() {
		$objViolationTemplatePropertyGroup = new CViolationTemplatePropertyGroup();
		$objViolationTemplatePropertyGroup->setCid( $this->getId() );

		return $objViolationTemplatePropertyGroup;
	}

	public function createMaintenanceException() {

		$objMaintenanceException = new CMaintenanceException();
		$objMaintenanceException->setCid( $this->getId() );
		$objMaintenanceException->setIsPublished( 1 );

		return $objMaintenanceException;
	}

	public function createPropertyMaintenanceException() {
		$objMaintenanceExceptionPropertyGroup = new CPropertyMaintenanceException();
		$objMaintenanceExceptionPropertyGroup->setCid( $this->getId() );

		return $objMaintenanceExceptionPropertyGroup;
	}

	public function fetchWebsiteAdPageTypesByWebsiteAdRunIds( $arrintAdRunIds, $objDatabase ) {
		return CWebsiteAdPageTypes::createService()->fetchWebsiteAdPageTypesByAdRunIdsByCid( $arrintAdRunIds, $this->getId(), $objDatabase );
	}

	public function fetchWebsiteAdPageTypesByWebsiteAdId( $intWebsiteAdId, $objDatabase ) {
		return CWebsiteAdPageTypes::createService()->fetchWebsiteAdPageTypesByWebsiteAdIdByCid( $intWebsiteAdId, $this->getId(), $objDatabase );
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valCompanyStatusTypeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCompanyStatusTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_status_type_id', 'Company status is required' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyName( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( false == isset( $this->m_strCompanyName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_name', 'Company name is required.' ) );
		}

		if( true == isset( $objDatabase ) ) {
			$intConflictingClientCount = \Psi\Eos\Admin\CClients::createService()->fetchConflictingClientCountByCompanyName( $this->m_strCompanyName, $this->getId(), $objDatabase );

			if( 0 < $intConflictingClientCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_name', 'A company with this name already exists.' ) );
			}
		}

		if( false == in_array( $this->getCompanyStatusTypeId(), [ CCompanyStatusType::CANCELLED, CCompanyStatusType::EMAIL_SUBSCRIBER, CCompanyStatusType::TEST_DATABASE ] ) && true == \Psi\CStringService::singleton()->stristr( $this->getCompanyName(), 'test' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_name', 'The company name cannot use the words test unless the status of the company is a) test database b) email subscriber or c) cancelled.' ) );
		}

		return $boolIsValid;
	}

	public function valRwxDomain( $objDatabase = NULL, $boolIsDomainRequired = false, $objConnectDatabase = NULL, $objClientDatabase = NULL ) {

		$boolIsValid = true;

		if( ( CCompanyStatusType::CLIENT == $this->m_intCompanyStatusTypeId || true == $boolIsDomainRequired ) && ( false == isset( $this->m_strRwxDomain ) ) ) {
			$boolIsValid = false;
			if( true == $boolIsDomainRequired ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rwx_domain', 'Subdomain is required.' ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rwx_domain', 'RWX Sub-domain is required.' ) );
			}
		}

		if( preg_match( '/[^a-zA-Z0-9\_\-]/', $this->getRwxDomain() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rwx_domain', 'Valid entrata subdomain is required.' ) );
			$boolIsValid = false;

			return $boolIsValid;

		} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_strRwxDomain, '\\' ) ) {
			// to prevent trailing backslash in rwx domain
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rwx_domain', 'Valid entrata subdomain is required.' ) );
			$boolIsValid = false;

			return $boolIsValid;
		}

		if( true == valObj( $objDatabase, 'CDatabase' ) && 0 != CReservedSubdomains::fetchReservedSubdomainRowCount( $this->m_strRwxDomain, $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rwx_domain', 'Sub domain entered is already in use.' ) );
			$boolIsValid = false;
		}

		if( true == valObj( $objDatabase, 'CDatabase' ) && false == is_null( $this->getRwxDomain() ) ) {
			$intClientsCount = \Psi\Eos\Admin\CClients::createService()->fetchCompetingClientsCountByRwxDomainByCompetingCid( $this->m_strRwxDomain, $this->getId(), $objDatabase );

			if( 0 < $intClientsCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rwx_domain', 'Subdomain selected is already in use.' ) );
				$boolIsValid = false;

				return $boolIsValid;
			}
		}

		if( !valObj( $objConnectDatabase, 'CDatabase' ) || !valObj( $objClientDatabase, 'CDatabase' ) ) {
			return $boolIsValid;
		}

		if( true == \Psi\CStringService::singleton()->stristr( \Psi\CStringService::singleton()->strtolower( $this->m_strRwxDomain ), 'split' ) && true == isset ( $objClientDatabase ) && true == is_null( $objClientDatabase->getCid() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rwx_domain', 'Subdomain cannot contain the word \'split\'.' ) );
			$boolIsValid = false;

			return $boolIsValid;
		}

		// TODO: To validate RWX domain global user check is not required, we will keep watch on it.
		if( true == $boolIsValid && false == is_null( $this->getRwxDomain() ) && true == valObj( $objConnectDatabase, 'CDatabase' ) && 0 < $this->fetchCompetingDirectiveCount( $objConnectDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rwx_domain', 'Subdomain you selected is already in use.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valGlobalUser( $objConnectDatabase ) {

		$strUserName  = 'Admin' . '@' . $this->getRwxDomain();
		$intUserCount = \Psi\Eos\Connect\CGlobalUsers::createService()->fetchGlobalUserCountByUserName( $strUserName, $this->getId(), $objConnectDatabase );
		return $intUserCount;
	}

	public function valSwitchingStatuses( $objDatabase, $objPaymentDatabase ) {

		//  If the status type is currently "client" or "4", and the status type is changing, make sure
		//  the client does not have a positive balance.
		$boolIsValid = true;

		//  If we're activating a client
		if( CCompanyStatusType::CLIENT == $this->m_intCompanyStatusTypeId && false == in_array( $this->m_intPreviousCompanyStatusTypeId, [ CCompanyStatusType::CLIENT, CCompanyStatusType::TERMINATED ] ) ) {
			//  If the status type is currently "client" or "4", and the status type is changing, make sure
			//  the client does not have a positive balance.
			$arrobjCompanyMerchantAccounts = \Psi\Eos\Payment\CCompanyMerchantAccounts::createService()->fetchActiveCompanyMerchantAccountsByCid( $this->getId(), $objPaymentDatabase );
			$arrintElectronicArPayments    = fetchData( 'SELECT count(id) FROM ar_payments WHERE payment_status_type_id > 3 AND cid = ' . ( int ) $this->getId(), $objPaymentDatabase );

			if( ( true == valArr( $arrobjCompanyMerchantAccounts ) ) && ( true == isset ( $arrintElectronicArPayments[0]['count'] ) && 0 < ( int ) $arrintElectronicArPayments[0]['count'] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'You cannot change a company from a non-client status type to client status type if the company has merchant accounts already added, or if test payments have been made on the company.' ) );
				$boolIsValid = false;
			}
		}

		//  If we're terminating a client.
		if( CCompanyStatusType::CLIENT == $this->m_intPreviousCompanyStatusTypeId ) {
			if( CCompanyStatusType::TERMINATED == $this->m_intCompanyStatusTypeId ) {

				//  Validate to make sure there are no unterminated contracts
				$arrobjActiveContractProperties = CContractProperties::createService()->fetchActiveContractPropertiesByCid( $this->getId(), $objDatabase );

				if( true == valArr( $arrobjActiveContractProperties ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'You must terminate all contracts before terminating this client.' ) );
				}

				//  Validate to make sure there are no active company charges
				$arrobjActiveCompanyCharges = CCompanyCharges::createService()->fetchActiveCompanyChargesByCid( $this->getId(), $objDatabase, $objPagination = NULL, $boolCountOnly = false, $strSelectedAccountIds = '', $boolTotalAmount = false, [ CCompanyCharge::ACTIVE ] );

				if( true == valArr( $arrobjActiveCompanyCharges ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'You must disable all recurring charges before terminating this client.' ) );
				}

			} elseif( $this->m_intPreviousCompanyStatusTypeId != $this->m_intCompanyStatusTypeId ) {

				//  Validate to make sure there are no ar payments processed
				$intElectronicArPaymentCount = CArPayments::fetchArPaymentCount( 'WHERE payment_type_id IN ( ' . implode( ',', CPaymentType::$c_arrintElectronicPaymentTypeIds ) . ' ) AND cid = ' . ( int ) $this->getId(), $objPaymentDatabase );

				if( 0 < ( int ) $intElectronicArPaymentCount ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'You cannot revert a client to a prospect if electronic payments have been processed.' ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valDatabase( $objClientDatabase = NULL ) {

		// Don't allow adding client company into prospects database, adding prospect company into clients database.
		//  Database id '9' is for prospects database.

		$boolIsValid = true;

		if( CCompanyStatusType::CLIENT == $this->getCompanyStatusTypeId() && 9 == $this->getDatabaseId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'You cannot add a client company into a prospects database.' ) );
			$boolIsValid = false;
		}

		if( true == in_array( $this->getDatabaseId(), [ CDatabase::ENTRATA53, CDatabase::ENTRATA39, CDatabase::ENTRATA25, CDatabase::ENTRATA52, CDatabase::ENTRATA60, CDatabase::ENTRATA61, CDatabase::ENTRATA62, CDatabase::ENTRATA_STANDARD_TERMINATED, CDatabase::ENTRATA20 ] ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Entrata 20, 25, 26, 39, 52, 53, 60, 61, 62 are for specific purpose, please select different database to continue.' ) );
			$boolIsValid = false;
		}

		if( true == valObj( $objClientDatabase, 'CDatabase' ) && true == is_numeric( $objClientDatabase->getCid() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'You cannot create another company within a test company database.' ) );
			$boolIsValid = false;
		}

		if( CCompanyStatusType::TEST_DATABASE == $this->getCompanyStatusTypeId() ) {
			if( CCluster::RAPID == $this->getClusterId() && CDatabase::ENTRATA95 != $this->getDatabaseId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please select <strong> entrata95 </strong> database for Rapid test clients/companies.' ) );
				$boolIsValid = false;
			} else if( CCluster::STANDARD == $this->getClusterId() && CDatabase::ENTRATA109 != $this->getDatabaseId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please select <strong> entrata109 </strong> database for Standard test clients/companies.' ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $objPaymentDatabase = NULL, $objConnectDatabase = NULL, $objClientDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCompanyStatusTypeId();
				$boolIsValid &= $this->valCompanyName( $objDatabase );
				$boolIsValid &= $this->valRwxDomain( $objDatabase, true, $objConnectDatabase, $objClientDatabase );
				$boolIsValid &= $this->valDatabase( $objClientDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCompanyStatusTypeId();
				$boolIsValid &= $this->valCompanyName( $objDatabase );
				$boolIsValid &= $this->valRwxDomain( $objDatabase, true, $objConnectDatabase, $objClientDatabase );
				$boolIsValid &= $this->valSwitchingStatuses( $objDatabase, $objPaymentDatabase );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */
	//  Determine whether a company has opted into the Resident Portal 2.0 closed beta
	//  Once the closed beta is over, this function should be removed

	public function checkIsResidentPortal20Available( $objClientDatabase, $intPropertyId = NULL ) {

		$intWebsitePreferenceCount = \Psi\Eos\Entrata\CWebsitePreferences::createService()->fetchWebsitePreferenceCountByKeyByCid( 'SHOW_NEW_RESIDENT_PORTAL', $this->m_intId, $objClientDatabase, $intPropertyId );

		return ( 0 < $intWebsitePreferenceCount );
	}

	public function checkIsCorporateCareLineActivated( $objVoipDatabase ) {
		$strWhere = ' WHERE cid = ' . ( int ) $this->getId() . ' AND is_activate_corporate_care_line IS TRUE ';
		return \Psi\Eos\Voip\CCompanyCallSettings::createService()->fetchCompanyCallSettingCount( $strWhere, $objVoipDatabase );
	}

	public function rebuildProductPermissions( $intUserId, $objAdminDatabase, $objClientDatabase, $objLogsDatabase = NULL, $intDatabaseUserTypeId = CDatabaseUserType::JOBEXECUTOR ) {
		$objPropertyProductPermissionsLibrary = new CPropertyProductPermissionsLibrary();

		return $objPropertyProductPermissionsLibrary->rebuildCompanyPermissions( $intUserId, [ $this->getId() => $this ], $objAdminDatabase, $objClientDatabase, $objLogsDatabase, $intDatabaseUserTypeId );
	}

	public function rebuildMerchantAccounts( $intUserId, $objPaymentDatabase, $objClientDatabase = NULL, $arrintMerchantAccountIds = [] ) {
		$objMerchantAccountSyncLibrary = new CMerchantAccountSyncLibrary();

		return $objMerchantAccountSyncLibrary->rebuildMerchantAccounts( $intUserId, $this, $objPaymentDatabase, $objClientDatabase, $arrintMerchantAccountIds );
	}

	public function incrementAmountToPost( $fltAmount ) {
		$this->m_fltAmountToPost += $fltAmount;
	}

	public function insert( $intCurrentUserId, $objClientDatabase, $objAdminDatabase = NULL, $objPaymentDatabase = NULL, $arrobjConnectDatabases = NULL, $objVoipDatabase = NULL ) {
		if( false == valObj( $objClientDatabase, 'CDatabase' ) || CDatabaseType::CLIENT != $objClientDatabase->getDatabaseTypeId() || false == valObj( $objPaymentDatabase, 'CDatabase' ) || CDatabaseType::PAYMENT != $objPaymentDatabase->getDatabaseTypeId() || false == valArr( $arrobjConnectDatabases ) ) {
			trigger_error( 'Failed to load database object', E_USER_ERROR );
			exit;
		}

		if( false == is_null( $objVoipDatabase ) ) {
			if( false == valObj( $objVoipDatabase, 'CDatabase' ) || CDatabaseType::VOIP != $objVoipDatabase->getDatabaseTypeId() ) {
				trigger_error( 'Failed to load database object', E_USER_ERROR );
				exit;
			}
		}

		// ToDo: Remove this once Client creation completely moved to queue.[DSP]
		if( valObj( $objAdminDatabase, 'CDatabase' ) ) {
			$objEntity = $this->createEntity();

			if( false == $objEntity->validate( 'validate_insert_client', $objAdminDatabase ) || false == $objEntity->insert( $intCurrentUserId, $objAdminDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to add entity.' ) );

				return false;
			}

			$this->setEntityId( $objEntity->getId() );

			if( false == isset( $this->m_intId ) ) {
				$this->fetchNextId( $objAdminDatabase );
			}

			if( false == parent::insert( $intCurrentUserId, $objAdminDatabase ) ) {
				return false;
			}
		}

		$objDirective = $this->createDirective( $objClientDatabase );
		$objDirective->fetchNextId( current( $arrobjConnectDatabases ) );

		if( false == parent::insert( $intCurrentUserId, $objPaymentDatabase ) ) {
			return false;
		}
		if( false == parent::insert( $intCurrentUserId, $objClientDatabase ) ) {
			return false;
		}

		foreach( $arrobjConnectDatabases as $objConnectDatabase ) {
			// if ( false == parent::insert( $intCurrentUserId, $objConnectDatabase ) ) return false;
			if( false == $objDirective->insert( $intCurrentUserId, $objConnectDatabase ) ) {
				return false;
			}
		}

		if( true == valObj( $objVoipDatabase, 'CDatabase' ) && false == parent::insert( $intCurrentUserId, $objVoipDatabase ) ) {
			return false;
		}

		return $this->addDeploymentF5OrRoute53( $intCurrentUserId );
	}

	public function parentInsert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function insertOnAdmin( $intCurrentUserId, $objAdminDatabase ) {
		if( false == valObj( $objAdminDatabase, 'CDatabase' ) || CDatabaseType::ADMIN != $objAdminDatabase->getDatabaseTypeId() ) {
			trigger_error( 'Failed to load database object', E_USER_ERROR );
			exit;
		}

		$objEntity = $this->createEntity();

		if( false == $objEntity->validate( 'validate_insert_client', $objAdminDatabase ) || false == $objEntity->insert( $intCurrentUserId, $objAdminDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to add entity.' ) );

			return false;
		}

		$this->setEntityId( $objEntity->getId() );

		if( false == isset( $this->m_intId ) ) {
			$this->fetchNextId( $objAdminDatabase );
		}

		if( false == parent::insert( $intCurrentUserId, $objAdminDatabase ) ) {
			return false;
		}

		return true;
	}

	public function updateOnAdmin( $intCurrentUserId, $objAdminDatabase ) {

		if( false == valObj( $objAdminDatabase, 'CDatabase' ) || CDatabaseType::ADMIN != $objAdminDatabase->getDatabaseTypeId() ) {
			trigger_error( 'Failed to load database object', E_USER_ERROR );
			exit;
		}

		if( false == parent::update( $intCurrentUserId, $objAdminDatabase ) ) {
			return false;
		}
		return true;
	}

	public function resetChangedColumns( $boolIsFinal = false ) {
		if( true == $boolIsFinal ) {
			parent::resetChangedColumns();
		}
	}

	public function update( $intCurrentUserId, $objClientDatabase, $objAdminDatabase = NULL, $objPaymentDatabase = NULL, $arrobjConnectDatabases = NULL, $objVoipDatabase = NULL, $boolUpdateClientLocale = false ) {

		if( false == is_null( $objClientDatabase ) ) {
			if( false == valObj( $objClientDatabase, 'CDatabase' ) || CDatabaseType::CLIENT != $objClientDatabase->getDatabaseTypeId() ) {
				trigger_error( 'Failed to load client database object', E_USER_ERROR );
				exit;
			}
		}

		if( valObj( $objAdminDatabase, 'CDatabase' ) ) {
			if( CDatabaseType::ADMIN != $objAdminDatabase->getDatabaseTypeId() || false == valObj( $objPaymentDatabase, 'CDatabase' ) || CDatabaseType::PAYMENT != $objPaymentDatabase->getDatabaseTypeId() || ( false == $boolUpdateClientLocale && false == valArr( $arrobjConnectDatabases ) ) ) {
				trigger_error( 'Failed to load database object', E_USER_ERROR );
				exit;
			}
			if( false == parent::update( $intCurrentUserId, $objAdminDatabase ) ) {
				return false;
			}
		}

		if( false == is_null( $objVoipDatabase ) ) {
			if( false == valObj( $objVoipDatabase, 'CDatabase' ) || CDatabaseType::VOIP != $objVoipDatabase->getDatabaseTypeId() ) {
				trigger_error( 'Failed to load database object', E_USER_ERROR );
				exit;
			}
		}

		if( false == parent::update( $intCurrentUserId, $objPaymentDatabase ) ) {
			return false;
		}

		if( true == valObj( $objClientDatabase, 'CDatabase' ) && false == parent::update( $intCurrentUserId, $objClientDatabase ) ) {
			return false;
		}

		if( true == valObj( $objVoipDatabase, 'CDatabase' ) && false == parent::update( $intCurrentUserId, $objVoipDatabase ) ) {
			return false;
		}

		if( false == $boolUpdateClientLocale ) {
			foreach( $arrobjConnectDatabases as $objDatabase ) {

				$objDirective = $this->fetchDirectiveByDirectiveTypeId( CDirectiveType::CLIENT, $objDatabase );

				if( false == valObj( $objDirective, 'CDirective' ) ) {
					return false;
				}

				$objDirective->setDomain( \Psi\CStringService::singleton()->strtolower( $this->getRwxDomain() ) );
				$objDirective->setAllowDifferentialUpdate( true );

				if( false == $objDirective->update( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}
			}

			//  add deployment for refresh test client list
			$objRefreshPsiMountsMappingDeployment = new CDeployment();
			$objRefreshPsiMountsMappingDeployment->setDeploymentTypeId( CDeploymentType::MOUNT_TEST_CLIENTS_MAPPING );
			$objRefreshPsiMountsMappingDeployment->setClusterId( CCluster::RAPID );
			$objRefreshPsiMountsMappingDeployment->setDeploymentDatetime( 'NOW()' );
			$objRefreshPsiMountsMappingDeployment->setExecCommand( CDeploymentType::MOUNT_TEST_CLIENTS_MAPPING_COMMAND );
			$objRefreshPsiMountsMappingDeployment->insert( $intCurrentUserId, CDatabases::createDeployDatabase( $boolRandomizeConnections = false ) );
		}
		$this->resetChangedColumns( true );
		return true;
	}

	public function updateGlobalUsersUserName( $strOldDomainValue, $strNewDomainValue, $objDatabase ) {

		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT
						*
					FROM
						update_global_users_username( ' . $this->getId() . ', \'' . $strOldDomainValue . '\', \'' . $strNewDomainValue . '\' ) AS result';

		if( false == $objDataset->execute( $strSql ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to set global users data. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}
	return;
	}

	public function updateCompanyUsersUserName( $strOldDomainValue, $strNewDomainValue, $objDatabase, $intCurrentUserId = NULL ) {

		$strSql = 'SELECT
						*
					FROM
						update_company_users_username( ' . $this->getId() . ', \'' . $strOldDomainValue . '\', \'' . $strNewDomainValue . '\', ' . ( int ) $intCurrentUserId . ' ) AS result';

		$arrintAffectedRows = fetchData( $strSql, $objDatabase );

		if( 0 > $arrintAffectedRows[0]['result'] ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to set global users data. The following error was reported.' ) );
			return false;
		} else {
			return;
		}
	}

	public function init( $intCurrentUserId, $objDatabase = NULL, $objConnectDatabase = NULL, $objAdminDatabase, $boolPerformAdminPropertyInsert = true ) {
		if( valObj( $objDatabase, 'CDatabase' ) && valObj( $objConnectDatabase, 'CDatabase' ) ) {
			$objDataset = $objDatabase->createDataset();

			$strSql = 'SELECT * ' .
					  'FROM clients_init( ' .
					  $this->sqlId() . ', ' .
					  ( int ) $intCurrentUserId . ' ) AS result;';

			if( false == $objDataset->execute( $strSql ) ) {
				// Unset new id on error
				$this->setId( NULL );

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to initialize client record. The following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

				$objDataset->cleanup();

				return false;
			}

			if( 0 < $objDataset->getRecordCount() ) {
				$this->setId( NULL );

				while( !$objDataset->eof() ) {
					$arrmixValues = $objDataset->fetchArray();
					$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
					$objDataset->next();
				}

				$objDataset->cleanup();

				return false;
			}

			$objDataset->cleanup();

			// Default value must be 12 for key System email history limit in company preferences table.
			$objCompanyPreference = $this->createCompanyPreference();
			$objCompanyPreference->setKey( 'SYSTEM_EMAIL_HISTORY_LIMIT' );
			$objCompanyPreference->setValue( CCompanyPreference::DEFAULT_SYSTEM_EMAIL_HISTORY_LIMIT );

			if( false == $objCompanyPreference->insert( $intCurrentUserId, $objDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Insert failed for key SYSTEM_EMAIL_HISTORY_LIMIT in Company preferences.' ) );

				return false;
			}
		}

		$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchManagerialPropertyByCid( $this->getId(), $objAdminDatabase );

		if( !valObj( $objProperty, 'CProperty' ) ) {
			// Create and insert managerial properties
			$objProperty = $this->createProperty();
			$objProperty->setPropertyName( $this->getCompanyName() );
			$objProperty->setPropertyTypeId( CPropertyType::CLIENT );
			$objProperty->setTimeZoneId( CTimeZone::MOUNTAIN_STANDARD_TIME );
			$objProperty->setIsManagerial( 1 );
			$objProperty->setIsDisabled( 1 );
			$objProperty->setDisabledOn( 'NOW()' );
		}

		if( false == $objProperty->insert( $intCurrentUserId, $objDatabase, $objAdminDatabase, false, $boolPerformAdminPropertyInsert ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Managerial property failed to insert.' ) );

			return false;
		}

		if( false == $objProperty->insertBasicDetails( $intCurrentUserId, $objDatabase, $objAdminDatabase, $boolPerformAdminPropertyInsert ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Managerial property basic details failed to insert.' ) );

			return false;
		}

		return true;
	}

	public function removeWebsiteCache( $objDatabase ) {

		return true == CPortalCachingEngine::removeWebsiteCacheByCid( $this->getId(), $objDatabase );
	}

	public function loadTicketAndTaskWatchlishUsersEmailAddress( $intTaskId, $objDatabase, $boolHideDisabledData = true ) {

		$arrstrWatchlistEmailAddress = [];

		$arrobjTicketAndTaskWatchlists = \Psi\Eos\Entrata\CTicketAndTaskWatchlists::createService()->fetchCustomTicketAndTaskWatchlistsByTaskIdByCid( $intTaskId, $this->getId(), $objDatabase, $boolHideDisabledData = true, $boolIsByEmailAddress = true );

		if( true == valArr( $arrobjTicketAndTaskWatchlists ) ) {
			foreach( $arrobjTicketAndTaskWatchlists as $objTicketAndTaskWatchlist ) {
				if( false == is_null( $objTicketAndTaskWatchlist->getCompanyEmployeeEmailAddress() ) && true == CValidation::validateEmailAddresses( $objTicketAndTaskWatchlist->getCompanyEmployeeEmailAddress() ) ) {
					$arrstrWatchlistEmailAddress[$objTicketAndTaskWatchlist->getCompanyUserId()] = $objTicketAndTaskWatchlist->getNameFirst() . ' ' . $objTicketAndTaskWatchlist->getNameLast() . ' - ' . $objTicketAndTaskWatchlist->getCompanyEmployeeEmailAddress();
				}
			}
		}

		return $arrstrWatchlistEmailAddress;
	}

	public function checkIsDnrClient() {
        $objResidentVerifyLibrary = new CResidentVerifyLibrary();
        return $objResidentVerifyLibrary->isDnrClient( $this->getId() );
	}

	public function getCountryCode( $boolUseDefaultCodeAsUsaIfCountryCodeNull = false ) {

		if( valStr( parent::getCountryCode() ) ) {
			return parent::getCountryCode();
		} else if( $boolUseDefaultCodeAsUsaIfCountryCodeNull ) {
			return CCountry::CODE_USA;
		}
		return NULL;

	}

	private function addDeploymentF5OrRoute53( $intCurrentUserId ) {
		// FixMe: Route53 changes should be irrespective of cloud(Except Lindon Cloud), due to the hard coded values cloud condition is added
		if( 'production' == CONFIG_ENVIRONMENT && false == is_null( $this->getCountryCode() ) && false == in_array( $this->getCountryCode(), [ CCountry::CODE_USA, CCountry::CODE_MEXICO ] ) && CCloud::REFERENCE_ID_IRELAND == CONFIG_CLOUD_REFERENCE_ID ) {
			// ToDo: For time being few values are hard code, will replace them with config vars once config vars is added.
			$objRoute53GatewayFactor	= CCloudServiceFactory::createObject( CCloudServiceFactory::SERVICE_TYPE_AMAZON_ROUTE53 );
			$arrstrResourceRecords		= [
				'HostedZoneId' => CConfig::get( 'RP_ALIAS_HOSTED_ZONE_ID' ),
				'DNSName' => 'dualstack.web-e-weblb-14dqvja1y82u-1117056414.eu-west-1.elb.amazonaws.com.',
				'EvaluateTargetHealth' => false
			];

			$arrstrARecord 			= $objRoute53GatewayFactor->prepareChange( 'Z2SHGFBXFCN3MW', 'CREATE', $this->getRwxDomain() . '.entrata.global.', 'A', $arrstrResourceRecords, 3600, true );
			$arrstrARecordResult 	= $objRoute53GatewayFactor->callRoute53Method( 'changeResourceRecordSets', $arrstrARecord );

			if( false == $arrstrARecordResult['status'] ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Unable to add route53 \'A\' record for ' . $this->getRwxDomain() . '.entrata.global.' . ' ErrorInfo: ' . $arrstrARecordResult['status']['error'] ) );
				return false;
			}
		} else {
			// add deployment for refresh test client list
			if( true == in_array( $this->getCompanyStatusTypeId(), [ CCompanyStatusType::TEST_DATABASE, CCompanyStatusType::CANCELLED, CCompanyStatusType::TERMINATED ] ) ) {
				$objRefreshPsiMountsMappingDeployment = new CDeployment();
				$objRefreshPsiMountsMappingDeployment->setDeploymentTypeId( CDeploymentType::MOUNT_TEST_CLIENTS_MAPPING );
				$objRefreshPsiMountsMappingDeployment->setClusterId( CCluster::RAPID );
				$objRefreshPsiMountsMappingDeployment->setDeploymentDatetime( 'NOW()' );
				$objRefreshPsiMountsMappingDeployment->setExecCommand( CDeploymentType::MOUNT_TEST_CLIENTS_MAPPING_COMMAND );
				$objRefreshPsiMountsMappingDeployment->insert( $intCurrentUserId, CDatabases::createDeployDatabase( $boolRandomizeConnections = false ) );
			}
		}

		return true;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchLatestDiagnostics( $objDatabase, $intDiagnosticTypeId = NULL, $arrintDiagnosticIds = NULL, $boolIsMigration = false ) {
		return \Psi\Eos\Logs\CDiagnostics::createService()->fetchLatestDiagnosticsByDiagnosticTypeIdByCid( $intDiagnosticTypeId, $this->getId(), $objDatabase, $arrintDiagnosticIds, $boolIsMigration );
	}

	public function fetchIntegratedSpecials( $objDatabase ) {
		return \Psi\Eos\Entrata\CSpecials::createService()->fetchIntegratedSpecialsByCid( $this->getId(), $objDatabase );
	}

	public function fetchIntegratedSpecialsByPropertyIds( $arrintPropertyIds, $objDatabase, $boolIncludeDeletedSpecials = true ) {
		return \Psi\Eos\Entrata\CSpecials::createService()->fetchIntegratedSpecialsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase, $boolIncludeDeletedSpecials );
	}

	public function fetchUnitTypesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CUnitTypes::createService()->fetchUnitTypesByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPublishedUnitTypesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CUnitTypes::createService()->fetchPublishedUnitTypesByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchLeaseTermsByPropertyIds( $arrintPropertyIds, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return CLeaseTerms::fetchLeaseTermsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase, false, $boolIsReturnKeyedArray );
	}

	public function fetchCountPropertyUnitsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CPropertyUnits::createService()->fetchCountPropertyUnitsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchUnitCountPropertyUnitsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CPropertyUnits::createService()->fetchUnitCountPropertyUnitsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyUnitsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CPropertyUnits::createService()->fetchPropertyUnitsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchUnitSpacesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CUnitSpaces::createService()->fetchUnitSpacesByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchUnitSpaceByPropertyUnitIdById( $intPropertyUnitId, $intUnitSpaceId, $objDatabase ) {
		return CUnitSpaces::createService()->fetchUnitSpaceByPropertyUnitIdByIdByCid( $intPropertyUnitId, $intUnitSpaceId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyUnitByPropetyUnitId( $intPropertyUnitId, $objDatabase ) {
		return CPropertyUnits::createService()->fetchPropertyUnitByIdByCid( $intPropertyUnitId, $this->getId(), $objDatabase );
	}

	public function fetchDatabaseByDatabaseUserTypeId( $intDatabaseUserTypeId, $objDatabase ) {
		return CDatabases::fetchDatabaseByDatabaseIdByDatabaseUserTypeIdByDatabaseTypeId( $this->getDatabaseId(), $intDatabaseUserTypeId, CDatabaseType::CLIENT, $objDatabase );
	}

	public function fetchScheduledEmailById( $intScheduledEmailId, $objDatabase ) {
		return \Psi\Eos\Entrata\CScheduledEmails::createService()->fetchScheduledEmailByIdByCid( $intScheduledEmailId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyStandardResponseById( $intPropertyStandardResponseId, $objDatabase ) {
		return CPropertyStandardResponses::fetchCustomPropertyStandardResponseByIdByCid( $intPropertyStandardResponseId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyStandardResponsesByIds( $arrintIds, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		return CPropertyStandardResponses::fetchPropertyStandardResponsesByIdsByCid( $arrintIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyStandardResponseHavingMaxOrderNumByPropertyId( $intPropertyId, $objDatabase ) {
		return CPropertyStandardResponses::fetchPropertyStandardResponseHavingMaxOrderNumByPropertyIdByCid( $intPropertyId, $this->getId(), $objDatabase );
	}

	public function fetchUnexportedApplicationsByInternetListingServiceIdByApplicationDatetime( $intInternetListingServiceId, $strApplicationDatetime, $objDatabase ) {
		return \Psi\Eos\Entrata\CApplications::createService()->fetchUnexportedApplicationsByInternetListingServiceIdByApplicationDatetimeByCid( $intInternetListingServiceId, $strApplicationDatetime, $this->getId(), $objDatabase );
	}

	public function fetchCompanyTransmissionVendorsByTransmissionVendorId( $intTransmissionVendorId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyTransmissionVendors::createService()->fetchCompanyTransmissionVendorsByTransmissionVendorIdByCid( $intTransmissionVendorId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyEmployeeByNameFirstByNameLast( $strNameFirst, $strNameLast, $objDatabase ) {
		return CCompanyEmployees::createService()->fetchCompanyEmployeeByNameFirstByNameLastByCid( $strNameFirst, $strNameLast, $this->getId(), $objDatabase );
	}

	public function fetchCompanyEmployeeByNameFirstByNameLastByEmailAddress( $strNameFirst, $strNameLast, $strEmailAddress, $objDatabase ) {
		return CCompanyEmployees::createService()->fetchCompanyEmployeeByNameFirstByNameLastByEmailAddressByCid( $strNameFirst, $strNameLast, $strEmailAddress, $this->getId(), $objDatabase );
	}

	public function fetchCompanyEmployeeByEmailAddress( $strEmailAddress, $objDatabase ) {
		return CCompanyEmployees::createService()->fetchCompanyEmployeeByEmailAddressByCid( $strEmailAddress, $this->getId(), $objDatabase );
	}

	public function fetchCompanyEmployeeByCompanyUserId( $intCompanyUserId, $objDatabase ) {
		return CCompanyEmployees::createService()->fetchCompanyEmployeeByCompanyUserIdByCid( $intCompanyUserId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyEmployeeByNameFirstWithNameLastNull( $strNameFirst, $objDatabase ) {
		return CCompanyEmployees::createService()->fetchCompanyEmployeeByNameFirstWithNameLastNullByCid( $strNameFirst, $this->getId(), $objDatabase );
	}

	public function fetchLeasesByIds( $arrintLeaseIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeasesByIdsByCid( $arrintLeaseIds, $this->getId(), $objDatabase );
	}

	public function fetchViewLeasesByIds( $arrintLeaseIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchViewLeasesByIdsByCid( $arrintLeaseIds, $this->getId(), $objDatabase );
	}

	public function fetchLeaseProcessesByRefundCustomerPaymentAccountId( $intRefundCustomerPaymentAccountId, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchLeaseProcessesByRefundCustomerPaymentAccountIdByCid( $intRefundCustomerPaymentAccountId, $this->getId(), $objDatabase );
	}

	public function fetchAssignedReturnTypes( $objDatabase ) {
		return CReturnTypes::fetchAssignedReturnTypesByCid( $this->getId(), $objDatabase );
	}

	public function fetchActiveLeasesByPropertyIdsByDateByLeaseStatusTypeIds( $arrintPropertyIds, $arrintLeaseStatusTypeIds, $objDatabase, $arrintOrganizationContractIds = NULL ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchActiveLeasesPropertyIdsByDateByLeaseStatusTypeIdsByCid( $arrintPropertyIds, $arrintLeaseStatusTypeIds, $this->getId(), $objDatabase, $arrintOrganizationContractIds );
	}

	public function fetchPropertyUnitsByIds( $arrintPropertyUnitIds, $objDatabase ) {

		return CPropertyUnits::createService()->fetchPropertyUnitsByIdsByCid( $arrintPropertyUnitIds, $this->getId(), $objDatabase );
	}

	public function fetchCustomers( $objDatabase ) {

		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomCustomersByCid( $this->getId(), $objDatabase );
	}

	public function fetchSimpleCustomersByIds( $arrintCustomerIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchSimpleCustomersByIdsByCid( $arrintCustomerIds, $this->getId(), $objDatabase );
	}

	public function fetchSimpleCustomersByLeaseIds( $arrintLeaseIds, $boolIsFromPackages, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchSimpleCustomersByLeaseIdsByCid( $arrintLeaseIds, $this->getId(), $boolIsFromPackages, $objDatabase );
	}

	public function fetchPrimaryCustomersByLeaseIds( $arrintLeaseIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchPrimaryCustomersByLeaseIdsByCid( $arrintLeaseIds, $this->getId(), $objDatabase );
	}

	public function fetchCustomersByPropertyIdsKeyedByRemotePrimaryKey( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersByPropertyIdsKeyedByRemotePrimaryKeyByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchCustomersByIds( $arrintCustomerIds, $objDatabase ) {

		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomCustomersByIdsByCid( $arrintCustomerIds, $this->getId(), $objDatabase );
	}

	public function fetchCustomerPortalSettingByUsername( $strUsername, $objDatabase ) {
		return CCustomerPortalSettings::fetchCustomerPortalSettingByUsernameByCid( $strUsername, $this->getId(), $objDatabase );
	}

	public function fetchCustomerPortalSettingsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CCustomerPortalSettings::fetchCustomerPortalSettingsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchCustomerByLeaseIdById( $intLeaseId, $intCustomerId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByLeaseIdByCid( $intCustomerId, $intLeaseId, $this->getId(), $objDatabase );
	}

	public function fetchCustomerByLeaseIdByLeaseStatusTypeIdsById( $intLeaseId, $arrintLeaseStatusTypeIds, $intCustomerId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByLeaseIdByLeaseStatusTypeIdsByCid( $intCustomerId, $intLeaseId, $arrintLeaseStatusTypeIds, $this->getId(), $objDatabase );
	}

	public function fetchCompanyPreferences( $objDatabase ) {
		return CCompanyPreferences::createService()->fetchAllCompanyPreferencesByCid( $this->getId(), $objDatabase );
	}

	public function fetchCachedCompanyPreferences( $objDatabase ) {
		$this->m_arrobjCompanyPreferences = [];

		$arrstrCompanyPreferences = CCompanyPreferences::createService()->fetchCachedAllCompanyPreferencesByCid( $this->getId(), $objDatabase );

		if( true == valArr( $arrstrCompanyPreferences ) ) {
			$objCompanyPreference = $this->createCompanyPreference();

			foreach( $arrstrCompanyPreferences as $strKey => $strValue ) {
				$objCompanyPreferenceCloned = clone $objCompanyPreference;
				$objCompanyPreferenceCloned->setKey( $strKey );
				$objCompanyPreferenceCloned->setValue( $strValue );
				$this->m_arrobjCompanyPreferences[$strKey] = $objCompanyPreferenceCloned;
			}
		}

		return $this->m_arrobjCompanyPreferences;
	}

	public function fetchCompanyPreferencesByKeys( $arrstrKeys, $objDatabase ) {

		$this->m_arrobjCompanyPreferences = CCompanyPreferences::createService()->fetchCompanyPreferencesByKeysByCid( $arrstrKeys, $this->getId(), $objDatabase );

		return $this->m_arrobjCompanyPreferences;
	}

	public function fetchCompanyPreferencesValueByKeys( $arrstrKeys, $objDatabase ) {
		return CCompanyPreferences::createService()->fetchCompanyPreferencesValueByKeysByCid( $arrstrKeys, $this->getId(), $objDatabase );
	}

	public function fetchMarketingMediaAssociations( $objDatabase ) {
		return $this->fetchActiveMarketingMediaAssociationBySubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objDatabase );
	}

	public function fetchMarketingMediaAssociationById( $intMarketingMediaAssociationId, $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchMarketingMediaAssociationByIdByCid( $intMarketingMediaAssociationId, $this->getId(), $objDatabase );
	}

	public function fetchMarketingMediaAssociationsByIds( $arrintMarketingMediaAssociationIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsByIdsByCid( $arrintMarketingMediaAssociationIds, $this->getId(), $objDatabase );
	}

	public function fetchMarketingMediaAssociationByMediaSubTypeId( $intMarketingMediaSubTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getId(), CMarketingMediaType::COMPANY_PREFERENCES_DEFAULT_IMAGE, $intMarketingMediaSubTypeId, $this->getId(), $objDatabase);
	}

	public function fetchActiveMarketingMediaAssociationsByMediaSubTypeId( $intMarketingMediaSubType, $objDatabase, $boolIsSingleAssociation ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getId(), CMarketingMediaType::COMPANY_PREFERENCES_DEFAULT_IMAGE, $intMarketingMediaSubType, $this->getId(), $objDatabase, $boolIsSingleAssociation );
	}

	public function fetchActiveMarketingMediaAssociationsByReferenceIdsByMediaTypeIdByMediaSubTypeIds( $arrintReferenceIds, $intMarketingMediaTypeId, $arrintMarketingMediaSubTypeIds, $objDatabase, $strOrderBy = '' ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsByReferenceIdsByMediaTypeIdByMediaSubTypeIdsByCid( $arrintReferenceIds, $intMarketingMediaTypeId, $arrintMarketingMediaSubTypeIds, $this->getId(), $objDatabase, $strOrderBy );
	}

	public function fetchCompanyMediaFileByPropertyIdsMediaTypeId( $arrintPropertyIds, $intMediaTypeId, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByPropertyIdsByMediaTypeIdByCid( $arrintPropertyIds, $intMediaTypeId, $this->getId(), $objDatabase );
	}

	public function fetchOldCompanyMediaFileByMediaTypeIdByCompanyMediaFileId( $intCompanyMediaFileId, $intMediaTypeId, $objDatabase ) {

		return CCompanyMediaFiles::createService()->fetchOldCompanyMediaFileByMediaTypeIdByCompanyMdiaFileIdByCid( $intMediaTypeId, $intCompanyMediaFileId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyPreferenceByKey( $strKey, $objDatabase ) {

		return CCompanyPreferences::createService()->fetchCompanyPreferenceByKeyByCid( $strKey, $this->getId(), $objDatabase );
	}

	public function fetchCompanyPreferenceByKeys( $arrstrKeys, $objDatabase ) {

		return CCompanyPreferences::createService()->fetchCompanyPreferencesByKeysByCid( $arrstrKeys, $this->getId(), $objDatabase );
	}

	public function fetchAccountAccountGroups( $objDatabase ) {

		return \Psi\Eos\Admin\CAccountAccountGroups::createService()->fetchAccountAccountGroupsByCid( $this->getId(), $objDatabase );
	}

	public function fetchPrimaryAccountCount( $objDatabase ) {

		return CAccounts::createService()->fetchPrimaryAccountCountByCid( $this->getId(), $objDatabase );
	}

	public function fetchLeaseById( $intLeaseId, $objDatabase ) {

		return \Psi\Eos\Entrata\CLeases::createService()->fetchViewLeaseByIdByCid( $intLeaseId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyMediaFileById( $intCompanyMediaFileId, $objDatabase ) {

		return CCompanyMediaFiles::createService()->fetchCompanyMediaFileByIdByCid( $intCompanyMediaFileId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyMediaFolderById( $intCompanyMediaFolderId, $objDatabase ) {

		return CCompanyMediaFolders::createService()->fetchCompanyMediaFolderByIdByCid( $intCompanyMediaFolderId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyMediafilesbyCompanyMediaFolderId( $intCompanyMediaFolderId, $objDatabase ) {

		return CCompanyMediaFiles::createService()->fetchCustomCompanyMediafilesByCompanyMediaFolderIdByCid( $intCompanyMediaFolderId, $this->getId(), $objDatabase );
	}

	public function fetchVisibleCompanyMediaFilesByCompanyMediaFilesSearch( $objCompanyMediaFile, $boolSearchInAllMediaLibraryFolders, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchVisibleCompanyMediaFilesByCidByCompanyMediaFilesSearch( $this->getId(), $objCompanyMediaFile, $boolSearchInAllMediaLibraryFolders, $objDatabase );
	}

	public function fetchVisibleCompanyMediaFilesByCompanyMediaFolderId( $intCompanyMediaFolderId, $objDatabase, $intLimit = NULL, $intOffset = 0 ) {
		return CCompanyMediaFiles::createService()->fetchVisibleCompanyMediaFilesByCompanyMediaFolderIdByCid( $intCompanyMediaFolderId, $this->getId(), $objDatabase, $intLimit, $intOffset );
	}

	public function fetchCompanyMediaFilesByCompanyMediaFolderIds( $arrintChildCompanyMediaFolderIds, $objDatabase ) {
		if( false == valArr( $arrintChildCompanyMediaFolderIds ) ) {
			return NULL;
		}

		return CCompanyMediaFiles::createService()->fetchCompanyMediafilesByCompanyMediaFolderIdsByCid( $arrintChildCompanyMediaFolderIds, $this->getId(), $objDatabase );
	}

	public function fetchCompanyMediaFilesCountByTrashFolderId( $intTrashFolderId, $objDatabase ) {

		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesCountByTrashFolderIdByCid( $intTrashFolderId, $this->getId(), $objDatabase );
	}

	public function fetchPsLeadDetail( $objDatabase ) {
		return \Psi\Eos\Admin\CPsLeadDetails::createService()->fetchPsLeadDetailByCid( $this->getId(), $objDatabase );
	}

	public function fetchAmenityByNameByAmenityTypeId( $strAmenityName, $intAmenityTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CAmenities::createService()->fetchAmenityByNameByAmenityTypeIdByCid( $strAmenityName, $intAmenityTypeId, $this->getId(), $objDatabase );
	}

	public function fetchFailedCompanyPayments( $objDatabase ) {
		return \Psi\Eos\Admin\CCompanyPayments::createService()->fetchFailedCompanyPaymentsByCid( $this->getId(), $objDatabase );
	}

	public function fetchAmenityByName( $strAmenityName, $objDatabase ) {
		return \Psi\Eos\Entrata\CAmenities::createService()->fetchAmenityByNameByCid( $strAmenityName, $this->getId(), $objDatabase );
	}

	public function fetchApplication( $intApplicationId, $objDatabase ) {
		return CApplications::fetchCustomApplicationByIdByCid( $intApplicationId, $this->getId(), $objDatabase );
	}

	public function fetchCustomerReferral( $intCustomerReferralId, $objDatabase ) {
		return CCustomerReferrals::fetchCustomerReferralByIdByCid( $intCustomerReferralId, $this->getId(), $objDatabase );
	}

	public function fetchCustomerReferralsByIds( $arrintCustomerReferralIds, $objDatabase ) {
		return CCustomerReferrals::fetchCustomerReferralsByIdsByCid( $arrintCustomerReferralIds, $this->getId(), $objDatabase );
	}

	public function fetchForm( $intFormId, $objDatabase ) {
		return CForms::fetchFormByIdByCid( $intFormId, $this->getId(), $objDatabase );
	}

	public function fetchFormsByMaintenanceRequestIds( $arrintMaintenanceRequestIds, $objDatabase ) {
		return CForms::fetchFormsByMaintenanceRequestIdsByCid( $arrintMaintenanceRequestIds, $this->getId(), $objDatabase );
	}

	public function fetchSalesEmployee( $objDatabase ) {

		$strSql = 'SELECT e.* FROM ps_lead_details pld, employees e
					WHERE pld.sales_employee_id = e.id
					AND pld.cid = ' . $this->getId();

		return CEmployees::fetchEmployee( $strSql, $objDatabase );
	}

	public function fetchSupportEmployee( $objDatabase ) {

		$strSql = 'SELECT e.* FROM ps_lead_details pld, employees e
					WHERE pld.support_employee_id = e.id
					AND pld.cid = ' . $this->getId();

		return CEmployees::fetchEmployee( $strSql, $objDatabase );
	}

	public function fetchMerchantAccountApplications( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CMerchantAccountApplications::createService()->fetchMerchantAccountApplicationByCid( $this->getId(), $objPaymentDatabase );
	}

	public function fetchIntegrationClients( $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchCustomIntegrationClientsByCid( $this->getId(), $objDatabase );
	}

	public function fetchIntegrationClientKeyValues( $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClientKeyValues::createService()->fetchIntegrationClientKeyValuesByCid( $this->getId(), $objDatabase );
	}

	public function fetchIntegrationClientKeyValue( $intIntegrationClientKeyValueId, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClientKeyValues::createService()->fetchIntegrationClientKeyValueByIdByCid( $intIntegrationClientKeyValueId, $this->getId(), $objDatabase );
	}

	public function fetchIntegrationClientKeyValueByKey( $strKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClientKeyValues::createService()->fetchIntegrationClientKeyValueByKeyByCid( $strKey, $this->getId(), $objDatabase );
	}

	public function fetchAllIntegrationClientKeyValuesByKey( $strKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClientKeyValues::createService()->fetchAllIntegrationClientKeyValuesByKeyByCid( $strKey, $this->getId(), $objDatabase );
	}

	public function fetchActiveIntegrationClientsByIntegrationServiceIds( $arrintIntegrationServicesIds, $objDatabase, $boolIsFetchIdsOnly = false ) {
		return \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchActiveIntegrationClientsByIntegrationServiceIdsByCid( $arrintIntegrationServicesIds, $this->getId(), $objDatabase, $boolIsFetchIdsOnly );
	}

	public function fetchIntegrationClientTypesAssociatedToClient( $objDatabase, $boolIncludeDisabled = true ) {
		return \Psi\Eos\Entrata\CIntegrationClientTypes::createService()->fetchIntegrationClientTypesByCid( $this->getId(), $objDatabase, $boolIncludeDisabled );
	}

	public function fetchIntegrationClientTypeIds( $objDatabase, $boolIncludeDisabled = true ) {
		return \Psi\Eos\Entrata\CIntegrationClientTypes::createService()->fetchIntegrationClientTypeIdsByCid( $this->getId(), $objDatabase, $boolIncludeDisabled );
	}

	public function fetchIntegrationClientById( $intIntegrationClientId, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchIntegrationClientByIdByCid( $intIntegrationClientId, $this->getId(), $objDatabase );
	}

	public function fetchIntegrationClientsByIds( $arrintClientIntegrationIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchIntegrationClientsByIdsByCid( $arrintClientIntegrationIds, $this->getId(), $objDatabase );
	}

	public function fetchPersonByPersonTypeId( $intPersonTypeId, $objDatabase ) {

		return CPersons::createService()->fetchPersonByPersonTypeIdByCid( $intPersonTypeId, $this->getId(), $objDatabase );
	}

	public function fetchOwnerPerson( $objDatabase ) {

		return CPersons::createService()->fetchPersonByPersonTypeIdByCid( CPersonContactType::OWNER, $this->getId(), $objDatabase );
	}

	public function fetchBillingPerson( $objDatabase ) {
		return CPersons::createService()->fetchPersonByPersonTypeIdByCid( CPersonContactType::ACCOUNTING, $this->getId(), $objDatabase );
	}

	public function fetchPersonByFirstNameByLastNameByEmailAddress( $strFirstName, $strLastName, $strEmailAddress, $objDatabase ) {
		return CPersons::createService()->fetchPersonByFirstNameByLastNameByCidByEmailAddress( $strFirstName, $strLastName, $this->getId(), $strEmailAddress, $objDatabase );
	}

	public function fetchPrimaryAccount( $objDatabase ) {
		return CAccounts::createService()->fetchPrimaryAccountByCid( $this->getId(), $objDatabase );
	}

	public function fetchTransactionById( $intTransactionId, $objDatabase ) {
		return CTransactions::createService()->fetchTransactionByCidById( $this->getId(), $intTransactionId, $objDatabase );
	}

	public function fetchEftChargeById( $intEftChargeId, $objDatabase ) {
		return \Psi\Eos\Payment\CEftCharges::createService()->fetchEftChargeByCidById( $this->getId(), $intEftChargeId, $objDatabase );
	}

	public function fetchTransactionsByIds( $arrintTransactionIds, $objDatabase ) {
		return CTransactions::createService()->fetchTransactionsByCidByIds( $this->getId(), $arrintTransactionIds, $objDatabase );
	}

	public function fetchTransactionsByAccountIds( $arrintAccountIds, $objAdminDatabase, $strAccountLedgerAction = NULL, $arrmixExtendedSearch = NULL, $boolIsDownload = false, $strAccountTypeIds = NULL, $arrstrCurrenciesCodes = NULL ) {
		return CTransactions::createService()->fetchTransactionsByCidByAccountIds( $this->getId(), $arrintAccountIds, $objAdminDatabase, $strAccountLedgerAction, $arrmixExtendedSearch, $boolIsDownload, $strAccountTypeIds, $arrstrCurrenciesCodes );
	}

	public function fetchAccount( $intAccountId, $objDatabase ) {
		return CAccounts::createService()->fetchAccountByIdAndCid( $intAccountId, $this->getId(), $objDatabase );
	}

	public function fetchStandardArTriggers( $objDatabase ) {
		$arrobjArTriggers = \Psi\Eos\Entrata\CArTriggers::createService()->fetchStandardArTriggers( $objDatabase );

		$arrobjCompanyPreferences = $this->getCompanyPreferences();

		if( false == valArr( $arrobjCompanyPreferences ) ) {
			$arrobjCompanyPreferences = ( array ) $this->fetchCompanyPreferenceByKeys( [ 'USE_APPLICATION_APPROVED_CHARGES', 'USE_LEASE_APPROVED_CHARGES' ], $objDatabase );
		}

		$arrobjCompanyPreferences = rekeyObjects( 'Key', $arrobjCompanyPreferences );

		if( false == array_key_exists( 'USE_APPLICATION_APPROVED_CHARGES', $arrobjCompanyPreferences ) ) {
			unset( $arrobjArTriggers[CArTrigger::APPLICATION_APPROVAL] );
		}

		if( false == array_key_exists( 'USE_LEASE_APPROVED_CHARGES', $arrobjCompanyPreferences ) ) {
			unset( $arrobjArTriggers[CArTrigger::LEASE_APPROVAL] );
		}

		return $arrobjArTriggers;
	}

	public function fetchCustomerRelationshipsByOccupancyTypeIdsByPropertyId( $arrintOccupancyTypeIds, $intPropertyId, $objDatabase, $boolIsShowInEntrata = false ) {
		return ( array ) CCustomerRelationships::fetchCustomerRelationshipsByOccupancyTypeIdsByPropertyIdByCid( $arrintOccupancyTypeIds, $intPropertyId, $this->getId(), $objDatabase, $boolIsShowInEntrata );
	}

	public function fetchCustomerRelationships( $objDatabase ) {
		return CCustomerRelationships::fetchCustomerRelationshipsByCid( $this->getId(), $objDatabase );
	}

	public function fetchLeaseCustomersByLeaseIds( $arrintLeaseIds, $objDatabase, $boolActiveOnly = false, $boolResponsibleResidentsOnly = false ) {
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchCustomLeaseCustomersByLeaseIdsByCid( $arrintLeaseIds, $this->getId(), $objDatabase, $boolActiveOnly, $boolResponsibleResidentsOnly );
	}

	public function fetchScheduledChargesByLeaseIds( $arrintLeaseIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchCustomScheduledChargesByLeaseIdsByCid( $arrintLeaseIds, $this->getId(), $objDatabase );
	}

	public function fetchScheduledChargesByArTriggerIdByLeaseIds( $intArTriggerId, $arrintLeaseIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByArTriggerIdByLeaseIdsByCid( $intArTriggerId, $arrintLeaseIds, $this->getId(), $objDatabase );
	}

	public function fetchAccountsByAccountTypeIds( $arrintAccountTypeIds, $objDatabase, $boolExcludeMAAccounts = true, $boolExcludeIntermediaryAccounts = false, $arrintPropertyIds = NULL, $boolIsOnlyActiveAccounts = false, $arrmixCurrencyCode = NULL ) {
		return CAccounts::createService()->fetchAccountsByCidByAccountTypeIds( $this->getId(), $arrintAccountTypeIds, $objDatabase, $boolExcludeMAAccounts, NULL, $boolExcludeIntermediaryAccounts, $arrmixCurrencyCode, false, $arrintPropertyIds, $boolIsOnlyActiveAccounts );
	}

	public function fetchAccounts( $objDatabase, $boolShowDisabledData = true ) {
		return CAccounts::createService()->fetchAccountsByCidOrderByAccountName( $this->getId(), $objDatabase, $boolShowDisabledData );
	}

	public function fetchAccountDetails( $objAdminDatabase ) {
		return CAccounts::createService()->fetchAccountDetailsByCid( $this->getId(), $objAdminDatabase );
	}

	public function fetchAccountsByAccountIds( $arrintAccountIds, $objDatabase, $boolShowDisabledData = true ) {
		return CAccounts::createService()->fetchAccountsByCidByAccountIds( $this->getId(), $arrintAccountIds, $objDatabase, $boolShowDisabledData = true );
	}

	// This will return accounts of a specific payment type (e.g. ach) or an account with an already existing association to a merchant account.

	public function fetchNonIntermediaryAccountsByPaymentTypeIdsOrMerchantAssociation( $arrintPaymentTypeIds, $objDatabase, $objPaymentDatabase ) {
		return CAccounts::createService()->fetchNonIntermediaryAccountsByPaymentTypeIdsOrMerchantAssociationByCid( $this->getId(), $arrintPaymentTypeIds, $objDatabase, $objPaymentDatabase );
	}

	public function fetchNonIntermediaryAccountsByAccountTypeIdByPaymentTypeIdsOrMerchantAssociation( $arrintPaymentTypeIds, $arrintAccountTypeIds, $objDatabase, $objPaymentDatabase ) {
		return CAccounts::createService()->fetchNonIntermediaryAccountsByPaymentTypeIdsOrMerchantAssociationByCid( $this->getId(), $arrintPaymentTypeIds, $objDatabase, $objPaymentDatabase, $arrintAccountTypeIds );
	}

	public function fetchApplicationKeyValues( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CApplicationKeyValues::createService()->fetchApplicationKeyValuesByCid( $this->getId(), $objPaymentDatabase );
	}

	public function fetchAccountsWithBalancesByAccountTypeIds( $arrintAccountTypeIds, $objDatabase, $boolShowDisabledData = false, $boolShowIntermediaryAccounts = true, $objPagination = NULL, $boolReturnCount = false, $arrstrAccountsFilter = NULL, $boolExcludeMAAccounts = false, $boolSortByDelinquencyLevel = false, $arrintPropertyIds = NULL ) {
		return CAccounts::createService()->fetchAccountsWithBalancesByAccountTypeIdsByCid( $arrintAccountTypeIds, $this->getId(), $objDatabase, $boolShowDisabledData, $boolShowIntermediaryAccounts, $objPagination, $boolReturnCount, $arrstrAccountsFilter, $boolExcludeMAAccounts, $boolSortByDelinquencyLevel, $arrintPropertyIds );
	}

	public function fetchAllAccountsWithBalancesByAccountTypeIds( $objDatabase, $objPagination = NULL, $boolCountOnly = false, $boolIsDownload = false ) {
		return CAccounts::createService()->fetchAllAccountsWithBalancesByAccountTypeIdsByCid( $this->getId(), $objDatabase, $objPagination, $boolCountOnly, $boolIsDownload );
	}

	public function fetchMerchantAccountByIdByPaymentMediumId( $intPaymentMediumId, $intMerchantAccountId, $objClientDatabase ) {
		return CMerchantAccounts::fetchCustomMerchantAccountByIdByCid( $intMerchantAccountId, $this->getId(), $objClientDatabase, $intPaymentMediumId );
	}

	public function fetchLastCompanyMerchantAccount( $intPaymentMediumId, $objPaymentDatabase ) {

		return \Psi\Eos\Payment\CCompanyMerchantAccounts::createService()->fetchLastCompanyMerchantAccountByPaymentMediumIdCid( $intPaymentMediumId, $this->getId(), $objPaymentDatabase );
	}

	public function fetchCompanyMerchantAccounts( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CCompanyMerchantAccounts::createService()->fetchCompanyMerchantAccountsByCid( $this->getId(), $objPaymentDatabase );
	}

	public function fetchMerchantAccounts( $objDatabase ) {
		$arrobjMerchantAccounts = CMerchantAccounts::fetchActiveMerchantAccountsByCid( $this->getId(), $objDatabase );

		return $arrobjMerchantAccounts;
	}

	public function fetchCompanyMerchantAccountsByIds( $arrintCompanyMerchantAccountIds, $objPaymentDatabase, $intPaymentMediumId = NULL ) {

		if( false == valArr( $arrintCompanyMerchantAccountIds ) ) {
			return NULL;
		}

		return \Psi\Eos\Payment\CCompanyMerchantAccounts::createService()->fetchCompanyMerchantAccountsByCidByIds( $this->getId(), $arrintCompanyMerchantAccountIds, $objPaymentDatabase, $intPaymentMediumId );
	}

	public function fetchEligibleCompanyStatusTypes( $objAdminDatabase ) {

		$arrintElibleCompanyStatusTypeIds = [];

		switch( $this->m_intCompanyStatusTypeId ) {

			case NULL:
				$arrintElibleCompanyStatusTypeIds = [ CCompanyStatusType::PROSPECT, CCompanyStatusType::CLIENT, CCompanyStatusType::TEST_DATABASE, CCompanyStatusType::SALES_DEMO ];
				break;

			case CCompanyStatusType::PROSPECT:
				$arrintElibleCompanyStatusTypeIds = [ CCompanyStatusType::PROSPECT, CCompanyStatusType::CLIENT, CCompanyStatusType::CANCELLED ];
				break;

			case CCompanyStatusType::TERMINATED:
			case CCompanyStatusType::CLIENT:
				$arrintElibleCompanyStatusTypeIds = [ CCompanyStatusType::CLIENT, CCompanyStatusType::TERMINATED, CCompanyStatusType::PROSPECT ];
				break;

			case CCompanyStatusType::SALES_DEMO:
			case CCompanyStatusType::TEST_DATABASE:
				$arrintElibleCompanyStatusTypeIds = [ CCompanyStatusType::TEST_DATABASE, CCompanyStatusType::SALES_DEMO ];
				break;

			case CCompanyStatusType::CANCELLED:
				$arrintElibleCompanyStatusTypeIds = [ CCompanyStatusType::PROSPECT, CCompanyStatusType::CLIENT, CCompanyStatusType::CANCELLED ];
				break;

			case CCompanyStatusType::EMAIL_SUBSCRIBER:
				$arrintElibleCompanyStatusTypeIds = [ CCompanyStatusType::PROSPECT, CCompanyStatusType::CLIENT, CCompanyStatusType::CANCELLED, CCompanyStatusType::EMAIL_SUBSCRIBER ];
				break;

			default:
				//  default case goes here
		}

		return CCompanyStatusTypes::fetchCompanyStatusTypesByIds( $arrintElibleCompanyStatusTypeIds, $objAdminDatabase );
	}

	public function fetchMerchantAccountsByIds( $arrintMerchantAccountIds, $objClientDatabase, $intPaymentMediumId = NULL, $strOrderBy = NULL ) {

		if( false == valArr( $arrintMerchantAccountIds ) ) {
			return NULL;
		}

		return CMerchantAccounts::fetchViewMerchantAccountsByIdsByCid( $arrintMerchantAccountIds, $this->getId(), $objClientDatabase, $intPaymentMediumId, $strOrderBy );
	}

	public function fetchActiveMerchantAccountsByIds( $arrintCompanyMerchantAccountIds, $objClientDatabase, $intPaymentMediumId = NULL ) {

		if( false == valArr( $arrintCompanyMerchantAccountIds ) ) {
			return NULL;
		}

		return CMerchantAccounts::fetchActiveMerchantAccountsByIdsByCid( $arrintCompanyMerchantAccountIds, $this->getId(), $objClientDatabase, $intPaymentMediumId );
	}

	public function fetchActiveCompanyMerchantAccounts( $objPaymentDatabase, $arrstrCountryCodes = NULL ) {
		return \Psi\Eos\Payment\CCompanyMerchantAccounts::createService()->fetchActiveCompanyMerchantAccountsByCid( $this->getId(), $objPaymentDatabase, NULL, $arrstrCountryCodes );
	}

	public function fetchActiveMerchantAccounts( $objClientDatabase, $intPaymentMediumId = NULL, $strOrderBy = 'account_name' ) {
		return CMerchantAccounts::fetchActiveMerchantAccountsByCid( $this->getId(), $objClientDatabase, $intPaymentMediumId, $strOrderBy );
	}

	public function fetchActiveMerchantAccountsByPropertyIds( $arrintPropertyIds, $objClientDatabase, $intPaymentMediumId = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		return CMerchantAccounts::fetchActiveMerchantAccountsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objClientDatabase, $intPaymentMediumId );
	}

	public function fetchInternationalMerchantAccounts( $objClientDatabase ) {
		return CMerchantAccounts::fetchInternationalMerchantAccounts( $this->getId(), $objClientDatabase, NULL, NULL );
	}

	public function fetchMerchantAccountsByPropertyId( $objDatabase, $intPropertyId = NULL ) {
		return CMerchantAccounts::fetchMerchantAccountsByPropertyIdByCid( $this->getId(), $objDatabase, $intPropertyId );
	}

	public function fetchPropertyFloorplanById( $intPropertyFloorplanId, $objDatabase ) {
		return CPropertyFloorplans::createService()->fetchPropertyFloorplanByIdByCid( $intPropertyFloorplanId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyFloorplansByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CPropertyFloorplans::createService()->fetchCustomPropertyFloorplansByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchTransactions( $objDatabase, $intOffset = NULL, $intLimit = NULL, $strAction = NULL, $arrmixExtendedSearch = NULL, $boolIsDownload = false, $strAccountTypeIds = NULL ) {
		return CTransactions::createService()->fetchTransactionsByCid( $this->getId(), $objDatabase, $intOffset, $intLimit, $strAction, $arrmixExtendedSearch, $boolIsDownload, $strAccountTypeIds );
	}

	public function fetchCustomPaginatedTransactions( $objDatabase, $objPagination = NULL, $arrintSelectedAccounts = NULL, $boolCountOnly = false ) {
		return CTransactions::createService()->fetchCustomPaginatedTransactionsByCid( $this->getId(), $objDatabase, $objPagination, $arrintSelectedAccounts, $boolCountOnly );
	}

	public function fetchInvoiceSummaryTransactionsByAccountIds( $arrintAccountIds, $objDatabase ) {
		return CTransactions::createService()->fetchInvoiceSummaryTransactionsByCidByAccountIds( $this->getId(), $arrintAccountIds, $objDatabase );
	}

	public function fetchPaginatedEftCharges( $objPagination, $objDatabase ) {
		return \Psi\Eos\Payment\CEftCharges::createService()->fetchPaginatedEftChargesByCid( $this->getId(), $objPagination, $objDatabase );
	}

	public function fetchTransactionsCount( $objDatabase ) {
		return CTransactions::createService()->fetchCountByCid( $this->getId(), $objDatabase );
	}

	public function fetchEftChargesCount( $objDatabase ) {
		return \Psi\Eos\Payment\CEftCharges::createService()->fetchEftChargesCountByCid( $this->getId(), $objDatabase );
	}

	public function fetchPaginatedSettlementDistributions( $intPageNo, $intPageSize, $objPaymentDatabase, $objSettlementDistributionsFilter = NULL ) {
		return \Psi\Eos\Payment\CSettlementDistributions::createService()->fetchPaginatedSettlementDistributionsByCid( $this->getId(), $intPageNo, $intPageSize, $objPaymentDatabase, $objSettlementDistributionsFilter );
	}

	public function fetchPaginatedSettlementDistributionsCount( $objPaymentDatabase, $objSettlementDistributionsFilter = NULL ) {
		return \Psi\Eos\Payment\CSettlementDistributions::createService()->fetchPaginatedSettlementDistributionsCountByCid( $this->getId(), $objPaymentDatabase, $objSettlementDistributionsFilter );
	}

	public function fetchPaginatedSettlementDistributionsByPropertyIds( $arrintPropertyIds, $objPaymentDatabase, $objSettlementDistributionsFilter = NULL, $boolIsAdministrator = false ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return false;
		}
		$arrobjTransactions = \Psi\Eos\Payment\CSettlementDistributions::createService()->fetchPaginatedSettlementDistributionsByCidByPropertyIds( $this->getId(), $arrintPropertyIds, $objPaymentDatabase, $objSettlementDistributionsFilter, $boolIsAdministrator );

		return $arrobjTransactions;
	}

	public function fetchNewPaginatedArPaymentsByPropertyIds( $arrintPropertyIds, $intPageNo, $intPageSize, $objArPaymentsFilter, $objDatabase, $boolSortAscending = false, $objPaymentDatabase = NULL ) {
		return CArPayments::fetchNewPaginatedArPaymentsByPropertyIdsByCid( $arrintPropertyIds, $intPageNo, $intPageSize, $objArPaymentsFilter, $this->getId(), $objDatabase, $boolSortAscending, $objPaymentDatabase );
	}

	public function fetchNewNonIntegratedNonFloatingArPaymentsByPropertyIds( $arrintPropertyIds, $objDatabase, $objArPaymentsFilter = NULL ) {
		return CArPayments::fetchNewNonIntegratedNonFloatingArPaymentsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase, $objArPaymentsFilter );
	}

	public function fetchNewNonIntegratedFloatingArPaymentsByPropertyIds( $arrintPropertyIds, $objDatabase, $objArPaymentsFilter = NULL ) {
		return CArPayments::fetchNewNonIntegratedFloatingArPaymentsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase, $objArPaymentsFilter );
	}

	public function fetchPaginatedCustomerReferralsByPropertyIds( $arrintPropertyIds, $intPageNo, $intPageSize, $objDatabase, $boolIsRemovedCustomerReferrals = NULL ) {
		return CCustomerReferrals::fetchPaginatedCustomerReferralsByPropertyIdsByCid( $intPageNo, $intPageSize, $arrintPropertyIds, $this->getId(), $objDatabase, $boolIsRemovedCustomerReferrals );
	}

	public function fetchPaginatedCustomerReferralsCountByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CCustomerReferrals::fetchPaginatedCustomerReferralsCountByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPaginatedInvoices( $objPagination, $objDatabase, $strSelectedAccounts = '', $boolUnpaidInvoices = false ) {
		return CInvoices::createService()->fetchPaginatedInvoicesByCid( $this->getId(), $objPagination, $objDatabase, $strSelectedAccounts, $boolUnpaidInvoices );
	}

	public function fetchPaginatedInvoiceDetails( $objPagination, $objDatabase, $strSelectedAccounts = '', $boolUnpaidInvoices = false, $boolIsCount = false ) {
		return CInvoices::createService()->fetchPaginatedInvoiceDetailsByCid( $this->getId(), $objPagination, $objDatabase, $strSelectedAccounts, $boolUnpaidInvoices, $boolIsCount );
	}

	public function fetchPaginatedInvoicesCount( $objDatabase, $objPagination = NULL, $strSelectedAccounts = '', $boolUnpaidInvoices = false ) {
		return CInvoices::createService()->fetchPaginatedInvoicesCountByCid( $this->getId(), $objDatabase, $objPagination, $strSelectedAccounts, $boolUnpaidInvoices );
	}

	public function fetchPublishedPropertyAddressesByPropertyIdsByAddressTypeId( $arrintPropertyIds, $intAddressTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyAddresses::createService()->fetchPublishedPropertyAddressesByPropertyIdsAddressTypeIdByCid( $arrintPropertyIds, $intAddressTypeId, $this->getId(), $objDatabase );
	}

	public function fetchPublishedPropertyPhoneNumbersByPropertyIdsByPhoneNumberTypeId( $arrintPropertyIds, $intPhoneNumberTypeId, $objDatabase ) {
		return CPropertyPhoneNumbers::fetchPropertyPhoneNumbersByPropertyIdsByPhoneNumberTypeIdsByCid( $arrintPropertyIds, [ $intPhoneNumberTypeId ], $this->getId(), $objDatabase );
	}

	public function fetchWebVisiblePropertyPhoneNumbersByPropertyIdsByPhoneNumberTypeIds( $arrintPropertyIds, $arrintPhoneNumberTypeId, $objDatabase ) {
		return CPropertyPhoneNumbers::fetchWebVisiblePropertyPhoneNumbersByPropertyIdsByPhoneNumberTypeIdsByCid( $arrintPropertyIds, $arrintPhoneNumberTypeId, $this->getId(), $objDatabase );
	}

	public function fetchOutstandingTransactions( $objDatabase ) {
		return CTransactions::createService()->fetchOutstandingTransactionsByCid( $this->getId(), $objDatabase );
	}

	public function fetchActiveCompanyCharges( $objDatabase, $objPagination = NULL, $boolCountOnly = false, $strSelectedAccounts = '', $boolTotalAmount = false, $arrintActiveCompanyChargeStatus = NULL, $boolIsDownload = false, $boolIsFromAccountDetails = false ) {
		return CCompanyCharges::createService()->fetchBillDirectActiveCompanyChargesByCid( $this->getId(), $objDatabase, $objPagination, $boolCountOnly, $strSelectedAccounts, $boolTotalAmount, $arrintActiveCompanyChargeStatus, $boolIsDownload, $boolIsFromAccountDetails );
	}

	public function fetchCurrentCompanyCharges( $objDatabase ) {

		return CCompanyCharges::createService()->fetchCurrentCompanyChargesByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyAdminUsers( $objDatabase ) {
		return  \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyAdminUsersByCid( $this->getId(), $objDatabase );
	}

	public function fetchPsProductIdsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CPropertyProducts::createService()->fetchPsProductIdsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPsProductPermissionsByPropertyIds( $arrintPsProductIds, $arrintPropertyIds, $objDatabase ) {
		return CPropertyProducts::createService()->fetchPsProductPermissionsByPsProductIdsByPropertyIdsByCid( $arrintPsProductIds, $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyProducts( $objDatabase ) {
		return CPropertyProducts::createService()->fetchPropertyProductsByCid( $this->getId(), $objDatabase );
	}

	public function fetchPropertyProductsByPsProductId( $intPsProductId, $objDatabase ) {
		return CPropertyProducts::createService()->fetchPropertyProductsByPsProductIdByCid( $intPsProductId, $this->getId(), $objDatabase );
	}

	public function fetchUnlimitedPsProducts( $objDatabase ) {

		//  Get unlimited prodocuts based on client, not based on this specific contract!
		$arrintUnlimitedProducts = [];
		$arrmixPsProductsData    = CContractProperties::createService()->fetchAllUnlimitedPsProductsByCid( $this->getId(), $objDatabase );

		if( true == valArr( $arrmixPsProductsData ) ) {
			foreach( $arrmixPsProductsData as $arrmixPsProduct ) {
				$arrintUnlimitedProducts[$arrmixPsProduct['ps_product_id']] = $arrmixPsProduct['ps_product_id'];
			}
		}

		return $arrintUnlimitedProducts;
	}

	public function fetchPsProductIds( $objDatabase ) {
		return CPropertyProducts::createService()->fetchPsProductIdsByCid( $this->getId(), $objDatabase );
	}

	public function fetchLastUpdatedAccount( $objDatabase ) {

		return CAccounts::createService()->fetchLastUpdatedAccountByCid( $this->getId(), $objDatabase );
	}

	public function fetchCustomerByIdLeaseId( $intCustomerId, $intLeaseId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByLeaseIdByIdByCid( $intLeaseId, $intCustomerId, $this->getId(), $objDatabase );
	}

	public function fetchCustomerById( $intCustomerId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $intCustomerId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyById( $intPropertyId, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $intPropertyId, $this->getId(), $objDatabase );
	}

	public function fetchWebsiteById( $intWebsiteId, $objDatabase ) {
		return CWebsites::createService()->fetchWebsiteByIdByCid( $intWebsiteId, $this->getId(), $objDatabase );
	}

	public function fetchProperty( $intPropertyId, $intCompanyUserId, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByCompanyUserIdByPropertyIdByCid( $intCompanyUserId, $intPropertyId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyByIdBySessionData( $intPropertyId, $intIsDisabledSessionData, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdBySessionDataByCid( $intPropertyId, $intIsDisabledSessionData, $this->getId(), $objDatabase );
	}

	public function fetchUnpermissionedProperty( $intPropertyId, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $intPropertyId, $this->getId(), $objDatabase );
	}

	public function fetchIntegratedPropertiesByIntegrationClientTypeIds( $arrintIntegrationClientTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchAssociatedPropertiesByIntegrationClientTypeIdsByCid( $arrintIntegrationClientTypeIds, $this->getId(), $objDatabase );
	}

	public function fetchScheduledPaymentById( $intScheduledPaymentId, $objDatabase ) {
		return CScheduledPayments::fetchScheduledPaymentByIdByCid( $intScheduledPaymentId, $this->getId(), $objDatabase );
	}

	public function fetchAmenitiesByAmenityTypeId( $intAmenityTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CAmenities::createService()->fetchAmenitiesByAmenityTypeIdByCid( $intAmenityTypeId, $this->getId(), $objDatabase );
	}

	public function fetchUnitSpaceAmenityRateAssociations( $objDatabase ) {
		return \Psi\Eos\Entrata\Custom\CAmenityRateAssociations::createService()->fetchUnitSpaceAmenityRateAssociationsByCid( $this->getId(), $objDatabase );
	}

	public function fetchUnitSpace( $intUnitSpaceId, $objDatabase ) {
		return CUnitSpaces::createService()->fetchCustomUnitSpaceByIdByCid( $intUnitSpaceId, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceProblems( $objDatabase ) {
		return CMaintenanceProblems::createService()->fetchCustomMaintenanceProblemsByCid( $this->getId(), $objDatabase );
	}

	public function fetchAllMaintenanceProblems( $objDatabase ) {
		return CMaintenanceProblems::createService()->fetchAllMaintenanceProblemsByCid( $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceProblem( $intMaintenanceProblemId, $objDatabase ) {
		return CMaintenanceProblems::createService()->fetchMaintenanceProblemByIdByCid( $intMaintenanceProblemId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyMaintenanceProblemDropdownData( $boolIsIntegrated, $boolIsFromResidentPortal, $objDatabase, $strProblemType = CMaintenanceProblem::INCLUDE_IN_BOTH, $boolIsSortByProblem = false ) {
		return CMaintenanceProblems::createService()->fetchCompanyMaintenanceProblemDropdownDataByCid( $this->getId(), $boolIsIntegrated, $boolIsFromResidentPortal, $objDatabase, $strProblemType, $boolIsSortByProblem );
	}

	public function fetchMaintenanceProblemBySystemCode( $strSystemCode, $objDatabase ) {
		return CMaintenanceProblems::createService()->fetchMaintenanceProblemBySystemCodeByCid( $strSystemCode, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceLocationBySystemCode( $strSystemCode, $objDatabase ) {
		return CMaintenanceLocations::createService()->fetchMaintenanceLocationBySystemCodeByCid( $strSystemCode, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceProblemById( $intMaintenanceProblemId, $objDatabase ) {
		return CMaintenanceProblems::createService()->fetchMaintenanceProblemByIdByCid( $intMaintenanceProblemId, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceStatuses( $objDatabase, $boolIsExcludeClosedStatuses = false ) {
		return CMaintenanceStatuses::createService()->fetchCustomMaintenanceStatusesByCid( $this->getId(), $objDatabase, NULL, $boolIsExcludeClosedStatuses );
	}

	public function fetchMaintenanceStatusesByMaintenanceStatusIds( $arrintMaintenanceStatusIds, $objDatabase ) {
		return CMaintenanceStatuses::createService()->fetchMaintenanceStatusesByCidByIds( $this->getId(), $arrintMaintenanceStatusIds, $objDatabase );
	}

	public function fetchMaintenancePriorityById( $intMaintenancePriorityId, $objDatabase ) {
		return CMaintenancePriorities::createService()->fetchMaintenancePriorityByCidById( $this->getId(), $intMaintenancePriorityId, $objDatabase );
	}

	public function fetchMaintenancePriorities( $objDatabase, $boolIsOrderByOrderNum = false ) {
		return CMaintenancePriorities::createService()->fetchCustomMaintenancePrioritiesByCid( $this->getId(), $objDatabase, $boolIsOrderByOrderNum );
	}

	public function fetchMaintenancePrioritiesByMaintenancePriorityIds( $arrintMaintenancePriorityIds, $objDatabase ) {
		return CMaintenancePriorities::createService()->fetchMaintenancePrioritiesByCidByIds( $this->getId(), $arrintMaintenancePriorityIds, $objDatabase );
	}

	public function fetchMaintenanceLocations( $objDatabase ) {
		return CMaintenanceLocations::createService()->fetchCustomMaintenanceLocationsByCid( $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceLocationsWithIsAppliedToProperty( $objDatabase ) {
		return CMaintenanceLocations::createService()->fetchMaintenanceLocationsByIsAppliedToPropertyByCid( 1, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceProblemsWithIsAppliedToProperty( $objDatabase ) {
		return CMaintenanceProblems::createService()->fetchMaintenanceProblemsByIsAppliedToPropertyByCid( 1, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceProblemsWithClient( $objDatabase ) {
		return CMaintenanceProblems::createService()->fetchMaintenanceProblemsByCid( $this->getId(), $objDatabase );
	}

	public function fetchPublishedMaintenanceProblems( $objDatabase, $arrstrSystemCodesFilterForCategories = NULL ) {
		return CMaintenanceProblems::createService()->fetchPublishedMaintenanceProblemsByCid( $this->getId(), $objDatabase, $arrstrSystemCodesFilterForCategories );
	}

	public function fetchPublishedMaintenanceStatuses( $objDatabase, $intMaintenanceStatusTypeId = NULL ) {
		return CMaintenanceStatuses::createService()->fetchPublishedMaintenanceStatusesByCid( $this->getId(), $objDatabase, $intMaintenanceStatusTypeId );
	}

	public function fetchSimplePublishedMaintenanceStatuses( $objDatabase, $boolIsPublished = true ) {
		return CMaintenanceStatuses::createService()->fetchSimplePublishedMaintenanceStatusesByCid( $this->getId(), $objDatabase, $boolIsPublished );
	}

	public function fetchPublishedMaintenancePriorities( $objDatabase ) {
		return CMaintenancePriorities::createService()->fetchPublishedMaintenancePrioritiesByCid( $this->getId(), $objDatabase );
	}

	public function fetchSimplePublishedMaintenancePriorities( $objDatabase ) {
		return CMaintenancePriorities::createService()->fetchSimplePublishedMaintenancePrioritiesByCid( $this->getId(), $objDatabase );
	}

	public function fetchPublishedMaintenanceLocations( $objDatabase, $arrstrSystemCodesFilterForLocations = NULL, $boolIsFromResidentPortal = false ) {
		return CMaintenanceLocations::createService()->fetchPublishedMaintenanceLocationsByCid( $this->getId(), $objDatabase, $arrstrSystemCodesFilterForLocations, $boolIsFromResidentPortal );
	}

	public function fetchSimplePublishedMaintenanceLocationsBySystemCodeFilterIds( $objDatabase, $arrstrSystemCodesFilterForLocations = NULL, $boolIsFromResidentPortal = false ) {
		return CMaintenanceLocations::createService()->fetchSimplePublishedMaintenanceLocationsBySystemCodeFilterIdsByCid( $this->getId(), $objDatabase, $arrstrSystemCodesFilterForLocations, $boolIsFromResidentPortal );
	}

	public function fetchSimplePublishedMaintenanceLocations( $objDatabase, $intMaintenanceLocationTypeId = NULL ) {
		return CMaintenanceLocations::createService()->fetchSimplePublishedMaintenanceLocationsByCid( $this->getId(), $objDatabase, $intMaintenanceLocationTypeId );
	}

	public function fetchSimpleMaintenanceRequestsByMaintenanceRequestIdsByMaintenanceProblemIdByPropertyId( $arrintMakeReadyMaintenanceRequestIds, $intMaintenanceProblemId, $intPropertyId, $objDatabase ) {
		return CMaintenanceRequests::createService()->fetchSimpleMaintenanceRequestsByMaintenanceRequestIdsByMaintenanceProblemIdByPropertyIdByCid( $arrintMakeReadyMaintenanceRequestIds, $intMaintenanceProblemId, $intPropertyId, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceStatusByCode( $strCode, $objDatabase ) {
		return CMaintenanceStatuses::createService()->fetchMaintenanceStatusByCidByCode( $this->getId(), $strCode, $objDatabase );
	}

	public function fetchMaintenanceStatusByMaintenanceStatusTypeId( $intMaintenanceStatusTypeId, $objDatabase ) {
		return CMaintenanceStatuses::createService()->fetchMaintenanceStatusByMaintenanceStatusTypeIdByCid( $intMaintenanceStatusTypeId, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceRequestById( $intMaintenanceRequestId, $objDatabase ) {
		return CMaintenanceRequests::createService()->fetchCustomMaintenanceRequestByIdByCid( $intMaintenanceRequestId, $this->getId(), $objDatabase );
	}

	public function fetchRecentlyCreatedParentMakeReadyMaintenanceRequestsForMakeReadyBoardByPropertyUnitTypeIdByPropertyBuildingIdByPropertyId( $intPropertyUnitTypeId, $intPropertyBuildingId, $intPropertyId, $objDatabase ) {
		return CMaintenanceRequests::createService()->fetchRecentlyCreatedParentMakeReadyMaintenanceRequestsForMakeReadyBoardByPropertyUnitTypeIdByPropertyBuildingIdByPropertyIdByCid( $intPropertyUnitTypeId, $intPropertyBuildingId, $intPropertyId, $this->getId(), $objDatabase );
	}

	public function fetchRecentlyUpdatedMakeReadyMaintenanceRequestsForMakeReadyBoardByPropertyUnitTypeIdByPropertyBuildingIdByPropertyId( $intPropertyUnitTypeId, $intPropertyBuildingId, $intPropertyId, $objDatabase ) {
		return CMaintenanceRequests::createService()->fetchRecentlyUpdatedMakeReadyMaintenanceRequestsForMakeReadyBoardByPropertyUnitTypeIdByPropertyBuildingIdByPropertyIdByCid( $intPropertyUnitTypeId, $intPropertyBuildingId, $intPropertyId, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceRequestByIds( $arrintMaintenanceRequestIds, $objDatabase, $boolIsShowSubTasks = false ) {
		return CMaintenanceRequests::createService()->fetchMaintenanceRequestsByIdsByCid( $arrintMaintenanceRequestIds, $this->getId(), $objDatabase, $boolIsShowSubTasks );
	}

	public function fetchArCodes( $objClientDatabase, $objArCodesFilter = NULL, $boolShowPropertyLedgerFilterArCode = false ) {
		$this->m_arrobjArCodes = \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodesByCidByArCodesFilter( $this->getId(), $objArCodesFilter, $objClientDatabase, $boolIsExactMatch = true, $boolShowPropertyLedgerFilterArCode );

		return $this->m_arrobjArCodes;
	}

	public function fetchArCodesByIds( $arrintArCodeIds, $objDatabase ) {

		require_once PATH_LIBRARIES_PSI . 'SearchFilters/CArCodesFilter.class.php';

		$objArCodesFilter = new CArCodesFilter();
		$objArCodesFilter->setArCodeIds( $arrintArCodeIds );

		return \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodesByCidByArCodesFilter( $this->getId(), $objArCodesFilter, $objDatabase );

	}

	public function fetchArCodeByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		require_once PATH_LIBRARIES_PSI . 'SearchFilters/CArCodesFilter.class.php';

		$objArCodesFilter = new CArCodesFilter();
		$objArCodesFilter->setRemotePrimaryKey( $strRemotePrimaryKey );

		return \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByCidByArCodesFilter( $this->getId(), $objArCodesFilter, $objDatabase );
	}

	public function fetchArCodeByRemotePrimaryKeyByIntegrationDatabaseId( $strRemotePrimaryKey, $intIntegrationDatabaseId, $objDatabase ) {
		require_once PATH_LIBRARIES_PSI . 'SearchFilters/CArCodesFilter.class.php';

		$objArCodesFilter = new CArCodesFilter();
		$objArCodesFilter->setRemotePrimaryKey( $strRemotePrimaryKey );
		$objArCodesFilter->setIntegrationDatabaseId( $intIntegrationDatabaseId );

		return \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByCidByArCodesFilter( $this->getId(), $objArCodesFilter, $objDatabase );
	}

	public function fetchArCodeByRemotePrimaryKeyWithIntegrationDatabaseIdNull( $strRemotePrimaryKey, $objDatabase ) {
		require_once PATH_LIBRARIES_PSI . 'SearchFilters/CArCodesFilter.class.php';

		$objArCodesFilter = new CArCodesFilter();
		$objArCodesFilter->setRemotePrimaryKey( $strRemotePrimaryKey );
		$objArCodesFilter->setIntegrationDatabasePreferenceId( CArCodesFilter::INTEGRATION_DATABASE_PREFERENCE_NULL );

		return \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByCidByArCodesFilter( $this->getId(), $objArCodesFilter, $objDatabase );
	}

	public function fetchArCodeByDefaultArCodeId( $intDefaultArCodeId, $objDatabase ) {
		require_once PATH_LIBRARIES_PSI . 'SearchFilters/CArCodesFilter.class.php';

		$objArCodesFilter = new CArCodesFilter();
		$objArCodesFilter->setShowDisabled( 1 );
		$objArCodesFilter->setDefaultArCodeIds( [ $intDefaultArCodeId ] );
		$objArCodesFilter->setShowDisabled( true );

		return \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByCidByArCodesFilter( $this->getId(), $objArCodesFilter, $objDatabase );
	}

	public function fetchArCodeByNameByIntegrationDatabaseId( $strName, $intIntegrationDatabaseId, $objDatabase ) {
		require_once PATH_LIBRARIES_PSI . 'SearchFilters/CArCodesFilter.class.php';

		$objArCodesFilter = new CArCodesFilter();
		$objArCodesFilter->setIntegrationDatabaseId( $intIntegrationDatabaseId );
		$objArCodesFilter->setName( $strName );

		return \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByCidByArCodesFilter( $this->getId(), $objArCodesFilter, $objDatabase );
	}

	public function fetchArCodeByNameByIntegrationDatabaseIdNull( $strName, $objDatabase ) {
		require_once PATH_LIBRARIES_PSI . 'SearchFilters/CArCodesFilter.class.php';

		$objArCodesFilter = new CArCodesFilter();
		$objArCodesFilter->setName( $strName );
		$objArCodesFilter->setIntegrationDatabasePreferenceId( CArCodesFilter::INTEGRATION_DATABASE_PREFERENCE_NULL );

		return \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByCidByArCodesFilter( $this->getId(), $objArCodesFilter, $objDatabase );
	}

	public function fetchArCodeByName( $strName, $objDatabase ) {
		require_once PATH_LIBRARIES_PSI . 'SearchFilters/CArCodesFilter.class.php';

		$objArCodesFilter = new CArCodesFilter();
		$objArCodesFilter->setName( $strName );

		return \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByCidByArCodesFilter( $this->getId(), $objArCodesFilter, $objDatabase );
	}

	public function fetchArCodeById( $intArCodeId, $objDatabase ) {
		require_once PATH_LIBRARIES_PSI . 'SearchFilters/CArCodesFilter.class.php';

		$objArCodesFilter = new CArCodesFilter();
		$objArCodesFilter->setArCodeIds( [ ( int ) $intArCodeId ] );

		return \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByCidByArCodesFilter( $this->getId(), $objArCodesFilter, $objDatabase );
	}

	public function fetchActiveBankAccounts( $objDatabase ) {
		return CBankAccounts::fetchActiveBankAccountsByCid( $this->getId(), $objDatabase );
	}

	public function fetchActiveBankAccountsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CBankAccounts::fetchActiveBankAccountsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchAllBankAccountsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CBankAccounts::fetchAllBankAccountsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyIdsByBankAccountId( $intBankAccountId, $objDatabase ) {
		return CPropertyBankAccounts::fetchPropertyIdsByBankAccountIdByCid( $intBankAccountId, $this->getId(), $objDatabase );
	}

	public function fetchContactSubmissionTypes( $objDatabase ) {
		return CContactSubmissionTypes::fetchContactSubmissionTypesByCid( $this->getId(), $objDatabase );
	}

	public function fetchLeadSources( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeadSources::createService()->fetchCustomLeadSourcesByCid( $this->getId(), $objDatabase );
	}

	public function fetchLeadSourcesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeadSources::createService()->fetchLeadSourcesByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchLeadSourceByName( $strName, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeadSources::createService()->fetchLeadSourceByNameByCid( $strName, $this->getId(), $objDatabase );
	}

	public function fetchLeadSourceByInternetListingServiceId( $intInternetListingServiceId, $objDatabase, $strInternetListingServiceName = NULL, $boolFetchLeadSourceByName = true ) {
		return \Psi\Eos\Entrata\CLeadSources::createService()->fetchLeadSourceByInternetListingServiceIdByCid( $intInternetListingServiceId, $this->getId(), $objDatabase, $strInternetListingServiceName, $boolFetchLeadSourceByName );
	}

	public function fetchLeadSourceById( $intLeadSourceId, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeadSources::createService()->fetchLeadSourceByIdByCid( $intLeadSourceId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyCategories( $objDatabase ) {
		return CCompanyCategories::fetchSimpleCompanyCategoriesByCid( $this->getId(), $objDatabase );
	}

	public function fetchFileTypes( $objDatabase, $intIsHidden = NULL, $strExcludingFileType = NULL ) {
		return CFileTypes::fetchCustomFileTypesByCid( $this->getId(), $objDatabase, $intIsHidden, false, $strExcludingFileType );
	}

	public function fetchFileTypesBySystemCodes( $arrstrSystemCodes, $objDatabase ) {
		return CFileTypes::fetchFileTypesBySystemCodesByCid( $arrstrSystemCodes, $this->getId(), $objDatabase );
	}

	public function fetchFileById( $intId, $objDatabase ) {
		return CFiles::createService()->fetchCustomFileByIdByCid( $intId, $this->getId(), $objDatabase );
	}

	public function fetchFileAssociationsByFileIds( $arrintFileIds, $objDatabase, $boolIsIncludeReferenceFileAssociation = true ) {

		if( false == valArr( $arrintFileIds ) ) {
			return NULL;
		}
		return CFileAssociations::createService()->fetchFileAssociationsByFileIdsByCid( $arrintFileIds, $this->getId(), $objDatabase, false, false, $boolIsIncludeReferenceFileAssociation );
	}

	public function fetchCompanyCategory( $intCompanyCategoryId, $objDatabase ) {
		return CCompanyCategories::fetchCompanyCategoryByIdByCid( $intCompanyCategoryId, $this->getId(), $objDatabase );
	}

	public function fetchIntermediaryAccounts( $objDatabase ) {
		return CAccounts::createService()->fetchIntermediaryAccountsByCid( $this->getId(), $objDatabase );
	}

	public function fetchClearingAccounts( $objDatabase ) {
		return CAccounts::createService()->fetchClearingAccountsByCid( $this->getId(), $objDatabase );
	}

	public function fetchScreeningPropertyAccounts( $objDatabase ) {
		return CScreeningPropertyAccounts::fetchPublishedScreeningPropertyAccountsByCid( $this->getId(), $objDatabase );
	}

	public function fetchPublishedCompanyScreeningConfigs( $objDatabase ) {
		return CScreeningConfigurations::fetchPublishedCompanyScreeningConfigsByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyScreeningConfigurationById( $intScreeningConfigId, $objDatabase ) {
		return CScreeningConfigurations::fetchCompanyScreeningConfigurationByIdByCid( $intScreeningConfigId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyApplications( $objDatabase, $boolOrderByTitle = false ) {
		return CCompanyApplications::fetchSimpleCompanyApplicationsByCid( $this->getId(), $objDatabase, $boolOrderByTitle );
	}

	public function fetchCompanyApplicationsByPropertyIdsByCompanyUserId( $arrintPropertyIds, $intCompanyUserId, $objDatabase ) {
		return CCompanyApplications::fetchCompanyApplicationsByPropertyIdsByCompanyUserIdByCid( $arrintPropertyIds, $intCompanyUserId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyApplicationsByCompanyApplicationId( $intCompanyApplicationId, $objDatabase ) {
		return CPropertyApplications::fetchPropertyApplicationsByCompanyApplicationIdByCid( $intCompanyApplicationId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyApplicationsByCompanyApplicationIdByPropertyIds( $intCompanyApplicationId, $arrintPropertyIds, $objDatabase ) {
		return CPropertyApplications::fetchPropertyApplicationsByCompanyApplicationIdByPropertyIdsByCid( $intCompanyApplicationId, $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchCompanyApplicationById( $intCompanyApplicationId, $objDatabase ) {
		return CCompanyApplications::fetchCompanyApplicationByIdByCid( $intCompanyApplicationId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyApplicationByDocumentId( $intDocumentId, $objDatabase ) {
		return CCompanyApplications::fetchCompanyApplicationByDocumentIdByCid( $intDocumentId, $this->getId(), $objDatabase );
	}

	public function fetchSwitchCompanyApplicationByCompanyApplicationIdByDirection( $intCompanyApplicationId, $intCompanyApplicationOrderNumber, $strDirection, $objDatabase ) {
		return CCompanyApplications::fetchSwitchCompanyApplicationByCompanyApplicationIdByDirectionByCid( $intCompanyApplicationId, $intCompanyApplicationOrderNumber, $strDirection, $this->getId(), $objDatabase );
	}

	public function fetchWebsites( $objDatabase ) {
		return CWebsites::createService()->fetchCustomWebsitesByCid( $this->getId(), $objDatabase );
	}

	public function fetchWebsitesByPropertyIds( $arrintPropertyIds, $objClientDatabase ) {
		return CWebsites::createService()->fetchWebsitesByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objClientDatabase );
	}

	public function fetchWebsite( $intWebsiteId, $objDatabase ) {
		return CWebsites::createService()->fetchCustomWebsiteByIdByCid( $intWebsiteId, $this->getId(), $objDatabase );
	}

	public function fetchProperties( $objDatabase, $boolShowDisabledData = false, $intQuickSearchPropertyId = NULL ) {

		if( true == valId( $intQuickSearchPropertyId ) ) {
			$arrintPropertyIds[] = $intQuickSearchPropertyId;
			return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByCid( $this->getId(), $objDatabase, $arrintPropertyIds, $boolShowDisabledData );
		} else {
			return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByCid( $this->getId(), $objDatabase, $arrintPropertyIds = NULL, $boolShowDisabledData );
		}
	}

	public function fetchProspectPortalPropertiesWithCityWithWebsiteDomain( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchProspectPortalPropertiesWithCityWithWebsiteDomainByCid( $this->getId(), $objDatabase );
	}

	public function fetchPropertyFloors( $objDatabase ) {
		return CPropertyFloors::createService()->fetchPropertyFloorsByCid( $this->getId(), $objDatabase );
	}

	public function fetchPropertiesByRemotePrimaryKeys( $arrstrRemotePrimaryKeys, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByRemotePrimaryKeysByCid( $arrstrRemotePrimaryKeys, $this->getId(), $objDatabase );
	}

	public function fetchAllProperties( $objDatabase, $boolOrderByIsdisabled = false ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchAllPropertiesByCid( $this->getId(), $objDatabase, $boolOrderByIsdisabled );
	}

	public function fetchAllPaginatedPropertiesAndCount( $objDatabase, $objPagination = NULL ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPaginatedPropertiesAndCountByCid( $this->getId(), $objDatabase, $objPagination );
	}

	public function fetchSimplePropertiesByPropertyIdsByFieldNames( $arrintPropertyIds, $arrstrFieldNames, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchSimplePropertiesByIdsByFieldNamesByCid( $arrintPropertyIds, $arrstrFieldNames, $this->getId(), $objDatabase );
	}

	public function fetchSimplePropertiesWithLookupCodeByPropertyIds( $arrintPropertyIds, $objDatabase, $boolShowDisabledData = false ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchSimplePropertiesWithLookupCodeByIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase, $boolShowDisabledData );
	}

	public function fetchPropertiesByPsProductIds( $arrintPsProductIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByPsProductIdsByCid( $arrintPsProductIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyIdsByCompanyUserIdBySessionData( $intCompanyUserId, $intIsDisabledSessionData, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyIdsByCidByCompanyUserIdBySessionData( $this->getId(), $intCompanyUserId, $intIsDisabledSessionData, $objDatabase );
	}

	public function fetchParentPropertyIdsByCompanyUserIdBySessionData( $intCompanyUserId, $intIsDisabledSessionData, $objDatabase, $boolIsAdministrator = false ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchParentPropertyIdsByCidByCompanyUserIdBySessionData( $this->getId(), $intCompanyUserId, $intIsDisabledSessionData, $objDatabase, $boolIsAdministrator );
	}

	public function fetchSimplePropertyIdsBySessionData( $objDatabase, $boolIncludeDisabledData = false, $boolShowDisabledData = false ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchSimplePropertiesByPropertyIdsByCidByDisabledData( NULL, $this->getId(), $objDatabase, true, false, $boolIncludeDisabledData, $boolShowDisabledData );
	}

	public function fetchPropertiesByIds( $arrintPropertyIds, $objDatabase, $objPropertyFilter = NULL, $boolShowDisabledData = false ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase, $objPropertyFilter, $boolShowDisabledData );
	}

	public function fetchActivePropertiesByIds( $arrintPropertyIds, $objDatabase, $objPropertyFilter = NULL ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchActivePropertiesByIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase, $objPropertyFilter );
	}

	public function fetchNonIntegratedProperties( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchNonIntegratedPropertiesByCid( $this->getId(), $objDatabase );
	}

	public function fetchNonIntegratedActiveProperties( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchNonIntegratedActivePropertiesByCid( $this->getId(), $objDatabase );
	}

	public function fetchWebsitesByIds( $arrintWebsiteIds, $objDatabase, $objWebsiteFilter = NULL ) {
		return CWebsites::createService()->fetchCustomWebsitesByIdsByCid( $arrintWebsiteIds, $objDatabase, $this->getId(), $objWebsiteFilter );
	}

	public function fetchWebsitesWithAssociatedPropertiesAndWebsiteTemplateByIds( $arrintWebsiteIds, $objDatabase ) {
		return CWebsites::createService()->fetchWebsitesWithAssociatedPropertiesAndWebsiteTemplateByIdsByCid( $arrintWebsiteIds, $this->getId(), $objDatabase );
	}

	public function fetchWebsitesWithBlogPostCount( $objDatabase ) {
		return CWebsites::createService()->fetchWebsitesWithBlogPostCountByCid( $this->getId(), $objDatabase );
	}

	public function fetchBlogTags( $objDatabase ) {
		return \Psi\Eos\Entrata\CBlogTags::createService()->fetchBlogTagsByCid( $this->getId(), $objDatabase );
	}

	public function fetchPropertiesWithRemotePrimaryKeyNotNull( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesWithRemotePrimaryKeyNotNullByCid( $this->getId(), $objDatabase );
	}

	public function fetchPropertiesWithRemotePrimaryKeyNull( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByCidWithRemotePrimaryKeyNull( $this->getId(), $objDatabase );
	}

	public function fetchPropertiesByTransmissionVendorIdsByPsProductIds( $arrintTransmissionVendorIds, $arrintPsProductIds, $objDatabase, $boolAllPropertyDetails = true, $boolIsActiveProperty = false ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByTransmissionVendorIdsByPsProductIdsByCid( $arrintTransmissionVendorIds, $arrintPsProductIds, $this->getId(), $objDatabase, $boolAllPropertyDetails, $boolIsActiveProperty );
	}

	public function fetchCompanyKeyValueByKey( $strKey, $objDatabase ) {
		return CCompanyKeyValues::fetchCompanyKeyValueByCidByKey( $this->getId(), $strKey, $objDatabase );
	}

	public function fetchCompanyKeyValues( $objDatabase ) {

		$arrobjCompanyKeyValues = CCompanyKeyValues::fetchCompanyKeyValuesByCid( $this->getId(), $objDatabase );

		$arrmixRekeyedCompanyKeyValues = [];

		if( true == valArr( $arrobjCompanyKeyValues ) ) {
			foreach( $arrobjCompanyKeyValues as $objCompanyKeyValue ) {
				$arrmixRekeyedCompanyKeyValues[$objCompanyKeyValue->getKey()] = $objCompanyKeyValue;
			}
		}

		return $arrmixRekeyedCompanyKeyValues;
	}

	public function fetchCompanyKeyValuesByKeys( $arrstrKeys, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) ) {
			return NULL;
		}

		return CCompanyKeyValues::fetchCompanyKeyValuesByCidByKeys( $this->getId(), $arrstrKeys, $objDatabase );
	}

	public function fetchPropertyKeyValues( $objDatabase ) {
		return CPropertyKeyValues::fetchPropertyKeyValuesByCid( $this->getId(), $objDatabase );
	}

	public function fetchWebsitePropertyIdsByWebsiteIds( $arrintWebsiteIds, $objDatabase ) {
		if( false == valArr( $arrintWebsiteIds ) ) {
			return NULL;
		}

		return CWebsiteProperties::createService()->fetchWebsitePropertyIdsByWebsiteIdsByCid( $arrintWebsiteIds, $this->getId(), $objDatabase );
	}

	public function fetchMerchantAccountsByPropertyIds( $arrintPropertyIds, $objClientDatabase, $intPaymentMediumId = NULL, $boolShowDisabledMerchantAccount = false ) {
		return CMerchantAccounts::fetchMerchantAccountsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objClientDatabase, $intPaymentMediumId, $boolShowDisabledMerchantAccount );
	}

	public function fetchEligibleMerchantAccountsByBankAccountIds( $arrintBankAccountIds, $objDatabase ) {
		return CMerchantAccounts::fetchEligibleMerchantAccountsByBankAccountIdsByCid( $arrintBankAccountIds, $this->getId(), $objDatabase );
	}

	public function fetchRootCompanyMediaFolder( $objDatabase ) {
		return CCompanyMediaFolders::createService()->fetchRootMediaFolderByCid( $this->getId(), $objDatabase );
	}

	public function fetchPropertyByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByRemotePrimaryKeyByCid( $strRemotePrimaryKey, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceRequestByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CMaintenanceRequests::createService()->fetchMaintenanceRequestByRemotePrimaryKeyByCid( $strRemotePrimaryKey, $this->getId(), $objDatabase );
	}

	public function fetchWebsiteTemplates( $objDatabase, $strOrderBy = 'name ASC' ) {
		return CWebsiteTemplates::createService()->fetchPublicAndPrivateWebsiteTemplatesByCompanyIdByTemplateTypeId( $this->getId(), CWebsiteTemplateType::TEMPLATE_TYPE_WEBSITE, $objDatabase, $strOrderBy );
	}

	public function fetchMaintenanceProblemByName( $strName, $objDatabase, $strProblemType = CMaintenanceProblem::INCLUDE_IN_BOTH ) {
		return CMaintenanceProblems::createService()->fetchMaintenanceProblemByCidByName( $this->getId(), $strName, $objDatabase, $strProblemType );
	}

	public function fetchMaintenanceProblemByNameByIntegrationDatabaseId( $strName, $intIntegrationDatabaseId, $objDatabase ) {
		return CMaintenanceProblems::createService()->fetchMaintenanceProblemByCidByNameByIntegrationDatabaseId( $this->getId(), $strName, $intIntegrationDatabaseId, $objDatabase );
	}

	public function fetchMaintenanceProblemsKeyedByName( $objDatabase ) {
		return CMaintenanceProblems::createService()->fetchMaintenanceProblemByCidKeyedByName( $this->getId(), $objDatabase );
	}

	public function fetchSimplePublishedMaintenanceProblems( $objDatabase, $boolFetchIntegrated = false ) {
		return CMaintenanceProblems::createService()->fetchSimplePublishedMaintenanceProblemsByCid( $this->getId(), $objDatabase, $boolFetchIntegrated );
	}

	public function fetchMaintenanceLocationByName( $strName, $objDatabase ) {
		return CMaintenanceLocations::createService()->fetchMaintenanceLocationByCidByName( $this->getId(), $strName, $objDatabase );
	}

	public function fetchMaintenanceLocation( $intMaintenanceLocationId, $objDatabase ) {

		return CMaintenanceLocations::createService()->fetchMaintenanceLocationByIdByCid( $intMaintenanceLocationId, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceLocationById( $intMaintenanceLocationId, $objDatabase ) {

		return CMaintenanceLocations::createService()->fetchMaintenanceLocationByIdByCid( $intMaintenanceLocationId, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceProblemByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CMaintenanceProblems::createService()->fetchMaintenanceProblemByCidByRemotePrimaryKey( $this->getId(), $strRemotePrimaryKey, $objDatabase );
	}

	public function fetchMaintenanceStatusByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CMaintenanceStatuses::createService()->fetchMaintenanceStatusByCidByRemotePrimaryKey( $this->getId(), $strRemotePrimaryKey, $objDatabase );
	}

	public function fetchMaintenanceStatusByName( $strName, $objDatabase ) {
		return CMaintenanceStatuses::createService()->fetchMaintenanceStatusByCidByName( $this->getId(), $strName, $objDatabase );
	}

	public function fetchMaintenanceStatusByNameByIntegrationDatabaseId( $strName, $intIntegrationDatabaseId, $objDatabase ) {
		return CMaintenanceStatuses::createService()->fetchMaintenanceStatusByCidByNameByIntegrationDatabaseId( $this->getId(), trim( $strName ), $intIntegrationDatabaseId, $objDatabase );
	}

	public function fetchMaintenanceStatusByNameByIntegrationDatabaseIdNull( $strName, $objDatabase ) {
		return CMaintenanceStatuses::createService()->fetchMaintenanceStatusByCidByNameByIntegrationDatabaseIdNull( $this->getId(), trim( $strName ), $objDatabase );
	}

	public function fetchMaintenanceStatusById( $intStatusId, $objDatabase ) {
		return CMaintenanceStatuses::createService()->fetchMaintenanceStatusByIdByCid( $intStatusId, $this->getId(), $objDatabase );
	}

	public function fetchMaintenancePriorityByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CMaintenancePriorities::createService()->fetchMaintenancePriorityByCidByRemotePrimaryKey( $this->getId(), $strRemotePrimaryKey, $objDatabase );
	}

	public function fetchMaintenancePriorityByName( $strName, $objDatabase ) {
		return CMaintenancePriorities::createService()->fetchMaintenancePriorityByCidByName( $this->getId(), trim( $strName ), $objDatabase );
	}

	public function fetchMaintenancePriorityByNameByIntegrationDatabaseId( $strName, $intIntegrationDatabaseId, $objDatabase ) {
		return CMaintenancePriorities::createService()->fetchMaintenancePriorityByCidByNameByIntegrationDatabaseId( $this->getId(), trim( $strName ), $intIntegrationDatabaseId, $objDatabase );
	}

	public function fetchLeaseByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByRemotePrimaryKeyByCid( $strRemotePrimaryKey, $this->getId(), $objDatabase );
	}

	public function fetchLeaseByRemotePrimaryKeyWithIntegrationDatabaseIdNull( $strRemotePrimaryKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByRemotePrimaryKeyWithIntegrationDatabaseIdNullByCid( $strRemotePrimaryKey, $this->getId(), $objDatabase );
	}

	public function fetchCustomerByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByRemotePrimaryKeyByCid( $strRemotePrimaryKey, $this->getId(), $objDatabase );
	}

	public function fetchCustomerByRemotePrimaryKeyWithIntegrationDatabaseIdNull( $strRemotePrimaryKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByRemotePrimaryKeyWithIntegrationDatabaseIdNullByCid( $strRemotePrimaryKey, $this->getId(), $objDatabase );
	}

	public function fetchCustomerByUsername( $strUsername, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomCustomerByUsernameByCid( $strUsername, $this->getId(), $objDatabase );
	}

	public function fetchIntegratedCustomerByNameFirstByNameLastByTaxNumber( $strNameFirst, $strNameLast, $strTaxNumber, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchIntegratedCustomerByNameFirstByNameLastByTaxNumberByCid( $strNameFirst, $strNameLast, $strTaxNumber, $this->getId(), $objDatabase );
	}

	public function fetchIntegratedCustomerByFullNameFirstByNameLastByTaxNumber( $strNameFirst, $strNameLast, $strTaxNumber, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchIntegratedCustomerByFullNameFirstByNameLastByTaxNumberByCid( $strNameFirst, $strNameLast, $strTaxNumber, $this->getId(), $objDatabase );
	}

	public function fetchCustomerByNameFirstByNameLastByTaxNumber( $strNameFirst, $strNameLast, $strTaxNumber, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByNameFirstByNameLastByTaxNumberByCid( $strNameFirst, $strNameLast, $strTaxNumber, $this->getId(), $objDatabase );
	}

	public function fetchCustomerByNameFirstByNameLastByTaxNumberWithSsn( $strNameFirst, $strNameLast, $strTaxNumber, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByNameFirstByNameLastByTaxNumberWithSsnByCid( $strNameFirst, $strNameLast, $strTaxNumber, $this->getId(), $objDatabase );
	}

	public function fetchCustomerByNameFirstByNameLastByTaxNumberByBirthDate( $strNameFirst, $strNameLast, $strTaxNumber, $strBirthDate, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByNameFirstByNameLastByTaxNumberByBirthDateByCid( $strNameFirst, $strNameLast, $strTaxNumber, $strBirthDate, $this->getId(), $objDatabase );
	}

	public function fetchIntegratedCustomerByFullNameFirstByNameLastByTaxNumberByBirthDate( $strNameFirst, $strNameLast, $strTaxNumber, $strBirthDate, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchIntegratedCustomerByFullNameFirstByNameLastByTaxNumberByBirthDateByCid( $strNameFirst, $strNameLast, $strTaxNumber, $strBirthDate, $this->getId(), $objDatabase );
	}

	public function fetchCustomerByNameFirstByNameLastByBirthDate( $strNameFirst, $strNameLast, $strBirthDate, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByNameFirstByNameLastByDateOfBirthByCid( $strNameFirst, $strNameLast, $strBirthDate, $this->getId(), $objDatabase );
	}

	public function fetchIntegratedCustomerByNameFirstByNameLastByBirthDate( $strNameFirst, $strNameLast, $strBirthDate, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchIntegratedCustomerByNameFirstByNameLastByDateOfBirthByCid( $strNameFirst, $strNameLast, $strBirthDate, $this->getId(), $objDatabase );
	}

	public function fetchIntegratedCustomerByFullNameFirstByNameLastByBirthDate( $strNameFirst, $strNameLast, $strBirthDate, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchIntegratedCustomerByFullNameFirstByNameLastByDateOfBirthByCid( $strNameFirst, $strNameLast, $strBirthDate, $this->getId(), $objDatabase );
	}

	public function fetchCustomerByNameFirstByNameLastBySecondaryNumber( $strNameFirst, $strNameLast, $strSecondaryNumber, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByNameFirstByNameLastBySecondaryNumberByCid( $strNameFirst, $strNameLast, $strSecondaryNumber, $this->getId(), $objDatabase );
	}

	public function fetchIntegratedCustomerByNameFirstByNameLastBySecondaryNumber( $strNameFirst, $strNameLast, $strSecondaryNumber, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchIntegratedCustomerByNameFirstByNameLastBySecondaryNumberByCid( $strNameFirst, $strNameLast, $strSecondaryNumber, $this->getId(), $objDatabase );
	}

	public function fetchCustomerByNameFirstByNameLastByPhoneNumber( $strNameFirst, $strNameLast, $strPhoneNumber, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByNameFirstByNameLastByPhoneNumberByCid( $strNameFirst, $strNameLast, $strPhoneNumber, $this->getId(), $objDatabase );
	}

	public function fetchCustomerByNameFirstByNameLastByPhoneNumberByEmailAddress( $strNameFirst, $strNameLast, $strPhoneNumber, $strEmailAddress, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByNameFirstByNameLastByPhoneNumberByEmailAddressByCid( $strNameFirst, $strNameLast, $strPhoneNumber, $strEmailAddress, $this->getId(), $objDatabase );
	}

	public function fetchIntegratedCustomerByFullNameFirstByNameLastByPhoneNumber( $strNameFirst, $strNameLast, $strPhoneNumber, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchIntegratedCustomerByFullNameFirstByNameLastByPhoneNumberByCid( $strNameFirst, $strNameLast, $strPhoneNumber, $this->getId(), $objDatabase );
	}

	public function fetchCustomerByNameFirstByNameLastByEmailAddress( $strNameFirst, $strNameLast, $strEmailAddress, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByNameFirstByNameLastByEmailAddressByCid( $strNameFirst, $strNameLast, $strEmailAddress, $this->getId(), $objDatabase );
	}

	public function fetchIntegratedCustomerByFullNameFirstByNameLastByEmailAddress( $strNameFirst, $strNameLast, $strEmailAddress, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchIntegratedCustomerByFullNameFirstByNameLastByEmailAddressByCid( $strNameFirst, $strNameLast, $strEmailAddress, $this->getId(), $objDatabase );
	}

	public function fetchCustomerByNameFirstByNameLastByAccountNumber( $strNameFirst, $strNameLast, $strAccountNumber, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByNameFirstByNameLastByAccountNumberByCid( $strNameFirst, $strNameLast, $strAccountNumber, $this->getId(), $objDatabase );
	}

	public function fetchIntegratedCustomerByNameFirstByNameLastByAccountNumber( $strNameFirst, $strNameLast, $strAccountNumber, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchIntegratedCustomerByNameFirstByNameLastByAccountNumberByCid( $strNameFirst, $strNameLast, $strAccountNumber, $this->getId(), $objDatabase );
	}

	public function fetchCustomerByEnrollingCustomer( $objEnrollingCustomer, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersByFloatingInfo( $objEnrollingCustomer->getNameFirst(), $objEnrollingCustomer->getNameLast(), $this->getId(), NULL, $objEnrollingCustomer->getTaxNumber(), $objEnrollingCustomer->getBirthDate(), NULL, NULL, $objDatabase );
	}

	public function fetchPaginatedDataExportSchedules( $intPageNo, $intPageSize, $objDatabase ) {
		return \Psi\Eos\Entrata\CDataExportSchedules::createService()->fetchPaginatedDataExportSchedulesByCid( $intPageNo, $intPageSize, $this->getId(), $objDatabase );
	}

	public function fetchTotalPaginatedDataExportSchedules( $objDatabase ) {
		return \Psi\Eos\Entrata\CDataExportSchedules::createService()->fetchTotalPaginatedDataExportSchedulesByCid( $this->getId(), $objDatabase );
	}

	public function fetchDataExportSchedulesByIds( $arrintDataExportScheduleIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CDataExportSchedules::createService()->fetchDataExportSchedulesByIdsByCid( $arrintDataExportScheduleIds, $this->getId(), $objDatabase );
	}

	public function fetchDataExportScheduleById( $intDataExportScheduleId, $objDatabase ) {
		return \Psi\Eos\Entrata\CDataExportSchedules::createService()->fetchDataExportScheduleByIdByCid( $intDataExportScheduleId, $this->getId(), $objDatabase );
	}

	public function fetchAmountDue( $objDatabase ) {

		$strSql = 'SELECT
					  cid,
					  SUM( transaction_amount_due ) as balance
				  FROM
					  view_transactions
				  WHERE
					   cid::integer = ' . $this->getId() . '::integer
					   AND charge_code_id != ' . CChargeCode::UNDERWRITING_RESERVES . '
				  GROUP BY
					   cid';

		$arrmixData = fetchData( $strSql, $objDatabase );
		$this->m_fltAmountDue = 0;

		if( true == valArr( $arrmixData ) && true == isset( $arrmixData[0] ) ) {
			$this->m_fltAmountDue = $arrmixData[0]['balance'];
		}

		return $this->m_fltAmountDue;
	}

	public function fetchCommissionAmountDue( $objDatabase ) {

		$strSql = 'SELECT
					  SUM( commission_amount ) as balance
				  FROM
					  commissions
				  WHERE
					   cid::integer = ' . $this->getId() . '::integer
					   AND deleted_on IS NULL
				  GROUP BY
					   cid';

		$arrmixData = fetchData( $strSql, $objDatabase );
		$this->m_fltCommissionAmountDue = 0;

		if( true == valArr( $arrmixData ) && true == isset ( $arrmixData[0] ) ) {
			$this->m_fltCommissionAmountDue = $arrmixData[0]['balance'];
		}

		return $this->m_fltCommissionAmountDue;
	}

	public function fetchMonthlyCompanyChargeTotal( $objDatabase ) {

		$this->m_fltMonthlyCompanyChargeTotal = CCompanyCharges::createService()->fetchMonthlyCompanyChargeTotalByCid( $this->getId(), $objDatabase );

		return $this->m_fltMonthlyCompanyChargeTotal;
	}

	public function fetchCompanyEmployeeByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CCompanyEmployees::createService()->fetchCompanyEmployeeByRemotePrimaryKeyByCid( $strRemotePrimaryKey, $this->getId(), $objDatabase );
	}

	public function fetchPropertyGroupTypesByIds( $arrintIds, $objDatabase, $strSortByField = NULL ) {
		return CPropertyGroupTypes::createService()->fetchPropertyGroupTypesByIdsByCid( $arrintIds, $this->getId(), $objDatabase, $strSortByField );
	}

	public function fetchCompanyGroups( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyGroups::createService()->fetchCompanyGroupsByCid( $this->getId(), $objDatabase );
	}

	public function fetchCustomPropertyGroupTypes( $objDatabase ) {
		return CPropertyGroupTypes::createService()->fetchCustomPropertyGroupTypesByCid( $this->getId(), $objDatabase );
	}

	public function fetchSmartPropertyGroupTypes( $objDatabase ) {
		return CPropertyGroupTypes::createService()->fetchSmartPropertyGroupTypesByCid( $this->getId(), $objDatabase );
	}

	public function fetchPropertyGroupTypeBySystemCode( $strSystemCode, $objDatabase ) {
		return CPropertyGroupTypes::createService()->fetchPropertyGroupTypeBySystemCodeByCid( $strSystemCode, $this->getId(), $objDatabase );
	}

	public function fetchPropertyGroups( $objDatabase ) {
		return CPropertyGroups::createService()->fetchPropertyGroupsByCid( $this->getId(), $objDatabase );
	}

	public function fetchPropertyGroupsByIds( $arrintPropertyGroupIds, $objDatabase, $boolShowDisabledData = false ) {
		return CPropertyGroups::createService()->fetchSimplePropertyGroupsByCidByIds( $this->getId(), $arrintPropertyGroupIds, $objDatabase, $boolShowDisabledData );
	}

	public function fetchCompanyUsersExceptPsAdmin( $intShowDisabledUsers, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUsersExceptPsAdminByCid( $this->getId(), $intShowDisabledUsers, $objDatabase );
	}

	public function fetchNonPsiCompanyUsers( $objDatabase, $boolIsHideDisabledCompanyUsers = true ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchNonPsiCompanyUsersByCid( $this->getId(), $objDatabase, $boolIsHideDisabledCompanyUsers = true );
	}

	public function fetchActiveMerchantChangeRequests( $objPaymentDatabase, $boolProcessed = false ) {
		return \Psi\Eos\Payment\CMerchantChangeRequests::createService()->fetchActiveMerchantChangeRequestsByCid( $this->getId(), $objPaymentDatabase, $boolProcessed );
	}

	public function fetchIntegratedCompanyUsers( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchIntegratedCompanyUsersByCid( $this->getId(), $objDatabase );
	}

	public function fetchPaginatedFormsByPropertyIds( $intPageNo, $intPageSize, $arrintPropertyIds, $objFormsFilter, $objDatabase ) {
		return CForms::fetchPaginatedFormsByPropertyIdsByCid( $intPageNo, $intPageSize, $arrintPropertyIds, $this->getId(), $objFormsFilter, $objDatabase );
	}

	public function fetchPaginatedFormsCountByPropertyIds( $arrintPropertyIds, $objFormsFilter, $objDatabase ) {
		return CForms::fetchPaginatedFormsCountByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objFormsFilter, $objDatabase );
	}

	public function fetchApplicationsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CApplications::createService()->fetchApplicationsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchApplicationsByApplicationIdsByCallFilter( $arrintApplicationIds, $objCallsFilter, $objDatabase ) {
		return \Psi\Eos\Entrata\CApplications::createService()->fetchApplicationsByApplicationIdsByCallFilterByCid( $arrintApplicationIds, $objCallsFilter, $this->getId(), $objDatabase );
	}

	public function fetchCompanyUsers( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCustomCompanyUsersByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyUsersWithName( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUsersWithNameByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyUsersWithNameWithEmailAddress( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUsersWithNameWithEmailAddressByCid( $this->getId(), $objDatabase );
	}

	public function fetchRelieverCompanyUsersWithNameWithEmailAddress( $arrstrModulesName, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchRelieverCompanyUsersWithNameWithEmailAddressByCid( $arrstrModulesName, $this->getId(), $objDatabase );
	}

	public function fetchNonAdminCompanyUsersWithNameByPropertyIdsByModuleId( $arrintPropertyIds, $intModuleId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchNonAdminCompanyUsersWithNameByPropertyIdsByModuleIdByCid( $arrintPropertyIds, $intModuleId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyEmployees( $objDatabase ) {
		return CCompanyEmployees::createService()->fetchCompanyEmployeesByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyEmployeesByCompanyDepartmentNameByPropertyIdsByStatusTypeIdOrderByName( $arrstrCompanyDepartmentName, $arrintPropertyIds, $arrintEmployeeStatusTypeIds, $boolIsAdmin, $objDatabase, $boolReturnArray = false ) {
		return CCompanyEmployees::createService()->fetchCompanyEmployeesByCompanyDepartmentNameByPropertyIdsByStatusTypeIdsByCid( $arrstrCompanyDepartmentName, $arrintPropertyIds, $arrintEmployeeStatusTypeIds, $this->getId(), $boolIsAdmin, $objDatabase, $boolReturnArray );
	}

	public function fetchOrderedLeasingAgents( $objDatabase, $strSortByField = NULL ) {
		return CCompanyEmployees::createService()->fetchLeasingAgents( $this->getId(), $objDatabase, $strSortByField );
	}

	public function fetchLeasingAgentsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CCompanyEmployees::createService()->fetchActiveLeasingAgentsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchAssociatedCompanyEmployees( $objDatabase ) {
		return CCompanyEmployees::createService()->fetchAssociatedCompanyEmployeesByCid( $this->getId(), $objDatabase );
	}

	public function fetchAssociatedCompanyEmployeesWithProperties( $objDatabase ) {

		$arrobjCompanyEmployees      = CCompanyEmployees::createService()->fetchAssociatedCompanyEmployeesByCid( $this->getId(), $objDatabase );
		$arrmixCompanyUserProperties = $this->fetchComplexCompanyUserProperties( $objDatabase );

		if( true == valArr( $arrmixCompanyUserProperties ) ) {
			foreach( $arrmixCompanyUserProperties as $arrmixCompanyUserProperty ) {
				if( true == valArr( $arrobjCompanyEmployees ) && true == array_key_exists( $arrmixCompanyUserProperty['company_employee_id'], $arrobjCompanyEmployees ) ) {
					$objCompanyEmployee = $arrobjCompanyEmployees[$arrmixCompanyUserProperty['company_employee_id']];
					$objCompanyEmployee->addPropertyId( $arrmixCompanyUserProperty['property_id'] );
				}
			}
		}

		return $arrobjCompanyEmployees;
	}

	public function fetchCompanyEmployeesByIds( $arrintCompanyEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintCompanyEmployeeIds ) ) {
			return NULL;
		}

		return CCompanyEmployees::createService()->fetchCompanyEmployeesByIdsByCid( $arrintCompanyEmployeeIds, $this->getId(), $objDatabase );
	}

	public function fetchCompanyEmployeesDetailsByIds( $arrintCompanyEmployeeIds, $arrstrFields, $objDatabase ) {
		if( false == valArr( $arrintCompanyEmployeeIds ) ) {
			return NULL;
		}

		return CCompanyEmployees::createService()->fetchCompanyEmployeesDetailsByIdsByCid( $arrintCompanyEmployeeIds, $arrstrFields, $this->getId(), $objDatabase );
	}

	public function fetchCompanyEmployeeUsersByEmployeeIds( $arrintCompanyEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintCompanyEmployeeIds ) ) {
			return NULL;
		}

		return CCompanyEmployees::createService()->fetchCompanyEmployeeUsersByEmployeeIdsByCid( $arrintCompanyEmployeeIds, $this->getId(), $objDatabase );
	}

	public function fetchUnassociatedCompanyEmployeesWithPropertyNameWithIntegrationDatabaseName( $objDatabase ) {
		return CCompanyEmployees::createService()->fetchUnassociatedCompanyEmployeesWithPropertyNameWithIntegrationDatabaseNameByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyEmployeeByCompanyEmployeeId( $intCompanyEmployeeId, $objDatabase ) {
		return CCompanyEmployees::createService()->fetchCompanyEmployeeByIdByCid( $intCompanyEmployeeId, $this->getId(), $objDatabase );
	}

	public function fetchDocumentsByPropertyIdsByDocumentAssociationTypeId( $arrintPropertyIds, $intDocumentAssociationTypeId, $objDatabase, $boolFetchChildDocuments = true, $boolFetchEmployeePortalDocuments = false ) {
		if( true == $boolFetchEmployeePortalDocuments ) {
			return CDocuments::fetchEmployeePortalDocumentsByPropertyIdsByDocumentAssociationTypeIdByCid( $arrintPropertyIds, $intDocumentAssociationTypeId, $this->getId(), $objDatabase );
		}
		return CDocuments::fetchDocumentsByPropertyIdsByDocumentAssociationTypeIdByCid( $arrintPropertyIds, $intDocumentAssociationTypeId, $this->getId(), $objDatabase, $boolFetchChildDocuments );
	}

	public function fetchDocumentsByDocumentInclusionTypeId( $intDocumentInclusionType, $objDatabase, $boolFetchChildDocuments = true ) {
		return CDocuments::fetchCustomDocumentsByDocumentInclusionTypeIdByCid( $intDocumentInclusionType, $this->getId(), $objDatabase, $boolFetchChildDocuments );
	}

	public function fetchDocumentsByDocumentTypeByDocumentInclusionTypeId( $arrintDocumentTypeIds, $intDocumentInclusionType, $objDatabase, $boolFetchChildDocuments = true ) {
		return CDocuments::fetchDocumentsByDocumentTypeByDocumentInclusionTypeIdByCid( $arrintDocumentTypeIds, $intDocumentInclusionType, $this->getId(), $objDatabase, $boolFetchChildDocuments );
	}

	public function fetchDocumentsByOwnerIds( $arrintApPayeeIds, $objDatabase, $boolFetchChildDocuments = true ) {
		return CDocuments::fetchDocumentsByOwnerIdsByCid( $arrintApPayeeIds, $this->getId(), $objDatabase, $boolFetchChildDocuments );
	}

	public function fetchDocumentsByDocumentTypeByOwnerId( $arrintDocumentTypeIds, $intApPayeeId, $objDatabase, $boolFetchChildDocuments = true ) {
		return CDocuments::fetchDocumentsByDocumentTypeByOwnerIdByCid( $arrintDocumentTypeIds, $intApPayeeId, $this->getId(), $objDatabase, $boolFetchChildDocuments );
	}

	public function fetchCorporateWebsite( $objClientDatabase ) {
		return CWebsites::createService()->fetchCorporateWebsiteByCid( $this->getId(), $objClientDatabase );
	}

	public function fetchCorporateWebsiteByIsPrimary( $intIsPrimary, $objClientDatabase ) {
		return CWebsites::createService()->fetchCorporateWebsiteByIsPrimaryByCid( $intIsPrimary, $this->getId(), $objClientDatabase );
	}

	public function fetchFormAnswersByFormId( $intFormId, $objDatabase ) {
		return CFormAnswers::fetchCustomFormAnswersByFormIdByCid( $intFormId, $this->getId(), $objDatabase );
	}

	public function fetchDocumentParametersByDocumentComponentIds( $arrintDocumentComponentIds, $objDatabase ) {
		$this->m_arrobjDocumentParameters = \Psi\Eos\Entrata\CDocumentParameters::createService()->fetchDocumentParametersByCidByDocumentComponentIds( $this->getId(), $arrintDocumentComponentIds, $objDatabase );

		return $this->m_arrobjDocumentParameters;
	}

	public function fetchDocumentParametersByDocumentIdByKey( $intDocumentId, $strKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CDocumentParameters::createService()->fetchDocumentParametersByCidByDocumentIdByKey( $this->getId(), $intDocumentId, $strKey, $objDatabase );
	}

	public function fetchDocumentParametersByDocumentIds( $arrintDocumentIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CDocumentParameters::createService()->fetchDocumentParametersByCidByDocumentIds( $this->getId(), $arrintDocumentIds, $objDatabase );
	}

	public function fetchLeaseDocuments( $intDocumentTypeId, $intDocumentSubTypeId, $objDatabase, $strSortBy = 'name', $strSortDirection = 'asc' ) {
		return CDocuments::fetchLeaseDocumentsByDocumentTypeIdByDocumentSubTypeIdByCid( $intDocumentTypeId, $intDocumentSubTypeId, $this->getId(), $objDatabase, $strSortBy, $strSortDirection );
	}

	public function fetchDelinquencyPolicyById( $intDelinquencyPolicyId, $objDatabase ) {
		return \Psi\Eos\Entrata\CDelinquencyPolicies::createService()->fetchDelinquencyPolicyByIdByCid( $intDelinquencyPolicyId, $this->getId(), $objDatabase );
	}

	public function fetchDocumentById( $intDocumentId, $objDatabase ) {
		return CDocuments::fetchDocumentByIdByCid( $intDocumentId, $this->getId(), $objDatabase );
	}

	public function fetchWebsitePageById( $intWebsitePageId, $objDatabase ) {
		return \Psi\Eos\Entrata\CWebsitePages::createService()->fetchWebsitePageByIdByCid( $intWebsitePageId, $this->getId(), $objDatabase );
	}

	public function fetchDocumentsByIds( $arrintDocumentIds, $objDatabase ) {
		return CDocuments::fetchDocumentsByIdsByCid( $arrintDocumentIds, $this->getId(), $objDatabase );
	}

	public function fetchPublishedDocumentsByDocumentTypeIdByDocumentAssociationTypeIdByPropertyId( $arrintDocumentTypeIds, $intDocumentAssociationTypeId, $intPropertyId, $boolIsLoggedIn, $objDatabase ) {
		return CDocuments::fetchPublishedDocumentsByDocumentTypeIdsByDocumentAssociationTypeIdByPropertyIdByCid( $arrintDocumentTypeIds, $intDocumentAssociationTypeId, $intPropertyId, $boolIsLoggedIn, $this->getId(), $objDatabase );
	}

	public function fetchPublishedRequiredLoginDocumentsByDocumentTypeIdByDocumentAssociationTypeIdByPropertyId( $arrintDocumentTypeIds, $intDocumentAssociationTypeId, $intPropertyId, $boolIsLoggedIn, $objDatabase ) {
		return CDocuments::fetchPublishedRequiredLoginDocumentsByDocumentTypeIdsByDocumentAssociationTypeIdByPropertyIdByCid( $arrintDocumentTypeIds, $intDocumentAssociationTypeId, $intPropertyId, $boolIsLoggedIn, $this->getId(), $objDatabase );
	}

	public function fetchPublishedDocumentsByDocumentTypeIdByDocumentAssociationTypeIdByPropertyIds( $arrintDocumentTypeIds, $intDocumentAssociationTypeId, $arrintPropertyIds, $boolIsLoggedIn, $objDatabase ) {
		return CDocuments::fetchPublishedDocumentsByDocumentTypeIdByDocumentAssociationTypeIdByPropertyIdsByCid( $arrintDocumentTypeIds, $intDocumentAssociationTypeId, $arrintPropertyIds, $boolIsLoggedIn, $this->getId(), $objDatabase );
	}

	public function fetchPublishedDocumentsByDocumentTypeIdByDocumentAssociationTypeIdByUserId( $arrintDocumentTypeIds, $intDocumentAssociationTypeId, $strUserTypeId, $boolIsLoggedIn, $strSearchFilter, $strSearchContent, $objDatabase ) {
		return CDocuments::fetchPublishedDocumentsByDocumentTypeIdByDocumentAssociationTypeIdByUserTypeIdByCid( $arrintDocumentTypeIds, $intDocumentAssociationTypeId, $strUserTypeId, $boolIsLoggedIn, $strSearchFilter, $strSearchContent, $this->getId(), $objDatabase );
	}

	public function fetchPublishedParentDocumentsByDocumentTypeIdByDocumentAssociationTypeIdByPropertyIds( $arrintDocumentTypeIds, $intDocumentAssociationTypeId, $arrintPropertyIds, $objDatabase ) {
		return CDocuments::fetchPublishedParentDocumentsByDocumentTypeIdByDocumentAssociationTypeIdByPropertyIdsByCid( $arrintDocumentTypeIds, $intDocumentAssociationTypeId, $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPublishedDocumentsByDocumentTypeIdsByDocumentSubTypeIdsByDocumentAssociationTypeIdByPropertyIds( $arrintDocumentTypeIds, $arrintDocumentSubTypeIds, $intDocumentAssociationTypeId, $arrintPropertyIds, $boolIsLoggedIn, $objDatabase, $boolIsFromResidentWorks = false, $boolIsCreateWorkOrderSurvey = false ) {
		return CDocuments::fetchPublishedDocumentsByDocumentTypeIdsByDocumentSubTypeIdsByDocumentAssociationTypeIdByPropertyIdsByCid( $arrintDocumentTypeIds, $arrintDocumentSubTypeIds, $intDocumentAssociationTypeId, $arrintPropertyIds, $boolIsLoggedIn, $this->getId(), $objDatabase, $boolIsFromResidentWorks, $boolIsCreateWorkOrderSurvey );
	}

	public function fetchPublishedDocumentIdByDocumentTypeIdsByDocumentSubTypeIdsByDocumentAssociationTypeIdByPropertyIds( $intDocumentTypeId, $intDocumentSubTypeId, $intDocumentAssociationTypeId, $arrintPropertyIds, $objDatabase ) {
		return CDocuments::fetchPublishedDocumentIdByDocumentTypeIdsByDocumentSubTypeIdsByDocumentAssociationTypeIdByPropertyIdsByCid( $intDocumentTypeId, $intDocumentSubTypeId, $intDocumentAssociationTypeId, $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPublishedDocumentByDocumentTypeIdsByDocumentSubTypeIdsByDocumentAssociationTypeIdByPropertyIds( $intDocumentTypeId, $intDocumentSubTypeId, $intDocumentAssociationTypeId, $arrintPropertyIds, $objDatabase, $boolIsExcludeArchived = false, $boolIsCreateWorkOrderSurvey = false ) {
		return CDocuments::fetchPublishedDocumentByDocumentTypeIdsByDocumentSubTypeIdsByDocumentAssociationTypeIdByPropertyIdsByCid( $intDocumentTypeId, $intDocumentSubTypeId, $intDocumentAssociationTypeId, $arrintPropertyIds, $this->getId(), $objDatabase, $boolIsExcludeArchived, $boolIsCreateWorkOrderSurvey );
	}

	public function fetchPublishedDocumentsCountByDocumentTypeIdsByDocumentSubTypeIdsByDocumentAssociationTypeIdByPropertyIds( $arrintDocumentTypeIds, $arrintDocumentSubTypeIds, $intDocumentAssociationTypeId, $arrintPropertyIds, $boolIsLoggedIn, $objDatabase, $boolIsCreateWorkOrderSurvey = false ) {
		return CDocuments::fetchPublishedDocumentsCountByDocumentTypeIdsByDocumentSubTypeIdsByDocumentAssociationTypeIdByPropertyIdsByCid( $arrintDocumentTypeIds, $arrintDocumentSubTypeIds, $intDocumentAssociationTypeId, $arrintPropertyIds, $boolIsLoggedIn, $this->getId(), $objDatabase, $boolIsCreateWorkOrderSurvey );
	}

	public function fetchArPaymentById( $intArPaymentId, $objDatabase ) {
		return CArPayments::fetchCustomArPaymentByIdByCid( $intArPaymentId, $this->getId(), $objDatabase );
	}

	public function fetchArPaymentReversedByArPaymentId( $intArPaymentId, $objDatabase ) {

		return CArPayments::fetchArPaymentReversedByArPaymentIdByCid( $intArPaymentId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyCharities( $objClientDatabase ) {
		return CCompanyCharities::fetchSimpleCompanyCharitiesByCid( $this->getId(), $objClientDatabase );
	}

	public function fetchReturnTypeOptions( $objDatabase ) {
		return CReturnTypeOptions::fetchCustomReturnTypeOptionsByCid( $this->getId(), $objDatabase );
	}

	public function fetchIntegrationDatabases( $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationDatabases::createService()->fetchCustomIntegrationDatabasesByCid( $this->getId(), $objDatabase );
	}

	public function fetchIntegrationDatabaseById( $intIntegrationDatabaseId, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationDatabases::createService()->fetchCustomIntegrationDatabaseByIdByCid( $intIntegrationDatabaseId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyIntegrationDatabasesByPropertyId( $intPropertyId, $objDatabase ) {

		return \Psi\Eos\Entrata\CPropertyIntegrationDatabases::createService()->fetchPropertyIntegrationDatabaseByPropertyIdByCid( $intPropertyId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyIntegrationDatabases( $objDatabase ) {

		return \Psi\Eos\Entrata\CPropertyIntegrationDatabases::createService()->fetchPropertyIntegrationDatabasesByCid( $this->getId(), $objDatabase );
	}

	public function fetchIntegrationClientsByIntegrationDatabaseId( $intIntegrationDatabaseId, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchCustomIntegrationClientsByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $this->getId(), $objDatabase );
	}

	public function fetchIntegrationClientByIntegrationDatabaseNameByServiceId( $strIntegrationDatabaseName, $intIntegrationServiceId, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchCustomIntegrationClientByIntegrationDatabaseNameByServiceIdByCid( $strIntegrationDatabaseName, $intIntegrationServiceId, $this->getId(), $objDatabase );
	}

	public function fetchCareerType( $intCareerTypeId, $objDatabase ) {
		return CCareerTypes::createService()->fetchCareerTypeByIdByCid( $intCareerTypeId, $this->getId(), $objDatabase );
	}

	public function fetchSortedCareerSources( $objDatabase ) {
		return \Psi\Eos\Entrata\CCareerSources::createService()->fetchSortedCareerSourcesByCid( $this->getId(), $objDatabase );
	}

	public function fetchCareerSource( $intCareerSourceId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCareerSources::createService()->fetchCareerSourceByIdByCid( $intCareerSourceId, $this->getId(), $objDatabase );
	}

	public function fetchTrashFolder( $objDatabase ) {
		return CCompanyMediaFolders::createService()->fetchTrashFolderByCid( $this->getId(), $objDatabase );
	}

	public function fetchNestedCompanyMediaFoldersByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CCompanyMediaFolders::createService()->fetchNestedCompanyMediaFoldersByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchCompanyMediaFoldersByPropertyIdsByFolderId( $arrintPropertyIds, $intCompanyMediaFolderId, $objDatabase, $intLimit = NULL, $intOffset = 0 ) {
		return CCompanyMediaFolders::createService()->fetchCompanyMediaFoldersByPropertyIdsByCompanyMediaFolderIdByCid( $arrintPropertyIds, $intCompanyMediaFolderId, $this->getId(), $objDatabase, $intLimit, $intOffset );
	}

	public function fetchChildCompanyMediaFolderIds( $objDatabase ) {
		return CCompanyMediaFolders::createService()->fetchChildCompanyMediaFolderIdsByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyUserPermissionsByCompanyUserId( $intCompanyUserId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserPermissions::createService()->fetchCustomCompanyUserPermissionsByCompanyUserIdByCid( $intCompanyUserId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyUserByCompanyUserId( $intCompanyUserId, $objDatabase ) {

		$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCustomCompanyUserByIdByCid( $intCompanyUserId, $this->getId(), $objDatabase );

		if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			$objCompanyUser->setRwxDomain( $this->getRwxDomain() );
		}

		return $objCompanyUser;
	}

	public function fetchCompanyUserByRemotePrimaryKey( $intCompanyUserId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByRemotePrimaryKeyByCid( $this->getId(), $intCompanyUserId, $objDatabase );
	}

	public function fetchCompanyUserGroupsByCompanyUserId( $intCompanyUserId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserGroups::createService()->fetchCompanyUserGroupsByCompanyUserIdByCid( $intCompanyUserId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyUserGroups( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserGroups::createService()->fetchCompanyUserGroupsByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyUserPropertyGroupsByCompanyUserId( $intCompanyUserId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserPropertyGroups::createService()->fetchCompanyUserPropertyGroupsByCompanyUserIdByCid( $intCompanyUserId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyUserPropertyGroups( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserPropertyGroups::createService()->fetchCompanyUserPropertyGroupsByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyUserPropertiesByCompanyUserId( $intCompanyUserId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserPropertyGroups::createService()->fetchCompanyUserPropertiesByCidByCompanyUserId( $this->getId(), $intCompanyUserId, $objDatabase );
	}

	public function fetchComplexCompanyUserProperties( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserPropertyGroups::createService()->fetchComplexCompanyUserPropertiesByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyUserWebsitesByCompanyUserId( $intCompanyUserId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserWebsites::createService()->fetchCompanyUserWebsitesByCidByCompanyUserId( $this->getId(), $intCompanyUserId, $objDatabase );
	}

	public function fetchCompanyUserServicesByCompanyUserId( $intCompanyUserId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserServices::createService()->fetchCompanyUserServicesByCompanyUserIdByCid( $intCompanyUserId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyUserWebsites( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserWebsites::createService()->fetchCompanyUserWebsitesByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyUserGroupsByCompanyUserIds( $arrintCompanyUserIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserGroups::createService()->fetchCompanyUserGroupsByCompanyUserIdsByCid( $arrintCompanyUserIds, $this->getId(), $objDatabase );
	}

	public function fetchCompanyUserByUsername( $strUsername, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByUsernameByCid( $strUsername, $this->getId(), $objDatabase );
	}

	public function fetchCompanyGroupByCompanyGroupId( $intCompanyGroupId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyGroups::createService()->fetchCompanyGroupByIdByCid( $intCompanyGroupId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyGroupByCompanyGroupName( $strCompanyGroupName, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyGroups::createService()->fetchCompanyGroupByGroupNameByCid( $strCompanyGroupName, $this->getId(), $objDatabase );
	}

	public function fetchCompanyUserGroupsByCompanyGroupId( $intCompanyGroupId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserGroups::createService()->fetchCompanyUserGroupsByCompanyGroupIdByCid( $intCompanyGroupId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyGroupPermissionsByCompanyGroupId( $intCompanyGroupId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyGroupPermissions::createService()->fetchSimpleCompanyGroupPermissionsByCompanyGroupIdByCid( $intCompanyGroupId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyGroupPermissionsByCompanyGroupIds( $arrintCompanyGroupIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyGroupPermissions::createService()->fetchSimpleCompanyGroupPermissionsByCompanyGroupIdByCid( $arrintCompanyGroupIds, $this->getId(), $objDatabase );
	}

	public function fetchArPaymentsByIds( $arrintArPaymentIds, $objPaymentDatabase, $boolUseView = false ) {
		return CArPayments::fetchArPaymentsByIdsByCid( $arrintArPaymentIds, $this->getId(), $objPaymentDatabase, $boolUseView );
	}

	public function fetchSimpleArPaymentsByIds( $arrintArPaymentIds, $objClientDatabase ) {
		return CArPayments::fetchSimpleArPaymentsByIdsByCid( $arrintArPaymentIds, $this->getId(), $objClientDatabase );
	}

	public function fetchWebsiteDomains( $objDatabase ) {
 		return \Psi\Eos\Entrata\CWebsiteDomains::createService()->fetchCustomWebsiteDomainsByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyDepartments( $objDatabase ) {
		return CCompanyDepartments::createService()->fetchCompanyDepartmentsByCid( $this->getId(), $objDatabase );
	}

	public function fetchClubById( $intClubId, $objDatabase ) {
		return CClubs::fetchSimpleClubByIdByCid( $intClubId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyEventById( $intCompanyEventId, $objDatabase ) {
		return CCompanyEvents::fetchSimpleCompanyEventByIdByCid( $intCompanyEventId, $this->getId(), $objDatabase );
	}

	public function fetchContactSubmissionById( $intContactSubmissionId, $objDatabase ) {
		return CContactSubmissions::fetchCustomContactSubmissionByIdByCid( $intContactSubmissionId, $this->getId(), $objDatabase );
	}

	public function fetchContactSubmissionsByIds( $arrintContactSubmissionIds, $objDatabase ) {
		if( false == valArr( $arrintContactSubmissionIds ) ) {
			return NULL;
		}

		return CContactSubmissions::fetchCustomContactSubmissionsByIdsByCid( $arrintContactSubmissionIds, $this->getId(), $objDatabase );
	}

	public function fetchClubsByIds( $arrintClubIds, $objDatabase, $boolIsRejectedClubs = false, $boolIsReturnDataInArray = false ) {
		return CClubs::fetchClubsByIdsByCid( $arrintClubIds, $this->getId(), $objDatabase, $boolIsRejectedClubs, $boolIsReturnDataInArray );
	}

	public function fetchCompanyEventsByIds( $arrintCompanyEventIds, $objDatabase ) {
		return CCompanyEvents::fetchCompanyEventsByIdsByCid( $arrintCompanyEventIds, $this->getId(), $objDatabase );
	}

	public function fetchCompanyClassifiedCategories( $objDatabase ) {
		return CCompanyClassifiedCategories::fetchCompanyClassifiedCategoriesByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyEventTypes( $objDatabase ) {
		return CCompanyEventTypes::fetchCompanyEventTypesByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyClassifiedCategoryById( $intCompanyClassifiedCategoryId, $objDatabase ) {
		if( true == is_null( $intCompanyClassifiedCategoryId ) ) {
			return NULL;
		}

		return CCompanyClassifiedCategories::fetchCompanyClassifiedCategoryByIdByCid( $intCompanyClassifiedCategoryId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyEventTypeById( $intCompanyEventTypeId, $objDatabase ) {
		if( true == is_null( $intCompanyEventTypeId ) ) {
			return NULL;
		}

		return CCompanyEventTypes::fetchCompanyEventTypeByIdByCid( $intCompanyEventTypeId, $this->getId(), $objDatabase );
	}

	public function fetchCustomerClassifiedById( $intCustomerClassifiedId, $objDatabase ) {
		return CCustomerClassifieds::fetchCustomerClassifiedByIdByCid( $intCustomerClassifiedId, $this->getId(), $objDatabase );
	}

	public function fetchCustomerClassifiedsByIds( $arrintCustomerClassifiedIds, $objDatabase ) {
		return CCustomerClassifieds::fetchCustomerClassifiedsByIdsByCid( $arrintCustomerClassifiedIds, $this->getId(), $objDatabase );
	}

	public function fetchPaginatedUnapprovedCustomerClassifiedsByPropertyIds( $objPagination, $arrintPropertyIds, $objDatabase, $boolIsRejectedClassifieds = false ) {
		return CCustomerClassifieds::fetchPaginatedUnapprovedCustomerClassifiedsByPropertyIdsByCid( $objPagination, $arrintPropertyIds, $this->getId(), $objDatabase, $boolIsRejectedClassifieds );
	}

	public function fetchUnapprovedCustomerClassifiedsCountByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CCustomerClassifieds::fetchUnapprovedCustomerClassifiedsCountByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchConciergeServicesCountByPropertyIds( $arrintPropertyIds, $objDatabase, $boolIsRemovedConciergeServices = false ) {
		return CCustomerConciergeServices::fetchConciergeServicesCountByCidByPropertyIds( $this->getId(), $arrintPropertyIds, $objDatabase, $boolIsRemovedConciergeServices );
	}

	public function fetchCompanyUsersByIds( $arrintCompanyUserIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUsersByIdsByCid( $arrintCompanyUserIds, $this->getId(), $objDatabase );
	}

	public function fetchChildDocumentsByParentDocumentId( $intParentDocumentId, $objDatabase ) {
		return CDocuments::fetchChildDocumentsByParentDocumentIdByCid( $intParentDocumentId, $this->getId(), $objDatabase );
	}

	public function fetchParentDocumentsByChildDocumentId( $intChildDocumentId, $objDatabase ) {
		return CDocuments::fetchParentDocumentsByDocumentIdByCid( $intChildDocumentId, $this->getId(), $objDatabase );
	}

	public function fetchParentDocumentById( $intDocumentId, $objDatabase, $boolIncludeArchived = false, $boolIncludeDeleted = false ) {
		return CDocuments::fetchParentDocumentByIdByCid( $intDocumentId, $this->getId(), $objDatabase, $boolIncludeArchived, $boolIncludeDeleted );
	}

	public function fetchPetTypes( $objDatabase ) {
		return CPetTypes::createService()->fetchPetTypesByCid( $this->getId(), $objDatabase );
	}

	public function fetchPetRateAssociations( $objDatabase ) {
		return \Psi\Eos\Entrata\Custom\CPetRateAssociations::createService()->fetchPetRateAssociationsByCid( $this->getId(), $objDatabase );
	}

	public function fetchPublishedPetTypes( $objDatabase ) {
		return CPetTypes::createService()->fetchPublishedPetTypesByCid( $this->getId(), $objDatabase );
	}

	public function fetchPetTypeById( $intPetTypeId, $objDatabase ) {
		return CPetTypes::createService()->fetchPetTypeByIdByCid( $intPetTypeId, $this->getId(), $objDatabase );
	}

	public function fetchCustomerPetsByPetTypeId( $intPetTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerPets::createService()->fetchCustomerPetsByPetTypeIdByCid( $intPetTypeId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyHolidayById( $intCompanyHolidayId, $objDatabase ) {
		return CCompanyHolidays::fetchCompanyHolidayByIdByCid( $intCompanyHolidayId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyCategoryById( $intId, $objDatabase ) {
		return CCompanyCategories::fetchCompanyCategoryByIdByCid( $intId, $this->getId(), $objDatabase );
	}

	public function fetchFileTypeById( $intId, $objDatabase ) {
		return CFileTypes::fetchFileTypeByIdByCid( $intId, $this->getId(), $objDatabase );
	}

	public function fetchApplicationById( $intApplicationId, $objDatabase ) {
		return CApplications::fetchCustomApplicationByIdByCid( $intApplicationId, $this->getId(), $objDatabase );
	}

	public function fetchPaginatedApplicationsByPropertyIdsByLeaseIntervalTypeIds( $arrintPropertyIds, $arrintLeaseIntervalTypeIds, $objApplicationsFilter, $objDatabase ) {
		return CApplications::fetchPaginatedApplicationsByPropertyIdsByLeaseIntervalTypeIdsByCid( $arrintPropertyIds, $arrintLeaseIntervalTypeIds, $objApplicationsFilter, $this->getId(), $objDatabase );
	}

	public function fetchPaginatedApplicationsByPropertyIdsByLeaseIntervalTypeIdsByTransmissionVendorIdByApplicationsFilterByCid( $arrintPropertyIds, $arrintLeaseIntervalTypeIds, $arrintTransmissionVendorId, $objApplicationsFilter, $objDatabase ) {
		return CApplications::fetchPaginatedApplicationsByPropertyIdsByLeaseIntervalTypeIdsByTransmissionVendorIdByApplicationsFilterByCid( $arrintPropertyIds, $arrintLeaseIntervalTypeIds, $arrintTransmissionVendorId, $objApplicationsFilter, $this->getId(), $objDatabase );
	}

	public function fetchTotalApplicationsCountByPropertyIdsByApplicationFilterByLeaseIntervalTypeIds( $arrintPropertyIds, $arrintLeaseIntervalTypeIds, $objApplicationsFilter, $objDatabase ) {
		return CApplications::fetchTotalApplicationsCountByPropertyIdsByApplicationFilterByLeaseIntervalTypeIdsByCid( $arrintPropertyIds, $arrintLeaseIntervalTypeIds, $objApplicationsFilter, $this->getId(), $objDatabase );
	}

	public function fetchFirstPrimaryApplicationByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CApplications::fetchFirstPrimaryApplicationByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchApplicantsByApplicationIdsByCustomerTypeId( $arrintApplicationIds, $intCustomerTypeId, $objDatabase ) {
		return CApplicants::fetchApplicantsByApplicationIdsByByCustomerTypeIdByCid( $arrintApplicationIds, $intCustomerTypeId, $this->getId(), $objDatabase );
	}

	public function fetchApplicantById( $intApplicantId, $objDatabase, $boolLoadFromView = true ) {
		return CApplicants::fetchSimpleApplicantByIdByCid( $intApplicantId, $this->getId(), $objDatabase, $boolLoadFromView );
	}

	public function fetchApplicantByUsernameByFirstNameByLastNameByHistoricaldays( $strNameFirst, $strNameLast, $strUsername, $intHistoricalDays, $objDatabase, $arrintApplicationStageStatusIds = NULL ) {
		return CApplicants::fetchApplicantByUsernameByFirstNameByLastNameByHistoricaldaysByCid( $strNameFirst, $strNameLast, $strUsername, $intHistoricalDays, $this->getId(), $objDatabase, $arrintApplicationStageStatusIds );
	}

	public function fetchApplicantWithPasswordByUsernameByFirstNameByLastNameByHistoricalDays( $strNameFirst, $strNameLast, $strUsername, $intHistoricalDays, $objDatabase ) {
		return CApplicants::fetchApplicantWithPasswordByUsernameByFirstNameByLastNameByHistoricalDaysByCid( $strNameFirst, $strNameLast, $strUsername, $intHistoricalDays, $this->getId(), $objDatabase );
	}

	public function fetchArPaymentTransmissionById( $intArPaymentTransmissionId, $objDatabase ) {
		return CArPaymentTransmissions::fetchArPaymentTransmissionByIdByCid( $intArPaymentTransmissionId, $this->getId(), $objDatabase );
	}

	public function fetchApplicantApplicationByApplicationIdByApplicantId( $intApplicationId, $intApplicantId, $objDatabase ) {
		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationByApplicationIdByApplicantIdByCid( $intApplicationId, $intApplicantId, $this->getId(), $objDatabase );
	}

	public function fetchApplicantsByApplicationId( $intApplicationId, $objDatabase ) {
		return CApplicants::fetchApplicantsByApplicationIdByCid( $intApplicationId, $this->getId(), $objDatabase );
	}

	public function fetchApplicantsByApplicantApplicationIds( $arrintApplicantApplicationIds, $objDatabase ) {
		return CApplicants::fetchApplicantsByApplicantApplicationIdsByCid( $arrintApplicantApplicationIds, $this->getId(), $objDatabase );
	}

	public function fetchApplicantsByFirstNameByLastNameByUsernameByCustomerTypes( $strNameFirst, $strNameLast, $strUsername, $arrstrCustomerTypes, $objDatabase ) {
		return CApplicants::fetchApplicantsByFirstNameByLastNameByUsernameByCustomerTypesByCid( $strNameFirst, $strNameLast, $strUsername, $arrstrCustomerTypes, $this->getId(), $objDatabase );
	}

	public function fetchWebsitePropertiesByWebsiteId( $intWebsiteId, $objDatabase ) {
		return CWebsiteProperties::createService()->fetchWebsitePropertiesByWebsiteIdByCid( $intWebsiteId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyTransmissionVendorById( $intCompanyTransmissionVendorId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyTransmissionVendors::createService()->fetchCompanyTransmissionVendorByIdByCid( $intCompanyTransmissionVendorId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyTransmissionVendors( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyTransmissionVendors::createService()->fetchCompanyTransmissionVendorsByCid( $this->getId(), $objDatabase );
	}

	public function fetchTransmissionsByApplicationIds( $arrintApplicationIds, $objDatabase ) {

		if( false == valArr( $arrintApplicationIds ) ) {
			return NULL;
		}

		return CTransmissions::fetchTransmissionsByApplicationIdsByCid( $arrintApplicationIds, $this->getId(), $objDatabase );
	}

	public function fetchCompanyTransmissionVendorByPropertyIdByTransmissionTypeId( $intPropertyId, $intTransmissionTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyTransmissionVendors::createService()->fetchCompanyTransmissionVendorByPropertyIdByTransmissionTypeIdByCid( $intPropertyId, $intTransmissionTypeId, $this->getId(), $objDatabase );
	}

	public function fetchDocumentAddendaByDocumentId( $intDocumentId, $objDatabase ) {
		return CDocumentAddendas::fetchDocumentAddendaByCidByDocumentId( $this->getId(), $intDocumentId, $objDatabase );
	}

	public function fetchDocumentAddendas( $intDocumentAddendaId, $objDatabase ) {
		return CDocumentAddendas::fetchDocumentAddendaByIdByCid( $intDocumentAddendaId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyConciergeServiceById( $intCompanyConciergeServiceId, $objDatabase ) {
		return CCompanyConciergeServices::fetchCompanyConciergeServiceByIdByCid( $intCompanyConciergeServiceId, $this->getId(), $objDatabase );
	}

	public function fetchCustomerConciergeServiceById( $intCompanyConciergeServiceId, $objDatabase ) {
		return CCustomerConciergeServices::fetchCustomerConciergeServiceByIdByCid( $intCompanyConciergeServiceId, $this->getId(), $objDatabase );
	}

	public function fetchCustomerConciergeServiceByIds( $arrintCompanyConciergeServiceIds, $objDatabase ) {
		return CCustomerConciergeServices::fetchCustomerConciergeServiceByIdsByCid( $arrintCompanyConciergeServiceIds, $this->getId(), $objDatabase );
	}

	public function fetchCompanyResidenceTypes( $objDatabase ) {
		return CCompanyResidenceTypes::fetchCompanyResidenceTypesByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyStateMetaSetting( $strStateCode, $objDatabase ) {
		return CStateMetaSettings::createService()->fetchStateMetaSettingByCidByStateCode( $this->getId(), $strStateCode, $objDatabase );
	}

	public function fetchCompanyStateAreaMetaSetting( $strStateCode, $intCompanyAreaId, $objDatabase ) {
		return CStateMetaSettings::createService()->fetchStateMetaSettingByCidByStateCodeByCompanyAreaId( $this->getId(), $strStateCode, $intCompanyAreaId, $objDatabase );
	}

	public function fetchCompanyCompanyAreaMetaSetting( $intCompanyAreaId, $objDatabase ) {
		return CStateMetaSettings::createService()->fetchStateMetaSettingByCidByCompanyAreaId( $this->getId(), $intCompanyAreaId, $objDatabase );
	}

	public function fetchCompanyAreas( $objDatabase ) {
		return CCompanyAreas::createService()->fetchSimpleCompanyAreasByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyAreasByCountryCode( $strCountryCode, $objDatabase ) {
		return CCompanyAreas::createService()->fetchCompanyAreasByCountryCodeByCid( $strCountryCode, $this->getId(), $objDatabase );
	}

	public function fetchCompanyAreaById( $intCompanyAreaId, $objDatabase ) {
		return CCompanyAreas::createService()->fetchCompanyAreaByIdByCid( $intCompanyAreaId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyAreaWithSubAreasById( $intCompanyAreaId, $objDatabase ) {
		return CCompanyAreas::createService()->fetchCompanyAreaWithSubAreasByIdByCid( $intCompanyAreaId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyAreasByPropertyIds( $arrintPropertyIds, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		return CCompanyAreas::createService()->fetchCompanyAreasByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchCompanyAreasByPropertyIdsByStateCode( $arrintPropertyIds, $strStateCode, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		return CCompanyAreas::createService()->fetchCompanyAreasByStateCodeByPropertyIdsByCid( $strStateCode, $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchCitiesByPropertyIds( $arrintPropertyIds, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = '	SELECT
						city, state_code
					FROM property_addresses
					WHERE cid = ' . ( int ) $this->getId() . '
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND is_alternate = false
					GROUP BY city, state_code
					ORDER BY city ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public function fetchPropertiesByCompanyApplicationIdByCompanyUserId( $intCompanyApplicationId, $intCompanyUserId, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByCompanyApplicationIdByCompanyUserIdByCid( $intCompanyApplicationId, $intCompanyUserId, $this->getId(), $objDatabase );
	}

	public function fetchPropertiesByPropertyApplicationIds( $arrintPropertyApplicationIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByPropertyApplicationIdsByCid( $arrintPropertyApplicationIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyAreasByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		return \Psi\Eos\Entrata\CPropertyAreas::createService()->fetchPropertyAreasByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyAreasByPropertyIdsByHasAvaialability( $arrintPropertyIds, $objDatabase, $intShowAvailability = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		return \Psi\Eos\Entrata\CPropertyAreas::createService()->fetchPropertyAreasByPropertyIdsByCidByHasAvailability( $arrintPropertyIds, $this->getId(), $objDatabase, $intShowAvailability );
	}

	public function fetchPropertyPhoneNumbersByPropertyIdsByPhoneNumberTypeIdByLeadSourceId( $arrintPropertyIds, $intPhoneNumberTypeId, $intLeadSourceId, $objDatabase ) {
		return CPropertyPhoneNumbers::fetchPropertyPhoneNumbersByPropertyIdsByPhoneNumberTypeIdByLeadSourceIdByCid( $arrintPropertyIds, $intPhoneNumberTypeId, $intLeadSourceId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyLeadSourcePhoneNumbersByPropertyIdsByInternetListingServiceId( $arrintPropertyIds, $intInternetListingServiceId, $objProspectPortalDatabase ) {
		return \Psi\Eos\Entrata\CPropertyPhoneNumbers::createService()->fetchPropertyLeadSourcePhoneNumbersByPropertyIdsByInternetListingServiceIdByCid( $arrintPropertyIds, $intInternetListingServiceId, $this->getId(), $objProspectPortalDatabase );
	}

	public function fetchOldScheduledPayments( $objDatabase ) {
		return CScheduledPayments::fetchOldScheduledPaymentsByCid( $this->getId(), $objDatabase );
	}

	public function fetchCareerById( $intCareerId, $objDatabase ) {
		return CCareers::createService()->fetchCareerByIdByCid( $intCareerId, $this->getId(), $objDatabase );
	}

	public function fetchSortedCareerTypes( $objDatabase ) {
		return CCareerTypes::createService()->fetchSortedCareerTypesByCid( $this->getId(), $objDatabase );
	}

	public function fetchSortedCareerTypesByPropertyIds( $arrintPropertyIds, $objDatabase, $boolShowAnonymous = true ) {
		return CCareerTypes::createService()->fetchSortedCareerTypesByCidByPropertyIds( $this->getId(), $arrintPropertyIds, $objDatabase, $boolShowAnonymous );
	}

	public function fetchCareerApplications( $objDatabase ) {
		return CCareerApplications::createService()->fetchSimpleCareerApplicationsByCid( $this->getId(), $objDatabase );
	}

	public function fetchCareerApplicationById( $intApplicationId, $objDatabase ) {
		return CCareerApplications::createService()->fetchSimpleCareerApplicationByIdByCid( $intApplicationId, $this->getId(), $objDatabase );
	}

	public function fetchSwitchCareerApplicationByCareerApplicationIdByDirection( $intCareerApplicationOrderNumber, $strDirection, $objDatabase ) {
		return CCareerApplications::createService()->fetchSwitchCareerApplicationByCareerApplicationIdByDirectionByCid( $intCareerApplicationOrderNumber, $strDirection, $this->getId(), $objDatabase );
	}

	public function fetchTotalCareers( $objCareerFilter, $objDatabase ) {
		return CCareers::createService()->fetchTotalCareersByCid( $this->getId(), $objCareerFilter, $objDatabase );
	}

	public function fetchCareersCountByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CCareers::createService()->fetchCareersCountByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPaginatedCareers( $intPageNumber, $intPageSize, $objCareerFilter, $objDatabase ) {
		return CCareers::createService()->fetchPaginatedCareersWithCareerTypesAndCareerSubTypesByCid( $this->getId(), $intPageNumber, $intPageSize, $objCareerFilter, $objDatabase );
	}

	public function fetchCareersByPropertyIdsOrderByStates( $objCareerFilter, $arrintPropertyIds, $objDatabase ) {
		return CCareers::createService()->fetchCareersByCidByPropertyIdsOrderByStates( $this->getId(), $objCareerFilter, $arrintPropertyIds, $objDatabase );
	}

	public function fetchCareersByPropertyIdsOrderByTypes( $objCareerFilter, $arrintPropertyIds, $objDatabase ) {
		return CCareers::createService()->fetchCareersByCidByPropertyIdsOrderByTypes( $this->getId(), $objCareerFilter, $arrintPropertyIds, $objDatabase );
	}

	public function fetchCareersByPropertyIdsByCareerStatusTypeIdsOrderByTypes( $objCareerFilter, $objCareerSearch, $arrintPropertyIds, $arrintCareerStatusTypeIds, $objDatabase ) {
		return CCareers::createService()->fetchCareersByCidByPropertyIdsByCareerStatusTypeIdsOrderByTypes( $this->getId(), $objCareerFilter, $objCareerSearch, $arrintPropertyIds, $arrintCareerStatusTypeIds, $objDatabase );
	}

	public function fetchApplicantsByPropertyIds( $arrintPropertyIds, $objDatabase, $boolIsView = false ) {
		return CApplicants::fetchApplicantsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase, $boolIsView );
	}

	public function fetchApplicantApplicationsByPropertyIds( $arrintPropertyIds, $objDatabase, $boolIncludeDeletedAA = false ) {
		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase, $boolIncludeDeletedAA );
	}

	public function fetchPressReleaseCategories( $objDatabase ) {
		return CPressReleaseCategories::createService()->fetchCustomPressReleaseCategoriesByCid( $this->getId(), $objDatabase );
	}

	public function fetchImplementedPressReleaseCategories( $objDatabase, $boolShowFutureDate = false ) {
		return CPressReleaseCategories::createService()->fetchImplementedPressReleaseCategoriesByCid( $this->getId(), $objDatabase, $boolShowFutureDate );
	}

	public function fetchImplementedPressReleaseCategoriesByPressReleaseCategoryPrefix( $strPressReleaseCategoryPrefix, $objDatabase, $boolShowFutureDate = false ) {
		return CPressReleaseCategories::createService()->fetchImplementedPressReleaseCategoriesByCidByPressReleaseCategoryPrefix( $this->getId(), $strPressReleaseCategoryPrefix, $objDatabase, $boolShowFutureDate );
	}

	public function fetchPublishedPressReleases( $objDatabase, $boolShowFutureDate = false, $intLimit = NULL, $boolOrderByCategory = true ) {
		return CPressReleases::createService()->fetchPublishedPressReleasesByCid( $this->getId(), $objDatabase, $boolShowFutureDate, $intLimit, $boolOrderByCategory );
	}

	public function fetchPressReleaseById( $intPressReleaseId, $objDatabase ) {
		return CPressReleases::createService()->fetchCustomPressReleaseByIdByCid( $intPressReleaseId, $this->getId(), $objDatabase );
	}

	public function fetchPublishedPressReleaseById( $intPressReleaseId, $objDatabase, $boolShowFutureDate = false ) {
		return CPressReleases::createService()->fetchPublishedPressReleaseByCidById( $this->getId(), $intPressReleaseId, $objDatabase, $boolShowFutureDate );
	}

	public function fetchPressReleaseCategoryById( $intPressReleaseCategoryId, $objDatabase ) {
		return CPressReleaseCategories::createService()->fetchCustomPressReleaseCategoryByIdByCid( $intPressReleaseCategoryId, $this->getId(), $objDatabase );
	}

	public function fetchPressReleasesByPressReleaseCategoryId( $intPressReleaseCategoryId, $objDatabase ) {
		return CPressReleases::createService()->fetchCustomPressReleasesByPressReleaseCategoryIdByCid( $intPressReleaseCategoryId, $this->getId(), $objDatabase );
	}

	public function fetchPublishedPressReleasesByPressReleaseCategoryNameByLimit( $strPressReleaseCategoryName, $intLimit, $objDatabase, $boolShowFutureDate = false ) {
		return CPressReleases::createService()->fetchPublishedPressReleasesByCidByPressReleaseCategoryNameByLimit( $this->getId(), $strPressReleaseCategoryName, $intLimit, $objDatabase, $boolShowFutureDate );
	}

	public function fetchPublishedPressReleasesByPressReleaseCategoryPrefixByLimit( $strPressReleaseCategoryPrefix, $intLimit, $objDatabase, $boolShowFutureDate = false ) {
		return CPressReleases::createService()->fetchPublishedPressReleasesByCidByPressReleaseCategoryPrefixByLimit( $this->getId(), $strPressReleaseCategoryPrefix, $intLimit, $objDatabase, $boolShowFutureDate );
	}

	public function fetchCommissionRateAssociations( $objDatabase ) {
		return CCommissionRateAssociations::createService()->fetchCommissionRateAssociationsByCid( $this->getId(), $objDatabase );
	}

	public function fetchCommissionRateAssociationsByCommissionStructureId( $intCommissionStructureId, $objDatabase ) {
		return CCommissionRateAssociations::createService()->fetchCommissionRateAssociationsByCidByCommissionStructureId( $this->getId(), $intCommissionStructureId, $objDatabase );
	}

	public function fetchPaginatedCommissions( $intPageNo, $intPageSize, $intShowDisabledData, $objDatabase ) {
		return CCommissions::createService()->fetchPaginatedCommissionsByCid( $this->getId(), $intPageNo, $intPageSize, $intShowDisabledData, $objDatabase );
	}

	public function fetchPaginatedCommissionsCount( $intShowDisabledData, $objDatabase ) {
		return CCommissions::createService()->fetchPaginatedCommissionsCountByCid( $this->getId(), $intShowDisabledData, $objDatabase );
	}

	public function fetchCommissionRateAssociationsByAccountIdByCommissionStructureId( $intAccountId, $intCommissionStructureId, $objDatabase ) {
		return CCommissionRateAssociations::createService()->fetchCommissionRateAssociationsByCidByAccountIdByCommissionStructureId( $this->getId(), $intAccountId, $intCommissionStructureId, $objDatabase );
	}

	public function fetchAssociatedCommissionStrucutres( $objDatabase ) {
		return CCommissionStructures::createService()->fetchAssociatedCommissionStructuresByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyAreaSearchesByCompanyAreaId( $intCompanyAreaId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyAreaSearches::createService()->fetchCompanyAreaSearchesByCompanyAreaIdByCid( $intCompanyAreaId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyUserByEmailAddress( $strEmailAddress, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByEmailAddressByCid( $strEmailAddress, $this->getId(), $objDatabase );
	}

	public function fetchCompanyUsersByCompanyEmployeeId( $intCompanyEmployeeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUsersByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyUserByCompanyEmployeeId( $intCompanyEmployeeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $this->getId(), $objDatabase );
	}

	public function fetchPaginatedInboundCallsByCallsSearchByPropertyIds( $intPageNo, $intCountPerPage, $objInboundCallSearch, $arrintPropertyIds, $objVoipDatabase ) {
		return CInboundCalls::fetchPaginatedInboundCallsByCidByCallsSearchByPropertyIds( $intPageNo, $intCountPerPage, $this->getId(), $objInboundCallSearch, $arrintPropertyIds, $objVoipDatabase );
	}

	public function fetchTotalInboundCallsCountByCallsSearchByPropertyIds( $objInboundCallSearch, $arrintPropertyIds, $objVoipDatabase ) {
		return CInboundCalls::fetchTotalInboundCallsCountByCidByCallsSearchByPropertyIds( $this->getId(), $objInboundCallSearch, $arrintPropertyIds, $objVoipDatabase );
	}

	public function fetchInboundCallProcessesByInboundCallIds( $arrintInboundCallIds, $objVoipDatabase ) {
		return CInboundCallProcesses::fetchInboundCallProcessesByInboundCallIdsByCid( $arrintInboundCallIds, $this->getId(), $objVoipDatabase );
	}

	public function fetchScheduledCallById( $intScheduledCallId, $objDatabase ) {
		return CScheduledCalls::fetchScheduledCallByIdByCid( $intScheduledCallId, $this->getId(), $objDatabase );
	}

	public function fetchCallById( $intCallId, $objDatabase ) {
		return CCalls::fetchCallByCidById( $this->getId(), $intCallId, $objDatabase );
	}

	public function fetchCallInfoById( $intCallId, $objDatabase ) {
		return CCalls::fetchCallInfoByCidById( $this->getId(), $intCallId, $objDatabase );
	}

	public function fetchAllAccountsWithBalances( $objDatabase, $intMonthInterval = NULL ) {
		return CAccounts::createService()->fetchAllAccountsWithBalancesByCid( $this->getId(), $objDatabase, $intMonthInterval );
	}

	public function fetchPropertyByIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchLeadSourceByIds( $arrintLeadSourceIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeadSources::createService()->fetchLeadSourceByIdsByCid( $arrintLeadSourceIds, $this->getId(), $objDatabase );
	}

	public function fetchIlsEmailById( $intIlsEmailId, $objDatabase ) {
		return \Psi\Eos\Entrata\CIlsEmails::createService()->fetchIlsEmailByIdByCid( $intIlsEmailId, $this->getId(), $objDatabase );
	}

	public function fetchCallsByPropertyIdsByIds( $arrintPropertyIds, $arrintIds, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		return CCalls::fetchCallsByCidByPropertyIdsByIds( $this->getId(), $arrintPropertyIds, $arrintIds, $objDatabase );
	}

	public function fetchApplicationsByIds( $arrintApplicationIds, $objDatabase, $boolIncludePrimaryApplicantDetails = false, $boolIncludeLeaseDocumentId = false ) {
		if( false == valArr( $arrintApplicationIds ) ) {
			return NULL;
		}

		return CApplications::fetchApplicationsByIdsByCid( $arrintApplicationIds, $this->getId(), $objDatabase, $boolIncludePrimaryApplicantDetails, $boolIncludeLeaseDocumentId );
	}

	public function fetchPropertiesForIlsFeedByInternetListingServiceId( $intInternetListingServiceId, $objDatabase, $boolIsLimited = NULL ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesForIlsFeedByInternetListingServiceIdByCid( $intInternetListingServiceId, $this->getId(), $objDatabase, $boolIsLimited );
	}

	public function fetchFeaturedPropertiesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchFeaturedPropertiesByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchEventResults( $objDatabase ) {
		return \Psi\Eos\Entrata\CEventResults::createService()->fetchCustomEventResultsByCid( $this->getId(), $objDatabase );
	}

	public function fetchArchivedTypePropertyListItemsCountByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyListItems::createService()->fetchPropertyListItemsCountByListTypeIdByPropertyIdsByCid( CListType::LEAD_ARCHIVE_REASONS, $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchAmenityRateAssociationsByPropertyIdsByAmenityTypeId( $arrintPropertyIds, $intAmenityTypeId, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		return \Psi\Eos\Entrata\Custom\CAmenityRateAssociations::createService()->fetchAmenityRateAssociationsByPropertyIdsByAmenityTypeIdByCid( $arrintPropertyIds, $intAmenityTypeId, $this->getId(), $objDatabase );
	}

	public function fetchReviewsByIds( $arrintReviewIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CReviews::createService()->fetchReviewsByIdsByCid( $arrintReviewIds, $this->getId(), $objDatabase );
	}

	public function fetchTestimonial( $intReviewId, $objDatabase ) {
		return \Psi\Eos\Entrata\CReviews::createService()->fetchAllReviewRelatedDataByIdByCid( $intReviewId, $this->getId(), $objDatabase );
	}

	public function fetchReviewRatingTypes( $intReviewDetailId, $intPropertyId, $objDatabase ) {
		return \Psi\Eos\Entrata\CReviewDetailRatings::createService()->fetchReviewRatingTypesByPropertyIdByCidByReviewId( $intPropertyId, $this->getId(), $intReviewDetailId, $objDatabase );
	}

	public function fetchAverageRatingAndReview( $arrintReviewTypeIds, $objDatabase, $intPropertyId = NULL ) {
		return \Psi\Eos\Entrata\CReviews::createService()->fetchAverageRatingAndReviewByCid( $this->getId(), $arrintReviewTypeIds, $objDatabase, $intPropertyId );
	}

	public function fetchWebsiteRedirectById( $intWebsiteRedirectId, $objDatabase ) {
		return CWebsiteRedirects::createService()->fetchCustomWebsiteRedirectByIdByCid( $intWebsiteRedirectId, $this->getId(), $objDatabase );
	}

	public function fetchWebsiteRedirectsByIds( $arrintWebsiteRedirectIds, $objDatabase ) {
		if( false == valArr( $arrintWebsiteRedirectIds ) ) {
			return NULL;
		}

		return CWebsiteRedirects::createService()->fetchWebsiteRedirectsByIdsByCid( $arrintWebsiteRedirectIds, $this->getId(), $objDatabase );
	}

	public function fetchAccountRegion( $intAccountRegionId, $objDatabase ) {
		return CAccountRegions::createService()->fetchAccountRegionByIdAndCid( $intAccountRegionId, $this->getId(), $objDatabase );
	}

	public function fetchAccountRegions( $objDatabase, $boolShowDisabledData = true ) {
		return CAccountRegions::createService()->fetchAccountRegionsByCid( $this->getId(), $objDatabase, $boolShowDisabledData );
	}

	public function fetchActivePublishedReviewsByPropertyIdsByReviewTypeIdByLimit( $arrintPropertyIds, $intReviewTypeId, $objDatabase, $intLimit = NULL ) {
		return \Psi\Eos\Entrata\CReviews::createService()->fetchActivePublishedReviewByCidByPropertyIdsByReviewTypeIdByLimit( $this->getId(), $arrintPropertyIds, $intReviewTypeId, $objDatabase, $intLimit );
	}

	public function fetchCompanyMediaFilesByMediaTypeId( $intMediaTypeId, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByMediaTypeIdByCid( $intMediaTypeId, $this->getId(), $objDatabase );
	}

	public function fetchPaginatedReviewsByReviewTypeId( $intReviewTypeId, $intPageNo, $intPageSize, $objDatabase ) {
		return \Psi\Eos\Entrata\CReviews::createService()->fetchPaginatedReviewsByCidByReviewTypeId( $this->getId(), $intReviewTypeId, $intPageNo, $intPageSize, $objDatabase );
	}

	public function fetchPaginatedReviewsByPropertyIdsByReviewTypeId( $arrintPropertyIds, $intReviewTypeId, $intPageNo, $intPageSize, $objDatabase ) {
		return \Psi\Eos\Entrata\CReviews::createService()->fetchPaginatedReviewsByCidByPropertyIdsByReviewTypeId( $this->getId(), $arrintPropertyIds, $intReviewTypeId, $intPageNo, $intPageSize, $objDatabase );
	}

	public function fetchPaginatedTestimonialsCountByReviewTypeId( $intReviewTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CReviews::createService()->fetchPaginatedTestimonialsCountByCidByReviewTypeId( $this->getId(), $intReviewTypeId, $objDatabase );
	}

	public function fetchAllWebsiteRedirects( $strQuickSearch, $strWebsiteRedirectSortBy, $strWebsiteRedirectSortOrder, $objDatabase ) {
		return CWebsiteRedirects::createService()->fetchCustomWebsiteRedirectsByCid( $this->getId(), $strQuickSearch, $strWebsiteRedirectSortBy, $strWebsiteRedirectSortOrder, $objDatabase );
	}

	public function fetchWebsiteRedirectsOldUrls( $objDatabase ) {
		return CWebsiteRedirects::createService()->fetchWebsiteRedirectOldUrlsByCid( $this->getId(), $objDatabase );
	}

	public function fetchWebsiteRedirectsNewUrls( $objDatabase ) {
		return CWebsiteRedirects::createService()->fetchWebsiteRedirectNewUrlsByCid( $this->getId(), $objDatabase );
	}

	public function fetchPaginatedWebsiteRedirects( $strQuickSearch, $intPageNo, $intPageSize, $strWebsiteRedirectSortBy, $strWebsiteRedirectSortOrder, $objDatabase ) {
		return CWebsiteRedirects::createService()->fetchPaginatedWebsiteRedirectsByCid( $this->getId(), $strQuickSearch, $intPageNo, $intPageSize, $strWebsiteRedirectSortBy, $strWebsiteRedirectSortOrder, $objDatabase );
	}

	public function fetchPaginatedWebsiteRedirectsCount( $strQuickSearch, $objDatabase ) {
		return CWebsiteRedirects::createService()->fetchPaginatedWebsiteRedirectsCountByCid( $this->getId(), $strQuickSearch, $objDatabase );
	}

	public function fetchPaginatedWebsiteRedirectsSearchCount( $strQuickSearch, $objDatabase ) {
		return CWebsiteRedirects::createService()->fetchPaginatedWebsiteRedirectsSearchCountByCid( $this->getId(), $strQuickSearch, $objDatabase );
	}

	public function fetchNewUrlByOldUrl( $strOldUrl, $objDatabase ) {
		return CWebsiteRedirects::createService()->fetchNewUrlByOldUrlByCid( $strOldUrl, $this->getId(), $objDatabase );
	}

	public function fetchNewUrlByDomainName( $strDomainName, $objDatabase ) {
		return CWebsiteRedirects::createService()->fetchNewUrlByDomainNameByCid( $strDomainName, $this->getId(), $objDatabase );
	}

	public function fetchStatesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CStates::createService()->fetchStatesByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyBuildingById( $intPropertyBuildingId, $objDatabase ) {
		return CPropertyBuildings::createService()->fetchPropertyBuildingByIdByCid( $intPropertyBuildingId, $this->getId(), $objDatabase );
	}

	public function fetchAllPropertyBuildings( $objDatabase ) {
		return CPropertyBuildings::createService()->fetchPropertyBuildingsByCid( $this->getId(), $objDatabase );
	}

	public function fetchPropertyBuildingsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CPropertyBuildings::createService()->fetchPropertyBuildingsByCidByPropertyIds( $this->getId(), $arrintPropertyIds, $objDatabase );
	}

	public function fetchPaginatedCallBlocks( $intPageNo, $intPageSize, $objDatabase ) {
		return CCallBlocks::fetchPaginatedCallBlocksByCid( $intPageNo, $intPageSize, $this->getId(), $objDatabase );
	}

	public function fetchPaginatedCallBlocksCount( $objDatabase ) {
		return CCallBlocks::fetchPaginatedCallBlocksCountByCid( $this->getId(), $objDatabase );
	}

	public function fetchFloorplanGroups( $objDatabase ) {
		return CFloorplanGroups::createService()->fetchFloorplanGroupsByCid( $this->getId(), $objDatabase );
	}

	public function fetchFloorplanGroupById( $intId, $objDatabase ) {
		return CFloorplanGroups::createService()->fetchFloorplanGroupByCidById( $this->getId(), $intId, $objDatabase );
	}

	public function fetchPublishedFloorplanGroups( $objDatabase ) {
		return CFloorplanGroups::createService()->fetchPublishedFloorplanGroupsByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyMediaFilesByIds( $arrintCompanyMediaFileIds, $objDatabase ) {

		if( false == valArr( $arrintCompanyMediaFileIds ) ) {
			return NULL;
		}

		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByIdsByCid( $arrintCompanyMediaFileIds, $this->getId(), $objDatabase );
	}

	public function fetchCompanyEmployeesOrderByName( $objDatabase ) {
		return CCompanyEmployees::createService()->fetchCompanyEmployeesOrderByNameByCid( $this->getId(), $objDatabase );
	}

	public function fetchApPayees( $objDatabase ) {
		return CApPayees::fetchApPayeesByCid( $this->getId(), $objDatabase );
	}

	public function fetchApPayeesByApPayeeTypeIds( $arrintApPayeeTypes, $objClientDatabase ) {
		return ( array ) \Psi\Eos\Entrata\CApPayees::createService()->fetchApPayeesByApPayeeTypeIdsByCid( $arrintApPayeeTypes, $this->getId(), $objClientDatabase );
	}

	public function fetchApPayeeTerms( $objDatabase ) {

		$strSql = 'SELECT * FROM ap_payee_terms WHERE cid = ' . $this->getId();

		return CApPayeeTerms::fetchApPayeeTerms( $strSql, $objDatabase );
	}

	public function fetchApPayeeLocationsByApPayeeLocationIds( $arrintApPayeeLocationIds, $objDatabase ) {
		return CApPayeeLocations::fetchApPayeeLocationsByIdsByCid( $arrintApPayeeLocationIds, $this->getId(), $objDatabase );
	}

	public function fetchApPayeeById( $intApPayeeId, $objDatabase ) {
		return CApPayees::fetchApPayeeByIdByCid( $intApPayeeId, $this->getId(), $objDatabase );
	}

	public function fetchApPayeeByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CApPayees::fetchApPayeeByRemotePrimaryKeyByCid( $strRemotePrimaryKey, $this->getId(), $objDatabase );
	}

	public function fetchGlTrees( $objClientDatabase ) {
		return CGlTrees::fetchGlTreesByCid( $this->getId(), $objClientDatabase );
	}

	public function fetchGlGroups( $objClientDatabase ) {
		return CGlGroups::fetchGlGroupsByCid( $this->getId(), $objClientDatabase );
	}

	public function fetchGlAccountByGlAccountSystemCodeByGlChartSystemCode( $strGlAccountSystemCode, $strGlChartSystemCode, $objClientDatabase ) {
		return CGlAccounts::createService()->fetchGlAccountByCidByGlAccountSystemCodeByGlChartSystemCode( $this->getId(), $strGlAccountSystemCode, $strGlChartSystemCode, $objClientDatabase );
	}

	public function fetchCompanyAmenityFilters( $objDatabase ) {
		return \Psi\Eos\Entrata\CAmenityFilters::createService()->fetchCustomAmenityFiltersByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyAmenityFilterById( $intAmenityFilterId, $objDatabase ) {
		return \Psi\Eos\Entrata\CAmenityFilters::createService()->fetchAmenityFilterByIdByCid( $intAmenityFilterId, $this->getId(), $objDatabase );
	}

	public function fetchWebsiteAmenityDependencyByAmenityFilterId( $intAmenityFilterId, $objDatabase ) {
		return CWebsiteAmenities::createService()->fetchWebsiteAmenityDependencyByAmenityFilterIdByCid( $intAmenityFilterId, $this->getId(), $objDatabase );
	}

	public function fetchWebsiteAmenitiesByAmenityFilterId( $intAmenityFilterId, $objDatabase ) {
		return CWebsiteAmenities::createService()->fetchWebsiteAmenitiesByAmenityFilterIdByCid( $intAmenityFilterId, $this->getId(), $objDatabase );
	}

	public function fetchApDetailsByApHeaderId( $intApHeaderId, $objClientDatabase ) {
		return \Psi\Eos\Entrata\CApDetails::createService()->fetchApDetailsByInvoiceOrPaymentApHeaderIdsByCid( [ $intApHeaderId ], $this->getId(), $objClientDatabase );
	}

	public function fetchApHeaderById( $intApHeaderId, $objClientDatabase, $boolHasJobCostingProduct = false ) {
		return CApHeaders::fetchSimpleApHeaderByIdByCid( $intApHeaderId, $this->getId(), $objClientDatabase, $boolHasJobCostingProduct );
	}

	public function fetchApHeadersByIds( $arrintApHeaderIds, $objClientDatabase ) {
		return CApHeaders::fetchSimpleApHeadersByIdsByCid( $arrintApHeaderIds, $this->getId(), $objClientDatabase );
	}

	public function fetchApHeadersByApPaymentIds( $arrintApPaymentIds, $objClientDatabase ) {
		return CApHeaders::fetchApHeadersByApPaymentIdsByCid( $arrintApPaymentIds, $this->getId(), $objClientDatabase );
	}

	public function fetchApDetailsByIds( $arrintApDetailIds, $objClientDatabase, $boolSkipReversal = true ) {
		return \Psi\Eos\Entrata\CApDetails::createService()->fetchApDetailsByIdsByCid( $arrintApDetailIds, $this->getId(), $objClientDatabase, $boolSkipReversal );
	}

	public function fetchApPayeesByApPayeeTypeId( $intApPayeeTypeId, $objClientDatabase ) {
		return CApPayees::fetchApPayeesByApPayeeTypeIdByCid( $intApPayeeTypeId, $this->getId(), $objClientDatabase );
	}

	public function fetchApPayeesByApPayeeTypeIdByApPayeeStatusTypeIds( $intApPayeeTypeId, $arrintApPayeeStatusTypeIds, $objClientDatabase, $arrmixApPayeesInfo ) {
		return CApPayees::fetchApPayeesByCidByApPayeeTypeIdByApPayeeStatusTypeIds( $this->getId(), $intApPayeeTypeId, $arrintApPayeeStatusTypeIds, $objClientDatabase, $arrmixApPayeesInfo );
	}

	public function fetchSortedCompanyGroups( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyGroups::createService()->fetchSortedCompanyGroupsByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyDocumentBySeoUri( $strSeoUri, $objClientDatabase ) {
		return CDocuments::fetchDocumentBySeoUriByCid( $strSeoUri, $this->getId(), $objClientDatabase );
	}

	public function fetchDocumentByName( $strName, $objClientDatabase ) {
		return CDocuments::fetchDocumentByNameByCid( $strName, $this->getId(), $objClientDatabase );
	}

	public function fetchDocumentByNameByDocumentTypeIdByDocumentSubTypeId( $strName, $intDocumentTypeId, $intDocumentSubTypeId, $objClientDatabase ) {
		return CDocuments::fetchDocumentByNameByDocumentTypeIdByDocumentSubTypeIdByCid( $strName, $intDocumentTypeId, $intDocumentSubTypeId, $this->getId(), $objClientDatabase );
	}

	public function fetchCompanyDocumentById( $intWebsiteDocumentId, $objClientDatabase ) {
		return CDocuments::fetchCustomDocumentByIdByCid( $intWebsiteDocumentId, $this->getId(), $objClientDatabase );
	}

	public function fetchPaginatedArDepositsByBankAccountIds( $arrintBankAccountIds, $objArDepositsFilter, $objClientDatabase, $boolIsPSIAdministrator = false, $intPageNo = NULL, $intPageSize = NULL, bool $boolIsCorporateTab = false ) {
		return \Psi\Eos\Entrata\CArDeposits::createService()->fetchPaginatedArDepositsByBankAccountIdsByCid( $arrintBankAccountIds, $this->getId(), $objArDepositsFilter, $objClientDatabase, $boolIsPSIAdministrator, $intPageNo, $intPageSize, $boolIsCorporateTab );
	}

	public function fetchArDepositById( $intArDepositId, $objClientDatabase ) {
		if( false == is_numeric( $intArDepositId ) ) {
			return NULL;
		}

		return \Psi\Eos\Entrata\CArDeposits::createService()->fetchArDepositByIdByCid( $intArDepositId, $this->getId(), $objClientDatabase );
	}

	public function fetchMessageById( $intId, $objSmsDatabase ) {
		return \Psi\Eos\Sms\CMessages::createService()->fetchMessageByIdByCid( $intId, $this->getId(), $objSmsDatabase );
	}

	public function fetchPropertyMoveOutListItems( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyListItems::createService()->fetchPropertyListItemsByListTypeIdByCid( CListType::MOVE_OUT, $this->getId(), $objDatabase );
	}

	public function fetchPropertyCancelListItems( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyListItems::createService()->fetchPropertyListItemsByListTypeIdByCid( CListType::LEAD_CANCELLATION, $this->getId(), $objDatabase );
	}

	public function fetchCheckTemplates( $objClientDatabase ) {
		return CCheckTemplates::fetchSimpleCheckTemplatesByCid( $this->getId(), $objClientDatabase );
	}

	public function fetchCheckTemplateById( $intCheckTemplateId, $objClientDatabase ) {
		return CCheckTemplates::fetchCheckTemplateByIdByCid( $intCheckTemplateId, $this->getId(), $objClientDatabase );
	}

	public function fetchGlAccountsByGlAccountTypeIds( $arrintGlAccountTypeIds, $objClientDatabase ) {
		return CGlAccounts::createService()->fetchGlAccountsByGlAccountTypeIdsByCid( $arrintGlAccountTypeIds, $this->getId(), $objClientDatabase );
	}

	public function fetchContractPropertiesByContractIds( $arrintContractIds, $objAdminDatabase, $boolIsPilot = false, $boolIsTermination = false, $boolIsRequestedToTerminate = false ) {
		return CContractProperties::createService()->fetchActiveContractPropertiesByContractIdsByCid( $arrintContractIds, $this->getId(), $objAdminDatabase, $boolIsPilot, $boolIsTermination, $boolIsRequestedToTerminate );
	}

	public function fetchDocumentAddendasByIds( $arrintDocumentAddendaIds, $objDatabase ) {

		if( false == valArr( $arrintDocumentAddendaIds ) ) {
			return NULL;
		}

		return CDocumentAddendas::fetchDocumentAddendasByCidByIds( $this->getId(), $arrintDocumentAddendaIds, $objDatabase );
	}

	public function fetchLeaseDocumentPacketById( $intDocumentId, $objDatabase ) {
		return CDocuments::fetchLeaseDocumentPacketByIdByCid( $intDocumentId, $this->getId(), $objDatabase );
	}

	public function fetchLeaseDocumentPackateById( $intDocumentId, $objDatabase ) {
		return CDocuments::fetchLeaseDocumentPacketByIdByCid( $intDocumentId, $this->getId(), $objDatabase );
	}

	public function fetchLeaseDocumentsByIds( $arrintDocumentIds, $objDatabase ) {
		return CDocuments::fetchLeaseDocumentsByIdsByCid( $arrintDocumentIds, $this->getId(), $objDatabase );
	}

	public function fetchFileSignaturesByFileAssociationIds( $arrintFileAssociationIds, $objDatabase ) {
		if( false == valArr( $arrintFileAssociationIds ) ) {
			return NULL;
		}

		return \Psi\Eos\Entrata\CFileSignatures::createService()->fetchFileSignaturesByFileAssociationIdsByCid( $arrintFileAssociationIds, $this->getId(), $objDatabase );
	}

	public function fetchUtilities( $objDatabase ) {
		return CUtilities::fetchAllUtilitiesByCid( $this->getId(), $objDatabase );
	}

	public function fetchApplicantsByIds( $arrintApplicantIds, $objDatabase ) {
		if( false == valArr( $arrintApplicantIds ) ) {
			return NULL;
		}

		return CApplicants::fetchApplicantsByCidByIds( $this->getId(), $arrintApplicantIds, $objDatabase );
	}

	public function fetchLeasesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeasesByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchApplicantApplicationsByApplicantIds( $arrintApplicantIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationsByApplicantIdsByCid( $arrintApplicantIds, $this->getId(), $objDatabase );
	}

	public function fetchCompanyEmployeesByCompanyUserIds( $arrintCompanyUserIds, $objDatabase ) {

		if( false == valArr( $arrintCompanyUserIds ) ) {
			return false;
		}

		return CCompanyEmployees::createService()->fetchCompanyEmployeesByCompanyUserIdsByCid( $arrintCompanyUserIds, $this->getId(), $objDatabase );
	}

	public function fetchFileAssociationByFileId( $intFileId, $objDatabase ) {
		return CFileAssociations::createService()->fetchFileAssociationByFileIdByCid( $intFileId, $this->getId(), $objDatabase );
	}

	public function fetchFileAssociationsByFileIdByCampaignIdByCid( $intFileId, $intCampaignId, $objDatabase ) {
		return CFileAssociations::createService()->fetchFileAssociationsByFileIdByCampaignIdByCid( $intFileId, $intCampaignId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyPhoneNumberByPhoneNumberTypeId( $intPhoneNumberTypeId, $objDatabase ) {
		return CCompanyPhoneNumbers::fetchCompanyPhoneNumberByCidByPhoneNumberTypeId( $this->getId(), $intPhoneNumberTypeId, $objDatabase );
	}

	public function fetchCompanyPhoneNumbersByPhoneNumberTypeIds( $arrintPhoneNumberTypeIds, $objDatabase ) {

		$strSql = 'SELECT * FROM company_phone_numbers cpn WHERE cpn.cid  = ' . ( int ) $this->getId() . ' AND cpn.phone_number_type_id IN ( ' . implode( ',', $arrintPhoneNumberTypeIds ) . ' )';

		return CCompanyPhoneNumbers::fetchCompanyPhoneNumbers( $strSql, $objDatabase );
	}

	public function fetchCompanyAddressByAddressTypeId( $intAddressTypeId, $objDatabase ) {
		return CCompanyAddresses::fetchCompanyAddressByCidByPhoneNumberTypeId( $this->getId(), $intAddressTypeId, $objDatabase );
	}

	public function fetchActiveContractsDetails( $objAdminDatabase, $arrstrContractFilter = [], $boolIsNonTerminated = false, $boolRenewalContractId = true ) {
		return CContracts::createService()->fetchActiveContractsDetailsByCid( $this->getId(), $objAdminDatabase, $arrstrContractFilter, $boolIsNonTerminated, $boolRenewalContractId );
	}

	public function fetchPaginatedActiveContractsDetailsOrCount( $objAdminDatabase, $arrstrContractFilter = [], $boolCountOnly = false, $boolProperty = false, $arrintPsProductIds = NULL, $intPropertyId = NULL, $boolReturnAllContracts = false ) {
		return CContracts::createService()->fetchPaginatedActiveContractsDetailsOrCountByCid( $this->getId(), $objAdminDatabase, $arrstrContractFilter, $boolCountOnly, $boolProperty, $arrintPsProductIds, $intPropertyId, $boolReturnAllContracts );
	}

	public function fetchAddOns( $objDatabase ) {
		return \Psi\Eos\Entrata\CAddOns::createService()->fetchActiveAddOnsByCid( $this->getId(), $objDatabase );
	}

	public function fetchPublishedAppsWithAppInstalledByAppIds( $arrintAppIds, $objDatabase, $boolIsNotInstalled = false ) {
		return \Psi\Eos\Entrata\CApps::createService()->fetchPublishedAppsWithAppInstalledByCidByAppIds( $this->getId(), $arrintAppIds, $objDatabase, $boolIsNotInstalled );
	}

	public function fetchPropertyAppsByAppIds( $arrintAppIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyApps::createService()->fetchPropertyAppsByAppIdsByCid( $arrintAppIds, $this->getId(), $objDatabase );
	}

	public function fetchCompanyAppsByAppIds( $arrintAppIds, $objDatabase ) {
		return CCompanyApps::fetchCompanyAppsByAppIdsByCid( $arrintAppIds, $this->getId(), $objDatabase );
	}

	public function fetchPublishedCompanyAppByAppId( $intAppId, $objDatabase ) {
		return CCompanyApps::fetchPublishedCompanyAppByAppIdByCid( $intAppId, $this->getId(), $objDatabase );
	}

	public function fetchLeaseBalanceDueDetailsByLeaseIds( $arrintLeaseIds, $objDatabase, $boolShowWriteOffDetails = false ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseBalanceDueDetailsByLeaseIdsByCid( $arrintLeaseIds, $this->getId(), $objDatabase, $boolShowWriteOffDetails );
	}

	public function fetchPropertyPrimaryOnSiteContactsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CPropertyOnSiteContacts::fetchPropertyOnSiteContactsByPropertyIdsContactTypeIdByCid( $arrintPropertyIds, CContactType::MANAGER, $this->getId(), $objDatabase );
	}

	public function fetchApPayeesByApPayeeTypeIdsByApPayeeStatusTypeIdsByPropertyIds( $arrintApPayeeTypeIds, $arrintApPayeeStatusTypeIds, $arrintPropertyIds, $objClientDatabase ) {
		return CApPayees::fetchApPayeesByApPayeeTypeIdsByApPayeeStatusTypeIdsByPropertyIdsByCid( $arrintApPayeeTypeIds, $arrintApPayeeStatusTypeIds, $arrintPropertyIds, $this->getId(), $objClientDatabase );
	}

	public function fetchSimpleMaintenanceRequestByCustomerId( $intCompanyUserId, $objDatabase ) {
		return CMaintenanceRequests::createService()->fetchSimpleMaintenanceRequestByCustomerIdByCid( $intCompanyUserId, $this->getId(), $objDatabase );
	}

	public function fetchEnabledPropertyIds( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyIdsByCid( $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceRequestFilterById( $intMaintenanceRequestFilterId, $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceRequestFilters::createService()->fetchMaintenanceRequestFilterByIdByCid( $intMaintenanceRequestFilterId, $this->getId(), $objDatabase );
	}

	public function fetchDirectiveByDirectiveTypeId( $intDirectiveTypeId, $objDatabase ) {
		return CDirectives::fetchDirectiveByCidByDirectiveTypeId( $this->getId(), $intDirectiveTypeId, $objDatabase );
	}

	public function fetchCompetingDirectiveCount( $objDatabase ) {
		return CDirectives::fetchCompetingDirectiveCountByDomainByDirectiveTypeIdByCid( $this->getRwxDomain(), CDirectiveType::CLIENT, $this->getId(), $objDatabase );
	}

	public function fetchGlSetting( $objDatabase ) {
		return CGlSettings::fetchGlSettingsByCid( $this->getId(), $objDatabase );
	}

	public function fetchGlAccounts( $objClientDatabase ) {
		return CGlAccounts::createService()->fetchGlAccountsByCid( $this->getId(), $objClientDatabase );
	}

	public function fetchActiveGlAccounts( $objClientDatabase ) {
		return CGlAccounts::createService()->fetchActiveGlAccountsByCid( $this->getId(), $objClientDatabase );
	}

	public function fetchSimpleActiveGlAccounts( $objClientDatabase ) {
		return CGlAccounts::createService()->fetchSimpleActiveGlAccountsByCid( $this->getId(), $objClientDatabase );
	}

	public function fetchSimpleActiveGlAccountsByGlChartSystemCode( $strGlChartSystemCode, $objClientDatabase, $intLastSyncOn = NULL ) {
		return CGlAccounts::createService()->fetchSimpleActiveGlAccountsByCidByGlChartSystemCode( $this->getId(), $strGlChartSystemCode, $objClientDatabase, $intLastSyncOn );
	}

	public function fetchGlAccountsByGlChartId( $intGlChartId, $objClientDatabase ) {
		return CGlAccounts::createService()->fetchGlAccountsByCidByGlChartId( $this->getId(), $intGlChartId, $objClientDatabase );
	}

	public function fetchGlAccountsByGlAccountGroupSystemCodesByGlChartSystemCode( $arrstrGlAccountGroupSystemCodes, $strGlChartSystemCode, $objClientDatabase ) {
		return CGlAccounts::createService()->fetchGlAccountsByCidByGlAccountGroupSystemCodesByGlChartSystemCode( $this->getId(), $arrstrGlAccountGroupSystemCodes, $strGlChartSystemCode, $objClientDatabase );
	}

	public function fetchActiveGlAccountsByGlChartSystemCode( $strGlChartSystemCode, $objClientDatabase, $boolAllowPostingToRestrictedGlAccounts = true, $boolJEAllowPostingToRestrictedGlAccounts = true, $boolIsFromBillbackListing = false ) {
		return CGlAccounts::createService()->fetchActiveGlAccountsByCidByGlChartSystemCode( $this->getId(), $strGlChartSystemCode, $objClientDatabase, $boolAllowPostingToRestrictedGlAccounts, $boolJEAllowPostingToRestrictedGlAccounts, $boolIsFromBillbackListing );
	}

	public function fetchGlAccountsByGlAccountTypeIdsByGlChartSystemCode( $arrintGlAccountTypeIds, $strGlChartSystemCode, $objClientDatabase ) {
		return CGlAccounts::createService()->fetchGlAccountsByCidByGlAccountTypeIdsByGlChartSystemCode( $this->getId(), $arrintGlAccountTypeIds, $strGlChartSystemCode, $objClientDatabase );
	}

	public function fetchCompanyPricings( $boolIsShowDisabledData, $objAdminDatabase ) {
		return CCompanyPricings::fetchCompanyPricingsByCid( $this->getId(), $objAdminDatabase, $boolIsShowDisabledData );
	}

	public function fetchCompanyPricingsByPropertyIds( $arrintPropertyIds, $objAdminDatabase ) {
		return CCompanyPricings::fetchCompanyPricingByCidByPropertyIds( $this->getId(), $arrintPropertyIds, $objAdminDatabase );
	}

	public function fetchGlChartBySystemCode( $strSystemCode, $objClientDatabase ) {
		return CGlCharts::fetchGlChartBySystemCodeByCid( $strSystemCode, $this->getId(), $objClientDatabase );
	}

	public function fetchAllGlAccounts( $objClientDatabase ) {
		return CGlAccounts::createService()->fetchEnabledGlAccountsByCid( $this->getId(), $objClientDatabase );
	}

	public function fetchAllApPayeeTerms( $objDatabase ) {
		return CApPayeeTerms::fetchAllApPayeeTermsByCid( $this->getId(), $objDatabase );
	}

	public function fetchApPayeeTermById( $intId, $objDatabase ) {
		return CApPayeeTerms::fetchApPayeeTermByIdByCid( $intId, $this->getId(), $objDatabase );
	}

	public function fetchApPayeesByIds( $arrintIds, $objClientDatabase ) {
		return CApPayees::fetchApPayeesByIdsByCid( $arrintIds, $this->getId(), $objClientDatabase );
	}

	public function fetchApPayeeContacts( $objClientDatabase ) {
		return CApPayeeContacts::fetchApPayeeContactsByCid( $this->getId(), $objClientDatabase );
	}

	public function fetchApPayeeContactByRemotePrimaryKey( $strRemotePrimaryKey, $objClientDatabase ) {
		return CApPayeeContacts::fetchApPayeeContactByRemotePrimaryKeyByCid( $strRemotePrimaryKey, $this->getId(), $objClientDatabase );
	}

	public function fetchPsLead( $objAdminDatabase ) {
		return CPsLeads::fetchPsLeadByCid( $this->getId(), $objAdminDatabase );
	}

	public function fetchActivePsLead( $objDatabase ) {
		return CPsLeads::fetchActivePsLeadByCid( $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceTemplates( $objClientDatabase ) {
		return CMaintenanceTemplates::createService()->fetchMaintenanceTemplatesByCid( $this->getId(), $objClientDatabase );
	}

	public function fetchMaintenanceTemplateById( $intMaintenanceTemplateId, $objClientDatabase ) {
		return CMaintenanceTemplates::createService()->fetchMaintenanceTemplateByCidById( $this->getId(), $intMaintenanceTemplateId, $objClientDatabase );
	}

	public function fetchMaintenanceTemplatesByParentMaintenanceTemplateId( $intParentMaintenanceTemplateId, $objClientDatabase ) {
		return CMaintenanceTemplates::createService()->fetchMaintenanceTemplateByCidByParentMaintenanceTemplateId( $this->getId(), $intParentMaintenanceTemplateId, $objClientDatabase );
	}

	public function fetchCustomMaintenanceTemplatesByParentMaintenanceTemplateId( $intParentMaintenanceTemplateId, $objClientDatabase ) {
		return CMaintenanceTemplates::createService()->fetchCustomMaintenanceTemplatesByParentMaintenanceTemplateIdByCid( $intParentMaintenanceTemplateId, $this->getId(), $objClientDatabase );
	}

	public function fetchCustomMaintenanceTemplatesByParentMaintenanceTemplateIds( $arrintParentMaintenanceTemplateIds, $objClientDatabase ) {
		return CMaintenanceTemplates::createService()->fetchCustomMaintenanceTemplatesByParentMaintenanceTemplateIdsByCid( $arrintParentMaintenanceTemplateIds, $this->getId(), $objClientDatabase );
	}

	public function fetchFilteredAmenitiesByAmenityTypeIdByPropertyIds( $intAmenityType, $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CDefaultAmenities::createService()->fetchDefaultAmenitiesByAmenityTypeIdByPropertyIdsByCid( $intAmenityType, $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchGlHeaderById( $intGlHeaderId, $objDatabase ) {
		return CGlHeaders::fetchCustomGlHeaderByIdByCid( $intGlHeaderId, $this->getId(), $objDatabase );
	}

	public function fetchPaginatedApPayeesByApPayeeSearchByPropertyIds( $arrintPropertyIds, $objApPayeeFilter, $objPagination, $objClientDatabase, $boolIsAdmin ) {
		return CApPayees::fetchPaginatedApPayeesByApPayeeSearchByPropertyIdsByCid( $arrintPropertyIds, $objApPayeeFilter, $objPagination, $this->getId(), $objClientDatabase, $boolIsAdmin );
	}

	public function fetchTotalApPayeesCountByApPayeeSearchByPropertyIds( $arrintPropertyIds, $objApPayeeFilter, $objClientDatabase, $boolIsAdmin ) {
		return CApPayees::fetchTotalApPayeesCountByCidByApPayeeSearchByPropertyIds( $this->getId(), $arrintPropertyIds, $objApPayeeFilter, $objClientDatabase, $boolIsAdmin );
	}

	public function fetchTableLogsByFormFilter( $arrstrFormFilter, $objClientDatabase, $arrmixSqlParam = NULL ) {
		return CTableLogs::fetchTableLogsByCidByFormFilter( $this->getId(), $arrstrFormFilter, $objClientDatabase, $arrmixSqlParam );
	}

	public function fetchVisibleCheckComponentsByCheckTemplateId( $intCheckTemplateId, $objClientDatabase ) {
		return CCheckComponents::fetchVisibleCheckComponentsByCheckTemplateIdByCid( $intCheckTemplateId, $this->getId(), $objClientDatabase );
	}

	public function fetchVisibleCheckComponentsByCheckTemplateIds( $arrintCheckTemplateIds, $objClientDatabase ) {
		return CCheckComponents::fetchVisibleCheckComponentsByCheckTemplateIdsByCid( $arrintCheckTemplateIds, $this->getId(), $objClientDatabase );
	}

	public function fetchAllGlAccountsByGlChartSystemCode( $strGlChartSystemCode, $objClientDatabase, $boolAllowPostingToRestrictedGlAccounts = true, $boolJEAllowPostingToRestrictedGlAccounts = true, $boolIsActiveOnly = true ) {
		return CGlAccounts::createService()->fetchAllGlAccountsByCidByGlChartSystemCode( $this->getId(), $strGlChartSystemCode, $objClientDatabase, $boolAllowPostingToRestrictedGlAccounts, $boolJEAllowPostingToRestrictedGlAccounts, $boolIsActiveOnly );
	}

	public function fetchLateFeeFormulas( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CLateFeeFormulas::createService()->fetchLateFeeFormulasByCid( $this->getId(), $objClientDatabase );
	}

	public function fetchPrimaryCompanyAddress( $objAdminDatabase ) {
		return CCompanyAddresses::fetchPrimaryCompanyAddressByCid( $this->getId(), $objAdminDatabase );
	}

	public function fetchDefaultCheckTemplate( $objClientDatabase ) {
		return CCheckTemplates::fetchDefaultCheckTemplateByCid( $this->getId(), $objClientDatabase );
	}

	public function fetchCompanyDepartmentByName( $strName, $objClientDatabase ) {
		return CCompanyDepartments::createService()->fetchCompanyDepartmentByNameByCid( $strName, $this->getId(), $objClientDatabase );
	}

	public function fetchPropertyGlSettingsByPropertyIds( $arrintPropertyIds, $objClientDatabase ) {
		return CPropertyGlSettings::fetchPropertyGlSettingsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objClientDatabase );
	}

	public function fetchPropertyGlSettingsByBankAccountId( $intBankAccountId, $objClientDatabase ) {
		return CPropertyGlSettings::fetchPropertyGlSettingsByBankAccountIdByCid( $intBankAccountId, $this->getId(), $objClientDatabase );
	}

	public function fetchNeighborhoodPropertiesByPropertyAddressByPropertyIdByIdsByMiles( $objPrimaryPropertyAddress, $intPropertyId, $arrintPropertyIds, $objDatabase, $intMilesWithin = 50 ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchNeighborhoodPropertiesByPropertyAddressByPropertyIdByIdsByMilesByCid( $objPrimaryPropertyAddress, $intPropertyId, $arrintPropertyIds, $this->getId(), $objDatabase, $intMilesWithin );
	}

	public function fetchOpenCompanyTasksCountOrderByTaskTypes( $objAdminDatabase ) {
		return \Psi\Eos\Admin\CTasks::createService()->fetchOpenCompanyTasksCountByCidOrderByTaskTypes( $this->getId(), $objAdminDatabase );
	}

	public function fetchPaginatedLeasesByLeaseRenewalsFilter( $intPageNo, $intPageSize, $objLeaseRenewalsFilter, $objClientDatabase, $boolIsOfferGeneratedRenewals = true, $boolIsReturnIds = false ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchPaginatedLeasesByCidByLeaseRenewalsFilter( $this->getId(), $intPageNo, $intPageSize, $objLeaseRenewalsFilter, $objClientDatabase, $boolIsOfferGeneratedRenewals, $boolIsReturnIds );
	}

	public function fetchTotalLeasesCountByLeaseRenewalsFilter( $objLeaseRenewalsFilter, $objClientDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchTotalLeasesCountByCidByLeaseRenewalsFilter( $this->getId(), $objLeaseRenewalsFilter, $objClientDatabase );
	}

	public function fetchPropertyOccupations( $objDataBase ) {
		return CPropertyOccupations::fetchPropertyOccupationsByCid( $this->getId(), $objDataBase );
	}

	public function fetchOccupations( $objDataBase ) {
		return COccupations::fetchOccupationsByCid( $this->getId(), $objDataBase );
	}

	public function fetchCompanyMoveOutListItems( $objDataBase ) {
		return \Psi\Eos\Entrata\CListItems::createService()->fetchListItemsByListTypeIdByCid( CListType::MOVE_OUT, $this->getId(), $objDataBase );
	}

	public function fetchCompanyCancelListItems( $objDataBase ) {
		return \Psi\Eos\Entrata\CListItems::createService()->fetchListItemsByListTypeIdByCid( CListType::LEAD_CANCELLATION, $this->getId(), $objDataBase );
	}

	public function fetchOccupationById( $intOccupationId, $objDatabase ) {
		return COccupations::fetchOccupationByIdByCid( $intOccupationId, $this->getId(), $objDatabase );
	}

	public function fetchListItemById( $intListItemId, $objDatabase ) {
		return \Psi\Eos\Entrata\CListItems::createService()->fetchListItemByIdByCid( $intListItemId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyEvents( $objDatabase ) {
		return CCompanyEvents::fetchSimpleCompanyEventsByCid( $this->getId(), $objDatabase );
	}

	public function fetchEventsByIds( $arrintIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchEventsByIdsByCid( $arrintIds, $this->getId(), $objDatabase );
	}

	public function fetchAnnouncementsByAnnouncementTypeId( $intAnnouncementTypeId, $objDatabase ) {
		return CAnnouncements::fetchAnnouncementsByCidByAnnouncementTypeId( $this->getId(), $intAnnouncementTypeId, $objDatabase );
	}

	public function fetchAnnouncementPsProductByAnnouncementId( $intAnnouncementId, $objDatabase ) {
		return CAnnouncementPsProducts::fetchAnnouncementPsProductByAnnouncementIdByCid( $intAnnouncementId, $this->getId(), $objDatabase );
	}

	public function fetchParentCompanyMediaFolderBySelectedCompanyMediaFolderId( $intSelectedCompanyMediaFolderId, $objDatabase ) {
		return CCompanyMediaFolders::createService()->fetchParentCompanyMediaFolderBySelectedCompanyMediaFolderIdByCid( $intSelectedCompanyMediaFolderId, $this->getId(), $objDatabase );
	}

	public function fetchFileTypeBySystemCode( $strSystemCode, $objDatabase ) {
		return CFileTypes::fetchFileTypeBySystemCodeByCid( $strSystemCode, $this->getId(), $objDatabase );
	}

	public function fetchFileTypeByName( $strAttachmentTypeName, $objDatabase ) {
		return CFileTypes::fetchFileTypeByNameByCid( $strAttachmentTypeName, $this->getId(), $objDatabase );
	}

	public function fetchFileByRemotePrimaryKey( $strFileRemotePrimaryKey, $objDatabase ) {
		return CFiles::createService()->fetchFilefetchFileByRemotePrimaryKeyByCid( $this->getId(), $strFileRemotePrimaryKey, $objDatabase );
	}

	public function fetchDocumentMergeFieldsByDocumentIds( $arrintDocumentIds, $objDatabase, $boolIsFetchCount = false ) {
		if( false == valArr( $arrintDocumentIds ) ) {
			return NULL;
		}

		return CDocumentMergeFields::fetchDocumentMergeFieldsByDocumentIdsByCid( $arrintDocumentIds, $this->getId(), $objDatabase, $boolIsFetchCount );
	}

	public function fetchDocumentMergeFieldsByPropertyIdsByDocumentIds( $arrintPropertyIds, $arrintDocumentIds, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}
		if( false == valArr( $arrintDocumentIds ) ) {
			return NULL;
		}

		return CDocumentMergeFields::fetchDocumentMergeFieldsByPropertyIdsByDocumentIdsByCid( $arrintPropertyIds, $arrintDocumentIds, $this->getId(), $objDatabase );
	}

	public function fetchCompanyIdentificationTypes( $objDataBase ) {
		return CCompanyIdentificationTypes::fetchCompanyIdentificationTypesByCid( $this->getId(), $objDataBase );
	}

	public function fetchCompanyIdentificationTypeById( $intCompanyIdentificationTypeId, $objDatabase ) {
		return CCompanyIdentificationTypes::fetchCompanyIdentificationTypeByIdByCid( $intCompanyIdentificationTypeId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyIdentificationTypeBySystemCode( $strSystemCode, $objDatabase ) {
		return CCompanyIdentificationTypes::fetchCompanyIdentificationTypeBySystemCodeByCid( $strSystemCode, $this->getId(), $objDatabase );
	}

	public function fetchPropertyIdentificationTypes( $objDataBase ) {
		return CPropertyIdentificationTypes::fetchPropertyIdentificationTypesByCid( $this->getId(), $objDataBase );
	}

	public function fetchPropertiesByAddressTypeId( $intAddressTypeId, $objClientDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByAddressTypeIdByCid( $intAddressTypeId, $this->getId(), $objClientDatabase );
	}

	public function fetchActiveProperties( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchActivePropertiesByCid( $this->getId(), $objDatabase );
	}

	public function fetchDisabledProperties( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchDisabledPropertiesByCid( $this->getId(), $objDatabase );
	}

	public function fetchPaginatedActivePropertiesAndCount( $objDatabase, $objPagination = NULL ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPaginatedActivePropertiesAndCountByCid( $this->getId(), $objDatabase, $objPagination );
	}

	public function fetchMigrateOrNonIntegratedProperties( $objDatabase, $boolIsSimpleData = false, $arrintPropertyIds = NULL, $boolIsCorporateCustomer = false, $boolIsNotFromCorporate = false, $boolShowDisabledData = false ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchMigrateOrNonIntegratedPropertiesByCid( $this->getId(), $objDatabase, $boolIsSimpleData, $arrintPropertyIds, $boolIsCorporateCustomer, $boolIsNotFromCorporate, $boolShowDisabledData );
	}

	public function fetchApplicantApplicationsByApplicationIdsByCustomerTypeId( $arrintApplicationIds, $intCustomerTypeId, $objDatabase ) {
		if( false == valArr( array_filter( $arrintApplicationIds ) ) ) {
			return NULL;
		}

		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationsByApplicationIdsByCustomerTypeIdByCid( $arrintApplicationIds, $intCustomerTypeId, $this->getId(), $objDatabase );
	}

	public function fetchApplicantApplicationByPropertyIdByFirstNameByLastNameByLastFourDigitsOfPhoneNumber( $intPropertyId, $strNameFirst, $strNameLast, $strLastFourDigitsOfPhoneNumber, $objDatabase ) {
		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationByPropertyIdByFirstNameByLastNameByLastFourDigitsOfPhoneNumberByCid( $intPropertyId, $strNameFirst, $strNameLast, $strLastFourDigitsOfPhoneNumber, $this->getId(), $objDatabase );
	}

	public function fetchApplicantApplicationByPropertyIdsByFirstNameByLastNameByLastFourDigitsOfPhoneNumber( $arrintPropertyIds, $strNameFirst, $strNameLast, $strLastFourDigitsOfPhoneNumber, $objDatabase ) {
		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationByPropertyIdsByFirstNameByLastNameByLastFourDigitsOfPhoneNumberByCid( $arrintPropertyIds, $strNameFirst, $strNameLast, $strLastFourDigitsOfPhoneNumber, $this->getId(), $objDatabase );
	}

	public function fetchLastTransmissionsTransmissionTypeIdByApplicationIds( $intTransmissionTypeId, $arrintApplicationIds, $objDatabase ) {
		return CTransmissions::fetchLastTransmissionsByTransmissionTypeIdByApplicationIdsByCid( $intTransmissionTypeId, $arrintApplicationIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyLeasingAgentsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeasingAgents::createService()->fetchPropertyLeasingAgentsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchNonAcceptedRenewalOfferLeaseIdsByLeaseIds( $arrintLeaseIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchNonAcceptedRenewalOfferLeaseIdsByLeaseIdsByCid( $arrintLeaseIds, $this->getId(), $objDatabase );
	}

	public function fetchNonAcceptedTransferQuoteByLeaseId( $intLeaseId, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchNonAcceptedTransferQuoteByLeaseIdByCid( $intLeaseId, $this->getId(), $objDatabase );
	}

	public function fetchArchivedContracts( $objAdminDatabase, $arrstrContractFilter = NULL ) {
		return CContracts::createService()->fetchArchivedContractsByCid( $this->getId(), $objAdminDatabase, $arrstrContractFilter );
	}

	public function fetchLeadCommunicationSchedules( $objDatabase ) {
		return CLeadCommunicationSchedules::fetchCustomLeadCommunicationSchedulesByCid( $this->getId(), $objDatabase );
	}

	public function fetchBankAccounts( $objClientDatabase ) {
		return CBankAccounts::fetchBankAccountsByCid( $this->getId(), $objClientDatabase );
	}

	public function fetchPaginatedApHeadersByPropertyIds( $arrintPropertyIds, $objPagination, $objClientDatabase, $arrintInvoiceTypePermissions, $objApTransactionsFilter = NULL, $intCompanyUserId = NULL, $boolIsAdministrator = false ) {
		return CApHeaders::fetchPaginatedApHeadersByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objPagination, $objClientDatabase, $arrintInvoiceTypePermissions, $objApTransactionsFilter, $intCompanyUserId, $boolIsAdministrator );
	}

	public function fetchApHeadersCountByPropertyIds( $arrintPropertyIds, $objClientDatabase, $arrintInvoiceTypePermissions, $objApTransactionsFilter = NULL, $intCompanyUserId = NULL, $boolIsAdministrator = false ) {
		return CApHeaders::fetchApHeadersCountByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objClientDatabase, $arrintInvoiceTypePermissions, $objApTransactionsFilter, $intCompanyUserId, $boolIsAdministrator );
	}

	public function fetchApDetailsByApHeaderIds( $arrintApHeaderIds, $objClientDatabase, $boolIsCheckCopyDocument = false ) {
		if( false == valArr( $arrintApHeaderIds ) ) {
			return NULL;
		}

		$arrmixParameters = [
			'ap_header_ids'				=> $arrintApHeaderIds,
			'is_check_copy_document'	=> $boolIsCheckCopyDocument
		];

		return \Psi\Eos\Entrata\CApDetails::createService()->fetchSimpleApDetailsByApHeaderIdsByCid( $arrmixParameters, $this->getId(), $objClientDatabase );
	}

	public function fetchApPayeeLocationById( $intApPayeeLocationId, $objDatabase ) {
		return CApPayeeLocations::fetchApPayeeLocationByIdByCid( $intApPayeeLocationId, $this->getId(), $objDatabase );
	}

	public function fetchActivePropertyDetailsByPropertyIdsByGlTreeId( $arrintPropertyIds, $objDatabase, $intGlTreeId = NULL ) {
		return CPropertyDetails::createService()->fetchActivePropertyDetailsByCidByPropertyIdsByGlTreeId( $this->getId(), $arrintPropertyIds, $objDatabase, $intGlTreeId );
	}

	public function fetchActiveGlTreeById( $intGlTreeId, $objDatabase ) {
		return CGlTrees::fetchActiveGlTreeByIdByCid( $intGlTreeId, $this->getId(), $objDatabase );
	}

	public function fetchActiveGlTreesByIds( $arrintGlTreeIds, $objDatabase ) {
		return CGlTrees::fetchActiveGlTreesByIdsByCid( $arrintGlTreeIds, $this->getId(), $objDatabase );
	}

	public function fetchGlAccountTreesReportByGlTreeIdsByGlTreeTypeId( $arrintGlTreeIds, $intGlTreeTypeId, $objDatabase, $intGlGroupId = NULL, $intGlAccountTypeId = NULL, $arrstrAccountNumbers = [] ) {
		return CGlAccountTrees::fetchGlAccountTreesReportByCidByGlTreeIdsByGlTreeTypeId( $this->getId(), $arrintGlTreeIds, $intGlTreeTypeId, $objDatabase, $intGlGroupId, $intGlAccountTypeId, $arrstrAccountNumbers );
	}

	public function fetchPrimaryPerson( $objDatabase ) {
		return CPersons::createService()->fetchPrimaryPersonByCid( $this->getId(), $objDatabase );
	}

	public function fetchPersonContactPreferences( $objDatabase ) {
		return \Psi\Eos\Admin\CPersonContactPreferences::createService()->fetchPersonContactPreferencesByCid( $this->getId(), $objDatabase );
	}

	public function fetchPersons( $objDatabase ) {
		return CPersons::createService()->fetchPersonsByCid( $this->getId(), $objDatabase );
	}

	public function fetchPaginatedBankAccounts( $objPagination, $objClientDatabase, $objBankAccountFilter = NULL ) {
		return CBankAccounts::fetchPaginatedBankAccountsByCid( $this->getId(), $objPagination, $objClientDatabase, $objBankAccountFilter );
	}

	public function fetchTotalPaginatedBankAccounts( $objClientDatabase, $objBankAccountFilter = NULL ) {
		return CBankAccounts::fetchTotalPaginatedBankAccountsByCid( $this->getId(), $objClientDatabase, $objBankAccountFilter );
	}

	public function fetchBankAccountById( $intBankAccountId, $objClientDatabase ) {
		return CBankAccounts::fetchBankAccountByIdByCid( $intBankAccountId, $this->getId(), $objClientDatabase );
	}

	public function fetchBankAccountsByIds( $arrintBankAccountIds, $objClientDatabase ) {
		return CBankAccounts::fetchBankAccountsByIdsByCid( $arrintBankAccountIds, $this->getId(), $objClientDatabase );
	}

	public function fetchPropertyBankAccounts( $objClientDatabase ) {
		return CPropertyBankAccounts::fetchPropertyBankAccountsByCid( $this->getId(), $objClientDatabase );
	}

	public function fetchCountActivePropertyBankAccounts( $objClientDatabase ) {
		return CPropertyBankAccounts::fetchCountActivePropertyBankAccountsByCid( $this->getId(), $objClientDatabase );
	}

	public function fetchActivePropertyBankAccounts( $objClientDatabase, $boolCheckIsPrimary = true ) {
		return CPropertyBankAccounts::fetchActivePropertyBankAccountsByCid( $this->getId(), $objClientDatabase, $boolCheckIsPrimary );
	}

	/**
	 * @deprecated - use CApDetails::fetchUnpaidApDetailsByCidByFilter
	 */
	public function fetchUnpaidApDetailsByPropertyIds( $arrintPropertyIds, $objDatabase, $objApTransactionsFilter = NULL, $intPeriodId = NULL ) {
		return \Psi\Eos\Entrata\CApDetails::createService()->fetchUnpaidApDetailsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase, $objApTransactionsFilter, $intPeriodId );
	}

	public function fetchUndepositedArTransactionsByBankAccountIdByIds( $intBankAccountId, $arrintIds, $objClientDatabase ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchUndepositedArTransactionsByBankAccountIdByIdsByCid( $intBankAccountId, $arrintIds, $this->getId(), $objClientDatabase );
	}

	public function fetchArDeposits( $objDatabase ) {
		return \Psi\Eos\Entrata\CArDeposits::createService()->fetchArDepositsByCid( $this->getId(), $objDatabase );
	}

	public function fetchPaginatedAssociatedCompanyEmployees( $intPageNo, $intPageSize, $objDatabase, $boolIsDisabled = 0, $strCompanyUserSearchCriteria = NULL, $objCompanyUser = NULL, $intCompanyUserTypeId = CCompanyUserType::ENTRATA ) {
		return CCompanyEmployees::createService()->fetchPaginatedAssociatedCompanyEmployeesByCid( $intPageNo, $intPageSize, $this->getId(), $objDatabase, $boolIsDisabled, [ 'search_text' => $strCompanyUserSearchCriteria ], $objCompanyUser, $intCompanyUserTypeId );
	}

	public function fetchAssociatedCompanyEmployeesCount( $objDatabase, $boolIsDisabled = 0, $strCompanyUserSearchCriteria = NULL, $objCompanyUser = NULL ) {
		return CCompanyEmployees::createService()->fetchAssociatedCompanyEmployeesCountByCid( $this->getId(), $objDatabase, $boolIsDisabled, $strCompanyUserSearchCriteria, $objCompanyUser );
	}

	public function fetchCompanyUserGroupsCountByGroupId( $intGroupId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserGroups::createService()->fetchCompanyUserCountByGroupIdByCid( $intGroupId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyGroupsByCompanyUserCount( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyGroups::createService()->fetchCompanyGroupsByCompanyUserCountByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyGroupsByCompanyUserCountByCompanyGroupPermission( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyGroups::createService()->fetchCompanyGroupsByCompanyUserCountByCompanyGroupPermissionByCid( $this->getId(), $objDatabase );
	}

	public function fetchPaginatedAssociatedCompanyEmployeesByGroupId( $intGroupId, $intPageNo, $intPageSize, $objDatabase, $boolIncludeAdminUsers = false ) {
		return CCompanyEmployees::createService()->fetchPaginatedAssociatedCompanyEmployeesByGroupIdByCid( $intPageNo, $intPageSize, $intGroupId, $this->getId(), $objDatabase, $boolIncludeAdminUsers );
	}

	public function fetchAssociatedCompanyEmployeesCountByGroupId( $intGroupId, $objDatabase, $boolIncludeAdminUsers = false ) {
		return CCompanyEmployees::createService()->fetchAssociatedCompanyEmployeesCountByGroupIdByCid( $intGroupId, $this->getId(), $objDatabase, $boolIncludeAdminUsers );
	}

	public function fetchUnAssociatedCompanyEmployeesByGroupId( $intGroupId, $objDatabase, $boolIncludeAdminUsers = false ) {
		return CCompanyEmployees::createService()->fetchUnAssociatedCompanyEmployeesByGroupIdByCid( $intGroupId, $this->getId(), $objDatabase, $boolIncludeAdminUsers );
	}

	public function fetchLeaseIntervalsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLeaseIntervalsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchAllLeaseIntervalsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchAllLeaseIntervalsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchLeaseProcessesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchLeaseProcessesByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchLeaseProcessesByLeaseIds( $arrintLeaseIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchLeaseProcessesByLeaseIdsByCid( $arrintLeaseIds, $this->getId(), $objDatabase );
	}

	public function fetchAllLeaseProcessesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchAllLeaseProcessesByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchRouteByRouteId( $intRouteId, $objDatabase ) {
		return \Psi\Eos\Entrata\CRoutes::createService()->fetchCustomRouteByIdByCid( $intRouteId, $this->getId(), $objDatabase );
	}

	public function fetchAssociatePropertyDetailsByRouteIdByPropertyIds( $intRouteId, $arrintPropertyIds, $objDatabase ) {
		return CPropertyDetails::createService()->fetchAssociatePropertyDetailsByRouteIdByCidByPropertyIds( $intRouteId, $this->getId(), $arrintPropertyIds, $objDatabase );
	}

	public function fetchRouteProprtyDetailsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CPropertyDetails::createService()->fetchRouteProprtyDetailsByCidByPropertyIds( $this->getId(), $arrintPropertyIds, $objDatabase );
	}

	public function fetchPropertyDetailsByIds( $arrintIds, $objDatabase ) {
		return CPropertyDetails::createService()->fetchPropertyDetailsByIdsByCid( $arrintIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyDetailsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CPropertyDetails::createService()->fetchPropertyDetailsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyDetailsByRouteId( $intRouteId, $objDatabase ) {
		return CPropertyDetails::createService()->fetchPropertyDetailsByRouteIdByCid( $intRouteId, $this->getId(), $objDatabase );
	}

	public function fetchUnAssociatedProprtyDetailsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CPropertyDetails::createService()->fetchUnAssociatedProprtyDetailsByCidByPropertyIds( $this->getId(), $arrintPropertyIds, $objDatabase );
	}

	public function fetchApplicationsByApplicationFilter( $objApplicationFilter, $objDatabase, $boolIsReturnIds = false ) {
		return CApplications::fetchApplicationsByApplicationFilterByCid( $objApplicationFilter, $this->getId(), $objDatabase, $boolIsReturnIds );
	}

	public function fetchApplicationsCountByApplicationFilter( $objApplicationFilter, $objDatabase ) {
		return CApplications::fetchApplicationsCountByApplicationFilterByCid( $objApplicationFilter, $this->getId(), $objDatabase );
	}

	public function fetchPropertyUnitsCountByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CPropertyUnits::createService()->fetchPropertyUnitsCountByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchRoutesByIsPublished( $intIsPublished, $objDatabase ) {
		return \Psi\Eos\Entrata\CRoutes::createService()->fetchRoutesByIsPublishedByCid( $intIsPublished, $this->getId(), $objDatabase );
	}

	public function fetchActiveGlAccountsWithGlAccountTypeName( $objClientDatabase ) {
		return CGlAccounts::createService()->fetchActiveGlAccountsWithGlAccountTypeNameByCid( $this->getId(), $objClientDatabase );
	}

	public function fetchPaginatedApPaymentsByApPaymentTypeIdsByPropertyIds( $intPageNo, $intCountPerPage, $arrintApPaymentTypeIds, $arrintPropertyIds, $objClientDatabase, $objApTransactionFilter = NULL, $boolIsFromBulkVoidPayments = false, $boolShowDisabledData = false ) {
		return CApPayments::fetchPaginatedApPaymentsByCidByApPaymentTypeIdsByPropertyIds( $intPageNo, $intCountPerPage, $this->getId(), $arrintApPaymentTypeIds, $arrintPropertyIds, $objClientDatabase, $objApTransactionFilter, $boolIsFromBulkVoidPayments, $boolShowDisabledData );
	}

	public function fetchTotalPaginatedApPaymentsByApPaymentTypeIdsByPropertyIds( $arrintApPaymentTypeIds, $arrintPropertyIds, $objClientDatabase, $objApTransactionFilter = NULL, $boolIsFromBulkVoidPayments = false ) {
		return CApPayments::fetchTotalPaginatedApPaymentsByCidByApPaymentTypeIdsByPropertyIds( $this->getId(), $arrintApPaymentTypeIds, $arrintPropertyIds, $objClientDatabase, $objApTransactionFilter, $boolIsFromBulkVoidPayments );
	}

	public function fetchRuleStopsByGroupId( $intGroupId, $objDatabase ) {
		return \Psi\Eos\Entrata\CRuleStops::createService()->fetchRuleStopsByGroupIdByCid( $intGroupId, $this->getId(), $objDatabase );
	}

	public function fetchNonAdminRouleStopsByRouteId( $intRouteId, $objDatabase ) {
		return \Psi\Eos\Entrata\CRuleStops::createService()->fetchNonAdminRuleStopsByRouteIdByCid( $intRouteId, $this->getId(), $objDatabase );
	}

	public function fetchRuleStopsByCompanyUserId( $intCompanyUserId, $objDatabase ) {
		return \Psi\Eos\Entrata\CRuleStops::createService()->fetchActiveRuleStopsByCompanyUserIdByCid( $intCompanyUserId, $this->getId(), $objDatabase );
	}

	public function fetchSettlementDistributionsByArPaymentIds( $arrintArPaymentIds, $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CSettlementDistributions::createService()->fetchSettlementDistributionsByCidByArPaymentIds( $this->getId(), $arrintArPaymentIds, $objPaymentDatabase );
	}

	public function fetchPropertyNamesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchCustomPropertyNamesByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchApAllocationsByCreditApDetailIds( $arrintCreditApDetailIds, $objClientDatabase ) {
		return CApAllocations::fetchApAllocationsByCreditApDetailIdsByIsDeletedByCid( $arrintCreditApDetailIds, $boolIsDeleted = false, $this->getId(), $objClientDatabase );
	}

	public function fetchPaginatedPausedApBatches( $objPagination, $objDatabase, $objApTransactionsFilter = NULL ) {
		return \Psi\Eos\Entrata\CApBatches::createService()->fetchPaginatedPausedApBatchesByCid( $this->getId(), $objPagination, $objDatabase, $objApTransactionsFilter );
	}

	public function fetchPausedApBatchesCount( $objDatabase, $objApTransactionsFilter = NULL ) {
		return \Psi\Eos\Entrata\CApBatches::createService()->fetchPausedApBatchesCountByCid( $this->getId(), $objDatabase, $objApTransactionsFilter );
	}

	public function fetchApHeadersByApBatchId( $intApBatchId, $objDatabase, $strBatchMemo = NULL ) {
		return CApHeaders::fetchSimpleApHeadersByApBatchIdByCid( $intApBatchId, $this->getId(), $objDatabase, $strBatchMemo );
	}

	public function fetchlatestTaskNotes( $objDatabase ) {
		return \Psi\Eos\Admin\CTaskNotes::createService()->fetchlatestTaskNotesByCid( $this->getId(), $objDatabase );
	}

	public function fetchActiveCallAnalysisCategories( $objVoipDatabase ) {
		return CCallAnalysisCategories::fetchActiveCallAnalysisCategoriesByCid( $this->getId(), $objVoipDatabase );
	}

	public function fetchApHeadersByApBatchIds( $arrintApBatchIds, $objDatabase ) {
		return CApHeaders::fetchApHeadersByApBatchIdsByCid( $arrintApBatchIds, $this->getId(), $objDatabase );
	}

	public function fetchApBatchesByIds( $arrintApBatchIds, $objDatabase ) {
		return CApBatches::fetchApBatchesByIdsByCid( $arrintApBatchIds, $this->getId(), $objDatabase );
	}

	public function fetchPaginatedGlReconciliations( $objPagination, $objClientDatabase, $objGlReconciliationsFilter = NULL, $arrintBankAccountIds = NULL ) {
		return CGlReconciliations::fetchPaginatedGlReconciliationsByCid( $this->getId(), $objPagination, $objClientDatabase, $objGlReconciliationsFilter, $arrintBankAccountIds );
	}

	public function fetchTotalPaginatedGlReconciliations( $objClientDatabase, $objBankReconciliationsFilter = NULL, $arrintBankAccountIds = NULL ) {
		return CGlReconciliations::fetchTotalPaginatedGlReconciliationsByCid( $this->getId(), $objClientDatabase, $objBankReconciliationsFilter, $arrintBankAccountIds );
	}

	public function fetchArDepositsByGlReconciliationFilter( $objGlReconciliationFilter, $objDatabase ) {
		return CGlHeaders::fetchArDepositsByGlReconciliationFilterByCid( $objGlReconciliationFilter, $this->getId(), $objDatabase );
	}

	public function fetchApPaymentsByGlReconciliationFilter( $objGlReconciliationFilter, $objDatabase ) {
		return CGlHeaders::fetchApPaymentsByGlReconciliationFilterByCid( $objGlReconciliationFilter, $this->getId(), $objDatabase );
	}

	public function fetchJournalEntriesByGlReconciliationFilter( $objGlReconciliationsFilter, $objDatabase ) {
		return CGlHeaders::fetchJournalEntriesByGlReconciliationFilterByCid( $objGlReconciliationsFilter, $this->getId(), $objDatabase );
	}

	public function fetchAllArDepositsByGlReconciliationIdByGlReconciliationFilter( $intGlReconciliationId, $objGlReconciliationsFilter, $objDatabase, $intSystemBankRecId = NULL ) {
		return CGlHeaders::fetchAllArDepositsByGlReconciliationIdByGlReconciliationFilterByCid( $intGlReconciliationId, $objGlReconciliationsFilter, $this->getId(), $objDatabase, $intSystemBankRecId );
	}

	public function fetchAllApPaymentsByGlReconciliationIdByGlReconciliationFilter( $intGlReconciliationId, $objGlReconciliationsFilter, $objDatabase, $intSystemBankRecId = NULL ) {
		return CGlHeaders::fetchAllApPaymentsByGlReconciliationIdByGlReconciliationFilterByCid( $intGlReconciliationId, $objGlReconciliationsFilter, $this->getId(), $objDatabase, $intSystemBankRecId );
	}

	public function fetchAllJournalEntriesByGlReconciliationIdByGlReconciliationFilter( $intGlReconciliationId, $objGlReconciliationsFilter, $objDatabase, $intSystemBankRecId = NULL ) {
		return CGlHeaders::fetchAllJournalEntriesByGlReconciliationIdByGlReconciliationFilterByCid( $intGlReconciliationId, $objGlReconciliationsFilter, $this->getId(), $objDatabase, $intSystemBankRecId );
	}

	public function fetchGlReconciliationItemsByReconciliationId( $intGlReconciliationId, $objDatabase ) {
		return CGlReconciliationItems::fetchGlReconciliationItemsByReconciliationIdOrByReversingGlReconciliationIdByCid( $intGlReconciliationId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyLeadSources( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPropertyLeadSourcesByCid( $this->getId(), $objDatabase );
	}

	public function fetchPropertyLeadSourcesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchLeadSourcesByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase, true );
	}

	public function fetchPropertyLeadSourcesByLeadSourceId( $intLeadSourceId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPropertyLeadSourcesByLeadSourceIdByCid( $intLeadSourceId, $this->getId(), $objDatabase );
	}

	public function fetchApPayeePropertiesByApPayeeIds( $arrintApPayeeIds, $objClientDatabase ) {
		return CApPayeePropertyGroups::fetchApPayeePropertiesByApPayeeIdsByCid( $arrintApPayeeIds, $this->getId(), $objClientDatabase );
	}

	public function fetchApPayeeProperties( $objDatabase ) {
		return CApPayeePropertyGroups::fetchApPayeePropertiesByCid( $this->getId(), $objDatabase );
	}

	public function fetchApPayeeLocations( $objDatabase ) {
		return CApPayeeLocations::fetchSimpleApPayeeLocationsByCid( $this->getId(), $objDatabase );
	}

	public function fetchApPayeeLocationsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CApPayeeLocations::fetchApPayeeLocationsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertiesByPropertyIdsByPropertyGroupTypeByPropertyGroupName( $arrintPropertyIds, $strPropertyGroupType, $strPropertyGroupName, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}
		if( true == is_null( $strPropertyGroupName ) && 0 < strlen( $strPropertyGroupName ) ) {
			return NULL;
		}

		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByPropertyIdsByPropertyGroupTypeByPropertyGroupNameByCid( $arrintPropertyIds, $strPropertyGroupType, $strPropertyGroupName, $this->getId(), $objDatabase );
	}

	public function fetchPropertyGroupTypeById( $intPropertyGroupTypeId, $objDatabase ) {
		return CPropertyGroupTypes::createService()->fetchPropertyGroupTypeByIdByCid( $intPropertyGroupTypeId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyGroupById( $intPropertyGroupId, $objDatabase ) {
		return CPropertyGroups::createService()->fetchPropertyGroupByIdByCid( $intPropertyGroupId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyGroupsBySystemCode( $strSystemCode, $objDatabase ) {
		return CPropertyGroups::createService()->fetchPropertyGroupsBySystemCodeByCid( $strSystemCode, $this->getId(), $objDatabase );
	}

	public function fetchPropertyGroupAssociationsByPropertyId( $intPropertyId, $objDatabase ) {
		return CPropertyGroupAssociations::fetchPropertyGroupAssociationsByCidByPropertyId( $this->getId(), $intPropertyId, $objDatabase );
	}

	public function fetchPropertyGroupAssociationByPropertyGroupId( $intPropertyGroupId, $objDatabase ) {
		return CPropertyGroupAssociations::fetchPropertyGroupAssociationsByCidByPropertyGroupId( $this->getId(), $intPropertyGroupId, $objDatabase );
	}

	public function fetchGlAccountsByGlChartSystemCode( $strGlChartSystemCode, $objClientDatabase ) {
		return CGlAccounts::createService()->fetchGlAccountsByCidByGlChartSystemCode( $this->getId(), $strGlChartSystemCode, $objClientDatabase );
	}

	public function fetchPropertiesByPropertyPreferenceKey( $strPropertyPreferenceKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByPropertyPreferenceKeyByCid( $strPropertyPreferenceKey, $this->getId(), $objDatabase );
	}

	public function fetchPropertiesByPropertyPreferenceKeyByPropertyIds( $strPropertyPreferenceKey, $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByPropertyPreferenceKeyByPropertyIdsByCid( $strPropertyPreferenceKey, $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function insertOrUpdateSimpleContrats( $arrobjOldSimpleContracts, $intTestPropertiesCount, $arrintTestPropertyIds, $objDatabase ) {

		if( false == valArr( $arrobjOldSimpleContracts ) ) {
			return NULL;
		}

		$this->m_arrobjProperties     = $this->fetchActiveProperties( $objDatabase );
		$intPropertiesCount           = \Psi\Libraries\UtilFunctions\count( $this->m_arrobjProperties );
		$arrobjRekeyedSimpleContracts = [];
		$arrobjSimpleContrats         = [];

		$objNewSimpleContract = new CSimpleContract();
		$strExpireOnDateLimit = strtotime( '+30 day', strtotime( date( 'm/d/Y' ) ) );

		foreach( $arrobjOldSimpleContracts as $objOldSimpleContract ) {

			if( true == is_null( $objOldSimpleContract->getPropertyId() ) && ( false == is_null( $objOldSimpleContract->getExpireOn() ) && strtotime( $objOldSimpleContract->getExpireOn() ) < strtotime( date( 'm/d/Y' ) ) ) && 0 != $intTestPropertiesCount && $intPropertiesCount == $intTestPropertiesCount ) {
				$objOldSimpleContract->setExpireOn( NULL );
				$arrobjSimpleContrats['insertorupdate'][] = $objOldSimpleContract;
				continue;
			}

			if( true == is_null( $objOldSimpleContract->getPropertyId() ) && ( true == is_null( $objOldSimpleContract->getExpireOn() ) || strtotime( $objOldSimpleContract->getExpireOn() ) > strtotime( date( 'm/d/Y' ) ) ) && 0 != $intTestPropertiesCount && $intPropertiesCount == $intTestPropertiesCount ) {
				continue;
			}

			if( true == is_null( $objOldSimpleContract->getPropertyId() ) && ( true == is_null( $objOldSimpleContract->getExpireOn() ) || strtotime( $objOldSimpleContract->getExpireOn() ) > $strExpireOnDateLimit ) && 0 == $intTestPropertiesCount ) {
				$objOldSimpleContract->setExpireOn( date( 'm/d/Y', strtotime( '+30 day', strtotime( date( 'm/d/Y' ) ) ) ) );
				$arrobjSimpleContrats['insertorupdate'][] = $objOldSimpleContract;
				continue;
			}

			if( 0 != $intTestPropertiesCount && $intPropertiesCount == $intTestPropertiesCount && false == is_null( $objOldSimpleContract->getPropertyId() ) ) {
				$arrobjRekeyedSimpleContracts[$objOldSimpleContract->getPsProductId()][] = $objOldSimpleContract;
				continue;
			}

			if( 0 != $intTestPropertiesCount && true == is_null( $objOldSimpleContract->getPropertyId() ) ) {
				foreach( $this->m_arrobjProperties as $objProperty ) {
					$objCloneSimpleContract = clone  $objNewSimpleContract;
					$objCloneSimpleContract->setCid( $this->getId() );
					$objCloneSimpleContract->setPsProductId( $objOldSimpleContract->getPsProductId() );
					$objCloneSimpleContract->setPropertyId( $objProperty->getId() );
					$objCloneSimpleContract->setNotes( $objOldSimpleContract->getNotes() );
					if( true == is_null( getArrayElementByKey( $objProperty->getId(), $arrintTestPropertyIds ) ) ) {
						$strSetExpireOn = ( true == is_null( $objOldSimpleContract->getExpireOn() ) || strtotime( $objOldSimpleContract->getExpireOn() ) > $strExpireOnDateLimit ) ? date( 'm/d/Y', strtotime( '+30 day', strtotime( date( 'm/d/Y' ) ) ) ) : $objOldSimpleContract->getExpireOn();
					} else {
						$strSetExpireOn = ( true == is_null( $objOldSimpleContract->getExpireOn() ) || strtotime( $objOldSimpleContract->getExpireOn() ) < strtotime( date( 'm/d/Y' ) ) ) ? NULL : $objOldSimpleContract->getExpireOn();
					}

					$objCloneSimpleContract->setExpireOn( $strSetExpireOn );
					$arrobjSimpleContrats['insertorupdate'][] = $objCloneSimpleContract;
				}
				$arrobjSimpleContrats['delete'][] = $objOldSimpleContract;
				continue;
			}

			if( false == is_null( $objOldSimpleContract->getPropertyId() ) && ( true == is_null( $objOldSimpleContract->getExpireOn() ) || strtotime( $objOldSimpleContract->getExpireOn() ) > $strExpireOnDateLimit ) && true == is_null( getArrayElementByKey( $objOldSimpleContract->getPropertyId(), $arrintTestPropertyIds ) ) ) {
				$objOldSimpleContract->setExpireOn( date( 'm/d/Y', strtotime( '+30 day', strtotime( date( 'm/d/Y' ) ) ) ) );
				$arrobjSimpleContrats['insertorupdate'][] = $objOldSimpleContract;
				continue;
			}

			if( false == is_null( $objOldSimpleContract->getPropertyId() ) && true == valStr( $objOldSimpleContract->getExpireOn() ) && ( strtotime( $objOldSimpleContract->getExpireOn() ) < strtotime( date( 'm/d/Y' ) ) ) && false == is_null( getArrayElementByKey( $objOldSimpleContract->getPropertyId(), $arrintTestPropertyIds ) ) ) {
				$objOldSimpleContract->setExpireOn( NULL );
				$arrobjSimpleContrats['insertorupdate'][] = $objOldSimpleContract;
			}
		}

		if( true == valArr( $arrobjRekeyedSimpleContracts ) ) {
			foreach( $arrobjRekeyedSimpleContracts as $arrobjSimpleContract ) {
				foreach( $arrobjSimpleContract as $objOldSimpleContract ) {
					if( false == is_null( $objOldSimpleContract->getExpireOn() ) && strtotime( $objOldSimpleContract->getExpireOn() ) < strtotime( date( 'm/d/Y' ) ) ) {
						$objOldSimpleContract->setExpireOn( NULL );
						$arrobjSimpleContrats['insertorupdate'][] = $objOldSimpleContract;
					}
				}
			}
		}

		return $arrobjSimpleContrats;
	}

	public function rebuildDNS() {

		//  Include the necessary files

		//  Instantiate the object of Manage DNS Library
		$objManageDNSLibrary = new CManageDNSLibrary();

		return $objManageDNSLibrary->rebuildDNS( $this );
	}

	public function addDNSEntry() {

		//  Include the necessary files

		//  Instantiate the object of Manage DNS Library
		$objManageDNSLibrary = new CManageDNSLibrary();

		$boolAddDNSEntry = $objManageDNSLibrary->addDNSEntry( $this );

		if( false == $boolAddDNSEntry ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to add DNS Entry.' ) );
		}

		return $boolAddDNSEntry;
	}

	public function updateDNSEntry( $strOldDomain ) {

		//  Include the necessary files

		//  Instantiate the object of Manage DNS Library
		$objManageDNSLibrary = new CManageDNSLibrary();

		$objDevDnsDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::DNS, CDatabaseUserType::PS_PROPERTYMANAGER );
		$intRecordCount    = CRecords::fetchRecordCount( ' WHERE name ILIKE \'' . addslashes( $strOldDomain . CONFIG_PROPERTYSOLUTIONS_DOMAIN_POSTFIX ) . '\'', $objDevDnsDatabase->createDataset() );

		if( 0 == $intRecordCount ) {
			$boolUpdateDNSEntry = $objManageDNSLibrary->addDNSEntry( $this );
		} else {
			$boolUpdateDNSEntry = $objManageDNSLibrary->updateDNSEntry( $this, $strOldDomain, $objDevDnsDatabase );
		}

		$objDevDnsDatabase->close();

		if( false == $boolUpdateDNSEntry ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update DNS Entry.' ) );
		}

		return $boolUpdateDNSEntry;
	}

	public function fetchCompanyApplicationsByPropertyIds( $arrintPropertyIds, $objDatabase, $boolOrderByTitle = false ) {
		return CCompanyApplications::fetchCompanyApplicationsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase, $boolOrderByTitle );
	}

	public function fetchManualApPaymentByBankAccountIdByCheckNumber( $intBankAccountId, $intCheckNumber, $objDatabase ) {
		return CApPayments::fetchManualApPaymentByBankAccountIdByCheckNumberByCid( $intBankAccountId, $intCheckNumber, $this->getId(), $objDatabase );
	}

	public function fetchArTransactionsByRemotePrimaryKeys( $arrstrRemotePrimaryKeys, $objDatabase ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionsByRemotePrimaryKeysByCid( $arrstrRemotePrimaryKeys, $this->getId(), $objDatabase );
	}

	public function fetchContractProductsWithProductNameByContractIds( $arrintContractIds, $objAdminDatabase, $arrintContractProductIds = [], $boolShowBundleOrNoIntentToUse = false ) {
		return \Psi\Eos\Admin\CContractProducts::createService()->fetchContractProductsWithProductNameByContractIds( $arrintContractIds, $this->getId(), $objAdminDatabase, $arrintContractProductIds, $boolShowBundleOrNoIntentToUse );
	}

	public function fetchAllContractPropertiesByContractIds( $arrintContractIds, $objAdminDatabase, $boolShowBundleOrNoIntentToUse = false ) {
		return CContractProperties::createService()->fetchAllContractPropertiesByContractIdsByCid( $arrintContractIds, $this->getId(), $objAdminDatabase, $boolShowBundleOrNoIntentToUse );
	}

	public function fetchPropertyLeadSourcesPhoneNumbersByPropertyIdsByLeadSourceId( $arrintPropertyIds, $intLeadSourceId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPropertyLeadSourcesPhoneNumbersByPropertyIdsByLeadSourceIdByCid( $arrintPropertyIds, $intLeadSourceId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyOfficeEmailAddressesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CPropertyEmailAddresses::fetchPropertyEmailAddressesByPropertyIdsByEmailAddressTypeIdByCid( $arrintPropertyIds, CEmailAddressType::PRIMARY, $this->getId(), $objDatabase );
	}

	public function fetchPropertyPreferencesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CPropertyPreferences::fetchPropertyPreferencesByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyPreferencesByKeyByPropertyIds( $strKey, $arrintPropertyIds, $objDatabase ) {
		return CPropertyPreferences::fetchPropertyPreferencesByKeyByPropertyIdsByCid( $strKey, $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyPrimaryAddressByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyAddresses::createService()->fetchPublishedPropertyAddressesByPropertyIdsAddressTypeIdByCid( $arrintPropertyIds, CAddressType::PRIMARY, $this->getId(), $objDatabase );
	}

	public function fetchPropertyOfficePhoneNumbers( $arrintPropertyIds, $objDatabase ) {
		return CPropertyPhoneNumbers::fetchPublishedPropertyPhoneNumberByPropertyIdsByPhoneNumberTypeIdByCid( $arrintPropertyIds, CPhoneNumberType::OFFICE, $this->getId(), $objDatabase );
	}

	public function fetchPropertyWebVisiblePhoneNumbersByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CPropertyPhoneNumbers::fetchWebVisiblePropertyPhoneNumbersByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchActiveContractPropertiesByPsProductIds( $arrintPsProductIds, $objDatabase ) {
		return CContractProperties::createService()->fetchActiveContractPropertiesByPsProductIdsByCid( $arrintPsProductIds, $this->getId(), $objDatabase );
	}

	public function fetchContractsByIds( $arrintContractIds, $objDatabase ) {
		return CContracts::createService()->fetchContractsByCidByContractIds( $this->getId(), $arrintContractIds, $objDatabase );
	}

	public function fetchSimplePropertyIdsByCompanyUserIdBySessionDataForPayment( $intCompanyUserId, $intIsDisabledSessionData, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchSimplePropertyIdsByCompanyUserIdBySessionDataForPaymentByCid( $intCompanyUserId, $intIsDisabledSessionData, $this->getId(), $objDatabase );
	}

	public function fetchPropertyEventsByCompanyEventIds( $arrintCompanyEventIds, $objDatabase ) {
		return CPropertyEvents::fetchPropertyEventsByCidByCompanyEventIds( $arrintCompanyEventIds, $this->getId(), $objDatabase );
	}

	public function fetchCustomerEventsByCompanyEventIds( $arrintCompanyEventIds, $objDatabase ) {
		return CCustomerEvents::fetchCustomerEventsByCidByCompanyEventIds( $arrintCompanyEventIds, $this->getId(), $objDatabase );
	}

	public function fetchCompanyMediaFoldersByPropertyIdsByFolderName( $intCompanyMediaFolderId, $objDatabase, $strSearchFolderName, $intLoadedFolderCount ) {
		return CCompanyMediaFolders::createService()->fetchCompanyMediaFoldersByPropertyIdsByCompanyMediaFolderNameByCid( $intCompanyMediaFolderId, $this->getId(), $objDatabase, $strSearchFolderName, $intLoadedFolderCount );
	}

	public function fetchCompanyMediaFoldersByPropertyId( $intCompanyMediaFolderId, $objDatabase, $intPropertyId ) {
		return CCompanyMediaFolders::createService()->fetchCustomCompanyMediaFoldersByPropertyIdByCid( $intCompanyMediaFolderId, $intPropertyId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyMediaFolderCountByPropertyIdsByFolderId( $arrintPropertyIds, $intCompanyMediaFolderId, $objDatabase ) {
		return CCompanyMediaFolders::createService()->fetchCompanyMediaFolderCountByPropertyIdsByCompanyMediaFolderIdByCid( $arrintPropertyIds, $intCompanyMediaFolderId, $this->getId(), $objDatabase );
	}

	public function fetchPsProducts( $objClientDatabase = NULL ) {

		if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
			$objClientDatabase = $this->loadDatabase( CDatabaseUserType::PS_PROPERTYMANAGER );
		}

		$arrintPsProductIds = $this->fetchPsProductIds( $objClientDatabase );
		return  CPsProduct::loadPsProductsByIds( $arrintPsProductIds );
	}

	public function fetchPersonsByPersonTypeIds( $arrintPersonTypeIds, $objDatabase ) {
		return CPersons::createService()->fetchPersonsByCidByPersonTypeIds( $this->getId(), $arrintPersonTypeIds, $objDatabase );
	}

	public function fetchTotalCountCustomerInspections( $arrmixInspectionFilter, $objDatabase, $boolIsEntrataMobile = false, $boolIsCheckMaintenanceGroups = false, $boolIsUnitProfile = false, $boolShowDisabledData = false ) {
		return CInspections::createService()->fetchTotalInspectionsCountByInspectionFilterByCid( $this->getId(), $arrmixInspectionFilter, $objDatabase, $boolIsEntrataMobile, $boolIsCheckMaintenanceGroups, $boolIsUnitProfile, $boolShowDisabledData );
	}

	public function fetchCompanyInspections( $objPagination, $objInspectionFilter, $objDatabase, $boolIsEntrataMobile = false ) {
		return CInspections::createService()->fetchPaginatedInspectionsByCidByUnitId( $objPagination, $this->getId(), $objInspectionFilter, $objDatabase, $boolIsEntrataMobile );
	}

	public function fetchPropertiesByPropertyIdsWithTypesWithPmSoftware( $arrintPropertyIds, $strPropertyNameSearch = NULL, $objDatabase, $boolReturnArray = false, $boolPropertySortbyPmSoftware = false, $arrintIntegrationClientTypeIds = [] ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesWithTypeWithPmSoftwareByCid( $arrintPropertyIds, $this->getId(), $strPropertyNameSearch, $objDatabase, $boolReturnArray, $boolPropertySortbyPmSoftware, $arrintIntegrationClientTypeIds );
	}

	public function fetchTotalInspectionFormCountByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CInspectionForms::createService()->fetchTotalInspectionFormCountByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchInspectionFormByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CInspectionForms::createService()->fetchInspectionFormByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchCompanyMediaFilesByIdsByPropertyBuildingIds( $arrintBuildingMediaFileIds, $arrintPropertyBuildingIds, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByIdsByPropertyBuildingIdsByCid( $arrintBuildingMediaFileIds, $arrintPropertyBuildingIds, $this->getId(), $objDatabase );
	}

	public function fetchCompanyMediaFilesByPropertyUnitIds( $arrintPropertyUnitIds, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByPropertyUnitIdsByCid( $arrintPropertyUnitIds, $this->getId(), $objDatabase );
	}

	public function fetchSimplePropertiesByIds( $arrintPropertyIds, $objDatabase, $objPropertyFilter = NULL ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchSimplePropertiesByIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase, $objPropertyFilter );
	}

	public function fetchPropertyGroupNamesByIds( $arrintPropertyGroupIds, $objDatabase ) {
		return CPropertyGroups::createService()->fetchPropertyGroupNamesByIdsByCid( $arrintPropertyGroupIds, $this->getId(), $objDatabase );
	}

	public function fetchSimpleProperties( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchSimplePropertiesByPropertyIdsByCidByDisabledData( NULL, $this->getId(), $objDatabase, true, true, false );
	}

	public function fetchPropertyGroupNames( $objDatabase, $arrintPsProductIds ) {
		return CPropertyGroups::createService()->fetchPropertyGroupNamesByCid( $this->getId(), $objDatabase, $arrintPsProductIds = [] );
	}

	public function fetchCompanyMediaFoldersDetailByPropertyIdsByCompanyMediaFolderId( $arrintPropertyIds, $intCompanyMediaFolderId, $objDatabase, $intLimit = NULL, $intOffset = 0 ) {
		return CCompanyMediaFolders::createService()->fetchCompanyMediaFoldersDetailByCidByPropertyIdsByCompanyMediaFolderId( $this->getId(), $arrintPropertyIds, $intCompanyMediaFolderId, $objDatabase, $intLimit, $intOffset );
	}

	public function fetchPaginatedApExportBatches( $objPagination, $objDatabase, $objApExportBatchesFilter = NULL ) {
		return CApExportBatches::fetchPaginatedApExportBatchesByCid( $this->getId(), $objPagination, $objDatabase, $objApExportBatchesFilter );
	}

	public function fetchApExportBatchesCount( $objDatabase, $objApExportBatchesFilter = NULL ) {
		return CApExportBatches::fetchApExportBatchesCountByCid( $this->getId(), $objDatabase, $objApExportBatchesFilter );
	}

	public function fetchScheduledApExportBatchesCount( $objDatabase, $objApExportBatchesFilter = NULL, $arrintApExportBatchTypeIds = NULL ) {
		return CScheduledApExportBatches::fetchScheduledApExportBatchesCountByCid( $this->getId(), $objDatabase, $objApExportBatchesFilter, $arrintApExportBatchTypeIds );
	}

	public function fetchPaginatedScheduledApExportBatches( $objPagination, $objDatabase, $objApExportBatchesFilter = NULL, $arrintApExportBatchTypeIds = NULL ) {
		return CScheduledApExportBatches::fetchPaginatedScheduledApExportBatchesByCid( $this->getId(), $objPagination, $objDatabase, $objApExportBatchesFilter, $arrintApExportBatchTypeIds );
	}

	public function fetchPaginatedGlHeaderTemplatesByPropertyIds( $arrintPropertyIds, $objPagination, $objDatabase, $objGlHeaderSchedulesFilter = NULL ) {
		return CGlHeaders::fetchPaginatedGlHeaderTemplatesByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objPagination, $objDatabase, $objGlHeaderSchedulesFilter );
	}

	public function fetchGlHeaderTemplatesCountByPropertyIds( $arrintPropertyIds, $objDatabase, $objGlHeaderSchedulesFilter = NULL ) {
		return CGlHeaders::fetchGlHeaderTemplatesCountByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase, $objGlHeaderSchedulesFilter );
	}

	public function fetchCompanyGroupAssessmentsByCompanyGroupId( $intCompanyGroupId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyGroupAssessments::createService()->fetchCompanyGroupAssessmentsByCompanyGroupIdByCid( $intCompanyGroupId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyGroupPreferencesByCompanyGroupId( $intCompanyGroupId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyGroupPreferences::createService()->fetchCompanyGroupPreferencesByCompanyGroupIdByCid( $intCompanyGroupId, $this->getId(), $objDatabase );
	}

	public function fetchContractPropertiesForAppStoreByContractId( $intContractId, $objDatabase, $strInstalledDateTime = NULL ) {
		return CContractProperties::createService()->fetchContractPropertiesForAppStoreByContractIdByCid( $intContractId, $this->getId(), $objDatabase, $strInstalledDateTime );
	}

	public function fetchApHeaderLogsByApHeaderId( $intApHeaderId, $objDatabase, $objApHeaderLogsFilter = NULL ) {
		return \Psi\Eos\Entrata\CApHeaderLogs::createService()->fetchSimpleApHeaderLogsByApHeaderIdByCid( $intApHeaderId, $this->getId(), $objDatabase, $objApHeaderLogsFilter );
	}

	public function fetchChildPropertyIdsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchChildPropertyIdsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPeriodsCountByPropertyIds( $arrintPropertyIds, $objDatabase, $objPeriodClosingsFilter = NULL ) {
		return CPeriods::fetchPeriodsCountByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase, $objPeriodClosingsFilter );
	}

	public function fetchPaginatedPropertiesAndCountByPropertyIds( $objDatabase, $objPagination = NULL, $boolCountOnly = false, $strPropertyIds = '' ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchAllPaginatedPropertiesAndCountByPropertyIdsAndCid( $this->getId(), $objDatabase, $objPagination, $boolCountOnly, $strPropertyIds );
	}

	public function fetchContractPropertyProducts( $objAdminDatabase, $objPagination = NULL, $strPropertyIds = '', $boolIncludeDisabledProperty = false ) {
		return CContractProperties::createService()->fetchContractPropertyProductsByCid( $this->getId(), $objAdminDatabase, $objPagination, $strPropertyIds, $boolIncludeDisabledProperty );
	}

	public function fetchFirstPrimaryPerson( $objDatabase ) {
		return CPersons::createService()->fetchFirstPrimaryPersonByCid( $this->getId(), $objDatabase );
	}

	public function fetchFirstOwnerPerson( $objDatabase ) {
		return CPersons::createService()->fetchFirstPersonByPersonTypeIdByCid( CPersonContactType::OWNER, $this->getId(), $objDatabase );
	}

	public function fetchFirstImplementationPerson( $objDatabase ) {
		return CPersons::createService()->fetchFirstPersonByPersonTypeIdByCid( CPersonContactType::IMPLEMENTATION, $this->getId(), $objDatabase );
	}

	public function fetchFirstBillingPerson( $objDatabase ) {
		return CPersons::createService()->fetchFirstPersonByPersonTypeIdByCid( CPersonContactType::ACCOUNTING, $this->getId(), $objDatabase );
	}

	public function fetchCustomMaintenancePrioritiesWithPropertyRemotePrimaryKeyAndPsProductId( $objDatabase ) {
		return CMaintenancePriorities::createService()->fetchCustomMaintenancePrioritiesWithPropertyRemotePrimaryKeyAndPsProductIdByCid( $this->getId(), $objDatabase );
	}

	public function fetchCustomMaintenanceStatusesWithPropertyRemotePrimaryKeyAndPsProductId( $objDatabase, $boolIsPublished = false ) {
		return CMaintenanceStatuses::createService()->fetchSimpleMaintenanceStatusesWithPropertyRemotePrimaryKeyAndPsProductIdByCid( $this->getId(), $objDatabase, $boolIsPublished );
	}

	public function fetchAccountByAccountId( $intAccountId, $objDatabase ) {
		return CAccounts::createService()->fetchAccountByCidByAccountId( $this->getId(), $intAccountId, $objDatabase );
	}

	public function fetchAccountPropertiesByAccountIds( $arrintAccountIds, $objAdminDatabase ) {
		return CAccountProperties::createService()->fetchAccountPropertiesByAccountIdsByCid( $arrintAccountIds, $this->getId(), $objAdminDatabase );
	}

	public function fetchAccountPropertiesByAccountTypeIdsByPropertyIds( $arrintAccountTypeIds, $arrintPropertyIds, $objAdminDatabase, $boolExcludeMAAccounts = false, $intPaymentTypeId = NULL ) {
		return CAccountProperties::createService()->fetchAccountPropertiesByAccountTypeIdsByPropertyIdsByCid( $arrintAccountTypeIds, $arrintPropertyIds, $this->getId(), $objAdminDatabase, $boolExcludeMAAccounts, $intPaymentTypeId );
	}

	public function fetchActiveRoommateInterests( $objDatabase ) {
		return CRoommateInterests::fetchActiveRoommateInterestsByCid( $this->getId(), $objDatabase );
	}

	public function fetchRoommateInterestById( $intRoommateInterestId, $objDatabase ) {
		return CRoommateInterests::fetchRoommateInterestByIdByCid( $intRoommateInterestId, $this->getId(), $objDatabase );
	}

	public function fetchRoommateInterestsByIds( $arrintRoommateInterestIds, $objDatabase ) {
		return CRoommateInterests::fetchRoommateInterestsByIdsByCid( $arrintRoommateInterestIds, $this->getId(), $objDatabase );
	}

	public function fetchPMEnabledProperties( $objDatabase, $strKey = NULL, $arrintIds = NULL, $intOccupancyTypeId = NULL, $boolIntegratedPropeties = false ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPMEnabledPropertiesByCid( $this->getId(), $objDatabase, $strKey, $arrintIds, $intOccupancyTypeId, $boolIntegratedPropeties );
	}

	public function fetchPMEnabledPropertiesCountByOccupancyTypeIdByPropertyPreferenceKeyByIds( $intOccupancyTypeId, $strPropertyPreferenceKey, $arrintIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPMEnabledPropertiesCountByOccupancyTypeIdByPropertyPreferenceKeyByIds( $intOccupancyTypeId, $strPropertyPreferenceKey, $arrintIds, $this->getId(), $objDatabase );
	}

	public function fetchUnitSpacesByBulkUnitAssignmentFilterByLeaseIntervalIds( $objBulkUnitAssignmentFilter, $arrintLeaseIntervalIds, $objDatabase, $objPagination = NULL, $boolIsFromUnitSpaceAvailablePopup = false, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitsFloorPlans = false, $boolShowOnWebsite = true ) {
		return CUnitSpaces::createService()->fetchUnitSpacesByBulkUnitAssignmentFilterByLeaseIntervalIdsByCid( $objBulkUnitAssignmentFilter, $arrintLeaseIntervalIds, $this->getId(), $objDatabase, $objPagination, $boolIsFromUnitSpaceAvailablePopup, $boolIncludeDeletedUnits, $boolIncludeDeletedUnitsFloorPlans, $boolShowOnWebsite );
	}

	public function fetchRoommateInterestCategories( $objDatabase ) {
		return CRoommateInterestCategories::fetchRoommateInterestCategoriesByCid( $this->getId(), $objDatabase );
	}

	public function fetchUnitSpacesCountForPropertyFloorplansByPropertyId( $intPropertyId, $objDatabase, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitsFloorPlans = false, $boolShowOnWebsite = true ) {
		return CUnitSpaces::createService()->fetchUnitSpacesCountForPropertyFloorplansByPropertyIdByCid( $intPropertyId, $this->getId(), $objDatabase, $boolIncludeDeletedUnits, $boolIncludeDeletedUnitsFloorPlans, $boolShowOnWebsite );
	}

	public function fetchGlDetailsByApAllocationIds( $arrintApAllocationIds, $objDatabase ) {
		return CGlDetails::fetchGlDetailsByApAllocationIdsByCid( $arrintApAllocationIds, $this->getId(), $objDatabase );
	}

	public function fetchTableLogsByPropertyIdByTableName( $intPropertyId, $strTableName, $objDatabase ) {
		return CTableLogs::fetchTableLogsByCidByPropertyIdByTableName( $this->getId(), $intPropertyId, $strTableName, $objDatabase );
	}

	public function fetchClAdsCountByDaysByPropertyIds( $arrintPropertyIds, $intNumberOfDays, $objDatabase ) {
		return \Psi\Eos\Entrata\CClAds::createService()->fetchClAdsCountByDaysByPropertyIdsByCid( $arrintPropertyIds, $intNumberOfDays, $this->getId(), $objDatabase );

	}

	public function fetchPaginatedParentLeadSourcesCount( $objDatabase, $strLeadSourceName, $intPropertyId, $boolIsHideInactiveLeadSources = false ) {
		return \Psi\Eos\Entrata\CLeadSources::createService()->fetchPaginatedParentLeadSourcesCountByCid( $this->getId(), $objDatabase, $strLeadSourceName, $intPropertyId, $boolIsHideInactiveLeadSources );
	}

	public function fetchDisassociatedLeadSourcesByCid( $objDatabase, $strChildLeadSourceSearchCriteria ) {
		return \Psi\Eos\Entrata\CLeadSources::createService()->fetchDisassociatedLeadSourcesByCid( $this->getId(), $objDatabase, $strChildLeadSourceSearchCriteria );
	}

	public function fetchLeadSourcesByCid( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeadSources::createService()->fetchLeadSourcesByCid( $this->getId(), $objDatabase );
	}

	public function fetchAllGlAccountsByGlAccountUsageTypeIds( $arrintGlAccountUsageTypeIds, $objDatabase ) {
		return CGlAccounts::createService()->fetchAllGlAccountsByGlAccountUsageTypeIdsByCid( $arrintGlAccountUsageTypeIds, $this->getId(), $objDatabase );
	}

	public function fetchManagerialProperty( $objDatabase, $boolIsMultipleProperties = false ) {
		if( false == $boolIsMultipleProperties ) {
			return \Psi\Eos\Entrata\CProperties::createService()->fetchManagerialPropertyByCid( $this->getId(), $objDatabase );
		}
		return \Psi\Eos\Entrata\CProperties::createService()->fetchManagerialPropertiesByCid( $this->getId(), $objDatabase );
	}

	public function fetchPropertyUnitsByPropertyIdsSortByUnitNumber( $arrintPropertyIds, $objDatabase ) {
		return CPropertyUnits::createService()->fetchPropertyUnitsByPropertyIdsByCidSortByUnitNumber( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchCurrentYearCompanyHolidaysByCid( $objDatabase ) {
		return CCompanyHolidays::fetchCurrentYearCompanyHolidaysByCid( $this->getId(), $objDatabase );
	}

	public function fetchCustomPropertiesByIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchCustomPropertiesByIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPsProductsByBundlePsProductId( $intPsBundleId, $intPsleadId, $objDatabase ) {
		return CPsProducts::createService()->fetchPsProductsByBundlePsProductIdByCid( $this->getId(), $intPsBundleId, $intPsleadId, $objDatabase );
	}

	public function fetchContractPropertiesWithContractDetails( $objDatabase ) {
		return CContractProperties::createService()->fetchContractPropertiesWithContractDetailsByCid( $this->getId(), $objDatabase );
	}

	public function fetchMerchantAccountApplicationsCountByCid( $objDatabase ) {
		return \Psi\Eos\Payment\CMerchantAccountApplications::createService()->fetchMerchantAccountApplicationsCountByCid( $this->getId(), $objDatabase );
	}

	public function fetchPaginatedMerchantAccountApplications( $objPagination, $objDatabase ) {
		return \Psi\Eos\Payment\CMerchantAccountApplications::createService()->fetchPaginatedMerchantAccountApplicationsByCid( $this->getId(), $objPagination, $objDatabase );
	}

	public function updateSelfImplementedPsLeadProductsOrProperties( $objDatabase ) {
	return valArr( CContractProperties::createService()->updateContractPropertiesForSelfImplementedProductsOrProperties( [ $this->getId() ], $objDatabase ), 0 );
	}

	public function fetchInspectionFilterById( $intInspectionFilterId, $objDatabase ) {
		return CInspectionFilters::fetchInspectionFilterByIdByCid( $intInspectionFilterId, $this->getId(), $objDatabase );
	}

	public function fetchScheduledTasksByScheduledTaskTypeId( $intScheduledTaskTypeId, $objDatabase ) {
		return CScheduledTasks::createService()->fetchScheduledTasksByScheduledTaskTypeIdByCid( $intScheduledTaskTypeId, $this->getId(), $objDatabase );
	}

	public function fetchApplicationFilterById( $intApplicationFilterId, $objDatabase ) {
		return \Psi\Eos\Entrata\CApplicationFilters::createService()->fetchApplicationFilterByIdByCid( $intApplicationFilterId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyUnitMaxOccupantsByPropertyId( $intPropertyId, $objDatabase, $intUnitSpaceId, $intPropertyUnitId ) {
		return CPropertyUnit::fetchPropertyUnitMaxOccupantsByPropertyIdByCid( $intPropertyId, $this->getId(), $objDatabase, $intUnitSpaceId, $intPropertyUnitId );
	}

	public function fetchUnitTypeByIdByPropertyId( $intUnitTypeId, $intPropertyId, $objDatabase ) {
		return CUnitTypes::createService()->fetchUnitTypeByIdByPropertyIdByCid( $intUnitTypeId, $intPropertyId, $this->getId(), $objDatabase );
	}

	public function fetchLatestApplicationByCustomerId( $intCustomerId, $objDatabase ) {
		return CApplications::fetchLatestApplicationByCustomerIdByCid( $intCustomerId, $this->getId(), $objDatabase );
	}

	public function fetchMinimumCountForMaximumOccupantsByPropertyFloorPlanId( $intFloorPlanId, $objDatabase ) {
		return CUnitTypes::createService()->fetchMinimumCountForMaximumOccupantsByPropertyFloorPlanIdByCid( $intFloorPlanId, $this->getId(), $objDatabase );
	}

	public function fetchApplicationsByLeaseId( $intLeaseId, $objDatabase ) {
		return CApplications::fetchApplicationsByLeaseIdByCid( $intLeaseId, $this->getId(), $objDatabase );
	}

	public function fetchAppsByIds( $arrintAppIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CApps::createService()->fetchAppsByIdsByCid( $arrintAppIds, $this->getId(), $objDatabase );
	}

	public function fetchCompanyAppsByIds( $arrintCompanyAppIds, $objDatabase ) {
		return CCompanyApps::fetchCompanyAppsByIdsByCid( $arrintCompanyAppIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyIntegrationDatabasesByServiceId( $intIntegrationServiceId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyIntegrationDatabases::createService()->fetchPropertyIntegrationDatabasesByServiceIdByCid( $intIntegrationServiceId, $this->getId(), $objDatabase );
	}

	public function fetchInspectionFormsByPropertyIds( $arrintPropertyIds, $objDatabase, $boolReturnArray = false ) {
		return CInspectionForms::createService()->fetchInspectionFormsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase, $boolReturnArray );
	}

	public function fetchEnabledInspectionFormsByPropertyIds( $arrintPropertyIds, $objDatabase, $boolReturnArray = false, $boolIsUnitTypeInspection ) {
		return CInspectionForms::createService()->fetchEnabledInspectionFormsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase, $boolReturnArray, $boolIsUnitTypeInspection );
	}

	public function fetchInspectionFormIdsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CInspectionForms::createService()->fetchInspectionFormIdsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchScheduledTaskById( $intScheduledTaskId, $objDatabase ) {
		return CScheduledTasks::createService()->fetchScheduledTaskByIdByCid( $intScheduledTaskId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyPreferenceValuesByPropertyIdsByKeys( $arrintPropertyIds, $arrstrKeys, $objDatabase ) {
		return CPropertyPreferences::fetchPropertyPreferenceValuesByPropertyIdsByKeysByCid( $arrintPropertyIds, $arrstrKeys, $this->getId(), $objDatabase );
	}

	public function fetchPublishedPropertyMaintenanceStatusesByPropertyIds( $arrintPropertyIds, $objClientDatabase ) {
		return \Psi\Eos\Entrata\CPropertyMaintenanceStatuses::createService()->fetchPublishedPropertyMaintenanceStatusesByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objClientDatabase );
	}

	public function fetchRentArCode( $objDatabase ) {
		$objGlSetting = $this->fetchGlSetting( $objDatabase );

		return \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByIdByCid( $objGlSetting->getRentArCodeId(), $this->getId(), $objDatabase );
	}

	public function fetchDepositArCode( $objDatabase ) {
		$objGlSetting = $this->fetchGlSetting( $objDatabase );

		return \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByIdByCid( $objGlSetting->getDepositArCodeId(), $this->getId(), $objDatabase );
	}

	public function fetchScheduledTasksByInspectionFormId( $intInspectionFormId, $objDatabase, $strOrderBy = NULL, $strOrder = NULL, $boolIsActive = false, $arrintPropertyIds = NULL ) {
		return CScheduledTasks::createService()->fetchScheduledTasksByInspectionFormIdByCid( $intInspectionFormId, $this->getId(), $objDatabase, $strOrderBy, $strOrder, $boolIsActive, $arrintPropertyIds );
	}

	public function fetchInspectionFormActionDataById( $intInspectionFormId, $objDatabase ) {
		return CInspectionForms::createService()->fetchInspectionFormActionDataByIdByCid( $intInspectionFormId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyPreferencesByPropertyIdsByKey( $arrintPropertyIds, $strKey, $objDatabase, $boolIsCheckValue = true, $boolIsGetCountOnly = false ) {
		return CPropertyPreferences::fetchPropertyPreferencesByPropertyIdsByKeyByCid( $arrintPropertyIds, $strKey, $this->getId(), $objDatabase, $boolIsCheckValue, $boolIsGetCountOnly );
	}

	public function fetchPropertyMaintenanceLocationWithName( $intPropertyMaintenanceLocationId, $objDatabase ) {
		return CPropertyMaintenanceLocations::createService()->fetchPropertyMaintenanceLocationWithNameByIdByCid( $intPropertyMaintenanceLocationId, $this->getId(), $objDatabase );
	}

	public function fetchInspection( $intInspectionId, $objDatabase ) {
		return CInspections::createService()->fetchInspectionByIdByCid( $intInspectionId, $this->getId(), $objDatabase );
	}

	public function fetchInspectionFormLocationProblems( $arrintIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CInspectionFormLocationProblems::createService()->fetchInspectionFormLocationProblemsByIdsByCid( $arrintIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyUnitByUnitSpaceId( $intUnitSpaceId, $objDatabase ) {
		return CPropertyUnits::createService()->fetchPropertyUnitByUnitSpaceIdByCid( $intUnitSpaceId, $this->getId(), $objDatabase );
	}

	public function fetchSubMaintenanceRequestsByMaintenanceRequestId( $intMaintenanceRequestId, $objDatabase, $boolFetchOpenSubtasks = false ) {
		return CMaintenanceRequests::createService()->fetchSubMaintenanceRequestsByMaintenanceRequestIdByCid( $intMaintenanceRequestId, $this->getId(), $objDatabase, $boolFetchOpenSubtasks );
	}

	public function fetchAllCompanyUsers( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchAllCompanyUsersByCid( $this->getId(), $objDatabase );
	}

	public function fetchCustomMaintenanceRequestByIds( $arrintIds, $objDatabase, $boolIncludeDeletedUnits = false, $boolOpenSubTaskCount = false ) {
		return CMaintenanceRequests::createService()->fetchCustomMaintenanceRequestByIdsByCid( $arrintIds, $this->getId(), $objDatabase, $boolIncludeDeletedUnits, $boolOpenSubTaskCount );
	}

	public function fetchLeaseProcessByLeaseId( $intLeaseId, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchLeaseProcessByLeaseIdByCid( $intLeaseId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyUnitById( $intPropertyUnitId, $objDatabase ) {
		return CPropertyUnits::createService()->fetchPropertyUnitByIdByCid( $intPropertyUnitId, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceStatusesByMaintenanceStatusTypeId( $intMaintenanceStatusTypeId, $objDatabase ) {
		return CMaintenanceStatuses::createService()->fetchMaintenanceStatusesByMaintenanceStatusTypeIdByCid( $intMaintenanceStatusTypeId, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceStatusesByPropertyIdsByMaintenanceStatusTypeIds( $arrintPropertyIds, $arrintMaintenanceStatusTypeIds, $objDatabase ) {
		return CMaintenanceStatuses::createService()->fetchPublishedMaintenanceStatusesByPropertyIdsByMaintenanceStatusTypeIdsByCid( $arrintPropertyIds, $arrintMaintenanceStatusTypeIds, $this->getId(), $objDatabase );
	}

	public function fetchCustomPropertyPreferencesByKeysByPropertyId( $arrstrPropertyPreferencesKeys, $intPropertyId, $objDatabase ) {
		return CPropertyPreferences::fetchCustomPropertyPreferencesByKeysByPropertyIdByCid( $arrstrPropertyPreferencesKeys, $intPropertyId, $this->getId(), $objDatabase );
	}

	public function fetchSimplePropertiesInfoByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchSimplePropertiesInfoByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceProblemsByMaintenanceProblemTypeIdsByMaintenanceProblemFilter( $arrintMaintenanceProblemTypes, $objMaintenanceProblemsFilter, $objDatabase, $strProblemType = CMaintenanceProblem::INCLUDE_IN_BOTH, $boolFetchData = false, $boolIsSortByCategory = false, $intPropertyId = NULL ) {
		return CMaintenanceProblems::createService()->fetchMaintenanceProblemsByCidByMaintenanceProblemTypeIdsByMaintenanceProblemFilter( $this->getId(), $arrintMaintenanceProblemTypes, $objMaintenanceProblemsFilter, $objDatabase, $strProblemType, $boolFetchData, $boolIsSortByCategory, $intPropertyId );
	}

	public function fetchIntegratedMaintenanceProblemsByMaintenanceProblemTypeIdsByMaintenanceProblemFilter( $arrintMaintenanceProblemTypes, $objMaintenanceProblemsFilter, $objDatabase, $strProblemType = CMaintenanceProblem::INCLUDE_IN_BOTH, $boolFetchData = false, $boolIsSortByCategory = false ) {
		return CMaintenanceProblems::createService()->fetchIntegratedMaintenanceProblemsByCidByMaintenanceProblemTypeIdsByMaintenanceProblemFilter( $this->getId(), $arrintMaintenanceProblemTypes, $objMaintenanceProblemsFilter, $objDatabase, $strProblemType, $boolFetchData, $boolIsSortByCategory );
	}

	public function fetchMaintenanceRequestLaborById( $intLaborId, $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceRequestLabors::createService()->fetchMaintenanceRequestLaborByIdByCid( $intLaborId, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceRequestMileageById( $intMileageId, $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceRequestMileages::createService()->fetchMaintenanceRequestMileageByIdByCid( $intMileageId, $this->getId(), $objDatabase );
	}

	public function fetchCustomMaintenanceRequestMileageCountByMaintenanceRequestId( $intMaintenanceRequestId, $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceRequestMileages::createService()->fetchCustomMaintenanceRequestMileageCountByMaintenanceRequestIdByCid( $intMaintenanceRequestId, $this->getId(), $objDatabase );
	}

	public function fetchUnitSpaceCountByUnitIds( $arrintPropertyUnitIds, $objDatabase ) {
		return CUnitSpaces::createService()->fetchUnitSpaceCountByUnitIdsByCid( $arrintPropertyUnitIds, $this->getId(), $objDatabase );
	}

	public function fetchTimeZoneNameByPropertyId( $intPropertyId, $objDatabase ) {
		return CTimeZones::fetchTimeZoneNameByPropertyIdByCid( $intPropertyId, $this->getId(), $objDatabase );
	}

	public function fetchActiveIntegrationDatabasesCount( $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationDatabases::createService()->fetchActiveIntegrationDatabasesCountByCid( $this->getId(), $objDatabase );
	}

	public function fetchLeaseCustomerById( $intLeaseCustomerId, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomerByIdByCid( $intLeaseCustomerId, $this->getId(), $objDatabase );
	}

	public function fetchInspectionById( $intInspectionId, $objDatabase ) {
		return CInspections::createService()->fetchInspectionByIdByCid( $intInspectionId, $this->getId(), $objDatabase );
	}

	public function fetchLeaseCustomerByCustomerIdByLeaseId( $intCustomerId, $intLeaseId, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomerByCustomerIdByLeaseIdByCid( $intCustomerId, $intLeaseId, $this->getId(), $objDatabase );
	}

	public function fetchInspectionFormById( $intInspectionFormId, $objDatabase ) {
		return CInspectionForms::createService()->fetchInspectionFormByIdByCid( $intInspectionFormId, $this->getId(), $objDatabase );
	}

	public function fetchUnitSpaceNumberByLeaseId( $intLeaseId, $objDatabase ) {
		return CUnitSpaces::createService()->fetchUnitSpaceNumberByLeaseIdByCid( $intLeaseId, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceProblemsByParentMaintenanceProblemIdsByPropertyId( $arrintMaintenanceProblemIds, $intPropertyId, $objDatabase, $arrintMaintenanceLocationIds = NULL ) {
		return CPropertyMaintenanceLocationProblems::createService()->fetchMaintenanceProblemsByParentMaintenanceProblemIdsByPropertyIdByCid( $arrintMaintenanceProblemIds, $intPropertyId, $this->getId(), $objDatabase, $arrintMaintenanceLocationIds );
	}

	public function fetchInspectionFormLocationProblemsByInspectionFormIdByPropertyIdByMaintenanceLocationIds( $intInspectionFormId, $intPropertyId, $arrintInspectionLocations, $objDatabase, $boolIsIncludePropertyDisabledLocationsProblems = false, $boolFetchData = false ) {
		return \Psi\Eos\Entrata\CInspectionFormLocationProblems::createService()->fetchInspectionFormLocationProblemsByInspectionFormIdByPropertyIdByMaintenanceLocationIdsByCid( $intInspectionFormId, $intPropertyId, $arrintInspectionLocations, $this->getId(), $objDatabase, $boolIsIncludePropertyDisabledLocationsProblems, $boolFetchData );
	}

	public function fetchInspectionFormLocationProblemsByInspectionFormIdByPropertyIdByInspectionFormLocationIds( $intInspectionFormId, $intPropertyId, $arrintInspectionLocationIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CInspectionFormLocationProblems::createService()->fetchInspectionFormLocationProblemsByInspectionFormIdByPropertyIdByInspectionFormLocationIdsByCid( $intInspectionFormId, $intPropertyId, $arrintInspectionLocationIds, $this->getId(), $objDatabase );
	}

	public function fetchInspectionNotesByInspectionId( $intInspectionId, $objDatabase ) {
		return CInspectionNotes::createService()->fetchInspectionNotesByInspectionIdByCid( $intInspectionId, $this->getId(), $objDatabase );
	}

	public function fetchPreviousInspectionsByPropertyUnitId( $intPropertyUnitId, $objDatabase ) {
		return CInspections::createService()->fetchPreviousInspectionsByPropertyUnitIdByCid( $intPropertyUnitId, $this->getId(), $objDatabase );
	}

	public function fetchInspectionResponsesByInspectionId( $intInspectionId, $objDatabase ) {
		return \Psi\Eos\Entrata\CInspectionResponses::createService()->fetchInspectionResponsesByInspectionIdByCid( $intInspectionId, $this->getId(), $objDatabase );
	}

	public function fetchSimpleFileById( $intInspectionFileId, $objDatabase ) {
		return CFiles::createService()->fetchFileByIdByCid( $intInspectionFileId, $this->getId(), $objDatabase );
	}

	public function fetchFilesByInspectionIdByInspectionFormLocationProblemId( $intInspectionId, $intInspectionFormLocationProblemId, $objDatabase ) {
		return CFiles::createService()->fetchFilesByInspectionIdByInspectionFormLocationProblemIdByCid( $intInspectionId, $intInspectionFormLocationProblemId, $this->getId(), $objDatabase );
	}

	public function fetchFilesByInspectionId( $intInspectionId, $objDatabase ) {
		return CFiles::createService()->fetchFilesByInspectionIdByCid( $intInspectionId, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceRequestPrintReportByMaintenanceRequestIds( $arrintMaintenanceRequestIds, $objDatabase, $boolShowDisabledData = false ) {
		return CMaintenanceRequests::createService()->fetchMaintenanceRequestPrintReportByMaintenanceRequestIdsByCid( $arrintMaintenanceRequestIds, $this->getId(), $objDatabase, $boolShowDisabledData );
	}

	public function fetchPropertyMaintenanceTemplatesByMaintenanceTemplateIds( $arrintSubMaintenanceTemplateIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyMaintenanceTemplates::createService()->fetchPropertyMaintenanceTemplatesByMaintenanceTemplateIdsByCid( $arrintSubMaintenanceTemplateIds, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceRequestsByMaintenanceTemplateIds( $arrintSubMaintenanceTemplateIds, $objDatabase ) {
		return CMaintenanceRequests::createService()->fetchMaintenanceRequestsByMaintenanceTemplateIdsByCid( $arrintSubMaintenanceTemplateIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyMaintenanceProblemsByMaintenanceProblemIdsByPropertyIds( $arrintMaintenanceTemplateProblemIds, $arrintMaintenanceTemplateAssociatedPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyMaintenanceProblems::createService()->fetchPropertyMaintenanceProblemsByMaintenanceProblemIdsByPropertyIdsByCid( $arrintMaintenanceTemplateProblemIds, $arrintMaintenanceTemplateAssociatedPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyMaintenancePrioritiesByMaintenancePriorityIdsByPropertyIds( $arrintMaintenanceTemplatePriorityIds, $arrintMaintenanceTemplateAssociatedPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyMaintenancePriorities::createService()->fetchPropertyMaintenancePrioritiesByMaintenancePriorityIdsByPropertyIdsByCid( $arrintMaintenanceTemplatePriorityIds, $arrintMaintenanceTemplateAssociatedPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyMaintenanceLocationProblemsByMaintenanceProblemIdsByMaintenanceLocationIdsByPropertyIds( $arrintMaintenanceProblemIds, $arrintMaintenanceLocationIds, $arrintPropertyIds, $objDatabase ) {
		return CPropertyMaintenanceLocationProblems::createService()->fetchPropertyMaintenanceLocationProblemsByMaintenanceProblemIdsByMaintenanceLocationIdsByPropertyIdsByCid( $arrintMaintenanceProblemIds, $arrintMaintenanceLocationIds, $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchAllParentMaintenanceTemplatesByRequestTypeIdByPropertyId( $intRequestTypeId, $intPropertyId, $objDatabase ) {
		return CMaintenanceTemplates::createService()->fetchAllParentMaintenanceTemplatesByRequestTypeIdByPropertyIdByCid( $intRequestTypeId, $intPropertyId, $this->getId(), $objDatabase );
	}

	public function fetchScheduledTaskByTemplateId( $intMaintenanceTemplateId, $objDatabase ) {
		return CScheduledTasks::createService()->fetchScheduledTaskByTemplateIdByCid( $intMaintenanceTemplateId, $this->getId(), $objDatabase );
	}

	public function fetchCountMaintenanceProblemCategories( $objDatabase ) {
		return CMaintenanceProblems::createService()->fetchCountMaintenanceProblemCategoriesByCid( $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceTemplatesByIds( $arrintSubTemplateIds, $objDatabase ) {
		return CMaintenanceTemplates::createService()->fetchMaintenanceTemplatesByCidByIds( $this->getId(), $arrintSubTemplateIds, $objDatabase );
	}

	public function fetchPropertyMaintenanceLocationsByMaintenanceLocationIdsByPropertyIds( $arrintMaintenanceTemplateLocationIds, $arrintMaintenanceTemplateAssociatedPropertyIds, $objDatabase ) {
		return CPropertyMaintenanceLocations::createService()->fetchPropertyMaintenanceLocationsByMaintenanceLocationIdsByPropertyIdsByCid( $arrintMaintenanceTemplateLocationIds, $arrintMaintenanceTemplateAssociatedPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchArCodesByIdsPropertyIds( $arrintArCodeIds, $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodesByIdsPropertyIdsByCid( $arrintArCodeIds, $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchArCodeTypeIdByArCodeId( $intArCodeTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeTypeIdByArCodeIdByCid( $intArCodeTypeId, $this->getId(), $objDatabase );
	}

	public function fetchPaginatedInspectionsByLeaseIdByInspectionFilter( $intLeaseId, $objInspectionFilter, $objPagination, $objDatabase, $intPropertyUnitId = NULL, $intCustomerId = NULL ) {
		return CInspections::createService()->fetchPaginatedInspectionsByLeaseIdByInspectionFilterByCid( $intLeaseId, $objInspectionFilter, $objPagination, $this->getId(), $objDatabase, $intPropertyUnitId, $intCustomerId );
	}

	public function fetchInspectionsCountByLeaseId( $intLeaseId, $objDatabase, $intPropertyUnitId = NULL, $intCustomerId = NULL ) {
		return CInspections::createService()->fetchInspectionsCountByLeaseIdByCid( $intLeaseId, $this->getId(), $objDatabase, $intPropertyUnitId, $intCustomerId );
	}

	public function fetchSimpleCompanyAppByAppId( $intAppId, $objDatabase ) {
		return CCompanyApps::fetchSimpleCompanyAppByAppIdByCid( $intAppId, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceAvailabilityById( $intMaintenanceAvailabilityId, $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceAvailabilities::createService()->fetchMaintenanceAvailabilityByIdByCid( $intMaintenanceAvailabilityId, $this->getId(), $objDatabase );

	}

	public function fetchMaintenanceAvailabilityExceptionById( $intMaintenanceAvailabilityExceptionId, $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceAvailabilityExceptions::createService()->fetchMaintenanceAvailabilityExceptionByIdByCid( $intMaintenanceAvailabilityExceptionId, $this->getId(), $objDatabase );
	}

	public function fetchKeyedParcelAlertPropertyNotificationsByPropertyId( $intPropertyId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyNotifications::createService()->fetchPropertyNotificationsByPropertyIdByNotificationKeysByCid( $intPropertyId, [ 'PARCEL_ALERT_SMS_TEXT', 'PARCEL_ALERT_EMAIL_TEXT', 'PARCEL_ALERT_EMAIL_PICKED_UP_TEXT', 'PARCEL_ALERT_EMAIL_DELIVERED_TEXT' ], $this->getId(), $objDatabase );
	}

	public function fetchSimpleLeaseCustomersByIds( $arrintLeaseCustomerIds, $objDatabase ) {
		// @TODO Test it thoroughly in second phase refactoring
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchSimpleLeaseCustomersByIdsByCid( $arrintLeaseCustomerIds, $this->getId(), $objDatabase );
	}

	public function fetchNonLobbyTypeWebsitesForSitetabletByPropertyId( $arrintPropertyIds, $objDatabase ) {
		return CWebsites::createService()->fetchNonLobbyTypeWebsitesForSitetabletByPropertyIdByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchEventsByDataReferenceIdsByEventTypeId( $arrintDataReferenceIds, $intEventTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchEventsByDataReferenceIdsByEventTypeIdByCid( $arrintDataReferenceIds, $intEventTypeId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyMaintenanceLocationProblemsByIds( $arrintMaintenanceLocationProblemIds, $objDatabase, $boolReturnArray = false ) {
		return CPropertyMaintenanceLocationProblems::createService()->fetchPropertyMaintenanceLocationProblemActionsByIdsByCid( $arrintMaintenanceLocationProblemIds, $this->getId(), $objDatabase, $boolReturnArray );
	}

	public function fetchApplicantsByCustomerIds( $arrintCustomerIds, $objDatabase ) {
		return CApplicants::fetchApplicantsByCustomerIdsByCid( $arrintCustomerIds, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceRequest( $intMaintenanceRequestId, $objDatabase ) {
		return CMaintenanceRequests::createService()->fetchMaintenanceRequestByIdByCid( $intMaintenanceRequestId, $this->getId(), $objDatabase );
	}

	public function fetchScheduledChargeById( $intScheduledChargeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargeByIdByCid( $intScheduledChargeId, $this->getId(), $objDatabase );
	}

	public function fetchScheduledChargesBySubMaintenanceRequestIds( $arrintSubMaintenanceRequestIds, $objDatabase, $boolReturnArray = false ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesBySubMaintenanceRequestIdsByCid( $arrintSubMaintenanceRequestIds, $this->getId(), $objDatabase, $boolReturnArray );
	}

	public function fetchMaintenanceRequestNotesByMaintenanceRequestId( $intMaintenanceRequestId, $objResidentPortalDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceRequestNotes::createService()->fetchMaintenanceRequestNotesByCidByMaintenanceRequestId( $this->getId(), $intMaintenanceRequestId, $objResidentPortalDatabase );
	}

	public function fetchInspectionResponseById( $intInspectionResponseId, $objDatabase ) {
		return \Psi\Eos\Entrata\CInspectionResponses::createService()->fetchInspectionResponseByIdByCid( $intInspectionResponseId, $this->getId(), $objDatabase );
	}

	public function fetchInspectionResponsesByIds( $arrintResponseIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CInspectionResponses::createService()->fetchInspectionResponsesByIdsByCid( $arrintResponseIds, $this->getId(), $objDatabase );
	}

	public function fetchInspectionNotesByInspectionResponseIds( $arrintInspectionResponseIds, $objDatabase ) {
		return CInspectionNotes::createService()->fetchInspectionNotesByInspectionResponseIdsByCid( $arrintInspectionResponseIds, $this->getId(), $objDatabase );
	}

	public function fetchInspectionNotesByInspectionIdByInspectionFormLocationProblemId( $intInspectionId, $intInspectionFormLocationProblemId, $objDatabase ) {
		return CInspectionNotes::createService()->fetchInspectionNotesByInspectionIdByInspectionFormLocationProblemIdByCid( $intInspectionId, $intInspectionFormLocationProblemId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyMaintenanceLocationActionsByPropertyIdByMaintenanceLocationIdByMaintenanceProblemId( $intPropertyId, $intMaintenanceProblemId, $objDatabase ) {
		return CPropertyMaintenanceLocationProblems::createService()->fetchPropertyMaintenanceLocationActionsByCidByPropertyIdByMaintenanceProblemId( $this->getId(), $intPropertyId, $intMaintenanceProblemId, $objDatabase );
	}

	public function fetchInspectionByIdWithInspectedByCompanyEmployee( $intInspectionId, $objDatabase ) {
		return CInspections::createService()->fetchInspectionByIdWithInspectedByCompanyEmployee( $intInspectionId, $this->getId(), $objDatabase );
	}

	public function fetchActiveContractsByPsProductIdByPropertyId( $arrintPsProductIds, $intPropertyId, $objDatabase ) {
		return CContractProperties::createService()->fetchActiveContractsByCidByPsProductIdsByPropertyId( $this->getId(), $arrintPsProductIds, $intPropertyId, $objDatabase );
	}

	public function fetchSimpleContractsByPsProductIdsByPropertyId( $arrintProductIds, $intPropertyId, $objDatabase ) {
		return CSimpleContracts::createService()->fetchSimpleContractsByCidByPsProductIdsByPropertyId( $this->getId(), $arrintProductIds, $intPropertyId, $objDatabase );
	}

	public function fetchPaginatedPackageBatchesByPackageFilter( $intPageNo, $intCountPerPage, $intLeaseCustomerId, $objPackageFilter, $objDatabase ) {
		return CPackageBatches::fetchPaginatedPackageBatchesByCidByPackageFilter( $intPageNo, $intCountPerPage, $this->getId(), $intLeaseCustomerId, $objPackageFilter, $objDatabase );
	}

	public function fetchPaginatedPackagesByCidByDateByPackageFilter( $intLeaseCustomerId, $objDatabase, $intSelectionLimit, $arrintPropertyIds = [] ) {
		return CPackages::fetchPaginatedPackagesByCidByDateByPackageFilter( $this->getId(), $intLeaseCustomerId, $objDatabase, $intSelectionLimit, $arrintPropertyIds );
	}

	public function fetchPackagesByCidByDateByPackageStatusByPackageBatchDates( $arrobjPackageBatches, $intLeaseCustomerId, $objDatabase, $arrintPropertyIds = [] ) {
		return CPackages::fetchPackagesByCidByDateByPackageStatusByPackageBatchDates( $this->getId(), $arrobjPackageBatches, $intLeaseCustomerId, $objDatabase, $arrintPropertyIds );
	}

	public function fetchIncompletePackageCountByPackageBatchDateByLeaseIdByCustomerIdByCid( $strPackageDate, $intLeaseCustomerId, $objDatabase, $boolTotalCount = false, $arrintPropertyIds = [] ) {
		return CPackages::fetchIncompletePackageCountByPackageBatchDateByLeaseIdByCustomerIdByCid( $this->getId(), $strPackageDate, $intLeaseCustomerId, $objDatabase, $boolTotalCount, $arrintPropertyIds );
	}

	public function fetchPackageBatchesCountsByPackageFilter( $intLeaseCustomerId, $objPackageFilter, $objDatabase ) {
		return CPackageBatches::fetchPackageBatchesCountsByCidByPackageFilter( $this->getId(), $intLeaseCustomerId, $objPackageFilter, $objDatabase );
	}

	public function fetchPaginatedPackagesByPackageFilter( $intPageNo, $intCountPerPage, $objPackageFilter, $arrintPropertyIds, $objDatabase ) {
		return CPackages::fetchPaginatedPackagesByCidByPackageFilter( $intPageNo, $intCountPerPage, $objPackageFilter, $this->getId(), $arrintPropertyIds, $objDatabase );
	}

	public function fetchPackagesCountByPackageFilter( $objPackageFilter, $arrintPropertyIds, $objDatabase ) {
		return CPackages::fetchPackagesCountByCidByPackageFilter( $objPackageFilter, $this->getId(), $arrintPropertyIds, $objDatabase );
	}

	public function fetchPackageBatchById( $intPackageBatchId, $objDatabase ) {
		return CPackageBatches::fetchPackageBatchByIdByCid( $intPackageBatchId, $this->getId(), $objDatabase );
	}

	public function fetchPackagesNotMailedNotificationByBatchId( $intPackageBatchId, $objDatabase ) {
		return CPackages::fetchPackagesNotMailedNotificationByBatchIdByCid( $intPackageBatchId, $this->getId(), $objDatabase );
	}

	public function fetchCustomerPackagesByPackageIds( $arrintPackageIds, $objDatabase ) {
		return CPackages::fetchCustomerPackagesByPackageIdsByCid( $arrintPackageIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyPreferencesCountByPropertyIdsByKeys( $arrintPropertyIds, $arrstrKeys, $objDatabase, $boolIsCheckValue = true ) {
		return CPropertyPreferences::fetchPropertyPreferencesCountByPropertyIdsByKeysByCid( $arrintPropertyIds, $arrstrKeys, $this->getId(), $objDatabase, $boolIsCheckValue );
	}

	public function fetchPackagesWithCustomerByPackageBatchIdByPackageFilter( $intPageNo, $intCountPerPage, $intPackageBatchDate, $intShowPickedUp, $objPackageFilter, $objDatabase, $boolIsFromSearch = false, $arrintPackageStatusTypeIds = [] ) {
		return CPackages::fetchPackagesWithCustomerByPackageBatchIdByPackageFilter( $intPageNo, $intCountPerPage, $intPackageBatchDate, $intShowPickedUp, $this->getId(), $objPackageFilter, $objDatabase, $boolIsFromSearch, $arrintPackageStatusTypeIds );
	}

	public function fetchPackagesByPackageBatchIdByPackageFilter( $intPackageBatchId, $objPackageFilter, $objDatabase ) {
		return CPackages::fetchPackagesByPackageBatchIdByPackageFilter( $intPackageBatchId, $this->getId(), $objPackageFilter, $objDatabase );
	}

	public function fetchUndeliveredPackagesWithCustomerByCustomerId( $intCustomerId, $intPackageStatusType, $arrintPropertyIds, $objDatabase ) {
		return CPackages::fetchUndeliveredPackagesWithCustomerByCustomerId( $intCustomerId, $this->getId(), $intPackageStatusType, $arrintPropertyIds, $objDatabase );
	}

	public function fetchPackagesByIds( $arrintPackageIds, $objDatabase ) {
		return CPackages::fetchPackagesByIdsByCid( $arrintPackageIds, $this->getId(), $objDatabase );
	}

	public function fetchCustomPackageById( $intPackageId, $objDatabase ) {
		return CPackages::fetchCustomPackageByIdByCid( $intPackageId, $this->getId(), $objDatabase );
	}

	public function fetchUndeliveredPackages( $intUndeliveredPackagesPageNo, $intPageSize, $intStatusTypePendingId, $objPackageFilter, $objDatabase, $intShowPickedUp = NULL ) {
		return CPackages::fetchUndeliveredPackagesByCid( $intUndeliveredPackagesPageNo, $intPageSize, $this->getId(), $intStatusTypePendingId, $objPackageFilter, $objDatabase, $intShowPickedUp );
	}

	public function fetchTotalUndeliveredPackagesCount( $intStatusTypePendingId, $arrintPropertyIds, $objDatabase, $intShowPickedUp = NULL ) {
		return CPackages::fetchTotalUndeliveredPackagesCountByCid( $this->getId(), $intStatusTypePendingId, $arrintPropertyIds, $objDatabase, $intShowPickedUp );
	}

	public function fetchPropertyPreferencesByKeyByPropertyId( $strKey, $intPropertyId, $objDatabase, $boolReturnCountOnly = false ) {
		return \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( $strKey, $intPropertyId, $this->getId(), $objDatabase, $boolReturnCountOnly );
	}

	public function fetchPackageBatchPropertiesByPackageBatchId( $intPackageBatchId, $objDatabase ) {
		return CPackageBatches::fetchPackageBatchPropertiesByCidByPackageBatchId( $this->getId(), $intPackageBatchId, $objDatabase );
	}

	public function fetchPackageBatchPropertiesByPackageBatchDate( $intPackageBatchDate, $objDatabase ) {
		return CPackages::fetchPackageBatchPropertiesByCidByPackageBatchDate( $this->getId(), $intPackageBatchDate, $objDatabase );
	}

	public function fetchPackagesWithCustomerByPackageBatchId( $intPackageBatchId, $objPackageFilter, $objDatabase ) {
		return CPackages::fetchPackagesWithCustomerByPackageBatchId( $intPackageBatchId, $this->getId(), $objPackageFilter, $objDatabase );
	}

	public function fetchCustomMaintenanceRequestLaborCountByMaintenanceRequestId( $intMaintenanceRequestId, $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceRequestLabors::createService()->fetchCustomMaintenanceRequestLaborCountByMaintenanceRequestIdByCid( $intMaintenanceRequestId, $this->getId(), $objDatabase );
	}

	public function fetchCustomMaintenanceRequestMaterialCountByMaintenanceRequestId( $intMaintenanceRequestId, $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceRequestMaterials::createService()->fetchCustomMaintenanceRequestMaterialCountByMaintenanceRequestIdByCid( $intMaintenanceRequestId, $this->getId(), $objDatabase );
	}

	public function fetchEntryNotesFromEventsByLeaseIdsByCustomerId( $intLeaseIds, $intCustomerId, $objDatabase ) {
		return CMaintenanceRequests::createService()->fetchEntryNotesFromEventsByLeaseIdsByCustomerIdByCid( $intLeaseIds, $intCustomerId, $this->getId(), $objDatabase );
	}

	public function fetchApCodesByPropertyId( $intPropertyId, $objDatabase, $boolIsTrackInventoryQuantity = false ) {
		return \Psi\Eos\Entrata\CApCodes::createService()->fetchApCodesByPropertyIdByCid( $this->getId(), $intPropertyId, $objDatabase, $boolIsTrackInventoryQuantity );
	}

	public function fetchMaintenanceRequestsByScheduledDate( $strTime, $strDate, $objDatabase ) {
		return CMaintenanceRequests::createService()->fetchMaintenanceRequestsByScheduledDateByCid( $strTime, $strDate, $this->getId(), $objDatabase );
	}

	public function refreshCachedAccountsBalances( $objAdminDatabase ) {
		return CClients::refreshCachedAccountsBalancesByCid( $this->getId(), $objAdminDatabase );
	}

	public function fetchFileAssociationsByInspectionResponseIds( $arrintInspectionResponseIds, $boolIsReviewInspection, $objDatabase ) {
		return CFileAssociations::createService()->fetchFileAssociationsByInspectionResponseIdsByCid( $arrintInspectionResponseIds, $this->getId(), $boolIsReviewInspection, $objDatabase );
	}

	public function fetchFileAssociationsAttachmentsByInspectionResponseIds( $arrintInspectionResponseIds, $objDatabase ) {
		return CFileAssociations::createService()->fetchFileAssociationsAttachmentsByInspectionResponseIdsByCid( $arrintInspectionResponseIds, $this->getId(), $objDatabase );
	}

	public function fetchSimpleFilesByIds( $arrintIds, $objDatabase ) {
		return CFiles::createService()->fetchSimpleFilesByIdsByCid( $arrintIds, $this->getId(), $objDatabase );
	}

	public function fetchSimpleInspectionResponsesByInspectionId( $intInspectionId, $objDatabase ) {
		return \Psi\Eos\Entrata\CInspectionResponses::createService()->fetchSimpleInspectionResponsesByInspectionIdByCid( $intInspectionId, $this->getId(), $objDatabase );
	}

	public function fetchFileAssociationsByIds( $arrintIds, $objDatabase ) {
		return CFileAssociations::createService()->fetchFileAssociationsByIdsByCid( $arrintIds, $this->getId(), $objDatabase );
	}

	public function fetchFileAssociationCountByFileIds( $arrintIds, $objDatabase ) {
		return CFileAssociations::createService()->fetchFileAssociationCountByFileIdsByCid( $arrintIds, $this->getId(), $objDatabase );
	}

	public function fetchFileAssociationCountByInspectionResponseIds( $arrintInspectionResponseIds, $objDatabase ) {
		return CFileAssociations::createService()->fetchFileAssociationCountByInspectionResponseIdsByCid( $arrintInspectionResponseIds, $this->getId(), $objDatabase );
	}

	public function fetchTotalMaintenanceRequestsCountByMaintenanceProblemIdByPropertyIdByUnitId( $intMaintenanceProblemId, $intPropertyId, $intUnitSpaceId, $intPropertyUnitId, $intDaysBackForSimilarProblem, $objDatabase ) {
		return CMaintenanceRequests::createService()->fetchTotalMaintenanceRequestsCountByMaintenanceProblemIdByPropertyIdByUnitIdByCid( $intMaintenanceProblemId, $intPropertyId, $intUnitSpaceId, $intPropertyUnitId, $intDaysBackForSimilarProblem, $this->getId(), $objDatabase );
	}

	public function fetchTotalMaintenanceRequestsCountByMaintenanceProblemIdByPropertyId( $intMaintenanceProblemId, $intPropertyId, $intDaysBackForSimilarProblem, $objDatabase ) {
		return CMaintenanceRequests::createService()->fetchTotalMaintenanceRequestsCountByMaintenanceProblemIdByPropertyIdByCid( $intMaintenanceProblemId, $intPropertyId, $intDaysBackForSimilarProblem, $this->getId(), $objDatabase );
	}

	public function fetchSubTaskCountByMaintenanceRequestIds( $arrintMaintenanceRequestIds, $objDatabase, $boolIsCheckClosedSubTasks = true ) {
		return CMaintenanceRequests::createService()->fetchSubTaskCountByMaintenanceRequestIdsByCid( $arrintMaintenanceRequestIds, $this->getId(), $objDatabase, $boolIsCheckClosedSubTasks );
	}

	public function fetchCustomMaintenanceRequestsLaborCountByMaintenanceRequestIds( $arrintMaintenanceRequestIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceRequestLabors::createService()->fetchCustomMaintenanceRequestsLaborCountByMaintenanceRequestIdsByCid( $arrintMaintenanceRequestIds, $this->getId(), $objDatabase );
	}

	public function fetchSimplePropertiesWithStatesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchSimplePropertiesWithStatesByCidByPropertyIds( $this->getId(), $arrintPropertyIds, $objDatabase );
	}

	public function fetchMaintenanceLocationsByIds( $arrintMaintenanceLocationIds, $objDatabase ) {
		return CMaintenanceLocations::createService()->fetchMaintenanceLocationsByCidByIds( $arrintMaintenanceLocationIds, $this->getId(), $objDatabase );
	}

	public function fetchCustomPropertyMaintenanceLocationsByPropertyId( $intPropertyId, $objDatabase ) {
		return CPropertyMaintenanceLocations::createService()->fetchCustomPropertyMaintenanceLocationsByPropertyIdByCid( $intPropertyId, $this->getId(), $objDatabase );
	}

	public function fetchPublishedMaintenanceProblemsByMaintenanceProblemTypeIds( $arrintMaintenanceProblemTypes, $objDatabase, $boolFetchIntegrated = false ) {
		return CMaintenanceProblems::createService()->fetchPublishedMaintenanceProblemsByMaintenanceProblemTypeIdsByCid( $arrintMaintenanceProblemTypes, $this->getId(), $objDatabase, $boolFetchIntegrated );
	}

	public function fetchCustomPropertyMaintenanceProblemsForPropertySettingsTemplatesByPropertyIds( $arrintPropertyIds, $objDatabase, $boolIsReturnObject = true ) {
		return \Psi\Eos\Entrata\CPropertyMaintenanceProblems::createService()->fetchCustomPropertyMaintenanceProblemsForPropertySettingsTemplatesByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase, $boolIsReturnObject );
	}

	public function fetchCustomPublishedMaintenanceLocations( $objDatabase ) {
		return CMaintenanceLocations::createService()->fetchCustomPublishedMaintenanceLocationsByCid( $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceRequestsDetailsByMaintenanceRequestIdsByPropertyIds( $arrintMaintenanceRequestIds, $arrintPropertyIds, $objDatabase ) {
		return CMaintenanceRequests::createService()->fetchMaintenanceRequestsDetailsByMaintenanceRequestIdsByPropertyIdsByCIds( $arrintMaintenanceRequestIds, $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyMaintenanceLocationProblemsByMaintenanceLocationIdByPropertyIds( $intMaintenanceLocationId, $arrintPropertyIds, $objDatabase ) {
		return CPropertyMaintenanceLocationProblems::createService()->fetchPropertyMaintenanceLocationProblemsByMaintenanceLocationIdByPropertyIdsByCid( $intMaintenanceLocationId, $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyPreferencesByPropertyIdsByKeys( $arrintPropertyIds, $arrstrKeys, $objDatabase, $boolIsCheckValue, $boolIsCheckZero ) {
		return CPropertyPreferences::fetchPropertyPreferencesByPropertyIdsByKeysByCid( $arrintPropertyIds, $arrstrKeys, $this->getId(), $objDatabase, $boolIsCheckValue, $boolIsCheckZero );
	}

	public function fetchCompanyVehicles( $objDatabase ) {
		return CVehicles::fetchCompanyVehiclesByCid( $this->getId(), $objDatabase );
	}

	public function fetchPrivateVehicle( $objDatabase ) {
		return CVehicles::fetchPrivateVehicleByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyVehicleById( $intVehicleId, $objDatabase ) {
		return CVehicles::fetchVehicleByIdByCid( $intVehicleId, $this->getId(), $objDatabase );
	}

	public function fetchCompanyUserPreferenceByCompanyUserIdsByKey( $arrintCompanyUserIds, $strKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserPreferences::createService()->fetchCompanyUserPreferenceByCompanyUserIdsByKeyByCid( $arrintCompanyUserIds, $strKey, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceRequestMaterialCountByMaintenanceRequestIds( $arrintMaintenanceRequestIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceRequestMaterials::createService()->fetchMaintenanceRequestInventoryMaterialsCountByMaintenanceRequestIdsByCid( $arrintMaintenanceRequestIds, $this->getId(), $objDatabase );
	}

	public function fetchCustomCompanyUsersByCompanyUserIds( $arrintCompanyUserIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCustomCompanyUsersByCompanyUserIdsByCid( $arrintCompanyUserIds, $this->getId(), $objDatabase );
	}

	public function fetchCustomPropertyIntegrationDatabasesByIntegrationClientTypeIds( $arrintIntegrationClientTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyIntegrationDatabases::createService()->fetchCustomPropertyIntegrationDatabasesByIntegrationClientTypeIdsByCid( $arrintIntegrationClientTypeIds, $this->getId(), $objDatabase );
	}

	public function fetchParentMaintenanceTemplatesByPropertyIds( $arrintPropertyIds, $objClientDatabase ) {
		return CMaintenanceTemplates::createService()->fetchParentMaintenanceTemplatesByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objClientDatabase );
	}

	public function fetchPropertyOfficeEmailAddressesByPropertyIdsByEmailAddressTypeIds( $arrintPropertyIds, $arrintEmailAddressTypeIds, $objDatabase ) {
		return CPropertyEmailAddresses::fetchPropertyEmailAddressesByPropertyIdsByEmailAddressTypeIdsByCid( $arrintPropertyIds, $arrintEmailAddressTypeIds, $this->getId(), $objDatabase );
	}

	public function fetchPaginatedInspectionsByInspectionFilter( $objInspectionFilter, $objDatabase, $objPagination, $boolIsEntrataMobile = false, $intInspectionsBulkGroupsTotalCount = NULL, $intInspectionsBulkGroupsCountOnPage = NULL, $boolIsAppliedFilter = false, $boolIsUnitProfile = false, $boolShowDisabledData = false ) {
		return CInspections::createService()->fetchPaginatedInspectionsByInspectionFilterByCid( $objInspectionFilter, $this->getId(), $objDatabase, $objPagination, $boolIsEntrataMobile, $intInspectionsBulkGroupsTotalCount, $intInspectionsBulkGroupsCountOnPage, $boolIsAppliedFilter, $boolIsUnitProfile, $boolShowDisabledData );
	}

	public function fetchInspectionsByMaintenanceGroupIdById( $intMaintenanceGroupId, $objDatabase, $intInspectionId = NULL ) {
		return CInspections::createService()->fetchInspectionsByMaintenanceGroupIdByIdByCid( $intMaintenanceGroupId, $this->getId(), $objDatabase, $intInspectionId );
	}

	public function fetchInspectionsBulkGroups( $objDatabase, $boolIsFetchParentBulkGroup = true, $intParentBulkGroupId = NULL, $arrintChildBulkGroupIds = NULL, $arrintPropertyGroupIds = NULL, $intOffset = NULL, $intPageLimit = NULL, $strSortBy = NULL, $boolShowDisabledData = false ) {
		return CInspections::createService()->fetchPaginatedInspectionsBulkGroupsByCid( $this->getId(), $objDatabase, $boolIsFetchParentBulkGroup, $intParentBulkGroupId, $arrintChildBulkGroupIds, $arrintPropertyGroupIds, $intOffset, $intPageLimit, $strSortBy, $boolShowDisabledData );
	}

	public function fetchInspectionsByInspectionStatusIdsByIds( $arrintInspectionStatusIds, $arrintInspectionIds, $objDatabase, $strSortBy = NULL ) {
		return CInspections::createService()->fetchInspectionsByInspectionStatusIdsByIdsByCid( $arrintInspectionStatusIds, $arrintInspectionIds, $this->getId(), $objDatabase, $strSortBy );
	}

	public function fetchInspectionsBulkGroupsTotalCountByPropertyGroupIds( $objDatabase, $arrintPropertyGroupIds = NULL, $boolShowDisabledData = false ) {
		return CInspections::createService()->fetchInspectionsBulkGroupsTotalCountByPropertyGroupIdsByCid( $this->getId(), $objDatabase, $arrintPropertyGroupIds, $boolShowDisabledData );
	}

	public function fetchParentMaintenanceGroupById( $intMaintenanceGroupId, $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceGroups::createService()->fetchParentMaintenanceGroupByIdByCid( $intMaintenanceGroupId, $this->getId(), $objDatabase );
	}

	public function fetchMaintenanceGroupsByIds( $arrintMaintenanceGroupIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceGroups::createService()->fetchMaintenanceGroupsByIdsByCid( $arrintMaintenanceGroupIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyVehiclesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CPropertyVehicles::fetchPropertyVehiclesByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchAllCompanyVehiclesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CPropertyVehicles::fetchAllCompanyVehiclesByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyVehiclesByVehicleIdByPropertyIds( $intVehicleId, $arrintPropertyIds, $objDatabase, $boolIsReturnArray = false ) {
		return CPropertyVehicles::fetchPropertyVehiclesByVehicleIdByPropertyIdsByCid( $intVehicleId, $arrintPropertyIds, $this->getId(), $objDatabase, $boolIsReturnArray );
	}

	public function fetchUnassociatedPropertyIdsByVehicleIdByPropertyIdsByCid( $intVehicleId, $arrintPropertyIds, $objDatabase ) {
		return CPropertyVehicles::fetchUnassociatedPropertyIdsByVehicleIdByPropertyIdsByCid( $intVehicleId, $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchTimeZonesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CTimeZones::fetchTimeZonesByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPrimaryWebsitesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CWebsites::createService()->fetchPrimaryWebsitesByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPetRateAssociationsWithDefaultPetTypeIdsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\Custom\CPetRateAssociations::createService()->fetchPetRateAssociationsWithDefaultPetTypeIdsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchPropertyHoursKeyedByWeekdayByPropertyHourTypeIdByPropertyIds( $intPropertyHourTypeId, $arrintPropertyIds, $objDatabase ) {
		return CPropertyHours::fetchPropertyHoursKeyedByWeekdayByPropertyHourTypeIdByPropertyIdsByCid( $intPropertyHourTypeId, $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchCustomInspectionById( $intInspectionId, $objDatabase ) {
		return CInspections::createService()->fetchCustomInspectionByIdByCid( $intInspectionId, $this->getId(), $objDatabase );
	}

	public function fetchCustomerInformationById( $intCustomerId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerInformationByIdByCid( $intCustomerId, $this->getId(), $objDatabase );
	}

	public function fetchCustomerInformationByIds( $arrintCustomerIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerInformationByIdsByCid( $arrintCustomerIds, $this->getId(), $objDatabase );
	}

	public function fetchPublishedPropertyLeadSourcesByInternetListingServiceIdsByPropertyIds( $arrintIlsIds, $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPublishedPropertyLeadSourcesByInternetListingServiceIdsByPropertyIdsByCid( $arrintIlsIds, $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchIntegratedMaintenanceStatusByName( $strName, $objDatabase ) {
		return CMaintenanceStatuses::createService()->fetchIntegratedMaintenanceStatusByCidByName( $this->getId(), trim( $strName ), $objDatabase );
	}

	public function fetchFmoProcessedOnByLeaseId( $intLeaseId, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchFmoProcessedOnByLeaseIdByCid( $intLeaseId, $this->getId(), $objDatabase );
	}

	public function fetchCustomPackagesDataWithCustomerByPackageBatchIdByPackageFilter( $objPackageFilter, $objDatabase ) {
		return CPackages::fetchCustomPackagesDataWithCustomerByPackageBatchIdByPackageFilter( $this->getId(), $objPackageFilter, $objDatabase );
	}

	public function fetchFileTypeBySystemCodeById( $intFileTypeId, $objDatabase ) {
		return CFileTypes::fetchFileTypeBySystemCodeById( $intFileTypeId, $this->getId(), $objDatabase );
	}

	public function fetchSimpleCustomersByIdsByLeaseIds( $arrintCustomerIds, $arrintLeaseIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchSimpleCustomersByIdsByLeaseIdsByCid( $arrintCustomerIds, $arrintLeaseIds, $this->getId(), $objDatabase );
	}

	public function fetchAllScheduledPaymentsByPaymentTypesByPropertyIdsByCid( $arrintEligiblePaymentTypes, $arrintPropertyIds, $objDatabase ) {
		return CScheduledPayments::fetchAllScheduledPaymentsByPaymentTypesByPropertyIdsByCid( $arrintEligiblePaymentTypes, $arrintPropertyIds, $this->getId(), $objDatabase );
	}

	public function fetchCustomerPortalSettingsByCustomerIds( $arrintCustomerIds, $objDatabase ) {
		return CCustomerPortalSettings::fetchCustomCustomerPortalSettingByCustomerIdsByCid( $arrintCustomerIds, $this->getId(), $objDatabase );
	}

	public function fetchCountOfLeasesWithActiveIntervalEndingNextMonthByLeaseIds( $arrintLeaseIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchCountOfLeasesWithActiveIntervalEndingNextMonthByIdsByCid( $arrintLeaseIds, $this->getId(), $objDatabase );
	}

	public function fetchFileAssociationsByInspectionId( $intInspectionId, $objDatabase ) {
		return CFileAssociations::createService()->fetchFileAssociationsByInspectionIdByCid( $intInspectionId, $this->getId(), $objDatabase );
	}

	public function fetchAllActiveCompanyWebsiteTags( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyWebsiteTags::createService()->fetchAllActiveCompanyWebsiteTagsByCid( $this->getId(), $objDatabase );
	}

	public function fetchCompanyWebsiteTagById( $intWebsiteTagId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyWebsiteTags::createService()->fetchCompanyWebsiteTagByIdByCid( $intWebsiteTagId, $this->getId(), $objDatabase );
	}

	public function fetchPropertyReviewAttributesCountByCompanyReviewAttributeId( $intAttributeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyReviewAttributes::createService()->fetchPropertyReviewAttributesCountByCIdByReviewAttributeId( $this->getId(), $intAttributeId, $objDatabase );
	}

	public function fetchUndeliveredPacakgesCountForChartByPropertyIdsByCid( $objDatabase, $arrintPropertyIds = [] ) {
		return CPackages::fetchUndeliveredPacakgesCountForChartByPropertyIdsByCid( $this->getId(), $objDatabase, $arrintPropertyIds );
	}

	public function fetchDeliveredPacakgesCountForChartByPropertyIdsByCid( $objDatabase, array $arrintPropertyIds ) {
		return CPackages::fetchDeliveredPacakgesCountForChartByPropertyIdsByCid( $this->getId(), $objDatabase, $arrintPropertyIds );
	}

	public function fetchAverageDeliveredPackagesForDateByPropertyIdsByCid( $objDatabase, array $arrintPropertyIds, $strFrom ) {
		return CPackages::fetchAverageDeliveredPackagesForDateByPropertyIdsByCid( $this->getId(), $objDatabase, $arrintPropertyIds, $strFrom );
	}

	public function fetchActiveNonSignedMerchantChangeRequestsCount( $objPaymentDatabase ) {
		return \Psi\Eos\Payment\CMerchantChangeRequests::createService()->fetchActiveNonSignedMerchantChangeRequestsCountByCid( $this->getId(), $objPaymentDatabase );
	}

	public function getSupportedLocaleCodes( $objDatabase ) {

		$arrstrSupportedLocaleCodes = CCache::fetchObject( 'supported_locale_codes_cid_' . $this->getId() );
		if( true == is_null( $arrstrSupportedLocaleCodes ) || false === $arrstrSupportedLocaleCodes ) {
			$arrstrSupportedLocaleCodes = [];
			$arrobjPropertyProducts = CPropertyProducts::createService()->fetchPropertyProductsByProductIdByProductOptionIdByCid( CPsProduct::ENTRATA, CPsProductOption::ENTRATA_INTERNATIONAL, $this->getId(), $objDatabase );
			if( true == valArr( $arrobjPropertyProducts ) ) {
				$arrobjSupportedLocaleCodes = ( array ) CCompanyLocales::fetchCompanyLocalesByCid( $this->getId(), $objDatabase );
				if( true == valArr( $arrobjSupportedLocaleCodes ) ) {
					foreach( $arrobjSupportedLocaleCodes as $objSupportedLocaleCode ) {
						$arrstrSupportedLocaleCodes[] = $objSupportedLocaleCode->getLocaleCode();
					}
				}
			}
			CCache::storeObject( 'supported_locale_codes_cid_' . $this->getId(), $arrstrSupportedLocaleCodes, $intSpecificLifetime = 3600, $arrstrTags = [] );
		}
		return $arrstrSupportedLocaleCodes;
	}

	public function fetchPropertyIdsForHistoricalData( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyProductLogs::createService()->fetchPropertyIdsForHistoricalDataByCid( $this->getId(), $objDatabase );
	}

	public function fetchPropertyGroupIdsForHistoricalData( $objDatabase ) {
		return CPropertyGroups::createService()->fetchPropertyGroupIdsForHistoricalDataByCid( $this->getId(), $objDatabase );
	}

	public function fetchInternationalMerchantChangeRequestAccounts( $objDatabase ) {
		return \Psi\Eos\Payment\CMerchantChangeRequestAccounts::createService()->fetchMerchantChangeRequestAccountsByCidByMerchantProcessingTypeId( $this->getId(), CMerchantProcessingType::INTERNATIONAL, $objDatabase );
	}

	public function fetchActiveDocumentById( $intDocumentId, $objDatabase ) {
		return \Psi\Eos\Entrata\CDocuments::createService()->fetchActiveDocumentByIdByCid( $intDocumentId, $this->getId(), $objDatabase );
	}
	public function fetchCustomerContactSubmissionById( $intContactSubmissionId, $objDatabase, $boolIsEmailInsertion = false) {
		return CContactSubmissions::fetchCustomerContactSubmissionByIdByCid( $intContactSubmissionId, $this->getId(), $objDatabase, $boolIsEmailInsertion );
	}
	/**
	 * @return bool
	 * function returns true if the client is enabled for punchout
	 **/
	public function isPunchOutEnabledClient() : bool {
		if( 'production' != CONFIG_ENVIRONMENT
		    || ( 'production' == CONFIG_ENVIRONMENT && true == in_array( $this->getId(), self::$c_arrintPunchOutEnabledClients ) ) ) {

			return true;
		}

		return false;
	}

	public function fetchFormsByDocumentIdsByPropertyIdByCustomerIdByCid( $arrintDocumentIds, $arrintPropertyIds, $intCustomerId, $intCid, $objDatabase ) {
		if( false == \Psi\Libraries\UtilFunctions\valArr( $arrintDocumentIds ) && false == \Psi\Libraries\UtilFunctions\valArr( $arrintPropertyIds ) && false == \Psi\Libraries\UtilFunctions\valId( $intCustomerId ) ) {
			return NULL;
		}
		return CForms::fetchFormsByDocumentIdsByPropertyIdByCustomerIdByCid( $arrintDocumentIds, $arrintPropertyIds, $intCustomerId, $intCid, $objDatabase );
	}
}
?>
