<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyStatusTypes
 * Do not add any new functions to this class.
 */

class CCompanyStatusTypes extends CBaseCompanyStatusTypes {

	/**
	 * Fetch Functions
	 */

	public static function fetchCompanyStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCompanyStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCompanyStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCompanyStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

    public static function fetchAllCompanyStatusTypes( $objDatabase ) {
        return self::fetchCompanyStatusTypes( sprintf( 'SELECT * FROM %s WHERE is_published = 1 ORDER BY order_num', 'company_status_types' ), $objDatabase );
    }

    public static function fetchCompanyStatusTypesByIds( $arrintCompanyStatusTypeIds, $objDatabase, $orderBy = NULL ) {

    	if( false == valArr( $arrintCompanyStatusTypeIds ) ) return;
    	if( true == is_null( $orderBy ) ) {
    		$strAppend = 'ORDER BY order_num';
    	} else {
    		$strAppend = $orderBy;
    	}

		$strSql = 'SELECT * FROM company_status_types WHERE is_published = 1 AND id IN ( ' . implode( ',', $arrintCompanyStatusTypeIds ) . ' ) ' . $strAppend;

		return self::fetchCompanyStatusTypes( $strSql, $objDatabase );
    }
}
?>