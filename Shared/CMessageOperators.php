<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CMessageOperators
 * Do not add any new functions to this class.
 */

class CMessageOperators extends CBaseMessageOperators {

	public static function fetchMessageOperators( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMessageOperator', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchMessageOperator( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMessageOperator', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchAllMessageOperators( $objSmsDatabase ) {
		return self::fetchMessageOperators( 'SELECT * FROM message_operators ORDER BY name ASC', $objSmsDatabase );
	}

	public static function fetchMessageOperatorByOperatorNumber( $intOperatorNumber, $objSmsDatabse ) {
		return self::fetchMessageOperator( 'SELECT * FROM message_operators WHERE operator_number = ' . ( int ) $intOperatorNumber, $objSmsDatabse );
	}

	public static function fetchPublishedMessageOperators( $objSmsDatabase ) {
		return self::fetchMessageOperators( 'SELECT * FROM message_operators WHERE is_published = 1 ORDER BY order_num', $objSmsDatabase );
	}
}
?>