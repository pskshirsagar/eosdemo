<?php

class CHelpResourceAssociation extends CBaseHelpResourceAssociation {

	protected $m_strHelpResourceTitle;
	protected $m_strHelpResourceContent;

	protected $m_intHelpResourceTypeId;

	const SURVEY_ASSOCIATION_ORDER_NUM = 9999;

	/**
	 * Get Functions
	 *
	 */

	public function getHelpResourceTitle() {
		return $this->m_strHelpResourceTitle;
	}

	public function getHelpResourceContent() {
		return $this->m_strHelpResourceContent;
	}

	public function getHelpResourceTypeId() {
		return $this->m_intHelpResourceTypeId;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setHelpResourceTitle( $strHelpResourceTitle ) {
		$this->m_strHelpResourceTitle = $strHelpResourceTitle;
	}

	public function setHelpResourceContent( $strHelpResourceContent ) {
		$this->m_strHelpResourceContent = $strHelpResourceContent;
	}

	public function setHelpResourceTypeId( $intHelpResourceTypeId ) {
		$this->m_intHelpResourceTypeId = $intHelpResourceTypeId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['help_resource_title'] ) ) 		$this->setHelpResourceTitle( $arrmixValues['help_resource_title'] );
		if( true == isset( $arrmixValues['help_resource_content'] ) ) 		$this->setHelpResourceContent( $arrmixValues['help_resource_content'] );
		if( true == isset( $arrmixValues['help_resource_type_id'] ) ) 		$this->setHelpResourceTypeId( $arrmixValues['help_resource_type_id'] );
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valParentHelpResourceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHelpResourceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHelpDocAssociation( $objDatabase ) {

		$boolIsValid = true;

		if( CDatabaseType::CLIENT == $objDatabase->getDatabaseTypeId() ) {
			$arrobjHelpResourceAssociations = \Psi\Eos\Entrata\CHelpResourceAssociations::createService()->fetchHelpResourceAssociationsBySectionHelpResourceIdByHelpResourceIdByCid( $this->getSectionHelpResourceId(), ( $this->getHelpResourceId() ) ? $this->getHelpResourceId() : $this->getTrackHelpResourceId(), $this->getCid(), $objDatabase );
		} else {
			$arrobjHelpResourceAssociations = \Psi\Eos\Admin\CHelpResourceAssociations::createService()->fetchHelpResourceAssociationsBySectionHelpResourceIdByHelpResourceIdByCid( $this->getSectionHelpResourceId(), $this->getHelpResourceId(), CClient::ID_DEFAULT, $objDatabase );
		}

		if( false == valArr( $arrobjHelpResourceAssociations ) ) {
			return true;
		}

		if( valId( $this->getId() ) ) {
			if( 1 == \Psi\Libraries\UtilFunctions\count( $arrobjHelpResourceAssociations ) && current( $arrobjHelpResourceAssociations )->getId() != $this->getId() ) {
				$boolIsValid = false;
			}
		} else {
			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrobjHelpResourceAssociations ) ) {
				$boolIsValid = false;
			}
		}

		if( false == $boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL', ' This help doc already associated with this section.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'unique_help_doc_association_for_section':
				$boolIsValid &= $this->valHelpDocAssociation( $objDatabase );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>