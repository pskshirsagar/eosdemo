<?php

class CArPaymentDetail extends CBaseArPaymentDetail {

	use TPostalAddressHelper;

	protected $m_strCcCardNumberEncrypted;
	protected $m_strCheckAccountNumberEncrypted;
	/**
	*
	* Get Functions
	*/

	public function getBilltoIpAddressEncrypted() {
		return $this->m_strBilltoIpAddress;
	}

	public function getBilltoEmailAddressEncrypted() {
		return $this->m_strBilltoEmailAddress;
	}

	public function getBilltoPhoneNumberEncrypted() {
		return $this->m_strBilltoPhoneNumber;
	}

	public function getAllocateArTransactionIds() {
		return json_decode( $this->m_strAllocateArTransactionIds );
	}

	public function getCcCardNumberEncrypted() {
		return $this->m_strCcCardNumberEncrypted;
	}

	public function getCheckAccountNumberEncrypted() {
		return $this->m_strCheckAccountNumberEncrypted;
	}

	/**
	*
	* Set Functions
	*/

	public function setBilltoAccountNumber( $strBilltoAccountNumber ) {
		$this->m_strBilltoAccountNumber = CStrings::strTrimDef( $strBilltoAccountNumber, 50, NULL, true, false, true );
	}

	public function setBilltoCity( $strBilltoCity ) {
		$this->m_strBilltoCity = CStrings::strTrimDef( \Psi\CStringService::singleton()->ucfirst( $strBilltoCity ), 50, NULL, true, false, true );
	}

	public function setBilltoCompanyName( $strBilltoCompanyName ) {
		$this->m_strBilltoCompanyName = CStrings::strTrimDef( $strBilltoCompanyName, 100, NULL, true, false, true );
	}

	public function setBilltoIpAddress( $strBilltoIpAddress ) {
		$this->m_strBilltoIpAddress = CStrings::strTrimDef( $strBilltoIpAddress, 23, NULL, true );
	}

	public function setBilltoEmailAddress( $strBilltoEmailAddress ) {
		$this->m_strBilltoEmailAddress = CStrings::strTrimDef( $strBilltoEmailAddress, 240, NULL, true, false, true );
	}

    public function setBilltoPhoneNumber( $strBilltoPhoneNumber ) {
        $this->m_strBilltoPhoneNumber = CStrings::strTrimDef( $strBilltoPhoneNumber, 30, NULL, true, false, true );
    }

    public function setBilltoStreetLine1( $strBilltoStreetLine1 ) {
        $this->m_strBilltoStreetLine1 = CStrings::strTrimDef( \Psi\CStringService::singleton()->ucwords( $strBilltoStreetLine1 ), 100, NULL, true, false, true );
    }

    public function setBilltoStreetLine2( $strBilltoStreetLine2 ) {
        $this->m_strBilltoStreetLine2 = CStrings::strTrimDef( \Psi\CStringService::singleton()->ucwords( $strBilltoStreetLine2 ), 100, NULL, true, false, true );
    }

    public function setBilltoStreetLine3( $strBilltoStreetLine3 ) {
        $this->m_strBilltoStreetLine3 = CStrings::strTrimDef( \Psi\CStringService::singleton()->ucwords( $strBilltoStreetLine3 ), 100, NULL, true, false, true );
    }

    public function setPaymentMemo( $strPaymentMemo ) {
        $this->m_strPaymentMemo = CStrings::strTrimDef( $strPaymentMemo, 2000, NULL, true, false, true );
    }

    public function setAllocateArTransactionIds( $arrintAllocateArTransactionIds ) {
    	$this->m_strAllocateArTransactionIds = json_encode( $arrintAllocateArTransactionIds );
    }

	public function setCcCardNumberEncrypted( $strCcCardNumberEncrypted ) {
		$this->set( 'm_strCcCardNumberEncrypted', CStrings::strTrimDef( $strCcCardNumberEncrypted, 240, NULL, true ) );
	}

	public function setCheckAccountNumberEncrypted( $strCheckAccountNumberEncrypted ) {
		$this->set( 'm_strCheckAccountNumberEncrypted', CStrings::strTrimDef( $strCheckAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['cc_card_number_encrypted'] ) ) {
			$this->setCcCardNumberEncrypted( $arrmixValues['cc_card_number_encrypted'] );
		}
		if( true == isset( $arrmixValues['check_account_number_encrypted'] ) ) {
			$this->setCheckAccountNumberEncrypted( $arrmixValues['check_account_number_encrypted'] );
		}
	}

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valArPaymentId() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valPaymentMemo() {
        $boolIsValid = true;

    	if( false == isset( $this->m_strPaymentMemo ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_memo', 'Note is required.' ) );
        }

        return $boolIsValid;
    }

    public function valBilltoIpAddress() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoAccountNumber() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoCompanyName() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoStreetLine1() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoStreetLine2() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoStreetLine3() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoCity() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoStateCode() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoProvince() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoPostalCode() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoCountryCode() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoPhoneNumber() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valBilltoEmailAddress() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valPhoneAuthRequired() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valPhoneAuthBy() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valPhoneAuthOn() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valApprovedBy() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valApprovedOn() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valExportedOn() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valReturnExportedOn() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
				break;

            case 'validate_update_status':
				$boolIsValid &= $this->valPaymentMemo();
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>