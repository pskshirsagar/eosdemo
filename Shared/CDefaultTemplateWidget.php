<?php

class CDefaultTemplateWidget extends CBaseDefaultTemplateWidget {

	protected $m_arrstrWidgetParameters;

	public function __construct() {
		parent::__construct();
		$this->m_boolShowDescription    = '';
		$this->m_strDescription         = '';
		$this->m_boolShowLink           = '';
		$this->m_intTarget              = '1';
		$this->m_boolShowImage          = '';
		$this->m_boolIsDefault          = '';
		$this->m_boolIsPublished        = '';
		$this->setAllowDifferentialUpdate( true );
	}

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valWebsiteTemplateId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valWidgetSpaceId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valWidgetNamespace() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valWidgetName() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valSupportedType() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valShowDescription() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valShowLink() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valLink() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valLinkText() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valTarget() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valAlt() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valShowImage() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valMediaName() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valMediaWidth() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valMediaHeight() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valWidgetParameters() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsDefault() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function updateSequence( $objClientDatabase ) {
		$intNextSeqId = $this->getId() + 1;
		$arrmixData   = fetchData( 'SELECT setval( \'public.default_template_widgets_id_seq\', ' . ( int ) $intNextSeqId . ' , FALSE );', $objClientDatabase );

		if( 0 >= $arrmixData[0]['setval'] ) {
			return false;
		}

		return true;
	}

	public function setEncodeWidgetParameters( $arrstrWidgetParameters ) {
		$this->m_arrstrWidgetParameters = $arrstrWidgetParameters;
		$strEncodedWidgetParameters     = base64_encode( serialize( $arrstrWidgetParameters ) );
		$this->setWidgetParameters( $strEncodedWidgetParameters );
	}

	public function getDecodeWidgetParameters() {
		$this->m_arrstrWidgetParameters = unserialize( base64_decode( $this->getWidgetParameters() ) );

		return $this->m_arrstrWidgetParameters;
	}

	public function getInsertSql( $intCurrentUserId ) {
		return $this->insert( $intCurrentUserId, NULL, true );
	}

	public function getUpdateSql( $intCurrentUserId ) {
        return $this->update( $intCurrentUserId, NULL, true );
	}

}

?>