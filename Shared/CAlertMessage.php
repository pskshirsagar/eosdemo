<?php

use Psi\Libraries\UtilPsHtmlMimeMail\CPsHtmlMimeMail;

class CAlertMessage extends CBaseAlertMessage {

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/*
	 * Set Message Function.
	 */

	public function setSerializeMessage( $arrstrMessages ) {
		$this->setMessage( ( string ) strval( serialize( $arrstrMessages ) ) );
	}

	public function setMessageCallId( $intCallId ) {
		$arrstrMessages = $this->getUnserializeMessage();
		$arrstrMessages['call_id'] = $intCallId;
		$this->setSerializeMessage( $arrstrMessages );
	}

	public function setCallerName( $strCallerName ) {
		$arrstrMessages = $this->getUnserializeMessage();
		$arrstrMessages['caller_name'] = $strCallerName;
		$this->setSerializeMessage( $arrstrMessages );
	}

	public function setCallQueueName( $strCallQueueName ) {
		$arrstrMessages = $this->getUnserializeMessage();
		$arrstrMessages['call_queue_name'] = $strCallQueueName;
		$this->setSerializeMessage( $arrstrMessages );
	}

	public function setCompanyName( $strCompanyName ) {
		$arrstrMessages = $this->getUnserializeMessage();
		$arrstrMessages['company_name'] = $strCompanyName;
		$this->setSerializeMessage( $arrstrMessages );
	}

	public function setPropertyName( $strPropertyName ) {
		$arrstrMessages = $this->getUnserializeMessage();
		$arrstrMessages['property_name'] = $strPropertyName;
		$this->setSerializeMessage( $arrstrMessages );
	}

	public function setLeadSourceName( $strLeadSourceName ) {
		$arrstrMessages = $this->getUnserializeMessage();
		$arrstrMessages['lead_source_name'] = $strLeadSourceName;
		$this->setSerializeMessage( $arrstrMessages );
	}

	public function setOriginPhoneNumber( $strOriginPhoneNumber ) {
		$arrstrMessages = $this->getUnserializeMessage();
		$arrstrMessages['origin_phone_number'] = $strOriginPhoneNumber;
		$this->setSerializeMessage( $arrstrMessages );
	}

	public function setDestinationPhoneNumber( $strDestinationPhoneNumber ) {
		$arrstrMessages = $this->getUnserializeMessage();
		$arrstrMessages['destination_phone_number'] = $strDestinationPhoneNumber;
		$this->setSerializeMessage( $arrstrMessages );
	}

	public function setCallTypeName( $strCallTypeName ) {
		$arrstrMessages = $this->getUnserializeMessage();
		$arrstrMessages['call_type_name'] = $strCallTypeName;
		$this->setSerializeMessage( $arrstrMessages );
	}

	public function setCallPhoneNumberId( $intCallPhoneNumberId ) {
		$arrstrMessages = $this->getUnserializeMessage();
		$arrstrMessages['call_phone_number_id'] = $intCallPhoneNumberId;
		$this->setSerializeMessage( $arrstrMessages );
	}

	public function setCallTypeId( $intCallTypeId ) {
		$arrstrMessages = $this->getUnserializeMessage();
		$arrstrMessages['call_type_id'] = $intCallTypeId;
		$this->setSerializeMessage( $arrstrMessages );
	}

	public function setRedirectUrl( $strRedirectUrl ) {
		$arrstrMessages = $this->getUnserializeMessage();
		$arrstrMessages['redirect_url'] = $strRedirectUrl;
		$this->setSerializeMessage( $arrstrMessages );
	}

	public function setDigitsPressed( $strDigitsPressed ) {
		$arrstrMessages = $this->getUnserializeMessage();
		$arrstrMessages['digits_pressed'] = $strDigitsPressed;
		$this->setSerializeMessage( $arrstrMessages );
	}

	/*
	 * Get Message Functions.
	 */

	public function getUnserializeMessage() {
		$arrstrMessages = ( array ) unserialize( $this->getMessage() );
		return $arrstrMessages;
	}

	public function getMessageCallId() {
		$arrstrMessages = $this->getUnserializeMessage();
		return ( true == valStr( $arrstrMessages['call_id'] ) ) ? $arrstrMessages['call_id'] : '';

	}

	public function getCallerName() {
		$arrstrMessages = $this->getUnserializeMessage();
		return ( true == valStr( $arrstrMessages['caller_name'] ) ) ? $arrstrMessages['caller_name'] : '';
	}

	public function getCallQueueName() {
		$arrstrMessages = $this->getUnserializeMessage();
		return ( true == valStr( $arrstrMessages['call_queue_name'] ) ) ? $arrstrMessages['call_queue_name'] : '';
	}

	public function getCompanyName() {
		$arrstrMessages = $this->getUnserializeMessage();
		return ( true == valStr( $arrstrMessages['company_name'] ) ) ? $arrstrMessages['company_name'] : '';
	}

	public function getPropertyName() {
		$arrstrMessages = $this->getUnserializeMessage();
		return ( true == valStr( $arrstrMessages['property_name'] ) ) ? $arrstrMessages['property_name'] : '';
	}

	public function getLeadSourceName() {
		$arrstrMessages = $this->getUnserializeMessage();
		return ( true == valStr( $arrstrMessages['lead_source_name'] ) ) ? $arrstrMessages['lead_source_name'] : '';
	}

	public function getOriginPhoneNumber() {
		$arrstrMessages = $this->getUnserializeMessage();
		return ( true == valStr( $arrstrMessages['origin_phone_number'] ) ) ? $arrstrMessages['origin_phone_number'] : '';
	}

	public function getDestinationPhoneNumber() {
		$arrstrMessages = $this->getUnserializeMessage();
		return ( true == valStr( $arrstrMessages['destination_phone_number'] ) ) ? $arrstrMessages['destination_phone_number'] : '';
	}

	public function getCallTypeName() {
		$arrstrMessages = $this->getUnserializeMessage();
		return ( true == valStr( $arrstrMessages['call_type_name'] ) ) ? $arrstrMessages['call_type_name'] : '';
	}

	public function getCallPhoneNumberId() {
		$arrstrMessages = $this->getUnserializeMessage();
		return ( true == valStr( $arrstrMessages['call_phone_number_id'] ) ) ? $arrstrMessages['call_phone_number_id'] : '';
	}

	public function getCallTypeId() {
		$arrstrMessages = $this->getUnserializeMessage();
		return ( true == valStr( $arrstrMessages['call_type_id'] ) ) ? $arrstrMessages['call_type_id'] : '';
	}

	public function getRedirectUrl() {
		$arrstrMessages = $this->getUnserializeMessage();
		return ( true == valStr( $arrstrMessages['redirect_url'] ) ) ? $arrstrMessages['redirect_url'] : '';
	}

	public function getDigitsPressed() {
		$arrstrMessages = $this->getUnserializeMessage();
		return ( true == valStr( $arrstrMessages['digits_pressed'] ) ) ? $arrstrMessages['digits_pressed'] : '';
	}

	/*
	 * Misc functions.
	 */

	public function syncAlertMessages( $intCompanyUserId, $objDatabase, $intEventId, $boolIncludeDeletedAA = false ) {

		if( false == defined( 'CONFIG_XMPP_ENABLED' )
		    || false == ( bool ) CONFIG_XMPP_ENABLED
		    || false == valId( $intEventId )
		    || false == valId( $this->getCid() ) ) return;

		$objEvent = \Psi\Eos\Entrata\CEvents::createService()->fetchEventByIdByCid( $intEventId, $this->getCid(), $objDatabase );

		$arrintLeadEventTypeIds = [
			CEventType::CALL_INCOMING,
			CEventType::SMS_INCOMING,
			CEventType::ONLINE_CHAT,
			CEventType::ONLINE_GUEST_CARD,
			CEventType::APPLICATION_PROGRESS,
			CEventType::VOICE_MAIL,
			CEventType::CRAIGSLIST_POST_REMINDER
		];

		// We are restricting Events of following type in LeadAlert created/updated in Entrata.
		$arrintEntrataGenereatedEvents	= [
			CEventType::CALL_INCOMING,
			CEventType::SMS_INCOMING
		];

		if( false == valObj( $objEvent, 'CEvent' )
		    || ( false == in_array( $objEvent->getEventTypeId(), $arrintLeadEventTypeIds ) )
		    || ( true == in_array( $objEvent->getEventTypeId(), $arrintEntrataGenereatedEvents ) && true == is_null( $objEvent->getDataReferenceId() ) ) ) return;

		preg_match( '/\d+/', $objEvent->getNotes(), $arrintPhoneNumber );

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		// Probably we are going to merge query for Craigslist and CallTracker in nearest release[Amit].
		if( CEventType::CALL_INCOMING == $objEvent->getEventTypeId() && true == intval( $intPhoneNumber = getArrayElementByKey( 0, $arrintPhoneNumber ) ) ) {
			$strSql = '
					SELECT
						DISTINCT ON( cu.id )
						NULL AS applicant_id,
						cu.id AS company_user_id,
						NULL AS lease_interval_type_id,
						' . CPsProduct::CALL_TRACKER . '	AS ps_product_id,
						0 AS application_id,
						' . $objEvent->getDataReferenceId() . '	AS call_id,
						NULL AS lead_source_id,
						NULL AS name_first,
						NULL AS name_last,
						' . ( int ) $intPhoneNumber . ' AS phone_number
					FROM
						company_users cu
						JOIN company_user_preferences cupr ON ( cu.id = cupr.company_user_id AND cu.cid = cupr.cid AND cupr.key = \'IS_LEAD_ALERT_USER\' )
						LEFT JOIN view_company_user_properties cup ON ( cup.company_user_id = cu.id AND cup.cid = cu.cid AND cu.default_company_user_id IS NULL AND cup.property_id = ' . ( int ) $objEvent->getPropertyId() . ' )
						LEFT JOIN company_user_permissions cups ON ( cups.company_user_id = cu.id AND cups.cid = cu.cid AND cu.default_company_user_id IS NULL AND module_id = ' . ( int ) CModule::REPORT_SYSTEM_COMMUNICATIONS_TAB . ' )
						LEFT JOIN company_user_groups cug ON( cug.company_user_id = cu.id AND cug.cid = cu.cid AND cu.default_company_user_id IS NULL )
						LEFT JOIN company_group_permissions cgp ON( cug.cid = cgp.cid AND cgp.company_group_id = cug.company_group_id AND cgp.module_id = ' . ( int ) CModule::REPORT_SYSTEM_COMMUNICATIONS_TAB . ' AND cgp.is_allowed = 1 AND ( cups.is_inherited = 1 OR cups.is_inherited IS NULL ) )
					WHERE
						cu.is_disabled = 0
						AND ( cu.is_administrator = 1 OR ( ( cups.is_allowed = 1 OR cgp.is_allowed = 1 ) AND cup.property_id = ' . ( int ) $objEvent->getPropertyId() . ' ) )
						AND cu.cid = ' . ( int ) $objEvent->getCid();

		} elseif( CEventType::CRAIGSLIST_POST_REMINDER == $objEvent->getEventTypeId() ) {
			$strSql = '
					SELECT
						DISTINCT ON( cu.id )
						' . $objEvent->getId() . ' AS applicant_id,
						cu.id AS company_user_id,
						NULL AS lease_interval_type_id,
						' . CPsProduct::CRAIGSLIST_POSTING . ' AS ps_product_id,
						0 AS application_id,
						NULL AS call_id,
						NULL AS lead_source_id,
						NULL AS name_first,
						NULL AS name_last,
						NULL AS phone_number
					FROM
						event_references er
						JOIN company_users cu on( cu.cid = er.cid AND cu.is_disabled = 0 )
						JOIN company_user_preferences cupr ON ( cu.id = cupr.company_user_id AND cu.cid = cupr.cid AND cupr.key = \'IS_LEAD_ALERT_USER\' )
						LEFT JOIN view_company_user_properties cup ON( cup.company_user_id = cu.id AND cup.cid = cu.cid AND cu.default_company_user_id IS NULL AND cup.property_id = ' . ( int ) $objEvent->getPropertyId() . ' )
						LEFT JOIN company_user_permissions cups ON( cups.company_user_id = cu.id AND cups.cid = cu.cid AND cu.default_company_user_id IS NULL AND module_id = ' . ( int ) CModule::CL_ADS . ' )
						LEFT JOIN company_user_groups cug ON( cug.company_user_id = cu.id AND cug.cid = cu.cid AND cu.default_company_user_id IS NULL )
						LEFT JOIN company_group_permissions cgp ON( cug.cid = cgp.cid AND cgp.company_group_id = cug.company_group_id AND cgp.module_id = ' . ( int ) CModule::CL_ADS . ' AND cgp.is_allowed = 1 AND ( cups.is_inherited = 1 OR cups.is_inherited IS NULL ) )
					WHERE
						er.cid = ' . ( int ) $objEvent->getCid() . '
						AND er.event_id = ' . ( int ) $objEvent->getId() . '
						AND ( cu.is_administrator = 1 OR( ( cups.is_allowed = 1 OR cgp.is_allowed = 1 ) AND cup.property_id = ' . ( int ) $objEvent->getPropertyId() . ' ) )
						AND er.event_reference_type_id = ' . ( int ) CEventReferenceType::CRAIGSLIST;

		} else {

			// Restricted those user who don't have Leads Tab view permissions.
			$strSql = 'SELECT
							DISTINCT ON( cu.id )
							a.id AS applicant_id,
							cu.id AS company_user_id,
							appl.lease_interval_type_id AS lease_interval_type_id,
							appl.ps_product_id AS ps_product_id,
							appl.id AS application_id,
							appl.call_id AS call_id,
							appl.originating_lead_source_id AS lead_source_id,
							COALESCE( a.name_first, NULL ) AS name_first,
							COALESCE( a.name_last,  NULL ) AS name_last,
							cpn.phone_number
						FROM
							events e
							JOIN cached_applications appl ON( appl.lease_interval_id = e.lease_interval_id AND appl.cid = e.cid )
							JOIN applicant_applications aa ON( aa.application_id = appl.id AND aa.cid = appl.cid ' . $strCheckDeletedAASql . ' )
							JOIN applicants a ON( a.id = aa.applicant_id AND a.cid = aa.cid )
							JOIN company_users cu ON( cu.cid = e.cid AND cu.is_disabled = 0 )
							JOIN company_user_preferences cupr ON( cu.id = cupr.company_user_id AND cu.cid = cupr.cid AND cupr.key = \'IS_LEAD_ALERT_USER\' )
							LEFT JOIN view_company_user_properties cup ON( cu.is_administrator <> 1 AND cup.company_user_id = cu.id AND cup.cid = cu.cid AND cu.default_company_user_id IS NULL AND cup.property_id = ' . ( int ) $objEvent->getPropertyId() . ' )
							LEFT JOIN company_user_permissions cups ON( cups.company_user_id = cu.id AND cups.cid = cu.cid AND cu.default_company_user_id IS NULL AND cups.module_id = ' . ( int ) CModule::PROSPECT_SYSTEM_LEAD . ' )
							LEFT JOIN company_user_groups cug ON( cug.company_user_id = cu.id AND cug.cid = cu.cid AND cu.default_company_user_id IS NULL )
							LEFT JOIN company_group_permissions cgp ON( cug.cid = cgp.cid AND cgp.company_group_id = cug.company_group_id AND cgp.module_id = ' . ( int ) CModule::PROSPECT_SYSTEM_LEAD . ' AND cgp.is_allowed = 1 AND ( cups.is_inherited = 1 OR cups.is_inherited IS NULL ) )
							LEFT JOIN customer_phone_numbers cpn ON a.customer_id = cpn.customer_id AND cpn.is_primary = true
						WHERE
							e.cid = ' . ( int ) $objEvent->getCid() . '
							AND e.id = ' . ( int ) $objEvent->getId() . '
							AND ( cu.is_administrator = 1 OR ( ( cups.is_allowed = 1 OR cgp.is_allowed = 1 ) AND cup.property_id = ' . ( int ) $objEvent->getPropertyId() . ' ) )
							AND cpn.deleted_on IS NULL
							AND e.lease_interval_id IS NOT NULL --( e.customer_id IS NOT NULL )';
		}

		$arrmixEvents = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixEvents ) ) return;

		$arrmixEventData = [
			'event_id' 			 	=> $objEvent->getId(),
			'created_on' 		 	=> $objEvent->getUpdatedOn(),
			'property_id' 		 	=> $objEvent->getPropertyId(),
			'reference_id'		 	=> $objEvent->getDataReferenceId(),
			'event_type_id' 		=> $objEvent->getEventTypeId(),
			'event_status_type_id'	=> $objEvent->getNewStatusId()
		];

		if( true == in_array( $objEvent->getEventTypeId(), [ CEventType::CALL_INCOMING, CEventType::VOICE_MAIL ] ) ) {
			$arrmixEventData['is_call_event'] = 1;
		}

		$strSql = 'INSERT INTO public.alert_messages ( cid, company_user_id, call_id, message_datetime, message, created_by, created_on ) VALUES ';

		foreach( $arrmixEvents as $arrmixAlertMessage ) {
			$arrmixAlertMessage['name_first'] 	= addslashes( $arrmixAlertMessage['name_first'] );
			$arrmixAlertMessage['name_last'] 	= addslashes( $arrmixAlertMessage['name_last'] );

			$arrstrSqlColumns[] = '( ' . $objEvent->getCid();
			$arrstrSqlColumns[] = getArrayElementByKey( 'company_user_id', $arrmixAlertMessage );
			$arrstrSqlColumns[] = ( true == is_null( getArrayElementByKey( 'call_id', $arrmixAlertMessage ) ) ) ? 'NULL' : $arrmixAlertMessage['call_id'];
			$arrstrSqlColumns[] = 'NOW()';
			$arrstrSqlColumns[] = '\'' . ( string ) strval( serialize( $arrmixAlertMessage + $arrmixEventData ) ) . '\'';
			$arrstrSqlColumns[] = $intCompanyUserId;
			$arrstrSqlColumns[] = 'NOW() )';
		}

		fetchData( $strSql . implode( ',', $arrstrSqlColumns ) . ';', $objDatabase );

		return true;
	}

	public function sendLogEmail( $objScript, $strMessage, $arrobjExceptions = NULL ) {

		$strObject = ( isset( $arrobjExceptions ) ) ? ' Object: ' . print_r( $arrobjExceptions, true ) : '';
		$objMimeMail = new CPsHtmlMimeMail();
		$objMimeMail->setText( $strMessage . $strObject );
		$objMimeMail->setFrom( $objScript->getScriptName() . '@' . $_SERVER['HTTP_HOST'] );
		$objMimeMail->setSubject( $objScript->getScriptName() . '#' . $objScript->m_objClientDatabase->getDatabaseName() . ' log at ' . date( 'Y-m-d H:i:s' ) );
		$objMimeMail->send( [ 'Amit <aprasad@xento.com>' ] );
	}

	public static function syncXmppUsers( $intDatabaseId ) {
		$objXmppSyncUsersSenderLibrary = new CXmppSyncUsersSenderLibrary();
		$objXmppSyncUsersSenderLibrary->setDatabaseId( $intDatabaseId );
		$objXmppSyncUsersSenderLibrary->sendChatXmppUserMessage();
	}

	public function sendCallNotification( $objCall, $objCallAgent ) {
		return ( new CCallsMessageSenderLibrary() )->sendCallNotificationMessage( $objCall, $objCallAgent );
	}

}
?>