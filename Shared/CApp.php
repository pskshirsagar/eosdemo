<?php

class CApp extends CBaseApp {

	protected $m_strCompetitorName;
	protected $m_intCompanyAppId;
	protected $m_intAppinstalled = NULL;
	protected $m_arrobjScopes;
	protected $m_arrobjAllowedServices;

	protected $m_strClientId;
	protected $m_strClientSecret;
	protected $m_strAuthUrl;
	protected $m_boolUseApiThrottling;
	protected $m_objOauthClient;
	protected $m_strPsProductName;
	protected $m_intContractStatusTypeId;
	protected $m_strTitle;
	protected $m_boolIsDocumentUploaded;
	protected $m_arrstrRequiredFields;
	protected $m_arrstrErrorMsg;
	protected $m_boolIsTermsAndConditionsRequired;
	protected $m_arrintScopeIds;
	protected $m_strAuthEmail;

	private $m_boolIsAuthEmailRequired;

	protected $m_fltRecurringAmount;

	const LEASE_EXECUTION	= 44;
	const RESIDENT_PORTAL	= 50;

	public static $c_arrintProductDependancyRelation = [
		CApp::LEASE_EXECUTION	=> CApp::RESIDENT_PORTAL
	];

	public function initialize( array $arrmixData ) {
		$this->setDefaults();
		$this->applyRequestForm( $arrmixData );
	}

	/**
	 * Create Function
	 */

	public function createCompanyApp( $intCompanyUserId ) {
		$objCompanyApp = new CCompanyApp();
		$objCompanyApp->setAppId( $this->getId() );
		$objCompanyApp->setNotified( $intCompanyUserId );
		$objCompanyApp->setNotifiedOn( date( 'm/d/Y H:i:s' ) );

		return $objCompanyApp;
	}

    /**
     * Get Function
     */

	public function getCompetitorName() {
		return $this->m_strCompetitorName;
	}

	public function getCompanyAppId() {
		return $this->m_intCompanyAppId;
	}

	public function getAppinstalled() {
		return $this->m_intAppinstalled;
	}

	public function getClientId() {
		return $this->m_strClientId;
	}

	public function getClientSecret() {
		return $this->m_strClientSecret;
	}

	public function getAuthUrl() {
		return $this->m_strAuthUrl;
	}

	public function getUseApiThrottling() {
		return $this->m_boolUseApiThrottling;
	}

	public function getPsProductName() {
		return $this->m_strPsProductName;
	}

	public function getBaseDirForAppLogoAndScreenshots( $intAppId ) {
		return PATH_MOUNTS . '/static_medias/apps/' . $intAppId;
	}

	public function getBaseDirForAppLogo( $intAppId ) {
		return PATH_MOUNTS . 'static_medias/apps/' . $intAppId . '/logos';
	}

	public function getBaseDirForAppScreenshots( $intAppId ) {
		return PATH_MOUNTS . 'static_medias/apps/' . $intAppId . '/screenshots';
	}

	public function getUrlForAllAppScreenshots() {

		$arrstrAppScreenShotUrl = [];

		if( 0 >= $this->getId() ) {
			return NULL;
		}

		$strBaseDirPath = $this->getBaseDirForAppScreenshots( $this->getId() );

		if( false != is_dir( $strBaseDirPath ) ) {

			$intHandle = opendir( $strBaseDirPath );
			if( false != $intHandle ) {
				while( false !== ( $strFileName = readdir( $intHandle ) ) ) {
					if( '.' != $strFileName && '..' != $strFileName ) {
						$arrstrAppScreenShotUrl[] = '/static_medias/apps/' . $this->getId() . '/screenshots/' . $strFileName;
					}
				}
				closedir( $intHandle );
			}
		}

		return $arrstrAppScreenShotUrl;
	}

	public function getUrlForAppLogo() {

		$strAppLogoUrl = NULL;

		if( false == valStr( $this->getLogoName() ) ) {
			return NULL;
		}

		$strBaseDirPath = $this->getBaseDirForAppLogo( $this->getId() );

		if( false != is_file( $strBaseDirPath . '/' . $this->getLogoName() ) ) {
			$strAppLogoUrl = '/static_medias/apps/' . $this->getId() . '/logos/' . $this->getLogoName();
		}

		return $strAppLogoUrl;

	}

	public function getScopes() {
		return $this->m_arrobjScopes;
	}

	public function getOrFetchScopes( CDatabase $objDatabase ) {
		if( true == empty( $this->m_arrobjScopes ) ) {
			$this->m_arrobjScopes = \Psi\Eos\Admin\CScopes::createService()->fetchScopesByAppId( $this->getId(), $objDatabase );
		}

		return $this->m_arrobjScopes;
	}

	public function getOrFetchAllowedServices( CDatabase $objDatabase ) {
		if( false == empty( $this->m_arrobjAllowedServices ) ) {
			$this->m_arrobjAllowedServices = \Psi\Eos\Entrata\CServices::createService()->fetchAllowedServicesByAppId( $this->getId(), $objDatabase );
		}

		return $this->m_arrobjAllowedServices;
	}

	public function getContractStatusTypeId() {
		return $this->m_intContractStatusTypeId;
	}

	public function getRecurringAmount() {
		return $this->m_fltRecurringAmount;
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function getIsDocumentUploaded() {
		return $this->m_boolIsDocumentUploaded;
	}

	public function getIsTermsAndConditionsRequired() {
		return $this->m_boolIsTermsAndConditionsRequired;
	}

	public function getAuthEmail() {
		return $this->m_strAuthEmail;
	}

	public function getIsAuthEmailRequired() {
		return $this->m_boolIsAuthEmailRequired;
	}

	public function getScopeIds() {
		return $this->m_arrintScopeIds;
	}

	/**
	 * Set Function
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['competitor_name'] ) )		$this->setCompetitorName( $arrmixValues['competitor_name'] );
		if( true == isset( $arrmixValues['company_app_id'] ) ) 		$this->setCompanyAppId( $arrmixValues['company_app_id'] );
		if( true == isset( $arrmixValues['app_installed'] ) )			$this->setAppinstalled( $arrmixValues['app_installed'] );
		if( true == isset( $arrmixValues['client_id'] ) )				$this->setClientId( $arrmixValues['client_id'] );
		if( true == isset( $arrmixValues['client_secret'] ) )			$this->setClientSecret( $arrmixValues['client_secret'] );
		if( true == isset( $arrmixValues['auth_url'] ) )				$this->setAuthUrl( $arrmixValues['auth_url'] );
		if( true == isset( $arrmixValues['use_api_throttling'] ) )		$this->setUseApiThrottling( $arrmixValues['use_api_throttling'] );
		if( true == isset( $arrmixValues['ps_product_name'] ) )		$this->setPsProductName( $arrmixValues['ps_product_name'] );
		if( true == isset( $arrmixValues['contract_status_type_id'] ) ) $this->setContractStatusTypeId( $arrmixValues['contract_status_type_id'] );
		if( true == isset( $arrmixValues['recurring_amount'] ) ) 		$this->setRecurringAmount( $arrmixValues['recurring_amount'] );
		if( true == isset( $arrmixValues['title'] ) )                  $this->setTitle( $arrmixValues['title'] );
		if( true == isset( $arrmixValues['auth_email'] ) )             $this->setAuthEmail( $arrmixValues['auth_email'] );
		if( true == isset( $arrmixValues['is_auth_email_required'] ) ) $this->setIsAuthEmailRequired( $arrmixValues['is_auth_email_required'] );
		return;
	}

	public function setCompetitorName( $strCompetiorName ) {
		$this->m_strCompetitorName = $strCompetiorName;
	}

	public function setCompanyAppId( $intCompanyAppId ) {
		$this->m_intCompanyAppId = $intCompanyAppId;
	}

	public function setAppinstalled( $intAppinstalled ) {
		$this->m_intAppinstalled = $intAppinstalled;
	}

	public function setClientId( $strClientId ) {
		$this->m_strClientId = $strClientId;
	}

	public function setClientSecret( $strClientSecret ) {
		$this->m_strClientSecret = $strClientSecret;
	}

	public function setAuthUrl( $strAuthUrl ) {
		$this->m_strAuthUrl = $strAuthUrl;
	}

	public function setUseApiThrottling( $boolUseApiThrottling ) {
		$this->m_boolUseApiThrottling = CStrings::strToBool( $boolUseApiThrottling );
	}

	public function setScopes( $arrobjScopes ) {
		$this->m_arrobjScopes = $arrobjScopes;
	}

	public function setPsProductName( $strPsProductName ) {
		$this->m_strPsProductName = $strPsProductName;
	}

	public function setContractStatusTypeId( $intContractStatusTypeId ) {
		$this->m_intContractStatusTypeId = $intContractStatusTypeId;
	}

	public function setRecurringAmount( $fltRecurringAmount ) {
		$this->m_fltRecurringAmount = $fltRecurringAmount;
	}

	public function setTitle( $strTitle ) {
		$this->m_strTitle = $strTitle;
	}

	public function setIsDocumentUploaded( $boolIsDocumentUploaded ) {
		$this->m_boolIsDocumentUploaded = $boolIsDocumentUploaded;
	}

	public function setIsTermsAndConditionsRequired( $boolIsTermsAndConditionsRequired ) {
		$this->m_boolIsTermsAndConditionsRequired = $boolIsTermsAndConditionsRequired;
	}

	public function setAuthEmail( $strAuthEmail ) {
		$this->m_strAuthEmail = CStrings::strTrimDef( $strAuthEmail, 240, NULL, true );
	}

	public function setIsAuthEmailRequired( $boolIsAuthEmailRequired ) {
		$this->m_boolIsAuthEmailRequired = CStrings::strToBool( $boolIsAuthEmailRequired );
	}

	public function setScopeIds( $arrintScopeIds ) {
		$this->m_arrintScopeIds = $arrintScopeIds;
	}

	/**
	 * Validate Function
	 */

	public function valCompetitorId( $objDatabase ) {
		$boolIsValid = true;

		if( false != valId( $this->getCompetitorId() ) ) {
			$intCount = \Psi\Eos\Admin\CCompetitors::createService()->fetchCompetitorCount( ' WHERE is_published = 1 AND id = ' . ( int ) $this->getCompetitorId(), $objDatabase );
			if( 0 == $intCount ) {
				$boolIsValid = false;
				$this->m_arrstrErrorMsg[] = 'Invalid competitor selected.';
			}
		}
		return $boolIsValid;
	}

	public function valPsProductId( $objDatabase ) {
		$boolIsValid = true;
		if( false != valId( $this->getPsProductId() ) && false == valId( $this->getId() ) ) {
			$intCount = \Psi\Eos\Admin\CPsProducts::createService()->fetchPsProductCount( ' WHERE is_published = 1 AND id = ' . ( int ) $this->getPsProductId(), $objDatabase );

			if( 0 == $intCount ) {
				$boolIsValid = false;
				$this->m_arrstrErrorMsg[] = 'Invalid Ps Product selected.';
			}
			$intCount = \Psi\Eos\Admin\CApps::createService()->fetchAppCount( ' WHERE ps_product_id = ' . ( int ) $this->getPsProductId(), $objDatabase );
			if( 0 != $intCount ) {
				$boolIsValid = false;
				$this->m_arrstrErrorMsg[] = 'The App is already exists with psProductId: ' . $this->getPsProductId() . '.';
			}
		}
		return $boolIsValid;
	}

	public function valScopeIds( $objDatabase ) {
		$boolIsValid = true;
		if( false != valArr( $this->getScopeIds() ) ) {
			$arrintInvalidScopes = [];
			$arrobjScopes = ( array ) \Psi\Eos\Admin\CScopes::createService()->fetchScopesByIds( $this->getScopeIds(), $objDatabase );
			if( false == valArr( $arrobjScopes ) ) {
				$arrintInvalidScopes = $this->getScopeIds();
			} else {
				$arrintInvalidScopes = array_diff( $this->getScopeIds(), array_keys( $arrobjScopes ) );
			}
			if( false != valArr( $arrintInvalidScopes ) ) {
				$boolIsValid = false;
				$this->m_arrstrErrorMsg[] = 'Invalid scopes selected: ' . implode( ',', $arrintInvalidScopes ) . '.';
			}
		}
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;
		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->m_arrstrRequiredFields[] = 'App Name';
		} else {
			$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
			$intCount = \Psi\Eos\Admin\CApps::createService()->fetchAppCount( ' WHERE lower( name )= \'' . pg_escape_string( \Psi\CStringService::singleton()->strtolower( $this->getName() ) ) . '\'' . $strSqlCondition, $objDatabase );
			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->m_arrstrErrorMsg[] = 'App name already exists. Duplicate app names are not allowed.';
			}
		}
		return $boolIsValid;
	}

	public function valCompanyName() {
		$boolIsValid = true;
		if( false == valStr( $this->getCompanyName() ) ) {
			$boolIsValid = false;
			$this->m_arrstrRequiredFields[] = 'Company Name';
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( false == valStr( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->m_arrstrRequiredFields[] = 'Description';
		}

		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;

		if( false == valStr( $this->getEmailAddress() ) ) {
			$boolIsValid = false;
			$this->m_arrstrRequiredFields[] = 'Support Email';

		} elseif( false == CValidation::validateEmailAddresses( $this->getEmailAddress() ) ) {
			$boolIsValid = false;
			$this->m_arrstrErrorMsg[] = 'Support Email does not appear to be valid.';
		}

		return $boolIsValid;
	}

	public function valMarketingUrl() {
		$boolIsValid = true;

		if( false == valStr( $this->getMarketingUrl() ) ) {
			$boolIsValid = false;
			$this->m_arrstrRequiredFields[] = 'Marketing url';
		} elseif( false == CValidation::checkUrl( $this->getMarketingUrl() ) ) {
			$boolIsValid = false;
			$this->m_arrstrErrorMsg[] = 'Marketing Url is not in correct format.';
		}
		return $boolIsValid;
	}

	public function valAuthUrl() {
		$boolIsValid = true;

		if( false == $this->getIsAuthEmailRequired() && false == valStr( $this->getAuthUrl() ) ) {
			$boolIsValid = false;
			$this->m_arrstrRequiredFields[] = 'App url';
		} elseif( false != valStr( $this->getAuthUrl() ) && false == CValidation::checkUrl( $this->getAuthUrl() ) ) {
			$boolIsValid = false;
			$this->m_arrstrErrorMsg[] = 'App Url is not in correct format.';
		}
		return $boolIsValid;
	}

	public function valDocumentUploaded() {
		$boolIsValid = true;

		if( true == $this->getIsTermsAndConditionsRequired() && false == $this->getIsDocumentUploaded() ) {
			$boolIsValid = false;
			$this->m_arrstrRequiredFields[] = 'Terms & Use document';
		}

		return $boolIsValid;
	}

	public function valAuthEmail() {
		$boolIsValid = true;
		if( true == $this->getIsAuthEmailRequired() && false == valStr( $this->getAuthEmail() ) ) {
			$boolIsValid = false;
			$this->m_arrstrRequiredFields[] = 'Auth email';
		} elseif( true == valStr( $this->getAuthEmail() ) && false == CValidation::validateEmailAddress( $this->getAuthEmail() ) ) {
			$boolIsValid = false;
			$this->m_arrstrErrorMsg[] = 'Auth Email does not appear to be valid.';
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCompanyName();
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valMarketingUrl();
				$boolIsValid &= $this->valAuthUrl();
				$boolIsValid &= $this->valAuthEmail();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valDocumentUploaded();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valCompanyName();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valMarketingUrl();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_api_insert':
				$boolIsValid &= $this->valCompanyName();
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valMarketingUrl();
				$boolIsValid &= $this->valAuthUrl();
				$boolIsValid &= $this->valAuthEmail();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valDocumentUploaded();
				$boolIsValid &= $this->valCompetitorId( $objDatabase );
				$boolIsValid &= $this->valPsProductId( $objDatabase );
				$boolIsValid &= $this->valScopeIds( $objDatabase );
				break;
			default:
				// default case
				$boolIsValid = true;
				break;
		}

		$strErrorMessage = '';
		if( false == $boolIsValid && false != valArr( $this->m_arrstrRequiredFields ) ) {
			$strErrorMessage = implode( ', ', $this->m_arrstrRequiredFields );
			$strErrorMessage .= ( \Psi\Libraries\UtilFunctions\count( $this->m_arrstrRequiredFields ) == 1 ) ? ' is required. ' : ' are required. ';
		}
		if( false != valArr( $this->m_arrstrErrorMsg ) ) {
			$strErrorMessage .= implode( ' ', $this->m_arrstrErrorMsg );
		}
		if( false != valStr( $strErrorMessage ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', $strErrorMessage ) );
		}

		return $boolIsValid;
	}

}
?>