<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CDistributionGroupTypes
 * Do not add any new functions to this class.
 */

class CDistributionGroupTypes extends CBaseDistributionGroupTypes {

	public static function fetchDistributionGroupTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDistributionGroupType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDistributionGroupType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDistributionGroupType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

   	public static function fetchAllDistributionGroupTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM distribution_group_types';
		return self::fetchDistributionGroupTypes( $strSql, $objDatabase );
	}
}
?>