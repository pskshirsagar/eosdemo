<?php

class CHelpResourceTopic extends CBaseHelpResourceTopic {

	protected $m_intModuleId;

	/**
	 * Get Functions
	 *
	 */

	public function getModuleId() {
		return $this->m_intModuleId;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setModuleId( $intModuleId ) {
		$this->m_intModuleId = $intModuleId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['module_id'] ) )	$this->setModuleId( $arrmixValues['module_id'] );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHelpResourceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHelpTopicId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPrimary() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRequiresSync() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}
		return $boolIsValid;
	}

}
?>