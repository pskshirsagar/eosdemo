<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPaymentTypes
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CPaymentTypes extends CBasePaymentTypes {

	use \Psi\Libraries\Container\TContainerized;

    public static function fetchPaymentTypeById( $intId, $objDatabase ) {
		$arrobjPaymentTypes = self::fetchPaymentTypesByIds( [ $intId ] );

		if( true == is_null( $arrobjPaymentTypes ) ) return NULL;

		if( 1 < \Psi\Libraries\UtilFunctions\count( $arrobjPaymentTypes ) ) {
			trigger_error( 'Invalid Payment Types Request:  Expecting one object and received two - CPaymentTypes::fetchPaymentTypeById', E_USER_ERROR );
		}

		return array_shift( $arrobjPaymentTypes );
	}

	public static function fetchElectronicPaymentTypes( $objDatabase = NULL ) {

		return self::fetchPaymentTypesByIds( [
			CPaymentType::ACH,
			CPaymentType::VISA,
			CPaymentType::MASTERCARD,
			CPaymentType::DISCOVER,
			CPaymentType::AMEX,
			CPaymentType::CHECK_21,
			CPaymentType::EMONEY_ORDER,
			CPaymentType::SEPA_DIRECT_DEBIT,
			CPaymentType::PAD
		] );
    }

    public static function fetchAllPaymentTypes( $objDatabase = NULL ) {

		return self::fetchPaymentTypesByIds( [
			CPaymentType::CASH,
			CPaymentType::CHECK,
			CPaymentType::MONEY_ORDER,
			CPaymentType::ACH,
			CPaymentType::VISA,
			CPaymentType::MASTERCARD,
			CPaymentType::DISCOVER,
			CPaymentType::AMEX,
			CPaymentType::CHECK_21,
			CPaymentType::EMONEY_ORDER,
		    CPaymentType::HAP,
			CPaymentType::BAH,
			CPaymentType::SEPA_DIRECT_DEBIT,
			CPaymentType::PAD,
			CPaymentType::AFT
		] );
    }

	public static function fetchPublishedPaymentTypes( $objDatabase = NULL ) {

		return self::fetchPaymentTypesByIds( [
			CPaymentType::CASH,
			CPaymentType::CHECK,
			CPaymentType::MONEY_ORDER,
			CPaymentType::ACH,
			CPaymentType::VISA,
			CPaymentType::MASTERCARD,
			CPaymentType::DISCOVER,
			CPaymentType::AMEX,
			CPaymentType::CHECK_21,
			CPaymentType::EMONEY_ORDER,
			CPaymentType::SEPA_DIRECT_DEBIT,
			CPaymentType::PAD,
			CPaymentType::AFT
		] );
	}

	public static function fetchPaymentTypesByIds( $arrintIds ) {
		if( false == valArr( $arrintIds ) || true == empty( $arrintIds ) ) {
			return NULL;
		}

		$arrobjPaymentTypes = [];

		if( in_array( CPaymentType::CHECK, $arrintIds ) ) {
			$objPaymentType = new CPaymentType();
			$objPaymentType->setId( CPaymentType::CHECK );
			$objPaymentType->setName( __( 'Check' ) );
			$objPaymentType->setDescription( 'Check payment' );
			$objPaymentType->setIsPublished( 1 );
			$objPaymentType->setOrderNum( CPaymentType::CHECK );

			$arrobjPaymentTypes[CPaymentType::CHECK] = $objPaymentType;
		}

		if( in_array( CPaymentType::MONEY_ORDER, $arrintIds ) ) {
			$objPaymentType = new CPaymentType();
			$objPaymentType->setId( CPaymentType::MONEY_ORDER );
			$objPaymentType->setName( __( 'Money Order' ) );
			$objPaymentType->setDescription( 'Money Order payment' );
			$objPaymentType->setIsPublished( 1 );
			$objPaymentType->setOrderNum( CPaymentType::MONEY_ORDER );

			$arrobjPaymentTypes[CPaymentType::MONEY_ORDER] = $objPaymentType;
		}

		if( in_array( CPaymentType::CASH, $arrintIds ) ) {
			$objPaymentType = new CPaymentType();
			$objPaymentType->setId( CPaymentType::CASH );
			$objPaymentType->setName( __( 'Cash' ) );
			$objPaymentType->setDescription( 'Cash payment' );
			$objPaymentType->setIsPublished( 1 );
			$objPaymentType->setOrderNum( CPaymentType::CASH );

			$arrobjPaymentTypes[CPaymentType::CASH] = $objPaymentType;
		}

		if( in_array( CPaymentType::ACH, $arrintIds ) ) {
			$objPaymentType = new CPaymentType();
			$objPaymentType->setId( CPaymentType::ACH );
			$objPaymentType->setName( __( 'eCheck' ) );
			$objPaymentType->setDescription( 'eCheck payment' );
			$objPaymentType->setIsPublished( 1 );
			$objPaymentType->setOrderNum( CPaymentType::ACH );

			$arrobjPaymentTypes[CPaymentType::ACH] = $objPaymentType;
		}

		if( in_array( CPaymentType::VISA, $arrintIds ) ) {
			$objPaymentType = new CPaymentType();
			$objPaymentType->setId( CPaymentType::VISA );
			$objPaymentType->setName( __( 'Visa' ) );
			$objPaymentType->setDescription( 'Visa payment' );
			$objPaymentType->setIsPublished( 1 );
			$objPaymentType->setOrderNum( CPaymentType::VISA );

			$arrobjPaymentTypes[CPaymentType::VISA] = $objPaymentType;
		}

		if( in_array( CPaymentType::MASTERCARD, $arrintIds ) ) {
			$objPaymentType = new CPaymentType();
			$objPaymentType->setId( CPaymentType::MASTERCARD );
			$objPaymentType->setName( __( 'Mastercard' ) );
			$objPaymentType->setDescription( 'Mastercard payment' );
			$objPaymentType->setIsPublished( 1 );
			$objPaymentType->setOrderNum( CPaymentType::MASTERCARD );

			$arrobjPaymentTypes[CPaymentType::MASTERCARD] = $objPaymentType;
		}

		if( in_array( CPaymentType::DISCOVER, $arrintIds ) ) {
			$objPaymentType = new CPaymentType();
			$objPaymentType->setId( CPaymentType::DISCOVER );
			$objPaymentType->setName( __( 'Discover' ) );
			$objPaymentType->setDescription( 'Discover payment' );
			$objPaymentType->setIsPublished( 1 );
			$objPaymentType->setOrderNum( CPaymentType::DISCOVER );

			$arrobjPaymentTypes[CPaymentType::DISCOVER] = $objPaymentType;
		}

		if( in_array( CPaymentType::AMEX, $arrintIds ) ) {
			$objPaymentType = new CPaymentType();
			$objPaymentType->setId( CPaymentType::AMEX );
			$objPaymentType->setName( __( 'American Express' ) );
			$objPaymentType->setDescription( 'American Express payment' );
			$objPaymentType->setIsPublished( 1 );
			$objPaymentType->setOrderNum( CPaymentType::AMEX );

			$arrobjPaymentTypes[CPaymentType::AMEX] = $objPaymentType;
		}

		if( in_array( CPaymentType::CHECK_21, $arrintIds ) ) {
			$objPaymentType = new CPaymentType();
			$objPaymentType->setId( CPaymentType::CHECK_21 );
			$objPaymentType->setName( __( 'Check 21' ) );
			$objPaymentType->setDescription( 'Check 21 Payment' );
			$objPaymentType->setIsPublished( 1 );
			$objPaymentType->setOrderNum( CPaymentType::CHECK_21 );

			$arrobjPaymentTypes[CPaymentType::CHECK_21] = $objPaymentType;
		}

		if( in_array( CPaymentType::EMONEY_ORDER, $arrintIds ) ) {
			$objPaymentType = new CPaymentType();
			$objPaymentType->setId( CPaymentType::EMONEY_ORDER );
			$objPaymentType->setName( __( 'E-Money Order' ) );
			$objPaymentType->setDescription( 'E-Money Order Payment' );
			$objPaymentType->setIsPublished( 1 );
			$objPaymentType->setOrderNum( CPaymentType::EMONEY_ORDER );

			$arrobjPaymentTypes[CPaymentType::EMONEY_ORDER] = $objPaymentType;
		}

		if( in_array( CPaymentType::HAP, $arrintIds ) ) {
			$objPaymentType = new CPaymentType();
			$objPaymentType->setId( CPaymentType::HAP );
			$objPaymentType->setName( __( 'HAP' ) );
			$objPaymentType->setDescription( 'HAP payment' );
			$objPaymentType->setIsPublished( 1 );
			$objPaymentType->setOrderNum( CPaymentType::HAP );

			$arrobjPaymentTypes[CPaymentType::HAP] = $objPaymentType;
		}

		if( in_array( CPaymentType::BAH, $arrintIds ) ) {
			$objPaymentType = new CPaymentType();
			$objPaymentType->setId( CPaymentType::BAH );
			$objPaymentType->setName( __( 'BAH Payment' ) );
			$objPaymentType->setDescription( 'BAH payment' );
			$objPaymentType->setIsPublished( 0 );
			$objPaymentType->setOrderNum( CPaymentType::BAH );

			$arrobjPaymentTypes[CPaymentType::BAH] = $objPaymentType;
		}

		if( in_array( CPaymentType::SEPA_DIRECT_DEBIT, $arrintIds ) ) {
			$objPaymentType = new CPaymentType();
			$objPaymentType->setId( CPaymentType::SEPA_DIRECT_DEBIT );
			$objPaymentType->setName( __( 'SEPA Direct Debit' ) );
			$objPaymentType->setDescription( __( 'SEPA Direct Debit' ) );
			$objPaymentType->setIsPublished( 1 );
			$objPaymentType->setOrderNum( CPaymentType::SEPA_DIRECT_DEBIT );

			$arrobjPaymentTypes[CPaymentType::SEPA_DIRECT_DEBIT] = $objPaymentType;
		}

		if( in_array( CPaymentType::PAD, $arrintIds ) ) {
			$objPaymentType = new CPaymentType();
			$objPaymentType->setId( CPaymentType::PAD );
			$objPaymentType->setName( CPaymentType::STR_PAD );
			$objPaymentType->setDescription( CPaymentType::STR_PAD );
			$objPaymentType->setIsPublished( 1 );
			$objPaymentType->setOrderNum( CPaymentType::PAD );

			$arrobjPaymentTypes[CPaymentType::PAD] = $objPaymentType;
		}

		if( in_array( CPaymentType::AFT, $arrintIds ) ) {
			$objPaymentType = new CPaymentType();
			$objPaymentType->setId( CPaymentType::AFT );
			$objPaymentType->setName( CPaymentType::STR_AFT );
			$objPaymentType->setDescription( CPaymentType::STR_AFT );
			$objPaymentType->setIsPublished( 1 );
			$objPaymentType->setOrderNum( CPaymentType::AFT );

			$arrobjPaymentTypes[CPaymentType::AFT] = $objPaymentType;
		}

		if( true == empty( $arrobjPaymentTypes ) ) return NULL;

		return $arrobjPaymentTypes;
	}

	public static function fetchSelectedPaymentTypes( $objDatabase ) {

		$arrintPaymentTypeIds = [
			CPaymentType::ACH,
			CPaymentType::CASH,
			CPaymentType::CHECK,
			CPaymentType::AMEX,
			CPaymentType::DISCOVER,
			CPaymentType::MASTERCARD,
			CPaymentType::VISA,
			\CPaymentType::PAD
		];

		return self::fetchPaymentTypesByIds( $arrintPaymentTypeIds, $objDatabase );
	}

	public function fetchCustomPaymentTypesByIds( $arrintIds, $objDatabase ) {
		$strSql = 'SELECT util_set_locale( \'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					SELECT
						id,
						initcap( util_get_system_translated( \'name\', name, details ) ) as name
					FROM
						payment_types
					WHERE
						id IN ( ' . implode( ',', $arrintIds ) . ')';

		return ( array ) fetchData( $strSql, $objDatabase );
	}

	/**
	 * Other Functions
	 */

	public function paymentTypeIdToLocalizedString( $intPaymentTypeId ) {
		$arrstrPaymentTypeNames = [
			CPaymentType::ACH				=> __( 'eCheck' ),
			CPaymentType::VISA				=> __( 'Visa' ),
			CPaymentType::MASTERCARD		=> __( 'MasterCard' ),
			CPaymentType::DISCOVER			=> __( 'Discover' ),
			CPaymentType::AMEX				=> __( 'American Express' ),
			CPaymentType::CHECK_21			=> __( 'Check 21' ),
			CPaymentType::EMONEY_ORDER		=> __( 'E-Money Order' ),
			CPaymentType::SEPA_DIRECT_DEBIT	=> __( 'SEPA Direct Debit' ),
			CPaymentType::PAD				=> __( 'Pre-Authorized Debit' ),
			CPaymentType::AFT				=> __( 'Canada Automated File Transfer' ),
		];
		return $arrstrPaymentTypeNames[$intPaymentTypeId] ?? '';
	}

	public static function paymentTypeIdToStr( $intPaymentTypeId ) {
		switch( $intPaymentTypeId ) {
			case CPaymentType::CASH:
				return 'Cash';
			case CPaymentType::CHECK:
				return 'Check';
			case CPaymentType::MONEY_ORDER:
				return 'Money Order';
			case CPaymentType::ACH:
				return 'eCheck';
			case CPaymentType::VISA:
				return 'Visa';
			case CPaymentType::MASTERCARD:
				return 'Mastercard';
			case CPaymentType::DISCOVER:
				return 'Discover';
			case CPaymentType::AMEX:
				return 'American Express';
			case CPaymentType::CHECK_21:
				return 'Check 21';
			case CPaymentType::EMONEY_ORDER:
				return 'E-money Order';
			case CPaymentType::HAP:
				return 'HAP';
			case CPaymentType::BAH:
				return 'BAH';
			case CPaymentType::SEPA_DIRECT_DEBIT:
				return __( 'SEPA Direct Debit' );
			case CPaymentType::PAD:
				return 'Pre-Authorized Debit';
			default:
				trigger_error( 'Invalid payment type id.', E_USER_WARNING );
				break;
		}
	}

	public static function strToPaymentTypeId( $strPaymentType ) {
		switch( $strPaymentType ) {
			case 'Cash':
				return CPaymentType::CASH;
			case 'Check':
				return CPaymentType::CHECK;
			case 'Money Order':
				return CPaymentType::MONEY_ORDER;
			case 'eCheck':
				return CPaymentType::ACH;
			case 'Visa':
				return CPaymentType::VISA;
			case 'Mastercard':
				return CPaymentType::MASTERCARD;
			case 'Discover':
				return CPaymentType::DISCOVER;
			case 'American Express':
				return CPaymentType::AMEX;
			case 'Check 21':
				return CPaymentType::CHECK_21;
			case 'E-Money Order':
				return CPaymentType::EMONEY_ORDER;
			case 'HAP Payment':
				return CPaymentType::HAP;
			case 'BAH Payment':
				return CPaymentType::BAH;
			case 'Pre-Authorized Debit':
				return CPaymentType::PAD;
			default:
				trigger_error( 'Invalid payment type.', E_USER_WARNING );
				break;
		}
	}

	public static function isAchPayment( $intPaymentTypeId ) {
		return CPaymentType::ACH == $intPaymentTypeId;
	}

	public static function isCreditCardPayment( $intPaymentTypeId ) {
		return in_array( $intPaymentTypeId, CPaymentType::$c_arrintCreditCardPaymentTypes, false );
	}

	public static function isCheck21Payment( $intPaymentTypeId ) {
		return CPaymentType::CHECK_21 == $intPaymentTypeId;
	}

	public static function isAftPayment( $intPaymentTypeId ) {
		return CPaymentType::AFT == $intPaymentTypeId;
	}

	public static function isElectronicPayment( $intPaymentTypeId ) {
		if( ( 4 <= $intPaymentTypeId && $intPaymentTypeId < 11 ) || CPaymentType::SEPA_DIRECT_DEBIT == $intPaymentTypeId || CPaymentType::PAD == $intPaymentTypeId || CPaymentType::AFT == $intPaymentTypeId ) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated doesn't seem to be in use.
	 */
	public static function loadPaymentTypeNames() {

		$arrstrComplianceStatusNames[CPaymentType::CASH]			= 'Cash';
		$arrstrComplianceStatusNames[CPaymentType::CHECK]			= 'Check';
		$arrstrComplianceStatusNames[CPaymentType::MONEY_ORDER]		= 'Money order';
		$arrstrComplianceStatusNames[CPaymentType::ACH]				= 'ACH(eCheck)';
		$arrstrComplianceStatusNames[CPaymentType::VISA]			= 'Visa';
		$arrstrComplianceStatusNames[CPaymentType::DISCOVER]		= 'Discover';
		$arrstrComplianceStatusNames[CPaymentType::MASTERCARD]		= 'Master card';
		$arrstrComplianceStatusNames[CPaymentType::AMEX]			= 'Amex';
		$arrstrComplianceStatusNames[CPaymentType::CHECK_21]		= 'Check 21';
		$arrstrComplianceStatusNames[CPaymentType::EMONEY_ORDER]	= 'E-Money order';
		$arrstrComplianceStatusNames[CPaymentType::HAP]	            = 'HAP';
		$arrstrComplianceStatusNames[CPaymentType::BAH]	            = 'BAH';
		$arrstrComplianceStatusNames[CPaymentType::PAD]	            = 'Pre-Authorized Debit';

		return $arrstrComplianceStatusNames;
	}

	public static function fetchPaymentTypeNameById( $intPaymentTypeId, $objDatabase ) {
		$strSql = 'SELECT
						name
					FROM
						payment_types
					WHERE
						id = ' . ( int ) $intPaymentTypeId;

		return fetchData( $strSql, $objDatabase );
	}

	/**
	 * Returns a list of supported CPaymentType IDs by country code
	 *
	 * @param $strCountryCode
	 * @return array
	 */
	public function getSupportedPaymentTypeIdsByCountryCode( $strCountryCode ) {
		$strKeyCountryCode = strtoupper( $strCountryCode );

		if( true == empty( $strCountryCode ) || false == array_key_exists( $strKeyCountryCode, \CPaymentType::$c_arrintPaymentTypesByCountryCode ) ) {
			return [];
		}

		return \CPaymentType::$c_arrintPaymentTypesByCountryCode[$strKeyCountryCode];
	}

}
?>