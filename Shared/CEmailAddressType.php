<?php

class CEmailAddressType extends CBaseEmailAddressType {
	const PRIMARY	= 1;
	const OTHER 	= 2;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getEmailAddressTypeNameByEmailAddressTypeId( $intEmailAddressTypeId ) {

		switch( $intEmailAddressTypeId ) {
			case self::PRIMARY:
				return __( 'Primary' );
				break;

			case self::OTHER:
				return __( 'Other' );
				break;

			default:
				return __( 'Other' );
				break;
		}
	}

}
?>