<?php

class CTableLog extends CBaseTableLog {

	protected $m_strTableName;
	protected $m_strUsername;
	protected $m_strCompanyName;
	protected $m_strPropertyName;
	protected $m_strWebsiteName;
	protected $m_strNameFirst;
	protected $m_strNameLast;

    /**
     * Get Functions
     */

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getWebsiteName() {
		return $this->m_strWebsiteName;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

    /**
     * Set Functions
     */

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['username'] ) ) $this->setUsername( $arrValues['username'] );
		if( true == isset( $arrValues['company_name'] ) ) $this->setCompanyName( $arrValues['company_name'] );
		if( true == isset( $arrValues['property_name'] ) ) $this->setPropertyName( $arrValues['property_name'] );
		if( true == isset( $arrValues['website_name'] ) ) $this->setWebsiteName( $arrValues['website_name'] );
		if( true == isset( $arrValues['name_first'] ) ) $this->setNameFirst( $arrValues['name_first'] );
		if( true == isset( $arrValues['name_last'] ) ) $this->setNameLast( $arrValues['name_last'] );
	}

	public function setUsername( $strUsername ) {
		$this->m_strUsername = $strUsername;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setWebsiteName( $strWebsiteName ) {
		$this->m_strWebsiteName = $strWebsiteName;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

    /**
     * Other Functions
     */

    public function insert( $intCurrentUserId, $objDatabase, $strTableName = NULL, $intReferenceNumber = NULL, $strAction = NULL, $intCid = NULL, $strOldData = NULL, $strNewData = NULL, $strDescription = NULL, $boolReturnSqlOnly = false ) {

    	$strSql 	= '';

		if( false == in_array( $strAction, [ 'INSERT', 'UPDATE', 'DELETE', 'SELECT' ] ) ) {
			trigger_error( 'Invalid action :' . $strAction, E_USER_WARNING );
			return ( true == $boolReturnSqlOnly ) ? $strSql : false;
		}

		$this->setTableName( $strTableName );
		$this->setReferenceNumber( $intReferenceNumber );
		$this->setAction( $strAction );
		$this->setCid( $intCid );
		$this->setDescription( $strDescription );
		$this->setOldData( $strOldData );
		$this->setNewData( $strNewData );

		if( true == $boolReturnSqlOnly ) {
			return $strSql .= parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return parent::insert( $intCurrentUserId, $objDatabase );
		}
    }

}
?>