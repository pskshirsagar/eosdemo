<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSubsidyIncomeLimitAreas
 * Do not add any new functions to this class.
 */

class CSubsidyIncomeLimitAreas extends CBaseSubsidyIncomeLimitAreas {

	public static function fetchSubsidyIncomeLimitArea( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CSubsidyIncomeLimitArea', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSubsidyIncomeLimitAreaIdBySubsidyIncomeLimitVersionIdByHmfaCode( $intSubsidyIncomeLimitVersionId, $strHmfaCode, $objDatabase ) {
		if( false == valStr( $strHmfaCode ) || false == valId( $intSubsidyIncomeLimitVersionId ) ) return NULL;

		$strSql = 'SELECT
						id
					FROM
						subsidy_income_limit_areas
					WHERE
						hmfa_code =  \'' . $strHmfaCode . '\'
						AND subsidy_income_limit_version_id = ' . ( int ) $intSubsidyIncomeLimitVersionId . '
					LIMIT 1';

		return self::fetchColumn( $strSql, 'id', $objDatabase );
	}

	public static function fetchMetroAreaNameBySubsidyIncomeLimitVersionIdByPropertyIdByCid( $intSubsidyIncomeLimitVersionId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intPropertyId ) || false == valId( $intSubsidyIncomeLimitVersionId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						sila.metro_area_name
					FROM
						property_subsidy_details psd
						JOIN subsidy_income_limit_areas sila ON ( sila.hmfa_code = psd.hmfa_code )
					WHERE
						psd.property_id = ' . ( int ) $intPropertyId . '
						AND psd.cid = ' . ( int ) $intCid . '
						AND sila.subsidy_income_limit_version_id = ' . ( int ) $intSubsidyIncomeLimitVersionId . '
						AND psd.hmfa_code IS NOT NULL
					LIMIT 1';

		return self::fetchColumn( $strSql, 'metro_area_name', $objDatabase );
	}

	// @TODO: Check with Jim if this is the right way to fetch income limit area
	// @TODO: Need to check if $boolIsHera and $intSubsidyIncomeVersionId conditions are correct

	public static function fetchSubsidyIncomeLimitAreaByPropertyIdByCid( $boolIsHera = false, $intSubsidyIncomeVersionId, $intPropertyId, $intCid, $objClientDatabase ) {

		$strHeraCondition = ( false == $boolIsHera ) ? ' NOT sil.is_hera ' : ' sil.is_hera ';

		$strSql = 'SELECT
						sila.*
					FROM
						subsidy_income_limit_areas sila
						JOIN property_subsidy_details psd ON ( psd.hmfa_code = sila.hmfa_code )
						JOIN subsidy_income_limit_versions silv ON ( silv.id = sila.subsidy_income_limit_version_id )
						JOIN( SELECT DISTINCT subsidy_income_limit_area_id,is_hera FROM subsidy_income_limits ) sil ON( sil.subsidy_income_limit_area_id = sila.id )
					WHERE
						sila.subsidy_income_limit_version_id = ' . ( int ) $intSubsidyIncomeVersionId . '
						AND ' . $strHeraCondition . '
						AND psd.cid = ' . ( int ) $intCid . '
						AND psd.property_id = ' . ( int ) $intPropertyId . '
						AND NOW () BETWEEN silv.effective_date AND silv.effective_through_date;';

		// Fetching parent as we don't want to use cachedObjects from self::fetchSubsidyIncomeLimitArea()
		return parent::fetchSubsidyIncomeLimitArea( $strSql, $objClientDatabase );

	}

	public static function fetchAllMetroAreasBySubsidyIncomeLimitVersionId( $intSubsidyIncomeLimitVersionId, $objClientDatabase ) {

		$strSql = 'SELECT
						sila.hmfa_code,
						sila.metro_area_name
		 			FROM
						subsidy_income_limit_areas sila
					WHERE
						sila.subsidy_income_limit_version_id = ' . ( int ) $intSubsidyIncomeLimitVersionId . '
					ORDER BY metro_area_name ASC';

		$arrmixResultData = fetchData( $strSql, $objClientDatabase );
		$arrmixUserData = [];

		if( true == valArr( $arrmixResultData ) ) {
			foreach( $arrmixResultData as $arrmixRecord ) {
				$arrmixUserData[$arrmixRecord['hmfa_code']] = $arrmixRecord['metro_area_name'];
			}
		}
		return $arrmixUserData;

	}

	public static function fetchIncomeLimitAreas( $boolIsHera = false, $intSubsidyTypeId, $intPropertyId, $intClientId, $intSubsidyIncomeVersionId, $objClientDatabase, $boolIsTaxCredit = false, $boolIsCustomIncomeLimit = false ) {
		if( false == valId( $intPropertyId ) || false == valId( $intClientId ) || false == valId( $intSubsidyIncomeVersionId ) ) return NULL;

		$strIsHera = ( false == $boolIsHera ) ? 'false' : 'true';

		$strForTaxCredit = '';

		If( true == $boolIsTaxCredit ) {
			$strForTaxCredit .= ' AND ( silt.percent = ' . CSubsidyIncomeLevelType::INCOME_LIMIT_VERY_LOW . ' OR silt.percent = ' . CSubsidyIncomeLevelType::INCOME_LIMIT_SIXTY_PERCENT . ' ) ';
		}

		if( false == $boolIsCustomIncomeLimit ) {
			$strForIncomeLimitArea = 'AND sila.subsidy_income_limit_version_id = sil.subsidy_income_limit_version_id ';
		}

		$strSql = 'SELECT
						sil.family_size,
						sil.income_limit,
						silt.percent
					FROM
						subsidy_income_limits sil
						JOIN subsidy_income_limit_areas sila ON ( sila.id = sil.subsidy_income_limit_area_id ' . $strForIncomeLimitArea . ' )
						JOIN property_subsidy_details psd ON ( psd.hmfa_code = sila.hmfa_code )
						JOIN subsidy_income_level_types silt ON ( silt.id = sil.subsidy_income_level_type_id )
						JOIN subsidy_income_limit_versions silv ON ( silv.id = sil.subsidy_income_limit_version_id )
					WHERE
						sil.subsidy_income_limit_version_id = ' . ( int ) $intSubsidyIncomeVersionId . '
						AND sil.is_hera = ' . ( string ) $strIsHera . '
						AND sil.subsidy_type_id = ' . ( int ) $intSubsidyTypeId . '
						AND psd.property_id = ' . ( int ) $intPropertyId . '
						AND psd.cid = ' . ( int ) $intClientId . $strForTaxCredit . '
					ORDER BY
						sil.family_size';

		$arrmixIncomeLimits = ( array ) fetchData( $strSql, $objClientDatabase );

		$arrmixKeyedIncomeLimits = [];

		foreach( $arrmixIncomeLimits as $arrmixIncomeLimit ) {
			$arrmixKeyedIncomeLimits[$arrmixIncomeLimit['percent']][$arrmixIncomeLimit['family_size']] = $arrmixIncomeLimit['income_limit'];
		}

		return $arrmixKeyedIncomeLimits;
	}

	public static function fetchHoldHarmlessIncomeLimitAreas( $boolIsHera = false, $intSubsidyTypeId, $intPropertyId, $intClientId, $intSubsidyIncomeVersionId, $objClientDatabase ) {
		if( false == valId( $intPropertyId ) || false == valId( $intClientId ) ) return NULL;

		$strHeraCondition = ( false == $boolIsHera ) ? ' NOT sil.is_hera ' : ' sil.is_hera ';
		$strFederalCondition = ( true == $boolIsHera ) ? ' AND cid = ' . CClient::ID_DEFAULT : '';

		$strSql = 'SELECT
						psa.property_building_id,
						pb.building_name,
						psa.placed_in_service_date,
						sil.family_size,
						silt.percent,
						sil.income_limit,
						CASE
							WHEN COALESCE( pst.subsidy_income_limit_version_id, 0 ) = sil.subsidy_income_limit_version_id
							THEN 1
							ELSE 0
						END AS same_version_as_property,
						ROW_NUMBER() OVER ( PARTITION BY sil.family_size, silt.percent, psa.property_building_id ORDER BY sil.income_limit DESC, sil.subsidy_income_limit_version_id DESC ) AS row_num
					FROM
						property_subsidy_details psd
						JOIN property_subsidy_types pst ON ( pst.cid = psd.cid AND pst.property_id = psd.property_id AND pst.deleted_on IS NULL AND pst.subsidy_type_id = 2 )
						JOIN subsidy_types st ON ( st.id = pst.subsidy_type_id )
						JOIN set_asides psa ON ( psa.cid = psd.cid AND psa.property_id = psd.property_id AND psa.deleted_on IS NULL )
						LEFT JOIN property_buildings pb ON ( pb.cid = psa.cid AND pb.property_id = psa.property_id AND pb.id = COALESCE( psa.property_building_id, 0 ) )
						JOIN LATERAL (
							SELECT
							*
							FROM
								subsidy_income_limit_versions silv
							WHERE
								silv.effective_date <= COALESCE( ( SELECT effective_date FROM subsidy_income_limit_versions WHERE id = ' . ( int ) $intSubsidyIncomeVersionId . $strFederalCondition . ' ), psa.placed_in_service_date )
						) silv ON ( TRUE )
													
						JOIN subsidy_income_limit_areas sila ON ( sila.hmfa_code = psd.hmfa_code AND sila.subsidy_income_limit_version_id = silv.id )
						JOIN LATERAL (
							SELECT
								COALESCE( sil.family_size, sil2.family_size ) AS family_size,
								COALESCE( sil.is_hera, sil2.is_hera ) AS is_hera,
								sil.id IS NULL AS is_calculated_from_50,
								COALESCE( sil.income_limit, sil2.income_limit / 0.5 * psa.median_income_percent ) AS income_limit,
								sil2.subsidy_income_limit_area_id,
								sil2.subsidy_income_limit_version_id,
								sil.subsidy_income_level_type_id
							FROM
								subsidy_income_limits sil2
								LEFT JOIN subsidy_income_limits sil ON ( sil.subsidy_income_limit_version_id = sil2.subsidy_income_limit_version_id
																			AND sil.subsidy_income_limit_area_id = sil2.subsidy_income_limit_area_id
																			AND sil.is_hera = sil2.is_hera
																			AND sil.family_size = sil2.family_size )
							WHERE
								sil2.subsidy_income_limit_version_id = silv.id
								AND sil2.subsidy_income_limit_area_id = sila.id
								AND sil2.subsidy_income_level_type_id = ' . CSubsidyIncomeLevelType::VERY_LOW . '
								AND sil.subsidy_type_id = ' . ( int ) $intSubsidyTypeId . '
								AND ' . $strHeraCondition . '
						) sil ON ( TRUE )
						JOIN subsidy_income_level_types silt ON ( silt.id = sil.subsidy_income_level_type_id )
					WHERE
						psd.cid = ' . ( int ) $intClientId . '
						AND psd.property_id = ' . ( int ) $intPropertyId . '
						AND CASE
								WHEN psd.is_tax_credit_multiple_building_project 
								THEN psa.property_building_id IS NOT NULL
								ELSE psa.property_building_id IS NULL
							END
						AND sil.family_size IS NOT NULL
					ORDER BY
						pb.building_name,
						psa.property_building_id,
						sil.family_size,
						psa.median_income_percent,
						sil.income_limit';

		$strSql = 'SELECT
						property_building_id,
						building_name,
						placed_in_service_date,
						family_size,
						income_limit,
						percent,
						same_version_as_property
					FROM
						( ' . $strSql . ' ) as SubQ
					WHERE
						row_num = 1
						AND ( percent = ' . CSubsidyIncomeLevelType::INCOME_LIMIT_VERY_LOW . ' OR percent =  ' . CSubsidyIncomeLevelType::INCOME_LIMIT_SIXTY_PERCENT . ' )';

		$arrmixIncomeLimits = ( array ) fetchData( $strSql, $objClientDatabase );

		$arrmixKeyedIncomeLimits = [];

		foreach( $arrmixIncomeLimits as $arrmixIncomeLimit ) {

			if( true == valId( $arrmixIncomeLimit['property_building_id'] ) ) {

				$arrmixKeyedIncomeLimits[$arrmixIncomeLimit['property_building_id']]['building_name']			= $arrmixIncomeLimit['building_name'];
				$arrmixKeyedIncomeLimits[$arrmixIncomeLimit['property_building_id']]['placed_in_service_date']	= $arrmixIncomeLimit['placed_in_service_date'];

				$arrmixKeyedIncomeLimits[$arrmixIncomeLimit['property_building_id']]['data'][$arrmixIncomeLimit['percent']][$arrmixIncomeLimit['family_size']] = [ 'income_limit'	=> $arrmixIncomeLimit['income_limit'],  'same_version_as_property'	=> $arrmixIncomeLimit['same_version_as_property'] ];
			} else {

				$arrmixKeyedIncomeLimits[$arrmixIncomeLimit['percent']][$arrmixIncomeLimit['family_size']] = [ 'income_limit'	=> $arrmixIncomeLimit['income_limit'], 'same_version_as_property'	=> $arrmixIncomeLimit['same_version_as_property'], 'property_building_id'		=> $arrmixIncomeLimit['property_building_id'], 'building_name'				=> $arrmixIncomeLimit['building_name'], 'placed_in_service_date'	=> $arrmixIncomeLimit['placed_in_service_date'] ];
			}
		}

		return $arrmixKeyedIncomeLimits;
	}

	public static function fetchSubsidyIncomeLimitAreaByPropertyIdsByCid( $boolIsHera = false, $intSubsidyIncomeVersionId, $arrintPropertyIds, $intCid, $objClientDatabase ) {

		$strHeraCondition = ( false == $boolIsHera ) ? ' NOT sila.is_hera ' : ' sila.is_hera ';

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						sila.*
					FROM
						subsidy_income_limit_areas sila
						JOIN property_subsidy_details psd ON ( psd.hmfa_code = sila.hmfa_code )
						JOIN subsidy_income_limit_versions silv ON ( silv.id = sila.subsidy_income_limit_version_id )
					WHERE
						sila.subsidy_income_limit_version_id = ' . ( int ) $intSubsidyIncomeVersionId . '
						AND ' . $strHeraCondition . '
						AND psd.cid = ' . ( int ) $intCid . '
						AND psd.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND NOW ( ) BETWEEN silv.effective_date AND silv.effective_through_date;';

		// Fetching parent as we don't want to use cachedObjects from self::fetchSubsidyIncomeLimitArea()
		return parent::fetchSubsidyIncomeLimitArea( $strSql, $objClientDatabase );

	}

	public static function fetchSubsidyIncomeLimitAreaByPropertyIdByCidByEffectiveDate( $intPropertyId, $intClientId, $objDatabase, $strEffectiveDate = NULL ) {

		if( false == valId( $intPropertyId ) || false == valId( $intClientId ) ) return NULL;

		$strEffectiveDate = ( true == valStr( $strEffectiveDate ) ) ? $strEffectiveDate : date( 'm/d/Y' );

		$strSql = 'SELECT
						sila.*
					FROM
						subsidy_income_limit_areas sila
						JOIN property_subsidy_details psd ON ( psd.hmfa_code = sila.hmfa_code )
						JOIN subsidy_income_limit_versions silv ON ( silv.id = sila.subsidy_income_limit_version_id AND silv.cid = 1 AND silv.property_id IS NULL )
					WHERE
						psd.cid = ' . ( int ) $intClientId . '
						AND psd.property_id = ' . ( int ) $intPropertyId . '
						AND \'' . $strEffectiveDate . '\' BETWEEN silv.effective_date AND silv.effective_through_date
					ORDER BY
						silv.effective_date DESC
					LIMIT 1';

		return parent::fetchSubsidyIncomeLimitArea( $strSql, $objDatabase );
	}

	public static function fetchMetroAreaNameByHmfaCode( $strHmfaCode, $objDatabase ) {
		$strSql = 'SELECT
						metro_area_name
					FROM
						subsidy_income_limit_areas
					WHERE
						hmfa_code =  \'' . $strHmfaCode . '\'
					ORDER BY
						id DESC
					LIMIT 1';

		$arrmixData = fetchData( $strSql, $objDatabase );
		return ( true == valArr( $arrmixData ) ) ? $arrmixData[0]['metro_area_name'] : NULL;
	}

}
?>