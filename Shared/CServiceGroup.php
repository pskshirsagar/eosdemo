<?php

class CServiceGroup extends CBaseServiceGroup {

	const OAUTH             = 20;
	const RESIDENT_PAYMENTS = 32;
	const ILS               = 33;
	const REPORT            = 38;
	const RESIDENT_PORTAL   = 22;
	const QUEUE             = 135;

	protected $m_strServiceGroupTypeName;
	public $m_strMajorApiVersion;

    public function __construct() {
        parent::__construct();
	    // FIXME remove this static declaration once UI implemented at CA which allows user to select specific version.
	    $this->m_intMajorApiVersionId = CApiVersion::TMP_DEFAULT_MAJOR_API_VERSION;

        return;
    }

    public function getServiceGroupTypeName() {
    	return $this->m_strServiceGroupTypeName;
    }

	public function getMajorApiVersion() {
		return $this->m_strMajorApiVersion;
	}

    public function setServiceGroupTypeName( $strServiceGroupTypeName ) {
    	$this->m_strServiceGroupTypeName = $strServiceGroupTypeName;
    }

    public function setSystemCode( $strSystemCode ) {
	    parent::setSystemCode( \Psi\CStringService::singleton()->strtolower( $strSystemCode ) );
    }

	public function setMajorApiVersion( $strMajorApiVersion ) {
		$this->m_strMajorApiVersion = $strMajorApiVersion;
	}

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['service_group_type_name'] ) ) {
		    $this->setServiceGroupTypeName( $arrmixValues['service_group_type_name'] );
	    }
	    if( false != isset( $arrmixValues['major_api_version'] ) ) {
    		$this->setMajorApiVersion( $arrmixValues['major_api_version'] );
	    }
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valServiceGroupTypeId() {
		$boolIsValid = true;
		if( false == is_numeric( $this->getServiceGroupTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'service_group_type_id', 'Group type is required.' ) );
		}
		return $boolIsValid;
	}

	public function valSystemCode( $objDatabase ) {
		$boolIsValid = true;
		if( false == valStr( $this->getSystemCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_code', 'Code is required.' ) );
		} elseif( false == ctype_alpha( $this->getSystemCode() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_code', 'Code must contain only alphabets in lowercase.' ) );
		} elseif( false != valObj( $objDatabase, 'CDatabase' ) && false != is_numeric( $this->getServiceGroupTypeId() ) ) {
			$strWhere = ' WHERE lower(system_code)= \'' . \Psi\CStringService::singleton()->strtolower( $this->getSystemCode() ) . '\' AND service_group_type_id = ' . ( int ) $this->getServiceGroupTypeId() . ' AND major_api_version_id = ' . ( int ) $this->getMajorApiVersionId();
			$strWhere .= ( 0 < $this->getId() ) ? ' AND id NOT IN (' . $this->getId() . ')' : '';
			$intCount = \Psi\Eos\Entrata\CServiceGroups::createService()->fetchServiceGroupCount( $strWhere, $objDatabase );
			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_code', 'This code already exists.' ) );
			}
		}

		return $boolIsValid;
	}

    public function valName() {
        $boolIsValid = true;
        if( false == valStr( $this->getName() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'group_name', 'Group name is required.' ) );
        }

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName();
            	$boolIsValid &= $this->valServiceGroupTypeId();
            	$boolIsValid &= $this->valSystemCode( $objDatabase );
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid = true;
                break;

            default:
            	// default case
            	$boolIsValid = true;
                break;
        }

        return $boolIsValid;
    }

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$strSql = rtrim( parent::delete( $intCurrentUserId, $objDatabase, true ), ';' );
		$strSql .= ' AND major_api_version_id = ' . ( int ) $this->sqlMajorApiVersionId() . ';';
		return ( true == $boolReturnSqlOnly ) ? $strSql : $this->executeSql( $strSql, $this, $objDatabase );
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$strSql = parent::update( $intCurrentUserId, $objDatabase, true );
		if( false == valStr( $strSql ) ) {
			return false;
		}
		$strSql = rtrim( $strSql, ';' );

		$strSql .= ' AND major_api_version_id = ' . ( int ) $this->sqlMajorApiVersionId() . ';';
		return ( true == $boolReturnSqlOnly ) ? $strSql : $this->executeSql( $strSql, $this, $objDatabase );
	}

}
?>