<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSubsidyIncomeLimitVersions
 * Do not add any new functions to this class.
 */

class CSubsidyIncomeLimitVersions extends CBaseSubsidyIncomeLimitVersions {

	public static function fetchPublishedLatestSubsidyIncomeLimitVersionIdByCid( $intCid, $objDatabase ) {
		if( false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						silv.id
					FROM
						subsidy_income_limit_versions silv
					WHERE
						silv.cid = ' . ( int ) $intCid . '
						AND silv.effective_date < NOW()
						AND silv.is_published = TRUE
					ORDER BY
						silv.year_published DESC
					LIMIT 1';

		return self::fetchColumn( $strSql, 'id', $objDatabase );
	}

	public static function fetchPublishedSubsidyIncomeLimitVersionIdByEffectiveDateByCid( $strEffectiveDate, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) ) {
			return NULL;
		}
		if( false == valStr( $strEffectiveDate ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						silv.id
					FROM
						subsidy_income_limit_versions silv
					WHERE
						\'' . $strEffectiveDate . '\' BETWEEN silv.effective_date AND silv.effective_through_date
						AND silv.cid = ' . ( int ) $intCid . '
						AND silv.is_published = TRUE
					LIMIT 1';

		return self::fetchColumn( $strSql, 'id', $objDatabase );
	}

	public static function fetchSubsidyIncomeLimitVersionsByCidByPropertyIdBySubsidyTypeIds( $intCid, $intPropertyId, $arrintSubsidyTypeIds, $objDatabase ) {
		if( false == valId( $intCid ) ) {
			return NULL;
		}
		if( false == valId( $intPropertyId ) ) {
			return NULL;
		}
		if( false == valArr( $arrintSubsidyTypeIds ) ) {
			return NULL;
		}

		$strSql = 'WITH income_limit_versions AS ( SELECT
							silv.*,
							st.id AS subsidy_type_id,
							rank() over ( PARTITION BY pst.subsidy_type_id ORDER BY pst.id DESC, silv.effective_date DESC )
						FROM
							subsidy_income_limit_versions AS silv
							LEFT JOIN subsidy_types st ON ( st.id IN ( ' . implode( $arrintSubsidyTypeIds, ',' ) . ' ) )
							LEFT JOIN property_subsidy_types AS pst ON ( pst.cid = ' . ( int ) $intCid . ' AND pst.property_id = ' . ( int ) $intPropertyId . ' AND pst.subsidy_type_id  = st.id  AND pst.deleted_by IS NULL )
						WHERE
							( CASE
								WHEN ( pst.subsidy_type_id = 2 AND pst.subsidy_income_limit_version_id IS NOT NULL)
									THEN silv.id = pst.subsidy_income_limit_version_id
								ELSE silv.effective_date <= NOW ( )
									AND silv.cid = 1
									AND silv.property_id IS NULL
							END )
							AND silv.is_published = TRUE
					)
						SELECT *
						FROM income_limit_versions
						WHERE income_limit_versions.rank = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchIncomeLimitVersionDetailsByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) || false == valArr( $arrintPropertyGroupIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
                        silv1.year_published,
                        silv1.since_days,
                        silv1.latest_subsidy_income_limit_version_id,
                        string_agg( CASE WHEN silv2.id IS NOT NULL THEN pst.property_id::TEXT END, \',\' ) AS property_ids,
                        string_agg( CASE WHEN silv2.id IS NOT NULL THEN silv2.effective_date::TEXT END, \',\' ) AS effective_dates
                    FROM
                        property_subsidy_types pst
                        JOIN load_affordable_properties( NULL, array [ ' . ( int ) $intCid . ' ], TRUE, array [ ' . CSubsidyType::TAX_CREDIT . ',' . CSubsidyType::HUD . ' ] ) lap ON ( lap.property_id = pst.property_id AND lap.cid = pst.cid )
                        LEFT JOIN subsidy_income_limit_versions silv2 ON ( pst.subsidy_income_limit_version_id = silv2.id AND silv2.cid = ' . CClient::ID_DEFAULT . ' AND silv2.property_id IS NULL )
                        JOIN (
                          SELECT
                              silv.id AS latest_subsidy_income_limit_version_id,
                              silv.year_published,
                              DATE_PART( \'day\', NOW( ) -( silv.effective_date )
                                  ::TIMESTAMP ) AS since_days
                          FROM
                              subsidy_income_limit_versions silv
                          WHERE
                              silv.is_published
                              AND CURRENT_DATE BETWEEN silv.effective_date AND silv.effective_through_date 
                              AND silv.cid = ' . CClient::ID_DEFAULT . '
                              AND silv.property_id IS NULL
                          ORDER BY
                              silv.id DESC
                          LIMIT 1
                        ) AS silv1 ON ( TRUE )
                    WHERE
                        pst.subsidy_type_id IN (' . CSubsidyType::TAX_CREDIT . ',' . CSubsidyType::HUD . ')
                        AND silv1.latest_subsidy_income_limit_version_id != pst.subsidy_income_limit_version_id
                        AND pst.deleted_on IS NULL
                        AND pst.cid = ' . ( int ) $intCid . '
                    GROUP BY
                        silv1.year_published,
                        silv1.since_days,
                        silv1.latest_subsidy_income_limit_version_id';

		$arrstrIncomeLimitVersion = ( array ) fetchData( $strSql, $objDatabase );

		return $arrstrIncomeLimitVersion[0];
	}

	public static function fetchAllPublishedSubsidyIncomeLimitVersionsByCid( $intCid, $objDatabase ) {
		if( false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						silv.*
					FROM
						subsidy_income_limit_versions silv
					WHERE
						silv.cid = ' . ( int ) $intCid . '
						AND silv.is_published
					ORDER BY
						silv.effective_date DESC';

		return self::fetchSubsidyIncomeLimitVersions( $strSql, $objDatabase );
	}

	public static function fetchSubsidyIncomeLimitVersionIdByYearPublishedByCid( $intYearPublished, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						silv.id
					FROM
						subsidy_income_limit_versions silv
					WHERE
						silv.cid = ' . ( int ) $intCid . '
						AND silv. year_published = ' . ( int ) $intYearPublished . '
						AND silv.is_published = TRUE
					LIMIT 1';

		return self::fetchColumn( $strSql, 'id', $objDatabase );
	}

	public static function fetchSubsidyIncomeLimitVersionIdByYearPublishedByPropertyIdByCid( $strYearPublished, $intPropertyId = NULL, $intCid = 1, $objDatabase ) {

		$strSql = 'SELECT
						silv.id AS subsidy_income_limit_version_id
					FROM
						subsidy_income_limit_versions silv
					WHERE
						silv.cid = ' . ( int ) $intCid . '
						AND silv.property_id = ' . ( int ) $intPropertyId . '
						AND silv.year_published = ' . $strYearPublished . '
						AND silv.is_published = TRUE';

		return self::fetchColumn( $strSql, 'subsidy_income_limit_version_id', $objDatabase );
	}

	public static function fetchPublishedLatestFederalSubsidyIncomeLimitVersionDetails( $intCid = 1, $objDatabase ) {

		$strSql = 'SELECT
						silv.*
					FROM
						subsidy_income_limit_versions silv
					WHERE
						silv.cid = ' . ( int ) $intCid . '
						AND silv.property_id IS NULL
						AND silv.effective_date < NOW()
						AND silv.is_published = TRUE
					ORDER BY
						silv.year_published DESC
					LIMIT 1';

		return self::fetchSubsidyIncomeLimitVersion( $strSql, $objDatabase );
	}

	public static function fetchPublishedLatestCustomOrFederalSubsidyIncomeLimitVersionIdByPropertyIdByCid( $intCid, $intPropertyId, $strEffectiveDate, $objDatabase ) {

		if( false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
				     *
				 FROM
				     subsidy_income_limit_versions
				 WHERE
				     \'' . $strEffectiveDate . '\'  BETWEEN effective_date  AND effective_through_date 
				     AND ( CID = ' . $intCid . ' AND property_id = ' . ( int ) $intPropertyId . ' )
				     OR ( CID = ' . CClient::ID_DEFAULT . ' AND property_id IS NULL )
				     AND is_published
				 ORDER BY
				     property_id NULLS LAST,
				     effective_date DESC
				 LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLatestCustomSubsidyIncomeLimitVersionByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					    silv.*
					FROM
					    subsidy_income_limit_versions silv
					    JOIN property_subsidy_details psd ON ( silv.cid = psd.cid AND silv.property_id = psd.property_id )
					    JOIN subsidy_income_limit_areas sila ON ( sila.hmfa_code = psd.hmfa_code AND silv.id = sila.subsidy_income_limit_version_id )
					WHERE
					    silv.is_published = TRUE
					    AND silv.property_id = ' . ( int ) $intPropertyId . '
					    AND silv.cid = ' . ( int ) $intCid . '
					    AND NOW() BETWEEN silv.effective_date AND silv.effective_through_date
					ORDER BY
					    silv.effective_date DESC
					LIMIT 1';

		return parent::fetchSubsidyIncomeLimitVersion( $strSql, $objDatabase );

	}

	public static function fetchSubsidyIncomeLimitVersionByIdByCid( $intId, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intId ) ) return NULL;

		$strSql = 'SELECT
						silv.*
					FROM
						subsidy_income_limit_versions silv
					WHERE
						silv.id = ' . ( int ) $intId . '
						AND silv.cid = ' . ( int ) $intCid . '
						AND silv.is_published';

		return self::fetchSubsidyIncomeLimitVersion( $strSql, $objDatabase );
	}

	public static function fetchCustomSubsidyIncomeLimitVersionsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $strHmfaCode = NULL ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;
		$strOrderByClause = ( true == valStr( $strHmfaCode ) ) ? ' sila.hmfa_code = \'' . $strHmfaCode . '\' DESC, ' : '';

		$strSql = 'SELECT
                        silv.id,
                        silv.cid,
                        silv.property_id,
                        silv.effective_date,
                        silv.effective_through_date,
                        sila.id as subsidy_income_limit_area_id,
                        sila.metro_area_name,
                        sila.hmfa_code,
                        pst.subsidy_income_limit_version_id as active_income_limit_version_id                        
                    FROM
                        subsidy_income_limit_versions silv
                        JOIN subsidy_income_limit_areas sila ON ( sila.subsidy_income_limit_version_id = silv.id )
                        LEFT JOIN property_subsidy_types pst ON pst.property_id = silv.property_id AND pst.cid = silv.cid AND pst.deleted_by IS NULL AND pst.deleted_on IS NULL AND pst.subsidy_type_id = ' . CSubsidyType::TAX_CREDIT . '
                    WHERE
                        silv.cid = ' . ( int ) $intCid . '
                        AND silv.property_id = ' . ( int ) $intPropertyId . '
                        AND silv.is_published IS TRUE                        
                    ORDER BY
                        ' . $strOrderByClause . ' silv.effective_date DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomSubsidyIncomeLimitVersionsByHmfaCodeByPropertyIdByCid( $strHmfaCode, $intPropertyId, $intCid, $objDatabase, $strEffectiveDate = NULL, $strEffectiveThroughDate = NULL ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valStr( $strHmfaCode ) ) return NULL;

		$strDateCondition = '';
		if( true == valStr( $strEffectiveDate ) ) {
			$strEffectiveThroughDate = ( true == valStr( $strEffectiveThroughDate ) ) ? $strEffectiveThroughDate : '12/31/2099';
			$strDateCondition = ' AND ( \'' . $strEffectiveDate . '\'  BETWEEN silv.effective_date  AND silv.effective_through_date OR \'' . $strEffectiveThroughDate . '\'  BETWEEN silv.effective_date AND silv.effective_through_date ) ';
		}

		$strSql = 'SELECT
					    silv.*
					FROM
					    subsidy_income_limit_versions silv
					    JOIN subsidy_income_limit_areas sila ON ( sila.subsidy_income_limit_version_id = silv.id )
					WHERE
					    silv. property_id = ' . ( int ) $intPropertyId . '
					    AND silv.CID = ' . ( int ) $intCid . $strDateCondition . '
					    AND sila. hmfa_code = \'' . $strHmfaCode . '\'
					    AND silv.is_published IS TRUE
					    AND is_hera IS FALSE
					 ORDER BY
					    silv.effective_date';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomSubsidyIncomeLimitVersionIdsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    silv.id
					FROM
					    subsidy_income_limit_versions silv
					WHERE
					    silv. property_id IS NOT NULL
					    AND silv.cid = ' . ( int ) $intCid . '
					    AND silv.is_published IS TRUE';

		$arrintIncomeLimitVersions = fetchData( $strSql, $objDatabase );

		return array_column( $arrintIncomeLimitVersions, 'id' );
	}

	public static function fetchCustomSubsidyIncomeLimitVersionByIdByPropertyIdByCid( $intId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valId( $intId ) ) return NULL;

		$strSql = 'SELECT
					    silv.*,
					    sila.hmfa_code
					FROM
					    subsidy_income_limit_versions silv
					    JOIN subsidy_income_limit_areas sila ON ( sila.subsidy_income_limit_version_id = silv.id )
					WHERE
						silv. id = ' . ( int ) $intId . '
					    AND silv. property_id = ' . ( int ) $intPropertyId . '
					    AND silv.cid = ' . ( int ) $intCid . '
					    AND silv.is_published IS TRUE';

		return self::fetchSubsidyIncomeLimitVersion( $strSql, $objDatabase );
	}

	public static function checkGapInCustomSubsidyIncomeLimitVersionsByIdByPropertyIdByCidByDates( $strHmfaCode, $intPropertyId, $intCid, $strEffectiveDate, $strEffectiveThroughDate, $objDatabase ) {

		$strSql = ' SELECT  count(*) FROM 
						 (       
						   (         
						   SELECT
						          silv.id,
						          silv.effective_date,
						          silv.effective_through_date,
						          ( \'' . $strEffectiveDate . '\' - silv.effective_through_date ) as day_count
						       FROM
						          subsidy_income_limit_versions silv
						          JOIN subsidy_income_limit_areas sila ON ( sila.subsidy_income_limit_version_id = silv.id )
						       WHERE
						          silv.property_id = ' . ( int ) $intPropertyId . '
						          AND silv.cid = ' . ( int ) $intCid . '
						          AND sila.hmfa_code = \'' . $strHmfaCode . '\'
						          AND silv.is_published IS TRUE
						          AND is_hera IS FALSE
						          AND \'' . $strEffectiveDate . '\' > silv.effective_through_date
						       ORDER BY
						          silv.effective_date DESC
						       LIMIT 1
						    )
						  UNION
						   (         
						     SELECT
						            silv.id,
						            silv.effective_date,
						            silv.effective_through_date,
						            ( silv.effective_date - \'' . $strEffectiveThroughDate . '\' ) as day_count
						         FROM
						            subsidy_income_limit_versions silv
						            JOIN subsidy_income_limit_areas sila ON ( sila.subsidy_income_limit_version_id = silv.id )
						         WHERE
						            silv.property_id = ' . ( int ) $intPropertyId . '
						            AND silv.cid = ' . ( int ) $intCid . '
						            AND sila.hmfa_code = \'' . $strHmfaCode . '\'
						            AND silv.is_published IS TRUE
						            AND is_hera IS FALSE
						            AND \'' . $strEffectiveThroughDate . '\' < silv.effective_date
						         ORDER BY
						            silv.effective_date ASC
						         LIMIT 1
						     )
						   )  as subq
						   WHERE
						    subq.day_count <> 1';

		$arrmixData = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrmixData ) && 0 <> $arrmixData[0]['count'] );
	}

}

?>