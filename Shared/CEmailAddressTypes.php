<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmailAddressTypes
 * Do not add any new functions to this class.
 */

class CEmailAddressTypes extends CBaseEmailAddressTypes {

	public static function fetchEmailAddressTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEmailAddressType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchEmailAddressType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEmailAddressType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllEmailAddressTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM email_address_types';

		return self::fetchEmailAddressTypes( $strSql, $objDatabase );
	}
}
?>