<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CFrequencies
 * Do not add any new functions to this class.
 */

class CFrequencies extends CBaseFrequencies {

	public static function fetchFrequencies( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CFrequency', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchFrequency( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CFrequency', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchAllFrequencies( $objDatabase ) {
		$strSql = 'SELECT * FROM frequencies';
		return self::fetchFrequencies( $strSql, $objDatabase );
	}

	public static function fetchAllFrequenciesByIds( $arrintIds, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return NULL;
		$strSql = 'SELECT * FROM frequencies WHERE id IN( ' . implode( ',', $arrintIds ) . ' ) ORDER BY order_num ASC';
		return self::fetchFrequencies( $strSql, $objDatabase );
	}

	public static function fetchAllPublishedFrequencies( $objDatabase ) {
		$strSql = 'SELECT * FROM
						frequencies
					WHERE
						is_published = 1
					ORDER BY
						order_num ASC';

		return self::fetchFrequencies( $strSql, $objDatabase );
	}

	public static function fetchFrequenciesByIds( $arrintFrequencyIds, $objDatabase ) {
		if( false == valArr( $arrintFrequencyIds ) ) return NULL;

		$strSql = 'SELECT * FROM
						frequencies
					WHERE
						id IN ( ' . implode( ', ', $arrintFrequencyIds ) . ' )
					ORDER BY
						order_num ASC ';

		return self::fetchFrequencies( $strSql, $objDatabase );
	}
}
?>