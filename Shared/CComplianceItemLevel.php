<?php

class CComplianceItemLevel extends CBaseComplianceItemLevel {

	public static $c_arrintInsuranceLevels = [ CComplianceLevel::PROPERTY, CComplianceLevel::WORKER, CComplianceLevel::VENDOR_LEGAL_ENTITY ];

	public function valId() {
		return true;
	}

	public function valComplianceItemId() {
		return true;
	}

	public function valComplianceLevelId() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	// @TODO: Temprory code, once done with migration in Entrata will remove this code.

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( CWorker::USER_ID_FLAG_TO_MIGRATE_COMPLIANCE_DATA != $intCurrentUserId ) {
			return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false );
		} else {
			$this->setDatabase( $objDatabase );

			$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

			$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, compliance_item_id, compliance_level_id, created_by, created_on )
					VALUES ( ' .
								$strId . ', ' .
								$this->sqlComplianceItemId() . ', ' .
								$this->sqlComplianceLevelId() . ', ' .
								$this->sqlCreatedBy() . ', ' .
								$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

			if( true == $boolReturnSqlOnly ) {
				return $strSql;
			} else {
				return $this->executeSql( $strSql, $this, $objDatabase );
			}
		}
	}

}
?>