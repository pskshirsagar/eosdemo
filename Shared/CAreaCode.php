<?php

class CAreaCode extends CBaseAreaCode {

	protected $m_strStateName;
	protected $m_strCountryCode;

	const VITELITY_API_USER_NAME = 'dbat_api';

	public static $c_arrintTollFreeAreaCodes = [ 800, 844, 855, 866, 877, 888, 833 ];

	/**
	 * Set Functions
	 */

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['state_name'] ) ) $this->setStateName( $arrstrValues['state_name'] );
		if( true == isset( $arrstrValues['country_code'] ) ) $this->setCountryCode( $arrstrValues['country_code'] );
	}

	public function setStateName( $strStateName ) {
		$this->m_strStateName = $strStateName;
	}

	public function setCountryCode( $strCountryCode ) {
		$this->m_strCountryCode = $strCountryCode;
	}

	/**
	 * Get Functions
	 */

	public function getStateName() {
		return $this->m_strStateName;
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAreaCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>