<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningRecommendationTypes
 * Do not add any new functions to this class.
 */

class CScreeningRecommendationTypes extends CBaseScreeningRecommendationTypes {

    public static function fetchScreeningRecommendationTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CScreeningRecommendationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchScreeningRecommendationType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CScreeningRecommendationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchAllScreeningRecommendationTypes( $objDatabase ) {
		return self::fetchScreeningRecommendationTypes( 'SELECT * FROM screening_recommendation_types Order By id', $objDatabase );
    }

    public static function fetchScreeningRecommendationTypesByIds( $arrintScreeningRecommendationTypeIds, $objDatabase ) {

		if( false == valArr( $arrintScreeningRecommendationTypeIds ) ) return;

    	$strSql = 'SELECT 
    	                *
    	           FROM     
    	                screening_recommendation_types
    	           WHERE
    	                id IN ( ' . sqlIntImplode( $arrintScreeningRecommendationTypeIds ) . ' ) ';

    	return parent::fetchScreeningRecommendationTypes( $strSql, $objDatabase );
    }

}
?>