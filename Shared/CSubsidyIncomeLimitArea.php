<?php

class CSubsidyIncomeLimitArea extends CBaseSubsidyIncomeLimitArea {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyIncomeLimitVersionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHmfaCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMetroAreaName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMedianIncome() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsMetro() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsHera() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>