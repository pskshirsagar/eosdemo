<?php

class CHelpQuiz extends CBaseHelpQuiz {

	const TEST_OUT_QUIZ          = 'Test Out Quiz';
	const HELP_QUIZ              = 'Help Quiz';
	const SURVEY                 = 'Survey';
	const SHORT                  = 2;
	const SCALE                  = 3;
	const MULTIPLE_CHOICE        = 4;
	const OPTION                 = 5;
	const TRUE_FALSE             = 6;
	const HELP_QUIZ_RETAKE_COUNT = 5;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRequiresSync() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	/**
	* Validate Functions
	*/

	public function valCourseHelpResourceId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getCourseHelpResourceId() ) && true == is_null( $this->getCourseHelpResourceId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'course_help_resource_id', 'Course Help Resource is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName() {

		$boolIsValid = true;
		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valInstruction() {

		$boolIsValid = true;

		if( false == valStr( $this->getInstruction() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'instruction ', 'Instruction is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCourseHelpResourceId();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valInstruction();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public static function loadHelpQuizTypeConstants( $arrmixTemplateParameters ) {

		$arrmixTemplateParameters['MULTIPLE_CHOICE'] = self::MULTIPLE_CHOICE;
		$arrmixTemplateParameters['OPTION']          = self::OPTION;
		$arrmixTemplateParameters['TRUE_FALSE']      = self::TRUE_FALSE;
		$arrmixTemplateParameters['SHORT']           = self::SHORT;
		$arrmixTemplateParameters['SCALE']           = self::SCALE;

		return $arrmixTemplateParameters;
	}

}
?>