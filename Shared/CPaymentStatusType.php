<?php

class CPaymentStatusType extends CBasePaymentStatusType {

	const PENDING 							= 1;
	const RECEIVED 							= 2;
	const AVS_ONLY 							= 3;
	const AVS_VERIFIED 						= 4;
	const AUTHORIZING 						= 5;
	const AUTHORIZED 						= 6;
	const VOIDING 							= 7;
	const VOIDED 							= 8;
	const CAPTURING 						= 9;
	const CAPTURED 							= 10;
	const REVERSAL_PENDING					= 11;
	const REVERSED 							= 12;
	const DECLINED 							= 13;
	const FAILED 							= 14;
	const RECALL							= 15;
	const NSF_FEE_APPLIED					= 16;
	const CHARGE_BACK_PENDING 				= 17;
	const CHARGED_BACK 						= 18;
	const DELETED 							= 19;
	const PHONE_CAPTURE_PENDING 			= 20;
	const CHARGE_BACK_FAILED 				= 21;
	const REVERSAL_FAILED					= 22;
	const REVERSAL_RETURN					= 23;
	const REVERSAL_CANCELLED				= 24;
	const CANCELLED 						= 25;
	const ADMIN_CANCELLED 					= 26;
	const BATCHING							= 27;
	const RETURN_REPRESENTED				= 28;
	const REVERSING							= 29;
	const RETURNING							= 30;
	const CANCELLING						= 31;
	const TRANSFERRED						= 32;
	const PROCESSING_CHARGED_BACK			= 33;
	const PROCESSING_CHARGE_BACK_FAILED		= 34;
	const PROCESSING_CHARGE_BACK_PENDING 	= 35;
	const PENDING_3DSECURE					= 36;

	public static $c_arrintFeeQualifyingPaymentStatusTypeIds	= [ // self::VOIDED,
		self::CAPTURED,
		// self::REVERSAL_PENDING,
		// self::REVERSAL_RETURN
		// self::REVERSED,
		self::RECALL,
		self::CHARGE_BACK_PENDING,
		self::CHARGED_BACK,
		self::CHARGE_BACK_FAILED
	];

	public static $c_arrintCompletedPaymentStatusTypes			 = [
		self::CAPTURED,
		self::REVERSED,
		self::RECALL,
		self::CHARGE_BACK_PENDING,
		self::CHARGED_BACK,
		self::REVERSAL_RETURN
	];

	public static $c_arrintRequiredDepositReversalStatusTypeIds = [
		self::RECALL,
		self::CHARGED_BACK,
		self::CHARGE_BACK_PENDING,
		self::CHARGE_BACK_FAILED,
		self::REVERSAL_RETURN
	];

	public static $c_arrintExcludedPaymentStatusTypes			 = [
		self::PENDING,
		self::AUTHORIZING,
		self::AUTHORIZED,
		self::VOIDING,
		self::VOIDED,
		self::CAPTURING,
		self::DECLINED,
		self::FAILED,
		self::PHONE_CAPTURE_PENDING,
		self::CANCELLED,
		self::ADMIN_CANCELLED,
		self::BATCHING
	];

	public static $c_arrintBlockDistributionPaymentStatusTypes		= [
		self::VOIDING,
		self::VOIDED,
		self::DECLINED,
		self::FAILED,
		self::DELETED,
		self::REVERSAL_CANCELLED,
		self::CANCELLED,
		self::ADMIN_CANCELLED
	];

	public static $c_arrintClientFacingPaymentStatusTypes	   = [
		self::AUTHORIZED,
		self::CANCELLED,
		self::CAPTURED,
		self::CHARGED_BACK,
		self::CHARGE_BACK_FAILED,
		self::CHARGE_BACK_PENDING,
		self::DECLINED,
		self::DELETED,
		self::PENDING,
		self::PHONE_CAPTURE_PENDING,
		self::RECEIVED,
		self::RECALL,
		self::RETURN_REPRESENTED,
		self::REVERSAL_CANCELLED,
		self::REVERSAL_PENDING,
		self::REVERSAL_RETURN,
		self::REVERSED,
		self::VOIDED
	];

	public static $c_arrintMigrationTemporaryReversalPaymentTypes = [
		self::REVERSED,
		self::RECALL,
		self::CHARGE_BACK_PENDING,
		self::CHARGED_BACK
	];

	public static $c_arrintPaperlessIncentivePaymentStatusTypes = [
		self::RECEIVED,
		self::CAPTURED,
		self::REVERSAL_PENDING,
		self::REVERSED,
		self::RECALL,
		self::CHARGE_BACK_PENDING,
		self::CHARGED_BACK,
		self::CHARGE_BACK_FAILED,
		self::REVERSAL_RETURN,
		self::REVERSAL_CANCELLED,
		self::RETURN_REPRESENTED,
		self::REVERSING,
		self::RETURNING,
		self::PROCESSING_CHARGED_BACK,
		self::PROCESSING_CHARGE_BACK_FAILED,
		self::PROCESSING_CHARGE_BACK_PENDING
	];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getPaymentStatusTypeIdToStr( $intPaymentStatusTypeId ) {

		$strPaymentStatusTypeName = NULL;

		switch( $intPaymentStatusTypeId ) {
			case CPaymentStatusType::PENDING:
				$strPaymentStatusTypeName = __( 'Pending' );
				break;

			case CPaymentStatusType::RECEIVED:
				$strPaymentStatusTypeName = __( 'Received' );
				break;

			case CPaymentStatusType::AVS_ONLY:
				$strPaymentStatusTypeName = __( 'AVS Only' );
				break;

			case CPaymentStatusType::AVS_VERIFIED:
				$strPaymentStatusTypeName = __( 'AVS Verified' );
				break;

			case CPaymentStatusType::AUTHORIZING:
				$strPaymentStatusTypeName = __( 'Authorizing' );
				break;

			case CPaymentStatusType::AUTHORIZED:
				$strPaymentStatusTypeName = __( 'Authorized' );
				break;

			case CPaymentStatusType::VOIDING:
				$strPaymentStatusTypeName = __( 'Voiding' );
				break;

			case CPaymentStatusType::VOIDED:
				$strPaymentStatusTypeName = __( 'Voided' );
				break;

			case CPaymentStatusType::CAPTURING:
				$strPaymentStatusTypeName = __( 'Capturing' );
				break;

			case CPaymentStatusType::CAPTURED:
				$strPaymentStatusTypeName = __( 'Captured' );
				break;

			case CPaymentStatusType::REVERSAL_PENDING:
				$strPaymentStatusTypeName = __( 'Reversal Pending' );
				break;

			case CPaymentStatusType::REVERSED:
				$strPaymentStatusTypeName = __( 'Reversed' );
				break;

			case CPaymentStatusType::DECLINED:
				$strPaymentStatusTypeName = __( 'Declined' );
				break;

			case CPaymentStatusType::FAILED:
				$strPaymentStatusTypeName = __( 'Failed' );
				break;

			case CPaymentStatusType::RECALL:
				$strPaymentStatusTypeName = __( 'Returned' );
				break;

			case CPaymentStatusType::CHARGE_BACK_PENDING:
				$strPaymentStatusTypeName = __( 'Pending Charge Back' );
				break;

			case CPaymentStatusType::CHARGED_BACK:
				$strPaymentStatusTypeName = __( 'Charged Back' );
				break;

			case CPaymentStatusType::DELETED:
				$strPaymentStatusTypeName = __( 'Deleted' );
				break;

			case CPaymentStatusType::PHONE_CAPTURE_PENDING:
				$strPaymentStatusTypeName = __( 'Pending Phone Capture' );
				break;

			case CPaymentStatusType::CHARGE_BACK_FAILED:
				$strPaymentStatusTypeName = __( 'Charge Back Failed' );
				break;

			case CPaymentStatusType::REVERSAL_CANCELLED:
				$strPaymentStatusTypeName = __( 'Reversal Cancelled' );
				break;

			case CPaymentStatusType::REVERSAL_RETURN:
				$strPaymentStatusTypeName = __( 'Reversal Return' );
				break;

			case CPaymentStatusType::CANCELLED:
				$strPaymentStatusTypeName = __( 'Cancelled' );
				break;

			case CPaymentStatusType::ADMIN_CANCELLED:
				$strPaymentStatusTypeName = __( 'Admin Cancelled' );
				break;

			case CPaymentStatusType::RETURN_REPRESENTED:
				$strPaymentStatusTypeName = __( 'Represented' );
				break;

			case CPaymentStatusType::TRANSFERRED:
				$strPaymentStatusTypeName = __( 'Transferred' );
				break;

			case CPaymentStatusType::BATCHING:
				$strPaymentStatusTypeName = __( 'Batching' );
				break;

			case CPaymentStatusType::NSF_FEE_APPLIED:
				$strPaymentStatusTypeName = __( 'NSF Fee Applied' );
				break;

			case CPaymentStatusType::REVERSAL_FAILED:
				$strPaymentStatusTypeName = __( 'Reversal Failed' );
				break;

			case CPaymentStatusType::REVERSING:
				$strPaymentStatusTypeName = __( 'Reversing' );
				break;

			case CPaymentStatusType::RETURNING:
				$strPaymentStatusTypeName = __( 'Returning' );
				break;

			case CPaymentStatusType::CANCELLING:
				$strPaymentStatusTypeName = __( 'Cancelling' );
				break;

			case CPaymentStatusType::PROCESSING_CHARGED_BACK:
				$strPaymentStatusTypeName = __( 'Processing Charged Back' );
				break;

			case CPaymentStatusType::PROCESSING_CHARGE_BACK_FAILED:
				$strPaymentStatusTypeName = __( 'Processing Chargeback Failed' );
				break;

			case CPaymentStatusType::PROCESSING_CHARGE_BACK_PENDING:
				$strPaymentStatusTypeName = __( 'Processing Chargeback Pending' );
				break;

			case CPaymentStatusType::PENDING_3DSECURE:
				$strPaymentStatusTypeName = __( 'Pending 3D Secure Authorization' );
				break;

			default:
				trigger_error( $intPaymentStatusTypeId . 'is not a valid payment status type id.', E_USER_WARNING );
				break;
		}

		return $strPaymentStatusTypeName;
	}

}
?>