<?php

class CToolTip extends CBaseToolTip {

	protected $m_strToolTipTypeName;
	protected $m_strUpdatedByUserName;
	protected $m_intPropertySettingKeyId;

	const SYNC_REQUIRED		= 1;
	const SYNC_NOT_REQUIRED = 0;

	public static $c_intCurrentUserId;

	/**
	 * Get Functions
	 */

	public function getToolTipTypeName() {
		return $this->m_strToolTipTypeName;
	}

	public function getUpdatedByUserName() {
		return $this->m_strUpdatedByUserName;
	}

	public function getPropertySettingKeyId() {
		return $this->m_intPropertySettingKeyId;
	}

	public function getHelpResourceDetails() {
		return $this->getDetailsField( [ 'help_resource_ids_order' ] );
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['tool_tip_type_name'] ) ) {
			$this->setToolTipTypeName( $arrstrValues['tool_tip_type_name'] );
		}
		if( true == isset( $arrstrValues['updated_by_user_name'] ) ) {
			$this->setUpdatedByUserName( $arrstrValues['updated_by_user_name'] );
		}
		if( true == isset( $arrstrValues['property_setting_key_id'] ) ) {
			$this->setPropertySettingKeyId( $arrstrValues['property_setting_key_id'] );
		}

	}

	public function setToolTipTypeName( $strToolTipTypeName ) {
		$this->m_strToolTipTypeName = $strToolTipTypeName;
	}

	public function setUpdatedByUserName( $strUpdatedByUserName ) {
		$this->m_strUpdatedByUserName = $strUpdatedByUserName;
	}

	public function setPropertySettingKeyId( $intPropertySettingKeyId ) {
		$this->m_intPropertySettingKeyId = $intPropertySettingKeyId;
	}

	public function setHelpResourceDetails( $strHelpResourceDetails ) {
		$this->setDetailsField( [ 'help_resource_ids_order' ], ( array ) $strHelpResourceDetails );
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valToolTipTypeId() {
		return true;
	}

	public function valKey( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getKey() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', 'Key is required. ' ) );
		}

		if( false == is_null( $this->getKey() ) && true == preg_match( '/[^\s\\|\w]/', $this->getKey() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', 'Key should contain only alphanumeric characters or underscores. ' ) );
		}

		if( false == is_null( $this->getKey() ) && true == preg_match( '/[\s]/', $this->getKey() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', 'Key should not contain space. ' ) );
		}

		if( true == $boolIsValid && true == valObj( $objDatabase, 'CDatabase' ) ) {
			$strSql = ' WHERE lower ( key ) = \'' . \Psi\CStringService::singleton()->strtolower( $this->getKey() ) . '\' ' . ( 0 < $this->getId() ? ' AND id <> ' . $this->m_intId : '' );

			$intCount = \Psi\Eos\Admin\CToolTips::createService()->fetchToolTipCount( $strSql, $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', 'Key already exists. ' ) );
			}
		}
		return $boolIsValid;
	}

	public function valClientDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getClientDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'client_description', 'Current customer text is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valClientDescriptionEdited() {
		$boolIsValid = true;

		if( true == is_null( $this->getClientDescriptionEdited() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'client_description', 'Change customer text to is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valInternalDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getInternalDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'internal_description', 'Support text is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsPublished() {
		return true;
	}

	public function valUpdatedBy() {
		return true;
	}

	public function valUpdatedOn() {
		return true;
	}

	public function valCreatedBy() {
		return true;
	}

	public function valCreatedOn() {
		return true;
	}

	public function validate( $strAction, $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valClientDescription();
				$boolIsValid &= $this->valKey( $objAdminDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valKey( $objAdminDatabase );
				$boolIsValid &= $this->valClientDescription();
				$boolIsValid &= $this->valClientDescriptionEdited();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Insert And Update Functions
	 *
	 */

	public function insert( $intCurrentUserId, $objAdminDatabase, $boolSync = false, $objClientDatabase = NULL ) {

		if( true == $boolSync ) {
			return parent::insert( $intCurrentUserId, $objAdminDatabase, $boolReturnSqlOnly = true );
		}

		if( true == is_null( $objAdminDatabase ) || false == valObj( $objAdminDatabase, 'CDatabase' ) || CDatabaseType::ADMIN != $objAdminDatabase->getDatabaseTypeId() ) {
			trigger_error( 'Failed to load database object', E_USER_ERROR );
			exit;
		}

		$strSql = parent::insert( $intCurrentUserId, $objAdminDatabase, $boolReturnSqlOnly = true );

		if( false === $strSql ) {
			return true;
		}

		if( false == $this->executeSql( $strSql, $this, $objAdminDatabase ) ) {
			return false;
		}

		if( true == valObj( $objClientDatabase, 'CDatabase' ) && CDatabaseType::CLIENT == $objClientDatabase->getDatabaseTypeId() ) {
			if( false == $this->executeSql( $strSql, $this, $objClientDatabase ) ) {
				return false;
			}
		}

		return true;
	}

	public function update( $intCurrentUserId, $objAdminDatabase, $boolSync = false, $objClientDatabase = NULL ) {

		if( true == $boolSync ) {
			return parent::update( $intCurrentUserId, $objAdminDatabase, $boolReturnSqlOnly = true );
		}

		if( true == is_null( $objAdminDatabase ) || false == valObj( $objAdminDatabase, 'CDatabase' ) || CDatabaseType::ADMIN != $objAdminDatabase->getDatabaseTypeId() ) {
			trigger_error( 'Failed to load database object', E_USER_ERROR );
			exit;
		}

		$strSql = parent::update( $intCurrentUserId, $objAdminDatabase, $boolReturnSqlOnly = true );

		if( false === $strSql ) {
			return true;
		}

		if( false == $this->executeSql( $strSql, $this, $objAdminDatabase ) ) {
			return false;
		}

		if( true == valObj( $objClientDatabase, 'CDatabase' ) && CDatabaseType::CLIENT == $objClientDatabase->getDatabaseTypeId() ) {
			if( false == $this->executeSql( $strSql, $this, $objClientDatabase ) ) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Other Functions
	 *
	 */

	public function createToolTipEdit( $objOldToolTip ) {

		$objToolTipEdit	= new CToolTipEdit();
		if( false == valObj( $objToolTipEdit, 'CToolTipEdit' ) ) {
			trigger_error( 'Application Error: Failed to load CToolTipView object .', E_USER_ERROR );
			exit;
		}

		$objToolTipEdit->setToolTipId( $this->getId() );
		$objToolTipEdit->setOldData( $objOldToolTip->getClientDescription() );
		$objToolTipEdit->setNewData( $this->getClientDescription() );
		$objToolTipEdit->setFieldName( 'Client Description' );

		return $objToolTipEdit;
	}

}
?>