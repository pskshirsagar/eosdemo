<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTableLogs
 * Do not add any new functions to this class.
 */

class CTableLogs extends CBaseTableLogs {

	public static function fetchCustomPaginatedTableLogs( $intPageNo, $intPageSize, $objDatabase, $arrstrTableLogsSearchParameters = NULL ) {
		$arrstrAndSearchParameters = self::fetchSearchCriteria( $arrstrTableLogsSearchParameters );

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;

		$strSql = 'SELECT
						tl.*,
						c.company_name as company_name
					FROM
						table_logs tl
						LEFT JOIN clients c ON ( c.id = tl.cid )'
						. ( ( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) )	? ' WHERE ' : '' )
						. ( ( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? implode( ' AND ', $arrstrAndSearchParameters ) : '' )
							. ( ( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? ' AND tl.id IN ( SELECT
																									max(id)
																							 	FROM
																									table_logs
																								GROUP
																									BY created_on
																							) ' : '' ) . '
					ORDER BY
						tl.id DESC
					OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;

		return self::fetchTableLogs( $strSql, $objDatabase );
	}

	public static function fetchCustomPaginatedTableLogsCount( $objDatabase, $arrstrTableLogsSearchParameters = NULL ) {

		$arrstrAndSearchParameters = self::fetchSearchCriteria( $arrstrTableLogsSearchParameters );

		$strSql = 'SELECT
						count(tl.id)
					FROM
						table_logs tl '
						. ( ( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? ' WHERE ' : '' )
						. ( ( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? implode( ' AND ', $arrstrAndSearchParameters ) : '' )
						. ( ( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? ' AND tl.id IN ( SELECT
																								max(id)
																							 	FROM
																								table_logs
																								GROUP
																								BY created_on
																							) ' : '' );

		$arrstrData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) {
			return $arrstrData[0]['count'];
		}

		return 0;
	}

	public static function fetchPropertyByReferenceNumberByTableNameByCid( $intReferenceNumber, $strTableName, $intCid, $objClientDatabase ) {

		switch( $strTableName ) {
			case CTable::WEBSITE_PREFERENCES:
				$strSubSql = self::buildSqlForWebsitePreferencesByReferenceNumberByCid( $intReferenceNumber, $intCid );
				break;

			case CTable::PROPERTY_GL_SETTINGS:
				$strSubSql = self::buildSqlForPropertyGlSettingsByReferenceNumberByCid( $intReferenceNumber, $intCid );
				break;

			case CTable::PROPERTY_AR_ORIGIN_RULES:
				$strSubSql = self::buildSqlForPropertyArOriginRulesByReferenceNumberByCid( $intReferenceNumber, $intCid );
				break;

			case CTable::PROPERTY_LATE_FEE_FORMULAS:
				$strSubSql = self::buildSqlForPropertyLateFeeFormulasByReferenceNumberByCid( $intReferenceNumber, $intCid );
				break;

			case CTable::PROPERTY_PREFERENCES:
			case CTable::PROPERTIES:
			case CTable::PROPERTY_MERCHANT_ACCOUNTS:
			case CTable::INTEGRATION_DATABASES:
			case CTable::INTEGRATION_CLIENTS:
			case CTable::INTEGRATION_CLIENT_KEY_VALUES:
				$strSubSql = self::buildSqlForPropertyRelatedTablesByReferenceNumberByCid( $intReferenceNumber, $intCid );
				break;

			case CTable::COMPANY_PREFERENCES:
			case CTable::GL_SETTINGS:
			case CTable::COMPANY_MERCHANT_ACCOUNTS:
			case CTable::ACCOUNTS:
			case CTable::GL_TREES:
			case CTable::COMPANY_USER_GROUPS:
			case CTable::COMPANY_USER_SERVICES:
			case CTable::COMPANY_USER_WEBSITES:
			case CTable::COMPANY_USER_PROPERTIES:
			case CTable::COMPANY_USER_PROPERTY_GROUPS:
			case CTable::COMPANY_USER_PERMISSIONS:
			case CTable::COMPANY_USERS:
			case CTable::COMPANY_EMPLOYEES:
			case CTable::COMPANY_GROUPS:
			case CTable::COMPANY_GROUP_PERMISSIONS:
			case CTable::RECORDS:
			case CTable::DOMAINS:
			case CTable::MAINTENANCE_STATUSES:
			case CTable::MAINTENANCE_PRIORITIES:
				$strSubSql = self::buildSqlForCompanyRelatedTablesByReferenceNumberByCid( $intReferenceNumber, $intCid, $strTableName );
				break;

			case CTable::APPLICATIONS:
				$strSubSql = self::buildSqlForApplicationsByReferenceNumberByCid( $intReferenceNumber, $intCid );
				break;

			case CTable::TEMPLATE_SLOT:
			case CTable::WEBSITE_INFO:
			case CTable::WEBSITE_TEMPLATE_SLOT:
				$strSubSql = self::buildSqlForWebsiteInfoByReferenceNumberByCid( $intReferenceNumber, $intCid );
				break;

			case CTable::LATE_FEE_FORMULAS:
				$strSubSql = self::buildSqlForLateFeeFormulasByReferenceNumberByCid( $intReferenceNumber, $intCid );
				break;

			case CTable::PROPERTY_CALL_SETTINGS:
				$strSubSql = self::buildSqlForPropertyCallSettingsByReferenceNumberByCid( $intReferenceNumber, $intCid );
				break;

			case CTable::AR_CODES:
				$strSubSql = self::buildSqlForArCodesByReferenceNumberByCid( $intReferenceNumber, $intCid );
				break;

			case CTable::PROPERTY_AR_CODES:
				$strSubSql = self::buildSqlForPropertyArCodesByReferenceNumberByCid( $intReferenceNumber, $intCid );
				break;

			case CTable::PROPERTY_CHARGE_SETTINGS:
				$strSubSql = self::buildSqlForPropertyChargeSettingsByReferenceNumberByCid( $intReferenceNumber, $intCid );
				break;

			case CTable::FILES:
			case CTable::FILE_NOTES:
			case CTable::FILE_ASSOCIATIONS:
			case CTable::FILE_METADATAS:
				$strSubSql = self::buildSqlForDocumentManagementRelatedTablesByReferenceNumberByCid( $intReferenceNumber, $intCid );
				break;

			case CTable::PROPERTY_NOTIFICATIONS:
				$strSubSql = self::buildSqlForPropertyNotificationsByReferenceNumberByCid( $intReferenceNumber, $intCid );
				break;

			case CTable::PROPERTY_LEAD_SOURCES:
				$strSubSql = self::buildSqlForPropertyLeadSourcesByReferenceNumberByCid( $intReferenceNumber, $intCid );
				break;

			default:
				// default action
				break;
		}

		$arrmixTableLogData = [];

		if( true == valStr( $strSubSql ) ) {

			$strSql = 'SELECT
							sub.website_name, p.property_name
						FROM
							( ' . $strSubSql . ' ) as sub
							LEFT JOIN properties p ON ( p.id = sub.property_id AND p.cid = sub.cid AND p.cid = ' . ( int ) $intCid . ')';

			$arrmixTableLogData = self::fetchTableLog( $strSql, $objClientDatabase );
		}

		return $arrmixTableLogData;
	}

	public static function fetchTableLogsByCidByFormFilter( $intCid, $arrstrFormFilter, $objClientDatabase, $arrmixSqlParam = NULL ) {

		$arrstrPropertyRelatedTables = [
			CTable::PROPERTY_PREFERENCES,
			CTable::PROPERTY_GL_SETTINGS,
			CTable::PROPERTY_AR_ORIGIN_RULES,
			CTable::PROPERTY_LATE_FEE_FORMULAS,
			CTable::PROPERTIES,
			CTable::PROPERTY_MERCHANT_ACCOUNTS,
			CTable::INTEGRATION_DATABASES,
			CTable::INTEGRATION_CLIENTS,
			CTable::PROPERTY_CHARGE_SETTINGS,
			CTable::MAINTENANCE_STATUSES,
			CTable::MAINTENANCE_PRIORITIES
		];

		$arrstrWebsiteRelatedTables = [
			CTable::WEBSITE_INFO,
			CTable::WEBSITE_TEMPLATE_SLOT,
			CTable::WEBSITE_PREFERENCES
		];

		$strSubSql = self::buildSqlForAllTablesByCid( $intCid, $arrstrFormFilter );

		$strColumns = 'sub.* ';

		if( true == valArr( $arrmixSqlParam ) && true == isset( $arrmixSqlParam['count'] ) && true == $arrmixSqlParam['count'] && 'screen' == $arrmixSqlParam['Output_type'] ) {
			$strColumns = 'count( sub.id )';
			$strGroupBy = ' GROUP BY sub.id, sub.table_name';
		} else {
			$strGroupBy = '';
		}

		if( true == isset( $arrstrFormFilter['table_name'] ) && true == in_array( $arrstrFormFilter['table_name'], $arrstrPropertyRelatedTables ) ) {
			if( true == valArr( $arrmixSqlParam ) && false == $arrmixSqlParam['count'] || 'screen' != $arrmixSqlParam['Output_type'] ) {
				$strColumns = '	sub.*, p.property_name';
			}

			$strSql = 'SELECT
							' . $strColumns . '
						FROM
							( ' . $strSubSql . ' ) AS sub
							LEFT JOIN properties p ON ( p.id = sub.reference_number AND p.cid = sub.cid AND p.cid = ' . ( int ) $intCid . ')';
			if( true == isset( $arrstrFormFilter['property_id'] ) && true == is_numeric( $arrstrFormFilter['property_id'] ) ) {
				$strSql .= '
						WHERE
							sub.reference_number = ' . $arrstrFormFilter['property_id'];
			}
			$strSql .= $strGroupBy . '
						ORDER BY
							sub.table_name,
							sub.id DESC ';
		} elseif( true == isset( $arrstrFormFilter['table_name'] ) && true == in_array( $arrstrFormFilter['table_name'], $arrstrWebsiteRelatedTables ) ) {

			if( true == valArr( $arrmixSqlParam ) && true == isset( $arrmixSqlParam['count'] ) && false == $arrmixSqlParam['count'] || 'screen' != $arrmixSqlParam['Output_type'] ) {
				$strColumns = 'sub.*, w.name as website_name';
			}
			$strSql = 'SELECT
							' . $strColumns . '
						FROM
							( ' . $strSubSql . ' ) AS sub
						LEFT JOIN websites w ON ( w.id = sub.reference_number AND w.cid = sub.cid AND w.cid = ' . ( int ) $intCid . ')';

			if( true == isset( $arrstrFormFilter['website_id'] ) && true == is_numeric( $arrstrFormFilter['website_id'] ) ) {

				$strSql .= 'WHERE
						sub.reference_number = ' . $arrstrFormFilter['website_id'];
			}

			$strSql .= $strGroupBy . '
						ORDER BY
							sub.table_name,
							sub.id DESC ';

		} else {
			if( true == valArr( $arrmixSqlParam ) && true == isset( $arrmixSqlParam['count'] ) && false == $arrmixSqlParam['count'] || 'screen' != $arrmixSqlParam['Output_type'] ) {
				$strColumns = 'sub.* ';
			}
			// If property and website related table is not selected
			$strSql = ' SELECT
							' . $strColumns . '
						FROM
							( ' . $strSubSql . ' ) as sub
							' . $strGroupBy . '
						ORDER BY
							sub.table_name,
							sub.id DESC ';
		}

		if( true == valArr( $arrmixSqlParam ) && false == $arrmixSqlParam['count'] && true == isset( $arrmixSqlParam['offset'] ) ) {

			$arrmixSqlParam['offset'] = ( $arrmixSqlParam['offset'] >= 1 ) ? $arrmixSqlParam['offset'] * $arrmixSqlParam['limit'] : 0;

			$strSql .= ' LIMIT ' . $arrmixSqlParam['limit'] . '
						 OFFSET ' . $arrmixSqlParam['offset'];
		}

		if( true == isset( $arrmixSqlParam['count'] ) && true == $arrmixSqlParam['count'] && 'screen' == $arrmixSqlParam['Output_type'] ) {
			return fetchData( $strSql, $objClientDatabase );
		} else {
			return self::fetchTableLogs( $strSql, $objClientDatabase );
		}
	}

	public static function buildSqlForAllTablesByCid( $intCid, $arrstrSearchParameters ) {
		$arrstrAndSearchParameters = self::fetchSearchCriteria( $arrstrSearchParameters );

		// If property is selected
		if( true == isset( $arrstrSearchParameters['property_id'] ) && true == is_numeric( $arrstrSearchParameters['property_id'] ) ) {

			$strSql = self::buildSqlForPropertyRelatedTablesByCid( $intCid, $arrstrSearchParameters );

		} elseif( true == isset( $arrstrSearchParameters['website_id'] ) && true == is_numeric( $arrstrSearchParameters['website_id'] ) ) {
			// If website is selected

			$strSql = self::buildSqlForWebsiteRelatedTablesByCid( $intCid, $arrstrSearchParameters );
		} else {
			// If property and website related table is not selected
			$strSql = 'SELECT
							tl.*,
							NULL::integer as property_id,
							NULL::varchar as website_name,
							CASE
								WHEN( e.name_first IS NOT NULL OR e.name_last IS NOT NULL )
								THEN
									CASE
										WHEN( e.name_first = e.name_last OR e.name_last IS NULL )
										THEN e.name_first
										WHEN( e.name_first IS NULL )
										THEN e.name_last
										ELSE e.name_first || \' \' || e.name_last
									END
								ELSE u.username
							END as username
						FROM
							table_logs tl
							LEFT JOIN company_users u ON ( tl.created_by = u.id AND tl.cid = u.cid AND ( tl.cid = ' . ( int ) $intCid . ' OR tl.cid = ' . CClients::$c_intNullCid . ' ) )
							LEFT JOIN company_employees e ON ( u.company_employee_id = e.id AND u.cid = e.cid AND ( u.cid = ' . ( int ) $intCid . ' OR u.cid = ' . CClients::$c_intNullCid . ' ) )
						WHERE
							( tl.cid = ' . ( int ) $intCid . ' OR tl.cid = ' . ( int ) CClients::$c_intNullCid . ')'
										. ( ( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? ' AND ' : '' )
										. ( ( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? implode( ' AND ', $arrstrAndSearchParameters ) : '' );
		}

		return $strSql;
	}

	public static function fetchCustomDistinctClientList( $objDatabase ) {
		$strSql = 'SELECT
						c.id,
						c.company_name
					FROM
						clients c
					WHERE
						c.id IN( SELECT DISTINCT ON ( cid ) cid FROM table_logs )
					ORDER BY LOWER( c.company_name ) ';

		return self::fetchTableLogs( $strSql, $objDatabase );
	}

	public static function fetchCustomTableLogByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						tl.*,
						pr.property_name as property_name,
						CASE
							WHEN( e.name_first IS NOT NULL OR e.name_last IS NOT NULL )
							THEN
								CASE
									WHEN( e.name_first = e.name_last OR e.name_last IS NULL )
									THEN e.name_first
									WHEN( e.name_first IS NULL )
									THEN e.name_last
									ELSE e.name_first || \' \' || e.name_last
								END
							ELSE u.username
						END as username
					FROM
						table_logs tl
						LEFT JOIN company_users u ON ( tl.created_by = u.id AND tl.cid = u.cid AND ( tl.cid = ' . ( int ) $intCid . ' OR tl.cid = ' . CClients::$c_intNullCid . ' ) )
						LEFT JOIN company_employees e ON ( u.company_employee_id = e.id AND u.cid = e.cid AND u.cid = ' . ( int ) $intCid . ' )
						LEFT JOIN properties pr ON ( tl.reference_number = pr.id AND tl.cid = pr.cid )
					WHERE
						tl.id = ' . ( int ) $intId . '
						AND ( tl.cid = ' . ( int ) $intCid . ' OR tl.cid = ' . ( int ) CClients::$c_intNullCid . ' )';

		return self::fetchTableLog( $strSql, $objDatabase );
	}

	public static function fetchSearchCriteria( $arrstrSearchParameters ) {
		$arrstrAndSearchParameters = [];

		if( false == is_null( $arrstrSearchParameters ) ) {

			if( true == valStr( $arrstrSearchParameters['table_name'] ) ) {

				$arrstrAndSearchParameters[] = 'tl.table_name IN (  \'' . str_replace( ',', '\', \'', $arrstrSearchParameters['table_name'] ) . '\' ) ';
			}

			if( true == isset( $arrstrSearchParameters['actions'] ) && true == valArr( $arrstrSearchParameters['actions'] ) ) {
				$strActionContion            = "'" . implode( "','", $arrstrSearchParameters['actions'] ) . "'";
				$arrstrAndSearchParameters[] = 'tl.action IN( ' . $strActionContion . ')';
			}

			if( true == isset( $arrstrSearchParameters['reference_number'] ) && true == is_numeric( trim( $arrstrSearchParameters['reference_number'] ) ) ) {
				$arrstrAndSearchParameters[] = 'tl.reference_number =' . ( int ) trim( $arrstrSearchParameters['reference_number'] );
			}

			if( true == isset( $arrstrSearchParameters['from_date'] ) && 0 < strlen( trim( $arrstrSearchParameters['from_date'] ) ) ) {
				$arrstrAndSearchParameters[] = 'tl.created_on >= \'' . date( 'Y-m-d 00:00:00', strtotime( $arrstrSearchParameters['from_date'] ) ) . '\'';
			}

			if( true == isset( $arrstrSearchParameters['to_date'] ) && 0 < strlen( trim( $arrstrSearchParameters['to_date'] ) ) ) {
				$arrstrAndSearchParameters[] = 'tl.created_on <= \'' . date( 'Y-m-d 23:59:59', strtotime( $arrstrSearchParameters['to_date'] ) ) . '\'';
			}

			if( true == isset( $arrstrSearchParameters['cid'] ) && true == is_numeric( $arrstrSearchParameters['cid'] ) ) {
				$arrstrAndSearchParameters[] = 'tl.cid =' . ( int ) $arrstrSearchParameters['cid'];
			}

			if( true == isset( $arrstrSearchParameters['created_by'] ) && true == is_numeric( $arrstrSearchParameters['created_by'] ) ) {
				$arrstrAndSearchParameters[] = 'tl.created_by =' . ( int ) $arrstrSearchParameters['created_by'];
			}
		}

		return $arrstrAndSearchParameters;
	}

	public static function buildSqlForWebsitePreferencesByReferenceNumberByCid( $intReferenceNumber, $intCid ) {

		$strSql = 'SELECT
						DISTINCT ON(reference_number) reference_number,
						NULL::integer AS property_id,
						w.name AS website_name,
						tl.cid
					FROM
						table_logs tl
						LEFT JOIN websites w ON ( tl.reference_number = w.id AND tl.cid = w.cid )
					WHERE
						tl.reference_number = ' . ( int ) $intReferenceNumber . '
						AND tl.table_name = \'' . CTable::WEBSITE_PREFERENCES . '\'
						AND ( tl.cid = ' . ( int ) $intCid . ' OR tl.cid = ' . ( int ) CClients::$c_intNullCid . ')';

		return $strSql;
	}

	public static function buildSqlForPropertyGlSettingsByReferenceNumberByCid( $intReferenceNumber, $intCid ) {

		$strSql = 'SELECT
						DISTINCT ON(reference_number) reference_number,
						pgs.property_id,
						NULL::varchar as website_name,
						tl.cid
					FROM
						table_logs tl
						LEFT JOIN property_gl_settings pgs ON ( tl.cid = pgs.cid AND tl.reference_number = pgs.property_id )
					WHERE
						tl.reference_number = ' . ( int ) $intReferenceNumber . '
						AND tl.table_name = \'' . CTable::PROPERTY_GL_SETTINGS . '\'
						AND ( tl.cid = ' . ( int ) $intCid . ' OR tl.cid = ' . ( int ) CClients::$c_intNullCid . ')';

		return $strSql;
	}

	public static function buildSqlForPropertyArOriginRulesByReferenceNumberByCid( $intReferenceNumber, $intCid ) {

		$strSql = 'SELECT
						DISTINCT ON(reference_number) reference_number,
						paor.property_id,
						NULL::varchar as website_name,
						tl.cid
					FROM
						table_logs tl
						LEFT JOIN property_ar_origin_rules paor ON ( tl.cid = paor.cid AND tl.reference_number = paor.property_id )
					WHERE
						tl.reference_number = ' . ( int ) $intReferenceNumber . '
						AND tl.table_name = \'' . CTable::PROPERTY_AR_ORIGIN_RULES . '\'
						AND ( tl.cid = ' . ( int ) $intCid . ' OR tl.cid = ' . ( int ) CClients::$c_intNullCid . ')';

		return $strSql;
	}

	public static function buildSqlForPropertyLateFeeFormulasByReferenceNumberByCid( $intReferenceNumber, $intCid ) {

		$strSql = 'SELECT
						DISTINCT ON(reference_number) reference_number,
						plff.property_id,
						NULL::varchar as website_name,
						tl.cid
					FROM
						table_logs tl
						LEFT JOIN property_late_fee_formulas plff ON ( tl.reference_number = plff.id AND tl.cid = plff.cid )
					WHERE
						tl.reference_number = ' . ( int ) $intReferenceNumber . '
						AND tl.table_name = \'' . CTable::PROPERTY_LATE_FEE_FORMULAS . '\'
						AND ( tl.cid = ' . ( int ) $intCid . ' OR tl.cid = ' . ( int ) CClients::$c_intNullCid . ')';

		return $strSql;
	}

	public static function buildSqlForPropertyRelatedTablesByReferenceNumberByCid( $intReferenceNumber, $intCid ) {

		$arrstrPropertyRelatedTables = [
			CTable::PROPERTY_PREFERENCES,
			CTable::PROPERTIES,
			CTable::PROPERTY_MERCHANT_ACCOUNTS,
			CTable::INTEGRATION_DATABASES,
			CTable::INTEGRATION_CLIENTS,
			CTable::INTEGRATION_CLIENT_KEY_VALUES
		];

		$strPropertyRelatedTables = "'" . implode( "', '", $arrstrPropertyRelatedTables ) . "'";

		$strSql = 'SELECT
						DISTINCT ON(reference_number) reference_number,
						tl.reference_number as property_id,
						NULL::varchar as website_name,
						tl.cid
					FROM
						table_logs tl
					WHERE
						tl.reference_number = ' . ( int ) $intReferenceNumber . '
						AND tl.table_name IN( ' . $strPropertyRelatedTables . ' )
						AND ( tl.cid = ' . ( int ) $intCid . ' OR tl.cid = ' . ( int ) CClients::$c_intNullCid . ')';

		return $strSql;
	}

	public static function buildSqlForPropertyRelatedTablesByCid( $intCid, $arrstrSearchParameters ) {
		$arrstrPropertyRelatedTables = [
			CTable::PROPERTY_PREFERENCES,
			CTable::PROPERTY_GL_SETTINGS,
			CTable::PROPERTY_AR_ORIGIN_RULES,
			CTable::PROPERTIES,
			CTable::PROPERTY_MERCHANT_ACCOUNTS,
			CTable::INTEGRATION_DATABASES,
			CTable::INTEGRATION_CLIENTS,
			CTable::PROPERTY_CHARGE_SETTINGS,
			CTable::MAINTENANCE_STATUSES,
			CTable::MAINTENANCE_PRIORITIES
		];

		$strPropertyRelatedTables = "'" . implode( "', '", $arrstrPropertyRelatedTables ) . "'";

		$arrstrAndSearchParameters = self::fetchSearchCriteria( $arrstrSearchParameters );

		$strSql = 'SELECT
						tl.*,
						tl.reference_number as property_id,
						NULL::varchar as website_name,
						CASE
							WHEN( e.name_first IS NOT NULL OR e.name_last IS NOT NULL )
							THEN
								CASE
									WHEN( e.name_first = e.name_last OR e.name_last IS NULL )
									THEN e.name_first
									WHEN( e.name_first IS NULL )
									THEN e.name_last
									ELSE e.name_first || \' \' || e.name_last
								END
							ELSE u.username
						END as username
					FROM
						table_logs tl
						LEFT JOIN company_users u ON ( tl.created_by = u.id AND tl.cid = u.cid AND ( tl.cid = ' . ( int ) $intCid . ' OR tl.cid = ' . CClients::$c_intNullCid . ' ) )
						LEFT JOIN company_employees e ON ( u.company_employee_id = e.id AND u.cid = e.cid AND ( u.cid = ' . ( int ) $intCid . ' OR u.cid = ' . CClients::$c_intNullCid . ' ) )
					WHERE
						tl.table_name IN( ' . $strPropertyRelatedTables . ' )
						AND ( tl.cid = ' . ( int ) $intCid . ' OR tl.cid = ' . ( int ) CClients::$c_intNullCid . ')'
									. ( ( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? ' AND ' : '' )
									. ( ( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? implode( ' AND ', $arrstrAndSearchParameters ) : '' );

		return $strSql;
	}

	public static function buildSqlForCompanyRelatedTablesByReferenceNumberByCid( $intReferenceNumber, $intCid, $strTableName ) {
		$strSql = 'SELECT
						DISTINCT ON(reference_number) reference_number,
						NULL::integer as property_id,
						NULL::varchar as website_name,
						tl.cid
					FROM
			 			table_logs tl
					WHERE
						tl.reference_number = ' . ( int ) $intReferenceNumber . '
						AND tl.table_name = \'' . $strTableName . '\'
						AND ( tl.cid = ' . ( int ) $intCid . ' OR tl.cid = ' . ( int ) CClients::$c_intNullCid . ')';

		return $strSql;
	}

	public static function buildSqlForApplicationsByReferenceNumberByCid( $intReferenceNumber, $intCid ) {

		$strSql = 'SELECT
						DISTINCT ON(reference_number) reference_number,
						ap.property_id as property_id,
						NULL::varchar as website_name,
						tl.cid
					FROM
						table_logs tl
						JOIN applications ap ON ( ap.id = tl.reference_number AND ap.cid = tl.cid )
					WHERE
						tl.reference_number = ' . ( int ) $intReferenceNumber . '
						AND tl.table_name = \'' . CTable::APPLICATIONS . '\'
						AND ( tl.cid = ' . ( int ) $intCid . ' OR tl.cid = ' . ( int ) CClients::$c_intNullCid . ')';

		return $strSql;
	}

	public static function buildSqlForWebsiteInfoByReferenceNumberByCid( $intReferenceNumber, $intCid ) {

		$arrstrWebsiteRelatedTables = [ CTable::WEBSITE_INFO, CTable::WEBSITE_TEMPLATE_SLOT ];

		$strWebsiteRelatedTables = "'" . implode( "', '", $arrstrWebsiteRelatedTables ) . "'";

		$strSql = 'SELECT
						DISTINCT ON(reference_number) reference_number,
						tl.reference_number as property_id,
						w.name as website_name,
						tl.cid
					FROM
						table_logs tl
						LEFT JOIN websites w ON ( tl.reference_number = w.id AND tl.cid = w.cid )
					WHERE
						tl.reference_number = ' . ( int ) $intReferenceNumber . '
						AND tl.table_name IN( ' . $strWebsiteRelatedTables . ' )
						AND ( tl.cid = ' . ( int ) $intCid . ' OR tl.cid = ' . ( int ) CClients::$c_intNullCid . ')';

		return $strSql;
	}

	public static function buildSqlForLateFeeFormulasByReferenceNumberByCid( $intReferenceNumber, $intCid, $boolIsUnion = false ) {

		$strSql = 'SELECT
						DISTINCT ON(reference_number) reference_number,
						lff.id AS late_fee_id, '
						. ( ( true == $boolIsUnion ) ? '' : ' plff.property_id, ' ) . '
						p.property_name as property_name,
						NULL::varchar as website_name,
						tl.cid
					FROM
						table_logs tl
						LEFT JOIN late_fee_formulas lff ON ( tl.reference_number = lff.id AND tl.cid = lff.cid )
						LEFT JOIN property_late_fee_formulas plff ON ( tl.cid = plff.cid AND lff.id = plff.late_fee_formula_id)
						LEFT JOIN properties p ON ( plff.property_id = p.id AND plff.cid = p.cid )
					WHERE
						tl.reference_number = ' . ( int ) $intReferenceNumber . '
						AND tl.table_name = \'' . CTable::LATE_FEE_FORMULAS . '\'
						AND ( tl.cid = ' . ( int ) $intCid . ' OR tl.cid = ' . ( int ) CClients::$c_intNullCid . ')';

		return $strSql;
	}

	public static function buildSqlForPropertyCallSettingsByReferenceNumberByCid( $intReferenceNumber, $intCid ) {

		$strSql = 'SELECT
						DISTINCT ON(reference_number) reference_number as property_id,
						NULL::varchar as website_name,
						cid
					FROM
						table_logs
					WHERE
						reference_number = ' . ( int ) $intReferenceNumber . '
						AND table_name = \'' . CTable::PROPERTY_CALL_SETTINGS . '\'
						AND ( cid = ' . ( int ) $intCid . ' OR cid = ' . ( int ) CClients::$c_intNullCid . ')';

		return $strSql;
	}

	public static function buildSqlForDocumentManagementRelatedTablesByReferenceNumberByCid( $intReferenceNumber, $intCid ) {

		$arrstrDocumentManagementRelatedTables = [
			CTable::FILE_NOTES,
			CTable::FILE_METADATAS,
			CTable::FILES,
			CTable::FILE_ASSOCIATIONS
		];

		$strDocumentManagementRelatedTables = "'" . implode( "', '", $arrstrDocumentManagementRelatedTables ) . "'";

		$strSql = 'SELECT
						DISTINCT ON( tl.reference_number ) reference_number,
						f.property_id as property_id,
						NULL::varchar as website_name,
						tl.*
					FROM
						table_logs tl
						JOIN files f ON ( f.id = tl.reference_number AND f.cid = tl.cid )
					WHERE
						tl.reference_number = ' . ( int ) $intReferenceNumber . '
						AND tl.table_name IN( ' . $strDocumentManagementRelatedTables . ' )
						AND ( tl.cid = ' . ( int ) $intCid . ' OR tl.cid = ' . ( int ) CClients::$c_intNullCid . ')';

		return $strSql;
	}

	public static function fetchOrderedTableLogsByTableNameByCid( $strTableName, $intCid, $objDatabase ) {
		return self::fetchTableLogs( 'SELECT * FROM table_logs WHERE cid = ' . ( int ) $intCid . ' AND table_name = \'' . $strTableName . '\'  ORDER BY id ASC', $objDatabase );
	}

	// this function is specific to retrieve contract system logs by contract id.

	public static function fetchPaginatedTableLogsGroupedByMinuteByContractIdByTableNameByCid( $intContractId, $strTableName, $intCid, $objDatabase, $objPagination = NULL, $boolReturnCountOnly = false ) {

		$strExtraSql = '';
		if( false == $boolReturnCountOnly ) {

			$strSql = 'SET STATEMENT_TIMEOUT = \'800s\';
							SELECT
								date_trunc( \'minute\', tl.created_on ) as log_date,
								count(1) as log_count ';
			$strExtraSql = ' GROUP BY
										date_trunc( \'minute\', tl.created_on )
								ORDER BY log_date DESC
								OFFSET ' . ( int ) $objPagination->getOffset() . '
								LIMIT ' . ( int ) $objPagination->getPageSize();
		} else {
			$strSql = 'SELECT count( DISTINCT date_trunc( \'minute\', tl.created_on ) ) AS count ';
		}

		$strSql .= 'FROM
						contract_properties cp
						JOIN table_logs tl ON ( tl.table_name = \'' . $strTableName . '\' AND tl.cid = ' . ( int ) $intCid . ' AND tl.reference_number = cp.id )
					WHERE
						cp.contract_id = ' . ( int ) $intContractId;

		$strSql .= $strExtraSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTableLogsByContractIdByTableNameByCreatedOnByCid( $intContractId, $strTableName, $strCreatedOn, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						tl.*
					FROM
						contract_properties cp
						JOIN table_logs tl ON ( tl.reference_number = cp.id AND tl.cid = cp.cid AND tl.cid = ' . ( int ) $intCid . ' AND tl.table_name = \'' . $strTableName . '\' )
					WHERE
						cp.contract_id = ' . ( int ) $intContractId . '
						AND date_trunc( \'minute\', tl.created_on ) = \'' . $strCreatedOn . '\'';

		return self::fetchTableLogs( $strSql, $objDatabase );
	}

	public static function fetchCustomAllTableNames( $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( table_name ) table_name
					FROM
						table_logs
					WHERE
						table_name IS NOT NULL
					GROUP BY
						table_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedChangeLogsByTableNamesByReferenceNumberByCid( $intPageNo, $intCountPerPage, $arrstrTableName, $intReferenceNumber, $intCid, $objDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intCountPerPage * ( $intPageNo - 1 ) : 0;

		$strJoinSql  = '';
		$strWhereSql = ' AND tl.reference_number = ' . ( int ) $intReferenceNumber;

		if( true == in_array( 'company_employees', $arrstrTableName ) ) {

			$strJoinSql = ' JOIN company_users cu1 ON (  cu1.cid = tl.cid AND cu1.id = ' . ( int ) $intReferenceNumber . ' )';

			$strWhereSql = ' AND( CASE
									WHEN tl.table_name = \'company_employees\' THEN tl.reference_number = cu1.company_employee_id
									ELSE tl.reference_number = cu1.id 
								END )';
		}

		$strSql = 'SELECT
						tl.*, 
						ce.name_first, 
						ce.name_last, 
						cu.username
					FROM 
						table_logs tl
						JOIN company_users cu ON ( cu.id = tl.created_by AND cu.cid = tl.cid ) AND cu.company_user_type_id IN( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						LEFT JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid )
						' . $strJoinSql . ' 
					WHERE
						tl.table_name IN ( \'' . implode( '\',\'', $arrstrTableName ) . '\'	)
						AND tl.cid = ' . ( int ) $intCid .
						$strWhereSql . '
					ORDER BY
						tl.created_on DESC, tl.id DESC
					OFFSET
						' . ( int ) $intOffset . '
					LIMIT
						' . ( int ) $intCountPerPage;

		return self::fetchTableLogs( $strSql, $objDatabase );
	}

	public static function fetchPaginatedChangeLogsCountByTableNamesByReferenceNumberByCid( $arrstrTableName, $intReferenceNumber, $intCid, $objDatabase ) {

		$strJoinSql  = '';
		$strWhereSql = ' AND tl.reference_number = ' . ( int ) $intReferenceNumber;

		if( true == in_array( 'company_employees', $arrstrTableName ) ) {

			$strJoinSql = ' JOIN company_users cu1 ON (  cu1.cid = tl.cid AND cu1.id = ' . ( int ) $intReferenceNumber . ' )';

			$strWhereSql = ' AND( CASE
									WHEN tl.table_name = \'company_employees\' THEN tl.reference_number = cu1.company_employee_id
									ELSE tl.reference_number = cu1.id 
								END )';
		}

		$strSql = 'SELECT
						count(tl.id)
					FROM
						table_logs tl
						JOIN company_users cu ON ( cu.id = tl.created_by AND cu.cid = tl.cid ) AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						LEFT JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid )
						' . $strJoinSql . ' 
					WHERE
						tl.table_name IN ( \'' . implode( '\',\'', $arrstrTableName ) . '\' )
						AND tl.cid = ' . ( int ) $intCid .
		                $strWhereSql;

		$arrstrData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) {
			return $arrstrData[0]['count'];
		}

		return 0;
	}

	public static function fetchTableLogsByCidByPropertyIdByTableName( $intCid, $intPropertyId, $strTableName, $objDatabase ) {

		if( true == empty( $intCid ) || true == empty( $intPropertyId ) || true == empty( $strTableName ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						tl.*
					FROM
						table_logs tl
					WHERE
						tl.cid = ' . ( int ) $intCid . '
						AND tl.table_name = \'' . $strTableName . '\'
						AND tl.reference_number = ' . ( int ) $intPropertyId . '
					ORDER BY
						tl.created_on DESC';

		return self::fetchTableLogs( $strSql, $objDatabase );
	}

	public static function fetchTableNameFromTableLogsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						table_name
					FROM
						table_logs
					WHERE
						cid = ' . ( int ) $intCid . '
					GROUP BY
						table_name
					ORDER BY
						table_name';

		$arrmixTableNames = fetchData( $strSql, $objDatabase );

		$arrstrTableNames = [];

		foreach( $arrmixTableNames as $arrmixTableName ) {
			$arrstrTableNames[$arrmixTableName['table_name']] = $arrmixTableName['table_name'];
		}

		return $arrstrTableNames;
	}

	public static function fetchUserNamesFromTableLogsByCid( $intCid, $objDatabase, $objAdminDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON(cu.id)
						cu.id, cu.username,cu.company_user_type_id
					FROM
						company_users cu
						JOIN table_logs tl ON( tl.cid = cu.cid AND tl.created_by = cu.id )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id IN( ' . implode( ' ,', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )';

		$arrmixUsernames = fetchData( $strSql, $objDatabase );

		$arrstrUsernames       = [];
		$arrintPSIAdminUserIds = [];

		foreach( $arrmixUsernames as $arrmixUsername ) {
			$arrstrUsernames[$arrmixUsername['id']] = $arrmixUsername['username'];

			if( true == in_array( $arrmixUsername['company_user_type_id'], CCompanyUserType::$c_arrintPsiUserTypeIds ) ) {
				$arrintPSIAdminUserIds[$arrmixUsername['id']] = $arrmixUsername['id'];
			}
		}

		$arrobjEmployees = ( true == valArr( $arrintPSIAdminUserIds ) ) ? CEmployees::fetchEmployeesByUserIds( $arrintPSIAdminUserIds, $objAdminDatabase ) : [];
		$arrobjEmployees = ( true == valArr( $arrobjEmployees ) ) ? rekeyObjects( 'UserId', $arrobjEmployees ) : [];

		foreach( $arrobjEmployees as $intUserId => $objEmployee ) {
			if( true == array_key_exists( $intUserId, $arrstrUsernames ) ) {
				$arrstrUsernames[$intUserId] = $objEmployee->getNameFirst() . ' ' . $objEmployee->getNameLast();
			}
		}

		return $arrstrUsernames;
	}

	public static function buildSqlForWebsiteRelatedTablesByCid( $intCid, $arrstrSearchParameters ) {

		$arrstrWebsiteRelatedTables = [
			CTable::WEBSITE_INFO,
			CTable::WEBSITE_TEMPLATE_SLOT,
			CTable::WEBSITE_PREFERENCES
		];

		$strWebsiteRelatedTables = "'" . implode( "', '", $arrstrWebsiteRelatedTables ) . "'";

		$arrstrAndSearchParameters = self::fetchSearchCriteria( $arrstrSearchParameters );

		$strSql = 'SELECT
						tl.*,
						tl.reference_number as website_id,
						NULL::varchar as website_name,
						CASE
							WHEN( e.name_first IS NOT NULL OR e.name_last IS NOT NULL )
							THEN
								CASE
									WHEN( e.name_first = e.name_last OR e.name_last IS NULL )
									THEN e.name_first
									WHEN( e.name_first IS NULL )
									THEN e.name_last
									ELSE e.name_first || \' \' || e.name_last
								END
							ELSE u.username
						END as username
					FROM
						table_logs tl
						LEFT JOIN company_users u ON ( tl.created_by = u.id AND tl.cid = u.cid AND ( tl.cid = ' . ( int ) $intCid . ' OR tl.cid = ' . CClients::$c_intNullCid . ' ) )
						LEFT JOIN company_employees e ON ( u.company_employee_id = e.id AND u.cid = e.cid AND ( u.cid = ' . ( int ) $intCid . ' OR u.cid = ' . CClients::$c_intNullCid . ' ) )
					WHERE
						tl.table_name IN( ' . $strWebsiteRelatedTables . ' )
						AND ( tl.cid = ' . ( int ) $intCid . ' OR tl.cid = ' . ( int ) CClients::$c_intNullCid . ')'
									. ( ( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? ' AND ' : '' )
									. ( ( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? implode( ' AND ', $arrstrAndSearchParameters ) : '' );

		return $strSql;
	}

	public static function buildSqlForArCodesByReferenceNumberByCid( $intReferenceNumber, $intCid ) {

		$strSql = 'SELECT
						DISTINCT ON(reference_number) reference_number as property_id,
						NULL::varchar as website_name,
						cid
					FROM
						table_logs
					WHERE
						reference_number = ' . ( int ) $intReferenceNumber . '
						AND table_name = \'' . CTable::AR_CODES . '\'
						AND ( cid = ' . ( int ) $intCid . ' OR cid = ' . ( int ) CClients::$c_intNullCid . ')';

		return $strSql;
	}

	public static function buildSqlForPropertyArCodesByReferenceNumberByCid( $intReferenceNumber, $intCid ) {

		$strSql = 'SELECT
						DISTINCT ON(reference_number) reference_number,
						pac.property_id,
						NULL::varchar as website_name,
						tl.cid
					FROM
						table_logs tl
						LEFT JOIN property_ar_codes pac ON ( tl.reference_number = pac.id AND tl.cid = pac.cid )
					WHERE
						tl.reference_number = ' . ( int ) $intReferenceNumber . '
						AND tl.table_name = \'' . CTable::PROPERTY_AR_CODES . '\'
						AND ( tl.cid = ' . ( int ) $intCid . ' OR tl.cid = ' . ( int ) CClients::$c_intNullCid . ')';

		return $strSql;
	}

	public static function buildSqlForPropertyChargeSettingsByReferenceNumberByCid( $intReferenceNumber, $intCid ) {

		$strSql = 'SELECT
						DISTINCT ON(reference_number) reference_number as property_id,
						NULL::varchar as website_name,
						cid
					FROM
						table_logs
					WHERE
						reference_number = ' . ( int ) $intReferenceNumber . '
						AND table_name = \'' . CTable::PROPERTY_CHARGE_SETTINGS . '\'
						AND ( cid = ' . ( int ) $intCid . ' OR cid = ' . ( int ) CClients::$c_intNullCid . ')';

		return $strSql;
	}

	public static function buildSqlForPropertyNotificationsByReferenceNumberByCid( $intReferenceNumber, $intCid ) {

		$strSql = 'SELECT
						DISTINCT ON(reference_number) reference_number AS property_id,
						NULL::varchar as website_name,
						cid
					FROM
						table_logs
					WHERE
						reference_number = ' . ( int ) $intReferenceNumber . '
						AND table_name = \'' . CTable::PROPERTY_NOTIFICATIONS . '\'
						AND ( cid = ' . ( int ) $intCid . ' OR cid = ' . ( int ) CClients::$c_intNullCid . ')';

		return $strSql;
	}

	public static function buildSqlForPropertyLeadSourcesByReferenceNumberByCid( $intReferenceNumber, $intCid ) {

		$strSql = 'SELECT
						DISTINCT ON( reference_number ) reference_number,
						tl.cid,
						pls.property_id,
						NULL::varchar as website_name
					FROM
						table_logs tl
						JOIN property_lead_sources pls ON ( pls.lead_source_id = tl.reference_number AND pls.cid = tl.cid )
					WHERE
						tl.reference_number = ' . ( int ) $intReferenceNumber . '
						AND tl.table_name = \'' . CTable::PROPERTY_LEAD_SOURCES . '\'
						AND ( tl.cid = ' . ( int ) $intCid . ' OR tl.cid = ' . ( int ) CClients::$c_intNullCid . ')';

		return $strSql;
	}

	public static function fetchSimpleTableLogsForPricingSettingsByCidByPropertyIdByArOriginId( $intCid, $intPropertyId, $intArOriginId, $objDatabase ) {

		$strSql = 'SELECT
						tl.id,
						tl.cid,
						paor.property_id,
						paor.ar_origin_id,
						tl.old_data,
						tl.new_data,
						CASE
							WHEN( e.name_first IS NOT NULL OR e.name_last IS NOT NULL )
							THEN
								CASE
									WHEN ( e.name_first = e.name_last OR e.name_last IS NULL )
									THEN e.name_first
									WHEN ( e.name_first IS NULL )
									THEN e.name_last
									ELSE e.name_first || \' \' || e.name_last
								END
							ELSE u.username
						END as username,
						tl.created_on
					FROM
						table_logs tl
						JOIN property_ar_origin_rules paor ON ( paor.cid = tl.cid AND paor.property_id = tl.reference_number AND paor.ar_origin_id = ' . ( int ) $intArOriginId . ' )
						LEFT JOIN company_users u ON ( tl.created_by = u.id AND tl.cid = u.cid AND ( tl.cid = ' . ( int ) $intCid . ' OR tl.cid = ' . CClients::$c_intNullCid . ' ) )
						LEFT JOIN company_employees e ON ( u.company_employee_id = e.id AND u.cid = e.cid AND ( u.cid = ' . ( int ) $intCid . ' OR u.cid = ' . CClients::$c_intNullCid . ' ) )
					WHERE
						tl.cid = ' . ( int ) $intCid . '
						AND tl.table_name = \'property_ar_origin_rules\'
						AND tl.reference_number = ' . ( int ) $intPropertyId . '
						AND tl.old_data ILIKE \'%' . \Psi\CStringService::singleton()->strtolower( CArOrigin::getIdToCamelizeName( $intArOriginId ) ) . '%\'
						AND tl.new_data ILIKE \'%' . \Psi\CStringService::singleton()->strtoupper( CArOrigin::getIdToCamelizeName( $intArOriginId ) ) . '%\'
					ORDER BY
						tl.created_on DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleTableLogForQuickResponseByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						tl.*
					FROM
						table_logs tl
						LEFT JOIN quick_responses qr ON ( tl.reference_number = qr.id AND tl.cid = qr.cid )
					WHERE
						tl.id = ' . ( int ) $intId . '
						AND tl.cid = ' . ( int ) $intCid;

		return self::fetchTableLog( $strSql, $objDatabase );
	}

	public static function fetchOrderedTableLogsByTableNameByCidByReferenceNumber( $strTableName, $intCid, $intReferenceNumber, $objDatabase ) {
		if( !valStr( $strTableName ) || !valId( $intCid ) || !valId( $intReferenceNumber ) ) {
			return NULL;
		}

		return self::fetchTableLogs( 'SELECT * FROM table_logs WHERE cid = ' . $intCid . ' AND reference_number = ' . $intReferenceNumber . ' AND table_name = \'' . $strTableName . '\'  ORDER BY id', $objDatabase );
	}

}

?>