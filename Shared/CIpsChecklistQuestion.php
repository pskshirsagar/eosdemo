<?php

class CIpsChecklistQuestion extends CBaseIpsChecklistQuestion {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIpsChecklistId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valQuestion() {
		$boolIsValid = true;

		if( false == valStr( $this->getQuestion() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'question', 'Question is required.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valIsRequiresSync() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valQuestion();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function validateCustomClientDatabases( $intCountAdminIpsChecklistQuestions, $arrobjClientDatabases, $intCurrentVersionId = NULL ) {

		$boolIsAdminEntrataSimilar 				= true;
		$boolIsSyncAllChecklistsAndQuestions 	= false;
		$strClientDatabaseNames = '';
		foreach( $arrobjClientDatabases as $objClientDatabase ) {
			$objClientDatabase->open();

			if( true == valId( $intCurrentVersionId ) ) {
				$arrobjIpsChecklistQuestions = \Psi\Eos\Entrata\CIpsChecklistQuestions::createService()->fetchIpsChecklistQuestionsByIpsPortalVersionId( $intCurrentVersionId, $objClientDatabase );
				if( false == valArr( $arrobjIpsChecklistQuestions ) ) {
					$boolIsSyncAllChecklistsAndQuestions = true;
				}
			}

			$intCountEntrataIpsChecklistQuestions = ( int ) \Psi\Eos\Entrata\CIpsChecklistQuestions::createService()->fetchIpsChecklistQuestionCount( NULL, $objClientDatabase );

			if( $intCountAdminIpsChecklistQuestions < $intCountEntrataIpsChecklistQuestions ) {
				$boolIsAdminEntrataSimilar &= false;
				$strClientDatabaseNames    .= $objClientDatabase->getDatabaseName() . ' ( ' . ( int ) $intCountEntrataIpsChecklistQuestions . ' ), ';
			}
		}

		if( true == valStr( $strClientDatabaseNames ) ) {
			$strClientDatabaseNames = trim( $strClientDatabaseNames, ', ' );
		}

		if( true == valId( $intCurrentVersionId ) ) {
			return [ $boolIsAdminEntrataSimilar, $strClientDatabaseNames, $boolIsSyncAllChecklistsAndQuestions ];
		} else {
			return [ $boolIsAdminEntrataSimilar, $strClientDatabaseNames ];
		}
	}

	public static function fetchIsRequireSyncCountByChecklistId( $intIpsChecklistId, $objDatabase ) {

		if( false == valId( $intIpsChecklistId ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						SUM(CASE
							WHEN icq.is_requires_sync = true
							THEN 1
							ELSE 0
							END) as ips_checklist_question_require_sync_count
					FROM
						ips_checklist_questions icq
					WHERE
						icq.ips_checklist_id = ' . $intIpsChecklistId;

		return ( int ) reset( fetchData( $strSql, $objDatabase ) )['ips_checklist_question_require_sync_count'];
	}

}
?>