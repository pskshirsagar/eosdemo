<?php

class CApHeaderSubType extends CBaseApHeaderSubType {

	const RFP_BUDGET					= 1;
	const JOB_PHASE_BUDGET				= 2;
	const CONTRACT_BUDGET				= 3;
	const STANDARD_PURCHASE_ORDER		= 4;
	const STANDARD_INVOICE				= 5;
	const MANAGEMENT_FEE_INVOICE		= 6;
	const OWNER_DISTRIBUTION_INVOICE	= 7;
	const CATALOG_INVOICE				= 8;
	const JOB_CHANGE_ORDER				= 9;
	const CONTRACT_CHANGE_ORDER			= 10;
	const CATALOG_PURCHASE_ORDER		= 11;
	const CREDIT_MEMO_INVOICE			= 12;
	const JOB_BUDGET_SUMMARY			= 13;
	const CONTRACT_BUDGET_SUMMARY		= 14;
	const UNCLAIMED_PROPERTY_PAYMENT	= 15;
	const JOB_BUDGET_ADJUSTMENT			= 16;
	const STANDARD_JOB_INVOICE			= 17;
	const CATALOG_JOB_INVOICE			= 18;
	const STANDARD_JOB_PURCHASE_ORDER	= 19;
	const CATALOG_JOB_PURCHASE_ORDER	= 20;
	const JOB_TYPE						= 'Job';
	const CONTRACT_TYPE					= 'Contract';

	public static $c_arrintInvoiceSubTypes = [
		self::STANDARD_INVOICE,
		self::MANAGEMENT_FEE_INVOICE,
		self::OWNER_DISTRIBUTION_INVOICE,
		self::CATALOG_INVOICE,
		self::CREDIT_MEMO_INVOICE,
		self::STANDARD_JOB_INVOICE,
		self::CATALOG_JOB_INVOICE
	];

	public static $c_arrstrApHeaderSubTypes = [
		self::STANDARD_INVOICE		=> 'Standard',
		self::STANDARD_JOB_INVOICE	=> 'Standard-Job/Contract',
		self::CATALOG_INVOICE		=> 'Catalog',
		self::CATALOG_JOB_INVOICE	=> 'Catalog Job/Contract',
		self::CREDIT_MEMO_INVOICE	=> 'Credit Memo'
	];

	public static $c_arrstrPoApHeaderSubTypes = [
		self::STANDARD_PURCHASE_ORDER		=> 'Standard',
		self::STANDARD_JOB_PURCHASE_ORDER	=> 'Standard-Job/Contract',
		self::CATALOG_PURCHASE_ORDER		=> 'Catalog'
	];

	public static $c_arrstrSyncToVaPoTypes = [
		self::STANDARD_PURCHASE_ORDER		=> 'Standard',
		self::STANDARD_JOB_PURCHASE_ORDER	=> 'Standard-Job/Contract',
		self::CATALOG_PURCHASE_ORDER	=> 'Catalog'
	];

	public static $c_arrintStandardPOs = [
		self::STANDARD_PURCHASE_ORDER => self::STANDARD_PURCHASE_ORDER,
		self::STANDARD_JOB_PURCHASE_ORDER => self::STANDARD_JOB_PURCHASE_ORDER
	];

	public static $c_arrmixSmartyConstants = [
		'STANDARD_INVOICE'				=> self::STANDARD_INVOICE,
		'CATALOG_INVOICE'				=> self::CATALOG_INVOICE,
		'CREDIT_MEMO_INVOICE'			=> self::CREDIT_MEMO_INVOICE,
		'STANDARD_PURCHASE_ORDER'		=> self::STANDARD_PURCHASE_ORDER,
		'CATALOG_PURCHASE_ORDER'		=> self::CATALOG_PURCHASE_ORDER,
		'MANAGEMENT_FEE_INVOICE'		=> self::MANAGEMENT_FEE_INVOICE,
		'STANDARD_JOB_INVOICE'			=> self::STANDARD_JOB_INVOICE,
		'CATALOG_JOB_INVOICE'			=> self::CATALOG_JOB_INVOICE,
		'STANDARD_JOB_PURCHASE_ORDER'	=> self::STANDARD_JOB_PURCHASE_ORDER,
		'CATALOG_JOB_PURCHASE_ORDER'	=> self::CATALOG_JOB_PURCHASE_ORDER,
		'JOB_PHASE_BUDGET' 				=> self::JOB_PHASE_BUDGET,
		'CONTRACT_BUDGET'				=> self::CONTRACT_BUDGET,
		'JOB'							=> self::JOB_TYPE,
		'CONTRACT'						=> self::CONTRACT_TYPE
	];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getApHeaderSubTypes() :array {
		return [
			self::STANDARD_INVOICE		=> __( 'Standard' ),
			self::STANDARD_JOB_INVOICE	=> __( 'Standard Job/Contract' ),
			self::CATALOG_INVOICE		=> __( 'Catalog' ),
			self::CATALOG_JOB_INVOICE	=> __( 'Catalog Job/Contract' ),
			self::CREDIT_MEMO_INVOICE	=> __( 'Credit Memo' )
		];
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApHeaderTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public static function assignSmartyConstants( $objSmarty ) {
		$objSmarty->assign( 'STANDARD_INVOICE',				self::STANDARD_INVOICE );
		$objSmarty->assign( 'CATALOG_INVOICE',				self::CATALOG_INVOICE );
		$objSmarty->assign( 'CREDIT_MEMO_INVOICE',			self::CREDIT_MEMO_INVOICE );
		$objSmarty->assign( 'STANDARD_PURCHASE_ORDER',		self::STANDARD_PURCHASE_ORDER );
		$objSmarty->assign( 'CATALOG_PURCHASE_ORDER',		self::CATALOG_PURCHASE_ORDER );
		$objSmarty->assign( 'MANAGEMENT_FEE_INVOICE',		self::MANAGEMENT_FEE_INVOICE );
		$objSmarty->assign( 'STANDARD_JOB_INVOICE',			self::STANDARD_JOB_INVOICE );
		$objSmarty->assign( 'CATALOG_JOB_INVOICE',			self::CATALOG_JOB_INVOICE );
		$objSmarty->assign( 'STANDARD_JOB_PURCHASE_ORDER',	self::STANDARD_JOB_PURCHASE_ORDER );
		$objSmarty->assign( 'CATALOG_JOB_PURCHASE_ORDER',	self::CATALOG_JOB_PURCHASE_ORDER );
		$objSmarty->assign( 'JOB_PHASE_BUDGET', 			self::JOB_PHASE_BUDGET );
		$objSmarty->assign( 'CONTRACT_BUDGET',				self::CONTRACT_BUDGET );
		$objSmarty->assign( 'JOB',							self::JOB_TYPE );
		$objSmarty->assign( 'CONTRACT',						self::CONTRACT_TYPE );
		$objSmarty->assign( 'OWNER_DISTRIBUTION_INVOICE',	self::OWNER_DISTRIBUTION_INVOICE );
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>