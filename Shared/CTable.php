<?php

class CTable extends CBaseTable {

	protected $m_arrobjUserTables;

	const COMPANY_PREFERENCES	 			= 'company_preferences';
	const PROPERTY_PREFERENCES 				= 'property_preferences';
	const PROPERTY_GL_SETTINGS 				= 'property_gl_settings';
	const PROPERTY_AR_ORIGIN_RULES			= 'property_ar_origin_rules';
	const PROPERTY_LATE_FEE_FORMULAS		= 'property_late_fee_formulas';
	const PROPERTIES	         			= 'properties';
	const PROPERTY_MERCHANT_ACCOUNTS		= 'property_merchant_accounts';
	const GL_SETTINGS 						= 'gl_settings';
	const COMPANY_MERCHANT_ACCOUNTS 		= 'company_merchant_accounts';
	const ACCOUNTS							= 'accounts';
	const AR_PAYMENTS						= 'ar_payments';
	const WEBSITE_PREFERENCES				= 'website_preferences';
	const CLIENTS							= 'clients';
	const APPLICATIONS						= 'applications';
	const GL_TREES							= 'gl_trees';
	const INTEGRATION_DATABASES				= 'integration_databases';
	const INTEGRATION_CLIENTS				= 'integration_clients';
	const INTEGRATION_CLIENT_KEY_VALUES	   = 'integration_client_key_values';
	const CONTRACT_PROPERTIES				= 'contract_properties';
	const LATE_FEE_FORMULAS					= 'late_fee_formulas';
	const WEBSITE_INFO						= 'website_info';
	const TEMPLATE_SLOT						= 'template_slot';
	const WEBSITE_TEMPLATE_SLOT				= 'website_template_slot';
	const COMPANY_USER_GROUPS				= 'company_user_groups';
	const COMPANY_USER_PROPERTIES		 	= 'company_user_properties';
	const COMPANY_USER_PROPERTY_GROUPS 		= 'company_user_property_groups';
	const COMPANY_USER_WEBSITES				= 'company_user_websites';
	const COMPANY_USER_SERVICES				= 'company_user_services';
	const COMPANY_USER_PERMISSIONS			= 'company_user_permissions';
	const COMPANY_USER_PREFERENCES          = 'company_user_preferences';
	const COMPANY_USERS						= 'company_users';
	const COMPANY_EMPLOYEES					= 'company_employees';
	const COMPANY_GROUPS					= 'company_groups';
	const COMPANY_GROUP_PERMISSIONS			= 'company_group_permissions';
	const PROPERTY_CALL_SETTINGS			= 'property_call_settings';
	const RECORDS							= 'records';
	const DOMAINS							= 'domains';
	const AR_CODES							= 'ar_codes';
	const PROPERTY_CHARGE_SETTINGS			= 'property_charge_settings';
	const PROPERTY_AR_CODES					= 'property_ar_codes';
	const FILES								= 'files';
	const FILE_NOTES						= 'file_notes';
	const FILE_ASSOCIATIONS					= 'file_associations';
	const FILE_METADATAS					= 'file_metadatas';
	const PROPERTY_NOTIFICATIONS			= 'property_notifications';
	const COMPANY_GROUP_PROPERTY_GROUPS 	= 'company_group_property_groups';
	const PROPERTY_LEAD_SOURCES				= 'property_lead_sources';
	const MAINTENANCE_STATUSES				= 'maintenance_statuses';
	const MAINTENANCE_PRIORITIES			= 'maintenance_priorities';
	const COMPANY_GROUP_PREFERENCES			= 'company_group_preferences';
	const APPROVAL_PREFERENCES				= 'approval_preferences';
	const COMPARABLE_ASSOCIATIONS			= 'comparable_associations';
	const PROPERTY_INSTRUCTIONS				= 'property_instructions';
	const COMPANY_EMPLOYEE_CONTACTS			= 'company_employee_contacts';
	const COMPANY_GL_SETTINGS				= 'company_gl_settings';
	const MAINTENANCE_PROBLEMS				= 'maintenance_problems';
	const PROPERTY_QUICK_RESPONSES			= 'property_quick_responses';
	const PROPERTY_SELLING_POINTS			= 'property_selling_points';
	const QUICK_RESPONSES					= 'quick_responses';
	const SECURE_DELETED_DOMAINS			= 'secure_deleted_domains';
	const AR_TRANSACTIONS		            = 'ar_transactions';

    /**
     * Add Functions
     */

    public function addUserTable( $objUserTable ) {
    	$this->m_arrobjUserTables[$objUserTable->getId()] = $objUserTable;
    }

    /**
     * Get Functions
     *
     */

    public function getUserTable() {
    	return $this->m_arrobjUserTables;
    }

    /**
     * Validate Functions
     *
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDatabaseId() {
        $boolIsValid = true;
        if( true == is_null( $this->getDatabaseId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Database id is required.' ) );
        }
        return $boolIsValid;
    }

    public function valPsProductId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTeamId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTableName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valKeywords() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLastOptimized() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valHasFilter() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsComposite() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
    	$boolIsValid = true;

    	switch( $strAction ) {
    		case VALIDATE_INSERT:
    		case VALIDATE_UPDATE:
    			$boolIsValid &= $this->valDatabaseId();
    			break;

    		case VALIDATE_DELETE:
    			break;

    		default:
    			// default case
    			break;
    	}

    	return $boolIsValid;
    }

}
?>