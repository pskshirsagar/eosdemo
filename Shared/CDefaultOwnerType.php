<?php

class CDefaultOwnerType extends CBaseDefaultOwnerType {

	const SOLE_PROPRIETORSHIP	= 1;
	const PARTNERSHIP			= 2;
	const S_CORPORATION			= 3;
	const C_CORPORATION			= 4;
	const LLC					= 5;
	const CLIENT				= 6;
	const TEMPORARY				= 7;
	const HUD_OWNED				= 8;
	const SINGLE_MEMBER_LLC     = 9;
	const TRUST_ESTATE          = 10;
	const LLC_S_CORPORATION     = 11;
    const LLC_P_PARTNERSHIP		= 12;
    const NONPROFIT             = 13;

	public static $c_arrintOwnerTypesForLegalEntity = [ self::SOLE_PROPRIETORSHIP, self::PARTNERSHIP, self::S_CORPORATION, self::C_CORPORATION, self::LLC ];

	public static $c_arrstrOwnerTypesForLegalEntity = [
		self::LLC					=> 'LLC C Corporation',
		self::TEMPORARY				=> 'Temporary',
		self::C_CORPORATION			=> 'C Corporation',
		self::S_CORPORATION			=> 'S Corporation',
		self::PARTNERSHIP			=> 'Partnership',
		self::SOLE_PROPRIETORSHIP	=> 'Individual/Sole Proprietor',
		self::SINGLE_MEMBER_LLC		=> 'Single-member LLC',
		self::LLC_S_CORPORATION		=> 'LLC S Corporation',
		self::LLC_P_PARTNERSHIP		=> 'LLC Partnership',
		self::TRUST_ESTATE			=> 'Trust/Estate',
		self::NONPROFIT	            => 'Nonprofit'
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>