<?php

class CFrequency extends CBaseFrequency {

	protected $m_arrstrScheduledBlackoutDateFrequencies;
	protected $m_strName;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	const ONCE             = 1;
	const DAILY            = 2;
	const WEEKLY           = 3;
	const MONTHLY          = 4;
	const QUARTERLY        = 5;
	const YEARLY           = 6;
	const LEASE_EXPIRATION = 7;
	const MOVE_IN          = 8;
	const WEEKDAY_OF_MONTH = 9;
	const SEMI_ANNUALLY    = 10;
	const EVERY_TWO_WEEKS  = 11;
	const TWICE_PER_MONTH  = 12;
	const MANUAL           = 13;
	const BI_MONTHLY       = 14;
	const HOURLY           = 15;
	const PERIOD_ADVANCE   = 16;
	const PERIOD_LOCK      = 17;

	const FREQUENCY_NAME_MONTHLY		= 'Monthly';
	const FREQUENCY_NAME_BI_MONTHLY		= 'Bi-Monthly';
	const FREQUENCY_NAME_QUARTERLY		= 'Quarterly';
	const FREQUENCY_NAME_BI_ANNUALLY	= 'Bi-Annually';
	const FREQUENCY_NAME_SEMI_ANNUALLY	= 'Semi-Annually';
	const FREQUENCY_NAME_YEARLY			= 'Yearly';

	public static $c_arrintScheduledJEFrequencies = [ self::DAILY, self::WEEKLY, self::MONTHLY, self::YEARLY, self::WEEKDAY_OF_MONTH ];
	public static $c_arrintScheduledEmailFrequencies = [ self::DAILY, self::WEEKLY, self::MONTHLY, self::QUARTERLY, self::YEARLY, self::SEMI_ANNUALLY ];
	public static $c_arrintScheduledApTransactionFrequencies = [ self::DAILY, self::WEEKLY, self::MONTHLY, self::YEARLY ];
	public static $c_arrintContractPropertyChargesFrequencyIds = [ self::MONTHLY, self::QUARTERLY, self::SEMI_ANNUALLY, self::YEARLY ];
	public static $c_arrintContractPropertyChargesNewFrequencyIds = [ self::MONTHLY, self::QUARTERLY, self::YEARLY ];
	public static $c_arrintPeriodFrequencies = [ self::PERIOD_ADVANCE, self::PERIOD_LOCK ];
	public static $c_arrintScheduledBasicFrequencies = [ self::DAILY, self::WEEKLY, self::MONTHLY ];
	public static $c_arrintQbeFrequencyIds = [ self::QUARTERLY, self::SEMI_ANNUALLY, self::YEARLY ];
	public static $c_arrintReviewFrequencies = [ self::DAILY, self::WEEKLY, self::MONTHLY, self::QUARTERLY, self::YEARLY, self::SEMI_ANNUALLY ];
	public static $c_arrintContractFrequencies = [ self::MONTHLY, self::QUARTERLY, self::YEARLY ];

	public static $c_arrstrIncomeFrequencies  = [
        'weekly'		=> CFrequency::WEEKLY,
        'bi-weekly'		=> CFrequency::EVERY_TWO_WEEKS,
        'semi-monthly'	=> CFrequency::TWICE_PER_MONTH,
        'monthly'		=> CFrequency::MONTHLY,
        'annually'		=> CFrequency::YEARLY
	];

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['name'] ) ) {
			$this->setNontranslatedName( $arrmixValues['name'] );
		}
	}

	public function getScheduledBlackoutDateFrequencies() {
		if( true == isset( $this->m_arrstrScheduledBlackoutDateFrequencies ) ) {
			return $this->m_arrstrScheduledBlackoutDateFrequencies;
		}

		$this->m_arrstrScheduledBlackoutDateFrequencies = [
			self::DAILY				=> __( 'daily' ),
			self::WEEKLY			=> __( 'weekly' ),
			self::MONTHLY			=> __( 'monthly' ),
			self::YEARLY			=> __( 'yearly' ),
			self::WEEKDAY_OF_MONTH	=> __( 'weekday' )
		];

		return $this->m_arrstrScheduledBlackoutDateFrequencies;
	}

	public static $c_arrstrContractPricingFrequencies	= [ self::MONTHLY => 'monthly', self::QUARTERLY => 'Quarterly', self::SEMI_ANNUALLY => 'Semi-Annually', self::YEARLY => 'yearly' ];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function getFrequencyLabelWithInterval( $intInterval ) {
		if( true == $intInterval ) {
			return $this->getName();

		} else {
			$strLabelBody = ( 'Daily' == $this->getName() ) ? 'Days' : str_replace( 'ly', 's', $this->getName() );

			return 'Every ' . ( int ) $intInterval . ' ' . $strLabelBody;
		}
	}

	public function getIntervalFrequencyLabel() {
		return ( 'Daily' == self::getName() ) ? 'Day(s)' : str_replace( 'ly', '(s)', self::getName() );
	}

	public function getIntervalFrequencyLabelPlural() {
		return ( 'Daily' == self::getName() ) ? 'Days' : str_replace( 'ly', 's', self::getName() );
	}

	public static function getIntervalFrequencyInDays( $intFrequency ) {
		switch( $intFrequency ) {

			case self::DAILY:
				$intIntervalFrequencyInDays = 1;
				break;

			case self::ONCE:
			case self::YEARLY:
				$intIntervalFrequencyInDays = 365;
				break;

			case self::WEEKLY:
				$intIntervalFrequencyInDays = 7;
				break;

			case self::MONTHLY:
				$intIntervalFrequencyInDays = 30;
				break;

			case self::QUARTERLY:
				$intIntervalFrequencyInDays = 90;
				break;

			case self::SEMI_ANNUALLY:
				$intIntervalFrequencyInDays = 180;
				break;

			case self::BI_MONTHLY:
				$intIntervalFrequencyInDays = 60;
				break;

			default:
				$intIntervalFrequencyInDays = 365;
				break;
		}

		return $intIntervalFrequencyInDays;
	}

	public static function frequencyIdToStr( $intFrequencyId, $boolNewFormat = false ) {

		switch( $intFrequencyId ) {
			case self::ONCE:
				return __( 'Once' );
				break;

			case self::HOURLY:
				return __( 'Hourly' );
				break;

			case self::DAILY:
				return __( 'Daily' );
				break;

			case self::WEEKLY:
				return __( 'Weekly' );
				break;

			case self::MONTHLY:
				return __( 'Monthly' );
				break;

			case self::QUARTERLY:
				return __( 'Quarterly' );
				break;

			case self::YEARLY:
				return ( true == $boolNewFormat ) ? __( 'Annually' ) : __( 'Yearly' );
				break;

			case self::LEASE_EXPIRATION:
				return __( 'Lease Expiration' );
				break;

			case self::MOVE_IN:
				return __( 'Move In' );
				break;

			case self::WEEKDAY_OF_MONTH:
				return __( 'Weekday of the Month' );
				break;

			case self::SEMI_ANNUALLY:
				return __( 'Semi-Annually' );
				break;

			case self::EVERY_TWO_WEEKS:
				return __( 'Bi-weekly' );
				break;

			case self::TWICE_PER_MONTH:
				return __( 'Semi-monthly' );
				break;

			default:
				// default case
				break;
		}
	}

	public function getCustomIntervalFrequencyLabelPlural() {
		return ( 'Daily' == self::getNontranslatedName() ) ? 'Days' : str_replace( 'ly', 's', self::getNontranslatedName() );
	}

	public function setNontranslatedName( $strName ) {
		$this->m_strName = $strName;
	}

	public function getNontranslatedName() {
		return $this->m_strName;
	}

	public static function getFrequencyByDays( $intFrequencyInDays ) {

		switch( $intFrequencyInDays ) {

			case ( 1 == $intFrequencyInDays ):
				$intFrequency = self::DAILY;
				break;

			case ( $intFrequencyInDays > 1 && $intFrequencyInDays <= 7 ):
				$intFrequency = self::WEEKLY;
				break;

			case ( $intFrequencyInDays > 7 && $intFrequencyInDays <= 30 ):
				$intFrequency = self::MONTHLY;
				break;

			case ( $intFrequencyInDays > 30 && $intFrequencyInDays <= 60 ):
				$intFrequency = self::BI_MONTHLY;
				break;

			case ( $intFrequencyInDays > 60 && $intFrequencyInDays <= 90 ):
				$intFrequency = self::QUARTERLY;
				break;

			case ( $intFrequencyInDays > 90 && $intFrequencyInDays <= 180 ):
				$intFrequency = self::SEMI_ANNUALLY;
				break;

			case ( $intFrequencyInDays > 180 ):
				$intFrequency = self::YEARLY;
				break;

			default:
				$intFrequency = self::MONTHLY;
				break;
		}

		return $intFrequency;
	}

	public static function getFrequencyByDaysForNewAccount( $intFrequencyInDays ) {

		switch( $intFrequencyInDays ) {

			case ( 1 == $intFrequencyInDays ):
				$intFrequency = self::DAILY;
				break;

			case ( $intFrequencyInDays > 1 && $intFrequencyInDays <= 7 ):
				$intFrequency = self::WEEKLY;
				break;

			case ( $intFrequencyInDays > 7 && $intFrequencyInDays <= 32 ):
				$intFrequency = self::MONTHLY;
				break;

			case ( $intFrequencyInDays > 32 && $intFrequencyInDays <= 60 ):
				$intFrequency = self::BI_MONTHLY;
				break;

			case ( $intFrequencyInDays > 60 && $intFrequencyInDays <= 90 ):
				$intFrequency = self::QUARTERLY;
				break;

			case ( $intFrequencyInDays > 90 && $intFrequencyInDays <= 180 ):
				$intFrequency = self::SEMI_ANNUALLY;
				break;

			case ( $intFrequencyInDays > 180 ):
				$intFrequency = self::YEARLY;
				break;

			default:
				$intFrequency = self::MONTHLY;
				break;
		}

		return $intFrequency;
	}

}

?>