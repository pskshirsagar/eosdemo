<?php

class CMilitaryHousingArea extends CBaseMilitaryHousingArea {

	protected $m_arrobjMilitaryHousingAreaZipcodes;
	protected $m_arrintMilitaryHousingAreaZipcodeIds;

	public function __construct() {
		parent::__construct();

		$this->m_arrobjMilitaryHousingAreaZipcodes = [];
		$this->m_arrintMilitaryHousingAreaZipcodeIds = [];

		return;
	}

	public function getMilitaryHousingAreaZipcodes() {
		return $this->m_arrobjMilitaryHousingAreaZipcodes;
	}

	public function getMilitaryHousingAreaZipCodeIds() {
		return $this->m_arrobjMilitaryHousingAreaZipcodeIds;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['zip_codes'] ) )							$this->setMilitaryHousingAreaZipcodeIds( explode( ',', $arrmixValues['zip_codes'] ) );
		return;
	}

	public function setMilitaryHousingAreaZipcodes( $arrobjMilitaryHousingAreaZipcodes ) {
		$this->m_arrobjMilitaryHousingAreaZipcodes = $arrobjMilitaryHousingAreaZipcodes;
	}

	public function setMilitaryHousingAreaZipcodeIds( $arrobjMilitaryHousingAreaZipcodeIds ) {
		$this->m_arrobjMilitaryHousingAreaZipcodeIds = $arrobjMilitaryHousingAreaZipcodeIds;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExternalReference() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '
					VALUES ( ' .
			$strId . ', ' .
			$this->sqlName() . ', ' .
			$this->sqlExternalReference() . ', ' .
			$this->sqlIsPublished() . ', ' .
			$this->sqlOrderNum() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = array();
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' name = ' . $this->sqlName() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlName() ) != $this->getOriginalValueByFieldName( 'name' ) ) {
			$arrstrOriginalValueChanges['name'] = $this->sqlName();
			$strSql .= ' name = ' . $this->sqlName() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' external_reference = ' . $this->sqlExternalReference() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlExternalReference() ) != $this->getOriginalValueByFieldName( 'external_reference' ) ) {
			$arrstrOriginalValueChanges['external_reference'] = $this->sqlExternalReference();
			$strSql .= ' external_reference = ' . $this->sqlExternalReference() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' is_published = ' . $this->sqlIsPublished() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlIsPublished() ) != $this->getOriginalValueByFieldName( 'is_published' ) ) {
			$arrstrOriginalValueChanges['is_published'] = $this->sqlIsPublished();
			$strSql .= ' is_published = ' . $this->sqlIsPublished() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' order_num = ' . $this->sqlOrderNum() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlOrderNum() ) != $this->getOriginalValueByFieldName( 'order_num' ) ) {
			$arrstrOriginalValueChanges['order_num'] = $this->sqlOrderNum();
			$strSql .= ' order_num = ' . $this->sqlOrderNum() . ',';
			$boolUpdate = true;
		}

		$strSql = rtrim( $strSql, ',' );
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

}
?>