<?php

class CPropertyType extends CBasePropertyType {

	const APARTMENT 				= 1;
	const SINGLE_FAMILY				= 2;
	const STORAGE 					= 3;
	const STUDENT 					= 4;
	const MILITARY 					= 5;
	const SUBSIDIZED				= 6;
	const HOME_OWNERS_ASSOCIATION	= 7;
	const COMMERCIAL 				= 8;
	const RETAIL	 				= 9;
	const SENIOR					= 10;
	const MIXED_USE					= 11;
	const TEMPLATE					= 38;
	const CORPORATE					= 97;
	const CLIENT					= 98;
	const SETTINGS_TEMPLATE			= 99;
	const CENTRAL_LEASING_OFFICE	= 100;

	public static $c_arrintExcludePropertyTypeIds = [
		CPropertyType::SETTINGS_TEMPLATE,
		CPropertyType::CLIENT,
		CPropertyType::TEMPLATE
	];

	public static $c_arrintExcludePropertyTypeIdsForPackages = [
		self::SETTINGS_TEMPLATE,
		self::TEMPLATE,
		self::CORPORATE
	];

	public static $c_arrintExcludePropertyTypeIdsForContracts = [
		self::SETTINGS_TEMPLATE,
		self::TEMPLATE
	];

	public static $c_arrintPropertyTypeIds = [
		self::APARTMENT,
		self::SINGLE_FAMILY,
		self::STORAGE,
		self::STUDENT,
		self::MILITARY,
		self::SUBSIDIZED,
		self::HOME_OWNERS_ASSOCIATION,
		self::COMMERCIAL,
		self::RETAIL,
		self::SENIOR,
		self::MIXED_USE,
		self::TEMPLATE,
		self::CORPORATE,
		self::CLIENT,
		self::SETTINGS_TEMPLATE
	];

	protected $m_boolIsSelected;

    public function setIsSelected( $boolIsSelected ) {
    	$this->m_boolIsSelected = $boolIsSelected;
    }

    public function getIsSelected() {
    	return $this->m_boolIsSelected;
    }

}
?>