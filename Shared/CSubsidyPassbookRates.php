<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSubsidyPassbookRates
 * Do not add any new functions to this class.
 */

class CSubsidyPassbookRates extends CBaseSubsidyPassbookRates {

	public static function fetchSubsidyPassbookRates( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CSubsidyPassbookRate', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSubsidyPassbookRate( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CSubsidyPassbookRate', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCurrentSubsidyPassbookRate( $objDatabase ) {

		$strSql = 'SELECT
						spr.passbook_rate_percent
					FROM
						subsidy_passbook_rates spr
					WHERE
						DATE( to_char( spr.effective_date, \'YYYY-MM-DD\' ) ) <= NOW()
						AND spr.is_published = TRUE
					ORDER BY
						spr.effective_date DESC
						LIMIT 1';

		return parent::fetchColumn( $strSql, 'passbook_rate_percent', $objDatabase );
	}

	public static function fetchCertificationSubsidyPassbookRate( $intSubsidyCertificationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						spr.passbook_rate_percent
					FROM
						subsidy_certifications AS sc
						JOIN subsidy_passbook_rates AS spr ON ( sc.effective_date BETWEEN spr.effective_date AND spr.effective_through_date )
					WHERE
						spr.is_published = TRUE
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.id = ' . ( int ) $intSubsidyCertificationId;

		return parent::fetchColumn( $strSql, 'passbook_rate_percent', $objDatabase );
	}

}
?>