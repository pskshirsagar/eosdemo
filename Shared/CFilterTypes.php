<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CFilterTypes
 * Do not add any new functions to this class.
 */

class CFilterTypes extends CBaseFilterTypes {

	public static function fetchFilterTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CFilterType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchFilterType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CFilterType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>