<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CGatewayTypes
 * Do not add any new functions to this class.
 */

class CGatewayTypes extends CBaseGatewayTypes {

	public static function fetchGatewayTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CGatewayType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchGatewayType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CGatewayType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllGatewayTypes( $objDatabase ) {
		return self::fetchGatewayTypes( 'SELECT * FROM gateway_types ORDER BY name ASC', $objDatabase );
	}

}
?>