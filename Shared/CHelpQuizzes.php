<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CHelpQuizzes
 * Do not add any new functions to this class.
 */

class CHelpQuizzes extends CBaseHelpQuizzes {

	public static function fetchHelpQuizzeByCourseHelpResourceIdByCid( $intCourseHelpResourceId, $intCid, $objDatabase ) {
		return self::fetchHelpQuiz( sprintf( 'SELECT * FROM help_quizzes WHERE course_help_resource_id = %d AND cid = %d', ( int ) $intCourseHelpResourceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAllSyncableHelpQuizzesByCourseHelpResourceIdsByCid( $arrintHelpResourceIds, $intCid, $objDatabase, $boolReturnObject = true ) {

		if( false == valId( $intCid ) || false == valArr( $arrintHelpResourceIds ) ) return NULL;

		$strSelectStatement = '*';
		$strWhereClause = 'is_requires_sync = ' . ( int ) CHelpResource::ENTRATA_SYNC_REQUIRED . ' AND';
		if( false == $boolReturnObject ) {
			$strSelectStatement = 'id';
			$strWhereClause = '';
		}

		$strSql = ' SELECT
						' . $strSelectStatement . '
					FROM
						help_quizzes
					WHERE
						' . $strWhereClause . '
						course_help_resource_id IN ( ' . implode( ',', $arrintHelpResourceIds ) . ' )
						AND cid =' . ( int ) $intCid;

		if( false == $boolReturnObject ) {
			return fetchData( $strSql, $objDatabase );
		}

		return self::fetchHelpQuizzes( $strSql, $objDatabase );
	}

	public static function fetchAllHelpQuizzesByIdsByCid( $arrintHelpQuizzIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintHelpQuizzIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						help_quizzes
					WHERE
						id IN ( ' . implode( ',', $arrintHelpQuizzIds ) . ' )
						AND cid = ' . ( int ) $intCid . '';

		return self::fetchHelpQuizzes( $strSql, $objDatabase );
	}

	public static function fetchHelpQuizByParentHelpResourceIdByCid( $intCourseHelpResourceId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						help_quizzes
					WHERE
						deleted_by IS NULL
						AND course_help_resource_id = ' . ( int ) $intCourseHelpResourceId . '
						AND cid =' . ( int ) $intCid;

		return self::fetchHelpQuiz( $strSql, $objDatabase );
	}

	public static function fetchHelpQuizzesByCourseHelpResourceIdByCid( $intCourseHelpResourceId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						hq.*
					FROM
					    help_quizzes hq
					WHERE
					    hq.course_help_resource_id = ' . ( int ) $intCourseHelpResourceId . '
					    AND hq.cid = ' . ( int ) $intCid . '
						AND hq.deleted_on IS NULL
					ORDER BY
					    hq.id';

		return self::fetchHelpQuizzes( $strSql, $objDatabase );
	}

	public static function fetchHelpQuizzesByCourseHelpResourceIdsByCid( $arrintCourseHelpResourceIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
					    help_quizzes
					WHERE
					    course_help_resource_id IN ( ' . implode( ',', $arrintCourseHelpResourceIds ) . ' )
					    AND cid = ' . ( int ) $intCid . '
						AND deleted_on IS NULL
					ORDER BY id';

		return self::fetchHelpQuizzes( $strSql, $objDatabase );
	}

	public static function fetchHelpQuizzByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						help_quizzes
					WHERE
						id = ' . ( int ) $intId . '
						AND cid =' . ( int ) $intCid;

		return self::fetchHelpQuiz( $strSql, $objDatabase );

	}

	public static function fetchHelpQuizzesByCompanyGroupIdsByCid( $arrintCompanyGroupIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCompanyGroupIds ) ) return NULL;

		$strSql = '
					SELECT
						cga.company_group_id,
					    hq.*
					FROM
						help_quizzes hq
					    JOIN company_group_assessments cga ON cga.help_resource_id = hq.course_help_resource_id AND ( (cga.cid = hq.cid ) OR hq.cid = ' . CClient::ID . ')
					WHERE cga.company_group_id IN ( ' . implode( ',', $arrintCompanyGroupIds ) . ' )
					    AND cga.cid =' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomHelpQuizzesDetailsByCourseHelpResourceIdsByCid( $arrintCourseHelpResourceIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCourseHelpResourceIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						help_quizzes
					WHERE
						course_help_resource_id IN ( ' . implode( ',', $arrintCourseHelpResourceIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return self::fetchHelpQuizzes( $strSql, $objDatabase );
	}

	public static function fetchCustomHelpQuizByParentHelpResourceIdByCid( $intCourseHelpResourceId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						section_help_resource_id,
						id as content_id,
						name as content_name,
						help_quiz_type as help_resource_type_id
					FROM
						help_quizzes
					WHERE
						deleted_by IS NULL
						AND ( help_quiz_type = \'' . CHelpQuiz::HELP_QUIZ . '\' OR help_quiz_type = \'' . CHelpQuiz::SURVEY . '\' )
						AND course_help_resource_id = ' . ( int ) $intCourseHelpResourceId . '
						AND cid IN ( ' . ( int ) $intCid . ' , ' . CClient::ID_DEFAULT . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchHelpQuizzesByCourseId( $intHelpResourceId, $objDatabase ) {

		if( !valId( $intHelpResourceId ) ) {
			return NULL;
		}

		$strSql = 'WITH sections AS (
						SELECT
							hr.id as section_id,
							hr.title as section_title,
							hr.description as section_description
						FROM
							help_resources hr
							INNER JOIN help_resource_associations hra ON hra.help_resource_id = hr.id
						WHERE
							hra.parent_help_resource_id = ' . $intHelpResourceId . '
							AND hr.deleted_by IS NULL
							AND hra.section_help_resource_id IS NULL
							AND hr.help_resource_type_id = ' . CHelpResourceType::HELP_SECTION . '
						)
					SELECT
						section_id,
						hq.name, 
						hq.instruction
					FROM
						help_quizzes hq 
						INNER JOIN sections s ON (s.section_id = hq.section_help_resource_id) 
					WHERE 
						hq.deleted_by IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchHelpQuizzesByHelpQuizTypeByCid( $strHelpQuizType, $intCid, $objDatabase ) {

		if( !valStr( $strHelpQuizType ) ) return NULL;

		$strSql = ' SELECT
						id,
						name,
						course_help_resource_id
					FROM
						help_quizzes
					WHERE
						help_quiz_type = \'' . $strHelpQuizType . '\'
						AND deleted_by IS NULL
						AND cid =' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchAllHelpQuizzesBySectionHelpResourceIdByCid( $intSectionHelpResourceId, $intCid, $objDatabase ) {

		if( false == valId( $intSectionHelpResourceId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						*
					FROM
						help_quizzes
					WHERE
						section_help_resource_id = ' . ( int ) $intSectionHelpResourceId . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL';

		return self::fetchHelpQuizzes( $strSql, $objDatabase );
	}

	public static function fetchHelpQuizDetailsByHelpQuizIdByCid( $intHelpQuizId, $intCid, $objDatabase ) {

		if( false == valId( $intHelpQuizId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						hq.id,
						hq.name,
						hq.section_help_resource_id,
						hq.instruction,
						hq.help_quiz_type,
						hqq.id AS question_id,
						hqq.question,
						hqq.help_question_type_id,
						hqa.answer,
						hqa.is_correct,
						hqa.id AS answer_id
					FROM
						help_quizzes AS hq
						JOIN help_quiz_questions AS hqq ON ( hqq.help_quiz_id = hq.id )
						JOIN help_quiz_answers AS hqa ON ( hqa.help_quiz_question_id = hqq.id )
					WHERE
						hq.id = ' . ( int ) $intHelpQuizId . '
						AND hq.cid = ' . ( int ) $intCid . '
						AND hqq.deleted_on IS NULL
						AND hqa.deleted_on IS NULL
					ORDER BY
						hqq.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchHelpQuizzesByCourseHelpResourceIdHelpQuizTypeByCid( $intCourseHelpResourceId, $strHelpQuizType, $intCid, $objDatabase, $boolTestOutQuiz = false ) {

		if( !valStr( $strHelpQuizType ) || !valId( $intCourseHelpResourceId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						*
					FROM
						help_quizzes
					WHERE
						course_help_resource_id = ' . ( int ) $intCourseHelpResourceId . '
						AND help_quiz_type = \'' . $strHelpQuizType . '\'
						AND deleted_by IS NULL
						AND cid =' . ( int ) $intCid;

		if( true == $boolTestOutQuiz ) {
			return self::fetchHelpQuiz( $strSql, $objDatabase );
		} else {
			return self::fetchHelpQuizzes( $strSql, $objDatabase );
		}
	}

}
?>