<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CPaymentBlacklistTypes
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CPaymentBlacklistTypes extends CBasePaymentBlacklistTypes {

    public static function fetchPaymentBlacklistTypes( $strSql, $objDatabase ) {
        return parent::fetchCachedObjects( $strSql, 'CPaymentBlacklistType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchPaymentBlacklistType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CPaymentBlacklistType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

}
?>