<?php

class CGatewayType extends CBaseGatewayType {
	const CC		= 1;
	const ACH		= 2;
	const CHECK21	= 3;
	const EMO		= 4;
	const SEPA		= 5;
	const PAD		= 6;

	static $c_arrstrGatewayTypePrefixes = [ self::ACH => 'ach', self::CC => 'cc', self::CHECK21 => 'ch21', self::EMO => 'emo', self::SEPA => 'sepa', self::PAD => 'pad' ];
	static $c_arrintMerchantAccountGatewayTypes = [ self::ACH, self::CC, self::CHECK21, self::EMO, self::SEPA, self::PAD ];

	static $c_arrintGatewayIdsByProcessingBank = [
		CProcessingBank::ZIONS_BANK => [ self::ACH, self::CC, self::CHECK21, self::EMO ],
		CProcessingBank::FIFTH_THIRD => [ self::ACH, self::CC, self::CHECK21, self::EMO ],
		CProcessingBank::BANK_OF_MONTREAL => [ self::PAD, self::CC ],
		CProcessingBank::INTERNATIONAL => [ self::CC, self::SEPA ]
	];

	public static function idToString( $intGatewayTypeId ) {
		switch( $intGatewayTypeId ) {
			case self::ACH:
				return 'ach';

			case self::CC:
				return 'cc';

			case self::CHECK21:
				return 'ch21';

			case self::EMO:
				return 'emo';

			case self::SEPA:
				return 'sepa';

			case self::PAD:
				return 'pad';

			default:
				return NULL;
		}
	}

}
?>