<?php

class CPropertySettingKeyValueType extends CBasePropertySettingKeyValueType {

	const PLAIN_VALUE			= 1;
	const YES_NO				= 2;
	const YES_NO_INVERSE		= 3;
	const SHOW_HIDE				= 4;
	const SHOW_HIDE_INVERSE		= 5;
	const ON_OFF				= 6;
	const LOOKUP				= 7;
	const CUSTOM_DROPDOWN		= 8;
	const IMAGE					= 9;
	const ON_OFF_INVERSE		= 10;
	const TEXTBOX				= 11;
	const TEXTAREA				= 12;
	const CUSTOM_RADIO_BUTTON	= 13;
	const CUSTOM_CHECKBOX		= 14;
	const DATE_PICKER			= 15;

	public static $c_arrintSimplePropertySettingKeyValueTypes = [
		self::PLAIN_VALUE,
		self::YES_NO,
		self::YES_NO_INVERSE,
		self::SHOW_HIDE,
		self::SHOW_HIDE_INVERSE,
		self::ON_OFF,
		self::ON_OFF_INVERSE
	];

	public static $c_arrintTogglePropertySettingKeyValueTypes = [
		self::YES_NO,
		self::YES_NO_INVERSE,
		self::SHOW_HIDE,
		self::SHOW_HIDE_INVERSE,
		self::ON_OFF,
		self::ON_OFF_INVERSE
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * If you are making any changes in populateSmartyConstants function then
	 * please make sure the same changes would be applied to populateTemplateConstants function also.
	 */

	public static function populateSmartyConstants( $objSmarty ) {
		$objSmarty->assign( 'PROPERTY_SETTING_KEY_VALUE_TYPE_PLAIN_VALUE',			self::PLAIN_VALUE );
		$objSmarty->assign( 'PROPERTY_SETTING_KEY_VALUE_TYPE_YES_NO', 				self::YES_NO );
		$objSmarty->assign( 'PROPERTY_SETTING_KEY_VALUE_TYPE_YES_NO_INVERSE', 		self::YES_NO_INVERSE );
		$objSmarty->assign( 'PROPERTY_SETTING_KEY_VALUE_TYPE_SHOW_HIDE', 			self::SHOW_HIDE );
		$objSmarty->assign( 'PROPERTY_SETTING_KEY_VALUE_TYPE_SHOW_HIDE_INVERSE',	self::SHOW_HIDE_INVERSE );
		$objSmarty->assign( 'PROPERTY_SETTING_KEY_VALUE_TYPE_ON_OFF', 				self::ON_OFF );
		$objSmarty->assign( 'PROPERTY_SETTING_KEY_VALUE_TYPE_LOOKUP', 				self::LOOKUP );
		$objSmarty->assign( 'PROPERTY_SETTING_KEY_VALUE_TYPE_CUSTOM_DROPDOWN',		self::CUSTOM_DROPDOWN );
		$objSmarty->assign( 'PROPERTY_SETTING_KEY_VALUE_TYPE_IMAGE',				self::IMAGE );
		$objSmarty->assign( 'PROPERTY_SETTING_KEY_VALUE_TYPE_ON_OFF_INVERSE',		self::ON_OFF_INVERSE );
		$objSmarty->assign( 'PROPERTY_SETTING_KEY_VALUE_TYPE_TEXTBOX',				self::TEXTBOX );
		$objSmarty->assign( 'PROPERTY_SETTING_KEY_VALUE_TYPE_TEXTAREA',				self::TEXTAREA );
		$objSmarty->assign( 'PROPERTY_SETTING_KEY_VALUE_TYPE_CUSTOM_RADIO_BUTTON',	self::CUSTOM_RADIO_BUTTON );
		$objSmarty->assign( 'PROPERTY_SETTING_KEY_VALUE_TYPE_CUSTOM_CHECKBOX',		self::CUSTOM_CHECKBOX );
		$objSmarty->assign( 'PROPERTY_SETTING_KEY_VALUE_TYPE_DATE_PICKER',			self::DATE_PICKER );
	}

	public static function populateTemplateConstants( $arrmixTemplateParameters ) {
		$arrmixTemplateParameters['PROPERTY_SETTING_KEY_VALUE_TYPE_PLAIN_VALUE']			= self::PLAIN_VALUE;
		$arrmixTemplateParameters['PROPERTY_SETTING_KEY_VALUE_TYPE_YES_NO'] 				= self::YES_NO;
		$arrmixTemplateParameters['PROPERTY_SETTING_KEY_VALUE_TYPE_YES_NO_INVERSE'] 		= self::YES_NO_INVERSE;
		$arrmixTemplateParameters['PROPERTY_SETTING_KEY_VALUE_TYPE_SHOW_HIDE'] 				= self::SHOW_HIDE;
		$arrmixTemplateParameters['PROPERTY_SETTING_KEY_VALUE_TYPE_SHOW_HIDE_INVERSE']		= self::SHOW_HIDE_INVERSE;
		$arrmixTemplateParameters['PROPERTY_SETTING_KEY_VALUE_TYPE_ON_OFF'] 				= self::ON_OFF;
		$arrmixTemplateParameters['PROPERTY_SETTING_KEY_VALUE_TYPE_LOOKUP'] 				= self::LOOKUP;
		$arrmixTemplateParameters['PROPERTY_SETTING_KEY_VALUE_TYPE_CUSTOM_DROPDOWN']		= self::CUSTOM_DROPDOWN;
		$arrmixTemplateParameters['PROPERTY_SETTING_KEY_VALUE_TYPE_IMAGE']					= self::IMAGE;
		$arrmixTemplateParameters['PROPERTY_SETTING_KEY_VALUE_TYPE_ON_OFF_INVERSE']			= self::ON_OFF_INVERSE;
		$arrmixTemplateParameters['PROPERTY_SETTING_KEY_VALUE_TYPE_TEXTBOX']				= self::TEXTBOX;
		$arrmixTemplateParameters['PROPERTY_SETTING_KEY_VALUE_TYPE_TEXTAREA']				= self::TEXTAREA;
		$arrmixTemplateParameters['PROPERTY_SETTING_KEY_VALUE_TYPE_CUSTOM_RADIO_BUTTON']	= self::CUSTOM_RADIO_BUTTON;
		$arrmixTemplateParameters['PROPERTY_SETTING_KEY_VALUE_TYPE_CUSTOM_CHECKBOX']		= self::CUSTOM_CHECKBOX;
		$arrmixTemplateParameters['PROPERTY_SETTING_KEY_VALUE_TYPE_DATE_PICKER']			= self::DATE_PICKER;

		return $arrmixTemplateParameters;
	}

}
?>