<?php

class CApFinancialStatusType extends CBaseApFinancialStatusType {

	const PENDING				= 1;
	const APPROVED				= 2;
	const PARTIALLY_INVOICED	= 3;
	const CLOSED				= 4;
	const CANCELLED				= 5;
	const REJECTED				= 6;
	const DELETED		        = 8;

	const SUBMITTED               = 'Submitted';
	const INVOICE_PAID            = 'Paid';
	const INVOICE_IS_NEW          = 'New';
	const INVOICE_REOPEN          = 'Re-Open';
	const INVOICE_POSTED          = 'Posted';
	const INVOICE_NOT_POSTED      = 'Not Posted';
	const INVOICE_CANCELLED       = 'Cancelled';
	const INVOICE_PARTIALLY_PAID  = 'Partially-Paid';
	const INVOICE_REJECTED		  = 'Rejected';

	public static $c_arrstrApFinancialStatusTypes = [
		self::PENDING				=> 'Pending',
		self::APPROVED				=> 'Approved',
		self::PARTIALLY_INVOICED	=> 'Partially Invoiced',
		self::CLOSED				=> 'Closed',
		self::CANCELLED				=> 'Cancelled',
		self::REJECTED				=> 'Rejected',
		self::DELETED               => 'Deleted'
	];

	public static $c_arrstrActionApFinancialStatusTypes = [
		self::SUBMITTED			    => self::SUBMITTED,
		self::CANCELLED				=> self::INVOICE_CANCELLED,
		self::INVOICE_REOPEN        => self::INVOICE_REOPEN
	];

	public static $c_arrstrOrderHeaderFinancialStatusTypesBulkActions = [
		'export_order_headers'  => 'Export Selected',
		self::SUBMITTED     	=> 'Submit',
		self::INVOICE_REOPEN	=> 'Re-Open',
		self::CANCELLED			=> 'Cancel',
	];

	public static $c_arrstrOrderHeaderFilterFinancialStatusTypes = [
		[ 'id' => self::INVOICE_IS_NEW,          'status_name' => 'New' ],
		[ 'id' => self::SUBMITTED,               'status_name' => self::SUBMITTED ],
		[ 'id' => self::INVOICE_POSTED,          'status_name' => self::INVOICE_POSTED ],
		[ 'id' => self::INVOICE_PARTIALLY_PAID,  'status_name' => self::INVOICE_PARTIALLY_PAID ],
		[ 'id' => self::INVOICE_PAID,            'status_name' => self::INVOICE_PAID ],
		[ 'id' => self::CANCELLED,               'status_name' => 'Cancelled' ]
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>