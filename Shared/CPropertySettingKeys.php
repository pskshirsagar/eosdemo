<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPropertySettingKeys
 * Do not add any new functions to this class.
 */

class CPropertySettingKeys extends CBasePropertySettingKeys {

	public static function fetchPaginatedPropertySettingKeys( $intPageNo, $intPageSize, $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL, $arrintSearchModulesIds = [], $boolIsForCount = false ) {

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intPageSize;

		$strOrderByField	= ( true == is_null( $strOrderByField ) && true == is_null( $strOrderByType ) ) ? ' updated_on ': $strOrderByField;
		$strOrderByType		= ( 0 == $strOrderByType )? 'DESC NULLS LAST': 'ASC NULLS FIRST';
		$strWhere			= '';

		if( valIntArr( $arrintSearchModulesIds ) ) {
			$strWhere = ' WHERE psg.module_id IN ( ' . implode( ',', $arrintSearchModulesIds ) . ')';
		}

		if( $boolIsForCount ) {
			$strSelect = 'COUNT( * ) AS property_setting_keys_count';
		} else {
			$strSelect = 'psk.*,
						psg.name AS property_setting_group_name,
						tt.key AS tool_tip_key,
						e.name_full as updated_by_user_name,
						pskvt.name as value_type';
		}

		$strSql = 'SELECT ' .
						$strSelect . '
					FROM
						property_setting_keys AS psk
						JOIN property_setting_groups AS psg ON ( psg.id = psk.property_setting_group_id )';

		if( !$boolIsForCount ) {
			$strSql .= ' LEFT JOIN property_setting_key_values AS pskv ON ( pskv.property_setting_key_id = psk.id )
						LEFT JOIN property_setting_key_value_types AS pskvt ON ( pskvt.id = pskv.property_setting_key_value_type_id )
						LEFT JOIN tool_tips AS tt ON ( tt.id = psk.tool_tip_id )
						LEFT JOIN users AS u ON ( u.id = psk.updated_by )
						LEFT JOIN employees AS e ON ( e.id = u.employee_id )';
		}

		$strSql .= $strWhere;

		if( !$boolIsForCount ) {
			$strSql .= ' GROUP BY
							psk.id,
							psg.name,
							tt.key,
							e.name_full,
							pskvt.name
						ORDER BY
						' . $strOrderByField . '
						' . $strOrderByType . '
						OFFSET
						' . ( int ) $intOffset;

			if( true == isset( $intLimit ) && 0 < $intLimit ) {
				$strSql .= ' LIMIT ' . ( int ) $intLimit;
			}

			return self::fetchPropertySettingKeys( $strSql, $objDatabase );
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertySettingKeysWithSearchCriteria( $strSearchKeyword, $objDatabase ) {
			$strSql = 'SELECT
						psk.id,
						psk.key,
						psg.name as property_setting_group_name,
						psk.old_label,
						psk.label
					FROM
						property_setting_keys psk
						JOIN property_setting_groups psg ON ( psk.property_setting_group_id = psg.id )
					WHERE
						( CAST ( psk.id AS TEXT ) ILIKE \'%' . $strSearchKeyword . '%\'
						OR ( psk.key ILIKE\'%' . $strSearchKeyword . '%\' OR to_tsvector( \'english\', psk.key ) @@ plainto_tsquery( \'' . $strSearchKeyword . '\' ) )
						OR ( psk.old_label ILIKE\'%' . $strSearchKeyword . '%\'  OR to_tsvector( \'english\', psk.old_label ) @@ plainto_tsquery( \'' . $strSearchKeyword . '\' ) )
						OR ( psk.label ILIKE\'%' . $strSearchKeyword . '%\' OR to_tsvector( \'english\', psk.label ) @@ plainto_tsquery( \'' . $strSearchKeyword . '\' ) )
						OR ( psg.name ILIKE\'%' . $strSearchKeyword . '%\' OR to_tsvector( \'english\', psg.name ) @@ plainto_tsquery( \'' . $strSearchKeyword . '\' ) )
						)
					ORDER BY
						id
					LIMIT
						20';
		return self::fetchPropertySettingKeys( $strSql, $objDatabase );
	}

	public static function fetchAllPropertySettingKeysByRequiresSync( $objDatabase ) {

		$strSql = 'SELECT
						psk.*
					FROM
						property_setting_keys psk
					WHERE
						psk.requires_sync = \'t\'';

		return self::fetchPropertySettingKeys( $strSql, $objDatabase );
	}

	public static function fetchPropertySettingKeysByIds( $arrintPropertySettingKeyIds, $objDatabase ) {
		if( false == valArr( $arrintPropertySettingKeyIds ) ) return NULL;
		$strSql = 'SELECT
						*
					FROM
						property_setting_keys
					WHERE
						id IN ( ' . implode( ',', $arrintPropertySettingKeyIds ) . ' ) ';
		return self::fetchPropertySettingKeys( $strSql, $objDatabase );
	}

	public static function fetchAllPropertySettingKeys( $objDatabase ) {
		return self::fetchPropertySettingKeys( 'SELECT * FROM property_setting_keys ORDER BY id', $objDatabase );
	}

	public static function fetchPropertySettingKeyByKey( $strKey, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						property_setting_keys
					WHERE
						key = \'' . $strKey . '\'';
		return self::fetchPropertySettingKey( $strSql, $objDatabase );
	}

	public static function fetchPropertySettingKeyByIdByKey( $intPropertySettingKeyId, $strKey, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						property_setting_keys
					WHERE
						key = \'' . $strKey . '\'
						AND id = ' . ( int ) $intPropertySettingKeyId;
		return self::fetchPropertySettingKey( $strSql, $objDatabase );
	}

	public static function fetchPropertySettingKeysByKeys( $arrstrKeys, $objDatabase, $boolRekeyByKey = true ) {
		if( false == valArr( $arrstrKeys ) ) return NULL;
		$strSql = 'SELECT
						*
					FROM
						property_setting_keys
					WHERE
						key IN( \'' . implode( '\', \'', $arrstrKeys ) . '\' );';

		$arrobjPropertySettingKeys = ( array ) parent::fetchPropertySettingKeys( $strSql, $objDatabase );

		if( true == $boolRekeyByKey ) {
			$arrobjPropertySettingKeys = rekeyObjects( 'Key', $arrobjPropertySettingKeys );
		}

		return $arrobjPropertySettingKeys;
	}

	public static function fetchPropertySettingKeysByKeysWithSettingLocationAndTooltip( $arrstrKeys, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) ) return NULL;
		$strSql = 'SELECT
						psk.id,
						psk.key,
						util_get_system_translated( \'label\', psk.label, psk.details ) AS label,
						psk.old_label,
						tt.title,
						tt.client_description,
						tt.key as tool_tip_key,
						psg.name AS group_name,
						m.url,
						util_get_system_translated( \'description\', psg.description, psg.details ) AS description,
						util_get_system_translated( \'title\', m.title, m.details ) AS module_name
					FROM
						property_setting_keys psk
						LEFT JOIN property_setting_groups psg ON psg.id = psk.property_setting_group_id
						LEFT JOIN modules m ON m.id = psg.module_id
						LEFT JOIN tool_tips tt ON tt.id = psk.tool_tip_id
					WHERE
						psk.key IN( \'' . implode( '\', \'', $arrstrKeys ) . '\' );';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertySettingKeysByPropertyIdsByPropertySettingGroupIdsByCid( $arrintPropertyIds, $arrintPropertySettingGroupIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintPropertySettingGroupIds ) ) return NULL;

		$strSql = 'SELECT
						ppk.id as key_id,
						ppk.key,
						ppk.label as caption,
						tt.key as tooltip_key,
						CASE WHEN stp.settings_template_id IS NULL THEN ppk.delete_value
							WHEN stp.template_count = value_count THEN stp.value
						ELSE \'Varies\'
						END as value
					FROM
						property_setting_keys ppk
						LEFT JOIN tool_tips tt ON (ppk.tool_tip_id = tt.id)
						LEFT JOIN (
							SELECT
								DISTINCT ON(key)
								MAX( rank ) OVER (PARTITION BY true ) as template_count,
								value_count,
								settings_template_id,
								property_setting_key_id,
								key,
								value
							FROM (
									SELECT
										dense_rank() OVER( ORDER BY st.id ) as rank,
										COUNT( pp.id ) OVER( PARTITION BY pp.key, pp.value ) as value_count,
										st.id as settings_template_id,
										pp.property_setting_key_id,
										pp.key,
										pp.value
									FROM
										settings_templates st
										LEFT JOIN property_preferences pp ON( st.property_id = pp.property_id AND pp.cid = st.cid )
									WHERE
										st.cid = ' . ( int ) $intCid . '
										AND st.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
									) as sub
							WHERE
								key IS NOT NULL
						) as stp ON( stp.key = ppk.key OR ppk.id = stp.property_setting_key_id )
					WHERE
						ppk.property_setting_group_id IN (' . implode( ',', $arrintPropertySettingGroupIds ) . ')
					ORDER BY
						ppk.key';

		$arrmixResponseData = ( array ) fetchData( $strSql, $objClientDatabase );

		$arrmixPropertyPreferences = [];

		foreach( $arrmixResponseData as $arrmixResponse ) {
			$arrmixPropertyPreferences[$arrmixResponse['key']]['caption'] 		= $arrmixResponse['caption'];
			$arrmixPropertyPreferences[$arrmixResponse['key']]['value'] 		= $arrmixResponse['value'];
			$arrmixPropertyPreferences[$arrmixResponse['key']]['tooltip_key'] 	= $arrmixResponse['tooltip_key'];
		}

		return $arrmixPropertyPreferences;
	}

	public static function fetchPropertySettingKeysByPropertySettingGroupIds( $arrintPropertySettingGroupIds, $objDatabase, $boolRekeyByKey = true, $strTableName = '', $boolIsCustomTable = false ) {
		if( false == valArr( $arrintPropertySettingGroupIds ) ) return NULL;

		$strSql = 'SELECT
						psk.*
					FROM
						property_setting_keys psk
					WHERE
						psk.property_setting_group_id IN ( ' . implode( ',', $arrintPropertySettingGroupIds ) . ' )';

		$strSql .= ( 0 < strlen( $strTableName ) ) ? ' AND psk.table_name = \'' . $strTableName . '\'' : '';
		$strSql .= ( true == $boolIsCustomTable ) ? ' AND psk.table_name IS NOT NULL AND psk.column_name IS NOT NULL' : '';

		$arrobjPropertySettingKeys = ( array ) parent::fetchPropertySettingKeys( $strSql, $objDatabase );

		if( true == $boolRekeyByKey ) {
			$arrobjPropertySettingKeys = rekeyObjects( 'Key', $arrobjPropertySettingKeys );
		}
		return $arrobjPropertySettingKeys;
	}

	public static function fetchSimplePropertySettingKeysByPropertySettingGroupIds( $arrintPropertySettingGroupIds, $objDatabase ) {
		if( false == valArr( $arrintPropertySettingGroupIds ) ) return NULL;

		$strSql = 'SELECT
						psk.key
					FROM
						property_setting_keys psk
					WHERE
						psk.property_setting_group_id IN ( ' . implode( ',', $arrintPropertySettingGroupIds ) . ' )
						AND psk.table_name IS NOT NULL
						AND psk.column_name IS NOT NULL';

		$arrmixResponseData = ( array ) fetchData( $strSql, $objDatabase );
		$arrmixKeys = [];

		foreach( $arrmixResponseData as $arrmixResponse ) {
			$arrmixKeys[] = $arrmixResponse['key'];
		}
		return $arrmixKeys;
	}

	public static function fetchDefaultCopyPropertySettingKeysByPropertySettingGroupIds( $arrintPropertySettingGroupIds, $objDatabase, $boolIsDefaultCopy = false ) {
		if( false == valArr( $arrintPropertySettingGroupIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						property_setting_keys psk
					WHERE
						psk.property_setting_group_id IN ( ' . implode( ',', $arrintPropertySettingGroupIds ) . ' )';

		$strCondition = ( true == $boolIsDefaultCopy ) ? ' AND psk.is_default_copy = true' : '';

		$strSql = $strSql . $strCondition;

		$arrobjPropertySettingKeys = ( array ) parent::fetchPropertySettingKeys( $strSql, $objDatabase );

		$arrobjPropertySettingKeys = rekeyObjects( 'Key', $arrobjPropertySettingKeys );

		return $arrobjPropertySettingKeys;
	}

	public static function fetchCustomPropertySettingKeysBySearchFilter( $strSearchFilter, $objClientDatabase, $boolIsBulkEdit = NULL ) {
		if( false == valStr( $strSearchFilter ) ) return NULL;

		$strWhereCondition = '';
		$strJoinCondition = '';

		if( false == is_null( $boolIsBulkEdit ) && true == $boolIsBulkEdit ) {
			$strWhereCondition = 'psk.is_bulk_editable = true AND';
			$strJoinCondition = 'JOIN property_setting_keys psk on psk.property_setting_group_id = psg.id';
		}

		$strSearchFilter = addslashes( trim( $strSearchFilter ) );

		$strSql = 'WITH full_text_doc AS (
						SELECT
							psk.id,
							psk.key,
							psk.label,
							psk.old_label,
							tt.title,
							tt.client_description,
							tt.key as tool_tip_key,
							psg.name AS group_name,
							psg.id AS group_id,
							m.url,
							m.description,
							util_get_translated( \'title\', m.title, m.details ) AS module_name,
							m.id as module_id,
							ts_rank ( coalesce ( setweight ( to_tsvector ( psk.key ), \'A\' ), \'\' )
							|| coalesce ( setweight ( to_tsvector ( label ), \'A\' ), \'\' )
							|| coalesce ( setweight ( to_tsvector ( old_label ), \'A\' ), \'\' )
							|| coalesce ( setweight ( to_tsvector ( psg.name ), \'B\' ), \'\' )
							|| coalesce ( setweight ( to_tsvector ( util_get_translated( \'title\', m.title, m.details ) ), \'B\' ), \'\' )
							|| coalesce ( setweight ( to_tsvector ( tt.title ), \'C\' ), \'\' )
							|| coalesce ( setweight ( to_tsvector ( tt.client_description ), \'C\' ), \'\' ), plainto_tsquery ( \'' . $strSearchFilter . '\' ) )
							AS doc
						FROM
							property_setting_keys psk
							LEFT JOIN property_setting_groups psg ON psg.id = psk.property_setting_group_id
							LEFT JOIN modules m ON m.id = psg.module_id
							LEFT JOIN tool_tips tt ON tt.id = psk.tool_tip_id
						WHERE ' . $strWhereCondition . '(
							( coalesce ( setweight ( to_tsvector ( psk.key ), \'A\' ), \'\' )
							|| coalesce ( setweight ( to_tsvector ( label ), \'A\' ), \'\' )
							|| coalesce ( setweight ( to_tsvector ( old_label ), \'A\' ), \'\' )
							|| coalesce ( setweight ( to_tsvector ( psg.name ), \'B\' ), \'\' )
							|| coalesce ( setweight ( to_tsvector ( m.title ), \'B\' ), \'\' )
							|| coalesce ( setweight ( to_tsvector ( tt.title ), \'C\' ), \'\' )
							|| coalesce ( setweight ( to_tsvector ( tt.client_description ), \'C\' ), \'\' )
							)
							@@
							(
								plainto_tsquery ( \'' . $strSearchFilter . '\' )
							)
						) AND psk.is_disabled = FALSE
					),
					full_text_group AS (
						SELECT
							psg.id,
							psg.name AS group_name,
							m.url,
							psg.description,
							util_get_translated( \'title\', m.title, m.details ) AS module_name,
							m.id as module_id,
							ts_rank ( coalesce ( setweight ( to_tsvector ( psg.name ), \'A\' ), \'\' )
							|| coalesce ( setweight ( to_tsvector ( util_get_translated( \'title\', m.title, m.details ) ), \'B\' ), \'\' ), plainto_tsquery ( \'' . $strSearchFilter . '\' ) ) AS doc
						FROM
							property_setting_groups psg
							LEFT JOIN modules m ON m.id = psg.module_id
							' . $strJoinCondition . '
						WHERE ' . $strWhereCondition . ' (
							( coalesce ( setweight ( to_tsvector ( psg.name ), \'A\' ), \'\' )
							|| coalesce ( setweight ( to_tsvector ( m.title ), \'B\' ), \'\' )
							) @@
							plainto_tsquery ( \'' . $strSearchFilter . '\' )
							)
					)
					SELECT
						id,
						KEY,
						label,
						old_label,
						title,
						client_description AS tool_tip_description,
						tool_tip_key,
						description,
						group_name,
						group_id,
						url,
						module_id,
						module_name,
						doc
					FROM
						full_text_doc

					UNION

					SELECT
						id,
						\'\' AS KEY ,
						\'\' AS label,
						\'\' AS old_label,
						\'\' AS title,
						\'\' AS tool_tip_description,
						\'\' AS tool_tip_key,
						description,
						group_name,
						id AS group_id,
						url,
						module_id,
						module_name,
						doc
					FROM
						full_text_group
					ORDER BY
						doc DESC';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchAllImplementationPropertySettingModules( $objDatabase, $intCid = NULL, $arrintPropertyIds = NULL, $boolIsReturnRawData = false, $intModuleId = NULL ) {
		$strJoinCondition 					= ( false == empty( $intCid ) ) ? ' AND ic.cid = ' . ( int ) $intCid : '';
		$strJoinCondition 					.= ( true == valArr( $arrintPropertyIds ) ) ? ' AND ic.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )' : '';
		$strWhereClause						= ( false == empty( $intModuleId ) ) ? ' WHERE ps.parent_module_id = ' . ( int ) $intModuleId . ' OR ps.module_id IN ( SELECT id FROM modules m WHERE m.parent_module_id = ' . ( int ) $intModuleId . ' )'  : '';
		$arrintPropertyImplementedModules	= [];

		$strSql = ' WITH RECURSIVE property_settings( module_id, title, parent_module_id, name, property_setting_key_id, depth ) AS (
						SELECT
							m.id,
							util_get_translated( \'title\', m.title, m.details ) AS title,
							m.parent_module_id,
							m.name,
							psk.id,
							1
						FROM
							property_setting_groups psg
							LEFT JOIN property_setting_keys psk ON ( psk.property_setting_group_id = psg.id )
							JOIN modules m ON ( psg.module_id = m.id )
						WHERE
							psk.is_implementation_required = true OR psg.is_implementation_required = true
					UNION ALL
						SELECT
							m.id,
							util_get_translated( \'title\', m.title, m.details ) AS title,
							m.parent_module_id,
							m.name,
							ps.property_setting_key_id,
							depth+1
						FROM
							modules m, property_settings ps
						WHERE
							m.id = ps.parent_module_id
							AND m.id <> ' . CModule::SETUP_PROPERTIES_BETA . '
					)
					SELECT
						DISTINCT ON ( ic.property_id,ps.module_id ) ic.property_id, ps.*,
						CASE WHEN TRUE = ic.is_implemented AND TRUE = ic.is_completion_recommended THEN 3
 							WHEN TRUE = ic.is_implemented AND FALSE = ic.is_completion_recommended THEN 2
 							WHEN TRUE = ic.is_completion_recommended THEN 1
							ELSE 0
						END AS check_mark_state
					FROM
						property_settings AS ps
						LEFT JOIN implementation_checklists AS ic ON ( ps.module_id = ic.module_id ' . $strJoinCondition . ' )' . $strWhereClause . '
					ORDER BY
						ps.module_id';

		$arrstrImplementationPropertySettingChecklists = fetchData( $strSql, $objDatabase );

		// Return raw data.
		if( true == $boolIsReturnRawData ) return $arrstrImplementationPropertySettingChecklists;

		$arrstrPropertySettingChecklists = [];

		if( true == valArr( $arrstrImplementationPropertySettingChecklists ) ) {
			$arrstrPropertySettingChecklistsTemp = rekeyArray( 'module_id', $arrstrImplementationPropertySettingChecklists );

			foreach( $arrstrImplementationPropertySettingChecklists as $arrstrImplementationPropertySettingChecklist ) {
				$intModuleId 		= $arrstrImplementationPropertySettingChecklist['module_id'];
				$intParentModuleId 	= $arrstrImplementationPropertySettingChecklist['parent_module_id'];

				if( CModule::SETUP_PROPERTIES_BETA == $intParentModuleId ) {
					if( false == empty( $arrstrPropertySettingChecklists[CModule::SETUP_PROPERTIES_BETA][$intModuleId] ) ) {
						$arrstrPropertySettingChecklists[CModule::SETUP_PROPERTIES_BETA][$intModuleId] += $arrstrImplementationPropertySettingChecklist;
					} else {
						$arrstrPropertySettingChecklists[CModule::SETUP_PROPERTIES_BETA][$intModuleId] = $arrstrImplementationPropertySettingChecklist;
					}
				} else {
					$intParentParentModuleId = ( false == empty( $arrstrPropertySettingChecklistsTemp[$intParentModuleId] ) ) ? $arrstrPropertySettingChecklistsTemp[$intParentModuleId]['parent_module_id'] : '';

					if( false == empty( $arrstrPropertySettingChecklists[CModule::SETUP_PROPERTIES_BETA][$intParentParentModuleId] ) ) {
						$arrstrPropertySettingChecklists[CModule::SETUP_PROPERTIES_BETA][$intParentParentModuleId]['child_modules'][$intParentModuleId]['child_modules'][$intModuleId] = $arrstrImplementationPropertySettingChecklist;
					} else {
						$arrstrPropertySettingChecklists[CModule::SETUP_PROPERTIES_BETA][$intParentModuleId]['child_modules'][$intModuleId] = $arrstrImplementationPropertySettingChecklist;
					}
				}

				if( 1 < $arrstrImplementationPropertySettingChecklist['check_mark_state'] ) {
					$arrintPropertyImplementedModules[$arrstrImplementationPropertySettingChecklist['property_id']] = ( true == isset( $arrintPropertyImplementedModules[$arrstrImplementationPropertySettingChecklist['property_id']] ) ) ? $arrintPropertyImplementedModules[$arrstrImplementationPropertySettingChecklist['property_id']]++ : 1;
				}
			}
		}

		if( true == valArr( $arrstrPropertySettingChecklists ) && true == array_key_exists( CModule::SETUP_PROPERTIES_BETA, $arrstrPropertySettingChecklists ) && true == valArr( $arrintPropertyImplementedModules ) ) {
			// Calculate total number of checklist modules.
			$arrintModuleCount = array_map( function( $arrstrPropertyImplementationChecklist ) {
				return $intCount = 1 + ( ( false == empty( $arrstrPropertyImplementationChecklist['child_modules'] ) ) ? \Psi\Libraries\UtilFunctions\count( $arrstrPropertyImplementationChecklist['child_modules'] ) : 0 );
			}, $arrstrPropertySettingChecklists[CModule::SETUP_PROPERTIES_BETA] );

			$intTotalModuleCount = ( true == valArr( $arrintModuleCount ) ) ? array_sum( $arrintModuleCount ) : 0;

			// Calculate implemented checklist percentage.
			array_walk( $arrintPropertyImplementedModules, function( &$intTotalImplementedChecklistItems, $intPropertyId, $intTotalModuleCount ) {
				$intTotalImplementedChecklistItems = ( 0 < $intTotalModuleCount ) ? round( ( $intTotalImplementedChecklistItems / $intTotalModuleCount ) * 100 ) : 0;
			}, $intTotalModuleCount );
		}

		$arrstrTempPropertyChecklists['percentage'] 				= $arrintPropertyImplementedModules;
		$arrstrTempPropertyChecklists['implementation_checklists']	= ( true == valArr( $arrstrPropertySettingChecklists ) && true == array_key_exists( CModule::SETUP_PROPERTIES_BETA, $arrstrPropertySettingChecklists ) ) ? $arrstrPropertySettingChecklists[CModule::SETUP_PROPERTIES_BETA] : [];

		return $arrstrTempPropertyChecklists;
	}

	public static function fetchCustomImplementationPropertySettingKeys( $intCid, $arrintPropertyIds, $objDatabase, $arrintModuleIds = NULL ) {
		if( true == empty( $arrintPropertyIds ) ) return NULL;

		$arrstrImplementationPropertySettingKeys = [];

		$strWhereClause = ( false == empty( $arrintModuleIds ) ) ? ' WHERE ( m.id IN ( ' . implode( ',', $arrintModuleIds ) . ' ) OR m.parent_module_id IN ( ' . implode( ',', $arrintModuleIds ) . ' ) ) OR m.parent_module_id IN ( SELECT id FROM modules m WHERE m.parent_module_id IN ( ' . implode( ',', $arrintModuleIds ) . ' ) ) ' : '';

		$strSql = ' WITH implementation_property_settings AS (
						SELECT
							property_setting_counts.*
						FROM (
							SELECT
								DISTINCT ON ( psl.property_id, psg.id )
								psl.property_id AS property_id,
								psg.id AS property_setting_group_id,
								0 AS property_setting_key_id,
								MAX( psg.module_id ) AS module_id,
								COUNT( CASE WHEN psl.action <> \'DELETE\' AND psl.new_text IS NOT NULL THEN 1 ELSE NULL END )::VARCHAR AS value,
								MAX( psk.KEY ) AS key,
								0 AS value_type,
								MAX( psg.table_name ) AS table_name,
								MAX( psg.column_name ) AS column_name,
								\'\'::varchar AS lookup_table_name,
								\'\'::varchar AS lookup_column_name,
								MAX( psg.name ) AS label,
								CASE
									WHEN TRUE = psg.can_mark_empty_completed THEN 1
									WHEN FALSE = psg.can_mark_empty_completed THEN 0
									ELSE NULL
								END AS can_mark_empty_completed,
								MAX( CASE WHEN TRUE = psg.is_list THEN 1 ELSE 0 END ) AS is_list,
								psg.implementation_description
							FROM
								property_setting_groups psg
								LEFT JOIN property_setting_keys psk ON ( psk.property_setting_group_id = psg.id )
								LEFT JOIN property_setting_logs psl ON ( psl.property_setting_key_id = psk.id AND psl.cid = ' . ( int ) $intCid . ' AND psl.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
							WHERE
								psg.is_list = TRUE
								AND ( psk.is_implementation_required = TRUE OR psg.is_implementation_required = TRUE )
							GROUP BY
								psl.cid,
								psl.property_id,
								psg.id,
								psl.created_on,
								psg.can_mark_empty_completed,
								psg.implementation_description
							ORDER BY
								psl.property_id,
								psg.id,
								psl.created_on DESC
					) AS property_setting_counts
					UNION ALL
					SELECT
						property_setting_values.*
					FROM (
						SELECT
							DISTINCT ON ( psl.property_id, psk.id )
							psl.property_id AS property_id,
							0 AS property_setting_group_id,
							psk.id AS property_setting_key_id,
							psg.module_id,
							( CASE
								WHEN psl.action <> \'DELETE\' AND psl.new_text IS NOT NULL THEN psl.new_text
								WHEN psl.action <> \'DELETE\' THEN psl.new_value
								ELSE psk.delete_value
							END )::VARCHAR AS value,
							psk.Key,
							pskv.property_setting_key_value_type_id AS value_type,
							psk.table_name,
							psk.column_name,
							pskv.table_name AS lookup_table_name,
							pskv.column_name AS lookup_column_name,
							psk.label,
							CASE
								WHEN FALSE = psg.can_mark_empty_completed AND TRUE = psg.is_implementation_required THEN 0
								WHEN TRUE = psk.can_mark_empty_completed THEN 1
								WHEN FALSE = psk.can_mark_empty_completed THEN 0
								WHEN TRUE = psg.can_mark_empty_completed THEN 1
								ELSE NULL
							END AS can_mark_empty_completed,
							0 AS is_list,
							psk.implementation_description
						FROM
							property_setting_keys psk
							JOIN property_setting_groups psg ON ( psk.property_setting_group_id = psg.id )
							LEFT JOIN property_setting_logs psl ON ( psk.id = psl.property_setting_key_id AND psl.cid = ' . ( int ) $intCid . ' AND psl.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
							LEFT JOIN property_setting_key_values pskv ON ( pskv.property_setting_key_id = psk.id )
						WHERE
							 ( psk.is_implementation_required = TRUE OR psg.is_implementation_required = TRUE )
						ORDER BY
							psl.property_id,
							psk.id,
							psl.created_on DESC
					) AS property_setting_values
				)
				SELECT
					ips.*,
					util_get_translated( \'title\', m.title, m.details ) AS title,
					m.name,
					m.parent_module_id
				FROM
					implementation_property_settings ips
					JOIN modules m ON ( ips.module_id = m.id )
					' . $strWhereClause . '
				ORDER BY
					ips.module_id';

		$arrstrImplementationPropertySettingKeysTemp = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrImplementationPropertySettingKeysTemp ) ) {
			foreach( $arrstrImplementationPropertySettingKeysTemp as $arrstrImplementationPropertySettingKey ) {
				$intPropertyId 					= $arrstrImplementationPropertySettingKey['property_id'];
				$intModuleId 					= $arrstrImplementationPropertySettingKey['module_id'];
				$intPropertySettingKeyId 		= $arrstrImplementationPropertySettingKey['property_setting_key_id'];

				$arrstrImplementationPropertySettingKeys[$intPropertyId][$intModuleId][] = $arrstrImplementationPropertySettingKey;
			}
		}

		return $arrstrImplementationPropertySettingKeys;
	}

	public static function fetchPropertySettingKeysAndValuesByTemplatePropertyIdByPropertySettingKeyIdsByCid( $intTemplatePropertyId, $arrintPropertySettingKeyIds, $intCid, $objDatabase ) {
		if( true == empty( $intTemplatePropertyId ) || false == valArr( $arrintPropertySettingKeyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON ( sta.property_id, sta.property_setting_key_id )
						sta.property_id,
						sta.property_setting_key_id,
						psk.key,
						psk.database_type_id,
						psk.table_name,
						psk.delete_value,
						psk.column_name,
						sta.settings_template_id,
						CASE
							WHEN pp.id IS NULL AND pn.id IS NULL THEN psk.delete_value
							WHEN pp.id IS NULL AND pn.id IS NOT NULL THEN pn.value
							WHEN pp.id IS NOT NULL AND pn.id IS NULL THEN pp.value
						END AS value,
						setting_log_details.new_text,
						setting_log_details.new_value,
						pp.id AS preference_id,
						pn.id AS notification_id,
						key_values.property_setting_key_value_type_id
					FROM
						settings_templates st
						JOIN settings_template_assignments AS sta ON sta.settings_template_id = st.id AND sta.property_id <> st.property_id AND sta.cid = st.cid
						JOIN property_setting_keys AS psk ON psk.id = sta.property_setting_key_id
						LEFT JOIN property_preferences AS pp ON pp.property_id = st.property_id AND pp.key = psk.key AND pp.cid = st.cid
						LEFT JOIN property_notifications AS pn ON pn.property_id = st.property_id AND ( pn.property_setting_key_id = psk.id OR pn.key = psk.key ) AND pn.cid = st.cid
						LEFT JOIN (
							SELECT
								pskv.property_setting_key_id,
								pskv.property_setting_key_value_type_id
							FROM
								property_setting_key_values pskv
							GROUP BY
								pskv.property_setting_key_id,
								pskv.property_setting_key_value_type_id
						) key_values ON ( key_values.property_setting_key_id = psk.id )
						LEFT JOIN (
									SELECT 
										id,
										property_id,
										cid,
										property_setting_key_id,
										created_on,
										new_text,
										new_value
									FROM (
											SELECT
												id,
												property_id,
												cid,
												property_setting_key_id,
												created_on,
												new_text,
												new_value,
												rank() OVER(PARTITION BY property_setting_key_id ORDER BY created_on DESC) AS rank
											FROM
												property_setting_logs
											WHERE
												property_id = ' . ( int ) $intTemplatePropertyId . '
												AND cid = ' . ( int ) $intCid . '
												AND property_setting_key_id IN ( ' . implode( ',', $arrintPropertySettingKeyIds ) . ' )
									) AS logs
							WHERE logs.rank = 1 
						) AS setting_log_details ON setting_log_details.property_id = st.property_id AND setting_log_details.property_setting_key_id = psk.id AND setting_log_details.cid = sta.cid
					WHERE
						st.property_id = ' . ( int ) $intTemplatePropertyId . '
						AND st.cid = ' . ( int ) $intCid . '
						AND st.settings_template_type_id = ' . ( int ) CSettingsTemplateType::TEMPLATE . '
						AND sta.property_setting_key_id IN ( ' . implode( ',', $arrintPropertySettingKeyIds ) . ' )';

		$arrmixResult = fetchData( $strSql, $objDatabase );
		return $arrmixResult;
	}

	public static function fetchPropertySettingKeysByGroupIdsByKeysByPropertyIdByCid( $arrintPropertySettingGroupIds, $arrstrPropertySettingKeys, $intPropertyId, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPropertySettingGroupIds ) ) return NULL;

		$strSelectClause 	= '';
		$strJoinClause 		= '';

		$strSql = 'SELECT
						psk.id as property_setting_key_id,
						psk.key,
						util_get_translated( \'label\', psk.label, psk.details ) AS caption,
						tt.key AS tooltip_key,
						CASE
							WHEN pp.id IS NULL AND pn.id IS NULL THEN psk.delete_value
							WHEN pp.id IS NULL AND pn.id IS NOT NULL THEN pn.value
							WHEN pp.id IS NOT NULL AND pn.id IS NULL THEN pp.value
						END AS value,
						psg.name group_name,
						CASE
							WHEN TRUE = psk.is_implementation_required THEN 1
							WHEN TRUE = psg.is_implementation_required THEN 1
							ELSE 0
						END AS is_required_for_setup,
						psk.implementation_description,
						CASE
							WHEN TRUE = psk.is_negated THEN 1
							ELSE 0
						END AS negate_value
						' . $strSelectClause . '
					FROM
						property_setting_keys psk
						LEFT JOIN tool_tips tt ON ( psk.tool_tip_id = tt.id )
						LEFT JOIN property_notifications pn ON ( ( psk.id = pn.property_setting_key_id OR psk.key = pn.key ) AND pn.cid = ' . ( int ) $intCid . ' AND ' . 'pn.property_id = ' . ( int ) $intPropertyId . ' )
						LEFT JOIN property_preferences pp ON ( ( psk.id = pp.property_setting_key_id OR psk.key = pp.key ) AND pp.cid = ' . ( int ) $intCid . ' AND ' . 'pp.property_id = ' . ( int ) $intPropertyId . ' )
						LEFT JOIN property_setting_groups psg ON ( psg.id = psk.property_setting_group_id )
						' . $strJoinClause . '
					WHERE
						psk.property_setting_group_id::int IN ( ' . implode( ',', $arrintPropertySettingGroupIds ) . ' )
						AND psk.key IN ( ' . implode( ',', $arrstrPropertySettingKeys ) . ' )';

		$arrmixResponseData = ( array ) fetchData( $strSql, $objClientDatabase );

		$arrmixPropertyPreferences = [];

		foreach( $arrmixResponseData as $arrmixResponse ) {
			$arrmixPropertyPreferences[$arrmixResponse['key']]['property_setting_key_id']		= $arrmixResponse['property_setting_key_id'];
			$arrmixPropertyPreferences[$arrmixResponse['key']]['key']							= $arrmixResponse['key'];
			$arrmixPropertyPreferences[$arrmixResponse['key']]['caption']						= $arrmixResponse['caption'];
			$arrmixPropertyPreferences[$arrmixResponse['key']]['value']							= $arrmixResponse['value'];
			$arrmixPropertyPreferences[$arrmixResponse['key']]['negate_value']					= $arrmixResponse['negate_value'];

			if( true == is_null( $arrmixResponse['tooltip_key'] ) || '' == trim( $arrmixResponse['tooltip_key'] ) ) {
				$arrmixPropertyPreferences[$arrmixResponse['key']]['tooltip_key'] = \Psi\CStringService::singleton()->strtoupper( 'PROPERTY_SETTING_' . str_replace( [ ' ', '(', ')', '&' ], [ '_' ], $arrmixResponse['group_name'] ) . '_' . $arrmixResponse['key'] );
			} else {
				$arrmixPropertyPreferences[$arrmixResponse['key']]['tooltip_key']				= $arrmixResponse['tooltip_key'];
			}
		}
		return $arrmixPropertyPreferences;

	}

	public static function fetchPropertySettingKeysByGroupIdsByKeysByPropertyIdsByCid( $arrintPropertySettingGroupIds, $arrstrPropertySettingKeys, $arrintPropertyIds, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ppk.key,
						util_get_system_translated( \'label\', ppk.label, ppk.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\'  ) AS caption,
						tt.key as tooltip_key,
						CASE
							WHEN ppreferences.property_id IS NULL AND pnotifications.property_id IS NULL THEN ppk.delete_value
							WHEN ppreferences.property_id IS NOT NULL AND ppreferences.count = pp_value_count AND pnotifications.property_id IS NULL THEN ppreferences.value
							WHEN ppreferences.property_id IS NULL AND pnotifications.property_id IS NOT NULL AND pnotifications.count = pn_value_count THEN pnotifications.value
							WHEN ppreferences.property_id Is NOT NULL AND TRIM(ppreferences.value) IS NULL THEN NULL
						ELSE \'Varies\'
						END AS value,
						CASE
							WHEN TRUE = ppk.is_implementation_required THEN 1
							WHEN TRUE = psg.is_implementation_required THEN 1
							ELSE 0
						END AS is_required_for_setup,
						psg.name group_name,
						CASE
							WHEN TRUE = ppk.is_negated THEN 1
							ELSE 0
						END AS negate_value,
						ppk.implementation_description
					FROM
						property_setting_keys ppk
						LEFT JOIN tool_tips tt ON (ppk.tool_tip_id = tt.id)
						LEFT JOIN (
							SELECT
								DISTINCT ON(key)
								MAX( rank ) OVER (PARTITION BY true ) AS count,
								pp_value_count,
								property_id,
								property_setting_key_id,
								key,
								value
							FROM (
								SELECT
									dense_rank() OVER( ORDER BY p.id ) AS rank,
									COUNT( pp.id ) OVER( PARTITION BY pp.key, pp.value ) AS pp_value_count,
									p.id AS property_id,
									pp.property_setting_key_id,
									pp.key,
									pp.value
								FROM
									properties p
									LEFT JOIN property_preferences pp ON( p.id = pp.property_id AND pp.cid = p.cid )
								WHERE
									p.cid = ' . ( int ) $intCid . '
									AND p.id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
							) AS sub
							WHERE
								key IS NOT NULL
						) AS ppreferences ON( ppreferences.key = ppk.key OR ppreferences.property_setting_key_id = ppk.id )
						LEFT JOIN (
							SELECT
								DISTINCT ON(key)
								MAX( rank ) OVER (PARTITION BY true ) AS count,
								pn_value_count,
								property_id,
								property_setting_key_id,
								key,
								value
							FROM (
									SELECT
										dense_rank() OVER( ORDER BY p.id ) AS rank,
										COUNT( pn.id ) OVER( PARTITION BY pn.key, pn.value ) AS pn_value_count,
										p.id AS property_id,
										pn.property_setting_key_id,
										pn.key,
										pn.value
									FROM
										properties p
										LEFT JOIN property_notifications pn ON( p.id = pn.property_id AND p.cid = pn.cid )
									WHERE
										p.cid = ' . ( int ) $intCid . '
										AND p.id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
							) AS sub
							WHERE
								key IS NOT NULL
						) AS pnotifications ON( pnotifications.key = ppk.key OR pnotifications.property_setting_key_id = ppk.id )
						LEFT JOIN property_setting_groups psg ON ( psg.id = ppk.property_setting_group_id )
					WHERE
						ppk.property_setting_group_id IN (' . implode( ',', $arrintPropertySettingGroupIds ) . ')
						AND ppk.key IN ( ' . implode( ',', $arrstrPropertySettingKeys ) . ' )
					ORDER BY ppk.key';

		$arrmixResponseData = ( array ) fetchData( $strSql, $objClientDatabase );

		$arrmixPropertyPreferences = [];

		foreach( $arrmixResponseData as $arrmixResponse ) {
			$arrmixPropertyPreferences[$arrmixResponse['key']]['key'] 			= $arrmixResponse['key'];
			$arrmixPropertyPreferences[$arrmixResponse['key']]['caption'] 		= $arrmixResponse['caption'];
			$arrmixPropertyPreferences[$arrmixResponse['key']]['value'] 		= $arrmixResponse['value'];
			$arrmixPropertyPreferences[$arrmixResponse['key']]['tooltip_key'] 	= $arrmixResponse['tooltip_key'];
			$arrmixPropertyPreferences[$arrmixResponse['key']]['negate_value'] 	= $arrmixResponse['negate_value'];

			if( true == $boolShowImplementationPortal ) {
				$arrmixPropertyPreferences[$arrmixResponse['key']]['is_required_for_setup']			= $arrmixResponse['is_required_for_setup'];
				$arrmixPropertyPreferences[$arrmixResponse['key']]['implementation_description']	= $arrmixResponse['implementation_description'];
			}
			if( true == is_null( $arrmixResponse['tooltip_key'] ) || '' == trim( $arrmixResponse['tooltip_key'] ) ) {
				$arrmixPropertyPreferences[$arrmixResponse['key']]['tooltip_key'] 	= \Psi\CStringService::singleton()->strtoupper( 'PROPERTY_SETTING_' . str_replace( ' ', '_', $arrmixResponse['group_name'] ) . '_' . $arrmixResponse['key'] );
			} else {
				$arrmixPropertyPreferences[$arrmixResponse['key']]['tooltip_key'] 	= $arrmixResponse['tooltip_key'];
			}
		}

		return $arrmixPropertyPreferences;
	}

	public static function fetchTemplatablePropertySettingKeysByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolWithCorporate = true, $boolIsTemplateAssociatedOnly = true ) {

		if( false == valId( $intPropertyId ) || false == valId( $intCid ) || true == empty( $objDatabase ) ) return NULL;
		$boolIsTemplatable = true;

		$strSql = 'SELECT
						DISTINCT ON ( psk.id ) psk.id AS property_setting_key_id,
						psk.key,
						psk.database_type_id,
						psk.table_name,
						psk.delete_value,
						psk.column_name,
						pp.id as preference_id,
						pn.id as notification_id,
						ppm.ps_product_id,
						psl.new_value,
						psl.new_text,
						key_values.property_setting_key_value_type_id,
						CASE
							WHEN pp.id IS NULL AND pn.id IS NULL THEN psk.delete_value
							WHEN pp.id IS NULL AND pn.id IS NOT NULL THEN pn.value
							WHEN pp.id IS NOT NULL AND pn.id IS NULL THEN pp.value
						END AS value, ' . $intPropertyId . ' as property_id';

			if( true == $boolIsTemplateAssociatedOnly ) {
				$strSql .= ', sta.id AS settings_template_assignment_id, sta.settings_template_id AS setting_template_id';
			}

		$strSql .= ' FROM
						property_setting_keys psk
					LEFT JOIN
						property_setting_groups psg
					ON
						( psg.id = psk.property_setting_group_id )
					LEFT JOIN
						product_modules ppm
					ON
						( ppm.module_id = psg.module_id )
					LEFT JOIN
						property_notifications pn
					ON
						( ( psk.id = pn.property_setting_key_id OR psk.key = pn.key ) AND pn.cid = ' . ( int ) $intCid . ' AND ' . 'pn.property_id = ' . ( int ) $intPropertyId . ' )
					LEFT JOIN
						property_preferences pp
					ON
						( ( psk.id = pp.property_setting_key_id OR psk.key = pp.key ) AND pp.cid = ' . ( int ) $intCid . ' AND ' . 'pp.property_id = ' . ( int ) $intPropertyId . ' )
					LEFT JOIN (
						SELECT
							pskv.property_setting_key_id,
							pskv.property_setting_key_value_type_id
						FROM
							property_setting_key_values pskv
						GROUP BY
							pskv.property_setting_key_id,
							pskv.property_setting_key_value_type_id
					) key_values ON ( key_values.property_setting_key_id = psk.id )
					LEFT JOIN
						property_setting_logs psl
					ON ( psl.property_setting_key_id = psk.id AND psl.cid = ' . ( int ) $intCid . ' AND ' . 'psl.property_id = ' . ( int ) $intPropertyId . ' )';

		if( true == $boolIsTemplateAssociatedOnly ) {
			$strSql .= ' JOIN
						settings_template_assignments sta
					ON
						( ( psk.id = sta.property_setting_key_id ) AND sta.cid = ' . ( int ) $intCid . ' AND ' . 'sta.property_id = ' . ( int ) $intPropertyId . ' )';
		}

		$strSql .= 'WHERE
						( psk.is_templatable = \'' . ( bool ) $boolIsTemplatable . '\' OR psg.is_templatable = \'' . ( bool ) $boolIsTemplatable . '\' )';

		if( false == $boolWithCorporate ) {
			$strSql .= ' AND psk.key NOT IN ( \'' . implode( '\',\'', CPropertySettingKey::$c_arrstrCorporateSettingKeys ) . '\' ) ';
		}

		$strSql .= ' ORDER BY
   						property_setting_key_id, psl.id DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRecurrsivePropertySettingKeysByParentModuleId( $intModuleId, $objClientDatabase, $intSettingTypeId, $arrstrPropertySettingKeys = NULL, $boolIsCustomSetting = false, $boolIsTotalCount = false, $arrintExcludePropertySettingGroups = NULL ) {
		$strSelectClause	= ' COUNT( DISTINCT psk.id ) as total_settings ';
		$strWhereClause 	= ( false == empty( $arrintExcludePropertySettingGroups ) ) ? ' psg.id NOT IN ( \'' . implode( '\',\'', $arrintExcludePropertySettingGroups ) . '\' )' : '1 = 1';
		$strOrderByClause	= '';

		if( false == $boolIsTotalCount ) {
			$strSelectClause = 'DISTINCT ON (psk.id)
				cm.module_id,
				cm.module_name,
				cm.parent_module_id,
				cm.module_url,
				psk.id,
				psk.key,
				psk.label,
				psg.description,
				psk.delete_value,
				psk.table_name,
				psk.column_name,
				CASE WHEN psk.database_type_id = 0 THEN ' . CDatabaseType::CLIENT . ' ELSE psk.database_type_id END AS database_type_id,
				cm.depth,
				cm.group_module_id,
				pskv.property_setting_key_value_type_id,
				pskv.table_name AS lookup_table_name,
				pskv.column_name AS lookup_column_name,
				pskv.display_text,
				CASE WHEN psk.is_templatable IS TRUE OR psg.is_templatable IS TRUE THEN 1 ELSE 0 END AS is_templatable';

			$strWhereClause .= ( true == $boolIsCustomSetting ) ? ' AND psk.table_name IS NOT NULL AND psk.column_name IS NOT NULL ' : ' AND psk.table_name IS NULL AND psk.column_name IS NULL ';
			$strWhereClause .= ( true == $boolIsCustomSetting && true == valArr( $arrstrPropertySettingKeys ) ) ? ' AND psk.key NOT IN ( \'' . implode( '\',\'', $arrstrPropertySettingKeys ) . '\' )' : '';
			$strWhereClause .= ( false == $boolIsCustomSetting && true == valArr( $arrstrPropertySettingKeys ) ) ? ' AND psk.key IN ( \'' . implode( '\',\'', $arrstrPropertySettingKeys ) . '\' )' : '';

			if( 1 == $intSettingTypeId ) {
				$strWhereClause .= ' AND ( psk.is_templatable IS TRUE OR psg.is_templatable IS TRUE )';
			} elseif( 2 == $intSettingTypeId ) {
				$strWhereClause .= ' AND ( psk.is_templatable IS FALSE AND psg.is_templatable IS FALSE )';
			}

			$strOrderByClause = 'ORDER BY psk.id, cm.depth';
		}

		$strSql = 'WITH RECURSIVE child_modules( module_id, module_name, parent_module_id, depth, group_module_id, module_url ) AS (
				SELECT m.id, util_get_translated( \'title\', m.title, m.details ) AS title, m.parent_module_id, 1 as depth, m.id AS group_module_id, m.url AS module_url FROM modules m WHERE ( m.parent_module_id = ' . ( int ) $intModuleId . ' OR m.id = ' . ( int ) $intModuleId . ' ) AND m.is_published = 1
				UNION ALL
				SELECT m.id, util_get_translated( \'title\', m.title, m.details ) AS title, m.parent_module_id, depth + 1 as depth, group_module_id, m.url AS module_url FROM child_modules AS cm, modules AS m WHERE m.parent_module_id = cm.module_id AND m.is_published = 1
			)

			SELECT
				' . $strSelectClause . '
			FROM
				child_modules cm
				JOIN property_setting_groups psg ON ( psg.module_id = cm.module_id )
				JOIN property_setting_keys psk ON ( psk.property_setting_group_id = psg.id AND psk.is_disabled = FALSE )
				LEFT JOIN property_setting_key_values pskv ON ( pskv.property_setting_key_id = psk.id )
			WHERE
				' . $strWhereClause . $strOrderByClause;

		if( true == $boolIsTotalCount ) {
			$arrintTotalSettings = fetchData( $strSql, $objClientDatabase );

			if( false == empty( $arrintTotalSettings[0]['total_settings'] ) ) {
				return $arrintTotalSettings[0]['total_settings'];
			} else {
				return 0;
			}
		} else {
			return fetchData( $strSql, $objClientDatabase );
		}
	}

	public static function checkIsVisaPilotEnabled( $intCid, $objDatabase, $intPropertyId = NULL, $arrintPropertyIds = NULL ) {
		$boolIsEnableVisaPilot = true;
		$arrmixPropertyMerchantAccounts = ( array ) CMerchantAccounts::fetchMerchantAccountsByPropertyIdByCid( $intCid, $objDatabase, $intPropertyId, $arrintPropertyIds );
		if( false == valArr( $arrmixPropertyMerchantAccounts ) ) {
			return false;
		}
		foreach( $arrmixPropertyMerchantAccounts as $arrmixPropertyMerchantAccount ) {
			if( true == isset( $arrmixPropertyMerchantAccount['enable_visa_pilot'] ) ) {
				if( 'f' == $arrmixPropertyMerchantAccount['enable_visa_pilot'] ) {
					$boolIsEnableVisaPilot = false;
					break;
				}
			}
		}
		return $boolIsEnableVisaPilot;
	}

	public static function fetchTemplateAssociatedPropertySettingKeysInfoByKeys( $arrmixKeysTemplateIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrmixKeysTemplateIds ) || false == valId( $intCid ) || true == empty( $objDatabase ) ) return NULL;

		$strKeysTemplates = [];
		foreach( $arrmixKeysTemplateIds as $strKey => $intTemplatePropertyId ) {
			$strKeysTemplates[] = '( \'' . $strKey . '\', \'' . $intTemplatePropertyId . '\' )';
		}

		$strSql = 'SELECT
						psk.id as property_setting_key_id,
						psk.key,
						psk.table_name,
						psk.column_name,
						pp.id as preference_id,
						pn.id as notification_id,
						CASE
							WHEN pp.id IS NULL AND pn.id IS NULL THEN psk.delete_value
							WHEN pp.id IS NULL AND pn.id IS NOT NULL THEN pn.value
							WHEN pp.id IS NOT NULL AND pn.id IS NULL THEN pp.value
						END AS value,
						sta.id as settings_template_assignment_id,
						sta.property_id
					FROM
						property_setting_keys as psk
					LEFT JOIN
						settings_template_assignments as sta
					ON
						( psk.id = sta.property_setting_key_id AND sta.cid = ' . ( int ) $intCid . ' AND sta.property_id IN ( \'' . implode( '\',\'', array_values( $arrmixKeysTemplateIds ) ) . '\' ) )
					LEFT JOIN
						property_notifications as pn
					ON
						( psk.id = pn.property_setting_key_id AND ( psk.key, sta.property_id )
						IN ( ' . implode( ',', $strKeysTemplates ) . ' ) AND pn.cid = ' . ( int ) $intCid . ' )
					LEFT JOIN
						property_preferences as pp
					ON
						( psk.id = pp.property_setting_key_id AND ( pp.key, pp.property_id )
						IN ( ' . implode( ',', $strKeysTemplates ) . ' ) AND pp.cid = ' . ( int ) $intCid . ' )
					
					WHERE
					
					 psk.key IN ( \'' . implode( '\',\'', array_keys( $arrmixKeysTemplateIds ) ) . '\' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyGlSettingKeysByPropertySettingGroupIds( $arrintPropertySettingGroupIds, $objDatabase ) {
		if( false == valArr( $arrintPropertySettingGroupIds ) ) return NULL;

		$strSql = 'SELECT
						 psk.*
					 FROM
						 property_setting_keys psk
					 WHERE
						 psk.property_setting_group_id IN ( ' . implode( ',', $arrintPropertySettingGroupIds ) . ' )';

		$arrmixResponseData = ( array ) fetchData( $strSql, $objDatabase );

		$arrmixPropertyPreferences = [];

		foreach( $arrmixResponseData as $arrmixResponse ) {
			$arrmixPropertyPreferences[$arrmixResponse['key']]['id']			= $arrmixResponse['id'];
			$arrmixPropertyPreferences[$arrmixResponse['key']]['key']			= $arrmixResponse['key'];
			$arrmixPropertyPreferences[$arrmixResponse['key']]['caption']		= $arrmixResponse['caption'];
			$arrmixPropertyPreferences[$arrmixResponse['key']]['value']			= $arrmixResponse['value'];
			$arrmixPropertyPreferences[$arrmixResponse['key']]['is_negated']	= $arrmixResponse['is_negated'];

			if( true == is_null( $arrmixResponse['tooltip_key'] ) || '' == trim( $arrmixResponse['tooltip_key'] ) ) {
				$arrmixPropertyPreferences[$arrmixResponse['key']]['tooltip_key'] = \Psi\CStringService::singleton()->strtoupper( 'PROPERTY_SETTING_' . str_replace( [ ' ', '(', ')', '&' ], [ '_' ], $arrmixResponse['group_name'] ) . '_' . $arrmixResponse['key'] );
			} else {
				$arrmixPropertyPreferences[$arrmixResponse['key']]['tooltip_key']				= $arrmixResponse['tooltip_key'];
			}
		}

		return $arrmixPropertyPreferences;
	}

	public static function fetchPropertySettingKeyValidationByKeys( $arrstrKeys, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = ' SELECT
						psk.id,
						psk.property_setting_group_id,
						psk.key,
						psk.label,
						psg.module_id,
						m.parent_module_id,
						m.name,
						psk.options::json->>\'is_only_for_admin_user\' AS is_only_for_admin,
						psk.options::json->>\'is_only_for_psi_user\' AS is_only_for_PSI_admin,
						psk.options::json->>\'is_only_for_integrated_property\' AS is_only_for_integrated,
						psk.options::json->>\'is_only_for_pm_enabled_property\' AS is_only_for_pm_enabled,
						psk.options::json->>\'ps_product_ids\' AS ps_product_ids,
						psk.options::json->>\'property_type_ids\' AS property_type_ids,
						psk.options::json->>\'occupancy_type_ids\' AS occupancy_type_ids,
						psk.options::json->>\'subsidy_type_ids\' AS subsidy_type_ids';

		$strSql .= ' FROM
						property_setting_keys psk
						JOIN property_setting_groups psg ON ( psk.property_setting_group_id = psg.id AND psk.is_disabled = FALSE )
						JOIN modules m ON ( psg.module_id = m.id )
					WHERE
						psk.key IN ( \'' . implode( '\', \'', $arrstrKeys ) . '\' )';

		$arrmixKeyDetails = fetchData( $strSql, $objDatabase );

		$arrmixValidationKeyDetails = [];
		if( false == empty( $arrmixKeyDetails ) ) {
			foreach( $arrmixKeyDetails as $arrmixKeyDetail ) {
				$arrmixValidationKeyDetails[$arrmixKeyDetail['key']] 										= $arrmixKeyDetail;
				$arrmixValidationKeyDetails[$arrmixKeyDetail['key']]['is_only_for_admin'] 					= json_decode( $arrmixKeyDetail['is_only_for_admin'], true );
				$arrmixValidationKeyDetails[$arrmixKeyDetail['key']]['is_only_for_PSI_admin'] 				= json_decode( $arrmixKeyDetail['is_only_for_PSI_admin'], true );
				$arrmixValidationKeyDetails[$arrmixKeyDetail['key']]['is_only_for_integrated'] 				= json_decode( $arrmixKeyDetail['is_only_for_integrated'], true );
				$arrmixValidationKeyDetails[$arrmixKeyDetail['key']]['is_only_for_pm_enabled'] 				= json_decode( $arrmixKeyDetail['is_only_for_pm_enabled'], true );
				$arrmixValidationKeyDetails[$arrmixKeyDetail['key']]['ps_product_ids'] 						= json_decode( $arrmixKeyDetail['ps_product_ids'], true );
				$arrmixValidationKeyDetails[$arrmixKeyDetail['key']]['property_type_ids'] 					= json_decode( $arrmixKeyDetail['property_type_ids'], true );
				$arrmixValidationKeyDetails[$arrmixKeyDetail['key']]['occupancy_type_ids'] 					= json_decode( $arrmixKeyDetail['occupancy_type_ids'], true );
				$arrmixValidationKeyDetails[$arrmixKeyDetail['key']]['subsidy_type_ids'] 					= json_decode( $arrmixKeyDetail['subsidy_type_ids'], true );
			}
		}

		return $arrmixValidationKeyDetails;
	}

	public static function fetchCustomPropertySettingKeysByIds( $arrintPropertySettingKeyIds, $objDatabase ) {

		if( false == valArr( $arrintPropertySettingKeyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						psk.*
					FROM
						property_setting_keys psk
					WHERE
						psk.id IN ( ' . implode( ',', $arrintPropertySettingKeyIds ) . ' )
						AND psk.table_name IS NOT NULL AND psk.column_name IS NOT NULL';

		return parent::fetchPropertySettingKeys( $strSql, $objDatabase );
	}

	public static function fetchSimplePropertySettingsByIdsByPropertyIdByCid( $arrintPropertySettingKeyIds, $intPropertyId, $intCid, $objClientDatabase, $boolShowSettingImplementationSettings, $intVersionId ) {

		if( false == valArr( $arrintPropertySettingKeyIds ) ) {
			return NULL;
		}

		$strSelectClause	= '';
		$strJoinClause		= '';

		if( true == $boolShowSettingImplementationSettings ) {
			$strSelectClause = ' , ipscq.id AS question_id,
				ipscq.question AS caption,
				CASE
					WHEN TRUE = ipscqpd.is_completed THEN 1 ELSE 0
				END AS is_completed';

			$strJoinClause = 'LEFT JOIN ips_checklist_questions ipscq ON ( ipscq.reference_key_id = ppk.id AND ipscq.deleted_by IS NULL )
							JOIN ips_checklist_question_types AS icqt ON ( icqt.id = ipscq.ips_checklist_question_type_id AND icqt.is_published = TRUE )
							JOIN ips_checklists AS ic ON( ic.id = ipscq.ips_checklist_id AND ic.ips_portal_version_id = ' . ( int ) $intVersionId . ' ) 
							LEFT JOIN ips_checklist_question_property_details ipscqpd ON ( ( ipscqpd.ips_checklist_question_id = ipscq.id ) AND ipscqpd.cid = ' . ( int ) $intCid . ' AND ' . 'ipscqpd.property_id = ' . ( int ) $intPropertyId . ' )';
		}

		$strSql = 'SELECT
						ppk.id as property_setting_key_id,
						ppk.key,
						CASE
							WHEN pp.id IS NULL AND pn.id IS NULL THEN ppk.delete_value
							WHEN pp.id IS NULL AND pn.id IS NOT NULL THEN pn.value
							WHEN pp.id IS NOT NULL AND pn.id IS NULL THEN pp.value
						END AS value,
						CASE
							WHEN TRUE = ppk.is_implementation_required THEN 1
							ELSE 0
						END AS is_implementation_required,
						CASE
							WHEN TRUE = ppk.is_negated THEN 1
							ELSE 0
						END AS negate_value,
						key_values_details.property_setting_key_value_type_id,
						key_values_details.key_values
						' . $strSelectClause . '
					FROM
						property_setting_keys ppk
						LEFT JOIN property_notifications pn ON ( ( ppk.id = pn.property_setting_key_id OR ppk.key = pn.key ) AND pn.cid = ' . ( int ) $intCid . ' AND ' . 'pn.property_id = ' . ( int ) $intPropertyId . ' )
						LEFT JOIN property_preferences pp ON ( ( ppk.id = pp.property_setting_key_id OR ppk.key = pp.key ) AND pp.cid = ' . ( int ) $intCid . ' AND ' . 'pp.property_id = ' . ( int ) $intPropertyId . ' )
						LEFT JOIN (
							SELECT
								pskv.property_setting_key_id,
								pskv.property_setting_key_value_type_id,
								JSON_STRIP_NULLS(JSON_OBJECT_AGG( COALESCE( pskv.display_text, 0::TEXT ), pskv.property_setting_value ORDER BY pskv.id ) ) AS key_values
							FROM
								property_setting_key_values pskv
							GROUP BY
								pskv.property_setting_key_id,
								pskv.property_setting_key_value_type_id
						) key_values_details ON ( key_values_details.property_setting_key_id = ppk.id )
						' . $strJoinClause . '
					WHERE
						ppk.id::int IN ( ' . implode( ',', $arrintPropertySettingKeyIds ) . ' )
					ORDER BY
						ppk.id';

		$arrmixResponseData = fetchData( $strSql, $objClientDatabase );

		$arrmixPropertyPreferences = [];

		if( true == valArr( $arrmixResponseData ) ) {
			foreach( $arrmixResponseData as $arrmixResponse ) {
				$arrmixPropertyPreferences[$arrmixResponse['key']]['property_setting_key_id']	= $arrmixResponse['property_setting_key_id'];
				$arrmixPropertyPreferences[$arrmixResponse['key']]['key']						= $arrmixResponse['key'];
				$arrmixPropertyPreferences[$arrmixResponse['key']]['caption']					= $arrmixResponse['caption'];
				$arrmixPropertyPreferences[$arrmixResponse['key']]['value']						= $arrmixResponse['value'];
				$arrmixPropertyPreferences[$arrmixResponse['key']]['negate_value']				= $arrmixResponse['negate_value'];
				$arrmixPropertyPreferences[$arrmixResponse['key']]['key_values']				= json_decode( $arrmixResponse['key_values'], true );

				if( true == $boolShowSettingImplementationSettings ) {
					$arrmixPropertyPreferences[$arrmixResponse['key']]['is_implementation_required']	= $arrmixResponse['is_implementation_required'];
					$arrmixPropertyPreferences[$arrmixResponse['key']]['is_completed']					= $arrmixResponse['is_completed'];
				}
			}
		}

		return $arrmixPropertyPreferences;

	}

	public static function fetchSimpleCustomFieldByTableNameByColumnNameByPropertyIdByCid( $strTableName, $strColumnName, $strWhere = NULL, $mixPropertyId, $intCid, $objDatabase, $strJoin = NULL, $boolReturnSql = false ) {

		if( true == is_numeric( $mixPropertyId ) ) {
			$arrintPropertyIds = [ ( int ) $mixPropertyId ];
		} else if( true == valArr( $mixPropertyId ) ) {
			$arrintPropertyIds = $mixPropertyId;
		}

		if( false == valStr( $strTableName ) || false == valStr( $strColumnName ) || false == isset( $arrintPropertyIds ) ) return false;

		if( 'properties' == $strTableName ) {
			$strSql = 'SELECT DISTINCT (tn.id) AS property_id, tn.' . $strColumnName . ' FROM ' . $strTableName . ' tn WHERE tn.cid = ' . ( int ) $intCid . ' AND tn.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )' . $strWhere;
		} elseif( 'company_media_files' == $strTableName ) {
			$strSql = ' SELECT
							DISTINCT ON ( pm.property_id ) property_id,
							 cmf.*
						FROM
							company_media_files cmf,
							property_medias pm
						WHERE
							pm.company_media_file_id = cmf.id
							AND pm.cid = cmf.cid
							AND cmf.cid = ' . ( int ) $intCid . '
							AND pm.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )' . $strWhere . '
						ORDER BY
							pm.property_id, pm.order_num';
		} else {
			$strSql = 'SELECT
							DISTINCT ON ( tn.property_id ) property_id,' .
							( '*' != $strColumnName ? ' CASE
								WHEN pg_typeof( tn.' . $strColumnName . ' ) <> pg_typeof( NULL::BOOLEAN ) THEN tn.' . $strColumnName . '::text
								WHEN pg_typeof( tn.' . $strColumnName . ' ) = pg_typeof( NULL::BOOLEAN ) AND tn.' . $strColumnName . '::text = \'true\' THEN 1::text
								 ELSE 0::text
							END AS ' . $strColumnName : $strColumnName ) . '
						FROM
							' . $strTableName . ' tn
							' . $strJoin . '
						WHERE
							tn.cid = ' . ( int ) $intCid . '
							AND tn.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )' . $strWhere . ' ORDER BY tn.property_id, tn.id DESC';
		}

		if( true == $boolReturnSql ) {
			return $strSql;
		} else {
			$arrmixResponseData = fetchData( $strSql, $objDatabase );

			if( true == valArr( $arrmixResponseData ) ) {
				return $arrmixResponseData[0][$strColumnName];
			}
		}
	}

	public static function fetchSimpleValueByTableNameByColumnNamesByPropertyIdsByCid( $strTableName, $strColumnName, $strWhere = NULL, $arrintPropertyIds, $intCid, $objDatabase, $boolReturnSql = false ) {
		if( false == valStr( $strTableName ) || false == valStr( $strColumnName ) || false == valArr( $arrintPropertyIds ) ) return false;

		$strSql = 'SELECT property_id, ' . $strColumnName . ' FROM ' . $strTableName . ' tn WHERE tn.cid = ' . ( int ) $intCid . ' AND tn.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' . $strWhere;

		if( true == $boolReturnSql ) return $strSql;

		$arrstrResponseData = fetchData( $strSql, $objDatabase );

		$arrmixValues = [];
		if( true == valArr( $arrstrResponseData ) ) {
			foreach( $arrstrResponseData as $arrstrResponse => $arrstrResponseValue ) {

				$arrmixValues[$arrstrResponseValue['property_id']] = $arrstrResponseValue;
			}
		}

		return $arrmixValues;
	}

	public static function fetchCustomPropertyIdsByTableNameByCid( $strTableName, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valStr( $strTableName ) || false == valArr( $arrintPropertyIds ) ) return false;

		$strColumnName = 'tn.property_id';
		$strSelectColumnName = 'property_id';
		if( 'properties' == $strTableName ) {
			$strColumnName = 'tn.id';
			$strSelectColumnName = 'id';
		}

		$strSql = 'SELECT DISTINCT ' . $strSelectColumnName . ' FROM ' . $strTableName . ' tn WHERE tn.cid = ' . ( int ) $intCid . ' AND ' . $strColumnName . ' IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		$arrstrResponseData = fetchData( $strSql, $objDatabase );

		$arrmixValues = [];
		if( true == valArr( $arrstrResponseData ) ) {
			foreach( $arrstrResponseData as $arrstrResponse => $arrstrResponseValue ) {
				$arrmixValues[] = $arrstrResponseValue[$strSelectColumnName];
			}
		}

		return $arrmixValues;
	}

	public static function fetchCustomTableValuesByTableNameByColumnNameByPropertyIdsByCid( $strTableName, $strColumnName, $arrintPropertyIds, $intCid, $objDatabase, $strWhere = NULL, $strJoin = NULL ) {
		if( false == valStr( $strTableName ) || false == valStr( $strColumnName ) || false == valArr( $arrintPropertyIds ) ) return false;

		if( 'properties' == $strTableName ) {
			$strColumnNameAlise = ( 'property_id' == $strColumnName ) ? 'parent_property_id' : $strColumnName;

			$strSql = ' SELECT
							tn.id as property_id,
							CASE
								WHEN pg_typeof( tn.' . $strColumnName . ' ) <> pg_typeof( NULL::BOOLEAN ) THEN tn.' . $strColumnName . '::text
								WHEN pg_typeof( tn.' . $strColumnName . ' ) = pg_typeof( NULL::BOOLEAN ) AND tn.' . $strColumnName . '::text = \'true\' THEN 1::text
								ELSE 0::text
							END AS ' . $strColumnNameAlise . '
						FROM
							' . $strTableName . ' tn
						WHERE
							tn.cid = ' . ( int ) $intCid . '
							AND tn.id IN (  ' . implode( ',', $arrintPropertyIds ) . ' ) ' . $strWhere;

		} elseif( 'company_media_files' == $strTableName ) {
			$strSql = 'SELECT
							pm.property_id,
							cmf.*
						FROM
							company_media_files cmf,
							property_medias pm
						WHERE
							pm.company_media_file_id = cmf.id
							AND pm.cid = cmf.cid
							AND cmf.cid = ' . ( int ) $intCid . '
							AND pm.property_id IN (  ' . implode( ',', $arrintPropertyIds ) . ' ) ' . $strWhere . '
						ORDER BY
							pm.order_num';

		} else {
			$strSql = ' SELECT
							 tn.property_id,
							CASE
								WHEN pg_typeof( tn.' . $strColumnName . ' ) <> pg_typeof( NULL::BOOLEAN ) THEN tn.' . $strColumnName . '::text
								WHEN pg_typeof( tn.' . $strColumnName . ' ) = pg_typeof( NULL::BOOLEAN ) AND tn.' . $strColumnName . '::text = \'true\' THEN 1::text
								ELSE 0::text
							END AS ' . $strColumnName . '
						FROM
							' . $strTableName . ' tn
							' . $strJoin . '
						WHERE
							tn.cid = ' . ( int ) $intCid . '
							AND tn.property_id IN (  ' . implode( ',', $arrintPropertyIds ) . ' ) ' . $strWhere;
		}

		$arrstrResponseData = fetchData( $strSql, $objDatabase );

		$arrmixValues = [];
		if( true == valArr( $arrstrResponseData ) ) {
			foreach( $arrstrResponseData as $arrstrResponse => $arrstrResponseValue ) {
				$strColumnNameAlise = ( 'property_id' == $strColumnName ) ? 'parent_property_id' : $strColumnName;

				$arrmixValues[$arrstrResponseValue['property_id']][] = $arrstrResponseValue[$strColumnNameAlise];
			}
		}

		return $arrmixValues;
	}

	public static function fetchCustomOtherTableFieldByTableNameByColumnNameByPropertyIdsByCid( $strTableName, $strColumnName, $strWhere = NULL, $arrintPropertyIds, $intCid, $objDatabase, $boolIsCount = false, $strJoin = NULL ) {
		if( true == is_null( $strTableName ) || true == is_null( $strColumnName ) ) return NULL;

		$strSelect 					= '';
		$strColumnNameAlias 		= '';
		$strPropertyIdsWhereClause	= '';

		if( 'company_media_files' == $strTableName ) {
			$strSql = 'SELECT
							DISTINCT CASE
							WHEN result.count = result.pp_value_count THEN result.file_name::TEXT
								ELSE \'Varies\'
							END AS value
						FROM
							(
								SELECT
									DISTINCT ON ( file_name ) MAX ( rank ) OVER ( PARTITION BY TRUE ) AS count,
									pp_value_count,
									file_name
								FROM
									(
										SELECT
											dense_rank ( ) OVER (
										ORDER BY
											pm.property_id ) AS rank,
											pm.property_id,
											COUNT ( cmf.id ) OVER ( PARTITION BY cmf.file_name ) AS pp_value_count,
											cmf.file_name
										FROM
											company_media_files cmf,
											property_medias pm
										WHERE
											pm.company_media_file_id = cmf.id
											AND pm.cid = cmf.cid
											AND cmf.cid = ' . ( int ) $intCid . '
											AND pm.property_id IN(  ' . implode( ',', $arrintPropertyIds ) . ' )' . $strWhere . '

										 ) AS sub
								WHERE
									file_name IS NOT NULL
								) AS result';

		} else {
			if( 'properties' == $strTableName ) {
				$strColumnNameAlias 		= ( 'property_id' == $strColumnName ) ? 'parent_property_id' : $strColumnName;
				$strSelect 					= ' SELECT
													dense_rank() OVER( ORDER BY tn.id ) AS rank, 
													COUNT( tn.id ) OVER( PARTITION BY tn.property_id ) AS pp_value_count,
													tn.id as property_id, tn.' . $strColumnName . ' AS ' . $strColumnNameAlias;
				$strPropertyIdsWhereClause	= ' AND tn.id IN( ' . implode( ',', $arrintPropertyIds ) . ' )';
				$strColumnName 				= $strColumnNameAlias;
			} else {
				$strSelect					= ' SELECT
													dense_rank() OVER( ORDER BY tn.property_id ) AS rank, 
													COUNT( tn.id ) OVER( PARTITION BY tn.' . $strColumnName . ' ) AS pp_value_count,
													tn.property_id, tn.' . $strColumnName;
				$strPropertyIdsWhereClause	= ' AND tn.property_id IN(  ' . implode( ',', $arrintPropertyIds ) . ' )';
			}

			$strSql = 'SELECT DISTINCT
						CASE
							WHEN result.count = result.pp_value_count THEN result.' . $strColumnName . '::TEXT
						ELSE \'Varies\'
						END AS value
					FROM
					(
						SELECT
							DISTINCT ON(  ' . $strColumnName . ' )
							MAX( rank ) OVER ( PARTITION BY true ) AS count,
							pp_value_count,
							' . $strColumnName . '
						FROM (
								' . $strSelect . '
								FROM
									' . $strTableName . ' tn
									' . $strJoin . '
								WHERE
									tn.cid = ' . ( int ) $intCid .
									$strPropertyIdsWhereClause . $strWhere . '
						 ) AS sub
						WHERE
							' . $strColumnName . ' IS NOT NULL
					) AS result';
		}

		$arrmixResponseData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResponseData ) ) {
			if( true == $boolIsCount ) {
				return 1;
			}
			return $arrmixResponseData[0]['value'];
		} else {
			return 0;
		}
	}

	public static function fetchCustomPropertySettingKeysDetailsById( $intPropertySettingKeyId, $objClientDatabase ) {

		if( false == valId( $intPropertySettingKeyId ) ) return NULL;

		$strSql = '		SELECT
							psk.label,
							psk.old_label,
							tt.title,
							tt.client_description,
							psg.name AS group_name,
							m.url,
							m.id AS module_id
						FROM
							property_setting_keys psk
							LEFT JOIN property_setting_groups psg ON psg.id = psk.property_setting_group_id
							LEFT JOIN modules m ON m.id = psg.module_id
							LEFT JOIN tool_tips tt ON tt.id = psk.tool_tip_id
						WHERE
							psk.id = ' . ( int ) $intPropertySettingKeyId;

		$arrmixValues = fetchData( $strSql, $objClientDatabase );

		if( true == valArr( $arrmixValues ) && true == isset( $arrmixValues[0] ) ) {
			return $arrmixValues[0];
		} else {
			return [];
		}

	}

}
?>