<?php

class CEmployeeStatusType extends CBaseEmployeeStatusType {

	const CURRENT		= 1;
	const PREVIOUS		= 2;
	const SYSTEM		= 3;
	const NO_SHOW		= 4;
	const RESTRICTED	= 5;

	public static $c_arrintNonSystemEmployeeStatusTypes = [ self::CURRENT, self::PREVIOUS ];

	public static $c_arrintEmployeeStatusTypeIds				= [ self::CURRENT, self::PREVIOUS, self::SYSTEM ];
	public static $c_arrintCurrentSystemEmployeeStatusTypeIds	= [ self::CURRENT, self::SYSTEM ];
	public static $c_arrintActiveEmployeeStatusTypeIds			= [ self::CURRENT, self::SYSTEM, self::RESTRICTED ];
	public static $c_arrintAllEmployeeStatusTypeIds				= [ self::CURRENT, self::PREVIOUS, self::SYSTEM, self::NO_SHOW, self::RESTRICTED ];

}
?>