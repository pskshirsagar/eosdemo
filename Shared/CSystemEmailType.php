<?php

class CSystemEmailType extends CBaseSystemEmailType {
	const PS_EMPLOYEE								= 1;
	const PS_TO_COMPANY_ADMINISTRATIVE				= 2;
	const PS_TO_COMPANY_ACCOUNTING					= 3;
	const PS_TO_COMPANY_MARKETING					= 4;
	const PS_TO_RESIDENT_MARKETING					= 5;
	const COMPANY_EMPLOYEE							= 6;
	const MAINTENANCE			 					= 7;
	const COMPANY_PAYMENT_PROCESSED 				= 8;
	const COMPANY_PAYMENT_ERROR						= 9;
	const APPLICATION_DOCUMENT						= 10;
	const COMPANY_GUEST_CARD						= 11;
	const COMPANY_NEWS_LETTER						= 12;
	const RENT_REMINDER 							= 13;
	const AUTO_POST_RECURRING_CHARGES 				= 14;
	const AUTO_POST_RECURRING_PAYMENTS 				= 15;
	const NSF_NOTICE 								= 16;
	const AR_PAYMENT_NOTIFICATION 					= 17;
	const SETTLEMENT_DISTRIBUTION		 			= 18;
	const PROSPECT_EMAILER			 				= 19;
	const DELETE_SCHEDULED_PAYMENT_NOTIFICATION		= 20;
	const SCHEDULED_PAYMENT_WARNING	 				= 21;
	const CONCIERGE_SERVICE	 						= 22;
	const CUSTOMER_PROFILE_UPDATE					= 23;
	const ILS_NOTIFICATION_EMAIL					= 24;
	const AVAILABILITY_ALERT_REMINDER				= 25;
	const STATISTICS_EMAIL							= 26;
	const CUSTOMER_REFERRAL							= 27;
	const BANK_NOTIFICATION							= 28;
	const SCHEDULED_PAYMENT_CONFIRMATION			= 29;
	const BOUNCE_EMAIL_NOTIFICATION					= 30;
	const TASK										= 31;
	const CONTACT_SUBMISSIONS						= 32;
	const SCHEDULED_EMAIL							= 33;
	const MASS_EMAIL								= 34;
	const TESTIMONIAL								= 35;
	const FEEDBACK									= 36;
	const ENROLLMENT								= 37;
	const REPUTATION_ADVISOR						= 109;
	const REPAYMENT_AGREEMENT_LATE_NOTICE			= 111;
	const DELINQUENCY_NOTIFICATIONS					= 112;
	const DELINQUENCY_CRUCIAL_NOTIFICATIONS			= 114;
	const CAMPAIGN									= 115;
	const SUPPORT_SURVEY_NOTIFICATION				= 117;
	const CALL_TRACKER_EMAIL						= 132;
	const RATES_CACHING								= 136;
	const REVIEW_RESPONSE_EMAIL						= 135;
	const SCHEDULED_REPORTS							= 146;

	// 2009.08.11 I SAW TWO NEW EMAIL TYPES ON LOCAL, BUT THERE WERE NO CONSTANTS,
	// SO I JUST ADDED THIS ONE AS 40.  If you see this and there are still no 39 id, use them and kill this comment.
	const CHARGE_BACK									= 40;
	const SCHEDULED_PARTIALLY_COMPLETED_APPLICATIONS	= 41;

	const PARCEL_ALERT									= 42;
	const VOICE_EMAIL									= 43;
	const VANITY_NUMBER_REQUEST							= 44;
	const SALES_SUPPORT_PEEP_EMAIL						= 45;
	const METER_EXCEPTION_REPORT						= 46;
	const NEW_SALE										= 47;
	const PROFILE_UPDATE								= 48;
	const PSI_CONTACT_SUBMISSIONS						= 49;
	const VACANCY_CONTACT_SUBMISSIONS					= 50;
	const LEASE_SIGNED									= 51;
	const LEASE_APPROVED								= 52;
	const SCHEDULED_PAYMENT_PARTICIPATION_REPORT		= 53;
	const EVENT_RESPONSE_NOTIFICATION					= 54;
	const RESIDENT_UTILITY_INVOICE						= 55;
	const PAYMENT_INTEGRATION_FAILURE					= 56;
	const CLEARING_BATCH_NOTIFICATION					= 57;
	const LEASE_RENEWAL									= 58;
	const APPLICATION_CONTACT_SUBMISSIONS				= 59;
	const UTILITY_BILL_EMAIL_NOTIFICATION				= 61;
	const MESSAGE_CENTER_EMAIL							= 62;
	const INSURANCE_POLICY_STATEMENT					= 63;
	const CORPORATE_CONTACT								= 64;
	const UNDELIVERED_RESIDENT_INVOICES_NOTIFICATION	= 65;
	const INVOICE_STAT_NOTIFICATION						= 66;
	const EMPLOYEE_APPLICATION_EMAIL					= 67;
	const OWNER_PORTAL_EMAIL							= 68;
	const CLUB_APPROVAL_NOTIFICATION					= 69;
	const EVENT_APPROVAL_NOTIFICATION					= 70;
	const CLASSIFIED_APPROVAL_NOTIFICATION				= 71;
	const TRAINER_APPROVAL_NOTIFICATION					= 72;
	const CLUB_REJECTED_NOTIFICATION					= 73;
	const EVENT_REJECTED_NOTIFICATION					= 74;
	const CLASSIFIED_REJECTED_NOTIFICATION				= 75;
	const COMPANY_WELCOME_CALL_EMAIL					= 76;
	const BRANCH_COMMIT_NOTIFICATION					= 78;
	const AMENITY_RESERVATION_NOTIFICATION				= 79;
	const LEASE_LATE_NOTICES							= 80;
	const LEASE_COLLECTIONS								= 81;
	const IIS_SERVER_DOWN								= 82;

	const PURCHASE_REQUEST_NOTIFICATION					= 85;
	const CONVENTIONAL_WAIT_LIST						= 86;
	const CONTRACT_SIGNED								= 87;

	const MISCELLANEOUS_PAYMENT_NOTIFICATION			= 89;
	const CALL_ANALYSIS_NOTIFICATION					= 90;
	const INSPECTION_MANAGER							= 91;
	const TOOLTIP_REQUEST								= 92;
	const INSURANCE_NOTIFICATION						= 93;
	const LEASING_CENTER_SURVEY_NOTIFICATION			= 94;
	const RESIDENT_GUEST_MANAGER						= 95;
	const LEASING_CENTER_STATISTICAL_EMAIL				= 96;

	const BULK_DOCUMENT_GENERATED						= 99;
	const JOB_POSTING_NOTIFICATION						= 101;

	const EVENT_SCHEDULER_EMAIL							= 102;
	const DOCUMENT_MANAGEMENT							= 103;

	const FMO_STATEMENT									= 104;
	const MESSAGE_CENTER_TEST_EMAIL						= 105;
	const NON_RENEWAL_NOTICE							= 106;
	const RESIDENT_INSURE_EMAIL							= 107;
	const ENTRATA_PASSWORD								= 108;
	const LEASE_PARTIALLY_COMPLETED						= 110;
	const CUSTOM_CONTACT								= 116;

	const FIRST_RENTERS_INSURANCE_NOTIFICATION			= 118;
	const SECOND_RENTERS_INSURANCE_NOTIFICATION			= 119;
	const RI_PROMOTIONAL_EMAIL							= 120;
	const COMPLIANCE_EMAIL_TO_UNINSURED_RESIDENTS		= 121;
	const PRICING_PUBLISHING_NOTIFICATION				= 122;
	const PRICING_APPROVE_NOTIFICATION					= 123;
	const VENDOR_ACCESS_EMAIL							= 125;
	const QUOTE_EMAIL									= 126;
	const SCREENING_EMAILS								= 127;
	const LEASING_CENTER_CONTACT						= 128;
	const BUA_ROOMMATE_NOTIFICATION                     = 129;

	const BLOG_NOTIFICATION                             = 130;
	const RABBITMQ_ALERT_EMAIL						 	= 131;
	const LEASE_COMPLETELY_SIGNED_NOTIFICATION          = 132;
	const KEEP_MY_VOICE                                 = 133;

	const CORPORATE_CARE_REQUEST_EMAIL					= 134;
	const LEASE_ERROR_NOTIFICATION						= 137;
	const PRICING_REVIEW_NOTIFICATION                   = 138;
	const PRICING_DENIAL_NOTIFICATION                   = 139;
	const PRICING_RENEWAL_APPROVE_NOTIFICATION          = 140;
	const PRICING_RENEWAL_DENIAL_NOTIFICATION           = 141;
	const PRICING_RENEWAL_PUBLISHING_NOTIFICATION       = 142;
	const RESIDENT_UTILITY_INVOICE_WITH_USAGE_HISTORY   = 143;
	const EMAIL_VIOLATION_NOTICE                        = 144;
	const PS_TO_THIRD_PARTY_VENDOR                      = 145;
	const AFFORDABLE_NOTIFICATION                 		= 147;

	const RESIDENT_COMMUNICATION                        = 162;
	const SCREENING_TOKEN_EMAILS                        = 163;
	const SIMPLEMENTATION_PORTAL_NOTIFICATION           = 164;
	const VENDOR_COMMUNICATION							= 166;
	const PAYMENT_ACCOUNT_NOTIFICATION					= 167;
	const CUSTOMER_INVOICE_NOTIFICATION					= 168;

	public static $c_arrintClickTrackEnableSystemEmailTypeIds		= [ self::MESSAGE_CENTER_EMAIL, self::RENT_REMINDER, self::EVENT_SCHEDULER_EMAIL, self::COMPANY_EMPLOYEE ];
	public static $c_arrintConfidentialSystemEmailTypeIds			= [ self::PS_EMPLOYEE, self::PURCHASE_REQUEST_NOTIFICATION ];
	public static $c_arrintCleanUpIgnoreSystemEmailTypeIds			= [ self::TASK, self::VACANCY_CONTACT_SUBMISSIONS, self::LEASE_SIGNED, self::LEASE_APPROVED, self::LEASE_RENEWAL, self::APPLICATION_CONTACT_SUBMISSIONS, self::INSURANCE_POLICY_STATEMENT, self::INSURANCE_NOTIFICATION, self::NON_RENEWAL_NOTICE ];

	public static $c_arrintQueuedSystemEmailTypeIds = [
		CSystemEmailType::PS_EMPLOYEE,
		CSystemEmailType::COMPANY_GUEST_CARD,
		CSystemEmailType::SCHEDULED_PARTIALLY_COMPLETED_APPLICATIONS,
		CSystemEmailType::ENTRATA_PASSWORD,
		CSystemEmailType::BRANCH_COMMIT_NOTIFICATION,
		CSystemEmailType::ILS_NOTIFICATION_EMAIL,
		CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL,
		CSystemEmailType::FEEDBACK,
		CSystemEmailType::TESTIMONIAL,
		CSystemEmailType::AVAILABILITY_ALERT_REMINDER,
		CSystemEmailType::PURCHASE_REQUEST_NOTIFICATION,
		CSystemEmailType::TOOLTIP_REQUEST,
		CSystemEmailType::PSI_CONTACT_SUBMISSIONS,
		CSystemEmailType::SUPPORT_SURVEY_NOTIFICATION,
		CSystemEmailType::COMPANY_PAYMENT_ERROR,
		CSystemEmailType::PS_TO_COMPANY_MARKETING,
		CSystemEmailType::NSF_NOTICE,
		CSystemEmailType::AR_PAYMENT_NOTIFICATION,
		CSystemEmailType::DELETE_SCHEDULED_PAYMENT_NOTIFICATION,
		CSystemEmailType::SCHEDULED_PAYMENT_WARNING,
		CSystemEmailType::CONCIERGE_SERVICE,
		CSystemEmailType::CUSTOMER_PROFILE_UPDATE,
		CSystemEmailType::CUSTOMER_REFERRAL,
		CSystemEmailType::BANK_NOTIFICATION,
		CSystemEmailType::SCHEDULED_PAYMENT_CONFIRMATION,
		CSystemEmailType::MASS_EMAIL,
		CSystemEmailType::ENROLLMENT,
		CSystemEmailType::CHARGE_BACK,
		CSystemEmailType::SALES_SUPPORT_PEEP_EMAIL,
		CSystemEmailType::NEW_SALE,
		CSystemEmailType::PROFILE_UPDATE,
		CSystemEmailType::LEASE_SIGNED,
		CSystemEmailType::PAYMENT_INTEGRATION_FAILURE,
		CSystemEmailType::PROSPECT_EMAILER,
		CSystemEmailType::REPUTATION_ADVISOR,
		CSystemEmailType::QUOTE_EMAIL,
		CSystemEmailType::APPLICATION_CONTACT_SUBMISSIONS,
		CSystemEmailType::CONTACT_SUBMISSIONS,
		CSystemEmailType::MAINTENANCE,
		CSystemEmailType::DELINQUENCY_NOTIFICATIONS,
		CSystemEmailType::DELINQUENCY_CRUCIAL_NOTIFICATIONS,
		CSystemEmailType::VENDOR_ACCESS_EMAIL,
		CSystemEmailType::EVENT_SCHEDULER_EMAIL,
		CSystemEmailType::AMENITY_RESERVATION_NOTIFICATION,
		CSystemEmailType::MESSAGE_CENTER_EMAIL,
		CSystemEmailType::JOB_POSTING_NOTIFICATION,
		CSystemEmailType::BLOG_NOTIFICATION,
		CSystemEmailType::MESSAGE_CENTER_TEST_EMAIL,
		CSystemEmailType::LEASING_CENTER_CONTACT,
		CSystemEmailType::LEASING_CENTER_STATISTICAL_EMAIL,
		CSystemEmailType::LEASING_CENTER_SURVEY_NOTIFICATION,
		CSystemEmailType::CALL_ANALYSIS_NOTIFICATION,
		CSystemEmailType::TASK,
		CSystemEmailType::CAMPAIGN,
		CSystemEmailType::VOICE_EMAIL,
		CSystemEmailType::PS_TO_COMPANY_ADMINISTRATIVE,
		CSystemEmailType::RESIDENT_GUEST_MANAGER,
		CSystemEmailType::PS_TO_COMPANY_ACCOUNTING,
		CSystemEmailType::DOCUMENT_MANAGEMENT,
		CSystemEmailType::UTILITY_BILL_EMAIL_NOTIFICATION,
		CSystemEmailType::RESIDENT_UTILITY_INVOICE,
		CSystemEmailType::INSURANCE_POLICY_STATEMENT,
		CSystemEmailType::FIRST_RENTERS_INSURANCE_NOTIFICATION,
		CSystemEmailType::SECOND_RENTERS_INSURANCE_NOTIFICATION,
		CSystemEmailType::RI_PROMOTIONAL_EMAIL,
		CSystemEmailType::COMPLIANCE_EMAIL_TO_UNINSURED_RESIDENTS,
		CSystemEmailType::RESIDENT_INSURE_EMAIL,
		CSystemEmailType::INSURANCE_NOTIFICATION,
		CSystemEmailType::LEASE_RENEWAL,
		CSystemEmailType::COMPANY_EMPLOYEE,
		CSystemEmailType::CONTRACT_SIGNED,
		CSystemEmailType::AUTO_POST_RECURRING_CHARGES,
		CSystemEmailType::REPAYMENT_AGREEMENT_LATE_NOTICE,
		CSystemEmailType::LEASE_COLLECTIONS,
		CSystemEmailType::NON_RENEWAL_NOTICE,
		CSystemEmailType::COMPANY_PAYMENT_PROCESSED,
		CSystemEmailType::PARCEL_ALERT,
		CSystemEmailType::COMPANY_WELCOME_CALL_EMAIL,
		CSystemEmailType::UNDELIVERED_RESIDENT_INVOICES_NOTIFICATION,
		CSystemEmailType::BUA_ROOMMATE_NOTIFICATION,
		CSystemEmailType::PRICING_PUBLISHING_NOTIFICATION,
		CSystemEmailType::PRICING_APPROVE_NOTIFICATION,
		CSystemEmailType::CUSTOM_CONTACT,
		CSystemEmailType::CORPORATE_CONTACT,
		CSystemEmailType::OWNER_PORTAL_EMAIL,
		CSystemEmailType::SCHEDULED_EMAIL,
		CSystemEmailType::APPLICATION_DOCUMENT,
		CSystemEmailType::CLUB_APPROVAL_NOTIFICATION,
		CSystemEmailType::EVENT_APPROVAL_NOTIFICATION,
		CSystemEmailType::CLASSIFIED_APPROVAL_NOTIFICATION,
		CSystemEmailType::EVENT_REJECTED_NOTIFICATION,
		CSystemEmailType::CLASSIFIED_REJECTED_NOTIFICATION,
		CSystemEmailType::CLUB_REJECTED_NOTIFICATION,
		CSystemEmailType::PS_TO_RESIDENT_MARKETING,
		CSystemEmailType::SETTLEMENT_DISTRIBUTION,
		CSystemEmailType::BULK_DOCUMENT_GENERATED,
		CSystemEmailType::LEASE_PARTIALLY_COMPLETED,
		CSystemEmailType::LEASE_APPROVED,
		CSystemEmailType::LEASE_COMPLETELY_SIGNED_NOTIFICATION,
		CSystemEmailType::IIS_SERVER_DOWN,
		CSystemEmailType::STATISTICS_EMAIL,
		CSystemEmailType::CORPORATE_CARE_REQUEST_EMAIL,
		CSystemEmailType::PRICING_REVIEW_NOTIFICATION,
		CSystemEmailType::PRICING_DENIAL_NOTIFICATION,
		CSystemEmailType::PRICING_RENEWAL_APPROVE_NOTIFICATION,
		CSystemEmailType::PRICING_RENEWAL_DENIAL_NOTIFICATION,
		CSystemEmailType::PRICING_RENEWAL_PUBLISHING_NOTIFICATION,
		CSystemEmailType::EMAIL_VIOLATION_NOTICE,
		CSystemEmailType::SCHEDULED_REPORTS,
		CSystemEmailType::RESIDENT_COMMUNICATION,
		CSystemEmailType::SCREENING_TOKEN_EMAILS,
		CSystemEmailType::SCREENING_EMAILS,
		CSystemEmailType::VENDOR_COMMUNICATION,
		CSystemEmailType::INSPECTION_MANAGER,
		CSystemEmailType::RABBITMQ_ALERT_EMAIL
	];

	public static $c_arrintQueueExcludedSystemEmailTypeIds = [
		CSystemEmailType::COMPANY_NEWS_LETTER,
		CSystemEmailType::RENT_REMINDER,
		CSystemEmailType::AUTO_POST_RECURRING_PAYMENTS,
		CSystemEmailType::BOUNCE_EMAIL_NOTIFICATION,
		CSystemEmailType::VANITY_NUMBER_REQUEST,
		CSystemEmailType::METER_EXCEPTION_REPORT,
		CSystemEmailType::VACANCY_CONTACT_SUBMISSIONS,
		CSystemEmailType::EVENT_RESPONSE_NOTIFICATION,
		CSystemEmailType::CLEARING_BATCH_NOTIFICATION,
		CSystemEmailType::INVOICE_STAT_NOTIFICATION,
		CSystemEmailType::TRAINER_APPROVAL_NOTIFICATION,
		CSystemEmailType::FMO_STATEMENT,
		CSystemEmailType::RESIDENT_UTILITY_INVOICE_WITH_USAGE_HISTORY,
		CSystemEmailType::PAYMENT_ACCOUNT_NOTIFICATION
	];

	// System email types that are non-client specific but requires email event tracking
	public static $c_arrintSystemEmailTypeIdsWithCidNullAndEventTrackingEnabled = [
		CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL
	];

	public static $c_arrintSkipNBMSystemEmailTypeIds = [
		CSystemEmailType::PS_EMPLOYEE,
		CSystemEmailType::COMPANY_GUEST_CARD,
		CSystemEmailType::SCHEDULED_PARTIALLY_COMPLETED_APPLICATIONS,
		CSystemEmailType::ENTRATA_PASSWORD,
		CSystemEmailType::BRANCH_COMMIT_NOTIFICATION,
		CSystemEmailType::ILS_NOTIFICATION_EMAIL,
		CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL,
		CSystemEmailType::FEEDBACK,
		CSystemEmailType::TESTIMONIAL,
		CSystemEmailType::AVAILABILITY_ALERT_REMINDER,
		CSystemEmailType::PURCHASE_REQUEST_NOTIFICATION,
		CSystemEmailType::TOOLTIP_REQUEST,
		CSystemEmailType::PSI_CONTACT_SUBMISSIONS,
		CSystemEmailType::SUPPORT_SURVEY_NOTIFICATION,
		CSystemEmailType::COMPANY_PAYMENT_ERROR,
		CSystemEmailType::PS_TO_COMPANY_MARKETING,
		CSystemEmailType::NSF_NOTICE,
		CSystemEmailType::AR_PAYMENT_NOTIFICATION,
		CSystemEmailType::DELETE_SCHEDULED_PAYMENT_NOTIFICATION,
		CSystemEmailType::SCHEDULED_PAYMENT_WARNING,
		CSystemEmailType::CONCIERGE_SERVICE,
		CSystemEmailType::CUSTOMER_PROFILE_UPDATE,
		CSystemEmailType::CUSTOMER_REFERRAL,
		CSystemEmailType::BANK_NOTIFICATION,
		CSystemEmailType::SCHEDULED_PAYMENT_CONFIRMATION,
		CSystemEmailType::MASS_EMAIL,
		CSystemEmailType::ENROLLMENT,
		CSystemEmailType::CHARGE_BACK,
		CSystemEmailType::SALES_SUPPORT_PEEP_EMAIL,
		CSystemEmailType::NEW_SALE,
		CSystemEmailType::PROFILE_UPDATE,
		CSystemEmailType::LEASE_SIGNED,
		CSystemEmailType::PAYMENT_INTEGRATION_FAILURE,
		CSystemEmailType::PROSPECT_EMAILER,
		CSystemEmailType::REPUTATION_ADVISOR,
		CSystemEmailType::QUOTE_EMAIL,
		CSystemEmailType::APPLICATION_CONTACT_SUBMISSIONS,
		CSystemEmailType::CONTACT_SUBMISSIONS,
		CSystemEmailType::MAINTENANCE,
		CSystemEmailType::DELINQUENCY_NOTIFICATIONS,
		CSystemEmailType::DELINQUENCY_CRUCIAL_NOTIFICATIONS,
		CSystemEmailType::VENDOR_ACCESS_EMAIL,
		CSystemEmailType::EVENT_SCHEDULER_EMAIL,
		CSystemEmailType::TASK
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( false == isset( $this->m_strName ) || 0 == strlen( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		if( true == $boolIsValid ) {
			$strSqlCondition = ' WHERE lower( name )= \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $this->m_strName ) ) . '\'';

			$intCount = CSystemEmailTypes::fetchSystemEmailTypeCount( $strSqlCondition, $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name ', 'System Email Type already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailServiceProviderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemEmailPriorityId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase= NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function checkIsEmailEventEnable( $intSystemEmailTypeId, $intEmailEventTypeId ) {

		$arrintDefaultEnableEmailEventIds = [
			CEmailEventType::PROCESSED,
			CEmailEventType::DROPPED,
			CEmailEventType::DEFERRED,
			CEmailEventType::DELIVERED,
			CEmailEventType::SPAM_REPORTS,
			CEmailEventType::OPEN,
			CEmailEventType::BOUNCE,
			CEmailEventType::BLOCKED,
			CEmailEventType::UNSUBSCRIBE
		];

		if( true == valArr( $arrintDefaultEnableEmailEventIds ) && true == in_array( $intEmailEventTypeId, $arrintDefaultEnableEmailEventIds ) ) {
				return true;
		}

		$arrmixSystemEmailTypeEnableEmailEventIds = [
			self::MESSAGE_CENTER_EMAIL => [
				CEmailEventType::PROCESSED,
				CEmailEventType::DROPPED,
				CEmailEventType::DEFERRED,
				CEmailEventType::DELIVERED,
				CEmailEventType::SPAM_REPORTS,
				CEmailEventType::CLICK,
				CEmailEventType::OPEN,
				CEmailEventType::BOUNCE,
				CEmailEventType::BLOCKED,
				CEmailEventType::EXPIRED,
				CEmailEventType::UNSUBSCRIBE
			],
			self::EVENT_SCHEDULER_EMAIL => [
				CEmailEventType::PROCESSED,
				CEmailEventType::DROPPED,
				CEmailEventType::DEFERRED,
				CEmailEventType::DELIVERED,
				CEmailEventType::SPAM_REPORTS,
				CEmailEventType::CLICK,
				CEmailEventType::OPEN,
				CEmailEventType::BOUNCE,
				CEmailEventType::BLOCKED,
				CEmailEventType::EXPIRED,
				CEmailEventType::UNSUBSCRIBE
			],
			self::COMPANY_EMPLOYEE => [
				CEmailEventType::PROCESSED,
				CEmailEventType::DROPPED,
				CEmailEventType::DEFERRED,
				CEmailEventType::DELIVERED,
				CEmailEventType::SPAM_REPORTS,
				CEmailEventType::CLICK,
				CEmailEventType::OPEN,
				CEmailEventType::BOUNCE,
				CEmailEventType::BLOCKED,
				CEmailEventType::EXPIRED,
				CEmailEventType::UNSUBSCRIBE
			]
		];

		if( true == valArr( $arrmixSystemEmailTypeEnableEmailEventIds ) && true == array_key_exists( $intSystemEmailTypeId, $arrmixSystemEmailTypeEnableEmailEventIds )
			&& true == valArr( $arrmixSystemEmailTypeEnableEmailEventIds[$intSystemEmailTypeId] ) && true == in_array( $intEmailEventTypeId, $arrmixSystemEmailTypeEnableEmailEventIds[$intSystemEmailTypeId] ) ) {
				return true;
		}

		return false;
	}

	public function putSystemEmailTypeLastSyncedOn( $intSystemEmailTypeId, $strLastSyncedOn, $objDatabase ) {

		if( true == is_null( $strLastSyncedOn ) ) return false;

		$objSystemEmailType = CSystemEmailTypes::fetchSystemEmailTypeById( $intSystemEmailTypeId, $objDatabase );

		if( false == valObj( $objSystemEmailType, 'CSystemEmailType' ) ) return false;

		$strSql = 'UPDATE system_email_types SET last_synced_on = \'' . $strLastSyncedOn . '\' WHERE id = ' . $objSystemEmailType->getId();

		if( false == fetchData( $strSql, $objDatabase ) )
			return false;

		return true;
	}

	public function update( $intCurrentUserId, $objEmailDatabase, $boolReturnSqlOnly = false ) {

		$boolIsValid = true;

		set_time_limit( 1000 );

		$objConnectDatabase		= CDatabases::createConnectDatabase();
		$arrobjClientDatabases 	= CDatabases::fetchDatabasesByDatabaseUserTypeIdByDatabaseTypeId( CDatabaseType::CLIENT, CDatabaseUserType::PS_PROPERTYMANAGER, $objConnectDatabase );

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) || false == valArr( $arrobjClientDatabases ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to load database object.' ) );
			return false;
		}

		$strSql = parent::update( $intCurrentUserId, $objEmailDatabase, $boolReturnSqlOnly = true );

		if( false === $strSql ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to update record in database.' ) );
			return false;
		}

		$objEmailDatabase->begin();

		if( false == $this->executeSql( $strSql, $this, $objEmailDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to update record in database.' ) );
			$this->$objEmailDatabase->rollback();
			return false;
		}

		if( true == valArr( $arrobjClientDatabases ) ) {

			foreach( $arrobjClientDatabases as $objClientDatabase ) {

				$objClientDatabase->open();

				if( false == valObj( $objClientDatabase, 'CDatabase' ) ) continue;

				$objClientDatabase->begin();

				if( false == $objClientDatabase->executeSql( $strSql, $this, $objClientDatabase ) ) {
					$boolIsValid = false;
				}
			}
		}

		if( false == $boolIsValid ) {
			$objEmailDatabase->rollback();
			foreach( $arrobjClientDatabases as $objClientDatabase ) {
				$objClientDatabase->rollback();
			}
		}

		$objEmailDatabase->commit();

		foreach( $arrobjClientDatabases as $objClientDatabase ) {
			$objClientDatabase->commit();
			$objClientDatabase->close();
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objEmailDatabase, $boolReturnSqlOnly = false ) {

		$boolIsValid = true;

		$objConnectDatabase		= CDatabases::createConnectDatabase();
		$arrobjClientDatabases 	= CDatabases::fetchDatabasesByDatabaseUserTypeIdByDatabaseTypeId( CDatabaseType::CLIENT, CDatabaseUserType::PS_PROPERTYMANAGER, $objConnectDatabase );

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) || false == valArr( $arrobjClientDatabases ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to load database object.' ) );
			return false;
		}

		$objEmailDatabase->begin();

		$strSql = parent::insert( $intCurrentUserId, $objEmailDatabase, $boolReturnSqlOnly = true );

		if( false === $strSql ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert record in database.' ) );
			return false;
		}

		if( false == $this->executeSql( $strSql, $this, $objEmailDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert record in database.' ) );
			$this->$objEmailDatabase->rollback();
			return false;
		}

		if( true == valArr( $arrobjClientDatabases ) ) {

			foreach( $arrobjClientDatabases as $objClientDatabase ) {

				$objClientDatabase->open();

				if( false == valObj( $objClientDatabase, 'CDatabase' ) ) continue;

				$objClientDatabase->begin();

				if( false == $objClientDatabase->executeSql( $strSql, $this, $objClientDatabase ) ) {
					$boolIsValid = false;
				}
			}
		}

		if( false == $boolIsValid ) {
			$objEmailDatabase->rollback();
			foreach( $arrobjClientDatabases as $objClientDatabase ) {
				$objClientDatabase->rollback();
			}
		}

		$objEmailDatabase->commit();

		foreach( $arrobjClientDatabases as $objClientDatabase ) {
			$objClientDatabase->commit();
			$objClientDatabase->close();
		}

		return $boolIsValid;
	}

	public static function checkIsContentStoreOnCloud( $intSystemEmailTypeId = NULL ) {
		// $intSystemEmailTypeId will use in future implementation.
		if( defined( 'CONFIG_CLOUD_REFERENCE_ID' ) && CONFIG_CLOUD_REFERENCE_ID == CCloud::REFERENCE_ID_CHINA ) {
			return false;
		} else {
			return true;
		}
	}

	public static function checkIsSkippingNbm( $intSystemEmailTypeId ) {

		if( empty( $intSystemEmailTypeId ) ) return false;
		if( 'production' != CONFIG_ENVIRONMENT
		    || ( defined( 'CONFIG_CLOUD_REFERENCE_ID' ) && CONFIG_CLOUD_REFERENCE_ID == CCloud::REFERENCE_ID_CHINA )
		    || !in_array( $intSystemEmailTypeId, self::$c_arrintSkipNBMSystemEmailTypeIds ) ) {
			return false;
		} else {
			return true;
		}
	}

}
?>