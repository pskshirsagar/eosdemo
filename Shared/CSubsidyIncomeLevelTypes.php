<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSubsidyIncomeLevelTypes
 * Do not add any new functions to this class.
 */

class CSubsidyIncomeLevelTypes extends CBaseSubsidyIncomeLevelTypes {

	public static function fetchActiveSubsidyIncomeLevelTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM subsidy_income_level_types WHERE hud_code IN(\'1\', \'2\', \'3\') AND is_published = TRUE ORDER BY order_num DESC';
		return self::fetchSubsidyIncomeLevelTypes( $strSql, $objDatabase );
	}

	public static function fetchSubsidyIncomeLevelTypesByIds( $arrintSubsidyIncomeLevelTypeIds, $objDatabase ) {

		$strSql = 'SELECT
						silt.*
					FROM
						subsidy_income_level_types silt
					WHERE
						silt.id IN ( ' . implode( ',', $arrintSubsidyIncomeLevelTypeIds ) . ' )
						AND silt.is_published = TRUE';

		return self::fetchSubsidyIncomeLevelTypes( $strSql, $objDatabase );
	}

}
?>