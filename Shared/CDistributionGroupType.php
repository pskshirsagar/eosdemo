<?php

class CDistributionGroupType extends CBaseDistributionGroupType {

	const ALL_TOGETHER						= 1;
	const BATCH_CREDITS_INDIVIDUAL_DEBITS	= 2;
	const BATCH_CREDITS_BATCH_DEBITS		= 3;
	const BATCH_CREDITS_BY_TYPE_INDIVIDUAL_DEBITS = 4;
}
?>