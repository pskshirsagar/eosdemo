<?php
use Psi\Libraries\UtilConfig\CConfig;

class CInternetListingService extends CBaseInternetListingService {

	use TEosStoredObject;

	const APARTMENTS 					= 1;
	const FORRENT 						= 2;
	const APARTMENTGUIDE 				= 3;
	const MYNEWPLACE 					= 4;
	const APARTMENTFINDER 				= 5;
	const MOVE 							= 6;
	const RENT 							= 7;
	const CRAIGS_LIST					= 8;
	const BESTAPRATMENTSINFTCAMPBELL	= 50;
	const EREI 							= 52;
	const BULLDOGRENT					= 55;
	const APARTMENTMARKETER				= 56;
	const RENTWIKI						= 57;
	const WALLS_4						= 58;
	const RENTALS						= 59;
	const VACANCY						= 60;
	const APARTMENTSEARCH				= 61;
	const RENT_JUNGLE					= 62;
	const APARTMENTSHOWCASE				= 63;
	const HOTPADS 						= 65;
	const FACEBOOK						= 66;
	const ZILLOW 						= 67;
	const MOBILE_PROSPECT_PORTAL		= 68;
	const PROSPECT_PORTAL				= 69;
	const RENTCAFE						= 70;
	const APARTMENTGUIDE_APPLICATION	= 71;
	const GOOGLE_SEM					= 80;
	const ILS_API_SANDBOX				= 87;
	const SITETABLET					= 91;
	const APARTMENTLIST					= 92;
	const CAMPUSCRIBZ					= 99;
	const VERYAPT						= 113;
	const SYNDICATION_ILS				= 115;
	const OFFCAMPUS_PARTNERS			= 123;
	const ILS_API_SANDBOX_PARENT		= 127;
	const ILS_API_SANDBOX_CHILD			= 128;
	const MARKETO						= 135;
	const FACEBOOK_MARKETPLACE			= 147;
	const GOOGLE_MY_BUSINESS			= 182;
	const YEXT_INTEGRATION  			= 183;
	const FACEBOOK_LEAD_ADS				= 184;

	protected $m_boolIsParent;

	public static $c_arrintInternetListingServiceIds = [
		self::APARTMENTS,
		self::FORRENT,
		self::APARTMENTGUIDE,
		self::MYNEWPLACE,
		self::APARTMENTFINDER,
		self::MOVE,
		self::RENT,
		self::WALLS_4,
		self::RENTALS,
		self::RENTWIKI,
		self::APARTMENTSEARCH,
		self::RENT_JUNGLE,
		self::APARTMENTMARKETER,
		self::APARTMENTSHOWCASE,
		self::HOTPADS,
		self::ZILLOW,
		self::RENTCAFE,
		self::APARTMENTGUIDE_APPLICATION,
		self::APARTMENTLIST
	];

	public static $c_arrintNoTrackerEmailIlsIds = [
		self::CRAIGS_LIST
	];

	public static $c_arrintAutomaticGuestCardParsebleIlsIds = [
		self::APARTMENTGUIDE_APPLICATION,
		self::CRAIGS_LIST,
		self::GOOGLE_SEM,
		self::MOBILE_PROSPECT_PORTAL,
		self::PROSPECT_PORTAL,
		self::SITETABLET,
		self::VACANCY
	];

	/**
	 * Get Functions
	 */

	public function getFtpUsernameDecrypted() {
		if( false == valStr( $this->m_strFtpUsernameEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strFtpUsernameEncrypted, CONFIG_SODIUM_KEY_LOGIN_USERNAME, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_USERNAME, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
	}

	public function getFtpPasswordDecrypted() {
		if( false == valStr( $this->m_strFtpPasswordEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strFtpPasswordEncrypted, CONFIG_SODIUM_KEY_LOGIN_PASSWORD, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_PASSWORD, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
	}

	public function getNameFirstDecrypted() {
		if( false == valStr( $this->m_strNameFirstEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strNameFirstEncrypted, CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION, [ 'legacy_secret_key' => CONFIG_KEY_SENSITIVE_DATA_ENCRYPTION, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
	}

	public function getNameLastDecrypted() {
		if( false == valStr( $this->m_strNameLastEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strNameLastEncrypted, CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION, [ 'legacy_secret_key' => CONFIG_KEY_SENSITIVE_DATA_ENCRYPTION, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
	}

	public function getEmailAddressDecrypted() {
		if( false == valStr( $this->m_strEmailAddressEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strEmailAddressEncrypted, CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION, [ 'legacy_secret_key' => CONFIG_KEY_SENSITIVE_DATA_ENCRYPTION, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
	}

	public function getDirectoryNameOrName() {
		return ( true == valStr( $this->getDirectoryName() ) ) ? $this->getDirectoryName() : $this->getName();
	}

	public function getIsParent() {
		return $this->m_boolIsParent;
	}

	/**
	 * Set Functions
	 */

	public function setEncryptFtpUsername( $strFtpUsername ) {
		if( true == valStr( $strFtpUsername ) ) {
			$this->setFtpUsernameEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strFtpUsername, CONFIG_SODIUM_KEY_LOGIN_USERNAME ) );
		}
	}

	public function setEncryptFtpPassword( $strFtpPassword ) {
		if( true == valStr( $strFtpPassword ) ) {
			$this->setFtpPasswordEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strFtpPassword, CONFIG_SODIUM_KEY_LOGIN_PASSWORD ) );
		}
	}

	public function setNameFirstEncryptedValue( $strNameFirst ) {
		if( true == valStr( $strNameFirst ) ) {
			$this->setNameFirstEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strNameFirst, CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION ) );
		}
	}

	public function setNameLastEncryptedValue( $strNameLast ) {
		if( true == valStr( $strNameLast ) ) {
			$this->setNameLastEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strNameLast, CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION ) );
		}
	}

	public function setEmailAddressEncryptedValue( $strEmailAddress ) {
		if( true == valStr( $strEmailAddress ) ) {
			$this->setEmailAddressEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strEmailAddress, CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION ) );
		}
	}

	public function setIsParent( $boolIsParent ) {
		$this->m_boolIsParent = $boolIsParent;
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrstrValues['is_parent'] ) ) $this->setIsParent( $arrstrValues['is_parent'] );
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		if( false == is_null( $this->getOrderNum() ) && false == is_int( $this->getOrderNum() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', 'Order Number must be integer. ' ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;
		$boolIsValidEmailAddress = true;

		if( false == is_null( $this->getEmailAddressDecrypted() ) ) {
			$arrstrEmailAddresses = explode( ',', $this->getEmailAddressDecrypted() );

			$arrstrEmailAddresses = array_filter( $arrstrEmailAddresses );

			if( true == valArr( $arrstrEmailAddresses ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrstrEmailAddresses ) ) {
				foreach( $arrstrEmailAddresses as $strEmailAddress ) {
					if( false == CValidation::validateEmailAddresses( trim( $strEmailAddress ) ) ) {
						$boolIsValidEmailAddress = false;
						break;
					}
				}
			}

			if( false == $boolIsValidEmailAddress ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Enter valid email id(s). ' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDuplicateIlsName( $objAdminDatabase ) {
		$boolIsValid = true;

		if( true == valObj( $objAdminDatabase, 'CDatabase' ) ) {
			$this->setName( addslashes( $this->getName() ) );
			$intDuplicateInternetListingServiceCount = \Psi\Eos\Admin\CInternetListingServices::createService()->fetchDuplicateInternetListingServiceNameCountByInternetListingService( $this, $objAdminDatabase );
			if( 0 < $intDuplicateInternetListingServiceCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name already exists. ' ) );
			}
		}
		return $boolIsValid;
	}

	public function valUrl() {
		$boolIsValid = true;
		$strUrl = $this->getUrl();

		if( true == is_null( $strUrl ) || false == CValidation::checkUrl( $strUrl ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', 'Valid URL is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valIconUri( $objStorageGateway ) : bool {
		$boolIsValid = true;

		if( false == empty( $_FILES['icon_uri']['name'] ) ) {

			if( ( $_FILES['icon_uri']['type'] == 'image/gif' ) || ( $_FILES['icon_uri']['type'] == 'image/jpeg' ) || ( $_FILES['icon_uri']['type'] == 'image/png' ) ) {

				$arrmixRequest = $this->createPutGatewayRequest( [ 'data' => CFileIo::fileGetContents( $_FILES['icon_uri']['tmp_name'] ), 'noEncrypt' => true, 'storageGateway' => CONFIG_STORAGE_GATEWAY_CDN ], 'icon' );
				$mixResponse   = $objStorageGateway->putObject( $arrmixRequest );

				if( true == $mixResponse->hasErrors() ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, $_FILES['icon_uri']['tmp_name'] . ' could not be moved to the temporary directory.' ) );

					return false;
				}

				$this->setDetailsField( 'icon', $mixResponse );
				$this->setIconUri( $arrmixRequest['key'] );

			} else {
				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, 'Icon image must be gif,jpeg or png only. ' ) );
			}
		} elseif( false == valStr( $this->getIconUri() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, 'Please select icon. ' ) );
		}

		return $boolIsValid;
	}

	public function valLogoUri( $objStorageGateway ) : bool {
		$boolIsValid = true;

		if( false == empty( $_FILES['logo_uri']['name'] ) ) {

			if( ( $_FILES['logo_uri']['type'] == 'image/gif' ) || ( $_FILES['logo_uri']['type'] == 'image/jpeg' ) || ( $_FILES['logo_uri']['type'] == 'image/png' ) ) {
				$arrmixRequest = $this->createPutGatewayRequest( [ 'data' => CFileIo::fileGetContents( $_FILES['logo_uri']['tmp_name'] ), 'noEncrypt' => true, 'storageGateway' => CONFIG_STORAGE_GATEWAY_CDN ], 'logo' );
				$mixResponse   = $objStorageGateway->putObject( $arrmixRequest );

				if( true == $mixResponse->hasErrors() ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, $_FILES['logo_uri']['tmp_name'] . ' could not be moved to the temporary directory.' ) );

					return false;
				}

				$this->setDetailsField( 'logo', $mixResponse );
				$this->setLogoUri( $arrmixRequest['key'] );
			} else {
				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, 'Logo image must be gif,jpeg or png only. ' ) );
			}
		} elseif( false == valStr( $this->getLogoUri() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, 'Please select Logo. ' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase = NULL, $objStorageGateway = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				if( true == $boolIsValid ) {
					$boolIsValid &= $this->valDuplicateIlsName( $objAdminDatabase );
				}
				$boolIsValid &= $this->valUrl();
				if( true == $this->getIsIlsApi() ) {
					$boolIsValid &= $this->valIconUri( $objStorageGateway );
					$boolIsValid &= $this->valLogoUri( $objStorageGateway );
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Update Functions
	 *
	 */

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->setDescription( addslashes( $this->getDescription() ) );
		$this->setEncryptFtpUsername( addslashes( $this->getFtpUsernameEncrypted() ) );
		$this->setEncryptFtpPassword( addslashes( $this->getFtpPasswordEncrypted() ) );
		$this->setCheckAvailabilityPageTemplate( addslashes( $this->getCheckAvailabilityPageTemplate() ) );

		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( $boolReturnSqlOnly == true ) {
			return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		$this->setDescription( addslashes( $this->getDescription() ) );
		$this->setEncryptFtpUsername( addslashes( $this->getFtpUsernameEncrypted() ) );
		$this->setEncryptFtpPassword( addslashes( $this->getFtpPasswordEncrypted() ) );
		$this->setCheckAvailabilityPageTemplate( addslashes( $this->getCheckAvailabilityPageTemplate() ) );

		if( true == parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			// Rebuilding cache for updated internet listing service object.
			return \Psi\Eos\Admin\CInternetListingServices::createService()->rebuildCachedInternetListingServiceById( $this );
		}

		return false;
	}

	/**
	 * Other Functions
	 *
	 */

	// NOTE: As per discussion with Vijay, decided to keep this function as is. We can not move this function into custom plural due to huge complexity hence this is an exception.

	public static function internetListingServiceIdToStr( $intInternetListingServiceId ) {
		switch( $intInternetListingServiceId ) {
			case self::APARTMENTS:
				return 'apartments';

			case self::FORRENT:
				return 'forrent';

			case self::APARTMENTGUIDE:
				return 'apartmentguide';

			case self::MYNEWPLACE:
				return 'mynewplace';

			case self::APARTMENTFINDER:
				return 'apartmentfinder';

			case self::MOVE:
				return 'move';

			case self::RENT:
				return 'rent';

			case self::WALLS_4:
				return '4walls';

			case self::RENTALS:
				return 'rentals';

			case self::RENTWIKI:
				return 'rentwiki';

			case self::APARTMENTSEARCH:
				return 'apartmentsearch';

			case self::RENT_JUNGLE:
				return 'rentjungle';

			case self::APARTMENTMARKETER:
				return 'rentbits';

			case self::APARTMENTSHOWCASE:
				return 'apartmentshowcase';

			case self::HOTPADS:
				return 'hotpads';

			case self::ZILLOW:
				return 'zillow';

			case self::RENTCAFE:
				return 'rentcafe';

			case self::APARTMENTGUIDE_APPLICATION:
				return 'apartmentguide_application';

			default:
				return 'common_ils';
				break;
		}
	}

	protected function calcStorageContainer( $strVendor = NULL ) : string {
		switch( $strVendor ) {
			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				return CMarketingHubLibrary::MEDIA_LIBRARY_PATH;
			default:
			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3:
				return CONFIG_OSG_BUCKET_MEDIA_LIBRARY;

		}
	}

	protected function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) : string {
		switch( $strReferenceTag ) {
			case 'logo':
				return CMarketingHubLibrary::ILS_LOGO_PATH . basename( $_FILES['logo_uri']['name'] );

			case 'icon':
			default:
				return CMarketingHubLibrary::ILS_ICON_PATH . basename( $_FILES['icon_uri']['name'] );
		}
	}

	public function getLogoUri() {
		return $this->getCdnUrl( $this->m_strLogoUri );
	}

	public function getIconUri() {
		return $this->getCdnUrl( $this->m_strIconUri );
	}

	protected function getCdnUrl( $strUrl ) {
		$strCdnUrl = '';
		if( true == valStr( $strUrl ) ) {
			return ( false === strpos( $strUrl, 'media_library' ) ) ? CConfig::createService()->get( 'is_secure_url' ) ? 'https://' : 'http://' . CConfig::createService()->get( 'osg_distribution_media_library' ) . '/'. $strUrl : CONFIG_MEDIA_LIBRARY_PATH . $strUrl;
		}

		return $strCdnUrl;
	}

}
?>