<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReturnTypes
 * Do not add any new functions to this class.
 */

class CReturnTypes extends CBaseReturnTypes {

	public static function fetchReturnTypeByCode( $strCode, $objDatabase ) {
		if( true == is_null( $strCode ) ) return NULL;
		$strSql = 'SELECT * FROM ' . 'return_types WHERE code = \'' . addslashes( trim( $strCode ) ) . '\'';
		return self::fetchReturnType( $strSql, $objDatabase );
	}

	public static function fetchAllReturnTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM return_types ORDER BY order_num';
		return self::fetchReturnTypes( $strSql, $objDatabase );
	}

	public static function fetchChargeBackReturnTypes( $objDatabase ) {

		$arrintChargeBackReturnTypeIds = [
			CReturnType::OTHER,
			CReturnType::CHARGE_BACK_ADJUSTMENT,
			CReturnType::FRAUD_NO_CARDHOLDER_AUTHORIZATION_OR_TRANSACTION_NOT_RECOGNIZED,
			CReturnType::NON_RECEIPT_OF_MERCHANDISE_SERVICES_NOT_RENDERED,
			CReturnType::DUPLICATE_PROCESSING,
			CReturnType::CANCELLED_RECURNING_TRANSACTION,
			CReturnType::CREDIT_NOT_PROCESSED,
			CReturnType::AVS_OR_CID_NOT_OBTAINED
		];

		$strSql = 'SELECT
						*,
						CASE
							WHEN id = 177
							THEN 0
							ELSE 1
						END AS top_order
					FROM
						return_types
					WHERE
						id IN ( ' . implode( ',', $arrintChargeBackReturnTypeIds ) . ' )
					ORDER BY top_order desc, order_num';

		return parent::fetchReturnTypes( $strSql, $objDatabase );
	}

	public static function fetchAchReturnTypes( $objDatabase ) {

		$strSql = 'SELECT
						*,
						CASE
							WHEN id = 99
							THEN 0
							ELSE 1
						END AS top_order
					FROM
						return_types
					WHERE
						id < 100
					ORDER BY top_order, order_num';

		return parent::fetchReturnTypes( $strSql, $objDatabase );
	}

	public static function fetchCheck21ReturnTypes( $objDatabase ) {

		$arrintCheck21ReturnTypeIds	= [
			CReturnType::MANUAL, CReturnType::REFER_TO_MAKER, CReturnType::BAD_CHECK21_IMAGE, CReturnType::CHECK21_DUPLICATE_PRESENTMENT, CReturnType::CHECK21_MICR_AND_ITEM_DO_NOT_MATCH, CReturnType::CHECK21_INSUFFICIENT_FUNDS,
			CReturnType::CHECK21_UNCOLLECTED_FUNDS_HOLD, CReturnType::CHECK21_STOP_PAYMENT, CReturnType::CHECK21_CLOSED_ACCOUNT, CReturnType::CHECK21_UNABLE_TO_LOCATE_ACCOUNT, CReturnType::CHECK21_UNABLE_TO_FROZEN_BLOCKED_ACCOUNT, CReturnType::CHECK21_STALE_DATED,
			CReturnType::CHECK21_POST_DATED, CReturnType::CHECK21_ENDORSEMENT_MISSING, CReturnType::CHECK21_ENDORSEMENT_IRREGULAR, CReturnType::CHECK21_SIGNATURE_MISSING, CReturnType::CHECK21_SIGNATURE_IRREGULAR, CReturnType::CHECK21_NON_CASH_ITEM,
			CReturnType::CHECK21_ALERTED_FICTICIOUS_ITEM, CReturnType::CHECK21_UNABLE_TO_PROCESS, CReturnType::CHECK21_ITEM_EXCEEDED_DOLLER_LIMIT, CReturnType::CHECK21_NOT_AUTHORIZED, CReturnType::CHECK21_BRANCH_ACCOUNT_SOLD,
			CReturnType::CHECK21_STOP_PAYMENT_SUSPECT, CReturnType::CHECK21_UNUSABLE_IMAGE, CReturnType::CHECK21_IMAGE_FAILS_SECURITY_CHECK, CReturnType::CHECK21_CANNOT_DETERMINE_ACCOUNT, CReturnType::CHECK21_DUPLICATE_PRESENTMENT_ADJUSTMENT,
			CReturnType::CHECK21_NON_CASH_ITEM_ADJUSTMENT, CReturnType::CHECK21_BAD_IMAGE_ADJUSTMENT, CReturnType::CHECK21_FOREIGN_ITEM, CReturnType::CHECK21_FOREIGN_ITEM_ADJUSTMENT
		];

		$strSql = 'SELECT
						*,
						CASE
							WHEN id = 99
							THEN 0
							ELSE 1
						END AS top_order
					FROM return_types
					WHERE
						id IN ( ' . implode( ',', $arrintCheck21ReturnTypeIds ) . ' )
					ORDER BY top_order, order_num';

		return parent::fetchReturnTypes( $strSql, $objDatabase );
	}

	public static function fetchAssignedReturnTypesByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						rt.*
					FROM
						return_types as rt
					WHERE
						rt.id IN ( SELECT return_type_id FROM return_type_options rto WHERE rto.cid = ' . ( int ) $intCid . ' )
						AND rt.is_published = 1';

		return parent::fetchReturnTypes( $strSql, $objDatabase );
	}

	public static function fetchReturnTypesByIds( $arrintReturnTypeIds, $objDatabase ) {

		if( false == valArr( $arrintReturnTypeIds ) ) {
			return [];
		}

		$strSql = 'SELECT
						rt.*
					FROM
						return_types as rt
					WHERE
						rt.id IN ( ' . implode( ',', $arrintReturnTypeIds ) . ')
						AND rt.is_published = 1';

		return parent::fetchReturnTypes( $strSql, $objDatabase );
	}

	public static function fetchPadReturnTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM return_types WHERE id in(' . implode( ',', CReturnType::$c_arrintPadReturnTypes ) . ')';

		return parent::fetchReturnTypes( $strSql, $objDatabase );
	}

}
?>