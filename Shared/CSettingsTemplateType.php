<?php

class CSettingsTemplateType extends CBaseSettingsTemplateType {

	protected $m_strUpdatedByUserName;

	public static $c_intCurrentUserId;

	const LEASING_LEAD_MANAGEMENT    = 1;
	const LEASING_APPLICATION        = 2;
	const LEASING_PRICING            = 3;
	const LEASING_RESIDENT           = 4;
	const LEASING_UNIT_AVAILABILITY  = 5;
	const LEASING_LEASING_CENTER     = 6;
	const LEASING_GOALS              = 7;
	const FINANCIAL_GENERAL          = 8;
	const MARKETING_GENERAL          = 9;
	const MARKETING_MOBILE           = 10;
	const MARKETING_SOCIAL           = 11;
	const MARKETING_CORPORATE_SEARCH = 12;
	const MARKETING_LIMITED_DISPLAY  = 13;
	const MARKETING_ILS_PORTAL       = 14;
	const MARKETING_CRAIGSLIST       = 15;
	const LEASING_SITE_TABLET        = 16;
	const RESIDENT_PORTAL_GENERAL    = 17;
	const RESIDENT_PORTAL_COMMUNITY  = 18;

	const SETTING_TEMPLATE_TYPE_COMMUNICATION_CALL_ANALYSIS = 20;

	const RESIDENTS_OFFERS                        = 22;
	const RESIDENT_PORTAL_PAYMENTS                = 23;
	const DETAILS_FPU_AVAILABILITY                = 24;
	const COMMUNICATION_SYSTEM_MESSAGES_MARKETING = 25;
	const COMMUNICATION_SYSTEM_MESSAGES_RESIDENT  = 26;
	const COMMUNICATION_NOTIFICATION_RECIPIENTS   = 27;
	const COMMUNICATION_MISC                      = 28;

	const RESIDENT_MAINTENANCE_GENERAL              = 30;
	const RESIDENT_PARCEL_ALERT                     = 31;
	const RESIDENTS_RESIDENT_PORTAL_MAINTENANCE     = 32;
	const COMMUNICATION_SYSTEM_MESSAGES_MAINTENANCE = 33;
	const FINANCIAL_PAYMENTS_NSF                    = 34;
	const COMMUNICATION_SYSTEM_MESSAGES_PAYMENTS    = 35;
	const INTEGRATIONS_RESIDENTS_GENERAL            = 36;
	const INTEGRATIONS_RESIDENTS_MAINTENANCE        = 37;
	const INTEGRATIONS_GENERAL                      = 39;
	const INTEGRATIONS_FLOORPLANS_UNITS             = 40;
	const INTEGRATIONS_LEADS                        = 41;
	const INTEGRATIONS_APPLICATION                  = 42;
	const INTEGRATIONS_LEASE                        = 43;
	const INTEGRATIONS_UNIT_AVAILABILITY            = 44;
	const INTEGRATIONS_REVENUE_MANAGEMENT           = 45;

	const INTEGRATIONS_PAYMENTS_GENERAL         = 47;
	const INTEGRATIONS_FINANCIAL                = 48;
	const RESIDENT_INSURANCE                    = 49;
	const LEASING_ONLINE_LEASE_SIGNING          = 50;
	const INTEGRATIONS_MIGRATIONS               = 51;
	const INTEGRATIONS_VENDOR_FINANCIAL         = 52;
	const INTEGRATIONS_CHARGE_CODE_FINANCIAL    = 53;
	const COMMUNICATION_QUICK_RESPONSES         = 54;
	const COMMUNICATION_SYSTEM_MESSAGES_LEASING = 55;

	const DATA_MANAGEMENT_VENDORS        = 57;
	const RESIDENT_MOVE_IN               = 58;
	const RESIDENTS_MOVE_OUT_MOVE_OUT    = 59;
	const RESIDENT_MOVEOUT_FMOSTATEMENT  = 60;
	const RESIDENTS_MOVE_OUT_COLLECTIONS = 61;

	const FINANCIAL_NOTICES                 = 63;
	const PRICING_LATE_FEES                 = 65;
	const LEASING_SCREENING                 = 66;
	const DATA_MANAGEMENT_LEASING_SCREENING = 67;

	const COMMUNICATION_SYSTEM_MESSAGES_MISC      = 72;
	const RESIDENT_REPUTATION_GENERAL             = 73;
	const RESIDENT_REPUTATION_ATTRIBUTE           = 74;
	const FINANCIAL_CLOSINGS                      = 75;
	const GROSS_POTENTIAL_RENT                    = 76;
	const RESIDENT_MAINTENANCE_EXPENSES           = 79;
	const FINANCIAL_CHARGES_GENERAL               = 81;
	const RESIDENT_MAINTENANCE_PRIORITIES         = 82;
	const RESIDENT_MAINTENANCE_STATUSES           = 83;
	const RESIDENT_MAINTENANCE_LOCATIONS_PROBLEMS = 84;
	const RESIDENT_MAINTENANCE_AVALABILITY        = 85;
	const TEMPLATE                                = 38;

	const RESIDENT_CONTACT_POINTS                  = 88;
	const COMMUNICATION_CONTACT_POINTS_MAINTENANCE = 87;
	const RENEWALS_LEASE_MODIFICATIONS             = 89;
	const APPLICANT_CONTACT_POINTS                 = 90;
	const RESIDENT_MAINTENANCE_NOTIFICATIONS       = 91;
	const RESIDENT_RP_RESIDENT_OPTIONS             = 92;
	const RESIDENT_RP_ENROLLMENT_LOGIN             = 93;
	const FINANCIAL_PAYMENTS_ACCOUNT_VERIFICATION  = 94;

	const MARKETING_AMENITIES           = 96;
	const MARKETING_CONTACT_METHODS     = 97;
	const MARKETING_FLOOR_PLANS         = 98;
	const MARKETING_MAPS_AND_DIRECTIONS = 99;
	const MARKETING_MEDIA               = 100;
	const MARKETING_RATINGS_AND_REVIEWS = 101;
	const MARKETING_PRIVACY             = 103;
	const MARKETING_FAQS                = 104;

	/**
	 * Get Functions
	 */

	public function getUpdatedByUserName() {
		return $this->m_strUpdatedByUserName;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['updated_by_user_name'] ) ) {
			$this->setUpdatedByUserName( $arrstrValues['updated_by_user_name'] );
		}

		return;
	}

	public function setUpdatedByUserName( $strUpdatedByUserName ) {
		$this->m_strUpdatedByUserName = $strUpdatedByUserName;
	}

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valModuleId( $strAction, $objDatabase ) {
		$boolIsValid = true;
		if( true == is_null( $this->getModuleId() ) || 0 == $this->getModuleId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'module_id', 'Module is required.' ) );
		} else {
			$arrobjSettingsTemplateTypes = \Psi\Eos\Admin\CSettingsTemplateTypes::createService()->fetchSettingsTemplateTypesByModuleId( $this->getModuleId(), $objDatabase );

			if( true == valArr( $arrobjSettingsTemplateTypes ) ) {
				if( VALIDATE_INSERT == $strAction ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Settings Template Type with this module already exists.' ) );
					$boolIsValid = false;
				}

				if( VALIDATE_UPDATE == $strAction ) {
					foreach( $arrobjSettingsTemplateTypes as $objSettingsTemplateTypes ) {
						if( $this->getModuleId() != $objSettingsTemplateTypes->getModuleId() ) {
							$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Settings Template Type with this module already exists.' ) );
							$boolIsValid = false;
							break;
						}
					}

				}
			}

			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Settings Template Type name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valRequiresSync() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				// $boolIsValid &= $this->valModuleId( $strAction, $objDatabase );
				$boolIsValid &= $this->valName();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public static function validateCustomClientDatabases( $intCountAdminSettingsTemplateTypes, $arrobjClientDatabases ) {

		$boolIsAdminEntrataSimilar = true;

		$strClientDatabaseNames = '';
		foreach( $arrobjClientDatabases as $objClientDatabase ) {

			$objClientDatabase->open();

			$intCountEntrataSettingsTemplateTypes = ( int ) \Psi\Eos\Entrata\CSettingsTemplateTypes::createService()->fetchSettingsTemplateTypeCount( NULL, $objClientDatabase );

			if( $intCountAdminSettingsTemplateTypes < $intCountEntrataSettingsTemplateTypes ) {
				$boolIsAdminEntrataSimilar &= false;
				$strClientDatabaseNames    .= $objClientDatabase->getDatabaseName() . ' ( ' . $intCountEntrataSettingsTemplateTypes . ' ), ';
			}

		}

		if( true == valStr( $strClientDatabaseNames ) ) {
			$strClientDatabaseNames = trim( $strClientDatabaseNames, ', ' );
		}

		return [ $boolIsAdminEntrataSimilar, $strClientDatabaseNames ];
	}

}

?>