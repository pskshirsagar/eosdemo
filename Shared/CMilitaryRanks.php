<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CMilitaryRanks
 * Do not add any new functions to this class.
 */

class CMilitaryRanks extends CBaseMilitaryRanks {

	public static function fetchAllPaginatedMilitaryRanksByMilitaryComponentIdsByMilitaryPayGradeIds( $intPageNo, $intPageSize, $arrintMilitaryComponentIds, $arrintMilitaryPayGradeIds, $objDatabase ) {
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;

		$strSql = 'SELECT
						mr.*,
						mc.name as military_component_name,
						mpg.name as military_pay_grade_name
					FROM
						military_ranks mr
					JOIN military_components mc ON ( mr.military_component_id = mc.id and mc.is_published = TRUE)
					JOIN military_pay_grade_ranks mpgr ON ( mr.id = mpgr.military_rank_id )
					JOIN military_pay_grades mpg ON ( mpgr.military_pay_grade_id = mpg.id and mpg.is_published = TRUE)
					WHERE
						mc.is_published = true';

		if( valArr( $arrintMilitaryComponentIds ) ) {
			$strSql .= ' AND mc.id IN( ' . implode( ',', $arrintMilitaryComponentIds ) . ') ';
		}

		if( valArr( $arrintMilitaryPayGradeIds ) ) {
			$strSql .= ' AND mpg.id IN( ' . implode( ',', $arrintMilitaryPayGradeIds ) . ') ';
		}

		$strSql .= ' order by mr.military_component_id OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;

		return self::fetchMilitaryRanks( $strSql, $objDatabase );
	}

	public static function fetchPaginatedMilitaryRanksCountByMilitaryComponentIdsByMilitaryPayGradeIds( $arrintMilitaryComponentIds, $arrintMilitaryPayGradeIds, $objDatabase ) {

		$strSql = ' WHERE is_published = true';

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrintMilitaryComponentIds ) ) {
			$strSql .= ' AND military_component_id IN ( ' . implode( ', ', $arrintMilitaryComponentIds ) . ' ) ';
		}

		return self::fetchMilitaryRankCount( $strSql, $objDatabase );
	}

	public static function fetchPublishedMilitaryRanks( $objDatabase ) {

		$strSql = 'SELECT * FROM military_ranks WHERE is_published=true order by order_num';
		return self::fetchMilitaryRanks( $strSql, $objDatabase );
	}

	public static function fetchPublishedMilitaryRanksByMilitaryComponentIdByMilitaryPayGradeId( $intMilitaryComponentId, $intMilitaryPayGradeId, $objDatabase ) {

		$strSql = 'SELECT
					*
					FROM
						military_ranks mr
						JOIN military_pay_grade_ranks mpr ON ( mr.id = mpr.military_rank_id )
					WHERE
						is_published=true
						AND mr.military_component_id = ' . ( int ) $intMilitaryComponentId . '
						AND mpr.military_pay_grade_id = ' . ( int ) $intMilitaryPayGradeId . '
					ORDER BY
						order_num';
		return self::fetchMilitaryRanks( $strSql, $objDatabase );
	}

	public static function fetchPublishedMilitaryRanksWithBranchAndPayGrade( $boolOrderByNum, $objDatabase ) {

		$strOrderBy = ( true == $boolOrderByNum ) ?  ' ORDER BY order_num' : ' ORDER BY name';
		$strSql = 'SELECT
					mr.*, mpr.military_pay_grade_id
					FROM
						military_ranks as mr
						JOIN military_pay_grade_ranks as mpr ON ( mr.id = mpr.military_rank_id )
					WHERE
						mr.is_published=true' . $strOrderBy;

		return self::fetchMilitaryRanks( $strSql, $objDatabase );
	}

	public static function fetchPublishedMilitaryRanksByMilitaryComponentId( $intMilitaryComponentId, $objDatabase, $boolOrderByOrderNum = false ) {

		$strSql = 'SELECT
						DISTINCT ON ( mr.name )
						mr.*
					FROM
						military_ranks mr
						JOIN military_pay_grade_ranks mpr ON ( mr.id = mpr.military_rank_id )
					WHERE
						mr.is_published=true
						AND mr.military_component_id = ' . ( int ) $intMilitaryComponentId . '
					ORDER BY
						mr.name';

		if( true == $boolOrderByOrderNum ) {
			$strSql = 'SELECT 
							sub.*
						FROM 
							( ' . $strSql . ' ) AS sub 
						ORDER BY 
							sub.order_num';
		}

		return self::fetchMilitaryRanks( $strSql, $objDatabase );
	}

}
?>