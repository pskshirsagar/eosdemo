<?php

class CApPhysicalStatusType extends CBaseApPhysicalStatusType {

	const NOT_ORDERED			= 1;
	const BACK_ORDERED			= 2;
	const ORDERED				= 3;
	const IN_PROGRESS			= 4;
	const FULFILLED				= 5;
	const PARTIALLY_RECEIVED	= 6;
	const RECEIVED				= 7;
	const RETURNED				= 8;
	const CANCELLED				= 9;
	const ON_HOLD				= 10;

	const PO_IS_NEW             = 'new';
	const PO_IS_OPEN            = 'open';
	const PO_CLOSED             = 'closed';
	const PO_PARTIALLY_INVOICED = 'partially_invoiced';

	public static $c_arrstrManualApPhysicalStatusTypes = [
		self::BACK_ORDERED	=> 'Backordered',
		self::ORDERED		=> 'Ordered',
		self::ON_HOLD		=> 'On Hold',
		self::CANCELLED		=> 'Cancelled'
	];

	public static $c_arrstrAutoApPhysicalStatusTypes = [
		self::PARTIALLY_RECEIVED	=> 'Partially Received',
		self::RECEIVED				=> 'Received',
		self::RETURNED				=> 'Returned'
	];

	public static $c_arrintOpenPOApPhysicalStatusTypes = [
		self::ORDERED,
		self::NOT_ORDERED
	];

	public static $c_arrstrApPhysicalStatusTypes = [
		self::BACK_ORDERED			=> 'Back-ordered',
		self::ORDERED				=> 'Ordered',
		self::ON_HOLD				=> 'On Hold',
		self::IN_PROGRESS			=> 'In Progress',
		self::FULFILLED				=> 'Fulfilled',
		self::PARTIALLY_RECEIVED	=> 'Partially Received',
		self::RECEIVED				=> 'Received',
		self::RETURNED				=> 'Returned',
		self::NOT_ORDERED			=> 'Not Ordered',
		self::CANCELLED				=> 'Cancelled'
	];

	public static $c_arrstrOrderHeaderPhysicalStatusTypes = [
		self::IN_PROGRESS			=> 'In Progress',
		self::FULFILLED				=> 'Fulfilled',
		self::CANCELLED				=> 'Cancelled',
		self::PO_IS_NEW				=> 'New'
	];

	public static $c_arrstrOrderHeaderFilterPhysicalStatusTypes = [
		[ 'id' => self::PO_IS_NEW,      'status_name' => 'New' ],
		[ 'id' => self::PO_IS_OPEN,     'status_name' => 'Open' ],
		[ 'id' => self::IN_PROGRESS,    'status_name' => 'In Progress' ],
		[ 'id' => self::FULFILLED,      'status_name' => 'Fulfilled' ],
		[ 'id' => self::ON_HOLD,        'status_name' => 'On Hold' ],
		[ 'id' => self::PO_PARTIALLY_INVOICED, 'status_name' => 'Partially Invoiced' ],
		[ 'id' => self::PO_CLOSED, 'status_name' => 'Closed' ],
		[ 'id' => self::CANCELLED,      'status_name' => 'Cancelled' ]
	];

	public static $c_arrstrOrderHeaderPhysicalStatusTypesBulkActions = [
		'export_order_headers'  => 'Export Selected',
		self::PO_IS_NEW			=> 'Mark as New',
		self::IN_PROGRESS		=> 'Mark as In Progress',
		self::FULFILLED			=> 'Mark as Fulfilled',
		self::CANCELLED			=> 'Deny/Cancel',
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function assignSmartyConstants( $objSmarty ) {
		$objSmarty->assign( 'NOT_ORDERED',			self::NOT_ORDERED );
		$objSmarty->assign( 'BACK_ORDERED',			self::BACK_ORDERED );
		$objSmarty->assign( 'ORDERED',				self::ORDERED );
		$objSmarty->assign( 'IN_PROGRESS',			self::IN_PROGRESS );
		$objSmarty->assign( 'FULFILLED',			self::FULFILLED );
		$objSmarty->assign( 'PARTIALLY_RECEIVED',	self::PARTIALLY_RECEIVED );
		$objSmarty->assign( 'RECEIVED',				self::RECEIVED );
		$objSmarty->assign( 'RETURNED',				self::RETURNED );
		$objSmarty->assign( 'CANCELLED',			self::CANCELLED );
		$objSmarty->assign( 'ON_HOLD',				self::ON_HOLD );
	}

}
?>