<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Screening\CScreeningDecisionTypes
 * Do not add any new functions to this class.
 */

class CScreeningDecisionTypes extends CBaseScreeningDecisionTypes {

    public static function fetchScreeningDecisionTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CScreeningDecisionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchScreeningDecisionType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CScreeningDecisionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

}
?>