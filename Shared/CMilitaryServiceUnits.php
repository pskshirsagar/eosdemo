<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CMilitaryServiceUnits
 * Do not add any new functions to this class.
 */

class CMilitaryServiceUnits extends CBaseMilitaryServiceUnits {

	public static function fetchAllMilitaryServiceUnits( $objDatabase ) {
		return self::fetchMilitaryServiceUnits( 'SELECT * FROM  military_service_units', $objDatabase );
	}

	public static function fetchMilitaryServiceUnitsByCode( $strCode, $objDatabase ) {
		$strSql		= 'SELECT
							msu.*
							
						FROM
							military_service_units msu WHERE msu.code = \'' . $strCode . '\'';

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) {
			return true;
		}
		return false;

	}

	public static function fetchMilitaryServiceUnitsByIdentifier( $strIdentifier, $objDatabase ) {
		$strSql		= 'SELECT
							msu.*
							
						FROM
							military_service_units msu WHERE msu.identifier = \'' . $strIdentifier . '\'';

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) {
			return true;
		}
		return false;
	}

	public static function fetchMilitaryServiceUnitsByCodeById( $intId, $strCode, $objDatabase ) {
		$strSql		= 'SELECT
							msu.*
							
						FROM
							military_service_units msu WHERE msu.code = \'' . $strCode . '\' AND id != ' . ( int ) $intId;

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) {
			return true;
		}
		return false;
	}

	public static function fetchMilitaryServiceUnitsByIdentifierById( $intId, $strIdentifier, $objDatabase ) {
		$strSql		= 'SELECT
							msu.*
							
						FROM
							military_service_units msu WHERE msu.identifier = \'' . $strIdentifier . '\' AND id != ' . ( int ) $intId;

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) {
			return true;
		}
		return false;
	}

	public static function fetchMilitaryServiceUnitsByPropertyId( $intPropertyId, $intCid, $objDatabase, $strSearchKey = '', $intCustomerId = NULL ) {

		if( false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
	                msu.code,
	                msu.identifier,
	                mi.name
		          FROM 
		            military_service_units msu
		            LEFT JOIN military_installations mi ON (mi.id = msu.military_installation_id)';

		if( false == valStr( $strSearchKey ) ) {
			$strSql .= ' LEFT JOIN property_details pd ON ( msu.military_installation_id = pd.military_installation_id AND pd.property_id = ' . ( int ) $intPropertyId . ' AND pd.cid = ' . ( int ) $intCid . ' ) 
						LEFT JOIN customer_military_details cmd ON ( msu.code = cmd.unit_id_code AND cmd.customer_id = ' . ( int ) $intCustomerId . ' AND cmd.cid = ' . ( int ) $intCid . ' ) 
						WHERE ( pd.id IS NOT NULL OR cmd.id IS NOT NULL ) 
						ORDER BY msu.code';
		} else {
			$strSql .= ' LEFT JOIN property_details pd ON ( msu.military_installation_id = pd.military_installation_id AND pd.military_installation_id IS NOT NULL AND pd.cid = ' . ( int ) $intCid . ' AND pd.property_id = ' . ( int ) $intPropertyId . ' ) 
						WHERE pd.id IS NULL 
							AND ( LOWER( msu.code ) LIKE LOWER( \'%' . $strSearchKey . '%\' ) 
								OR LOWER( msu.identifier ) LIKE LOWER( \'%' . $strSearchKey . '%\' ) 
								OR LOWER( mi.name ) LIKE LOWER( \'%' . $strSearchKey . '%\' ) ) 
						ORDER BY msu.code';
		}

		return fetchData( $strSql, $objDatabase );
	}

}
?>