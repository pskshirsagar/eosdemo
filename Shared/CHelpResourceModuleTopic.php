<?php

class CHelpResourceModuleTopic extends CBaseHelpResourceModuleTopic {

	protected $m_intModuleId;
	protected $m_intHelpResourceTypeId;
	protected $m_strHelpResourceTitle;
	protected $m_strHelpResourceType;

    public function getModuleId() {
    	return $this->m_intModuleId;
    }

    public function getHelpResourceTitle() {
    	return $this->m_strHelpResourceTitle;
    }

    public function getHelpResourceTypeId() {
    	return $this->m_intHelpResourceTypeId;
    }

    public function getHelpResourceType() {
    	return $this->m_strHelpResourceType;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['help_resource_type_id'] ) ) $this->setHelpResourceTypeId( $arrmixValues['help_resource_type_id'] );
    	if( true == isset( $arrmixValues['module_id'] ) ) $this->setModuleId( $arrmixValues['module_id'] );
    	if( true == isset( $arrmixValues['help_resource_type'] ) ) $this->setHelpResourceType( $arrmixValues['help_resource_type'] );
    	if( true == isset( $arrmixValues['help_resource_title'] ) ) $this->setHelpResourceTitle( $arrmixValues['help_resource_title'] );
    }

    public function setModuleId( $intModuleId ) {
    	$this->m_intModuleId = $intModuleId;
    }

    public function setHelpResourceTitle( $strHelpResourceTitle ) {
    	$this->m_strHelpResourceTitle = CStrings::strTrimDef( $strHelpResourceTitle, -1, NULL, true );
    }

    public function setHelpResourceTypeId( $intHelpResourceTypeId ) {
    	$this->m_intHelpResourceTypeId = $intHelpResourceTypeId;
    }

    public function setHelpResourceType( $strHelpResourceType ) {
    	$this->m_strHelpResourceType = $strHelpResourceType;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valHelpResourceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valHelpResourceModuleId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valHelpTopicId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

    	$this->setDeletedBy( $intCurrentUserId );
    	$this->setDeletedOn( 'NOW()' );
    	$this->setUpdatedBy( $intCurrentUserId );
    	$this->setUpdatedOn( 'NOW()' );
    	$this->setIsRequiresSync( CHelpResource::ENTRATA_SYNC_REQUIRED );
    	$this->setCid( CClient::ID );

    	return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    }

}
?>