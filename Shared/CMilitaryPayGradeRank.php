<?php

class CMilitaryPayGradeRank extends CBaseMilitaryPayGradeRank {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMilitaryPayGradeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMilitaryRankId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>