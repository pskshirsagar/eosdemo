<?php

class CState extends CBaseState {

	protected $m_arrobjCompanyAreas;
	protected $m_arrobjDocumentTemplates;
	protected $m_arrobjJob;

	const STATE_CODE_DC = 'DC';
	const STATE_CODE_CA = 'CA';
	const STATE_CODE_WI = 'WI';
	const STATE_CODE_NC = 'NC';
	const STATE_CODE_TX = 'TX';
	const STATE_CODE_CO = 'CO';
    const STATE_CODE_MI = 'MI';
    const STATE_CODE_IL = 'IL';
	const STATE_CODE_NJ = 'NJ';
	const STATE_CODE_IN = 'IN';
	const STATE_CODE_OR = 'OR';

	public static $c_arrstrCoreUsStateCodes = [ 'AE', 'AP', 'AA', 'AL', 'AK', 'AS', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'FM', 'FL', 'GA', 'GU', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MH', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'MP', 'OH', 'OK', 'OR', 'PW', 'PA', 'PR', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VI', 'VA', 'WA', 'WV', 'WI', 'WY' ];

	public static $c_arrstrConsumerReportStateCodes = [ self::STATE_CODE_CA, self::STATE_CODE_WI, self::STATE_CODE_CO ];
	public static $c_arrstrConsumerReportNonCAStateCodes = [ self::STATE_CODE_WI, self::STATE_CODE_CO ];
	public static $c_arrstrRegistrationSettingStateCodes = [ self::STATE_CODE_NC, self::STATE_CODE_NJ ];

	protected $m_boolIsSelected;
	protected $m_boolIsAssociated;

    public function __construct() {
        parent::__construct();

        $this->m_arrobjCompanyAreas 		= [];
        $this->m_arrobjDocumentTemplates 	= [];

        return;
    }

	/**
	 * Add Functions
	 */

	public function addCompanyArea( $objCompanyArea ) {
		$this->m_arrobjCompanyAreas[$objCompanyArea->getId()] = $objCompanyArea;
	}

	public function addDocumentTemplate( $objDocumentTemplate ) {
		$this->m_arrobjDocumentTemplates[$objDocumentTemplate->getId()] = $objDocumentTemplate;
	}

	/**
	 * Get Functions
	 */

	public function getCompanyAreas() {
		return $this->m_arrobjCompanyAreas;
	}

	public function getDocumentTemplates() {
		return $this->m_arrobjDocumentTemplates;
	}

    public function getJob() {
		return $this->m_arrobjJob;
    }

    public function getSeoName() {
    	return  \Psi\CStringService::singleton()->strtolower( preg_replace( '/\s+/', '-', $this->getName() ) );
    }

    public function getIsSelected() {
    	return $this->m_boolIsSelected;
    }

    public function getIsAssociated() {
    	return $this->m_boolIsAssociated;
    }

	/**
	 * Set Functions
	 */

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
	   	if( true == isset( $arrmixValues['is_associated'] ) )		$this->setIsAssociated( $arrmixValues['is_associated'] );
    }

	public function setCompanyAreas( $arrobjCompanyAreas ) {
		$this->m_arrobjCompanyAreas = $arrobjCompanyAreas;
	}

	public function setDocumentTemplates( $arrobjDocumentTemplates ) {
		$this->m_arrobjDocumentTemplates = $arrobjDocumentTemplates;
	}

	public function setJob( $objJob ) {
		$this->m_arrobjJob[$objJob->getId()] = $objJob;
    }

    public function setIsSelected( $boolIsSelected ) {
    	$this->m_boolIsSelected = $boolIsSelected;
    }

    public function setIsAssociated( $boolIsAssociated ) {
    	$this->m_boolIsAssociated = $boolIsAssociated;
    }

	/**
	 * Validate Functions
	 */

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
                break;

            default:
            	// default case
            	$boolIsValid = true;
                break;
        }

        return $boolIsValid;
    }

}
?>