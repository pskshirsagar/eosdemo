<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CMessageTypes
 * Do not add any new functions to this class.
 */

class CMessageTypes extends CBaseMessageTypes {

	public static function fetchMessageTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMessageType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMessageType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMessageType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMessageTypesByIds( $arrintMessageTypeIds, $objSmsDatabse ) {
		if( false == valArr( $arrintMessageTypeIds ) ) return NULL;
		return self::fetchMessageTypes( 'SELECT * FROM message_types WHERE id IN ( ' . implode( ', ', $arrintMessageTypeIds ) . ' ) ORDER BY order_num ', $objSmsDatabse );
	}

	public static function fetchAllMessageTypes( $objSmsDatabse ) {
		return self::fetchMessageTypes( 'SELECT * FROM message_types ORDER BY name, order_num', $objSmsDatabse );
	}
}
?>