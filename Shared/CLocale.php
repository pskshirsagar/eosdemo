<?php

class CLocale extends CBaseLocale {

	const DEFAULT_LOCALE = 'en_US';
	const ENGLISH_UNITED_STATES = 'en_US';
	const ENGLISH_CANADA = 'en_CA';
	const ENGLISH_GREAT_BRITAIN = 'en_GB';
	const SPANISH_MEXICO = 'es_MX';
	const SPANISH_PERU = 'es_PE';
	const HINDI_INDIA = 'hi_IN';
	const CHINESE_CHINA = 'zh_CN';
	const ENGLISH_IRELAND = 'en_IE';
	const SPANISH_ENGLISH  = 'es_US';
	const SPANISH_SPAIN  = 'es_ES';
	const FRENCH_FRANCE  = 'fr_FR';

	// @FIXME: This should probably go in CInternationalTranslator?
	const TRANSLATION_VENDOR_LINGOTEK = 'lingotek';
	const TRANSLATION_VENDOR_GOOGLE = 'google';
    const TRANSLATION_VENDOR_ZAB = 'zab';

	public function valLocaleCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsoLanguageCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsoLanguageName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsoRegionCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsoRegionName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPluralForms() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsBaseLocale() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getDisplayName() {
		return $this->getIsoLanguageName( $this->getLocaleCode() ) . ' (' . $this->getIsoRegionName( $this->getLocaleCode() ) . ')';
	}

	public function getTranslationVendorName() {
		switch( $this->getLocaleCode() ) {
			case 'hi_IN':
				return self::TRANSLATION_VENDOR_GOOGLE;
				break;
			default:
				return self::TRANSLATION_VENDOR_LINGOTEK;
		}
	}

}
?>