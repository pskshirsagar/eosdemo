<?php

class CComplianceLevel extends CBaseComplianceLevel {

	const PROPERTY					= 1;
	const WORKER					= 2;
	const LOCATION					= 3;
	const VENDOR_LEGAL_ENTITY		= 4;

	public function valId() {
		return true;
	}

	public function valName() {
		return true;
	}

	public function valDescription() {
		return true;
	}

	public function valIsPublished() {
		return true;
	}

	public function valOrderNum() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function loadComplianceLevelNames( $arrmixTemplateParameters ) {

		$arrmixTemplateParameters['compliance_level_names'] = [
			self::PROPERTY	=> 'Property',
			self::WORKER	=> 'Worker',
			self::LOCATION	=> 'Location',
			self::VENDOR_LEGAL_ENTITY	=> 'Vendor Legal entity',
		];

		return $arrmixTemplateParameters;
	}

	public static function getComplianceLevelName( $intComplianceLevelId ) {

		$strComplianceLevelName = '';

		switch( $intComplianceLevelId ) {

			case self::PROPERTY:
				$strComplianceLevelName = 'Property';
				break;

			case self::WORKER:
				$strComplianceLevelName = 'Worker';
				break;

			case self::LOCATION:
				$strComplianceLevelName = 'Location';
				break;

			case self::VENDOR_LEGAL_ENTITY:
				$strComplianceLevelName = 'Vendor Legal Entity';
				break;

			default:
				// default action

		}

		return $strComplianceLevelName;
	}

}
?>