<?php

class CMilitaryRank extends CBaseMilitaryRank {

	protected $m_strMilitaryComponentName;
	protected $m_strMilitaryPayGradeName;
	protected $m_intMilitaryPayGradeId;

	public function getMilitaryComponentName() {
		return $this->m_strMilitaryComponentName;
	}

	public function getMilitaryPayGradeName() {
		return $this->m_strMilitaryPayGradeName;
	}

	public function getMilitaryPayGradeId() {
		return $this->m_intMilitaryPayGradeId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['military_component_name'] ) )							$this->setMilitaryComponentName( $arrmixValues['military_component_name'] );
		if( true == isset( $arrmixValues['military_pay_grade_name'] ) )							$this->setMilitaryPayGradeName( $arrmixValues['military_pay_grade_name'] );
		if( true == isset( $arrmixValues['military_pay_grade_id'] ) )							$this->setMilitaryPayGradeId( $arrmixValues['military_pay_grade_id'] );
	}

	public function setMilitaryComponentName( $strMilitaryComponentName ) {
		$this->m_strMilitaryComponentName = $strMilitaryComponentName;
	}

	public function setMilitaryPayGradeName( $strMilitaryPayGradeName ) {
		$this->m_strMilitaryPayGradeName = $strMilitaryPayGradeName;
	}

	public function setMilitaryPayGradeId( $intMilitaryPayGradeId ) {
		$this->m_intMilitaryPayGradeId = $intMilitaryPayGradeId;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMilitaryComponentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMilitaryPayGradeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>