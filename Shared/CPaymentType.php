<?php

class CPaymentType extends CBasePaymentType {

    const CASH 						= 1;
    const CHECK 					= 2;
    const MONEY_ORDER 				= 3;
    const ACH 						= 4;
    const VISA 						= 5;
    const MASTERCARD 				= 6;
    const DISCOVER 					= 7;
    const AMEX 						= 8;
    const CHECK_21					= 9;
    const EMONEY_ORDER				= 10;
	const HAP						= 11;
	const BAH						= 12;
	const SEPA_DIRECT_DEBIT			= 13;
	const PAD						= 14;
	const AFT						= 15;

	const STR_EMONEY_ORDER_CONSTANT	= 'E-Money Order';
	const STR_ACH					= 'ACH';
	const STR_VISA					= 'Visa';
	const STR_MASTERCARD			= 'Master Card';
	const STR_DISCOVER				= 'Discover';
	const STR_AMEX					= 'American Express';
	const STR_CHECK21				= 'Check21';
	const STR_SEPA_DIRECT_DEBIT		= 'SEPA Direct Debit';
	const STR_PAD					= 'Pre-Authorized Debit';
	const STR_AFT					= 'Canada Automated File Transfer';
	const STR_MAESTRO		        = 'Maestro';
	const STR_JCB		            = 'JCB';
	const STR_DINERS_CLUB		    = 'Diners Club';

	const GETTER_SETTER_REGEX = '/^(get|set)(ach|vi|di|mc|ae|ch21|emo|sepa|pad)(.*)$/i';

    public static $c_arrintWebAllowedPaymentTypes = [ self::ACH, self::MASTERCARD, self::VISA, self::DISCOVER, self::AMEX, self::SEPA_DIRECT_DEBIT, self::PAD ];
    public static $c_arrintCreditCardPaymentTypes = [ self::MASTERCARD, self::VISA, self::DISCOVER, self::AMEX ];
	public static $c_arrintAccountCreditCardPaymentTypes = [ self::MASTERCARD, self::VISA, self::DISCOVER ];
	public static $c_arrintFeeQualifyingPaymentTypeIds = [ self::ACH, self::MASTERCARD, self::VISA, self::DISCOVER, self::AMEX, self::CHECK_21, self::PAD ];
    public static $c_arrintElectronicPaymentTypeIds = [ self::ACH, self::MASTERCARD, self::VISA, self::DISCOVER, self::AMEX, self::CHECK_21, self::EMONEY_ORDER, self::SEPA_DIRECT_DEBIT, self::PAD ];
    public static $c_arrintElectronicPaymentTypeIdsWithoutCheck21 = [ self::ACH, self::MASTERCARD, self::VISA, self::DISCOVER, self::AMEX, self::EMONEY_ORDER, self::SEPA_DIRECT_DEBIT, self::PAD ];
    public static $c_arrintNonElectronicPaymentTypeIds = [ self::CASH, self::CHECK, self::MONEY_ORDER, self::HAP, self::BAH ];
	public static $c_arrintExcludedNonElectronicPaymentTypeIds = [ self::CASH, self::CHECK, self::MONEY_ORDER, self::HAP, self::BAH ];
	public static $c_arrintECheckPaymentTypeIds = [ self::ACH, self::PAD ];
	public static $c_arrintBlockedInEntrata = [ self::SEPA_DIRECT_DEBIT ];
	public static $c_arrintInsurancePolicyPaymentTypeIds = [ self::ACH, self::CASH, self::CHECK, self::AMEX, self::DISCOVER, self::MASTERCARD, self::VISA, self::PAD ];

	public static $c_arrstrWebAllowedPaymentTypeNames = [
		self::ACH			=> 'eCheck',
		self::MASTERCARD	=> 'MasterCard',
		self::VISA			=> 'Visa',
		self::DISCOVER		=> 'Discover',
		self::AMEX			=> 'Amex'
	];

	/**
	 * List of supported Payment types by country code
	 *
	 * @var array
	 */
	public static $c_arrintPaymentTypesByCountryCode = [
		CCountry::CODE_USA => [ self::ACH, self::VISA, self::MASTERCARD, self::DISCOVER, self::AMEX, self::CHECK_21, self::EMONEY_ORDER ],
		CCountry::CODE_CANADA => [ self::VISA, self::MASTERCARD, self::DISCOVER, self::AMEX, self::PAD ],
		CCountry::CODE_MEXICO => [ self::VISA, self::MASTERCARD, self::DISCOVER, self::AMEX ],
		CCountry::CODE_SPAIN => [ self::SEPA_DIRECT_DEBIT, self::VISA, self::MASTERCARD, self::DISCOVER, self::AMEX ],
		CCountry::CODE_IRELAND => [ self::SEPA_DIRECT_DEBIT, self::VISA, self::MASTERCARD, self::DISCOVER, self::AMEX ],
		CCountry::CODE_FRANCE => [ self::SEPA_DIRECT_DEBIT, self::VISA, self::MASTERCARD, self::DISCOVER, self::AMEX ],
		CCountry::CODE_UNITED_KINGDOM => [ self::VISA, self::MASTERCARD, self::DISCOVER, self::AMEX ]
	];

	public static $c_arrintInternationalPaymentTypes = [ self::MASTERCARD, self::VISA, self::DISCOVER, self::AMEX, self::SEPA_DIRECT_DEBIT ];

	public static $c_arrintMerchantAccountPaymentTypeIds = [ self::ACH, self::VISA, self::MASTERCARD, self::DISCOVER, self::AMEX, self::CHECK_21, self::EMONEY_ORDER, self::SEPA_DIRECT_DEBIT, self::PAD ];

	public static $c_arrstrMerchantAccountAbbreviatedPaymentTypes = [
		self::ACH => 'ACH',
		self::VISA => 'Visa',
		self::MASTERCARD => 'MC',
		self::DISCOVER => 'Disc',
		self::AMEX => 'Amex',
		self::CHECK_21 => 'Check 21',
		self::EMONEY_ORDER => 'EMO',
		self::SEPA_DIRECT_DEBIT => 'SEPA',
		self::PAD => 'PAD'
	];

	public static $c_arrmixPaymentTypesByGatewayTypeId = [
		CGatewayType::ACH => [
			self::ACH
		],
		CGatewayType::CC => [
			self::VISA,
			self::MASTERCARD,
			self::DISCOVER,
			self::AMEX,
		],
		CGatewayType::CHECK21 => [
			self::CHECK_21,
		],
		CGatewayType::EMO => [
			self::EMONEY_ORDER,
		],
		CGatewayType::SEPA => [
			self::SEPA_DIRECT_DEBIT,
		],
		CGatewayType::PAD => [
			self::PAD
		]
	];

    protected $m_fltConvenienceFeeAmount;
    protected $m_fltDebitConvenienceFeeAmount;
    protected $m_fltCompanyChargeAmount;
    protected $m_fltCompanyDebitChargeAmount;
    protected $m_fltMaxPaymentAmount;
    protected $m_fltPaymentCost;
    protected $m_fltDebitPaymentCost;
	protected $m_intMerchantGatewayId;
	protected $m_fltResidentInternationalFeeAmount;
	protected $m_fltResidentCommercialFeeAmount;
	protected $m_fltResidentDebitInternationalFeeAmount;
	protected $m_fltResidentDebitCommercialFeeAmount;

    /**
     * Get Functions
     */
	public function getWebAllowedPaymentTypeNameById( $intPaymentTypeId ) {

		switch( $intPaymentTypeId ) {
			case self::ACH:
				return __( 'eCheck' );
	            break;

	        case self::MASTERCARD:
				return __( 'Mastercard' );
	            break;

	        case self::VISA:
		        return __( 'Visa' );
	            break;

			case self::DISCOVER:
				return __( 'Discover' );
				break;

			case self::AMEX:
				return __( 'Amex' );
				break;

			case self::SEPA_DIRECT_DEBIT:
				return __( 'SEPA Direct Debit' );
				break;

			case self::PAD:
				return __( 'Pre-Authorized Debit' );
				break;

			case self::AFT:
				return __( 'Canada Automated File Transfer' );

			default:
				return NULL;
		}
	}

    public function getConvenienceFeeAmount() {
        return $this->m_fltConvenienceFeeAmount;
    }

    public function getDebitConvenienceFeeAmount() {
    	return $this->m_fltDebitConvenienceFeeAmount;
    }

    public function getMaxPaymentAmount() {
        return $this->m_fltMaxPaymentAmount;
    }

    public function getCompanyChargeAmount() {
        return $this->m_fltCompanyChargeAmount;
    }

    public function getCompanyDebitChargeAmount() {
    	return $this->m_fltCompanyDebitChargeAmount;
    }

    public function getPaymentCost() {
        return $this->m_fltPaymentCost;
    }

    public function getDebitPaymentCost() {
    	return $this->m_fltDebitPaymentCost;
    }

    public function getMerchantGatewayId() {
        return $this->m_intMerchantGatewayId;
    }

	/**
	 * Set Functions
	 */

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrmixValues['customer_convenience_fee'] ) ) $this->setCustomerConvenienceFee( $arrmixValues['customer_convenience_fee'] );
        if( true == isset( $arrmixValues['client_charge_amount'] ) ) $this->setClientChargeAmount( $arrmixValues['client_charge_amount'] );

        return;
    }

    public function setMaxPaymentAmount( $fltMaxPaymentAmount ) {
        $this->m_fltMaxPaymentAmount = $fltMaxPaymentAmount;
    }

    public function setConvenienceFeeAmount( $fltConvenienceFeeAmount ) {
        $this->m_fltConvenienceFeeAmount = CStrings::strToFloatDef( $fltConvenienceFeeAmount, NULL, true, 2 );
    }

    public function setDebitConvenienceFeeAmount( $fltDebitConvenienceFeeAmount ) {
    	$this->m_fltDebitConvenienceFeeAmount = CStrings::strToFloatDef( $fltDebitConvenienceFeeAmount, NULL, true, 2 );
    }

    public function setCompanyChargeAmount( $fltCompanyChargeAmount ) {
        $this->m_fltCompanyChargeAmount = CStrings::strToFloatDef( $fltCompanyChargeAmount, NULL, true, 2 );
    }

    public function setCompanyDebitChargeAmount( $fltCompanyDebitChargeAmount ) {
    	$this->m_fltCompanyDebitChargeAmount = CStrings::strToFloatDef( $fltCompanyDebitChargeAmount, NULL, true, 2 );
    }

    public function setPaymentCost( $fltPaymentCost ) {
        $this->m_fltPaymentCost = CStrings::strToFloatDef( $fltPaymentCost, NULL, true, 2 );
    }

    public function setDebitPaymentCost( $fltDebitPaymentCost ) {
    	$this->m_fltDebitPaymentCost = CStrings::strToFloatDef( $fltDebitPaymentCost, NULL, true, 2 );
    }

    public function setMerchantGatewayId( $intMerchantGatewayId ) {
        $this->m_intMerchantGatewayId = $intMerchantGatewayId;
    }

    public static function stringToId( $strKey ) {
    	switch( $strKey ) {
			case 'Ach':
				return self::ACH;

			case 'Vi':
				return self::VISA;

			case 'Mc':
				return self::MASTERCARD;

			case 'Di':
				return self::DISCOVER;

			case 'Ae':
				return self::AMEX;

			case 'Ch21':
				return self::CHECK_21;

			case 'Emo':
				return self::EMONEY_ORDER;

			case 'Sepa':
				return self::SEPA_DIRECT_DEBIT;

			case 'Pad':
				return self::PAD;

			case 'Aft':
				return self::AFT;

			default:
				return NULL;
		}
	}

	public static function idToString( $intId ) {
		switch( $intId ) {
			case self::ACH:
				return 'ach';

			case self::VISA:
				return 'vi';

			case self::MASTERCARD:
				return 'mc';

			case self::DISCOVER:
				return 'di';

			case self::AMEX:
				return 'ae';

			case self::CHECK_21:
				return 'ch21';

			case self::EMONEY_ORDER:
				return 'emo';

			case self::SEPA_DIRECT_DEBIT:
				return 'sepa';

			case self::PAD:
				return 'pad';

			case self::AFT:
				return 'aft';

			default:
				return NULL;
		}
	}

	public static function idToFormattedString( $intPaymentTypeId ) {
    	switch( $intPaymentTypeId ) {
			case self::ACH:
				return self::STR_ACH;

			case self::VISA:
				return self::STR_VISA;

			case self::MASTERCARD:
				return self::STR_MASTERCARD;

			case self::DISCOVER:
				return self::STR_DISCOVER;

			case self::AMEX:
				return self::STR_AMEX;

			case self::CHECK_21:
				return self::STR_CHECK21;

			case self::EMONEY_ORDER:
				return self::STR_EMONEY_ORDER_CONSTANT;

			case self::SEPA_DIRECT_DEBIT:
				return self::STR_SEPA_DIRECT_DEBIT;

			case self::PAD:
				return self::STR_PAD;

			case self::AFT:
				return self::STR_AFT;

			default:
				return '';
		}
	}

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function setResidentInternationalFeeAmount( $fltResidentInternationalFeeAmount ) {
		$this->m_fltResidentInternationalFeeAmount = $fltResidentInternationalFeeAmount;
	}

	public function getResidentInternationalFeeAmount() {
		return $this->m_fltResidentInternationalFeeAmount;
	}

	public function setResidentCommercialFeeAmount( $fltResidentCommercialFeeAmount ) {
		$this->m_fltResidentCommercialFeeAmount = $fltResidentCommercialFeeAmount;
	}

	public function getResidentCommercialFeeAmount() {
		return $this->m_fltResidentCommercialFeeAmount;
	}

	public function setResidentDebitInternationalFeeAmount( $fltResidentDebitInternationalFeeAmount ) {
		$this->m_fltResidentDebitInternationalFeeAmount = $fltResidentDebitInternationalFeeAmount;
	}

	public function getResidentDebitInternationalFeeAmount() {
		return $this->m_fltResidentDebitInternationalFeeAmount;
	}

	public function setResidentDebitCommercialFeeAmount( $fltResidentDebitCommercialFeeAmount ) {
		$this->m_fltResidentDebitCommercialFeeAmount = $fltResidentDebitCommercialFeeAmount;
	}

	public function getResidentDebitCommercialFeeAmount() {
		return $this->m_fltResidentDebitCommercialFeeAmount;
	}

	public static function getInternationalCardFormattedString( $strPaymentCardType ) {
		switch( $strPaymentCardType ) {
			case 'Maestro':
				return self::STR_MAESTRO;

			case 'JCB':
				return self::STR_JCB;

			case 'DinersClub':
				return self::STR_DINERS_CLUB;

			default:
				return '';
		}
	}

	public static function getPaymentTypeInOrder( $arrobjPaymentTypes ) {
		$paymentTypeOrders = [
			\CPaymentType::ACH,
			\CPaymentType::SEPA_DIRECT_DEBIT,
			\CPaymentType::PAD,
			\CPaymentType::MASTERCARD,
			\CPaymentType::VISA,
		];
		$arrobjPaymentTypeInOrder = [];

		foreach( $paymentTypeOrders as $paymentTypeOrder ) {
			if( isset( $arrobjPaymentTypes[ $paymentTypeOrder ] ) ) {
				$arrobjPaymentTypeInOrder[$paymentTypeOrder] = $arrobjPaymentTypes[ $paymentTypeOrder ];
				unset( $arrobjPaymentTypes[ $paymentTypeOrder ] );
			}
		}

		if( 0 < count( $arrobjPaymentTypes ) ) {
			foreach( $arrobjPaymentTypes as $objPaymentType ) {
				$arrobjPaymentTypeInOrder[$objPaymentType->getId()] = $objPaymentType;
			}
		}

		return $arrobjPaymentTypeInOrder;
	}

}
?>