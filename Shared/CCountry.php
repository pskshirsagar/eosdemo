<?php

class CCountry extends CBaseCountry {

	const CODE_USA     = 'US';
	const CODE_INDIA   = 'IN';
	const CODE_CANADA  = 'CA';
	const CODE_IRELAND = 'IE';
	const CODE_MEXICO  = 'MX';
	const CODE_SPAIN   = 'ES';
	const CODE_FRANCE  = 'FR';
	const CODE_CHINA   = 'CN';
	const CODE_PERU	   = 'PE';
	const CODE_UNITED_KINGDOM	= 'GB';

	public static $c_arrstrCountryCodes = [
		self::CODE_USA,
		self::CODE_CANADA,
		self::CODE_MEXICO,
		self::CODE_IRELAND,
		self::CODE_SPAIN,
		self::CODE_FRANCE,
	];

	public static $c_arrstrCountriesWithThePrefix = [
		'BS' => 'Bahamas',
		'KM' => 'Comoros',
		'CG' => 'Congo',
		'CK' => 'Cook Islands',
		'DO' => 'Dominican Republic',
		'MV' => 'Maldives',
		'MH' => 'Marshall Islands',
		'NL' => 'Netherlands',
		'PH' => 'Philippines',
		'SC' => 'Seychelles',
		'SB' => 'Solomon Islands',
		'AE' => 'United Arab Emirates',
		'GB' => 'United Kingdom',
		'US' => 'United States',
		'VG' => 'Virgin Islands, British',
		'VI' => 'Virgin Islands, U.S.'
	];

	public static $c_arrstrPaymentFacilitatorCountryCodes = [
		self::CODE_USA,
		self::CODE_CANADA
	];

	public static $c_arrstrCountryCodesRequiringDefaultConsent = [
		self::CODE_CHINA,
		self::CODE_SPAIN,
		self::CODE_FRANCE,
		self::CODE_UNITED_KINGDOM,
		self::CODE_IRELAND
	];

	public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
                break;

            default:
            	// default case
            	$boolIsValid = true;
                break;
        }

        return $boolIsValid;
    }

    /**
     * If you are making any changes in loadSmartyConstants function then
     * please make sure the same changes would be applied to loadTemplateConstants function also.
     */

	public static function loadSmartyConstants( $objSmarty ) {

    	$objSmarty->assign( 'COUNTRY_CODE_USA', 	self::CODE_USA );
    	$objSmarty->assign( 'COUNTRY_CODE_INDIA', 	self::CODE_INDIA );
    	$objSmarty->assign( 'COUNTRY_CODE_CANADA', 	self::CODE_CANADA );
    }

    public static function loadTemplateConstants( $arrmixTemplateParameters ) {

    	$arrmixTemplateParameters['COUNTRY_CODE_USA']		= self::CODE_USA;
    	$arrmixTemplateParameters['COUNTRY_CODE_INDIA']		= self::CODE_INDIA;
    	$arrmixTemplateParameters['COUNTRY_CODE_CANADA']	= self::CODE_CANADA;
    	return $arrmixTemplateParameters;
    }

}
?>