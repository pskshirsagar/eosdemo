<?php

class CCompanySettingKey extends CBaseCompanySettingKey {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanySettingGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDatabaseTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valToolTipId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLabel() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTableName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valColumnName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequiresSync() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDisabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>