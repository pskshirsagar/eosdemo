<?php

class CPaymentDistributionType extends CBasePaymentDistributionType {

    const SETTLEMENT 			= 1;
    const REVERSAL_RETURN		= 2;
    const RECALL 				= 3;
    const REVERSAL 				= 4;
    const INTERNAL_TRANSFER		= 5;
}
?>