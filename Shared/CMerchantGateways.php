<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CMerchantGateways
 * Do not add any new functions to this class.
 */

/**
 * @todo : EOS migrated
 */
class CMerchantGateways extends CBaseMerchantGateways {

	public static function fetchMerchantGateways( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMerchantGateway', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchMerchantGateway( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMerchantGateway', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

    public static function fetchAllMerchantGateways( $objDatabase ) {
        return self::fetchMerchantGateways( 'SELECT * FROM merchant_gateways ORDER BY order_num, name', $objDatabase );
    }

    public static function fetchMerchantGatewaysByIds( $arrintMerchantGatewayIds, $objDatabase ) {
    	if( false == valArr( $arrintMerchantGatewayIds ) ) return false;
    	$strSql = 'SELECT
						id,
    					name
    				FROM
    					merchant_gateways
    				WHERE
    					id IN ( ' . implode( ',', $arrintMerchantGatewayIds ) . ' )
    				ORDER BY
    					order_num,
    					name';

        return fetchData( $strSql, $objDatabase );
    }

	public static function fetchAllMerchantGatewaysOrderByName( $objDatabase ) {
        return self::fetchMerchantGateways( 'SELECT * FROM merchant_gateways ORDER BY name', $objDatabase );
    }

    public static function fetchAllActiveMerchantGatewaysOrderByName( $objDatabase ) {
        return CMerchantGateways::fetchMerchantGateways( 'SELECT * FROM merchant_gateways where is_published = 1 ORDER BY name', $objDatabase );
    }

	public static function fetchMerchantGatewaysByIsPublished( $intIsPublished, $objDatabase ) {
        return self::fetchMerchantGateways( sprintf( 'SELECT * FROM merchant_gateways WHERE is_published = %d', ( int ) $intIsPublished ), $objDatabase );
    }

}
?>