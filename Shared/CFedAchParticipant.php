<?php

class CFedAchParticipant extends CBaseFedAchParticipant {

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;

        if( true == is_null( $this->m_intId ) ) {
			trigger_error( 'Invalid CFedAchParticipant: Id is required - CFedAchParticipant::valId()', E_USER_ERROR );
        }

        return $boolIsValid;
    }

    public function valRoutingNumber() {
        $boolIsValid = true;

        if( true == is_null( $this->m_strRoutingNumber ) ) {
			trigger_error( 'Invalid CFedAchParticipant: Id is required - CFedAchParticipant::valId()', E_USER_ERROR );
        }

        if( 9 != strlen( $this->m_strRoutingNumber ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'routing_number', 'Routing Number must be 9 digits' ) );
        }

        return $boolIsValid;
    }

    public function valOfficeCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valServicingFedRoutingNumber() {
        $boolIsValid = true;

        if( 9 != strlen( $this->m_strServicingFedRoutingNumber ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'servicing_fed_routing_number', 'Servicing Fed Routing Number must be 9 digits' ) );
        }

        return $boolIsValid;
    }

    public function valRecordTypeCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valChangeDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNewRoutingNumber() {
        $boolIsValid = true;

        if( 9 != strlen( $this->m_strNewRoutingNumber ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'new_routing_number', 'New Routing Number must be 9 digits' ) );
        }

        return $boolIsValid;
    }

    public function valCustomerName() {
        $boolIsValid = true;

        // Validation example
        if( true == is_null( $this->getCustomerName() ) ) {
			trigger_error( 'Invalid CFedAchParticipant: Customer Name is required - CFedAchParticipant::valCustomerName()', E_USER_ERROR );
        }

        return $boolIsValid;
    }

    public function valAddress() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCity() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valStateCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPostalCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPhoneNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInstitutionStatusCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDataViewCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
				$boolIsValid &= $this->valRoutingNumber();
				$boolIsValid &= $this->valServicingFedRoutingNumber();
				$boolIsValid &= $this->valNewRoutingNumber();
				$boolIsValid &= $this->valCustomerName();
		        break;

            case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valRoutingNumber();
				$boolIsValid &= $this->valServicingFedRoutingNumber();
				$boolIsValid &= $this->valNewRoutingNumber();
				$boolIsValid &= $this->valCustomerName();
		        break;

            case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
                break;

            default:
            	// default case
            	$boolIsValid = true;
                break;
        }

        return $boolIsValid;
    }
}
?>