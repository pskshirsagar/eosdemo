<?php

class CReturnType extends CBaseReturnType {

	const R05 = 5;
	const R07 = 7;
	const R10 = 10;
	const R11 = 11;
	const R29 = 29;
	const R51 = 51;

	// Here are notice of change codes
	const MANUAL 																				= 99;
	const INCORRECT_DFI_ACCOUNT_NUMBER 															= 101;
	const INCORRECT_ROUTING_NUMBER																= 102;
	const INCORRECT_ROUTING_NUMBER_AND_INCORRECT_DFI_ACCOUNT_NUMBER								= 103;
	const INCORRECT_INDIVIDUAL_NAME_RECEIVING_COMPANY_NAME										= 104;
	const INCORRECT_TRANSACTION_CODE															= 105;
	const INCORRECT_DFI_ACCOUNT_NUMBER_AND_INCORRECT_TRANSACTION_CODE							= 106;
	const INCORRECT_ROUTING_NUMBER_INCORRECT_DFI_ACCOUNT_NUMBER_AND_INCORRECT_TRANSACTION_CODE	= 107;
	const INCORRECT_FOREIGN_RECEIVING_DFI_IDENTIFICATION										= 108;
	const INCORRECT_INDIVIDUAL_IDENTIFICATION_NUMBER											= 109;
	const BAD_CHECK21_IMAGE																		= 179;
	const CHECK21_AMOUNT_ADJUSTMENT																= 180;
	const CHECK21_DUPLICATE_PRESENTMENT															= 181;
	const CHECK21_MICR_AND_ITEM_DO_NOT_MATCH													= 182;
	const CHECK21_INSUFFICIENT_FUNDS															= 183;
	const CHARGE_BACK_ADJUSTMENT																= 201;
	const OTHER																					= 177;
	const REFER_TO_MAKER																		= 173;

	const CHECK21_UNCOLLECTED_FUNDS_HOLD														= 207;
	const CHECK21_STOP_PAYMENT																	= 208;
	const CHECK21_CLOSED_ACCOUNT																= 209;
	const CHECK21_UNABLE_TO_LOCATE_ACCOUNT														= 210;
	const CHECK21_UNABLE_TO_FROZEN_BLOCKED_ACCOUNT												= 211;
	const CHECK21_STALE_DATED																	= 212;
	const CHECK21_POST_DATED																	= 213;
	const CHECK21_ENDORSEMENT_MISSING															= 214;
	const CHECK21_ENDORSEMENT_IRREGULAR															= 215;
  	const CHECK21_SIGNATURE_MISSING																= 216;
	const CHECK21_SIGNATURE_IRREGULAR															= 217;
	const CHECK21_NON_CASH_ITEM																	= 218;
	const CHECK21_ALERTED_FICTICIOUS_ITEM														= 219;
	const CHECK21_UNABLE_TO_PROCESS																= 220;
	const CHECK21_ITEM_EXCEEDED_DOLLER_LIMIT													= 221;
	const CHECK21_NOT_AUTHORIZED																= 222;
	const CHECK21_BRANCH_ACCOUNT_SOLD															= 223;
	const CHECK21_STOP_PAYMENT_SUSPECT															= 224;
	const CHECK21_UNUSABLE_IMAGE																= 225;
	const CHECK21_IMAGE_FAILS_SECURITY_CHECK													= 226;
	const CHECK21_CANNOT_DETERMINE_ACCOUNT														= 227;
	const AVS_OR_CID_NOT_OBTAINED																= 228;

	const PAD_INST_ID_INVALID																	= 234;
	const PAD_ACCT_NUMBER_INVALID																= 235;
	const PAD_INST_ID_FOR_RETURN_INVALID														= 236;
	const PAD_ACCT_NUMBER_FOR_RETURN_INVALID													= 237;
	const PAD_DESTINATION_INST_IS_NOT_DEFINED													= 238;
	const PAD_DESTINATION_ACCT_NUMBER_INVALID													= 239;
	const PAD_INST_FOR_RETURN_NOT_DEFINED														= 240;
	const PAD_INST_FOR_RETURN_CROSS_REF_INVALID													= 241;
	const PAD_ACCT_FOR_RETURN_INVALID															= 242;
	const PAD_NFS_DEBIT_ONLY																	= 243;
	const PAD_CANNOT_TRACE																		= 244;
	const PAD_PAYMENT_STOPPED_RECALLED															= 245;
	const PAD_POST_STALE_DATED																	= 246;
	const PAD_CA_ACCOUNT_CLOSED																	= 247;
	const PAD_NO_DEBIT_ALLOWED																	= 248;
	const PAD_FUNDS_NOT_CLEARED																	= 249;
	const PAD_CURRENCY_ACCOUNT_MISMATCH															= 250;
	const PAD_PAYOR_PAYEE_DECEASED																= 251;
	const PAD_CA_ACCOUNT_FROZEN																	= 252;
	const PAD_INVALID_INCORRECT_ACCT_NUMBER														= 253;
	const PAD_INCORRECT_PAYOR_PAYEE_NAME														= 254;
	const PAD_NO_AGREEMENT_EXISTED_BUSINESS_PERSONAL											= 255;
	const PAD_NOT_ACCORDING_TO_AGREEMENT_PERSONAL 												= 256;
	const PAD_AGREEMENT_REVOKED_PERSONAL		 												= 257;
	const PAD_NO_CONFIRMATION_PERSONAL			 												= 258;
	const PAD_NOT_ACCORDING_TO_AGREEMENT_BUSINESS		 										= 259;
	const PAD_AGREEMENT_REVOKED_BUSINESS				 										= 260;
	const PAD_NO_CONFIRMATION_BUSINESS					 										= 261;
	const PAD_CUSTOMER_INITIATED_RETURN_CREDIT													= 262;
	const PAD_INSTITUTION_DEFAULT																= 263;


    const CHECK21_DUPLICATE_PRESENTMENT_ADJUSTMENT												= 229;
    const CHECK21_NON_CASH_ITEM_ADJUSTMENT									                    = 230;
    const CHECK21_BAD_IMAGE_ADJUSTMENT															= 231;
    const CHECK21_FOREIGN_ITEM																    = 232;
    const CHECK21_FOREIGN_ITEM_ADJUSTMENT														= 233;

	const FRAUD_NO_CARDHOLDER_AUTHORIZATION_OR_TRANSACTION_NOT_RECOGNIZED 						= 202;
	const NON_RECEIPT_OF_MERCHANDISE_SERVICES_NOT_RENDERED 										= 203;
	const DUPLICATE_PROCESSING 																	= 204;
	const CANCELLED_RECURNING_TRANSACTION 														= 205;
	const CREDIT_NOT_PROCESSED 																	= 206;
	const INVALID_ACCOUNT_NUMBER																= 4;
	const NON_TRANSACTION_ACCOUNT																= 20;
	const NO_ACCOUNT_OR_UNABLE_TO_LOCATE_ACCOUNT												= 3;
	const ACCOUNT_CLOSED																		= 2;
	// GENERIC CHECK21 RETURN TYPE
	const GENERIC_CHECK21_RETURN_TYPE 															= 184;

	const GENERIC_SEPA_REJECTION																= 338;

	const AFT_FILE_REJECTION																	= 340;

	// NSF and Insufficient Funds codes
	const INSUFFICIENT_FUNDS																	= 1;
	const NSF_UNCOLLECTED																		= 170;

	// CANADA
	const NFS_DIRECT_DEBIT																		= 243;

	const ACCOUNT_FROZEN																		= 16;
	const ROUTING_NUMBER_CHECK_DIGIT_ERROR														= 28;
	const CORPORATE_CUSTOMER_ADVISES_NOT_AUTHORIZED												= 29;

	const BANK_OF_MONTREAL_NOTICE_OF_CHANGE	= 339;
	const BANK_OF_MONTREAL_ACCOUNT_CLOSED = 247;
	const BANK_OF_MONTREAL_INSTITUTION_IN_DEFAULT = 263;
	const BANK_OF_MONTREAL_INVALID_INCORRECT_ACCOUNT = 253;

	protected $m_strPaymentType;

    public static $c_arrintCheck21AdjustmentTypes = [ CReturnType::CHECK21_AMOUNT_ADJUSTMENT, CReturnType::CHECK21_DUPLICATE_PRESENTMENT_ADJUSTMENT, CReturnType::CHECK21_NON_CASH_ITEM_ADJUSTMENT, CReturnType::CHECK21_BAD_IMAGE_ADJUSTMENT, CReturnType::CHECK21_FOREIGN_ITEM_ADJUSTMENT ];

    public static $c_arrintBlacklistEligibleTypes = [ CReturnType::INCORRECT_DFI_ACCOUNT_NUMBER, CReturnType::INCORRECT_ROUTING_NUMBER_AND_INCORRECT_DFI_ACCOUNT_NUMBER, CReturnType::INCORRECT_INDIVIDUAL_NAME_RECEIVING_COMPANY_NAME,
		CReturnType::INCORRECT_DFI_ACCOUNT_NUMBER_AND_INCORRECT_TRANSACTION_CODE, CReturnType::INCORRECT_ROUTING_NUMBER_INCORRECT_DFI_ACCOUNT_NUMBER_AND_INCORRECT_TRANSACTION_CODE, CReturnType::INVALID_ACCOUNT_NUMBER,
		CReturnType::NON_TRANSACTION_ACCOUNT, CReturnType::NO_ACCOUNT_OR_UNABLE_TO_LOCATE_ACCOUNT, CReturnType::ACCOUNT_CLOSED ];

    public static $c_arrintUnauthorizedReturnTypes = [ CReturnType::R05, CReturnType::R07, CReturnType::R10, CReturnType::R11, CReturnType::R29, CReturnType::R51 ];

	public static $c_arrintFailedPaymentReturnTypeIds = [ CReturnType::ACCOUNT_CLOSED, CReturnType::ACCOUNT_FROZEN, CReturnType::CORPORATE_CUSTOMER_ADVISES_NOT_AUTHORIZED, CReturnType::INSUFFICIENT_FUNDS, CReturnType::INVALID_ACCOUNT_NUMBER, CReturnType::NO_ACCOUNT_OR_UNABLE_TO_LOCATE_ACCOUNT, CReturnType::ROUTING_NUMBER_CHECK_DIGIT_ERROR ];


	public static $c_arrintPadReturnTypes = [ CReturnType::PAD_INST_ID_INVALID, CReturnType::PAD_ACCT_NUMBER_INVALID, CReturnType::PAD_INST_ID_FOR_RETURN_INVALID, CReturnType::PAD_ACCT_NUMBER_FOR_RETURN_INVALID, CReturnType::PAD_DESTINATION_INST_IS_NOT_DEFINED,
		CReturnType::PAD_DESTINATION_ACCT_NUMBER_INVALID, CReturnType::PAD_INST_FOR_RETURN_NOT_DEFINED, CReturnType::PAD_INST_FOR_RETURN_CROSS_REF_INVALID, CReturnType::PAD_ACCT_FOR_RETURN_INVALID, CReturnType::PAD_NFS_DEBIT_ONLY, CReturnType::PAD_CANNOT_TRACE,
		CReturnType::PAD_PAYMENT_STOPPED_RECALLED, CReturnType::PAD_POST_STALE_DATED, CReturnType::PAD_CA_ACCOUNT_CLOSED, CReturnType::PAD_NO_DEBIT_ALLOWED, CReturnType::PAD_FUNDS_NOT_CLEARED, CReturnType::PAD_CURRENCY_ACCOUNT_MISMATCH, CReturnType::PAD_PAYOR_PAYEE_DECEASED,
		CReturnType::PAD_CA_ACCOUNT_FROZEN, CReturnType::PAD_INVALID_INCORRECT_ACCT_NUMBER, CReturnType::PAD_INCORRECT_PAYOR_PAYEE_NAME, CReturnType::PAD_NO_AGREEMENT_EXISTED_BUSINESS_PERSONAL, CReturnType::PAD_NOT_ACCORDING_TO_AGREEMENT_PERSONAL, CReturnType::PAD_AGREEMENT_REVOKED_PERSONAL,
		CReturnType::PAD_NO_CONFIRMATION_PERSONAL, CReturnType::PAD_NOT_ACCORDING_TO_AGREEMENT_BUSINESS, CReturnType::PAD_AGREEMENT_REVOKED_BUSINESS, CReturnType::PAD_NO_CONFIRMATION_BUSINESS, CReturnType::PAD_CUSTOMER_INITIATED_RETURN_CREDIT, CReturnType::PAD_INSTITUTION_DEFAULT	 ];

	/**
	 * Set Functions
	 */

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['payment_type'] ) ) $this->setPaymentType( $arrValues['payment_type'] );
    }

	public function setPaymentType( $strPaymentType ) {
		$this->m_strPaymentType = $strPaymentType;
    }

    /**
     * Get Functions
     */

	public function getPaymentType() {
		return $this->m_strPaymentType;
    }

	public function update( $intCurrentUserId, $objAdminDatabase, $objPaymentDatabase = NULL, $objClientDatabase = NULL ) {
		$boolIsValid = true;

		$boolIsValid &= parent::update( $intCurrentUserId, $objAdminDatabase );
		$boolIsValid &= parent::update( $intCurrentUserId, $objPaymentDatabase );
		$boolIsValid &= parent::update( $intCurrentUserId, $objClientDatabase );

		return $boolIsValid;

	}

}
?>