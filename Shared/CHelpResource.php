<?php

class CHelpResource extends CBaseHelpResource {

	const FILE_MAX_SIZE 			= 52428800; 	// 50 mb

	protected $m_strHelpResourceTypeName;
	protected $m_strFilePath;
	protected $m_strHelpTopicTitle;
	protected $m_intModuleId;
	protected $m_intHelpTopicId;
	protected $m_intPsProductId;
	protected $m_strSignedFileName;
	protected $m_strVideoUrl;

	protected $m_arrstrCustomCourseDeadlineOptions;
	protected $m_arrstrCustomCourseRecurringOptions;
	protected $m_arrstrCustomCourseReminderOptions;
	protected $m_arrstrCustomCourseReassignAllowanceOptions;

	const COURSE_RETAKE_COUNT 		= 5;
	const COURSE_RESET_YES 			= 1;
	const COURSE_RESET_NO 			= 0;

	const ENTRATA_SYNC_REQUIRED 	= 1;
	const ENTRATA_SYNC_NOT_REQUIRED = 0;

	const IS_NOT_SEARCHABLE			= 0;

	const SEARCH_BY_TOPIC			= 1;
	const SEARCH_BY_MODULE			= 2;
	const SEARCH_BY_PRODUCT			= 3;
	const SEARCH_BY_STRING			= 4;

	const VISIBLE_TO_ADMINS			= 0;
	const VISIBLE_TO_ALL			= 1;
	const VISIBLE_TO_PS_ADMIN		= 2;

	const CUSTOM_COURSE_ID 						= 12;
	const ENTRATA_COURSE_ID 					= 9;
	const ENTRATA_COURSE_PASSING_PERCENTAGE 	= 80;

	const STORAGE_CONTAINER 		= 'entrata-system-documents';
	const STORAGE_CONTAINER_DEV 	= 'entrata-system-documents-dev';

	const PUBLISH_STATUS            = 'publish';
	const UNPUBLISH_STATUS          = 'unpublish';
	const PUBLISHED          		= '1';
	const UNPUBLISHED          		= '0';

	const ASSOCIATED_WITH_ENTRATA		= 'ENTRATA';
	const ASSOCIATED_WITH_CLIENTADMIN	= 'CLIENTADMIN';

	const RECURRING_NONE        = 0;
	const RECURRING_ANNUAL      = 1;
	const RECURRING_QUARTERLY   = 2;
	const RECURRING_SEMI_ANNUAL = 3;

	const NEVER_SKIP_REASSIGN            = 0;
	const SKIP_REASSIGN_FOR_ONE_YEAR     = 1;
	const SKIP_REASSIGN_FOR_TWO_YEAR     = 2;
	const ALWAYS_SKIP_REASSIGN           = 3;

	const NO_DEAD_LINE		= 0;
	const ASSIGNMENT_AFTER_ONE_MONTH		= 1;
	const ASSIGNMENT_AFTER_TWO_MONTHS		= 2;
	const ASSIGNMENT_AFTER_THREE_MONTHS		= 3;
	const ASSIGNMENT_AFTER_FIFTEEN_DAYS		= 4;

	const RETAKE_NEVER     			= 0;
	const RETAKE_AFTER_ONE_YEAR		= 1;
	const RETAKE_AFTER_TWO_YEARS	= 2;
	const NO_LIMIT_RETAKES			= 9999;
	protected $m_intHelpHelpResourceAssociationId;

	const NO_REMINDER                 = 0;
	const REMINDER_BEFORE_ONE_DAY     = 1;
	const REMINDER_BEFORE_TWO_DAYS    = 2;
	const REMINDER_BEFORE_THREE_DAYS  = 3;
	const REMINDER_BEFORE_ONE_WEEK    = 4;
	const REMINDER_BEFORE_TWO_WEEKS   = 5;
	const REMINDER_BEFORE_THREE_WEEKS = 6;

	const ADD_USER_TO_GROUP      = 1;
	const REMOVE_USER_FROM_GROUP = 2;
	const SEND_EMAIL_TO_EMPLOYEE = 3;

	const FAILED_COURSE   = 'failed_course';
	const COMPLETE_TRACK  = 'complete_track';
	const COMPLETE_COURSE = 'complete_course';

	const COMPLETE_TRACK_ID  = 1;
	const FAILED_COURSE_ID   = 2;
	const COMPLETE_COURSE_ID = 3;

	const SURVEY_SECTION_NAME = 'Survey';

	public static $c_arrstrCustomCourseRetake = [ self::RETAKE_NEVER => 'Never', self::RETAKE_AFTER_ONE_YEAR => '1 Year', self::RETAKE_AFTER_TWO_YEARS => '2 Years' ];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getCustomCourseDeadlineOptions() {

		$this->m_arrstrCustomCourseDeadlineOptions = [
			self::NO_DEAD_LINE 						=> __( 'No Deadline' ),
			self::ASSIGNMENT_AFTER_FIFTEEN_DAYS 	=> __( '15 Days After Assignment' ),
			self::ASSIGNMENT_AFTER_ONE_MONTH 		=> __( '1 Month After Assignment' ),
			self::ASSIGNMENT_AFTER_TWO_MONTHS 		=> __( '2 Months After Assignment' ),
			self::ASSIGNMENT_AFTER_THREE_MONTHS 	=> __( '3 Months After Assignment' )
		];

		return $this->m_arrstrCustomCourseDeadlineOptions;
	}

	public function getCustomCourseRecurringOptions() {

		$this->m_arrstrCustomCourseRecurringOptions = [
			self::RECURRING_NONE 			=> __( 'None' ),
			self::RECURRING_ANNUAL 			=> __( 'Annual' ),
			self::RECURRING_QUARTERLY 		=> __( 'Quarterly' ),
			self::RECURRING_SEMI_ANNUAL 	=> __( 'Semi-Annual' )
		];

		return $this->m_arrstrCustomCourseRecurringOptions;
	}

	public function getCustomCourseReminderOptions() {

		$this->m_arrstrCustomCourseReminderOptions = [
			self::REMINDER_BEFORE_ONE_WEEK		=> __( '1 Week Before' ),
			self::REMINDER_BEFORE_ONE_DAY 		=> __( '1 Day Before' ),
			self::REMINDER_BEFORE_TWO_DAYS 		=> __( '2 Days Before' ),
			self::REMINDER_BEFORE_THREE_DAYS	=> __( '3 Days Before' ),
			self::REMINDER_BEFORE_TWO_WEEKS		=> __( '2 Weeks Before' ),
			self::REMINDER_BEFORE_THREE_WEEKS 	=> __( '3 Weeks Before' ),
			self::NO_REMINDER 					=> __( 'None' )
		];

		return $this->m_arrstrCustomCourseReminderOptions;
	}

	public function getCustomCourseReassignAllowanceOptions() {

		$this->m_arrstrCustomCourseReassignAllowanceOptions = [
			self::NEVER_SKIP_REASSIGN 			=> __( 'Never Skip' ),
			self::SKIP_REASSIGN_FOR_ONE_YEAR 	=> __( 'Skip for 1 Year' ),
			self::SKIP_REASSIGN_FOR_TWO_YEAR 	=> __( 'Skip for 2 Years' ),
			self::ALWAYS_SKIP_REASSIGN 			=> __( 'Always Skip' )
		];

		return $this->m_arrstrCustomCourseReassignAllowanceOptions;
	}

	/**
	* Get Functions
	*
	*/

	public function getHelpResourceTypeName() {
		return $this->m_strHelpResourceTypeName;
	}

	public function getFilePath( $boolIsFullPath = true, $boolIsDbFilePath = false ) {
		$strArticleDirectory		= 'article/';
		$strSimulationDirectory		= 'simulation/';

		if( self::ASSOCIATED_WITH_CLIENTADMIN == $this->getHelpResourceSystem() ) {
			$strArticleDirectory	= 'ca_article/';
			$strSimulationDirectory	= 'ca_simulation/';
		}

		if( true == $boolIsDbFilePath ) {
			return $this->m_strFilePath;
		} else {
			if( CHelpResourceType::ARTICLE == $this->getHelpResourceTypeId() ) {
				$this->m_strFilePath = $strArticleDirectory;
			} elseif( CHelpResourceType::VIDEO == $this->getHelpResourceTypeId() ) {
				$this->m_strFilePath = 'video/';
			} elseif( CHelpResourceType::SIMULATION == $this->getHelpResourceTypeId() ) {
				if( false == is_null( $this->getId() ) ) {
					$this->m_strFilePath = $strSimulationDirectory . 'simulation_' . $this->getId() . '/';
				} else {
					return NULL;
				}
			}

			$strFilePath = PATH_MOUNTS_GLOBAL_HELP_RESOURCES . $this->m_strFilePath;

			if( false == is_dir( $strFilePath ) && false == CFileIo::recursiveMakeDir( $strFilePath ) ) {
				trigger_error( 'Couldn\'t create directory(' . $strFilePath . ').', E_USER_ERROR );
				return false;
			}

			if( true == $boolIsFullPath ) {
				return $strFilePath;
			}

			return $this->m_strFilePath;
		}
	}

	public function getHelpTopicTitle() {
		return $this->m_strHelpTopicTitle;
	}

	public function getModuleId() {
		return $this->m_intModuleId;
	}

	public function getHelpTopicId() {
		return $this->m_intHelpTopicId;
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function getSignedFileName() {
		return $this->m_strSignedFileName;
	}

	public function getVideoUrl() {
		return $this->m_strVideoUrl;
	}

	public function getDocumentPath( $intUserId ) {
		$arrstrDocumentPath = [];
		$this->m_objHelpSystemLibrary = new CHelpSystemLibrary( $this->getDatabase() );
		if( true == valObj( $this->m_objHelpSystemLibrary, 'CHelpSystemLibrary' ) ) {
			$arrstrDocumentPath = $this->m_objHelpSystemLibrary->loadHelpResourceDocumentName( $this, $intUserId );
		}

		return $arrstrDocumentPath;
	}

	public function getPdfFilePath() {
		$arrstrPdfFilePath = [];
		$this->m_objHelpSystemLibrary = new CHelpSystemLibrary( $this->getDatabase() );
		if( true == valObj( $this->m_objHelpSystemLibrary, 'CHelpSystemLibrary' ) ) {
			$arrstrPdfFilePath = $this->m_objHelpSystemLibrary->loadPdfFilePathDetails( $this );
		}

		return $arrstrPdfFilePath;
	}

	public static function getStorageContainer() {
		if( 'production' != CONFIG_ENVIRONMENT ) {
			return self::STORAGE_CONTAINER_DEV;
		}

		return self::STORAGE_CONTAINER;
	}

	public function getHelpHelpResourceAssociationId() {
		return $this->m_intHelpHelpResourceAssociationId;
	}

	/**
	* Set Functions
	*
	*/

	public function setModuleId( $intModuleId ) {
		$this->m_intModuleId = $intModuleId;
	}

	public function setHelpResourceTypeName( $strHelpResourceTypeName ) {
		$this->m_strHelpResourceTypeName = $strHelpResourceTypeName;
	}

	public function setHelpTopicId( $intHelpTopicId ) {
		$this->m_intHelpTopicId = $intHelpTopicId;
	}

	public function setHelpHelpResourceAssociationId( $intHelpHelpResourceAssociationId ) {
		$this->m_intHelpHelpResourceAssociationId = $intHelpHelpResourceAssociationId;
	}

	public function setPsProductId( $intPsProductId ) {
		$this->m_intPsProductId = $intPsProductId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['help_resource_type_name'] ) ) 		$this->setHelpResourceTypeName( $arrmixValues['help_resource_type_name'] );
		if( true == isset( $arrmixValues['help_topic_title'] ) ) 				$this->setHelpTopicTitle( $arrmixValues['help_topic_title'] );
		if( true == isset( $arrmixValues['module_id'] ) ) 						$this->setModuleId( $arrmixValues['module_id'] );
		if( true == isset( $arrmixValues['help_topic_id'] ) ) 					$this->setHelpTopicId( $arrmixValues['help_topic_id'] );
		if( true == isset( $arrmixValues['ps_product_id'] ) ) 					$this->setPsProductId( $arrmixValues['ps_product_id'] );
		if( true == isset( $arrmixValues['help_resource_association_id'] ) ) 	$this->setHelpHelpResourceAssociationId( $arrmixValues['help_resource_association_id'] );
	}

	public function setHelpTopicTitle( $strHelpTopicTitle ) {
		$this->m_strHelpTopicTitle = CStrings::strTrimDef( $strHelpTopicTitle, -1, NULL, true );
	}

	public function sqlHelpTopicTitle() {
		return  ( true == isset( $this->m_strHelpTopicTitle ) ) ? '\'' . $this->m_strHelpTopicTitle . '\'' : 'NOW()';
	}

	public function setSignedFileName( $strSignedFileName ) {
		$this->m_strSignedFileName = $strSignedFileName;
	}

	public function setVideoUrl( $strVideoUrl ) {
		$this->m_strVideoUrl = $strVideoUrl;
	}

	/**
	* Validate Functions
	*
	*/

	public function validate( $strAction, $arrintHelpResourcesIds = NULL, $boolIsUpload = false, $objDatabase = NULL, $intSectionId = NULL, $intCid = NULL ) {

		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				break;

			case 'help_resource':
				$boolIsValid &= $this->valHelpResourceDetails( $objDatabase );
				break;

			case 'help_resource_association':
				$boolIsValid &= $this->valHelpResourceContents( $arrintHelpResourcesIds );
				break;

			case 'help_resource_article':
				if( true == $boolIsUpload ) {
					$boolIsValid &= $this->valHelpResourceDetails( $objDatabase );
					$boolIsValid &= $this->valDocumentFileExtensions( $objDatabase, 'pdf' );
					$boolIsValid &= $this->valFileSize();
				}
				break;

			case 'help_resource_admin_guide':
				$boolIsValid &= $this->valHelpResourceContents( $arrintHelpResourcesIds );
				break;

			case 'help_resource_custom_course':
				$boolIsValid &= $this->valUniqueTitle( $objDatabase );
				break;

			case 'help_resource_custom_course_section':
				$boolIsValid &= $this->valUniqueSectionTitle( $arrintHelpResourcesIds, $objDatabase );
				break;

			case 'help_resource_link_content':
				$boolIsValid &= $this->valLinkDetails();
				break;

			case 'help_resource_learning_track':
				$boolIsValid &= $this->valLearningTrackDetails();
				$boolIsValid &= $this->valUniqueTitle( $objDatabase );
				break;

			case 'help_resource_content':
				$boolIsValid &= $this->valUniqueTitleOfSameContentType( $arrintHelpResourcesIds[0], $intSectionId, $intCid, $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;

		if( false == valStr( $this->getTitle() ) || true == is_null( $this->getTitle() ) ) {
			$boolIsValid = false;

			if( CHelpResourceType:: FAQ == $this->getHelpResourceTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Question is required. ' ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Title is required. ' ) );
			}
			return $boolIsValid;
		}

		if( false == \Psi\CStringService::singleton()->preg_match( '/^[a-zA-Z0-9 .\:\;\<\>\[\]\{\}\(\)\$\#\%\*\@\~\/\,\-\_\&\'\"\!\?\/\\\]*$/', $this->getTitle() ) ) {
			$boolIsValid = false;

			if( CHelpResourceType:: FAQ == $this->getHelpResourceTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Invalid Question.' ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Invalid Title.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valUniqueTitle( $objDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->getTitle() ) && true == in_array( $this->getHelpResourceTypeId(), [ CHelpResourceType::CUSTOM_COURSE, CHelpResourceType::LEARNING_TRACK ] ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', ' Title is required.' ) );
			return false;
		}

		if( false == is_null( $this->getTitle() ) || true == valStr( $this->getTitle() ) ) {
			$arrintHelpResourceTypeIds = [ $this->getHelpResourceTypeId() ];
			if( true == in_array( $this->getHelpResourceTypeId(), [ CHelpResourceType::INTERACTIVE_COURSE, CHelpResourceType::COURSES ] ) ) {
				$arrintHelpResourceTypeIds = [ CHelpResourceType::INTERACTIVE_COURSE, CHelpResourceType::COURSES ];
			}

			$intHelpResourcesCounts = \Psi\Eos\Admin\CHelpResources::createService()->fetchHelpResourcesCountByIdByTitleByCid( $this->getId(), $this->getTitle(), $arrintHelpResourceTypeIds, $this->getCid(), $objDatabase );

			if( 1 <= $intHelpResourcesCounts ) {
				$boolIsValid = false;
				if( CHelpResourceType::FAQ == $this->getHelpResourceTypeId() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unique question', ' This Question already exists.' ) );
				} elseif( CHelpResourceType::CUSTOM_COURSE == $this->getHelpResourceTypeId() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', ' Course name already exists.' ) );
				} elseif( CHelpResourceType::LEARNING_TRACK == $this->getHelpResourceTypeId() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', ' Learning track with same name already exists.' ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unique title', ' Content with this title already exists.' ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valUniqueSectionTitle( $arrintHelpResourcesIds, $objDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->getTitle() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Section name is required.' ) );

			return false;
		}

		if( false == is_null( $this->getTitle() ) || true == valStr( $this->getTitle() ) ) {

			$intHelpResourceId   = current( $arrintHelpResourcesIds );
			$arrmixHelpResources = \Psi\Eos\Admin\CHelpResources::createService()->fetchCustomCourseSectionsByHelpResourceIdByTitleByCid( $intHelpResourceId, $this->getTitle(), $this->getHelpResourceTypeId(), $this->getCid(), $objDatabase );

			if( false == valArr( $arrmixHelpResources ) ) {
				return true;
			}

			if( valId( $this->getId() ) ) {
				if( 1 == \Psi\Libraries\UtilFunctions\count( $arrmixHelpResources ) && current( $arrmixHelpResources )->getId() != $this->getId() ) {
					$boolIsValid = false;
				}
			} else {
				if( 0 < \Psi\Libraries\UtilFunctions\count( $arrmixHelpResources ) ) {
					$boolIsValid = false;
				}
			}
		}

		if( false == $boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Section title', ' Section name already exists.' ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			if( CHelpResourceType:: FAQ == $this->getHelpResourceTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Answer is required. ' ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required. ' ) );
			}
		}

		return $boolIsValid;
	}

	public function valKeywords() {
		$boolIsValid = true;

		if( true == filter_var( $this->getKeywords(), FILTER_VALIDATE_URL ) || true == preg_match( '/^\W+$/', $this->getKeywords() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'keywords', 'Enter a relevant valid keywords.' ) );
		}

		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;

		if( true == isset( $_FILES['help_resource_file'] ) && 0 == ( $_FILES['help_resource_file']['error'] ) ) {
			$boolIsValid = true;
		} elseif( true == is_null( $this->getFileName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', 'File is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valFileSize() {
		$boolIsValid = true;

		$intDownloadFileSize = $_FILES['help_resource_file']['size'];

		if( self::FILE_MAX_SIZE < $intDownloadFileSize ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'File size exceeds 50 mb limit.' ) );
		}

		return $boolIsValid;
	}

	public function valUniqueFile( $objDatabase ) {

		$boolIsValid = true;

		$arrobjHelpResources = \Psi\Eos\Admin\CHelpResources::createService()->fetchHelpResourcesByFileNameByCid( $this->getFileName(), \Psi\Eos\Admin\CClients::$c_intNullCid, $objDatabase );

		if( valArr( $arrobjHelpResources ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', 'File with same name already exists.' ) );
		}

		return $boolIsValid;

	}

	public function valDocumentFileExtensions( $objDatabase, $strFileType = 'text' ) {

		$boolIsValid = true;

		$objFileExtension = $this->fetchFileExtension( $objDatabase );

		if( false == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$boolIsValid = false;

			if( false == is_null( $this->getFileName() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', 'Invalid file.' ) );
			}

			return $boolIsValid;
		}

		if( $strFileType == 'video' ) {
			$arrintPermittedFileExtensions = [ CFileExtension::WAV, CFileExtension::VIDEO_AVI, CFileExtension::VIDEO_FLV, CFileExtension::VIDEO_MOV,CFileExtension::AUDIO_MP3, CFileExtension::VIDEO_MP4, CFileExtension::VIDEO_MPEG, CFileExtension::VIDEO_WMV ];
		} elseif( $strFileType == 'zip' ) {
			$arrintPermittedFileExtensions = [ CFileExtension::ZIP ];
		} elseif( $strFileType == 'pdf' ) {
			$arrintPermittedFileExtensions = [ CFileExtension::APPLICATION_PDF ];
		} else {
			$arrintPermittedFileExtensions = [ CFileExtension::APPLICATION_TXT, CFileExtension::APPLICATION_XLS, CFileExtension::APPLICATION_DOC, CFileExtension::APPLICATION_PDF,CFileExtension::IMAGE_BMP, CFileExtension::IMAGE_GIF, CFileExtension::IMAGE_JPEG, CFileExtension::IMAGE_JPG, CFileExtension::IMAGE_PNG, CFileExtension::APPLICATION_DOCX ];
		}

		if( false == in_array( $objFileExtension->getId(), $arrintPermittedFileExtensions ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', 'Only PDF file can be uploaded for a article.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valHelpResourceDetails( $objDatabase ) {

		$boolIsValid = true;

			$boolIsValid &= $this->valTitle();
			$boolIsValid &= $this->valUniqueTitle( $objDatabase );
			$boolIsValid &= $this->valDescription();
			$boolIsValid &= $this->valKeywords();

		if( CHelpResourceType::VIDEO == $this->getHelpResourceTypeId() ) {
			$boolIsValid &= $this->valYouTubeUrl();
		}

		if( CHelpResourceType::INTERACTIVE_COURSE == $this->getHelpResourceTypeId() ) {
			$boolIsValid &= $this->valInteractiveCourse();
		}

		return $boolIsValid;
	}

	public function valYouTubeUrl() {

		$boolIsValid = true;
		if( false == valStr( $this->getExternalPath() ) && false == valStr( $this->getFileName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'external_path', ' Video url is required. ' ) );
		}

		if( true == valStr( $this->getExternalPath() ) && 0 == preg_match( '/^(http|https):\/\/(?:www\.)?youtube.com\/watch\?(?=[^?]*v=[\w\-]+)(?:[^\s?]+)?$/', $this->getExternalPath() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'external_path', ' Invalid video url.' ) );
		}

		return $boolIsValid;
	}

	public function valHelpResourceContents( $arrintHelpResourcesIds ) {
		$boolIsValid = true;
		if( CHelpResourceType::COURSES == $this->getHelpResourceTypeId() && false == valArr( $arrintHelpResourcesIds ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Title', 'Content is required. ' ) );
			$boolIsValid &= false;
		}
		if( CHelpResourceType::ADMIN_GUIDE == $this->getHelpResourceTypeId() && false == valArr( $arrintHelpResourcesIds ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Title', 'Content is required. ' ) );
			$boolIsValid &= false;
		}
		if( CHelpResourceType::ARTICLE == $this->getHelpResourceTypeId() && false == valArr( $arrintHelpResourcesIds ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Title', 'Video content is required. ' ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valSimulationFile( $intIndexHTMLIndex, $intIndexScormHTMLIndex ) {

		$boolIsValid = true;
		if( false == $intIndexHTMLIndex && false == $intIndexScormHTMLIndex ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'simulation', 'index.html / index_SCORM.html is missing. ' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valIsArticleReadyToPublish( $strRequestedStatus, $objDatabase ) {

		if( 'publish' == $strRequestedStatus ) {
			if( ( true == is_null( $this->getFileName() ) && true == is_null( $this->getFilePath( true, true ) ) && true == is_null( $this->getContent() ) ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Please provide content to publish this article. ', NULL ) );
				return false;
			}
			return true;
		} else {
				$boolIsAssociated = $this->checkAssociation( $objDatabase );
				return $boolIsAssociated;
		}
	}

	public function valIsVideoReadyToPublish( $strRequestedStatus, $objDatabase ) {

		if( 'publish' == $strRequestedStatus ) {
			if( true == is_null( $this->getExternalPath() ) && true == is_null( $this->getFileName() ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Please provide embedded path to publish this video.', NULL ) );
				return false;
			}
			return true;
		} else {
			$boolIsAssociated = $this->checkAssociation( $objDatabase );
			return $boolIsAssociated;
		}
	}

	public function valIsFaqReadyToPublish( $strRequestedStatus, $objDatabase ) {

		if( 'publish' == $strRequestedStatus ) {
			if( true == is_null( $this->getDescription() ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Please provide answer to publish this faq. ', NULL ) );
				return false;
			}
			return true;
		} else {
			$boolIsAssociated = $this->checkAssociation( $objDatabase );
			return $boolIsAssociated;
		}

	}

	public function valIsSimulationReadyToPublish( $strRequestedStatus, $objDatabase ) {

		if( 'publish' == $strRequestedStatus ) {
			if( true == is_null( $this->getFileName() ) || true == is_null( $this->getFilePath( true, true ) ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Please upload simulation file to publish this simulation. ', NULL ) );
				return false;
			}
			return true;
		} else {

			$boolIsAssociated = $this->checkAssociation( $objDatabase );

			return $boolIsAssociated;
		}

	}

	public function valIsCourseReadyToPublish( $strRequestedStatus, $objDatabase ) {

		if( 'publish' == $strRequestedStatus ) {

			// check course has contents added to it
			$arrobjHelpResourceAssociations = \Psi\Eos\Admin\CHelpResourceAssociations::createService()->fetchHelpResourceAssociationsByParentHelpResourceIdByCid( $this->getId(), CClient::ID_DEFAULT, $objDatabase );
			if( false == valArr( $arrobjHelpResourceAssociations ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Please provide contents to publish this course. ', NULL ) );
				return false;
			}

			// check course has quiz added to it
			$objHelpQuiz = \Psi\Eos\Admin\CHelpQuizzes::createService()->fetchHelpQuizzeByCourseHelpResourceIdByCid( $this->getId(), CClient::ID_DEFAULT, $objDatabase );
			if( valObj( $objHelpQuiz, 'CHelpQuiz' ) ) {
				$arrobjHelpQuizQuestions = \Psi\Eos\Entrata\CHelpQuizQuestions::createService()->fetchHelpQuizQuestionsByHelpQuizIdByCid( $objHelpQuiz->getId(), CClient::ID_DEFAULT, $objDatabase, true );

			// check at least two quiz questions are added
				if( !valArr( $arrobjHelpQuizQuestions ) || ( valArr( $arrobjHelpQuizQuestions ) && CHelpQuizQuestion::MINIMUM_HELP_QUIZ_QUESTIONS > \Psi\Libraries\UtilFunctions\count( $arrobjHelpQuizQuestions ) ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Minimum ' . CHelpQuizQuestion::MINIMUM_HELP_QUIZ_QUESTIONS . ' quiz questions are required to publish course. ', NULL ) );
					return false;
				}

			} else {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Quiz is required to publish this course. ', NULL ) );
				return false;
			}

			// check total content counts and total published content counts
			$arrintCounts = \Psi\Eos\Admin\CHelpResources::createService()->fetchAssociationsCountByParentHelpResourceIdByCid( $this->getId(), CClient::ID_DEFAULT, $objDatabase );
			if( $arrintCounts[0]['published_count'] != $arrintCounts[0]['content_count'] ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Please publish all course contents. ', NULL ) );
				return false;
			}
		}

		return true;
	}

	public function valIsAdminGuideReadyToPublish( $strRequestedStatus, $objDatabase ) {

		$boolIsPublish = true;
		if( 'publish' == $strRequestedStatus ) {

			// check Admin Guide has contents added to it
			$arrobjHelpResourceAssociations = \Psi\Eos\Admin\CHelpResourceAssociations::createService()->fetchHelpResourceAssociationsByParentHelpResourceIdByCid( $this->getId(), CClient::ID_DEFAULT, $objDatabase );

			if( false == valArr( $arrobjHelpResourceAssociations ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Please provide contents to publish this admin guide. ', NULL ) );
				return false;
			}

			// check if all contents are published
			foreach( $arrobjHelpResourceAssociations as $objHelpResourceAssociation ) {
				if( 0 == $objHelpResourceAssociation->getIsPublished() ) {
					$boolIsPublish = false;
					break;
				}
			}

			if( false == valStr( $this->getFileName() ) ) {
				$boolIsPublish = false;
			}

			if( false == $boolIsPublish ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Admin guide pdf need to be generated in order to publish. ', NULL ) );
				return false;
			}
		}

		return true;
	}

	public function checkAssociation( $objDatabase, $boolFromValPublish = true ) {

		$arrintAssociatedPublishResourcesCount = \Psi\Eos\Admin\CHelpResources::createService()->fetchAssociatedParentResourcesCountByHelpResourceIdByCid( $this->getId(), CClient::ID_DEFAULT, $objDatabase );
		$arrintAssociatedPublishResourcesCount 	 = $arrintAssociatedPublishResourcesCount[0]['associated_published_resources_count'];
		$strMsg = ( $arrintAssociatedPublishResourcesCount > 1 ) ?  $arrintAssociatedPublishResourcesCount . ' published courses.' :  $arrintAssociatedPublishResourcesCount . ' published course.';

		if( $arrintAssociatedPublishResourcesCount > 0 ) {
			if( true == $boolFromValPublish ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to unpublish content, as it is associated with ' . $strMsg, NULL ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'VisibleTo', ' Unable to change visibility of content as it is associated with ' . $strMsg ) );
			}
			return false;
		}
		return true;
	}

	public function valInteractiveCourse() {

		$boolIsValid = true;
		$strFileName = str_replace( '/', '', $this->getFileName() );
		if( false == valStr( $strFileName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', ' AWS Interactive Course URL is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valLinkDetails() {

		if( false == valStr( $this->getTitle() ) || true == is_null( $this->getTitle() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'URL name is required. ' ) );

			return false;
		}

		if( false == valStr( $this->getExternalPath() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'external_path', ' URL is required. ' ) );

			return false;
		}

		if( valStr( $this->getExternalPath() ) && !CValidation::checkUrl( $this->getExternalPath() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'external_path', 'Please start URL with prefix https:// or http://' ) );

			return false;
		}

		return true;
	}

	public function valLearningTrackDetails() {

		if( false == valStr( $this->getTitle() ) || true == is_null( $this->getTitle() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Learning track name is required. ' ) );

			return false;
		}

		return true;
	}

	public function valUniqueTitleOfSameContentType( $intCourseId, $intSectionId, $intCid, $objDatabase ) {

		$arrobjHelpResources = \Psi\Eos\Admin\CHelpResources::createService()->fetchHelpResourceDetailsByParentHelpResourceIdBySectionHelpResourceIdByCid( $intCourseId, $intSectionId, $intCid, $objDatabase );

		if( valArr( $arrobjHelpResources ) ) {

			foreach( $arrobjHelpResources as $objHelpResource ) {

				if( $this->getHelpResourceTypeId() == $objHelpResource->getHelpResourceTypeId() && strtolower( $this->getTitle() ) == strtolower( $objHelpResource->getTitle() ) && $this->getId() != $objHelpResource->getId() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Slide with same name already exists.' ) );
					return false;
				}
			}
		}
		return true;
	}

	/**
	* Other Functions
	*
	*/

	public function fetchFileExtension( $objDatabase ) {

		if( false == is_null( $this->getFileName() ) ) {
			$arrstrFileParts = pathinfo( $this->getFileName() );
			if( true == valArr( $arrstrFileParts ) && true == array_key_exists( 'extension', $arrstrFileParts ) && false == is_null( $arrstrFileParts['extension'] ) ) {
				return \Psi\Eos\Admin\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrFileParts['extension'], $objDatabase );
			}
		}

		return false;
	}

	public static function loadCustomCourseConstants( $arrmixTemplateParameters ) {

		$arrmixTemplateParameters['custom_course_deadlines']  = CHelpResource::createService()->getCustomCourseDeadlineOptions();
		$arrmixTemplateParameters['custom_course_recurrings']  = CHelpResource::createService()->getCustomCourseRecurringOptions();
		$arrmixTemplateParameters['custom_course_reassign_allowances']  = CHelpResource::createService()->getCustomCourseReassignAllowanceOptions();
		$arrmixTemplateParameters['custom_course_retakes']    = CHelpResource::$c_arrstrCustomCourseRetake;
		$arrmixTemplateParameters['custom_course_reminders']  = CHelpResource::createService()->getCustomCourseReminderOptions();
		return $arrmixTemplateParameters;
	}

}
?>