<?php

class CMilitaryInstallation extends CBaseMilitaryInstallation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMilitaryComponentId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intMilitaryComponentId ) || 0 > $this->m_intMilitaryComponentId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'military_component_id', __( 'military component does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( false == isset( $this->m_strName ) || 0 > $this->m_strName ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'military name does not appear valid.' ) ) );
		}

		if( true == $boolIsValid && true == isset ( $objDatabase ) ) {
			$objMilitaryInstallation	= CMilitaryInstallations::fetchMilitaryInstallationByMilitaryNameByMilitaryComponentId( $this->m_strName, $this->m_intMilitaryComponentId, $objDatabase );

			if( true == valObj( $objMilitaryInstallation, 'CMilitaryInstallation' ) && $this->m_intId != $objMilitaryInstallation->getId() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'The combination of Name and Component is already being used.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valMilitaryComponentId();
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>