<?php

class CApiVersion extends CBaseApiVersion {

	// FIXME: Remove this static declaration once we release all phases of api versioning.
	const TMP_DEFAULT_MAJOR_API_VERSION = 1;
	const TMP_DEFAULT_MINOR_API_VERSION = 2;
	const TMP_DEFAULT_MAJOR_API_VERSION_NAME = 'v1';
	const TMP_DEFAULT_MINOR_API_VERSION_NAME = 'r1';

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>