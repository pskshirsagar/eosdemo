<?php

class CCluster extends CBaseCluster {

	const RAPID 		= 1;
	const STANDARD  	= 2;
	const RVSECURE		= 4;
	const PSPAYMENTS	= 5;
	const MOBILE		= 6;
	const HA			= 8;
	const ENTRATAMATION	= 10;
	const PSSECURE	    = 11;
	const RESIDENTINSURE = 12;

	public static $c_arrintClusterIds = [
		CCluster::RAPID,
		CCluster::STANDARD,
	];

	public static $c_arrintAllClusterIds = [
		CCluster::RAPID,
		CCluster::STANDARD,
		CCluster::MOBILE,
	];

	public static $c_arrintAllClusterIdsForSqlDeployment = [
		CCluster::RAPID 	=> [ 'id' => CCluster::RAPID, 'name' => 'rapid' ],
		CCluster::STANDARD 	=> [ 'id' => CCluster::STANDARD, 'name' => 'standard' ],
		CCluster::RVSECURE 	=> [ 'id' => CCluster::RVSECURE, 'name' => 'RvSecure' ]
	];

	public static $c_arrintQueueConsumerClusterIds = [
		CCluster::RAPID,
		CCluster::STANDARD,
		CCluster::HA
	];

	public static function getClusterNamesForDeployment() {
		$arrstrClusterNames[CCluster::RAPID] 		= 'Rapid';
		$arrstrClusterNames[CCluster::STANDARD] 	= 'Standard';
		return $arrstrClusterNames;
	}

	public static function getClusterNamesForScript() {
		$arrstrClusterNames[CCluster::RAPID] 		= 'Rapid';
		$arrstrClusterNames[CCluster::STANDARD] 	= 'Standard';
		$arrstrClusterNames[CCluster::HA] 	        = 'HA';
		return $arrstrClusterNames;
	}

	public static function getClusterNamesForTaskSystem() {
		$arrstrClusterNames[CCluster::RAPID] 	= [ 'id' => CCluster::RAPID, 'name' => 'rapid' ];
		$arrstrClusterNames[CCluster::STANDARD]	= [ 'id' => CCluster::STANDARD, 'name' => 'standard' ];

		return $arrstrClusterNames;
	}

	public static function getClusterNamesForHardware() {
		$arrstrClusterNames[CCluster::RAPID] 	    = [ 'id' => CCluster::RAPID, 'name' => 'Rapid' ];
		$arrstrClusterNames[CCluster::STANDARD]	    = [ 'id' => CCluster::STANDARD, 'name' => 'Standard' ];
		$arrstrClusterNames[CCluster::RVSECURE]	    = [ 'id' => CCluster::RVSECURE, 'name' => 'RvSecure' ];
		$arrstrClusterNames[CCluster::PSPAYMENTS]	= [ 'id' => CCluster::PSPAYMENTS, 'name' => 'Payments' ];
		$arrstrClusterNames[CCluster::MOBILE]	    = [ 'id' => CCluster::MOBILE, 'name' => 'Mobile' ];
		$arrstrClusterNames[CCluster::HA]	        = [ 'id' => CCluster::HA, 'name' => 'HA' ];

		return $arrstrClusterNames;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {

		$boolIsValid = true;
		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Cluster name is required.' ) );
		}

		if( true == $boolIsValid && false == is_null( $this->getName() ) ) {

			$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
			$strWhere = 'WHERE name like \'' . trim( $this->getName() ) . '\'' . $strSqlCondition;

			$intCount = CClusters::fetchClusterCount( $strWhere, $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cluster_name', 'Cluster name already exists.' ) );
			}

		return $boolIsValid;
		}
	}

	public function valDescription() {
		$boolIsValid = true;
		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Cluster description is required.' ) );
		}
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valActivatedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valActivatedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valDescription();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valDescription();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public static function getClusterNameByClusterId( $intClusterId ) {

		switch( $intClusterId ) {
			case self::RAPID:
				return 'rapid';
				break;

			case self::STANDARD:
				return 'standard';
				break;

			case self::RVSECURE:
				return 'RvSecure';
				break;

			case self::PSPAYMENTS:
				return 'PsPayments';
				break;

			case self::HA:
				return 'HomeAutomation';
				break;

			case self::MOBILE:
				return 'mobile';
				break;

			default:
				// default case
				break;
		}
	}

	public static function loadSmartyConstants( $objSmarty ) {

		$objSmarty->assign( 'CLUSTER_RAPID',		self::RAPID );
		$objSmarty->assign( 'CLUSTER_STANDARD',		self::STANDARD );
	}

	public static function loadTemplateConstants( $arrmixTemplateParameters ) {

		$arrmixTemplateParameters['CLUSTER_RAPID']        = self::RAPID;
		$arrmixTemplateParameters['CLUSTER_STANDARD']     = self::STANDARD;
		$arrmixTemplateParameters['CLUSTER_MOBILE']       = self::MOBILE;

		return $arrmixTemplateParameters;
	}

}

?>