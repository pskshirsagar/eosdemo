<?php

class CCallPriority extends CBaseCallPriority {

	protected $m_arrstrCallPriorities;

	const NORMAL 	= 1;
	const URGENT 	= 2;
	const CRITICAL	= 3;

	const CALL_EVENT_PRIORITY_NORMAL	= 0;
	const CALL_EVENT_PRIORITY_LOW		= -1;
	const CALL_EVENT_PRIORITY_HIGH		= 1;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getCallPriorities() {
		if( true == valArr( $this->m_arrstrCallPriorities ) ) {
			return $this->m_arrstrCallPriorities;
		}

		$this->m_arrstrCallPriorities = [
			self::NORMAL   => __( 'Normal' ),
			self::URGENT   => __( 'High' ),
			self::CRITICAL => __( 'Urgent' )
		];

		return $this->m_arrstrCallPriorities;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>