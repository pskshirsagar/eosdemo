<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPaymentStatusTypes
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */

class CPaymentStatusTypes extends CBasePaymentStatusTypes {

	public static function fetchPaymentStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CPaymentStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPaymentStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CPaymentStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllPaymentStatusTypes( $objDatabase, $strOrderBy = NULL ) {
		$strSql = 'SELECT * FROM payment_status_types';

		if( false == is_null( $strOrderBy ) ) {
			$strSql .= ' ORDER BY ' . addslashes( $strOrderBy );
		}

		return self::fetchPaymentStatusTypes( $strSql, $objDatabase );
	}

	public static function fetchPaymentStatusTypesByIds( $arrintPaymentStatusTypeIds, $objDatabase ) {
		if( false == valArr( $arrintPaymentStatusTypeIds ) ) return NULL;

		$strSql = 'SELECT * FROM payment_status_types WHERE id IN (' . implode( ',', $arrintPaymentStatusTypeIds ) . ') ORDER BY name';
		return self::fetchPaymentStatusTypes( $strSql, $objDatabase );
	}

	public static function fetchPaymentStatusNamesByIds( $arrintPaymentStatusTypeIds, $objDatabase ) {

		if( false == valArr( $arrintPaymentStatusTypeIds ) ) return NULL;

		$strSql = 'SELECT
						pst.id,
						pst.name
					FROM payment_status_types pst
					WHERE pst.id IN( ' . implode( ',', $arrintPaymentStatusTypeIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function isCompletedPayment( $intPaymentStatusTypeId ) {
		switch( $intPaymentStatusTypeId ) {
			case CPaymentStatusType::CAPTURED:
				return true;
			case CPaymentStatusType::RECEIVED:
				return true;

			default:
				return false;
				break;
		}
	}

	public static function paymentStatusTypeIdToStr( $intPaymentStatusTypeId ) {
		switch( $intPaymentStatusTypeId ) {
			case CPaymentStatusType::PENDING:
				return __( 'Pending' );
			case CPaymentStatusType::RECEIVED:
				return __( 'Received' );
			case CPaymentStatusType::AUTHORIZING:
				return __( 'Authorizing' );
			case CPaymentStatusType::AUTHORIZED:
				return __( 'Authorized' );
			case CPaymentStatusType::VOIDING:
				return __( 'Voiding' );
			case CPaymentStatusType::VOIDED:
				return __( 'Voided' );
			case CPaymentStatusType::CAPTURING:
				return __( 'Capturing' );
			case CPaymentStatusType::CAPTURED:
				return __( 'Captured' );
			case CPaymentStatusType::REVERSAL_PENDING:
				return __( 'Reversal Pending' );
			case CPaymentStatusType::REVERSED:
				return __( 'Reversed' );
			case CPaymentStatusType::REVERSING:
				return __( 'Reversing' );
			case CPaymentStatusType::DECLINED:
				return __( 'Declined' );
			case CPaymentStatusType::FAILED:
				return __( 'Failed' );
			case CPaymentStatusType::RECALL:
				return __( 'Returned' );
			case CPaymentStatusType::CHARGE_BACK_PENDING:
				return __( 'Pending Charge Back' );
			case CPaymentStatusType::CHARGED_BACK:
				return __( 'Charged Back' );
			case CPaymentStatusType::DELETED:
				return __( 'Deleted' );
			case CPaymentStatusType::PHONE_CAPTURE_PENDING:
				return __( 'Pending Phone Capture' );
			case CPaymentStatusType::CHARGE_BACK_FAILED:
				return __( 'Charge Back Failed' );
			case CPaymentStatusType::REVERSAL_CANCELLED:
				return __( 'Reversal Cancelled' );
			case CPaymentStatusType::REVERSAL_RETURN:
				return __( 'Reversal Return' );
			case CPaymentStatusType::CANCELLED:
				return __( 'Cancelled' );
			case CPaymentStatusType::ADMIN_CANCELLED:
				return __( 'Admin Cancelled' );
			case CPaymentStatusType::RETURN_REPRESENTED:
				return __( 'Return Represented' );
			case CPaymentStatusType::TRANSFERRED:
				return __( 'Transferred' );
			case CPaymentStatusType::BATCHING:
				return __( 'Batching' );
			case CPaymentStatusType::PENDING_3DSECURE:
				return __( 'Pending 3D Secure Authorization' );
			case CPaymentStatusType::CANCELLING:
				return __( 'Cancelling' );
			default:
				trigger_error( $intPaymentStatusTypeId . 'is not a valid payment status type id.', E_USER_WARNING );
				break;
		}
	}

}
?>