<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSubsidyIncomeLimits
 * Do not add any new functions to this class.
 */

class CSubsidyIncomeLimits extends CBaseSubsidyIncomeLimits {

	public static function fetchIncomeLimitByIncomeByFamilySizeByVersionIdByHmfaCodeByConsiderHera( $intTotalIncome, $intFamilySize, $intVersionId, $strHmfaCode, $objDatabase, $boolConsiderHera = false, $intSubsidyTypeId = \CSubsidyType::HUD ) {
		if( false == valId( $intFamilySize ) || false == valId( $intVersionId ) || false == valStr( $strHmfaCode ) || false == is_bool( $boolConsiderHera ) ) return NULL;

		$strSql = '
			SELECT
				sil.*
			FROM
				subsidy_income_limits AS sil
				JOIN subsidy_income_limit_areas AS sila ON (
					sila.subsidy_income_limit_version_id = ' . ( int ) $intVersionId . '
					AND sila.hmfa_code = \'' . $strHmfaCode . '\' )
			WHERE
				sil.subsidy_income_limit_area_id = sila.id
				AND sil.family_size = ' . ( int ) $intFamilySize . '
				AND sil.subsidy_type_id = ' . ( int ) $intSubsidyTypeId . '
				AND sil.income_limit >= ' . ( int ) $intTotalIncome;

		if( true == $boolConsiderHera ) {
			$strSql .= '
				AND (
					( sila.is_hera = FALSE )
					OR ( sila.is_hera = TRUE AND sil.is_hera = TRUE )
				)';
		} else {
			$strSql .= '
				AND sil.is_hera = FALSE';
		}

		$strSql .= '
			ORDER By
				sil.income_limit ASC
			LIMIT 1';

		return self::fetchSubsidyIncomeLimit( $strSql, $objDatabase );
	}

	public static function fetchSubsidyIncomeLimitBySubsidyIncomeLevelTypeIdByHmfaCodeByFamilySize( $intSubsidyIncomeLevelTypeId, $intClientId, $intPropertyId, $intFamilySize, $objDatabase ) {

		if( false == valId( $intSubsidyIncomeLevelTypeId ) || false == valId( $intClientId ) || false == valId( $intPropertyId ) || false == valId( $intFamilySize ) ) return;

		$strSql = 'SELECT
							sil.income_limit
						FROM
							subsidy_income_limits sil
							JOIN subsidy_income_limit_areas sila ON ( sil.subsidy_income_limit_area_id = sila.id )
							JOIN property_subsidy_details psd ON ( sila.hmfa_code = psd.hmfa_code )
							JOIN subsidy_income_limit_versions silv ON ( sil.subsidy_income_limit_version_id = silv.id )
						WHERE
							sil.subsidy_income_level_type_id =' . ( int ) $intSubsidyIncomeLevelTypeId . '
							AND psd.cid =' . ( int ) $intClientId . '
							AND psd.property_id =' . ( int ) $intPropertyId . '
							AND sil.family_size =' . ( int ) $intFamilySize . '
							AND silv.effective_date <= NOW()
							AND silv.is_published = TRUE
						ORDER BY silv.effective_date DESC
						LIMIT 1';

		return parent::fetchColumn( $strSql, 'income_limit', $objDatabase );
	}

	public static function fetchSubsidyIncomeLimitByPropertyIdByCidBySubsidyIncomeLevelPercentByFamilySizeByEffectiveDate( $intPropertyId, $intCid, $fltSubsidyIncomeLevelPercent, $intFamilySize, $strEffectiveDate, $objDatabase ) {

		if( false == valId( $intPropertyId ) || 0 >= $fltSubsidyIncomeLevelPercent || false == valId( $intFamilySize ) || false == valId( $intCid ) || false == valstr( $strEffectiveDate ) ) return false;

		$strSql = '	SELECT
					    sil_subq.subsidy_income_limit
					FROM
					    (
					      SELECT
					          sil.id,
					          subq.property_subsidy_type_ids,
					          CASE
					            WHEN ( ( NOT ( \'' . CSubsidyType::HUD . '\' = ANY ( subq.property_subsidy_type_ids ) ) ) AND silt.percent = ' . ( float ) $fltSubsidyIncomeLevelPercent . ' ) THEN sil.income_limit
					            WHEN ( ( NOT ( \'' . CSubsidyType::HUD . '\' = ANY ( subq.property_subsidy_type_ids ) ) ) AND ' . ( float ) $fltSubsidyIncomeLevelPercent . ' NOT IN ( ' . CSubsidyIncomeLevelType::INCOME_LIMIT_VERY_LOW . ', ' . CSubsidyIncomeLevelType::INCOME_LIMIT_SIXTY_PERCENT . ' ) AND silt.percent = ' . CSubsidyIncomeLevelType::INCOME_LIMIT_VERY_LOW . ' ) THEN sil.income_limit / ' . CSubsidyIncomeLevelType::INCOME_LIMIT_VERY_LOW . ' * ' . ( float ) $fltSubsidyIncomeLevelPercent . '
					            WHEN ( ( \'' . CSubsidyType::HUD . '\' = ANY ( subq.property_subsidy_type_ids ) AND sil.subsidy_type_id = ' . CSubsidyType::HUD . ' AND silt.percent = ' . ( float ) $fltSubsidyIncomeLevelPercent . ' ) ) THEN sil.income_limit
					          END subsidy_income_limit
					      FROM
					          subsidy_income_limit_versions silv
					          JOIN 
					          (
					            SELECT
					                pst.cid,
					                pst.property_id,
					                CASE
					                  WHEN count( pst.id ) > 1 OR ( count( pst.id ) = 1 AND max ( silv.cid ) = ' . CClient::ID_DEFAULT . ' ) THEN TRUE
					                  ELSE FALSE
					                END AS is_federal,
					                array_agg ( pst.subsidy_type_id ) AS property_subsidy_type_ids
					            FROM
					                property_subsidy_types pst
					                JOIN subsidy_income_limit_versions silv ON ( pst.subsidy_income_limit_version_id = silv.id )
					            WHERE
					                pst.cid = ' . ( int ) $intCid . '
					                AND pst.property_id = ' . ( int ) $intPropertyId . '
					                AND pst.deleted_by IS NULL
					            GROUP BY
					                pst.property_id,
					                pst.cid
					          ) AS subq ON ( CASE
					                           WHEN subq.is_federal = TRUE THEN silv.cid = ' . CClient::ID_DEFAULT . '
					                           ELSE silv.cid = subq.cid AND silv.property_id = subq.property_id
					                         END )
					          JOIN property_subsidy_details psd ON ( psd.property_id = subq.property_id AND psd.cid = subq.cid )
					          JOIN subsidy_income_limit_areas sila ON ( sila.hmfa_code = psd.hmfa_code AND silv.id = sila.subsidy_income_limit_version_id )
					          JOIN subsidy_income_limits sil ON ( sil.subsidy_income_limit_area_id = sila.id AND sil.subsidy_income_limit_version_id = silv.id )
					          JOIN subsidy_income_level_types silt ON ( sil.subsidy_income_level_type_id = silt.id )
					      WHERE
					          \'' . $strEffectiveDate . '\' BETWEEN silv.effective_date
					          AND silv.effective_through_date
					          AND ( ( \'' . CSubsidyType::HUD . '\' = ANY ( subq.property_subsidy_type_ids )
					          AND sil.subsidy_type_id = ' . CSubsidyType::HUD . ' )
					          OR ( NOT ( \'' . CSubsidyType::HUD . '\' = ANY ( subq.property_subsidy_type_ids ) )
					          AND sil.subsidy_type_id = ' . CSubsidyType::TAX_CREDIT . ' ) )
					          AND ( ( psd.tax_credit_use_hera_limits IS TRUE
					          AND sil.is_hera IS TRUE )
					          OR ( ( psd.tax_credit_use_hera_limits IS FALSE
					          AND sil.is_hera IS FALSE ) ) )
					          AND sil.family_size = ' . ( int ) $intFamilySize . '
					    )sil_subq
					WHERE
					    sil_subq.subsidy_income_limit IS NOT NULL';

		return parent::fetchColumn( $strSql, 'subsidy_income_limit', $objDatabase );
	}

	public static function fetchSubsidyIncomeLimitsBySubsidyIncomeLimitVersionIdBySubsidyLimitAreaIdBySubsidyIncomeLevelTypeIdsByPropertyIdByCid( $intSubsidyIncomeLimitVersionId, $intSubsidyLimitAreaId, $arrintSubsidyIncomeLevelTypeIds, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						sil.*
					FROM
						subsidy_income_limits sil
		                JOIN subsidy_income_limit_versions silv ON ( silv.id = sil.subsidy_income_limit_version_id )
					WHERE
						sil.cid = ' . ( int ) $intCid . '
						AND silv.effective_through_date > NOW()
						AND sil.property_id = ' . ( int ) $intPropertyId . '
						AND sil.subsidy_income_limit_version_id = ' . ( int ) $intSubsidyIncomeLimitVersionId . '
						AND sil.subsidy_income_limit_area_id = ' . ( int ) $intSubsidyLimitAreaId . '
						AND sil.subsidy_income_level_type_id IN ( ' . implode( ',', $arrintSubsidyIncomeLevelTypeIds ) . ' )
						AND silv.is_published
					ORDER BY
						sil.family_size';

		return self::fetchSubsidyIncomeLimits( $strSql, $objDatabase );
	}

	public static function fetchSubsidyIncomeLimitsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) return false;

		$strSql = 'SELECT
						sil.*
					FROM
						subsidy_income_limits sil
						JOIN property_subsidy_details psd ON ( sil.cid = psd.cid AND sil.property_id = psd.property_id )
						JOIN subsidy_income_limit_versions silv ON ( sil.cid = silv.cid AND sil.property_id = silv.property_id AND sil.subsidy_income_limit_version_id = silv.id )
						JOIN subsidy_income_limit_areas sila ON ( psd.hmfa_code = sila.hmfa_code AND sil.subsidy_income_limit_version_id = sila.subsidy_income_limit_version_id AND sil.subsidy_income_limit_area_id = sila.id )
					WHERE
						psd.cid = ' . ( int ) $intCid . '
						AND psd.property_id = ' . ( int ) $intPropertyId . '
						AND CURRENT_DATE BETWEEN silv.effective_date AND silv.effective_through_date
						AND silv.is_published
					ORDER BY
						silv.effective_date DESC, silv.id DESC, sil.family_size';

		return self::fetchSubsidyIncomeLimits( $strSql, $objDatabase );
	}

}
?>
