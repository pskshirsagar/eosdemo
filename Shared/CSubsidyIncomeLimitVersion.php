<?php

class CSubsidyIncomeLimitVersion extends CBaseSubsidyIncomeLimitVersion {

	const INCOME_LIMIT_VERSION_2017 = 13;
	const INCOME_LIMIT_VERSION_2018 = 19;

	const CUSTOM_INCOME_LIMIT = 'custom';
	const FEDERAL_INCOME_LIMIT = 'federal';

	private $m_strHmfaCode;

	public static $c_arrintHomeIncomeLimitVersions = [
		self::INCOME_LIMIT_VERSION_2018,
		self::INCOME_LIMIT_VERSION_2017
	];

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );

		if( true == isset( $arrmixValues['hmfa_code'] ) )    $this->setHmfaCode( trim( $arrmixValues['hmfa_code'] ) );
		return;
	}

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valYearPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setHmfaCode( $strHmfaCode ) {
		$this->m_strHmfaCode = $strHmfaCode;
	}

	public function getHmfaCode() {
		return $this->m_strHmfaCode;
	}

}
?>