<?php

class CPhoneNumberCountryCode extends CBasePhoneNumberCountryCode {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsoAlpha2Code() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCountry() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDialCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasI18nSupport() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>