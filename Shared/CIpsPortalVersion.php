<?php

class CIpsPortalVersion extends CBaseIpsPortalVersion {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIpsPortalId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVersionName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
