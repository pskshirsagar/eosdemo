<?php

class CMilitaryRate extends CBaseMilitaryRate {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMilitaryHousingAreaId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMilitaryPayGradeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsWithDependents() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '
					VALUES ( ' .
			$strId . ', ' .
			$this->sqlMilitaryHousingAreaId() . ', ' .
			$this->sqlMilitaryPayGradeId() . ', ' .
			$this->sqlYear() . ', ' .
			$this->sqlIsWithDependents() . ', ' .
			$this->sqlRate() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = array();
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' military_housing_area_id = ' . $this->sqlMilitaryHousingAreaId() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlMilitaryHousingAreaId() ) != $this->getOriginalValueByFieldName( 'military_housing_area_id' ) ) {
			$arrstrOriginalValueChanges['military_housing_area_id'] = $this->sqlMilitaryHousingAreaId();
			$strSql .= ' military_housing_area_id = ' . $this->sqlMilitaryHousingAreaId() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' military_pay_grade_id = ' . $this->sqlMilitaryPayGradeId() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlMilitaryPayGradeId() ) != $this->getOriginalValueByFieldName( 'military_pay_grade_id' ) ) {
			$arrstrOriginalValueChanges['military_pay_grade_id'] = $this->sqlMilitaryPayGradeId();
			$strSql .= ' military_pay_grade_id = ' . $this->sqlMilitaryPayGradeId() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' year = ' . $this->sqlYear() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlYear() ) != $this->getOriginalValueByFieldName( 'year' ) ) {
			$arrstrOriginalValueChanges['year'] = $this->sqlYear();
			$strSql .= ' year = ' . $this->sqlYear() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' is_with_dependents = ' . $this->sqlIsWithDependents() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlIsWithDependents() ) != $this->getOriginalValueByFieldName( 'is_with_dependents' ) ) {
			$arrstrOriginalValueChanges['is_with_dependents'] = $this->sqlIsWithDependents();
			$strSql .= ' is_with_dependents = ' . $this->sqlIsWithDependents() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' rate = ' . $this->sqlRate() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlRate() ) != $this->getOriginalValueByFieldName( 'rate' ) ) {
			$arrstrOriginalValueChanges['rate'] = $this->sqlRate();
			$strSql .= ' rate = ' . $this->sqlRate() . ',';
			$boolUpdate = true;
		}

		$strSql = rtrim( $strSql, ',' );
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

}
?>