<?php

class CSubsidyIncomeLimit extends CBaseSubsidyIncomeLimit {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyIncomeLimitVersionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyIncomeLimitAreaId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyIncomeLevelTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFamilySize() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIncomeLimit() {
		$boolIsValid = true;
		if( true == is_null( $this->getIncomeLimit() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'income_limit', 'Tax credit income limits are required.' ) );
		}

		if( $this->getIncomeLimit() === 0 ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'income_limit', 'Tax credit income limits must be greater than zero.' ) );
		}

		if( 0 > $this->getIncomeLimit() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'income_limit', 'Tax credit income limits must be positive.' ) );
		}
		return $boolIsValid;
	}

	public function valIsHera() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			$boolIsValid &= $this->valIncomeLimit();
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>