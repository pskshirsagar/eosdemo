<?php

class CMilitaryPayGrade extends CBaseMilitaryPayGrade {

	const E01 = 1;
	const E02 = 2;
	const E03 = 3;
	const E04 = 4;
	const E05 = 5;
	const E06 = 6;
	const E07 = 7;
	const E08 = 8;
	const E09 = 9;
	const W01 = 10;
	const W02 = 11;
	const W03 = 12;
	const W04 = 13;
	const W05 = 14;
	const O01E = 15;
	const O02E = 16;
	const O03E = 17;
	const O01 = 18;
	const O02 = 19;
	const O03 = 20;
	const O04 = 21;
	const O05 = 22;
	const O06 = 23;
	const O07 = 24;
	const O08 = 25;
	const O09 = 26;
	const O10 = 27;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPayGradeMacId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>