<?php

class CPropertySettingKeyValue extends CBasePropertySettingKeyValue {

	protected $m_strPropertySettingKeyValueTypeName;

	public function getPropertySettingKeyValueTypeName() {
		return $this->m_strPropertySettingKeyValueTypeName;
	}

	public function setPropertySettingKeyValueTypeName( $strPropertySettingKeyValueTypeName ) {
		$this->m_strPropertySettingKeyValueTypeName = $strPropertySettingKeyValueTypeName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_setting_key_value_type_name'] ) ) $this->setPropertySettingKeyValueTypeName( $arrmixValues['property_setting_key_value_type_name'] );
	}

	public function valPropertySettingKeyId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intPropertySettingKeyId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_setting_key_id', 'Property Setting Key Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertySettingKeyValueTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_intPropertySettingKeyValueTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_setting_key_value_type_id', 'Property Setting Key Value Type Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTableName() {
		$boolIsValid = true;

		if( false == is_null( $this->m_strTableName ) && CPropertySettingKeyValueType::LOOKUP != $this->m_intPropertySettingKeyValueTypeId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'table_name', 'Table Name can only be set when value type is Lookup.' ) );
		}

		if( true == is_null( $this->m_strTableName ) && CPropertySettingKeyValueType::LOOKUP == $this->m_intPropertySettingKeyValueTypeId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'table_name', 'Table Name is required when value type is Lookup.' ) );
		}

		return $boolIsValid;
	}

	public function valColumnName() {
		$boolIsValid = true;

		if( false == is_null( $this->getColumnName() ) && CPropertySettingKeyValueType::LOOKUP != $this->getPropertySettingKeyValueTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'column_name', 'Column Name can only be set when value type is Lookup.' ) );
		}

		if( true == is_null( $this->getColumnName() ) && CPropertySettingKeyValueType::LOOKUP == $this->getPropertySettingKeyValueTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'column_name', 'Column Name is required when value type is Lookup.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertySettingValue() {
		$boolIsValid = true;

		if( false == is_null( $this->m_strPropertySettingValue ) && CPropertySettingKeyValueType::CUSTOM_DROPDOWN != $this->m_intPropertySettingKeyValueTypeId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_setting_value', 'Property Setting Value can only be set when value type is Custom.' ) );
		}

		return $boolIsValid;
	}

	public function valDisplayText() {
		$boolIsValid = true;

		if( false == is_null( $this->m_strDisplayText ) && CPropertySettingKeyValueType::CUSTOM_DROPDOWN != $this->m_intPropertySettingKeyValueTypeId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'display_text', 'Display Text can only be set when value type is Custom.' ) );
		}

		if( true == is_null( $this->m_strDisplayText ) && CPropertySettingKeyValueType::CUSTOM_DROPDOWN == $this->m_intPropertySettingKeyValueTypeId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'display_text', 'Display Text is required when value type is Custom.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPropertySettingKeyId();
				$boolIsValid &= $this->valPropertySettingKeyValueTypeId();
				$boolIsValid &= $this->valTableName();
				$boolIsValid &= $this->valColumnName();
				$boolIsValid &= $this->valPropertySettingValue();
				$boolIsValid &= $this->valDisplayText();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getDisplayTextByTypeByValue( $intPropertySettingKeyValueTypeId, $strPropertySettingValue, $strTableName = NULL, $strColumnName = NULL, $objDatabase = NULL, $intCid = NULL, $strPropertySettingKey = NULL, $objCurrentDatabase = NULL ) {
		switch( $intPropertySettingKeyValueTypeId ) {
			case CPropertySettingKeyValueType::YES_NO:
				$strDisplayText = ( true == $strPropertySettingValue ? 'Yes' : 'No' );
				break;

			case CPropertySettingKeyValueType::YES_NO_INVERSE:
				$strDisplayText = ( true == $strPropertySettingValue ? 'No' : 'Yes' );
				break;

			case CPropertySettingKeyValueType::SHOW_HIDE:
				$strDisplayText = ( true == $strPropertySettingValue ? 'Show' : 'Hide' );
				break;

			case CPropertySettingKeyValueType::SHOW_HIDE_INVERSE:
				$strDisplayText = ( true == $strPropertySettingValue ? 'Hide' : 'Show' );
				break;

			case CPropertySettingKeyValueType::ON_OFF:
				$strDisplayText = ( true == $strPropertySettingValue ? 'On' : 'Off' );
				break;

			case CPropertySettingKeyValueType::ON_OFF_INVERSE:
				$strDisplayText = ( true == $strPropertySettingValue ? 'Off' : 'On' );
				break;

			case CPropertySettingKeyValueType::LOOKUP:
				$strDisplayText = $strPropertySettingValue;

				if( true == isset( $strTableName, $strColumnName, $objDatabase ) && false == empty( $strPropertySettingValue ) ) {

					$strFieldSql	= 'SELECT column_name FROM information_schema.table_constraints tc JOIN information_schema.key_column_usage kcu ON ( tc.constraint_name = kcu.constraint_name AND tc.table_name = kcu.table_name ) WHERE tc.table_name = \'' . $strTableName . '\' AND tc.constraint_type = \'PRIMARY KEY\';';
					$arrstrColumnName = fetchData( $strFieldSql, $objDatabase );

					if( true == valArr( $arrstrColumnName ) ) {
						$arrstrColumnName = array_keys( rekeyArray( 'column_name', $arrstrColumnName ) );

						$strSql = 'SELECT ' . $strColumnName . ' FROM ' . $strTableName . ' WHERE \'' . ( string ) $arrstrColumnName[0] . '\'=\'' . ( string ) $strPropertySettingValue . '\'';

						if( false == is_null( $intCid ) && true == in_array( $strTableName, self::fetchCidTables( $objDatabase ) ) ) {
							$strSql .= ' AND cid = ' . ( int ) $intCid;
						}

						$arrmixData = fetchData( $strSql, $objDatabase );

						if( true == valArr( $arrmixData ) ) {
							$strDisplayText = reset( $arrmixData[0] );
						}
					}
				}
				break;

			case CPropertySettingKeyValueType::CUSTOM_DROPDOWN:
				$strDisplayText = $strPropertySettingValue;

				if( false == empty( $objCurrentDatabase ) ) {
					$objDatabase = $objCurrentDatabase;
				} else {
					$objDatabase = $objDatabase;
				}

				if( true == valStr( $strPropertySettingKey ) ) {
					$strWhereClause = ' AND pskv.property_setting_value = \'' . $strPropertySettingValue . '\'::TEXT';

					if( false == valStr( $strPropertySettingValue ) ) {
						$strWhereClause = ' AND pskv.property_setting_value IS NULL';
					}

					$strSql = ' SELECT
									pskv.display_text
								FROM
									property_setting_key_values pskv
									JOIN property_setting_keys psk ON ( psk.id = pskv.property_setting_key_id )
								WHERE
									psk.key = \'' . $strPropertySettingKey . '\'
									AND pskv.property_setting_key_value_type_id = ' . CPropertySettingKeyValueType::CUSTOM_DROPDOWN . $strWhereClause;

					$arrmixData = fetchData( $strSql, $objDatabase );

					if( true == valArr( $arrmixData ) ) {
						$strDisplayText = $arrmixData[0]['display_text'];
					}
				}
				break;

			default:
				$strDisplayText = $strPropertySettingValue;
				break;
		}

		return $strDisplayText;

	}

	private static function fetchCidTables( $objDatabase ) {
		$arrstrCidTables = CCache::fetchObject( 'cid_tables' );

		if( false === $arrstrCidTables ) {
			$strFetchSql = "SELECT table_name FROM information_schema.columns WHERE column_name = 'cid' AND table_schema = 'public' ORDER BY table_name";
			$arrmixData = fetchData( $strFetchSql, $objDatabase );
			$arrstrCidTables = extractUniqueFieldValues( 'table_name', $arrmixData );
			CCache::storeObject( 'cid_tables', $arrstrCidTables );
		}

		return $arrstrCidTables;
	}

	public static function deletePropertySettingKeyValueByPropertySettingKeyId( $intPropertySettingKeyId, $objDatabase, $boolIsReturnSqlOnly = false ) {
		$strSql = 'DELETE FROM property_setting_key_values WHERE property_setting_key_id = ' . ( int ) $intPropertySettingKeyId . ';';

		if( true == $boolIsReturnSqlOnly ) {
			return $strSql;
		}

		$objDataset = $objDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return false;
		} else {
			$objDataset->cleanup();
			return true;
		}
	}

}
?>