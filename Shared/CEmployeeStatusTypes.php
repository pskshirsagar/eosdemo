<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeStatusTypes
 * Do not add any new functions to this class.
 */

class CEmployeeStatusTypes extends CBaseEmployeeStatusTypes {

	public static function fetchEmployeeStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEmployeeStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchEmployeeStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEmployeeStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllEmployeeStatusTypes( $objDatabase ) {
		return self::fetchEmployeeStatusTypes( 'SELECT * FROM employee_status_types WHERE id != ' . CEmployeeStatusType::NO_SHOW . ' ', $objDatabase );
	}

	public static function fetchEmployeeStatusTypesByIds( $arrintStatusTypeIds, $objDatabase ) {
		if( false == valArr( $arrintStatusTypeIds ) ) return NULL;

		$strSql = 'SELECT *	FROM
						employee_status_types
					WHERE
						id IN ( ' . implode( ',', $arrintStatusTypeIds ) . ' );';

		return self::fetchEmployeeStatusTypes( $strSql, $objDatabase );
	}

}
?>