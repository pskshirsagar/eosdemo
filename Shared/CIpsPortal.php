<?php

use Psi\Eos\Admin\CIpsPortals;

class CIpsPortal extends CBaseIpsPortal {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objAdminDatabase ) {
		$boolIsValid = false;

		if( false == valStr( $this->m_strName ) || true == preg_match( '/[\\\\@#!%&*+:?;,.\/]/', $this->m_strName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Please enter valid portal title.' ) );
			return $boolIsValid;
		}

		if( 2 >= \Psi\CStringService::singleton()->strlen( $this->m_strName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', ' Portal title should contain atleast 3 letters.' ) );
			return $boolIsValid;
		}

		if( true == valObj( $objAdminDatabase, 'CDatabase' ) ) {
			if( true == valId( $this->getId() ) ) {
				$objIpsPortal = CIpsPortals::createService()->fetchIpsPortalById( $this->getId(), $objAdminDatabase );
				if( false == valObj( $objIpsPortal, 'CIpsPortal' ) ) {
					return $boolIsValid;
				} elseif( \Psi\CStringService::singleton()->strtolower( $this->m_strName ) == \Psi\CStringService::singleton()->strtolower( $objIpsPortal->getName() ) ) {
					return true;
				}
			}

			$boolIsValid = $this->checkIsPotalNameAlreadyExist( $this->m_strName, $objAdminDatabase );
		}

		return $boolIsValid;
	}

	public function valOccupancyTypeIds( $objAdminDatabase ) {
		$boolIsValid = false;

		if( false == valArr( $this->m_arrintOccupancyTypeIds ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'occupancy_type_ids', ' Please select occupancy type(s).' ) );
			return $boolIsValid;
		}

		$boolIsValid = $this->checkIsPortalOccupancyTypesAlreadyExist( $this->m_arrintOccupancyTypeIds, $objAdminDatabase );

		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $objAdminDatabase );
				$boolIsValid &= $this->valOccupancyTypeIds( $objAdminDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objAdminDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function checkIsPotalNameAlreadyExist( $strName, $objAdminDatabase ) : bool {
		$boolIsValid = true;
		$intPortalNameCount = CIpsPortals::createService()->fetchIpsPortalCountByTitle( $strName, $objAdminDatabase );

		if( 0 < $intPortalNameCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Portal title already exists.' ) );
		}
		return $boolIsValid;
	}

	public function checkIsPortalOccupancyTypesAlreadyExist( $arrintPortalOccupancyTypeIds, $objAdminDatabase ) : bool {
		$boolIsValid = true;
		$intOccupancyTypeIdsCount = CIpsPortals::createService()->fetchIpsPortalCountByOccupancyTypeIds( $arrintPortalOccupancyTypeIds, $objAdminDatabase );

		if( 0 < $intOccupancyTypeIdsCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', ' A Portal with the same occupancy type(s) already exists. Please select a different type set or edit the existing portal.' ) );
		}
		return $boolIsValid;
	}

}
?>
