<?php

class 	CMessageType extends CBaseMessageType {

	private $m_arrintSubscribedListMessageTypes;

	const SCHEDULED_PAYMENT 			= 1;
	const RENT_REMINDER 				= 2;
	const PROPERTY_NOTIFICATION 		= 3;
	const INFORMATION_REQUEST 			= 4;
	const AVAILABILITY_ALERT 			= 5;
	const MAINTENANCE_NOTIFICATION		= 6;
	const HELP							= 7;
	const STOP							= 8;
	const MENU							= 9;
	const UNRECOGNIZED					= 10;
	const PROPERTY						= 11;
	const STOP_ALL						= 12;
	const CANCEL						= 13;
	const CANCEL_ALL					= 14;
 	const END							= 15;
 	const END_ALL						= 16;
 	const UNSUBSCRIBE					= 17;
	const UNSUBSCRIBE_ALL				= 18;
	const QUIT							= 19;
	const QUIT_ALL						= 20;
	const ADMIN							= 21;
	const POLL							= 22;
	const RESERVED 						= 23;
	const MONITOR						= 24;
	const COMMUNITY_ANNOUNCEMENTS		= 25;
	const LEAD_COMMUNICATION			= 26;
	const PACKAGE_NOTIFICATION			= 27;
	const SMS_VERIFICATION				= 28;
	const APPLICATION_UPDATES			= 29;
	const RESIDENT_COMMUNICATION		= 30;
	const ONE_TIME_PASSWORD				= 31;
	const TICKET_NOTIFICATION			= 33;
	const TASK_NOTIFICATION				= 34;
	const SMS_CHAT						= 35;
	const HA_WATER_LEAKAGE				= 36;
	const HA_GUEST_DOOR_LOCK			= 37;
	const PAPERLESS                     = 38;
	const CONTACT_POINT                 = 39;
	const EMERGENCY                     = 40;
	const MARKETING_SMS_OPTIN_CONFIRMATION			= 41;
	const JOIN_ALL                      = 42;

	const HANDLE_SCHEDULED_PAYMENT 			= 'PAY';
	const HANDLE_RENT_REMINDER 				= 'RENT';
	const HANDLE_PROPERTY_NOTIFICATION 		= 'NOTE';
	const HANDLE_INFORMATION_REQUEST 		= 'INFO';
	const HANDLE_AVAILABILITY_ALERT 		= 'ALERT';
	const HANDLE_MAINTENANCE_NOTIFICATION	= 'FIX';
	const HANDLE_HELP						= 'HELP';
	const HANDLE_STOP						= 'STOP';
	const HANDLE_MENU						= 'MENU';
	const HANDLE_PROPERTY					= 'PROPERTY';
	const HANDLE_STOP_ALL					= 'STOP ALL';
	const HANDLE_STOPALL					= 'STOPALL';
	const HANDLE_CANCEL						= 'CANCEL';
	const HANDLE_CANCEL_ALL					= 'CANCEL ALL';
 	const HANDLE_END						= 'END';
 	const HANDLE_END_ALL					= 'END ALL';
 	const HANDLE_UNSUBSCRIBE				= 'UNSUBSCRIBE';
	const HANDLE_UNSUBSCRIBE_ALL			= 'UNSUBSCRIBE ALL';
	const HANDLE_QUIT						= 'QUIT';
	const HANDLE_QUIT_ALL					= 'QUIT ALL';
	const HANDLE_MONITOR					= 'MONITOR';
	const HANDLE_COMMUNITY_ANNOUNCEMENTS	= 'COMMUNITY ANNOUNCEMENTS';
	const HANDLE_LEAD_COMMUNICATION			= 'LEAD COMMUNICATION';
	const HANDLE_PACKAGE_NOTIFICATION 		= 'PARCEL';
	const HANDLE_APPLICATION_UPDATES 		= 'APPUPDATES';
	const HANDLE_RESIDENT_COMMUNICATION		= 'RESIDENT COMMUNICATION';
	const HANDLE_ONE_TIME_PASSWORD			= 'ONE TIME PASSWORD';
	const HANDLE_OPTIN_CONFIRMATION			= 'Y';

	const HANDLE_APPLICATION_UPDATE_SMS_OPTIN_CONFIRMATION  = 'JOIN APPUPDATES';
	const HANDLE_AVAILABILITY_ALERT_SMS_OPTIN_CONFIRMATION  = 'JOIN ALERT';
	const HANDLE_COMMUNITY_SMS_OPTIN_CONFIRMATION           = 'JOIN COMMUNITY';
	const HANDLE_MAINTENANCE_SMS_OPTIN_CONFIRMATION         = 'JOIN FIX';
	const HANDLE_RENT_REMINDER_SMS_OPTIN_CONFIRMATION       = 'JOIN RENT';
	const HANDLE_PARCEL_ALERT_SMS_OPTIN_CONFIRMATION        = 'JOIN PARCEL';
	const HANDLE_JOIN_ALL_SMS_OPTIN_CONFIRMATION            = 'JOIN ALL';

	const HANDLE_REOPEN_TICKET		= 'REOPENTICKET';
	const HANDLE_STOP_TICKET		= 'STOPTICKET';
	const HANDLE_STOP_ALL_TICKET	= 'STOPALLTICKET';

	const HANDLE_ACK_TICKET			= '1';
	const HANDLE_RESOLVE_TICKET		= '2';
	const HANDLE_ESCALATE_TICKET	= '3';
	const HANDLE_DETAIL_TICKET      = '4';

	const HANDLE_PAPERLESS                  = 'PAPERLESS';

	public static $c_arrintContactPointMessageTypeIds = [
		self::INFORMATION_REQUEST
	];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getSubscribedListMessageTypes() {
		if( isset( $this->m_arrintSubscribedListMessageTypes ) ) {
			return $this->m_arrintSubscribedListMessageTypes;
		}

		$this->m_arrintSubscribedListMessageTypes = [
			self::LEAD_COMMUNICATION        => __( 'Lead Communication' ),
			self::COMMUNITY_ANNOUNCEMENTS   => __( 'Resident Communication' )
		];

		return $this->m_arrintSubscribedListMessageTypes;
	}

}
?>