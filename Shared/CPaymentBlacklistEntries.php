<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CPaymentBlacklistEntries
 * Do not add any new functions to this class.
 */
/**
 * @todo : EOS migrated
 */
class CPaymentBlacklistEntries extends CBasePaymentBlacklistEntries {

	public static function fetchAllPaymentBlacklistEntries( $objDatabase ) {
		return parent::fetchPaymentBlacklistEntries( 'SELECT * FROM payment_blacklist_entries', $objDatabase );
	}

	public static function fetchPaymentBlacklistEntriesByPaymentBlacklistTypesAndByLookupStringHashed( $mixedPaymentBlacklistTypeId, $mixedLookupStringHashed, $objDatabase ) {
		$strPaymentBlacklistTypes = '';
		if( valArr( $mixedPaymentBlacklistTypeId ) ) {
			$strPaymentBlacklistTypes = implode( ',', $mixedPaymentBlacklistTypeId );
		} else {
			$strPaymentBlacklistTypes = $mixedPaymentBlacklistTypeId;
		}

		$strLookupStringHashed = '';
		if( valArr( $mixedLookupStringHashed ) ) {
			$strLookupStringHashed = '\'' . implode( '\',\'', $mixedLookupStringHashed ) . '\'';
		} else {
			$strLookupStringHashed = '\'' . $mixedLookupStringHashed . '\'';
		}
		return parent::fetchPaymentBlacklistEntries( sprintf( 'SELECT * FROM payment_blacklist_entries WHERE lookup_string_hashed IN (%s) AND payment_blacklist_type_id IN (%s) AND deleted_on IS NULL', $strLookupStringHashed, $strPaymentBlacklistTypes ), $objDatabase );
	}

	public static function fetchPaginatedPaymentBlacklistEntriesCount( $strSearchFilterText, $objPaymentDatabase, $objAdminDatabase ) {

		$strWhereClause = $strJoinClause = '';
		if( true == valStr( $strSearchFilterText ) ) {

			$strSql = 'SELECT
						id
					FROM
						company_payments
					WHERE
						check_routing_number = \'' . addslashes( trim( $strSearchFilterText ) ) . '\'
						OR check_account_number_bindex = \'' . \Psi\Libraries\Cryptography\CCrypto::createService()->blindIndex( trim( $strSearchFilterText ), CONFIG_SODIUM_KEY_BLIND_INDEX ) . '\'';

			$arrintResponse = ( array ) fetchData( $strSql, $objAdminDatabase );
			$arrintCompanyPaymentIds = [ $strSearchFilterText ];
			if( true == valArr( $arrintResponse ) ) {
				$arrintCompanyPaymentIds = array_keys( rekeyArray( 'id', $arrintResponse ) );

			}

			$strWhereClause = ' AND ( nradr.ar_payment_id = ' . $strSearchFilterText . '
									OR nradr.settlement_distribution_id = ' . $strSearchFilterText . '
									OR nradr.eft_instruction_id = ' . $strSearchFilterText . '
									OR apb.check_routing_number = \'' . addslashes( trim( $strSearchFilterText ) ) . '\'
									OR apb.check_account_number_bindex = \'' . \Psi\Libraries\Cryptography\CCrypto::createService()->blindIndex( trim( $strSearchFilterText ), CONFIG_SODIUM_KEY_BLIND_INDEX ) . '\'
									OR nradr.company_payment_id IN(' . implode( ',', $arrintCompanyPaymentIds ) . ') )';

			$strJoinClause  = ' LEFT JOIN ar_payment_billings apb ON( nradr.ar_payment_id = apb.ar_payment_id AND nradr.cid = apb.cid )';
		}

		$strSql = 'SELECT
						count( distinct( pbe.id ) ) as count
					FROM
						payment_blacklist_entries pbe
						JOIN nacha_return_addenda_detail_records nradr ON( nradr.payment_blacklist_entry_id = pbe.id )
						JOIN clients c ON( c.id = nradr.cid )' . $strJoinClause . '
					WHERE
						nradr.clearing_batch_id IS NULL' . $strWhereClause;

		$arrintResponse = fetchData( $strSql, $objPaymentDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchPaginatedPaymentBlacklistEntries( $strSearchFilterText, $objPaymentDatabase, $objAdminDatabase, $objPagination = NULL ) {

		$strLimitClause = $strWhereClause = $strJoinClause = '';
		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$intOffset 		= ( 0 < $objPagination->getPageNo() ) ? $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) : 0;
			$intLimit 		= ( int ) $objPagination->getPageSize();

			$strLimitClause = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;
		}

		if( true == valStr( $strSearchFilterText ) ) {

			$strSql = 'SELECT
						id
					FROM
						company_payments
					WHERE
						check_routing_number = \'' . addslashes( trim( $strSearchFilterText ) ) . '\'
						OR check_account_number_bindex = \'' . \Psi\Libraries\Cryptography\CCrypto::createService()->blindIndex( trim( $strSearchFilterText ), CONFIG_SODIUM_KEY_BLIND_INDEX ) . '\'';

			$arrintResponse = ( array ) fetchData( $strSql, $objAdminDatabase );
			$arrintCompanyPaymentIds = [ $strSearchFilterText ];
			if( true == valArr( $arrintResponse ) ) {
				$arrintCompanyPaymentIds = array_keys( rekeyArray( 'id', $arrintResponse ) );

			}

			$strWhereClause = ' AND ( nradr.ar_payment_id = ' . $strSearchFilterText . '
									OR nradr.settlement_distribution_id = ' . $strSearchFilterText . '
									OR apb.check_routing_number = \'' . addslashes( trim( $strSearchFilterText ) ) . '\'
									OR apb.check_account_number_bindex = \'' . \Psi\Libraries\Cryptography\CCrypto::createService()->blindIndex( trim( $strSearchFilterText ), CONFIG_SODIUM_KEY_BLIND_INDEX ) . '\'
									OR nradr.company_payment_id IN(' . implode( ',', $arrintCompanyPaymentIds ) . ') )';

			$strJoinClause  = ' LEFT JOIN ar_payment_billings apb ON( nradr.ar_payment_id = apb.ar_payment_id AND nradr.cid = apb.cid )';
		}

		$strSql = 'SELECT
       						pbe.id,
							pbe.system_email_id,
							pbe.blacklist_reason,
							pbe.deleted_on,
							pbe.created_on,
							nradr.cid,
							c.company_name,
       						nradr.ar_payment_id,
       						nradr.company_payment_id,
       						nradr.settlement_distribution_id,
       						nradr.eft_instruction_id
					FROM
							payment_blacklist_entries pbe
       						JOIN nacha_return_addenda_detail_records nradr ON( nradr.payment_blacklist_entry_id = pbe.id )
							JOIN clients c ON( c.id = nradr.cid ) ' . $strJoinClause . '
       				WHERE
							nradr.clearing_batch_id IS NULL ' . $strWhereClause . '
					ORDER BY
							pbe.id DESC' . $strLimitClause;

		$arrmixPaymentBlacklistEntries = ( array ) fetchData( $strSql, $objPaymentDatabase );

		if( true == valArr( $arrmixPaymentBlacklistEntries ) ) {
			$arrmixPaymentBlacklistEntries = rekeyArray( 'id', $arrmixPaymentBlacklistEntries );
		} else {
			return NULL;
		}

		$arrintCompanyPaymentIds = [];
		$arrintArPaymentIds  = [];

		foreach( $arrmixPaymentBlacklistEntries as $arrmixPaymentBlacklistEntry ) {

			if( false == is_null( $arrmixPaymentBlacklistEntry['company_payment_id'] ) ) {

				$arrintCompanyPaymentIds[$arrmixPaymentBlacklistEntry['company_payment_id']] = $arrmixPaymentBlacklistEntry['company_payment_id'];

			} elseif( false == is_null( $arrmixPaymentBlacklistEntry['ar_payment_id'] ) ) {

				$arrintArPaymentIds[$arrmixPaymentBlacklistEntry['ar_payment_id']] = $arrmixPaymentBlacklistEntry['ar_payment_id'];

			}
		}

		$arrmixCompanyPayments = ( array ) \Psi\Eos\Admin\CCompanyPayments::createService()->fetchSimpleCompanyPaymentsByIds( $arrintCompanyPaymentIds, $objAdminDatabase );

		if( true == valArr( $arrmixCompanyPayments ) ) {
			$arrmixCompanyPayments = rekeyArray( 'id', $arrmixCompanyPayments );
		}

		$arrmixArPayments = ( array ) \Psi\Eos\Payment\CArPaymentBillings::createService()->fetchArPaymentBillingsByArPaymentIds( $arrintArPaymentIds, $objPaymentDatabase );

		if( true == valArr( $arrmixArPayments ) ) {
			$arrmixArPayments = rekeyArray( 'id', $arrmixArPayments );
		}

		foreach( $arrmixPaymentBlacklistEntries as $arrmixPaymentBlacklistEntry ) {

			$strCheckRoutingNumber = NULL;
			$strCheckAccountNumberEncrypted = NULL;
			$strTransactionType = NULL;

			if( false == is_null( $arrmixPaymentBlacklistEntry['company_payment_id'] ) && true == valArr( $arrmixCompanyPayments ) ) {

				$strCheckRoutingNumber = $arrmixCompanyPayments[$arrmixPaymentBlacklistEntry['company_payment_id']]['check_routing_number'];
				$strCheckAccountNumberEncrypted = $arrmixCompanyPayments[$arrmixPaymentBlacklistEntry['company_payment_id']]['check_account_number_encrypted'];
				$strTransactionType = 'Company Payment';

			} elseif( false == is_null( $arrmixPaymentBlacklistEntry['ar_payment_id'] ) && true == valArr( $arrmixArPayments ) ) {

				$strCheckRoutingNumber = $arrmixArPayments[$arrmixPaymentBlacklistEntry['ar_payment_id']]['check_routing_number'];
				$strCheckAccountNumberEncrypted = $arrmixArPayments[$arrmixPaymentBlacklistEntry['ar_payment_id']]['check_account_number_encrypted'];
				$strTransactionType = 'Ar Payment';

			} elseif( false == is_null( $arrmixPaymentBlacklistEntry['settlement_distribution_id'] ) ) {

				$strTransactionType = 'Settlement Distribution';

			} elseif( false == is_null( $arrmixPaymentBlacklistEntry['eft_instruction_id'] ) ) {

				$strTransactionType = 'EFT Instruction';

			}

			$arrmixPaymentBlacklistEntries[$arrmixPaymentBlacklistEntry['id']]['transaction_type'] = $strTransactionType;
			$arrmixPaymentBlacklistEntries[$arrmixPaymentBlacklistEntry['id']]['check_routing_number'] = $strCheckRoutingNumber;
			$arrmixPaymentBlacklistEntries[$arrmixPaymentBlacklistEntry['id']]['check_account_number_masked'] = self::getCheckAccountNumberMasked( $strCheckAccountNumberEncrypted );
		}

		return $arrmixPaymentBlacklistEntries;
	}

	public static function getCheckAccountNumberMasked( $strCheckAccountNumberEncrypted ) {
		if( false == \valStr( $strCheckAccountNumberEncrypted ) ) {
			return NULL;
		}
		$strCheckAccountNumber = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $strCheckAccountNumberEncrypted, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );

		$intStringLength = strlen( $strCheckAccountNumber );
		$strLastFour = \Psi\CStringService::singleton()->substr( $strCheckAccountNumber, -4 );
		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public static function getCheckAccountNumber( $strCheckAccountNumberEncrypted ) {
		if( false == \valStr( $strCheckAccountNumberEncrypted ) ) {
			return NULL;
		}
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $strCheckAccountNumberEncrypted, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );

	}

}
?>