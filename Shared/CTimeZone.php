<?php

class CTimeZone extends CBaseTimeZone {

	const INERNATIONAL_DATE_TIME_LINE_WEST	= 1;
	const BERING_TIME						= 2;
	const HAWAII_ALEUTIAN_STANDARD_TIME		= 3;
	const YUKON_STANDARD_TIME 				= 4;
	const PACIFIC_STANDARD_TIME				= 5;
	const MOUNTAIN_STANDARD_TIME 			= 6;
	const CENTRAL_STANDARD_TIME				= 7;
	const EASTERN_STANDARD_TIME				= 8;
	const ATLANTIC_STANDARD_TIME			= 9;
	const NEWFOUNDLAND_STANDARD_TIME		= 10;
	const ALASKA_TIME						= 11;

	const DEFAULT_TIME_ZONE_NAME = 'America/Denver';

	public static $c_arrstrTimezonesAbbreviationsByOffset = [
		'-1000'	=> 'HST',
		'-900'	=> 'AKST',
		'-800'	=> 'PST',
		'-700'	=> 'MST',
		'-600'	=> 'CST',
		'-500'	=> 'EST',
		'-400'	=> 'AST',
		'-350'	=> 'NST'
	];

	public static $c_arrstrTimezonesIdsByOffset = [
		'-1000'	=> 3,
		'-800'	=> 5,
		'-700'	=> 6,
		'-600'	=> 7,
		'-500'	=> 8,
		'-400'	=> 9,
		'-350'	=> 10,
		'-900'	=> 11
	];

	public static function getOffsetToTimeZone( $strTimeZoneOffset ) {
		$strTimeZone = getArrayElementByKey( $strTimeZoneOffset, self::$c_arrstrTimezonesAbbreviationsByOffset );
		return ( false == is_null( $strTimeZone ) ) ? $strTimeZone : 'MST';
	}

}
?>