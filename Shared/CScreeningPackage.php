<?php

class CScreeningPackage extends CBaseScreeningPackage {

	const COMBINE_CONDITION_SET = 0;

	const CREDIT_EVICTION_COMBINE_CONDITION_SET_NAME	= 0;
	const CREDIT_ONLY_COMBINE_CONDITION_SET_NAME		= 1;
	const EVICTION_ONLY_COMBINE_CONDITION_SET_NAME		= 2;

	protected $m_intScreeningApplicantTypeId;
	protected $m_boolIsCriminalOnlyEnabledForNoSsn;
	protected $m_strPackageVersion;
	protected $m_arrstrErrorMessages;
	protected $m_arrstrConditionNames;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getConditionNames() {

		if( NULL !== $this->m_arrstrConditionNames ) {
			return $this->m_arrstrConditionNames;
		}

		$this->m_arrstrConditionNames = [
			self::CREDIT_EVICTION_COMBINE_CONDITION_SET_NAME	=> __( 'Credit & Eviction' ),
			self::CREDIT_ONLY_COMBINE_CONDITION_SET_NAME		=> __( 'Credit Only' ),
			self::EVICTION_ONLY_COMBINE_CONDITION_SET_NAME		=> __( 'Eviction Only' )
		];

		return $this->m_arrstrConditionNames;
	}

	public function setScreeningApplicantTypeId( $intScreeningApplicantTypeId ) {
		$this->m_intScreeningApplicantTypeId = $intScreeningApplicantTypeId;
	}

	public function setIsCriminalOnlyEnabledForNoSsn( $boolIsCriminalOnlyEnabledForNoSsn ) {
		$this->m_boolIsCriminalOnlyEnabledForNoSsn = $boolIsCriminalOnlyEnabledForNoSsn;
	}

	public function setPackageVersion( $strPackageVersion ) {
		$this->m_strPackageVersion = $strPackageVersion;
	}

	public function getIsCriminalOnlyEnabledForNoSsn() {
		return $this->m_boolIsCriminalOnlyEnabledForNoSsn;
	}

	public function getScreeningApplicantTypeId() {
		return $this->m_intScreeningApplicantTypeId;
	}

	public function getPackageVersion() {
		return $this->m_strPackageVersion;
	}

	public function valExternalName() {
		$boolIsValid = true;

		if( true == is_null( $this->getExternalName() ) ) {
			$boolIsValid = false;
			$this->m_arrstrErrorMessages[5] = new CErrorMsg( ERROR_TYPE_VALIDATION, 'external_name_id', __( 'External Key Name is required' ) );
		}

		return $boolIsValid;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDelinquentAccountTimeframeTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningScoringModelId() {
		$boolIsValid = true;

		if( true == is_null( $this->getScreeningScoringModelId() ) ) {
			$boolIsValid = false;
			$this->m_arrstrErrorMessages[4] = new CErrorMsg( ERROR_TYPE_VALIDATION, 'screening_model_type_id', __( 'External Key is required.' ) );
		}

		return $boolIsValid;
	}

	public function valScreeningPackageTypeId() {

		$boolIsValid = true;

		if( true == is_null( $this->getScreeningPackageTypeId() ) ) {
			$boolIsValid = false;
			$this->m_arrstrErrorMessages[1] = new CErrorMsg( ERROR_TYPE_VALIDATION, 'screening_package_type_Id', __( 'Type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->m_arrstrErrorMessages[0] = new CErrorMsg( ERROR_TYPE_VALIDATION, 'screening_package_id', __( 'Package Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsUseForRenewal() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsCombineIncome() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsUseForLeaseModifications() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPackageCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function valStopProcessingOnRecommendationIds() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function validate( $strAction, $objScreeningDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				if( 0 < ( int ) CScreeningPackages::fetchPackageCountByIdByCidByNameByScreeningPackageTypeId( $this->getId(), $this->getCid(), $this->getName(), $this->getScreeningPackageTypeId(), $objScreeningDatabase ) ) {
					$boolIsValid = false;
					$this->m_arrstrErrorMessages[] = new CErrorMsg( ERROR_TYPE_VALIDATION, 'screening_package_name', __( 'The Package Name {%s, 0} already exists. Please enter a different name.', [ $this->getName() ] ) );
				}

				if( false == $boolIsValid ) {
					return $boolIsValid;
				}

				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valScreeningPackageTypeId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( isset( $arrmixValues['screening_applicant_type_id'] ) ) {
			$this->setScreeningApplicantTypeId( $arrmixValues['screening_applicant_type_id'] );
		}

		if( isset( $arrmixValues['is_criminal_only_enabled_for_no_ssn'] ) ) {
			$this->setIsCriminalOnlyEnabledForNoSsn( $arrmixValues['is_criminal_only_enabled_for_no_ssn'] );
		}

		if( isset( $arrmixValues['package_version'] ) ) {
			$this->setPackageVersion( $arrmixValues['package_version'] );
		}
	}

	public function getScreeningPackageScreenTypes( $objScreeningApplicant, $objDatabase ) {

		$intPreScreeningSetId = NULL;

		if( true == $this->getIsUseForPrescreening() ) {
			$intPreScreeningSetId = ( int ) \Psi\Eos\Screening\CScreeningPackageScreenTypeAssociations::createService()->fetchNextPreScreeningSetIdByScreeningPackageIdByScreeningApplicantIdByScreeningIdByCid( $objScreeningApplicant->getScreeningPackageId(), $objScreeningApplicant->getId(), $objScreeningApplicant->getScreeningId(), $objScreeningApplicant->getCid(), $objDatabase );
		}

		return CScreeningPackageScreenTypeAssociations::fetchPublishedScreeningPackageAssociatedScreenTypesByScreeningPackageId( $objScreeningApplicant->getScreeningPackageId(), $objDatabase, $this->getIsUseForPrescreening(), $intPreScreeningSetId );
	}

	public function getCustomErrorMessages() {
		return $this->m_arrstrErrorMessages;
	}

	public function generateArchiveScreeningPackageScreenTypeAssociationsSql() {
		$strSql = 'UPDATE 
						screening_package_screen_type_associations
					SET 
						is_published = 0
					WHERE 
						screening_package_id = ' . ( int ) $this->getId() . ';';

		return $strSql;
	}

	public function generateArchiveScreeningPackageApplicantsSql() {
		$strSql = 'UPDATE 
						screening_package_applicants
					SET 
						is_published = 0
					WHERE 
						screening_package_id = ' . ( int ) $this->getId() . ';';

		return $strSql;
	}

	public function generateArchiveScreeningPackagePropertiesSql() {
		$strSql = 'UPDATE 
						screening_packages_properties
					SET 
						is_active = 0
					WHERE 
						cid = ' . ( int ) $this->getCid() . '
						AND screening_package_id = ' . ( int ) $this->getId() . ';';

		return $strSql;
	}

	public function generateArchiveScreeningPackageCustomerTypeMappingsSql() {
		$strSql = 'UPDATE 
						screening_package_customer_types_mappings
					SET 
						is_active = 0
					WHERE 
						cid = ' . ( int ) $this->getCid() . '
						AND default_screening_package_id = ' . ( int ) $this->getId() . ';';

		return $strSql;
	}

	public function generateScreeningPackageScreenTypeAssociationsSql( $intOldScreeningPackageId ) {
		$strSql = 'INSERT INTO 
						screening_package_screen_type_associations ( screening_package_id, screen_type_id, prescreening_set_id, screening_data_provider_id, screening_criteria_id, is_published, updated_by,updated_on, created_by, created_on )
					SELECT
					    ' . $this->getId() . ',
					    screen_type_id,
					    prescreening_set_id,
					    screening_data_provider_id,
					    screening_criteria_id,
					    is_published,
					    updated_by,
					    NOW(),
					    created_by,
					    NOW()
					FROM
                        screening_package_screen_type_associations
					WHERE
						is_published = 1
                        AND screening_package_id = ' . ( int ) $intOldScreeningPackageId . ';';

		return $strSql;
	}

	public function generateScreeningPackageApplicantsSql( $intOldScreeningPackageId ) {
		$strSql = 'INSERT INTO 
							screening_package_applicants ( screening_package_id, screening_applicant_type_id, is_published, updated_by, updated_on, created_by, created_on )
						SELECT
						    ' . $this->getId() . ',
						    screening_applicant_type_id,
						    is_published,
						    updated_by,
						    NOW(),
						    created_by,
						    NOW()
						FROM
						    screening_package_applicants
						WHERE
							is_published = 1
						    AND screening_package_id = ' . ( int ) $intOldScreeningPackageId . ';';

		return $strSql;
	}

	public function generateScreeningPackagePropertiesSql( $intOldScreeningPackageId ) {
		$strSql = 'INSERT INTO 
							screening_packages_properties ( cid, property_id, screening_package_id, screening_package_name, is_active, updated_by, updated_on, created_by, created_on )
						SELECT
						    cid,
						    property_id,
						    ' . $this->getId() . ',
						    screening_package_name,
						    is_active,
						    updated_by,
						    NOW(),
						    created_by,
						    NOW()
						FROM
						    screening_packages_properties
						WHERE
						    is_active = 1
						    AND screening_package_id = ' . ( int ) $intOldScreeningPackageId . '
						    AND cid = ' . ( int ) $this->getCid() . ';';

		return $strSql;
	}

	public function generateScreeningPackagePropertiesCustomerMappingSql( $intOldScreeningPackageId ) {

		$strSql = 'INSERT INTO 
							screening_package_customer_types_mappings ( cid, property_id, lease_type_id, customer_type_id, customer_relationship_id, default_screening_package_id, screening_applicant_requirement_type_id, is_active  updated_by, updated_on, created_by, created_on )
						SELECT
						  id SERIAL,
						  cid,
						  property_id,
						  lease_type_id,
						  customer_type_id,
						  customer_relationship_id,
						  default_screening_package_id,
						  screening_applicant_requirement_type_id,
						  is_active,
						  updated_by,
						  updated_on,
						  created_by,
						  created_on,
						FROM
						    screening_package_customer_types_mappings
						WHERE
						    is_active = 1
						    AND screening_package_id = ' . ( int ) $intOldScreeningPackageId . '
						    AND cid = ' . ( int ) $this->getCid() . ';';

		return $strSql;
	}

	public function createScreeningPackageApplicant() {
		$objScreeningPackageApplicant = new CScreeningPackageApplicant();
		$objScreeningPackageApplicant->setScreeningPackageId( $this->getId() );
		$objScreeningPackageApplicant->setIsPublished( true );

		return $objScreeningPackageApplicant;
	}

	public function getUpdateScreeningPackageNameSQL() {

		$strSql = 'UPDATE
						screening_packages_properties 
					SET
						screening_package_name = \'' . pg_escape_string( $this->getName() ) . '\',
						updated_on = NOW(),
						updated_by = ' . ( int ) $this->getUpdatedBy() . '
					WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND screening_package_id = ' . ( int ) $this->getId();
		return $strSql;
	}

    public function setScreeningPackageStopProcessingOnRecommendationIds() {

        if( true == $this->getIsUseForPrescreening() ) {

            $arrintFinalSelectedStopProcessingOnRecommendationIds = [ CScreeningRecommendationType::FAIL ];

            $arrintSelectedStopProcessingOnRecommendationIds = $this->getStopProcessingOnRecommendationIds();
            if( true == valArr( $arrintSelectedStopProcessingOnRecommendationIds ) ) {

                array_push( $arrintSelectedStopProcessingOnRecommendationIds, CScreeningRecommendationType::FAIL );
                $arrintFinalSelectedStopProcessingOnRecommendationIds = $arrintSelectedStopProcessingOnRecommendationIds;
            }

            $this->setStopProcessingOnRecommendationIds( $arrintFinalSelectedStopProcessingOnRecommendationIds );
        } else {

            $arrintFinalSelectedStopProcessingOnRecommendationIds = [ CScreeningRecommendationType::FAIL ];
            $this->setStopProcessingOnRecommendationIds( $arrintFinalSelectedStopProcessingOnRecommendationIds );
        }
    }

    public function getScreeningPackageStopProcessingOnRecommendationIds() {
        return ( true == valArr( $this->getStopProcessingOnRecommendationIds() ) ) ? array_merge( $this->getStopProcessingOnRecommendationIds(), [ CScreeningRecommendationType::PENDING_UNKNOWN, CScreeningRecommendationType::ERROR ] ) : [ CScreeningRecommendationType::FAIL, CScreeningRecommendationType::PENDING_UNKNOWN, CScreeningRecommendationType::ERROR ];
    }

}
?>