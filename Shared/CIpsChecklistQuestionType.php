<?php

class CIpsChecklistQuestionType extends CBaseIpsChecklistQuestionType {

	const PROPERTY			= 1;
	const WEBSITE			= 2;
	const COMPANY			= 3;
	const NON_SETTING		= 4;
	const MODULE			= 5;
	const TEXT_RESPONSE		= 6;
	const PROPERTY_MODULE	= 7;
	const UPLOAD_DOC		= 8;
	const MULTIPLE_CHOICE	= 9;
	const MULTIPLE_SELECT	= 10;

	public static $c_arrintSettingQuestionTypes = [
		self::PROPERTY	=> 'Property',
		self::WEBSITE	=> 'Website',
		self::COMPANY	=> 'Company',
	];

	public static $c_arrintMultiChoiceMultiSelectQuestionTypes = [
		self::MULTIPLE_CHOICE,
		self::MULTIPLE_SELECT
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>