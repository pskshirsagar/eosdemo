<?php

class CPhoneNumberType extends CBasePhoneNumberType {

	const PRIMARY 				= 1;
	const HOME 					= 2;
	const OFFICE 				= 3;
	const MOBILE 				= 4;
	const FAX 					= 5;
	const TOLL_FREE 			= 6;
	const OTHER 				= 7;
	const MARKETING 			= 8;
	const WEBSITE 				= 9;
	const MAINTENANCE 			= 10;
	const MAINTENANCE_EMERGENCY = 11;
	const RESIDENT_OFFICE		= 12;
	const INTERNAL				= 13;

	public static $c_arrstrValidResidentPhoneNumberTypes    = [ \CPhoneNumberType::HOME => 'Home', \CPhoneNumberType::MOBILE => 'Mobile', \CPhoneNumberType::OFFICE => 'Office' ];
	public static $c_arrintTenantPhoneNumberTypes           = [ self::HOME, self::MOBILE, self::OFFICE ];
	public static $c_arrintCustomerPhoneNumberTypes         = [ self::HOME, self::MOBILE, self::OFFICE ];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getPhoneNumberTypeNameByPhoneNumberTypeId( $intPhoneNumberTypeId ) {

		switch( $intPhoneNumberTypeId ) {
			case self::PRIMARY:
				return __( 'Primary' );
				break;

			case self::HOME:
				return __( 'Home' );
				break;

			case self::OFFICE:
				return __( 'Office' );
				break;

			case self::MOBILE:
				return __( 'Mobile' );
				break;

			case self::FAX:
				return __( 'Fax' );
				break;

			case self::TOLL_FREE:
				return __( 'Toll Free' );
				break;

			case self::OTHER:
				return __( 'Other' );
				break;

			case self::WEBSITE:
				return __( 'Website' );
				break;

			case self::MAINTENANCE:
				return __( 'Maintenance' );
				break;

			case self::MAINTENANCE_EMERGENCY:
				return __( 'Maintenance Emergency' );
				break;

			case self::RESIDENT_OFFICE:
				return __( 'Resident Office' );
				break;

			case self::MARKETING:
				return __( 'Marketing' );
				break;

			default:
				return __( 'Other' );
				break;
		}
	}

	public function getPhoneNumberTypes() {
		return [
			CPhoneNumberType::PRIMARY				=> $this->getPhoneNumberTypeNameByPhoneNumberTypeId( CPhoneNumberType::PRIMARY ),
			CPhoneNumberType::OFFICE				=> $this->getPhoneNumberTypeNameByPhoneNumberTypeId( CPhoneNumberType::OFFICE ),
			CPhoneNumberType::WEBSITE				=> $this->getPhoneNumberTypeNameByPhoneNumberTypeId( CPhoneNumberType::WEBSITE ),
			CPhoneNumberType::FAX					=> $this->getPhoneNumberTypeNameByPhoneNumberTypeId( CPhoneNumberType::FAX ),
			CPhoneNumberType::MAINTENANCE			=> $this->getPhoneNumberTypeNameByPhoneNumberTypeId( CPhoneNumberType::MAINTENANCE ),
			CPhoneNumberType::MAINTENANCE_EMERGENCY	=> $this->getPhoneNumberTypeNameByPhoneNumberTypeId( CPhoneNumberType::MAINTENANCE_EMERGENCY ),
			CPhoneNumberType::HOME					=> $this->getPhoneNumberTypeNameByPhoneNumberTypeId( CPhoneNumberType::HOME ),
			CPhoneNumberType::RESIDENT_OFFICE		=> $this->getPhoneNumberTypeNameByPhoneNumberTypeId( CPhoneNumberType::RESIDENT_OFFICE ),
			CPhoneNumberType::OTHER					=> $this->getPhoneNumberTypeNameByPhoneNumberTypeId( CPhoneNumberType::OTHER ),
			CPhoneNumberType::MOBILE				=> $this->getPhoneNumberTypeNameByPhoneNumberTypeId( CPhoneNumberType::MOBILE ),
			CPhoneNumberType::TOLL_FREE				=> $this->getPhoneNumberTypeNameByPhoneNumberTypeId( CPhoneNumberType::TOLL_FREE ),
			CPhoneNumberType::MARKETING				=> $this->getPhoneNumberTypeNameByPhoneNumberTypeId( CPhoneNumberType::MARKETING )
		];
	}

	public function getValidResidentPhoneNumberTypes() {
		return [
			CPhoneNumberType::HOME					=> $this->getPhoneNumberTypeNameByPhoneNumberTypeId( CPhoneNumberType::HOME ),
			CPhoneNumberType::MOBILE				=> $this->getPhoneNumberTypeNameByPhoneNumberTypeId( CPhoneNumberType::MOBILE ),
			CPhoneNumberType::OFFICE				=> $this->getPhoneNumberTypeNameByPhoneNumberTypeId( CPhoneNumberType::OFFICE )
		];
	}

}
?>