<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CMilitaryInstallations
 * Do not add any new functions to this class.
 */

class CMilitaryInstallations extends CBaseMilitaryInstallations {

	public static function fetchAllPublishedMilitaryInstallations( $objDatabase ) {

		$strSql = 'SELECT
					    mi.id,
					    mi.name,
					    mi.state_code
					FROM
					    military_installations mi
					WHERE
					    mi.is_published = TRUE
					ORDER BY
                        mi.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedMilitaryInstallationsByMilitaryComponentIds( $intPageNo, $intPageSize, $arrintMilitaryComponentIds, $strMilitaryQuickSearch = NULL, $objDatabase ) {
		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;

		$strSql		= 'SELECT
							mi.*,
							mc.name as military_component_name
						FROM
							military_installations mi
						JOIN military_components mc ON ( mi.military_component_id = mc.id )';

		if( valArr( $arrintMilitaryComponentIds ) ) {
			$strSql	.= ' WHERE mc.id IN( ' . implode( ',', $arrintMilitaryComponentIds ) . ') ';
		}

		if( valStr( $strMilitaryQuickSearch ) ) {
			$strSql	.= ' WHERE mi.name iLIKE \'%' . $strMilitaryQuickSearch . '%\'';
		}

		$strSql	.= ' ORDER BY mi.id OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;

		return self::fetchMilitaryInstallations( $strSql, $objDatabase );
	}

	public static function fetchPaginatedMilitaryInstallationsCountByMilitaryComponentIds( $arrintMilitaryComponentIds, $strMilitaryQuickSearch = NULL, $objDatabase ) {

		$strSql	= ' WHERE is_published = true';

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrintMilitaryComponentIds ) ) {
			$strSql	.= ' AND military_component_id IN ( ' . implode( ', ', $arrintMilitaryComponentIds ) . ' ) ';
		}

		if( valStr( $strMilitaryQuickSearch ) ) {
			$strSql	.= ' AND name LIKE \'%' . $strMilitaryQuickSearch . '%\'';
		}

		return self::fetchMilitaryInstallationCount( $strSql, $objDatabase );
	}

	public static function fetchMilitaryInstallationByMilitaryNameByMilitaryComponentId( $intMilitaryName, $intMilitaryComponentId, $objDatabase ) {

		$strSql = 'SELECT
						mi.*
					FROM
						military_installations mi';

		if( false == is_null( $intMilitaryName ) && false == is_null( $intMilitaryComponentId ) ) {
			$strSql .= ' WHERE name = \'' . ( string ) \Psi\CStringService::singleton()->ucwords( $intMilitaryName ) . '\' AND military_component_id = ' . ( int ) $intMilitaryComponentId;
		}

		return self::fetchMilitaryInstallation( $strSql, $objDatabase );

	}

	public static function fetchMilitaryInstallationsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT mi.*
					FROM
						military_installations mi
						JOIN property_details pd ON ( pd.military_installation_id = mi.id )
					WHERE
						pd.cid = ' . ( int ) $intCid . '
						AND mi.is_published = TRUE';

		return self::fetchMilitaryInstallations( $strSql, $objDatabase );
	}

	public static function fetchAllMilitaryInstallations( $objDatabase ) {
		return self::fetchMilitaryInstallations( 'SELECT * FROM military_installations', $objDatabase );
	}

}
?>