<?php

class CScreeningDecisionType extends CBaseScreeningDecisionType {
	const APPROVE                   = 1;
	const APPROVE_WITH_CONDITIONS   = 2;
	const DENIED                    = 3;
	const PENDING_UNKNOWN           = 4;
	const COMPLETED                 = 5;
	const RESET						= 6;
	const RESET_CONDITION			= 7;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public static function getScreeningDecisionTypeText( $intScreeningRecommendationTypeId ) {
        $strScreeningDecisionTypeText = '';

        switch( $intScreeningRecommendationTypeId ) {
            case self::APPROVE:
                $strScreeningDecisionTypeText = __( 'Approved' );
                break;

            case self::APPROVE_WITH_CONDITIONS:
                $strScreeningDecisionTypeText = __( 'Approve With Conditions' );
                break;

            case self::DENIED:
                $strScreeningDecisionTypeText = __( 'Denied' );
                break;

            case self::COMPLETED:
                $strScreeningDecisionTypeText = __( 'Complete' );
                break;

            case self::PENDING_UNKNOWN:
                $strScreeningDecisionTypeText = __( 'Pending' );
                break;

            case self::RESET:
              	$strScreeningDecisionTypeText = __( 'Reset' );
               	break;

            default:
                // added default case
        }

        return $strScreeningDecisionTypeText;
    }

	public function getScreeningDecisionTypeIdToStrArray() {
		return [
			self::APPROVE => __( 'Approved' ),
			self::APPROVE_WITH_CONDITIONS => __( 'Approve With Conditions' ),
			self::DENIED => __( 'Denied' ),
			self::COMPLETED => __( 'Complete' ),
			self::PENDING_UNKNOWN => __( 'Pending' ),
			self::RESET => __( 'Reset' )
		];
	}

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

	public static function getScreeningDecisionTypeColor( $intScreeningDecisionTypeId ) {

		$strScreeningDecisionTypeColor = '';

		switch( $intScreeningDecisionTypeId ) {
			case self::APPROVE:
				$strScreeningDecisionTypeColor = 'green';
				break;

			case self::APPROVE_WITH_CONDITIONS:
				$strScreeningDecisionTypeColor = 'yellow';
				break;

			case self::DENIED:
				$strScreeningDecisionTypeColor = 'red';
				break;

			default:
				// added default case
		}

		return $strScreeningDecisionTypeColor;
	}

	public static function getScreeningDecisionCssClass( $intScreeningDecisionTypeId ) {
		$strScreeningDecisionCssClass = '';

		switch( $intScreeningDecisionTypeId ) {
			case self::APPROVE:
				$strScreeningDecisionCssClass = 'greenlight';
				break;

			case self::APPROVE_WITH_CONDITIONS:
				$strScreeningDecisionCssClass = 'yellowlight';
				break;

			case self::DENIED:
				$strScreeningDecisionCssClass = 'redlight';
				break;

			default:
				// added default case
		}

		return $strScreeningDecisionCssClass;
	}

}
?>