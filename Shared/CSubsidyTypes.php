<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSubsidyTypes
 * Do not add any new functions to this class.
 */

class CSubsidyTypes extends CBaseSubsidyTypes {

	public static function fetchActiveSubsidyTypes( $objDatabase, $arrintIds = NULL ) {
		$strSql = 'SELECT 
						* 
					FROM 
						subsidy_types 
					WHERE 
						is_published = TRUE' .
		                ( ( true == valArr( $arrintIds ) ) ? ' AND id IN ( ' . implode( ',', $arrintIds ) . ' )' : '' ) . '
					ORDER BY
						order_num';

		return self::fetchSubsidyTypes( $strSql, $objDatabase );
	}

	public static function fetchSubsidyTypesNameByIds( $objDatabase, $arrintSubsidyTypeIds ) {
		if( false == valArr( $arrintSubsidyTypeIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT id, name FROM subsidy_types st WHERE st.id IN ( ' . implode( ',', $arrintSubsidyTypeIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

}
?>