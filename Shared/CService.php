<?php

class CService extends CBaseService {

	protected $m_intCompanyUserServiceId;
	protected $m_intScopeServiceId;

	protected $m_strServiceGroupName;
	protected $m_strServiceGroupDescription;
	protected $m_strServiceGroupSystemCode;
	protected $m_strServiceGroupTypeSystemCode;
	protected $m_strMinorApiVersion;
	protected $m_strMajorApiVersion;
	protected $m_intServiceGroupTypeId;

	protected $m_arrobjServiceVersions;
	/** @var  \CServiceParameter[] */
	protected $m_arrobjServiceParameters;
	/** @var  \CServiceDetail[] */
	protected $m_arrobjServiceDetails;

	const GET_MITS_PROPERTYUNITS				= 1;
	const GET_UNITS_AVAILABILITY_AND_PRICING	= 45;
	const GET_FLOORPLANS						= 58;

	// DocScan Services
	const GET_OPEN_AR_PAYMENT_TRANSMISSIONS		= 6;
	const CREATE_AR_PAYMENT_TRANSMISSION		= 7;
	const CLOSE_AR_PAYMENT_TRANSMISSION			= 8;
	const PROCESS_AR_PAYMENT					= 11;
	const GET_COMPANY_PREFERENCES				= 13;
	const GET_MANAGEMENT_COMPANY				= 20;
	const GET_MERCHANT_ACCOUNTS					= 21;
	const GET_ELECTRONIC_PAYMENT_TYPES			= 23;
	const GET_PROPERTY_PREFERENCES				= 26;
	const GET_COMPANY_USER						= 46;
	const GET_VENDOR_LOCATIONS					= 66;
	const GET_MERCHANT_ACCOUNT_PROPERTIES		= 71;
	const SEND_INVOICES_NEW						= 74;
	const ISSUE_AR_PAYMENT_IDS					= 76;
	const GET_PREVIOUS_AR_PAYMENT_DETAILS		= 79;

	// LeadAlert Services
	const GET_CHAT_STATUS						= 60;
	const UPDATE_CHAT_STATUS					= 61;
	const APPROVE_CHAT							= 62;
	const END_CHAT								= 63;
	const GET_LEAD_ALERT_SETTINGS				= 126;

	// Report Services
	const GET_REPORT_INFO						= 265;
	const GET_REPORT_DATA						= 289;
	const GET_REPORT_LIST						= 359;
	const GET_DEPENDENT_FILTER					= 382;
	const GET_TASK_STATUS					    = 124;

	// ILS Services
	const GET_CLIENT_INFO						= 209;
	const GET_PROPERTY_INFO						= 210;
	const UPDATE_PROPERTY_FEED_TYPE				= 211;
	const PROCESS_PROPERTY_ACTIVATION			= 212;
	const GET_ILS_PROPERTIES_DATA				= 213;
	const SEND_LEAD_DETAILS						= 214;
	const GET_STUDENT_ILS_PROPERTIES_DATA		= 316;
	const SEND_STUDENT_LEAD_DETAILS 			= 317;
	const GET_LEAD_STATUS						= 357;
	const GET_ACTIVATIONS_PROPERTIES			= 412;
	const GET_PROPERTIES_PENDING_ACTIVATION		= 413;
	const GET_TOUR_AVAILABILITY					= 528;
	const SEND_SCHEDULED_TOUR_DETAILS			= 529;

	public static $c_arrstrDocScanServices = [
		'arpayments' 	=> [
			self::GET_ELECTRONIC_PAYMENT_TYPES		=> 'getelectronicpaymenttypes',
			self::GET_MERCHANT_ACCOUNTS				=> 'getmerchantaccounts',
			self::GET_PREVIOUS_AR_PAYMENT_DETAILS	=> 'getpreviousarpaymentdetails',
			self::ISSUE_AR_PAYMENT_IDS				=> 'issuearpaymentids',
			self::CLOSE_AR_PAYMENT_TRANSMISSION		=> 'closearpaymenttransmission',
			self::GET_OPEN_AR_PAYMENT_TRANSMISSIONS	=> 'getopenarpaymenttransmissions',
			self::CREATE_AR_PAYMENT_TRANSMISSION	=> 'createarpaymenttransmission',
			self::PROCESS_AR_PAYMENT				=> 'processarpayment'
		],
		'company'		=> [
			self::GET_COMPANY_PREFERENCES			=> 'getcompanypreferences',
			self::GET_MANAGEMENT_COMPANY			=> 'getmanagementcompany'
		],
		'companyusers' 	=> [
			self::GET_COMPANY_USER				=> 'getcompanyuser'
		],
		'properties' 	=> [
			self::GET_MERCHANT_ACCOUNT_PROPERTIES	=> 'getmerchantaccountproperties',
			self::GET_PROPERTY_PREFERENCES			=> 'getpropertypreferences'
		],
		'vendors' 		=> [
			self::GET_VENDOR_LOCATIONS				=> 'getvendorlocations',
			self::SEND_INVOICES_NEW					=> 'sendinvoicesnew'
		]
	];

	public static $c_arrstrLeadAlertServices = [
		'leads' => [
			self::GET_CHAT_STATUS					=> 'getchatstatus',
			self::UPDATE_CHAT_STATUS				=> 'updatechatstatus',
			self::APPROVE_CHAT						=> 'approvechat',
			self::END_CHAT							=> 'endchat',
			self::GET_LEAD_ALERT_SETTINGS			=> 'getleadalertsettings'
		]
	];

    public function __construct() {
        parent::__construct();

        // FIXME remove this static declaration once UI implemented at CA which allows user to select specific version.
	    $this->m_intMinorApiVersionId = CApiVersion::TMP_DEFAULT_MINOR_API_VERSION;
	    $this->m_intMajorApiVersionId = CApiVersion::TMP_DEFAULT_MAJOR_API_VERSION;
        return;
    }

    /**
     * Create Functions
     */

    public function createServiceDetail() {

    	$objServiceDetail = new CServiceDetail();

    	$objServiceDetail->setServiceId( $this->getId() );
	    $objServiceDetail->setMinorApiVersionId( $this->getMinorApiVersionId() );
    	$objServiceDetail->setServiceName( $this->getName() );

    	return $objServiceDetail;
    }

    public function createServiceParameter() {

    	$objServiceParameter = new CServiceParameter();
    	$objServiceParameter->setServiceId( $this->getId() );
    	$objServiceParameter->setMinorApiVersionId( $this->getMinorApiVersionId() );

    	return $objServiceParameter;
    }

    /**
     * Get Functions
     */

    public function getServiceGroupName() {
    	return $this->m_strServiceGroupName;
    }

    public function getServiceGroupDescription() {
    	return $this->m_strServiceGroupDescription;
    }

    public function getCompanyUserServiceId() {
    	return $this->m_intCompanyUserServiceId;
    }

    public function getScopeServiceId() {
    	return $this->m_intScopeServiceId;
    }

    public function getServiceGroupSystemCode() {
    	return $this->m_strServiceGroupSystemCode;
    }

    public function getServiceGroupTypeSystemCode() {
    	return $this->m_strServiceGroupTypeSystemCode;
    }

	public function getFormattedServiceAuthenticationTypes() {
		if( false == valArr( $this->getServiceAuthenticationTypeIds() ) ) return [];
		return array_intersect_key( CServiceAuthenticationType::$c_arrstrSystemCodeServiceAuthenticationTypes, array_flip( $this->getServiceAuthenticationTypeIds() ) );
	}

	public function getMinorApiVersion() {
		return $this->m_strMinorApiVersion;
	}

	public function getMajorApiVersion() {
		return $this->m_strMajorApiVersion;
	}

	public function getServiceGroupTypeId() {
		return $this->m_intServiceGroupTypeId;
	}

	public function getServiceVersions() {
		return $this->m_arrobjServiceVersions;
	}

	public function getServiceParameters() {
		return $this->m_arrobjServiceParameters;
	}

	public function getServiceDetails() {
    	return $this->m_arrobjServiceDetails;
	}

    /**
     * Set Functions
     */

    public function setServiceGroupName( $strServiceGroupName ) {
    	$this->m_strServiceGroupName = $strServiceGroupName;
    }

    public function setServiceGroupDescription( $strServiceGroupDescription ) {
    	$this->m_strServiceGroupDescription = $strServiceGroupDescription;
    }

    public function setCompanyUserServiceId( $intCompanyUserServiceId ) {
    	$this->m_intCompanyUserServiceId = $intCompanyUserServiceId;
    }

    public function setScopeServiceId( $intScopeServiceId ) {
    	$this->m_intScopeServiceId = $intScopeServiceId;
    }

    public function setServiceGroupSystemCode( $strServiceGroupSystemCode ) {
    	$this->m_strServiceGroupSystemCode = $strServiceGroupSystemCode;
    }

    public function setServiceGroupTypeSystemCode( $strServiceGroupTypeSystemCode ) {
    	$this->m_strServiceGroupTypeSystemCode = $strServiceGroupTypeSystemCode;
    }

    public function setMinorApiVersion( $strMinorApiVersion ) {
    	$this->m_strMinorApiVersion = $strMinorApiVersion;
    }

	public function setMajorApiVersion( $strMajorApiVersion ) {
		$this->m_strMajorApiVersion = $strMajorApiVersion;
	}

	public function setServiceGroupTypeId( $intServiceGroupTypeId ) {
		$this->m_intServiceGroupTypeId = $intServiceGroupTypeId;
	}

	public function addServiceVersion( $objService ) {
		$this->m_arrobjServiceVersions[] = $objService;
	}

	public function addServiceParameter( $objServiceParameter ) {
		$this->m_arrobjServiceParameters[] = $objServiceParameter;
	}

	public function addServiceDetail( $objServiceDetail ) {
    	$this->m_arrobjServiceDetails[] = $objServiceDetail;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['service_group_name'] ) )				$this->setServiceGroupName( $arrmixValues['service_group_name'] );
		if( true == isset( $arrmixValues['service_group_description'] ) )		$this->setServiceGroupDescription( $arrmixValues['service_group_description'] );
		if( true == isset( $arrmixValues['company_user_service_id'] ) )			$this->setCompanyUserServiceId( $arrmixValues['company_user_service_id'] );
		if( true == isset( $arrmixValues['scope_service_id'] ) )				$this->setScopeServiceId( $arrmixValues['scope_service_id'] );
		if( true == isset( $arrmixValues['service_group_system_code'] ) )		$this->setServiceGroupSystemCode( $arrmixValues['service_group_system_code'] );
		if( true == isset( $arrmixValues['service_group_type_system_code'] ) )	$this->setServiceGroupTypeSystemCode( $arrmixValues['service_group_type_system_code'] );
		if( true == isset( $arrmixValues['minor_api_version'] ) )	$this->setMinorApiVersion( $arrmixValues['minor_api_version'] );
		if( true == isset( $arrmixValues['major_api_version'] ) )	$this->setMajorApiVersion( $arrmixValues['major_api_version'] );
		if( true == isset( $arrmixValues['service_group_type_id'] ) )	$this->setServiceGroupTypeId( $arrmixValues['service_group_type_id'] );

    	return;
    }

    /**
     * Other Functions
     */

	public function fetchServiceParameters( $objDatabase, $boolPublishedOnly = false ) {
		return \Psi\Eos\Entrata\CServiceParameters::createService()->fetchAllServiceParametersByServiceIdByMinorApiVersionId( $this->m_intId, $this->getMinorApiVersionId(), $objDatabase, $boolPublishedOnly );
	}

    public function fetchServiceDetails( $objDatabase ) {
    	return \Psi\Eos\Admin\CServiceDetails::createService()->fetchServiceDetailsByServiceId( $this->m_intId, $objDatabase );
    }

    /**
     * Validate Functions
     */

    public function valName( $objDatabase ) {

    	$boolIsValid = true;
    	if( false == valStr( $this->getName() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'service_name', 'Service Name is required.' ) );
    	} elseif( false != \Psi\CStringService::singleton()->strstr( $this->getName(), ' ' ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Space is not allowed in service name.' ) );
    	} elseif( false == ctype_alpha( $this->getName() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Invalid service name.' ) );
    	} elseif( false != isset( $objDatabase ) && false != is_numeric( $this->getServiceGroupId() ) && false != valStr( $this->getName() ) && false != is_numeric( $this->getServiceTypeId() ) ) {
    		$objServiceGroup = \Psi\Eos\Entrata\CServiceGroups::createService()->fetchServiceGroupById( $this->getServiceGroupId(), $objDatabase );
    		if( false != valObj( $objServiceGroup, 'CServiceGroup' ) ) {
				if( false != isset( CService::$c_arrstrLeadAlertServices[\Psi\CStringService::singleton()->strtolower( $objServiceGroup->getSystemCode() )] ) && false != isset( array_flip( CService::$c_arrstrLeadAlertServices[\Psi\CStringService::singleton()->strtolower( $objServiceGroup->getSystemCode() )] )[\Psi\CStringService::singleton()->strtolower( $this->getName() )] ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'This service name already exists for one of the LeadAlert service having the same service group in other group type.<br>This service having same name is still allowed to use old LeadAlert URL, therefore we can not store this as a service name.<br>Once all LeadAlert services are migrated to new service URL we can remove this validation.' ) );
				} else {
					$strWhere = ' WHERE lower(name) = \'' . \Psi\CStringService::singleton()->strtolower( $this->getName() ) . '\' AND service_type_id = ' . $this->getServiceTypeId() . ' AND service_group_id = ' . $this->getServiceGroupId() . ' AND minor_api_version_id = ' . $this->getMajorApiVersionId() . ' AND major_api_version_id = ' . $this->getMajorApiVersionId();
					$strWhere .= ( 0 < $this->getId() ) ? 'AND id NOT IN (' . $this->getId() . ')'  : '';
					$intCount = \Psi\Eos\Entrata\CServices::createService()->fetchServiceCount( $strWhere, $objDatabase );
					if( 0 < $intCount ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'This service name is already exist.' ) );
					}
				}
    		}
    	}
    	return $boolIsValid;
    }

    public function valServiceTypeId() {
    	$boolIsValid = true;
    	if( false != is_null( $this->getServiceTypeId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'service_type_id', 'Please select service type.' ) );
    	}
    	return $boolIsValid;
    }

	public function valServiceAuthenticationTypeIds() {
		$boolIsValid = true;
		if( false == valArr( $this->getServiceAuthenticationTypeIds() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'service_type_ids', 'Please select service authentication type.' ) );
		}
		return $boolIsValid;
	}

    public function valServiceGroupId() {
    	$boolIsValid = true;
    	if( false == is_numeric( $this->getServiceGroupId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Service group id is required.' ) );
    	}
    	return $boolIsValid;
    }

	public function getDecodedServiceThresholds() {
		$arrstrDecodedServiceThresholds = NULL;
		if( false == is_null( $this->getServiceThresholds() ) ) {
			$arrstrDecodedServiceThresholds = json_decode( $this->getServiceThresholds(), true );
		}
		return $arrstrDecodedServiceThresholds;
	}

	public function setEncodedServiceThresholds( $arrstrServiceThresholds ) {
		$strServiceThresholds = NULL;
		if( false != valArr( $arrstrServiceThresholds ) ) {
			$arrstrServiceThresholds = $this->clearEmptyElementsFromArray( $arrstrServiceThresholds );
			if( false != valArr( $arrstrServiceThresholds ) ) {
				$strServiceThresholds = json_encode( $arrstrServiceThresholds );
			}
		}
		parent::setServiceThresholds( $strServiceThresholds );
	}

	public function clearEmptyElementsFromArray( $arrmixSourceArray ) {
		if( false == valArr( $arrmixSourceArray ) ) {
			return $arrmixSourceArray;
		}
		foreach( $arrmixSourceArray as $strKey => $strValue ) {
			if( false != valArr( $strValue ) ) {
				$arrmixSourceArray[$strKey] = $this->clearEmptyElementsFromArray( $arrmixSourceArray[$strKey] );
			}
			if( false != empty( $arrmixSourceArray[$strKey] ) ) {
				unset( $arrmixSourceArray[$strKey] );
			}
		}
		return $arrmixSourceArray;
	}

	public function valServiceThresholds() {
		$boolIsValid = true;
		$arrstrServiceThresholds = $this->getDecodedServiceThresholds();
		if( false != valArr( $arrstrServiceThresholds ) ) {
			foreach( $arrstrServiceThresholds as $strServiceThresholdType => $arrmixThresholdScopes ) {
				if( false == valArr( $arrmixThresholdScopes ) ) {
					continue;
				}
				foreach( $arrmixThresholdScopes as $strScope => $arrintServiceThresholds ) {
					if( false == valArr( $arrintServiceThresholds ) ) {
						continue;
					}
					if( 'general' == $strScope ) {
						foreach( $arrintServiceThresholds as $strThresholdKey => $intThresholdValue ) {
							if( false == empty( $intThresholdValue ) && ( false == is_numeric( $intThresholdValue ) || 0 >= $intThresholdValue || false == ctype_digit( $intThresholdValue ) ) ) {
								$strKey = \Psi\CStringService::singleton()->ucfirst( $strServiceThresholdType ) . '_' . \Psi\CStringService::singleton()->ucfirst( $strThresholdKey );
								$boolIsValid = false;
								$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strKey, 'Invalid ' . str_replace( '_', ' ', $strKey ) . ' value. The value must be positive and non fractional.' ) );
							}
						}
					} else {
						foreach( $arrintServiceThresholds as $intId => $arrintServiceThresholdValues ) {
							if( false == valArr( $arrintServiceThresholdValues ) ) {
								continue;
							}
							foreach( $arrintServiceThresholdValues as $strThresholdKey => $intThresholdValue ) {
								if( false == empty( $intThresholdValue ) && ( false == is_numeric( $intThresholdValue ) || 0 >= $intThresholdValue || false == ctype_digit( $intThresholdValue ) ) ) {
									$strKey = \Psi\CStringService::singleton()->ucfirst( $strServiceThresholdType ) . '_' . \Psi\CStringService::singleton()->ucfirst( $strThresholdKey );
									$boolIsValid = false;
									$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strKey, 'Invalid ' . str_replace( '_', ' ', $strKey ) . ' value. The value must be positive and non fractional.' ) );
								}
							}
						}
					}
				}
			}
		}
		return $boolIsValid;
	}

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valServiceTypeId();
            	$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valServiceAuthenticationTypeIds();
            	$boolIsValid &= $this->valServiceGroupId();
            	$boolIsValid &= $this->valServiceThresholds();
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid = true;
                break;

            default:
            	// default case
            	$boolIsValid = true;
                break;
        }

        return $boolIsValid;
    }

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$strSql = parent::update( $intCurrentUserId, $objDatabase, true );
		if( false == valStr( $strSql ) ) {
			return false;
		}
		$strSql = rtrim( $strSql, ';' );
		$strSql .= ' AND minor_api_version_id = ' . ( int ) $this->sqlMinorApiVersionId() . ' AND major_api_version_id = ' . ( int ) $this->sqlMajorApiVersionId() . ';';

		return ( true == $boolReturnSqlOnly ) ? $strSql : $this->executeSql( $strSql, $this, $objDatabase );
    }

	public function updateDefaultApiVersion( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$strSql = 'UPDATE
						services
					SET
						is_default_api_version = false,
						updated_by = ' . ( int ) $intCurrentUserId . ',
						updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->getId() . '
						AND major_api_version_id = ' . ( int ) $this->sqlMajorApiVersionId() . '
						AND minor_api_version_id <> ' . ( int ) $this->sqlMinorApiVersionId() . ';';

		return ( true == $boolReturnSqlOnly ) ? $strSql : $this->executeSql( $strSql, $this, $objDatabase );
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$strSql = rtrim( parent::delete( $intCurrentUserId, $objDatabase, true ), ';' );
		$strSql .= ' AND minor_api_version_id = ' . ( int ) $this->sqlMinorApiVersionId() . ' AND major_api_version_id = ' . ( int ) $this->sqlMajorApiVersionId() . ';';
		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function loadNextMinorVersion( $objDatabase ) {
		$intNextMinorVersion = \Psi\Eos\Admin\CApiVersions::createService()->fetchNextMinorApiVersionIdByMajorVersionIdByMinorVersionId( $this->getMajorApiVersionId(), $this->getMinorApiVersionId(), $objDatabase );
		if( false == valId( $intNextMinorVersion ) ) {
			return false;
		}
		$objService = new CService();
		$objService->setId( $this->getId() );
		$objService->setMinorApiVersionId( $intNextMinorVersion );
		$objService->setServiceGroupId( $this->getServiceGroupId() );
		$objService->setMajorApiVersionId( $this->getMajorApiVersionId() );
		$objService->setServiceTypeId( $this->getServiceTypeId() );
		$objService->setServiceAuthenticationTypeIds( $this->getServiceAuthenticationTypeIds() );
		$objService->setName( $this->getName() );
		$objService->setIsPublished( 0 );

		return $objService;

	}

}
?>