<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Payment\CArPaymentDetails
 * Do not add any new functions to this class.
 */

class CArPaymentDetails extends CBaseArPaymentDetails {

	public static function fetchArPaymentDetailByArPaymentId( $intArPaymentId, $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM ar_payment_details WHERE ar_payment_id = ' . ( int ) $intArPaymentId;
		return self::fetchArPaymentDetail( $strSql, $objPaymentDatabase );
	}

	public static function fetchArPaymentDetailByArPaymentIds( $arrintArPaymentIds, $objPaymentDatabase ) {
		if( false == valArr( $arrintArPaymentIds ) ) return NULL;
		$strSql = 'SELECT * FROM ar_payment_details WHERE ar_payment_id IN ( ' . implode( ',', $arrintArPaymentIds ) . ' ) ';
		return self::fetchArPaymentDetails( $strSql, $objPaymentDatabase );
	}

	public static function fetchSimpleArPaymentDetailByArPaymentIdsByCid( $arrintArPaymentIds, $intCid, $objPaymentDatabase ) {
		if( false == valArr( $arrintArPaymentIds ) ) return NULL;

		$strSql = 'SELECT id, payment_memo FROM ar_payment_details WHERE ar_payment_id IN ( ' . implode( ',', $arrintArPaymentIds ) . ' ) AND cid = ' . ( int ) $intCid;

		$arrmixArPaymentDetails = fetchData( $strSql, $objPaymentDatabase );

		return rekeyArray( 'id', $arrmixArPaymentDetails );
	}

	public static function fetchArPaymentDetailArrayByArPaymentId( $intArPaymentId, $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM ar_payment_details WHERE ar_payment_id = ' . ( int ) $intArPaymentId;
		return fetchData( $strSql, $objPaymentDatabase );
	}

	public static function fetchArPaymentDetailByRemotePaymentNumber( $strRemotePaymentNumber, $objDatabase, $boolReturnSqlOnly = false ) {
		$strSql = 'SELECT 
						id 
					FROM 
						ar_payment_details 
					WHERE 
						remote_payment_number = \'' . $strRemotePaymentNumber . '\' 
					LIMIT 1';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchArPaymentDetailWithArPaymentBillingByArPaymentIds( $arrintArPaymentIds, $objPaymentDatabase ) {

		$strSql = 'SELECT 
						apd.*,
						apb.cc_card_number_encrypted,
						apb.check_account_number_encrypted
					FROM 
						ar_payment_details apd
						JOIN ar_payment_billings apb ON ( apd.ar_payment_id = apb.ar_payment_id AND apd.cid = apb.cid  )
					WHERE 
						apd.ar_payment_id IN( ' . implode( ',', $arrintArPaymentIds ) . ' )';

		return self::fetchArPaymentDetails( $strSql, $objPaymentDatabase );
	}

}
?>