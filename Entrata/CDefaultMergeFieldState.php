<?php

class CDefaultMergeFieldState extends CBaseDefaultMergeFieldState {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDefaultMergeFieldId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valStateCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
           		$boolIsValid = true;
           		break;
        }

        return $boolIsValid;
    }
}
?>