<?php

class CMaintenanceBoardProblem extends CBaseMaintenanceBoardProblem {

	protected $m_objMaintenanceBoardProblemName;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceBoardId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceProblemId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['name'] ) ) $this->setName( $arrmixValues['name'] );
		return;
	}

	public function setName( $objMaintenanceBoardProblemName ) {
		$this->m_objMaintenanceBoardProblemName = $objMaintenanceBoardProblemName;
	}

	public function getName() {
		return $this->m_objMaintenanceBoardProblemName;
	}
}
?>