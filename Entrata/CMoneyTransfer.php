<?php

class CMoneyTransfer extends CBaseMoneyTransfer {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPaymentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApCodeTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFromBankAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valToApRemittanceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInvoiceApHeaderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentApHeaderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReimbursementInvoiceApHeaderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReimbursementPaymentApHeaderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChargeArTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentArTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReimbursementChargeArTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReimbursementPaymentArTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransferAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>