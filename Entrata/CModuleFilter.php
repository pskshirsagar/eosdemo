<?php

class CModuleFilter extends CBaseModuleFilter {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyUserId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFilterType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		$objModuleFilter = CModuleFilters::fetchModuleFilterByNameByCompanyUserIdByCid( $this->m_strName, $this->m_intCompanyUserId, $this->m_intCid, $objDatabase, $this->getFilterType() );

		if( true == valObj( $objModuleFilter, 'CModuleFilter' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Filter name is already exists.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valFilters() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDefault() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			$boolIsValid = $this->valName( $objDatabase );
				break;

			case VALIDATE_UPDATE:
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( ' NOW() ' );

		if( $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return true;
		}
	}

}
?>