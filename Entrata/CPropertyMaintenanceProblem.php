<?php

class CPropertyMaintenanceProblem extends CBasePropertyMaintenanceProblem {

	protected $m_intMaintenanceProblemTypeId;
	protected $m_intLaborStandardHours;
	protected $m_intMaintenanceLocationId;
	protected $m_intPropertyMaintenanceLocationProblemId;
	protected $m_intParentMaintenanceProblemId;
	protected $m_intShowOnResidentPortal;
	protected $m_intProblemActionsCount;

	protected $m_strName;
	protected $m_strDescription;
	protected $m_strPropertyName;
	protected $m_strLocations;
	protected $m_strCategoryName;
	protected $m_strIntegrationClientTypeName;
	protected $m_arrintProblemAssociatedLocationIds;
	protected $m_arrstrProblemAssociatedLocationNames;
	protected $m_intCategoryId;
	protected $m_intSettingsTemplateId;

	public function applyRequestForm( $arrmixRequestForm, $arrmixFormFields = NULL ) {

		parent::applyRequestForm( $arrmixRequestForm, $arrmixFormFields );

		if( true == valArr( $arrmixFormFields ) ) {
			$arrmixRequestForm = mergeIntersectArray( $arrmixFormFields, $arrmixRequestForm );
		}

		$this->setValues( $arrmixRequestForm, false );

		return;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolIsDirectSet );

		if( true == isset( $arrmixValues['name'] ) ) $this->setName( $arrmixValues['name'] );
		if( true == isset( $arrmixValues['description'] ) ) $this->setDescription( $arrmixValues['description'] );
		if( true == isset( $arrmixValues['maintenance_problem_type_id'] ) ) $this->setMaintenanceProblemTypeId( $arrmixValues['maintenance_problem_type_id'] );
		if( true == isset( $arrmixValues['labor_standard_hours'] ) ) $this->setLaborStandardHours( $arrmixValues['labor_standard_hours'] );
		if( true == isset( $arrmixValues['maintenance_location_id'] ) ) $this->setMaintenanceLocationId( $arrmixValues['maintenance_location_id'] );
		if( true == isset( $arrmixValues['property_maintenance_location_problem_id'] ) ) $this->setPropertyMaintenanceLocationProblemId( $arrmixValues['property_maintenance_location_problem_id'] );
		if( true == isset( $arrmixValues['parent_maintenance_problem_id'] ) ) $this->setParentMaintenanceProblemId( $arrmixValues['parent_maintenance_problem_id'] );
		if( true == isset( $arrmixValues['locations'] ) ) $this->setLocations( $arrmixValues['locations'] );
		if( true == isset( $arrmixValues['problem_actions_count'] ) ) $this->setProblemActionsCount( $arrmixValues['problem_actions_count'] );
		if( true == isset( $arrmixValues['category_name'] ) ) $this->setCategoryName( $arrmixValues['category_name'] );
		if( true == isset( $arrmixValues['show_on_resident_portal'] ) ) $this->setShowOnResidentPortal( $arrmixValues['show_on_resident_portal'] );
		if( true == isset( $arrmixValues['integration_client_type_name'] ) ) $this->setIntegrationClientTypeName( $arrmixValues['integration_client_type_name'] );
		if( true == isset( $arrmixValues['location_ids'] ) ) $this->setProblemAssociatedLocationIds( $arrmixValues['location_ids'] );
		if( true == isset( $arrmixValues['location_names'] ) ) $this->setProblemAssociatedLocationNames( $arrmixValues['location_names'] );
		if( true == isset( $arrmixValues['category_id'] ) ) $this->setCategoryId( $arrmixValues['category_id'] );
		if( true == isset( $arrmixValues['settings_template_id'] ) ) $this->setSettingsTemplateId( $arrmixValues['settings_template_id'] );

		return;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = CStrings::strTrimDef( $strPropertyName, 240, NULL, true );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function setName( $strName ) {
		$this->m_strName = CStrings::strTrimDef( $strName, 240, NULL, true );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function setDescription( $strDescription ) {
		$this->m_strDescription = CStrings::strTrimDef( $strDescription, 240, NULL, true );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function setMaintenanceProblemTypeId( $intMaintenanceProblemTypeId ) {
		$this->m_intMaintenanceProblemTypeId = $intMaintenanceProblemTypeId;
	}

	public function getMaintenanceProblemTypeId() {
		return $this->m_intMaintenanceProblemTypeId;
	}

	public function setLaborStandardHours( $intLaborStandardHours ) {
		$this->m_intLaborStandardHours = $intLaborStandardHours;
	}

	public function getLaborStandardHours() {
		return $this->m_intLaborStandardHours;
	}

	public function setMaintenanceLocationId( $intMaintenanceLocationId ) {
		$this->m_intMaintenanceLocationId = $intMaintenanceLocationId;
	}

	public function getMaintenanceLocationId() {
		return $this->m_intMaintenanceLocationId;
	}

	public function setPropertyMaintenanceLocationProblemId( $intPropertyMaintenanceLocationProblemId ) {
		$this->m_intPropertyMaintenanceLocationProblemId = $intPropertyMaintenanceLocationProblemId;
	}

	public function getPropertyMaintenanceLocationProblemId() {
		return $this->m_intPropertyMaintenanceLocationProblemId;
	}

	public function setParentMaintenanceProblemId( $intParentMaintenanceProblemId ) {
		$this->m_intParentMaintenanceProblemId = $intParentMaintenanceProblemId;
	}

	public function getParentMaintenanceProblemId() {
		return $this->m_intParentMaintenanceProblemId;
	}

	public function setLocations( $strLocations ) {
		$this->m_strLocations = $strLocations;
	}

	public function getLocations() {
		return $this->m_strLocations;
	}

  	public function setProblemActionsCount( $intProblemActionsCount ) {
		$this->m_intProblemActionsCount = $intProblemActionsCount;
	}

	public function getProblemActionsCount() {
		return $this->m_intProblemActionsCount;
	}

	public function setCategoryName( $strCategoryName ) {
		$this->m_strCategoryName = $strCategoryName;
	}

	public function getCategoryName() {
		return $this->m_strCategoryName;
	}

	public function setShowOnResidentPortal( $intShowOnResidentPortal ) {
		$this->m_intShowOnResidentPortal = $intShowOnResidentPortal;
	}

	public function getShowOnResidentPortal() {
		return $this->m_intShowOnResidentPortal;
	}

	public function setIntegrationClientTypeName( $strIntegrationClientTypeName ) {
		$this->m_strIntegrationClientTypeName = $strIntegrationClientTypeName;
	}

	public function getIntegrationClientTypeName() {
		return $this->m_strIntegrationClientTypeName;
	}

	public function setCategoryId( $intCategoryId ) {
		$this->m_intCategoryId = $intCategoryId;
	}

	public function getCategoryId() {
		return $this->m_intCategoryId;
	}

	public function setProblemAssociatedLocationIds( $arrintJsonLocationIds ) {
		$this->m_arrintProblemAssociatedLocationIds = json_decode( $arrintJsonLocationIds );
	}

	public function getProblemAssociatedLocationIds() {
		return $this->m_arrintProblemAssociatedLocationIds;
	}

	public function setProblemAssociatedLocationNames( $arrstrJsonLocationNames ) {
		$this->m_arrstrProblemAssociatedLocationNames = json_decode( $arrstrJsonLocationNames );
	}

	public function getProblemAssociatedLocationNames() {
		return $this->m_arrstrProblemAssociatedLocationNames;
	}

	public function setSettingsTemplateId( $intSettingsTemplateId ) {
		$this->m_intSettingsTemplateId = $intSettingsTemplateId;
	}

	public function getSettingsTemplateId() {
		return $this->m_intSettingsTemplateId;
	}

	// Validation Functions

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceProblemId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyMaintenanceProblemMaintenanceTemplates( $objDatabase ) {
		$boolIsValid = true;

		if( false == is_null( $this->getMaintenanceProblemId() ) ) {
			$arrobjPropertyMaintenanceTemplates = \Psi\Eos\Entrata\CPropertyMaintenanceTemplates::createService()->fetchPropertyMaintenanceTemplatesByCidByPropertyIdByMaintenanceProblemId( $this->getCid(), $this->getPropertyId(), $this->getMaintenanceProblemId(), $objDatabase );

			if( true == valArr( $arrobjPropertyMaintenanceTemplates ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_problem_id', 'Maintenance template(s) exists for the maintenance problem.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valPropertyMaintenanceProblemInspectionChecklists( $objDatabase ) {
		$boolIsValid = true;

		if( false == is_null( $this->getMaintenanceProblemId() ) ) {
			$arrobjInspectionForm = \Psi\Eos\Entrata\CInspectionForms::createService()->fetchInspectionFormsByMaintenanceProblemIdByPropertyIdByCid( $this->getMaintenanceProblemId(), $this->getPropertyId(), $this->getCid(), $objDatabase );

			if( true == valArr( $arrobjInspectionForm ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_problem_id', 'Inspection checklist(s) exists for the Maintenance problem.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
					$boolIsValid &= $this->valPropertyMaintenanceProblemMaintenanceTemplates( $objDatabase );
				break;

			case VALIDATE_DELETE:
					$boolIsValid &= $this->valPropertyMaintenanceProblemMaintenanceTemplates( $objDatabase );
					$boolIsValid &= $this->valPropertyMaintenanceProblemInspectionChecklists( $objDatabase );
				break;

			case 'validate_bulk_update':
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	// Fetch Functions

	public function fetchPropertyMaintenanceProblems( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyMaintenanceProblems::createService()->fetchPropertyMaintenanceProblemsByPropertyMaintenanceProblemIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	// Other Functions

	public function deleteMaintenanceProblem( $intCurrentUserId, $objDatabase ) {

		/**
		 *	Comment: DELETE MAINTENANCE PROBLEM
		 *	1. Delete rates function
		 *	2. Hard delete rate associations
		 *	3. Hard delete property maintenance problem
		 */

		// Rates Criteria

		$objRatesCriteria	= new CRatesCriteria();

		$objRatesCriteria->setCid( $this->getCid() );
		$objRatesCriteria->setPropertyId( $this->getPropertyId() );
		$objRatesCriteria->setArCascadeId( CArCascade::PROPERTY );

		$objRatesCriteria->setArOriginId( CArOrigin::MAINTENANCE );
		$objRatesCriteria->setMaintenanceProblemId( $this->getMaintenanceProblemId() );

		$objRatesLibrary	= new CRatesLibrary( $objRatesCriteria );

		// Rate Associations
		$arrobjRateAssociations = ( array ) \Psi\Eos\Entrata\CRateAssociations::createService()->fetchRateAssociationsByArCascadeIdByArOriginIdByArOriginReferenceIdByPropertyIdByCid( CArCascade::PROPERTY, CArOrigin::MAINTENANCE, $this->getId(), $this->getPropertyId(), $this->getCid(), $objDatabase );

		switch( NULL ) {
			default:
				$boolIsValid = true;

				if( false == $objRatesLibrary->deleteRates( $intCurrentUserId, $objDatabase ) ) {
					$boolIsValid &= false;
					break;
				}

				foreach( $arrobjRateAssociations as $objRateAssociation ) {

					if( false == $objRateAssociation->delete( $intCurrentUserId, $objDatabase ) ) {
						$boolIsValid &= false;
						break 2;
					}
				}

				if( false == self::delete( $intCurrentUserId, $objDatabase ) ) {
					$boolIsValid &= false;
					break;
				}
		}

		return $boolIsValid;
	}

}
?>