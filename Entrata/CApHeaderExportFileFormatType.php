<?php

class CApHeaderExportFileFormatType extends CBaseApHeaderExportFileFormatType {

	const LEGACY_WF_ARP_INBOUND 		= 1;
	const STANDARD_ARP_INBOUND 			= 2;
	const BANK_OF_AMERICA_SB			= 3;
	const BANK_OF_AMERICA_NATIONAL 		= 4;
	const BANK_OF_AMERICA_IDS 			= 5;
	const JP_MORGAN_CHASE 				= 6;
	const JP_MORGAN_CHASE_ARP			= 7;
	const FROST_BANK 					= 8;
	const FROST_BANK_ANSI 				= 9;
	const WELLS_FARGO_ASCII 			= 10;
	const JP_MORGAN_CHASE_CP 			= 11;
	const US_BANK_CSV 			        = 12;
	const US_BANK_TXT 			        = 13;
	const JP_MORGAN_CHASE_PCL 			= 15;
	const CNB_CNZ = 16;
	const CNB_TXT = 17;

	const AP_HEADER_EXPORT_FILE_FORMAT_CSV = 1;
	const AP_HEADER_EXPORT_FILE_FORMAT_TXT = 2;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApHeaderExportBatchTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>