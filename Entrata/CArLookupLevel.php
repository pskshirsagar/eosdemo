<?php
class CArLookupLevel extends CBaseArLookupLevel {

	const MIN_ITEM_COUNT			= 1;
	const MAX_ITEM_COUNT			= 9999;

    public function valId() {
        $boolIsValid = true;

        if( true == is_null( $this->getId() ) || 0 >= ( int ) $this->getId() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( NULL, 'id', __( 'Valid id is required.' ), NULL ) );
        }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) || 0 >= ( int ) $this->getCid() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( NULL, 'cid', __( 'Valid client id is required.' ), NULL ) );
        }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyId() ) || 0 >= ( int ) $this->getPropertyId() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( NULL, 'property_id', __( 'Valid property id is required.' ), NULL ) );
        }

        return $boolIsValid;
    }

    public function valArLookupTableId() {
        $boolIsValid = true;

        if( true == is_null( $this->getArLookupTableId() ) || 0 >= ( int ) $this->getArLookupTableId() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( NULL, 'ar_lookup_table_id', __( 'Valid Ar lookup table id is required.' ), NULL ) );
        }

        return $boolIsValid;
    }

    public function valMinItemCount() {
        $boolIsValid = true;

        if( true == is_null( $this->getMinItemCount() ) || 0 >= ( int ) $this->getMinItemCount() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( NULL, 'min_item_count', __( 'Min item count must be greater then zero.' ), NULL ) );
        }

        return $boolIsValid;
    }

    public function valMaxItemCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRateAmount( $intArFormulaId ) {
        $boolIsValid = true;

        if( true == in_array( $intArFormulaId, [ CArFormula::PER_ITEM_WITH_VOLUME_PRICING, CArFormula::PER_ITEM_WITH_TIERED_PRICING ] ) && ( true == is_null( $this->getRateAmount() ) || 0 >= ( int ) $this->getRateAmount() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( NULL, 'rate_amount', __( 'Rate amount must be greater then zero.' ), NULL ) );
        }

        return $boolIsValid;
    }

    public function valRatePercent( $intArFormulaId ) {
        $boolIsValid = true;

        if( false == in_array( $intArFormulaId, [ CArFormula::PER_ITEM_WITH_VOLUME_PRICING, CArFormula::PER_ITEM_WITH_TIERED_PRICING ] ) && ( true == is_null( $this->getRatePercent() ) || 0 >= ( float ) $this->getRatePercent() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( NULL, 'rate_percent', __( 'Rate percent must be greater then zero.' ), NULL ) );
        }

        return $boolIsValid;
    }

    public function valIsDefault() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $intArFormulaId = CArFormula::FIXED_AMOUNT ) {
        $boolIsValid = true;

        switch( $strAction ) {

        	case VALIDATE_INSERT:
        		$boolIsValid &= $this->valCid();
        		$boolIsValid &= $this->valPropertyId();
        		$boolIsValid &= $this->valMinItemCount();
        		$boolIsValid &= $this->valMaxItemCount();
        		$boolIsValid &= $this->valRateAmount( $intArFormulaId );
        		$boolIsValid &= $this->valRatePercent( $intArFormulaId );
				break;

        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valCid();
        		$boolIsValid &= $this->valPropertyId();
        		$boolIsValid &= $this->valArLookupTableId();
        		$boolIsValid &= $this->valMinItemCount();
        		$boolIsValid &= $this->valMaxItemCount();
        		$boolIsValid &= $this->valRateAmount( $intArFormulaId );
        		$boolIsValid &= $this->valRatePercent( $intArFormulaId );
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>