<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileBatchFiles
 * Do not add any new functions to this class.
 */

class CFileBatchFiles extends CBaseFileBatchFiles {

	public static function fetchFilesBatchFilesByCidByFileBatchId( $intCid, $intFileBatchId, $objDatabase ) {
		$strSql = '	SELECT fbf.*
					FROM file_batch_files fbf
					WHERE
							fbf.cid = ' . ( int ) $intCid . '
						AND fbf.file_batch_id = ' . ( int ) $intFileBatchId . '
						AND fbf.deleted_by IS NULL
						AND fbf.deleted_on Is NULL';

		return self::fetchFileBatchFiles( $strSql, $objDatabase );
	}

	public static function fetchSimpleFilesBatchFilesWithEntityByCidByFileBatchId( $intCid, $intFileBatchId, $objDatabase ) {
		$strSql = '	SELECT
						fbf.id as file_batch_file_id,
						fa.id as file_association_id,
						f.id as file_id,
						util_get_translated( \'title\', f.title, f.details ),
						ft.name file_type_name,
						fa.id file_association_id,
						fa.created_on,
						cu.username created_by_name,
						ft.id as file_type_id,
						ft.system_code,
						c.id as entity_id,
						c.name_first || \' \' || c.name_last as entity_name
					FROM file_batch_files fbf
						JOIN files f ON ( fbf.file_id = f.id and f.cid = fbf.cid )
						JOIN file_associations fa ON ( fbf.file_association_id = fa.id AND fa.cid = f.cid )
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = fa.cid )
						LEFT JOIN company_users cu ON ( fa.created_by = cu.id AND cu.cid = fa.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						JOIN customers c ON ( fa.customer_id = c.id and c.cid = fa.cid )
					WHERE
							fbf.cid = ' . ( int ) $intCid . '
						AND fbf.file_batch_id = ' . ( int ) $intFileBatchId . '
						AND fbf.deleted_by IS NULL
						AND fbf.deleted_on IS NULL
					ORDER BY ' . $objDatabase->getCollateSort( 'c.name_first' ) . ',' . $objDatabase->getCollateSort( 'c.name_last' );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFileBatchFilesListByFileBatchIdByCid( $intFileBatchId, $intCid, $intPage, $intOffSet, $objDatabase ) {
		$arrmixSqlFilters	= [ 'join' => [], 'where' => [] ];

		$strSql = '	SELECT
						fbf.file_batch_id,
						f.id as file_id,
						CASE
							WHEN f.title IS NULL
								THEN f.file_name
							ELSE
								util_get_translated( \'title\', f.title, f.details )
						END  as title,
						ft.name as file_type,
						COALESCE( ce.name_first || \' \' || ce.name_last, cu.username ) as created_by_username
					FROM file_batch_files fbf
						JOIN files f ON ( fbf.file_id = f.id and f.cid = fbf.cid )
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.cid = f.cid )
						JOIN file_metadatas fm ON ( fm.file_id = f.id AND fm.cid = f.cid )
						LEFT JOIN company_users cu ON ( ( cu.cid = fbf.cid ) AND ( cu.id = f.created_by ) AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid )
					WHERE
							fbf.cid = ' . ( int ) $intCid . '
						AND fbf.file_batch_id = ' . ( int ) $intFileBatchId . '
						AND fbf.deleted_by IS NULL
						AND fbf.deleted_on IS NULL';

		if( true == is_numeric( $intPage ) && true == is_numeric( $intOffSet ) ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffSet . ' LIMIT ' . ( int ) $intPage;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFileBatchFilesByIdsByCid( $arrintFileBatchFileIds, $intCid, $objDatabase, $boolIsIncludeDeleted = false ) {
		$strDeleteConditions = '';

		if( false == $boolIsIncludeDeleted ) {
			$strDeleteConditions = 'AND fbf.deleted_by IS NULL
									AND fbf.deleted_on IS NULL';
		}

		$strSql = '	SELECT
						*
					FROM file_batch_files fbf
					WHERE
						fbf.cid = ' . ( int ) $intCid . '
						AND fbf.id IN ( ' . implode( ',', $arrintFileBatchFileIds ) . ' )
						' . $strDeleteConditions . ';';

		return self::fetchFileBatchFiles( $strSql, $objDatabase );
	}

	public static function fetchTopBatchFileByFileBatchIdByCid( $arrintFileBatchIds, $intCid, $objDatabase ) {
		$strSql = '	SELECT
						fbf.file_batch_id, ft.name, f.id as file_id
					FROM file_batch_files fbf
						JOIN files f ON (f.id = fbf.file_id AND f.cid = fbf.cid)
						JOIN file_types ft ON ( f.file_type_id = ft.id AND ft.cid = fbf.cid)
					WHERE
						fbf.cid = ' . ( int ) $intCid . '
						AND fbf.file_batch_id IN ( ' . implode( ',', $arrintFileBatchIds ) . ' )
						AND fbf.deleted_by IS NULL
						AND fbf.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCountFileBatchFileByFileBatchIdsByCid( $arrintFileBatchIds, $intCid, $objDatabase, $boolIsIncludeFileTypes = false ) {

		$strWhereSql = '';
		if( true == $boolIsIncludeFileTypes ) {
			$strWhereSql .= ' AND ( (ft.system_code IN ( \'' . implode( '\',\'', CFileType::$c_arrstrDocStorageFileTypes ) . '\' ) ) OR ( ft.is_system = 0 AND ft.system_code IS NULL ) ) ';
		}

		$strSql = 'SELECT
						COUNT( 1 ) AS total_documents,
						fbf.file_batch_id as file_batch_id
					FROM file_batch_files fbf
						JOIN files f ON ( f.id = fbf.file_id AND f.cid = fbf.cid )
						JOIN file_metadatas fm ON ( fm.file_id = f.id AND fm.cid = f.cid )
						JOIN file_types ft ON ( ft.id = f.file_type_id AND f.cid = ft.cid )
					WHERE
						fbf.cid = ' . ( int ) $intCid . '
						AND fbf.file_batch_id IN ( ' . implode( ',', $arrintFileBatchIds ) . ' ) ' . $strWhereSql . '
		                AND fbf.deleted_by IS NULL
		                AND fbf.deleted_on IS NULL
					GROUP BY fbf.file_batch_id,fbf.cid';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCountFileBatchFileByFileBatchIdByCid( $intFileBatchId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						COUNT( DISTINCT fbf.file_id ) AS total_documents,
						fbf.file_batch_id as file_batch_id
					FROM file_batch_files fbf
						JOIN file_metadatas fm ON ( fm.file_id = fbf.file_id AND fm.cid = fbf.cid )
					WHERE
						fbf.cid = ' . ( int ) $intCid . '
						AND fbf.file_batch_id  = ' . ( int ) $intFileBatchId . '
						AND fbf.deleted_by IS NULL
						AND fbf.deleted_on IS NULL
					GROUP BY fbf.file_batch_id,fbf.cid';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFileBatchFileByFileBatchIdByCid( $intFileBatchId, $intCid, $objDatabase, $boolIsRecoveryDocuments = false ) {

		$strDeletedConditions = 'AND fbf.deleted_by IS NULL
								 AND fbf.deleted_on IS NULL';

		if( true == $boolIsRecoveryDocuments ) {
			$strDeletedConditions = 'AND fbf.deleted_by IS NOT NULL
								     AND fbf.deleted_on IS NOT NULL';
		}

		$arrintEntityTypesShowByLastNameFirstName = [
			CEntityType::RESIDENT,
			CEntityType::LEAD,
			CEntityType::EMPLOYEE,
			CEntityType::BLUE_MOON_DOCUMENTS
		];

		$strSql = '	SELECT
						fm.entity_type_id,
		                CASE 
		                    WHEN entity_type_id IN (' . implode( ', ', $arrintEntityTypesShowByLastNameFirstName ) . ' ) 
								THEN CONCAT_WS (\', \', NULLIF(fm.data->>\'person_last_name1\', \'\') , NULLIF(fm.data->>\'person_first_name1\', NULL) )
							ELSE 
								entity_name
						END AS entity_name,
						fm.entity_id,
						fbf.id AS file_batch_file_id,
						fbf.file_id AS file_id,
						fbf.file_association_id AS file_association_id,
						ft.name AS file_type_name,
						util_get_translated(\'data\', NULL, fm.details)::jsonb->> \'external_source_system\' as external_source_system,
						fm.data->>\'document_file_type_id\' AS file_type_id,
						util_get_translated(\'data\', NULL, fm.details)::jsonb->> \'unit_number_cache\' as unit_number_cache,
						util_get_translated(\'data\', NULL, fm.details)::jsonb->> \'lease_id\' as lease_id,	
						CASE WHEN fm.data->>\'document_name\' IS NULL THEN fm.data->>\'file_name\' ELSE fm.data->>\'document_name\'	END  as title,
						fm.created_by AS file_updated_by,
						fm.created_on AS file_updated_on,
						COALESCE( ce.name_first || \' \' || ce.name_last, cu.username ) as file_created_by_username
					FROM file_batch_files fbf
						JOIN file_metadatas fm ON ( fm.file_id = fbf.file_id AND fm.cid = fbf.cid )
						JOIN file_types ft ON ( ft.id = ( NULLIF( ( fm.data ->> \'document_file_type_id\' ), \'\' ) )::INTEGER AND ft.cid = fm.cid )						
						LEFT JOIN company_users cu ON ( cu.cid = fbf.cid AND cu.id = fm.created_by AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid )
					WHERE
						fbf.cid = ' . ( int ) $intCid . '
						AND fbf.file_batch_id = ' . ( int ) $intFileBatchId . '
						' . $strDeletedConditions . '
					ORDER BY entity_type_id, entity_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFileBatchFilesByCidByFileBatchId( $intCid, $intFileBatchId, $objDatabase, $boolIsIncludeSoftDeleted = false ) {

		if( false == $boolIsIncludeSoftDeleted ) {
			$strDeleteCondition = 'AND fbf.deleted_by IS NULL
									AND fbf.deleted_on IS NULL';
		}

		$strSql = ' SELECT
						fbf.*
					FROM
						file_batch_files fbf
					WHERE
						fbf.cid = ' . ( int ) $intCid . '
						AND fbf.file_id IS NOT NULL
						AND fbf.file_batch_id = ' . ( int ) $intFileBatchId
						. $strDeleteCondition . ';';

		return self::fetchFileBatchFiles( $strSql, $objDatabase );
	}

	public static function fetchFileBatchFilesByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						file_batch_files
					WHERE
						file_id = ' . ( int ) $intFileId . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchFileBatchFiles( $strSql, $objDatabase );
	}

	public static function fetchRemovedFileBatchFilesByCidByFileBatchId( $intCid, $intFileBatchId, $objDatabase ) {

		$strSql = ' SELECT
						fbf.*
					FROM
						file_batch_files fbf
					WHERE
						fbf.cid = ' . ( int ) $intCid . '
						AND fbf.file_id IS NOT NULL
						AND fbf.file_batch_id = ' . ( int ) $intFileBatchId . ' 
		                AND fbf.deleted_by IS NOT NULL
						AND fbf.deleted_on IS NOT NULL ;';

		return self::fetchFileBatchFiles( $strSql, $objDatabase );
	}

}
?>