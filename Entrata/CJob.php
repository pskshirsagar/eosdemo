<?php

class CJob extends CBaseJob {

	protected $m_boolHasPhases;

	protected $m_arrmixImportFiles;
	protected $m_arrmixBudgetApDetails;
	protected $m_arrmixUnitTypeBudgetApDetails;
	protected $m_arrmixLocationBudgetApDetails;
	protected $m_strApprovalNote;
	protected $m_strImportFileName;
	protected $m_intImportJobCostCodeCount;

	protected $m_arrobjJobPhases;

	const MAX_LENGTH           = 50;
	const MAX_UNIT_TYPE_LENGTH = 120;
	const HAS_PHASES           = 1;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valJobCategoryId() {
		$boolIsValid = true;

		if( CJobType::JOB_TYPE_TEMPLATE != $this->getJobTypeId() && false == valId( $this->getJobCategoryId() ) ) {
			$boolIsValid = false;
			$strErrorMessage = __( 'Job category is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'job_category_id', $strErrorMessage ) );
		}

		return $boolIsValid;
	}

	public function valJobStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$strErrorMessage = __( 'Property is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', $strErrorMessage ) );
		}

		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;

		if( true == is_null( $this->getLeaseId() ) ) {
			$boolIsValid = false;
			$strErrorMessage = __( 'Lease/Tenant is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', $strErrorMessage ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$strErrorMessage = __( 'Name is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'job_name', $strErrorMessage ) );
		}

		return $boolIsValid;
	}

	public function valDefaultRetentionPercent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginalStartDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getOriginalStartDate() ) ) {
			$boolIsValid = false;
			$strErrorMessage = __( 'Planned start date is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_start_date', $strErrorMessage ) );
		} elseif( CJobType::JOB_TYPE_TEMPLATE != $this->getJobTypeId() && false == CValidation::validateDate( $this->getOriginalStartDate() ) ) {
			$boolIsValid = false;
			$strErrorMessage = __( 'Planned start date should be in mm/dd/yyyy format.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_start_date', $strErrorMessage ) );
		}

		return $boolIsValid;
	}

	public function valOriginalEndDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getOriginalEndDate() ) ) {
			$boolIsValid = false;
			$strErrorMessage = __( 'Planned completion date is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_end_date', $strErrorMessage ) );
		} elseif( CJobType::JOB_TYPE_TEMPLATE != $this->getJobTypeId() && false == CValidation::validateDate( $this->getOriginalEndDate() ) ) {
			$boolIsValid = false;
			$strErrorMessage = __( 'Planned completion date should be in mm/dd/yyyy format.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_end_date', $strErrorMessage ) );
		}

		if( false == is_null( $this->getOriginalStartDate() ) && false == is_null( $this->getOriginalEndDate() ) && ( strtotime( $this->getOriginalStartDate() ) > strtotime( $this->getOriginalEndDate() ) ) ) {
			$boolIsValid = false;
			$strErrorMessage = __( 'Planned completion date cannot be earlier than Planned start date.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_end_date', $strErrorMessage ) );
		}

		return $boolIsValid;
	}

	public function valActualStartDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getActualStartDate() ) ) {
			$boolIsValid = false;
			$strErrorMessage = __( 'Actual Start Date should not be blank.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_start_date', $strErrorMessage ) );
		}

		if( false == is_null( $this->getActualStartDate() ) && false == CValidation::validateDate( $this->getActualStartDate() ) ) {
			$boolIsValid = false;
			$strErrorMessage = __( 'Actual start date should be in mm/dd/yyyy format.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_start_date', $strErrorMessage ) );
		}

		return $boolIsValid;
	}

	public function valActualEndDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getActualEndDate() ) ) {
			$boolIsValid = false;
			$strErrorMessage = __( 'Actual Completion Date should not be blank.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_end_date', $strErrorMessage ) );
		}

		if( false == is_null( $this->getActualEndDate() ) && false == CValidation::validateDate( $this->getActualEndDate() ) ) {
			$boolIsValid = false;
			$strErrorMessage = __( 'Actual completion date should be in mm/dd/yyyy format.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_end_date', $strErrorMessage ) );
		}

		if( false == is_null( $this->getActualStartDate() ) && false == is_null( $this->getActualEndDate() ) && ( strtotime( $this->getActualStartDate() ) > strtotime( $this->getActualEndDate() ) ) ) {
			$boolIsValid = false;
			$strErrorMessage = __( 'Actual completion date cannot be earlier than Actual start date.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_end_date', $strErrorMessage ) );
		}

		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCancelledBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCancelledOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsJobExists( $objDatabase ) {
		$boolIsValid = true;

		$strWhere = 'WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND property_id = ' . ( int ) $this->getPropertyId() . '
						AND name = \'' . addslashes( $this->getName() ) . '\'
						AND id <> ' . ( int ) $this->getId();

		if( 0 < CJobs::fetchJobCount( $strWhere, $objDatabase ) ) {
			$strErrorMessage = ( CJobType::JOB_TYPE_TEMPLATE == $this->getJobTypeId() ) ? 'Template name already exists.' : 'Job name already exists.';
			$strErrorMessage = __( $strErrorMessage );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_job_exists', $strErrorMessage ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPostRenovationMakeReady() {
		$boolIsValid = true;

		if( CJobType::JOB_TYPE_ROLLING_RENOVATION == $this->getJobTypeId() && true == is_null( $this->getMaintenanceTemplateId() ) ) {
				$boolIsValid = false;
				$strErrorMessage = __( 'Post renovation make ready is required.' );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_template_id', $strErrorMessage ) );
		}
		return $boolIsValid;

	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valJobCategoryId();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPostRenovationMakeReady();
				if( false == $this->getHasPhases() ) {
					$boolIsValid &= $this->valOriginalStartDate();
					$boolIsValid &= $this->valOriginalEndDate();
				}
				$boolIsValid &= $this->valIsJobExists( $objDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valJobCategoryId();
				$boolIsValid &= $this->valOriginalStartDate();
				$boolIsValid &= $this->valOriginalEndDate();
				if( CJobStatus::IN_PROGRESS == $this->getJobStatusId() ) {
					$boolIsValid &= $this->valActualStartDate();
				}
				if( CJobStatus::COMPLETED == $this->getJobStatusId() ) {
					$boolIsValid &= $this->valActualEndDate();
				}
				$boolIsValid &= $this->valIsJobExists( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			case 'import_external_budget_file':
				$boolIsValid &= $this->valInsertImportFile( $objDatabase );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function valInsertImportFile( $objDatabase ) {

		$boolIsValid 					= true;
		$strExtension					= NULL;
		$strUploadTempDir				= PATH_NON_BACKUP_MOUNTS_GLOBAL_TMP;
		$strUploadMaxFilesize			= ini_get( 'upload_max_filesize' );
		$intMaxUpload					= ( int ) str_replace( 'M', '', $strUploadMaxFilesize ) * 1024 * 1024;

		// Validate uploaded CSV file.
		$arrmixImportFiles = $this->getImportFiles();

		if( false == valArr( $arrmixImportFiles ) || UPLOAD_ERR_NO_FILE == $arrmixImportFiles['error'] ) {
			$strErrorMessage = __( 'Select a file to upload.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'no_file', $strErrorMessage ) );
			return false;
		}

		if( true == array_key_exists( 'name', $arrmixImportFiles ) && true == valStr( $arrmixImportFiles['name'] ) ) {
			$strExtension = pathinfo( $arrmixImportFiles['name'], PATHINFO_EXTENSION );
		}

		$arrstrFileHeaders			= [ 'job_cost_code', 'phase_name', 'period', 'amount', 'unit_type', 'property_location' ];
		$arrmixAllowedExtensions	= [ 'csv' => [ 'mimes' => [ 'application/vnd.ms-excel' ], 'exts' => [ 'csv' ] ] ];

		if( false != valStr( $strExtension ) && false == array_key_exists( $strExtension, $arrmixAllowedExtensions ) ) {
			$strErrorMessage = __( 'Cannot upload other than CSV file.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_type', $strErrorMessage ) );
			return false;
		}

		if( $intMaxUpload < $arrmixImportFiles['size'] || UPLOAD_ERR_OK != $arrmixImportFiles['error'] ) {
			$strErrorMessage = __( 'Files larger than {%s, strUploadMaxFilesize}B cannot be uploaded.', [ 'strUploadMaxFilesize' => $strUploadMaxFilesize ] );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_size', $strErrorMessage ) );
			return false;
		}

		$strTempUri = ( string ) uniqid( '', false ) . sprintf( '%3d', mt_rand( 100, 999 ) );

		if( false == move_uploaded_file( $arrmixImportFiles['tmp_name'], $strUploadTempDir . $strTempUri ) || UPLOAD_ERR_NO_TMP_DIR == $arrmixImportFiles['error'] ) {
			$strErrorMessage = __( 'file_upload {%s, strFileName} could not be moved to the temporary directory.', [ 'strFileName' => $arrmixImportFiles['name'] ] );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strErrorMessage ) );
			return false;
		}

		// Feed data from CSV file.
		$arrmixRawData	= ( array ) $this->parseCsvFileImport( $strUploadTempDir . $strTempUri );
		if( false == valArr( $arrmixRawData ) || ( true == array_key_exists( 0, $arrmixRawData ) && $arrstrFileHeaders != array_keys( $arrmixRawData[0] ) ) ) {
			$boolIsValid &= false;
		} else {
			// Check for blank lines and columns
			foreach( $arrmixRawData as $intRowNumber => $arrmixRow ) {

				$strRowContents	= implode( '', array_values( $arrmixRow ) );
				if( false == valStr( $strRowContents ) ) {
					unset( $arrmixRawData[$intRowNumber] );
					continue;
				}

				foreach( array_keys( $arrmixRow ) as $strArrayKey ) {
					if( false == valStr( $strArrayKey ) ) {
						$boolIsValid &= false;
						break 2;
					}
				}
			}
		}

		if( false == $boolIsValid ) {
			$strErrorMessage = __( 'Invalid format. Please refer to sample data format (CSV).' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invalid format', $strErrorMessage ) );
			return $boolIsValid;
		}

		if( false == valArr( $arrmixRawData ) ) {
			$strErrorMessage = __( 'No record found. Please refer to sample data format (CSV).' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invalid format', $strErrorMessage ) );
			return false;
		}

		$arrobjJobUnitTypes                = rekeyObjects( 'UnitTypeId', ( array ) CJobUnitTypes::fetchJobUnitTypesByJobIdByCid( $this->getId(), $this->getCid(), $objDatabase ) );
		$arrobjJobMaintenanceLocations     = rekeyObjects( 'MaintenanceLocationId', ( array ) \Psi\Eos\Entrata\CJobMaintenanceLocations::createService()->fetchJobMaintenanceLocationsDetailByJobIdByCid( $this->getId(), $this->getCid(), $objDatabase ) );
		$arrmixApCodes                     = rekeyArray( 'id', ( array ) \Psi\Eos\Entrata\CApCodes::createService()->fetchAllActiveJobCostCodesByCid( $this->getCid(), $objDatabase ) );
		$arrmixJobPhases                   = $this->fetchJobPhases( $objDatabase );

		$arrmixJobPhase                    = [];
		$arrmixApDetails                   = [];
		$arrmixUnitTypeApDetails           = [];
		$arrmixLocationApDetails           = [];
		$arrmixValJobCostCodes             = [];
		$arrstrJobPhaseNames               = [];
		$arrstrJobUnitTypeNames            = [];
		$arrstrJobMaintenanceLocationNames = [];
		$arrintImortJobCostCodeIds         = [];

		$strPostMonthRegEx                 = '~^(0?[1-9]{1}|1[012])\/\d{4}$~';
		$strJobUnitTypeRegEx               = '/[@#!]/';

		$objPhaseStartDate                 = NULL;
		$objPhaseEndDate                   = NULL;

		$fltTotalBudget                    = 0;

		$boolValidatedTotalBudget          = false;

		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrmixJobPhases ) ) {
			$arrmixJobPhase    = array_pop( $arrmixJobPhases );
			$objPhaseStartDate = ( new DateTime( $arrmixJobPhase['original_start_date'] ) )->modify( 'first day of this month' );
			$objPhaseEndDate   = ( new DateTime( $arrmixJobPhase['original_end_date'] ) )->modify( 'first day of this month' );
		} else {
			$arrstrJobPhaseNames = array_map( function ( $arrmixJobPhase ) {
				return \Psi\CStringService::singleton()->strtolower( $arrmixJobPhase['name'] );
			}, $arrmixJobPhases );
		}

		$arrstrApCodeNames = array_map( function ( $arrmixApCode ) {
			return \Psi\CStringService::singleton()->strtolower( $arrmixApCode['name'] );
		}, $arrmixApCodes );

		$arrstrJobUnitTypeNames = array_map( function ( $objJobUnitType ) {
			return \Psi\CStringService::singleton()->strtolower( $objJobUnitType->getUnitTypeName() );
		}, $arrobjJobUnitTypes );

		$arrstrJobMaintenanceLocationNames = array_map( function ( $objJobMaintenanceLocation ) {
			return \Psi\CStringService::singleton()->strtolower( $objJobMaintenanceLocation->getMaintenanceLocationName() );
		}, $arrobjJobMaintenanceLocations );

		$arrstrInvalidUnitTypesAndPropertyLocations = [];
		$boolValidatedUnitTypeOrPropertyLocation = $this->getIsBudgetByUnitLocation();

		foreach( $arrmixRawData as $intRowNumber => $arrmixRow ) {

			$arrmixValJobCostCode = [];

			$intJobMaintenanceLocationId = 0;
			$intJobUnitTypeId            = 0;

			$strJobCostCode            = trim( $arrmixRow['job_cost_code'] );
			$strPhaseName              = trim( $arrmixRow['phase_name'] );
			$strPeriod                 = trim( $arrmixRow['period'] );
			$fltAmount                 = trim( $arrmixRow['amount'] );
			$strJobUnitType            = trim( $arrmixRow['unit_type'] );
			$strJobMaintenanceLocation = trim( $arrmixRow['property_location'] );

			if( false == $boolValidatedUnitTypeOrPropertyLocation && ( true == valStr( $strJobUnitType ) || true == valStr( $strJobMaintenanceLocation ) ) ) {
				$strErrorMessage = __( 'This job is not configured to budget by unit types or property locations.' );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
				$boolIsValid &= false;
				$boolValidatedUnitTypeOrPropertyLocation = true;
			}

			$boolIsValidPhaseName = true;

			if( false == valStr( $strJobCostCode ) ) {
				$strErrorMessage = __( 'Job cost code is required for row #{%d,intRowNumber}.', [ 'intRowNumber' => $intRowNumber + 2 ] );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
				$boolIsValid &= false;
			} elseif( self::MAX_LENGTH < strlen( $strJobCostCode ) ) {
				$strErrorMessage = __( 'Job cost code should not be more than {%d,intMaxLength} characters for row #{%d,intRowNumber}.', [ 'intMaxlength' => self::MAX_LENGTH, 'intRowNumber' => $intRowNumber + 2 ] );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
				$boolIsValid &= false;
			} elseif( false == in_array( \Psi\CStringService::singleton()->strtolower( $strJobCostCode ), $arrstrApCodeNames ) ) {
				$strErrorMessage = __( 'Job cost code is not valid for row #{%d, intRowNumber}.', [ 'intRowNumber' => $intRowNumber + 2 ] );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
				$boolIsValid &= false;
			}

			if( 1 < \Psi\Libraries\UtilFunctions\count( $arrstrJobPhaseNames ) ) {
				if( false == valStr( $strPhaseName ) ) {
					$strErrorMessage = __( 'Phase name is required for row #{%d, intRowNumber}.', [ 'intRowNumber' => $intRowNumber + 2 ] );
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
					$boolIsValidPhaseName = false;
					$boolIsValid          &= false;
				} elseif( self::MAX_LENGTH < strlen( $strPhaseName ) ) {
					$strErrorMessage = __( 'Phase name should not be more than {%d,intMaxLength} characters for row #{%d,intRowNumber}.', [ 'intMaxlength' => self::MAX_LENGTH, 'intRowNumber' => $intRowNumber + 2 ] );
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
					$boolIsValidPhaseName = false;
					$boolIsValid          &= false;
				} elseif( false == in_array( \Psi\CStringService::singleton()->strtolower( $strPhaseName ), $arrstrJobPhaseNames ) ) {
					$strErrorMessage = __( 'Phase name is not valid for row #{%d, intRowNumber}.', [ 'intRowNumber' => $intRowNumber + 2 ] );
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
					$boolIsValidPhaseName = false;
					$boolIsValid          &= false;
				} else {
					$intJobPhaseId     = array_search( \Psi\CStringService::singleton()->strtolower( $strPhaseName ), $arrstrJobPhaseNames );
					$arrmixJobPhase    = $arrmixJobPhases[$intJobPhaseId];
					$objPhaseStartDate = ( new DateTime( $arrmixJobPhase['original_start_date'] ) )->modify( 'first day of this month' );
					$objPhaseEndDate   = ( new DateTime( $arrmixJobPhase['original_end_date'] ) )->modify( 'first day of this month' );
				}
			}

			if( true == $boolIsValidPhaseName && 't' == $arrmixJobPhase['is_budget_by_period'] ) {
				if( false == valStr( $strPeriod ) ) {
					$strErrorMessage = __( 'Period is required for row #{%d,intRowNumber}.', [ 'intRowNumber' => $intRowNumber + 2 ] );
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
					$boolIsValid &= false;
				} elseif( false == preg_match( $strPostMonthRegEx, $strPeriod ) ) {
					$strErrorMessage = __( 'Invalid period format at row #{%d,intRowNumber}.', [ 'intRowNumber' => $intRowNumber + 2 ] );
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
					$boolIsValid &= false;
				} else {
					$objPeriod = new DateTime( str_replace( '/', '/01/', $strPeriod ) );
					if( $objPhaseStartDate > $objPeriod || $objPhaseEndDate < $objPeriod ) {
						$strErrorMessage = __( 'Period should be within start and end date range of the phase at row #{%d,intRowNumber}.', [ 'intRowNumber' => $intRowNumber + 2 ] );
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
						$boolIsValid &= false;
					}
				}
			} elseif( true == $boolIsValidPhaseName && true == valStr( $strPeriod ) ) {
				$strErrorMessage = __( 'Period is not required at row #{%d,intRowNumber}.', [ 'intRowNumber' => $intRowNumber + 2 ] );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
				$boolIsValid &= false;
			} else {
				$strPeriod = ( true == valObj( $objPhaseStartDate, 'DateTime' ) ) ? sprintf( '%s', $objPhaseStartDate->format( 'm/Y' ) ) : '';
			}

			if( false == valStr( $fltAmount ) || ( true == is_numeric( $fltAmount ) && 0 == $fltAmount ) ) {
				$strErrorMessage = __( 'Amount is required for row #{%d,intRowNumber}.', [ 'intRowNumber' => $intRowNumber + 2 ] );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
				$boolIsValid &= false;
			} elseif( false == is_numeric( $fltAmount ) ) {
				$strErrorMessage = __( 'Invalid budget amount for row #{%d,intRowNumber}.', [ 'intRowNumber' => $intRowNumber + 2 ] );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
				$boolIsValid &= false;
			} elseif( 0 > $fltAmount ) {
				$strErrorMessage = __( 'Invalid budget amount for row #{%d,intRowNumber}. Budget amount should be greater than 0.00.', [ 'intRowNumber' => $intRowNumber + 2 ] );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
				$boolIsValid &= false;
			} else {
				$fltTotalBudget += abs( $fltAmount );
			}

			if( false == $boolValidatedTotalBudget && 1000000000 < $fltTotalBudget ) {
				$boolValidatedTotalBudget = true;
				$strErrorMessage = __( 'The overall budget value can not be more than {%m,strMaxTotalBudget}', [ 'strMaxTotalBudget' => 1000000000 ] );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
				$boolIsValid &= false;
			}

			if( true == $this->getIsBudgetByUnitLocation() ) {
				if( true == valStr( $strJobUnitType ) && self::MAX_UNIT_TYPE_LENGTH < strlen( $strJobUnitType ) ) {
					$strErrorMessage = __( 'Unit type name should not be more than {%d,intMaxUnitTypeLength} characters for row #{%d,intRowNumber}.', [ 'intMaxUnitTypeLength' => self::MAX_UNIT_TYPE_LENGTH, 'intRowNumber' => $intRowNumber + 2 ] );
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
					$boolIsValid &= false;
				} elseif( true == valStr( $strJobUnitType ) && true == preg_match( $strJobUnitTypeRegEx, $strJobUnitType ) ) {
					$strErrorMessage = __( 'Unit type name should not be contains @,# and ! characters for row #{%d,intRowNumber}.', [ 'intRowNumber' => $intRowNumber + 2 ] );
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
					$boolIsValid &= false;
				} elseif( true == valStr( $strJobUnitType ) && false == in_array( \Psi\CStringService::singleton()->strtolower( $strJobUnitType ), $arrstrJobUnitTypeNames ) ) {
					$arrstrInvalidUnitTypesAndPropertyLocations[] = $strJobUnitType;
					$boolIsValid &= false;
				} else {
					$intJobUnitTypeId = array_search( \Psi\CStringService::singleton()->strtolower( $strJobUnitType ), $arrstrJobUnitTypeNames );
				}

				if( true == valStr( $strJobMaintenanceLocation ) && self::MAX_LENGTH < strlen( $strJobMaintenanceLocation ) ) {
					$strErrorMessage = __( 'Property location name should not be more than {%d,intMaxLength} characters for row #{%d,intRowNumber}.', [ 'intMaxLength' => self::MAX_LENGTH, 'intRowNumber' => $intRowNumber + 2 ] );
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
					$boolIsValid &= false;
				} elseif( true == valStr( $strJobMaintenanceLocation ) && false == in_array( \Psi\CStringService::singleton()->strtolower( $strJobMaintenanceLocation ), $arrstrJobMaintenanceLocationNames ) ) {
					$arrstrInvalidUnitTypesAndPropertyLocations[] = $strJobMaintenanceLocation;
					$boolIsValid &= false;
				} else {
					$intJobMaintenanceLocationId = array_search( \Psi\CStringService::singleton()->strtolower( $strJobMaintenanceLocation ), $arrstrJobMaintenanceLocationNames );
				}

				if( true == valStr( $strJobMaintenanceLocation ) && true == valStr( $strJobUnitType ) ) {
					$strErrorMessage = __( 'Job cost code can not be tied to both a Unit type and property location for row #{%d,intRowNumber}.', [ 'intRowNumber' => $intRowNumber + 2 ] );
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
					$boolIsValid &= false;
				}
			}

			if( true == $boolIsValidPhaseName ) {
				$arrmixValJobCostCode['job_cost_code_id']          = array_search( \Psi\CStringService::singleton()->strtolower( $strJobCostCode ), $arrstrApCodeNames );
				$arrmixValJobCostCode['job_phase_id']              = $arrmixJobPhase['id'];
				$arrmixValJobCostCode['is_budget_by_period']       = $arrmixJobPhase['is_budget_by_period'];
				$arrmixValJobCostCode['period']                    = $strPeriod;
				$arrmixValJobCostCode['unit_type_name']            = $strJobUnitType;
				$arrmixValJobCostCode['unit_type_id']              = ( true == valId( $intJobUnitTypeId ) ) ? $intJobUnitTypeId : '';
				$arrmixValJobCostCode['maintenance_location_name'] = $strJobMaintenanceLocation;
				$arrmixValJobCostCode['maintenance_location_id']   = ( true == valId( $intJobMaintenanceLocationId ) ) ? $intJobMaintenanceLocationId : '';

				array_walk( $arrmixValJobCostCodes, function ( $arrmixTempValJobCostCode, $intTempRowNumber ) use ( $arrmixValJobCostCode, $intRowNumber, &$boolIsValid ) {

					$boolIsJobLevelJobCostCode = $boolIsTempJobLevelJobCostCode = $boolIsUnitLevelJobCostCode = $boolIsTempUnitLevelJobCostCode = $boolIsLocationLevelJobCostCode = $boolIsTempLocationLevelJobCostCode = false;

					if( false == valStr( $arrmixValJobCostCode['unit_type_name'] ) && false == valStr( $arrmixValJobCostCode['maintenance_location_name'] ) ) {
						$boolIsJobLevelJobCostCode = true;
					} elseif( true == valStr( $arrmixValJobCostCode['unit_type_name'] ) && false == valStr( $arrmixValJobCostCode['maintenance_location_name'] ) ) {
						$boolIsUnitLevelJobCostCode = true;
					} elseif( false == valStr( $arrmixValJobCostCode['unit_type_name'] ) && true == valStr( $arrmixValJobCostCode['maintenance_location_name'] ) ) {
						$boolIsLocationLevelJobCostCode = true;
					}

					if( false == valStr( $arrmixTempValJobCostCode['unit_type_name'] ) && false == valStr( $arrmixTempValJobCostCode['maintenance_location_name'] ) ) {
						$boolIsTempJobLevelJobCostCode = true;
					} elseif( true == valStr( $arrmixTempValJobCostCode['unit_type_name'] ) && false == valStr( $arrmixTempValJobCostCode['maintenance_location_name'] ) ) {
						$boolIsTempUnitLevelJobCostCode = true;
					} elseif( false == valStr( $arrmixTempValJobCostCode['unit_type_name'] ) && true == valStr( $arrmixTempValJobCostCode['maintenance_location_name'] ) ) {
						$boolIsTempLocationLevelJobCostCode = true;
					}

					$boolHasEqualPeriods    = ( 't' == $arrmixTempValJobCostCode['is_budget_by_period'] ) ? ( $arrmixValJobCostCode['period'] == $arrmixTempValJobCostCode['period'] ) : true;
					$boolHasSameJobCostCode = ( $arrmixValJobCostCode['job_cost_code_id'] == $arrmixTempValJobCostCode['job_cost_code_id'] );
					$boolHasSameJobPhase    = ( $arrmixValJobCostCode['job_phase_id'] == $arrmixTempValJobCostCode['job_phase_id'] );
					$boolHasSameUnitType    = ( true == $boolIsUnitLevelJobCostCode && true == $boolIsTempUnitLevelJobCostCode && $arrmixValJobCostCode['unit_type_name'] == $arrmixTempValJobCostCode['unit_type_name'] );
					$boolHasSameLocation    = ( true == $boolIsLocationLevelJobCostCode && true == $boolIsTempLocationLevelJobCostCode && $arrmixValJobCostCode['maintenance_location_name'] == $arrmixTempValJobCostCode['maintenance_location_name'] );

					if( true == $boolHasSameJobCostCode && true == $boolHasSameJobPhase && true == $boolHasEqualPeriods && true == $boolIsJobLevelJobCostCode && true == $boolIsTempJobLevelJobCostCode ) {
						$strErrorMessage = __( 'The same job cost code and phase are used in row #{%d,intRowNumber} and row #{%d,intTempRowNumber}. A Job Cost Code can only be added once per phase.', [ 'intRowNumber' => $intRowNumber + 2, 'intTempRowNumber' => $intTempRowNumber + 2 ] );
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
						$boolIsValid &= false;
					} elseif( true == $boolHasSameJobCostCode && true == $boolHasSameJobPhase && true == $boolHasEqualPeriods && true == $boolHasSameUnitType ) {
						$strErrorMessage = __( 'The same job cost code,job phase and unit type are used in row #{%d,intRowNumber} and row #{%d,intTempRowNumber}. A Job Cost Code can only be added once per phase per unit type.', [ 'intRowNumber' => $intRowNumber + 2, 'intTempRowNumber' => $intTempRowNumber + 2 ] );
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
						$boolIsValid &= false;
					} elseif( true == $boolHasSameJobCostCode && true == $boolHasSameJobPhase && true == $boolHasEqualPeriods && true == $boolHasSameLocation ) {
						$strErrorMessage = __( 'The same job cost code,job phase and location are used in row #{%d,intRowNumber} and row #{%d,intTempRowNumber. A Job Cost Code can only be added once per phase per location.', [ 'intRowNumber' => $intRowNumber + 2, 'intTempRowNumber' => $intTempRowNumber + 2 ] );
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
						$boolIsValid &= false;
					}

				} );

				$arrmixValJobCostCodes[$intRowNumber] = $arrmixValJobCostCode;
			}

			if( true == $boolIsValid ) {

				if( false == valId( $intJobUnitTypeId ) && false == valId( $intJobMaintenanceLocationId ) ) {
					$arrmixApDetails[$arrmixJobPhase['id']][array_search( \Psi\CStringService::singleton()->strtolower( $strJobCostCode ), $arrstrApCodeNames )][$strPeriod] = $fltAmount;
					$arrintImortJobCostCodeIds[]                                                                                           = array_search( \Psi\CStringService::singleton()->strtolower( $strJobCostCode ), $arrstrApCodeNames );
				} elseif( true == valId( $intJobUnitTypeId ) && false == valId( $intJobMaintenanceLocationId ) ) {
					$arrmixUnitTypeApDetails[$intJobUnitTypeId][$arrmixJobPhase['id']][array_search( \Psi\CStringService::singleton()->strtolower( $strJobCostCode ), $arrstrApCodeNames )][$strPeriod] = $fltAmount;
					$arrintImortJobCostCodeIds[]                                                                                                                      = array_search( \Psi\CStringService::singleton()->strtolower( $strJobCostCode ), $arrstrApCodeNames );
				} elseif( false == valId( $intJobUnitTypeId ) && true == valId( $intJobMaintenanceLocationId ) ) {
					$arrmixLocationApDetails[$intJobMaintenanceLocationId][$arrmixJobPhase['id']][array_search( \Psi\CStringService::singleton()->strtolower( $strJobCostCode ), $arrstrApCodeNames )][$strPeriod] = $fltAmount;
					$arrintImortJobCostCodeIds[]                                                                                                                                 = array_search( \Psi\CStringService::singleton()->strtolower( $strJobCostCode ), $arrstrApCodeNames );
				}

			}
		}

		if( true == valArr( $arrstrInvalidUnitTypesAndPropertyLocations ) ) {
			$strErrorMessage = __( 'The following Unit Type(s) / Property Location(s) have not been associated to this job:<br>{%s,strInvalidUnitTypesAndPropertyLocations}<br>', [ 'strInvalidUnitTypesAndPropertyLocations' => implode( ', ', $arrstrInvalidUnitTypesAndPropertyLocations ) ] );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', $strErrorMessage ) );
		}

		$this->setImportFileName( ( true == valStr( $arrmixImportFiles['name'] ) ) ? $arrmixImportFiles['name'] : '' );
		$this->setImportJobCostCodeCount( \Psi\Libraries\UtilFunctions\count( array_unique( $arrintImortJobCostCodeIds ) ) );
		$this->setBudgetApDetails( $arrmixApDetails );
		$this->setUnitTypeBudgetApDetails( $arrmixUnitTypeApDetails );
		$this->setLocationBudgetApDetails( $arrmixLocationApDetails );

		return $boolIsValid;
	}

	/**
	 * Get Functions
	 */

	public function getHasPhases() {
		return $this->m_boolHasPhases;
	}

	public function getImportFiles() {
		return $this->m_arrmixImportFiles;
	}

	public function getBudgetApDetails() {
		return $this->m_arrmixBudgetApDetails;
	}

	public function getUnitTypeBudgetApDetails() {
		return $this->m_arrmixUnitTypeBudgetApDetails;
	}

	public function getLocationBudgetApDetails() {
		return $this->m_arrmixLocationBudgetApDetails;
	}

	public function getApprovalNote() {
		return $this->m_strApprovalNote;
	}

	public function getImportFileName() {
		return $this->m_strImportFileName;
	}

	public function getImportJobCostCodeCount() {
		return $this->m_intImportJobCostCodeCount;
	}

	/**
	 * Set Functions
	 */

	public function setHasPhases( $boolHasPhases ) {
		$this->m_boolHasPhases = CStrings::strToBool( $boolHasPhases );
	}

	public function setImportFiles( $arrmixImportFiles ) {
		$this->m_arrmixImportFiles = $arrmixImportFiles;
	}

	public function setBudgetApDetails( $arrmixBudgetApDetails ) {
		$this->m_arrmixBudgetApDetails = $arrmixBudgetApDetails;
	}

	public function setUnitTypeBudgetApDetails( $arrmixUnitTypeBudgetApDetails ) {
		$this->m_arrmixUnitTypeBudgetApDetails = $arrmixUnitTypeBudgetApDetails;
	}

	public function setLocationBudgetApDetails( $arrmixLocationBudgetApDetails ) {
		$this->m_arrmixLocationBudgetApDetails = $arrmixLocationBudgetApDetails;
	}

	public function setApprovalNote( $strApprovalNote ) {
		$this->m_strApprovalNote = $strApprovalNote;
	}

	public function setImportFileName( $strImportFileName ) {
		$this->m_strImportFileName = $strImportFileName;
	}

	public function setImportJobCostCodeCount( $intImportJobCostCodeCount ) {
		$this->m_intImportJobCostCodeCount = $intImportJobCostCodeCount;
	}

	// Override setOriginalStartDate and setOriginalEndDate functions. It will acccept date in mm/yyyy format and set it to first or last day with mm/dd/yyyy format

	public function setOriginalStartDate( $strOriginalStartDate ) {

		if( false == valStr( $strOriginalStartDate ) ) {
			$this->m_strOriginalStartDate = NULL;

			return;
		}

		$strDateRegex = '/(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d/';

		if( false == preg_match( $strDateRegex, $strOriginalStartDate ) ) {

			$arrstrStartDate = explode( '/', $strOriginalStartDate );

			if( true == valArr( $arrstrStartDate ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrStartDate ) ) {
				$strOriginalStartDate = date( 'm/d/Y', strtotime( $arrstrStartDate[0] . '/01/' . $arrstrStartDate[1] ) );
			}
		}

		$this->m_strOriginalStartDate = $strOriginalStartDate;

	}

	public function setOriginalEndDate( $strOriginalEndDate ) {

		if( false == valStr( $strOriginalEndDate ) ) {
			$this->m_strOriginalEndDate = NULL;

			return;
		}

		$strDateRegex = '/(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d/';

		if( false == preg_match( $strDateRegex, $strOriginalEndDate ) ) {

			$arrstrEndDate = explode( '/', $strOriginalEndDate );

			if( true == valArr( $arrstrEndDate ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrEndDate ) ) {
				$strOriginalEndDate = date( 'm/d/Y', strtotime( $arrstrEndDate[0] . '/01/' . $arrstrEndDate[1] ) );
			}
		}

		$this->m_strOriginalEndDate = $strOriginalEndDate;
	}

	/**
	 * @return mixed
	 */
	public function getJobPhases() {
		return $this->m_arrobjJobPhases;
	}

	/**
	 * @param mixed $arrobjJobPhases
	 */
	public function setJobPhases( $arrobjJobPhases ) {
		$this->m_arrobjJobPhases = $arrobjJobPhases;
	}

	/**
	 * Create Functions
	 */

	public function createJobPhase() {

		$objJobPhase = new CJobPhase();
		$objJobPhase->setCid( $this->getCid() );
		$objJobPhase->setJobId( $this->getId() );
		$objJobPhase->setFiscalYear( 'NOW()' );

		return $objJobPhase;
	}

	public function createJobFundingSource() {
		$objJobFundingSource = new CJobFundingSource();
		$objJobFundingSource->setCid( $this->getCid() );
		$objJobFundingSource->setJobId( $this->getId() );

		return $objJobFundingSource;
	}

	public function createDrawRequest() {

		$objDrawRequest = new CDrawRequest();
		$objDrawRequest->setJobId( $this->getId() );
		$objDrawRequest->setPropertyId( $this->getPropertyId() );
		$objDrawRequest->setCid( $this->getCid() );

		return $objDrawRequest;
	}

	public function createApHeader() {

		$objApHeader = new CApHeader();
		$objApHeader->setDefaults();
		$objApHeader->setCid( $this->getCid() );
		$objApHeader->setGlTransactionTypeId( CGlTransactionType::AP_CHARGE );
		$objApHeader->setTransactionDatetime( 'NOW()' );
		$objApHeader->setPostDate( date( 'm/d/Y' ) );
		$objApHeader->setApHeaderTypeId( CApHeaderType::JOB );
		$objApHeader->setBulkPropertyId( $this->getPropertyId() );
		$objApHeader->setHeaderNumber( $this->getId() );
		$objApHeader->setApHeaderModeId( CApHeaderMode::AP_HEADER_MODES_JOB );

		return $objApHeader;

	}

	public function createJobCostingFilter() {

		$objJobCostingFilter  = new CJobCostingFilter();
		$objJobCostingFilter->setJobId( $this->getId() );

		return $objJobCostingFilter;

	}

	public function createJobGroup() {

		$objJobGroup  = new CJobGroup();

		$objJobGroup->setJobId( $this->getId() );
		$objJobGroup->setCid( $this->getCid() );

		return $objJobGroup;
	}

	public function createBudgetAdjustment() {

		$objBudgetAdjustment  = new CBudgetAdjustment();
		$objBudgetAdjustment->setJobId( $this->getId() );
		$objBudgetAdjustment->setCid( $this->getCid() );

		return $objBudgetAdjustment;
	}

	public function createJobUnitType() {
		$objJobUnitType = new CJobUnitType();
		$objJobUnitType->setCid( $this->getCid() );
		$objJobUnitType->setJobId( $this->getId() );

		return $objJobUnitType;
	}

	public function createJobPhaseUnit() {

		$objJobPhaseUnit  = new CJobPhaseUnit();
		$objJobPhaseUnit->setJobId( $this->getId() );
		$objJobPhaseUnit->setCid( $this->getCid() );

		return $objJobPhaseUnit;
	}

	public function createJobUnitTypePhase() {

		$objJobUnitTypePhase  = new CJobUnitTypePhase();
		$objJobUnitTypePhase->setJobId( $this->getId() );
		$objJobUnitTypePhase->setCid( $this->getCid() );

		return $objJobUnitTypePhase;
	}

	public function createJobMaintenanceLocation() {
		$objJobMaintenanceLocation = new CJobMaintenanceLocation();
		$objJobMaintenanceLocation->setCid( $this->getCid() );
		$objJobMaintenanceLocation->setJobId( $this->getId() );

		return $objJobMaintenanceLocation;
	}

	public function createApContract() {

		$objApContract = new CApContract();
		$objApContract->setCid( $this->getCid() );
		$objApContract->setJobId( $this->getId() );

		return $objApContract;
	}

	public function createJobStakeholder() {

		$objJobStakeholder = new CJobStakeholder();
		$objJobStakeholder->setCid( $this->getCid() );
		$objJobStakeholder->setJobId( $this->getId() );

		return $objJobStakeholder;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchJobPhases( $objDatabase ) {
		return rekeyArray( 'id', ( array ) CJobPhases::fetchJobPhasesWithOrderNumByJobIdByCid( $this->getId(), $this->getCid(), $objDatabase, true ) );
	}

	public function fetchEventsByEventTypeIds( $arrintEventSubTypeIds, $objDatabase, $objPagination = NULL ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchPaginatedEventsByReferenceIdByReferenceTypeIdByEventSubTypeIdsByCid( $this->getId(), CEventReferenceType::JOB, $arrintEventSubTypeIds, $this->getCid(), $objDatabase, $objPagination );
	}

	public function parseCsvFileImport( $strFileUri ) {

		$arrmixRawData 			= [];
		$boolHeaderRowSelected	= false;

		if( false == file_exists( $strFileUri ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'no_file', 'Application Error: Failed to load File.' ) );
			return;
		}

		if( false !== ( $resHandle = CFileIo::fileOpen( $strFileUri, 'r' ) ) ) {

			while( false !== ( $arrmixData = fgetcsv( $resHandle, 0, ',' ) ) ) {

				if( false == $boolHeaderRowSelected ) {
					$boolHeaderRowSelected	 = ftell( $resHandle );
					$arrstrCsvHeader		 = array_map( 'str_replace', array_fill( 0, \Psi\Libraries\UtilFunctions\count( $arrmixData ), ' ' ), array_fill( 0, \Psi\Libraries\UtilFunctions\count( $arrmixData ), '_' ), array_map( 'strtolower', array_map( 'trim', $arrmixData ) ) );
					continue;
				}

				if( \Psi\Libraries\UtilFunctions\count( $arrstrCsvHeader ) == \Psi\Libraries\UtilFunctions\count( $arrmixData ) ) {
					$arrmixRawData[] = array_combine( $arrstrCsvHeader, $arrmixData );
				}
			}

			fclose( $resHandle );
		}
		return $arrmixRawData;
	}

	public function fetchEventsCountByEventSubTypeIds( $arrintEventSubTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchEventsCountByReferenceIdByReferenceTypeIdByEventSubTypeIdsByCid( $this->getId(), CEventReferenceType::JOB, $arrintEventSubTypeIds, $this->getCid(), $objDatabase );
	}

}
?>