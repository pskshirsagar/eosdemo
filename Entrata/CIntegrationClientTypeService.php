<?php

class CIntegrationClientTypeService extends CBaseIntegrationClientTypeService {

	protected $m_strDefaultServiceUri;

    /**
     * Get Functions
     */

    public function getDefaultServiceUri() {
		return $this->m_strDefaultServiceUri;
	}

	/**
	 * Set Functions
	 */

	public function setDefaultServiceUri( $strDefaultServiceUri ) {
		$this->m_strDefaultServiceUri = ( string ) $strDefaultServiceUri;
	}

	/**
	 * Create Functions
	 */

	public function createIntegrationClient( $intCid ) {

		$objNewIntegrationClient = new CIntegrationClient();
		$objNewIntegrationClient->setCid( $intCid );
		$objNewIntegrationClient->setIntegrationClientTypeId( $this->m_intIntegrationClientTypeId );
		$objNewIntegrationClient->setIntegrationServiceId( $this->m_intIntegrationServiceId );
		$objNewIntegrationClient->setIntegrationSyncTypeId( $this->m_intDefaultSyncTypeId );
		$objNewIntegrationClient->setServiceUri( $this->m_strDefaultServiceUri );

		return $objNewIntegrationClient;
	}

	/**
	 * Validation Functions
	 */

    public function valId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIntegrationClientTypeId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getIntegrationClientTypeId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'integration_client_type_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIntegrationServiceId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getIntegrationServiceId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'integration_service_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDefaultSyncTypeId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getDefaultSyncTypeId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_sync_type_id', '' ) );
        // }

        return $boolIsValid;
    }

	public function valIsPropertySpecific() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>