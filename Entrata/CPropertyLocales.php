<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyLocales
 * Do not add any new functions to this class.
 */

class CPropertyLocales extends CBasePropertyLocales {

	public static function fetchPropertyLocalesByCidByLocaleCode( $intCid, $strLocaleCode, $objDatabase ) {

		if( !valId( $intCid ) || !valStr( $strLocaleCode ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						property_locales AS pl
					WHERE
						pl.cid = ' . ( int ) $intCid . '
						AND pl.locale_code = \'' . addslashes( $strLocaleCode ) . '\'';

		return self::fetchPropertyLocales( $strSql, $objDatabase );
	}

	public static function fetchPropertyLocalesByIdByCid( $intCid, $objDatabase, $arrstrCompanyLanguages, $arrmixFilteredExplodedSearch = NULL, $boolIsForCount = false ) {

		if( !valId( $intCid ) ) {
			return NULL;
		}

		$intPageSize		= ( int ) $arrmixFilteredExplodedSearch['page_size'];
		$intPageNo			= ( int ) $arrmixFilteredExplodedSearch['page_no'];
		$strOrderBy			= $arrmixFilteredExplodedSearch['sort_by'];
		$strOrderByField	= $arrmixFilteredExplodedSearch['order_by'];
		$arrmixCities		= $arrmixFilteredExplodedSearch['cities'] ?? NULL;
		$strCountryCode		= $arrmixFilteredExplodedSearch['country_code'] ?? NULL;

		$intOffset	= ( 0 < $arrmixFilteredExplodedSearch['page_no'] ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= $intPageSize;

		$strPropertyName	= trim( $arrmixFilteredExplodedSearch['property_search'] );
		$strWhereCondition	= '';

		if( valStr( $strPropertyName ) ) {
			$strWhereCondition .= ' AND ( p.property_name ILIKE \'%' . pg_escape_string( $strPropertyName ) . '%\' OR to_tsvector( \'english\', p.property_name ) @@ plainto_tsquery( \'english\', \'' . pg_escape_string( $strPropertyName ) . '\' ) )';
		}

		if( valArr( $arrmixCities ) ) {
			$strWhereCondition .= ' AND LOWER( pa.city ) IN ( \'' . \Psi\CStringService::singleton()->strtolower( implode( '\',\'', $arrmixCities ) ) . '\' )';
		}

		if( valStr( $strCountryCode ) ) {
			$strWhereCondition .= ' AND pa.country_code  = \'' . $strCountryCode . '\'';
		}

		$strSelect		= 'COUNT(*)';
		$strPagination	= '';

		if( !$boolIsForCount ) {
			$strSelect	= '*';

			if( valArr( $arrstrCompanyLanguages ) ) {
				$strOuterOrderBy = ' ORDER BY ' . $strOrderByField . ' ' . $strOrderBy;
			} else {
				$strOuterOrderBy = '';
				$strWhereCondition .= ' ORDER BY ' . $strOrderByField . ' ' . $strOrderBy;
			}

			$strPagination = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		if( valArr( $arrstrCompanyLanguages ) ) {
			$strOuterPagination		= $strPagination;
			$strInnerPagination		= '';
			$arrstrCompanyLanguages	= array_column( $arrstrCompanyLanguages, 'locale_code' );
		} else {
			$strOuterPagination	= '';
			$strInnerPagination	= $strPagination;
		}

		$strSqlMain = 'SELECT p.id AS property_id,
							p.cid,
							p.property_name,
							pa.city,
							pa.state_code,
							c.name as country_name,
							(
								SELECT 
									CASE WHEN l.iso_region_code IS NOT NULL THEN l.iso_language_name || \'-\' || l.iso_region_code ELSE l.iso_language_name END
								FROM
									clients as cl
									LEFT JOIN locales AS l ON ( cl.locale_code = l.locale_code )
								WHERE 
									cl.id = ' . ( int ) $intCid . '
								LIMIT 1 
							) as default_language,
							pl.locale_code,
							CASE WHEN pl.locale_code IS NOT NULL THEN TRUE ELSE FALSE END AS property_locale_status
							FROM
								properties AS p
								LEFT JOIN property_addresses AS pa ON ( p.id = pa.property_id AND p.cid = pa.cid AND pa.address_type_id =' . CAddressType::PRIMARY . ' AND pa.is_alternate = false )
								LEFT JOIN address_types AS at ON ( pa.address_type_id = at.id )
								LEFT JOIN property_locales AS pl ON ( p.id = pl.property_id AND ( p.locale_code <> pl.locale_code OR p.locale_code IS NULL ) )
								LEFT JOIN countries AS c ON ( c.code = pa.country_code )
								LEFT JOIN locales AS l ON ( pl.locale_code = l.locale_code )
							WHERE
								p.cid = ' . ( int ) $intCid . '
								AND p.is_disabled <> 1
								AND p.is_test <> 1
								AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
								AND p.termination_date IS NULL
							' . $strWhereCondition . $strInnerPagination;

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrCompanyLanguages ) ) {

			$strCompanyLanguageCodes = implode( ' TEXT, ', $arrstrCompanyLanguages ) . ' TEXT';

			$strSql = 'SELECT
						' . $strSelect . '
						FROM
						CROSSTAB( $$' . $strSqlMain . ' ORDER BY property_id $$,
						$$
							SELECT
								l.locale_code
							FROM company_locales AS cl
								JOIN locales l ON ( cl.locale_code = l.locale_code AND l.is_published )
								JOIN clients c ON ( c.id = cl.cid AND ( cl.locale_code <> c.locale_code OR c.locale_code IS NULL ) )
							WHERE cl.cid = ' . ( int ) $intCid . '
							ORDER BY l.locale_code
						$$ ) AS subqr(
							property_id integer,
							cid integer,
							property_name TEXT,
							city TEXT,
							state_code TEXT,
							country_name TEXT,
							default_language TEXT,
							' . $strCompanyLanguageCodes . ' ) ' . $strOuterOrderBy . $strOuterPagination;
		} else {
			$strSql = $strSqlMain;
		}

		$arrmixPropertyLocalsResult = fetchData( $strSql, $objDatabase );

		if( !$boolIsForCount ) {
			return $arrmixPropertyLocalsResult;
		} else {
			if( valArr( $arrstrCompanyLanguages ) ) {
				return $arrmixPropertyLocalsResult[0]['count'];
			}

			return \Psi\Libraries\UtilFunctions\count( $arrmixPropertyLocalsResult );
		}
	}

	public static function fetchPropertyLanguageMappingsByCidByPropertyIds( $intCid, $arrintPropertyIds, $objDatabase ) {
		if( !valId( $intCid ) || !valIntArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						pl.*
					FROM
						property_locales pl
						JOIN properties p ON ( pl.property_id = p.id AND pl.cid = p.cid AND ( pl.locale_code <> p.locale_code OR p.locale_code IS NULL ) )
					WHERE
						pl.cid = ' . ( int ) $intCid . '
					AND pl.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return self::fetchPropertyLocales( $strSql, $objDatabase );
	}

	public static function fetchPropertyLocalesByCIdByPropertyIdsByLocaleCodes( $intCid, $arrintPropertyIds, $arrstrLocaleCodes, $objDatabase ) {

		if( !valId( $intCid ) || !valArr( $arrintPropertyIds ) || !valArr( $arrstrLocaleCodes ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						* 
					FROM 
						property_locales 
					WHERE 
						cid = ' . ( int ) $intCid .
						' AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						  AND locale_code IN (' . implode( ',', $arrstrLocaleCodes ) . ')';

		return self::fetchObjects( $strSql, 'CPropertyLocale', $objDatabase );
	}

	public static function fetchPropertyLocalesByCidByPropertyId( $intCid, $intPropertyId, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						l.locale_code AS locale_code,
						l.iso_region_code AS display_locale_code,
						l.iso_language_name AS language_name
					FROM
						property_locales AS pl
						JOIN locales AS l ON ( pl.locale_code = l.locale_code )
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyLocalesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		return self::fetchPropertyLocales( sprintf( 'SELECT * FROM property_locales WHERE property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>