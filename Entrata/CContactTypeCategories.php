<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CContactTypeCategories
 * Do not add any new functions to this class.
 */

class CContactTypeCategories extends CBaseContactTypeCategories {

	public static function fetchContactTypeCategory( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CContactTypeCategory', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}
}
?>