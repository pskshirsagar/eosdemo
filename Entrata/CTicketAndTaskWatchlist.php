<?php

class CTicketAndTaskWatchlist extends CBaseTicketAndTaskWatchlist {

	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strCompanyEmployeeEmailAddress;

    public function __construct() {
        parent::__construct();

        $this->m_strNameFirst 						= NULL;
        $this->m_strNameLast 						= NULL;
        $this->m_strCompanyEmployeeEmailAddress 	= NULL;

        return;
    }

    // Get functions

    public function getNameFirst() {
    	return $this->m_strNameFirst;
    }

    public function getNameLast() {
    	return $this->m_strNameLast;
    }

    public function getCompanyEmployeeEmailAddress() {
    	return $this->m_strCompanyEmployeeEmailAddress;
    }

    // Set functions

    public function setNameFirst( $strNameFirst ) {
    	$this->m_strNameFirst = $strNameFirst;
    }

    public function setNameLast( $strNameLast ) {
    	$this->m_strNameLast = $strNameLast;
    }

    public function setCompanyEmployeeEmailAddress( $strCompanyEmployeeEmailAddress ) {
    	$this->m_strCompanyEmployeeEmailAddress = $strCompanyEmployeeEmailAddress;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrValues['name_first'] ) && true == valStr( $arrValues['name_first'] ) ) 											$this->setNameFirst( $arrValues['name_first'] );
    	if( true == isset( $arrValues['name_last'] ) && true == valStr( $arrValues['name_last'] ) )												$this->setNameLast( $arrValues['name_last'] );
    	if( true == isset( $arrValues['company_employee_email_address'] ) && true == valStr( $arrValues['company_employee_email_address'] ) ) 	$this->setCompanyEmployeeEmailAddress( $arrValues['company_employee_email_address'] );

    	return;
    }

    // Val functions

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyUserId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTaskId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {

            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
        }

        return $boolIsValid;
    }

}
?>