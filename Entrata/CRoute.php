<?php

class CRoute extends CBaseRoute {

	// Val Functions

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Route is required.' ) ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( 0 == strlen( trim( $this->getName() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Route name is required.' ) ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valRouteTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getRouteTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'route_type_id', __( 'Route type is required.' ) ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEnableDisableRoute( $objDatabase ) {
		if( 0 == $this->getIsPublished() ) return true;

		$arrobjRouteRules	= ( array ) $this->fetchRouteRulesWithRuleConditionsAndRuleConditionDetails( $objDatabase );
		$arrobjRouteRules	= rekeyObjects( 'RouteRuleId', $arrobjRouteRules );
		$arrobjRuleStops	= ( array ) $this->fetchRuleStopsByRouteRuleIds( array_keys( $arrobjRouteRules ), $objDatabase );

		// Rule stops with deleted group associated to it.
		foreach( $arrobjRuleStops as $intRuleStopId => $objRuleStop ) {
			if( true == valId( $objRuleStop->getCompanyGroupId() && false == valStr( $objRuleStop->getCompanyGroupName() ) ) ) {
				$arrobjRuleStopsWithDeletedGroup[$intRuleStopId] = $objRuleStop;
			}
		}

		$arrobjDisabledUserRuleStops = [];

		foreach( $arrobjRuleStops as $objRuleStop ) {

			if( true == is_numeric( $objRuleStop->getCompanyUserId() ) && 1 == $objRuleStop->getCompanyUserIsDisabled() ) {

				$arrobjDisabledUserRuleStops[$objRuleStop->getCompanyUserId()] = $objRuleStop;
			}
		}

		$strUserName = '';

		foreach( $arrobjDisabledUserRuleStops as $objRuleStop ) {

			$strUserName .= ( false == valStr( $strUserName ) ) ? '' : ', ';
			$strUserName .= $objRuleStop->getCompanyUserNameFirst() . ' ' . $objRuleStop->getCompanyUserNameLast();
		}

		if( true == valArr( $arrobjDisabledUserRuleStops ) && true == valStr( $strUserName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rule_stop_users', __( 'This Route is associated with the following disabled user(s) : {%s, 0}', [ $strUserName ] ) ) );
			return $boolIsValid;
		}

		if( true == valArr( $arrobjRuleStopsWithDeletedGroup ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rule_stop_users', __( 'Users or Groups have been deleted from this route. Please update your approval stops to enable this route.' ) ) );
			return $boolIsValid;
		}

		return true;
	}

	public function valDuplicateRouteName( $objDatabase ) {

		$boolIsValid = $this->valName();

		if( true == $boolIsValid ) {

			$arrobjDuplicateRoutes = ( array ) \Psi\Eos\Entrata\CRoutes::createService()->fetchConflictingRoutesByNameByIdByRouteTypeIdByCid( $this->getName(), $this->getId(), $this->getRouteTypeId(), $this->getCid(), $objDatabase );

			if( true == valArr( $arrobjDuplicateRoutes ) ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Route with the selected name already exists.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valDuplicateRouteName( $objDatabase );
				$boolIsValid &= $this->valRouteTypeId();
				break;

			case VALIDATE_DELETE:
				break;

			case 'enable_disable_route':
				$boolIsValid &= $this->valEnableDisableRoute( $objDatabase );
				break;

			case 'validate_route_name':
				$boolIsValid &= $this->valDuplicateRouteName( $objDatabase );
				break;

			default:
		}

		return $boolIsValid;
	}

	// Get or Fetch Functions

	public function getPropertyNames() {
		return $this->m_arrstrPropertyNames;
	}

	public function fetchRouteRuleByRouteRuleId( $intRouteRuleId, $objClientDatabase ) {
		return \Psi\Eos\Entrata\CRouteRules::createService()->fetchRouteRuleByRouteIdByIdByCid( $this->getId(), $intRouteRuleId, $this->getCid(), $objClientDatabase );
	}

	public function fetchRouteRules( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CRouteRules::createService()->fetchActiveRouteRulesByRouteIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchRouteRulesWithRuleConditionsAndRuleConditionDetails( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CRouteRules::createService()->fetchRouteRulesWithRuleConditionsAndRuleConditionDetailsByRouteIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchRuleStopsByRouteRuleIds( $arrintRouteRulesIds, $objClientDatabase ) {
		return \Psi\Eos\Entrata\CRuleStops::createService()->fetchRuleStopsByRouteRuleIdsByRouteIdByCid( $arrintRouteRulesIds, $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchPropertyRoutes( $objClientDatabase ) {
		return CPropertyRoutes::fetchPropertyRoutesByRouteTypeIdByRouteIdIdByCid( $this->getRouteTypeId(), $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchRouteTypes( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CRouteTypes::createService()->fetchRouteTypesByIds( CRouteType::getAssociatedRouteTypeIds( $this->getRouteTypeId() ), $objClientDatabase );
	}

	// Set Functions

	public function setPropertyNames( $arrstrPropertyNames ) {
		$this->m_arrstrPropertyNames = $arrstrPropertyNames;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_names'] ) ) $this->setPropertyNames( $arrmixValues['property_names'] );

		return;
	}

	// Create Functions

	public function createRouteRule() {
		$objRouteRule = new CRouteRule();

		$objRouteRule->setCid( $this->getCid() );
		$objRouteRule->setRouteId( $this->getId() );
		$objRouteRule->setRouteTypeId( $this->getRouteTypeId() );

		return $objRouteRule;
	}

	public function createPropertyRoute( $intPropertyId ) {
		$objPropertyRoute = new CPropertyRoute();

		$objPropertyRoute->setCid( $this->getCid() );
		$objPropertyRoute->setPropertyId( $intPropertyId );
		$objPropertyRoute->setRouteTypeId( $this->getRouteTypeId() );
		$objPropertyRoute->setRouteId( $this->getId() );

		return $objPropertyRoute;
	}

}
?>