<?php

class CGlHeaderSchedule extends CBaseGlHeaderSchedule {

	protected $m_intPropertyId;
	protected $m_intScheduledGlDetailsCount;

	protected $m_fltCreditAmount;
	protected $m_fltDebitAmount;

	protected $m_strEndOn;
	protected $m_strCompanyName;
	protected $m_strPropertyName;
	protected $m_strAccountingMethod;

	protected $m_arrintCompanyUserPropertyIds;

	protected $m_arrobjProperties;

	/**
	 * Get Functions
	 */

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getCreditAmount() {
		return $this->m_fltCreditAmount;
	}

	public function getDebitAmount() {
		return $this->m_fltDebitAmount;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getAccountingMethod() {
		return $this->m_strAccountingMethod;
	}

	public function getProperties() {
		return $this->m_arrobjProperties;
	}

	public function getEndOn() {
		return $this->m_strEndOn;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getCompanyUserPropertyIds() {
		return $this->m_arrintCompanyUserPropertyIds;
	}

	public function getScheduledGlDetailsCount() {
		return $this->m_intScheduledGlDetailsCount;
	}

	/**
	 * Set Functions
	 */

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setCreditAmount( $fltCreditAmount ) {
		$this->m_fltCreditAmount = CStrings::strToFloatDef( $fltCreditAmount, 0 );
	}

	public function setDebitAmount( $fltDebitAmount ) {
		$this->m_fltDebitAmount = CStrings::strToFloatDef( $fltDebitAmount, 0 );
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setAccountingMethod( $strAccountingMethod ) {
		$this->m_strAccountingMethod = $strAccountingMethod;
	}

	public function setProperties( $arrobjProperties ) {
		$this->m_arrobjProperties = $arrobjProperties;
	}

	public function setEndOn( $strEndOn ) {
		$this->m_strEndOn = $strEndOn;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setCompanyUserPropertyIds( $arrintCompanyUserPropertyIds ) {
		return $this->m_arrintCompanyUserPropertyIds = $arrintCompanyUserPropertyIds;
	}

	public function setScheduledGlDetailsCount( $intScheduledGlDetailsCount ) {
		return $this->m_intScheduledGlDetailsCount = $intScheduledGlDetailsCount;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['end_on'] ) ) $this->setEndOn( $arrmixValues['end_on'] );
		if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['credit_amount'] ) ) $this->setCreditAmount( $arrmixValues['credit_amount'] );
		if( true == isset( $arrmixValues['debit_amount'] ) ) $this->setDebitAmount( $arrmixValues['debit_amount'] );
		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['accounting_method'] ) ) $this->setAccountingMethod( $arrmixValues['accounting_method'] );
		if( true == isset( $arrmixValues['company_name'] ) ) $this->setCompanyName( $arrmixValues['company_name'] );

		return;
	}

	/**
	 * Validate Function
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFrequencyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getFrequencyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_id', __( 'Repeat frequency is required.' ) ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valGlHeaderScheduleId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFrequencyInterval() {
		$boolIsValid = true;

		if( true == is_null( $this->getFrequencyInterval() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_interval', __( 'Frequency interval is required.' ) ) );
		} elseif( false == is_numeric( $this->getFrequencyInterval() ) || true == is_null( $this->getFrequencyInterval() ) || 0 >= $this->getFrequencyInterval() || 365 < $this->getFrequencyInterval() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_interval', __( 'Frequency interval is invalid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDayOfWeek() {
		$boolIsValid = true;

		if( true == is_null( $this->getDayOfWeek() ) && ( CFrequency::WEEKLY == $this->getFrequencyId() || CFrequency::WEEKDAY_OF_MONTH == $this->getFrequencyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'day_of_week', __( 'Weekday is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDaysOfMonth() {
		$boolIsValid = true;

		if( false == valStr( $this->getDaysOfMonth() ) && CFrequency::MONTHLY == $this->getFrequencyId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_of_month', __( 'Day(s) of month is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valStartDate() {

		$boolIsValid = true;

		if( false == valStr( $this->getStartDate() ) ) {
			$boolIsValid &= false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start date is required.' ) ) );
			return $boolIsValid;
		}

		if( false == CValidation::validateDate( $this->getStartDate() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start date is invalid.' ) ) );
			return $boolIsValid;
		}

		$strStartDate = __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->getStartDate() ] );

		if( false == CValidation::checkDateFormat( $strStartDate, true ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start date is invalid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valEndDate() {

		$boolIsValid = true;

		if( true == valStr( $this->getEndOn() ) && 'date' == $this->getEndOn() ) {
			if( false == valStr( $this->getEndDate() ) ) {
				$boolIsValid &= false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date is required.' ) ) );
				return $boolIsValid;
			}

			if( false == CValidation::validateDate( $this->getEndDate() ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date is invalid.' ) ) );

				return $boolIsValid;
			}

			$strEndDate = __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->getEndDate() ] );

			if( false == CValidation::checkDateFormat( $strEndDate, true ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date is invalid.' ) ) );
			} elseif( true == $this->valStartDate( $this->getStartDate() ) && strtotime( $this->getStartDate() ) > strtotime( $this->getEndDate() ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date should be greater than or equal to {%t, 0, DATE_NUMERIC_STANDARD }.', [ $this->getStartDate() ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valNumberOfOccurrences() {
		$boolIsValid = true;

		if( true == valStr( $this->getEndOn() ) && 'occurrences' == $this->getEndOn() ) {
			if( true == is_null( $this->getNumberOfOccurrences() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_occurrences', __( 'Number of occurrences is required.' ) ) );
			} elseif( false == is_numeric( $this->getNumberOfOccurrences() ) || 0 == $this->getNumberOfOccurrences() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_occurrences', __( 'Invalid number of occurrences.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valLastPostedDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNextPostDate() {
		$boolIsValid = true;

		if( true == valStr( $this->getEndOn() ) && false == is_null( $this->getEndDate() ) && ( 'date' == $this->getEndOn() || 'occurrences' == $this->getEndOn() ) ) {
			if( true == valStr( $this->getNextPostDate() ) && true == $this->valEndDate( $this->getEndDate() ) && false == CValidation::checkDateFormat( $this->getNextPostDate(), true ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'next_post_date', __( 'Next Post date is invalid.' ) ) );
			} elseif( true == $this->valEndDate( $this->getEndDate() ) && strtotime( $this->getEndDate() ) < strtotime( $this->getNextPostDate() ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'next_post_date', __( 'The end date {%t, 0, DATE_NUMERIC_STANDARD} should be greater than or equal to the next occurrence date {%t, 1, DATE_NUMERIC_STANDARD} of the journal entry.', [ $this->getEndDate(), $this->getNextPostDate() ] ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valHeaderNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsNextScheduledWeekDayOfMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsReverseNextMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPaused() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDisabledBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDisabledOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndOn() {
		$boolIsValid = true;

		if( true == is_null( $this->getEndOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_on', __( 'End on action is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIsDisabled() {

		$boolIsValid = true;

		if( false == is_null( $this->getDisabledBy() ) || false == is_null( $this->getDisabledOn() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'disabled_by', __( 'Scheduled JE has already been disabled.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

    public function valIsEnabled() {

        $boolIsValid = true;

        if( is_null( $this->getDisabledBy() ) && is_null( $this->getDisabledOn() ) ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'disabled_by', __( 'Scheduled JE has already been enabled.' ) ) );
            $boolIsValid = false;
        }

        return $boolIsValid;
    }

	public function valIsApproved() {

		$boolIsValid = true;

		if( false == is_null( $this->getApprovedBy() ) || false == is_null( $this->getApprovedOn() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'disabled_by', __( 'Scheduled JE has already been approved.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valUserAssignedProperties( $arrintPropertyIds ) {

		$boolIsValid = true;

		if( false == valArr( $this->getCompanyUserPropertyIds() ) || false == valArr( $arrintPropertyIds ) ) {
			return $boolIsValid;
		}

		$arrobjProperties	= ( array ) $this->getProperties();

		$arrstrPropertyNames = [];

		foreach( $arrintPropertyIds as $intPropertyId ) {

			if( false == in_array( $intPropertyId, $this->getCompanyUserPropertyIds() ) ) {
				$arrstrPropertyNames[$intPropertyId] = $arrobjProperties[$intPropertyId]->getPropertyName();
			}
		}

		if( true == valArr( $arrstrPropertyNames ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Property(s) \'{%s, 0}\' is/are not assigned to this user.', [ implode( ', ', $arrstrPropertyNames ) ] ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $arrintPropertyIds = [] ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
            case 'validate_enable':
				$boolIsValid &= $this->valFrequencyId();
				$boolIsValid &= $this->valFrequencyInterval();
				$boolIsValid &= $this->valDayOfWeek();
				$boolIsValid &= $this->valDaysOfMonth();
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndOn();
				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valNextPostDate();
				$boolIsValid &= $this->valNumberOfOccurrences();

				if( 'validate_enable' == $strAction ) {
                    $boolIsValid &= $this->valIsEnabled();
                } else {
                    $boolIsValid &= $this->valIsDisabled();
                }
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valIsDisabled();
				$boolIsValid &= $this->valUserAssignedProperties( $arrintPropertyIds );
				break;

			case 'validate_approve':
				$boolIsValid &= $this->valIsApproved();
				$boolIsValid &= $this->valIsDisabled();
				$boolIsValid &= $this->valUserAssignedProperties( $arrintPropertyIds );
				break;

			default:
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 */

	public function createGlHeaderScheduleLog() {

		$objGlHeaderScheduleLog = new CGlHeaderScheduleLog();

		$objGlHeaderScheduleLog->setGlHeaderScheduleId( $this->getId() );
		$objGlHeaderScheduleLog->setCid( $this->getCid() );
		$objGlHeaderScheduleLog->setFrequencyId( $this->getFrequencyId() );
		$objGlHeaderScheduleLog->setFrequencyInterval( $this->getFrequencyInterval() );
		$objGlHeaderScheduleLog->setDayOfWeek( $this->getDayOfWeek() );
		$objGlHeaderScheduleLog->setDaysOfMonth( $this->getDaysOfMonth() );
		$objGlHeaderScheduleLog->setStartDate( $this->getStartDate() );
		$objGlHeaderScheduleLog->setEndDate( $this->getEndDate() );
		$objGlHeaderScheduleLog->setNumberOfOccurrences( $this->getNumberOfOccurrences() );
		$objGlHeaderScheduleLog->setLastPostedDate( $this->getLastPostedDate() );
		$objGlHeaderScheduleLog->setNextPostDate( $this->getNextPostDate() );
		$objGlHeaderScheduleLog->setIsReverseNextMonth( $this->getIsReverseNextMonth() );
		$objGlHeaderScheduleLog->setIsPaused( $this->getIsPaused() );
		$objGlHeaderScheduleLog->setApprovedBy( $this->getApprovedBy() );
		$objGlHeaderScheduleLog->setApprovedOn( $this->getApprovedOn() );
		$objGlHeaderScheduleLog->setDisabledBy( $this->getDisabledBy() );
		$objGlHeaderScheduleLog->setDisabledOn( $this->getDisabledOn() );

		return $objGlHeaderScheduleLog;
	}

	public function createFileAssociation() {

		$objFileAssociation = new CFileAssociation();

		$objFileAssociation->setCid( $this->getCid() );
		$objFileAssociation->setGlHeaderScheduleId( $this->getId() );

		return $objFileAssociation;
	}

	/**
	 * Other Functions
	 */

	public function insert( $intCurrentUserId, $objClientDatabase, $boolReturnSqlOnly = false ) {

		if( false == parent::insert( $intCurrentUserId, $objClientDatabase, $boolReturnSqlOnly ) ) {
			return false;
		}

		$this->logHistory( CGlHeaderScheduleLog::ACTION_CREATED, $intCurrentUserId, $objClientDatabase );

		return true;
	}

	public function logHistory( $strLogAction, $intCurrentUserId, $objCleintDatabase, $objOldGlHeaderSchedule = NULL ) {

		$boolIsChangesMade				= false;
		$arrobjScheduledGLDetailLogs	= [];

		if( true == valObj( $objOldGlHeaderSchedule, 'CGlHeaderSchedule' ) ) {

			// If GlHeaderSchedule is modified
			if( $this->getFrequencyId() != $objOldGlHeaderSchedule->getFrequencyId()
				|| $this->getFrequencyInterval() != $objOldGlHeaderSchedule->getFrequencyInterval()
				|| $this->getDayOfWeek() != $objOldGlHeaderSchedule->getDayOfWeek()
				|| $this->getDaysOfMonth() != $objOldGlHeaderSchedule->getDaysOfMonth()
				|| $this->getStartDate() != $objOldGlHeaderSchedule->getStartDate()
				|| $this->getEndDate() != $objOldGlHeaderSchedule->getEndDate()
				|| $this->getNumberOfOccurrences() != $objOldGlHeaderSchedule->getNumberOfOccurrences()
				|| $this->getLastPostedDate() != $objOldGlHeaderSchedule->getLastPostedDate()
				|| $this->getNextPostDate() != $objOldGlHeaderSchedule->getNextPostDate()
				|| $this->getIsReverseNextMonth() != $objOldGlHeaderSchedule->getIsReverseNextMonth()
				|| $this->getIsPaused() != $objOldGlHeaderSchedule->getIsPaused()
				|| $this->getApprovedBy() != $objOldGlHeaderSchedule->getApprovedBy()
				|| $this->getApprovedOn() != $objOldGlHeaderSchedule->getApprovedOn()
				|| $this->getDisabledBy() != $objOldGlHeaderSchedule->getDisabledBy()
				|| $this->getDisabledOn() != $objOldGlHeaderSchedule->getDisabledOn() ) {
					$boolIsChangesMade = true;
			}
		}

		if( true == $boolIsChangesMade ) {

			$objGlHeaderScheduleLog = $this->createGlHeaderScheduleLog();
			$objGlHeaderScheduleLog->setAction( $strLogAction );

			// Insert GlHeaderScheduleLog
			if( false == $objGlHeaderScheduleLog->insert( $intCurrentUserId, $objCleintDatabase ) ) {
				$this->addErrorMsgs( $objGlHeaderScheduleLog->getErrorMsgs() );
				return false;
			}
		}

		return true;
	}

	public function calculateScriptStartDate() {

		$boolTakeLastDayOfMonth		= false;
		$boolTakeNextDayFromMonth	= false;
		$strScriptStartDate			= NULL;
		$strPostDate				= __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->getStartDate() ] );
		$intFrequencyInterval		= $this->getFrequencyInterval();
		$arrintDaysOfMonth			= ( true == valStr( $this->getDaysOfMonth() ) ) ? explode( ',', $this->getDaysOfMonth() ) : [];
		$arrmixDayNames				= [ 0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday' ];
		$arrmixRelativeFormats		= [ 1 => 'first', 2 => 'second', 3 => 'third', 4 => 'fourth', 5 => 'fifth' ];

		if( false == is_numeric( $intFrequencyInterval ) || false == $this->valStartDate() ) {
			return NULL;
		}

		switch( $this->getFrequencyId() ) {

			case CFrequency::DAILY:
			case CFrequency::YEARLY:
				$strScriptStartDate = $strPostDate;
				break;

			case CFrequency::WEEKLY:
				if( true == is_numeric( $this->getDayOfWeek() ) ) {
					$objStartDatetime	= new DateTime( $strPostDate );
					$objStartDatetime->modify( $arrmixDayNames[$this->getDayOfWeek()] );
					$strScriptStartDate	= $objStartDatetime->format( 'm/d/Y' );

					// if script start date is passed then calculate next day date
					if( strtotime( $strPostDate ) > strtotime( $strScriptStartDate ) ) {
						$objStartDatetime->modify( 'next ' . $arrmixDayNames[$this->getDayOfWeek()] );
						$strScriptStartDate = $objStartDatetime->format( 'm/d/Y' );
					}
				}
				break;

			case CFrequency::MONTHLY:
			 	if( false == valArr( $arrintDaysOfMonth ) ) {
					return NULL;
				}

				$intNextPostDay	= $arrintDaysOfMonth[0];
				$intDayKey		= array_search( date( 'j', strtotime( $strPostDate ) ), $arrintDaysOfMonth );

				if( true == is_numeric( $intDayKey ) ) {

					if( true == array_key_exists( $intDayKey, $arrintDaysOfMonth ) ) {
						$intNextPostDay = $arrintDaysOfMonth[$intDayKey];
					}
					$strScriptStartDate 			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
					$strScriptStartDate 			= date( 'm/d/Y', strtotime( $strScriptStartDate ) );
					$boolTakeNextDayFromMonth		= true;
				} else {

					$arrstrPostDate = explode( '/', $strPostDate );

					$intDay = $arrstrPostDate[1];

					foreach( $arrintDaysOfMonth as $intDate ) {
						if( $intDate > $intDay ) {
							$intNextPostDay				= $intDate;
							$boolTakeNextDayFromMonth	= true;
							break;
						}
					}

					$strScriptStartDate = date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );

					if( strtotime( $strPostDate ) > strtotime( $strScriptStartDate ) ) {
						$objNextPostMonthDatetime = new DateTime( $strScriptStartDate );
						$objNextPostMonthDatetime->modify( $intFrequencyInterval . ' month' );
						$strScriptStartDate = $objNextPostMonthDatetime->format( 'm/d/Y' );
					}
				}

				if( false == $boolTakeNextDayFromMonth && ( 0 == $intNextPostDay || ( false == $intDayKey && false != array_search( 0, $arrintDaysOfMonth ) ) ) ) {
					$boolTakeLastDayOfMonth = true;
				}
				break;

			case CFrequency::WEEKDAY_OF_MONTH:
				if( CFrequency::WEEKDAY_OF_MONTH == $this->getFrequencyId() && false == is_numeric( $this->getDayOfWeek() ) ) {
					return NULL;
				}

				$strPostDate 		= date( $strPostDate );
				$strDayName 		= $arrmixDayNames[$this->getDayOfWeek()];
				$strRelativeNumber	= $arrmixRelativeFormats[$intFrequencyInterval];

				$objNextPostMonthDatetime = new DateTime( $strPostDate );
				$objNextPostMonthDatetime->modify( $strRelativeNumber . ' ' . $strDayName . ' of this month' );
				$strScriptStartDate = $objNextPostMonthDatetime->format( 'm/d/Y' );

				if( strtotime( $strPostDate ) > strtotime( $strScriptStartDate ) ) {
					$objNextPostMonthDatetime->modify( $strRelativeNumber . ' ' . $strDayName . ' of next month' );
					$strScriptStartDate = $objNextPostMonthDatetime->format( 'm/d/Y' );
				}
				break;

			default:
		}// switch

		$arrstrNextPostDate = explode( '/', $strScriptStartDate );
		if( 3 == \Psi\Libraries\UtilFunctions\count( $arrstrNextPostDate ) && false == checkdate( $arrstrNextPostDate[0], $arrstrNextPostDate[1], $arrstrNextPostDate[2] ) ) {
			$boolTakeLastDayOfMonth = true;
		}

		if( true == $boolTakeLastDayOfMonth ) {

			$objDateTimeCurrentPostDate = new DateTime( $strPostDate );
			$objDateTimeCurrentPostDate->modify( 'last day of this month' );
			$strScriptStartDate = $objDateTimeCurrentPostDate->format( 'm/d/Y' );
		}

		return date( 'm/d/Y', strtotime( $strScriptStartDate ) );
	}

	public function calculateEndDate( $objFrequency ) {

		if( false == is_numeric( $this->getNumberOfOccurrences() ) || 0 >= $this->getNumberOfOccurrences() ) {
			return NULL;
		}

		$strEndDate	= date( 'm/d/Y', strtotime( $this->getNextPostDate() ) );

		switch( $this->getFrequencyId() ) {
			case CFrequency::DAILY:
			case CFrequency::YEARLY:
			case CFrequency::WEEKLY:
				$intNumberOfIntervalUnits	= ( $this->getNumberOfOccurrences() - 1 ) * $this->getFrequencyInterval();
				$strIntervalLabel			= '';

				if( true == valObj( $objFrequency, 'CFrequency' ) ) {
					$strIntervalLabel = $objFrequency->getIntervalFrequencyLabelPlural();
				}

				if( 0 < $intNumberOfIntervalUnits ) {
					$strEndDate	= date( 'm/d/Y', strtotime( $this->getNextPostDate() . ' +' . $intNumberOfIntervalUnits . ' ' . $strIntervalLabel ) );
				}
				break;

			case CFrequency::MONTHLY:
			case CFrequency::WEEKDAY_OF_MONTH:
				if( ( CFrequency::WEEKDAY_OF_MONTH == $this->getFrequencyId() && false == is_numeric( $this->getDayOfWeek() ) ) || ( CFrequency::MONTHLY == $this->getFrequencyId() && false == valStr( $this->getDaysOfMonth() ) ) ) {
					return NULL;
				}

				$strScriptStartDate		= $this->getNextPostDate();
				$intNumberOfOccurrences	= $this->getNumberOfOccurrences();

				for( $intCounter = 2; $intCounter <= $intNumberOfOccurrences; $intCounter++ ) {

					$strEndDate = $this->calculateNextPostDate();
					$this->setNextPostDate( $strEndDate );
				}
				$this->setNextPostDate( $strScriptStartDate );
				break;

			default:
		}

		return date( 'm/d/Y', strtotime( $strEndDate ) );
	}

	public function calculateNextPostDate() {

		if( false == is_numeric( $this->getFrequencyInterval() ) ) {
			return NULL;
		}

		$intNextPostDay				= NULL;
		$intFrequencyInterval		= $this->getFrequencyInterval();
		$arrintDaysOfMonth			= explode( ',', $this->getDaysOfMonth() );
		$strPostDate				= $this->getNextPostDate();

		$arrmixRelativeFormats	= [ 1 => 'first', 2 => 'second', 3 => 'third', 4 => 'fourth', 5 => 'fifth' ];
		$arrmixDayNames			= [ 0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday' ];

		switch( $this->getFrequencyId() ) {

			case CFrequency::DAILY:
				$strNextPostDate = date( 'm/d/Y', strtotime( $strPostDate . ' + ' . ( int ) $intFrequencyInterval . ' Days' ) );
				break;

			case CFrequency::WEEKLY:
				$strNextPostDate = date( 'm/d/Y', strtotime( $strPostDate . ' + ' . ( int ) $intFrequencyInterval . ' Weeks' ) );
				break;

			case CFrequency::YEARLY:
				$strNextPostDate = date( 'm/d/Y', strtotime( $strPostDate . ' + ' . ( int ) $intFrequencyInterval . ' Years' ) );

				if( date( 'm', strtotime( $strPostDate ) ) != date( 'm', strtotime( $strNextPostDate ) ) ) {

					$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
					$objDateTimeNextPostDate->modify( 'last day of previous month' );
					$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
				}
				break;

			case CFrequency::MONTHLY:
				if( true == valArr( $arrintDaysOfMonth ) ) {

					$intDayKey = array_search( date( 'j', strtotime( $strPostDate ) ), $arrintDaysOfMonth );

					if( true == is_numeric( $intDayKey ) ) {

						if( true == array_key_exists( $intDayKey + 1, $arrintDaysOfMonth ) ) {

							$intNextPostDay = $arrintDaysOfMonth[$intDayKey + 1];

							if( 0 == $intNextPostDay ) {

								$objDateTimeNextPostDate	= new DateTime( $strPostDate );
								$objDateTimeNextPostDate->modify( 'last day of this month' );
								$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );

								if( strtotime( $strPostDate ) == strtotime( $strNextPostDate ) ) {

									$intNextPostDay				= current( $arrintDaysOfMonth );
									$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
									$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
									$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
									$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
								}
							} else {

								$strNextPostDate = date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );

								$arrstrNextPostDate = explode( '/', $strNextPostDate );

								if( 3 == \Psi\Libraries\UtilFunctions\count( $arrstrNextPostDate ) && false == checkdate( $arrstrNextPostDate[0], $arrstrNextPostDate[1], $arrstrNextPostDate[2] ) ) {

									$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/01/' . date( 'Y', strtotime( $strPostDate ) );
									$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
									$objDateTimeNextPostDate->modify( 'last day of this month' );
									$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
								}

								if( strtotime( $strPostDate ) == strtotime( $strNextPostDate ) ) {

									$intNextPostDay				= current( $arrintDaysOfMonth );
									$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
									$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
									$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
									$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
								}
							}
						} else {

							$intNextPostDay				= current( $arrintDaysOfMonth );
							$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
							$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
							$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
							$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );

						}
					} else {

						if( 1 == \Psi\Libraries\UtilFunctions\count( $arrintDaysOfMonth ) ) {

							$intNextPostDay = current( $arrintDaysOfMonth );

							if( 0 == $intNextPostDay ) {

								$objDateTimeNextPostDate	= new DateTime( $strPostDate );
								$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
								$objDateTimeNextPostDate->modify( 'last day of this month' );
								$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );

							} else {

								$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
								$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
								$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
								$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
							}

						} else {

							$intNextPostDay				= current( $arrintDaysOfMonth );
							$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
							$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
							$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
							$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
						}
					}
				}

				$objPostDate = new DateTime( $strPostDate );
				$objPostDate->modify( 'first day of this month' );

				$objNextPostDate = new DateTime( $strNextPostDate );
				$objNextPostDate->modify( 'first day of this month' );

				$objInterval = date_diff( $objPostDate, $objNextPostDate );

				if( $objInterval->format( '%m' ) != $intFrequencyInterval && $objInterval->format( '%m' ) != 0 ) {

					$intNextPostDay		= current( $arrintDaysOfMonth );
					$objNextPostDate	= new DateTime( $strNextPostDate );
					$objNextPostDate->modify( 'last day of previous month' );
					$strNextPostDate	= $objNextPostDate->format( 'm/d/Y' );

					if( 0 != $intNextPostDay ) {

						$strNextPostDateCopy = $strNextPostDate;

						$strNextPostDate	= date( 'm', strtotime( $strNextPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strNextPostDate ) );
						$arrstrNextPostDate	= explode( '/', $strNextPostDate );

						if( 3 == \Psi\Libraries\UtilFunctions\count( $arrstrNextPostDate ) && false == checkdate( $arrstrNextPostDate[0], $arrstrNextPostDate[1], $arrstrNextPostDate[2] ) ) {
							$strNextPostDate = $strNextPostDateCopy;
						}
					}
				}
				break;

			case CFrequency::WEEKDAY_OF_MONTH:
				$strDayName = $arrmixDayNames[$this->getDayOfWeek()];
				$strRelativeNumber = $arrmixRelativeFormats[$intFrequencyInterval];
				$objNextPostMonthDatetime = new DateTime( $strPostDate );
				$objNextPostMonthDatetime->modify( $strRelativeNumber . ' ' . $strDayName . ' of next month' );
				$strNextPostDate = $objNextPostMonthDatetime->format( 'm/d/Y' );
				break;

			default:
		}

		return date( 'm/d/Y', strtotime( $strNextPostDate ) );
	}

	public function isModifiedScheduleJE( $objOldGlHeaderSchedule ) {

		$boolIsEdit	= false;

		// if updating existing JE template by selecting schedule JE
		if( true == valObj( $objOldGlHeaderSchedule, 'CGlHeaderSchedule' ) && false == valId( $objOldGlHeaderSchedule->getId() ) ) {
			return true;
		}

		if( true == valObj( $objOldGlHeaderSchedule, 'CGlHeaderSchedule' ) && true == valId( $objOldGlHeaderSchedule->getId() ) ) {

			// If GlHeaderSchedule is modified
			if( $this->getFrequencyId() != $objOldGlHeaderSchedule->getFrequencyId()
			|| $this->getFrequencyInterval() != $objOldGlHeaderSchedule->getFrequencyInterval()
			|| $this->getDayOfWeek() != $objOldGlHeaderSchedule->getDayOfWeek()
			|| $this->getDaysOfMonth() != $objOldGlHeaderSchedule->getDaysOfMonth()
			|| $this->getStartDate() != $objOldGlHeaderSchedule->getStartDate()
			|| $this->getEndDate() != $objOldGlHeaderSchedule->getEndDate()
			|| $this->getNumberOfOccurrences() != $objOldGlHeaderSchedule->getNumberOfOccurrences()
			|| $this->getLastPostedDate() != $objOldGlHeaderSchedule->getLastPostedDate()
			|| $this->getNextPostDate() != $objOldGlHeaderSchedule->getNextPostDate()
			|| $this->getIsReverseNextMonth() != $objOldGlHeaderSchedule->getIsReverseNextMonth()
			|| $this->getIsPaused() != $objOldGlHeaderSchedule->getIsPaused()
			|| $this->getApprovedBy() != $objOldGlHeaderSchedule->getApprovedBy()
			|| $this->getApprovedOn() != $objOldGlHeaderSchedule->getApprovedOn()
			|| $this->getDisabledBy() != $objOldGlHeaderSchedule->getDisabledBy()
			|| $this->getDisabledOn() != $objOldGlHeaderSchedule->getDisabledOn() ) {
				$boolIsEdit = true;
			}
		}
		return $boolIsEdit;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchFiles( $objDatabase ) {
		return CFiles::fetchFilesByGlHeaderScheduleIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

}
?>