<?php

class CScheduledCallType extends CBaseScheduledCallType {

	const RENT_REMINDER 			= 1;
	const LATE_RENT_NOTICE 			= 2;
	const HAPPY_BIRTHDAY_MESSAGE 	= 3;
	const CUSTOM					= 4;
	const EMERGENCY					= 5;

	/**
	 * Get Functions
	 */

	public function getBeginSendTimeHour() {
		$arrstrBeginSendTime = explode( ':', $this->m_strBeginSendTime );

		return $arrstrBeginSendTime['0'];
	}

	public function getBeginSendTimeMinute() {
		$arrstrBeginSendTime = explode( ':', $this->m_strBeginSendTime );

		if( true == valArr( $arrstrBeginSendTime, 2 ) ) {
			return $arrstrBeginSendTime['1'];
		} else {
			return 0;
		}
	}

	public function getEndSendTimeHour() {
		$arrstrEndSendTime = explode( ':', $this->m_strEndSendTime );

		return $arrstrEndSendTime['0'];
	}

	public function getEndSendTimeMinute() {
		$arrstrEndSendTime = explode( ':', $this->m_strEndSendTime );

		if( true == valArr( $arrstrEndSendTime, 2 ) ) {
			return $arrstrEndSendTime['1'];
		} else {
			return 0;
		}
	}

}
?>