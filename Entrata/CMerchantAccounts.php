<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMerchantAccounts
 * Do not add any new functions to this class.
 */

class CMerchantAccounts extends CBaseMerchantAccounts {

	const MERCHANT_ACCOUNTS							= 'merchant_accounts';

	const VIEW_COMPANY_MERCHANT_ACCOUNTS 					= 'view_merchant_accounts';
	const VIEW_RECURRING_COMPANY_MERCHANT_ACCOUNTS 			= 'view_recurring_merchant_accounts';
	const VIEW_TERMINAL_COMPANY_MERCHANT_ACCOUNTS 			= 'view_terminal_merchant_accounts';

	/**
	 * @param CMerchantAccount $objMerchantAccount
	 * @param CDatabase $objClientDatabase
	 * @param $intPaymentMediumId
	 */
	public static function setDataOnObject( $objMerchantAccount, $objClientDatabase, $intPaymentMediumId = NULL ) {
		if( false == valObj( $objMerchantAccount, CMerchantAccount::class ) ) {
			return;
		}

		if( true == is_null( $intPaymentMediumId ) ) {
			$intPaymentMediumId = CPaymentMedium::WEB;
		}

		$objMerchantAccount->setPaymentMediumId( $intPaymentMediumId );
		$objMerchantAccount->setClientDatabase( $objClientDatabase );
	}

	/**
	 * @param CMerchantAccount[] $arrobjMerchantAccounts
	 * @param CDatabase $objClientDatabase
	 * @param int $intPaymentMediumId
	 */
	public static function setDataOnObjects( $arrobjMerchantAccounts, $objClientDatabase, $intPaymentMediumId = NULL ) {
		if( false == valArr( $arrobjMerchantAccounts ) ) {
			return;
		}

		foreach( $arrobjMerchantAccounts as $objMerchantAccount ) {
			self::setDataOnObject( $objMerchantAccount, $objClientDatabase, $intPaymentMediumId );
		}
	}

	public static function fetchViewMerchantAccountsByIdsByCid( $arrintCompanyMerchantAccountIds, $intCid, $objClientDatabase, $intPaymentMediumId = NULL, $strOrderBy = NULL ) {
		if( false == valArr( $arrintCompanyMerchantAccountIds ) ) return NULL;
		$strOrderBySubSql = '';
		if( true == valStr( $strOrderBy ) ) {
			$strOrderBySubSql = 'ORDER BY merchant_processing_type_id';
		}
		$strSql = 'SELECT * FROM ' . self::MERCHANT_ACCOUNTS . ' WHERE cid = ' . ( int ) $intCid . ' AND id IN ( ' . implode( ',', $arrintCompanyMerchantAccountIds ) . ' )';
		$strSql .= $strOrderBySubSql;

		$arrobjMerchantAccounts = self::fetchMerchantAccounts( $strSql, $objClientDatabase );

		self::setDataOnObjects( $arrobjMerchantAccounts, $objClientDatabase, $intPaymentMediumId );

		return $arrobjMerchantAccounts;
	}

	public static function fetchActiveMerchantAccountsByIdsByCid( $arrintCompanyMerchantAccountIds, $intCid, $objClientDatabase, $intPaymentMediumId = NULL ) {
		if( false == valArr( $arrintCompanyMerchantAccountIds ) ) return NULL;
		$strSql = 'SELECT * FROM ' . self::MERCHANT_ACCOUNTS . ' WHERE cid = ' . ( int ) $intCid . ' AND id IN ( ' . implode( ',', $arrintCompanyMerchantAccountIds ) . ' ) AND is_disabled <> 1 ORDER BY account_name';
		$arrobjMerchantAccounts = self::fetchMerchantAccounts( $strSql, $objClientDatabase );

		self::setDataOnObjects( $arrobjMerchantAccounts, $objClientDatabase, $intPaymentMediumId );

		return $arrobjMerchantAccounts;
	}

	public static function fetchMerchantAccountByMerchantAccountIdByPaymentMediumIdByCid( $intMerchantAccountId, $intPaymentMediumId, $intCid, $objClientDatabase ) {
		if( false == is_numeric( $intMerchantAccountId ) ) return NULL;
		$strSql = 'SELECT * FROM ' . self::MERCHANT_ACCOUNTS . ' WHERE id = ' . ( int ) $intMerchantAccountId . ' AND cid = ' . ( int ) $intCid;
		$objMerchantAccount = CMerchantAccounts::fetchMerchantAccount( $strSql, $objClientDatabase );

		self::setDataOnObject( $objMerchantAccount, $objClientDatabase, $intPaymentMediumId );

		return $objMerchantAccount;
	}

	public static function fetchCustomMerchantAccountByIdByCid( $intId, $intCid, $objClientDatabase, $intPaymentMediumId = NULL ) {
		if ( false == is_numeric( $intPaymentMediumId ) ) $intPaymentMediumId = CPaymentMedium::WEB;
		$objMerchantAccount = self::fetchMerchantAccount( sprintf( 'SELECT * FROM %s WHERE id = %d AND cid = %d ', self::MERCHANT_ACCOUNTS, ( int ) $intId, ( int ) $intCid ), $objClientDatabase );

		self::setDataOnObject( $objMerchantAccount, $objClientDatabase, $intPaymentMediumId );

		return $objMerchantAccount;
	}

	public static function fetchActiveMerchantAccountsByCid( $intCid, $objDatabase, $intPaymentMediumId = NULL, $strOrderBy = 'account_name' ) {
		$arrobjMerchantAccounts = self::fetchMerchantAccounts( sprintf( 'SELECT * FROM %s WHERE cid = %d AND is_disabled != 1 ORDER BY ' . $strOrderBy, self::MERCHANT_ACCOUNTS, ( int ) $intCid ), $objDatabase );

		self::setDataOnObjects( $arrobjMerchantAccounts, $objDatabase, $intPaymentMediumId );

		return $arrobjMerchantAccounts;
	}

	public static function fetchInternationalMerchantAccounts( $intCid, $objClientDatabase, $intPaymentMediumId = NULL, $arrstrCountryCodes = NULL ) {
		$strSql = 'SELECT * FROM %s WHERE cid = %d AND merchant_processing_type_id = ' . ( int ) CMerchantProcessingType::INTERNATIONAL;
		if( is_null( $arrstrCountryCodes ) == false ) $strSql .= ' AND country_code IN (\'' . implode( '\',\'', $arrstrCountryCodes ) . '\')';
		$strSql .= ' ORDER BY account_name';
		$arrobjMerchantAccounts = self::fetchMerchantAccounts( sprintf( $strSql, self::MERCHANT_ACCOUNTS, ( int ) $intCid ), $objClientDatabase );

		self::setDataOnObjects( $arrobjMerchantAccounts, $intPaymentMediumId );

		return $arrobjMerchantAccounts;
	}

	public static function fetchActiveMerchantAccountsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase, $intPaymentMediumId = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						ma.*
					FROM
    					' . self::MERCHANT_ACCOUNTS . ' ma, property_merchant_accounts pma
    				WHERE
    					ma.id = pma.company_merchant_account_id
						AND	ma.cid = pma.cid
						AND	pma.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
    					AND ma.cid = ' . ( int ) $intCid . '
    					AND ma.is_disabled <> 1
    					AND pma.disabled_by IS NULL
    					AND pma.disabled_on IS NULL
					ORDER BY
						account_name';

		$arrobjMerchantAccounts = self::fetchMerchantAccounts( $strSql, $objClientDatabase );

		self::setDataOnObjects( $arrobjMerchantAccounts, $objClientDatabase, $intPaymentMediumId );

		return $arrobjMerchantAccounts;
	}

	public static function fetchMerchantAccountsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase, $intPaymentMediumId = NULL, $boolShowDisabledMerchantAccount = false ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
    					ma.*
    			   FROM
    					' . self::MERCHANT_ACCOUNTS . ' ma
    					JOIN property_merchant_accounts pma ON ( ma.cid = pma.cid AND ma.id = pma.company_merchant_account_id )
    				WHERE
    					ma.cid = ' . ( int ) $intCid . '
    					AND pma.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		if( false == $boolShowDisabledMerchantAccount ) {
			$strSql .= 'AND pma.disabled_by IS NULL
    					AND pma.disabled_on IS NULL ';
		}
		$strSql	.= 'ORDER BY
    					account_name';

		$arrobjMerchantAccounts = self::fetchMerchantAccounts( $strSql, $objClientDatabase );

		self::setDataOnObjects( $arrobjMerchantAccounts, $objClientDatabase, $intPaymentMediumId );

		return $arrobjMerchantAccounts;
	}

	public static function fetchMerchantAccountsByPropertyIdByCid( $intCid, $objDatabase, $intPropertyId = NULL, $arrintPropertyIds = NULL ) {
		$strSql = 'SELECT
    					ma.*,
    					pma.property_id,
    					pma.ar_code_id
    				FROM
    					merchant_accounts ma
    					JOIN property_merchant_accounts pma ON ( ma.id = pma.company_merchant_account_id AND ma.cid = pma.cid AND pma.disabled_by IS NULL AND pma.disabled_on IS NULL )
    				WHERE
    				    ma.is_disabled = 0
    					AND ma.cid = ' . ( int ) $intCid;
		if( NULL != $intPropertyId ) {
			$strSql .= ' AND pma.property_id = ' . ( int ) $intPropertyId;
		}
		if( NULL != $arrintPropertyIds ) {
			$strSql .= ' AND pma.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' );';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveMerchantAccountCountByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase, $intPaymentMediumId = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT count( ma.id )
			       FROM
			    		' . self::MERCHANT_ACCOUNTS . ' ma, property_merchant_accounts pma
			       WHERE
			    		ma.id= pma.company_merchant_account_id
			    		AND ma.cid = pma.cid
			    		AND pma.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
			    		AND pma.disabled_by IS NULL
			    		AND pma.disabled_on IS NULL
			    		AND ma.cid = ' . ( int ) $intCid . '
			    		AND ma.is_disabled <> 1';

		$arrstrResults = fetchData( $strSql, $objClientDatabase );

		if( true == isset ( $arrstrResults[0]['count'] ) ) {
			return $arrstrResults[0]['count'];
		} else {
			return 0;
		}
	}

	public static function fetchActiveMerchantAccountsByPropertyIdByCid( $intPropertyId, $intCid, $objClientDatabase, $intPaymentMediumId = NULL ) {

		$strSql = 'SELECT
    						ma.*,
    						pma.ar_code_id
    			       FROM
    			       		' . self::MERCHANT_ACCOUNTS . ' ma,
    			       		property_merchant_accounts pma
    			       WHERE
    			    		ma.id= pma.company_merchant_account_id
    			       		AND ma.cid = pma.cid
    				    	AND	pma.property_id = ' . ( int ) $intPropertyId . '
    				    	AND ma.cid = ' . ( int ) $intCid . '
    			    		AND ma.is_disabled <> 1
    				    	AND pma.disabled_by IS NULL
    				    	AND pma.disabled_on IS NULL
    				ORDER BY
    						COALESCE ( pma.ar_code_id, 0 );';

		$arrobjMerchantAccounts = self::fetchMerchantAccounts( $strSql, $objClientDatabase );

		self::setDataOnObjects( $arrobjMerchantAccounts, $objClientDatabase, $intPaymentMediumId );

		return $arrobjMerchantAccounts;
	}

	public static function fetchEligibleMerchantAccountsByBankAccountIdByCid( $intBankAccountId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ma.*
					FROM
						merchant_accounts ma
						LEFT JOIN bank_account_merchant_accounts bama ON ( ma.cid = bama.cid AND ma.id = bama.merchant_account_id )
					WHERE
						ma.cid = ' . ( int ) $intCid . '
						AND ma.is_disabled <> 1
						AND ( bama.bank_account_id = ' . ( int ) $intBankAccountId . ' OR bama.bank_account_id IS NULL )';

		$arrobjMerchantAccounts = self::fetchMerchantAccounts( $strSql, $objClientDatabase );

		self::setDataOnObjects( $arrobjMerchantAccounts, $objClientDatabase );

		return $arrobjMerchantAccounts;
	}

	public static function fetchEligibleMerchantAccountsByBankAccountIdsByCid( $arrintBankAccountIds, $intCid, $objClientDatabase ) {

		if ( false == valArr( $arrintBankAccountIds ) ) return NULL;

		$strSql = 'SELECT
						ma.*,
						bama.bank_account_id
					FROM
						merchant_accounts ma
						LEFT JOIN bank_account_merchant_accounts bama ON ( ma.cid = bama.cid AND ma.id = bama.merchant_account_id )
					WHERE
						ma.cid = ' . ( int ) $intCid . '
						AND ma.is_disabled <> 1
						AND bama.bank_account_id IN (' . implode( ',', $arrintBankAccountIds ) . ') ';

		$arrobjMerchantAccounts = self::fetchMerchantAccounts( $strSql, $objClientDatabase );

		self::setDataOnObjects( $arrobjMerchantAccounts, $objClientDatabase );

		return $arrobjMerchantAccounts;
	}

	public static function fetchMerchantAccountsByPropertyIdByArCodeIdsByCid( $intPropertyId, $arrintArCodeIds, $intCid, $objDatabase, $intPaymentMediumId, $boolUseView ) {
		$strTableName = ( true == $boolUseView ) ? self::MERCHANT_ACCOUNTS : 'merchant_accounts';

		$strSql = 'SELECT
						ma.*,
        				pma.ar_code_id
        			FROM
			    		' . $strTableName . ' ma,
        				property_merchant_accounts pma
        			WHERE
        				ma.id = pma.company_merchant_account_id
			    		AND ma.cid = pma.cid
        				AND pma.property_id = ' . ( int ) $intPropertyId . '
        				AND pma.cid = ' . ( int ) $intCid . '
    					AND pma.disabled_by IS NULL
        				AND pma.disabled_on IS NULL';

		$strSql .= ( true == valArr( $arrintArCodeIds ) ) ? ' AND ( pma.ar_code_id IS NULL OR pma.ar_code_id IN ( ' . implode( ',', $arrintArCodeIds ) . ' ) )' : ' AND pma.ar_code_id IS NULL';

		$arrobjMerchantAccounts = self::fetchMerchantAccounts( $strSql, $objDatabase );

		if( false == $boolUseView ) {
			return $arrobjMerchantAccounts;
		}

		self::setDataOnObjects( $arrobjMerchantAccounts, $objDatabase, $intPaymentMediumId );

		return $arrobjMerchantAccounts;
	}

	public static function fetchMerchantAccountsDataByPropertyIdByArCodeIdsByCid( $intPropertyId, $arrintArCodeIds, $intCid, $objDatabase, $intPaymentMediumId, $boolUseView ) {
		if( false == valArr( $arrintArCodeIds ) ) return NULL;
		$strTableName = ( true == $boolUseView ) ? self::MERCHANT_ACCOUNTS : 'merchant_accounts';

		$strSql = 'SELECT
						ma.id,
						pma.ar_code_id
					FROM
						' . $strTableName . ' ma,
						property_merchant_accounts pma
					WHERE
						ma.id = pma.company_merchant_account_id
						AND ma.cid = pma.cid
						AND pma.property_id = ' . ( int ) $intPropertyId . '
						AND pma.cid = ' . ( int ) $intCid . '
						AND pma.disabled_by IS NULL
						AND pma.disabled_on IS NULL
						AND pma.ar_code_id IN ( ' . implode( ',', $arrintArCodeIds ) . ' )';

		$arrobjMerchantAccounts = self::fetchMerchantAccounts( $strSql, $objDatabase );

		if( false == $boolUseView ) {
			return $arrobjMerchantAccounts;
		}

		self::setDataOnObjects( $arrobjMerchantAccounts, $objDatabase, $intPaymentMediumId );

		return $arrobjMerchantAccounts;
	}

	public static function fetchDefaultMerchantAccountByCidByPropertyId( $intCid, $intPropertyId, $objClientDatabase, $intPaymentMediumId = NULL ) {

		$strSql = 'SELECT ma.*
			    	FROM
			    		' . self::MERCHANT_ACCOUNTS . ' ma,
			    		property_merchant_accounts pma
			    	WHERE
			    		ma.id= pma.company_merchant_account_id
			    		AND ma.cid = pma.cid
			    		AND	pma.property_id = ' . ( int ) $intPropertyId . '
			    		AND ma.cid = ' . ( int ) $intCid . '
			    		AND pma.ar_code_id IS NULL
			    		AND pma.disabled_by IS NULL
			    		AND pma.disabled_on IS NULL';

		$objMerchantAccount = self::fetchMerchantAccount( $strSql, $objClientDatabase );

		self::setDataOnObject( $objMerchantAccount, $objClientDatabase, $intPaymentMediumId );

		return $objMerchantAccount;
	}

	public static function fetchDefaultMerchantAccountsByCidByPropertyIds( $arrintPropertyIds, $objClientDatabase, $intPaymentMediumId = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT ma.*, pma.property_id,pma.cid
					FROM
						' . self::MERCHANT_ACCOUNTS . ' ma,
						property_merchant_accounts pma
					WHERE
						ma.id= pma.company_merchant_account_id
						AND ma.cid = pma.cid
						AND	pma.property_id in( ' . implode( ',', $arrintPropertyIds ) . ')
						AND pma.ar_code_id IS NULL
						AND pma.disabled_by IS NULL
						AND pma.disabled_on IS NULL';

		$arrmixData = fetchData( $strSql, $objClientDatabase );

		return $arrmixData;

	}

	public static function fetchCustomDefaultMerchantAccountsDataByCidByPropertyIds( $intCid, $arrintPropertyIds, $objClientDatabase, $intPaymentMediumId = NULL ) {
		if( false == valId( $intCid ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT ma.id, ma.enable_visa_pilot, pma.property_id, pma.cid
					FROM
						' . self::MERCHANT_ACCOUNTS . ' ma,
						property_merchant_accounts pma
					WHERE
						ma.id= pma.company_merchant_account_id
						AND ma.cid = pma.cid
						AND	pma.property_id in( ' . implode( ',', $arrintPropertyIds ) . ')
						AND ma.cid = ' . ( int ) $intCid . '
						AND ma.is_disabled <> 1
						AND pma.ar_code_id IS NULL
						AND pma.disabled_by IS NULL
						AND pma.disabled_on IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchDefaultMerchantAccountIdByCidByPropertyId( $intCid, $intPropertyId, $objClientDatabase, $intPaymentMediumId = NULL ) {

		$strSql = 'SELECT
						ma.id as id
					FROM
						 ' . self::MERCHANT_ACCOUNTS . ' ma,
						property_merchant_accounts pma
					WHERE
						ma.id= pma.company_merchant_account_id
						AND ma.cid = pma.cid
						AND	pma.property_id = ' . ( int ) $intPropertyId . '
						AND ma.cid = ' . ( int ) $intCid . '
						AND pma.ar_code_id IS NULL
						AND pma.disabled_by IS NULL
						AND pma.disabled_on IS NULL';
		return self::fetchColumn( $strSql, 'id', $objClientDatabase );
	}

	public static function fetchEmoPaymentAmountByCidByPropertyId( $intCid, $intPropertyId, $objClientDatabase, $intPaymentMediumId = NULL ) {
		if( true == is_null( $intPaymentMediumId ) ) {
			$intPaymentMediumId = CPaymentMedium::WEB;
		}

		$strSql = 'SELECT mm.max_payment_amount AS emo_max_payment_amount
					FROM
						' . self::MERCHANT_ACCOUNTS . ' ma,
						merchant_methods mm,
						property_merchant_accounts pma
					WHERE
						ma.id= pma.company_merchant_account_id
						AND ma.id = mm.merchant_account_id
						AND mm.payment_type_id = ' . CPaymentType::EMONEY_ORDER . '
						AND mm.payment_medium_id = ' . ( int ) $intPaymentMediumId . '
						AND ma.cid = pma.cid
						AND	pma.property_id = ' . ( int ) $intPropertyId . '
						AND ma.cid = ' . ( int ) $intCid . '
						AND pma.ar_code_id IS NULL
						AND pma.disabled_by IS NULL
						AND pma.disabled_on IS NULL';

		$arrintResponse = fetchData( $strSql, $objClientDatabase );
		if( true == isset( $arrintResponse[0]['emo_max_payment_amount'] ) ) return $arrintResponse[0]['emo_max_payment_amount'];

		return 0;
	}

	public static function fetchMerchantAccountCountByCidByPropertyIds( $intCid, $arrintPropertyIds, $objClientDatabase, $boolIsDefault = false ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		if( true == $boolIsDefault ) {
			$strAndCondition = ' AND pma.ar_code_id IS NULL';
		} else {
			$strAndCondition = ' AND ma.is_disabled <> 1';
		}

		$strSql = 'SELECT count( ma.id ) as count
				   FROM
						view_merchant_accounts ma, property_merchant_accounts pma
					WHERE
						ma.id= pma.company_merchant_account_id
						AND ma.cid = pma.cid
						AND pma.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ma.cid = ' . ( int ) $intCid . $strAndCondition . '
						AND pma.disabled_by IS NULL
						AND pma.disabled_on IS NULL
						AND ma.convert_check21_to_ach = 1';

		$arrstrResults = fetchData( $strSql, $objClientDatabase );

		if( true == isset ( $arrstrResults[0]['count'] ) ) {
			return $arrstrResults[0]['count'];
		} else {
			return 0;
		}

	}

	// Here we pull company merchant accounts, but map them into the merchant accounts objects from the works database

	public static function fetchPseudoCompanyMerchantAccountsByCid( $intCid, $objClientDatabase ) {
		$strSql = 'SELECT * FROM company_merchant_accounts WHERE cid = ' . ( int ) $intCid;

		return self::fetchMerchantAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchPseudoCompanyMerchantAccountsByIdsByCid( $arrintMerchantAccountIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintMerchantAccountIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT * FROM company_merchant_accounts WHERE id IN ( ' . implode( ',', $arrintMerchantAccountIds ) . ' ) AND cid = ' . ( int ) $intCid;

		return self::fetchMerchantAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchMerchantAccountsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase, $intPropertyId = NULL ) {
		$strSql = 'SELECT
						ma.*,
						pma.property_id,
						pma.ar_code_id
					FROM
						merchant_accounts ma
						JOIN property_merchant_accounts pma on ( ma.id = pma.company_merchant_account_id and ma.cid = pma.cid AND pma.disabled_by IS NULL AND pma.disabled_on IS NULL )
    					JOIN view_company_user_properties cup on ( cup.property_id = pma.property_id and cup.cid = pma.cid )
					WHERE
					    ma.is_disabled = 0
						AND company_user_id = ' . ( int ) $intCompanyUserId . '
						AND ma.cid = ' . ( int ) $intCid;

		if( false == is_null( $intPropertyId ) ) {
			$strSql .= ' AND pma.property_id = ' . ( int ) $intPropertyId;
		}

		return fetchData( $strSql, $objDatabase );

		return self::fetchMerchantAccounts( $strSql, $objDatabase, $objDatabase );
	}

	public static function fetchMerchantAccountNamesByMerchantAccountIdsByCid( $arrintMerchantAccountIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintMerchantAccountIds ) ) return NULL;
		$strSql = '
				SELECT
					ma.id,
					ma.account_name,
					ma.clearing_account_id,
					ma.billing_account_id
				FROM
					merchant_accounts ma
				WHERE
					ma.cid = ' . ( int ) $intCid . '
					AND ma.id IN ( ' . implode( ',', $arrintMerchantAccountIds ) . ' )
				ORDER BY ma.account_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDonationAccountIdsByMerchantAccountIdsByCid( $arrintMerchantAccountIds, $intCid, $objDatabase ) {
		$strSql = '
			SELECT
					DISTINCT ma.donation_account_id as id
				FROM
					merchant_accounts ma
				WHERE
					ma.id IN ( ' . implode( ',', $arrintMerchantAccountIds ) . ' )
					AND ma.donation_account_id IS NOT NULL
					AND ma.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMerchantAccountDetailsForLitleByAccountIdByCid( $intMerchantAccountId, $intCid, $objDatabase, $boolUseExitingLegalEntities = false ) {
		$strSql = '
			SELECT
			--LEGAL ENTITY DETAILS
					ma.tax_legal_name AS legal_entity_name,
					(CASE WHEN REPLACE (ma.tax_legal_name, \'.\', \'\') ILIKE \'%llc\' THEN \'LIMITED_LIABILITY_COMPANY\'
						WHEN REPLACE (ma.tax_legal_name, \'.\', \'\') ILIKE \'%inc\' THEN \'CORPORATION\'
						WHEN REPLACE (ma.tax_legal_name, \'.\', \'\') ILIKE \'%lp\' THEN \'PARTNERSHIP\'
						WHEN REPLACE (ma.tax_legal_name, \'.\', \'\') ILIKE \'%limited partnership\' THEN \'PARTNERSHIP\'
						WHEN REPLACE (ma.tax_legal_name, \'.\', \'\') ILIKE \'%ltd\' THEN \'PARTNERSHIP\'
						WHEN REPLACE (ma.tax_legal_name, \'.\', \'\') ILIKE \'%corp\' THEN \'CORPORATION\'
						WHEN REPLACE (ma.tax_legal_name, \'.\', \'\') ILIKE \'%corporation\' THEN \'CORPORATION\'
						ELSE null END) AS legal_entity_type,
					\'n/a\' AS doing_business_as,
					\'USA\' AS country_code,
					COALESCE( acp.sum_total_payment_amount, 99000 ) AS annual_credit_card_sales_volume,
					--acp.sum_total_payment_amount AS annual_credit_card_sales_volume,
					(CASE WHEN acp.sum_total_payment_amount > 0 THEN 1 ELSE 0 END) AS accepted_cc_in_past,
			--SUBMERCHANT FIELDS
					ma.id AS psp_merchant_id,
					SUBSTRING(mc.company_name FROM 1 FOR 49) AS merchant_name,
					COALESCE (w.sub_domain || \'.residentportal.com\', \'\') AS url,
					(CASE
						WHEN ma.cid = ' . CClient::ID_BRE . ' THEN ' . CCompanyMerchantAccount::CLIENT_BRE_PHONE . '
						ELSE ' . CCompanyMerchantAccount::CLIENT_PSI_PHONE . '
					END) AS customer_service_number,
					\'ETT*\' || substring(replace (replace (replace (replace (replace (replace (replace (replace (replace (replace (replace (ma.account_name, \'-\', \'\'), \'#\', \'\'), \'$\', \'\'), \'&\',
					\'\'), \')\', \'\'), \'(\', \'\'), \'.\', \'\'), \',\', \'\'), \'/\', \'\'), (COALESCE (p.lookup_code,\' \')), \'\'), \' \', \'\') FROM 3 FOR 50) || \'RENT\' AS default_billing_descriptor,
					mpa.max_transaction_amount,
					\'USD\' AS purchase_currency,
					\'Merchant\' AS primary_contact_first_name,
					\'Services\' AS primary_contact_last_name,
					\'' . CSystemEmail::MERCHANTSERVICES_EMAIL_ADDRESS . '\' AS primary_contact_email,
					\'' . CCompanyMerchantAccount::PRIMARY_CONTACT_PHONE . '\' AS primary_contact_phone
			FROM merchant_accounts ma
			JOIN clients mc ON ma.cid = mc.id
			LEFT JOIN --accepted_cc_in_past
				(
				SELECT ap.cid,
						ap.company_merchant_account_id AS ma_id,
						max(ap.total_amount) AS max_transaction_amount,
						sum(ap.total_amount) AS sum_total_payment_amount
				FROM ar_payments ap
				WHERE ap.payment_type_id in ( ' . CPaymentType::VISA . ', ' . CPaymentType::MASTERCARD . ', ' . CPaymentType::DISCOVER . ', ' . CPaymentType::AMEX . ')
				AND ap.captured_on > NOW() - INTERVAL \'12 months\'
				AND ap.cid = ' . ( int ) $intCid . '
				GROUP BY 1,2
				) AS acp ON acp.cid = ma.cid AND acp.ma_id = ma.id
			LEFT JOIN --max_payment_amount
				(
				SELECT mm.cid,
						mm.merchant_account_id AS ma_id,
						max(mm.max_payment_amount) as max_transaction_amount
				FROM merchant_methods mm
				WHERE mm.payment_medium_id IN ( ' . CPaymentMedium::WEB . ' , ' . CPaymentMedium::RECURRING . ' )
				AND mm.payment_type_id IN ( ' . CPaymentType::VISA . ', ' . CPaymentType::MASTERCARD . ', ' . CPaymentType::DISCOVER . ', ' . CPaymentType::AMEX . ')
				AND mm.merchant_gateway_id = ' . CMerchantGateway::LITLE_CREDIT_CARD . '
				AND mm.cid = ' . ( int ) $intCid . '
				GROUP BY 1,2
				) AS mpa ON mpa.cid = ma.cid AND mpa.ma_id = ma.id
			LEFT JOIN --default property
				(
				SELECT pma.cid,
						pma.company_merchant_account_id AS cma_id,
						min(pma.property_id) AS property_id
				FROM property_merchant_accounts pma
				JOIN properties p on p.id = pma.property_id AND p.cid = pma.cid AND p.is_disabled <> 1
				WHERE pma.ar_code_id IS NULL
					AND pma.cid = ' . ( int ) $intCid . '
				GROUP BY 1,2
				) AS dp on dp.cid = ma.cid AND dp.cma_id = ma.id
			LEFT JOIN properties p ON p.cid = dp.cid AND p.id = dp.property_id
			LEFT JOIN --default website
				(
				SELECT wp.cid,
						wp.property_id,
						min(wp.website_id) as website_id
				FROM website_properties wp
				JOIN properties p ON p.id = wp.property_id AND p.cid = wp.cid AND p.is_disabled <> 1
				JOIN websites w ON w.id = wp.website_id AND w.cid = wp.cid AND w.is_disabled <> 1 AND w.deleted_on IS NULL
				WHERE p.cid = ' . ( int ) $intCid . '
				GROUP BY 1,2
				) AS dw ON dw.cid = p.cid AND dw.property_id = p.id
			LEFT JOIN websites w ON w.cid = dw.cid AND w.id = dw.website_id
			WHERE 1 = 1
			AND ma.is_disabled <> 1
			AND ma.id = ' . ( int ) $intMerchantAccountId;

		if( false == $boolUseExitingLegalEntities ) {
			$strSql .= ' AND ( ma.external_legal_entity_id IS NULL OR ma.external_legal_entity_id = \'\' ) ';
		}

		$strSql .= 'AND ma.cid = ' . ( int ) $intCid . '
			ORDER BY 1,2';

		$arrmixData = fetchData( $strSql, $objDatabase );
		return ( true == isset ( $arrmixData[0] ) ) ? $arrmixData[0] : NULL;
	}

	public static function fetchMerchantAccountsByIdsByCid( $arrintIds, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						merchant_accounts
					WHERE
						cid = ' . ( int ) $intCid . ' AND ';

		if( true == valArr( $arrintIds ) ) {
			$strSql .= ' id IN ( ' . implode( ',', $arrintIds ) . ' )';
		} else {
			$strSql .= ' id IN ( ' . $arrintIds . ' )';
		}
		$arrobjMerchantAccounts = self::fetchMerchantAccounts( $strSql, $objClientDatabase );

		self::setDataOnObjects( $arrobjMerchantAccounts, $objClientDatabase );

		return $arrobjMerchantAccounts;
	}

	public static function fetchEligibleMerchantAccountsByBankAccountLogIdsByCid( $arrintBankAccountLogIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintBankAccountLogIds ) ) return NULL;

		$strSql = 'SELECT
						ma.id,
						ma.account_name
					FROM
						merchant_accounts ma
						JOIN bank_account_merchant_account_logs bamal ON ( ma.cid = bamal.cid AND ma.id = bamal.merchant_account_id )
					WHERE
						ma.cid = ' . ( int ) $intCid . '
						AND ma.is_disabled <> 1
						AND bamal.bank_account_log_id IN ( ' . implode( ',', $arrintBankAccountLogIds ) . ' )';

		$arrobjMerchantAccounts = self::fetchMerchantAccounts( $strSql, $objClientDatabase );

		self::setDataOnObjects( $arrobjMerchantAccounts, $objClientDatabase );

		return $arrobjMerchantAccounts;
	}

	public static function fetchDefaultMerchantAccountsWithPropertyNameByPropertyIdsByCid( $intCid, $arrintPropertyIds, $objClientDatabase, $intPaymentMediumId = NULL ) {

		$strSql = 'WITH property_merchant_account_count AS (
                    SELECT
                        pma.company_merchant_account_id,
                        count ( DISTINCT ( pma.property_id ) ) AS prop_count
                    FROM
                        property_merchant_accounts pma
                        LEFT JOIN merchant_accounts ma ON ( pma.company_merchant_account_id = ma.id AND pma.cid = ma.cid )
                    WHERE
                        ma.cid = ' . ( int ) $intCid . '
                        AND pma.ar_code_id IS NULL
                        AND pma.disabled_by IS NULL
                        AND pma.disabled_on IS NULL
                    GROUP BY
                        pma.company_merchant_account_id )
                    SELECT
                        ma.*,
                        p.property_name,
                        p.number_of_units,
                        p.id AS property_id,
                        pmac.prop_count
                    FROM
                        merchant_accounts ma
                        LEFT JOIN property_merchant_accounts pma ON ( pma.company_merchant_account_id = ma.id AND pma.cid = ma.cid )
                        LEFT JOIN properties p ON ( p.id = pma.property_id AND pma.cid = p.cid )
                        JOIN property_merchant_account_count pmac ON ( pmac.company_merchant_account_id = ma.id )
                    WHERE
                        ma.id = pma.company_merchant_account_id
                        AND ma.cid = pma.cid
                        AND pma.property_id = p.id
                        AND p.cid = ' . ( int ) $intCid . '
                        AND pma.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
                        AND ma.cid = ' . ( int ) $intCid . '
                        AND pma.ar_code_id IS NULL
                        AND pma.disabled_by IS NULL
                        AND pma.disabled_on IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchDonationAccountIdByMerchantAccountIdByCid( $intId, $intCid, $objClientDatabase ) {
		if( false === is_numeric( $intId ) ) return NULL;
		$strSql = 'SELECT
						donation_account_id
					FROM
						merchant_accounts
					WHERE
						id = ' . ( int ) $intId . '
						AND cid = ' . ( int ) $intCid;

		$arrintDonationId = fetchData( $strSql, $objClientDatabase );
		return ( true == isset ( $arrintDonationId[0]['donation_account_id'] ) ) ? $arrintDonationId[0]['donation_account_id'] : NULL;
	}

	public static function fetchAccountsPayableMerchantAccountByCid( $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
 						  *
					 FROM
					 	  merchant_accounts
					WHERE
						  cid = ' . ( int ) $intCid . ' 
						  AND merchant_processing_type_id = ' . CMerchantProcessingType::ACCOUNTS_PAYABLE . ' 
						  AND is_disabled = 0 
					LIMIT 1';

		$objMerchantAccount = self::fetchMerchantAccount( $strSql, $objClientDatabase );

		self::setDataOnObject( $objMerchantAccount, $objClientDatabase );

		return $objMerchantAccount;
	}

	public static function fetchMerchantAccountByMerchantNameByCid( $strMerchantName, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    *
					FROM
					    merchant_accounts
					WHERE
					    merchant_name = \'' . $strMerchantName . '\'
					    AND cid =' . ( int ) $intCid;

		$objMerchantAccount = self::fetchMerchantAccount( $strSql, $objDatabase );

		self::setDataOnObject( $objMerchantAccount, $objDatabase );

		return $objMerchantAccount;
	}

	public static function fetchRecurringPaymentTypesByMerchantAccountIdByCid( $intCid, $intMerchantAccountId, $objDatabase ) {

		$strSql = 'SELECT 
						payment_type_id
					FROM 
						merchant_methods
					WHERE 
						payment_medium_id = ' . CPaymentMedium::RECURRING . ' AND ' .
			'merchant_account_id =' . ( int ) $intMerchantAccountId . ' AND ' .
			'cid = ' . ( int ) $intCid . ' AND ' .
			'is_enabled = 1;';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDefaultMerchantAccountCountByVerificationTypeByCidByPropertyIds( $intCid, $strPropertyIds, $objClientDatabase, $intPaymentMediumId = NULL ) {

		$strSql = 'SELECT count( ma.id )
			    	FROM
			    		' . self::MERCHANT_ACCOUNTS . ' ma,
			    		property_merchant_accounts pma
			    	WHERE
			    		ma.id= pma.company_merchant_account_id
			    		AND ma.cid = pma.cid
			    		AND pma.property_id IN( ' . $strPropertyIds . ' )
			    	    AND ma.account_verification_type = 1
			    		AND ma.cid = ' . ( int ) $intCid . '
			    		AND pma.ar_code_id IS NULL
			    		AND pma.disabled_by IS NULL
			    		AND pma.disabled_on IS NULL';
		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchMerchantAccountsByCidByDistributionAccountId( $intCid, $intDistributionAccountId, $objClientDatabase, $boolShowDisabledMerchantAccount = false ) {

		$strDisabledSql = '';
		if( false == $boolShowDisabledMerchantAccount ) {
			$strDisabledSql = 'AND pma.disabled_by IS NULL
    					AND pma.disabled_on IS NULL ';
		}

		$strSql = 'SELECT
    					cma.*
    			   FROM
    					company_merchant_accounts cma
    				WHERE
    					cma.cid = ' . ( int ) $intCid . '
    					AND cma.distribution_account_id = ' . ( int ) $intDistributionAccountId;
		$strSql	.= $strDisabledSql;
		$strSql	.= 'ORDER BY
    					account_name';

		$arrobjMerchantAccounts = self::fetchMerchantAccounts( $strSql, $objClientDatabase );

		self::setDataOnObjects( $arrobjMerchantAccounts, $objClientDatabase );

		return $arrobjMerchantAccounts;
	}

	public static function fetchDefaultMerchantAccountCountByClientIdByPropertyIdsByVisaPilotSetting( $intClientId, $objDatabase, $arrintPropertyIds, $boolIsVisaPilotEnabled = false ) {
		if( false == valId( $intClientId ) || false == valObj( $objDatabase, 'CDatabase' ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strIsVisaPilotEnabled = ( false == $boolIsVisaPilotEnabled ) ? 'f' : 't';

		$strPropertyJoin = ' AND pma.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )';

		$strSql = 'SELECT COUNT( ma.id )
						FROM merchant_accounts ma
						JOIN property_merchant_accounts pma ON ( pma.cid = ' . $intClientId . $strPropertyJoin . ' AND pma.cid = ma.cid
							AND pma.company_merchant_account_id = ma.id AND pma.ar_code_id IS NULL AND pma.disabled_by IS NULL
							AND pma.disabled_on IS NULL AND pma.property_id IS NOT NULL )
						WHERE ma.enable_visa_pilot = \'' . $strIsVisaPilotEnabled . '\' AND ma.is_disabled = 0';

		return fetchData( $strSql, $objDatabase )[0]['count'];
	}

}
