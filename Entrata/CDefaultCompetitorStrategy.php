<?php

class CDefaultCompetitorStrategy extends CBaseDefaultCompetitorStrategy {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRevenueCalcTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAdjustmentAmount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsFixedAmount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>