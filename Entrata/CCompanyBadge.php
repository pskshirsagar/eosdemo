<?php

class CCompanyBadge extends CBaseCompanyBadge {

    public function setDefaults() {

    	$this->m_strRewardDatetime = date( 'Y-m-d H:i:s' );

    	return;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDefaultBadgeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyActivityPointId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFileHandle() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBaseDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valProDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBaseCriteria() {
        $boolIsValid = true;

        if( true == is_null( $this->m_intBaseCriteria ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'base_criteria', __( 'Base Badge is required.' ) ) );
        } elseif( 0 >= $this->m_intBaseCriteria ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'base_criteria', __( 'Base Badge should always be greater than {%d, 0}.', [ 0 ] ) ) );
        }

        return $boolIsValid;
    }

    public function valProCriteria() {
        $boolIsValid = true;

        if( true == is_null( $this->m_intProCriteria ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pro_criteria', __( 'Pro Badge is required.' ) ) );
        } elseif( 0 >= $this->m_intProCriteria ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pro_criteria', __( 'Pro Badge should always be greater than {%d, 0}.', [ 0 ] ) ) );
    	} elseif( $this->m_intProCriteria < $this->m_intBaseCriteria ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pro_criteria', __( 'Pro Badge should always be greater than or equal to Base Badge.' ) ) );
        }

        return $boolIsValid;
    }

    public function valIsPenalty() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDisplayToAll() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDisplayToCustomer() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valBaseCriteria();
            	$boolIsValid &= $this->valProCriteria();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	break;
        }

        return $boolIsValid;
    }

}
?>