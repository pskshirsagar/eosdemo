<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultReturnTypeOptions
 * Do not add any new functions to this class.
 */

class CDefaultReturnTypeOptions extends CBaseDefaultReturnTypeOptions {

	public static function fetchDefaultReturnTypeOptions( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CDefaultReturnTypeOption', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>