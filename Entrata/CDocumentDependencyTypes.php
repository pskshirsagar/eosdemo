<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentDependencyTypes
 * Do not add any new functions to this class.
 */

class CDocumentDependencyTypes extends CBaseDocumentDependencyTypes {

    public static function fetchDocumentDependencyTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CDocumentDependencyType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchDocumentDependencyType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CDocumentDependencyType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchPublishedDocumentDependencyTypes( $objDatabase ) {
    	$strSql = ' SELECT * FROM document_dependency_types WHERE is_published = 1 ORDER BY order_num';
    	return self::fetchDocumentDependencyTypes( $strSql, $objDatabase );
	}

}

?>