<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlAccountTypes
 * Do not add any new functions to this class.
 */

class CGlAccountTypes extends CBaseGlAccountTypes {

	public static function fetchGlAccountTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CGlAccountType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllGlAccountTypes( $objClientDatabase, $boolIsPublished = false, $strOrderBy = 'id' ) {

		$strSql = 'SELECT * FROM gl_account_types ORDER BY order_num';

		if( false != $boolIsPublished ) {
			$strSql = 'SELECT * FROM gl_account_types WHERE is_published = 1 ORDER BY ' . $strOrderBy;
		}

		return self::fetchGlAccountTypes( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountTypesByIds( $arrintGlAccountTypeIds, $objClientDatabase ) {

		if( false == valArr( $arrintGlAccountTypeIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						gl_account_types
					WHERE
						id IN ( ' . implode( ',',  $arrintGlAccountTypeIds ) . ')';

		return self::fetchGlAccountTypes( $strSql, $objClientDatabase );
	}

	public static function fetchTemplateKeyValuePair() {

		return [ CGlAccountType::ASSETS => 'Assets', CGlAccountType::LIABILITIES => 'Liabilities', CGlAccountType::EQUITY => 'Equity', CGlAccountType::INCOME => 'Income', CGlAccountType::EXPENSES => 'Expenses' ];
	}

}
?>