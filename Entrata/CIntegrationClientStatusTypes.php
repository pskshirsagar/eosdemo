<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationClientStatusTypes
 * Do not add any new functions to this class.
 */

class CIntegrationClientStatusTypes extends CBaseIntegrationClientStatusTypes {

	public static function fetchIntegrationClientStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CIntegrationClientStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchIntegrationClientStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CIntegrationClientStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllIntegrationClientStatusTypes( $objDatabase ) {
		return self::fetchIntegrationClientStatusTypes( 'SELECT * FROM integration_client_status_types', $objDatabase );
	}
}
?>