<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationClientKeyValues
 * Do not add any new functions to this class.
 */

class CIntegrationClientKeyValues extends CBaseIntegrationClientKeyValues {

	public static function fetchIntegrationClientKeyValuesByKey( $strKey, $objDatabase ) {
		return self::fetchIntegrationClientKeyValues( sprintf( 'SELECT * FROM integration_client_key_values WHERE key = \'%s\'', $strKey ), $objDatabase );
	}

	public static function fetchIntegrationClientKeyValuesByValue( $strValue, $objDatabase ) {
		return self::fetchIntegrationClientKeyValues( sprintf( 'SELECT * FROM integration_client_key_values WHERE value = \'%s\'', $strValue ), $objDatabase );
	}

	public static function fetchIntegrationClientKeyValueByPropertyIdByKeyByCid( $intPropertyId, $strKey, $intCid, $objDatabase ) {
		return self::fetchIntegrationClientKeyValue( sprintf( 'SELECT * FROM %s WHERE key = \'%s\' AND property_id = %d AND cid = %d ORDER BY value LIMIT 1', 'integration_client_key_values', trim( $strKey ), ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationClientKeyValueByPropertyIdByKeyByLimitByCid( $intPropertyId, $strKey, $intLimit, $intCid, $objDatabase ) {
		return self::fetchIntegrationClientKeyValue( sprintf( 'SELECT * FROM %s WHERE key = \'%s\' AND property_id = %d AND cid = %d LIMIT %d', 'integration_client_key_values', trim( $strKey ), ( int ) $intPropertyId, ( int ) $intCid, ( int ) $intLimit ), $objDatabase );
	}

	public static function fetchIntegrationClientKeyValueByPropertyIdByKeyWithNullUnitTypeIdByCid( $intPropertyId, $strKey, $intCid, $objDatabase ) {
		return self::fetchIntegrationClientKeyValue( sprintf( 'SELECT * FROM %s WHERE key = \'%s\' AND property_id = %d AND cid = %d AND unit_type_id IS NULL LIMIT 1', 'integration_client_key_values', trim( $strKey ), ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationClientKeyValueByPropertyFloorplanIdByKeyByCid( $intPropertyFloorplanId, $strKey, $intCid, $objDatabase ) {
		return self::fetchIntegrationClientKeyValue( sprintf( 'SELECT * FROM %s WHERE key = \'%s\' AND property_floorplan_id = %d AND cid = %d LIMIT 1', 'integration_client_key_values', trim( $strKey ), ( int ) $intPropertyFloorplanId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationClientKeyValueByUnitTypeIdByKeyByCid( $intUnitTypeId, $strKey, $intCid, $objDatabase ) {
		return self::fetchIntegrationClientKeyValue( sprintf( 'SELECT * FROM %s WHERE key = \'%s\' AND unit_type_id = %d AND cid =%d ORDER BY value LIMIT 1', 'integration_client_key_values', trim( $strKey ), ( int ) $intUnitTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationClientKeyValuesByPropertyIdByValueByCid( $intPropertyId, $strValue, $intCid, $objDatabase ) {
		return self::fetchIntegrationClientKeyValue( sprintf( 'SELECT * FROM %s WHERE lower(value) = lower( \'%s\' ) AND property_id = %d AND cid = %d LIMIT 1', 'integration_client_key_values', trim( $strValue ), ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationClientKeyValuesByPropertyIdByValueByKeyByCid( $intPropertyId, $strValue, $strKey, $intCid, $objDatabase ) {
		$strSql = 'SELECT *
    				FROM
    					integration_client_key_values
		    		WHERE
		    		    key = \'' . $strKey . '\'
		    			AND lower(value) = lower( \'' . $strValue . '\' )
		    			AND property_id = ' . ( int ) $intPropertyId . '
		    			AND cid = ' . ( int ) $intCid;

		return self::fetchIntegrationClientKeyValue( $strSql,  $objDatabase );
	}

	public static function fetchIntegrationClientKeyValuesByPropertyIdByKeyByCid( $intPropertyId, $strKey, $intCid, $objDatabase ) {
		return self::fetchIntegrationClientKeyValues( sprintf( 'SELECT * FROM %s WHERE key = \'%s\' AND property_id = %d AND cid = %d ', 'integration_client_key_values', trim( $strKey ), ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationClientKeyValueByKeyByIntegrationClientIdByCid( $strKey, $intIntegrationClientId, $intCid, $objDatabase ) {
		return self::fetchIntegrationClientKeyValue( sprintf( 'SELECT * FROM %s WHERE  key = \'%s\' AND integration_client_id = %d AND cid = %d LIMIT 1', 'integration_client_key_values', trim( $strKey ), ( int ) $intIntegrationClientId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationClientKeyValuesByPropertyIdByKeyKeyedByValueByCid( $intPropertyId, $strKey, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM integration_client_key_values WHERE key = \'' . $strKey . '\' AND property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid;
		$arrobjIntegrationClientKeyValues = self::fetchIntegrationClientKeyValues( $strSql, $objDatabase );

		$arrobjRekeyedIntegrationClientKeyValues = [];

		if( true == valArr( $arrobjIntegrationClientKeyValues ) ) {
			foreach( $arrobjIntegrationClientKeyValues as $objIntegrationClientKeyValue ) {
				$arrobjRekeyedIntegrationClientKeyValues[$objIntegrationClientKeyValue->getValue()] = $objIntegrationClientKeyValue;
			}
		}

		return $arrobjRekeyedIntegrationClientKeyValues;
	}

	public static function fetchIntegrationClientKeyValueByKeyByCid( $strKey, $intCid, $objDatabase ) {
		return self::fetchIntegrationClientKeyValue( sprintf( 'SELECT * FROM %s WHERE key = \'%s\' AND property_id IS NULL AND cid = %d LIMIT 1', 'integration_client_key_values', trim( $strKey ), ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAllIntegrationClientKeyValuesByKeyByCid( $strKey, $intCid, $objDatabase ) {
		return self::fetchIntegrationClientKeyValues( sprintf( 'SELECT * FROM %s WHERE key = \'%s\' AND cid = %d', 'integration_client_key_values', trim( $strKey ), ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBlueMoonLeaseIntegrationClientKeyValuesByPropertyIdByCid( $intPropertyid, $intCid, $objDatabase ) {

		$strSql = 'SELECT *
					FROM
						integration_client_key_values
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyid . '
						AND key ILIKE \'BLUE_MOON_LEASE_%\'';

		return self::fetchIntegrationClientKeyValues( $strSql, $objDatabase );
	}

	public static function fetchRentAndDepositIntegrationClientKeyValuesByIntegrationDatabasesIdsByCidKeyedByIntegrationDatabaseId( $arrintIntegrationDatabasesIds, $intCid, $objDatabase ) {

		$strRentArcodeId 	= 'RENT_AR_CODE_ID';
		$strDepositArcodeId = 'DEPOSIT_AR_CODE_ID';

		$strSql = 'SELECT
				    *
					FROM
					    integration_client_key_values
					WHERE
					    integration_database_id in ( ' . implode( ',', $arrintIntegrationDatabasesIds ) . ' )
					    AND ( KEY = \'' . $strRentArcodeId . '\' OR KEY = \'' . $strDepositArcodeId . '\' )
					    AND cid = ' . ( int ) $intCid;

		$arrobjIntegrationClientKeyValues = self::fetchIntegrationClientKeyValues( $strSql, $objDatabase );

		$arrobjIntegrationClientKeyValuesKeyedByIntegrationDatabaseIdByKey = [];

		if( true == valArr( $arrobjIntegrationClientKeyValues ) ) {
			foreach( $arrobjIntegrationClientKeyValues as $objIntegrationClientKeyValue ) {

				$arrobjIntegrationClientKeyValuesKeyedByIntegrationDatabaseIdByKey[$objIntegrationClientKeyValue->getIntegrationDatabaseId()][$objIntegrationClientKeyValue->getKey()] = $objIntegrationClientKeyValue;

			}
		}
		return $arrobjIntegrationClientKeyValuesKeyedByIntegrationDatabaseIdByKey;

	}

	public static function fetchIntegrationClientKeyValuesByIntegrationDatabasesIdsByCidKeyedByIntegrationDatabaseId( $arrintIntegrationDatabasesIds, $intCid, $objDatabase ) {

		$arrstrArCodeIds = [ 'RENT_AR_CODE_ID', 'DEPOSIT_AR_CODE_ID', 'PET_RENT_AR_CODE_ID', 'PET_DEPOSIT_AR_CODE_ID', 'PET_FEE_AR_CODE_ID' ];

		$strSql = 'SELECT
				    *
					FROM
					    integration_client_key_values
					WHERE
					    integration_database_id in ( ' . implode( ',', $arrintIntegrationDatabasesIds ) . ' )
					    AND KEY IN ( \'' . implode( "','", $arrstrArCodeIds ) . '\' )
					    AND cid = ' . ( int ) $intCid;

		$arrobjIntegrationClientKeyValues = self::fetchIntegrationClientKeyValues( $strSql, $objDatabase );

		$arrobjIntegrationClientKeyValuesKeyedByIntegrationDatabaseIdByKey = [];

		if( true == valArr( $arrobjIntegrationClientKeyValues ) ) {
			foreach( $arrobjIntegrationClientKeyValues as $objIntegrationClientKeyValue ) {

				$arrobjIntegrationClientKeyValuesKeyedByIntegrationDatabaseIdByKey[$objIntegrationClientKeyValue->getIntegrationDatabaseId()][$objIntegrationClientKeyValue->getKey()] = $objIntegrationClientKeyValue;

			}
		}
		return $arrobjIntegrationClientKeyValuesKeyedByIntegrationDatabaseIdByKey;

	}

	public static function fetchIntegrationClientKeyValueByKeyByIntegrationDatabaseIdByCid( $strKey, $intIntegrationDatabaseId, $intCid, $objDatabase ) {
		return self::fetchIntegrationClientKeyValue( sprintf( 'SELECT * FROM %s WHERE key = \'%s\' AND integration_database_id = %d AND cid = %d LIMIT 1', 'integration_client_key_values', trim( $strKey ), ( int ) $intIntegrationDatabaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationClientKeyValuesByKeysByIntegrationDatabaseIdsByCid( $arrstrKeys, $arrintIntegrationDatabaseIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) || false == valArr( $arrintIntegrationDatabaseIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						integration_client_key_values
					WHERE
						cid = ' . ( int ) $intCid . '
						AND integration_database_id IN( ' . implode( ',', $arrintIntegrationDatabaseIds ) . ' )
						AND key IN( \'' . implode( '\',\'', $arrstrKeys ) . '\' )';

		return parent::fetchIntegrationClientKeyValues( $strSql, $objDatabase );
	}

	public function fetchIntegrationClientKeyValueByKeyByIntegrationDatabaseIdByCidForTestClients( $strKey, $intIntegrationDatabaseId, $intCid, $objDatabase ) {
		$strSql = 'SELECT ickv.*
    				FROM
    					integration_client_key_values ickv
		    			JOIN clients c ON ( ickv.cid = c.id )
		    		WHERE
		    		    ickv.key = \'' . $strKey . '\'
		    			AND ickv.cid = ' . ( int ) $intCid . '
		    			AND integration_database_id =' . ( int ) $intIntegrationDatabaseId . '
		    			AND c.company_status_type_id IN ( ' . CCompanyStatusType::TEST_DATABASE . ' )';

		return self::fetchIntegrationClientKeyValue( $strSql,  $objDatabase );
	}

	public static function fetchIntegrationClientKeyValuesByKeysByPropertyIdByCid( $arrstrKeys, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrstrKeys ) ) return [];

		$strSql = 'SELECT
						*
					FROM
						integration_client_key_values
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND key IN( \'' . implode( '\',\'', $arrstrKeys ) . '\' )';

		return parent::fetchIntegrationClientKeyValues( $strSql, $objDatabase );
	}

	public static function fetchIntegrationClientKeyValuesByIntegrationDatabaseIdsByKeysByCid( $arrintIntegrationDatabaseIds, $arrstrKeys, $intCid, $objDatabase ) {
		if( !valArr( $arrintIntegrationDatabaseIds ) || !valArr( $arrstrKeys ) ) return [];

		$strSql = 'SELECT
						*
					FROM
						integration_client_key_values
					WHERE
						cid = ' . ( int ) $intCid . '
						AND integration_database_id IN(' . implode( ',', $arrintIntegrationDatabaseIds ) . ')
						AND key IN( \'' . implode( '\',\'', $arrstrKeys ) . '\' )';

		return parent::fetchIntegrationClientKeyValues( $strSql, $objDatabase );
	}

}
?>