<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEmployeeContactAvailabilities
 * Do not add any new functions to this class.
 */

class CCompanyEmployeeContactAvailabilities extends CBaseCompanyEmployeeContactAvailabilities {

	public static function fetchCompanyEmployeeContactAvailabilitiesByCidByPropertyId( $intCid, $intPropertyId, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT 
						ceca.*,
						util_get_translated( \'name\', cecat.name, cecat.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS availability_type_name
					FROM 
						company_employee_contact_availabilities AS ceca
						JOIN company_employee_contact_availability_types AS cecat ON ( ceca.company_employee_contact_availability_type_id = cecat.id )
					WHERE 
						ceca.cid = ' . ( int ) $intCid . '
						AND ceca.property_id = ' . ( int ) $intPropertyId . '
					ORDER BY 
						ceca.id';

		return parent::fetchCompanyEmployeeContactAvailabilities( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeContactAvailabilitiesByCidByPropertyIdByCompanyEmployeeContactIds( $intCid, $intPropertyId, $strCompanyEmployeeContactIds, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valStr( $strCompanyEmployeeContactIds ) ) return NULL;

		$strSql = 'SELECT
						ceca.*,
						util_get_translated( \'name\', cecat.name, cecat.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS availability_type_name
					FROM
						company_employee_contact_availabilities AS ceca
						JOIN company_employee_contact_availability_types AS cecat ON ( ceca.company_employee_contact_availability_type_id = cecat.id ) 
					WHERE
						ceca.cid = ' . ( int ) $intCid . '
						AND ceca.property_id = ' . ( int ) $intPropertyId . '
						AND ceca.company_employee_contact_id IN( ' . $strCompanyEmployeeContactIds . ' )
					ORDER BY
						ceca.id';

		return self::fetchCompanyEmployeeContactAvailabilities( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeContactAvailabilitiesByCidByPropertyIdByCompanyEmployeeContactId( $intCid, $intPropertyId, $intCompanyEmployeeContactId, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valId( $intCompanyEmployeeContactId ) ) return NULL;

		$strSql = 'SELECT
						ceca.*
					FROM
						company_employee_contact_availabilities AS ceca
					WHERE
						ceca.cid = ' . ( int ) $intCid . '
						AND ceca.property_id = ' . ( int ) $intPropertyId . '
						AND ceca.company_employee_contact_id = ' . ( int ) $intCompanyEmployeeContactId;

		return self::fetchCompanyEmployeeContactAvailabilities( $strSql, $objDatabase );
	}

}
?>