<?php
use Psi\Eos\Entrata\CPropertyMaintenanceProblems;

class CMaintenanceProblem extends CBaseMaintenanceProblem {

	const SYSTEM_CODE_MAKE_READY = 'MR';
	const SYSTEM_CODE_INSPECTION = 'IN';
	const INCLUDE_IN_MAINTENANCE_REQUESTS = 'Work Orders';
	const INCLUDE_IN_INSPECTIONS = 'Inspections';
	const INCLUDE_IN_BOTH = 'Both';

	protected $m_strCategoryName;
	protected $m_intMaintenanceLocationId;
	protected $m_intPsProductId;
	protected $m_strPropertyRemotePrimaryKey;
	protected $m_intIntegrationClientStatusTypeId;

	public static $c_arrintIntegratedClientTypes = [
		CIntegrationClientType::AMSI,
		CIntegrationClientType::YARDI,
		CIntegrationClientType::MRI,
		CIntegrationClientType::TIMBERLINE,
		CIntegrationClientType::REAL_PAGE,
		CIntegrationClientType::JENARK,
		CIntegrationClientType::REAL_PAGE_API,
		CIntegrationClientType::YARDI_RPORTAL,
	];

	public function applyRequestForm( $arrmixRequestForm, $arrmixFormFields = NULL ) {

		parent::applyRequestForm( $arrmixRequestForm, $arrmixFormFields );

		if( true == valArr( $arrmixFormFields ) ) {
		    $arrmixRequestForm = mergeIntersectArray( $arrmixFormFields, $arrmixRequestForm );
		}

		$this->setValues( $arrmixRequestForm, false );

		return;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, true, $boolDirectSet );
		if( true == isset( $arrmixValues['maintenance_problem_id'] ) ) $this->setMaintenanceProblemId( $arrmixValues['maintenance_problem_id'] );
		if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['category_name'] ) ) $this->setCategoryName( $arrmixValues['category_name'] );
		if( true == isset( $arrmixValues['maintenance_location_id'] ) ) $this->setMaintenanceLocationId( $arrmixValues['maintenance_location_id'] );
		if( true == isset( $arrmixValues['ps_product_id'] ) ) $this->setPsProductId( $arrmixValues['ps_product_id'] );
		if( true == isset( $arrmixValues['property_remote_primary_key'] ) ) $this->setPropertyRemotePrimaryKey( $arrmixValues['property_remote_primary_key'] );
		if( true == isset( $arrmixValues['integration_client_status_type_id'] ) ) $this->setIntegrationClientStatusTypeId( $arrmixValues['integration_client_status_type_id'] );
		return;
	}

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getMaintenanceProblemName( $intMaintenanceProblemId, $intCid, $objDatabase ) {
		$objMaintenanceProblem = \Psi\Eos\Entrata\CMaintenanceProblems::createService()->fetchMaintenanceProblem( sprintf( 'SELECT name, details FROM maintenance_problems WHERE id = %d AND cid = %d', ( int ) $intMaintenanceProblemId, ( int ) $intCid ), $objDatabase );
		$strMaintenanceProblemName = NULL;
		if( true == valObj( $objMaintenanceProblem, CMaintenanceProblem::class ) ) {
			$strMaintenanceProblemName = $objMaintenanceProblem->getName();
		}
		return $strMaintenanceProblemName;
	}

    /**
     * Set Functions
     */

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function setCategoryName( $strCategoryName ) {
		return $this->m_strCategoryName = $strCategoryName;
	}

	public function setMaintenanceLocationId( $intMaintenanceLocationId ) {
		return $this->m_intMaintenanceLocationId = $intMaintenanceLocationId;
	}

	public function setPsProductId( $intPsProductId ) {
		return $this->m_intPsProductId = $intPsProductId;
	}

	public function setPropertyRemotePrimaryKey( $strPropertyRemotePrimaryKey ) {
		return $this->m_strPropertyRemotePrimaryKey = $strPropertyRemotePrimaryKey;
	}

	public function setIntegrationClientStatusTypeId( $intIntegrationClientStatusTypeId ) {
		return $this->m_intIntegrationClientStatusTypeId = $intIntegrationClientStatusTypeId;
	}

    /**
     * Get Functions
     */

	public function getCategoryName() {
		return $this->m_strCategoryName;
	}

	public function getMaintenanceLocationId() {
		return $this->m_intMaintenanceLocationId;
	}

	public function getPropertyRemotePrimaryKey() {
		return $this->m_strPropertyRemotePrimaryKey;
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function getIntegrationClientStatusTypeId() {
		return $this->m_intIntegrationClientStatusTypeId;
	}

    /**
     * Fetch Functions
     */

	public function fetchPropertyMaintenanceProblemByPropertyId( $intPropertyId, $objDatabase ) {
    	return CPropertyMaintenanceProblems::createService()->fetchPropertyMaintenanceProblemByMaintenanceProblemIdByPropertyIdByCid( $this->m_intId, $intPropertyId, $this->getCid(), $objDatabase );
    }

	public function fetchPropertyMaintenanceProblemByPropertyIdByPropertyMaintenanceProblemId( $intPropertyId, $intPropertyMaintenanceProblemId, $objDatabase ) {
		return CPropertyMaintenanceProblems::createService()->fetchPropertyMaintenanceProblemByMaintenanceProblemIdByPropertyIdByPropertyMaintenanceProblemIdByCid( $this->m_intId, $intPropertyId, $intPropertyMaintenanceProblemId, $this->getCid(), $objDatabase );
	}

    /**
     * Validation Functions
     */

    public function valName() {
        $boolIsValid = true;

       	if( true == is_null( $this->getName() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Problem name is required.' ) ) );
		}

         return $boolIsValid;
    }

    public function valCategoryName() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getName() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Category name is required.' ) ) );
    	}

    	return $boolIsValid;
    }

    public function valMaintenanceProblemMaintenanceRequests( $objDatabase, $boolIsProblemAction = NULL ) {
    	$boolIsValid = true;

    	if( false == is_null( $this->getId() ) ) {
    		$arrobjMaintenanceRequests = \Psi\Eos\Entrata\CMaintenanceRequests::createService()->fetchCustomMaintenanceRequestsByMaintenanceProblemIdByCid( $this->getId(), $this->getCid(), $objDatabase );
    		if( true == valArr( $arrobjMaintenanceRequests ) && false == is_null( $arrobjMaintenanceRequests ) ) {
    			$boolIsValid = false;
    			if( true == $boolIsProblemAction ) {
    				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_problem_id', __( 'Maintenance request(s) exists for the maintenance action.' ) ) );
    			} else {
    				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_problem_id', __( 'Maintenance request(s) exists for the maintenance problem.' ) ) );
    			}
    		}
    	}

    	return $boolIsValid;
    }

    public function valMaintenanceProblemCategory( $objDatabase ) {
    	$boolIsValid = true;

    	if( false == is_null( $this->getId() ) ) {
    		$arrobjMaintenanceProblems = \Psi\Eos\Entrata\CMaintenanceProblems::createService()->fetchMaintenanceProblemsByParentMaintenanceProblemIdByCid( $this->getId(), $this->getCid(), $objDatabase );

    		if( true == valArr( $arrobjMaintenanceProblems ) && false == is_null( $arrobjMaintenanceProblems ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_problem_id', __( 'Maintenance Problem(s) associated for the maintenance problem category.' ) ) );
    			return $boolIsValid;
    		}

    		$arrobjSubMaintenanceCategories = CBaseMaintenanceProblems::fetchMaintenanceProblemsByMaintenanceProblemIdByCid( $this->getId(), $this->getCid(), $objDatabase );

    		if( true == valArr( $arrobjSubMaintenanceCategories ) ) {
	    		foreach( $arrobjSubMaintenanceCategories as $objSubMaintenanceCategory ) {
	    			$arrobjMaintenanceProblems = \Psi\Eos\Entrata\CMaintenanceProblems::createService()->fetchMaintenanceProblemsByParentMaintenanceProblemIdByCid( $objSubMaintenanceCategory->getId(), $this->getCid(), $objDatabase );
	    			if( true == valArr( $arrobjMaintenanceProblems ) && false == is_null( $arrobjMaintenanceProblems ) ) {
	    				$boolIsValid &= false;
	    			}
	    		}
    		}

    		if( false == $boolIsValid ) {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_problem_id', __( 'Maintenance Problem(s) associated for the maintenance problem category.' ) ) );
    		}
    	}

    	return $boolIsValid;
    }

    public function valMaintenanceProblemMaintenanceTemplates( $objDatabase ) {
    	$boolIsValid = true;

    	if( false == is_null( $this->getId() ) ) {
    		$arrobjMaintenanceTemplates = \Psi\Eos\Entrata\CMaintenanceTemplates::createService()->fetchMaintenanceTemplatesByCidByMaintenanceProblemId( $this->getCid(), $this->getId(), $objDatabase );
    		if( true == valArr( $arrobjMaintenanceTemplates ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_problem_id', __( 'Maintenance template(s) exists for the maintenance problem.' ) ) );
    		}
    	}

    	return $boolIsValid;
    }

    public function valToggleMaintenanceProblemMaintenanceTemplates( $objDatabase ) {
    	$boolIsValid = true;

    	if( false == is_null( $this->getId() ) ) {
    		$arrobjMaintenanceTemplates = \Psi\Eos\Entrata\CMaintenanceTemplates::createService()->fetchMaintenanceTemplatesByCidByMaintenanceProblemId( $this->getCid(), $this->getId(), $objDatabase );
    		if( true == valArr( $arrobjMaintenanceTemplates ) && 0 == $this->getIsPublished() ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_problem_id', __( 'Maintenance template(s) exists for the maintenance problem.' ) ) );
    		}
    	}

    	return $boolIsValid;
    }

	public function valMaintenanceProblemInspectionChecklists( $objDatabase ) {
		$boolIsValid = true;

		if( false == is_null( $this->getId() ) ) {
			$arrobjInspectionFormLocationProblem = \Psi\Eos\Entrata\CInspectionFormLocationProblems::createService()->fetchInspectionFormsByProblemIdByCid( $this->getId(), $this->getCid(), $objDatabase );

			if( true == valArr( $arrobjInspectionFormLocationProblem ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_problem_id', __( 'Inspection checklist(s) exists for the Maintenance problem.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valMaintenanceProblemAction( $objDatabase ) {
		$boolIsValid = true;

		$strWhere = 'WHERE maintenance_problem_id = ' . $this->getId() . ' AND cid = ' . $this->getCid();

		if( true == valId( \Psi\Eos\Entrata\CInspectionResponses::createService()->fetchInspectionResponseCount( $strWhere, $objDatabase ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_problem_id', __( 'Inspection(s) exists for the maintenance problem.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMaintenanceProblemPropertyAssociations( $objDatabase ) {
		$boolIsValid = true;

		$strWhere = 'WHERE maintenance_problem_id = ' . $this->getId() . ' AND cid = ' . $this->getCid();

		if( true == valId( CPropertyMaintenanceProblems::createService()->fetchPropertyMaintenanceProblemCount( $strWhere, $objDatabase ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_problem_id', __( 'Maintenance problem is associated to the properties.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIncludeInWorkorderOrInspection() {
		$boolIsValid = true;

		if( false == $this->getIncludeInMaintenanceRequests() && false == $this->getIncludeInInspections() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Include in is required.' ) ) );
		}

		return $boolIsValid;
	}

    public function validate( $strAction, $objDatabase = NULL, $boolIsProblemAction = NULL ) {

		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
	            $boolIsValid &= $this->valName();
	            break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName();
		        $boolIsValid &= $this->valToggleMaintenanceProblemMaintenanceTemplates( $objDatabase );
	            $boolIsValid &= $this->valIncludeInWorkorderOrInspection();
            	break;

            case 'maintenance_problem_toggle':
            	$boolIsValid &= $this->valToggleMaintenanceProblemMaintenanceTemplates( $objDatabase );
            	break;

            case 'maintenance_problem_category_insert':
	        case 'maintenance_problem_category_update':
            	$boolIsValid &= $this->valCategoryName();
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valMaintenanceProblemMaintenanceRequests( $objDatabase, $boolIsProblemAction );

            	if( CMaintenanceProblemType::PROBLEM_CATEGORY == $this->m_intMaintenanceProblemTypeId ) {
            		$boolIsValid &= $this->valMaintenanceProblemCategory( $objDatabase );
            	} else {
            		if( false == $boolIsProblemAction ) {
            			$boolIsValid &= $this->valMaintenanceProblemMaintenanceTemplates( $objDatabase );
            			$boolIsValid &= $this->valMaintenanceProblemInspectionChecklists( $objDatabase );
			            $boolIsValid &= $this->valMaintenanceProblemPropertyAssociations( $objDatabase );
            		} else {
			            $boolIsValid &= $this->valMaintenanceProblemAction( $objDatabase );
		            }
            	}
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

      	return $boolIsValid;
    }

    public function checkUnique( $objDatabase ) {

    	$boolIsValid = true;
    	$arrintActionTypes = [ CMaintenanceProblemType::INSPECTION_ACTION_REPAIR, CMaintenanceProblemType::INSPECTION_ACTION_REPLACE, CMaintenanceProblemType::INSPECTION_ACTION_CLEAN ];

    	if( false == in_array( $this->getMaintenanceProblemTypeId(), $arrintActionTypes ) ) {

		    $objMaintenanceProblem = \Psi\Eos\Entrata\CMaintenanceProblems::createService()->fetchMaintenanceProblemByIdCidByMaintenanceProblemIdByNameByMaintenanceProblemTypeId( $this->getId(), $this->m_intCid, $this->m_intMaintenanceProblemId, $this->getName(), $this->getMaintenanceProblemTypeId(), $objDatabase, $this->getIntegrationDatabaseId() );

    		if( true == valObj( $objMaintenanceProblem, 'CMaintenanceProblem' ) ) {
    			$boolIsValid = false;

    			if( CMaintenanceProblemType::PROBLEM_CATEGORY == $this->getMaintenanceProblemTypeId() ) {
    				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Maintenance category {%s, 0} already exists. Please enter a category with a different name.', [ $this->getName() ] ) ) );
    			} else {
    				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Maintenance problem {%s, 0} already exists. Please enter a problem with a different name.', [ $this->getName() ] ) ) );
    			}
    		}
    	}
    	return $boolIsValid;
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
    	if( false == is_null( $this->getName() ) ) {
    		$this->setName( $this->replaceBreakTag( $this->getName() ) );
    	}

    	if( true == $this->checkUnique( $objDatabase ) ) {
    		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    	}

    	return false;
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
    	if( false == is_null( $this->getName() ) ) {
    		$this->setName( $this->replaceBreakTag( $this->getName() ) );
    	}

    	if( true == $this->checkUnique( $objDatabase ) ) {
    		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    	}

    	return false;
    }

    public function replaceBreakTag( $strProblemName ) {
	    $strProblemName = preg_replace( '/<br\W*?>/', ' ', $strProblemName );
	    return $strProblemName;
    }

}
?>