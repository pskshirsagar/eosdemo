<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CHelpResourceRatings
 * Do not add any new functions to this class.
 */

class CHelpResourceRatings extends CBaseHelpResourceRatings {

	public static function fetchHelpResourceRatingByUserIdByHelpResourceIdByCid( $intUserId, $intHelpResourceId, $intCid, $objDatabase ) {

		if( true == empty( $intCid ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						help_resource_ratings
					WHERE
						company_user_id = ' . ( int ) $intUserId . '
						AND help_resource_id = ' . ( int ) $intHelpResourceId . '
						AND cid = ' . ( int ) $intCid . '
					LIMIT 1';

		return parent::fetchHelpResourceRating( $strSql, $objDatabase );
	}

	public static function fetchHelpResourceRatingByUserIdByCidByRatingValueByHelpResourceId( $intUserId, $intCid, $objDatabase, $intRatingValue = NULL, $intHelpResourceId = NULL ) {

		if( true == empty( $intCid ) || false == is_numeric( $intCid ) ) return NULL;

		$strHelpResourceIdCondition = ( $intHelpResourceId != NULL ) ?  ' AND help_resource_id = ' . ( int ) $intHelpResourceId : '';
		$strHelpResourceRatingValueCondition = ( $intRatingValue != NULL ) ? ' AND rating_value = ' . ( int ) $intRatingValue : '';

		$strSql = 'SELECT
						*
					FROM
						help_resource_ratings
					WHERE
						company_user_id = ' . ( int ) $intUserId .
		          $strHelpResourceIdCondition . '
						AND cid = ' . ( int ) $intCid .
		          $strHelpResourceRatingValueCondition . '
						ORDER BY updated_on DESC
					LIMIT 1';

		return parent::fetchHelpResourceRating( $strSql, $objDatabase );
	}

	public static function fetchHelpResourceRatingByUserIdByHelpResourceIdsByCid( $intUserId, $arrintHelpResourceIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintHelpResourceIds ) ) return NULL;
		if( true == empty( $intCid ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						help_resource_id
					FROM
						help_resource_ratings
					WHERE
						company_user_id = ' . ( int ) $intUserId . '
						AND help_resource_id IN( ' . implode( ',',  $arrintHelpResourceIds ) . ' )
						AND cid = ' . ( int ) $intCid . ' ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomHelpResourceRatingsToSyncHelpResourceRatingHoldingsByDate( $strTodayDate, $objDatabase ) {

		$strWhereCondition = '';

		if( true == isset( $strTodayDate ) ) {
			$strWhereCondition = ' AND \'' . $strTodayDate . '\' = date_trunc( \'day\', hrr.created_on )';
		}

		$strSql = 'SELECT
						hrr.cid,
						hrr.help_resource_id,
						hrr.rating_value,
						hrr.feedback,
						hrr.created_by,
						hrr.created_on,
				 		ce.name_first || \' \' || ce.name_last as comment_by_company_user_name,
						ce.title,
						c.company_name
					FROM
						help_resource_ratings hrr
						JOIN company_users cu ON ( hrr.created_by = cu.id AND hrr.cid = cu.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
						JOIN clients c ON ( c.id = hrr.cid )
					WHERE
						cu.username NOT LIKE \'Entrata_%\'' . $strWhereCondition;

		return fetchData( $strSql, $objDatabase );
	}

}
?>