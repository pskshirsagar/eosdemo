<?php

class CWebsiteType extends CBaseWebsiteType {

	const PROSPECT_PORTAL				= 1;
	const RESIDENT_PORTAL				= 2;
	const LOBBY_DISPLAY_A				= 3;
	const LOBBY_DISPLAY_B				= 4;
	const LOBBY_DISPLAY_RESIDENTS		= 5;
	const LOBBY_DISPLAY_PARCELS_ONLY	= 6;
	const LOBBY_DISPLAY_NEW				= 7;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>