<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFees
 * Do not add any new functions to this class.
 */
class CFees extends CBaseFees {

	public static function fetchFeesByIdsByCid( $arrintIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						fees
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN ( ' . implode( ',', $arrintIds ) . ' )';

		return self::fetchFees( $strSql, $objClientDatabase );
	}
}
?>