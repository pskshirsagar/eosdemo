<?php

use Psi\Libraries\Queue\Message\CMessageStatusMessage;

class CFee extends CBaseFee {

	const ALL_FEES                      = 1;
	const PENDING_FEES	                = 2;
	const COMPLETED_FEES	            = 3;
	const PROCESSING_FEES	            = 4;
	const FAILED_FEES	                = 5;
	const PENDING_AND_FAILED_FEES	    = 6;

	protected $m_intPropertyId;
	protected $m_intIncludeSecurityDeposits;
	protected $m_intIncludePaymentsInTransit;

	protected $m_fltManagementFeeAmount;

	protected $m_strName;
	protected $m_strPostMonth;
	protected $m_strPropertyName;

	protected $m_arrintPeriodIds;
	protected $m_arrobjManagementFeeRules;

	protected $m_objFeeCalculation;

	public static $c_arrstrFilterFeeStatuses = [
		self::ALL_FEES	                    => 'All',
		self::PENDING_FEES			        => CMessageStatusMessage::STATUS_PENDING,
		self::PROCESSING_FEES			    => CMessageStatusMessage::STATUS_PROCESSING,
		self::COMPLETED_FEES	            => CMessageStatusMessage::STATUS_COMPLETED,
		self::FAILED_FEES	                => CMessageStatusMessage::STATUS_FAILED
	];

	/**
	 * Get Functions
	 */

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getIncludePaymentsInTransit() {
		return $this->m_intIncludePaymentsInTransit;
	}

	public function getIncludeSecurityDeposits() {
		return $this->m_intIncludeSecurityDeposits;
	}

	public function getManagementFeeAmount() {
		return $this->m_fltManagementFeeAmount;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function getManagementFeeRules() {
		return $this->m_arrobjManagementFeeRules;
	}

	public function getPeriodIds() {
		return $this->m_arrintPeriodIds;
	}

	public function getFeeCalculation() {
		return $this->m_objFeeCalculation;
	}

	public function getName() {
		return $this->m_strName;
	}

	/**
	 * Set Functions
	 */

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setIncludePaymentsInTransit( $intIncludePaymentsInTransit ) {
		$this->m_intIncludePaymentsInTransit = CStrings::strToIntDef( $intIncludePaymentsInTransit, NULL, false );
	}

	public function setIncludeSecurityDeposits( $intIncludeSecurityDeposits ) {
		$this->m_intIncludeSecurityDeposits = CStrings::strToIntDef( $intIncludeSecurityDeposits, NULL, false );
	}

	public function setManagementFeeAmount( $fltManagementFeeAmount ) {
		$this->m_fltManagementFeeAmount = CStrings::strToFloatDef( $fltManagementFeeAmount, NULL, false, 2 );
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setPostMonth( $strPostMonth ) {

		if( true == valStr( $strPostMonth ) ) {

			$arrstrPostMonth = explode( '/', $strPostMonth );

			if( true == valArr( $arrstrPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
				$strPostMonth = $arrstrPostMonth[0] . '/01/' . $arrstrPostMonth[1];
			}
		}

		$this->m_strPostMonth = $strPostMonth;
	}

	public function setManagementFeeRules( $arrobjManagementFeeRules ) {
		$this->m_arrobjManagementFeeRules = $arrobjManagementFeeRules;
	}

	public function setPeriodIds( $arrintPeriodIds ) {
		$this->m_arrintPeriodIds = $arrintPeriodIds;
	}

	public function setFeeCalculation( $objFeeCalculation ) {
		$this->m_objFeeCalculation = $objFeeCalculation;
	}

	private function setJsonFeeCalculation() {

		if( false === valObj( $this->m_objFeeCalculation, 'CAbstractFeeCalculation' ) ) {
			return;
		}

		$this->setDetails( $this->m_objFeeCalculation->toJson() );
	}

	public function setName( $strName ) {
		$this->m_strName = $strName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['include_payments_in_transit'] ) ) $this->setIncludePaymentsInTransit( $arrmixValues['include_payments_in_transit'] );
		if( true == isset( $arrmixValues['include_security_deposits'] ) ) $this->setIncludeSecurityDeposits( $arrmixValues['include_security_deposits'] );
		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['post_month'] ) ) $this->setPostMonth( $arrmixValues['post_month'] );
		$this->setFeeCalculation( $this->loadFeeCalculation() );
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFeeTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valManagementFeeAmount() {
		$boolIsValid = true;

		if( 0 >= $this->getManagementFeeAmount() ) {

			$strValidationMsg = 'Management fee should be greater than zero.';
			if( CFeeType::OWNER_DISTRIBUTIONS == $this->getFeeTypeId() )
				$strValidationMsg = 'Distribution amount should be greater than zero.';

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'management_fee_amount', $strValidationMsg ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valPostMonth() {

		$boolIsValid = true;

		if( false == valStr( $this->getPostMonth() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Post month is required.' ) );
		} elseif( false == preg_match( '#^(((0?[1-9])|(1[012]))(/0?1)/([12]\d)?\d\d)$#', $this->getPostMonth() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Post month of format mm/yyyy is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPeriodIds() {

		$boolIsValid = true;

		if( false == valArr( $this->getPeriodIds() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'period_ids', 'Please select property(s).' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valManagementFeeAmount();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				$boolIsValid &= $this->valPostMonth();
				$boolIsValid &= $this->valPeriodIds();
				break;

			default:
				$boolIsValid = true;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function addManagementFeeRules( $objManagementFeeRule ) {
		$this->m_arrobjManagementFeeRules[$objManagementFeeRule->getId()] = $objManagementFeeRule;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->setJsonFeeCalculation();

		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function delete( $strMsg, $intCompanyUserId, $objDatabase = NULL, $boolIsAllowDelete = false ) {
		$boolIsValid = true;

		if( true == $boolIsAllowDelete ) {
			return parent::delete( $intCompanyUserId, $objDatabase );
		}

		// Soft delete from management fees.

		$this->setDeletedOn( 'NOW()' );
		$this->setDeletedBy( $intCompanyUserId );

		if( false == $this->update( $intCompanyUserId, $objDatabase ) ) {
			$objDatabase->rollback();
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Invoice could not be ' . $strMsg . '.' ) );
			return false;
		}

		return $boolIsValid;
	}

	public function calculate() {
		if( false == valObj( $this->getFeeCalculation(), 'CAbstractFeeCalculation' ) ) {
			return;
		}

		$this->setManagementFeeAmount( $this->getFeeCalculation()->calculateTotalFeeAmount() );
	}

    private function loadFeeCalculation() {
	    $objFeeCalculation = ( new CFeeCalculationFactory )->createFeeCalculation( $this->getFeeCalculationTypeId() );

	    $objFeeCalculation->parse( ( array ) $this->getDetails() );
        return $objFeeCalculation;
    }

}
?>