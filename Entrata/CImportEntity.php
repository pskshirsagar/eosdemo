<?php

class CImportEntity extends CBaseImportEntity {

	const GL_GROUPS								= 1;
	const GL_ACCOUNTS							= 2;
	const CHARGE_CODES							= 3;
	const FLOOR_PLANS							= 4;
	const STANDARD_RESIDENTS_AND_APPLICANTS 	= 5;
	const CONSOLIDATED_RESIDENTS_AND_APPLICANTS = 6;
	const OCCUPANTS								= 7;
	const TRANSACTIONS							= 8;
	const RECURRING_CHARGES 					= 9;
	const LEASE_INTERVALS 						= 10;
	const DEMOGRAPHICS							= 11;
	const VENDORS								= 12;
	const AMENITIES 							= 13;
	const ADD_ONS								= 14;
	const MAINTENANCE_REQUESTS 					= 15;
	const INVOICES								= 16;
	const DEPOSITS								= 17;
	const GUEST_CARD							= 18;
	const CUSTOMER_DEMOGRAPHICS					= 19;
	const VEHICLES								= 20;
	const EMPLOYERS								= 21;
	const PETS									= 22;
	const OTHER									= 23;
	const ADDRESSES								= 24;
	const RESIDENT_INSURANCE					= 25;
	const EMERGENCY_CONTACTS					= 26;
	const TENANTS_AND_SUITES					= 39;
	const COMMERCIAL_BUILDINGS					= 28;
	const COMMERCIAL_SUITES						= 27;
	const COMMERCIAL_ADD_ONS					= 40;
	const COMMERCIAL_RECURRING_CHARGES			= 29;
	const COMMERCIAL_ACTIVITY_LOGS				= 41;
	const COMMERCIAL_ADDRESSES					= 31;
	const COMMERCIAL_CONTACTS					= 32;
	const COMMERCIAL_RENEWAL_OPTIONS			= 33;
	const RENT_STEPS							= 30;
	const COMMERCIAL_GUARANTOR					= 34;
	const COMMERCIAL_MAINTENANCE_REQUESTS		= 35;
	const COMMERCIAL_TRANSACTIONS				= 37;
	const COMMERCIAL_RESIDENT_INSURANCE			= 38;
	const COMMERCIAL_OTHER_CLAUSE				= 36;

	public static $c_arrstrCSVImportControllers = [
		self::GL_GROUPS								=> 'CImportDataGlGroupsController',
		self::GL_ACCOUNTS							=> 'CImportDataGlAccountsController',
		self::CHARGE_CODES							=> 'CImportDataArCodesController',
		self::FLOOR_PLANS							=> 'CImportDataFloorPlansController',
		self::STANDARD_RESIDENTS_AND_APPLICANTS		=> 'CImportDataResidentsDataController',
		self::CONSOLIDATED_RESIDENTS_AND_APPLICANTS	=> 'CImportDataResidentsTransportController',
		self::OCCUPANTS								=> 'CImportDataOccupantsController',
		self::TRANSACTIONS							=> 'CImportDataTransactionsController',
		self::RECURRING_CHARGES						=> 'CImportDataRecurringChargesController',
		self::LEASE_INTERVALS						=> 'CImportDataLeaseIntervalsController',
		self::VENDORS								=> 'CImportDataVendorsController',
		self::AMENITIES								=> 'CImportDataAmenitiesController',
		self::ADD_ONS								=> 'CImportDataAddOnsController',
		self::MAINTENANCE_REQUESTS					=> 'CImportDataMaintenanceRequestsController',
		self::INVOICES								=> 'CImportDataInvoicesController',
		self::DEPOSITS								=> 'CImportDataTransactionsController',
		self::GUEST_CARD							=> 'CImportDataGuestCardsController',
		self::CUSTOMER_DEMOGRAPHICS					=> 'CImportDataCustomerDemographicsController',
		self::VEHICLES	    						=> 'CImportDataVehiclesController',
		self::EMPLOYERS		    					=> 'CImportDataEmployersController',
		self::PETS							        => 'CImportDataPetsController'
	];

	public static $c_arrintAdvancedCSV = [
		self::CONSOLIDATED_RESIDENTS_AND_APPLICANTS,
		self::OCCUPANTS,
		self::TRANSACTIONS,
		self::DEPOSITS,
		self::RECURRING_CHARGES,
		self::CUSTOMER_DEMOGRAPHICS,
		self::LEASE_INTERVALS
	];

	public static $c_arrmixDemographicCSV = [
		self::EMPLOYERS						=> 'Employers',
		self::PETS							=> 'Pets',
		self::VEHICLES						=> 'Vehicles'
	];

	public static $c_arrintExcludeEntityIds= [
		CImportEntity::COMMERCIAL_BUILDINGS,
		CImportEntity::COMMERCIAL_SUITES,
		CImportEntity::COMMERCIAL_ADD_ONS,
		CImportEntity::COMMERCIAL_RECURRING_CHARGES,
		CImportEntity::COMMERCIAL_ACTIVITY_LOGS,
		CImportEntity::COMMERCIAL_ADDRESSES,
		CImportEntity::COMMERCIAL_CONTACTS,
		CImportEntity::COMMERCIAL_RENEWAL_OPTIONS,
		CImportEntity::RENT_STEPS,
		CImportEntity::COMMERCIAL_GUARANTOR,
		CImportEntity::COMMERCIAL_MAINTENANCE_REQUESTS,
		CImportEntity::COMMERCIAL_TRANSACTIONS,
		CImportEntity::COMMERCIAL_RESIDENT_INSURANCE,
		CImportEntity::COMMERCIAL_OTHER_CLAUSE,
		CImportEntity::TENANTS_AND_SUITES,
		CImportEntity::INVOICES,
		CImportEntity::ADDRESSES,
		CImportEntity::RESIDENT_INSURANCE,
		CImportEntity::EMERGENCY_CONTACTS
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>