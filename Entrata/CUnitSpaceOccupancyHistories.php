<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitSpaceOccupancyHistories
 * Do not add any new functions to this class.
 */

class CUnitSpaceOccupancyHistories extends CBaseUnitSpaceOccupancyHistories {

	public static function fetchConflictingUnitSpaceOccupancyHistoryCountByLeaseIdByLeaseActionIdByCidByPropertyUnitIdByLeaseActionDate( $intLeaseId, $intActionId, $intCid,$intPropertyUnitId, $strEventDate, $objDatabase ) {
		$strWhereSql = 'WHERE
							lease_id = ' . ( int ) $intLeaseId . '
							AND lease_action_id = ' . ( int ) $intActionId . '
							AND cid = ' . ( int ) $intCid . '
							AND property_unit_id = ' . ( int ) $intPropertyUnitId . '
							AND lease_action_date = \'' . $strEventDate . '\'';
		return self::fetchUnitSpaceOccupancyHistoryCount( $strWhereSql, $objDatabase );
	}

	public static function fetchUnitSpaceOccupancyHistoryByLeaseIdByLeaseActionIdByCid( $intLeaseId, $intActionId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM unit_space_occupancy_histories WHERE lease_id = ' . ( int ) $intLeaseId . ' AND lease_action_id = ' . ( int ) $intActionId . ' AND cid = ' . ( int ) $intCid . ' ORDER BY id DESC LIMIT 1';
		return self::fetchUnitSpaceOccupancyHistory( $strSql, $objDatabase );
	}

	public static function fetchUnitSpaceOccupancyHistoriesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT * FROM unit_space_occupancy_histories WHERE property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND cid = ' . ( int ) $intCid;
		return self::fetchUnitSpaceOccupancyHistories( $strSql, $objDatabase );
	}

}
?>