<?php

class CIntegrationQueueStatusType extends CBaseIntegrationQueueStatusType {

	const READY          = 1;
	const WAITING        = 2;
	const PENDING_REVIEW = 3;
	const COMPLETED      = 4;
	const DELETED        = 5;

	public static $c_arrintIntegrationQueueStatusTypeNameById = [
		self::READY				=> 'Ready',
		self::WAITING			=> 'Waiting',
		self::PENDING_REVIEW	=> 'Pending Review',
		self::COMPLETED			=> 'Completed',
		self::DELETED			=> 'Deleted'
	];

}
?>