<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyRecertificationDetails
 * Do not add any new functions to this class.
 */

class CSubsidyRecertificationDetails extends CBaseSubsidyRecertificationDetails {

	public static function fetchSubsidyRecertificationDetailsBySubsidyTypeIdByPropertyIdByCidByIsCheckBuildingId( $intSubsidyTypeId, $intPropertyId, $intCid, $boolIsCheckBuildingId, $objDatabase ) {
		if( true == is_null( $intPropertyId ) ) {
			return NULL;
		}

		$strSqlConditions = ( true == $boolIsCheckBuildingId ) ? ' AND srd.property_building_id IS NOT NULL' : '';
		$strSql           = 'SELECT
						srd.*,
						pb.building_name
					FROM
						subsidy_recertification_details srd
						LEFT JOIN property_buildings pb ON( srd.property_building_id = pb.id AND srd.cid = pb.cid AND pb.deleted_on IS NULL )
					WHERE
						srd. cid = ' . ( int ) $intCid . '
						AND srd.subsidy_type_id = ' . ( int ) $intSubsidyTypeId . '
						AND srd.property_id = ' . ( int ) $intPropertyId . '
						' . $strSqlConditions . '
						AND srd.deleted_by IS NULL
						AND srd.deleted_on IS NULL';

		return self::fetchSubsidyRecertificationDetails( $strSql, $objDatabase );
	}

}
?>