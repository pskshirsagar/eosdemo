<?php

class CCamPoolTypes extends CBaseCamPoolTypes {

	public static function fetchCamPoolTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCamPoolType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchCamPoolType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCamPoolType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>