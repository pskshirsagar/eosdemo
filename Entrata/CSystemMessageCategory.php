<?php

class CSystemMessageCategory extends CBaseSystemMessageCategory {

	const PAYMENT_NOTIFICATIONS 			= 1;
	const RESIDENT_PORTAL_ACTIVITY 			= 2;
	const GUEST_CARD_NOTIFICATIONS 			= 3;
	const APPLICATION_NOTIFICATIONS 		= 4;
	const RENEWAL_NOTIFICATIONS 			= 5;
	const RESIDENT_NOTIFICATIONS 			= 6;
	const MAINTENANCE_NOTIFICATIONS 		= 7;
	const RESIDENT_INSURANCE_NOTIFICATIONS 	= 8;
	const RESIDENT_UTILITY_NOTIFICATIONS 	= 9;
	const LATE_NOTICES_COLLECTIONS 			= 10;
	const LEASING_CENTER_NOTIFICATIONS 		= 11;
	const PARCEL_ALERT_NOTIFICATIONS 		= 12;

	public static $c_arrstrSystemMessageCategories = [
		self::PAYMENT_NOTIFICATIONS 			=> 'Payment Notifications',
		self::RESIDENT_PORTAL_ACTIVITY 			=> 'Resident Portal Activity',
		self::GUEST_CARD_NOTIFICATIONS 			=> 'Guest Card Notifications',
		self::APPLICATION_NOTIFICATIONS 		=> 'Application Notifications',
		self::RENEWAL_NOTIFICATIONS 			=> 'Renewal Notifications',
		self::RESIDENT_NOTIFICATIONS 			=> 'Resident Notifications',
		self::MAINTENANCE_NOTIFICATIONS 		=> 'Maintenance Notifications',
		self::RESIDENT_INSURANCE_NOTIFICATIONS 	=> 'Resident Insurance Notifications',
		self::RESIDENT_UTILITY_NOTIFICATIONS 	=> 'Resident Utility Notifications',
		self::LATE_NOTICES_COLLECTIONS 			=> 'Late Notices Collections',
		self::LEASING_CENTER_NOTIFICATIONS 		=> 'Leasing Center Notifications',
		self::PARCEL_ALERT_NOTIFICATIONS 		=> 'Parcel Alert Notifications'
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>