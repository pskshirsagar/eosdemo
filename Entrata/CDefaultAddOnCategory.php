<?php

class CDefaultAddOnCategory extends CBaseDefaultAddOnCategory {

	const GARAGE			= 1;
	const APPLIANCES		= 2;
	const FURNITURE			= 3;
	const FULL_FURNISHING	= 4;
	const STORAGE			= 5;
	const PARKING			= 6;
	const MEAL_PLANS		= 7;
	const AMENITY_RENTALS	= 8;
	const FEATURE_UPGRADES	= 12;
	const RESERVATIONS		= 13;
	const OTHER				= 100;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddOnTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsComparable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRequirable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getDefaultAddOnCategoryIdToStrArray() {
		return [
				self::GARAGE			=> __( 'Garage' ),
				self::APPLIANCES		=> __( 'Appliances' ),
				self::FURNITURE			=> __( 'Furniture' ),
				self::FULL_FURNISHING	=> __( 'Full Furnishing' ),
				self::STORAGE			=> __( 'Storage' ),
				self::PARKING			=> __( 'Parking' ),
				self::MEAL_PLANS		=> __( 'Meal Plans' ),
				self::AMENITY_RENTALS	=> __( 'Amenity rentals' ),
				self::FEATURE_UPGRADES	=> __( 'Feature upgrades' ),
				self::RESERVATIONS      => __( 'Reservations' ),
				self::OTHER				=> __( 'other' )
		];
	}

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

}
?>