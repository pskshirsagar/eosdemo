<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyMessages
 * Do not add any new functions to this class.
 */

class CSubsidyMessages extends CBaseSubsidyMessages {

	public static function fetchPaginatedSubsidyMessagesByCid( $intOffset, $intPageSize, $arrmixTracsFilter, $intCid, $objDatabase ) {

		$intLimit					= ( false == is_null( $intPageSize ) ) ? ( int ) $intPageSize : NULL;
		$strTracsWhereConditions	= '';
		$strPropertyGroupJoin		= '';

		if( true == valArr( $arrmixTracsFilter ) ) {

			$arrintPropertyGroupIds	= ( true == isset( $arrmixTracsFilter['property_group_ids'] ) ) ? $arrmixTracsFilter['property_group_ids'] : NULL;
			$strTransactionId		= ( true == isset( $arrmixTracsFilter['transaction_id'] ) ) ? $arrmixTracsFilter['transaction_id'] : NULL;
			$arrintTransactionType	= ( true == isset( $arrmixTracsFilter['transaction_type'] ) ) ? $arrmixTracsFilter['transaction_type'] : NULL;
			// It's important to use strtotime as it will convert invalid date from non-leap year ( 02/29/2014 ) to ( 03/01/2014 )
			$strMinTransmissionDate	= ( true == isset( $arrmixTracsFilter['transmission_date']['min-date'] ) ) ? date( 'Y-m-d', strtotime( $arrmixTracsFilter['transmission_date']['min-date'] ) ) : NULL;
			$strMaxTransmissionDate	= ( true == isset( $arrmixTracsFilter['transmission_date']['max-date'] ) ) ? date( 'Y-m-d', strtotime( $arrmixTracsFilter['transmission_date']['max-date'] ) ) : NULL;
			$strMinResponseDate		= ( true == isset( $arrmixTracsFilter['response_date']['min-date'] ) ) ? date( 'Y-m-d', strtotime( $arrmixTracsFilter['response_date']['min-date'] ) ) : NULL;
			$strMaxResponseDate		= ( true == isset( $arrmixTracsFilter['response_date']['max-date'] ) ) ? date( 'Y-m-d', strtotime( $arrmixTracsFilter['response_date']['max-date'] ) ) : NULL;
			$arrmixFrom				= ( true == isset( $arrmixTracsFilter['from_sender'] ) ) ? $arrmixTracsFilter['from_sender'] : NULL;
			$strSubject				= ( true == isset( $arrmixTracsFilter['subject'] ) ) ? $arrmixTracsFilter['subject'] : NULL;
			$boolIsArchived			= ( true == isset( $arrmixTracsFilter['is_archived'] ) ) ? $arrmixTracsFilter['is_archived'] : false;

			if( true == valArr( $arrintPropertyGroupIds ) ) {
				$strPropertyGroupJoin = 'JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', ( array ) $arrintPropertyGroupIds ) . ' ] ) AS lp ON ( lp.cid = sm.cid AND lp.is_disabled = 0 AND lp.property_id = sm.property_id ) ';
			}

			if( true == valStr( $strTransactionId ) ) {
				$strTracsWhereConditions .= ' AND sm.remote_primary_key = \'' . $strTransactionId . '\'';
			}

			if( true == valArr( $arrintTransactionType ) ) {
				$strTracsWhereConditions .= ' AND smbt.id IN ( ' . implode( ', ', $arrintTransactionType ) . ' )';
			}

			// @TODO get Best way to escape below 2 if loops and acheive the result in one
			if( true == valStr( $strMinTransmissionDate ) || true == valStr( $strMaxTransmissionDate ) ) {

				if( true == valStr( $strMinTransmissionDate ) && false == valStr( $strMaxTransmissionDate ) ) {
					$strTracsWhereConditions .= ' AND ( sm.sent_to_imax_date >= \'' . $strMinTransmissionDate . '\' )';
					// \'' . date( 'Y-m-d', strtotime( $arrstrFilterValues['date']['min-date'] ) ) . ' 00:00:00\'';
				}

				if( true == valStr( $strMaxTransmissionDate ) && false == valStr( $strMinTransmissionDate ) ) {
					$strTracsWhereConditions .= ' AND ( sm.sent_to_imax_date <= \'' . $strMaxTransmissionDate . '\' )';
				}

				if( true == valStr( $strMinTransmissionDate ) && true == valStr( $strMaxTransmissionDate ) ) {
					$strTracsWhereConditions .= ' AND ( DATE_TRUNC( \'day\', sm.sent_to_imax_date ) BETWEEN \'' . $strMinTransmissionDate . '\' AND \'' . $strMaxTransmissionDate . '\' )';
				}

				$strTracsWhereConditions .= ' AND sm.subsidy_message_type_id IN ( ' . implode( ', ', CSubsidyMessageType::$c_arrintTransmissionMessageTypes ) . ' )';
			}

			if( true == valStr( $strMinResponseDate ) || true == valStr( $strMaxResponseDate ) ) {

				if( true == valStr( $strMinResponseDate ) && false == valStr( $strMaxResponseDate ) ) {
					$strTracsWhereConditions .= ' AND ( sm.sent_to_imax_date >= \'' . $strMinResponseDate . '\' )';
				}

				if( true == valStr( $strMaxResponseDate ) && false == valStr( $strMinResponseDate ) ) {
					$strTracsWhereConditions .= ' AND ( sm.sent_to_imax_date <= \'' . $strMaxResponseDate . '\' )';
				}

				if( true == valStr( $strMinResponseDate ) && true == valStr( $strMaxResponseDate ) ) {
					$strTracsWhereConditions .= ' AND ( DATE_TRUNC( \'day\', sm.sent_to_imax_date ) BETWEEN \'' . $strMinResponseDate . '\' AND \'' . $strMaxResponseDate . '\' )';
				}

				$strTracsWhereConditions .= ' AND sm.subsidy_message_type_id IN ( ' . implode( ', ', CSubsidyMessageType::$c_arrintResponseMessageTypes ) . ' )';
			}

			$strTracsWhereConditions .= ' AND sm.subsidy_message_type_id != ' . CSubsidyMessageType::IMAX_BROADCAST;

			if( true == valArr( $arrmixFrom ) ) {
				$strTracsWhereConditions .= ' AND sm.sender IN ( \'' . implode( '\', \'', $arrmixFrom ) . '\' )';
			}

			if( true == valStr( $strSubject ) ) {
				$strTracsWhereConditions .= ' AND sm.subject LIKE \'%' . $strSubject . '%\'';
			}

			$strTracsWhereConditions .= ' AND sm.is_archived = ' . $boolIsArchived . '::boolean';

		}

		$strSql = 'SELECT

					sm.id,
					sm.subsidy_message_type_id,
					sm.subsidy_message_status_type_id,
					sm.subsidy_message_sub_type_id,
					sm.remote_primary_key AS transaction_id,
					sm.sender,
					sm.recipients,
					sm.subject,
					sm.message,
					sm.sent_to_imax_date,
					sm.is_archived,
					sm.subsidy_message_id,
					p.property_name,
					smbt.name AS transaction_type,
					COUNT( smd.id ) AS items,
					COUNT( smd1.id ) AS result_items,

					CASE
						WHEN
							sm1.subsidy_message_type_id IN ( ' . implode( ', ', CSubsidyMessageType::$c_arrintResponseMessageTypes ) . ' )
						THEN
							sm1.sent_to_imax_date || \':\' || sm1.remote_primary_key
						ELSE
							smst.name
					END as response_values

				FROM

					subsidy_messages sm
					' . $strPropertyGroupJoin . '
					JOIN properties p ON ( p.id = sm.property_id AND p.cid = sm.cid)
					JOIN subsidy_message_sub_types smbt ON ( smbt.id = sm.subsidy_message_sub_type_id )
					JOIN subsidy_message_status_types smst ON ( smst.id = sm.subsidy_message_status_type_id )
					LEFT JOIN subsidy_messages sm1 ON ( sm1.cid = sm.cid AND sm1.subsidy_message_id = sm.id AND sm1.subsidy_message_type_id IN ( ' . implode( ', ', CSubsidyMessageType::$c_arrintResponseMessageTypes ) . ' ) )
					LEFT JOIN subsidy_message_details smd ON ( smd.cid = sm.cid AND smd.submission_subsidy_message_id = sm.id )
					LEFT JOIN subsidy_message_details smd1 ON ( smd1.cid = sm.cid AND smd1.result_subsidy_message_id = sm.id )

				WHERE
					sm.cid = ' . ( int ) $intCid . $strTracsWhereConditions . '

				GROUP BY
					sm.id,
					sm.subsidy_message_type_id,
					sm.subsidy_message_status_type_id,
					sm.subsidy_message_sub_type_id,
					sm.remote_primary_key ,
					sm.sender,
					sm.recipients,
					sm.subject,
					sm.message,
					sm.sent_to_imax_date,
					sm.is_archived,
					sm.subsidy_message_id,
					p.property_name,
					smbt.name,
					smst.name,
					sm1.remote_primary_key,
					sm1.sent_to_imax_date,
					sm1.subsidy_message_type_id

				ORDER BY sm.sent_to_imax_date DESC ';

		if( false == is_null( $intOffset ) && false == is_null( $intLimit ) ) {
			$strSql .= ' LIMIT ' . ( int ) $intLimit . ' OFFSET ' . ( int ) $intOffset . ' ';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSubsidyMessageSendersByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						sm.sender
					FROM
						subsidy_messages sm
					WHERE
						sm.cid = ' . ( int ) $intCid . '
						AND sm.subsidy_message_type_id <> ' . CSubsidyMessageType::IMAX_BROADCAST . '
						AND sm.sender IS NOT NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchBroadcastMessagesByDaysToSortByCid( $intDaysToSort, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						sm.id,
						sm.message,
						sm.sent_to_imax_date
					FROM
						subsidy_messages sm
					WHERE
						sm.cid = ' . ( int ) $intCid . '
						AND sm.subsidy_message_type_id = ' . ( int ) CSubsidyMessageType::IMAX_BROADCAST . '
						AND sm.sent_to_imax_date >= \'' . date( 'Y-m-d', strtotime( ' - ' . ( int ) $intDaysToSort . ' days ' ) ) . '\'
					ORDER BY sm.sent_to_imax_date DESC ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function bulkArchive( $arrintSubsidyMessageIds, $intCid, $intCurrentUserId, $objDatabase ) {

		if( false == valArr( $arrintSubsidyMessageIds ) ) return NULL;

		$objDataset = $objDatabase->createDataset();

		$strSql = 'UPDATE
						public.subsidy_messages
					SET
						is_archived = true,
						updated_by = ' . ( int ) $intCurrentUserId . ',
						updated_on = \'NOW()\'
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN (' . implode( ',', $arrintSubsidyMessageIds ) . ')';

		return $objDataset->execute( $strSql, $objDatabase );

	}

	public static function fetchSentToImaxDateDateBySubsidyMessageTypeIdByCid( $intSubsidyMessageTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						( max( sm.sent_to_imax_date ) + INTERVAL \'1 SECOND\') as broadcast_message_date
					FROM
						subsidy_messages sm
					WHERE
						sm.cid = ' . ( int ) $intCid . '
						AND sm.subsidy_message_type_id = ' . ( int ) $intSubsidyMessageTypeId;

		return self::fetchColumn( $strSql, 'broadcast_message_date', $objDatabase );
	}

	public static function fetchSubsidyMessageByRemotePrimaryKeyByCid( $strRemotePrimaryKey, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						subsidy_messages sm
					WHERE
						sm.cid = ' . ( int ) $intCid . '
						AND sm.remote_primary_key ILIKE \'%' . addslashes( trim( $strRemotePrimaryKey ) ) . '%\'';

		return self::fetchSubsidyMessage( $strSql, $objDatabase );
	}
}
?>