<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseChecklistItems
 * Do not add any new functions to this class.
 */

class CLeaseChecklistItems extends CBaseLeaseChecklistItems {

	public static function fetchLeaseChecklistItemByLeaseIntervalIdByLeaseIdByCustomerIdByChecklistItemIdByCid( $intLeaseIntervalId, $intLeaseId, $intCustomerId, $intChecklistItemId, $intCid, $objDatabase ) {

		if( false == valId( $intLeaseIntervalId ) || false == valId( $intChecklistItemId ) || false == valId( $intLeaseId ) || false == valId( $intCustomerId ) ) {
			return NULL;
		}

		$strSql = 'SELECT lci.*
		  			FROM lease_checklist_items lci
		   				JOIN lease_checklists lc ON ( lci.cid = lc.cid AND lci.lease_checklist_id = lc.id )
		  			WHERE lci.cid = ' . ( int ) $intCid . '
		     			AND lc.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
		     			AND lc.lease_id = ' . ( int ) $intLeaseId . '
		     			AND lc.customer_id = ' . ( int ) $intCustomerId . '
		 				AND lci.checklist_item_id = ' . $intChecklistItemId;

		return parent::fetchLeaseChecklistItem( $strSql, $objDatabase );

	}

	public static function fetchCustomLeaseChecklistItemsByLeaseIdsByLeaseIntervalIdsByChecklistTriggerIdByPropertyIdByCid( $arrintLeaseIds, $arrintLeaseIntervalIds, $intChecklistTriggerId, $intPropertyId, $intCid, $objDatabase, $boolIsRequired = false ) {

		if( false == valArr( $arrintLeaseIds ) || false == valArr( $arrintLeaseIntervalIds ) ) {
			return;
		}

		$strWhere = '';

		if( CChecklistTrigger::MOVE_IN == $intChecklistTriggerId ) {
			$strWhere = ' AND lp.move_in_review_completed_on IS NOT NULL';
		}

		if( true == $boolIsRequired ) {
			$strWhere .= ' AND ci.is_required IS TRUE';
		}

		$strSql = 'SELECT
				        lc.lease_id,
				        lc.lease_interval_id,
				        count ( DISTINCT lci.checklist_item_id ),
						array_to_string( array_agg( DISTINCT lci.checklist_item_id ), \',\' ) as completed_checklist_items,
						array_to_string( array_agg( cu.username ), \',\' ) as completed_checklist_items_completed_by_username,
                        array_to_string( array_agg( lci.completed_on ), \',\' ) as completed_checklist_items_completed_on
				    FROM
				        lease_checklist_items lci
				        JOIN lease_checklists lc ON ( lci.cid = lc.cid AND lci.lease_checklist_id = lc.id )
				        JOIN property_checklists pc ON ( pc.cid = lc.cid AND pc.checklist_id = lc.checklist_id )
				        JOIN checklists c ON ( c.cid = pc.cid AND c.id = pc.checklist_id AND c.deleted_by IS NULL )
		                JOIN checklist_items ci ON ( ci.cid = lci.cid AND lci.checklist_item_id = ci.id AND c.id = ci.checklist_id )
		                JOIN company_users cu ON ( lci.cid = cu.cid AND lci.completed_by = cu.id )
		                JOIN lease_processes lp ON ( lc.cid = lp.cid AND lc.lease_id = lp.lease_id AND lp.customer_id IS NULL )
		            WHERE
				        lci.cid = ' . ( int ) $intCid . '
				        AND pc.property_id = ' . ( int ) $intPropertyId . '
				        AND lc.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
				        AND lc.lease_interval_id IN ( ' . implode( ',', $arrintLeaseIntervalIds ) . ' )
				        AND c.checklist_trigger_id = ' . ( int ) $intChecklistTriggerId . '
				        AND ci.deleted_on IS NULL 
						' . $strWhere . '
				    GROUP BY
				        lc.lease_id,
				        lc.lease_interval_id,
				        lp.move_in_review_completed_on';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompletedLeaseChecklistItemsByChecklistTriggerIdByLeaseIdByLeaseIntervalIdByCid( $intChecklistTriggerId, $intLeaseId, $intLeaseIntervalId, $intCid, $objDatabase ) {

		$strSql = 'SELECT lci.*
					FROM lease_checklist_items lci
						JOIN lease_checklists lc ON ( lc.cid = lci.cid AND lc.id = lci.lease_checklist_id )
						JOIN checklists c ON (c.cid = lci.cid AND c.id = lc.checklist_id AND c.deleted_by IS NULL )
					WHERE c.checklist_trigger_id = ' . ( int ) $intChecklistTriggerId . '
							AND lc.cid = ' . ( int ) $intCid . '
							AND lc.lease_id = ' . ( int ) $intLeaseId . '
							AND lc.lease_interval_id = ' . ( int ) $intLeaseIntervalId;

		return parent::fetchLeaseChecklistItems( $strSql, $objDatabase );
	}

	public static function fetchLeaseChecklistItemByFileIdByCid( $intCid, $intFileId, $objDatabase ) {
		$strSql = 'SELECT * FROM
						lease_checklist_items lci
					WHERE
						lci.cid = ' . ( int ) $intCid . '
						AND lci.file_id = ' . ( int ) $intFileId . ' ';

		return parent::fetchLeaseChecklistItem( $strSql, $objDatabase );
	}

	public static function fetchLeaseChecklistItemByLeaseChecklistIdByChecklistItemIdByCid( $intLeaseChecklistId, $intChecklistItemId, $intCid, $objDatabase ) {
		$strSql = 'SELECT *
					FROM lease_checklist_items lci
					WHERE lci.cid = ' . ( int ) $intCid . ' AND
					      lci.lease_checklist_id = ' . ( int ) $intLeaseChecklistId . ' AND
					      lci.checklist_item_id = ' . ( int ) $intChecklistItemId . ' ';

		return parent::fetchLeaseChecklistItem( $strSql, $objDatabase );
	}

	public static function fetchLeaseChecklistItemsByLeaseIdsByChecklistIdByCid( $arrintLeaseIds, $intChecklistId, $intCid, $objDatabase, $arrintChecklistItemsIds = array(), $intCustomerId = NULL ) {

		if( false == valId( $intChecklistId ) || false == valArr( $arrintLeaseIds ) ) {
			return NULL;
		}

		$strWhereSql = ( true == valId( $intCustomerId ) ) ? ' AND lc.customer_id = ' . ( int ) $intCustomerId : '';

		$strSql = 'SELECT lci.id,
					       lc.id as lease_checklist_id,
					       l.id as lease_id,
					       lci.checklist_item_id,
					       lci.completed_by
					FROM leases l
					     JOIN lease_checklists lc ON ( l.cid = lc.cid AND l.id = lc.lease_id AND l.active_lease_interval_id = lc.lease_interval_id )
					     JOIN lease_checklist_items lci ON (lci.cid = lc.cid AND lci.lease_checklist_id = lc.id)
					WHERE lc.cid = ' . ( int ) $intCid . ' AND
					      lc.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ') AND
					      lc.checklist_id = ' . $intChecklistId . $strWhereSql;

		if( true == valArr( $arrintChecklistItemsIds ) ) {
			$strSql .= ' AND lci.checklist_item_id IN (' . implode( ',', $arrintChecklistItemsIds ) . ')';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchIncompleteLeaseChecklistItemsByLeaseIdsByChecklistIdByCid( $arrintLeaseIds, $intChecklistId, $intCid, $objDatabase ) {

		if( false == valId( $intChecklistId ) || false == valArr( $arrintLeaseIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					    l.id,
					    total_counts.chk_count AS total_items,
					    completed_counts.completed_chk_count AS item_completed,
					    total_counts.required_items_count - completed_counts.completed_required_items AS incomplete_required_item_count,
					    total_counts.payment_chk_count - completed_counts.completed_payment_chk_count AS incomplete_payment_item_count,
					    total_counts.optional_items_count - completed_counts.completed_optional_items AS incomplete_optional_item_count
					FROM
					    leases l
					    JOIN lease_checklists lc ON ( l.cid = lc.cid AND l.id = lc.lease_id AND l.active_lease_interval_id = lc.lease_interval_id )
					    JOIN lease_processes lp ON ( l.cid = lp.cid AND l.id = lp.lease_id AND lp.customer_id IS NULL AND lp.move_in_review_completed_on IS NOT NULL )
					    JOIN LATERAL
				        (
					        SELECT
					           count ( CASE
					                   WHEN ( ci.is_required IS TRUE AND ci.checklist_item_type_id = ' . ( int ) CChecklistItemType::ACCEPT_PAYMENT . ' ) THEN 1
					                   ELSE NULL
					                   END ) AS payment_chk_count,
					           count ( 1 ) AS chk_count,
					           count ( CASE
					                   WHEN ci.is_required IS TRUE THEN 1
					                   ELSE NULL
					                   END ) AS required_items_count,
					           count ( CASE
					                   WHEN ci.is_required IS FALSE THEN 1
					                   ELSE NULL
					                   END ) AS optional_items_count
					        FROM
					           checklist_items ci
					        WHERE
					           ci.cid = lc.cid
					           AND ci.checklist_id = lc.checklist_id
					           AND ci.deleted_by IS NULL
					           AND ci.deleted_on IS NULL
				        ) AS total_counts ON ( TRUE )
						LEFT JOIN LATERAL
						(
							SELECT
								count ( CASE
								        WHEN ci.is_required IS TRUE AND ci.checklist_item_type_id = ' . ( int ) CChecklistItemType::ACCEPT_PAYMENT . ' THEN 1
								        ELSE NULL
								        END ) AS completed_payment_chk_count,
								count ( 1 ) AS completed_chk_count,
								count ( CASE
								        WHEN ci.is_required IS TRUE THEN 1
								        ELSE NULL
								        END ) AS completed_required_items,
								count ( CASE
								        WHEN ci.is_required IS FALSE THEN 1
								        ELSE NULL
								        END ) AS completed_optional_items
							FROM
								lease_checklist_items lci
								JOIN checklist_items ci ON ( lci.cid = ci.cid AND lci.checklist_item_id = ci.id AND ci.deleted_on IS NULL AND ci.deleted_by IS NULL )
							WHERE
								lci.cid = lc.cid
								AND lc.id = lci.lease_checklist_id
								AND lci.completed_on IS NOT NULL
						 ) AS completed_counts ON ( TRUE )
					WHERE
						l.cid = ' . ( int ) $intCid . '
						AND lc.checklist_id = ' . ( int ) $intChecklistId . '
						AND l.id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

}
?>