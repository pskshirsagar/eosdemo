<?php

class CLeaseInterval extends CBaseLeaseInterval {

	protected $m_boolIsPreviousLeaseInterval;
	protected $m_boolIsActiveLeaseInterval;
	protected $m_boolIsFutureLeaseInterval;
	protected $m_boolAllowCancellation;
	protected $m_boolEndChargesAtNotice;

	protected $m_strLeaseTerm;
	protected $m_strInstallmentName;
	protected $m_strMoveOutDate;

	protected $m_intPrimaryLeaseStatusTypeId;

	protected $m_objLease;

    /**
    * Get or Fetch Functions
	*
   	*/

    public function getOrFetchLease( $objDatabase ) {
    	if( true == valObj( $this->m_objLease, 'CLease' ) ) return $this->m_objLease;

    	$this->m_objLease = $this->fetchLease( $objDatabase );

    	return $this->m_objLease;
    }

	/**
	* Set Functions
	*
   	*/

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['is_previous_lease_interval'] ) && $boolDirectSet ) {
			$this->m_boolIsPreviousLeaseInterval = trim( $arrmixValues['is_previous_lease_interval'] );
		} elseif( isset( $arrmixValues['is_previous_lease_interval'] ) ) {
			$this->setIsPreviousLeaseInterval( $arrmixValues['is_previous_lease_interval'] );
		}

		if( isset( $arrmixValues['is_current_lease_interval'] ) && $boolDirectSet ) {
			$this->m_boolIsActiveLeaseInterval = trim( $arrmixValues['is_current_lease_interval'] );
		} elseif( isset( $arrmixValues['is_current_lease_interval'] ) ) {
			$this->setIsActiveLeaseInterval( $arrmixValues['is_current_lease_interval'] );
	    }

		if( isset( $arrmixValues['is_future_lease_interval'] ) && $boolDirectSet ) {
			$this->m_boolIsFutureLeaseInterval = trim( $arrmixValues['is_future_lease_interval'] );
		} elseif( isset( $arrmixValues['is_future_lease_interval'] ) ) {
			$this->setIsFutureLeaseInterval( $arrmixValues['is_future_lease_interval'] );
		}

		if( isset( $arrmixValues['lease_term'] ) ) $this->setLeaseTerm( $arrmixValues['lease_term'] );
		if( true == isset( $arrmixValues['installment_name'] ) ) $this->setInstallmentName( $arrmixValues['installment_name'] );
		if( isset( $arrmixValues['allow_cancellation'] ) ) $this->setAllowCancellation( $arrmixValues['allow_cancellation'] );
		if( isset( $arrmixValues['end_charges_at_notice'] ) ) $this->setEndChargesAtNotice( $arrmixValues['end_charges_at_notice'] );
		if( isset( $arrmixValues['move_out_date'] ) ) $this->setMoveOutDate( $arrmixValues['move_out_date'] );
	    if( isset( $arrmixValues['primary_lease_status_type_id'] ) ) $this->setPrimaryLeaseStatusTypeId( $arrmixValues['primary_lease_status_type_id'] );

		$this->setAllowDifferentialUpdate( true );

    }

	public function setIsPreviousLeaseInterval( $boolIsPreviousLeaseInterval ) {
		$this->m_boolIsPreviousLeaseInterval = $boolIsPreviousLeaseInterval;
	}

	public function setIsActiveLeaseInterval( $boolIsActiveLeaseInterval ) {
		$this->m_boolIsActiveLeaseInterval = $boolIsActiveLeaseInterval;
	}

	public function setIsFutureLeaseInterval( $boolIsFutureLeaseInterval ) {
    	$this->m_boolIsFutureLeaseInterval = $boolIsFutureLeaseInterval;
    }

    public function setLease( $objLease ) {
    	$this->m_objLease = $objLease;
    }

    public function setLeaseTerm( $strLeaseTerm ) {
    	$this->m_strLeaseTerm = $strLeaseTerm;
    }

	public function setInstallmentName( $strInstallmentName ) {
		$this->m_strInstallmentName = $strInstallmentName;
	}

	public function setAllowCancellation( $boolAllowCancellation ) {
    	$this->m_boolAllowCancellation = $boolAllowCancellation;
    }

	public function setEndChargesAtNotice( $boolEndChargesAtNotice ) {
		$this->m_boolEndChargesAtNotice = $boolEndChargesAtNotice;
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->m_strMoveOutDate = $strMoveOutDate;
	}

	public function setPrimaryLeaseStatusTypeId( $intPrimaryLeaseStatusTypeId ) {
		$this->m_intPrimaryLeaseStatusTypeId = $intPrimaryLeaseStatusTypeId;
	}

    /**
    * Get Functions
    *
   	*/

    public function getIsPreviousLeaseInterval() {
    	return $this->m_boolIsPreviousLeaseInterval;
    }

    public function getIsActiveLeaseInterval() {
    	return $this->m_boolIsActiveLeaseInterval;
    }

    public function getIsFutureLeaseInterval() {
    	return $this->m_boolIsFutureLeaseInterval;
    }

    public function getLease() {
    	return $this->m_objLease;
    }

    public function getLeaseTerm() {
    	return $this->m_strLeaseTerm;
    }

	public function getInstallmentName() {
		return $this->m_strInstallmentName;
	}

	public function getAllowCancellation() {
    	return $this->m_boolAllowCancellation;
    }

	public function getEndChargesAtNotice() {
		return $this->m_boolEndChargesAtNotice;
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function getPrimaryLeaseStatusTypeId() {
		return $this->m_intPrimaryLeaseStatusTypeId;
	}

    /**
    * Validate Functions
    *
   	*/

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) || 0 >= ( int ) $this->getCid() ) {
        	$boolIsValid = false;
        	trigger_error( __( 'Invalid Company Lease Request:  client id required - CLeaseInterval::valCid()' ), E_USER_ERROR );
        }

        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;

        if( true == is_null( $this->getLeaseId() ) || 0 >= ( int ) $this->getLeaseId() ) {
        	$boolIsValid = false;
        	trigger_error( __( 'Invalid Company Lease Request:  Id required - CLeaseInterval::valLeaseId()' ), E_USER_ERROR );
        }

        return $boolIsValid;
    }

    public function valRemotePrimaryKey() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIntervalDatetime() {
        $boolIsValid = true;

        if( true == is_null( $this->getIntervalDatetime() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interval_datetime', __( 'Interval dateime is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valGuestCardLeaseStartDate() {

    	$boolIsValid = true;
    	if( true == valStr( $this->getLeaseStartDate() ) ) {
    		$mixValidatedStartDate = CValidation::checkISODateFormat( $this->getLeaseStartDate(), $boolFormat = true );
    		if( false === $mixValidatedStartDate ) {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Lease start date is not valid.' ) ) );
    			$boolIsValid = false;
    		} else {
    			$this->setLeaseStartDate( date( 'm/d/Y', strtotime( $mixValidatedStartDate ) ) );
    		}
    	} else {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Lease start date is required.' ) ) );
    	}
    	return $boolIsValid;
    }

    public function valLeaseStartDate( $objDatabase ) {
		$boolIsValid = true;

		$objLease = $this->getOrFetchLease( $objDatabase );

		if( false == valObj( $objLease, 'CLease' ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Unable to fetch lease' ) ) );
			$boolIsValid = false;
			return $boolIsValid;
		}

		$objPrimaryLeaseCustomer = $objLease->getOrFetchPrimaryLeaseCustomer( $objDatabase );
		$objPropertyGlSetting = $objLease->getOrFetchPropertyGlSetting( $objDatabase );

	    if( true == valStr( $this->getLeaseStartDate() ) ) {
	       	$mixValidatedStartDate = CValidation::checkISODateFormat( $this->getLeaseStartDate(), $boolFormat = true );
	       	if( false === $mixValidatedStartDate ) {
	       		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Lease start date is not valid.' ) ) );
	       		$boolIsValid = false;
	       	} else {
	       		$this->setLeaseStartDate( date( 'm/d/Y', strtotime( $mixValidatedStartDate ) ) );
	       	}
		} elseif( COccupancyType::OTHER_INCOME != $objLease->getOccupancyTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Lease start date is required.' ) ) );
		}

		if ( false == $objPropertyGlSetting->getActivateStandardPosting() ) return $boolIsValid;

		if( false == is_null( $this->getLeaseStartDate() ) && false == is_null( $this->getLeaseEndDate() ) ) {

			if( strtotime( $this->getLeaseStartDate() ) > strtotime( $this->getLeaseEndDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date', __( 'Lease end date must be after the Lease start date.' ) ) );
			}
		}

        return $boolIsValid;
    }

    public function valLeaseEndDate( $objDatabase, $boolShouldBeNull = false, $boolIsFromBulkEdit = false ) {
		$boolIsValid = true;

		$objLease = $this->getOrFetchLease( $objDatabase );
		$arrobjFutureLeaseIntervals = [];

		if( false == valObj( $objLease, 'CLease' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date', __( 'Unable to fetch lease' ) ) );
			return false;
		}

		if( false == $boolIsFromBulkEdit ) {
			$arrobjFutureLeaseIntervals = ( array ) \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchfutureLeaseIntervalsByActiveLeaseIdsByCid( [ $objLease->getId() ], $this->getCid(), $objDatabase );
		}

		// Here we are ensuring that end date of MTM LI is always same as move out date if move out date is null then end date of MTM LI should also be null .
		// And if there are future LI, end date of the MTM LI can not be null

		if( false == $boolIsFromBulkEdit && false == $boolShouldBeNull && false == valArr( $arrobjFutureLeaseIntervals )
				&& CLeaseIntervalType::MONTH_TO_MONTH == $this->getLeaseIntervalTypeId() && false == valStr( $objLease->getMoveOutDate() ) ) {
			$boolShouldBeNull = true;
		}

		if( true == $boolShouldBeNull && false == is_null( $this->getLeaseEndDate() ) && COccupancyType::OTHER_INCOME != $objLease->getOccupancyTypeId() ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date', __( 'Lease end date must be null for latest lease interval if it is a month to month lease.' ) ) );
		} else {

	        if( true == valStr( $this->getLeaseEndDate() ) ) {

	        	$mixValidatedEndDate = CValidation::checkISODateFormat( $this->getLeaseEndDate(), $boolFormat = true );

	        	if( false === $mixValidatedEndDate ) {
	        		$boolIsValid = false;
	        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date', __( 'Lease end date is not valid.' ) ) );
	        	} else {
	        		$this->setLeaseEndDate( date( 'm/d/Y', strtotime( $mixValidatedEndDate ) ) );
	        	}
	        } elseif( true == is_null( $this->getLeaseEndDate() ) && COccupancyType::OTHER_INCOME != $objLease->getOccupancyTypeId() && false == $boolShouldBeNull && CLeaseIntervalType::MONTH_TO_MONTH != $this->getLeaseIntervalTypeId() ) {
	        	$boolIsValid = false;
	        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date', __( 'Lease end date is required.' ) ) );
	        }
		}

        return $boolIsValid;
    }

    // If this validation function is failing

	public function valOverlappingIntervalDates( $objDatabase, $boolIsStudentSemesterSelectionEnabled = false, $arrintSkippedLeaseStatusTypeIds = NULL ) {

		$boolIsValid = true;

		$objLease                       = $this->getOrFetchLease( $objDatabase );
		$objPrimaryLeaseCustomer        = $objLease->getOrFetchPrimaryLeaseCustomer( $objDatabase );
		$intCoreEntrataPropertiesCount  = CPropertyGlSettings::fetchCoreEntrataPropertiesByPropertyIdsByCid( [ $this->getPropertyId() ], $this->getCid(), $objDatabase, true );

		if ( 0 == $intCoreEntrataPropertiesCount || COccupancyType::OTHER_INCOME == $objLease->getOccupancyTypeId() || false == in_array( $objPrimaryLeaseCustomer->getLeaseStatusTypeId(), CLeaseStatusType::$c_arrintActivatedLeaseStatusTypes ) ) return $boolIsValid;

		if( true == $boolIsStudentSemesterSelectionEnabled && true == is_null( $objLease->getUnitSpaceId() ) ) {
			$objUnitSpace = new CunitSpace();
		} else {
			$objUnitSpace = $objLease->getUnitSpace();
			if( false == valObj( $objUnitSpace, 'CUnitSpace' ) ) {
				$boolIsFetchDeletedUnit = ( CLeaseStatusType::PAST == $objPrimaryLeaseCustomer->getLeaseStatusTypeId() );
				$objUnitSpace = \Psi\Eos\Entrata\CUnitSpaces::createService()->fetchCustomUnitSpaceByIdByCid( $objLease->getUnitSpaceId(), $objLease->getCid(), $objDatabase, $boolIsFetchDeletedUnit );
			}
		}

		if( false == valObj( $objUnitSpace, 'CUnitSpace' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Unit space must be set for an active Entrata lease for [ Lease id : {%s, 0} ].', [ $objLease->getId() ] ), E_USER_ERROR ) );
			return false;
		}

		// For Past lease validating interval dates of the same lease intervals
		if( CLeaseStatusType::PAST == $objPrimaryLeaseCustomer->getLeaseStatusTypeId() ) {
			$arrobjLeaseIntervals = \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLeaseIntervalsByLeaseIdByCid( $objLease->getId(), $objLease->getCid(), $objDatabase, $boolIsOrderByLeaseStartDate = true, $boolIsStudentSemesterSelectionEnabled, $boolIsExcludeLeaseModification = true );
		} else {
			$arrobjLeaseIntervals = $objUnitSpace->getOrFetchActiveLeaseIntervals( $objDatabase, $arrintSkippedLeaseStatusTypeIds, $objLease->getId() );
		}

		if( false == valArr( $arrobjLeaseIntervals ) ) return $boolIsValid;

		if( true == valStr( $this->getId() ) && true == array_key_exists( $this->getId(), $arrobjLeaseIntervals ) ) {
			$arrobjLeaseIntervals[$this->getId()] = $this;
		}

		$arrobjRekeyedLeaseIntervals = [];

		foreach( $arrobjLeaseIntervals as $objLeaseInterval ) {
			$arrobjRekeyedLeaseIntervals[] = $objLeaseInterval;
		}

		$objPreviousLeaseInterval = NULL;

		foreach( $arrobjRekeyedLeaseIntervals as $intKey => $objLeaseInterval ) {

			if( true == $boolIsStudentSemesterSelectionEnabled ) {
				// restrict move-in for overlapping leases only
				if( $this->m_objLease->getId() != $objLeaseInterval->getLeaseId() && strtotime( $this->m_objLease->getLeaseStartDate() ) <= strtotime( $objLeaseInterval->getLeaseEndDate() ) && strtotime( $this->m_objLease->getLeaseEndDate() ) >= strtotime( $objLeaseInterval->getLeaseStartDate() ) ) {
				     $boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Lease start date must be greater than the end date of the preceding lease interval on this unit [{%t, 0, DATE_NUMERIC_STANDARD}]', [ $objLeaseInterval->getLeaseEndDate() ] ) ) );
					break;
				}
			} else {
				// Not First lease interval
				if( 0 != $intKey ) {
					$arrmixLeaseIntervalData = $objLeaseInterval->getLastContractualLeaseInterval( $intKey, $objLeaseInterval, $arrobjRekeyedLeaseIntervals );

					$boolIsSkipValidation       = $arrmixLeaseIntervalData[0];
					$objPreviousLeaseInterval   = $arrmixLeaseIntervalData[1];

					if( true == valObj( $objPreviousLeaseInterval, 'CLeaseInterval' ) ) {
						// Validate the interval start date is one day after the previous interval end date
						if( strtotime( $objLeaseInterval->getLeaseStartDate() ) <= strtotime( $objPreviousLeaseInterval->getLeaseEndDate() ) ) {
							$boolIsValid = false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Lease start date must be greater than the end date of the preceding lease interval on this unit [{%t, 0, DATE_NUMERIC_STANDARD}]', [ $objPreviousLeaseInterval->getLeaseEndDate() ] ) ) );
							break;
						} else {
							if( ( false == $boolIsSkipValidation && $objLeaseInterval->getLeaseId() == $objPreviousLeaseInterval->getLeaseId() && false == is_null( $objPreviousLeaseInterval->getLeaseEndDate() ) ) && strtotime( '+1 day', strtotime( $objPreviousLeaseInterval->getLeaseEndDate() ) ) != strtotime( $objLeaseInterval->getLeaseStartDate() ) ) {
								$boolIsValid = false;
								$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Lease start date must be one day after previous lease interval end date' ) ) );
							}
						}
					}
				}
			}
		}

		return $boolIsValid;
	}

	// Validating bulk lease intervals

	public static function validateBulkLeaseIntervals( $arrobjBulkLeaseIntervals, $objLease, $objDatabase, $boolIsStudentSemesterSelectionEnabled = false, $boolIsSkipForMonthToMonth = false ) {

		$boolIsValid                    = true;
		$arrobjRekeyedLeaseIntervals    = [];
		$arrobjCloneBulkLeaseIntervals  = [];

		$objPrimaryLeaseCustomer = $objLease->getOrFetchPrimaryLeaseCustomer( $objDatabase );
		$objPropertyGlSetting 	 = $objLease->getOrFetchPropertyGlSetting( $objDatabase );

		if( true == $objPropertyGlSetting->getActivateStandardPosting() && true == in_array( $objPrimaryLeaseCustomer->getLeaseStatusTypeId(), CLeaseStatusType::$c_arrintActivatedLeaseStatusTypes ) ) {

			if( CLeaseStatusType::PAST == $objPrimaryLeaseCustomer->getLeaseStatusTypeId() ) {
				$arrobjLeaseIntervals = $arrobjBulkLeaseIntervals;
			} else {
				$objUnitSpace = $objLease->getOrFetchUnitSpace( $objDatabase );
				if( false == valObj( $objUnitSpace, 'CUnitSpace' ) ) {
					trigger_error( 'Unit space must be set for an active Entrata lease for [ Lease id : ' . $objLease->getId() . ' ].', E_USER_ERROR );
					return false;
				}

				$arrobjLeaseIntervals = $objUnitSpace->getOrFetchActiveLeaseIntervals( $objDatabase, NULL, $objLease->getId() );
			}

			if ( false == valArr( $arrobjLeaseIntervals ) ) return $boolIsValid;

			foreach( $arrobjLeaseIntervals as $objLeaseInterval ) {
				$arrobjRekeyedLeaseIntervals[] = $objLeaseInterval;
			}
		}

		$objPreviousLeaseInterval = NULL;
		$boolIsClean 			  = true;
		$intCount 				  = 0;

		foreach( $arrobjBulkLeaseIntervals as $objLeaseInterval ) {
			$arrobjCloneBulkLeaseIntervals[] = $objLeaseInterval;
		}

		foreach( $arrobjBulkLeaseIntervals as $objBulkLeaseInterval ) {

			$boolShouldBeNull 	= false;
			$boolIsValid &= $objBulkLeaseInterval->valCid();
			$boolIsValid &= $objBulkLeaseInterval->valLeaseId();
			$boolIsValid &= $objBulkLeaseInterval->valIntervalDatetime();
			$boolIsValid &= $objBulkLeaseInterval->valLeaseStartDate( $objDatabase );

			if( true == $boolIsStudentSemesterSelectionEnabled ) {
				$boolIsValid &= $objBulkLeaseInterval->valLeaseTermDates( $objDatabase, $boolIsSkipForMonthToMonth );
			}

			$objNextLeaseInterval = $arrobjCloneBulkLeaseIntervals[$intCount + 1];
			if( ( false == valObj( $objNextLeaseInterval, 'CLeaseInterval' ) || ( CLeaseStatusType::APPLICANT == $objNextLeaseInterval->getLeaseStatusTypeId() ) )
			    && CLeaseIntervalType::MONTH_TO_MONTH == $objBulkLeaseInterval->getLeaseIntervalTypeId()
			    && true == in_array( $objBulkLeaseInterval->getLeaseStatusTypeId(), [ CLeaseStatusType::FUTURE, CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE ] )
			    && false == valStr( $objLease->getMoveOutDate() ) ) {
					$boolShouldBeNull = true;
			}

			$boolIsValid &= $objBulkLeaseInterval->valLeaseEndDate( $objDatabase, $boolShouldBeNull, $boolIsFromBulkEdit = true );

			if ( false == $objPropertyGlSetting->getActivateStandardPosting() || COccupancyType::OTHER_INCOME == $objLease->getOccupancyTypeId() || false == in_array( $objPrimaryLeaseCustomer->getLeaseStatusTypeId(), CLeaseStatusType::$c_arrintActivatedLeaseStatusTypes ) ) continue;

			if( true == $boolIsValid ) {
				// Validating Coinciding with own lease's lease intervals
				if( $intCount > 0 ) {

					$arrmixLeaseIntervalData = $objBulkLeaseInterval->getLastContractualLeaseInterval( $intCount, $objBulkLeaseInterval, $arrobjCloneBulkLeaseIntervals );

					$boolIsSkipValidation       = $arrmixLeaseIntervalData[0];
					$objPreviousLeaseInterval   = $arrmixLeaseIntervalData[1];

					if( true == valObj( $objPreviousLeaseInterval, 'CLeaseInterval' ) ) {
						// Validate the interval start date is one day after the previous interval end date
						if( strtotime( $objBulkLeaseInterval->getLeaseStartDate() ) <= strtotime( $objPreviousLeaseInterval->getLeaseEndDate() ) ) {
							$boolIsClean = false;
							$objBulkLeaseInterval->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Lease start date must be greater than the end date of the preceding lease interval on this unit [{%t, 0, DATE_NUMERIC_STANDARD}]', [ $objPreviousLeaseInterval->getLeaseEndDate() ] ) ) );
						} else {
							if( false == $boolIsSkipValidation && strtotime( '+1 day', strtotime( $objPreviousLeaseInterval->getLeaseEndDate() ) ) != strtotime( $objBulkLeaseInterval->getLeaseStartDate() ) ) {
								$boolIsClean = false;
								$objBulkLeaseInterval->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Lease start date must be one day after previous lease interval end date' ) ) );
							}
						}
					}
				}

				foreach( $arrobjRekeyedLeaseIntervals as $objLeaseInterval ) {

					if( $objLeaseInterval->getLeaseId() == $objBulkLeaseInterval->getLeaseId() || CLeaseStatusType::PAST == $objPrimaryLeaseCustomer->getLeaseStatusTypeId() ) continue;

					// Validating Coinciding with other lease's lease intervals
					if( ( strtotime( $objBulkLeaseInterval->getLeaseEndDate() ) >= strtotime( $objLeaseInterval->getLeaseStartDate() ) && strtotime( $objBulkLeaseInterval->getLeaseStartDate() ) <= strtotime( $objLeaseInterval->getLeaseStartDate() ) )
							|| ( strtotime( $objBulkLeaseInterval->getLeaseEndDate() ) >= strtotime( $objLeaseInterval->getLeaseEndDate() ) && strtotime( $objBulkLeaseInterval->getLeaseStartDate() ) <= strtotime( $objLeaseInterval->getLeaseEndDate() ) )
								|| ( strtotime( $objBulkLeaseInterval->getLeaseStartDate() ) >= strtotime( $objLeaseInterval->getLeaseStartDate() ) && strtotime( $objBulkLeaseInterval->getLeaseEndDate() ) <= strtotime( $objLeaseInterval->getLeaseEndDate() ) ) ) {

						$boolIsClean = false;
						$objBulkLeaseInterval->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'This coincides with the lease dates of another lease on this unit [Start Date : {%t, lease_start_date, DATE_NUMERIC_STANDARD}, End Date : {%t, lease_end_date, DATE_NUMERIC_STANDARD}]', [ 'lease_start_date' => $objLeaseInterval->getLeaseStartDate(), 'lease_end_date' => $objLeaseInterval->getLeaseEndDate() ] ) ) );

					}
				}
			}

			$intCount++;
		}

		$boolIsValid &= $boolIsClean;

		return $boolIsValid;
	}

	public function getLastContractualLeaseInterval( $intIndex, $objLeaseInterval, $arrobjLeaseIntervals ) {

		$boolIsSkipValidation = false;

		if( $objLeaseInterval->getLeaseId() == $arrobjLeaseIntervals[$intIndex - 1]->getLeaseId() && ( ( CLeaseStatusType::APPLICANT == $objLeaseInterval->getLeaseStatusTypeId() && CLeaseIntervalType::MONTH_TO_MONTH == $arrobjLeaseIntervals[$intIndex - 1]->getLeaseIntervalTypeId() )
		 || ( CLeaseIntervalType::MONTH_TO_MONTH == $objLeaseInterval->getLeaseIntervalTypeId() && CLeaseStatusType::APPLICANT == $arrobjLeaseIntervals[$intIndex - 1]->getLeaseStatusTypeId() ) ) ) {

			if( CLeaseStatusType::APPLICANT == $objLeaseInterval->getLeaseStatusTypeId() ) {
				$boolIsSkipValidation   = true;
			}

			$intKey                 = $intIndex;
			for( $intKey = $intKey - 2; $intKey >= 0; $intKey-- ) {

				if( true == in_array( $arrobjLeaseIntervals[$intKey]->getLeaseStatusTypeId(), [ CLeaseStatusType::APPLICANT, CLeaseStatusType::CANCELLED ] ) ) {
					continue;
				}

				return [ $boolIsSkipValidation, $arrobjLeaseIntervals[$intKey] ];
			}
		}

		return [ $boolIsSkipValidation, $arrobjLeaseIntervals[$intIndex - 1] ];
	}

	public function valLateFeeFormulaId() {
		$boolIsValid = true;

		if( true == is_null( $this->getLateFeeFormulaId() ) || 0 >= ( int ) $this->getLateFeeFormulaId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'late_fee_formula_id ', __( 'Please select late fee formula.' ) ) );
		}

		return $boolIsValid;
	}

	public function validateDataIntegrity( $objDatabase, $strAction = NULL ) {

		$boolIsValid = true;

		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			trigger_error( 'Database object is required while validating lease interval.', E_USER_ERROR );
			exit;
		}

		$objPropertyGlSetting = NULL;
		$objLease			  = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCidBySchema( $this->getLeaseId(), $this->getCid(), $objDatabase, $boolFetchFromView = false );

		if( true == valObj( $objLease, 'CLease' ) ) {
			$objPropertyGlSetting = $objLease->getOrFetchPropertyGlSetting( $objDatabase );
 		}

		if( true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) && true == $objPropertyGlSetting->getActivateStandardPosting() ) {

			// Do not allow more than one current lease interval on a lease
			$intCurrentLeaseIntervalsCount = \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchCurrentLeaseIntervalsCountByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );

			if( 1 < $intCurrentLeaseIntervalsCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_status_type_id', __( 'More than one current lease interval cannot exist on a lease.' ) ) );
				$boolIsValid = false;
			}

			// The end date on a lease interval should always be set unless it's a month 2 month interval or other income (entrata core only)
			if( false == in_array( $this->getLeaseIntervalTypeId(), [ CLeaseIntervalType::LEASE_MODIFICATION, CLeaseIntervalType::MONTH_TO_MONTH ] ) && COccupancyType::OTHER_INCOME != $objLease->getOccupancyTypeId() && true == is_null( $this->getLeaseEndDate() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date', __( 'Lease end date must be set.' ) ) );
				$boolIsValid = false;
			}

			// Past month to month interval should have an end date
			if( $this->getLeaseIntervalTypeId() == CLeaseIntervalType::MONTH_TO_MONTH && $this->getLeaseStatusTypeId() == CLeaseStatusType::PAST && true == is_null( $this->getLeaseEndDate() ) && true == $objPropertyGlSetting->getActivateStandardPosting() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'past_month_to_month', __( 'Past month to month interval should have an end date.' ) ) );
				$boolIsValid = false;
			}

		}

		// Don't allow interval start to be greater than interval end date
		if( true == valObj( $objLease, 'CLease' ) && COccupancyType::AFFORDABLE != $objLease->getOccupancyTypeId() ) {
			if( true == valStr( $this->getLeaseEndDate() ) && strtotime( $this->getLeaseStartDate() ) > strtotime( $this->getLeaseEndDate() ) && CLeaseStatusType::CANCELLED != $this->getLeaseStatusTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_interval_dates', __( 'Lease start date must be less than or equal to lease end date.' ) ) );
				$boolIsValid = false;
			}
		}

		// Late fee formula must be set
		if( true == is_null( $this->getLateFeeFormulaId() ) && 'update' == $strAction ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'late_fee_formula', __( 'Late fee formula id must be set on a lease interval.' ) ) );
			$boolIsValid = false;
		}

		// Property lease term id must be set
		if( true == is_null( $this->getLeaseTermId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_term', __( 'A Lease Term must be selected for the current Lease Interval.' ) ) );
			$boolIsValid = false;
		}

		// lease_intervals.lease_start_date must be not null
		if( true == is_null( $this->getLeaseStartDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Lease start date must be set on a lease interval.' ) ) );
			$boolIsValid = false;
		}

		// lease_intervals.lease_interval_type_id must be not null
		if( true == is_null( $this->getLeaseIntervalTypeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_interval_type_id', __( 'Lease interval type id must be set on a lease interval.' ) ) );
			$boolIsValid = false;
		}

		// lease_intervals.lease_status_type_id must be not null
		if( true == is_null( $this->getLeaseStatusTypeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_status_type_id', __( 'Lease status type id must be set on a lease interval.' ) ) );
			$boolIsValid = false;
		}

		// If property requires installments, company_special_id must be set
		$arrmixPropertyChargeSettingsData = \Psi\Eos\Entrata\CPropertyChargeSettings::createService()->fetchSimplePropertyChargeSettingsDataByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );

		if( true == valArr( $arrmixPropertyChargeSettingsData ) && 1 == $arrmixPropertyChargeSettingsData['require_installments'] && true == is_null( $this->getSpecialId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_special_id', __( 'Company special id must be set on a lease interval when property requires installment.' ) ) );
			$boolIsValid = false;
		}

		// If interval type = month 2 month, it cannot have a status of applicant
		if( $this->getLeaseIntervalTypeId() == CLeaseIntervalType::MONTH_TO_MONTH && $this->getLeaseStatusTypeId() == CLeaseStatusType::APPLICANT ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( ' A month to month interval canot have a status of applicant.' ) ) );
			$boolIsValid = false;
		}

		// Mid Lease modification intervals should always have a parent_lease_interval_id set
		if( $this->getLeaseIntervalTypeId() == CLeaseIntervalType::LEASE_MODIFICATION && true == is_null( $this->getParentLeaseIntervalId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mid_lease_parent_lease_interval', __( ' Mid Lease modification intervals should always have a parent_lease_interval_id set.' ) ) );
			$boolIsValid = false;
		}

		// Mid Lease modification intervals should always have either applicant, past or cancelled as lease_status_type
		if( $this->getLeaseIntervalTypeId() == CLeaseIntervalType::LEASE_MODIFICATION && false == in_array( $this->getLeaseStatusTypeId(), [ CLeaseStatusType::FUTURE, CLeaseStatusType::APPLICANT, CLeaseStatusType::CANCELLED, CLeaseStatusType::PAST ] ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mid_lease_modification_status', __( 'Mid Lease modification intervals should always have either applicant, past or cancelled as lease_status_type.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;

	}

	public function valLeaseTermDates( $objDatabase, $boolIsSkipForMonthToMonth = false ) {
		$boolIsValid = true;

		if( true == valId( $this->getLeaseStartWindowId() ) && ( ( true == $boolIsSkipForMonthToMonth && CLeaseIntervalType::MONTH_TO_MONTH != $this->getLeaseIntervalTypeId() ) || false == $boolIsSkipForMonthToMonth ) ) {
			$strBillingEndDate = CLeaseStartWindows::fetchBillingEndDateByLeaseTermIdByPropertyIdByCid( $this->getLeaseStartWindowId(), $this->getPropertyId(), $this->getCid(), $objDatabase );

			if( true == valStr( $strBillingEndDate ) && strtotime( $this->getLeaseStartDate() ) >= strtotime( $strBillingEndDate ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Lease start date must be less than the billing end date ( {%t, 0, DATE_NUMERIC_STANDARD} ) of selected lease term', [ $strBillingEndDate ] ) ) );
			}
		}

		return $boolIsValid;
	}

    public function validate( $strAction, $objDatabase, $boolIsStudentSemesterSelectionEnabled = false, $arrintSkippedLeaseStatusTypeIds = NULL, $boolIsSkipOverlappingIntervalDatesValidation = false ) {

        $boolIsValid = true;

        if( false == valObj( $objDatabase, 'CDatabase' ) ) {
        	trigger_error( __( 'Database object is required while validating lease interval.' ), E_USER_ERROR );
        	exit;
        }

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valLeaseId();
            	$boolIsValid &= $this->valIntervalDatetime();
            	$boolIsValid &= $this->valLeaseStartDate( $objDatabase );
            	$boolIsValid &= $this->valLeaseEndDate( $objDatabase );
	            if( true == $boolIsStudentSemesterSelectionEnabled ) {
		            $boolIsValid &= $this->valLeaseTermDates( $objDatabase );
	            }
				if( false == $boolIsSkipOverlappingIntervalDatesValidation ) {
					$boolIsValid &= $this->valOverlappingIntervalDates( $objDatabase );
				}
				break;

            case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valLeaseId();
            	$boolIsValid &= $this->valIntervalDatetime();
            	$boolIsValid &= $this->valLeaseStartDate( $objDatabase );
            	$boolIsValid &= $this->valLeaseEndDate( $objDatabase, false, false );
	            if( true == $boolIsStudentSemesterSelectionEnabled ) {
		            $boolIsValid &= $this->valLeaseTermDates( $objDatabase, true );
	            }
            	if( false == $boolIsSkipOverlappingIntervalDatesValidation ) {
            		$boolIsValid &= $this->valOverlappingIntervalDates( $objDatabase, $boolIsStudentSemesterSelectionEnabled, $arrintSkippedLeaseStatusTypeIds );
            	}
            	break;

            case VALIDATE_DELETE:
            	break;

            case 'validate_approve_lease':
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valLeaseId();
            	$boolIsValid &= $this->valIntervalDatetime();
            	$boolIsValid &= $this->valLeaseStartDate( $objDatabase );
            	$boolIsValid &= $this->valLeaseEndDate( $objDatabase );
	            if( true == $boolIsStudentSemesterSelectionEnabled ) {
		            $boolIsValid &= $this->valLeaseTermDates( $objDatabase );
	            }
            	// $boolIsValid &= $this->valOverlappingIntervalDates( $objDatabase );
				break;

			case 'validate_late_fee_formula_association':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valLateFeeFormulaId();
				break;

			case 'validate_guest_card':
				$boolIsValid &= $this->valGuestCardLeaseStartDate();
				break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
    * Other Functions
    *
   	*/

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsToValidateDataIntegrity = true ) {
		if( false == valId( $this->getId() ) || false == valId( $this->getCreatedBy() ) || false == valStr( $this->getCreatedOn() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly, $boolIsToValidateDataIntegrity );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly, $boolIsToValidateDataIntegrity );
		}
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsToValidateDataIntegrity = true ) {
		$boolIsValid = ( !$boolIsToValidateDataIntegrity || true == $this->validateDataIntegrity( $objDatabase, 'insert' ) ) ? true : false;

		return ( true == $boolIsValid ) ? parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) : false;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsToValidateDataIntegrity = true ) {
		$boolIsValid = ( !$boolIsToValidateDataIntegrity || true == $this->validateDataIntegrity( $objDatabase, 'update' ) ) ? true : false;

		return ( true == $boolIsValid ) ? parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) : false;
	}

    /**
    * Fetch Functions
    *
   	*/

    public function fetchLease( $objDatabase ) {
    	return $this->m_objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCidBySchema( $this->getLeaseId(), $this->getCid(), $objDatabase, $boolFetchFromView = true );
    }

    public function fetchLeaseTerm( $objDatabase ) {
    	return CLeaseTerms::fetchLeaseTermByIdByCid( $this->getLeaseTermId(), $this->getCid(), $objDatabase );
    }

    public function fetchAddOnLeaseAssociationsByAddOnTypeIds( $arrintAddOnTypeIds, $objDatabase, $boolIsCheckDeleted = false, $boolExcludeAddOnsAttachedToUnit = false, $arrintLeaseStatusTypeIds = NULL ) {
    	return \Psi\Eos\Entrata\Custom\CAddOnLeaseAssociations::createService()->fetchAddOnLeaseAssociationsByPropertyIdByLeaseIntervalIdsByAddOnTypeIdsByCid( $this->getPropertyId(), [ $this->getId() ], $arrintAddOnTypeIds, $this->getCid(), $objDatabase, $boolIsCheckDeleted, $boolExcludeAddOnsAttachedToUnit, $arrintLeaseStatusTypeIds );
    }

	public function fetchNewAddOnLeaseAssociationsByAddOnTypeIds( $arrintAddOnTypeIds, $objDatabase, $boolIsCheckDeleted = false, $boolExcludeAddOnsAttachedToUnit = false, $arrintLeaseStatusTypeIds = NULL ) {
		return \Psi\Eos\Entrata\Custom\CAddOnLeaseAssociations::createService()->fetchNewAddOnLeaseAssociationsByPropertyIdByLeaseIntervalIdsByAddOnTypeIdsByCid( $this->getPropertyId(), [ $this->getId() ], $arrintAddOnTypeIds, $this->getCid(), $objDatabase, $boolIsCheckDeleted, $boolExcludeAddOnsAttachedToUnit, $arrintLeaseStatusTypeIds );
	}

	public function fetchApplication( $objDatabase ) {
		return \Psi\Eos\Entrata\CApplications::createService()->fetchApplicationByLeaseIdByLeaseIntervalIdByCid( $this->getLeaseId(), $this->getId(), $this->getCid(), $objDatabase );
	}

	public function createLeaseIntervalInstallmentPlan() {

		$objLeaseIntervalInstallmentPlan = new CLeaseIntervalInstallmentPlan();

		$objLeaseIntervalInstallmentPlan->setCid( $this->getCid() );
		$objLeaseIntervalInstallmentPlan->setLeaseIntervalId( $this->getId() );
		$objLeaseIntervalInstallmentPlan->setLeaseId( $this->getLeaseId() );

		return $objLeaseIntervalInstallmentPlan;
	}

	public function createLeaseUtility() {

		$objLeaseUtility = new CLeaseUtility();

		$objLeaseUtility->setCid( $this->getCid() );
		$objLeaseUtility->setPropertyId( $this->getPropertyId() );
		$objLeaseUtility->setLeaseIntervalId( $this->getId() );
		$objLeaseUtility->setLeaseId( $this->getLeaseId() );

		return $objLeaseUtility;
	}

	public function isContractualLeaseInterval() : bool {
		return !in_array( $this->getLeaseIntervalTypeId(), CLeaseIntervalType::$c_arrintNonContractulaLeaseIntervalTypes );
	}

	public function isRenewalInterval() : bool {
    	return $this->getLeaseIntervalTypeId() == CLeaseIntervalType::RENEWAL;
	}

}
?>