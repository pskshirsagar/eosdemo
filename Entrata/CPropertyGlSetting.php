<?php

class CPropertyGlSetting extends CBasePropertyGlSetting {

	const IS_MODIFIED_ACCRUAL_GPR_NO			= 0;

	const AUTO_POST_GPR_ADJUSTMENT_NO			= 0;
	const AUTO_POST_GPR_ADJUSTMENT_MONTHLY		= 1;
	const AUTO_POST_GPR_ADJUSTMENT_DAILY		= 2;

	const GPR_CALCULATE_VACANCY_ON_AR_ADVANCE	= 0;
	const GPR_CALCULATE_VACANCY_ON_AR_CLOSE		= 1;

	const GPR_BOOK_BASIS_ACCRUAL				= 'accrual';
	const GPR_BOOK_BASIS_CASH					= 'cash';
	const GPR_BOOK_BASIS_BOTH					= 'both';

	const VALIDATION_MODE_STRICT				= 0;
	const VALIDATION_MODE_OPEN_PERIODS			= 1;
	const VALIDATION_MODE_EDIT_CLOSED_PERIODS	= 2;
	const VALIDATION_MODE_EDIT_INITIAL_IMPORT	= 3;

	public $m_arrmixPropertyGroupIds;

	protected $m_intGlAccountId;
	protected $m_intMonthToMonthRentArCodeId;
	protected $m_strGlTreeName;
	protected $m_strPropertyName;
	protected $m_strGprCashBasis;
	protected $m_strGprAccrualBasis;

	protected $m_arrobjAssociatedGlAccounts;

	/**
	 * Get Functions
	 *
	 */

	public function getGlTreeName() {
		return $this->m_strGlTreeName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getAssociatedGlAccounts() {
		return $this->m_arrobjAssociatedGlAccounts;
	}

	public function getGprCashBasis() {
		return $this->m_strGprCashBasis;
	}

	public function getGprAccrualBasis() {
		return $this->m_strGprAccrualBasis;
	}

	public function getMonthToMonthRentArCodeId() {
		return $this->m_intMonthToMonthRentArCodeId;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setGlTreeName( $strGlTreeName ) {
		$this->m_strGlTreeName = $strGlTreeName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = CStrings::strTrimDef( $strPropertyName, 50, NULL, true );
	}

	public function setAssociatedGlAccounts( $arrobjAssociatedGlAccounts ) {
		$this->m_arrobjAssociatedGlAccounts = $arrobjAssociatedGlAccounts;
	}

	public function setGlAccountId( $intGlAccountId ) {
		$this->m_intGlAccountId = CStrings::strToIntDef( $intGlAccountId, NULL, false );
	}

	public function setGprCashBasis( $strGprCashBasis ) {
		return $this->m_strGprCashBasis = CStrings::strTrimDef( $strGprCashBasis, 50, NULL, true );
	}

	public function setGprAccrualBasis( $strGprAccrualBasis ) {
		return $this->m_strGprAccrualBasis = CStrings::strTrimDef( $strGprAccrualBasis, 50, NULL, true );
	}

	public function setMonthToMonthRentArCodeId( $intMonthToMonthRentArCodeId ) {
		return $this->m_intMonthToMonthRentArCodeId = $intMonthToMonthRentArCodeId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );

		if( true == isset( $arrmixValues['ap_lock_month'] ) ) $this->setApLockMonth( $this->formatPostMonth( $arrmixValues['ap_lock_month'] ) );
		if( true == isset( $arrmixValues['ar_lock_month'] ) ) $this->setArLockMonth( $this->formatPostMonth( $arrmixValues['ar_lock_month'] ) );
		if( true == isset( $arrmixValues['gl_lock_month'] ) ) $this->setGlLockMonth( $this->formatPostMonth( $arrmixValues['gl_lock_month'] ) );

		if( true == isset( $arrmixValues['ar_post_month'] ) ) $this->setArPostMonth( $this->formatPostMonth( $arrmixValues['ar_post_month'] ) );
		if( true == isset( $arrmixValues['ap_post_month'] ) ) $this->setApPostMonth( $this->formatPostMonth( $arrmixValues['ap_post_month'] ) );
		if( true == isset( $arrmixValues['gl_post_month'] ) ) $this->setGlPostMonth( $this->formatPostMonth( $arrmixValues['gl_post_month'] ) );

		if( true == isset( $arrmixValues['gl_account_id'] ) ) $this->setGlAccountId( $arrmixValues['gl_account_id'] );

		if( true == isset( $arrmixValues['gpr_cash_basis'] ) ) $this->setGprCashBasis( $arrmixValues['gpr_cash_basis'] );
		if( true == isset( $arrmixValues['gpr_accrual_basis'] ) ) $this->setGprAccrualBasis( $arrmixValues['gpr_accrual_basis'] );

		if( true == isset( $arrmixValues['month_to_month_rent_ar_code_id'] ) ) $this->setMonthToMonthRentArCodeId( $arrmixValues['month_to_month_rent_ar_code_id'] );
		if( true == isset( $arrmixValues['gl_tree_name'] ) ) $this->setGlTreeName( $arrmixValues['gl_tree_name'] );

		// We will remove it after standard release.
		if( true == isset( $arrmixValues['tax_percent'] ) ) $this->setTaxPercent( $arrmixValues['tax_percent'] );

		return;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getGlAccountId() {
		return $this->m_intGlAccountId;
	}

	public function formatPostMonth( $strPostDate ) {
		$strPostDate = trim( $strPostDate );
		$arrstrPostDate = explode( '/', $strPostDate );
		if( true == valArr( $arrstrPostDate ) && ( 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostDate ) ) ) {
			return $arrstrPostDate[0] . '/01/' . $arrstrPostDate[1];
		} else {
			return $strPostDate;
		}
	}

	public function getFormattedArPostMonth() {
		if( true == is_null( $this->m_strArPostMonth ) ) return NULL;
		return preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', $this->m_strArPostMonth );
	}

	/**
	 * Validate Functions
	 *
	 */

	/**
	 * @param						$strAction
	 * @param \CDatabase			$objDatabase
	 * @param \CPropertyGlSetting	$objOldPropertyGlSetting
	 * @param bool					$boolIsFromClosings
	 * @param bool					$boolIsSettingsTemplateProperty
	 * @return bool
	 */

	public function validate( $strAction, $objDatabase, $objOldPropertyGlSetting = NULL, $boolIsFromClosings = false, $boolIsSettingsTemplateProperty = false ) {
		$boolIsValid = true;

		$objPropertyGlSettingValidator = new CPropertyGlSettingValidator();
		$objPropertyGlSettingValidator->setPropertyGlSetting( $this );
		$boolIsValid &= $objPropertyGlSettingValidator->validate( $strAction, $objDatabase, $objOldPropertyGlSetting, $boolIsFromClosings, $boolIsSettingsTemplateProperty );

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createunitSpaceExclusion() {

		$objUnitSpaceExclusion = new CunitSpaceExclusion();
		$objUnitSpaceExclusion->setCid( $this->getCid() );
		$objUnitSpaceExclusion->setPropertyId( $this->getPropertyId() );

		return $objUnitSpaceExclusion;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchAssosiatedGlAccounts( $objDatabase, $arrintGlAccountTypeIds = NULL, $objBudgetsFilter = NULL, $boolAllowPostingToRestrictedGlAccounts = true, $boolJEAllowPostingToRestrictedGlAccount = true, $boolIsSettingsTemplateProperty = false ) {

		$arrintPropertyIds					= [];
		$arrintPropertyIds[]				= $this->getPropertyId();
		$this->m_arrmixPropertyGroupIds		= [];
		$this->m_arrmixPropertyGroupIds		= rekeyArray( 'property_group_id', ( array ) \Psi\Eos\Entrata\CPropertyGroupAssociations::createService()->fetchPropertyGroupAssociationsByCidByPropertyIds( $this->getCid(), $arrintPropertyIds, $objDatabase ) );
		return \Psi\Eos\Entrata\CGlAccounts::createService()->fetchAssociatedGlAccountsByPropertyGroupIdsByCid( array_keys( $this->m_arrmixPropertyGroupIds ), $this->getCid(), $objDatabase, $arrintGlAccountTypeIds, $objBudgetsFilter, $boolAllowPostingToRestrictedGlAccounts, $boolJEAllowPostingToRestrictedGlAccount, $boolIsSettingsTemplateProperty );
	}

	public function fetchProperty( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpaceExclusions( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CUnitSpaceExclusions::createService()->fetchUnitSpaceExclusionsByPropertyIdsByCid( [ $this->getPropertyId() ], $this->getCid(), $objClientDatabase );
	}

	public function fetchAssosiatedGPRGlAccounts( $objDatabase ) {

		$arrintPropertyIds					= [];
		$arrintPropertyIds[]				= $this->getPropertyId();
		$arrintGlAccountUsageTypeIds 			= [ CGlAccountUsageType::RENT, CGlAccountUsageType::GAIN_LOSS_ON_LEAVE, CGlAccountUsageType::VACANCY_LOSS ];
		$this->m_arrmixPropertyGroupIds		= [];

		$this->m_arrmixPropertyGroupIds	= rekeyArray( 'property_group_id', ( array ) \Psi\Eos\Entrata\CPropertyGroupAssociations::createService()->fetchPropertyGroupAssociationsByCidByPropertyIds( $this->m_objClient->getId(), $arrintPropertyIds, $this->m_objDatabase ) );
		return CGlAccounts::fetchAssociatedGlAccountsByPropertyGroupIdsByGlAccountUsageTypeIdsByCid( array_keys( $this->m_arrmixPropertyGroupIds ), $arrintGlAccountUsageTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchPeriods( $objDatabase ) {
		return CPeriods::fetchPeriodsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Other Functions
	 *
	 */

	/**
	 * @param $objPropertyGlSetting
	 * @param $arrobjBankAccounts
	 * @param $arrmixGlAccounts
	 * @return string
	 */

	public function mapKeyValuePair( $objPropertyGlSetting, $arrobjBankAccounts, $arrmixGlAccounts ) {

		if( true == valArr( $arrmixGlAccounts ) ) {
			foreach( $arrmixGlAccounts as $arrmixGlAccount ) {
				$arrmixGlAccounts[$arrmixGlAccount['id']] = $arrmixGlAccount;
			}
		}

		$strKeyValuePair = '';

		if( true == ( $objPropertyGlSetting->getRetainedEarningsGlAccountId() ) && true == valArr( $arrmixGlAccounts ) && true == array_key_exists( $objPropertyGlSetting->getRetainedEarningsGlAccountId(), $arrmixGlAccounts ) ) {
			$strKeyValuePair .= 'Retained Earnings GL Account::' . $arrmixGlAccounts[$objPropertyGlSetting->getRetainedEarningsGlAccountId()]['account_number'] . ' : ' . $arrmixGlAccounts[$objPropertyGlSetting->getRetainedEarningsGlAccountId()]['account_name'] . '~^~';
		} else {
			$strKeyValuePair .= 'Retained Earnings GL Account::' . '~^~';
		}

		if( true == ( $objPropertyGlSetting->getApGlAccountId() ) && true == valArr( $arrmixGlAccounts ) && true == array_key_exists( $objPropertyGlSetting->getApGlAccountId(), $arrmixGlAccounts ) ) {
			$strKeyValuePair .= 'AP Account::' . $arrmixGlAccounts[$objPropertyGlSetting->getApGlAccountId()]['account_number'] . ' : ' . $arrmixGlAccounts[$objPropertyGlSetting->getApGlAccountId()]['account_name'] . '~^~';
		} else {
			$strKeyValuePair .= 'AP Account::' . '~^~';
		}

		$strKeyValuePair .= 'Auto Post Refund Invoice::' . $objPropertyGlSetting->getAutoPostRefundInvoice() . '~^~';

		if( true == valId( $objPropertyGlSetting->getMarketRentGlAccountId() ) && true == valArr( $arrmixGlAccounts ) && true == array_key_exists( $objPropertyGlSetting->getMarketRentGlAccountId(), $arrmixGlAccounts ) ) {
			$strKeyValuePair .= 'Market Rent GL Account::' . $arrmixGlAccounts[$objPropertyGlSetting->getMarketRentGlAccountId()]['account_number'] . ' : ' . $arrmixGlAccounts[$objPropertyGlSetting->getMarketRentGlAccountId()]['account_name'] . '~^~';
		} else {
			$strKeyValuePair .= 'Market Rent GL Account::' . '~^~';
		}
		if( true == valId( $objPropertyGlSetting->getGainToLeaseGlAccountId() ) && true == valArr( $arrmixGlAccounts ) && true == array_key_exists( $objPropertyGlSetting->getGainToLeaseGlAccountId(), $arrmixGlAccounts ) ) {
			$strKeyValuePair .= 'Gain To Lease GL Account::' . $arrmixGlAccounts[$objPropertyGlSetting->getGainToLeaseGlAccountId()]['account_number'] . ' : ' . $arrmixGlAccounts[$objPropertyGlSetting->getGainToLeaseGlAccountId()]['account_name'] . '~^~';
		} else {
			$strKeyValuePair .= 'Gain To Lease GL Account::' . '~^~';
		}
		if( true == valId( $objPropertyGlSetting->getLossToLeaseGlAccountId() ) && true == valArr( $arrmixGlAccounts ) && true == array_key_exists( $objPropertyGlSetting->getLossToLeaseGlAccountId(), $arrmixGlAccounts ) ) {
			$strKeyValuePair .= 'Loss To Lease GL Account::' . $arrmixGlAccounts[$objPropertyGlSetting->getLossToLeaseGlAccountId()]['account_number'] . ' : ' . $arrmixGlAccounts[$objPropertyGlSetting->getLossToLeaseGlAccountId()]['account_name'] . '~^~';
		} else {
			$strKeyValuePair .= 'Loss To Lease GL Account::' . '~^~';
		}
		if( true == valId( $objPropertyGlSetting->getVacancyLossGlAccountId() ) && true == valArr( $arrmixGlAccounts ) && true == array_key_exists( $objPropertyGlSetting->getVacancyLossGlAccountId(), $arrmixGlAccounts ) ) {
			$strKeyValuePair .= 'Vacancy Loss GL Account::' . $arrmixGlAccounts[$objPropertyGlSetting->getVacancyLossGlAccountId()]['account_number'] . ' : ' . $arrmixGlAccounts[$objPropertyGlSetting->getVacancyLossGlAccountId()]['account_name'] . '~^~';
		} else {
			$strKeyValuePair .= 'Vacancy Loss GL Account::' . '~^~';
		}

		if( true == valId( $objPropertyGlSetting->getDelinquentRentGlAccountId() ) && true == valArr( $arrmixGlAccounts ) && true == array_key_exists( $objPropertyGlSetting->getDelinquentRentGlAccountId(), $arrmixGlAccounts ) ) {
			$strKeyValuePair .= 'Delinquent Rent GL Account::' . $arrmixGlAccounts[$objPropertyGlSetting->getDelinquentRentGlAccountId()]['account_number'] . ' : ' . $arrmixGlAccounts[$objPropertyGlSetting->getDelinquentRentGlAccountId()]['account_name'] . '~^~';
		} else {
			$strKeyValuePair .= 'Delinquent Rent GL Account::' . '~^~';
		}

		if( true == valId( $objPropertyGlSetting->getPrepaidRentGlAccountId() ) && true == valArr( $arrmixGlAccounts ) && true == array_key_exists( $objPropertyGlSetting->getPrepaidRentGlAccountId(), $arrmixGlAccounts ) ) {
			$strKeyValuePair .= 'Prepaid Rent GL Account::' . $arrmixGlAccounts[$objPropertyGlSetting->getPrepaidRentGlAccountId()]['account_number'] . ' : ' . $arrmixGlAccounts[$objPropertyGlSetting->getPrepaidRentGlAccountId()]['account_name'] . '~^~';
		} else {
			$strKeyValuePair .= 'Prepaid Rent GL Account::' . '~^~';
		}

		$strKeyValuePair .= 'AR Post Month::' . $objPropertyGlSetting->getArPostMonth() . '~^~';
		$strKeyValuePair .= 'AP Post Month::' . $objPropertyGlSetting->getApPostMonth() . '~^~';
		$strKeyValuePair .= 'GL Post Month::' . $objPropertyGlSetting->getGlPostMonth() . '~^~';

		$strKeyValuePair .= 'AR Lock Month::' . $objPropertyGlSetting->getArLockMonth() . '~^~';
		$strKeyValuePair .= 'AP Lock Month::' . $objPropertyGlSetting->getApLockMonth() . '~^~';
		$strKeyValuePair .= 'GL Lock Month::' . $objPropertyGlSetting->getGlLockMonth() . '~^~';

		$strKeyValuePair .= 'Is Cash Basis::' . $objPropertyGlSetting->getIsCashBasis() . '~^~';
		$strKeyValuePair .= 'Auto Update Post Month::' . $objPropertyGlSetting->getArAutoAdvancePostMonth() . '~^~';

		if( true == ( $objPropertyGlSetting->getArAdvanceDay() ) ) {
			$strKeyValuePair .= 'AR Auto Roll Day::' . $objPropertyGlSetting->getArAdvanceDay() . '~^~';
		} else {
			$strKeyValuePair .= 'AR Auto Roll Day::' . '~^~';
		}

		if( true == ( $objPropertyGlSetting->getApAdvanceDay() ) ) {
			$strKeyValuePair .= 'AP Auto Roll Day::' . $objPropertyGlSetting->getApAdvanceDay() . '~^~';
		} else {
			$strKeyValuePair .= 'AP Auto Roll Day::' . '~^~';
		}

		if( true == ( $objPropertyGlSetting->getGlAdvanceDay() ) ) {
			$strKeyValuePair .= 'GL Auto Roll Day::' . $objPropertyGlSetting->getGlAdvanceDay() . '~^~';
		} else {
			$strKeyValuePair .= 'GL Auto Roll Day::' . '~^~';
		}

		$strKeyValuePair .= 'Ignore Holidays And Weekends::' . $objPropertyGlSetting->getIgnoreHolidaysAndWeekends() . '~^~';
		$strKeyValuePair .= 'Activate GL Posting::' . $objPropertyGlSetting->getActivateStandardPosting() . '~^~';

		if( true == valArr( $arrobjBankAccounts ) && true == ( $objPropertyGlSetting->getRentBankAccountId() ) && true == array_key_exists( $objPropertyGlSetting->getRentBankAccountId(), $arrobjBankAccounts ) ) {
			$strKeyValuePair .= 'AR Bank Account::' . $arrobjBankAccounts[$objPropertyGlSetting->getRentBankAccountId()]->getAccountName() . '~^~';
		} else {
			$strKeyValuePair .= 'AR Bank Account::' . '~^~';
		}

		if( true == valArr( $arrobjBankAccounts ) && true == ( $objPropertyGlSetting->getDepositBankAccountId() ) && true == array_key_exists( $objPropertyGlSetting->getDepositBankAccountId(), $arrobjBankAccounts ) ) {
			$strKeyValuePair .= 'Security Deposit Bank Account::' . $arrobjBankAccounts[$objPropertyGlSetting->getDepositBankAccountId()]->getAccountName() . '~^~';
		} else {
			$strKeyValuePair .= 'Security Deposit Bank Account::' . '~^~';
		}

		if( true == valArr( $arrobjBankAccounts ) && true == ( $objPropertyGlSetting->getRefundBankAccountId() ) && true == array_key_exists( $objPropertyGlSetting->getRefundBankAccountId(), $arrobjBankAccounts ) ) {
			$strKeyValuePair .= 'Security Deposit Refund Account::' . $arrobjBankAccounts[$objPropertyGlSetting->getRefundBankAccountId()]->getAccountName() . '~^~';
		} else {
			$strKeyValuePair .= 'Security Deposit Refund Account::' . '~^~';
		}

		if( true == valArr( $arrobjBankAccounts ) && true == ( $objPropertyGlSetting->getApBankAccountId() ) && true == array_key_exists( $objPropertyGlSetting->getApBankAccountId(), $arrobjBankAccounts ) ) {
			$strKeyValuePair .= 'AP Bank Account::' . $arrobjBankAccounts[$objPropertyGlSetting->getApBankAccountId()]->getAccountName() . '~^~';
		} else {
			$strKeyValuePair .= 'AP Bank Account::' . '~^~';
		}

		if( true == ( $objPropertyGlSetting->getUseCustomArCodes() ) ) {
			$strKeyValuePair .= 'Use Custom Ar Codes::' . $objPropertyGlSetting->getUseCustomArCodes() . '~^~';
		} else {
			$strKeyValuePair .= 'Use Custom Ar Codes::' . '~^~';
		}

		if( true == $objPropertyGlSetting->getIsArMigrationMode() ) {
			$strKeyValuePair .= 'Is AR Migration Mode::' . $objPropertyGlSetting->getIsArMigrationMode() . '~^~';
		} else {
			$strKeyValuePair .= 'Is AR Migration Mode::' . 0 . '~^~';
		}

		if( true == $objPropertyGlSetting->getIsApMigrationMode() ) {
			$strKeyValuePair .= 'Is AP Migration Mode::' . $objPropertyGlSetting->getIsApMigrationMode() . '~^~';
		} else {
			$strKeyValuePair .= 'Is AP Migration Mode::' . 0 . '~^~';
		}

		if( true == $objPropertyGlSetting->getIsAssetMigrationMode() ) {
			$strKeyValuePair .= 'Is Asset Migration Mode::' . $objPropertyGlSetting->getIsAssetMigrationMode() . '~^~';
		} else {
			$strKeyValuePair .= 'Is Asset Migration Mode::' . 0 . '~^~';
		}

		$strKeyValuePair .= 'User Validation Mode::' . $objPropertyGlSetting->getUserValidationMode() . '~^~';

		$strKeyValuePair .= 'Use Market Rent In Gpr::' . $objPropertyGlSetting->getUseMarketRentInGpr() . '~^~';

		$strKeyValuePair .= 'Group Excluded Units In Gpr::' . $objPropertyGlSetting->getGroupExcludedUnitsInGpr() . '~^~';

		$strKeyValuePair .= 'Determine Market Rent As Of::' . $objPropertyGlSetting->getUseMarketRentInGpr() . '~^~';

		$strKeyValuePair .= 'Auto-post GPR adjustment::' . $objPropertyGlSetting->getAutoPostGprAdjustment() . '~^~';

		$strKeyValuePair .= 'Book basis::' . $objPropertyGlSetting->getGprBookBasis() . '~^~';

		$strKeyValuePair .= 'Adjust accrual to modified accrual::' . $objPropertyGlSetting->getIsModifiedAccrualGpr() . '~^~';

		$strKeyValuePair .= 'Calculate vacancy when AR period is::' . $objPropertyGlSetting->getCalculateVacancyOnArClose() . '~^~';

		$strKeyValuePair .= 'Fiscal Year End::' . $objPropertyGlSetting->getFiscalYearEndMonth() . '~^~';

		return $strKeyValuePair;
	}

	public function log( $objOldPropertyGlSetting, $objNewPropertyGlSetting, $arrobjBankAccounts, $arrmixGlAccounts, $intCompanyUserId, $objClientDatabase ) {

		$arrstrPropertyGlSettingLogfields = [
			'RetainedEarningsGlAccountId',
			'MarketRentGlAccountId',
			'GainToLeaseGlAccountId',
			'LossToLeaseGlAccountId',
			'VacancyLossGlAccountId',
			'DelinquentRentGlAccountId',
			'PrepaidRentGlAccountId',
			'ApGlAccountId',
			'AutoPostRefundInvoice',
			'AccelRentArCodeId',
			'GprBookBasis',
			'IsCashBasis',
			'ArAutoAdvancePostMonth',
			'ArAdvanceDay',
			'ApAdvanceDay',
			'GlAdvanceDay',
			'IgnoreHolidaysAndWeekends',
			'ActivateStandardPosting',
			'ArPostMonth',
			'ApPostMonth',
			'GlPostMonth',
			'ArLockMonth',
			'ApLockMonth',
			'GlLockMonth',
			'RentBankAccountId',
			'DepositBankAccountId',
			'RefundBankAccountId',
			'ApBankAccountId',
			'IsApMigrationMode',
			'IsArMigrationMode',
			'IsAssetMigrationMode',
			'UserValidationMode',
			'ReturnFeeArCodeId',
			'UseCustomArCodes',
			'UseMarketRentInGpr',
			'GroupExcludedUnitsInGpr',
			'DetermineMarketRentAsOf',
			'DepositArCodeId',
			'RentArCodeId',
			'MtmRentArCodeId',
			'MtmFeeArCodeId',
			'AutoPostGprAdjustment',
			'IsModifiedAccrualGpr',
			'CalculateVacancyOnArClose',
			'FiscalYearEndMonth'
		];

		$arrmixChangedPropertyGlSettings = compareObjects( $objNewPropertyGlSetting, $objOldPropertyGlSetting, $arrstrPropertyGlSettingLogfields );

		if( false == valArr( $arrmixChangedPropertyGlSettings ) ) {
			return true;
		}

		$intUpdatePropertyGlSettingCounter = 0;
		$intDeletePropertyGlSettingCounter = 0;

		foreach( $arrmixChangedPropertyGlSettings as $arrstrChangedPropertyGlSettings ) {
			if( true == isset( $arrstrChangedPropertyGlSettings['new_value'] ) ) {
				$intUpdatePropertyGlSettingCounter++;
			} else {
				$intDeletePropertyGlSettingCounter++;
			}
		}

		$strPropertyGlSettingDiscription = '';

		if( 0 < $intUpdatePropertyGlSettingCounter && 0 == $intDeletePropertyGlSettingCounter ) {
			$strPropertyGlSettingDiscription = ( 1 == $intUpdatePropertyGlSettingCounter ) ? 'A property GL setting has been modified.' : 'Property GL settings have been modified.';
		}

		if( 0 < $intDeletePropertyGlSettingCounter && 0 == $intUpdatePropertyGlSettingCounter ) {
			$strPropertyGlSettingDiscription = ( 1 == $intDeletePropertyGlSettingCounter ) ? ' A property GL setting has been deleted.' : 'Property GL settings have been deleted.';
		}

		if( 0 < $intUpdatePropertyGlSettingCounter && 0 < $intDeletePropertyGlSettingCounter ) {
			$strPropertyGlSettingDiscription = ( 1 == $intUpdatePropertyGlSettingCounter && 1 == $intDeletePropertyGlSettingCounter ) ? 'A property GL setting has been modified and deleted.' : 'Property GL settings have been modified and deleted.';
		}

		$strOldPropertyGlSettingsKeyValuePair = $this->mapKeyValuePair( $objOldPropertyGlSetting, $arrobjBankAccounts, $arrmixGlAccounts );
		$strNewPropertyGlSettingsKeyValuePair = $this->mapKeyValuePair( $objNewPropertyGlSetting, $arrobjBankAccounts, $arrmixGlAccounts );

		if( true == valStr( $strPropertyGlSettingDiscription ) ) {
			$objTableLog = new CTableLog();
			return $objTableLog->insert( $intCompanyUserId, $objClientDatabase, 'property_gl_settings', $objNewPropertyGlSetting->getPropertyId(), 'UPDATE', $objNewPropertyGlSetting->getCid(), $strOldPropertyGlSettingsKeyValuePair, $strNewPropertyGlSettingsKeyValuePair, $strPropertyGlSettingDiscription );
		}
	}

	public function insertPeriods( $intCompanyUserId, $objDatabase ) {

		$strGreaterPostMonth = $this->getArPostMonth();

		if( strtotime( $this->getApPostMonth() ) > strtotime( $this->getArPostMonth() ) ) {
			$strGreaterPostMonth = $this->getApPostMonth();
		}

		CPeriod::insertPeriods( $this->getCid(), $this->getPropertyId(), $strGreaterPostMonth, $intCompanyUserId, $objDatabase );

		if( strtotime( $strGreaterPostMonth ) > strtotime( $this->getGlPostMonth() ) ) {
			CPeriod::insertPeriods( $this->getCid(), $this->getPropertyId(), $this->getGlPostMonth(), $intCompanyUserId, $objDatabase );
		}
	}
}