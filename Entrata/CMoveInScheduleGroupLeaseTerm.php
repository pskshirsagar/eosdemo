<?php

class CMoveInScheduleGroupLeaseTerm extends CBaseMoveInScheduleGroupLeaseTerm {

	public function valLeaseStartWindowId() {
		$boolIsValid = true;

		if( false == valId( $this->getLeaseStartWindowId() ) ) {
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'lease_start_window', __( 'Lease Start Window can\'t be Empty.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valLeaseStartWindowId();
				break;

			case VALIDATE_UPDATE:
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>