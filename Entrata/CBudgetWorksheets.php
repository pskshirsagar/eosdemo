<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetWorksheets
 * Do not add any new functions to this class.
 */

class CBudgetWorksheets extends CBaseBudgetWorksheets {

	public static function fetchBudgetWorksheetsDataByBudgetWorkBookIdByCid( $intBudgetWorkBookId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) && false == valId( $intBudgetWorkBookId ) ) return NULL;

		$strSql = ' SELECT
						bw.id,
						util_get_translated( \'name\', bw.name, bw.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS budget_worksheet_name,
						util_get_system_translated( \'name\', bwdst.name, bwdst.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS default_data_source_name
					FROM
						budget_worksheets bw
						JOIN budget_data_source_types bwdst ON ( bw.budget_data_source_type_id = bwdst.id )
					WHERE
						bw.cid = ' . ( int ) $intCid . ' 
						AND bw.budget_workbook_id = ' . ( int ) $intBudgetWorkBookId . '
						AND bw.deleted_by IS NULL
					ORDER BY
						bw.order_num,
						bw.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchBudgetWorksheetsByIdsByCid( $arrintIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintIds ) && false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
					    *
				   FROM
					    budget_worksheets
				   WHERE
						id IN( ' . implode( ',', $arrintIds ) . ' )
						AND cid = ' . ( int ) $intCid . ' AND deleted_by IS NULL';

		return parent::fetchBudgetWorksheets( $strSql, $objDatabase );
	}

	public static function fetchMaxOrderNumberByBudgetWorkbookIdByCid( $intBudgetWorkbookId, $intCid, $objDatabase ) {

		if( false == valId( $intBudgetWorkbookId ) ) return NULL;

		$strSql = 'SELECT
						max( order_num ) as order_num
					FROM
						budget_worksheets
					WHERE 
						cid = ' . ( int ) $intCid . '
						AND budget_workbook_id =  ' . ( int ) $intBudgetWorkbookId . ' AND deleted_by IS NULL';

		return parent::fetchcolumn( $strSql, 'order_num', $objDatabase );
	}

	public static function fetchBudgetWorksheetCountByBudgetWorkbookIdByCid( $intBudgetWorkbookId, $intCid, $objDatabase ) {
		if( false == valId( $intBudgetWorkbookId ) || false == valId( $intCid ) ) return NULL;

		$strWhere = 'WHERE
						cid = ' . ( int ) $intCid . '
						AND budget_workbook_id = ' . ( int ) $intBudgetWorkbookId . ' AND deleted_on IS NULL ';

		return parent::fetchBudgetWorksheetCount( $strWhere, $objDatabase );
	}

}
?>