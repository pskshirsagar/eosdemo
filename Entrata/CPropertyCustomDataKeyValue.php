<?php

class CPropertyCustomDataKeyValue extends CBasePropertyCustomDataKeyValue {

	public function valKey( $objDatabase ) {

		$boolIsValid = true;

		// Check if its valid string
		if( false == valStr( $this->m_strKey ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', 'Valid Key is required.' ) );
		}

		// Check for duplicate
		$strWhereSql = 'WHERE
							cid = ' . ( int ) $this->m_intCid . '
							AND property_id = ' . ( int ) $this->m_intPropertyId . ' 
							AND lower(key) = \'' . \Psi\CStringService::singleton()->strtolower( $this->m_strKey ) . '\'
							AND id <> ' . ( int ) $this->m_intId;

		if( 0 < CPropertyCustomDataKeyValues::fetchPropertyCustomDataKeyValueCount( $strWhereSql, $objDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', 'Key already exists.' ) );
		}

		return $boolIsValid;
	}

	public function valValue() {
		$boolIsValid = true;
		if( false == valStr( $this->m_strValue ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Valid Value is required.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, CDatabase $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valKey( $objDatabase );
				$boolIsValid &= $this->valValue();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>