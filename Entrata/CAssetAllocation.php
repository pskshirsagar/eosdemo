<?php

class CAssetAllocation extends CBaseAssetAllocation {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPeriodId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valGlTransactionTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valChargeAssetTransactionId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCreditAssetTransactionId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDebitGlAccountId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCreditGlAccountId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAssetAllocationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valQuantity() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitCost() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAlllocationDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllocationAmount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReportingPostDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPostDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPostMonth() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDeleted() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPosted() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>