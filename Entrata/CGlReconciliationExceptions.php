<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlReconciliationExceptions
 * Do not add any new functions to this class.
 */

class CGlReconciliationExceptions extends CBaseGlReconciliationExceptions {

	public static function fetchGlReconciliationExceptionCountByGlDetailIdsByCid( $arrintGlDetailIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintGlDetailIds ) ) {
			return NULL;
		}

		$strWhere = ' WHERE
						cid = ' . ( int ) $intCid . '
						AND gl_detail_id IN ( ' . implode( ',', $arrintGlDetailIds ) . ' )';

		return self::fetchRowCount( $strWhere, 'gl_reconciliation_exceptions', $objClientDatabase );
	}
}
?>