<?php

use  \Psi\Core\AccountsReceivables\ArAllocations\CLeaseAutoAllocationLibrary;
use  \Psi\Core\AccountsReceivables\DepositCredit\CDepositCreditsLibrary;
use Psi\Core\AccountsReceivables\Ledger\CLedgerLibrary;
use Psi\Core\AccountsReceivables\Ledger\CLedgerCriteria;
use Psi\Core\AccountsReceivables\Ledger\CSummarizedLedgerLibrary;
use Psi\Core\AccountsReceivables\Ledger\CNonSummarizedLedgerLibrary;

use Psi\Eos\Entrata\CUnitSpaces;

class CLease extends CBaseLease {

	const LEASE_TYPE_STANDARD		= 1;
	const LEASE_TYPE_OTHER_INCOME	= 2;
	const LEASE_TYPE_CORPORATE		= 3;

	const REMOTE_FOREIGN_KEY_DELIMITER		= '~..~';
	const TRANSFER_TYPE_UNIT_TRANSFER		= 1;
	const TRANSFER_TYPE_RENEWAL_TRANSFER	= 2;

	protected $m_intCustomerId;
	protected $m_intLeaseCustomerId;
	protected $m_intUnitTypeId;
	protected $m_intRentAmount;
	protected $m_intLeaseDetailId;
	protected $m_intApplicationId;
	protected $m_intTransferType;
	protected $m_intRenewalOfferId;
	protected $m_intLeaseProcessId;
	protected $m_intLeaseExpiryDays;
	protected $m_intTransferLeaseId;
	protected $m_intLateFeeFormulaId;
	protected $m_intIsTransferringIn;
	protected $m_intApplicationLeaseId;
	protected $m_intPrimaryApplicantId;
	protected $m_intLeaseIntervalTypeId;
	protected $m_intApplicationPriority;
	protected $m_intPropertyFloorPlanId;
	protected $m_intCalculatedDelayDays;
	protected $m_intIsEligibleForCollection;
	protected $m_intIsSyncApplicationAndLease;
	protected $m_intLeaseStartWindowId;
	protected $m_intTransferredFromLeaseId;
	protected $m_intSpaceConfigurationId;
	protected $m_intMakeReadyStatusId;
	protected $m_intLeaseIntervalId;
	protected $m_intCustomerTypeId;

	protected $m_fltBalance;
	protected $m_fltTransactionAmount;
	protected $m_fltWriteOffBalance;
	protected $m_fltDelinquencyAmount;
	protected $m_fltScheduledChargeTotal;
	protected $m_fltMiniminumPaymentAmount;
	protected $m_fltPreGraceDaysDelinquencyAmount;
	protected $m_fltUnappliedCreditsAndPaymentsBalance;
	protected $m_fltLeaseCurrentRepaymentAmount;

	protected $m_strPostMonth;
	protected $m_strUnitNumber;
	protected $m_strSpaceNumber;
	protected $m_strPhoneNumber;
	protected $m_strMoveOutType;
	protected $m_strCustomerCity;
	protected $m_strBuildingName;
	protected $m_strPropertyName;
	protected $m_strTransferredOn;
	protected $m_strUnitSpaceName;
	protected $m_strCustomerNameFull;
	protected $m_strPropertyUnitName;
	protected $m_strRenewalStartDate;
	protected $m_strPendingFmoDueDate;
	protected $m_strInsurancePolicyId;
	protected $m_strCustomerStateCode;
	protected $m_strCustomerPostalCode;
	protected $m_strCustomerStreetLine1;
	protected $m_strCustomerStreetLine2;
	protected $m_strCustomerStreetLine3;
	protected $m_strCustomerCountryCode;
	protected $m_strLeaseStatusTypeName;
	protected $m_strCustomerEmailAddress;
	protected $m_strCustomerMobileNumber;
	protected $m_strInsurancePolicyLeaseId;
	protected $m_strLeasingTierSpecialIds;
	protected $m_strCompanyName;
	protected $m_strGender;
	protected $m_strUnitTypeName;
	protected $m_strPropertyFloorPlanName;
	protected $m_strRenewalStatus;

	protected $m_objProperty;
	protected $m_objUnitSpace;
	protected $m_objLeaseDetail;
	protected $m_objPropertyUnit;
	protected $m_objLeaseProcess;
	protected $m_objLeaseCustomer;
	protected $m_objPropertyGlSetting;
	protected $m_objPropertyChargeSetting;
	// @FIXME temporarily making this variable as Public and after refactoring will mark it as protected.
	public $m_objApplication;
	/** @var CLeaseInterval */
	protected $m_objActiveLeaseInterval;
	protected $m_objPrimaryCustomer;
	protected $m_objPrimaryLeaseCustomer;
	protected $m_objRepayment;
	protected $m_objPropertyOccupancyTypeSetting;

	protected $m_boolIsRenewed;
	protected $m_boolIsSoftLocked;
	protected $m_boolIsLeadPayment;
	protected $m_boolIsRenewalTransfer;
	protected $m_boolIsAddResidentProcess;
	protected $m_boolActivateStandardPosting;
	protected $m_boolHasLeadManagementProduct;
	protected $m_boolIsPaymentEligibleLease;
	protected $m_boolIsStudentSemesterSelectionEnabled;
	protected $m_boolIsDebitEnabled;
	protected $m_boolIsVisaPilot;
	protected $m_boolIsImmediateMoveIn;
	protected $m_boolIsRuleStopUpdated;
	protected $m_boolIsSyncSpaceConfigurationIdToApplication;

	protected $m_arrobjFiles;
	protected $m_arrobjDetails;
	protected $m_arrobjCustomers;
	protected $m_arrstrEventLogs;
	protected $m_arrobjSystemEmails;
	protected $m_arrobjArAllocations;
	protected $m_arrobjArTransactions;
	protected $m_arrobjLeaseIntervals;
	protected $m_arrobjLeaseCustomers;
	protected $m_arrobjScheduledCharges;
	protected $m_arrobjMerchantAccounts;
	protected $m_arrobjCheckAccountTypes;
	protected $m_arrobjFailRoutedDetails;
	protected $m_arrobjBaseRentLeaseTerms;

	protected $m_arrintLeaseUnitSpaceIds;

	protected $m_arrmixRequiredParameters;

	public function __construct() {
		parent::__construct();

		$this->m_arrobjMerchantAccounts 	= [];

		$this->m_arrintLeaseUnitSpaceIds	= [];

		$this->m_fltBalance 				= 0;
		$this->m_fltWriteOffBalance 		= 0;
		$this->m_fltMiniminumPaymentAmount 	= 0;

		$this->m_intIsMonthToMonth 			= '0';
		$this->m_intReturnedPaymentsCount  		= '0';
		$this->m_intDontChargeLateFees 		= '0';
		$this->m_intOccupancyTypeId 		= COccupancyType::CONVENTIONAL;

		$this->m_intIsSyncApplicationAndLease	= true;

		return;
	}

	/**
	 * Add Functions
	 *
	 */

	public function addCustomer( $objCustomer ) {
		$this->m_arrobjCustomers[$objCustomer->getId()] = $objCustomer;
	}

	public function addLeaseCustomer( $objLeaseCustomer ) {
		$this->m_arrobjLeaseCustomers[$objLeaseCustomer->getId()] = $objLeaseCustomer;
	}

	public function addFile( $objFile ) {
		$this->m_arrobjFiles[$objFile->getId()] = $objFile;
	}

	public function addArTransaction( $objArTransaction ) {
		$this->m_arrobjArTransactions[$objArTransaction->getId()] = $objArTransaction;
	}

	public function addScheduledCharge( $objScheduledCharges ) {
		$this->m_arrobjScheduledCharges[$objScheduledCharges->getId()] = $objScheduledCharges;
	}

	/**
	 * Get or Fetch Functions
	 *
	 */

	public function getRequiredParameters() {
		return $this->m_arrmixRequiredParameters;
	}

	public function getOrFetchPostMonth( $objDatabase ) {

		if( true == is_null( $this->getPostMonth() ) ) {
			$objPropertyGlSetting = CPropertyGlSettings::fetchCustomPropertyGlSettingsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );

			if( false == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
				trigger_error( 'Property GL Setting could not be loaded.' );
				exit;
			}

			$this->setPostMonth( $objPropertyGlSetting->getArPostMonth() );
		}

		return $this->m_strPostMonth;
	}

	public function getOrFetchProperty( $objDatabase ) {
		if( true == valObj( $this->m_objProperty, 'CProperty' ) ) return $this->m_objProperty;

		$this->m_objProperty = $this->fetchProperty( $objDatabase );

		return $this->m_objProperty;
	}

	public function getOrFetchPropertyUnit( $objDatabase ) {
		if( true == valObj( $this->m_objPropertyUnit, 'CPropertyUnit' ) ) return $this->m_objPropertyUnit;

		$this->m_objPropertyUnit = \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPropertyUnitByIdByCid( $this->getPropertyUnitId(), $this->getCid(), $objDatabase );

		return $this->m_objPropertyUnit;
	}

	public function getOrFetchUnitSpace( $objDatabase ) {
		if( true == valObj( $this->m_objUnitSpace, 'CUnitSpace' ) ) return $this->m_objUnitSpace;

		$objDatabase->checkAndReOpenBrokenConnection();

		if( 0 < ( int ) $this->m_intUnitSpaceId ) {
			$this->m_objUnitSpace = CUnitSpaces::createService()->fetchCustomUnitSpaceByIdByCid( $this->m_intUnitSpaceId, $this->getCid(), $objDatabase );
			return $this->m_objUnitSpace;
		}
		return NULL;
	}

	public function getOrFetchLeaseCustomer( $intCustomerId, $objDatabase ) {
		if( true == is_null( $this->m_objLeaseCustomer ) ) {
			$this->m_objLeaseCustomer = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomerByCustomerIdByLeaseIdByCid( $intCustomerId, $this->m_intId, $this->getCid(), $objDatabase );
		}
		return $this->m_objLeaseCustomer;
	}

	public function getOrfetchLeaseProcess( $objDatabase = NULL ) {
		if( true == valObj( $this->m_objLeaseProcess, 'CLeaseProcess' ) ) return $this->m_objLeaseProcess;

        if( true == is_null( $objDatabase ) ) {
            $objDatabase = $this->m_objDatabase;
            if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
        }

		$this->m_objLeaseProcess = \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchLeaseProcessByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		return $this->m_objLeaseProcess;
	}

	public function getOrFetchPrimaryLeaseCustomer( $objDatabase ) {
		if( true == is_null( $this->m_objPrimaryLeaseCustomer ) ) {
			$this->m_objPrimaryLeaseCustomer = $this->fetchPrimaryLeaseCustomer( $objDatabase );
		}
		return $this->m_objPrimaryLeaseCustomer;
	}

	public function getOrFetchLeaseCustomers( $objDatabase ) {
		if( false == valArr( $this->m_arrobjLeaseCustomers ) ) {
			$this->m_arrobjLeaseCustomers = ( array ) $this->fetchLeaseCustomers( $objDatabase );
		}
		return $this->m_arrobjLeaseCustomers;
	}

	public function getOrFetchPropertyGlSetting( $objDatabase ) {
		if( true == valObj( $this->m_objPropertyGlSetting, 'CPropertyGlSetting' ) ) return $this->m_objPropertyGlSetting;

		$this->m_objPropertyGlSetting = $this->fetchPropertyGlSetting( $objDatabase );

		return $this->m_objPropertyGlSetting;
	}

	public function getOrFetchPropertyChargeSetting( $objDatabase ) {
		if( true == valObj( $this->m_objPropertyChargeSetting, 'CPropertyChargeSetting' ) ) return $this->m_objPropertyChargeSetting;

		$this->m_objPropertyChargeSetting = $this->fetchPropertyChargeSetting( $objDatabase );

		return $this->m_objPropertyChargeSetting;
	}

	public function getOrFetchApplication( $objDatabase ) {
		if( true == is_null( $this->m_objApplication ) ) {
			$this->m_objApplication = $this->fetchApplication( $objDatabase );
		}

		return $this->m_objApplication;
	}

	public function getOrFetchLeaseIntervals( $objDatabase ) {
		if( true == valArr( $this->m_arrobjLeaseIntervals ) ) return $this->m_arrobjLeaseIntervals;

		$this->m_arrobjLeaseIntervals = ( array ) $this->fetchLeaseIntervals( $objDatabase );

		return $this->m_arrobjLeaseIntervals;
	}

	public function getOrFetchActiveLeaseInterval( $objDatabase ) {

		if( true == valObj( $this->getActiveLeaseInterval(), 'CLeaseInterval' ) ) return $this->getActiveLeaseInterval();

		$strSql = 'SELECT
						li.*
					FROM
						lease_intervals li
						JOIN leases l ON ( li.cid = l.cid AND li.lease_id = l.id AND li.id = l.active_lease_interval_id )
					WHERE
						li.cid = ' . ( int ) $this->getCid() . '
						AND li.lease_id = ' . ( int ) $this->getId();

		$objLeaseInterval = \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLeaseInterval( $strSql, $objDatabase );

		if( true == valObj( $objLeaseInterval, 'CLeaseInterval' ) ) {
			$this->setActiveLeaseInterval( $objLeaseInterval );
			return $this->getActiveLeaseInterval();
		}

		return NULL;
	}

	public function getOrFetchLeaseDetail( $objDatabase ) {

		if( true == valObj( $this->getLeaseDetail(), 'CLeaseDetail' ) ) return $this->getLeaseDetail();

		$objLeaseDetail = \Psi\Eos\Entrata\CLeaseDetails::createService()->fetchLeaseDetailByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		if( true == valObj( $objLeaseDetail, 'CLeaseDetail' ) ) {
			$this->setLeaseDetail( $objLeaseDetail );
			return $this->getLeaseDetail();
		}

		return NULL;
	}

	public function getOrFetchRentAmount( $objDatabase ) {

		$intTotalRentAmountForCurrentInterval = $this->fetchSimpleRentAmountForCurrentInterval( $objDatabase );

		if( false == is_null( $intTotalRentAmountForCurrentInterval ) ) {

			$this->setRentAmount( $intTotalRentAmountForCurrentInterval );
			return $this->getRentAmount();
		}

		return NULL;
	}

	public function getOrFetchBaseRentLeaseTerms( $objDatabase, $objRentArCode = NULL, $boolAllowLeaseExpirationLimitPermission = false, $intLeaseStartWindowId = NULL ) {

		if( false == valArr( $this->getBaseRentLeaseTerms() ) ) {
			$arrobjBaseRentLeaseTerms = CRenewalRates::fetchBaseRentForLeaseTermsByLease( $this, $objDatabase, $objRentArCode, $boolAllowLeaseExpirationLimitPermission, $boolPropertyRatesOnly = false, $intLeaseStartWindowId );
			$this->setBaseRentLeaseTerms( $arrobjBaseRentLeaseTerms );
		}

		return $this->getBaseRentLeaseTerms();
	}

	public function getOrFetchPrimaryCustomer( $objDatabase ) {
		if( true == valObj( $this->getPrimaryCustomer(), 'CCustomer' ) ) return $this->getPrimaryCustomer();
        if( false == valObj( $objDatabase, 'CDatabase' ) ) $objDatabase = $this->m_objDatabase;
        if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;

		$strSql = 'SELECT
						c.*
					FROM
						customers c
						JOIN leases l ON ( c.cid = l.cid AND c.id = l.primary_customer_id )
					WHERE
						l.cid = ' . ( int ) $this->getCid() . '
						AND l.id = ' . ( int ) $this->getId();

		$objPrimaryCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomer( $strSql, $objDatabase );

		if( true == valObj( $objPrimaryCustomer, 'CCustomer' ) ) {
			$this->setPrimaryCustomer( $objPrimaryCustomer );
			return $this->getPrimaryCustomer();
		}

		return NULL;
	}

	public function getOrFetchPropertyOccupancyTypeSetting( CDatabase $objDatabase ) {
		if( false == valObj( $this->m_objPropertyOccupancyTypeSetting, 'CPropertyOccupancyTypeSetting' ) ) {
			$this->m_objPropertyOccupancyTypeSetting = $this->fetchPropertyOccupancyTypeSetting( $objDatabase );
		}

		return $this->m_objPropertyOccupancyTypeSetting;
	}

	public function getIsPaymentAllowedForEvicting() {

		if( CListType::EVICTION == $this->getTerminationListTypeId()
			&& true == in_array( $this->getLeaseStatusTypeId(), CLeaseStatusType::$c_arrintActiveLeaseStatusTypes )
			&& CPaymentAllowanceType::BLOCK_ALL == $this->getPaymentAllowanceTypeId() ) {
			return false;
		}
		return true;
	}

	public function getIsSemesterSelectionEnabled( $objDatabase ) {

		if( true == \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferenceValueByPropertyPreferenceKeyByPropertyIdByCid( 'ENABLE_SEMESTER_SELECTION', $this->getPropertyId(), $this->getCid(), $objDatabase ) ) {
			return true;
		}

		return false;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createAncillaryCustomerOrder( $intCustomerId = NULL ) {

		$objAncillaryCustomerOrder = new CAncillaryCustomerOrder();
		$objAncillaryCustomerOrder->setCid( $this->getCid() );
		$objAncillaryCustomerOrder->setLeaseId( $this->getId() );
		$objAncillaryCustomerOrder->setCustomerId( $intCustomerId );

		return $objAncillaryCustomerOrder;
	}

	public function createLeaseCustomer( $intCustomerId = NULL ) {

		$objLeaseCustomer = new CLeaseCustomer();
		$objLeaseCustomer->setCid( $this->getCid() );
		$objLeaseCustomer->setPropertyId( $this->getPropertyId() );
		$objLeaseCustomer->setLeaseId( $this->getId() );
		$objLeaseCustomer->setCustomerId( $intCustomerId );

		return $objLeaseCustomer;
	}

	public function createFileMergeValue() {
		$objFileMergeValue = new CFileMergeValue();
		$objFileMergeValue->setCid( $this->m_intCid );
		$objFileMergeValue->setLeaseId( $this->m_intId );

		return $objFileMergeValue;
	}

	public function createArTransaction( $intArCodeId = NULL, $objDatabase = NULL ) {

		$objArTransaction = new CArTransaction();
		$objArTransaction->setCid( $this->getCid() );
		$objArTransaction->setPropertyId( $this->getPropertyId() );
		$objArTransaction->setLeaseId( $this->getId() );
		$objArTransaction->setLeaseIntervalId( $this->getActiveLeaseIntervalId() );
		$objArTransaction->setArCodeId( $intArCodeId );
		$objArTransaction->setPostDate( date( 'm/d/Y', time() ) );

		if( true == isset( $objDatabase ) ) {
			$objArTransaction->getOrFetchPostMonth( $objDatabase );
		}

		return $objArTransaction;
	}

	public function createScheduledCharge() {

		$objScheduledCharge = new CScheduledCharge();
		$objScheduledCharge->setCid( $this->getCid() );
		$objScheduledCharge->setLeaseId( $this->getId() );
		$objScheduledCharge->setPropertyId( $this->getPropertyId() );
		$objScheduledCharge->setArTriggerId( CArTrigger::MONTHLY );
		$objScheduledCharge->setLeaseIntervalId( $this->getActiveLeaseIntervalId() );

		return $objScheduledCharge;
	}

	public function createRenewalScheduledCharge( $objArCode = NULL ) {

		$objScheduledCharge = new CScheduledCharge();
		$objScheduledCharge->setCid( $this->getCid() );
		$objScheduledCharge->setPropertyId( $this->getPropertyId() );
		$objScheduledCharge->setLeaseId( $this->getId() );
		$objScheduledCharge->setScheduledChargeTypeId( CScheduledChargeType::RENEWAL );
		$objScheduledCharge->setScheduledChargeLogTypeId( CScheduledChargeLogType::APPLICATION_RENEWAL_UPDATE );

		$objScheduledCharge->setArTriggerId( CArTrigger::MONTHLY );
		$objScheduledCharge->setArFormulaId( CArFormula::FIXED_AMOUNT );
		$objScheduledCharge->setArCascadeId( CArCascade::PROPERTY );
		$objScheduledCharge->setArCascadeReferenceId( $this->getPropertyId() );

		if( false == valObj( $objArCode, 'CArCode' ) ) return $objScheduledCharge;

		$objScheduledCharge->setArCodeId( $objArCode->getId() );
		$objScheduledCharge->setArCodeTypeId( $objArCode->getArCodeTypeId() );
		$objScheduledCharge->setArOriginId( $objArCode->getArOriginId() );
		$objScheduledCharge->setArTriggerId( $objArCode->getArTriggerId() );

		return $objScheduledCharge;
	}

	public function createExtensionScheduledCharge() {

		$objScheduledCharge = new CScheduledCharge();
		$objScheduledCharge->setCid( $this->getCid() );
		$objScheduledCharge->setPropertyId( $this->getPropertyId() );
		$objScheduledCharge->setLeaseId( $this->getId() );
		$objScheduledCharge->setScheduledChargeTypeId( CScheduledChargeType::EXTENSION );
		$objScheduledCharge->setScheduledChargeLogTypeId( CScheduledChargeLogType::EXTENSION_APPROVED );
		$objScheduledCharge->setArFormulaId( CArFormula::FIXED_AMOUNT );

		return $objScheduledCharge;
	}

	/**
	 *
	 * @return CArPayment
	 */

	public function createArPayment( $intPsProductId, $intPaymentMediumId, $intApplicantApplicationId, $arrintPsProductIds, $objCustomer, $objDatabase, $boolIsOverrideCertifiedPayments = false ) {
		$objArPayment = new CArPayment();
		$objArPayment->setDefaults();
		$objArPayment->setCid( $this->getCid() );
		$objArPayment->setPropertyId( $this->getPropertyId() );
		$objArPayment->setCustomerId( $objCustomer->getId() );
		$objArPayment->setLeaseId( $this->m_intId );
		$objArPayment->setPaymentMediumId( $intPaymentMediumId );
		$objArPayment->setPsProductId( $intPsProductId );
		$objArPayment->setPaymentDatetime( date( 'm/d/Y H:i:s' ) );

		$objArPayment->setProperty( $this->getOrFetchProperty( $objDatabase ) );
		$objArPayment->setPropertyGlSetting( $this->getOrFetchPropertyGlSetting( $objDatabase ) );
		$objArPayment->getOrFetchPostMonth( $objDatabase );
		$objArPayment->setLease( $this );

		if( CPsProduct::ENTRATA == $intPsProductId ) {
			$objArPayment->setIsEntrataPayment( true );
		}

		$objArPayment->fetchDefaultMerchantAccount( $objDatabase, $boolSetCompanyMerchantAccountId = true );

		if( true == valObj( $objArPayment->getMerchantAccount(), 'CMerchantAccount' ) ) {
			$objArPayment->setCurrencyCode( $objArPayment->getMerchantAccount()->getCurrencyCode() );
		} else {
			$objClient = $objArPayment->getOrFetchClient( $objDatabase );
			$objArPayment->setCurrencyCode( true == valStr( $objClient->getCurrencyCode() ) ? $objClient->getCurrencyCode() : CCurrency::CURRENCY_CODE_USD );
		}
		$objArPayment->fetchCheckAccountTypes( $objDatabase );
		$objArPayment->setPsProductIds( $arrintPsProductIds );

		$objArPayment->fetchPaymentTypes( $objDatabase, $boolIsOverrideCertifiedPayments );

		$objArPayment->setApplicantApplicationId( $intApplicantApplicationId );

		$this->setArPaymentBillingInfo( $objArPayment, $objCustomer, $objDatabase );

		if( true == valObj( $objCustomer, 'CCustomer' ) ) {
			$objArPayment->setCustomer( $objCustomer );

			$objArPayment->setBilltoNameFirst( $objCustomer->getNameFirst() );
			$objArPayment->setBilltoNameLast( $objCustomer->getNameLast() );
			$objArPayment->setBilltoCompanyName( $objCustomer->getCompanyName() );

			if( true == valArr( $objArPayment->getPaymentTypes() ) ) {
				$objCustomer->fetchCustomerPaymentAccountsByPaymentTypeIds( array_keys( $objArPayment->getPaymentTypes() ), $objDatabase );
			}
		}
		return $objArPayment;
	}

	/**
	 * // I created this new function for creating payments in the new resident portal.  Hopefully we can use this in
	 * res port, prospect portal, and resident
	 * // works at some point and get rid of the other function.  This function doesn't require a payment database
	 * function.
	 *
	 * @param integer           $intPsProductId
	 * @param integer           $intPaymentMediumId
	 * @param string[]          $arrstrRequestData
	 * @param string[]          $arrstrRequestForm
	 * @param integer[]         $arrintPsProductIds
	 * @param \CMerchantAccount $objMerchantAccount
	 * @param \CCustomer        $objCustomer
	 * @param \CDatabase        $objClientDatabase
	 * @param null|\CArPayment  $objArPayment
	 *
	 * @return \CArPayment|null
	 */
	public function createSimpleArPayment( $intPsProductId, $intPaymentMediumId, $arrstrRequestData, $arrstrRequestForm, $arrintPsProductIds, $objMerchantAccount, $objCustomer, $objClientDatabase, $objArPayment = NULL, $boolIsResPortalResponsive = false ) {

		if( false == valObj( $objArPayment, 'CArPayment' ) ) {
			$objArPayment = new CArPayment();
			$objArPayment->setArTransactions( $this->getArTransactions() );
		}
		$objArPayment->setDefaults();

		$objArPayment->setCid( $this->getCid() );
		$objArPayment->setLeaseId( $this->m_intId );
		$objArPayment->setPropertyId( $this->getPropertyId() );
		$objArPayment->setCustomerId( $objCustomer->getId() );
		$objArPayment->setPaymentDatetime( date( 'm/d/Y H:i:s' ) );
		$objArPayment->setPsProductId( $intPsProductId );
		$objArPayment->setPaymentMediumId( $intPaymentMediumId );
		$objArPayment->setProperty( $this->getOrFetchProperty( $objClientDatabase ) );
		$objArPayment->setPropertyGlSetting( $this->getOrFetchPropertyGlSetting( $objClientDatabase ) );
		$objArPayment->getOrFetchPostMonth( $objClientDatabase );
		$objArPayment->setLease( $this );
		$objArPayment->setPsProductIds( $arrintPsProductIds ); // Used to decide if we need non-electronic payment types in Entrata
		if( CPsProduct::RESIDENT_PORTAL == $intPsProductId ) $objArPayment->setIsResidentPortalPayment( true );

		$objArPayment->setCustomer( $objCustomer );

		$objArPayment->setBilltoNameFirst( $objCustomer->getNameFirst() );
		$objArPayment->setBilltoNameLast( $objCustomer->getNameLast() );
		$objArPayment->setBilltoCompanyName( $objCustomer->getCompanyName() );

		$objCustomer->getOrFetchCustomerPaymentAccounts( $objClientDatabase );

		$objArPayment->setTotalAmount( $objArPayment->getPaymentAmount() );
		$objCustomerPortalSetting = $objCustomer->fetchCustomerPortalSetting( $objClientDatabase ); // TODO : This may have already set on Customer Object. We need to check that.
		if( true == valObj( $objCustomerPortalSetting, 'CCustomerPortalSetting' ) ) {
			$objArPayment->setBilltoEmailAddress( $objCustomerPortalSetting->getUsername() );
		}

		if( true == valObj( $objCustomer, 'CCustomer' ) && true == is_null( $objArPayment->getBilltoEmailAddress() ) ) {
			$objArPayment->setBilltoEmailAddress( $objCustomer->getEmailAddress() );
		}

		if( true == $this->getIsLeadPayment() ) {
			$objArPayment->setIsLeadPayment( true );
		}

		if( false == is_null( $arrstrRequestData ) && false == is_null( $arrstrRequestForm ) ) {
			$objArPayment->applyRequestForm( $arrstrRequestData, $arrstrRequestForm );
		}
		if( \CPsProductOption::WHITE_LABEL_MOBILE_APP == $objArPayment->getPsProductOptionId() || \CPsProductOption::RESIDENT_PORTAL_APP_ANDROID == $objArPayment->getPsProductOptionId() || \CPsProductOption::RESIDENT_PORTAL_APP_IOS == $objArPayment->getPsProductOptionId() ) {
			$objArPayment->setSkipCommercialInternationalFeeFlow( true );
		}
		if( true == valStr( $this->getUnitNumberCache() ) ) {
			$objArPayment->setBilltoUnitNumber( $this->getUnitNumberCache() );
		}
		if( true == $boolIsResPortalResponsive ) {
			$this->setArPaymentBillingInfoForResPortalResponsive( $objArPayment, $objCustomer, $objClientDatabase );
		} else {
			$this->setArPaymentBillingInfo( $objArPayment, $objCustomer, $objClientDatabase );
		}
		$objArPayment->loadCustomerPaymentAccount( $objClientDatabase );

		// Here we have to try to load the merchant account (if not already sent into the function)
		if( false == valObj( $objMerchantAccount, 'CMerchantAccount' ) && false == valArr( $this->m_arrobjMerchantAccounts ) ) {
			$this->m_arrobjMerchantAccounts = $this->m_objProperty->fetchMerchantAccounts( $objClientDatabase );
		}

		if( false == valObj( $objMerchantAccount, 'CMerchantAccount' ) && true == valArr( $this->m_arrobjMerchantAccounts ) ) {
			foreach( $this->m_arrobjMerchantAccounts as $objCurrentMerchantAccount ) {

				if( true == is_numeric( $objArPayment->getCompanyMerchantAccountId() ) && $objArPayment->getCompanyMerchantAccountId() == $objCurrentMerchantAccount->getId() ) {
					$objMerchantAccount = $objCurrentMerchantAccount;
					break;

				} elseif( false == is_numeric( $objArPayment->getCompanyMerchantAccountId() ) && false == is_numeric( $objCurrentMerchantAccount->getArCodeId() ) ) {
					$objMerchantAccount = $objCurrentMerchantAccount;
					break;
				}
			}
		}

		if( CPaymentType::PAD == $objArPayment->getPaymentTypeId() && !valStr( $objArPayment->getCheckRoutingNumber() ) && array_key_exists( 'check_bank_number', $arrstrRequestData ) ) {
			$objArPayment->setCheckRoutingNumber( '0' . $arrstrRequestData['check_bank_number'] . $arrstrRequestData['check_bank_transit_number'] );
		}
		if( true == valObj( $objMerchantAccount, 'CMerchantAccount' ) ) {
			$objArPayment->setCurrencyCode( $objMerchantAccount->getCurrencyCode() );
			$objArPayment->setMerchantAccount( $objMerchantAccount );
			$objArPayment->setCompanyMerchantAccountId( $objMerchantAccount->getId() );
			$objMerchantAccount->setArPayment( $objArPayment );
		} else {
			$objClient = $objArPayment->getOrFetchClient( $objClientDatabase );
			$objArPayment->setCurrencyCode( true == valStr( $objClient->getCurrencyCode() ) ? $objClient->getCurrencyCode() : CCurrency::CURRENCY_CODE_USD );
		}
		$objArPayment->getOrFetchPaymentTypes( $objClientDatabase, false, $arrstrRequestData['consumer'] );

		return $objArPayment;
	}

	public function setArPaymentBillingInfo( $objArPayment, $objCustomer, $objClientDatabase ) {

		// load the unit or property address - We should not take billing address from customer's record
		$objUnitOrPropertyAddress = \Psi\Eos\Entrata\CUnitAddresses::createService()->fetchUnitPrimaryAddressByPropertyUnitIdByCid( $this->getPropertyUnitId(), $this->getCid(), $objClientDatabase );

		if( false == valObj( $objUnitOrPropertyAddress, 'CUnitAddress' ) || ( ( true == valObj( $objUnitOrPropertyAddress, 'CUnitAddress' ) && ( false == valStr( $objUnitOrPropertyAddress->getStreetLine1() ) || false == valStr( $objUnitOrPropertyAddress->getCity() ) ) ) ) ) {
			$objUnitOrPropertyAddress = \Psi\Eos\Entrata\CPropertyAddresses::createService()->fetchPropertyAddressByPropertyIdByAddressTypeIdByCid( $this->getPropertyId(), ( array ) CAddressType::PRIMARY, $this->getCid(), $objClientDatabase );
		}
		if( true == valObj( $objCustomer, 'CCustomer' ) ) {
			$objPrimaryCustomerAddress = \Psi\Eos\Entrata\CCustomerAddresses::createService()->fetchCustomerAddressByCustomerIdByAddressTypeIdByCid( $objCustomer->getId(), CAddressType::PRIMARY, $this->getCid(), $objClientDatabase );
		}

		if( true == valObj( $objPrimaryCustomerAddress, 'CCustomerAddress' ) ) {
			$objArPayment->setBilltoStreetLine1( $objPrimaryCustomerAddress->getStreetLine1() );
			$objArPayment->setBilltoStreetLine2( $objPrimaryCustomerAddress->getStreetLine2() );
			$objArPayment->setBilltoStreetLine3( $objPrimaryCustomerAddress->getStreetLine3() );
			$objArPayment->setBilltoCity( $objPrimaryCustomerAddress->getCity() );
			$objArPayment->setBilltoCountryCode( $objPrimaryCustomerAddress->getCountryCode() );
			$objArPayment->setBilltoStateCode( $objPrimaryCustomerAddress->getStateCode() );
			$objArPayment->setBilltoProvince( $objPrimaryCustomerAddress->getProvince() );
			if( true == is_null( $objArPayment->getBilltoPostalCode() ) && false == is_null( $objPrimaryCustomerAddress->getPostalCode() ) ) {
				$objArPayment->setBilltoPostalCode( $objPrimaryCustomerAddress->getPostalCode() );
			}
		} elseif( true == valObj( $objUnitOrPropertyAddress, 'CUnitAddress' ) || true == valObj( $objUnitOrPropertyAddress, 'CPropertyAddress' ) ) {
			$objArPayment->setBilltoStreetLine1( $objUnitOrPropertyAddress->getStreetLine1() );
			$objArPayment->setBilltoStreetLine2( $objUnitOrPropertyAddress->getStreetLine2() );
			$objArPayment->setBilltoStreetLine3( $objUnitOrPropertyAddress->getStreetLine3() );
			$objArPayment->setBilltoCity( $objUnitOrPropertyAddress->getCity() );
			$objArPayment->setBilltoStateCode( $objUnitOrPropertyAddress->getStateCode() );
			$objArPayment->setBilltoCountryCode( $objUnitOrPropertyAddress->getCountryCode() );
			if( true == is_null( $objArPayment->getBilltoPostalCode() ) && false == is_null( $objUnitOrPropertyAddress->getPostalCode() ) ) {
				$objArPayment->setBilltoPostalCode( $objUnitOrPropertyAddress->getPostalCode() );
			}
		} elseif( false == valObj( $objUnitOrPropertyAddress, 'CUnitAddress' ) && false == valObj( $objUnitOrPropertyAddress, 'CPropertyAddress' ) && true == valObj( $objCustomer, 'CCustomer' ) ) {
			$objArPayment->setBilltoStreetLine1( $objCustomer->getPrimaryStreetLine1() );
			$objArPayment->setBilltoStreetLine2( $objCustomer->getPrimaryStreetLine2() );
			$objArPayment->setBilltoStreetLine3( $objCustomer->getPrimaryStreetLine3() );
			$objArPayment->setBilltoCity( $objCustomer->getPrimaryCity() );
			$objArPayment->setBilltoStateCode( $objCustomer->getPrimaryStateCode() );
			if( true == is_null( $objArPayment->getBilltoPostalCode() ) && false == is_null( $objCustomer->getPrimaryPostalCode() ) ) {
				$objArPayment->setBilltoPostalCode( $objCustomer->getPrimaryPostalCode() );
			}
		}

	}

	public function createPaymentArTransaction( $objArPayment, $objDatabase, $objArCode = NULL ) {

		$objArTransaction = new CArTransaction();
		$objArTransaction->setCid( $this->getCid() );
		$objArTransaction->setPropertyId( $this->getPropertyId() );
		$objArTransaction->setLeaseId( $this->getId() );
		$objArTransaction->setLeaseIntervalId( $this->getActiveLeaseIntervalId() );
		$objArTransaction->setArCodeTypeId( CArCodeType::PAYMENT );
		$objArTransaction->setArOriginId( CArOrigin::BASE );
		$objArTransaction->setArTriggerId( CArTrigger::UNKNOWN );
		$objArTransaction->setRepaymentId( $objArPayment->getRepaymentId() );

		if( true == is_null( $objArPayment->getPaymentArCodeId() ) ) {
			if( false == valObj( $objArCode, 'CArCode' ) ) {
				$objClient = $objArPayment->getOrFetchClient( $objDatabase );
				$objArCode = $objClient->fetchArCodeByDefaultArCodeId( CDefaultArCode::PAYMENT, $objDatabase );
			}
			$objArTransaction->setArCodeId( $objArCode->getId() );
		} else {
			$objArTransaction->setArCodeId( $objArPayment->getPaymentArCodeId() );
		}

		$objArTransaction->setArPaymentId( $objArPayment->getId() );
		$objArTransaction->setPostDate( ( ( false == is_null( $objArPayment->getTransactionDatetime() ) ) ? $objArPayment->getTransactionDatetime() : $objArPayment->getPaymentDatetime() ) );
		$objArTransaction->setPostMonth( $objArPayment->getOrFetchPostMonth( $objDatabase ) );
		$objArTransaction->setPaymentAmount( $objArPayment->getPaymentAmount() );
		$objArTransaction->setTransactionAmountDue( -1 * $objArPayment->getPaymentAmount() );
		$objArTransaction->setUseUnallocatedAmount( $objArPayment->getUseUnallocatedAmount() );
		$objArTransaction->setMemo( ( ( false == is_null( $objArPayment->getPostMemo() ) ) ? $objArPayment->getPostMemo() : $objArPayment->getPaymentMemo() ) );
		$objArTransaction->setInternalMemo( $objArPayment->getPaymentInternalMemo() );

		return $objArTransaction;
	}

	public function createApplication() {

		$objApplication = new CApplication();
		$objApplication->setCid( $this->getCid() );
		$objApplication->setPropertyId( $this->getPropertyId() );
		$objApplication->setPropertyUnitId( $this->getPropertyUnitId() );
		$objApplication->setUnitSpaceId( $this->getUnitSpaceId() );
		$objApplication->setLateFeeFormulaId( $this->getLateFeeFormulaId() );
		$objApplication->setLeaseTypeId( $this->getLeaseTypeId() );
		$objApplication->setLeaseId( $this->getId() );
		$objApplication->setOccupancyTypeId( $this->getOccupancyTypeId() );
		$objApplication->setApplicationDatetime( 'NOW()' );

		return $objApplication;
	}

	public function createFileAssociation() {

		$objFileAssociation = new CFileAssociation();

		$objFileAssociation->setCid( $this->getCid() );
		$objFileAssociation->setLeaseId( $this->getId() );

		return $objFileAssociation;
	}

	public function createLeaseProcess() {

		$objLeaseProcess = new CLeaseProcess();
		$objLeaseProcess->setCid( $this->getCid() );
		$objLeaseProcess->setLeaseId( $this->getId() );

		return $objLeaseProcess;
	}

	public function createLeaseAssociation( $intArOriginId, $intArOriginReferenceId, $intArCascadeId, $intArCascadeReferenceId ) {

		$objLeaseAssociation = new CLeaseAssociation();
		$objLeaseAssociation->setCid( $this->getCid() );
		$objLeaseAssociation->setPropertyId( $this->getPropertyId() );
		$objLeaseAssociation->setLeaseId( $this->getId() );
		$objLeaseAssociation->setArOriginId( $intArOriginId );
		$objLeaseAssociation->setArOriginReferenceId( $intArOriginReferenceId );
		$objLeaseAssociation->setArCascadeId( $intArCascadeId );
		$objLeaseAssociation->setArCascadeReferenceId( $intArCascadeReferenceId );

		return $objLeaseAssociation;
	}

	public function createInspection() {

		$objInspection = new CInspection();
		$objInspection->setCid( $this->getCid() );
		$objInspection->setPropertyId( $this->getPropertyId() );
		$objInspection->setPsProductId( CPsProduct::ENTRATA );
		$objInspection->setLeaseId( $this->getId() );

		return $objInspection;
	}

	public function createArApprovalRequest( $intRouteTypeId ) {

		$objArApprovalRequest = new CArApprovalRequest();
		$objArApprovalRequest->setCid( $this->getCid() );
		$objArApprovalRequest->setLeaseId( $this->getId() );
		$objArApprovalRequest->setRouteTypeId( $intRouteTypeId );

		return $objArApprovalRequest;
	}

	public function createLeaseAccommodation() {

		$objLeaseAccommodation = new CLeaseAccommodation();
		$objLeaseAccommodation->setCid( $this->getCid() );
		$objLeaseAccommodation->setLeaseId( $this->getId() );

		return $objLeaseAccommodation;
	}

	public function getIsPaymentEligibleLease() {
		return $this->m_boolIsPaymentEligibleLease;
	}

	public function createLeaseDetail() {

		$objLeaseDetail = new CLeaseDetail();
		$objLeaseDetail->setCid( $this->getCid() );
		$objLeaseDetail->setLeaseId( $this->getId() );

		return $objLeaseDetail;
	}

	public function createNoticeBatchDetail() {

		$objNoticeBatchDetail = new CNoticeBatchDetail();
		$objNoticeBatchDetail->setCid( $this->getCid() );
		$objNoticeBatchDetail->setPropertyId( $this->getPropertyId() );
		$objNoticeBatchDetail->setLeaseId( $this->getId() );

		return $objNoticeBatchDetail;
	}

	public function createLeaseUtility() {

		$objLeaseUtility = new CLeaseUtility();

		$objLeaseUtility->setCid( $this->getCid() );
		$objLeaseUtility->setPropertyId( $this->getPropertyId() );
		$objLeaseUtility->setLeaseId( $this->getId() );
		$objLeaseUtility->setLeaseIntervalId( $this->getActiveLeaseIntervalId() );

		return $objLeaseUtility;
	}

	public function createRepayment() {
		$objRepayment = new CRepayment();

		$objRepayment->setCid( $this->getCid() );
		$objRepayment->setPropertyId( $this->getPropertyId() );
		$objRepayment->setLeaseId( $this->getId() );

		return $objRepayment;
	}

	public function initializeLeaseRePaymentBalance( $objClientDatabase ) {
		$fltTotalAmount = 0.0;
		$fltLeaseCurrentRepaymentAmount = 0.0;

		$this->getRepaymentDetails( $objClientDatabase );

		/** @var \CRepayment $objRepayment */
		$arrobjCurrentRepayment = $this->getRepayment();

		if( true == valArr( $arrobjCurrentRepayment ) ) {
			foreach( $arrobjCurrentRepayment as $objRepayment ) {
				if( true === valObj( $objRepayment, 'CRepayment' ) ) {
					$fltTotalAmount -= $objRepayment->getCurrentBalance();
					if( strtotime( $objRepayment->getStartsOn() ) <= strtotime( date( 'm/d/Y' ) ) ) {
						$fltTotalAmount += $objRepayment->getCurrentAmountDue();
						$fltLeaseCurrentRepaymentAmount += $objRepayment->getCurrentAmountDue();
					}
				}
			}
		}
		$this->setRepaymentCurrentAmountDue( $fltLeaseCurrentRepaymentAmount );
		return $fltTotalAmount;
	}

	public function getTotalRepaymentCurrentAmountDue( $objClientDatabase ) {
		$fltLeaseCurrentRepaymentAmountDue = 0.0;

		$this->getRepaymentDetails( $objClientDatabase );

		/** @var \CRepayment $objRepayment */
		$arrobjCurrentRepayment = $this->getRepayment();
		if( true == \valArr( $arrobjCurrentRepayment ) ) {
			foreach( $arrobjCurrentRepayment as $objRepayment ) {
				if( true === \valObj( $objRepayment, \CRepayment::class ) ) {
					$fltLeaseCurrentRepaymentAmountDue += $objRepayment->getCurrentAmountDue();
				}
			}
		}
		return $fltLeaseCurrentRepaymentAmountDue;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getSystemEmails() {
		return $this->m_arrobjSystemEmails;
	}

	public function getIsRuleStopUpdated() {
		return $this->m_boolIsRuleStopUpdated;
	}

	public function getIsSyncSpaceConfigurationIdToApplication() {
		return $this->m_boolIsSyncSpaceConfigurationIdToApplication;
	}

	public function getFailRoutedDetails() {
		return $this->m_arrobjFailRoutedDetails;
	}

	public function getTransactionAmount() {
		return $this->m_fltTransactionAmount;
	}

	public function getDetails() {
		return $this->m_arrobjDetails;
	}

	public function getIsImmediateMoveIn() {
		return $this->m_boolIsImmediateMoveIn;
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function getTransferType() {
		return $this->m_intTransferType;
	}

	public function getPrimaryLeaseCustomer() {
		return $this->m_objPrimaryLeaseCustomer;
	}

	public function getApplication() {
		return $this->m_objApplication;
	}

	public function fetchResponsibleLeaseCustomersByLeaseIdByCid( $objDatabase ) {
		// @TODO Test it thoroughly in second phase refactoring
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersByLeaseIdByCustomerTypeIdsByCid( $this->m_intId, CCustomerType::$c_arrintResponsibleCustomerTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseCustomersByIdsByCustomerTypeIds( $arrintIds, $arrintCustomerTypeIds, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return NULL;

		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersByIdsByLeaseIdByCustomerTypeIdsByCid( $arrintIds, $this->getId(), $arrintCustomerTypeIds, $this->getCid(), $objDatabase );
	}

	public function getLeaseStatusTypeId() {
		if( true == isset( $this->m_intLeaseStatusTypeId ) ) {
			return $this->m_intLeaseStatusTypeId;
		}

		if( true == valObj( $this->m_objDatabase, 'CDatabase' ) ) {
            $this->m_objPrimaryLeaseCustomer = $this->getOrFetchPrimaryLeaseCustomer( $this->m_objDatabase );
		}

		if( false == valObj( $this->m_objPrimaryLeaseCustomer, 'CLeaseCustomer' ) ) return NULL;

		$this->m_intLeaseStatusTypeId = $this->m_objPrimaryLeaseCustomer->getLeaseStatusTypeId();

		return $this->m_intLeaseStatusTypeId;
	}

	public function getLeaseStatusTypeName() {
		if( true == isset( $this->m_strLeaseStatusTypeName ) ) {
			return $this->m_strLeaseStatusTypeName;

		} else {
			$this->m_strLeaseStatusTypeName = CLeaseStatusType::getLeaseStatusTypeNameByLeaseStatusTypeId( $this->getLeaseStatusTypeId() );
			return $this->m_strLeaseStatusTypeName;
		}
	}

	public function getTransferredOn() {
		if( true == isset( $this->m_strTransferredOn ) ) {
			return $this->m_strTransferredOn;
		}

		if( true == valObj( $this->m_objDatabase, 'CDatabase' ) ) {
            $this->m_objLeaseProcess = $this->getOrfetchLeaseProcess( $this->m_objDatabase );
		}

		if( false == valObj( $this->m_objLeaseProcess, 'CLeaseProcess' ) )  return NULL;

		$this->m_strTransferredOn = $this->m_objLeaseProcess->getTransferredOn();

		return $this->m_strTransferredOn;
	}

	public function getFmoStartedOn() {
		if( true == isset( $this->m_strFmoStartedOn ) ) {
			return $this->m_strFmoStartedOn;
		}

        if( true == valObj( $this->m_objDatabase, 'CDatabase' ) ) {
            $this->m_objLeaseProcess = $this->getOrfetchLeaseProcess( $this->m_objDatabase );
        }

        if( false == valObj( $this->m_objLeaseProcess, 'CLeaseProcess' ) )  return NULL;

		$this->m_strFmoStartedOn = $this->m_objLeaseProcess->getFmoStartedOn();

		return $this->m_strFmoStartedOn;
	}

	public function getFmoApprovedOn() {
		if( true == isset( $this->m_strFmoApprovedOn ) ) {
			return $this->m_strFmoApprovedOn;
		}

        if( true == valObj( $this->m_objDatabase, 'CDatabase' ) ) {
            $this->m_objLeaseProcess = $this->getOrfetchLeaseProcess( $this->m_objDatabase );
        }

        if( false == valObj( $this->m_objLeaseProcess, 'CLeaseProcess' ) )  return NULL;

		$this->m_strFmoApprovedOn = $this->m_objLeaseProcess->getFmoApprovedOn();

		return $this->m_strFmoApprovedOn;
	}

	public function getFmoProcessedOn() {

	    if( true == isset( $this->m_strFmoProcessedOn ) ) {
			return $this->m_strFmoProcessedOn;
		}

        if( true == valObj( $this->m_objDatabase, 'CDatabase' ) ) {
            $this->m_objLeaseProcess = $this->getOrfetchLeaseProcess( $this->m_objDatabase );
        }

        if( false == valObj( $this->m_objLeaseProcess, 'CLeaseProcess' ) )  return NULL;

		$this->m_strFmoProcessedOn = $this->m_objLeaseProcess->getFmoProcessedOn();

		return $this->m_strFmoProcessedOn;
	}

	public function getSpaceNumber() {
		return $this->m_strSpaceNumber;
	}

	public function getBalance() {
		return $this->m_fltBalance;
	}

	public function getWriteOffBalance() {
		return $this->m_fltWriteOffBalance;
	}

	public function getBalanceWithWriteOff() {
		$fltTotalBalance = $this->getBalance();

		// If Financial Move Out has been performed, add in the write off balance
		if( true === valStr( $this->getFmoProcessedOn() ) ) {
			$fltTotalBalance += $this->fetchWriteOffBalance( $this->m_objDatabase );
		}

		return $fltTotalBalance;
	}

	public function getMiniminumPaymentAmount() {
		return $this->m_fltMiniminumPaymentAmount;
	}

	public function getCheckAccountTypes() {
		return $this->m_arrobjCheckAccountTypes;
	}

	public function getFormattedBalance() {

		$strBalance = ( 0 > $this->m_fltBalance ) ? '($' . number_format( abs( $this->m_fltBalance ), 2 ) . ')' : '$' . number_format( $this->m_fltBalance, 2 );

		return $strBalance;
	}

	public function getFormattedMiniminumPaymentAmount() {
		if( 0 > $this->m_fltMiniminumPaymentAmount ) {
			return '(' . __( '{%m, 0}', [ $this->m_fltMiniminumPaymentAmount ] ) . ')';
		} else {
			return __( '{%m, 0}', [ $this->m_fltMiniminumPaymentAmount ] );
		}
	}

	public function getScheduledChargeTotal() {
		return $this->m_fltScheduledChargeTotal;
	}

	public function getArAllocations() {
		return $this->m_arrobjArAllocations;
	}

	public function getCustomerNameFull() {

		if( true == isset( $this->m_strCustomerNameFull ) ) {
			return $this->m_strCustomerNameFull;
		}

		if( true == valObj( $this->m_objDatabase, 'CDatabase' ) ) {
			$this->m_objPrimaryCustomer = $this->getOrFetchPrimaryCustomer( $this->m_objDatabase );
		}

		if( false == valObj( $this->m_objPrimaryCustomer, 'CCustomer' ) ) return NULL;

		$this->m_strCustomerNameFull = $this->m_objPrimaryCustomer->getNameFull();

		return $this->m_strCustomerNameFull;
	}

	public function getPropertyGlSetting() {
		return $this->m_objPropertyGlSetting;
	}

	public function getPropertyChargeSetting() {
		return $this->m_objPropertyChargeSetting;
	}

	public function getUnappliedCreditsAndPaymentsBalance() {
		return $this->m_fltUnappliedCreditsAndPaymentsBalance;
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function getUnitNumberCache( $boolFetchFromDB = true ) {
		if( true == isset( $this->m_strUnitNumberCache ) ) {
			return $this->m_strUnitNumberCache;
		}

		if( true == valObj( $this->m_objDatabase, 'CDatabase' ) && true == $boolFetchFromDB ) {
			$this->m_objUnitSpace = $this->getOrFetchUnitSpace( $this->m_objDatabase );
		}

		if( false == valObj( $this->m_objUnitSpace, 'CUnitSpace' ) ) return NULL;

		$this->m_strUnitNumberCache = $this->m_objUnitSpace->getUnitNumberCache();

		return $this->m_strUnitNumberCache;
	}

	public function getDisplayNumber() {
		if( true == isset( $this->m_intDisplayNumber ) ) {
			return $this->m_intDisplayNumber;
		}

        if( true == valObj( $this->m_objDatabase, 'CDatabase' ) ) {
            $this->m_objUnitSpace = $this->getOrFetchUnitSpace( $this->m_objDatabase );
        }

        if( false == valObj( $this->m_objUnitSpace, 'CUnitSpace' ) ) return NULL;

		$this->m_intDisplayNumber = $this->m_objUnitSpace->getDisplayNumber();

		return $this->m_intDisplayNumber;
	}

	public function getBuildingName( $boolFetchFromDB = true ) {

		if( true == isset( $this->m_strBuildingName ) ) {
			return $this->m_strBuildingName;
		}

		if( true == valObj( $this->m_objDatabase, 'CDatabase' ) && true == $boolFetchFromDB ) {
			$this->m_objUnitSpace = $this->getOrFetchUnitSpace( $this->m_objDatabase );
		}

		if( false == valObj( $this->m_objUnitSpace, 'CUnitSpace' ) ) return NULL;

		$this->m_strBuildingName = $this->m_objUnitSpace->getBuildingName();

		return $this->m_strBuildingName;
	}

	public function getPropertyName() {
		if( true == isset( $this->m_strPropertyName ) ) {
			return $this->m_strPropertyName;
		}

		if( true == valObj( $this->m_objDatabase, 'CDatabase' ) ) {
			$this->m_objProperty = $this->getOrFetchProperty( $this->m_objDatabase );
		}

        if( false == valObj( $this->m_objProperty, 'CProperty' ) ) return NULL;

		$this->m_strPropertyName = $this->m_objProperty->getPropertyName();

		return $this->m_strPropertyName;
	}

	public function getPropertyUnit() {
		return $this->m_objPropertyUnit;
	}

	public function getUnitSpace() {
		return $this->m_objUnitSpace;
	}

	public function getProperty() {
		return $this->m_objProperty;
	}

	public function getTransferLeaseId() {
		return $this->m_intTransferLeaseId;
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function getLeaseCustomerId() {
		return $this->m_intLeaseCustomerId;
	}

	public function getCustomers() {
		return $this->m_arrobjCustomers;
	}

	public function getFiles() {
		return $this->m_arrobjFiles;
	}

	public function getArTransactions() {
		return $this->m_arrobjArTransactions;
	}

	public function getScheduledCharges() {
		return $this->m_arrobjScheduledCharges;
	}

	public function getIsAddResidentProcess() {
		return $this->m_boolIsAddResidentProcess;
	}

	public function getActivateStandardPosting() {
		return $this->m_boolActivateStandardPosting;
	}

	public function getHasLeadManagementProduct() {
		return $this->m_boolHasLeadManagementProduct;
	}

	public function getNameFirst() {

		if( true == isset( $this->m_strNameFirst ) ) {
			return $this->m_strNameFirst;
		}

		if( true == valObj( $this->m_objDatabase, 'CDatabase' ) ) {
            $this->m_objPrimaryCustomer = $this->getOrFetchPrimaryCustomer( $this->m_objDatabase );
		}

		if( false == valObj( $this->m_objPrimaryCustomer, 'CCustomer' ) ) return NULL;

		$this->m_strNameFirst = $this->m_objPrimaryCustomer->getNameFirst();

		return $this->m_strNameFirst;
	}

	public function getIsRenewalTransfer() {
		return $this->m_boolIsRenewalTransfer;
	}

	public function getTransferredFromLeaseId() {
		return $this->m_intTransferredFromLeaseId;
	}

	public function getLeasingTierSpecialIds() {
		return $this->m_strLeasingTierSpecialIds;
	}

	public function setIsPaymentEligibleLease( $boolIsPaymentEligibleLease ) {
		$this->m_boolIsPaymentEligibleLease = $boolIsPaymentEligibleLease;
	}

	public function setRepayment( $objRepayment ) {
		$this->m_objRepayment = $objRepayment;
	}

	public function setIsShowIntegrationError( $boolIsShowIntegrationError = false ) {
		$this->m_boolIsShowIntegrationError = $boolIsShowIntegrationError;
	}

	public function getRepayment() {
		return $this->m_objRepayment;
	}

	public function getNameLast() {

		if( true == isset( $this->m_strNameLast ) ) {
			return $this->m_strNameLast;
		}

        if( true == valObj( $this->m_objDatabase, 'CDatabase' ) ) {
            $this->m_objPrimaryCustomer = $this->getOrFetchPrimaryCustomer( $this->m_objDatabase );
        }

        if( false == valObj( $this->m_objPrimaryCustomer, 'CCustomer' ) ) return NULL;

		$this->m_strNameLast = $this->m_objPrimaryCustomer->getNameLast();

		return $this->m_strNameLast;
	}

	public function getCustomerEmailAddress() {
		return $this->m_strCustomerEmailAddress;
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function getCustomerMobileNumber() {
		return $this->m_strCustomerMobileNumber;
	}

	public function getIsRenewed() {
		return $this->m_boolIsRenewed;
	}

	public function getIsLeadPayment() {
		return $this->m_boolIsLeadPayment;
	}

	public function getIsSoftLocked() {
		return $this->m_boolIsSoftLocked;
	}

	public function getApplicationLeaseId() {
		return $this->m_intApplicationLeaseId;
	}

	public function getApplicationPriority() {
		return $this->m_intApplicationPriority;
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function getPrimaryApplicantId() {
		return $this->m_intPrimaryApplicantId;
	}

	public function getRenewalOfferId() {
		return $this->m_intRenewalOfferId;
	}

	public function getLeaseIntervalTypeId() {
		if( true == isset( $this->m_intLeaseIntervalTypeId ) ) {
			return $this->m_intLeaseIntervalTypeId;
		}

		if( true == valObj( $this->m_objDatabase, 'CDatabase' ) ) {
            $this->m_objActiveLeaseInterval = $this->getOrFetchActiveLeaseInterval( $this->m_objDatabase );
		}

		if( false == valObj( $this->m_objActiveLeaseInterval, 'CLeaseInterval' ) ) return NULL;

		$this->m_intLeaseIntervalTypeId = $this->m_objActiveLeaseInterval->getLeaseIntervalTypeId();

		return $this->m_intLeaseIntervalTypeId;
	}

	public function getLeaseStartDate() {
		if( true == isset( $this->m_strLeaseStartDate ) ) {
			return $this->m_strLeaseStartDate;
		}

        if( true == valObj( $this->m_objDatabase, 'CDatabase' ) ) {
            $this->m_objActiveLeaseInterval = $this->getOrFetchActiveLeaseInterval( $this->m_objDatabase );
        }

        if( false == valObj( $this->m_objActiveLeaseInterval, 'CLeaseInterval' ) ) return NULL;

		$this->m_strLeaseStartDate = $this->m_objActiveLeaseInterval->getLeaseStartDate();

		return $this->m_strLeaseStartDate;
	}

	public function getLeaseEndDate( $boolFetchFromDB = true ) {
		if( true == isset( $this->m_strLeaseEndDate ) ) {
			return $this->m_strLeaseEndDate;
		}

		if( true == valObj( $this->m_objDatabase, 'CDatabase' ) && true == $boolFetchFromDB ) {
			$this->m_objDatabase->checkAndReOpenBrokenConnection();
			$this->m_objActiveLeaseInterval = $this->getOrFetchActiveLeaseInterval( $this->m_objDatabase );
		}

		if( false == valObj( $this->m_objActiveLeaseInterval, 'CLeaseInterval' ) ) return NULL;

		$this->m_strLeaseEndDate = $this->m_objActiveLeaseInterval->getLeaseEndDate();

		return $this->m_strLeaseEndDate;
	}

	public function getMoveInDate() {

		if( true == isset( $this->m_strMoveInDate ) ) {
			return $this->m_strMoveInDate;
		}

		if( true == valObj( $this->m_objDatabase, 'CDatabase' ) ) {
            $this->m_objLeaseProcess = $this->getOrfetchLeaseProcess( $this->m_objDatabase );
		}

		if( false == valObj( $this->m_objLeaseProcess, 'CLeaseProcess' ) ) return NULL;

		$this->m_strMoveInDate = $this->m_objLeaseProcess->getMoveInDate();

		return $this->m_strMoveInDate;
	}

	public function getNoticeDate() {
		if( true == isset( $this->m_strNoticeDate ) ) {
			return $this->m_strNoticeDate;
		}

        if( true == valObj( $this->m_objDatabase, 'CDatabase' ) ) {
            $this->m_objLeaseProcess = $this->getOrfetchLeaseProcess( $this->m_objDatabase );
        }

        if( false == valObj( $this->m_objLeaseProcess, 'CLeaseProcess' ) ) return NULL;

		$this->m_strNoticeDate = $this->m_objLeaseProcess->getNoticeDate();

		return $this->m_strNoticeDate;
	}

	public function getMoveOutDate() {
		if( true == isset( $this->m_strMoveOutDate ) ) {
			return $this->m_strMoveOutDate;
		}

        if( true == valObj( $this->m_objDatabase, 'CDatabase' ) ) {
            $this->m_objLeaseProcess = $this->getOrfetchLeaseProcess( $this->m_objDatabase );
        }

        if( false == valObj( $this->m_objLeaseProcess, 'CLeaseProcess' ) ) return NULL;

		$this->m_strMoveOutDate = $this->m_objLeaseProcess->getMoveOutDate();

		return $this->m_strMoveOutDate;
	}

	public function getCollectionsStartDate() {
		if( true == isset( $this->m_strCollectionsStartDate ) ) {
			return $this->m_strCollectionsStartDate;
		}

        if( true == valObj( $this->m_objDatabase, 'CDatabase' ) ) {
            $this->m_objLeaseProcess = $this->getOrfetchLeaseProcess( $this->m_objDatabase );
        }

        if( false == valObj( $this->m_objLeaseProcess, 'CLeaseProcess' ) ) return NULL;

		$this->m_strCollectionsStartDate = $this->m_objLeaseProcess->getCollectionsStartDate();

		return $this->m_strCollectionsStartDate;
	}

	public function getPropertyUnitName() {
		return $this->m_strPropertyUnitName;
	}

	public function getUnitSpaceName() {
		return $this->m_strUnitSpaceName;
	}

	public function getInsurancePolicyId() {
		return $this->m_strInsurancePolicyId;
	}

	public function getInsurancePolicyLeaseId() {
		return $this->m_strInsurancePolicyLeaseId;
	}

	public function getLeaseCustomers() {
		return $this->m_arrobjLeaseCustomers;
	}

	public function getLeaseIntervals() {
		return $this->m_arrobjLeaseIntervals;
	}

	public function getDelinquencyAmount() {
		return $this->m_fltDelinquencyAmount;
	}

	public function getFormattedDelinquencyAmount() {
		return number_format( $this->m_fltDelinquencyAmount, 2, '.', ',' );
	}

	public function getPreGraceDaysDelinquencyAmount() {
		return $this->m_fltPreGraceDaysDelinquencyAmount;
	}

	public function getFormattedPreGraceDaysDelinquencyAmount() {
		if( 0 > $this->m_fltPreGraceDaysDelinquencyAmount ) {
			return '($' . number_format( abs( $this->m_fltPreGraceDaysDelinquencyAmount ), 2 ) . ')';
		} else {
			return ' $' . number_format( $this->m_fltPreGraceDaysDelinquencyAmount, 2 );
		}
	}

	public function getActiveLeaseInterval() {
		return $this->m_objActiveLeaseInterval;
	}

	public function getLeaseDetail() {
		return $this->m_objLeaseDetail;
	}

	public function getLeaseProcessId() {
		return $this->m_intLeaseProcessId;
	}

	public function getLeaseDetailId() {
		return $this->m_intLeaseDetailId;
	}

	public function getRentAmount() {
		return $this->m_intRentAmount;
	}

	public function getCustomerStreetLine1() {
		return $this->m_strCustomerStreetLine1;
	}

	public function getCustomerStreetLine2() {
		return $this->m_strCustomerStreetLine2;
	}

	public function getCustomerStreetLine3() {
		return $this->m_strCustomerStreetLine3;
	}

	public function getCustomerCity() {
		return $this->m_strCustomerCity;
	}

	public function getCustomerStateCode() {
		return $this->m_strCustomerStateCode;
	}

	public function getCustomerPostalCode() {
		return $this->m_strCustomerPostalCode;
	}

	public function getCustomerCountryCode() {
		return $this->m_strCustomerCountryCode;
	}

	public function getLateFeeFormulaId() {
		return $this->m_intLateFeeFormulaId;
	}

	public function getLeaseExpiryDays() {
		return $this->m_intLeaseExpiryDays;
	}

	public function getCalculatedDelayDays() {
		return $this->m_intCalculatedDelayDays;
	}

	public function getIsEligibleForCollection() {
		return $this->m_intIsEligibleForCollection;
	}

	public function getIsSyncApplicationAndLease() {
		return $this->m_intIsSyncApplicationAndLease;
	}

	public function getLeaseStartWindowId() {
		return $this->m_intLeaseStartWindowId;
	}

	public function getBaseRentLeaseTerms() {
		return $this->m_arrobjBaseRentLeaseTerms;
	}

	public function getPropertyFloorPlanId() {
		return $this->m_intPropertyFloorPlanId;
	}

	public function getRenewalStartDate() {
		return $this->m_strRenewalStartDate;
	}

	public function getIsStudentSemesterSelectionEnabled() {
		return $this->m_boolIsStudentSemesterSelectionEnabled;
	}

	public function getSpaceConfigurationId() {
		return $this->m_intSpaceConfigurationId;
	}

	public function getIsDebitEnabled() {
		return $this->m_boolIsDebitEnabled;
	}

	public function getIsVisaPilot() {
		return $this->m_boolIsVisaPilot;
	}

	public function getCompanyName() {
		if( true == isset( $this->m_strCompanyName ) ) {
			return $this->m_strCompanyName;
		}

		if( true == valObj( $this->m_objDatabase, 'CDatabase' ) ) {
			$this->m_objPrimaryCustomer = $this->getOrFetchPrimaryCustomer( $this->m_objDatabase );
		}

        if( false == valObj( $this->m_objPrimaryCustomer, 'CCustomer' ) ) {
            return NULL;
        }

		$this->m_strCompanyName = $this->m_objPrimaryCustomer->getCompanyName();
		return $this->m_strCompanyName;
	}

	public function getGender() {
		return $this->m_strGender;
	}

	public function getPrimaryCustomer() {
		return $this->m_objPrimaryCustomer;
	}

	public function getRepaymentCurrentAmountDue() {
		return $this->m_fltLeaseCurrentRepaymentAmount;
	}

	public function getMoveOutType() {
		return $this->m_strMoveOutType;
	}

	public function getLeaseIntervalId() {
		return $this->m_intLeaseIntervalId;
	}

	public function getMakeReadyStatusId() {
		return $this->m_intMakeReadyStatusId;
	}

	public function getUnitTypeName() {
		return $this->m_strUnitTypeName;
	}

	public function getPropertyFloorPlanName() {
		return $this->m_strPropertyFloorPlanName;
	}

	/**
	 * @return array
	 */
	public function getLeaseUnitSpaceIds() {
		return $this->m_arrintLeaseUnitSpaceIds;
	}

	public function getIsTransferringIn() {
		return $this->m_intIsTransferringIn;
	}

	public function getIsShowIntegrationError() {
		return $this->m_boolIsShowIntegrationError;
	}

	public function getRenewalStatus() {
		return $this->m_strRenewalStatus;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['is_immediate_move_in'] ) ) $this->setIsImmediateMoveIn( $arrmixValues['is_immediate_move_in'] );
		if( true == isset( $arrmixValues['blob'] ) ) $this->setBlob( $arrmixValues['blob'] );
		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_name'] ) : $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['unit_number'] ) ) $this->setUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['unit_number'] ) : $arrmixValues['unit_number'] );
		if( true == isset( $arrmixValues['building_name'] ) ) $this->setBuildingName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['building_name'] ) : $arrmixValues['building_name'] );
		if( true == isset( $arrmixValues['space_number'] ) ) $this->setSpaceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['space_number'] ) : $arrmixValues['space_number'] );
		if( true == isset( $arrmixValues['is_soft_locked'] ) ) $this->setIsSoftLocked( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_soft_locked'] ) : $arrmixValues['is_soft_locked'] );

		if( true == isset( $arrmixValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrmixValues['unit_type_id'] );
		if( true == isset( $arrmixValues['unit_type_name'] ) ) $this->setUnitTypeName( $arrmixValues['unit_type_name'] );
		if( true == isset( $arrmixValues['customer_id'] ) ) $this->setCustomerId( $arrmixValues['customer_id'] );
		if( true == isset( $arrmixValues['lease_customer_id'] ) ) $this->setLeaseCustomerId( $arrmixValues['lease_customer_id'] );
		if( true == isset( $arrmixValues['transfer_lease_id'] ) ) $this->setTransferLeaseId( $arrmixValues['transfer_lease_id'] );
		if( true == isset( $arrmixValues['customer_name_full'] ) ) $this->setCustomerNameFull( $arrmixValues['customer_name_full'] );
		if( true == isset( $arrmixValues['is_add_resident_process'] ) ) $this->setIsAddResidentProcess( $arrmixValues['is_add_resident_process'] );
		if( true == isset( $arrmixValues['lease_status_type_name'] ) ) $this->setLeaseStatusTypeName( $arrmixValues['lease_status_type_name'] );
		if( true == isset( $arrmixValues['transferred_on'] ) ) $this->setTransferredOn( $arrmixValues['transferred_on'] );
		if( true == isset( $arrmixValues['transferred_from_lease_id'] ) ) $this->setTransferredFromLeaseId( $arrmixValues['transferred_from_lease_id'] );

		if( true == isset( $arrmixValues['customer_email_address'] ) ) $this->setCustomerEmailAddress( $arrmixValues['customer_email_address'] );
		if( true == isset( $arrmixValues['phone_number'] ) ) $this->setPhoneNumber( $arrmixValues['phone_number'] );
		if( true == isset( $arrmixValues['is_renewed'] ) ) $this->setIsRenewed( $arrmixValues['is_renewed'] );
		if( true == isset( $arrmixValues['application_lease_id'] ) ) $this->setApplicationLeaseId( $arrmixValues['application_lease_id'] );
		if( true == isset( $arrmixValues['application_priority'] ) ) $this->setApplicationPriority( $arrmixValues['application_priority'] );
		if( true == isset( $arrmixValues['application_id'] ) ) $this->setApplicationId( $arrmixValues['application_id'] );
		if( true == isset( $arrmixValues['primary_applicant_id'] ) ) $this->setPrimaryApplicantId( $arrmixValues['primary_applicant_id'] );
		if( true == isset( $arrmixValues['renewal_offer_id'] ) ) $this->setRenewalOfferId( $arrmixValues['renewal_offer_id'] );
		if( true == isset( $arrmixValues['renewal_status'] ) ) $this->setRenewalStatus( $arrmixValues['renewal_status'] );

		if( true == isset( $arrmixValues['unit_number'] ) ) $this->setPropertyUnitName( $arrmixValues['unit_number'] );
		if( true == isset( $arrmixValues['space_number'] ) ) $this->setUnitSpaceName( $arrmixValues['space_number'] );
		if( true == isset( $arrmixValues['insurance_policy_id'] ) ) $this->setInsurancePolicyId( $arrmixValues['insurance_policy_id'] );
		if( true == isset( $arrmixValues['insurance_policy_lease_id'] ) ) $this->setInsurancePolicyLeaseId( $arrmixValues['insurance_policy_lease_id'] );
		if( true == isset( $arrmixValues['balance'] ) ) $this->setBalance( $arrmixValues['balance'] );
		if( true == isset( $arrmixValues['delinquency_amount'] ) ) $this->setDelinquencyAmount( $arrmixValues['delinquency_amount'] );
		if( true == isset( $arrmixValues['pre_grace_days_delinquency_amount'] ) ) $this->setPreGraceDaysDelinquencyAmount( $arrmixValues['pre_grace_days_delinquency_amount'] );

		if( true == isset( $arrmixValues['lease_process_id'] ) ) $this->setLeaseProcessId( $arrmixValues['lease_process_id'] );
		if( true == isset( $arrmixValues['lease_detail_id'] ) ) $this->setLeaseDetailId( $arrmixValues['lease_detail_id'] );
		if( true == isset( $arrmixValues['late_fee_formula_id'] ) ) $this->setLateFeeFormulaId( $arrmixValues['late_fee_formula_id'] );

		if( true == isset( $arrmixValues['customer_street_line_1'] ) )				$this->setCustomerStreetLine1( $arrmixValues['customer_street_line_1'] );
		if( true == isset( $arrmixValues['customer_street_line_2'] ) )				$this->setCustomerStreetLine2( $arrmixValues['customer_street_line_2'] );
		if( true == isset( $arrmixValues['customer_street_line_3'] ) )				$this->setCustomerStreetLine3( $arrmixValues['customer_street_line_3'] );
		if( true == isset( $arrmixValues['customer_city'] ) )						$this->setCustomerCity( $arrmixValues['customer_city'] );
		if( true == isset( $arrmixValues['customer_state_code'] ) )					$this->setCustomerStateCode( $arrmixValues['customer_state_code'] );
		if( true == isset( $arrmixValues['customer_postal_code'] ) )				$this->setCustomerPostalCode( $arrmixValues['customer_postal_code'] );
		if( true == isset( $arrmixValues['customer_country_code'] ) )				$this->setCustomerCountryCode( $arrmixValues['customer_country_code'] );
		if( true == isset( $arrmixValues['pending_fmo_due_date'] ) )				$this->setPendingFmoDueDate( $arrmixValues['pending_fmo_due_date'] );
		if( true == isset( $arrmixValues['lease_expiry_days'] ) )					$this->setLeaseExpiryDays( $arrmixValues['lease_expiry_days'] );
		if( true == isset( $arrmixValues['activate_standard_posting'] ) )			$this->setActivateStandardPosting( $arrmixValues['activate_standard_posting'] );
		if( true == isset( $arrmixValues['renewal_start_date'] ) )					$this->setRenewalStartDate( $arrmixValues['renewal_start_date'] );
		if( true == isset( $arrmixValues['has_lead_management_product'] ) )			$this->setHasLeadManagementProduct( $arrmixValues['has_lead_management_product'] );
		if( true == isset( $arrmixValues['is_payment_eligible_lease'] ) )			$this->setIsPaymentEligibleLease( $arrmixValues['is_payment_eligible_lease'] );
		if( true == isset( $arrmixValues['is_immediate_move_in'] ) )				$this->setIsImmediateMoveIn( $arrmixValues['is_immediate_move_in'] );
		if( true == isset( $arrmixValues['is_renewal_transfer'] ) )					$this->setIsRenewalTransfer( $arrmixValues['is_renewal_transfer'] );
		if( true == isset( $arrmixValues['property_floorplan_id'] ) )				$this->setPropertyFloorPlanId( $arrmixValues['property_floorplan_id'] );
		if( true == isset( $arrmixValues['property_floorplan_name'] ) )				$this->setPropertyFloorPlanName( $arrmixValues['property_floorplan_name'] );
		if( true == isset( $arrmixValues['renewal_start_date'] ) )					$this->setRenewalStartDate( $arrmixValues['renewal_start_date'] );

		if( true == isset( $arrmixValues['is_eligible_for_collection'] ) ) 		$this->setIsEligibleForCollection( $arrmixValues['is_eligible_for_collection'] );
		if( true == isset( $arrmixValues['calculated_delay_days'] ) ) $this->setCalculatedDelayDays( $arrmixValues['calculated_delay_days'] );
		if( true == isset( $arrmixValues['write_off_balance'] ) ) $this->setWriteOffBalance( $arrmixValues['write_off_balance'] );
		if( true == isset( $arrmixValues['transfer_type'] ) ) $this->setTransferType( $arrmixValues['transfer_type'] );

		if( true == isset( $arrmixValues['lease_start_window_id'] ) ) $this->setLeaseStartWindowId( $arrmixValues['lease_start_window_id'] );

		if( true == isset( $arrmixValues['leasing_tier_special_id'] ) ) $this->setLeasingTierSpecialIds( $arrmixValues['leasing_tier_special_id'] );

		if( true == isset( $arrmixValues['is_student_semester_selection_enabled'] ) ) $this->setIsStudentSemesterSelectionEnabled( $arrmixValues['is_student_semester_selection_enabled'] );
		if( true == isset( $arrmixValues['space_configuration_id'] ) ) $this->setSpaceConfigurationId( $arrmixValues['space_configuration_id'] );
		if( true == isset( $arrmixValues['company_name'] ) ) $this->setCompanyName( $arrmixValues['company_name'] );
		if( true == isset( $arrmixValues['gender'] ) ) $this->setGender( $arrmixValues['gender'] );
		if( true == isset( $arrmixValues['is_transferring_in'] ) ) $this->setIsTransferringIn( $arrmixValues['is_transferring_in'] );
		if( true == isset( $arrmixValues['move_out_type'] ) ) $this->setMoveOutType( $arrmixValues['move_out_type'] );
		if( true == isset( $arrmixValues['make_ready_status_id'] ) ) $this->setMakeReadyStatusId( $arrmixValues['make_ready_status_id'] );
		if( true == isset( $arrmixValues['lease_interval_id'] ) ) $this->setLeaseIntervalId( $arrmixValues['lease_interval_id'] );
		if( true == isset( $arrmixValues['customer_type_id'] ) ) $this->setCustomerTypeId( $arrmixValues['customer_type_id'] );
	}

	// This field comes from the data blobs table, and is used for fast searches.  We apply the string of data from data blobs to this object in this function

	public function setBlob( $strBlob ) {
		if( 0 < strlen( trim( $strBlob ) ) ) {
			$arrstrBlobData = explode( '~..$', $strBlob );

			$this->m_strNameFirst 				= ( true == isset ( $arrstrBlobData[0] ) ) ? $arrstrBlobData[0] : NULL;
			$this->m_strNameLast 				= ( true == isset ( $arrstrBlobData[1] ) ) ? $arrstrBlobData[1] : NULL;
			$this->m_strPropertyName 			= ( true == isset ( $arrstrBlobData[2] ) ) ? $arrstrBlobData[2] : NULL;
			$this->m_strBuildingName			= ( true == isset ( $arrstrBlobData[3] ) ) ? $arrstrBlobData[3] : NULL;
			$this->m_strUnitNumber 				= ( true == isset ( $arrstrBlobData[4] ) ) ? $arrstrBlobData[4] : NULL;
			$this->m_strSpaceNumber 			= ( true == isset ( $arrstrBlobData[5] ) ) ? $arrstrBlobData[5] : NULL;
			$this->m_strLeaseStatusTypeName 	= ( true == isset ( $arrstrBlobData[6] ) ) ? $arrstrBlobData[6] : NULL;
			$this->m_strCustomerEmailAddress 	= ( true == isset ( $arrstrBlobData[7] ) ) ? $arrstrBlobData[7] : NULL;
			$this->m_strPhoneNumber 			= ( true == isset ( $arrstrBlobData[9] ) ) ? $arrstrBlobData[9] : NULL;
			$this->m_strCustomerMobileNumber 	= ( true == isset ( $arrstrBlobData[10] ) ) ? $arrstrBlobData[10] : NULL;
		}
	}

	public function setSystemEmails( $objSysyemEmail, $strKey ) {
		$this->m_arrobjSystemEmails[$strKey] = $objSysyemEmail;
	}

	public function setIsRuleStopUpdated( $boolIsRuleStopUpdated ) {
		$this->m_boolIsRuleStopUpdated = $boolIsRuleStopUpdated;
	}

	public function setIsSyncSpaceConfigurationIdToApplication( $boolIsSyncSpaceConfigurationIdToApplication ) {
		$this->m_boolIsSyncSpaceConfigurationIdToApplication = $boolIsSyncSpaceConfigurationIdToApplication;
	}

	public function setFailRoutedDetails( $arrobjFailRoutedDetails ) {
		$this->m_arrobjFailRoutedDetails = $arrobjFailRoutedDetails;
	}

	public function setTransactionAmount( $fltTransactionAmount ) {
		$this->m_fltTransactionAmount = $fltTransactionAmount;
	}

	public function setDetails( $arrobjDetails ) {
		$this->m_arrobjDetails = $arrobjDetails;
	}

	public function setIsImmediateMoveIn( $boolIsImmediateMoveIn ) {
		$this->m_boolIsImmediateMoveIn = CStrings::strToBool( $boolIsImmediateMoveIn );
	}

	public function setPostMonth( $strPostMonth ) {
		$this->m_strPostMonth = $strPostMonth;
	}

	public function setTransferType( $intTransferType ) {
		$this->m_intTransferType = $intTransferType;
	}

	public function setDelinquencyAmount( $fltDelinquencyAmount ) {
		// TODO: Why are we returning here?
		return $this->m_fltDelinquencyAmount = $fltDelinquencyAmount;
	}

	public function setPreGraceDaysDelinquencyAmount( $fltPreGraceDaysDelinquencyAmount ) {
		// TODO: Why are we returning here?
		return $this->m_fltPreGraceDaysDelinquencyAmount = $fltPreGraceDaysDelinquencyAmount;
	}

	public function setProperty( $objProperty ) {
		$this->m_objProperty = $objProperty;
	}

	public function setPrimaryLeaseCustomer( $objPrimaryLeaseCustomer ) {
		$this->m_objPrimaryLeaseCustomer = $objPrimaryLeaseCustomer;
	}

	public function setPropertyGlSetting( $objPropertyGlSetting ) {
		$this->m_objPropertyGlSetting = $objPropertyGlSetting;
	}

	public function setPropertyChargeSetting( $objPropertyChargeSetting ) {
		$this->m_objPropertyChargeSetting = $objPropertyChargeSetting;
	}

	public function setLeaseStatusTypeName( $strLeaseStatusTypeName ) {
		$this->m_strLeaseStatusTypeName = $strLeaseStatusTypeName;
	}

	public function setArTransactions( $arrobjArTransactions ) {
		$this->m_arrobjArTransactions = $arrobjArTransactions;
	}

	public function setScheduledChargeTotal( $fltScheduledChargeTotal ) {
		$this->m_fltScheduledChargeTotal = $fltScheduledChargeTotal;
	}

	public function setSpaceNumber( $strSpaceNumber ) {
		$this->m_strSpaceNumber = CStrings::strTrimDef( $strSpaceNumber, 50, NULL, true );
	}

	public function setCustomerNameFull( $strCustomerNameFull ) {
		$this->m_strCustomerNameFull = $strCustomerNameFull;
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->m_strUnitNumber = CStrings::strTrimDef( $strUnitNumber, 50, NULL, true );
	}

	public function setBuildingName( $strBuildingName ) {
		$this->m_strBuildingName = CStrings::strTrimDef( $strBuildingName, 50, NULL, true );
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = CStrings::strTrimDef( $strPropertyName, 50, NULL, true );
	}

	public function setTransferLeaseId( $intTransferLeaseId ) {
		$this->m_intTransferLeaseId = CStrings::strToIntDef( $intTransferLeaseId, NULL, false );
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->m_intUnitTypeId = CStrings::strToIntDef( $intUnitTypeId, NULL, true );
	}

	public function setCustomerId( $intCustomerId ) {
		$this->m_intCustomerId = CStrings::strToIntDef( $intCustomerId, NULL, true );
	}

	public function setLeaseCustomerId( $intLeaseCustomerId ) {
		$this->m_intLeaseCustomerId = CStrings::strToIntDef( $intLeaseCustomerId, NULL, true );
	}

	public function setBalance( $fltBalance ) {
		$this->m_fltBalance = $fltBalance;
	}

	public function setMiniminumPaymentAmount( $fltMiniminumPaymentAmount ) {
		$this->m_fltMiniminumPaymentAmount = $fltMiniminumPaymentAmount;
	}

	public function setIsAddResidentProcess( $boolIsAddResidentProcess ) {
		return $this->m_boolIsAddResidentProcess = $boolIsAddResidentProcess;
	}

	public function setActivateStandardPosting( $boolActivateStandardPosting ) {
		$this->m_boolActivateStandardPosting = CStrings::strToBool( $boolActivateStandardPosting );
	}

	public function setHasLeadManagementProduct( $boolHasLeadManagementProduct ) {
		$this->m_boolHasLeadManagementProduct = $boolHasLeadManagementProduct;
	}

	public function setIsRenewalTransfer( $boolIsRenewalTransfer ) {
		$this->m_boolIsRenewalTransfer = $boolIsRenewalTransfer;
	}

	public function setCustomerEmailAddress( $strCustomerEmailAddress ) {
		$this->m_strCustomerEmailAddress = $strCustomerEmailAddress;
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->m_strPhoneNumber = $strPhoneNumber;
	}

	public function setCustomerMobileNumber( $strCustomerMobileNumber ) {
		$this->m_strCustomerMobileNumber = $strCustomerMobileNumber;
	}

	public function setIsRenewed( $boolIsRenewed ) {
		$this->m_boolIsRenewed = $boolIsRenewed;
	}

	public function setIsLeadPayment( $boolIsLeadPayment ) {
		$this->m_boolIsLeadPayment = $boolIsLeadPayment;
	}

	public function setIsSoftLocked( $boolIsSoftLocked ) {
		$this->m_boolIsSoftLocked = $boolIsSoftLocked;
	}

	public function setApplicationLeaseId( $intApplicationLeaseId ) {
		$this->m_intApplicationLeaseId = $intApplicationLeaseId;
	}

	public function setApplicationPriority( $intApplicationPriority ) {
		$this->m_intApplicationPriority = $intApplicationPriority;
	}

	public function setApplicationId( $intApplicationId ) {
		$this->m_intApplicationId = $intApplicationId;
	}

	public function setPrimaryApplicantId( $intPrimaryApplicantId ) {
		$this->m_intPrimaryApplicantId = $intPrimaryApplicantId;
	}

	public function setRenewalOfferId( $intRenewalOfferId ) {
		$this->m_intRenewalOfferId = $intRenewalOfferId;
	}

	public function setPropertyUnitName( $strPropertyUnitName ) {
		$this->m_strPropertyUnitName = $strPropertyUnitName;
	}

	public function setUnitSpaceName( $strUnitSpaceName ) {
		$this->m_strUnitSpaceName = $strUnitSpaceName;
	}

	public function setInsurancePolicyId( $intInsurancePolicyId ) {
		$this->m_strInsurancePolicyId = $intInsurancePolicyId;
	}

	public function setInsurancePolicyLeaseId( $intId ) {
		$this->m_strInsurancePolicyLeaseId = $intId;
	}

	public function setLeaseCustomers( $arrobjLeaseCustomers ) {
		$this->m_arrobjLeaseCustomers = $arrobjLeaseCustomers;
	}

	public function setLeaseProcess( $objLeaseProcess ) {
		$this->m_objLeaseProcess = $objLeaseProcess;
	}

	public function setUnitSpace( $objUnitSpace ) {
		$this->m_objUnitSpace = $objUnitSpace;
	}

	public function setScheduledCharges( $arrobjScheduledCharges ) {
		$this->m_arrobjScheduledCharges = $arrobjScheduledCharges;
	}

	public function setLeaseIntervals( $arrobjLeaseIntervals ) {
		$this->m_arrobjLeaseIntervals = $arrobjLeaseIntervals;
	}

	public function setActiveLeaseInterval( $objActiveLeaseInterval ) {
		$this->m_objActiveLeaseInterval = $objActiveLeaseInterval;
	}

	public function setLeaseDetail( $objLeaseDetail ) {
		$this->m_objLeaseDetail = $objLeaseDetail;
	}

	public function setLeaseProcessId( $intLeaseProcessId ) {
		$this->m_intLeaseProcessId = $intLeaseProcessId;
	}

	public function setLeaseDetailId( $intLeaseDetailId ) {
		$this->m_intLeaseDetailId = $intLeaseDetailId;
	}

	public function setRentAmount( $intRentAmount ) {
		$this->m_intRentAmount = $intRentAmount;
	}

	public function setCustomerStreetLine1( $strCustomerStreetLine1 ) {
		$this->m_strCustomerStreetLine1 = $strCustomerStreetLine1;
	}

	public function setCustomerStreetLine2( $strCustomerStreetLine2 ) {
		$this->m_strCustomerStreetLine2 = $strCustomerStreetLine2;
	}

	public function setCustomerStreetLine3( $strCustomerStreetLine3 ) {
		$this->m_strCustomerStreetLine3 = $strCustomerStreetLine3;
	}

	public function setCustomerCity( $strCustomerCity ) {
		$this->m_strCustomerCity = $strCustomerCity;
	}

	public function setCustomerStateCode( $strCustomerStateCode ) {
		$this->m_strCustomerStateCode = $strCustomerStateCode;
	}

	public function setCustomerPostalCode( $strCustomerPostalCode ) {
		$this->m_strCustomerPostalCode = $strCustomerPostalCode;
	}

	public function setCustomerCountryCode( $strCustomerCountryCode ) {
		$this->m_strCustomerCountryCode = $strCustomerCountryCode;
	}

	public function setLateFeeFormulaId( $intLateFeeFormulaId ) {
		$this->m_intLateFeeFormulaId = $intLateFeeFormulaId;
	}

	public function setPendingFmoDueDate( $strPendingFmoDueDate ) {
		$this->m_strPendingFmoDueDate = $strPendingFmoDueDate;
	}

	public function setLeaseExpiryDays( $intLeaseExpiryDays ) {
		$this->m_intLeaseExpiryDays = $intLeaseExpiryDays;
	}

	public function setCalculatedDelayDays( $intCalculatedDelayDays ) {
		$this->m_intCalculatedDelayDays = $intCalculatedDelayDays;
	}

	public function setIsEligibleForCollection( $intIsEligibleForCollection ) {
		$this->m_intIsEligibleForCollection = $intIsEligibleForCollection;
	}

	public function setIsSyncApplicationAndLease( $intIsSyncApplicationAndLease ) {
		$this->m_intIsSyncApplicationAndLease = $intIsSyncApplicationAndLease;
	}

	public function setWriteOffBalance( $fltWriteOffBalance ) {
		$this->m_fltWriteOffBalance = CStrings::strToFloatDef( $fltWriteOffBalance, NULL, false, 2 );
	}

	public function setLeaseStartWindowId( $intLeaseStartWindowId ) {
		$this->m_intLeaseStartWindowId = CStrings::strToIntDef( $intLeaseStartWindowId, NULL, false );
	}

	public function setBaseRentLeaseTerms( $arrobjLeaseTerms ) {
		$this->m_arrobjBaseRentLeaseTerms = $arrobjLeaseTerms;
	}

	public function setPropertyFloorPlanId( $intPropertyFloorPlanId ) {
		$this->m_intPropertyFloorPlanId = CStrings::strToIntDef( $intPropertyFloorPlanId, NULL, false );
	}

	public function setRenewalStartDate( $strRenewalStartDate ) {
		$this->m_strRenewalStartDate = $strRenewalStartDate;
	}

	public function setTransferredFromLeaseId( $intTransferredFromLeaseId ) {
		$this->m_intTransferredFromLeaseId = CStrings::strToIntDef( $intTransferredFromLeaseId, NULL, false );
	}

	public function setLeasingTierSpecialIds( $strLeasingTierSpecialIds ) {
		$this->m_strLeasingTierSpecialIds = $strLeasingTierSpecialIds;
	}

	public function setIsStudentSemesterSelectionEnabled( $boolIsStudentSemesterSelectionEnabled ) {
		$this->m_boolIsStudentSemesterSelectionEnabled = CStrings::strToBool( $boolIsStudentSemesterSelectionEnabled );
	}

	public function setSpaceConfigurationId( $intSpaceConfigurationId ) {
		$this->m_intSpaceConfigurationId = CStrings::strToIntDef( $intSpaceConfigurationId, NULL, false );
	}

	public function setIsDebitEnabled( $boolIsDebitEnabled ) {
		$this->m_boolIsDebitEnabled = CStrings::strToBool( $boolIsDebitEnabled );
	}

	public function setIsVisaPilot( $boolIsVisaPilot ) {
		$this->m_boolIsVisaPilot = CStrings::strToBool( $boolIsVisaPilot );
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setPrimaryCustomer( $objPrimaryCustomer ) {
		$this->m_objPrimaryCustomer = $objPrimaryCustomer;
	}

    public function setLeaseIntervalTypeId( $intLeaseIntervalTypeId ) {
        $this->m_intLeaseIntervalTypeId = $intLeaseIntervalTypeId;
    }

    public function setFmoStartedOn( $strFmoStartedOn ) {
       parent::setFmoStartedOn( $strFmoStartedOn );

        $objLeaseProcess = $this->getOrfetchLeaseProcess();
        if( false == valObj( $objLeaseProcess, 'CLeaseProcess' ) )  return;
        $objLeaseProcess->setFmoStartedOn( $strFmoStartedOn );
    }

    public function setFmoProcessedOn( $strFmoProcessedON ) {
        parent::setFmoProcessedOn( $strFmoProcessedON );

         $objLeaseProcess = $this->getOrfetchLeaseProcess();
         if( false == valObj( $objLeaseProcess, 'CLeaseProcess' ) )  return;
         $objLeaseProcess->setFmoProcessedOn( $strFmoProcessedON );
    }

    public function setFmoApprovedOn( $strFmoApprovedOn ) {
        parent::setFmoApprovedOn( $strFmoApprovedOn );

        $objLeaseProcess = $this->getOrfetchLeaseProcess();
        if( false == valObj( $objLeaseProcess, 'CLeaseProcess' ) )  return;
        $objLeaseProcess->setFmoApprovedOn( $strFmoApprovedOn );
    }

    public function setTransferredOn( $strTransferredOn ) {
        $this->m_strTransferredOn = CStrings::strTrimDef( $strTransferredOn, -1, NULL, true );

        $objLeaseProcess = $this->getOrfetchLeaseProcess();
        if( false == valObj( $objLeaseProcess, 'CLeaseProcess' ) )  return;
        $objLeaseProcess->setTransferredOn( $strTransferredOn );
    }

    public function setCollectionsStartDate( $strCollectionsStartDate ) {
        parent::setCollectionsStartDate( $strCollectionsStartDate );

        $objLeaseProcess = $this->getOrfetchLeaseProcess();
        if( false == valObj( $objLeaseProcess, 'CLeaseProcess' ) )  return;
        $objLeaseProcess->setCollectionsStartDate( $strCollectionsStartDate );
    }

    public function setMoveInDate( $strMoveInDate ) {
        parent::setMoveInDate( $strMoveInDate );

        $objLeaseProcess = $this->getOrfetchLeaseProcess();
        if( false == valObj( $objLeaseProcess, 'CLeaseProcess' ) )  return;
        $objLeaseProcess->setMoveInDate( $strMoveInDate );
    }

    public function setMoveOutDate( $strMoveOutDate ) {
        parent::setMoveOutDate( $strMoveOutDate );

        $objLeaseProcess = $this->getOrfetchLeaseProcess();
        if( false == valObj( $objLeaseProcess, 'CLeaseProcess' ) )  return;
        $objLeaseProcess->setMoveOutDate( $strMoveOutDate );
    }

    public function setNoticeDate( $strNoticeDate ) {
        parent::setNoticeDate( $strNoticeDate );

        $objLeaseProcess = $this->getOrfetchLeaseProcess();
        if( false == valObj( $objLeaseProcess, 'CLeaseProcess' ) )  return;
        $objLeaseProcess->setNoticeDate( $strNoticeDate );
    }

	public function setIsTransferringIn( $intIsTransferringIn ) {
		$this->m_intIsTransferringIn = $intIsTransferringIn;
	}

	public function setRepaymentCurrentAmountDue( $fltLeaseCurrentRepaymentAmount ) {
		$this->m_fltLeaseCurrentRepaymentAmount = $fltLeaseCurrentRepaymentAmount;
	}

	public function setGender( $strGender ) {
		$this->m_strGender = CStrings::strTrimDef( $strGender, -1, NULL, true );
	}

	public function setMoveOutType( $strMoveOutType ) {
		return $this->m_strMoveOutType = $strMoveOutType;
	}

	public function setMakeReadyStatusId( $intMakeReadyStatusId ) {
		return $this->m_intMakeReadyStatusId = $intMakeReadyStatusId;
	}

	public function setLeaseIntervalId( $intLeaseIntervalId ) {
		return $this->m_intLeaseIntervalId = $intLeaseIntervalId;
	}

	public function setUnitTypeName( $strUnitTypeName ) {
		$this->m_strUnitTypeName = $strUnitTypeName;
	}

	public function setPropertyFloorPlanName( $strPropertyFloorPlanName ) {
		$this->m_strPropertyFloorPlanName = $strPropertyFloorPlanName;
	}

	public function setRequiredParameters( $arrmixCustomData ) {
		$this->m_arrmixRequiredParameters = $arrmixCustomData;
	}

	/**
	 * @param array $arrintLeaseUnitSpaceIds
	 */
	public function setLeaseUnitSpaceIds( $arrintLeaseUnitSpaceIds ) {
		$this->m_arrintLeaseUnitSpaceIds = ( array ) $arrintLeaseUnitSpaceIds;
	}

	public function setRenewalStatus( $strRenewalStatus ) {
		$this->m_strRenewalStatus = $strRenewalStatus;
	}

	public function setApplication( $objApplication ) {
		$this->m_objApplication = $objApplication;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchPrimaryLeaseCustomer( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomerByCustomerIdByLeaseIdByCid( $this->getPrimaryCustomerId(), $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchCustomers( $objDatabase ) {
		if( is_null( $this->m_arrobjCustomers ) ) {
			$this->m_arrobjCustomers =& \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		}
		return $this->m_arrobjCustomers;
	}

	public function fetchLeaseCustomers( $objDatabase, $boolFromApplication = false, $boolActiveOnly = false ) {
		// @TODO Test it thoroughly in second phase refactoring
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchCustomLeaseCustomersByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolFromApplication, $boolActiveOnly );
	}

	public function fetchActivePrimaryCustomer( $objDatabase ) {

		$arrobjLeaseCustomers = [];
		$objLeaseCustomer = NULL;

		if( false == valArr( $this->m_arrobjLeaseCustomers ) ) {
			$this->m_arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchActiveLeaseCustomersByLeaseIdCid( $this->getId(), $this->getCid(), $objDatabase );
		}

		if( true == valArr( $this->m_arrobjLeaseCustomers ) ) {
			$arrobjLeaseCustomers = rekeyObjects( 'CustomerTypeId', $this->m_arrobjLeaseCustomers );
		}

		if( true == valArr( $arrobjLeaseCustomers ) ) {
			$objLeaseCustomer = getArrayElementByKey( CCustomerType::PRIMARY, $arrobjLeaseCustomers );
		}

		if( true == valObj( $objLeaseCustomer, 'CLeaseCustomer' ) ) {
			return $this->fetchCustomer( $objLeaseCustomer->getCustomerId(), $objDatabase );

		} else {
			return NULL;
		}
	}

	public function fetchLeaseCustomersByIds( $arrintLeaseCustomersIds, $objDatabase ) {
		// @TODO Test it thoroughly in second phase refactoring
		$this->m_arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersByLeaseIdByIdsByCid( $this->getId(), $arrintLeaseCustomersIds, $this->getCid(), $objDatabase );
		return $this->m_arrobjLeaseCustomers;
	}

	public function fetchLeaseCustomersByLeaseStatusTypeIds( $arrintStatusTypeIds, $objDatabase ) {
		if( true == is_null( $this->m_arrobjLeaseCustomers ) ) {
			$this->m_arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersByLeaseIdByLeaseStatusTypeIdsByCid( $this->m_intId, $arrintStatusTypeIds, $this->getCid(), $objDatabase );
		}
		return $this->m_arrobjLeaseCustomers;
	}

	public function fetchFileMergeValues( $objDatabase ) {
		return CFileMergeValues::fetchFileMergeValuesByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchFileMergeValueCount( $objDatabase ) {
		return CFileMergeValues::fetchFileMergeValueCountByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchExistingApplicationByIsRenewalPending( $objDatabase ) {
		return CApplications::fetchExistingApplicationByIsRenewalPendingByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCustomScheduledChargesByArTriggerIds( $arrintArTriggerIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchCustomScheduledChargesByArTriggerIdsByLeaseIdByCid( $arrintArTriggerIds, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchScheduledChargeByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchIntegratedScheduledChargeByRemotePrimaryKeyByLeaseIdByCid( $strRemotePrimaryKey, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchScheduledChargesByArTriggerIds( $arrintArTriggerIds, $objDatabase, $boolExcludeAddOnLeaseAssociations = false ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByArTriggerIdsByLeaseIdsByCid( $arrintArTriggerIds, ( array ) $this->getId(), $this->getCid(), $objDatabase, $boolExcludeAddOnLeaseAssociations );
	}

	public function fetchActiveScheduledChargesByArTriggerIds( $arrintArTriggerIds, $objDatabase, $boolExcludeAddOnLeaseAssociations = false ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchActiveScheduledChargesByArTriggerIdsByLeaseIdByCid( $arrintArTriggerIds, ( int ) $this->getId(), $this->getCid(), $objDatabase, $boolExcludeAddOnLeaseAssociations = false );
	}

	public function fetchScheduledChargeById( $intScheduledChargeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchCustomScheduledChargeByIdByCid( $intScheduledChargeId, $this->m_intCid, $objDatabase );
	}

	public function fetchScheduledChargeByIds( $arrintScheduledChargeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchCustomScheduledChargeByIdsByCid( $arrintScheduledChargeIds, $this->m_intCid, $objDatabase );
	}

	public function fetchIntegratedScheduledChargesByArTriggerIds( $arrintArTriggerIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchIntegratedScheduledChargesByArTriggerIdsByLeaseIdByCid( $arrintArTriggerIds, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchScheduledChargesByArTriggerIdsByMoveOutDate( $arrintArTriggerIds, $strMoveOutDate, $objDatabase ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByLeaseIdByArTriggerIdsByMoveOutDateByCid( $this->m_intId, $arrintArTriggerIds, $strMoveOutDate, $this->getCid(), $objDatabase );
	}

	public function fetchCustomer( $intCustomerId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomCustomerByIdByLeaseIdByCid( $intCustomerId, $this->m_intId, $this->getCid(), $objDatabase );
	}

	/**
	 * @param $objDatabase
	 *
	 * @return \CProperty|null
	 */
	public function fetchProperty( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUnit( $objDatabase ) {
		$this->m_objPropertyUnit = \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPropertyUnitByIdByCid( $this->getPropertyUnitId(), $this->getCid(), $objDatabase );

		return $this->m_objPropertyUnit;
	}

	public function fetchLeaseCustomerByCustomerId( $intCustomerId, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomerByCustomerIdByLeaseIdByCid( $intCustomerId, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseStatusTypeIdByCustomerId( $intCustomerId, $objDatabase ) {
		// @TODO Test it thoroughly in second phase refactoring
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseStatusTypeIdByCustomerIdByLeaseIdByCid( $intCustomerId, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchSimpleIncompleteMaintenanceRequestsByMaintenanceStatusCodeByLimit( $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceRequests::createService()->fetchSimpleIncompleteMaintenanceRequestsByLeaseIdByMaintenanceStatusCodeByLimitByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpace( $objDatabase ) {
		if( 0 < ( int ) $this->m_intUnitSpaceId ) {
			$this->m_objUnitSpace = CUnitSpaces::createService()->fetchCustomUnitSpaceByIdByCid( $this->m_intUnitSpaceId, $this->getCid(), $objDatabase );
			return $this->m_objUnitSpace;
		}
		return NULL;
	}

	public function fetchUnitSpaceByRemovingDefaultSpaceNumber( $objDatabase ) {
		if( 0 < ( int ) $this->m_intUnitSpaceId && 0 < ( int ) $this->getPropertyId() ) {
			$this->m_objUnitSpace = CUnitSpaces::createService()->fetchUnitSpaceByIdByRemovingDefaultSpaceNumberByCid( $this->m_intUnitSpaceId, $this->getPropertyId(), $this->getCid(), $objDatabase );
			return $this->m_objUnitSpace;
		}
		return NULL;
	}

	public function fetchPropertyGlSetting( $objDatabase ) {
		$this->m_objPropertyGlSetting = CPropertyGlSettings::fetchCustomPropertyGlSettingsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		return $this->m_objPropertyGlSetting;
	}

	public function fetchPropertyOccupancyTypeSetting( $objDatabase ) {
		$this->m_objPropertyOccupancyTypeSetting = \Psi\Eos\Entrata\CPropertyOccupancyTypeSettings::createService()->fetchPropertyOccupancyTypeSettingsByOccupancyTypeIdByPropertyIdByCid( $this->getOccupancyTypeId(), $this->getPropertyId(), $this->getCid(), $objDatabase );
		return $this->m_objPropertyOccupancyTypeSetting;
	}

	public function fetchPropertyChargeSetting( $objDatabase ) {
		$this->m_objPropertyChargeSetting = \Psi\Eos\Entrata\CPropertyChargeSettings::createService()->fetchPropertyChargeSettingByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		return $this->m_objPropertyChargeSetting;
	}

	public function fetchApplication( $objDatabase ) {
		return $this->m_objApplication = CApplications::fetchApplicationByLeaseIdByLeaseIntervalIdByCid( $this->getId(), $this->getActiveLeaseIntervalId(), $this->getCid(), $objDatabase );
	}

	public function fetchScheduledPayments( $objDatabase ) {
		return CScheduledPayments::fetchScheduledPaymentsByIdByLeaseIdCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchAllScheduledPayments( $objDatabase ) {
		return CScheduledPayments::fetchAllScheduledPaymentsByIdByLeaseIdCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchPausedScheduledPayments( $intCustomerId, $objDatabase ) {
		return CScheduledPayments::fetchPausedScheduledPaymentsByIdByLeaseIdCid( $intCustomerId, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchScheduledCharges( $objDatabase, $intArTriggerId = NULL ) {
		if( true == is_null( $intArTriggerId ) ) {
			return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByLeaseIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
		}
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByArTriggerIdByLeaseIdByCid( $intArTriggerId, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchHasMerchantAccount( $objDatabase ) {

		$strWhere = ' WHERE property_id = ' . $this->getPropertyId() . ' AND cid = ' . $this->getCid() . ' AND ar_code_id IS NULL AND disabled_by IS NULL AND disabled_on IS NULL';

		$intCount = CPropertyMerchantAccounts::fetchPropertyMerchantAccountCount( $strWhere, $objDatabase );

		if( true == isset( $intCount ) ) return $intCount;

		return 0;
	}

	// This is getting the sum of recurring charges set up on the lease.

	public function fetchScheduledChargeTotal( $objDatabase, $arrintArTriggerIds = NULL, $boolCheckIsHidden = false ) {

		$strSql = 'SELECT
						sum( sc.charge_amount )
					FROM
						scheduled_charges sc
						JOIN leases l ON (l.active_lease_interval_id = sc.lease_interval_id AND l.cid = sc.cid )';
			if( true == $boolCheckIsHidden ) {
					$strSql .= ' JOIN ar_codes ac ON ( sc.cid = ac.cid AND sc.ar_code_id = ac.id AND ac.hide_charges = false )';
			}
		$strSql .= ' WHERE
						sc.lease_id = ' . ( int ) $this->m_intId . '
						AND sc.deleted_on IS NULL
						AND sc.cid = ' . ( int ) $this->getCid() . '
						AND sc.charge_start_date <= \'' . date( 'm/d/Y' ) . '\'
						AND( sc.charge_end_date >= \'' . date( 'm/d/Y' ) . '\' OR sc.charge_end_date IS NULL )
						AND ( ( sc.charge_end_date <> sc.posted_through_date OR sc.posted_through_date IS NULL OR sc.charge_end_date IS NULL ) OR ( sc.charge_end_date = sc.posted_through_date ) )
						AND sc.is_unselected_quote IS FALSE';

		if( true == valArr( $arrintArTriggerIds ) ) {
			$strSql .= '  AND sc.ar_trigger_id IN( ' . implode( ', ', $arrintArTriggerIds ) . ' )';
		}

		$arrstrBalance = fetchData( $strSql, $objDatabase );

		if( 0 == $arrstrBalance[0]['sum'] ) {
			$strSql = 'SELECT
							sum( sc.charge_amount )
						FROM
							scheduled_charges sc';
			if( true == $boolCheckIsHidden ) {
					$strSql .= ' JOIN ar_codes ac ON ( sc.cid = ac.cid AND sc.ar_code_id = ac.id AND ac.hide_charges = false )';
			}
						$strSql .= ' WHERE
							sc.lease_id = ' . ( int ) $this->m_intId . '
							AND sc.deleted_on IS NULL
							AND sc.cid = ' . ( int ) $this->getCid() . '
							AND sc.charge_start_date > \'' . date( 'm/d/Y' ) . '\'
							AND sc.is_unselected_quote IS FALSE';

			if( true == valArr( $arrintArTriggerIds ) ) {
				$strSql .= '  AND sc.ar_trigger_id IN( ' . implode( ', ', $arrintArTriggerIds ) . ' )';
			}

			$arrstrBalance = fetchData( $strSql, $objDatabase );
		}

		$this->m_fltScheduledChargeTotal = sprintf( '%01.2f', $arrstrBalance[0]['sum'] );

		return $this->m_fltScheduledChargeTotal;
	}

	public function fetchUnitSpaceByUnitNumberAndSpaceNumber( $strUnitNumberSpaceNumber, $objDatabase ) {

		if( false == valStr( trim( $strUnitNumberSpaceNumber ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_space_id', __( 'Unable to find unit. Please use unit selector to find desired unit.' ) ) );
			return NULL;
		}

		// Attempt to split up the information into unit number and space number
		$arrmixMatches = [];
		\Psi\CStringService::singleton()->preg_match( '/(\w+)\W*(\w*)/', $strUnitNumberSpaceNumber, $arrmixMatches );

		$strUnitNumber  = trim( $arrmixMatches[1] );
		$strSpaceNumber = trim( $arrmixMatches[2] );

		$arrobjUnitSpaces = [];

		if( 0 < strlen( $strSpaceNumber ) ) {
			$arrobjUnitSpaces = CUnitSpaces::createService()->fetchUnitSpacesByUnitNumberSpaceNumberPropertyIdByCid( $strUnitNumber, $strSpaceNumber, $this->getPropertyId(), $this->getCid(), $objDatabase );
		} else {
			$arrobjUnitSpaces = CUnitSpaces::createService()->fetchUnitSpacesByUnitNumberPropertyIdByCid( $strUnitNumber, $this->getPropertyId(), $this->getCid(), $objDatabase );
		}

		if( 0 == \Psi\Libraries\UtilFunctions\count( $arrobjUnitSpaces ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_space_id', __( 'Unable to find unit. Please use unit selector to find desired unit.' ) ) );
			return NULL;

		} elseif( 1 < \Psi\Libraries\UtilFunctions\count( $arrobjUnitSpaces ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_space_id', __( 'More than one unit matched your criteria. Please use unit selector to find desired unit.' ) ) );
			return NULL;
		}

		// it is exactly one that was returned from the db
		return array_shift( $arrobjUnitSpaces );
	}

	public function fetchIntegrationClientType( $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClientTypes::createService()->fetchIntegrationClientTypeByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function fetchLeaseCustomersWithDetailsByLeaseIdByLeaseStatusTypeIdsByPropertyIds( $arrintLeaseStatusTypeIds, $arrintPropertyIds, $objDatabase, $boolIsStudentSemesterSelectionEnabled = false, $boolUseApplicationFloorplanIfUnitNotAssigned = false, $intCustomerId = NULL ) {
		// @TODO Test it thoroughly in second phase refactoring
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersWithDetailsByLeaseIdsByLeaseStatusTypeIdsByPropertyIdsByCid( ( array ) $this->getId(), $arrintLeaseStatusTypeIds, $arrintPropertyIds, $this->getCid(), $objDatabase, $boolIsStudentSemesterSelectionEnabled, false, false, $boolUseApplicationFloorplanIfUnitNotAssigned, $intCustomerId );
	}

	public function fetchLeaseUnitSpaceNumbers( $objDatabase ) {
		return CLeaseUnitSpaces::fetchLeaseUnitSpaceNumbersByLeaseIdByPropertyIdByCid( $this->getId(), $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpaces( $objDatabase, $intLeaseTermId = NULL, $arrintSpaceConfigurationIds = [], $boolIsCheckHaveAmenityAssigned = false, $boolConsiderLeaseTermAvailability = false ) {
		return CUnitSpaces::createService()->fetchUnitSpacesByLeaseIdByPropertyUnitIdByPropertyIdByCid( $this->getId(), $this->getPropertyUnitId(), $this->getPropertyId(), $this->getCid(), $objDatabase, false, $intLeaseTermId, $arrintSpaceConfigurationIds, $boolIsCheckHaveAmenityAssigned, $this->getLeaseStartWindowId(), $boolConsiderLeaseTermAvailability );
	}

	public function fetchLeaseUnitSpaces( $objDatabase, $boolSkipPrimaryUnitSpace = false ) {
		return CLeaseUnitSpaces::fetchActiveLeaseUnitSpacesByLeaseIdByPropertyIdByCid( $this->getId(), $this->getPropertyId(), $this->getCid(), $objDatabase, false, $boolSkipPrimaryUnitSpace );
	}

	public function fetchLeaseCustomersCountByLeaseIdByLeaseStatusTypeIdsByCustomerTypeIdsByPropertyIds( $arrintLeaseStatusTypeIds, $arrintCustomerTypeIds, $intLeaseId, $arrintPropertyIds, $objDatabase ) {
		// @TODO Test it thoroughly in second phase refactoring
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersCountByLeaseIdByLeaseStatusTypeIdsByCustomerTypeIdsByPropertyIdsByCid( $intLeaseId, $arrintLeaseStatusTypeIds, $arrintCustomerTypeIds, $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchPetRateAssociations( $objDatabase, $boolIsProperty = false ) {
		$arrintUnitSpaceIds = [];
        $intUnitSpaceId = $this->getUnitSpaceId();
		if( 0 < ( int ) $intUnitSpaceId ) {
			$arrintUnitSpaceIds[$this->getUnitSpaceId()] = $this->getUnitSpaceId();
		}
		return \Psi\Eos\Entrata\Custom\CPetRateAssociations::createService()->fetchPetRateAssociationsByPropertyIdByUnitSpaceIdsWithPetTypeNameByCid( $this->getPropertyId(), $arrintUnitSpaceIds, $this->getCid(), $objDatabase, $boolIsProperty );
	}

	public function fetchAddOnScheduledChargeIdsByLeaseIdByCid( $objDatabase, $arrintExcludeLeaseStatusTypeIds = [], $boolExcludeAddOnsAttachedToUnit = false ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchAddonScheduledChargeIdsByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase, $arrintExcludeLeaseStatusTypeIds, $boolExcludeAddOnsAttachedToUnit );
	}

	public function fetchAddOnRecurringScheduledChargesByLeaseIdByCid( $objDatabase, $arrintAddOnLeaseAssociationIds = NULL ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchAddOnRecurringScheduledChargesByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase, $arrintAddOnLeaseAssociationIds );
	}

	public function fetchAddOnScheduledChargesByLeaseIdByCid( $objDatabase, $arrintAddOnLeaseAssociationIds = NULL ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchAddOnScheduledChargesByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase, $arrintAddOnLeaseAssociationIds );
	}

	public function fetchLeaseCustomerVehicles( $objDatabase ) {
		return CLeaseCustomerVehicles::fetchActiveLeaseCustomerVehiclesByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCustomerPets( $objDatabase ) {
		// MEGARATES_COMMENTS:fetchCustomCustomerAnimalsByLeaseIdByCid renamed to fetchCustomerPetsByLeaseIdByLeaseIntervalIdByCid
		// MEGARATES CHANGES: Replacement of CLease::fetchCustomerAnimals
		$intLeaseIntervalId = $this->getActiveLeaseIntervalId();
		$objLeaseInterval 	= $this->getOrFetchActiveLeaseInterval( $objDatabase );

		if( true == valObj( $objLeaseInterval, 'CLeaseInterval' ) && CLeaseIntervalType::MONTH_TO_MONTH == $objLeaseInterval->getLeaseIntervalTypeId() ) {
			$intLeaseIntervalId = $objLeaseInterval->getParentLeaseIntervalId();
		}

		return \Psi\Eos\Entrata\CCustomerPets::createService()->fetchCustomerPetsByLeaseIdByLeaseIntervalIdByCid( $this->getId(), $intLeaseIntervalId, $this->getCid(), $objDatabase );
	}

	public function fetchInsurancePolicies( $objInsurancePortalDatabase ) {
		return \Psi\Eos\Insurance\CInsurancePolicies::createService()->fetchInsurancePoliciesByLeaseId( $this->getId(), $objInsurancePortalDatabase );
	}

	public function fetchCustomersByCustomerIds( $arrintCustomerIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersByLeaseIdsByCustomerIdsByCid( ( array ) $this->getId(), $arrintCustomerIds, $this->getCid(), $objDatabase );
	}

	public function fetchSpecials( $objDatabase ) {
		return \Psi\Eos\Entrata\CSpecials::createService()->fetchAppliedSpecials( $this, $objDatabase );
	}

	public function fetchIntegratedFiles( $objDatabase ) {
		return CFiles::fetchIntegratedFilesByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchNonIntegratedFiles( $objDatabase ) {
		return CFiles::fetchNonIntegratedFilesByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchLeaseIntervalsByIntervalDatetime( $strIntevalDatetime, $objDatabase, $boolIsMonthToMonth = true ) {
		return \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLeaseIntervalsByLeaseIdByIntervalDatetimeByCid( $this->getId(), $strIntevalDatetime, $this->getCid(), $objDatabase, $boolIsMonthToMonth );
	}

// 	public function fetchEventByEventReferenceTypeIdByEventTypeId( $intEventReferenceTypeId, $intEventTypeId, $objDatabase ) {
// 		return \Psi\Eos\Entrata\CEvents::createService()->fetchEventByEventReferenceTypeIdByEventTypeIdByLeaseIdByCid( $intEventReferenceTypeId, $intEventTypeId, $this->getId(), $this->getCid(), $objDatabase );
// 	}

	public function fetchEventByEventTypeId( $intEventTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchEventByEventTypeIdByPropertyIdByLeaseIdByCid( $intEventTypeId, $this->getPropertyId(), $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchApplicants( $objDatabase, $boolIncludeDeletedAA = false ) {
		return CApplicants::fetchSimpleApplicantsByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIncludeDeletedAA );
	}

	public function fetchApplicantApplications( $objDatabase ) {
		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationsByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchFutureRenewalApplication( $objDatabase ) {
		return CApplications::fetchFutureRenewalApplicationByLeaseIdByLeaseEndDateByCid( $this->getId(), $this->getLeaseEndDate(), $this->getCid(), $objDatabase );
	}

	public function fetchExtendedRenewalOfferCount( $objDatabase ) {
		return CApplications::fetchExtendedRenewalOfferCountByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchSignedFileSignaturesByFileIdByPageNumber( $intFileId, $intPageNumber, $objDatabase ) {
		return \Psi\Eos\Entrata\CFileSignatures::createService()->fetchSignedFileSignaturesByLeaseIdByFileIdByPageNumberByCid( $this->getId(), $intFileId, $intPageNumber, $this->getCid(), $objDatabase );
	}

	public function fetchSignedFileSignaturesBySystemCode( $strSystemCode, $objDatabase ) {
		return \Psi\Eos\Entrata\CFileSignatures::createService()->fetchSignedFileSignaturesByLeaseIdBySystemCodeByCid( $this->getId(), $strSystemCode, $this->getCid(), $objDatabase );
	}

	public function fetchSignedLeaseCustomersByFileIdByPageNumber( $intFileId, $intPageNumber, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseSignedLeaseCustomersByLeaseIdByFileIdByPageNumberByCid( $this->getId(), $intFileId, $intPageNumber, $this->getCid(), $objDatabase );
	}

	/**
	 * Transaction Related Fetch Functions
	 */

	public function fetchArAllocationsForUnallocatedCharges( $objDatabase, $arrintGlAccountTypeIds = NULL, $intLedgerFilterId = NULL, $intExcludeArPaymentTransactionId = NULL, $boolExcludeRepaymentTransactions = false ) {
		$this->m_arrobjArAllocations = \Psi\Eos\Entrata\CArAllocations::createService()->createArAllocationsForUnallocatedChargeTransactionsByLeaseIdByCid( $this->getId(), $this->getCid(), $arrintGlAccountTypeIds, $objDatabase, $boolListLiabilitiesLast = false, $arrintArTransactions = [], $intArProcessId = NULL, $intLedgerFilterId, $intExcludeArPaymentTransactionId, $intRepaymentId = NULL, $boolExcludeRepaymentTransactions );
		return $this->m_arrobjArAllocations;
	}

	// @TODO: THIS FUNCTION IS NOT IN USE.

	public function fetchArAllocationsForUnallocatedPaymentsAndConcessions( $objDatabase ) {
		$this->m_arrobjArAllocations = \Psi\Eos\Entrata\CArAllocations::createService()->fetchArAllocationsForUnallocatedPaymentsAndConcessionsByLeaseIdByCid( $this->getId(), $this->getCid(), NULL, $objDatabase );
		return $this->m_arrobjArAllocations;
	}

	public function fetchArTransactions( $objArTransactionsFilter, $objDatabase, $intLimit = NULL, $intOffset = 0 ) {
		return $this->m_arrobjArTransactions = \Psi\Eos\Entrata\CArTransactions::createService()->fetchViewArTransactionsByLeaseIdByCid( $this->getId(), $this->getCid(), $objArTransactionsFilter, $objDatabase, $intLimit, $intOffset );
	}

	public function fetchUnallocatedPaymentAndConcessionTransactions( $objDatabase, $intLedgerFilterId = NULL, $strChargePostMonth = NULL, $boolExcludeTemporary = false, array $arrintIncludeDefaultArCodeIds = [], $boolIncludeHeaderNumber = false, $arrintExcludedArCodeTypes = [] ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchUnallocatedLeasePaymentsAndConcessionsLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase, $intLedgerFilterId, $strChargePostMonth, $boolExcludeTemporary, $arrintIncludeDefaultArCodeIds, $boolIncludeHeaderNumber, $arrintExcludedArCodeTypes );
	}

	public function fetchSummarizedArTransactions( $objDatabase, $intLedgerFilterId = NULL, $boolIsExcludeTemporary = false, $arrintIncludeDefaultArCodeIds = [], $boolIsOnlyUnappliedPaymentsAndCredits = true, $boolIsOnlyDeposits = false, $boolIsFetchOpenItems = false, $arrintExcludedArCodeTypes = [] ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchSummarizedArTransactionsByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase, $intLedgerFilterId, $boolIsExcludeTemporary, $arrintIncludeDefaultArCodeIds, $boolIsOnlyUnappliedPaymentsAndCredits, $boolIsOnlyDeposits, $boolIsFetchOpenItems, false, $arrintExcludedArCodeTypes );
	}

	public function fetchUnallocatedChargeTransactions( $objDatabase, $boolIsNotTemporaryCharge = false, $intLedgerFilterId = NULL, $boolExcludeRepaymentTransactions = false, $arrintExcludedArTriggerIds = [], $arrintExcludedArCodeTypeIds = [], $boolExcludePaymentTransactions = false ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchUnallocatedChargesByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsNotTemporaryCharge, $intLedgerFilterId, $boolExcludeRepaymentTransactions, $arrintExcludedArTriggerIds, $arrintExcludedArCodeTypeIds, $boolExcludePaymentTransactions );
	}

	public function fetchUnallocatedPaymentsAndConcessionsTotal( $objDatabase, $intLedgerFilterId = NULL, $boolExcludeTemporary = false, array $arrintIncludeDefaultArCodeIds = [], $arrintExcludedArCodeTypes = [] ) {
		$this->m_fltUnappliedCreditsAndPaymentsBalance = \Psi\Eos\Entrata\CArTransactions::createService()->fetchUnallocatedPaymentsAndConcessionsTotalByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase, $intLedgerFilterId, $boolExcludeTemporary, $arrintIncludeDefaultArCodeIds, $arrintExcludedArCodeTypes );
		return $this->m_fltUnappliedCreditsAndPaymentsBalance;
	}

	public function fetchDepositArTransactions( $objDatabase, $intDefaultResidentLedgerFilterId = NULL, $boolRefundableDeposits = false ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchDepositArTransactionsByLeaseIdsByCid( [ $this->m_intId ], $this->getCid(), $objDatabase, $boolIncludeDeleted = false, $intDefaultResidentLedgerFilterId, $boolRefundableDeposits );
	}

	public function fetchArTransaction( $intArTransactionId, $objDatabase ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionByLeaseIdByIdByCid( $this->getId(), $intArTransactionId, $this->getCid(), $objDatabase );
	}

	public function fetchArPayment( $intArPaymentId, $objPaymentDatabase ) {
		return CArPayments::fetchArPaymentByLeaseIdByIdByCid( $this->getId(), $intArPaymentId, $this->getCid(), $objPaymentDatabase );
	}

	public function fetchArTransactionReimbursementDetails( $arrintArTransactionIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionReimbursementDetailsByLeaseIdByIdsByCid( $this->getId(), $arrintArTransactionIds, $this->getCid(), $objDatabase );
	}

	public function fetchArTransactionReimbursementPaymentDetails( $arrintArTransactionIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionReimbursementPaymentDetailsByLeaseIdByIdsByCid( $this->getId(), $arrintArTransactionIds, $this->getCid(), $objDatabase );
	}

	public function fetchNonIntegratedApplicationCharges( $objDatabase ) {
		return $this->m_arrobjArTransactions = \Psi\Eos\Entrata\CArTransactions::createService()->fetchApplicationChargesByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase, true );
	}

	public function fetchNonIntegratedInsuranceCharges( $objDatabase ) {
		return $this->m_arrobjArTransactions = \Psi\Eos\Entrata\CArTransactions::createService()->fetchInsuranceChargesByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase, true );
	}

	public function fetchCustomerDisplayArTransactions( $objArTransactionsFilter, $objDatabase, $boolIsFetchScheduledChargeId = false ) {
		$this->m_arrobjArTransactions = \Psi\Eos\Entrata\CArTransactions::createService()->fetchCustomerDisplayArTransactionsByLeaseIdByCid( $this->getId(), $this->getCid(), $objArTransactionsFilter, $objDatabase, $boolIsFetchScheduledChargeId );
		return $this->m_arrobjArTransactions;
	}

	// Trying to write an optional function for fetch And RollbackCustomerDisplayArTransactions to fetch only balance and not the transactions because fetchAndRollbackCustomerDisplayArTransactions is expensive function - SC

	public function fetchBalance( $objArTransactionsFilter, $objDatabase, $boolFetchWriteOffBalance = false, $boolIsFromResidentPortal = false ) {

		if( false == valObj( $objArTransactionsFilter, 'CArTransactionsFilter' ) ) {
			trigger_error( 'ar transaction filter failed to load.', E_USER_ERROR );
		}

		$strColumnName = ( true == $boolIsFromResidentPortal ) ? 'transaction_amount_due' : 'transaction_amount';

		$strSql = 'SELECT
						sum( ' . $strColumnName . ' )
					FROM
						ar_transactions at,
						ar_codes ac
					WHERE
						at.cid = ac.cid
						AND ac.id = at.ar_code_id
						AND at.lease_id = ' . ( int ) $this->m_intId . '
						AND at.cid = ' . ( int ) $this->getCid() . ' ';

		if( true == $objArTransactionsFilter->getExcludeNonIntegratedTransactions() ) {
			$strSql .= ' AND at.remote_primary_key IS NOT NULL';
		}

		if( true == $objArTransactionsFilter->getHideHiddens() ) {

			$strSql .= ' AND ( at.transaction_amount < 0 OR ac.hide_charges = false )
						AND ( at.transaction_amount > 0 OR ac.hide_credits = false ) ';
		}

		if( true == $boolIsFromResidentPortal ) {
			$strSql .= ' AND at.is_deleted = false';
		}

		$strSql 	.= ( true == $objArTransactionsFilter->getHideTemporary() ) ? ' AND at.is_temporary <> true ' : '';
		$strSql 	.= ( true == valId( $objArTransactionsFilter->getLedgerFilterId() ) ) ? ' AND ac.ledger_filter_id = ' . ( int ) $objArTransactionsFilter->getLedgerFilterId() : '';

		$arrstrBalance 		= fetchData( $strSql, $objDatabase );
		$this->m_fltBalance	= sprintf( '%01.2f', $arrstrBalance[0]['sum'] );

		if( true == $boolFetchWriteOffBalance ) {
			// Fetch write-off balance
			$this->fetchWriteOffBalance( $objDatabase );
		}

		return $this->m_fltBalance;
	}

	public function fetchWriteOffBalance( $objDatabase ) {
		$strSql = 'SELECT
						sum( transaction_amount )
					FROM
						ar_transactions at
					WHERE
						at.ar_trigger_id IN ( ' . CArTrigger::BAD_DEBT_WRITE_OFF . ', ' . CArTrigger::BAD_DEBT_RECOVERY . ' )
						AND at.lease_id = ' . ( int ) $this->m_intId . '
						AND at.cid = ' . ( int ) $this->getCid() . '
						AND at.is_deleted = false';

		$arrstrWriteOffBalance 		= fetchData( $strSql, $objDatabase );
		$this->m_fltWriteOffBalance	= sprintf( '%01.2f', abs( $arrstrWriteOffBalance[0]['sum'] ) );

		return $this->m_fltWriteOffBalance;
	}

	// This function is used exclusively in integration to get the balance to compare against imported balances.
	// That way we can decide if we need to delete old trans and reimport.

	public function fetchSimpleBalance( $objArTransactionsFilter, $objDatabase ) {

		if( false == valObj( $objArTransactionsFilter, 'CArTransactionsFilter' ) ) {
			trigger_error( 'ar transaction filter failed to load.', E_USER_ERROR );
		}

		$strSql	= 'SELECT sum(transaction_amount) FROM ar_transactions WHERE lease_id = ' . ( int ) $this->m_intId . ' AND cid = ' . ( int ) $this->getCid();
		$strSql	.= ( true == $objArTransactionsFilter->getHideTemporary() ) ? ' AND is_temporary <> true ' : '';

		$arrstrBalance = fetchData( $strSql, $objDatabase );

		$this->m_fltBalance = sprintf( '%01.2f', $arrstrBalance[0]['sum'] );

		return $this->m_fltBalance;
	}

	// This function is used exclusively in integration to get the balance to compare against imported balances.
	// That way we can decide if we need to delete old trans and reimport.

	public function fetchArTransactionsCount( $objArTransactionsFilter, $objDatabase ) {

		if( false == valObj( $objArTransactionsFilter, 'CArTransactionsFilter' ) ) {
			trigger_error( 'ar transaction filter failed to load.', E_USER_ERROR );
		}

		$strSql	= 'SELECT count(id) as count FROM ar_transactions WHERE lease_id = ' . ( int ) $this->m_intId . ' AND cid = ' . ( int ) $this->getCid();
		$strSql	.= ( true == $objArTransactionsFilter->getHideTemporary() ) ? ' AND is_temporary <> true ' : '';

		$arrstrBalance = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrstrBalance[0]['count'] ) ) {
			return $arrstrBalance[0]['count'];
		} else {
			return 0;
		}
	}

	// This function is used exclusively in integration to get the all transactions to compare against imported transactions.
	// That way we can decide if we need to delete old trans and reimport.

	public function fetchAllArTransactions( $objArTransactionsFilter, $objDatabase ) {

		if( false == valObj( $objArTransactionsFilter, 'CArTransactionsFilter' ) ) {
			trigger_error( 'ar transaction filter failed to load.', E_USER_ERROR );
		}

		$strSql	= 'SELECT * FROM ar_transactions WHERE lease_id = ' . ( int ) $this->m_intId . ' AND cid = ' . ( int ) $this->getCid();
		$strSql	.= ( true == $objArTransactionsFilter->getHideTemporary() ) ? ' AND is_temporary <> true ' : '';

		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactions( $strSql, $objDatabase );
	}

	public function fetchUnallocatedChargeAmount( $objDatabase, $objArTransactionsFilter = NULL ) {

		$strSql = 'SELECT
						sum( vat.transaction_amount_due )
					FROM
						view_ar_transactions vat
					WHERE
						vat.cid = ' . ( int ) $this->getCid() . '
						AND vat.lease_id = ' . ( int ) $this->getId() . '
						AND vat.transaction_amount_due > 0';

		if( true == valObj( $objArTransactionsFilter, 'CArTransactionsFilter' ) ) {

			$strSql		.= ( true == $objArTransactionsFilter->getHideTemporary() ) ? ' AND vat.is_temporary <> true ' : '';
			$strSql		.= ( true == valId( $objArTransactionsFilter->getLedgerFilterId() ) ) ? ' AND vat.ledger_filter_id = ' . ( int ) $objArTransactionsFilter->getLedgerFilterId() : '';
			$strSql		.= ( false == valArr( $objArTransactionsFilter->getExcludedArTriggerIds() ) ) ? '' : ' AND vat.ar_trigger_id NOT IN ( ' . implode( ',', $objArTransactionsFilter->getExcludedArTriggerIds() ) . ' ) ';
			$strSql		.= ( false == valArr( $objArTransactionsFilter->getExcludedArCodeTypeIds() ) ) ? '' : ' AND vat.ar_code_type_id NOT IN ( ' . implode( ',', $objArTransactionsFilter->getExcludedArCodeTypeIds() ) . ' ) ';
		}

		$arrstrUnallocatedFund = fetchData( $strSql, $objDatabase );

		$fltUnallocatedFund = ( 0 < ( float ) $arrstrUnallocatedFund[0]['sum'] ) ?( $arrstrUnallocatedFund[0]['sum'] ) : 0.00;

		return  sprintf( '%01.2f', $fltUnallocatedFund );
	}

	public function fetchAndRollbackCustomerDisplayArTransactions( $objArTransactionsFilter, $strPostMonth, $strPostDate, $intCompanyUserId, $objDatabase, $boolIsFetchScheduledChargeId = false, $intLimit = NULL, $intOffset = 0 ) {
		// As per task (#281087) we should delete temporary ar transactions assocciated to scheduled charges before loading ledger balance
		\Psi\Core\Utility\CDeleteTemporaryUtilityTransactions::createService()->deleteTemporaryArTransactions( [ $this->getPropertyId() ], $this->getCid(), $this->m_objDatabase, [ $this->getId() ] );

		$objDatabase->begin();

		if( false == CValidation::validateISODate( $strPostMonth ) || false == CValidation::validateISODate( $strPostDate ) ) {
			$this->getOrFetchPropertyGlSetting( $objDatabase );

			if( false == valObj( $this->m_objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
				return NULL;
			}

			$strPostMonth = $this->m_objPropertyGlSetting->getArPostMonth();
			$strPostDate = $this->m_objPropertyGlSetting->getArPostMonth();
			if( false == $this->m_objPropertyGlSetting->getActivateStandardPosting() ) {
				$strPostDate = date( 'm/d/Y' );
				$strPostMonth = date( 'm/01/Y' );
			}
		}

		// SHOULD WE BE ADJUSTING THE LEDGER AMOUNT TO INCLUDE UNEXPORTED AR PAYMENTS?

		if( false == valObj( $objArTransactionsFilter, 'CArTransactionsFilter' ) ) {
			trigger_error( 'ar transaction filter failed to load.', E_USER_ERROR );
		}

		// This only happens in Resident Portal usually
		if( true == $objArTransactionsFilter->getPerformPsuedoAllocations() ) {

			$objIntegrationClientKeyValue	= ( true == valStr( $this->getRemotePrimaryKey() ) && 0 < ( int ) $this->getIntegrationDatabaseId() ) ? \Psi\Eos\Entrata\CIntegrationClientKeyValues::createService()->fetchIntegrationClientKeyValueByKeyByIntegrationDatabaseIdByCid( 'IMPORT_FULL_LEDGER', $this->getIntegrationDatabaseId(), $this->getCid(), $objDatabase ) : NULL;

			// Handle the unallocated initial_import transactions for integrated properties - Import Full Ledger
			if( true == valObj( $objIntegrationClientKeyValue, 'CIntegrationClientKeyValue' ) ) {
				$strSql = 'UPDATE ar_transactions at 
							SET initial_amount_due = transaction_amount_due 
							FROM leases l
							WHERE
								at.cid = ' . ( int ) $this->getCid() . '
								AND at.lease_id = ' . ( int ) $this->getId() . '
								AND at.cid = l.cid
								AND at.lease_id = l.id
								AND l.remote_primary_key IS NOT NULL
								AND at.is_initial_import = TRUE 
								AND at.initial_amount_due <> at.transaction_amount_due;';

				$objDataset = $objDatabase->createDataset();
				if( ( false == $objDataset->execute( $strSql ) ) ) {
					trigger_error( 'The following message was returned from database: ' . $objDatabase->errorMsg(), E_USER_WARNING );

					$objDataset->cleanup();
					$objDatabase->rollback();
					$objDatabase->begin();
				}
			}

			// Now that we have posted the items above, we need to psuedo allocate the ledger(which we will rollback).  This will make sure items are all allocated.
			if( false == $this->autoAllocate( $intCompanyUserId, $strPostMonth, $strPostDate, $objDatabase ) ) {
				trigger_error( 'Failed to allocate the ledger. Client : ' . $this->getCid() . ', Property : ' . $this->getPropertyId() . ', postmonth : ' . $strPostMonth . ', postdate : ' . $strPostDate . ', lease id : ' . $this->getId() . ', The following message was returned from database: ' . $objDatabase->errorMsg(), E_USER_WARNING );
				$objDatabase->rollback();
				$objDatabase->begin();
			}
		}

		if( true == $objArTransactionsFilter->getHideHiddens() && false == $objArTransactionsFilter->getIds() ) {
			$objLedgerFilter = \Psi\Eos\Entrata\CLedgerFilters::createService()->fetchLedgerFilterByDefaultLedgerFilterIdByCid( CDefaultLedgerFilter::RESIDENT, $this->getCid(), $objDatabase );
			if( true == valObj( $objLedgerFilter, 'CLedgerFilter' ) ) {
				$objArTransactionsFilter->setLedgerFilterId( $objLedgerFilter->getId() );
			}
			// This gets us summarized and non-hidden ar transactions based on charge code groups
			$this->m_arrobjArTransactions = $this->fetchCustomerDisplayArTransactions( $objArTransactionsFilter, $objDatabase, $boolIsFetchScheduledChargeId );
		} else {
			// This gets us the raw balance without any additional manipulation
			$this->m_arrobjArTransactions = $this->fetchArTransactions( $objArTransactionsFilter, $objDatabase, $intLimit, $intOffset );
		}

		$objDatabase->rollback();

		$fltBalance 				= 0;
		$fltMiniminumPaymentAmount 	= 0;

		// Set minimum payment amount, and the balance due.
		if( true == valArr( $this->m_arrobjArTransactions ) ) {
			foreach( $this->m_arrobjArTransactions as $objArTransaction ) {
                if( false == $objArTransaction->getHasDependencies() ) {
                    $fltBalance = ( float ) bcadd( $fltBalance, $objArTransaction->getTransactionAmountDue(), 2 );
                }

				// Resident Must Pay Balance in Full
				if( 1 == $objArTransactionsFilter->getBalancePaymentRequirement() ) {
					$fltMiniminumPaymentAmount += ( float ) $objArTransaction->getTransactionAmountDue();

				// Resident Must Pay Rent Balance in Full
				} elseif( 2 == $objArTransactionsFilter->getBalancePaymentRequirement() && CArCodeType::RENT == $objArTransaction->getArCodeTypeId() ) {
					$fltMiniminumPaymentAmount += ( float ) $objArTransaction->getTransactionAmountDue();
				}
			}
		}

		$this->setBalance( $fltBalance );
		$this->setMiniminumPaymentAmount( $fltMiniminumPaymentAmount );

		return $this->m_arrobjArTransactions;
	}

	public function fetchTotalUnallocatedPaymentsAndConcessionsAmount( $objDatabase ) {
		$strSql = 'SELECT sum( transaction_amount_due ) FROM view_ar_transactions WHERE lease_id = ' . ( int ) $this->m_intId . ' AND cid = ' . ( int ) $this->getCid() . ' AND is_deleted = false AND transaction_amount_due < 0 AND is_temporary != true ';

		return fetchData( $strSql, $objDatabase );
	}

	public function fetchScheduledChargesByScheduledChargesFilter( $objScheduledChargesFilter, $objDatabase ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByScheduledChargesFilterByLeaseIdByCid( $objScheduledChargesFilter, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseProcess( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchLeaseProcessByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchLeaseDetail( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseDetails::createService()->fetchLeaseDetailByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnallocatedArTransactions( $objDatabase, $intLedgerFilterId = NULL, $arrintArTransactionIds = [] ) {
		return  \Psi\Eos\Entrata\CArTransactions::createService()->fetchUnallocatedArTransactionsByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase, $intLedgerFilterId, $arrintArTransactionIds );
	}

	public function fetchBadDebtLimitAmount( $objDatabase, $intLedgerFilterId = NULL, $boolExcludePaymentTransactions = false, $boolWriteOffBaseRent = true ) {
		return  \Psi\Eos\Entrata\CArTransactions::createService()->fetchBadDebtLimitAmountByLeaseIdsByCid( [ $this->getId() ], $this->getCid(), $objDatabase, $intLedgerFilterId, $boolExcludePaymentTransactions, $boolWriteOffBaseRent );
	}

	public function fetchBadDebtLimitAmountByArcode( $intArCodeId, $objDatabase ) {
		return  \Psi\Eos\Entrata\CArTransactions::createService()->fetchBadDebtLimitAmountByLeaseIdsByArCodeIdByCid( [ $this->getId() ], $intArCodeId, $this->getCid(), $objDatabase );
	}

	public function fetchReturnedPaymentArTransactions( $objDatabase, $arrstrReturnArTransactionsSortParameter = NULL ) {
		return  \Psi\Eos\Entrata\CArTransactions::createService()->fetchReturnedPaymentArTransactions( $this->getId(), $this->getCid(), $objDatabase, $arrstrReturnArTransactionsSortParameter );
	}

	public function fetchUnitSpaceOccupancyHistoryByLeaseActionId( $intActionId, $objDatabase ) {
		return \Psi\Eos\Entrata\CUnitSpaceOccupancyHistories::createService()->fetchUnitSpaceOccupancyHistoryByLeaseIdByLeaseActionIdByCid( $this->getId(), $intActionId, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseIntervals( $objDatabase, $boolIsOrderByLeaseStartDate = false, $boolIsStudentProperty = false, $boolIsExcludeLeaseModification = false, $boolIsConventionalLease = false ) {
		return \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLeaseIntervalsByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsOrderByLeaseStartDate, $boolIsStudentProperty, $boolIsExcludeLeaseModification, false, $boolIsConventionalLease );
	}

	public function fetchAllUnitSpecificLeaseIntervalsOfActiveLeases( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchAllLeaseIntervalsOfActiveLeasesByUnitSpaceIdByLeaseStartDateByLeaseEndDateByCid( $this->getUnitSpaceId(), $this->getLeaseStartDate(), $this->getLeaseEndDate(), $this->getCid(), $objDatabase );
	}

	public function fetchLeaseBalanceDueDetails( $objDatabase, $boolShowWriteOffDetails = false ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseBalanceDueDetailsByLeaseIdsByCid( [ $this->getId() ], $this->getCid(), $objDatabase, $boolShowWriteOffDetails );
	}

	public function fetchSimpleScheduledChargesTransactionDetailsByArTriggerIds( $arrintArTriggerIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchSimpleScheduledChargesTransactionDetailsByArTriggerIdsByLeaseIdByCid( $arrintArTriggerIds, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchLeaseIntervalByNoticeDate( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLeaseIntervalByLeaseIdByNoticeDateByCid( $this->getId(), $this->getNoticeDate(), $this->getCid(), $objDatabase );
	}

	public function fetchRecurringScheduledChargesByScheduledChargesFilter( $objScheduledChargesFilter, $objDatabase ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchRecurringScheduledChargesByScheduledChargesFilterByLeaseIdByCid( $objScheduledChargesFilter, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchOtherScheduledChargesByScheduledChargesFilter( $objScheduledChargesFilter, $objDatabase ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchOtherScheduledChargesByScheduledChargesFilterByLeaseIdByCid( $objScheduledChargesFilter, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchApplicationByLeaseIntervalId( $intLeaseIntervalId, $objDatabase ) {
		if( false == is_numeric( $intLeaseIntervalId ) ) return NULL;
		return CApplications::fetchApplicationByLeaseIdByLeaseIntervalIdByCid( $this->m_intId, $intLeaseIntervalId, $this->getCid(), $objDatabase );
	}

	public function fetchAddOnLeaseAssociationsByAddOnTypeIds( $arrintAddOnTypeIds, $objDatabase, $boolIsCheckDeleted = true, $arrintLeaseStatusTypeIds = NULL, $boolExcludeAddOnsAttachedToUnit = false ) {
		return \Psi\Eos\Entrata\Custom\CAddOnLeaseAssociations::createService()->fetchNewCustomAddOnLeaseAssociationsByLeaseIdsByAddOnTypeIdsByPropertyIdsByCid( [ $this->getId() ], $arrintAddOnTypeIds, [ $this->getPropertyId() ], $this->getCid(), $objDatabase, $boolIsCheckDeleted, $arrintLeaseStatusTypeIds, $boolExcludeAddOnsAttachedToUnit );
	}

	public function fetchRepayments( $objDatabase, $boolActiveOnly = false, $arrintRepaymentIds = [] ) {
		return \Psi\Eos\Entrata\CRepayments::createService()->fetchAllRepaymentsByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolActiveOnly, $arrintRepaymentIds );
	}

	public function fetchRepaymentsArTransactions( $objDatabase ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchRepaymentArTransactionsByLeaseIdByPropertyIdByCid( $this->getId(), $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function fetchSummarizedScheduledChargesArTransactionsDetailsByArTriggerIds( $arrintArTriggerIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchSummarizedScheduledChargesArTransactionsDetailsByArTriggerIdsByLeaseIdByCid( $arrintArTriggerIds, $this->getId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Validation Functions
	 */

	public function validate( $strAction, $objDatabase = NULL, $objCompanyUser = NULL, $objLeaseIntervalForGivenPerNoticeDate = NULL, $boolIsStudentSemesterSelectionEnabled = false, $arrmixDataToValidate = NULL ) {
		$boolIsValid = true;

		$objLeaseValidator = new CLeaseValidator();
		$objLeaseValidator->setLease( $this );

		$boolIsValid &= $objLeaseValidator->validate( $strAction, $objDatabase, $objCompanyUser, $objLeaseIntervalForGivenPerNoticeDate, $boolIsStudentSemesterSelectionEnabled, $arrmixDataToValidate );

		return $boolIsValid;
	}

	// @Fixme remove this function once all root causes are fixed

	public function validatePrimaryCustomerType( $objDatabase ) {

		// @TODO Test it thoroughly in second phase refactoring
		$intCustomerCount = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchCountOfNonPrimaryCustomersByPrimaryCustomerIdByLeaseIdByPropertyIdByCid( $this->getPrimaryCustomerId(), $this->getId(), $this->getPropertyId(), $this->getCid(), $objDatabase );

		if( 0 < $intCustomerCount ) {
			trigger_error( 'Customer Id::' . $this->getPrimaryCustomerId() . ' is recorded as the primary customer but is not of a Primary occupant type.', E_USER_ERROR );
		}
	}

	/**
	 * Other Functions
	 */

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == is_null( $this->getUnitSpaceId() ) || true == is_numeric( $this->getUnitSpaceId() ) ) {
			$objUnitSpace = $this->getOrFetchUnitSpace( $objDatabase );

			if( true == valObj( $objUnitSpace, 'CUnitSpace' ) ) {
				$this->setUnitNumberCache( $objUnitSpace->getUnitNumberCache() );
			}
		}

		// Insert Lease Interval
		// Insert Application
		// Insert Applicant
		// Insert Applicant Application
		// Insert Customers
		// Insert Leases, Lease details, lease processes
		// Insert Lease Customers

		if( false == parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) return false;

		// this is not the right logic for assigning the active lease inteval = Dave
		$this->refreshField( 'active_lease_interval_id', 'leases', $objDatabase );

		if( true == is_null( $this->getActiveLeaseIntervalId() ) ) return false;

		if( true == $this->getIsSyncApplicationAndLease() && true == is_null( $this->getApplicationId() ) ) {

			$this->m_objApplication = CApplicationUtils::loadOrCreateApplication( $this, $objDatabase );
			$objLeaseInterval		= $this->getOrFetchActiveLeaseInterval( $objDatabase );

			$this->m_objApplication->setLeaseIntervalId( $objLeaseInterval->getId() );
			$this->m_objApplication->setLeaseEndDate( $objLeaseInterval->getLeaseEndDate() );

			// For an assigning floor plan at the time of adding a new resident
			if( true == valObj( $this->m_objUnitSpace, 'CUnitSpace' ) && true == valStr( $this->m_objUnitSpace->getPropertyFloorplanId() ) ) {
				$this->m_objApplication->setPropertyFloorplanId( $this->m_objUnitSpace->getPropertyFloorplanId() );
				$this->m_objApplication->setUnitTypeId( $this->m_objUnitSpace->getUnitTypeId() );
			}

			if( true == $this->getIsSyncSpaceConfigurationIdToApplication() ) {
				$this->m_objApplication->setSpaceConfigurationId( $this->getSpaceConfigurationId() );
			}

			if( false == valStr( $this->m_objApplication->getId() ) && false == $this->m_objApplication->insert( $intCurrentUserId, $objDatabase ) ) {
				return false;
			}
		}

		return true;
	}

	// This function is used for integration purpose, please do not use/call it from anywhere

	public function insertIntegratedLease( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == is_null( $this->getUnitSpaceId() ) || true == is_numeric( $this->getUnitSpaceId() ) ) {
			$objUnitSpace = $this->getOrFetchUnitSpace( $objDatabase );

			if( true == valObj( $objUnitSpace, 'CUnitSpace' ) ) {
				$this->setUnitNumberCache( $objUnitSpace->getUnitNumberCache() );
			}
		}

		$strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.leases_id_seq\' )' : ( int ) $this->m_intId;

		$strSql = 'INSERT INTO public.leases
					(
						id,
						cid,
						primary_customer_id,
						property_id,
						property_unit_id,
						unit_space_id,
						integration_database_id,
						payment_allowance_type_id,
						active_lease_interval_id,
						remote_primary_key,
						property_name,
						building_name,
						unit_number_cache,
						display_number,
						ledger_last_sync_on,
						occupancy_type_id,
						updated_by,
						updated_on,
						created_by,
						created_on
					) VALUES (
						' . $strId . ',
						' . $this->sqlCid() . ',
						' . $this->sqlPrimaryCustomerId() . ',
						' . $this->sqlPropertyId() . ',
						' . $this->sqlPropertyUnitId() . ',
						' . $this->sqlUnitSpaceId() . ',
						' . $this->sqlIntegrationDatabaseId() . ',
						' . $this->sqlPaymentAllowanceTypeId() . ',
						' . $this->sqlActiveLeaseIntervalId() . ',
						' . $this->sqlRemotePrimaryKey() . ',
						' . $this->sqlPropertyName() . ',
						' . $this->sqlBuildingName() . ',
						' . $this->sqlUnitNumberCache() . ',
						' . $this->sqlDisplayNumber() . ',
						' . $this->sqlLedgerLastSyncOn() . ',
						' . $this->sqlOccupancyTypeId() . ',
						' . ( int ) $intCurrentUserId . ',
						NOW(),
						' . ( int ) $intCurrentUserId . ',
						NOW()
					);';

		$objLeaseProcess = $this->createLeaseProcess();
		$objLeaseProcess->setMoveInDate( $this->getMoveInDate() );
		$objLeaseProcess->setMoveOutDate( $this->getMoveOutDate() );
		$objLeaseProcess->setNoticeDate( $this->getNoticeDate() );
		$strSql .= $objLeaseProcess->insert( $intCurrentUserId, $objDatabase, true );

		$objLeaseDetail = $this->createLeaseDetail();
		$objLeaseDetail->setLeaseTypeId( $this->getLeaseTypeId() );
		$objLeaseDetail->setLateFeePostedOn( $this->getLateFeePostedOn() );
		$strSql .= $objLeaseDetail->insert( $intCurrentUserId, $objDatabase, true );

		if( true == $boolReturnSqlOnly ) return $strSql;

		$strResultValue = fetchData( $strSql, $objDatabase );

		return ( false === $strResultValue ) ? false : true;
	}

	// This function is used for integration purpose, please do not use/call it from anywhere

	public function updateIntegratedLeaseNew( $intCurrentUserId, $objDatabase, $boolIsLeaseProcessUpdate = false, $boolIsLeaseDetailUpdate = false, $boolReturnSqlOnly = false ) {

		$strSql = '';

		// lease_processes update sql
		$intLeaseProcessId = $this->getLeaseProcessId();

		// we should not come inside this conditions, if we are we need to fix the issue.
		if( false == valStr( $intLeaseProcessId ) ) {
			$arrstrLeaseProcesses = fetchData( 'SELECT id FROM lease_processes WHERE lease_processes.cid = ' . ( int ) $this->getCid() . ' AND lease_id = ' . ( int ) $this->getId() . ' AND customer_id IS NULL LIMIT 1', $objDatabase );

			if( true == isset( $arrstrLeaseProcesses[0]['id'] ) ) {
				$intLeaseProcessId = $arrstrLeaseProcesses[0]['id'];
			}
		}

		if( false == valStr( $intLeaseProcessId ) ) {
			$objLeaseProcess 	= $this->createLeaseProcess();
			$objLeaseProcess->setMoveInDate( $this->getMoveInDate() );
			$objLeaseProcess->setMoveOutDate( $this->getMoveOutDate() );
			$objLeaseProcess->setNoticeDate( $this->getNoticeDate() );
			$strSql .= $objLeaseProcess->insert( $intCurrentUserId, $objDatabase, true );
		} elseif( true == $boolIsLeaseProcessUpdate ) {
			$strSql .= 'UPDATE
							lease_processes
						SET
							' . ( ( true == valStr( $this->getMoveInDate() ) ) ? 'move_in_date = \'' . $this->getMoveInDate() . '\',' : ( ( NULL === $this->getMoveInDate() ) ? 'move_in_date = NULL,' : '' ) ) . '
							' . ( ( true == valStr( $this->getMoveOutDate() ) ) ? 'move_out_date = \'' . $this->getMoveOutDate() . '\',' : ( ( NULL === $this->getMoveOutDate() ) ? 'move_out_date = NULL,' : '' ) ) . '
							' . ( ( true == valStr( $this->getNoticeDate() ) ) ? 'notice_date = \'' . $this->getNoticeDate() . '\',' : ( ( NULL === $this->getNoticeDate() ) ? 'notice_date = NULL,' : '' ) ) . '
							' . ( ( true == valStr( $this->getCollectionsStartDate() ) ) ? 'collections_start_date = \'' . $this->getCollectionsStartDate() . '\',' : ( ( NULL === $this->getCollectionsStartDate() ) ? 'collections_start_date = NULL,' : '' ) ) . '
							' . ( ( true == valStr( $this->getTerminationStartDate() ) ) ? 'termination_start_date = \'' . $this->getTerminationStartDate() . '\',' : ( ( NULL === $this->getTerminationStartDate() ) ? 'termination_start_date = NULL,' : '' ) ) . '
							updated_by = ' . ( int ) $intCurrentUserId . ',
							updated_on = NOW()
						WHERE
							id = ' . ( int ) $intLeaseProcessId . '
							AND lease_processes.cid = ' . $this->getCid() . '
							AND customer_id IS NULL;';
		}

		// lease_details update sql
		$intLeaseDetailId = $this->getLeaseDetailId();

		// we should not come inside this conditions, if we are we need to fix the issue.
		if( false == valStr( $intLeaseDetailId ) ) {
			$arrstrLeaseDetails = fetchData( 'SELECT id FROM lease_details WHERE cid = ' . ( int ) $this->getCid() . ' AND lease_id = ' . ( int ) $this->getId() . ' LIMIT 1', $objDatabase );

			if( true == isset( $arrstrLeaseDetails[0]['id'] ) ) {
				$intLeaseDetailId = $arrstrLeaseDetails[0]['id'];
			}
		}

		if( false == valStr( $intLeaseDetailId ) ) {
			$objLeaseDetail = $this->createLeaseDetail();
			$objLeaseDetail->setLateFeePostedOn( $this->getLateFeePostedOn() );
			$strSql .= $objLeaseDetail->insert( $intCurrentUserId, $objDatabase, true );
		} elseif( true == $boolIsLeaseDetailUpdate ) {
			$strSql .= 'UPDATE
							lease_details
						SET
							' . ( ( true == valStr( $this->getLateFeePostedOn() ) ) ? 'late_fee_posted_on = \'' . $this->getLateFeePostedOn() . '\',' : ( ( NULL === $this->getLateFeePostedOn() ) ? 'late_fee_posted_on = NULL,' : '' ) ) . '
							updated_by = ' . ( int ) $intCurrentUserId . ',
							updated_on = NOW()
						WHERE
							id = ' . ( int ) $intLeaseDetailId . '
							AND cid = ' . $this->getCid() . ';';
		}

		// leases update sql
		$strSql .= $this->update( $intCurrentUserId, $objDatabase, true, $boolIsIntegrated = true );

		if( true == $boolReturnSqlOnly ) return $strSql;

		$strResultValue = fetchData( $strSql, $objDatabase );

		return ( false === $strResultValue ) ? false : true;
	}

	public function updateLedgerLastSyncOn( $intCurrentUserId, $objDatabase ) {
		$strSql = 'UPDATE leases
					SET ledger_last_sync_on = ' . $this->sqlLedgerLastSyncOn() . ',
						updated_by = ' . ( int ) $intCurrentUserId . ',
						updated_on = NOW()
					WHERE cid=' . $this->getCid() . ' AND id =' . $this->getId() . ';';

		if( false === fetchData( $strSql, $objDatabase ) ) {
			return false;
		}

		return true;
	}

	// This function is used for integration purpose, please do not use/call it from anywhere

	public function updateIntegratedLease( $intCurrentUserId, $objDatabase, $boolIsLeaseProcessUpdate = false, $boolIsLeaseIntervalUpdate = false, $boolIsLeaseDetailUpdate = false, $boolReturnSqlOnly = false ) {
		$strSql = '';

		// lease_processes update sql
		$intLeaseProcessId = $this->getLeaseProcessId();

		// we should not come inside this conditions, if we are we need to fix the issue.
		if( false == valStr( $intLeaseProcessId ) ) {
			$arrstrLeaseProcesses = fetchData( 'SELECT id FROM lease_processes WHERE lease_processes.cid = ' . ( int ) $this->getCid() . ' AND lease_id = ' . ( int ) $this->getId() . ' AND customer_id IS NULL LIMIT 1', $objDatabase );

			if( true == isset( $arrstrLeaseProcesses[0]['id'] ) ) {
				$intLeaseProcessId = $arrstrLeaseProcesses[0]['id'];
			}
		}

		if( false == valStr( $intLeaseProcessId ) ) {
			$objLeaseProcess 	= $this->createLeaseProcess();
			$objLeaseProcess->setMoveInDate( $this->getMoveInDate() );
			$objLeaseProcess->setMoveOutDate( $this->getMoveOutDate() );
			$objLeaseProcess->setNoticeDate( $this->getNoticeDate() );
			$strSql .= $objLeaseProcess->insert( $intCurrentUserId, $objDatabase, true );
		} elseif( true == $boolIsLeaseProcessUpdate ) {
			$strSql .= 'UPDATE
							lease_processes
						SET
							' . ( ( true == valStr( $this->getMoveInDate() ) ) ? 'move_in_date = \'' . $this->getMoveInDate() . '\',' : ( ( NULL === $this->getMoveInDate() ) ? 'move_in_date = NULL,' : '' ) ) . '
							' . ( ( true == valStr( $this->getMoveOutDate() ) ) ? 'move_out_date = \'' . $this->getMoveOutDate() . '\',' : ( ( NULL === $this->getMoveOutDate() ) ? 'move_out_date = NULL,' : '' ) ) . '
							' . ( ( true == valStr( $this->getNoticeDate() ) ) ? 'notice_date = \'' . $this->getNoticeDate() . '\',' : ( ( NULL === $this->getNoticeDate() ) ? 'notice_date = NULL,' : '' ) ) . '
							' . ( ( true == valStr( $this->getCollectionsStartDate() ) ) ? 'collections_start_date = \'' . $this->getCollectionsStartDate() . '\',' : ( ( NULL === $this->getCollectionsStartDate() ) ? 'collections_start_date = NULL,' : '' ) ) . '
							updated_by = ' . ( int ) $intCurrentUserId . ',
							updated_on = NOW()
						WHERE
							id = ' . ( int ) $intLeaseProcessId . '
							AND lease_processes.cid = ' . $this->getCid() . '
							AND customer_id IS NULL;';
		}

		// lease_details update sql
		$intLeaseDetailId = $this->getLeaseDetailId();

		// we should not come inside this conditions, if we are we need to fix the issue.
		if( false == valStr( $intLeaseDetailId ) ) {
			$arrstrLeaseDetails = fetchData( 'SELECT id FROM lease_details WHERE cid = ' . ( int ) $this->getCid() . ' AND lease_id = ' . ( int ) $this->getId() . ' LIMIT 1', $objDatabase );

			if( true == isset( $arrstrLeaseDetails[0]['id'] ) ) {
				$intLeaseDetailId = $arrstrLeaseDetails[0]['id'];
			}
		}

		if( false == valStr( $intLeaseDetailId ) ) {
			$objLeaseDetail = $this->createLeaseDetail();
			$objLeaseDetail->setLateFeePostedOn( $this->getLateFeePostedOn() );
			$strSql .= $objLeaseDetail->insert( $intCurrentUserId, $objDatabase, true );
		} elseif( true == $boolIsLeaseDetailUpdate ) {
			$strSql .= 'UPDATE
							lease_details
						SET
							' . ( ( true == valStr( $this->getLateFeePostedOn() ) ) ? 'late_fee_posted_on = \'' . $this->getLateFeePostedOn() . '\',' : ( ( NULL === $this->getLateFeePostedOn() ) ? 'late_fee_posted_on = NULL,' : '' ) ) . '
							updated_by = ' . ( int ) $intCurrentUserId . ',
							updated_on = NOW()
						WHERE
							id = ' . ( int ) $intLeaseDetailId . '
							AND cid = ' . $this->getCid() . ';';
		}

		// lease_intervals update sql
		$intLeaseIntervalId	= $this->getActiveLeaseIntervalId();

		// we should not come inside this conditions, if we are we need to fix the issue.
		if( false == is_numeric( $intLeaseIntervalId ) ) {
			$arrstrLeaseIntervals = fetchData( 'SELECT id FROM lease_intervals WHERE lease_intervals.cid = ' . ( int ) $this->getCid() . ' AND lease_id = ' . ( int ) $this->getId() . ' LIMIT 1', $objDatabase );

			if( true == isset( $arrstrLeaseIntervals[0]['id'] ) ) {
				$intLeaseIntervalId = $arrstrLeaseIntervals[0]['id'];
			}
		}

		if( false == valStr( $intLeaseIntervalId ) ) {
			$objLeaseInterval 	= $this->createLeaseInterval();
			$intLeaseIntervalId = $objLeaseInterval->fetchNextId( $objDatabase );

			$objLeaseInterval->setId( $intLeaseIntervalId );
			$objLeaseInterval->setLeaseStartDate( $this->getLeaseStartDate() );
			$objLeaseInterval->setLeaseEndDate( $this->getLeaseEndDate() );
			$objLeaseInterval->setLeaseIntervalTypeId( $this->getLeaseIntervalTypeId() );
			$objLeaseInterval->setLeaseStatusTypeId( $this->getLeaseStatusTypeId() );
			$objLeaseInterval->setLateFeeFormulaId( $this->getLateFeeFormulaId() );
			$objLeaseInterval->setLeaseTermId( $this->getLeaseTermId() );

			$strSql .= $objLeaseInterval->insert( $intCurrentUserId, $objDatabase, true );

		} elseif( true == $boolIsLeaseIntervalUpdate ) {
			$strSql .= 'UPDATE
							lease_intervals
						SET
							' . ( ( true == valStr( $this->getLeaseStartDate() ) ) ? 'lease_start_date = \'' . $this->getLeaseStartDate() . '\',' : '' ) . '
							' . ( ( true == valStr( $this->getLeaseEndDate() ) ) ? 'lease_end_date = \'' . $this->getLeaseEndDate() . '\',' : ( ( NULL === $this->getLeaseEndDate() ) ? 'lease_end_date = NULL,' : '' ) ) . '
							lease_interval_type_id  = ' . ( int ) $this->getLeaseIntervalTypeId() . ',
							' . ( ( true == valStr( $this->getLeaseStatusTypeId() ) ) ? 'lease_status_type_id  = ' . ( int ) $this->getLeaseStatusTypeId() . ',' : '' ) . '
							' . ( ( true == valStr( $this->getLateFeeFormulaId() ) ) ? 'late_fee_formula_Id  = ' . ( int ) $this->getLateFeeFormulaId() . ',' : '' ) . '
							' . ( ( true == valStr( $this->getLeaseTermId() ) ) ? 'lease_term_id  = ' . ( int ) $this->getLeaseTermId() . ',' : '' ) . '
							' . 'updated_by = ' . ( int ) $intCurrentUserId . ',
							updated_on = NOW()
						WHERE
							id = ' . ( int ) $intLeaseIntervalId . '
							AND lease_intervals.cid = ' . ( int ) $this->getCid() . ';';
		}

		$this->setActiveLeaseIntervalId( $intLeaseIntervalId );

		// leases update sql
		$strSql .= $this->update( $intCurrentUserId, $objDatabase, true, $boolIsIntegrated = true );

		if( true == $boolReturnSqlOnly ) return $strSql;

		$strResultValue = fetchData( $strSql, $objDatabase );

		return ( false === $strResultValue ) ? false : true;
	}

	public function logActivityForLeaseInformation( $intEventTypeId, $objDatabase, $objOldReferenceData = NULL, $objNewReferenceData = NULL, $arrmixData = NULL, $boolIsForDelete = false ) {

		$objEventLibrary = new CEventLibrary();
		$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setLease( $this );
		$objEventLibraryDataObject->setDatabase( $objDatabase );

		$objSelectedEvent = $objEventLibrary->createEvent( [ $this ], $intEventTypeId );
		$objSelectedEvent->setCid( $this->getCid() );
		$objSelectedEvent->setLeaseId( $this->getId() );
		$objSelectedEvent->setOldReferenceData( $objOldReferenceData );
		$objSelectedEvent->setNewReferenceData( $objNewReferenceData );

		if( true == valArr( $arrmixData ) && true == isset( $arrmixData['refer_event_to_customer'] ) && 1 == $arrmixData['refer_event_to_customer'] ) {
			$objSelectedEvent->setDataReferenceId( $arrmixData['customer_id'] );
		} else {
			$objSelectedEvent->setDataReferenceId( $this->getId() );
		}

		$objSelectedEvent->setEventDatetime( 'NOW()' );

		$arrmixEventDescription = [];
		$strEventDescription = '';

		switch( $intEventTypeId ) {

			case CEventType::UNIT_CHANGE:
				$arrmixData['unit_change_process'] = true;
				$arrmixData['old_unit_number_cache'] = $objOldReferenceData->getUnitNumber();
				$arrmixData['new_unit_number_cache'] = $objNewReferenceData->getUnitNumber();
				break;

			case CEventType::LEASE_INFO:
				$arrmixData['is_adjust_move_in'] = false;
				if( true == array_key_exists( 'lease_start_date', $arrmixData )
				    || true == array_key_exists( 'lease_end_date', $arrmixData ) ) {
					$arrmixData['is_adjust_move_in'] = true;
				}
				$arrmixChanges = [];

				if( true == valObj( $objNewReferenceData, CCustomer::class )
				    && is_null( $objOldReferenceData ) ) {
					$arrmixData['is_new_customer_added'] = true;
					$arrmixData['customer_name'] = $objNewReferenceData->getNameFirst() . ' ' . $objNewReferenceData->getNameLast();
				} elseif( true == valObj( $objNewReferenceData, CLeaseCustomer::class )
				        && is_null( $objOldReferenceData ) ) {
					$arrmixData['is_new_customer_added'] = true;
					$arrmixData['customer_name'] = $objNewReferenceData->getCustomerNameFirst() . ' ' . $objNewReferenceData->getCustomerNameLast();
				} elseif( true == valObj( $objNewReferenceData, CLeaseInterval::class )
							&& true == valObj( $objOldReferenceData, CLeaseInterval::class )
							&& false == $arrmixData['is_adjust_move_in'] ) {
					$arrmixChanges = $this->compareObject( $objNewReferenceData, $objOldReferenceData, [ 'LeaseStartDate', 'LeaseEndDate', 'LeaseTermId', 'LeaseStartWindowId', 'LeaseStatusTypeId' ] );
				} elseif( true == valObj( $objNewReferenceData, CLease::class )
					         && true == valObj( $objOldReferenceData, CLease::class )
							 && false == $arrmixData['is_adjust_move_in'] ) {
					$arrmixChanges = $this->compareObject( $objNewReferenceData, $objOldReferenceData, [ 'LeaseStartDate', 'LeaseEndDate', 'MoveInDate', 'LeaseStatusTypeId', 'NoticeDate', 'LeaseTermId', 'LeaseStartWindowId' ] );
				}

				$arrmixData = array_merge( $arrmixData, $arrmixChanges );
				break;

			case CEventType::LEDGER_INFO:
				if( true == $boolIsForDelete ) {
					if( $arrmixData['event_type'] == 'Charge' ) {
						$objSelectedEvent->setEventSubTypeId( CEventSubType::LEDGER_INFO_CHARGE );
					} elseif( true == ( $arrmixData['event_type'] == 'Payment' ) && false == is_null( $objNewReferenceData->getId() ) ) {
						$objSelectedEvent->setEventSubTypeId( CEventSubType::LEDGER_INFO_PAYMENT );
					}
				}
				break;

			case CEventType::UNIT_TRANSFER:
				$objSelectedEvent->setEventSubTypeId( $arrmixData['event_sub_type_id'] );
				break;

			case CEventType::INDIVIDUAL_TRANSFER:
				$objSelectedEvent->setEventSubTypeId( $arrmixData['event_sub_type_id'] );
				break;

			case CEventType::TRANSFER_QUOTES:
				$objSelectedEvent->setEventSubTypeId( CEventSubType::TRANSFER_QUOTES_CREATED );
				if( true == array_key_exists( 'cancel_transfer_quote', $arrmixData ) ) {
					$objSelectedEvent->setEventSubTypeId( CEventSubType::TRANSFER_QUOTES_CANCELLED );
				}
				break;

			default:
				// default case
				break;
		}
		$objEventLibraryDataObject->setData( $arrmixData );
		$objEventLibrary->buildEventDescription( $this );
		return $objEventLibrary;
	}

	public function compareObject( $objOldObject, $objNewObject, $arrstrLogFields ) {
		$strGet = 'get';
		$arrmixOutPut = [];
		$strNewClassName = substr( ( string ) get_class( $objNewObject ), 1, ( strlen( ( string ) get_class( $objNewObject ) ) - 1 ) );
		$strNewClassName = \Psi\Libraries\UtilStrings\CStrings::createService()->inflectUnderscore( $strNewClassName );
		$arrmixOutPut[$strNewClassName] = false;
		foreach( $arrstrLogFields As $strFieldName ) {
			$strGetterFunctionName = $strGet . $strFieldName;
			if( true == method_exists( $objOldObject, $strGetterFunctionName )
			    && true == method_exists( $objNewObject, $strGetterFunctionName )
			    && $objOldObject->{$strGetterFunctionName}() != $objNewObject->{$strGetterFunctionName}() ) {
				$strUnderScoreFiledName = \Psi\Libraries\UtilStrings\CStrings::createService()->inflectUnderscore( $strFieldName );
				$arrmixOutPut[$strNewClassName] = true;
				$arrmixOutPut[$strUnderScoreFiledName . '_changed']   = true;
				$arrmixOutPut[$strUnderScoreFiledName]['old_value'] = $objOldObject->{$strGetterFunctionName}();
				$arrmixOutPut[$strUnderScoreFiledName]['new_value'] = $objNewObject->{$strGetterFunctionName}();
			} else {
				continue;
			}
		}
		return $arrmixOutPut;
	}

	public function logEvent( $intOldLeaseStatusTypeId, $intNewStatusTypeId, $objCompanyUser = NULL, $objDatabase, $intReversalEventId = NULL, $intEventTypeId = NULL, $strEffectiveDate = NULL, $objLeaseCustomer = NULL, $strNote = NULL, $intEventSubTypeId = NULL ) {

		$intEventTypeId = ( true == is_null( $intEventTypeId ) ) ? CEventType::STATUS_CHANGE : $intEventTypeId;

		$objEventLibrary = new CEventLibrary();
		$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();

		$objEventLibraryDataObject->setLease( $this );
		$objEventLibraryDataObject->setDatabase( $objDatabase );
		$objEventLibraryDataObject->setLeaseCustomer( $objLeaseCustomer );

		$objSelectedEvent = $objEventLibrary->createEvent( [ $this ], $intEventTypeId, $intEventSubTypeId );
		$objSelectedEvent->setCid( $this->getCid() );

		if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			$objCompanyEmployee = $objCompanyUser->fetchCompanyEmployee( $objDatabase );

			$objSelectedEvent->setCompanyUser( $objCompanyUser );

			if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
				$objSelectedEvent->setCompanyEmployeeId( $objCompanyEmployee->getId() );
				$objSelectedEvent->setCompanyEmployee( $objCompanyEmployee );
			}
		}

		$objSelectedEvent->setNotes( $strNote );
		$objSelectedEvent->setOldStatusId( $intOldLeaseStatusTypeId );
		$objSelectedEvent->setNewStatusId( $intNewStatusTypeId );
		$objSelectedEvent->setLeaseId( $this->getId() );
		$objSelectedEvent->setDataReferenceId( $this->getId() );
		$objSelectedEvent->setAssociatedEventId( $intReversalEventId );
		$objSelectedEvent->setEventDatetime( 'NOW()' );
		$objSelectedEvent->setScheduledDatetime( $strEffectiveDate );
		$objEventLibrary->buildEventDescription( $this );

		return $objEventLibrary;
	}

	public function getLeasePaymentStatusName( $intLeaseStatusTypeId ) {
		$strPaymentStatusTypeName = CPaymentAllowanceType::getPaymentAllowanceTypeNameByPaymentAllowanceTypeId( $intLeaseStatusTypeId );

		return $strPaymentStatusTypeName;
	}

	public function getLeaseScheduleChargeTypeName( $intScheduleArTriggerId ) {
		$strScheduleChargeTypeName = CChargeType::getChargeTypeName( $intScheduleArTriggerId );

		return $strScheduleChargeTypeName;
	}

	public function createArAllocation() {

		$objArAllocation = new CArAllocation();

		$objArAllocation->setCid( $this->getCid() );
		$objArAllocation->setLeaseId( $this->getId() );
		$objArAllocation->setPropertyId( $this->getPropertyId() );

		return $objArAllocation;
	}

	public function autoAllocate( $intCompanyUserId, $strPostMonth, $strPostDate, $objDatabase, $intArProcessId = NULL, $boolGenerateBadDebtRecoveryTransactions = true, $boolIgnorePaymentAllocationSetting = false ) : bool {
		// $intArProcessId = 'NULL' is purposely passed as string please do not remove ''.
		/** @var  CLeaseAutoAllocationLibrary $objLeaseAutoAllocationLibrary */
		$objLeaseAutoAllocationLibrary 	= CLeaseAutoAllocationLibrary::create( $this, $objDatabase, $intArProcessId );
		$objLeaseAutoAllocationLibrary->setIgnorePaymentAllocationSetting( $boolIgnorePaymentAllocationSetting );

		$boolIsValid = true;

		try {
			$boolIsValid &= $objLeaseAutoAllocationLibrary->autoAllocate( $strPostMonth, $strPostDate, $intCompanyUserId );

		} catch( \Exception $objException ) {
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_GENERAL, NULL, $objException->getMessage() ) ] );
			$boolIsValid = false;
		}

		if( false == $boolIsValid || false == $boolGenerateBadDebtRecoveryTransactions ) {
			return $boolIsValid;
		}

		require_once( PATH_APPLICATION_ENTRATA . 'Library/CBadDebtLibrary.class.php' );

		$objPropertyGlSetting = $this->getOrFetchPropertyGlSetting( $objDatabase );

		$objBadDebtLibrary = new CBadDebtLibrary();
		$objBadDebtLibrary->setPropertyGlSetting( $objPropertyGlSetting );

		$boolIsValid &= $objBadDebtLibrary->generateBadDebtRecoveryTransactions( $this, $intCompanyUserId, $objDatabase );

		return $boolIsValid;
	}

	// This function allocates only liabilities, and is useful right after an initial import so the deposit ledger allocates properly

	public function autoAllocateLiabilities( $intCompanyUserId, $objDatabase ) {
		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * FROM ar_transactions_apply_liability_balance( ' . ( int ) $this->getCid() . ', ' . ( int ) $this->getPropertyId() . ', ' . ( int ) $this->getId() . ', ' . ( int ) $intCompanyUserId . ')';

		if( ( false == $objDataset->execute( $strSql ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to allocate lease balance. The following message was returned:' ) ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();
			return false;
		}

		return true;
	}

	// This function allocates only the items that are initial import

	public function autoAllocateInitialImport( $intCompanyUserId, $objDatabase ) {

		$objDataset = $objDatabase->createDataset();

		$strReversalTransactionSql = 'SELECT * FROM reversal_ar_transactions_initial_import_apply_balance( ' . ( int ) $this->getCid() . ', ' . ( int ) $this->getPropertyId() . ', ' . ( int ) $this->getId() . ', ' . ( int ) $intCompanyUserId . ')';

		if( ( false == $objDataset->execute( $strReversalTransactionSql ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to allocate reversal transaction lease balance. The following message was returned:' ) ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();
			return false;
		}

		$strSql = 'SELECT * FROM ar_transactions_initial_import_apply_balance( ' . ( int ) $this->getCid() . ', ' . ( int ) $this->getPropertyId() . ', ' . ( int ) $this->getId() . ', ' . ( int ) $intCompanyUserId . ')';

		if( ( false == $objDataset->execute( $strSql ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to allocate lease balance. The following message was returned:' ) ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();
			return false;
		}

		return true;
	}

	// This function preserves the original post month and allocation datetime that should have historically bet set

	public function autoAllocateWithPreservedPostMonths( $intCompanyUserId, $objDatabase ) {
		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * FROM ar_transactions_apply_historical_balance( ' . ( int ) $this->getCid() . ', ' . ( int ) $this->getPropertyId() . ', ' . ( int ) $this->getId() . ', ' . ( int ) $intCompanyUserId . ')';

		if( ( false == $objDataset->execute( $strSql ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to allocate lease balance. The following message was returned:' ) ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();
			return false;
		}

		return true;
	}

	public function getAutoAllocatePayments( CDatabase $objDatabase ) : bool {

		if( in_array( $this->getLeaseStatusTypeId(), [ CLeaseStatusType::CANCELLED, CLeaseStatusType::PAST ] ) ) {
			return true;
		}

		$boolAutoAllocatePayments = true;
		$objPropertyOccupancyTypeSetting = $this->getOrFetchPropertyOccupancyTypeSetting( $objDatabase );
		if( true == valObj( $objPropertyOccupancyTypeSetting, 'CPropertyOccupancyTypeSetting' ) ) {
			$boolAutoAllocatePayments = $objPropertyOccupancyTypeSetting->getAutoAllocatePayments();
		}

		return $boolAutoAllocatePayments;
	}

	public function calculateAndSetBalance( $arrobjArTransactions, $boolShowCreditBalances = false ) {

		if( false == valArr( $arrobjArTransactions ) ) {
			$this->setBalance( 0 );
			return true;
		}

		$fltBalance = 0;

		foreach( $arrobjArTransactions as $objArTransaction ) {
			$fltBalance += ( float ) $objArTransaction->getTransactionAmountDue();
		}

		if( 0 >= $fltBalance && false == $boolShowCreditBalances ) {
			$this->setBalance( 0 );
			return true;
		}

		$this->setBalance( $fltBalance );

		return true;
	}

	public function generateLeaseNumber( $strFirstName, $strLastName, $intCompanyUserId, $objDatabase ) {

		$objCompanyNumber = CCompanyNumbers::fetchNextLeaseNumber( $this->getCid(), $intCompanyUserId, $objDatabase );

		// Ensure we got a company Customer Number object
		if( false == valObj( $objCompanyNumber, 'CCompanyNumber' ) ) {
			trigger_error( 'Invalid Lease Number Request Expecting a CCompanyNumber Object and did not get one - CLease::generateLeaseNumber()', E_USER_ERROR );
			exit;
		}

		$strCode = 'l';
		$strCode .= ( string ) \Psi\CStringService::singleton()->strtoupper( \Psi\CStringService::singleton()->substr( preg_replace( '/[^A-Za-z]/i', '', $strLastName ), 0, 3 ) );
		$strCode .= ( string ) \Psi\CStringService::singleton()->strtoupper( \Psi\CStringService::singleton()->substr( preg_replace( '/[^A-Za-z]/i', '', $strFirstName ), 0, 3 ) );

		$strBufferPattern = '%03u';
		if( strlen( trim( $strCode ) ) < 7 ) {
			$strBufferPattern = sprintf( '%%0%du', ( int ) ( 10 - strlen( trim( $strCode ) ) ) );
		}

		$strNewNumber = ( string ) sprintf( '%s' . $strBufferPattern, $strCode, $objCompanyNumber->getLeaseNumber() );

		return $strNewNumber;
	}

	public function removeArTransactions( $intCompanyUserId, $objDatabase, $objPaymentDatabase = NULL ) {

		// Temporary transactions are intended to stay when integrated data is deleted.
		// Typically the temp transactions could be coming in from a utility billing company, for convergetnt billing.
		// We should not delete application charges if they are not exported.
		$strSql = 'SELECT
						at.*
					FROM
						ar_transactions at
						LEFT JOIN scheduled_charges lc ON( lc.id = at.scheduled_charge_id AND lc.cid = at.cid )
						LEFT JOIN property_preferences pp ON( at.property_id = pp.property_id AND at.cid = pp.cid AND pp.key = \'DONT_EXPORT_APPLICATION_CHARGES\' AND pp.value IS NOT NULL )
						LEFT JOIN ar_codes ac ON( ac.id = at.ar_code_id AND ac.cid = at.cid AND ac.dont_export IS TRUE )
						LEFT JOIN lease_customers lc1 ON( lc1.cid = at.cid AND lc1.lease_id = at.lease_id AND lc1.customer_id = at.customer_id )
					WHERE
						at.lease_id = ' . ( int ) $this->getId() . '
						AND at.cid = ' . ( int ) $this->getCid() . '
						AND ( at.is_temporary = false
								OR ( at.scheduled_charge_id IS NOT NULL AND lc.id IS NULL )
								OR ( lc.charge_end_date IS NOT NULL AND lc.charge_end_date < NOW() )
								OR ( lc.last_posted_on IS NOT NULL AND at.created_on < lc.last_posted_on )
								OR ( lc.last_posted_on IS NOT NULL AND lc.charge_end_date IS NOT NULL AND lc.charge_end_date < ( lc.last_posted_on + INTERVAL \'1 MONTH\' ) )
							)
						AND ( at.remote_primary_key IS NOT NULL
								OR at.customer_id IS NULL
								OR lc1.id IS NULL
								OR lc1.lease_status_type_id <> ' . CLeaseStatusType::APPLICANT . '
								OR pp.id IS NOT NULL
								OR ac.id IS NOT NULL
							)';

		$arrobjArTransactions	= \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactions( $strSql, $objDatabase );
		$boolPerformRapidDelete	= ( true == valStr( $this->getRemotePrimaryKey() ) ) ? true : false;

		if( true == valArr( $arrobjArTransactions ) ) {
			// DELETE ALLOCATIONS
			$arrobjArAllocations = \Psi\Eos\Entrata\CArAllocations::createService()->fetchArAllocationsByCidByArTransactionIds( $this->getCid(), array_keys( $arrobjArTransactions ), $objDatabase );

			if( true == valArr( $arrobjArAllocations ) && false === \Psi\Eos\Entrata\CArAllocations::createService()->bulkDelete( $arrobjArAllocations, $objDatabase ) ) {
				return false;
			}

			// Hadingling error while deleting ar transaction: tuple to be updated was already modified by an operation triggered by the current command
			$strSql = 'UPDATE ar_transactions SET ar_transaction_id = NULL, ar_origin_object_id = NULL WHERE cid= ' . $this->getCid() . ' AND lease_id=' . $this->getId() . ' AND ( ar_transaction_id IS NOT NULL OR ar_origin_object_id IS NOT NULL );';

			fetchData( $strSql, $objDatabase );

			foreach( $arrobjArTransactions as $objArTransaction ) {
				if( false == $objArTransaction->delete( $intCompanyUserId, $objDatabase, $objPaymentDatabase, NULL, $boolPerformRapidDelete ) ) {
					return false;
				}
			}
		}

		return true;
	}

	public function removeNonIntegratedArTransactions( $intCompanyUserId, $objDatabase, $objPaymentDatabase = NULL ) {

		// Temporary transactions are intended to stay when non integrated data is deleted.
		// Typically the temp transactions could be coming in from a utility billing company, for convergetnt billing.
		// We should not delete application charges if they are not exported.
		$strSql = 'SELECT
						at.*
					FROM
						ar_transactions at
						LEFT JOIN scheduled_charges lc ON( lc.id = at.scheduled_charge_id AND lc.cid = at.cid )
						LEFT JOIN property_preferences pp ON( at.property_id = pp.property_id AND at.cid = pp.cid AND pp.key = \'DONT_EXPORT_APPLICATION_CHARGES\' AND pp.value IS NOT NULL )
						LEFT JOIN ar_codes ac ON( ac.id = at.ar_code_id AND ac.cid = at.cid AND ac.dont_export IS TRUE )
						LEFT JOIN lease_customers lc1 ON( lc1.cid = at.cid AND lc1.lease_id = at.lease_id AND lc1.customer_id = at.customer_id )
					WHERE
						at.lease_id = ' . ( int ) $this->getId() . '
						AND at.cid = ' . ( int ) $this->getCid() . '
						AND at.remote_primary_key IS NULL
						AND ( at.is_temporary = false
								OR ( at.scheduled_charge_id IS NOT NULL AND lc.id IS NULL )
								OR ( lc.charge_end_date IS NOT NULL AND lc.charge_end_date < NOW() )
								OR ( lc.last_posted_on IS NOT NULL AND at.created_on < lc.last_posted_on )
								OR ( lc.last_posted_on IS NOT NULL AND lc.charge_end_date IS NOT NULL AND lc.charge_end_date < ( lc.last_posted_on + INTERVAL \'1 MONTH\' ) )
							)
						AND (
								at.customer_id IS NULL
								OR lc1.id IS NULL
								OR lc1.lease_status_type_id <> ' . CLeaseStatusType::APPLICANT . '
								OR pp.id IS NOT NULL
								OR ac.id IS NOT NULL
							);';

		$arrobjArTransactions	= \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactions( $strSql, $objDatabase );
		$boolPerformRapidDelete	= ( true == valStr( $this->getRemotePrimaryKey() ) ) ? true : false;

		if( true == valArr( $arrobjArTransactions ) ) {
			// DELETE ALLOCATIONS
			$arrobjArAllocations = \Psi\Eos\Entrata\CArAllocations::createService()->fetchArAllocationsByCidByArTransactionIds( $this->getCid(), array_keys( $arrobjArTransactions ), $objDatabase );

			if( true == valArr( $arrobjArAllocations ) && false === \Psi\Eos\Entrata\CArAllocations::createService()->bulkDelete( $arrobjArAllocations, $objDatabase ) ) {
				return false;
			}

			foreach( $arrobjArTransactions as $objArTransaction ) {
				if( false == $objArTransaction->delete( $intCompanyUserId, $objDatabase, $objPaymentDatabase, NULL, $boolPerformRapidDelete ) ) {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * @param CCustomer $objCustomer
	 * @param CProperty $objProperty
	 * @param CClient $objClient
	 * @param CDatabase $objDatabase
	 */
	public function checkAndImportLedger( $objCustomer, $objProperty, $objClient, $objDatabase ) {
		$boolIsIntegratedProperty = $objProperty->isIntegrated( $objDatabase );

		if( false === $boolIsIntegratedProperty || NULL === $boolIsIntegratedProperty ) {
			return;
		}

		if( \CCompanyStatusType::PROSPECT == $objClient->getCompanyStatusTypeId() || false == valStr( $this->getLedgerLastSyncOn() ) || 600 < ( strtotime( 'now' ) - strtotime( $this->getLedgerLastSyncOn() ) ) ) {
			$this->importLedger( \CUser::ID_RESIDENT_PORTAL, $objCustomer, $objDatabase, NULL );
		}
	}

	public function importLedger( $intCompanyUserId, $objCustomer, $objDatabase, $intLedgerDisplayDays = NULL, $boolProcessThroughAMQP = false ) {
		if( true == is_null( $this->getRemotePrimaryKey() ) || 0 == strlen( $this->getRemotePrimaryKey() ) ) return true;

		// Import and Update CCustomer if this company has integration

		// For AMSI there is no separate web method to import ledger, we are getting ledger details in GetSingleResident
		$objIntegrationClientType 			= $this->fetchIntegrationClientType( $objDatabase );
		$arrintIntegrationClientTypeIds		= [ CIntegrationClientType::AMSI, CIntegrationClientType::JENARK ];

		if( true == valObj( $objIntegrationClientType, 'CIntegrationClientType' ) && true == in_array( $objIntegrationClientType->getId(), $arrintIntegrationClientTypeIds ) ) {
			$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->getCid(), CIntegrationService::RETRIEVE_CUSTOMER, $intCompanyUserId, $objDatabase );
		} else {
			$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->getCid(), CIntegrationService::RETRIEVE_AR_TRANSACTIONS, $intCompanyUserId, $objDatabase );
		}

		$objProperty = $this->fetchProperty( $objDatabase );

		$objGenericWorker->setCustomer( $objCustomer );
		$objGenericWorker->setProperty( $objProperty );
		$objGenericWorker->setLease( $this );
		$objGenericWorker->setLedgerDisplayDays( $intLedgerDisplayDays );
		$objGenericWorker->setCallTimeLimit( 20 );

		$boolIsSuccessful = $objGenericWorker->process();

		return $boolIsSuccessful;
	}

	public function importPayments( $objCustomer, $objDatabase ) {

		if( true == is_null( $this->getRemotePrimaryKey() ) || 0 == strlen( $this->getRemotePrimaryKey() ) ) return true;

		// Import and Update CCustomer if this company has integration

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->getCid(), CIntegrationService::RETRIEVE_AR_PAYMENTS, 1, $objDatabase );

		$objProperty = $this->fetchProperty( $objDatabase );

		$objGenericWorker->setCustomer( $objCustomer );
		$objGenericWorker->setProperty( $objProperty );
		$objGenericWorker->setLease( $this );
		$objGenericWorker->setDatabase( $objDatabase );

		$boolIsSuccessful = $objGenericWorker->process();

		return $boolIsSuccessful;
	}

	public function importOccupants( $intLeaseStatusTypeId, $objDatabase ) {

		if( true == is_null( $this->getRemotePrimaryKey() ) || 0 == strlen( $this->getRemotePrimaryKey() ) ) return true;

		// Import lease occupants - AMSI

		$objIntegrationClientType = $this->fetchIntegrationClientType( $objDatabase );

		if( true == valObj( $objIntegrationClientType, 'CIntegrationClientType' ) && CIntegrationClientType::AMSI == $objIntegrationClientType->getId() ) {
			$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->getCid(), CIntegrationService::RETRIEVE_CUSTOMERS, 1, $objDatabase );
		} else {
			return true;
		}

		$objLeaseStatusType = \Psi\Eos\Entrata\CLeaseStatusTypes::createService()->fetchLeaseStatusTypeById( $intLeaseStatusTypeId, $objDatabase );

		$objProperty = $this->fetchProperty( $objDatabase );

		$objGenericWorker->setProperty( $objProperty );
		$objGenericWorker->setLease( $this );
		$objGenericWorker->setLeaseStatusType( $objLeaseStatusType );
		$objGenericWorker->setDatabase( $objDatabase );

		$boolIsSuccessful = $objGenericWorker->process();

		return $boolIsSuccessful;
	}

	public function exportArTransactions( $intCompanyUserId, $objDatabase, $objPaymentDatabase, $boolQueueSyncOverride = false ) {

		if( true == is_null( $this->getRemotePrimaryKey() ) || 0 == strlen( $this->getRemotePrimaryKey() ) ) {
			return true;
		}

		$arrobjArTransactions = $this->fetchNonIntegratedApplicationCharges( $objDatabase );

		if( true == valArr( $arrobjArTransactions ) ) {
			$objApplication         = NULL;
			$objIntegrationDatabase = $this->fetchProperty( $objDatabase )->fetchIntegrationDatabase( $objDatabase );

			if( true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) && CIntegrationClientType::YARDI_RPORTAL == $objIntegrationDatabase->getIntegrationClientTypeId() ) {
				$objApplication = \Psi\Eos\Entrata\CApplications::createService()->fetchApplicationByLeaseIntervalIdByPropertyIdByCid( $this->getOrFetchActiveLeaseInterval( $objDatabase )->getId(), $this->getPropertyId(), $this->getCid(), $objDatabase );
			}

			$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->getCid(), CIntegrationService::SEND_AR_TRANSACTIONS, $intCompanyUserId, $objDatabase, $objPaymentDatabase );
			$objGenericWorker->setProperty( $this->fetchProperty( $objDatabase ) );
			$objGenericWorker->setLease( $this );
			$objGenericWorker->setApplication( $objApplication );
			$objGenericWorker->setArTransactions( $arrobjArTransactions );
			$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );

			return $objGenericWorker->process();

		} else {
			return false;
		}
	}

	public function exportInsuranceTransactions( $intCompanyUserId, $objDatabase, $objPaymentDatabase, $boolQueueSyncOverride = false ) {

	if( true == is_null( $this->getRemotePrimaryKey() ) || 0 == strlen( $this->getRemotePrimaryKey() ) ) return true;

		$arrobjArTransactions = $this->fetchNonIntegratedInsuranceCharges( $objDatabase );

		if( true == valArr( $arrobjArTransactions ) ) {
			$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->getCid(), CIntegrationService::SEND_INSURANCE_TRANSACTIONS, $intCompanyUserId, $objDatabase, $objPaymentDatabase );
			$objGenericWorker->setProperty( $this->fetchProperty( $objDatabase ) );
			$objGenericWorker->setLease( $this );
			$objGenericWorker->setArTransactions( $arrobjArTransactions );
			$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );
			return $objGenericWorker->process();

		} else {
			return false;
		}
	}

	public function exportUpdate( $intCompanyUserId, $objDatabase, $intIntegrationSyncTypeId = NULL, $boolQueueSyncOverride = false, $arrstrTransportRules = [] ) {
		$this->getOrFetchProperty( $objDatabase );

		// Throw an error if Company Property is not integrated or the
		// application has already been exported.
		if( false == valObj( $this->m_objProperty, 'CProperty', 'RemotePrimaryKey' ) ) return true;

		if( true == is_null( $this->getRemotePrimaryKey() ) ) return true;

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->getCid(), CIntegrationService::UPDATE_LEASE, $intCompanyUserId, $objDatabase );

		$objGenericWorker->setLease( $this );
		$objGenericWorker->setProperty( $this->getProperty() );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );

		if( false == is_null( $intIntegrationSyncTypeId ) ) {
			$objGenericWorker->setIntegrationSyncTypeId( $intIntegrationSyncTypeId );
		}

		if( true == valArr( $arrstrTransportRules ) ) {
			$objGenericWorker->setTransportRules( $arrstrTransportRules );
		}

		$boolIsValid = $objGenericWorker->process();

		if( true == valArr( $objGenericWorker->getErrorMsgs() ) ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		if( false == valObj( $objGenericWorker->getIntegrationClient(), 'CIntegrationClient' ) ) {
			$this->addErrorMsgs( [ new CErrorMsg( NULL, NULL, __( 'Application cannot integrate - updateApplication service is not added.' ), NULL ) ] );
			$boolIsValid = false;
		} elseif( CIntegrationClientStatusType::DISABLED == $objGenericWorker->getIntegrationClient()->getIntegrationClientStatusTypeId() ) {
			$this->addErrorMsgs( [ new CErrorMsg( NULL, NULL, __( 'Application cannot integrate - updateApplication is disabled.' ), NULL ) ] );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function postUnconfirmedPayments( $arrobjArPayments, $intPaymentChargeCodeId, $intUserId, $objDatabase ) {

		$boolIsValid = true;

		if( true == valArr( $arrobjArPayments ) ) {
			foreach( $arrobjArPayments as $objArPayment ) {

				if( 0 == $objArPayment->getPaymentAmount() ) continue;

				$objPaymentArTransaction = $this->createArTransaction( $intPaymentChargeCodeId );
				$objPaymentArTransaction->setArPaymentId( $objArPayment->getId() );
				$objPaymentArTransaction->setTransactionAmount( $objArPayment->getPaymentAmount() );
				$objPaymentArTransaction->setPostDate( 'now()' );
				$objPaymentArTransaction->setMemo( $objArPayment->getPaymentMemo() );
				$objPaymentArTransaction->getOrFetchPostMonth( $objDatabase );

				if( false == $objPaymentArTransaction->postPayment( $intUserId, $objDatabase ) ) {
					$boolIsValid &= false;
				}
			}
		}

		return $boolIsValid;
	}

	public function formatLeaseDates() {
		if( false == is_null( $this->getLeaseStartDate() ) ) $this->setLeaseStartDate( date( 'M y', strtotime( $this->getLeaseStartDate() ) ) );
		if( false == is_null( $this->getLeaseEndDate() ) ) $this->setLeaseEndDate( date( 'M y', strtotime( $this->getLeaseEndDate() ) ) );
	}

	public function createLeaseInterval() {

		$objLeaseInterval = new CLeaseInterval();

		$objLeaseInterval->setCid( $this->getCid() );
		$objLeaseInterval->setPropertyId( $this->getPropertyId() );
		$objLeaseInterval->setLeaseId( $this->getId() );
		$objLeaseInterval->setLateFeeFormulaId( $this->getLateFeeFormulaId() );
		$objLeaseInterval->setIntervalDatetime( 'NOW()' );

		return $objLeaseInterval;
	}

	public function createLeaseUnitSpace() {

		$objLeaseUnitSpace = new CLeaseUnitSpace();
		$objLeaseUnitSpace->setCid( $this->getCid() );
		$objLeaseUnitSpace->setPropertyId( $this->getPropertyId() );
		$objLeaseUnitSpace->setLeaseId( $this->getId() );

		return $objLeaseUnitSpace;
	}

	public function adjustLeaseIntervals( $intUserId, $objDatabase, $strPostThroughDate = NULL ) {

		// $strPostThroughDate = ( true == valStr( $strPostThroughDate ) ) ? '\'' . $strPostThroughDate . '\'' : 'NULL';

		//  Call [scheduled_charges_adjust_month_to_month_intervals] to property align lease_intervals and fill gaps with month-to-month intervals
		// 	 pCid
		// 	 pPropertyGroupIds
		// 	 pLeaseIds
		//	 pUserId

		$objPostScheduledChargesLibrary 	= CPostScheduledChargeFactory::createObject( CScheduledChargePostingMode::ADJUST_MTM_LEASE_INTERVALS, $objDatabase );
		$objPostScheduledChargesCriteria 	= $objPostScheduledChargesLibrary->loadPostScheduledChargesCriteria( $this->getCid(), [ $this->getPropertyId() ], $intUserId );
		$objPostScheduledChargesCriteria->setPostWindowEndDateOverride( $strPostThroughDate )
										->setLeaseIds( [ $this->getId() ] );

		// This function will just adjust the lease interval
		$objPostScheduledChargesLibrary->postScheduledCharges( $objPostScheduledChargesCriteria );
	}

	public function updateCustomLeaseStartDateAndLeaseEndDateUsingLeaseInterval( $objDatabase ) {

		$strSql = ' UPDATE leases
						SET lease_start_date = subquery.startdate,
						lease_end_date = subquery.enddate
					FROM (
							SELECT
								DISTINCT li.lease_id,
								li.cid,
								MAX(li.lease_start_date) OVER(PARTITION BY li.lease_id,	li.cid) AS startdate,
								MAX(li.lease_end_date) OVER(PARTITION BY li.lease_id, li.cid) AS enddate
							FROM
								lease_intervals AS li
								JOIN leases ON ( leases.id = li.lease_id AND li.cid = leases.cid AND li.lease_start_date <> leases.lease_start_date AND leases.lease_start_date <= li.lease_start_date )
							WHERE
								li.lease_start_date <= CURRENT_DATE + INTERVAL \'1 day\'
								AND ( li.lease_end_date IS NULL OR li.lease_end_date >= CURRENT_DATE )
								AND li.lease_id = ' . $this->getId() . '
								AND li.cid = ' . ( int ) $this->getCid() . '
								AND li.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
					) AS subquery
						WHERE
							leases.id = subquery.lease_id
							AND leases.cid = subquery.cid;';

		fetchData( $strSql, $objDatabase );

		return true;
	}

	public function fetchDepositHeld( $objDatabase, $intLedgerFilterId = NULL, $intDepositArCodeId = NULL ) {

		$fltDepositHeldAmount = 0.00;

		$arrmixDepositHeldAmount = ( array ) \Psi\Eos\Entrata\CLeases::createService()->fetchDepositHeldAmountByLeaseIdsByCid( [ $this->getId() ], $this->getCid(), $objDatabase, $intLedgerFilterId, $intDepositArCodeId );
		$arrmixDepositHeldAmount = rekeyArray( 'lease_id', $arrmixDepositHeldAmount );

		if( true == valArr( $arrmixDepositHeldAmount ) ) {
			$fltDepositHeldAmount = $arrmixDepositHeldAmount[$this->getId()]['deposit_held'];
		}

		return $fltDepositHeldAmount;
	}

    public function fetchDepositDue( $objDatabase, $intLedgerFilterId = NULL, $intDepositArCodeId = NULL ) {

        $fltDepositDueAmount = 0.00;

        $arrmixDepositDueAmount = ( array ) \Psi\Eos\Entrata\CLeases::createService()->fetchDepositDueAmountByLeaseIdsByCid( [ $this->getId() ], $this->getCid(), $objDatabase, $intLedgerFilterId, $intDepositArCodeId );
        $arrmixDepositDueAmount = rekeyArray( 'lease_id', $arrmixDepositDueAmount );

        if( true == valArr( $arrmixDepositDueAmount ) ) {
            $fltDepositDueAmount = $arrmixDepositDueAmount[$this->getId()]['deposit_due'];
        }

        return $fltDepositDueAmount;
    }

	public function fetchTransferredLease( $objDatabase ) {

		if( false == valId( $this->getTransferredFromLeaseId() ) ) return NULL;

		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $this->getTransferredFromLeaseId(), $this->getCid(), $objDatabase );

	}

	public function fetchMaxPostDateAndPostMonth( $objDatabase, $intLedgerFilterId = NULL, $arrintIncludeDefaultArCodeIds = NULL ) {

		$arrstrMaxDates = ( array ) \Psi\Eos\Entrata\CLeases::createService()->fetchMaxPostDateAndPostMonthByLeaseIdsByCid( [ $this->getId() ], $this->getCid(), $objDatabase, $intLedgerFilterId, $arrintIncludeDefaultArCodeIds );
		$arrstrMaxDates = rekeyArray( 'lease_id', $arrstrMaxDates );

		return $arrstrMaxDates;
	}

	public function fetchCurrentPostMonth( $objDatabase ) {

		$objPropertyGlSetting = CPropertyGlSettings::fetchCustomPropertyGlSettingsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );

		if( false == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
			trigger_error( 'Property GL Setting could not be loaded.' );
			exit;
		}

		return $objPropertyGlSetting->getArPostMonth();
	}

	public function fetchOriginalLease( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCidBySchema( $this->getId(), $this->getCid(), $objDatabase, false );
	}

	public function fetchLatestFinancialMoveOutStatement( $objDatabase ) {
		return CFiles::fetchLatestFileByLeaseIdBySystemCodeByCid( $this->getId(), CFileType::SYSTEM_CODE_FINANCIAL_MOVE_OUT_STATEMENT, $this->getCid(), $objDatabase );
	}

	public function updateUnitStatus( $objOriginalLease, $intCompanyUserId, $objDatabase ) {

		$objPrimaryLeaseCustomer = $objOriginalLease->getOrFetchPrimaryLeaseCustomer( $objDatabase );
		$objOriginalPrimaryLeaseCustomer = clone $objPrimaryLeaseCustomer;
		$objPrimaryLeaseCustomer->setLeaseStatusTypeId( CLeaseStatusType::CANCELLED );

		$objUnitSpaceStatusManager = new CUnitSpaceStatusManager();

		if( false == valObj( $objOriginalLease, 'CLease' ) ) {
			trigger_error( 'Failed to load lease object.', E_USER_ERROR );
			return false;
		}

		$objUnitSpace = $objOriginalLease->getOrFetchUnitSpace( $objDatabase );

		if( true == valObj( $objUnitSpace, 'CUnitSpace' ) ) {
			if( false == $objUnitSpaceStatusManager->changeUnitSpaceStatus( $intCompanyUserId, $objUnitSpace, $objOriginalPrimaryLeaseCustomer, $objPrimaryLeaseCustomer, $objOriginalLease, $objDatabase, $boolDontLogEvents = true ) ) {
				$this->addErrorMsgs( $objUnitSpaceStatusManager->getErrorMsgs() );
				return false;
			}
		} else {

			$objPrimaryLeaseCustomer->setLeaseStatusTypeId( $objOriginalPrimaryLeaseCustomer->getLeaseStatusTypeId() );
			$objNewUnitSpace = $this->getOrFetchUnitSpace( $objDatabase );

			if( true == valObj( $objNewUnitSpace, 'CUnitSpace' ) ) {
				if( false == $objUnitSpaceStatusManager->changeUnitSpaceStatus( $intCompanyUserId, $objNewUnitSpace, $objOriginalPrimaryLeaseCustomer, $objPrimaryLeaseCustomer, $objOriginalLease, $objDatabase, $boolDontLogEvents = true ) ) {
					$this->addErrorMsgs( $objUnitSpaceStatusManager->getErrorMsgs() );
					return false;
				}
			}
		}

		return true;
	}

	public function update( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsIntegrated = false, $boolIsSemesterSelectionEnabled = false ) {

		if( false == $boolReturnSqlOnly ) {
			$this->validatePrimaryCustomerType( $objDatabase );
		}

		$objOriginalLease = $this->fetchOriginalLease( $objDatabase );

		if( NULL != $objOriginalLease->getOrganizationContractId() && $objOriginalLease->getOrganizationContractId() != $this->getOrganizationContractId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Leases associated to Group Leasing contracts cannot be disassociated from the contract.' ) ) );
			return false;
		}

		if( true == $boolIsIntegrated || true == $boolReturnSqlOnly ) {
			return parent::update( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly );
		} else {

			// here we need to find the old unit (if changing), and update it's status.
			// check to see if the unit space id has changed. If so, we need to update unit statuses
			// We don't need to change unit status if lease status is changing.
			if( ( true == $boolIsSemesterSelectionEnabled || COccupancyType::MILITARY == $this->getOccupancyTypeId() ) && $this->getUnitSpaceId() != $objOriginalLease->getUnitSpaceId() ) {

				$strMoveInDate = ( true == valStr( $this->getMoveInDate() ) ) ? $this->getMoveInDate() : $this->getLeaseStartDate();

				if( false == CLeaseUnitSpaces::updateLeaseUnitSpaces( $this->getCid(), $this->getId(), $this->getUnitSpaceId(), $this->getPropertyId(), $this->getActiveLeaseIntervalId(), $strMoveInDate, $objDatabase ) ) {
					return false;
				}
			}

			if( false == parent::update( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly ) ) {
				return false;
			}

			if( true == valObj( $objOriginalLease, 'CLease' ) ) {

				if( $this->getPropertyId() != $objOriginalLease->getPropertyId() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Property on the original lease cannot be modified.' ) ) );
					return false;
				}

				// Update lease_status_type_id & active_lease_interval_id.
				if( COccupancyType::AFFORDABLE != $this->getOccupancyTypeId() && ( $objOriginalLease->getActiveLeaseIntervalId() != $this->getActiveLeaseIntervalId() || ( $objOriginalLease->getPrimaryCustomerId() != $this->getPrimaryCustomerId() ) ) ) {
					if( false == $this->syncLeaseStatusTypeAndActiveLeaseInterval( $intCompanyUserId, $objDatabase ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Failed to sync lease status type and active lease interval.' ) ) );
						return false;
					}
				}

				if( $this->getUnitSpaceId() != $objOriginalLease->getUnitSpaceId() ) {

					// Update move-in date as per move-in scheduler.
					if( true == $boolIsSemesterSelectionEnabled && $this->getLeaseStatusTypeId() == CLeaseStatusType::FUTURE && CLeaseIntervalType::APPLICATION == $this->getLeaseIntervalTypeId() ) {
						$this->updateMoveInDateTimeOnLease( $intCompanyUserId );
					}

					if( false == $this->updateUnitStatus( $objOriginalLease, $intCompanyUserId, $objDatabase ) ) {
						return false;
					}

				}
			}
		}

		return true;
	}

	public function fetchLastContractualLeaseInterval( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLastContractualLeaseIntervalByCidByLeaseId( $this->getCid(), $this->getId(), $objDatabase );
	}

	public function fetchSimpleRentAmountForCurrentInterval( $objDatabase ) {

		$intTotalRentAmountForCurrentMonth = \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchSimpleRentAmountForCurrentIntervalByLeaseIdByActiveLeaseIntervalIdByCid( $this->m_intId, $this->getActiveLeaseIntervalId(), $this->getCid(), $objDatabase );
		return $intTotalRentAmountForCurrentMonth;
	}

	public function fetchSimpleRentAmountByLeaseInterval( $intLeaseIntervalId, $objDatabase ) {
		$intTotalRentAmount = \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchSimpleRentAmountByLeaseIntervalIdByCid( $intLeaseIntervalId, $this->getCid(),  $objDatabase );
		return $intTotalRentAmount;
	}

	public function fetchPossibleDepositCreditArTransactions( $objDatabase, $arrintLedgerFilterIds = [] ) {

		$objDepositCreditsLibrary  		= CDepositCreditsLibrary::create( $this, $objDatabase );
		return $objDepositCreditsLibrary->getDepositCreditsTransactions( $arrintLedgerFilterIds );
	}

	public function fetchArTransactionsByArCodeTypeIdsByCid( $arrintArCodeTypeIds, $intCid, $objDatabase, $boolExcludeDepositCreditTransactions = false ) {
		return  \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionsByLeaseIdByArCodeTypeIdsByCid( $this->getId(), $arrintArCodeTypeIds, $intCid, $objDatabase, $boolExcludeDepositCreditTransactions );
	}

	public function fetchArTransactionsForReverseMoveInByArCodeTypeIdsByCid( $arrintArCodeTypeIds, $intCid, $objDatabase ) {
		return  \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionsForReverseMoveInByLeaseIdByArCodeTypeIdsByCid( $this->getId(), $arrintArCodeTypeIds, $intCid, $objDatabase );
	}

	public function fetchArTransactionsByIds( $arrintArTransactionIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionsByLeaseIdByIdByCid( $this->getId(), $arrintArTransactionIds, $this->getCid(), $objDatabase );
	}

	public function fetchAllResidentNames( $objDatabase, $boolMoreDetails = false, $boolIsIncludeApplicant = false ) {

		$strMoreDetails			= ( true == $boolMoreDetails ) ? ', c.id as customer_id, c.name_first as name_first, c.name_last as name_last ' : '';
		$arrintCustomerTypes	= [ CCustomerType::PRIMARY, CCustomerType::RESPONSIBLE ];

		$arrintLeaseStatusTypes = ( true == $boolIsIncludeApplicant ) ? CLeaseStatusType::$c_arrintActiveOrganizationContractLeaseStatusTypeIds : CLeaseStatusType::$c_arrintActiveAddOnReservationStatusTypes;

		$strSql = 'SELECT
						c.name_first || \' \' || c.name_last as full_name ' . $strMoreDetails . '
					FROM
						leases l
						JOIN lease_customers lc ON ( l.cid = lc.cid AND lc.lease_id = l.id )
						JOIN customers c ON ( c.cid = l.cid AND c.id = lc.customer_id )
					WHERE
						l.cid= ' . ( int ) $this->getCid() . '
						AND l.id = ' . ( int ) $this->getId() . '
						AND lc.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypes ) . ' )
						AND lc.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypes ) . ' )
					ORDER BY
						lc.customer_type_id,
						full_name';

		return fetchData( $strSql, $objDatabase );
	}

	// We are going to remove this functions after replacing all calls with CDeleteTemporaryUtilityTransactions

	public function deleteTemporaryArTransactions( $intCompanyUserId, $objDatabase ) {

		$strSql = 'SELECT
						art.*
					FROM
						ar_transactions art
						JOIN scheduled_charges sc ON ( sc.cid = art.cid AND sc.id = art.scheduled_charge_id )
					WHERE
						art.cid = ' . ( int ) $this->getCid() . '
						AND art.lease_id = ' . ( int ) $this->getId() . '
						AND ( sc.deleted_on IS NOT NULL OR art.post_date < sc.posted_through_date OR art.post_date > sc.charge_end_date )
						AND art.is_temporary = true';

		$arrobjTemporaryArTransactions = ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactions( $strSql, $objDatabase );

		foreach( $arrobjTemporaryArTransactions as $objTemporaryArTransaction ) {
			$objTemporaryArTransaction->delete( $intCompanyUserId, $objDatabase );
		}
	}

	public function syncLeaseStatusTypeAndActiveLeaseInterval( $intCompanyUserId, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->syncLeaseStatusTypeAndActiveLeaseInterval( $this->getCid(), $this->getId(), $intCompanyUserId, $objDatabase );
	}

	public function incrementReturnedPaymentsCount( $intCurrentUserId, $objDatabase ) {
		$this->setReturnedPaymentsCount( $this->getReturnedPaymentsCount() + 1 );
		$strSql = 'UPDATE
												lease_details
										SET
												returned_payments_count = returned_payments_count + 1,
												updated_by = ' . ( int ) $intCurrentUserId . ',
												updated_on = NOW()
										WHERE
												lease_id = ' . ( int ) $this->getId() . '
												AND cid = ' . ( int ) $this->getCid();

		if( false == fetchData( $strSql, $objDatabase ) ) {
			$this->addErrorMsgs( $objDatabase->getErrorMsgs() );
			return false;
		}

		return true;
	}

	public function checkLeaseInCollection() {

		$boolIsInCollection = false;

		if( NULL != $this->getCollectionsStartDate() && strtotime( $this->getCollectionsStartDate() ) <= strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsInCollection = true;
		}

		return $boolIsInCollection;
	}

	public function checkToleranceDaysWindow( $intPastResidentToleranceDays = 0 ) {
		$strMoveOutDate		= $this->getMoveOutDate();
		$strLeaseEndDate	= $this->getLeaseEndDate();

// 		Either of move out date or lease end date must be present
		if( true == empty( $strMoveOutDate ) && true == empty( $strLeaseEndDate ) ) return false;

		$intUnixTimeStampCompareDate	= ( false == is_null( $strMoveOutDate ) ) ? strtotime( '-1 day', strtotime( $strMoveOutDate ) ) : strtotime( '-1 day', strtotime( $strLeaseEndDate ) );
		$intUnixTimeStampCurrentDay		= strtotime( date( 'm/d/Y' ) );

		$intDaysDifferencBetweenCurrentAndMoveOutDate	= ( int ) ( ( $intUnixTimeStampCurrentDay - $intUnixTimeStampCompareDate ) / ( 60 * 60 * 24 ) );
		return ( $intDaysDifferencBetweenCurrentAndMoveOutDate <= $intPastResidentToleranceDays );
	}

	public function checkToleranceDaysWindowByDate( $intPastResidentToleranceDays = 0, $strDate = 'm/d/Y' ) {
		$strMoveOutDate		= $this->getMoveOutDate();
		$strLeaseEndDate	= $this->getLeaseEndDate();

// 		Either of move out date or lease end date must be present
		if( true == empty( $strMoveOutDate ) && true == empty( $strLeaseEndDate ) ) return false;

		$intUnixTimeStampCompareDate	= ( false == is_null( $strMoveOutDate ) ) ? strtotime( '-1 day', strtotime( $strMoveOutDate ) ) : strtotime( '-1 day', strtotime( $strLeaseEndDate ) );
		$intUnixTimeStampCurrentDay		= strtotime( date( $strDate ) );

		$intDaysDifferencBetweenCurrentAndMoveOutDate	= ( int ) ( ( $intUnixTimeStampCurrentDay - $intUnixTimeStampCompareDate ) / ( 60 * 60 * 24 ) );
		return ( $intDaysDifferencBetweenCurrentAndMoveOutDate <= $intPastResidentToleranceDays );
	}

	public function fetchUnallocatedChargeTransactionsByArChargeCodeGroups( $objDatabase, $boolIsNotTemporaryCharge = false, $intLedgerFilterId = NULL, $boolIsShowTaxTransaction = false, $boolExcludeRepaymentTransactions = false, $boolIsFetchForDefaultResidentLedgerOnly = false ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchUnallocatedChargeTransactionsByArCodeGroupsByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsNotTemporaryCharge, $intLedgerFilterId, $boolIsShowTaxTransaction, $boolExcludeRepaymentTransactions, $boolIsFetchForDefaultResidentLedgerOnly );
	}

	public function fetchUnallocatedPaymentAndConcessionTransactionsByArChargeCodeGroups( $objDatabase, $boolIsNotTemporaryCharge = false, $intLedgerFilterId = NULL, $boolIsShowTaxTransaction = false, $boolIsFetchForDefaultResidentLedgerOnly = false ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchUnallocatedPaymentAndConcessionTransactionsByArCodeGroupsByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsNotTemporaryCharge, $intLedgerFilterId, $boolIsShowTaxTransaction, $boolIsFetchForDefaultResidentLedgerOnly );
	}

	public function getRepaymentDetails( $objDatabase, $boolIsFromResidentPortal = false ) {

		$objProperty = $this->getOrFetchProperty( $objDatabase );

		if( valObj( $objProperty, 'CProperty' ) ) {
			$objPropertyChargeSetting = $objProperty->getOrFetchPropertyChargeSetting( $objDatabase );

			if( true == valObj( $objPropertyChargeSetting, 'CPropertyChargeSetting' ) && true == $objPropertyChargeSetting->getAllowRepaymentAgreements() ) {

				$arrobjCurrentRepayments	= $this->fetchAllActiveRepayments( $objDatabase, $boolIsFromResidentPortal );
				$this->setRepayment( $arrobjCurrentRepayments );
			}
		}
	}

	public function getRepaymentDetailsWithTaxAmount( $objDatabase ) {

		$objProperty = $this->getOrFetchProperty( $objDatabase );

		if( valObj( $objProperty, 'CProperty' ) ) {
			$objPropertyChargeSetting = $objProperty->getOrFetchPropertyChargeSetting( $objDatabase );

			if( true == valObj( $objPropertyChargeSetting, 'CPropertyChargeSetting' ) && true == $objPropertyChargeSetting->getAllowRepaymentAgreements() ) {

				$arrobjCurrentRepayments	= \Psi\Eos\Entrata\CRepayments::createService()->fetchAllRepaymentsWithTaxAmountByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
				$this->setRepayment( $arrobjCurrentRepayments );
			}
		}
	}

	public function fetchRepaymentDueByRepaymentIdByDate( $intRepaymentId, $strDate, $objDatabase ) {
		return \Psi\Eos\Entrata\CRepayments::createService()->fetchRepaymentDueByRepaymentIdByDateByLeaseIdByCid( $intRepaymentId, $strDate, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchLeaseCustomerDetailByCustomerId( $intCustomerId, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomerDetailByCustomerIdByLeaseIdByCid( $intCustomerId, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchLastTransactionRemotePrimaryKey( $objDatabase ) {
		$objArTransaction = \Psi\Eos\Entrata\CArTransactions::createService()->fetchLastIntegratedArTransactionByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		return ( true == valObj( $objArTransaction, 'CArTransaction' ) ) ? $objArTransaction->getRemotePrimaryKey() : 0;
	}

	public function fetchArTransactionsByRemotePrimaryKeys( $arrstrRemotePrimaryKeys, $objDatabase ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionsByRemotePrimaryKeysByLeaseIdByCid( $arrstrRemotePrimaryKeys, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchIntegratedArTransactions( $objDatabase ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchIntegratedArTransactionsByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCustomerAnimalsWithRatesAssociated( $objDatabase ) {

		$arrintUnitSpaceIds = [];
		if( false == is_null( $this->getUnitSpaceId() ) ) {
			$arrintUnitSpaceIds[$this->getUnitSpaceId()] = $this->getUnitSpaceId();
		}

		return \Psi\Eos\Entrata\CCustomerPets::createService()->fetchCustomerPetsAssociatedtoLeaseByLeaseIdsByUnitSpaceIdsByCid( [ $this->getId() ], $arrintUnitSpaceIds, $this->getCid(), $objDatabase );
	}

	public function fetchSecurityDepositDetails( $objDatabase ) {
		return rekeyArray( 'ar_code_id', ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchSecurityDepositDetailsByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase ) );
	}

	public function createLeaseChecklist() {

		$objLeaseChecklist = new CLeaseChecklist();
		$objLeaseChecklist->setCid( $this->getCid() );
		$objLeaseChecklist->setLeaseId( $this->getId() );
		$objLeaseChecklist->setLeaseIntervalId( $this->getActiveLeaseIntervalId() );
		return $objLeaseChecklist;

	}

	public function fetchAllActiveRepayments( $objDatabase, $boolIsFromResidentPortal = false ) {
		return \Psi\Eos\Entrata\CRepayments::createService()->fetchAllRepaymentsByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase, true, NULL, $boolIsFromResidentPortal );
	}

	public function getMilitaryEffectiveYear() {
	    if( true == valObj( $this->getActiveLeaseInterval(), 'CLeaseInterval' ) && $this->getActiveLeaseInterval()->getLeaseIntervalTypeId() == CLeaseIntervalType::APPLICATION ) {
	        return date( 'Y', strtotime( min( $this->getLeaseStartDate(), $this->getMoveInDate() ) ) );
        } else {
            return date( 'Y', strtotime( $this->getLeaseStartDate() ) );
        }
    }

    public function getMilitaryHousingAreaId() {

        $objPrimaryCustomer = $this->getOrFetchPrimaryCustomer( $this->m_objDatabase );
        if( false == valObj( $objPrimaryCustomer, 'CCustomer' ) ) return NULL;

        $objPrimaryCustomerMilitaryDetail = CCustomerMilitaryDetails::fetchCustomerMilitaryDetailByCustomerIdByCid( $objPrimaryCustomer->getId(), $this->getCid(), $this->m_objDatabase );

	    if( false == valObj( $objPrimaryCustomerMilitaryDetail, 'CCustomerMilitarydetail' ) || ( true == valObj( $objPrimaryCustomerMilitaryDetail, 'CCustomerMilitarydetail' ) && true == is_null( $objPrimaryCustomerMilitaryDetail->getDutyAddressPostalCode() ) ) ) return NULL;

	    $objMilitaryHousingAreaZipcodes = \Psi\Eos\Entrata\CMilitaryHousingAreaZipcodes::createService()->fetchMilitaryHousingAreaZipcodesByZipcode( $objPrimaryCustomerMilitaryDetail->getDutyAddressPostalCode(), $this->m_objDatabase );

	    if( false == valArr( $objMilitaryHousingAreaZipcodes ) ) return NULL;

	    $objMilitaryHousingAreaZipcode = current( $objMilitaryHousingAreaZipcodes );

        return ( true == valObj( $objMilitaryHousingAreaZipcode, 'CMilitaryHousingAreaZipcode' ) ) ? $objMilitaryHousingAreaZipcode->getMilitaryHousingAreaId() : NULL;
    }

	public function getNextMonthScheduledChargesTotal( $objDatabase ) {

		$fltNextMonthScheduledChargesTotal = ( float ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByLeaseId( $this->getId(), $this->getCid(), $objDatabase, true, true, true );
		return $fltNextMonthScheduledChargesTotal;
	}

	public function getFutureScheduledCharges() {

		$this->m_objDatabase->begin();

		// This code is for conventional leases
		$objLeaseCustomer = $this->fetchPrimaryLeaseCustomer( $this->m_objDatabase );
		if( true == valObj( $objLeaseCustomer, CLeaseCustomer::class ) && CLeaseStatusType::FUTURE == $objLeaseCustomer->getLeaseStatusTypeId() ) {
			$objLeaseCustomer->setLeaseStatusTypeId( CLeaseStatusType::CURRENT );
			$objLeaseCustomer->update( CCompanyUser::SYSTEM, $this->m_objDatabase );
		}

		$arrintPostingArTriggerIds			= array_merge( CPostScheduledChargesCriteria::$c_arrintRecurringAndOneTimeArTriggerIds, [ CArTrigger::MOVE_IN ] );
		$objPostScheduledChargesLibrary		= CPostScheduledChargeFactory::createObject( CScheduledChargePostingMode::MERGE_FIELD_FUTURE_CHARGES, $this->m_objDatabase );
		$objPostScheduledChargesCriteria	= $objPostScheduledChargesLibrary->loadPostScheduledChargesCriteria( $this->getCid(), [ $this->getPropertyId() ], CCompanyUser::SYSTEM );

		$strPostWindowEndDate = ( true == valStr( $this->getMoveInDate() ) && strtotime( $this->getMoveInDate() ) > strtotime( date( 'm/d/Y' ) ) ) ? $this->getMoveInDate() : '+ 1 month';
		$objPostScheduledChargesCriteria->setDataReturnTypeId( CPostScheduledChargesCriteria::DATA_RETURN_TYPE_AR_TRANSACTIONS )
			->setIsDryRun( true )
			->setLeaseIds( [ $this->getId() ] )
			->setEndScheduledChargesForceFully( true )
			->setPostWindowEndDateOverride( date( 'm/t/Y', strtotime( $strPostWindowEndDate ) ) );

		$objPostScheduledChargesCriteria->setArTriggerIds( $arrintPostingArTriggerIds );
		$objPostScheduledChargesLibrary->postScheduledCharges( $objPostScheduledChargesCriteria );

		$arrfltArCharges = ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionsByLeaseIdsByArTriggerTypeIdByCid( [ $this->getId() ], CArTriggerType::RECURRING_LEASE_CHARGES, $this->m_intCid, $this->m_objDatabase, $this->getMoveInDate() );
		$this->m_objDatabase->rollback();

		$strFutureScheduledCharges = '';
		if( true == valArr( $arrfltArCharges ) ) {
			$arrfltArCharge               = current( $arrfltArCharges );
			$strFutureScheduledCharges = $arrfltArCharge['future_scheduled_charges'];
		}
		return __( '{%m, 0}', [ $strFutureScheduledCharges ] );
	}

	public function updateMoveInDateTimeOnLease( $intCompanyUserId ) {

		$objApplication = CApplications::fetchApplicationWithLeaseIntervalDataByLeaseIntervalIdByCid( $this->getActiveLeaseIntervalId(), $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objApplication, 'CApplication' ) ) return false;
		$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByIdByCid( $intCompanyUserId, $this->getCid(), $this->m_objDatabase );

		$objMoveInSchedulerService = new CMoveInSchedulerService();

		$objMoveInSchedulerService->setCid( $this->getCid() );
		$objMoveInSchedulerService->setPropertyId( $this->getPropertyId() );
		$objMoveInSchedulerService->setCompanyUser( $objCompanyUser );
		$objMoveInSchedulerService->setDatabase( $this->m_objDatabase );
		$objMoveInSchedulerService->setLease( $this );

		$objMoveInSchedulerService->updateMoveInDateTimeOnLease( $objApplication, CMoveInSchedule::SCHEDULE_TRIGGER_UNIT_ASSIGNED_AND_LOCKED );

	}

	public function fetchResidentInsurancePolicyByIdByCid( $intPolicyId, $objDatabase ) {
		return CResidentInsurancePolicies::fetchResidentInsurancePolicyByIdByCid( $intPolicyId, $this->getCid(), $objDatabase );
	}

	public function fetchUseMoveInMoveOutChecklist( $strFieldName, $objDatabase ) {

		if( false == valId( $this->getOrganizationContractId() ) ) return false;

		$strSelectSql = ' CASE
							WHEN \'1\' = JSONB_EXTRACT_PATH_TEXT( oc.details, \'' . ( string ) $strFieldName . '\' ) THEN true
							else false
						END::INTEGER as ' . $strFieldName;
		if( 'use_move_in_move_out_checklist' == $strFieldName ) {
			$strSelectSql = 'CASE
								WHEN \'1\' = JSONB_EXTRACT_PATH_TEXT( oc.details, \'use_move_in_checklist\' ) THEN true
								else false
							END::INTEGER as use_move_in_checklist,
							CASE
								WHEN \'1\' = JSONB_EXTRACT_PATH_TEXT( oc.details, \'use_move_out_checklist\' ) THEN true
								else false
							END::INTEGER as use_move_out_checklist';
		}

		$strSql = 'SELECT
						' . $strSelectSql . '
					FROM
						organization_contracts oc
					WHERE
						oc.cid = ' . ( int ) $this->getCid() . '
						AND oc.property_id = ' . ( int ) $this->getPropertyId() . '
						AND oc.id = ' . ( int ) $this->getOrganizationContractId();

		$arrmixResult = fetchData( $strSql, $objDatabase );
		return ( ( true == valArr( $arrmixResult ) ) ? ( ( 'use_move_in_move_out_checklist' == $strFieldName ) ? $arrmixResult[0] : $arrmixResult[0][$strFieldName] ) : true );
	}

	/**
	 * @return string
	 */
	public function getMoveInTime() {
		$strMoveInTime = '';

		$strSql = 'SELECT
						ld.scheduled_move_in_start_time,
						ld.scheduled_move_in_end_time
					FROM lease_details ld
					WHERE ld.cid = ' . ( int ) $this->getCid() . '
						AND ld.lease_id = ' . ( int ) $this->getId();

		$arrmixData	= fetchData( $strSql, $this->getDatabase() );

		if( true == valArr( $arrmixData ) && true == valStr( $arrmixData[0]['scheduled_move_in_start_time'] ) && true == valStr( $arrmixData[0]['scheduled_move_in_end_time'] ) ) {

			$strPropertyTimeZoneName = CTimeZone::DEFAULT_TIME_ZONE_NAME;
			$objProperty = $this->m_objProperty;
			if( false == valObj( $objProperty, 'CProperty' ) ) {
				$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $this->m_objDatabase );
			}
			if( true == valObj( $objProperty, 'CProperty' ) ) {
				$objPropertyTimeZone = $objProperty->getOrfetchTimezone( $this->m_objDatabase );
				if( true == valObj( $objPropertyTimeZone, 'CTimeZone' ) ) {
					$strPropertyTimeZoneName = $objPropertyTimeZone->getTimeZoneName();
				}
			}

			$strMoveInTime = __( '{%t, 0, TIME_SHORT}', [ CInternationalDateTime::create( $arrmixData[0]['scheduled_move_in_start_time'], $strPropertyTimeZoneName, CTimeZone::DEFAULT_TIME_ZONE_NAME ) ] ) . ' - ' . __( '{%t, 0, TIME_SHORT}', [ CInternationalDateTime::create( $arrmixData[0]['scheduled_move_in_end_time'], $strPropertyTimeZoneName, CTimeZone::DEFAULT_TIME_ZONE_NAME ) ] );
			if( strtotime( $arrmixData[0]['scheduled_move_in_start_time'] ) == strtotime( $arrmixData[0]['scheduled_move_in_end_time'] ) ) {
				$strMoveInTime = __( '{%t, 0, TIME_SHORT}', [ CInternationalDateTime::create( $arrmixData[0]['scheduled_move_in_start_time'], $strPropertyTimeZoneName, CTimeZone::DEFAULT_TIME_ZONE_NAME ) ] );
			}
		}

		return $strMoveInTime;
	}

	public function checkIfLeaseIsPureTaxCredit() : bool {
		$boolIsPureTaxCredit = false;
		if( COccupancyType::AFFORDABLE == $this->getOccupancyTypeId() ) {
			$arrobjPropertySubsidyTypes = ( array ) rekeyObjects( 'SubsidyTypeId', ( array ) \Psi\Eos\Entrata\CPropertySubsidyTypes::createService()->fetchActivePropertySubsidyTypesByPropertyIdByCId( $this->getPropertyId(), $this->getCid(), $this->m_objDatabase ) );
			if( 1 == \Psi\Libraries\UtilFunctions\count( $arrobjPropertySubsidyTypes ) && true == array_key_exists( CSubsidyType::TAX_CREDIT, $arrobjPropertySubsidyTypes ) ) {
				$boolIsPureTaxCredit = true;
			} else {
				$objSubsidyCertification = \Psi\Eos\Entrata\CSubsidyCertifications::createService()->fetchLatestActiveSubsidyCertificationByLeaseIdByPropertyIdByCid( $this->getId(), $this->getCid(), $this->getDatabase() );
				if( true == valObj( $objSubsidyCertification, CSubsidyCertification::class ) && true == valId( $objSubsidyCertification->getSetAsideId() ) && false == valId( $objSubsidyCertification->getSubsidyContractId() ) ) {
					$boolIsPureTaxCredit = true;
				}
			}

		}

		return $boolIsPureTaxCredit;
	}

	public function exportMoveInStatus( $intCompanyUserId, $objProperty, $strMoveInDate, $objDatabase ) {

		$boolIsValid 								= true;
		if( false == valObj( $objProperty, 'CProperty', 'RemotePrimaryKey' ) ) {
			return true;
		}
		$objIntegrationDatabase = $objProperty->fetchIntegrationDatabase( $objDatabase );

		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) || ( false == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), [ CIntegrationClientType::EMH, CIntegrationClientType::EMH_ONE_WAY ] ) ) ) return true;

		$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $objProperty->getCid(), CIntegrationService::UPDATE_MOVE_IN_STATUS, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setLease( $this );
		$objGenericWorker->setProperty( $objProperty );
		$objGenericWorker->setMoveInDate( $strMoveInDate );
		$boolIsValid = $objGenericWorker->process();

		if( true == valArr( $objGenericWorker->getErrorMsgs() ) && true == $this->getIsShowIntegrationError() ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		return $boolIsValid;
	}

	public function exportMoveInCancelledStatus( $intCompanyUserId, $objProperty, $objDatabase ) {

		$boolIsValid 				= true;
		$objIntegrationDatabase     = NULL;
		if( false == valObj( $objProperty, 'CProperty', 'RemotePrimaryKey' ) ) {
			return true;
		}

		$objIntegrationDatabase = $objProperty->fetchIntegrationDatabase( $objDatabase );

		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) || ( false == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), [ CIntegrationClientType::EMH_ONE_WAY ] ) ) ) return true;

		$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $objProperty->getCid(), CIntegrationService::UPDATE_MOVE_IN_CANCELLED_STATUS, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setLease( $this );
		$objGenericWorker->setProperty( $objProperty );
		$boolIsValid = $objGenericWorker->process();

		if( true == valArr( $objGenericWorker->getErrorMsgs() ) && true == $this->getIsShowIntegrationError() ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		return $boolIsValid;
	}

	public function exportProcessTransfer( $intCompanyUserId, $objProperty, $strPlannedMoveOutDate, $objDatabase ) {
		$boolIsValid 								= true;

		if( false == valObj( $objProperty, 'CProperty', 'RemotePrimaryKey' ) ) {
			return true;
		}

		$objIntegrationDatabase = $objProperty->fetchIntegrationDatabase( $objDatabase );

		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) || ( $objIntegrationDatabase->getIntegrationClientTypeId() != CIntegrationClientType::EMH ) ) return true;

		$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $this->getCid(), CIntegrationService::UPDATE_UNIT_TRANSFER_STATUS, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setLease( $this );
		$objGenericWorker->setProperty( $objProperty );
		$objGenericWorker->setPlannedMoveOutDate( $strPlannedMoveOutDate );
		$boolIsValid = $objGenericWorker->process();

		if( true == valArr( $objGenericWorker->getErrorMsgs() ) && true == $this->getIsShowIntegrationError() ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		return $boolIsValid;
	}

	public function setArPaymentBillingInfoForResPortalResponsive( $objArPayment, $objCustomer, $objClientDatabase ) {
		$arrmixPrimaryCustomerAddress = [];
		// load the unit or property address - We should not take billing address from customer's record
		$arrmixUnitOrPropertyAddress = \Psi\Eos\Entrata\CUnitAddresses::createService()->fetchUnitPrimaryAddressForRPByPropertyUnitIdByCid( $this->getPropertyUnitId(), $this->getCid(), $objClientDatabase );
		if( false == valArr( $arrmixUnitOrPropertyAddress ) ) {
			$arrmixUnitOrPropertyAddress = \Psi\Eos\Entrata\CPropertyAddresses::createService()->fetchPropertyAddressForRPByPropertyIdByAddressTypeIdByCid( $this->getPropertyId(), ( array ) CAddressType::PRIMARY, $this->getCid(), $objClientDatabase );
		}
		if( true == valObj( $objCustomer, 'CCustomer' ) ) {
			$arrmixPrimaryCustomerAddress = \Psi\Eos\Entrata\CCustomerAddresses::createService()->fetchCustomerAddressRPByCustomerIdByAddressTypeIdByCid( $objCustomer->getId(), CAddressType::PRIMARY, $this->getCid(), $objClientDatabase );
		}

		if( true == valArr( $arrmixPrimaryCustomerAddress ) ) {
			$objArPayment->setBilltoStreetLine1( $arrmixPrimaryCustomerAddress[0]['street_line1'] );
			$objArPayment->setBilltoStreetLine2( $arrmixPrimaryCustomerAddress[0]['street_line2'] );
			$objArPayment->setBilltoStreetLine3( $arrmixPrimaryCustomerAddress[0]['street_line3'] );
			$objArPayment->setBilltoCity( $arrmixPrimaryCustomerAddress[0]['city'] );
			$objArPayment->setBilltoCountryCode( $arrmixPrimaryCustomerAddress[0]['country_code'] );
			$objArPayment->setBilltoStateCode( $arrmixPrimaryCustomerAddress[0]['state_code'] );
			$objArPayment->setBilltoProvince( $arrmixPrimaryCustomerAddress[0]['province'] );
			if( false == is_null( $arrmixPrimaryCustomerAddress[0]['postal_code'] ) && true == is_null( $objArPayment->getBilltoPostalCode() ) ) {
				$objArPayment->setBilltoPostalCode( $arrmixPrimaryCustomerAddress[0]['postal_code'] );
			}
		} elseif( true == valArr( $arrmixUnitOrPropertyAddress ) ) {
			$objArPayment->setBilltoStreetLine1( $arrmixUnitOrPropertyAddress[0]['street_line1'] );
			$objArPayment->setBilltoStreetLine2( $arrmixUnitOrPropertyAddress[0]['street_line2'] );
			$objArPayment->setBilltoStreetLine3( $arrmixUnitOrPropertyAddress[0]['street_line3'] );
			$objArPayment->setBilltoCity( $arrmixUnitOrPropertyAddress[0]['city'] );
			$objArPayment->setBilltoStateCode( $arrmixUnitOrPropertyAddress[0]['state_code'] );
			$objArPayment->setBilltoCountryCode( $arrmixUnitOrPropertyAddress[0]['country_code'] );
			if( false == is_null( $arrmixUnitOrPropertyAddress[0]['postal_code'] ) && true == is_null( $objArPayment->getBilltoPostalCode() ) ) {
				$objArPayment->setBilltoPostalCode( $arrmixUnitOrPropertyAddress[0]['postal_code'] );
			}
		} elseif( false == valArr( $arrmixUnitOrPropertyAddress ) && true == valObj( $objCustomer, 'CCustomer' ) ) {
			$objArPayment->setBilltoStreetLine1( $objCustomer->getPrimaryStreetLine1() );
			$objArPayment->setBilltoStreetLine2( $objCustomer->getPrimaryStreetLine2() );
			$objArPayment->setBilltoStreetLine3( $objCustomer->getPrimaryStreetLine3() );
			$objArPayment->setBilltoCity( $objCustomer->getPrimaryCity() );
			$objArPayment->setBilltoStateCode( $objCustomer->getPrimaryStateCode() );
			if( true == is_null( $objArPayment->getBilltoPostalCode() ) && false == is_null( $objCustomer->getPrimaryPostalCode() ) ) {
				$objArPayment->setBilltoPostalCode( $objCustomer->getPrimaryPostalCode() );
			}
		}
	}

	public function getResidentProfileUrl() {
		$strSecureHostPrefix	= ( true == defined( CONFIG_SECURE_HOST_PREFIX ) ) ? CONFIG_SECURE_HOST_PREFIX : 'http://';
		$strRwxSuffix			= ( false == is_null( CConfig::get( 'rwx_login_suffix' ) ) ) ? CConfig::get( 'rwx_login_suffix' ) : '.entrata.com';
		$objClient = CClients::fetchClientById( $this->getCid(), $this->m_objDatabase );
		$strResidentProfileUrl = '';
		if( true == valObj( $objClient, 'CClient' ) ) {
			$strResidentProfileUrl = $strSecureHostPrefix . $objClient->getRwxDomain() . $strRwxSuffix . '/?module=customers_systemxxx&load_tab=retrieve_customers_list&lease[id]=' . $this->getId() . '&customer[id]=' . $this->getPrimaryCustomerId() . ' ';
		}

		return $strResidentProfileUrl;
	}

	public function fetchOldLeaseDataByOldLeaseIdByActiveLeaseIntervalIdByTransferLeaseIdByCid( int $intOldLeaseId, int $intActiveLeaseIntervalId, int $intClientId, CDatabase $objDatabase, $intTransferLeaseId = NULL, $intLeaseStatusTypeId = NULL ) {

		$strWhereConditionSql	= ( valId( $intLeaseStatusTypeId ) ) ? ' AND cll.lease_status_type_id = ' . ( int ) $intLeaseStatusTypeId . ' AND cll.transfer_lease_id IS NULL  ' : '';
		$strWhereConditionSql	.= ( valId( $intTransferLeaseId ) ) ? ' AND cll.transfer_lease_id = ' . ( int ) $intTransferLeaseId . ' ' : '';
		$strOrderBySql			= ( valId( $intTransferLeaseId ) ) ? '  cll.id ASC ' : ' cll.id DESC';

		$strSql = 'SELECT
						cll.*
					FROM
						cached_lease_logs cll
					WHERE
						cll.lease_id = ' . ( int ) $intOldLeaseId . '
						AND cll.active_lease_interval_id = ' . ( int ) $intActiveLeaseIntervalId . '
						AND cll.cid = ' . ( int ) $intClientId . '
						' . $strWhereConditionSql . '
						ORDER BY ' . $strOrderBySql . ' LIMIT 1';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		return $arrintResponse[0];
	}

	public function getStudentRentRangeMaxAmenities() {
		$objApplication = \Psi\Eos\Entrata\CApplications::createService()->fetchApplicationByLeaseIdByCid( $this->getId(), $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objApplication, CApplication::class ) ) {
			return NULL;
		}

		return $objApplication->getPropertyFloorPlanRentRange( true );
	}

	public function getStudentRentRangeOptedAmenities() {
		$objApplication = \Psi\Eos\Entrata\CApplications::createService()->fetchApplicationByLeaseIdByCid( $this->getId(), $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objApplication, CApplication::class ) ) {
			return NULL;
		}

		return $objApplication->getPropertyFloorPlanRentRange( false, true );
	}

	public function getStudentLeaseTotalWAmenities() {
		$objApplication = \Psi\Eos\Entrata\CApplications::createService()->fetchApplicationByLeaseIdByCid( $this->getId(), $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objApplication, CApplication::class ) ) {
			return NULL;
		}

		return $objApplication->getPropertyFloorPlanRentRange( true, false, true );
	}

	public function setCustomerTypeId( $intCustomerTypeId ) {
		$this->set( 'm_intCustomerTypeId', CStrings::strToIntDef( $intCustomerTypeId, NULL, false ) );
	}

	public function getCustomerTypeId() {
		return $this->m_intCustomerTypeId;
	}

	public function getBalanceSummary() {

		$intBalance = $this->getBalanceForMergeField();

		$strTitle = __( 'Balance:' );
		if( 0 > $intBalance ) {
			$strTitle = __( 'Current Balance:' );
		} elseif( 0 < $intBalance ) {
			$strTitle = __( 'Outstanding Balance:' );
		}

		$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $this->m_objDatabase );

		$strCurrentDate = '';

		if( true == valObj( $objProperty, CProperty::class ) ) {
			$strCurrentDate = $objProperty->getCurrentDate();
		}

		$strHtmlContent = __( '<table style="width:100%; padding-top:10px;">
								<tr style="color:#5f5f5f; font-size:12px;">
									<td style="width:50%; text-align: right; margin-right:10px; ">Statement Date:</td>
									<td style="width:50%; margin-left:0px; ">{%s, 0}</td>
								 </tr>
								 <tr style="padding-bottom:10px; ">
									<td style="width:50% !important; font-size:14px; line-height:17px; font-weight:bold;text-align:right; margin-right:10px; color:#000000;">{%s, 1}</td>
									<td style="font-size:16px; line-height:19px; font-weight:bold; margin-left:0; color:#5f5f5f;">{%m, 2, n:()}</td>
								</tr>
								 {%s, 3}
							</table>', [ $strCurrentDate, $strTitle, $intBalance, $this->getRepaymentDue() ] );

		return $strHtmlContent;
	}

	public function getBalanceForMergeField() {
		$arrmixLeaseBalances = ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchLeaseBalancesByLedgerFilterByArCodeTypeIdsByLeaseIdByCid( $this->getId(), $this->getCid(), $this->m_objDatabase, true, [], [] );

		$arrmixLeaseBalances = rekeyArray( 'ledger_filter_id', $arrmixLeaseBalances );
		$arrmixRequiredParameters = $this->getRequiredParameters();
		$intLedgerFilterId = $arrmixRequiredParameters['ledger_filter_id'] ?? ( \Psi\Eos\Entrata\CLedgerFilters::createService()->fetchLedgerFilterByDefaultLedgerFilterIdByCid( CDefaultLedgerFilter::RESIDENT, $this->getCid(), $this->m_objDatabase ) )->getId();

		return $arrmixLeaseBalances[$intLedgerFilterId]['total_amount'];
	}

	public function getRepaymentDue() {
		if( true == $this->getHasRepaymentAgreement() ) {
			return '';
		}

		$arrmixRepayments = ( array ) \Psi\Eos\Entrata\CRepayments::createService()->fetchCurrentRepaymentDueDetailsByLeaseIdByCid( $this->getId(), $this->getCid(), $this->m_objDatabase );

		if( false == valArrKeyExists( $arrmixRepayments, 'current_balance' ) || 0 == $arrmixRepayments['current_balance'] ) {
			return '';
		}

		$strRepayment = __( '<tr style="padding-bottom:10px; ">
								<td style="width:50% !important; font-size:14px; line-height:17px; font-weight:bold; text-align:right; margin-right:10px; color:#000000;">Repayment Balance:</td>
								<td style="font-size:16px; line-height:19px; font-weight:bold; margin-left:0; color:#5f5f5f;">{%m, 0, n:()}</td>
							</tr>', [ $arrmixRepayments['current_balance'] ] );

		return $strRepayment;
	}

	public function getLedgerStatements() {
		$arrmixParameters = $this->getRequiredParameters();

		if( false == valArr( $arrmixParameters ) ) {
			return '';
		}

		if( true == $arrmixParameters['view_open_ledger'] ) {
			if( true == $arrmixParameters['is_resident_friendly'] ) {
				if( true == $arrmixParameters['is_corporate_tab'] ) {
					$objLedgerCriteria = CLedgerCriteria::forCorporateResidentFriendlyOpenItems( $this->getCid(), $this->getPropertyId(), $this->getId(), $arrmixParameters['filter_id'] );
				} else {

					$objLedgerCriteria = CLedgerCriteria::forResidentFriendlyOpenItems( $this->getCid(), $this->getPropertyId(), $this->getId(), $arrmixParameters['filter_id'] );
				}
			} else {
				$objLedgerCriteria = CLedgerCriteria::forOpenItems( $this->getCid(), $this->getPropertyId(), $this->getId(), $arrmixParameters['filter_id'] );
			}
		} else {
			if( true == $arrmixParameters['is_resident_friendly'] ) {
				if( true == $arrmixParameters['is_corporate_tab'] ) {
					$objLedgerCriteria = CLedgerCriteria::forCorporateResidentFriendlyFullLedger( $this->getCid(), $this->getPropertyId(), $this->getId(), $arrmixParameters['filter_id'] );
				} else {

					$objLedgerCriteria = CLedgerCriteria::forResidentFriendlyFullLedger( $this->getCid(), $this->getPropertyId(), $this->getId(), $arrmixParameters['filter_id'] );
				}
			} else {
				$objLedgerCriteria = CLedgerCriteria::forFullLedger( $this->getCid(), $this->getPropertyId(), $this->getId(), $arrmixParameters['filter_id'] );
			}
		}

		$strHtmlContent = '';

		if( true == $arrmixParameters['view_open_ledger'] ) {
			$strHtmlContent = $this->getOpenLedgerData( $objLedgerCriteria );
		} else {
			$strHtmlContent = $this->getFullLedgerData( $objLedgerCriteria );
		}
		return $strHtmlContent;
	}

	public function getFullLedgerData( $objLedgerCriteria ) {
		$arrmixParameters = $this->getRequiredParameters();

		if( true == $arrmixParameters['show_reversal'] ) {
			$objLedgerCriteria->setExcludeDeleted( false );
		}

		$objLedgerCriteria->setOccupancyTypeId( $this->getOccupancyTypeId() );
		$objLedgerCriteria->setShowResidentFriendlyMode( ( bool ) $arrmixParameters['is_resident_friendly'] )->setExcludeDeleted( !$arrmixParameters['show_reversal'] );

		if( $objLedgerCriteria->getShowResidentFriendlyMode() ) {
			$objLedgerCriteria->setSortBy( ( string ) $arrmixParameters['sort_by'] )
				->setSortOrder( ( string ) $arrmixParameters['sort_order'] )
				->setExcludeTemporary( !$arrmixParameters['show_temporary'] );
		}

		if( true == $arrmixParameters['is_inter_company_ap_transactions'] ) {
			$objLedgerCriteria->setApTransactionTypeIds( [ CApTransactionType::STANDARD_REIMBURSEMENT, CApTransactionType::STANDARD_REIMBURSEMENT_NEW ] );
		} else if( true == $arrmixParameters['is_billback_ap_transactions'] ) {
			$objLedgerCriteria->setApTransactionTypeIds( [ CApTransactionType::BILL_BACK_REIMBURSEMENT ] );
		}

		if( true == $objLedgerCriteria->getShowResidentFriendlyMode() ) {
			$objLedgerLibrary = CSummarizedLedgerLibrary::create( $objLedgerCriteria, $this->m_objDatabase );
		} else {
			$objLedgerLibrary = CNonSummarizedLedgerLibrary::create( $objLedgerCriteria, $this->m_objDatabase );
		}

		if( false == valObj( $objLedgerLibrary, CLedgerLibrary::class ) ) {
			return '';
		}

		$arrmixLedgerItems = $objLedgerLibrary->loadLedgerItemsWithRunningTotal( [] );
		$intBalance = $this->getBalanceForMergeField();

		$strHtmlLedgerStatement = __( '<h4 style="font-size:16px; line-height:19px; font-weight:bold; margin:0px; padding:0px">Full Ledger</h4>
									<br>
									<table style="border-collapse: collapse;border-spacing: 0;width: 100%;font-size:12px;">
										<tr>
											<th style="width:13%; border-bottom: 1px solid #000000; border-top: 1px solid #A3A3A3; font-weight: bold; text-align: left;">Date</th>
											<th style="width:13%; border-bottom: 1px solid #000000; border-top: 1px solid #A3A3A3; font-weight: bold; text-align: left;">Post Month</th>
											<th style="width:14%; border-bottom: 1px solid #000000; border-top: 1px solid #A3A3A3; font-weight: bold; text-align: left;">Transaction Id</th>
											<th style="width:45%; border-bottom: 1px solid #000000; border-top: 1px solid #A3A3A3; font-weight: bold; text-align: left;">Memo</th>
											<th style="width:5%; border-bottom: 1px solid #000000; border-top: 1px solid #A3A3A3; font-weight: bold; text-align: left;">Allocated</th>
											<th style="width:10%; border-bottom: 1px solid #000000; border-top: 1px solid #A3A3A3; font-weight: bold; padding: 6px; text-align: right;">Charges</th>
											<th style="width:10%; border-bottom: 1px solid #000000; border-top: 1px solid #A3A3A3; font-weight: bold; padding: 6px; text-align: right;">Payments</th>
											<th style="width:10%; border-bottom: 1px solid #000000; border-top: 1px solid #A3A3A3; font-weight: bold; padding: 6px; text-align: right;">Balance</th>
										</tr>
										<tablebody>
									</table>' );

		$strTableBody = '';

		if( true == valArr( $arrmixLedgerItems ) ) {
			foreach( $arrmixLedgerItems as $arrmixLedgerItemDetails ) {
				$strAllocatedFor = __( 'Partial' );
				if( 0 === $arrmixLedgerItemDetails['total_amount_due'] ) {
					$strAllocatedFor = __( 'Full' );
				} elseif( $arrmixLedgerItemDetails['charge_amount'] == abs( $arrmixLedgerItemDetails['total_amount_due'] ) || abs( $arrmixLedgerItemDetails['payment_amount'] ) == abs( $arrmixLedgerItemDetails['total_amount_due'] ) ) {
					$strAllocatedFor = __( 'None' );
				}

				$strTableBody .= __( '<tr>
										<td nowrap>{%t, 0, DATE_ALPHA_SHORT}</td>
										<td>{%t, 1, DATE_NUMERIC_POSTMONTH}</td>
										<td>{%d, 2}</td>
										<td>{%s, 3}</td>
										<td>{%s, 4}</td>
										<td style="text-align: right !important;">{%m, 5, n:()}</td>
										<td style="text-align: right !important;">{%m, 6, n:()}</td>
										<td style="text-align: right !important;">{%m, 7, n:()}</td>
									</tr>', [ $arrmixLedgerItemDetails['post_date'], $arrmixLedgerItemDetails['post_month'], $arrmixLedgerItemDetails['id'], $arrmixLedgerItemDetails['memo'], $strAllocatedFor, $arrmixLedgerItemDetails['charge_amount'], $arrmixLedgerItemDetails['payment_amount'], $arrmixLedgerItemDetails['total_amount_due'] ] );
			}
		}

		$strTableBody .= __( '<tr class="total">
							<th colspan="7" class="align-right" style="text-align:right; border-bottom:1px solid #A3A3A3 ; border-top:1px solid #000; padding:8px; color:#000000;">Current Balance:</th>
							<th class="align-right" style="text-align:right; border-bottom:1px solid #A3A3A3 ; border-top:1px solid #000; padding:8px; color:#000000;" >{%m, 0, n:()}</th>
						</tr>', [ $intBalance ] );

		return str_replace( '<tablebody>', $strTableBody, $strHtmlLedgerStatement );
	}

	public function getOpenLedgerData( $objLedgerCriteria ) {
		$arrmixParameters = $this->getRequiredParameters();
		$objLedgerCriteria->setOccupancyTypeId( $this->getOccupancyTypeId() );
		$objLedgerCriteria->setShowResidentFriendlyMode( ( bool ) $arrmixParameters['is_resident_friendly'] );

		if( true == $objLedgerCriteria->getShowResidentFriendlyMode() ) {
			$objLedgerLibrary = CSummarizedLedgerLibrary::create( $objLedgerCriteria, $this->m_objDatabase );
		} else {
			$objLedgerLibrary = CNonSummarizedLedgerLibrary::create( $objLedgerCriteria, $this->m_objDatabase );
		}

		if( false == valObj( $objLedgerLibrary, CLedgerLibrary::class ) ) {
			return '';
		}

		$arrmixLedgerItems = $objLedgerLibrary->loadLedgerItemsWithRunningTotal( [] );
		$intBalance = $this->getBalanceForMergeField();

		$strHtmlLedgerStatement = __( '<h4 style="font-size:16px; line-height:19px; font-weight:bold; margin: 0; padding: 0;">Open Items</h4>
									<br/>
									<h4 style="font-size:14px; line-height:19px; font-weight:bold; margin: 0; padding: 0;">Unapplied Charges/Credits</h4>
									<br>
									<table style="border-collapse: collapse;  border-spacing: 0; width: 100%; font-size:12px; ">
										<tr>
											<th style="border-bottom: 1px solid #000000; border-top: 1px solid #A3A3A3; font-weight: bold; padding: 8px; text-align: left; color:#000000;">Date</th>
											<th style="border-bottom: 1px solid #000000; border-top: 1px solid #A3A3A3; font-weight: bold; padding: 8px; text-align: left; color:#000000;">Description</th>
											<th style="border-bottom: 1px solid #000000; border-top: 1px solid #A3A3A3; font-weight: bold; padding: 8px; text-align:right; color:#000000;">Charges</th>
											<th style="border-bottom: 1px solid #000000; border-top: 1px solid #A3A3A3; font-weight: bold; padding: 8px; text-align:right; color:#000000;">Payments</th>
										</tr>
										<tablebody>
									</table>
									<br/>
									<br/>' );

		$strTableBody = '';

		if( true == valArr( $arrmixLedgerItems ) ) {
			foreach( $arrmixLedgerItems as $arrmixLedgerItemDetails ) {
				$strTableBody .= __( '<tr>
									<td nowrap style="padding: 8px">{%t, 0, DATE_ALPHA_SHORT}</td>
									<td style="padding: 8px">{%s, 1}</td>
									<td style="text-align:right; padding: 8px">{%m, 2, n:()}</td>
									<td>{%m, 3, n:()}</td>
								</tr>',
					[ $arrmixLedgerItemDetails['post_date'], $arrmixLedgerItemDetails['memo'], $arrmixLedgerItemDetails['charge_amount'], $arrmixLedgerItemDetails['payment_amount'] ] );
			}
		}

		$strTableBody .= __( '<tr class="total">
									<th colspan="3" style="text-align:right; border-bottom:1px solid #A3A3A3 ; border-top:1px solid #000; padding:8px; color:#000000">Current Balance:</th>
									<th style="text-align:right; border-bottom:1px solid #A3A3A3 ; border-top:1px solid #000; padding:8px;" >({%m, 0, n:()})</th>
								</tr>', [ $intBalance ] );

		$strHtmlLedgerStatement = str_replace( '<tablebody>', $strTableBody, $strHtmlLedgerStatement );
		$strHtmlLedgerStatement .= $this->getRePaymentAgreements();

		return $strHtmlLedgerStatement;
	}

	public function getIncompleteChecklistItems() {
		$strIncompleteChecklistItems = NULL;
		$arrobjIncompleteChecklistItems = \Psi\Eos\Entrata\CChecklistItems::createService()->fetchIncompleteChecklistItemsByLeaseIdByLeaseIntervalIdByCid( $this->getId(), $this->getActiveLeaseIntervalId(), $this->getCid(), $this->m_objDatabase );

		if( true == valArr( $arrobjIncompleteChecklistItems ) ) {
			$strIncompleteChecklistItems = '<ul>';

			foreach( $arrobjIncompleteChecklistItems as $objIncompleteChecklistItem ) {
				$strIncompleteChecklistItems .= '<li style="padding: 10px; margin: 0;"> ' . $objIncompleteChecklistItem->getItemTitle() . ' </li>';
			}

			$strIncompleteChecklistItems .= '</ul>';
		}

		return $strIncompleteChecklistItems ?? '-';
	}

	public function getRePaymentAgreements() {
		$arrobjRepaymentAgreements = ( array ) \Psi\Eos\Entrata\CRepayments::createService()->fetchAllRepaymentsByLeaseIdByCid( $this->getId(), $this->getCid(), $this->m_objDatabase, $boolActiveOnly = true );

		if( false == valArr( $arrobjRepaymentAgreements ) ) {
			return '';
		}

		$arrmixRepayments = ( array ) \Psi\Eos\Entrata\CRepayments::createservice()->fetchCurrentRepaymentDueDetailsByLeaseIdByCid( $this->getId(), $this->getCid(), $this->m_objDatabase );

		$strHtmlData = __( '<h4 style="font-size:14px; line-height:19px; font-weight:bold; margin: 0; padding: 0;">Repayment Agreements</h4>
						<br/>
						<table style="border-collapse: collapse;  border-spacing: 0; width: 100%; font-size:12px;">
							<tr>
								<th style="border-bottom: 1px solid #000000; border-top: 1px solid #A3A3A3; font-weight: bold; padding: 8px; text-align: left;">Id</th>
								<th class="align-left" style="border-bottom: 1px solid #000000; border-top: 1px solid #A3A3A3; font-weight: bold; padding: 8px; text-align: left;">Start Date</th>
								<th style="border-bottom: 1px solid #000000; border-top: 1px solid #A3A3A3; font-weight: bold; padding: 8px; text-align: left;">End Date</th>
								<th class="align-center" style="border-bottom: 1px solid #000000; border-top: 1px solid #A3A3A3; font-weight: bold; padding: 8px; text-align:center;">Payments Due On</th>
								<th class="align-right" style="border-bottom: 1px solid #000000; border-top: 1px solid #A3A3A3; font-weight: bold; padding: 8px; text-align:right;">Payment Amount</th>
								<th class="align-right" style="border-bottom: 1px solid #000000; border-top: 1px solid #A3A3A3; font-weight: bold; padding: 8px; text-align:right;">Due Now</th>
								<th class="align-right" style="border-bottom: 1px solid #000000; border-top: 1px solid #A3A3A3; font-weight: bold; padding: 8px; text-align:right;">Balance</th>
							</tr>
							<tablebody>
						</table>' );

		$strTableData = '';

		foreach( $arrobjRepaymentAgreements as $objRepaymentAgreement ) {
			$strOffsetDays = $objRepaymentAgreement->getOffsetDays() . __( 'th' );

			if( false == in_array( $objRepaymentAgreement->getOffsetDays() % 100, [ 11, 12, 13 ] ) ) {
				if( 1 == $objRepaymentAgreement->getOffsetDays() ) {
					$strOffsetDays = $objRepaymentAgreement->getOffsetDays() . __( 'st' );
				} elseif( 2 == $objRepaymentAgreement->getOffsetDays() % 10 ) {
					$strOffsetDays = $objRepaymentAgreement->getOffsetDays() . __( 'nd' );
				} elseif( 3 == $objRepaymentAgreement->getOffsetDays() % 10 ) {
					$strOffsetDays = $objRepaymentAgreement->getOffsetDays() . __( 'rd' );
				} else {
					$strOffsetDays = $objRepaymentAgreement->getOffsetDays() . __( 'th' );
				}
			}

			$strTableData .= __( '<tr>
								<td nowrap style="padding: 8px">{%d, 0}</td>
								<td style="text-align:left; padding:8px">{{%t, 1, DATE_ALPHA_SHORT}}</td>
								<td style="text-align:left; padding:8px">{{%t, 2, DATE_ALPHA_SHORT}}</td>
								<td style="text-align:center; padding:8px">{%s, 3}</td>
								<td style="text-align:right;  padding: 8px">{{%m, 4, n:()}</td>
								<td style="text-align:right;  padding: 8px">{{%m, 5, n:()}</td>
								<td style="text-align:right;  padding: 8px">{{%m, 6, n:()}</td>
							</tr>', [ $objRepaymentAgreement->getId(), $objRepaymentAgreement->getStartsOn(), $objRepaymentAgreement->getEndsOn(), $strOffsetDays, $objRepaymentAgreement->getAgreementAmountDue(), $objRepaymentAgreement->getCurrentAmountDue(), $objRepaymentAgreement->getCurrentBalance() ] );
		}

		$strTableData .= __( '<tr class="total">
							<th colspan="6" style="text-align:right; border-bottom:1px solid #A3A3A3 ; border-top:1px solid #000; padding:8px;padding-right: 51px;">Current Balance:</th>
							<th style="text-align:right; border-bottom:1px solid #A3A3A3 ; border-top:1px solid #000; padding:8px;">{%m, 0, n:()}</th>
						</tr>', [ $arrmixRepayments['current_balance'] ?? 0 ] );

		return str_replace( '<tablebody>', $strTableData, $strHtmlData );
	}

	/**
	 * @param $intOccupancyTypeId
	 * @param $intPropertyId
	 * @param $intCid
	 * @param $objDatabase
	 * @return array
	 */
	public function getAllowedLedgerFilterIds( $objDatabase ) : array {
		$arrmixAllowedLedgerFilters = ( array ) \Psi\Eos\Entrata\CPropertyLedgerFilters::createService()->fetchPublishedPropertyLedgerFiltersByOccupancyTypeIdByPropertyIdByCid( $this->getOccupancyTypeId(), $this->getPropertyId(), $this->getCid(), $objDatabase );

		$arrintAllowedLedgerFilterIds = array_map( function( $arrmixAllowedLedgerFilter ){
			return  $arrmixAllowedLedgerFilter['ledger_filter_id'];
		}, $arrmixAllowedLedgerFilters );

		return $arrintAllowedLedgerFilterIds;
	}

	/**
	 * @param $objLease
	 * @param $objdatabase
	 * @return bool
	 */
	public function showDepositWarning( $objDatabase ) : bool {

		$objArTransactionFilter = new CArTransactionsFilter();
		$objArTransactionFilter->setArCodeTypeIds( [ CArCodeType::DEPOSIT ] );
		$objArTransactionFilter->setOutstandingOnly( true );
		$objArTransactionFilter->setLeaseIds( [ $this->getId() ] );

		$arrobjDepositArTransactions        = ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchDepositHeldArTransactionsByLeaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		$arrobjUnpaidDepositArTransactions  = ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionsByArTransactionsFilterByCid( $objArTransactionFilter, $this->getCid(), $objDatabase );

		return $this->getSelectedDepositAlternative() && ( valArr( $arrobjDepositArTransactions ) || valArr( $arrobjUnpaidDepositArTransactions ) );
	}

	/**
	 * @return bool
	 */
	public function isAffordableLease() : bool {
		return $this->getOccupancyTypeId() == \COccupancyType::AFFORDABLE;
	}

	public function exportMoveOutStatus( $intCompanyUserId, $objProperty, $objDatabase ) {

		$boolIsValid 								= true;
		if( false == valObj( $objProperty, 'CProperty', 'RemotePrimaryKey' ) ) {
			return true;
		}
		$objIntegrationDatabase = $objProperty->fetchIntegrationDatabase( $objDatabase );

		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) || ( false == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), [ CIntegrationClientType::EMH, CIntegrationClientType::EMH_ONE_WAY ] ) ) ) return true;

		$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $objProperty->getCid(), CIntegrationService::UPDATE_MOVE_OUT_STATUS, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setLease( $this );
		$objGenericWorker->setProperty( $objProperty );
		$boolIsValid = $objGenericWorker->process();

		if( true == valArr( $objGenericWorker->getErrorMsgs() ) && true == $this->getIsShowIntegrationError() ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		return $boolIsValid;
	}

	public function exportUpdateOccupancy( $intNewLeaseId, $objOldUnitSpace, $objNewUnitSpace, $objProperty, $intCompanyUserId, $objDatabase ) {
		$boolIsValid 								= true;

		if( false == valObj( $objProperty, 'CProperty', 'RemotePrimaryKey' ) ) {
			return true;
		}

		$objIntegrationDatabase = $objProperty->fetchIntegrationDatabase( $objDatabase );

		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) || ( $objIntegrationDatabase->getIntegrationClientTypeId() != CIntegrationClientType::EMH_ONE_WAY ) ) return true;
		$arrstrTransportRules = [ 'intNewLeaseId' => $intNewLeaseId, 'stroldUnitSpaceRPK' => $objOldUnitSpace->getRemotePrimaryKey(), 'strnewUnitSpaceRPK' => $objNewUnitSpace->getRemotePrimaryKey() ];
		$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $this->getCid(), CIntegrationService::UPDATE_OCCUPANCY_STATUS, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setLease( $this );
		$objGenericWorker->setTransportRules( $arrstrTransportRules );
		$objGenericWorker->setProperty( $objProperty );
		$boolIsValid = $objGenericWorker->process();

		if( true == valArr( $objGenericWorker->getErrorMsgs() ) && true == $this->getIsShowIntegrationError() ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		return $boolIsValid;
	}

}
?>