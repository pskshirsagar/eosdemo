<?php

class CIntegrationVersion extends CBaseIntegrationVersion {

	const VERSION_550  			= 1;
	const VERSION_552  			= 2;
	const VERSION_552_IRVINE  	= 4;
	const RM_94		  			= 5;
	const RM_96		  			= 6;
}
?>