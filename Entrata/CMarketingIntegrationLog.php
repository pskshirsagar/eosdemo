<?php

class CMarketingIntegrationLog extends CBaseMarketingIntegrationLog {

	use TEosStoredObject;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInternetListingServiceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMarketingIntegrationLogTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndTime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResponseStatus() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valServiceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCategoryType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndPointUrl() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartTime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * @param $intDuration
	 * @return array
	 */
	public static function getDatesByDuration( $intDuration ) : array {
		switch( $intDuration ) {
			case 1:
				$strFromDate = date( 'Y-m-d', strtotime( '-' . '1day' ) );
				$strToDate   = date( 'Y-m-d', strtotime( '-' . '1day' ) );
				break;

			case 6:
				$strFromDate = date( 'Y-m-d', strtotime( '-' . '6day' ) );
				$strToDate   = date( 'Y-m-d' );
				break;

			case 29:
				$strFromDate = date( 'Y-m-d', strtotime( '-' . '29day' ) );
				$strToDate   = date( 'Y-m-d' );
				break;

			case 0:
			default:
				$strFromDate = $strToDate = date( 'Y-m-d' );
				break;
		}

		return [ $strToDate, $strFromDate ];
	}

	protected function calcStorageContainer( $strVendor = NULL ) : string {
		switch( $strVendor ) {
			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				return CMarketingHubLibrary::GLOBAL_PATH;
			default:
			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3:
				return CONFIG_AWS_CLIENTS_BUCKET_NAME;

		}
	}

	protected function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) : string {
		switch( $strReferenceTag ) {
			case 'old_logs':
				return 'ils/' . $this->getDetails()->file_path . $this->getDetails()->file_name;
				break;
			case 'facebook_webhook_log':
			default:
				return $this->getCid() . CMarketingHubLibrary::LOGS_PATH . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/' . $this->sqlInternetListingServiceId() . '/' . strtotime( date( 'm/d/Y H:i:s' ) ) . '.xml';
		}
	}

}
?>