<?php

class CPropertyGroupAssociation extends CBasePropertyGroupAssociation {

	protected $m_strName;
	protected $m_strPropertyName;

	/**
	 * Get Functions
	 */

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getName() {
		return $this->m_strName;
	}

	/**
	 * Set Functions
	 */

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = CStrings::strTrimDef( $strPropertyName, 50, NULL, true );
	}

	public function setName( $strName ) {
		$this->m_strName = CStrings::strTrimDef( $strName, 100, NULL, true );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_name'] ) )		$this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['name'] ) )				$this->setName( $arrmixValues['name'] );

		return;
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyGroupId( $objDatabase ) {
		$boolIsValid = true;

		// Checking if the Property Group Type ID provided exists
		if( true == is_null( \Psi\Eos\Entrata\CPropertyGroups::createService()->fetchPropertyGroupByIdByCid( $this->getPropertyGroupId(), $this->getCid(), $objDatabase ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_group_type_id', __( 'Property Group Selected does not exists' ) ) );
		}

	    return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyGroupId( $objDatabase );
				$boolIsValid &= $this->valPropertyId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

}
?>