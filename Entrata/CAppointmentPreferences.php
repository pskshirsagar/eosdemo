<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAppointmentPreferences
 * Do not add any new functions to this class.
 */

class CAppointmentPreferences extends CBaseAppointmentPreferences {

	public static function fetchAvailabilityAppointmentPreferencesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $intDayOfWeek = NULL, $strAppointmentPreferenceType = 'Staff' ) {

		$strDaysOfWeek = ( NULL !== $intDayOfWeek ) ? 'unnest( array[' . intval( $intDayOfWeek ) . '] )' : 'generate_series( 1, 7 )';

		$strKey = 'ENTRATA_CALENDAR_USE_APPOINTMENT_PREFERENCES';
		if( 'Self Guided Tour' == $strAppointmentPreferenceType ) {
			$strKey = 'ENTRATA_CALENDAR_USE_SELF_GUIDED_TOUR_PREFERENCES';
		}
		$strSql = '
				SELECT
					week_days.day_of_week AS day_of_week,
					CASE
						WHEN pp.value = \'1\' THEN ap.available_start
						ELSE poh.open_time
					END AS available_start,
					CASE
						WHEN pp.value = \'1\' THEN ap.available_end
						ELSE poh.close_time
					END AS available_end,
					poh_lunch_hours.open_time AS lunch_start_time,
					poh_lunch_hours.close_time AS lunch_end_time,
					ap.max_simultaneous_appointments,
					CASE
						WHEN pp.value = \'1\' AND ap.available_start IS NULL AND ap.available_end IS NULL THEN TRUE
						ELSE poh.by_appointment_only
					END AS by_appointment_only
				FROM
					' . $strDaysOfWeek . ' AS week_days( day_of_week )
					LEFT JOIN appointment_preferences AS ap ON (
						ap.property_id = ' . intval( $intPropertyId ) . '
						AND ap.cid = ' . intval( $intCid ) . '
						AND ap.day_of_week = week_days.day_of_week
						AND ap.appointment_preference_type = \'' . $strAppointmentPreferenceType . '\' )
					LEFT JOIN property_preferences AS pp ON (
						pp.property_id = ' . intval( $intPropertyId ) . '
						AND pp.cid = ' . intval( $intCid ) . '
						AND pp.key = \'' . $strKey . '\' )
					LEFT JOIN property_hours AS poh ON (
						( pp.value IS NULL OR pp.value != \'1\' )
						AND poh.property_id = ' . intval( $intPropertyId ) . '
						AND poh.cid = ' . intval( $intCid ) . '
						AND poh.property_hour_type_id = ' . CPropertyHourType::OFFICE_HOURS . '
						AND poh.is_old_format <> TRUE
						AND poh.is_published = TRUE
						AND poh.day = week_days.day_of_week )
					LEFT JOIN property_hours AS poh_lunch_hours ON (
						poh_lunch_hours.property_id = poh.property_id
						AND poh_lunch_hours.cid = poh.cid
						AND poh_lunch_hours.property_hour_type_id = ' . CPropertyHourType::OFFICE_HOURS . '
						AND poh_lunch_hours.is_old_format <> TRUE
						AND poh_lunch_hours.is_published = TRUE
						AND poh_lunch_hours.day = poh.day + 10 )
				WHERE
					( pp.value = \'1\' AND ap.id IS NOT NULL )
					OR ( ( pp.value IS NULL OR pp.value != \'1\' ) AND poh.id IS NOT NULL )';

		if( NULL !== $intDayOfWeek ) {
			return self::fetchAppointmentPreference( $strSql, $objDatabase );
		} else {
			$strSql .= ' ORDER BY week_days.day_of_week ASC';
			return self::fetchAppointmentPreferences( $strSql, $objDatabase );
		}
	}

}
?>