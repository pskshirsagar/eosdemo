<?php

class CBudgetWorksheet extends CBaseBudgetWorksheet {

	protected $m_arrobjBudgetWorksheetGlAccounts;
	protected $m_strApprovalNote;

	const PERCENTAGE_CONSTANT	= 100;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	/**
	 * set Functions
	 */

	public function setWorksheetGlAccounts( $arrobjBudgetWorksheetGlAccounts ) {
		$this->m_arrobjBudgetWorksheetGlAccounts = $arrobjBudgetWorksheetGlAccounts;
	}

	public function setApprovalNote( $strApprovalNote ) {
		$this->m_strApprovalNote = $strApprovalNote;
	}

	/**
	 * get Functions
	 */

	public function getWorksheetGlAccounts() {
		return $this->m_arrobjBudgetWorksheetGlAccounts;
	}

	public function valBudgetDataSourceTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getBudgetDataSourceTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'budget_data_source_type_id', __( 'Data Source is required.' ) ) );
		} else {
			$boolIsValid = $this->valBudgetDataSourceSubTypeId();
		}

		return $boolIsValid;
	}

	public function getApprovalNote() {
		return $this->m_strApprovalNote;
	}

	/**
	 * validate Functions
	 */

	public function valBudgetWorksheetTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getBudgetWorksheetTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'budget_worksheet_type_id', __( 'Budget Worksheet Template is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valBudgetDataSourceSubTypeId() {
		$boolIsValid = true;

		if( CBudgetDataSourceType::SYSTEM_DATA == $this->getBudgetDataSourceTypeId() ) {

			if( false == valId( $this->getBudgetDataSourceSubTypeId() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'budget_data_source_sub_type_id', __( 'Initial Data from is required.' ) ) );
			}

			if( self::PERCENTAGE_CONSTANT < abs( $this->getAdjustmentPercent() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'adjustment_percent', __( 'Percentage should be less than or equal to 100.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Budget Worksheet Name is required.' ) ) );
		}

		$intBudgetWorksheetCount    = CBudgetWorksheets::fetchBudgetWorksheetCount( 'WHERE name = \'' . addslashes( $this->getName() ) . '\' AND id <>' . ( int ) $this->getId() . ' AND budget_workbook_id = ' . $this->getBudgetWorkbookId() . ' AND cid =' . $this->getCid() . ' AND deleted_on IS NULL', $objDatabase );

		if( 0 < $intBudgetWorksheetCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Budget Worksheet Name already exists.' ) ) );
		}

		return $boolIsValid;
	}

	public function valWorksheetGlAccounts() {

		$boolIsValid = true;

		if( false == valArr( $this->getWorksheetGlAccounts() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'budget_worksheet_gl_accounts', __( 'Please select at least one GL Account.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valBudgetWorksheetTypeId();
				$boolIsValid &= $this->valName( $objDatabase );
				if( CBudgetWorksheetType::BUDGET_WORKSHEET_TYPE_RENTAL_INCOME != $this->getBudgetWorksheetTypeId() ) {
					$boolIsValid &= $this->valWorksheetGlAccounts();
					$boolIsValid &= $this->valBudgetDataSourceTypeId();
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * create Functions
	 */

	public function createBudgetWorksheetGlAccount() {
		$objBudgetWorksheetGlAccount = new CBudgetWorksheetGlAccount();
		$objBudgetWorksheetGlAccount->setCid( $this->getCid() );
		$objBudgetWorksheetGlAccount->setBudgetWorksheetId( $this->getId() );
		$objBudgetWorksheetGlAccount->setBudgetDataSourceTypeId( $this->getBudgetDataSourceTypeId() );
		$objBudgetWorksheetGlAccount->setBudgetDataSourceSubTypeId( $this->getBudgetDataSourceSubTypeId() );
		$objBudgetWorksheetGlAccount->setAdjustmentPercent( $this->getAdjustmentPercent() );

		return $objBudgetWorksheetGlAccount;
	}

	public function createConcessionBudgetWorksheetGlAccount() {

		$objBudgetWorksheetGlAccount = new CBudgetWorksheetGlAccount();
		$objBudgetWorksheetGlAccount->setCid( $this->getCid() );
		$objBudgetWorksheetGlAccount->setBudgetWorksheetId( $this->getId() );
		$objBudgetWorksheetGlAccount->setBudgetWorksheetDataTypeId( CBudgetWorksheetDataType::CONCESSION );

		return $objBudgetWorksheetGlAccount;
	}

	public function createUnrecoverableBadDebtBudgetWorksheetGlAccount() {
		$objBudgetWorksheetGlAccount = new CBudgetWorksheetGlAccount();
		$objBudgetWorksheetGlAccount->setCid( $this->getCid() );
		$objBudgetWorksheetGlAccount->setBudgetWorksheetId( $this->getId() );
		$objBudgetWorksheetGlAccount->setBudgetDataSourceTypeId( CBudgetDataSourceType::SYSTEM_DATA );

		return $objBudgetWorksheetGlAccount;
	}

}
?>