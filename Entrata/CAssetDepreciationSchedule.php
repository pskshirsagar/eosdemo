<?php

class CAssetDepreciationSchedule extends CBaseAssetDepreciationSchedule {

	/**
	 * Validation Functions
	 */

	public function valName() {
        $boolIsValid = true;

        if( true == is_null( $this->getName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
        }

        return $boolIsValid;
    }

    public function valMonthlyDepreciation() {
        $boolIsValid = true;

        if( false == is_numeric( $this->getMonthlyDepreciation() ) && false == is_null( $this->getMonthlyDepreciation() ) ) {
            $boolIsValid = false;

            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'monthly_depreciation', 'Monthly Depreciation Should be Numeric.' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valName();
            	$boolIsValid &= $this->valMonthlyDepreciation();
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName();
            	$boolIsValid &= $this->valMonthlyDepreciation();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>