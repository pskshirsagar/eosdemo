<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultFileTypes
 * Do not add any new functions to this class.
 */

class CDefaultFileTypes extends CBaseDefaultFileTypes {

	public static function fetchDefaultFileTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CDefaultFileType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDefaultFileType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CDefaultFileType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>