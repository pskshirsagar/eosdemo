<?php

class CUnitOfMeasure extends CBaseUnitOfMeasure {

	protected $m_strUnitOfMeasureType;

	public function getUnitOfMeasureType() {
		return $this->m_strUnitOfMeasureType;
	}

	public function setUnitOfMeasureType( $strUnitOfMeasureType ) {
		$this->m_strUnitOfMeasureType = $strUnitOfMeasureType;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['unit_of_measure_type'] ) ) $this->setUnitOfMeasureType( $arrmixValues['unit_of_measure_type'] );

		return;
	}

	public function valUnitOfMeasureTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getUnitOfMeasureTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_of_measure_type_id', __( 'Unit of measure type is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valName( $objClientDatabase, $arrobjUnitOfMeasureType = [], $boolIsCreateUnitOfMeasureFromCatalog = false ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			if( true == $boolIsCreateUnitOfMeasureFromCatalog ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Purchasing Unit of Measure Name is required.' ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
			}
		}

		if( true == $boolIsValid ) {

			$strWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND LOWER( name ) = LOWER( \'' . addslashes( $this->getName() ) . '\' )
							AND unit_of_measure_type_id = ' . ( int ) $this->getUnitOfMeasureTypeId() . '
							AND deleted_by IS NULL';

			if( true == valId( $this->getId() ) ) {
				$strWhere .= ' AND id <> ' . ( int ) $this->getId();
			}

			if( 0 < \Psi\Eos\Entrata\CUnitOfMeasures::createService()->fetchUnitOfMeasuresCount( $strWhere, $objClientDatabase ) ) {
				$boolIsValid = false;

				$strUnitOfMeasureType = ( true == valObj( getArrayElementByKey( $this->getUnitOfMeasureTypeId(), $arrobjUnitOfMeasureType ), 'CUnitOfMeasureType' ) ) ? $arrobjUnitOfMeasureType[$this->getUnitOfMeasureTypeId()]->getName() : '';

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( "Name '{%s, 0}' already exists in selected unit type '{%s, 1}'.", [ $this->getName(), $strUnitOfMeasureType ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valUnitOfMeasureDependencies( $objClientDatabase, $boolIsDisableCheck = false ) {

		$boolIsValid			= true;
		$strDisableValidation	= NULL;

		if( true == $boolIsDisableCheck ) {
			$strDisableValidation = __( '&nbspcannot be disabled because it' );
		}

		$strWhere = 'WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND unit_of_measure_id = ' . ( int ) $this->getId();

		if( 0 < \Psi\Eos\Entrata\CAssetTransactions::createService()->fetchAssetTransactionCount( $strWhere, $objClientDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( "'{%s, 0}' {%s, 1} is associated to inventory adjustment.", [ $this->getName(), $strDisableValidation ] ) ) );
			$boolIsValid &= false;
		}

		$strWhere = 'WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND unit_of_measure_id = ' . ( int ) $this->getId() . '
						AND deleted_by IS NULL';

		if( 0 < \Psi\Eos\Entrata\CApCodes::createService()->fetchApCodeCount( $strWhere, $objClientDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( "'{%s, 0}' {%s, 1} is associated to catalog items.", [ $this->getName(), $strDisableValidation ] ) ) );
			$boolIsValid &= false;
		}

		$strWhere = 'WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND ( from_unit_of_measure_id = ' . ( int ) $this->getId() . ' OR to_unit_of_measure_id = ' . ( int ) $this->getId() . ')';

		if( 0 < \Psi\Eos\Entrata\CUnitOfMeasureConversions::createService()->fetchUnitOfMeasureConversionCount( $strWhere, $objClientDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( "'{%s, 0}' {%s, 1} is associated to unit conversions.", [ $this->getName(), $strDisableValidation ] ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valIsPublished( $objClientDatabase ) {

		$boolIsValid = true;

		if( 0 == $this->getIsPublished() ) {

			$boolIsValid &= $this->valUnitOfMeasureDependencies( $objClientDatabase, true );
		}

		return $boolIsValid;
	}

	public function valCode() {

		$boolIsValid = true;

		if( false == valStr( $this->getCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'code', __( 'Abbreviation is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase, $arrobjUnitOfMeasureType = [],$boolIsCreateUnitOfMeasureFromCatalog = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valUnitOfMeasureTypeId();
				$boolIsValid &= $this->valName( $objClientDatabase, $arrobjUnitOfMeasureType, $boolIsCreateUnitOfMeasureFromCatalog );
				$boolIsValid &= $this->valIsPublished( $objClientDatabase );
				$boolIsValid &= $this->valCode();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valUnitOfMeasureDependencies( $objClientDatabase );
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>