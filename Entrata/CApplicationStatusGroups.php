<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationStatusGroups
 * Do not add any new functions to this class.
 */

class CApplicationStatusGroups extends CBaseApplicationStatusGroups {

	public static function fetchApplicationStatusGroup( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CApplicationStatusGroup', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}
}
?>