<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFieldValues
 * Do not add any new functions to this class.
 */

class CFieldValues extends CBaseFieldValues {

	public static function fetchFieldValuessByFieldIdsByReferenceIdByCid( $intFieldIds, $intReferenceId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						field_values
					WHERE
						field_id IN (' . implode( ',', $intFieldIds ) . ' )
						AND reference_id = ' . ( int ) $intReferenceId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchFieldValues( $strSql, $objDatabase );

	}

}
?>