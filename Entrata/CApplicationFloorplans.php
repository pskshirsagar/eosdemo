<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationFloorplans
 * Do not add any new functions to this class.
 */

class CApplicationFloorplans extends CBaseApplicationFloorplans {

	public static function fetchApplicationFloorPlanCountByApplicationIdByPropertyIdsByCid( $intApplicationId, $arrintPropertyIds, $intCid, $objDatabase ) {

		$arrintApplicationStageStatusIds = [
			CApplicationStage::APPLICATION => [ CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ],
			CApplicationStage::LEASE => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED ]
		];

		$strSql = 'SELECT
						count( ap.id ) as count
					FROM
						applications ap
						LEFT JOIN application_floorplans af ON ( af.cid = ap.cid AND af.application_id = ap.id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ( ap.property_floorplan_id IS NOT NULL OR af.property_floorplan_id IS NOT NULL )
						AND ap.id = ' . ( int ) $intApplicationId . '
						AND (ap.application_stage_id,ap.application_status_id) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )';

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchApplicationFloorplansByIdApplicationIdByCid( $intFloorplanId, $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						application_floorplans
					WHERE
						property_floorplan_id = ' . ( int ) $intFloorplanId . '
						AND application_id = ' . ( int ) $intApplicationId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchApplicationFloorplans( $strSql, $objDatabase );
	}

	public static function fetchApplicationFloorplansWithNameByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedFloorPlans = false ) {

		$strCheckDeletedUnitFloorPlanSql = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pf.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						af.*,
						pf.floorplan_name
					FROM
						application_floorplans af
						JOIN property_floorplans pf ON ( pf.cid = af.cid AND pf.id = af.property_floorplan_id ' . $strCheckDeletedUnitFloorPlanSql . ' )
					WHERE
						af.application_id = ' . ( int ) $intApplicationId . '
						AND af.cid = ' . ( int ) $intCid;

		return self::fetchApplicationFloorplans( $strSql, $objDatabase );
	}

	public static function fetchApplicationFloorplanByApplicationIdByPropertyFloorplanIdByCid( $intApplicationId, $intPropertyFloorplanId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM application_floorplans WHERE cid = ' . ( int ) $intCid . ' AND application_id = ' . ( int ) $intApplicationId . ' AND property_floorplan_id = ' . ( int ) $intPropertyFloorplanId;
		 return self::fetchApplicationFloorplan( $strSql, $objDatabase );
	}

	public static function fetchApplicationFloorplansByApplicationIdByPropertyFloorplanIdsByCid( $intApplicationId, $arrintPropertyFloorplanId, $intCid, $objDatabase ) {
		if( false == valId( $intApplicationId ) || false == valArr( $arrintPropertyFloorplanId ) ) return NULL;

		$strSql = 'SELECT * FROM application_floorplans WHERE cid = ' . ( int ) $intCid . ' AND application_id = ' . ( int ) $intApplicationId . ' AND property_floorplan_id IN ( ' . implode( ',', $arrintPropertyFloorplanId ) . ' )';
		return self::fetchApplicationFloorplans( $strSql, $objDatabase );
	}

	public static function fetchPropertyFloorplanIdsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						property_floorplan_id
					FROM
						application_floorplans
					WHERE
						cid = ' . ( int ) $intCid . '
						AND application_id = ' . ( int ) $intApplicationId;
		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchApplicationFloorPlanCountByApplicationIdByPropertyIdByCid( $intApplicationId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						count( ap.id ) as count
					FROM
						applications ap
						JOIN application_floorplans af ON ( af.cid = ap.cid AND af.application_id = ap.id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.property_id = ' . ( int ) $intPropertyId . '
						AND ap.id = ' . ( int ) $intApplicationId;

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchApplicationFloorplanIdsByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						application_id,
						property_floorplan_id
					FROM
						application_floorplans
					WHERE
						cid = ' . ( int ) $intCid . '
						AND application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' ) ';

		$arrmixData = fetchData( $strSql, $objDatabase );
		foreach( $arrmixData as $arrintData ) {
			$arrmixApplicationFloorplans[$arrintData['application_id']][$arrintData['property_floorplan_id']] = $arrintData['property_floorplan_id'];
		}
		return $arrmixApplicationFloorplans;
	}

}
?>