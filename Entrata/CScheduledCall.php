<?php

class CScheduledCall extends CBaseScheduledCall {

	protected $m_intPropertyId;
	protected $m_intInsertedCallCount;
	protected $m_intPropertyScheduledCallId;

	protected $m_boolIsWeekly;
	protected $m_objClient;
	protected $m_objScheduleCallType;

	protected $m_arrintProcessedPropertyIds;
	protected $m_arrobjScheduledCallCustomerTypes;
	protected $m_arrintSelectedPropertyIds;
	protected $m_arrintSelectedLeaseStatusTypeIds;
	protected $m_strUserName;

	const SCHEDULED_CALL_TERMINATOR = 7;

	/**
	 * Get Functions
	 */

	public function getBeginSendTimeHour() {
		$arrstrBeginSendTime = explode( ':', $this->m_strBeginSendTime );

		return $arrstrBeginSendTime[0];
	}

	public function getBeginSendTimeMinute() {
		$arrstrBeginSendTime = explode( ':', $this->m_strBeginSendTime );

		if( true == valArr( $arrstrBeginSendTime, 2 ) ) {
			return $arrstrBeginSendTime['1'];
		} else {
			return 0;
		}
	}

	public function getEndSendTimeHour() {
		$arrstrEndSendTime = explode( ':', $this->m_strEndSendTime );

		return $arrstrEndSendTime[0];
	}

	public function getEndSendTimeMinute() {
		$arrstrEndSendTime = explode( ':', $this->m_strEndSendTime );

		if( true == valArr( $arrstrEndSendTime, 2 ) ) {
			return $arrstrEndSendTime['1'];
		} else {
			return 0;
		}
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getLeaseStatusTypeId() {
		return $this->m_intLeaseStatusTypeId;
	}

	public function getInsertedCallCount() {
		return $this->m_intInsertedCallCount;
	}

	public function getIsWeekly() {
		return $this->m_boolIsWeekly;
	}

	public function getPropertyScheduledCallId() {
		return $this->m_intPropertyScheduledCallId;
	}

	public function getSelectedPropertyIds() {
		return $this->m_arrintSelectedPropertyIds;
	}

	public function getSelectedLeaseStatusTypeIds() {
		return $this->m_arrintSelectedLeaseStatusTypeIds;
	}

	public function getUserName() {
		return $this->m_strUserName;
	}

	public function getScheduledCallType() {
		return $this->m_objScheduleCallType;
	}

	/**
	 * Set Functions
	 */

	public function setScheduledCallChunkFields( $objCallTypeChunk ) {

		$objScheduledCallChunk = new CScheduledCallChunk();

		$objScheduledCallChunk->setCid( $this->getCid() );
		$objScheduledCallChunk->setScheduledCallId( $this->m_intId );

		$objScheduledCallChunk->setCallChunkTypeId( $objCallTypeChunk->getCallChunkTypeId() );
		$objScheduledCallChunk->setCompanyMediaFileId( $objCallTypeChunk->getCompanyMediaFileId() );
		$objScheduledCallChunk->setText( $objCallTypeChunk->getText() );
		$objScheduledCallChunk->setOrderNum( $objCallTypeChunk->getOrderNum() );

		return $objScheduledCallChunk;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setPropertyScheduledCallId( $intPropertyScheduledCallId ) {
		$this->m_intPropertyScheduledCallId = $intPropertyScheduledCallId;
	}

	public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$this->m_intLeaseStatusTypeId = $intLeaseStatusTypeId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['lease_status_type_id'] ) )	$this->setLeaseStatusTypeId( $arrmixValues['lease_status_type_id'] );
		if( true == isset( $arrmixValues['property_scheduled_call_id'] ) ) $this->setPropertyScheduledCallId( $arrmixValues['property_scheduled_call_id'] );
		if( true == isset( $arrmixValues['username'] ) ) $this->setUserName( $arrmixValues['username'] );

		if( isset( $arrmixValues['property_ids'] ) && true == valArr( $arrmixValues['property_ids'] ) ) {
			$this->setSelectedPropertyIds( $arrmixValues['property_ids'] );
		}

		if( isset( $arrmixValues['lease_status_type_ids'] ) && true == valArr( $arrmixValues['lease_status_type_ids'] ) ) {
			$this->setSelectedLeaseStatusTypeIds( $arrmixValues['lease_status_type_ids'] );
		}
	}

	public function setIsWeekly( $boolIsWeekly ) {
		$this->m_boolIsWeekly = $boolIsWeekly;
	}

	public function setSelectedPropertyIds( $arrintPropertyIds ) {
		$this->m_arrintSelectedPropertyIds = $arrintPropertyIds;
	}

	public function setSelectedLeaseStatusTypeIds( $arrintLeaseStatusTypeIds ) {
		$this->m_arrintSelectedLeaseStatusTypeIds = $arrintLeaseStatusTypeIds;
	}

	public function setUserName( $strUserName ) {
		$this->m_strUserName = $strUserName;
	}

	public function setScheduledCallType( $objScheduledCallType ) {
		$this->m_objScheduleCallType = $objScheduledCallType;
	}

	/**
	 * Create Functions
	 */

	public function createScheduledCallChunk() {

		$objScheduledCallChunk = new CScheduledCallChunk();
		$objScheduledCallChunk->setCid( $this->getCid() );
		$objScheduledCallChunk->setScheduledCallId( $this->m_intId );

		return $objScheduledCallChunk;
	}

	public function createCall( $objCustomer, $objProperty, $strPhoneNumber, $objDatabase ) {
		$arrstrBeginTime 	= explode( '-', $this->getBeginSendTime() );
		$arrstrEndTime 		= explode( '-', $this->getEndSendTime() );
		$strBeginTime 		= $this->getStartDate() . ' ' . $arrstrBeginTime[0];
		$strEndTime 		= $this->getEndDate() . ' ' . $arrstrEndTime[0];
		$strTzBeginTime		= date( 'H:i:s', $objProperty->getMstTimestamp( $strBeginTime, $objDatabase ) );
		$strTzEndTime 	= date( 'G:i:s', $objProperty->getMstTimestamp( $strEndTime, $objDatabase ) );

		$objCall = new CCall();

		$objCall->setCallTypeId( CCallType::OUTBOUND_PROPERTY_SCHEDULED_CALL );
		$objCall->setCid( $this->getCid() );
		$objCall->setScheduledCallId( $this->getId() );
		$objCall->setScheduledCallTypeId( $this->getScheduledCallTypeId() );
		$objCall->setCallPriorityId( $this->getCallPriorityId() );
		$objCall->setVoiceTypeId( $this->getVoiceTypeId() );
		$objCall->setAllowBlock( $this->getAllowBlock() );
		$objCall->setCallToday( $this->getCallToday() );
		$objCall->setCallStatusTypeId( CCallStatusType::WAITING_TO_SEND );
		$objCall->setCustomerId( $objCustomer->getId() );
		$objCall->setPropertyId( $objCustomer->getPropertyId() );
		$objCall->setDestinationPhoneNumber( $strPhoneNumber );
		$objCall->setBeginSendTime( $strTzBeginTime );
		$objCall->setEndSendTime( $strTzEndTime );
		$objCall->setScheduledSendDatetime( $this->getLastSentOn() . ' ' . $strTzBeginTime . '-' . $arrstrBeginTime[1] );
		$objCall->setOriginPhoneNumber( ( true == valStr( $objProperty->getOfficePhoneNumber() ) ? $objProperty->getOfficePhoneNumber() : '8012281282' ) );
		$objCall->setCallerName( $objCustomer->getNameFull() );
		$objCall->setCallResultId( CCallResult::RESIDENT );

		return $objCall;
	}

	public function createTestScheduledCall() {

		$objCall = new CCall();
		$objCall->setCallTypeId( CCallType::OUTBOUND_PROPERTY_SCHEDULED_CALL );
		$objCall->setCallStatusTypeId( CCallStatusType::WAITING_TO_SEND );
		$objCall->setCid( $this->getCid() );
		$objCall->setScheduledCallId( $this->getId() );
		$objCall->setScheduledCallTypeId( $this->getScheduledCallTypeId() );
		$objCall->setCallPriorityId( $this->getCallPriorityId() );
		$objCall->setVoiceTypeId( $this->getVoiceTypeId() );
		$objCall->setPropertyId( $this->getPropertyId() );
		$objCall->setAllowBlock( $this->getAllowBlock() );
		$objCall->setOriginPhoneNumber( '<unknown>' );
		$objCall->setCallResultId( CCallResult::RESIDENT );

		return $objCall;
	}

	public function createScheduledCallProperty() {

		$objScheduledCallProperty = new CPropertyScheduledCall();
		$objScheduledCallProperty->setCid( $this->getCid() );

		return $objScheduledCallProperty;
	}

	public function createScheduledCallCustomerType() {

		$objScheduledCallCustomerType = new CScheduledCallCustomerType();
		$objScheduledCallCustomerType->setCid( $this->getCid() );

		return $objScheduledCallCustomerType;
	}

	public function createAndInsertCalls( $objRwxDatabase, $objVoipDatabase ) {
		$boolIsExcludeFmoPending	= false;
		$boolRequireBirthDateToday	= false;

		// If the call frequency is once and is already scheduled, return false.
		if( CCallFrequency::ONCE == $this->getCallFrequencyId() && false == is_null( $this->getLastSentOn() ) ) {
			return false;
		}

		// If the call frequency is once, call today is set and current date is greater than start date, return false.
		if( CCallFrequency::ONCE == $this->getCallFrequencyId() && 1 == $this->getCallToday() && ( strtotime( date( 'm/d/Y' ) ) > strtotime( $this->m_strStartDate ) ) ) {
			return false;
		}

		// If today is not in the week days array, and the array is not null, return false.
		if( false == is_null( $this->getWeekDays() ) && false == in_array( date( 'N' ), explode( ',', $this->getWeekDays() ) ) ) {
			return false;
		}

		// If today is not in the month days array, and the array is not null, return false.
		if( false == is_null( $this->getMonthDays() ) && false == in_array( date( 'j' ), explode( ',', $this->getMonthDays() ) ) ) {
			return false;
		}

		$intTodayTimeStamp 	= strtotime( date( 'm/d/Y' ) );

		// Here we figure out the first scheduled call date that we need to post.
		if( false == isset( $this->m_strLastSentOn ) ) {
			$intNextPostDate = strtotime( $this->m_strStartDate );

		} else {

			$intLastSentOn		= strtotime( $this->m_strLastSentOn );
			$intNextPostDate	= strtotime( $this->m_strStartDate );

			switch( $this->getCallFrequencyId() ) {
				case CCallFrequency::ONCE:
					break;

				case CCallFrequency::DAILY:
					while( $intNextPostDate <= ( $intLastSentOn ) ) {
						$intNextPostDate = strtotime( '+1 day', $intNextPostDate );
					}
					break;

				case CCallFrequency::WEEKLY:
					while( $intNextPostDate <= ( $intLastSentOn ) ) {
						$intNextPostDate = strtotime( '+7 days', $intNextPostDate );
					}
					break;

				case CCallFrequency::MONTHLY:
					while( $intNextPostDate <= ( $intLastSentOn ) ) {
						$intNextPostDate = strtotime( '+1 month', $intNextPostDate );
					}
					break;

				case CCallFrequency::QUARTERLY:
					while( $intNextPostDate <= ( $intLastSentOn ) ) {
						$intNextPostDate = strtotime( '+3 months', $intNextPostDate );
					}
					break;

				case CCallFrequency::ANNUALLY:
					while( $intNextPostDate <= ( $intLastSentOn ) ) {
						$intNextPostDate = strtotime( '+1 year', $intNextPostDate );
					}
					break;

				default:
					// Default statement
					break;
			}
		}

		// If the next date falls after the end date, return false.
		if( true == isset( $this->m_strEndDate ) && $intNextPostDate > strtotime( $this->m_strEndDate ) ) {
			return false;
		}

		// If the next date falls after the current date, return false.
		if( $intNextPostDate > $intTodayTimeStamp ) {
			return false;
		}

		// If the current date is less than the begin date, return false.
		if( $intTodayTimeStamp < strtotime( $this->m_strStartDate ) ) {
			return false;
		}

		$intLastIncrementingTimeStamp = NULL;

		while( $intNextPostDate <= $intTodayTimeStamp && ( false == isset( $this->m_strEndDate ) || ( true == isset( $this->m_strEndDate ) && $intNextPostDate <= strtotime( $this->m_strEndDate ) ) ) ) {

			$intLastIncrementingTimeStamp = $intNextPostDate;

			switch( $this->getCallFrequencyId() ) {
				case CCallFrequency::ONCE:
					$intLastIncrementingTimeStamp = $intTodayTimeStamp;
					break 2;

				case CCallFrequency::DAILY:
					$intNextPostDate = strtotime( '+1 day', $intNextPostDate );
					break;

				case CCallFrequency::WEEKLY:
					$intNextPostDate = strtotime( '+7 days', $intNextPostDate );
					break;

				case CCallFrequency::MONTHLY:
					$intNextPostDate = strtotime( '+1 month', $intNextPostDate );
					break;

				case CCallFrequency::QUARTERLY:
					$intNextPostDate = strtotime( '+3 months', $intNextPostDate );
					break;

				case CCallFrequency::ANNUALLY:
					$intNextPostDate = strtotime( '+1 year', $intNextPostDate );
					break;

				default:
					trigger_error( 'Call frequency unrecognized.', E_USER_ERROR );
					exit;
			}
		}

		// If there is no last incrementing time stamp value, we have to return false.
		if( true == is_null( $intLastIncrementingTimeStamp ) ) {
			return false;
		}

		$this->setLastSentOn( date( 'm/d/Y', $intLastIncrementingTimeStamp ) );

		$this->m_arrobjScheduledCallCustomerTypes = $this->fetchScheduledCallCustomerTypes( $objRwxDatabase );
		if( false == valArr( $this->m_arrobjScheduledCallCustomerTypes ) ) return false;

		$arrintEnabledProperties = \Psi\Eos\Entrata\CProperties::createService()->fetchSimplePropertiesByPsProductIdsByCid( [ CPsProduct::MESSAGE_CENTER ], $this->getCid(), $objRwxDatabase );

		$arrobjProperties = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesWithOfficePhoneNumberByScheduledCallIdByPropertyIdsByCid( $this->getId(), $arrintEnabledProperties, $this->getCid(), $objRwxDatabase );

		if( false == valArr( $arrobjProperties ) ) return false;

		if( CScheduledCallType::HAPPY_BIRTHDAY_MESSAGE == $this->getScheduledCallTypeId() ) {
			$boolRequireBirthDateToday = true;
		}

		$arrobjScheduledCallPreferences = $this->fetchScheduledCallPreferences( $objRwxDatabase );

		if( true == valArr( $arrobjScheduledCallPreferences ) ) {
			$arrobjScheduledCallPreferences = rekeyObjects( 'Key', $arrobjScheduledCallPreferences );
		}

		if( true == isset( $arrobjScheduledCallPreferences['EXCLUDE_FMO_PENDING_LEASES'] ) && true == valObj( $arrobjScheduledCallPreferences['EXCLUDE_FMO_PENDING_LEASES'], 'CScheduledCallPreference' ) ) {
			$boolIsExcludeFmoPending = $arrobjScheduledCallPreferences['EXCLUDE_FMO_PENDING_LEASES']->getValue();
		}

		$arrobjCustomers = \Psi\Eos\Entrata\CCustomers::createService()->fetchVoipPreparedCustomersByLeaseStatusTypeIdsByPropertyIdsByCid( array_keys( $this->m_arrobjScheduledCallCustomerTypes ), array_keys( $arrobjProperties ), $this->getCid(), $objRwxDatabase, $boolRequireBirthDateToday, $boolIsExcludeFmoPending );

		if( false == valArr( $arrobjCustomers ) ) return true;

		$arrobjCustomerPhoneNumbers = rekeyObjects( 'CustomerId', \Psi\Eos\Entrata\CCustomerPhoneNumbers::createService()->fetchCustomerPhoneNumbersByCustomerIdsByCid( array_keys( rekeyObjects( 'Id', $arrobjCustomers ) ), $this->getCid(), $objRwxDatabase, true, false ) );
		$objClient = $this->fetchClient( $objRwxDatabase );

		$this->m_arrintProcessedPropertyIds = [];

		foreach( $arrobjCustomers as $objCustomer ) {

			$strPhoneNumber = NULL;
			if( true == valObj( $arrobjCustomerPhoneNumbers[$objCustomer->getId()], 'CCustomerPhoneNumber' ) ) {
				$strPhoneNumber = $arrobjCustomerPhoneNumbers[$objCustomer->getId()]->getPhoneNumber();
			}

			if( false == ( $strPhoneNumber = $objCustomer->formatPhoneNumberForVoip( $strPhoneNumber ) ) ) {
				continue;
			}

			if( true == CCallBlocks::determineIsBlocked( $strPhoneNumber, $this->getScheduledCallTypeId(), $objVoipDatabase ) ) {
				continue;
			}

			$objCall = $this->createCall( $objCustomer, $arrobjProperties[$objCustomer->getPropertyId()], $strPhoneNumber, $objRwxDatabase );

			if( false == $this->insertCall( $objRwxDatabase, $objVoipDatabase, $objCall, $objCustomer, $arrobjProperties[$objCustomer->getPropertyId()], $objClient ) ) {
				return false;
			}
		}

		return true;
	}

	public function createScheduledCallPreference() {

		$objCScheduledCallPreference = new CScheduledCallPreference();
		$objCScheduledCallPreference->setCid( $this->getCid() );
		$objCScheduledCallPreference->setScheduledCallId( $this->m_intId );

		return $objCScheduledCallPreference;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchScheduledlCallCustomerTypesById( $objDatabase ) {

		return CScheduledCallCustomerTypes::fetchScheduledCallCustomerTypesByScheduledCallIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchScheduledCallPropertiesById( $objDatabase ) {

		return CPropertyScheduledCalls::fetchPropertyScheduledCallsByScheduledCallIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchScheduledCallChunksByScheduledCallIdOrderByOrderNum( $objDatabase ) {

		return CScheduledCallChunks::fetchScheduledCallChunksByScheduledCallIdOrderByOrderNumByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchScheduledCallChunkById( $intScheduledCallChunkId, $objDatabase ) {

		return CScheduledCallChunks::fetchScheduledCallChunksByScheduledCallIdByIdByCid( $this->m_intId, $intScheduledCallChunkId, $this->getCid(), $objDatabase );
	}

	public function fetchScheduledCallChunks( $objDatabase ) {

		return CScheduledCallChunks::fetchScheduledCallChunksByScheduledCallIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchScheduledCallType( $objDatabase ) {

		return CScheduledCallTypes::fetchScheduledCallTypeById( $this->m_intScheduledCallTypeId, $objDatabase );
	}

	public function fetchScheduledCallCustomerTypes( $objDatabase ) {

		return CScheduledCallCustomerTypes::fetchScheduledCallCustomerTypesByScheduledCallIdKeyedByCustomerTypeIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchProperties( $objClientDatabase ) {

		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesWithOfficePhoneNumberByScheduledCallIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchClient( $objDatabase ) {

		return CClients::fetchClientById( $this->getCid(), $objDatabase );
	}

	public function fetchCallTypeCustomerTypesByScheduledCallTypeId( $objDatabase ) {

		$arrobjCallTypeCustomerTypes = CCallTypeCustomerTypes::fetchCallTypeCustomerTypesByScheduledCallTypeId( $this->getScheduledCallTypeId(), $objDatabase );
		return rekeyObjects( 'LeaseStatusTypeId', $arrobjCallTypeCustomerTypes );
	}

	public function fetchScheduledCallPreferences( $objDatabase ) {

		return CScheduledCallPreferences::fetchScheduledCallPreferencesByScheduledCallIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Delete Functions
	 */

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( 'NOW()' );

		if( $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return true;
		}
	}

	/**
	 * Validate Functions
	 */

	public function validate( $strAction, $objDatabase = NULL, $arrintScheduledCallPropertyPreferences = NULL, $arrintScheduledCallResidentStatusPreferences = NULL ) {
		$boolIsValid = true;

		$objScheduledCallValidator = new CScheduledCallValidator();

		$objScheduledCallValidator->setScheduledCall( $this );

		$boolIsValid &= $objScheduledCallValidator->validate( $strAction, $objDatabase, $arrintScheduledCallPropertyPreferences, $arrintScheduledCallResidentStatusPreferences );

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function insertTestScheduledCall( $objRwxDatabase, $objVoipDatabase, $objCall, $arrmixValues ) {

		$arrobjScheduledCallChunks = $this->fetchScheduledCallChunks( $objRwxDatabase );

		if( true == isset( $arrobjScheduledCallChunks ) && true == valArr( $arrobjScheduledCallChunks ) ) {

			$objVoipDatabase->begin();

			if( false == $objCall->insert( SYSTEM_USER_ID, $objVoipDatabase ) ) {
				$objVoipDatabase->rollback();
				return false;
			}

			foreach( $arrobjScheduledCallChunks as $objScheduledCallChunk ) {
				$objCallChunk = $objScheduledCallChunk->createCallChunk( $arrmixValues, $objCall, $objRwxDatabase );

				if( false == valObj( $objCallChunk, 'CCallChunk' ) ) continue;

				$objCallChunk->setCallId( $objCall->getId() );

				if( false == $objCallChunk->insert( SYSTEM_USER_ID, $objVoipDatabase ) ) {
					$objVoipDatabase->rollback();
					return false;
				}
			}
			$objVoipDatabase->commit();
		} else {
			return false;
		}

		return true;
	}

	public function sendTestScheduledCall( $objRwxDatabase, $objVoipDatabase, $objCall, $arrmixValues ) {
		$arrobjScheduledCallChunks = $this->fetchScheduledCallChunks( $objRwxDatabase );

		if( false == valArr( $arrobjScheduledCallChunks ) ) {
			return false;
		}

		if( false == $objCall->insert( CUser::ID_SYSTEM, $objVoipDatabase ) ) {
			return false;
		}

		foreach( $arrobjScheduledCallChunks as $objScheduledCallChunk ) {
			$objCallChunk = $objScheduledCallChunk->createCallChunk( $arrmixValues, $objCall, $objRwxDatabase );

			if( false == valObj( $objCallChunk, 'CCallChunk' ) ) continue;

			$objCallChunk->setCallId( $objCall->getId() );

			if( false == $objCallChunk->insert( SYSTEM_USER_ID, $objVoipDatabase ) ) {
				return false;
			}
		}

		$objVoipDatabase->close();

		return ( new CCallsMessageSenderLibrary() )->addScheduledCall( $objCall );
	}

	public function insertCall( $objRwxDatabase, $objVoipDatabase, $objCall, $objCustomer, $objProperty, $objClient ) {
		if( false == valObj( $objCall, 'CCall' ) ) return false;

		$arrobjScheduledCallChunks 		= $this->fetchScheduledCallChunks( $objRwxDatabase );
		$arrobjScheduledCallPreferences = $this->fetchScheduledCallPreferences( $objRwxDatabase );

		if( true == valArr( $arrobjScheduledCallPreferences ) ) {
			$arrobjScheduledCallPreferences = rekeyObjects( 'Key', $arrobjScheduledCallPreferences );
		}

		if( true == isset( $arrobjScheduledCallChunks ) && true == valArr( $arrobjScheduledCallChunks ) ) {

			$arrmixValues = [];
			$arrmixValues['company_name']			= $objClient->getCompanyName();
			$arrmixValues['customer_first_name']	= $objCustomer->getNameFirst();
			$arrmixValues['customer_last_name']		= $objCustomer->getNameLast();
			$arrmixValues['property_name']			= $objProperty->getPropertyName();
			$arrmixValues['website_url']			= '';

			// Check to see if <<WEBSITE_URL>> exists in any of the call chunks
			foreach( $arrobjScheduledCallChunks as $objScheduledCallChunk ) {
				if( true == \Psi\CStringService::singleton()->stristr( $objScheduledCallChunk->getText(), '<<WEBSITE_URL>>' ) ) {
					$objWebsite = $objProperty->getOrFetchSingularWebsite( $objRwxDatabase );

					if( true == valObj( $objWebsite, 'CWebsite' ) ) {
						$objWebsiteDomain = $objWebsite->getOrFetchPrimaryWebsiteDomain( $objRwxDatabase );

						if( true == valObj( $objWebsiteDomain, 'CWebsiteDomain' ) ) {
							$arrmixValues['website_url'] = 'www.' . $objWebsiteDomain->getWebsiteDomain();
						} else {
							$arrmixValues['website_url'] = $objWebsite->getSubDomain() . CONFIG_PROSPECTPORTAL_DOMAIN_POSTFIX;
						}
					}
					break;
				}
			}

			$arrmixValues['money_owed'] = '';

			// Check to see if <<MONEY_OWED>> exists in any of the call chunks
//	 		if( true == valObj( $objProperty, 'CProperty' ) && false == in_array( $objProperty->getId(), $this->m_arrintProcessedPropertyIds ) && false == is_null( $objProperty->getRemotePrimaryKey() ) ) {

//	 		}

			foreach( $arrobjScheduledCallChunks as $objScheduledCallChunk ) {
				if( true == \Psi\CStringService::singleton()->stristr( $objScheduledCallChunk->getText(), '<<MONEY_OWED>>' ) || CScheduledCallType::LATE_RENT_NOTICE == $this->getScheduledCallTypeId() ) {

					if( true == valObj( $objProperty, 'CProperty' ) && false == in_array( $objProperty->getId(), $this->m_arrintProcessedPropertyIds ) && false == is_null( $objProperty->getRemotePrimaryKey() ) ) {
						$objIntegrationDatabase = $objProperty->fetchIntegrationDatabase( $objRwxDatabase );

						if( true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) && CIntegrationClientType::REAL_PAGE == $objIntegrationDatabase->getIntegrationClientTypeId() ) {
							return true;
						}

						$this->m_arrintProcessedPropertyIds[] = $objProperty->getId();

						if( true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) && CIntegrationClientType::TENANT_PRO != $objIntegrationDatabase->getIntegrationClientTypeId() ) {
							$objUtilitiesDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::UTILITIES, CDatabaseUserType::PS_PROPERTYMANAGER );

							$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $this->getCid(), CIntegrationService::RETRIEVE_CUSTOMERS_WITH_BALANCE_DUE, SYSTEM_USER_ID, $objRwxDatabase );
							$objGenericWorker->setProperty( $objProperty );
							$objGenericWorker->setLeaseStatusTypeIds( array_keys( $this->m_arrobjScheduledCallCustomerTypes ) );

							$objGenericWorker->setDontUpdateLastSyncOn( true );
							$objGenericWorker->setPullLedger( false );
							$objGenericWorker->setDatabase( $objRwxDatabase );
							$objGenericWorker->setUtilityDatabase( $objUtilitiesDatabase );

							if( true == valObj( $objGenericWorker, 'CGenericClientWorker' ) ) {
								return false;
							}

							if( false == $objGenericWorker->process() ) {
								return false;
							}

						}
					}

					$intThresholdAmountDue	= ( true == array_key_exists( 'LATE_RENT_AMOUNT_DUE_THRESHOLD', $arrobjScheduledCallPreferences ) ) ?  $arrobjScheduledCallPreferences['LATE_RENT_AMOUNT_DUE_THRESHOLD']->getValue() : 0;
					$fltAmountDue			= $objCustomer->fetchLateRentAmountDue( array_keys( $this->m_arrobjScheduledCallCustomerTypes ), $intThresholdAmountDue, $objRwxDatabase, 30 );

					// If amount is over due or zero, don't insert a call
					if( CScheduledCallType::LATE_RENT_NOTICE == $this->getScheduledCallTypeId() && 0 >= $fltAmountDue ) {
						return true;
					}

					$arrmixValues['money_owed'] = '$' . number_format( $fltAmountDue, 2 );
					break;
				}
			}

			if( false == $objCall->insert( SYSTEM_USER_ID, $objVoipDatabase ) ) {
				return false;
			}

			foreach( $arrobjScheduledCallChunks as $objScheduledCallChunk ) {

				$objCallChunk = $objScheduledCallChunk->createCallChunk( $arrmixValues, $objCall, $objRwxDatabase );

				if( false == valObj( $objCallChunk, 'CCallChunk' ) ) {
					$this->addErrorMsg( new CErrorMsg( SCRIPT_ERROR, 'call_chunk', 'Invalid CallChunk Object for cid:' . $this->m_intCid . ' call_id: ' . $objCall->getId() ) );
					return false;
				}

				$objCallChunk->setCallId( $objCall->getId() );

				if( false == $objCallChunk->insert( SYSTEM_USER_ID, $objVoipDatabase ) ) {
					return false;
				}
			}

			// Need to create event
			$boolIsEventInserted = $this->insertScheduledCallEvent( $objCall, $objCustomer, $objRwxDatabase );

			$this->m_intInsertedCallCount++;
		} else {
			return false;
		}

		return true;
	}

	public function insertScheduledCallEvent( $objCall, $objCustomer, $objRwxDatabase ) {

		if( false == valObj( $objCall, 'CCall' ) ) return false;

		$objEventLibrary = new CEventLibrary();

		$objEventLibraryDataObject 	= $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $objRwxDatabase );
		$objEventLibraryDataObject->setCustomer( $objCustomer );
		$objEvent = $objEventLibrary->createEvent( [ $objCustomer ], CEventType::CALL_OUTGOING );
		$objEvent->setCid( $this->getCid() );
		$objEvent->setEventDatetime( 'NOW()' );
		$objEvent->setPsProductId( CPsProduct::MESSAGE_CENTER );
		$objEvent->setReference( $objCall );
		$objEvent->setDataReferenceId( $objCall->getId() );

		$objEventLibrary->buildEventDescription( $objCustomer );
		$objEventLibrary->buildEventSystemNotes( $objCustomer );

		return $objEventLibrary->insertEvent( SYSTEM_USER_ID, $objRwxDatabase );
	}

}
?>