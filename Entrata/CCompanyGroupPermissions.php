<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyGroupPermissions
 * Do not add any new functions to this class.
 */

class CCompanyGroupPermissions extends CBaseCompanyGroupPermissions {

	public static function fetchSimpleCompanyGroupPermissionsByCompanyGroupIdByCid( $intGroupId, $intCid, $objDatabase ) {
		// Objects are going to be sorted by module name in the array.

		$strSql = sprintf( 'SELECT * FROM %s WHERE cid = ' . ( int ) $intCid . ' AND company_group_id = %d', 'company_group_permissions', ( int ) $intGroupId );

		$arrobjCompanyGroupPermissions = NULL;
		$objGroupPermission = NULL;

		$objDataset = $objDatabase->createDataset();

		if( !$objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return NULL;
		}

		if( $objDataset->getRecordCount() > 0 ) {
			$arrobjCompanyGroupPermissions = [];

			while( !$objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();

				$objGroupPermission = new CCompanyGroupPermission();
				$objGroupPermission->setValues( $arrmixValues );
				$arrobjCompanyGroupPermissions[$objGroupPermission->getModuleId()] = $objGroupPermission;

				$objDataset->next();
			}
		}

		$objDataset->cleanup();
		return $arrobjCompanyGroupPermissions;
	}

	public static function fetchCompanyGroupPermissionsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cgp.module_id,
						CASE
							WHEN max( cgp.is_allowed ) = 1 THEN min( cgp.is_read_only )
							ELSE max( cgp.is_read_only )
						END AS is_read_only,
						max( cgp.is_allowed ) AS is_allowed
					FROM
						company_group_permissions cgp
					JOIN company_user_groups cug ON ( cug.company_group_id = cgp.company_group_id AND cug.cid = cgp.cid AND cug.company_user_id =  ' . ( int ) $intCompanyUserId . ' AND cug.cid = ' . ( int ) $intCid . ')
					WHERE
						cgp.is_allowed = 1
					GROUP BY
						cgp.module_id';

		return self::fetchCompanyGroupPermissions( $strSql, $objDatabase );
	}

	public static function fetchCustomPermissionedModulesByCompanyGroupIdByModuleIdsByCid( $intCompanyGroupId, $arrintModuleIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cgp.module_id
					FROM
						company_group_permissions cgp
					WHERE
						cgp.cid = ' . ( int ) $intCid . '
						AND cgp.company_group_id = ' . ( int ) $intCompanyGroupId . '
						AND cgp.module_id IN ( ' . implode( ',', $arrintModuleIds ) . ' )
						AND cgp.is_allowed = 1
					GROUP BY
						cgp.module_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyGroupPermissionsByModuleIdByCompanyUserIdByCid( $intModuleId, $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cgp.is_allowed
					FROM
						company_group_permissions cgp
						JOIN company_user_groups cug ON ( cug.cid = cgp.cid AND cug.company_group_id = cgp.company_group_id )
					WHERE
						cgp.cid = ' . ( int ) $intCid . '
						AND cug.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND cgp.module_id = ' . ( int ) $intModuleId . '
						AND cgp.is_allowed = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPermissionedModulesByCompanyUserIdByModuleIdsByCid( $intCompanyUserId, $arrintModuleIds, $intCid, $objDatabase ) {

		if( false === valId( $intCompanyUserId ) || false === valArr( $arrintModuleIds ) || false === valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						cgp.module_id
					FROM
						company_group_permissions cgp
						JOIN company_user_groups cug ON ( cug.cid = cgp.cid AND cug.company_group_id = cgp.company_group_id )
					WHERE
						cgp.cid = ' . ( int ) $intCid . '
						AND cug.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND cgp.module_id in ( ' . sqlIntImplode( $arrintModuleIds ) . ' )
						AND cgp.is_allowed = 1';

		return fetchData( $strSql, $objDatabase );
	}

}
?>