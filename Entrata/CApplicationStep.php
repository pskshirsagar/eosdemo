<?php

class CApplicationStep extends CBaseApplicationStep {

	const ACCOUNT_CREATION 		= 1;
	const PRE_SCREENING 		= 2;
	const BASIC_INFO 			= 3;
	const UNIT_INFO				= 4;
	const ADDITIONAL_INFO 		= 5;
	const FINANCIAL 			= 6;
	const PEOPLE 				= 7;
	const OPTIONS_AND_FEES 		= 8;
	const ROOMMATE_INTEREST 	= 9;
	const CONTACTS 				= 10;
	const CUSTOM_FORM 			= 11;
	const PAYMENT 				= 12;
	const SUMMARY 				= 13;
	const CONFIRMATION 			= 14;
	const PROCESS_LEASE 		= 15;
	const ROOMMATES				= 16;
	const BUSINESS_INFO			= 17;
	const HOUSEHOLD				= 18;
	const MILITARY				= 19;

	public static $c_arrintAbandonedApplicationSteps = [
		self::PRE_SCREENING,
		self::UNIT_INFO,
		self::OPTIONS_AND_FEES,
		self::BASIC_INFO,
		self::ADDITIONAL_INFO,
		self::FINANCIAL,
		self::PEOPLE,
		self::CONTACTS,
		self::CUSTOM_FORM,
		self::SUMMARY,
		self::PAYMENT
	];

	public static $c_arrstrApplicationStepIdsKeyedByValidationStrings = [
		'is_valid_basic_info' 			=> self::BASIC_INFO,
		'is_valid_unit_info' 			=> self::UNIT_INFO,
		'is_valid_options_info' 		=> self::OPTIONS_AND_FEES,
		'is_valid_additional_info' 		=> self::ADDITIONAL_INFO,
		'is_valid_contacts_info' 		=> self::CONTACTS,
		'is_valid_custom_form' 			=> self::CUSTOM_FORM,
		'is_valid_financial_info' 		=> self::FINANCIAL,
		'is_valid_household'			=> self::HOUSEHOLD,
	    'is_valid_military'			    => self::MILITARY,
		'is_valid_roommates' 	        => self::ROOMMATES
	];

	public static $c_arrintApplicationStepIdsByCustomerDataTypeId = [
		CCustomerDataType::INCOME 				=> self::FINANCIAL,
		CCustomerDataType::ASSET 				=> self::FINANCIAL,
		CCustomerDataType::EXPENSE 				=> self::FINANCIAL,
		CCustomerDataType::SSN 					=> self::BASIC_INFO,
		CCustomerDataType::CITIZENSHIP 			=> self::BASIC_INFO,
		CCustomerDataType::BIRTHDATE 			=> self::BASIC_INFO,
		CCustomerDataType::DISABILITY 			=> self::BASIC_INFO,
		CCustomerDataType::STUDENT 				=> self::BASIC_INFO,
		CCustomerDataType::VETERAN 				=> self::BASIC_INFO,
		CCustomerDataType::DISASTER 			=> self::BASIC_INFO,
		CCustomerDataType::POLICE_OFFICER 		=> self::BASIC_INFO,
		CCustomerDataType::JOINT_DEPENDENT 		=> self::BASIC_INFO,
		CCustomerDataType::ASSISTANCE_ANIMAL	=> self::OPTIONS_AND_FEES,
		CCustomerDataType::DISPLACEMENT 		=> self::HOUSEHOLD
	];

	public static $c_arrintRwxEnabledApplicationStepIds = [
		self::BASIC_INFO,
		self::UNIT_INFO,
		self::ADDITIONAL_INFO,
        self::FINANCIAL,
		self::OPTIONS_AND_FEES,
		self::CONTACTS,
		self::CUSTOM_FORM,
		self::ROOMMATES,
		self::BUSINESS_INFO,
		self::HOUSEHOLD,
	    self::MILITARY,
  	];

	public static $c_arrstrApplicationStepNameArray = [
		self::ACCOUNT_CREATION  => 'Account Creation',
		self::PRE_SCREENING     => 'Pre Qualification',
		self::BASIC_INFO        => 'Basic Info',
		self::UNIT_INFO         => 'Unit Info',
		self::ADDITIONAL_INFO   => 'Additional Info',
		self::FINANCIAL         => 'Financial',
		self::PEOPLE            => 'People',
		self::OPTIONS_AND_FEES  => 'Options & Fees',
		self::CONTACTS          => 'Contacts',
		self::CUSTOM_FORM       => 'Custom Forms',
		self::PAYMENT           => 'Payment',
		self::SUMMARY           => 'Summary',
		self::CONFIRMATION      => 'Confirmation',
		self::PROCESS_LEASE     => 'Lease',
		self::ROOMMATES         => 'Roommates',
		self::BUSINESS_INFO     => 'Business Info',
		self::HOUSEHOLD         => 'Household',
	    self::MILITARY          => 'Military',
	];

	public static $c_arrstrAffordableStepNameArray = [
	    self::BASIC_INFO           => 'Member',
	    self::OPTIONS_AND_FEES     => 'Pets/Assistance Animals',
	    self::ADDITIONAL_INFO      => 'Additional Info',
	    self::HOUSEHOLD            => 'Household',
	    self::FINANCIAL            => 'Financial',
	    self::UNIT_INFO            => 'Unit Preference',
	    self::CONTACTS             => 'Contacts',
	    self::CUSTOM_FORM          => 'Application',
	];



	protected $m_intId;
	protected $m_strExitUri;
	protected $m_intOrderNum;
	protected $m_strApplicationStepName;

	public function setApplicationStepName( $strApplicationStepName ) {
		$this->m_strApplicationStepName = $strApplicationStepName;
	}

	public function setExitUri( $strExitUri ) {
		$this->m_strExitUri = $strExitUri;
	}

	public function getExitUri() {
		return $this->m_strExitUri;
	}

	public function getApplicationStepName() {
		return $this->m_strApplicationStepName;
	}

	public static function getCustomApplicationStep( $intApplicationStepId, $intOccupancyTypeId ) {
		switch( $intOccupancyTypeId ) {

			case COccupancyType::AFFORDABLE:
				if( COccupancyType::AFFORDABLE == $intOccupancyTypeId ) {
					return self::$c_arrstrAffordableStepNameArray[$intApplicationStepId];
				}
				break;

			default:
				// default case
				break;
		}
	}

}
?>