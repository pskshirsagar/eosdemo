<?php

class CConstraintVariable extends CBaseConstraintVariable {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valVariableName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valVariableDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valVariableType() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valVariableDefinition() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>