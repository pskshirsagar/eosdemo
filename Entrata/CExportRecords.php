<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CExportRecords
 * Do not add any new functions to this class.
 */

class CExportRecords extends CBaseExportRecords {

	public static function fetchExportRecordsByTableKeyByExportTypeKeyByReferenceNumbersByCid( $strTableKey, $strExportTypeKey, $arrintReferenceNumbers, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						export_records
					WHERE
						cid = ' . ( int ) $intCid . '
						AND table_key = \'' . $strTableKey . '\'
						AND export_type_key = \'' . $strExportTypeKey . '\'
						AND reference_number IN ( ' . implode( ',', $arrintReferenceNumbers ) . ')';

		return self::fetchExportRecords( $strSql, $objDatabase );
	}
}
?>