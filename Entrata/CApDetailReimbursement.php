<?php

class CApDetailReimbursement extends CBaseApDetailReimbursement {

	protected $m_strReimbursementHeaderNumber;
	protected $m_strOriginalHeaderNumber;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginalApDetailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReimbursementApDetailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReimbursementGlDetailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getReimbursementHeaderNumber() {
		return $this->m_strReimbursementHeaderNumber;
	}

	public function setReimbursementHeaderNumber( $strReimbursementHeaderNumber ) {
		$this->m_strReimbursementHeaderNumber = $strReimbursementHeaderNumber;
	}

	public function getOriginalHeaderNumber() {
		return $this->m_strOriginalHeaderNumber;
	}

	public function setOriginalHeaderNumber( $strOriginalHeaderNumber ) {
		$this->m_strOriginalHeaderNumber = $strOriginalHeaderNumber;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['original_header_number'] ) )			$this->setOriginalHeaderNumber( $arrmixValues['original_header_number'] );
		if( true == isset( $arrmixValues['reimbursement_header_number'] ) )		$this->setReimbursementHeaderNumber( $arrmixValues['reimbursement_header_number'] );
		return;
	}

}
?>