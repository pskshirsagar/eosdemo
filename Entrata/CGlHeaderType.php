<?php

class CGlHeaderType extends CBaseGlHeaderType {

	const STANDARD				= 1;
	const TRIAL_BALANCE			= 2;
	const ACCRUAL_MONTH_END		= 3;
	const CASH_MONTH_END		= 4;
	const YEAR_END				= 5;

    public static function loadGlHeaderTypesNameArray() {

    	$arrstrGlHeaderTypes = [];

    	$arrstrGlHeaderTypes[self::STANDARD]			= 'Standard';
    	$arrstrGlHeaderTypes[self::TRIAL_BALANCE]		= 'Trial Balance';
    	$arrstrGlHeaderTypes[self::ACCRUAL_MONTH_END]	= 'Accrual Month End';
    	$arrstrGlHeaderTypes[self::CASH_MONTH_END]		= 'Cash Month End';
    	$arrstrGlHeaderTypes[self::YEAR_END]			= 'Year End';

    	return $arrstrGlHeaderTypes;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>