<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentInclusionTypes
 * Do not add any new functions to this class.
 */

class CDocumentInclusionTypes extends CBaseDocumentInclusionTypes {

	public static function fetchDocumentInclusionTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDocumentInclusionType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDocumentInclusionType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDocumentInclusionType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>