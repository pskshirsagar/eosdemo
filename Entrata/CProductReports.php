<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CProductReports
 * Do not add any new functions to this class.
 */

class CProductReports extends CBaseProductReports {

	public static function fetchProductReports( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return self::fetchCachedObjects( $strSql, 'CProductReport', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false, $boolIsReturnKeyedArray );
	}

	public static function fetchProductReport( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CProductReport', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>