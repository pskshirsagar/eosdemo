<?php

class CWebsiteActivityType extends CBaseWebsiteActivityType {

	const TEMPLATE 						= 1;
	const SUB_DOMAIN 					= 2;
	const LANDING_PAGE_TITLE 			= 3;
	const SEO_META_TITLE 				= 4;
	const SEO_KEYWORDS 					= 5;
	const LANDING_PAGE_HEADING		 	= 6;
	const SEO_DESCRIPTION 				= 7;
	const LANDING_PAGE_TEXT 			= 8;
	const LOGO_TEXT 					= 9;
	const IS_DISABLED 					= 10;
	const IS_DELETED 					= 11;
	const DOMAIN_NAME					= 12;
	const VANITY_DOMAIN					= 13;
	const IS_PRIMARY_DOMAIN				= 14;
	const IS_SECURE_DOMAIN				= 15;
	const DOMAIN_TRANSFER				= 16;
	const DNS							= 17;
	const WEBSITE_CANONICAL_URL			= 18;
	const IS_DELETED_DOMAIN				= 19;
	const PRIVACY_COOKIE_NAME			= 20;
	const PRIVACY_CATEGORY				= 21;
	const PRIVACY_DESCRIPTION			= 22;
	const PRIVACY_TOKEN					= 23;
	const PRIVACY_PRODUCT				= 24;
	const PRIVACY_ITEM_TYPE				= 25;
	const IS_DELETED_PRIVACY_ITEM		= 26;
	const IS_BULK_DELETED_PRIVACY_ITEM	= 27;
	const LANGUAGE_SETTINGS             = 28;
    const HIDE_WEBSITE                  = 29;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>