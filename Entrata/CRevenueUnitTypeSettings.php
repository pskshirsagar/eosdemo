<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueUnitTypeSettings
 * Do not add any new functions to this class.
 */

class CRevenueUnitTypeSettings extends CBaseRevenueUnitTypeSettings {

	public static function fetchRevenueUnitTypeSettingByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						revenue_unit_type_settings
					WHERE
						unit_type_id = ' . ( int ) $intUnitTypeId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchRevenueUnitTypeSetting( $strSql, $objDatabase );
	}

	public static function fetchRevenueUnitTypeSettingByUnitTypeIdsByCid( $arrintUnitTypeIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						revenue_unit_type_settings
					WHERE
						unit_type_id IN( ' . implode( ',', $arrintUnitTypeIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return self::fetchRevenueUnitTypeSettings( $strSql, $objDatabase, true );
	}

	public static function fetchRevenueBulkUnitTypeSettingValueByUnitTypeIdsByPropertyIdByCid( $strSettingKey, $strUnitTypeIds, $intPropertyId, $intCid, $objDatabase ) {
		$strKeyLabel = 'Max Optimal Rent';
		if( 'PRICING_MIN_OPTIMAL_PRICE' == $strSettingKey ) {
			$strKeyLabel = 'Min Optimal Rent';
		}

		$strSql = ' SELECT
		                 Distinct rutp.id, 
		                rutp.cid,
                        rutp.property_id,
                        rutp.unit_type_id,
                        rutp.updated_on, ';
		if( 'PRICING_MIN_OPTIMAL_PRICE' == $strSettingKey ) {
			$strSql .= 'rutp.min_optimal_price as value,';
		} else {
			$strSql .= 'rutp.max_optimal_price as value,';
		}
		$strSql .= 'ut.name, \'' . $strKeyLabel . '\' as label, cu.username
					FROM 
						revenue_unit_type_settings rutp
					 JOIN unit_types ut on ut.id = rutp.unit_type_id
					 JOIN company_users cu ON cu.id = rutp.updated_by
					WHERE 
						rutp.property_id = ' . ( int ) $intPropertyId . '
					     AND rutp.unit_type_id IN ( ' . $strUnitTypeIds . ' )
	    			     AND rutp.cid::integer = ' . ( int ) $intCid . '
	    			     ORDER BY rutp.updated_on DESC';
		return fetchData( $strSql, $objDatabase );
	}

}
?>