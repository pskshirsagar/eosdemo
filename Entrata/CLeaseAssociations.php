<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseAssociations
 * Do not add any new functions to this class.
 */

class CLeaseAssociations extends CBaseLeaseAssociations {

	public static function fetchLeaseAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, [ 'CLeaseAssociation', 'createConcreteLeaseAssociation' ], $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchLeaseAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, [ 'CLeaseAssociation', 'createConcreteLeaseAssociation' ], $objDatabase );
	}

	public static function fetchLeaseAssociationsByLeaseIntervalIdByCid( $intLeaseIntervalId, $intCid, $objDatabase, $boolFetchDeletedOnly = false ) {
		( true == $boolFetchDeletedOnly ) ? $strDeletedCondition = ' AND la.deleted_on IS NOT NULL' : $strDeletedCondition = ' AND la.deleted_on IS NULL';
		$strSql = 'SELECT
						la.*
					FROM
						lease_associations la
					WHERE
						la.cid = ' . ( int ) $intCid . $strDeletedCondition . '
						AND la.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
						AND la.lease_status_type_id = ' . CLeaseStatusType::APPLICANT . '
						AND la.cid = ' . ( int ) $intCid;
		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAllLeaseAssociationsByLeaseIntervalIdByCid( $intLeaseIntervalId, $intCid, $objDatabase, $boolIncludeDeleted = false ) {
		$strDeletedCondition = ( false == $boolIncludeDeleted ) ? ' AND la.deleted_on IS NULL' : ' ';
		$strSql = 'SELECT
						la.*
					FROM
						lease_associations la
					WHERE
						la.cid = ' . ( int ) $intCid . $strDeletedCondition . '
						AND la.lease_interval_id = ' . ( int ) $intLeaseIntervalId;

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchSpecialLeaseAssociationsCountByQuoteIdsByCid( $arrintQuoteIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintQuoteIds ) ) return NULL;

		$strSql = 'SELECT
						la.quote_id,
						la.quote_lease_term_id,
						COUNT( la.id ) AS count
					FROM
						lease_associations la
				   	WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.ar_origin_id = ' . ( int ) CArOrigin::SPECIAL . '
						AND la.quote_id IN ( ' . implode( ',', $arrintQuoteIds ) . ' )
					GROUP BY
						la.quote_id,
						la.quote_lease_term_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRenewalSpecialTierSpecialLeaseAssociationsByLeaseIdByLeaseIntervalIdByCid( $intLeaseId, $intLeaseIntervalId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						la.*,
						qlt.lease_term_id,
						q.leasing_tier_special_id
					FROM
						lease_associations la
						JOIN quote_lease_terms qlt ON ( qlt.cid = la.cid AND qlt.id = la.quote_lease_term_id AND qlt.quote_id = la.quote_id )
						JOIN quotes q ON ( q.cid = la.cid AND q.id = la.quote_id )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.lease_id = ' . ( int ) $intLeaseId . '
						AND la.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
						AND la.ar_origin_id = ' . CArOrigin::SPECIAL . '
						AND la.deleted_by IS NULL
						AND la.deleted_on IS NULL
						AND qlt.deleted_by IS NULL
						AND qlt.deleted_on IS NULL
						AND la.ar_origin_reference_id IS NOT NULL';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchSpecialLeaseAssociationsByQuoteIdBySpecialTypeIdByCid( $intQuoteId,$intSpecialTypeId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						la.*,
						s.gift_value as rate_amount
					FROM
						lease_associations la
						JOIN specials s ON ( la.cid = s.cid AND la.ar_origin_reference_id = s.id AND s.special_type_id = ' . ( int ) $intSpecialTypeId . ' )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.quote_id = ' . ( int ) $intQuoteId . '
						AND la.ar_origin_id = ' . CArOrigin::SPECIAL;
		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetLeaseAssociationsByQuoteIdsByCid( $arrintQuoteIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintQuoteIds ) ) return NULL;

		$strSql = 'SELECT
						la.quote_id,
						la.id,
						COUNT( la.id ) AS count
					FROM
						lease_associations la
				   	WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND la.quote_id IN ( ' . implode( ',', $arrintQuoteIds ) . ' )
					GROUP BY
						la.id,
						la.quote_id';
		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchLeaseAssociationsByLeaseIntervalIdsByCid( $arrintLeaseIntervalIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintLeaseIntervalIds ) ) return NULL;

		$strSql = 'SELECT
						la.*
					FROM
						lease_associations la
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.deleted_on IS NULL
						AND la.lease_interval_id IN (' . implode( ',', $arrintLeaseIntervalIds ) . ' )
						AND la.lease_status_type_id = ' . CLeaseStatusType::APPLICANT . '
						AND la.cid = ' . ( int ) $intCid;
		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsByLeaseIntervalIdsByCid( $arrintLeaseIntervalIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintLeaseIntervalIds ) ) return NULL;

		$strSql = 'SELECT
						la.*
					FROM
						lease_associations la
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.deleted_on IS NULL
						AND la.lease_interval_id IN (' . implode( ',', $arrintLeaseIntervalIds ) . ' )
						AND la.lease_status_type_id = ' . CLeaseStatusType::CURRENT . '
						AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND la.cid = ' . ( int ) $intCid;
		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchLeaseAssociationsByQuoteIdsByCid( $arrintQuoteIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintQuoteIds ) ) return NULL;

		$strSql = 'SELECT
						la.*
					FROM
						lease_associations la
				   	WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.quote_id IN ( ' . implode( ',', $arrintQuoteIds ) . ' )';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAddOnPetsLeaseAssociationsByLeaseIntervalIdsByByQuoteIdByCid( $intLeaseIntervalId, $intQuoteId, $intCid, $objDatabase ) {
		if( false == valId( $intLeaseIntervalId ) ) return NULL;

		$strSql = 'SELECT
						la.*
					FROM
						lease_associations la
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND ( quote_id = ' . ( int ) $intQuoteId . ' OR quote_id IS NULL )
						AND la.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
						AND la.lease_status_type_id = ' . CLeaseStatusType::APPLICANT . '
						AND la.ar_origin_id IN ( ' . CArOrigin::ADD_ONS . ', ' . CArOrigin::PET . ' )
						AND la.deleted_on IS NULL';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetsLeaseAssociationsByLeaseIntervalIdsByByQuoteIdByCid( $intLeaseIntervalId, $intQuoteId, $intCid, $objDatabase ) {
		if( false == valId( $intLeaseIntervalId ) ) return NULL;

		$strSql = 'SELECT
						la.*
					FROM
						lease_associations la
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND quote_id = ' . ( int ) $intQuoteId . ' 
						AND la.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '  
						AND la.lease_status_type_id = ' . CLeaseStatusType::APPLICANT . '
						AND la.ar_origin_id IN ( ' . CArOrigin::PET . ' )
						AND la.deleted_on IS NULL';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetsLeaseAssociationsByLeaseIntervalIdsByCid( $arrintLeaseIntervalIds, $intCid, $objDatabase, $intLeaseStatusType = CLeaseStatusType::CURRENT ) {
		if( false == valArr( $arrintLeaseIntervalIds ) ) return NULL;

		$strSql = 'SELECT
						la.*
					FROM
						lease_associations la
					WHERE
						la.cid = ' . ( int ) $intCid . '

						AND la.lease_interval_id IN (' . ( int ) implode( ',', $arrintLeaseIntervalIds ) . ' )
						AND la.lease_status_type_id = ' . ( int ) $intLeaseStatusType . '
						AND la.ar_origin_id = ' . CArOrigin::PET . '
						AND la.deleted_on IS NULL';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAllLeaseAssociationsByLeaseIntervalIdsByArOriginIdsByCid( $arrintLeaseIntervalIds, $arrintArOriginIds, $intCid, $objDatabase, $intLeaseStatusType = CLeaseStatusType::CURRENT ) {
		if( false === valArr( $arrintLeaseIntervalIds ) || false === valArr( $arrintArOriginIds ) || false === valId( $intCid ) || false === valObj( $objDatabase, 'CDatabase' ) ) return NULL;

		$strSql = 'SELECT
						la.*
					FROM
						lease_associations la
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.lease_interval_id IN (' . sqlIntImplode( $arrintLeaseIntervalIds ) . ' )
						AND la.lease_status_type_id = ' . ( int ) $intLeaseStatusType . '
						AND la.ar_origin_id IN (' . sqlIntImplode( $arrintArOriginIds ) . ' )
						AND la.deleted_on IS NULL';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAllApplicationAndQuoteLeaseAssociationsByQuoteIdByLeaseIntervalIdByArOriginIdsByCid( $intQuoteId, $intLeaseIntervalId, $arrintArOriginIds, $intCid, $objDatabase, $intLeaseStatusType = CLeaseStatusType::APPLICANT ) {
		if( false === valId( $intLeaseIntervalId ) || false === valId( $intCid ) || false === valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		if( true == valId( $intQuoteId ) ) $strWhereQuoteId = 'OR quote_id = ' . $intQuoteId;

		$strSql = 'SELECT
						la.*
					FROM
						lease_associations la
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.lease_interval_id = ' . $intLeaseIntervalId . ' 
						AND la.lease_status_type_id = ' . ( int ) $intLeaseStatusType . '
						AND la.ar_origin_id IN (' . sqlIntImplode( $arrintArOriginIds ) . ' )
						AND la.deleted_on IS NULL
						AND ( quote_id IS NULL ' . $strWhereQuoteId . ' )';
		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchRenwalGiftIncentivesByLeaseIntervalIdByQuoteLeaseTermIdsByCid( $intLeaseIntervalId, $arrintQuoteLeaseTermIds, $intCid, $objDatabase ) {

		if( false == valId( $intLeaseIntervalId ) || false == valArr( $arrintQuoteLeaseTermIds ) ) return NULL;

		$strSql = 'SELECT
						la.*,
						util_get_translated( \'name\', s.name, s.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS description,
						qlt.lease_term_id AS lease_term_id
					FROM
						lease_associations la
						JOIN specials s ON ( la.cid = s.cid AND s.id = la.ar_origin_reference_id )
						LEFT JOIN quote_lease_terms qlt ON ( la.cid = qlt.cid AND la.quote_lease_term_id = qlt.id )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND s.deleted_on IS NULL
						AND s.is_active IS TRUE
						AND la.deleted_on IS NULL
						AND s.special_type_id = ' . CSpecialType::GIFT_INCENTIVE . '
						AND s.special_recipient_id = ' . CSpecialRecipient::RENEWALS . '
						AND la.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
						AND (
								la.quote_lease_term_id IN ( ' . sqlIntImplode( $arrintQuoteLeaseTermIds ) . ' )
								OR ( la.quote_id IS NULL AND la.quote_lease_term_id IS NULL )
							)
						';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchLeaseAssociationsByCustomerIdByLeaseIdByCid( $intCustomerId, $intLeaseId, $intCid, $objDatabase ) {

		if( false == valId( $intCustomerId ) || false == valId( $intLeaseId ) ) return NULL;

		$strSql = 'SELECT
					    la.*
					FROM
					    lease_associations la
					    JOIN customer_pets cp ON ( cp.id = la.ar_origin_object_id AND cp.cid = la.cid )
					WHERE
					    la.cid = ' . ( int ) $intCid . '
					    AND la.lease_id = ' . ( int ) $intLeaseId . '
					    AND cp.customer_id = ' . ( int ) $intCustomerId . '
					    AND la.ar_origin_id = ' . CArOrigin::PET . '
					    AND la.deleted_on IS NULL';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchLeaseAssociationsByCidByPropertyIdByLeaseId( $intCid, $intPropertyId, $intLeaseId, $objDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intLeaseId ) ) return NULL;

		$strSql = 'SELECT
				DISTINCT( la.ar_origin_reference_id ),
				CASE
					WHEN ao.name IS NOT NULL THEN util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' )
					WHEN pt.name IS NOT NULL THEN util_get_translated( \'name\', pt.name, pt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' )
				END AS name,
				la.ar_origin_id,
				CASE
					WHEN la.ar_origin_id = ' . CArOrigin::PET . ' THEN NULL
					ELSE la.id
				END AS lease_association_id
			FROM
				lease_associations la
				LEFT JOIN pet_types pt ON( pt.cid = la.cid AND pt.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::PET . ' )
				LEFT JOIN add_ons ao ON( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
			WHERE
				la.cid = ' . ( int ) $intCid . '
				AND la.property_id = ' . ( int ) $intPropertyId . '
				AND la.lease_id = ' . ( int ) $intLeaseId . '
				AND la.deleted_on IS NULL
				AND  CASE
			            WHEN la.ar_origin_id = ' . CArOrigin::PET . ' THEN la.lease_status_type_id IN( ' . implode( ',', CLeaseStatusType::$c_arrintResidentTransferLeaseStatusTypes ) . ' )
			            ELSE la.lease_status_type_id IN( ' . implode( ',', CLeaseStatusType::$c_arrintActiveAddOnReservationStatusTypes ) . ' )
			         END';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMealPlanServicesLeaseAssociationsByLeaseIdByPropertyIdByCid( $intLeaseId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intLeaseId ) ) return NULL;

		$strSql = '	SELECT
						la.id,
						aog.id as add_on_group_id,
						ao.id as add_on_id
					FROM
						lease_associations la
						JOIN add_ons ao ON( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id )
						JOIN add_on_groups aog ON( aog.cid = ao.cid AND aog.id = ao.add_on_group_id AND aog.add_on_type_id = ' . CAddOnType::SERVICES . ' )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id = aog.property_id OR aoc.property_id IS NULL ) AND aoc.default_add_on_category_id = ' . CDefaultAddOnCategory::MEAL_PLANS . ' )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.property_id = ' . ( int ) $intPropertyId . '
						AND la.lease_id = ' . ( int ) $intLeaseId . '
						AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND la.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMealPlanServiceLeaseAssociationByLeaseIdByPropertyIdByCid( $intLeaseId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intLeaseId ) ) return NULL;

		$strSql = '	SELECT
						la.*
					FROM
						lease_associations la
						JOIN add_ons ao ON( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id )
						JOIN add_on_groups aog ON( aog.cid = ao.cid AND aog.id = ao.add_on_group_id AND aog.add_on_type_id = ' . CAddOnType::SERVICES . ' )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id = aog.property_id OR aoc.property_id IS NULL ) AND aoc.default_add_on_category_id = ' . CDefaultAddOnCategory::MEAL_PLANS . ' )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.property_id = ' . ( int ) $intPropertyId . '
						AND la.lease_id = ' . ( int ) $intLeaseId . '
						AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND la.deleted_on IS NOT NULL
					ORDER BY
						id DESC
					LIMIT 1';

		return parent::fetchLeaseAssociation( $strSql, $objDatabase );
	}

	public static function fetchPetLeaseAssociationsByPropertyIdByArOriginObjectIdsByCid( $intPropertyId, $arrintCustomerPetIds, $intCid, $objDatabase ) {
		if( false == valId( $intPropertyId ) || false == valArr( $arrintCustomerPetIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = ' SELECT
                        la.*
					FROM
                        lease_associations la
					    JOIN customer_pets cp ON ( cp.id = la.ar_origin_object_id AND cp.cid = la.cid )
					WHERE
                        la.cid = ' . ( int ) $intCid . '
						AND la.property_id = ' . ( int ) $intPropertyId . '
						AND ar_origin_id = ' . ( int ) CArOrigin::PET . '
						AND la.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
						AND la.ar_origin_object_id IN ( ' . implode( ',', $arrintCustomerPetIds ) . ' )';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchLeaseAssociationsByCustomerIdByLeaseIdByArOriginIdByPropertyIdByCid( $intCustomerId, $intLeaseId, $arrintCustomerPetId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intCustomerId ) || false == valId( $intLeaseId ) || false == valArr( $arrintCustomerPetId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
					    la.*
					FROM
					    lease_associations la
					    JOIN customer_pets cp ON ( cp.id = la.ar_origin_object_id AND cp.cid = la.cid )
					WHERE
					    la.cid = ' . ( int ) $intCid . '
					    AND la.lease_id = ' . ( int ) $intLeaseId . '
					    AND cp.customer_id = ' . ( int ) $intCustomerId . '
					    AND la.ar_origin_id = ' . CArOrigin::PET . '
					    AND la.property_id =  ' . ( int ) $intPropertyId . '
						AND la.ar_origin_object_id IN ( ' . implode( ',', $arrintCustomerPetId ) . ' )';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchPetLeaseAssociationsByPropertyUnitIdsByPropertyIdByCid( $arrintPropertyUnitId, $intPropertyId, $intCid, $objDatabase, $intPetTypeId = NULL ) {
		if( false == valArr( $arrintPropertyUnitId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSqlCondition = '';

		if( true == valId( $intPetTypeId ) ) {
			$strSqlCondition = ' AND cp.pet_type_id = ' . ( int ) $intPetTypeId;
		}

		$strSql = 'SELECT
						la.*
					FROM
						customer_pets cp
						JOIN pet_types pt ON ( pt.id = cp.pet_type_id AND pt.cid = cp.cid )
						JOIN lease_associations la ON ( cp.cid = la.cid AND ar_origin_id = ' . CArOrigin::PET . ' AND cp.id = la.ar_origin_object_id AND la.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . ' )
						JOIN leases AS l ON( l.cid = la.cid AND l.id = la.lease_id )
						JOIN unit_spaces us ON ( l.unit_space_id = us.id AND l.cid = us.cid )
					WHERE
						cp.cid = ' . ( int ) $intCid . '
					AND l.property_id = ' . ( int ) $intPropertyId . '
					' . $strSqlCondition . '
					AND us.property_unit_id IN ( ' . implode( ',', $arrintPropertyUnitId ) . ' )';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public function fetchLeaseAssociationsByCustomerIdByLeaseIdByPropertyIdByCid( $intCustomerId, $intLeaseId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intCustomerId ) || false == valId( $intLeaseId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
					    *
					FROM
					    lease_associations
					WHERE
					    cid = ' . ( int ) $intCid . '
					    AND lease_id = ' . ( int ) $intLeaseId . '
					    AND customer_id = ' . ( int ) $intCustomerId . '
					    AND ar_origin_id = ' . CArOrigin::ADD_ONS . '
					    AND property_id =  ' . ( int ) $intPropertyId . '
					    AND deleted_on IS NULL
					    AND deleted_by IS NULL';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchLeaseAssociationByArOriginIdsByArOriginReferenceIdByLeaseIntervalIdByCid( $arrintArOriginIds, $intArOriginReferenceId, $intLeaseIntervalId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintArOriginIds ) || false == valId( $intArOriginReferenceId ) || false == valId( $intLeaseIntervalId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						la.id
					FROM
						lease_associations la
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.ar_origin_id IN ( ' . implode( ',', $arrintArOriginIds ) . ' )
						AND la.ar_origin_reference_id = ' . ( int ) $intArOriginReferenceId . '
						AND la.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
						AND la.deleted_on IS NULL
						AND la.deleted_by IS NULL';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function archiveRiskPremiumLeaseAssociations( $intLeaseIntervalId, $intCid, $intCurrentUserId, $objDatabase ) {
		if( false == valId( $intLeaseIntervalId ) || false == valId( $intCid ) ) {
			return false;
		}

		$strSql = ' UPDATE
						lease_associations
					SET
						deleted_by = ' . ( int ) $intCurrentUserId . ',
						deleted_on = NOW(),
						is_published = false
					WHERE
						lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
						AND cid = ' . ( int ) $intCid . '
						AND ar_origin_id = ' . ( int ) CArOrigin::RISK_PREMIUM;

		$arrstrResult = executeSql( $strSql, $objDatabase );
		return $arrstrResult['failed'];
	}

	public static function fetchLeaseAssociationsByLeaseCustomerIdsByCid( $arrintLeaseCustomerIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseCustomerIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						la.*,
						cp.customer_id
					FROM
						lease_associations la
						JOIN customer_pets cp ON ( cp.id = la.ar_origin_object_id AND cp.cid = la.cid )
						JOIN lease_customers lc ON ( lc.cid = la.cid AND lc.customer_id = cp.customer_id AND lc.lease_id = la.lease_id )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND lc.id IN ( ' . sqlIntImplode( $arrintLeaseCustomerIds ) . ' )
						AND la.ar_origin_id = ' . CArOrigin::PET . '
						AND la.deleted_on IS NULL';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchLeaseAssociationsByArOriginReferenceIdsByLeaseIdByCid( $arrintArOriginReferenceIds, $intLeaseId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintArOriginReferenceIds ) || false == valId( $intLeaseId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						lease_associations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND lease_id = ' . ( int ) $intLeaseId . ' 
						AND ar_origin_reference_id IN ( ' . sqlIntImplode( $arrintArOriginReferenceIds ) . ' )
						AND deleted_on IS NULL
						AND deleted_by IS NULL';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchSpecialsLeaseAssociationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					  la.*
				  FROM
					  lease_associations la
					  JOIN lease_intervals li ON ( li.cid = la.cid AND li.lease_id = la.lease_id AND li.id = la.lease_interval_id )
				  WHERE
					  la.cid = ' . ( int ) $intCid . '
					  AND la.property_id = ' . ( int ) $intPropertyId . '
					  AND la.ar_origin_id = ' . CArOrigin::SPECIAL . '
					  AND li.property_id = la.property_id 
					  AND la.deleted_by IS NULL';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

}
?>
