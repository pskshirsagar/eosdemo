<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeAccounts
 * Do not add any new functions to this class.
 */
class CApPayeeAccounts extends CBaseApPayeeAccounts {

	public static function fetchApPayeeAccountsByAccountNumberByApPayeeIdByApPayeeLocationIdByCid( $strAccountNumber, $intApPayeeId, $intApPayeeLocationId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_payee_accounts
				 	WHERE
						upper( account_number ) = upper( \'' . $strAccountNumber . '\' )
						AND cid = ' . ( int ) $intCid . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ap_payee_location_id = ' . ( int ) $intApPayeeLocationId;

		return self::fetchApPayeeAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeAccountsByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_payee_accounts
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' ) ';

		return self::fetchApPayeeAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeAccountByUtilityBillAccountIdByApPayeeIdByCid( $intUtilityBillAccountId, $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					 FROM
						ap_payee_accounts
					WHERE
						utility_bill_account_id = ' . ( int ) $intUtilityBillAccountId . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND cid = ' . ( int ) $intCid . '
					LIMIT 1';

		return self::fetchApPayeeAccount( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeAccountDataByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase, $arrintLocationIds = NULL, $arrintPropertyIds = NULL ) {

		if( false == valId( $intCid ) || false == valId( $intApPayeeId ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strWhereCondition = '';

		if( true == valIntArr( $arrintLocationIds ) ) {
			$strWhereCondition = ' AND apl.id IN (' . implode( ',', $arrintLocationIds ) . ')';
		}

		$strSql = 'SELECT
						apa.id,
						ar.id as remittance_id,
						apa.account_number,
						apa.account_description,
						apa.utility_bill_account_id,
						apa.buyer_account_id,
						apa.is_capture_invoice_total_only,
						apa.is_disabled,
						array_to_string( array_agg(  apsa.account_number ),  \',\' ) as sub_account_number,
						ah.header_number,
						ah.post_date,
						apl.location_name,
						p.property_name,
						ar.street_line1,
						ar.street_line2,
						ar.street_line3,
						ar.city,
						ar.country_code,
						ar.state_code,
						ar.postal_code,
						ar.check_routing_number,
						ar.check_account_number_encrypted,
						apt.id as remittance_type_id,
						apt.name AS payment_type_name,
						apa.is_punchout
					FROM
						ap_payee_accounts apa
						LEFT JOIN ap_payee_sub_accounts apsa ON ( apa.id = apsa.ap_payee_account_id AND apa.cid = apsa.cid )
						LEFT JOIN ap_headers ah ON ( ah.ap_payee_account_id = apa.id AND apa.cid = ah.cid AND ah.is_posted = true AND ah.ap_header_type_id = ' . CApHeaderType::JOB . ' )
						JOIN ap_payee_locations apl ON ( apa.ap_payee_location_id = apl.id AND apa.cid = apl.cid )
						JOIN ap_remittances ar ON ( ar.id = apa.default_ap_remittance_id AND apa.cid = ar.cid )
						LEFT JOIN ap_payment_types apt ON ( ar.ap_payment_type_id = apt.id )
						LEFT JOIN properties p ON ( apa.default_property_id = p.id AND apa.cid = p.cid )
					WHERE
						apa.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND apa.cid = ' . ( int ) $intCid . $strWhereCondition . '
						AND apa.account_number IS NOT NULL
						AND ( apa.default_property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) OR apa.default_property_id IS NULL )
					GROUP BY
						apa.id,
						apa.account_number,
						apa.utility_bill_account_id,
						apa.buyer_account_id,
						apa.account_description,
						apa.is_disabled,
						ah.header_number,
						ah.post_date,
						apl.location_name,
						p.property_name,
						ar.street_line1,
						ar.street_line2,
						ar.street_line3,
						ar.city,
						ar.country_code,
						ar.state_code,
						ar.postal_code,
						ar.check_routing_number,
						ar.check_account_number_encrypted,
						apt.id,
						apt.name,
						apa.is_capture_invoice_total_only,
						apa.is_punchout,
						ar.id
					ORDER BY
						apa.account_number ASC';

		return fetchData( $strSql, $objClientDatabase );

	}

	public static function fetchApPayeeAccountByApPayeeIdByApPayeeLocationIdByCid( $intApPayeeId, $intApPayeeLocationId, $intCid, $objClientDatabase, $boolIsDisabled = false ) {

		$strSubSql = ( $boolIsDisabled == true ) ? ' AND is_disabled IS false' : '';

		$strSql = 'SELECT
						*
					FROM
						ap_payee_accounts
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ap_payee_location_id = ' . ( int ) $intApPayeeLocationId . '
						AND id IN ( SELECT
										MAX ( id )
									FROM
										ap_payee_accounts apa
									WHERE
										cid = ' . ( int ) $intCid . '
										AND ap_payee_id = ' . ( int ) $intApPayeeId . '
										AND ap_payee_location_id = ' . ( int ) $intApPayeeLocationId . $strSubSql . '
								)';

		return self::fetchApPayeeAccount( $strSql, $objClientDatabase );

	}

	public static function fetchEnabledApPayeeAccountsCountByApPayeeIdByApPayeeLocationIdByCid( $intApPayeeId, $intApPayeeLocationId, $intCid, $objClientDatabase, $intExceptApPayeeAccountId = NULL ) {

		$strSubSql = ( false == is_null( $intExceptApPayeeAccountId ) ) ? ' AND id <> ' . ( int ) $intExceptApPayeeAccountId : '';

		$strWhereSql = 'WHERE
							cid = ' . ( int ) $intCid . '
							AND ap_payee_id = ' . ( int ) $intApPayeeId . '
							AND ap_payee_location_id = ' . ( int ) $intApPayeeLocationId . '
							AND is_disabled IS FALSE ' . $strSubSql;

		return self::fetchApPayeeAccountCount( $strWhereSql, $objClientDatabase );

	}

	public static function fetchApPayeeAccountsByApPayeeLocationIdsByCid( $arrintApPayeeLocationIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeLocationIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_payee_accounts
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_location_id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' ) ';

		return self::fetchApPayeeAccounts( $strSql, $objClientDatabase );

	}

	public static function fetchApPayeeAccountsByApPayeeIdsByApPayeeLocationIdsByCid( $arrintApPayeeIds, $arrintApPayeeLocationIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintApPayeeIds ) || false == valIntArr( $arrintApPayeeLocationIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						apa.*,
						apt.id as ap_payee_term_id
					FROM
						ap_payee_accounts apa
						LEFT JOIN ap_payee_terms apt ON ( apa.cid = apt.cid AND apt.system_code ILIKE \'Net\' )
					WHERE
						apa.cid = ' . ( int ) $intCid . '
						AND apa.ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND apa.ap_payee_location_id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' ) ';

		return fetchData( $strSql, $objClientDatabase );

	}

	public static function fetchSimpleApPayeeAccountsByCids( $arrintCids, $objClientDatabase ) {

		if( false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						id,
						cid,
						ap_payee_id,
						audit_day,
						audit_frequency_id,
						audit_invoice_count,
						utility_bill_receipt_type_id
					FROM
						ap_payee_accounts
					WHERE
						cid IN ( ' . sqlIntImplode( $arrintCids ) . ' )
						AND utility_bill_account_id IS NULL
						AND audit_invoice IS TRUE
						AND audit_day IS NOT NULL
						AND audit_frequency_id IS NOT NULL
						AND is_disabled IS FALSE
						AND replacement_ap_payee_account_id IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeAccountsByApPayeeLocationIdsByCids( $arrintApPayeeLocationIds, $arrintCids, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeLocationIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						apa.*, ap.is_utilities
					FROM
						ap_payee_accounts apa
						JOIN ap_payees ap ON ( ap.id = apa.ap_payee_id )
					WHERE
						apa.cid IN ( ' . sqlIntImplode( $arrintCids ) . ' )
						AND apa.ap_payee_location_id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' ) ';

		return self::fetchApPayeeAccounts( $strSql, $objClientDatabase, false );

	}

	public static function fetchApPayeeAccountsByIdsByCids( $arrintApPayeeAccountIds, $arrintCids, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeAccountIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_payee_accounts
					WHERE
						cid IN ( ' . sqlIntImplode( $arrintCids ) . ' )
						AND id IN ( ' . sqlIntImplode( $arrintApPayeeAccountIds ) . ' ) ';

		return self::fetchApPayeeAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchSyncApPayeeAccountsByApLegalEntityIdsByCid( $arrintApLegalEntityIds, $intCid, $objDatabase ) {

		if( false == valIntArr( $arrintApLegalEntityIds ) ) return NULL;

		$strSql = 'SELECT
						apa.*
					FROM
						ap_payee_accounts apa
						JOIN ap_payee_locations apl ON ( apa.ap_payee_location_id = apl.id and apa.cid = apl.cid )
					WHERE
						apl.ap_legal_entity_id IN (' . implode( ',', $arrintApLegalEntityIds ) . ' )
						AND apl.cid = ' . ( int ) $intCid . '
						AND apl.store_id IS NOT NULL
						AND apl.disabled_by IS NULL
						AND apa.is_disabled IS FALSE
						AND apa.buyer_account_id IS NOT NULL';

		return self::fetchApPayeeAccounts( $strSql, $objDatabase );
	}

	public static function fetchUnsyncApPayeeAccountsByApLegalEntityIdsByCid( $arrintApLegalEntityIds, $intCid, $objDatabase ) {

		if( false == valIntArr( $arrintApLegalEntityIds ) ) return NULL;

		$strSql = 'SELECT
						apa.*,
						apl.location_name,
						apl.ap_legal_entity_id
					FROM
						ap_payee_accounts apa
						JOIN ap_payee_locations apl ON ( apa.ap_payee_location_id = apl.id and apa.cid = apl.cid AND apl.store_id IS NOT NULL )
					WHERE
						apl.ap_legal_entity_id IN (' . implode( ',', $arrintApLegalEntityIds ) . ' )
						AND apa.cid = ' . ( int ) $intCid . '
						AND apa.buyer_account_id IS NULL
						AND apa.is_disabled IS FALSE';

		return self::fetchApPayeeAccounts( $strSql, $objDatabase );
	}

	public static function fetchApPayeeAccountsByUtilityBillAccountIdsByCid( $arrintUtilityBillAccountIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintUtilityBillAccountIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						id,
						utility_bill_account_id
					FROM
						ap_payee_accounts
					WHERE
						utility_bill_account_id IN ( ' . implode( ',', $arrintUtilityBillAccountIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApPayeeAccountsByUtilityBillReceiptTypeIdByPropertyIdsByCid( $intUtilityBillReceiptTypeId, $arrintPropertyIds, $intCid, $boolIsFetchCount, $boolIsFetchUnAssociatedWithProperty, $objDatabase ) {

		if( false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strWhere = $strSelectSql = '';
		if( true == valArr( $arrintPropertyIds ) ) {
			$strWhere = ' AND ( apa.default_property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' ) ';
		}

		if( true == $boolIsFetchUnAssociatedWithProperty ) {
			$strWhere .= ( true == valArr( $arrintPropertyIds ) ) ? ' OR ' : ' AND ( ';
			$strWhere .= 'apa.default_property_id IS NULL';
		}

		if( true == valStr( $strWhere ) ) {
			$strWhere .= ' )';
		}

		if( true == is_numeric( $intUtilityBillReceiptTypeId ) ) {
			$strWhere .= ' AND apa.utility_bill_receipt_type_id = ' . ( int ) $intUtilityBillReceiptTypeId;
		}

		if( false == $boolIsFetchCount ) {
			$strWhere .= ' AND apa.entrata_address_on IS NULL ';
			$strSelectSql = ' apa.*,
						app.company_name,
						apl.location_name ';
		} else {
			$strSelectSql = ' count ( DISTINCT apa.id ) ';
		}

		$strSql = 'SELECT
						' . $strSelectSql . '
					FROM
						ap_payee_accounts apa
						JOIN ap_payees app ON( apa.cid = app.cid AND apa.ap_payee_id = app.id )
						JOIN ap_payee_locations apl ON ( apa.ap_payee_location_id = apl.id AND apa.cid = apl.cid )
					WHERE
						apa.cid = ' . ( int ) $intCid . ' 
						AND apa.is_disabled IS FALSE 
						AND apa.utility_bill_account_id IS NULL 
						' . $strWhere;

		$arrmixApPayeeAccountsCount = fetchData( $strSql, $objDatabase );

		return ( false == $boolIsFetchCount ) ? $arrmixApPayeeAccountsCount : $arrmixApPayeeAccountsCount[0]['count'];
	}

	public static function fetchSyncedApPayeeAccountsByApPayeeLocationIdsByApPayeeIdByCid( $arrintApPayeeLocationIds, $intApPayeeId, $intCid, $objClientDatabase, $boolIn = true ) {

		$strSql = '';

		if( false == valIntArr( $arrintApPayeeLocationIds ) ) return NULL;

		if( false == $boolIn ) $strSql = 'NOT';

		$strSql = 'SELECT
		                *
		            FROM
                        ap_payee_accounts
    	            WHERE
    	                buyer_account_id IS NOT NULL
    	                AND ap_payee_location_id ' . $strSql . ' IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )
    	                AND cid = ' . ( int ) $intCid . '
    	                AND ap_payee_id = ' . ( int ) $intApPayeeId;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeAccountByIdByApPayeeIdByCid( $intApPayeeAccountId, $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						apa.*,
						apl.store_id
					FROM
						ap_payee_accounts apa
						JOIN ap_payee_locations apl ON( apl.id = apa.ap_payee_location_id AND apl.cid = apa.cid )
					WHERE
						apa.id = ' . ( int ) $intApPayeeAccountId . '
						AND apa.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND apa.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeAccountsByAccountNumberByCid( $strAccountNumber, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_payee_accounts
				 	WHERE
						upper( account_number ) = upper( \'' . $strAccountNumber . '\' )
						AND cid = ' . ( int ) $intCid . '
						AND is_disabled IS false';

		return self::fetchApPayeeAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeAccountsByAccountNumbersByApPayeeIdByCid( $arrstrAccountNumbers, $intApPayeeId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrstrAccountNumbers ) ) return NULL;

		$strSql = 'SELECT * 
					FROM ap_payee_accounts
					WHERE 
						id IN( 
							SELECT
								min( id )
							FROM
								ap_payee_accounts
				 	           WHERE
								upper( account_number ) IN ( ' . '\'' . implode( '\',\'', $arrstrAccountNumbers ) . '\'' . ' )
								AND cid = ' . ( int ) $intCid . '
								AND ap_payee_id = ' . ( int ) $intApPayeeId . '
							GROUP BY 
								cid,
								account_number )
						AND cid = ' . ( int ) $intCid . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId;

		return self::fetchApPayeeAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeAccountsCountsByCids( $arrintCIds, $objDatabase ) {

		if( false == valArr( $arrintCIds ) ) return NULL;

		$strSql = 'SELECT 
						COUNT(apa.id) as total,
						COUNT( CASE WHEN apa.buyer_account_id IS NOT NULL THEN apa.id END ) as synced,
						apa.cid
					FROM
						ap_payee_accounts apa
						JOIN ap_payees app ON( apa.cid = app.cid AND apa.ap_payee_id = app.id )
					WHERE
						apa.cid IN (' . sqlIntImplode( $arrintCIds ) . ' )
						AND app.ap_payee_status_type_id = ' . ( int ) CApPayeeStatusType::ACTIVE . '
						AND app.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ' )
						AND apa.account_number IS NOT NULL
					GROUP BY 
						apa.cid	';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomApPayeeAccountsByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeIds ) ) return NULL;

		$strSql = 'SELECT
						apa.*,
						ar.name as remittance_name,
						ar.ap_payment_type_id,
						apl.location_name
					FROM
						ap_payee_accounts apa
						JOIN ap_payee_locations apl ON ( apa.ap_payee_location_id = apl.id AND apa.cid = apl.cid )
						JOIN ap_remittances ar ON ( ar.id = apa.default_ap_remittance_id AND apa.cid = ar.cid )
					WHERE
						apa.cid = ' . ( int ) $intCid . '
						AND apa.ap_payee_id IN (' . implode( ',', $arrintApPayeeIds ) . ')
						AND apa.account_number IS NOT NULL';
		return self::fetchApPayeeAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchDefaultApPayeeAccountByApPayeeIdByApPayeeLocationIdByCid( $intApPayeeId, $intApPayeeLocationId, $intCid, $objClientDatabase ) {

		if( false == valId( $intApPayeeLocationId ) || false == valId( $intApPayeeId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						apa.*
					FROM
						ap_payee_accounts apa
					WHERE
						apa.cid = ' . ( int ) $intCid . '
						AND apa.account_number IS NULL
						AND apa.account_description IS NULL
						AND apa.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND apa.ap_payee_location_id = ' . ( int ) $intApPayeeLocationId . '
					LIMIT 1';

		return self::fetchApPayeeAccount( $strSql, $objClientDatabase );
	}

	public static function updateApPayeeAccountsByIdsByApPayeeIdByCId( $arrintIds, $intApPayeeId, $intCId, $intCompanyUserId ) {

		if( false == ( $arrintIds = getIntValuesFromArr( $arrintIds ) ) ) {
			return NULL;
		}

		$strSql = 'UPDATE
						ap_payee_accounts
					SET 
						buyer_account_id = NULL,
						is_punchout = FALSE, 
						updated_on = NOW(),
						updated_by = ' . ( int ) $intCompanyUserId . '
					WHERE
						id IN ( ' . sqlIntImplode( $arrintIds ) . ' ) 
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND cid = ' . ( int ) $intCId . ';';

		return $strSql;
	}

	public static function fetchSystemApPayeeAccountsByApPayeeLocationIdsByCid( $arrintApPayeeLocationIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeLocationIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_payee_accounts
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_location_id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )
						AND account_number IS NULL
						AND is_disabled Is false';

		return self::fetchApPayeeAccounts( $strSql, $objClientDatabase );

	}

	public static function fetchActiveApPayeeAccountByIdByCid( $intId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT 
						* 
					FROM 
						ap_payee_accounts 
					WHERE 
						id = ' . ( int ) $intId . ' 
						AND cid = ' . ( int ) $intCid . '
						AND is_disabled IS false';

		return self::fetchApPayeeAccount( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeAccountsByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase, $boolIsAllowNullAccounts = true ) {

		if( !valId( $intApPayeeId ) ) {
			return NULL;
		}

		$strWhereSql = NULL;

		if( !$boolIsAllowNullAccounts ) {
			$strWhereSql .= 'AND apa.account_number IS NOT NULL';
		}

		$strSql = 'SELECT
						apa.*,
						apl.location_name
					FROM
						ap_payee_accounts apa
						JOIN ap_payee_locations apl ON ( apa.cid = apl.cid AND apa.ap_payee_location_id = apl.id )
					WHERE
						apa.cid = ' . ( int ) $intCid . '
						AND apa.ap_payee_id = ' . ( int ) $intApPayeeId . '
						' . $strWhereSql . '
					ORDER BY
						apl.location_name,
						apa.id';

		return self::fetchApPayeeAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchSyncedApPayeeAccountByBuyerAccountIdByCid( $intBuyerAccountId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_payee_accounts
					WHERE
						buyer_account_id = ' . ( int ) $intBuyerAccountId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchApPayeeAccount( $strSql, $objClientDatabase );
	}

	public static function fetchSimpleApPayeeAccountsByIdsByCids( $arrintIds, $arrintCids, $objClientDatabase ) {

		if( false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						id,
						cid,
						ap_payee_id,
						audit_day,
						audit_frequency_id,
						audit_invoice_count,
						utility_bill_receipt_type_id
					FROM
						ap_payee_accounts
					WHERE
						id IN ( ' . sqlIntImplode( $arrintIds ) . ' )
						AND cid IN ( ' . sqlIntImplode( $arrintCids ) . ' )
						AND utility_bill_account_id IS NULL
						AND audit_day IS NOT NULL
						AND audit_frequency_id IS NOT NULL
						AND is_disabled IS FALSE
						AND replacement_ap_payee_account_id IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchSimpleApPayeeAccountsByApPayeeIdsByPropertyGroupIdsByCid( $arrintApPayeeLocationIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintApPayeeLocationIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						apa.account_number,
						apa.id,
						apa.ap_payee_id
					FROM
						ap_payee_accounts apa
					WHERE
						apa.cid = ' . ( int ) $intCid . '
						AND apa.ap_payee_location_id IN ( ' . sqlIntImplode( $arrintApPayeeLocationIds ) . ' )
						AND apa.account_number IS NOT NULL  
					ORDER BY
						1, 2, 3';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCustomApPayeeAccountsByApPayeeIdByCidByPropertyUserPermission( $intApPayeeId, $intCid, $intCompanyUserId, $objClientDatabase ) {

		if( false == valId( $intApPayeeId ) || false == valId( $intCid ) || false == valId( $intCompanyUserId ) ) return NULL;

		$strSql = ' SELECT 
							* 
					FROM
						( SELECT
							case WHEN apa.is_disabled = true THEN  
							CONCAT( apa.account_number,  \'  ( disabled )\' )
							ELSE
							 apa.account_number
							 END AS account_number,
							apa.id,
							apa.ap_payee_id,
							apa.cid,
							apa.is_disabled
						FROM
							ap_payee_accounts apa
							JOIN ( ' . \Psi\Eos\Entrata\CProperties::createService()->buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId ) . ' ) AS p ON ( apa.cid = p.cid AND apa.default_property_id = p.id )
						WHERE
							apa.cid = ' . ( int ) $intCid . '
							AND apa.ap_payee_id = ' . ( int ) $intApPayeeId . '
							AND apa.default_property_id IS NOT NULL

						UNION
						SELECT
							case WHEN apa.is_disabled = true THEN  
							CONCAT( apa.account_number,  \'  ( disabled )\' )
							ELSE
							 apa.account_number
							 END AS account_number,
							apa.id,
							apa.ap_payee_id,
							apa.cid,
							apa.is_disabled
						FROM
							ap_payee_accounts apa
						WHERE
							apa.cid = ' . ( int ) $intCid . '
							AND apa.ap_payee_id = ' . ( int ) $intApPayeeId . '
							AND apa.default_property_id IS NULL 
						) sub
					WHERE 
						sub.cid  = ' . ( int ) $intCid . '
						AND sub.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND sub.account_number IS NOT NULL
			ORDER by
				sub.is_disabled';

		return self::fetchApPayeeAccounts( $strSql, $objClientDatabase );
	}

}
?>