<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultLeaseStartStructures
 * Do not add any new functions to this class.
 */

class CDefaultLeaseStartStructures extends CBaseDefaultLeaseStartStructures {

    public static function fetchDefaultLeaseStartStructures( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CDefaultLeaseStartStructure', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchDefaultLeaseStartStructure( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CDefaultLeaseStartStructure', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>