<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningEventTypes
 * Do not add any new functions to this class.
 */

class CScreeningEventTypes extends CBaseScreeningEventTypes {

	public static function fetchScreeningEventTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScreeningEventType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningEventType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScreeningEventType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllPublishedScreeningEventTypes( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						screening_event_types
					WHERE 
						is_published = TRUE';

		return parent::fetchScreeningEventTypes( $strSql, $objDatabase );
	}

}
?>