<?php

class CCompanyActivityPoint extends CBaseCompanyActivityPoint {

	protected $m_boolBadgeGranted;
	protected $m_boolIsPro;

    public function __construct() {
        parent::__construct();

        $this->m_boolBadgeGranted = false;
        $this->m_boolIsPro = false;

        return;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDefaultActivityPointId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNoBadgePoints() {
        $boolIsValid = true;

        if( true == is_null( $this->m_intNoBadgePoints ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'no_badge_points', __( 'Points for Each Completion  are required.' ) ) );
        } elseif( 0 >= $this->m_intNoBadgePoints ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'no_badge_points', __( 'Points for Each Completion should always be greater than or equal to {%d, 0}.', [ 0 ] ) ) );
        }

        return $boolIsValid;
    }

    public function valBaseBadgePoints() {
        $boolIsValid = true;

        if( true == is_null( $this->m_intBaseBadgePoints ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'base_badge_points', __( 'Points for Base Badge are required.' ) ) );
        } elseif( 0 >= $this->m_intBaseBadgePoints ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'base_badge_points', __( 'Points for Base Badge should always be greater than or equal to {%d, 0}.', [ 0 ] ) ) );
        }

        return $boolIsValid;
    }

    public function valProBadgePoints() {
        $boolIsValid = true;

        if( true == is_null( $this->m_intProBadgePoints ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pro_badge_points', __( 'Points for Pro Badge are required.' ) ) );
        } elseif( 0 >= $this->m_intProBadgePoints ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pro_badge_points', __( 'Points for Pro Badge should always be greater than or equal to {%d, 0}.', [ 0 ] ) ) );
        }

        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valNoBadgePoints();
            	$boolIsValid &= $this->valBaseBadgePoints();
            	$boolIsValid &= $this->valProBadgePoints();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	break;
        }

        return $boolIsValid;
    }

    public function badgeGranted( $boolIsBadgeGranted ) {
    	if( false == $boolIsBadgeGranted ) {
    		$this->m_boolBadgeGranted = false;
    		$this->m_boolIsPro = false;
    	} else {
    		$this->m_boolBadgeGranted = true;
    	}
    }

    public function proGranted( $boolIsProGranted ) {
    	if( false == $boolIsProGranted ) {
    		$this->m_boolIsPro = false;
    	} else {
    		$this->m_boolBadgeGranted = true;
    		$this->m_boolIsPro = true;
    	}
    }

    public function fetchCompanyBadge( $objDatabase ) {
    	return CCompanyBadges::fetchCompanyBadgeByCompanyActivityPointIdByCid( $this->getId(), $this->getCid(), $objDatabase );
    }

    public function fetchCustomerBadgeByCustomerId( $intCustomerId, $objDatabase ) {
    	$objCompanyBadge = $this->fetchCompanyBadge( $objDatabase );
    	if( false == is_null( $objCompanyBadge ) ) {
    		return CCustomerBadges::fetchCustomerBadgeByCompanyBadgeIdByCustomerIdByCid( $objCompanyBadge->getId(), $intCustomerId, $this->getCid(), $objDatabase );
    	} else {
    		return NULL;
    	}
    }

	public function getPointsToAward() {
		$intPointsToInsert = $this->getNoBadgePoints();
		if( false != $this->m_boolBadgeGranted ) {
			if( false == $this->m_boolIsPro ) {
				$intPointsToInsert = $this->getBaseBadgePoints();
			} else {
				$intPointsToInsert = $this->getProBadgePoints();
			}
		}
		return $intPointsToInsert;
	}

	public function insertPoints( $intCompanyUserId, $objCustomer, $objDatabase ) {
		$objCustomerActivityPoint = $objCustomer->createCustomerActivityPoint( $this->getPointsToAward() );
		$objCustomerActivityPoint->setCompanyActivityPointId( $this->getId() );

		return $objCustomerActivityPoint->insert( $intCompanyUserId, $objDatabase );
	}

}
?>