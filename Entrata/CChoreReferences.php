<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChoreReferences
 * Do not add any new functions to this class.
 */

class CChoreReferences extends CBaseChoreReferences {

	public static function fetchChoreReferencesByChoreIdsByCid( $arrintChoreIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintChoreIds ) ) return NULL;
		return self::fetchChoreReferences( 'SELECT * FROM chore_references WHERE cid = ' . ( int ) $intCid . ' AND chore_id IN ( ' . implode( ',', $arrintChoreIds ) . ' )', $objClientDatabase );
	}
}
?>