<?php

class CUnitExclusionReasonType extends CBaseUnitExclusionReasonType {

	const NOT_EXCLUDED			= 1;
	const HOSPITALITY_UNIT		= 2;
	const DOWN_UNIT				= 3;
	const EMPLOYEE_UNIT			= 4;
	const MODEL_UNIT			= 5;
	const ADMIN_UNIT			= 6;
	const CORPORATE_UNIT		= 7;
	const CONSTRUCTION_UNIT		= 8;

	protected $m_boolIsMarketed;
	protected $m_boolIsReportable;
	protected $m_boolIsConsideredOccupied;
	protected $m_strCustomName;
	protected $m_intUnitExclusionReasonTypeId;
	protected $m_intPropertyId;

	public static function getUnitExclusionReasonNameByUnitExclusionReasonTypeId( $intUnitExclusionReasonTypeId ) {
		$strUnitExclusionReasonName = NULL;

		switch( $intUnitExclusionReasonTypeId ) {

			case CUnitExclusionReasonType::NOT_EXCLUDED:
				$strUnitExclusionReasonName = __( 'Not Excluded' );
				break;

			case CUnitExclusionReasonType::HOSPITALITY_UNIT:
				$strUnitExclusionReasonName = __( 'Hospitality Unit' );
				break;

			case CUnitExclusionReasonType::DOWN_UNIT:
				$strUnitExclusionReasonName = __( 'Down Unit' );
				break;

			case CUnitExclusionReasonType::EMPLOYEE_UNIT:
				$strUnitExclusionReasonName = __( 'Employee Unit' );
				break;

			case CUnitExclusionReasonType::MODEL_UNIT:
				$strUnitExclusionReasonName = __( 'Model Unit' );
				break;

			case CUnitExclusionReasonType::ADMIN_UNIT:
				$strUnitExclusionReasonName = __( 'Admin Unit' );
				break;

			case CUnitExclusionReasonType::CORPORATE_UNIT:
				$strUnitExclusionReasonName = __( 'Corporate Unit' );
				break;

			case CUnitExclusionReasonType::CONSTRUCTION_UNIT:
				$strUnitExclusionReasonName = __( 'Construction Unit' );
				break;

			default:
				// default case
				break;
		}

		return $strUnitExclusionReasonName;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['is_marketed'] ) )
			$this->m_boolIsMarketed = trim( stripcslashes( $arrmixValues['is_marketed'] ) );
		elseif( isset( $arrmixValues['is_marketed'] ) )
			$this->setIsMarketed( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_marketed'] ) : $arrmixValues['is_marketed'] );

		if( isset( $arrmixValues['is_reportable'] ) )
			$this->m_boolIsReportable = trim( stripcslashes( $arrmixValues['is_reportable'] ) );
		elseif( isset( $arrmixValues['is_reportable'] ) )
			$this->setIsReportable( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_reportable'] ) : $arrmixValues['is_reportable'] );

		if( isset( $arrmixValues['is_considered_occupied'] ) )
			$this->m_boolIsConsideredOccupied = trim( stripcslashes( $arrmixValues['is_considered_occupied'] ) );
		elseif( isset( $arrmixValues['is_considered_occupied'] ) )
			$this->setIsConsideredOccupied( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_considered_occupied'] ) : $arrmixValues['is_considered_occupied'] );

		if( isset( $arrmixValues['custom_name'] ) )
			$this->m_strCustomName = trim( stripcslashes( $arrmixValues['custom_name'] ) );
		elseif( isset( $arrmixValues['custom_name'] ) )
			$this->setCustomName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['custom_name'] ) : $arrmixValues['custom_name'] );

		if( isset( $arrmixValues['unit_exclusion_reason_type_id'] ) )
			$this->m_intUnitExclusionReasonTypeId = trim( stripcslashes( $arrmixValues['unit_exclusion_reason_type_id'] ) );
		elseif( isset( $arrmixValues['unit_exclusion_reason_type_id'] ) )
			$this->setUnitExclusionReasonTypeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['unit_exclusion_reason_type_id'] ) : $arrmixValues['unit_exclusion_reason_type_id'] );

		if( isset( $arrmixValues['property_id'] ) )
			$this->m_intPropertyId = trim( stripcslashes( $arrmixValues['property_id'] ) );
		elseif( isset( $arrmixValues['property_id'] ) )
			$this->setPropertyId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_id'] ) : $arrmixValues['property_id'] );
	}

	/**
	 * Set Functions
	 *
	 */

	public function setIsMarketed( $boolIsMarketed ) {
		$this->m_boolIsMarketed = $boolIsMarketed;
	}

	public function setIsReportable( $boolIsReportable ) {
		$this->m_boolIsReportable = CStrings::strToBool( $boolIsReportable );
	}

	public function setIsConsideredOccupied( $boolIsConsideredOccupied ) {
		$this->m_boolIsConsideredOccupied = CStrings::strToBool( $boolIsConsideredOccupied );
	}

	public function setCustomName( $strName ) {
		$this->m_strCustomName = $strName;
	}

	public function setUnitExclusionReasonTypeId( $intUnitExclusionReasonTypeId ) {
		$this->m_intUnitExclusionReasonTypeId = $intUnitExclusionReasonTypeId;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getIsMarketed() {
		return $this->m_boolIsMarketed;
	}

	public function getIsReportable() {
		return $this->m_boolIsReportable;
	}

	public function getIsConsideredOccupied() {
		return $this->m_boolIsConsideredOccupied;
	}

	public function getCustomName() {
		return $this->m_strCustomName;
	}

	public function getUnitExclusionReasonTypeId() {
		return $this->m_intUnitExclusionReasonTypeId;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

}
?>