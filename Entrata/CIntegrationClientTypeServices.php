<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationClientTypeServices
 * Do not add any new functions to this class.
 */

class CIntegrationClientTypeServices extends CBaseIntegrationClientTypeServices {

	public static function fetchDefaultIntegrationClientTypeServicesByIntegrationClientTypeId( $intIntegrationClientTypeId, $objDatabase ) {
        return self::fetchIntegrationClientTypeServices( sprintf( 'SELECT * FROM integration_client_type_services WHERE integration_client_type_id = \'%s\' ORDER BY order_num', ( int ) $intIntegrationClientTypeId ), $objDatabase );
    }

    public static function fetchDefaultIntegrationClientServicesByIntegrationClientTypeIdByIntegrationSerivceIds( $intIntegrationClientTypeId, $arrintIntegrationServiceIds, $objDatabase ) {
    	if( false == valArr( $arrintIntegrationServiceIds ) ) return NULL;
    	return self::fetchIntegrationClientTypeServices( sprintf( 'SELECT * FROM integration_client_type_services WHERE integration_client_type_id = \'%s\' AND integration_service_id IN( %s ) ORDER BY order_num', $intIntegrationClientTypeId, implode( ',', $arrintIntegrationServiceIds ) ), $objDatabase );
    }

    public static function fetchIntegrationClientTypeServiceByIntegrationClientTypeIdByIntegrationServiceId( $intIntegrationClientTypeId, $intIntegrationServiceId, $objDatabase ) {
    	return self::fetchIntegrationClientTypeService( sprintf( 'SELECT * FROM integration_client_type_services WHERE integration_client_type_id = \'%s\' AND integration_service_id = \'%s\' ORDER BY order_num', ( int ) $intIntegrationClientTypeId, ( int ) $intIntegrationServiceId ), $objDatabase );
    }

    public static function fetchDefaultAvailableIntegrationClientTypeServicesByIntegrationDatabaseIdByIntegrationClientTypeIdByCid( $intIntegrationDatabaseId, $intIntegrationClientTypeId, $intCid, $objDatabase ) {

    	$strSql = 'SELECT icts.* FROM integration_client_type_services icts
    				WHERE icts.integration_client_type_id = ' . ( int ) $intIntegrationClientTypeId . '
						AND icts.integration_service_id NOT IN ( SELECT ic.integration_service_id FROM integration_clients ic
															WHERE ic.integration_database_id = ' . ( int ) $intIntegrationDatabaseId . ' AND ic.cid = ' . ( int ) $intCid . '
															)
    					ORDER BY order_num';

    	return self::fetchIntegrationClientTypeServices( $strSql, $objDatabase );
    }

	public static function getDefaultRealPageIntegrationClientTypeServiceUrlsArrayByServiceIds( $arrintClientTypeServiceIds ) {

    	$arrmixClientTypeServiceUrls = [];

		if( true == valArr( $arrintClientTypeServiceIds ) ) {

			foreach( $arrintClientTypeServiceIds as $intClientTypeServiceId ) {

				switch( $intClientTypeServiceId ) {

					case CIntegrationService::RETRIEVE_PROPERTIES:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/GeneralServices/File.asmx?wsdl', 'name' => 'SDE - General Services - DownloadList', 'title' => 'Service URI => /GeneralServices/File.asmx?wsdl', 'method' => '' ];
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][1] = [ 'value' => '/Crossfire/prospectmanagement/service.asmx?wsdl', 'name' => 'ODE - Prospect Management - GetAllProperties', 'title' => 'Service URI => /Crossfire/prospectmanagement/service.asmx?wsdl', 'method' => '' ];
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][2] = [ 'value' => '/GeneralServices/SiteData.asmx?wsdl', 'name' => 'ODE - General Services - GetSiteList', 'title' => 'Service URI => /GeneralServices/SiteData.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::RETRIEVE_PROPERTY_UNITS:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/GeneralServices/File.asmx?wsdl', 'name' => 'SDE - General Services - DownloadList', 'title' => 'Service URI => /GeneralServices/File.asmx?wsdl', 'method' => '' ];
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][1] = [ 'value' => '/CrossFire/AvailabilityAndPricing/Unit.asmx?wsdl', 'name' => 'ODE - CrossFire AvailabilityAndPricing - List', 'title' => 'Service URI => /CrossFire/AvailabilityAndPricing/Unit.asmx?wsdl', 'method' => '' ];
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][2] = [ 'value' => '/CrossFire/prospectmanagement/service.asmx?wsdl', 'name' => 'ODE - Prospect Management - GetUnitsByProperty', 'title' => 'Service URI => /CrossFire/prospectmanagement/service.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::RETRIEVE_CUSTOMERS:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/GeneralServices/File.asmx?wsdl', 'name' => 'SDE - General Services - DownloadList', 'title' => 'Service URI => /GeneralServices/File.asmx?wsdl', 'method' => '' ];
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][1] = [ 'value' => '/residentservices/residentservice.asmx?wsdl', 'name' => 'ODE - Resident Services - GetResidentList', 'title' => 'Service URI => /residentservices/residentservice.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::RETRIEVE_CUSTOMER:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][1] = [ 'value' => '/residentservices/residentservice.asmx?wsdl', 'name' => 'ODE - Resident Services - GetResidentList', 'title' => 'Service URI => /residentservices/residentservice.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::RETRIEVE_UNIT_TYPES_AVAILABILITY_AND_PRICING:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/CrossFire/AvailabilityAndPricing/Unit.asmx?wsdl', 'name' => 'ODE - CrossFire AvailabilityAndPricing - List', 'title' => 'Service URI => /CrossFire/AvailabilityAndPricing/Unit.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::RETRIEVE_MAINTENANCE_PROBLEMS:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/Facilities/ServiceRequest.asmx?wsdl', 'name' => 'ODE - Facilities - GetServiceIssueList', 'title' => 'Service URI => /Facilities/ServiceRequest.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::RETRIEVE_LEAD_SOURCES:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/Crossfire/prospectmanagement/service.asmx?wsdl', 'name' => 'ODE - Prospect Management - GetMarketingSourcesByProperty', 'title' => 'Service URI => /Crossfire/prospectmanagement/service.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::RETRIEVE_MAINTENANCE_PRIORITIES:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/Facilities/ServiceRequest.asmx?wsdl', 'name' => 'ODE - Facilities - GetWOPriorityList', 'title' => 'Service URI => /Facilities/ServiceRequest.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::RETRIEVE_LEASING_AGENTS:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/Crossfire/prospectmanagement/service.asmx?wsdl', 'name' => 'ODE - Prospect Management - GetLeasingAgentsByProperty', 'title' => 'Service URI => /Crossfire/prospectmanagement/service.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::RETRIEVE_AR_TRANSACTIONS:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/GeneralServices/File.asmx?wsdl', 'name' => 'SDE - General Services - DownloadList', 'title' => 'Service URI => /GeneralServices/File.asmx?wsdl', 'method' => '' ];
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][1] = [ 'value' => '/residentservices/residentservice.asmx?wsdl', 'name' => 'ODE - Resident Services - GetResidentLedger', 'title' => 'Service URI => /residentservices/residentservice.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::RETURN_AR_PAYMENT:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/GeneralServices/File.asmx?wsdl', 'name' => 'SDE - General Services - TransactionsConvergent', 'title' => 'Service URI => /GeneralServices/File.asmx?wsdl', 'method' => '' ];
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][1] = [ 'value' => '/TransactionServices/ManageTransactions.asmx?wsdl', 'name' => 'ODE - Transaction Services - NonSufficientFunds', 'title' => 'Service URI => /TransactionServices/ManageTransactions.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::REVERSE_AR_PAYMENT:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/GeneralServices/File.asmx?wsdl', 'name' => 'SDE - General Services - TransactionsConvergent', 'title' => 'Service URI => /GeneralServices/File.asmx?wsdl', 'method' => '' ];
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][1] = [ 'value' => '/TransactionServices/ManageTransactions.asmx?wsdl', 'name' => 'ODE - Transaction Services - TransactionDelete', 'title' => 'Service URI => /TransactionServices/ManageTransactions.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::SEND_AR_PAYMENT:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/GeneralServices/File.asmx?wsdl', 'name' => 'SDE - General Services - TransactionsConvergent', 'title' => 'Service URI => /GeneralServices/File.asmx?wsdl', 'method' => 'TransactionsConvergent' ];
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][1] = [ 'value' => '/residentservices/residentservice.asmx?wsdl', 'name' => 'ODE - Resident Services - PostSinglePayment', 'title' => 'Service URI => /residentservices/residentservice.asmx?wsdl', 'method' => 'PostSinglePayment' ];
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][2] = [ 'value' => '/residentservices/residentservice.asmx?wsdl', 'name' => 'ODE - Resident Services - OneSiteSinglePayment', 'title' => 'Service URI => /residentservices/residentservice.asmx?wsdl', 'method' => 'OneSiteSinglePayment' ];
						break;

					case CIntegrationService::SEND_GUEST_CARD:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/Crossfire/prospectmanagement/service.asmx?wsdl', 'name' => 'ODE - Prospect Management - InsertProspect', 'title' => 'Service URI => /Crossfire/prospectmanagement/service.asmx?wsdl', 'method' => '' ];
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][1] = [ 'value' => '/CrossFire/OnlineLeasing/GuestCard_Simple.asmx?wsdl', 'name' => 'ODE - CrossFire Online Leasing - CreateGuestCards', 'title' => 'Service URI => /CrossFire/OnlineLeasing/GuestCard_Simple.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::SEND_APPLICATION:
						// internally calling sendGuestCard
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/Crossfire/prospectmanagement/service.asmx?wsdl', 'name' => 'ODE - Prospect Management - InsertProspect', 'title' => 'Service URI => /Crossfire/prospectmanagement/service.asmx?wsdl', 'method' => '' ];
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][1] = [ 'value' => '/CrossFire/OnlineLeasing/GuestCard_Simple.asmx?wsdl', 'name' => 'ODE - CrossFire Online Leasing - CreateGuestCards', 'title' => 'Service URI => /CrossFire/OnlineLeasing/GuestCard_Simple.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::UPDATE_CUSTOMER:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/residentservices/residentservice.asmx?wsdl', 'name' => 'ODE - Resident Services - UpdateOccupant', 'title' => 'Service URI => /residentservices/residentservice.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::RETRIEVE_AR_CODES:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/TransactionServices/ManageTransactions.asmx?wsdl', 'name' => 'ODE - Transaction Services - TransactionCodes', 'title' => 'Service URI => /TransactionServices/ManageTransactions.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::CLOSE_PAYMENT_BATCH:
						// internally calling sendArPayment
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/GeneralServices/File.asmx?wsdl', 'name' => 'SDE - General Services - TransactionsConvergent', 'title' => 'Service URI => /GeneralServices/File.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::SEND_MAINTENANCE_REQUEST:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/Facilities/ServiceRequest.asmx?wsdl', 'name' => 'ODE - Facilities - Add', 'title' => 'Service URI => /Facilities/ServiceRequest.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::SEND_UTILITY_TRANSACTIONS:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/GeneralServices/File.asmx?wsdl', 'name' => 'SDE - General Services - TransactionsConvergent', 'title' => 'Service URI => /GeneralServices/File.asmx?wsdl', 'method' => '' ];
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][1] = [ 'value' => '/residentservices/residentservice.asmx?wsdl', 'name' => 'ODE - Resident Services - InsertChargesIntoLedger', 'title' => 'Service URI => /residentservices/residentservice.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::RETRIEVE_CUSTOMERS_WITH_BALANCE_DUE:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/residentservices/residentservice.asmx?wsdl', 'name' => 'ODE - Resident Services - GetResidentList', 'title' => 'Service URI => /residentservices/residentservice.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::SEND_AR_TRANSACTIONS:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/residentservices/residentservice.asmx?wsdl', 'name' => 'ODE - Resident Services - PostSingleCharge', 'title' => 'Service URI => /residentservices/residentservice.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::RETRIEVE_MAINTENANCE_REQUESTS:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/Facilities/ServiceRequest.asmx?wsdl', 'name' => 'ODE - Facilities - Get', 'title' => 'Service URI => /Facilities/ServiceRequest.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::RETRIEVE_MAINTENANCE_STATUSES:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/Facilities/ServiceRequest.asmx?wsdl', 'name' => 'ODE - Facilities - GetStatusList', 'title' => 'Service URI => /Facilities/ServiceRequest.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::UPDATE_MAINTENANCE_REQUEST:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/Facilities/ServiceRequest.asmx?wsdl', 'name' => 'ODE - Facilities - Update', 'title' => 'Service URI => /Facilities/ServiceRequest.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::RETRIEVE_UNITS_OPTIMIZED_PRICING:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/CrossFire/AvailabilityAndPricing/Unit.asmx?wsdl', 'name' => 'ODE - CrossFire AvailabilityAndPricing - List', 'title' => 'Service URI => /CrossFire/AvailabilityAndPricing/Unit.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::SEND_QUOTE:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/CrossFire/Leasing/Unit.asmx?wsdl', 'name' => 'ODE - CrossFire SendQuote - List', 'title' => 'Service URI => /CrossFire/Leasing/Unit.asmx?wsdl', 'method' => '' ];
						break;

					case CIntegrationService::RETRIEVE_RESIDENT_ADDRESS:
						$arrmixClientTypeServiceUrls[$intClientTypeServiceId][0] = [ 'value' => '/residentservices/residentservice.asmx?wsdl', 'name' => 'ODE - Resident Services - Resident Address', 'title' => 'Service URI => /residentservices/residentservice.asmx?wsdl', 'method' => '' ];
						break;

					default:
						// default case
						break;
				}
			}
		}

		return $arrmixClientTypeServiceUrls;
    }

}
?>