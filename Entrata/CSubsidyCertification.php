<?php
use Psi\Eos\Entrata\CSubsidyContracts;
class CSubsidyCertification extends CBaseSubsidyCertification {

	protected $m_arrobjCustomerAssets;
	protected $m_arrobjCustomerData;
	protected $m_arrobjCustomerIncomes;

	protected $m_intLatestLeaseId; // This is used for certifications prior to a unit transfer
	protected $m_intUnitTypeId;
	private $m_intUnitSpaceId;

	protected $m_strHeadOfHouseholdName;
	protected $m_strSubsidyCertificationTypeName;

	protected $m_objBasicRecord;
	protected $m_objCustomerRecord;
	protected $m_objApplicationDetail;

	protected $m_boolIsSignatureException;
	protected $m_boolIsRentOverridden = false;
	protected $m_boolIsSecurityDepositOverridden = false;

	protected $m_intSubsidyContractTypeId;

	/** @var  \CRentCalculation */
	private $m_objRentCalculation;
	/** @var  \CRentCalculation */
	private $m_objPreviousRentCalculation;
	private $m_objLease;
	private $m_objApplication;

	private $m_boolNeedDependentUpdates = true;

	private $m_boolIsCreatedByGrossRentChange;

	private $m_strLeaseStatusType;

	// If there are multiple certifications per lease, this number is gives the position in the sequence
	private $m_intSequencePosition;

	private $m_fltPassbookRatePercent;
	private $m_strCaseRef;
	private $m_boolTruncateCharges;
	private $m_boolAddNewCharges;
	private $m_boolRewindCharges;
	private $m_strUnitNumberCache;

	private $m_objSetAside;
	private $m_objSubsidyContract;
	private $m_boolIsLastCertification;

	public static $c_intFirstDayOfMonth = 1;

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );

		if( true == isset( $arrmixValues['customer_data'] ) ) 				$this->setCustomerData( $arrmixValues['customer_data'] );
		if( true == isset( $arrmixValues['is_signature_exception'] ) )		$this->setIsSignatureException( $arrmixValues['is_signature_exception'] );
		if( true == isset( $arrmixValues['customer_assets'] ) ) 			$this->setCustomerAssets( $arrmixValues['customer_assets'] );
		if( true == isset( $arrmixValues['customer_incomes'] ) ) 			$this->setCustomerIncomes( $arrmixValues['customer_incomes'] );
		if( true == isset( $arrmixValues['basic_record'] ) ) 				$this->setBasicRecord( $arrmixValues['basic_record'] );
		if( true == isset( $arrmixValues['customer_record'] ) ) 			$this->setCustomerRecord( $arrmixValues['customer_record'] );
		if( true == isset( $arrmixValues['application_detail'] ) ) 			$this->setApplicationDetail( $arrmixValues['application_detail'] );
		if( true == isset( $arrmixValues['subsidy_contract_type_id'] ) ) 	$this->setSubsidyContractTypeId( $arrmixValues['subsidy_contract_type_id'] );
		if( true == isset( $arrmixValues['head_of_household_name'] ) ) 		$this->setHeadOfHouseholdName( $arrmixValues['head_of_household_name'] );
		if( true == isset( $arrmixValues['subsidy_certification_type_name'] ) )	$this->setSubsidyCertificationTypeName( $arrmixValues['subsidy_certification_type_name'] );
		if( true == isset( $arrmixValues['latest_lease_id'] ) ) 			$this->setLatestLeaseId( $arrmixValues['latest_lease_id'] );
		if( true == isset( $arrmixValues['unit_type_id'] ) ) 				$this->setUnitTypeId( $arrmixValues['unit_type_id'] );
		if( true == isset( $arrmixValues['sequence_position'] ) ) 			$this->setSequencePosition( $arrmixValues['sequence_position'] );
		if( true == isset( $arrmixValues['passbook_rate_percent'] ) ) 		$this->setPassbookRatePercent( $arrmixValues['passbook_rate_percent'] );
		if( true == isset( $arrmixValues['lease_status_type'] ) ) 		    $this->setLeaseStatusType( $arrmixValues['lease_status_type'] );
		if( true == isset( $arrmixValues['unit_number_cache'] ) ) 		    $this->setUnitNumberCache( $arrmixValues['unit_number_cache'] );
		if( true == isset( $arrmixValues['is_last_certification'] ) )       $this->setIsLastCertification( CStrings::strToBool( $arrmixValues['is_last_certification'] ) );
		// Overriding mat_data for stripslashes, it's removing slashes inside the JSON object data
		if( true == isset( $arrmixValues['mat_data'] ) )                    $this->setMatData( trim( $arrmixValues['mat_data'] ) );
		return;
	}

	public function setCustomerRecord( $objCustomerRecord ) {
		$this->m_objCustomerRecord = $objCustomerRecord;
	}

	public function setBasicRecord( $objBasicRecord ) {
		$this->m_objBasicRecord = $objBasicRecord;
	}

	public function setCustomerData( $arrobjCustomerData ) {
		$this->m_arrobjCustomerData = $arrobjCustomerData;
	}

	public function setCustomerAssets( $arrobjCustomerAssets ) {
		$this->m_arrobjCustomerAssets = $arrobjCustomerAssets;
	}

	public function setCustomerIncomes( $arrobjCustomerIncomes ) {
		$this->m_arrobjCustomerIncomes = $arrobjCustomerIncomes;
	}

	public function setIsRentOverriden( $boolIsRentOverriden ) {
		$this->m_boolIsRentOverridden = $boolIsRentOverriden;
	}

	public function setIsSecurityDepositOverriden( $boolIsSecurityDepositOverridden ) {
		$this->m_boolIsSecurityDepositOverridden = $boolIsSecurityDepositOverridden;
	}

	public function setIsSignatureException( $boolIsSignatureException = false ) {
		$this->m_boolIsSignatureException = $boolIsSignatureException;
	}

	public function setSubsidyContractTypeId( $intSubsidyContractTypeId ) {
		$this->m_intSubsidyContractTypeId = $intSubsidyContractTypeId;
	}

	public function setHeadOfHouseholdName( $strHeadOfHouseholdName ) {
		$this->m_strHeadOfHouseholdName = $strHeadOfHouseholdName;
	}

	public function setSubsidyCertificationTypeName( $strSubsidyCertificationTypeName ) {
		$this->m_strSubsidyCertificationTypeName = $strSubsidyCertificationTypeName;
	}

	public function setLatestLeaseId( $intLatestLeaseId ) {
		$this->m_intLatestLeaseId = $intLatestLeaseId;
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->m_intUnitTypeId = $intUnitTypeId;
	}

	public function setRentCalculation( CRentCalculation $objRentCalculation ) {
		$this->m_objRentCalculation = $objRentCalculation;
	}

	public function setPreviousRentCalculation( CRentCalculation $objPreviousRentCalculation ) {
		$this->m_objPreviousRentCalculation = $objPreviousRentCalculation;
	}

	public function setSequencePosition( $intSequencePosition ) {
		$this->m_intSequencePosition = $intSequencePosition;
	}

	public function setApplication( $objApplication ) {
		$this->m_objApplication = $objApplication;
	}

	public function setNeedDependentUpdates( $boolNeedDependentUpdates ) {
		$this->m_boolNeedDependentUpdates = $boolNeedDependentUpdates;
	}

	public function setPassbookRatePercent( $fltPassbookRatePercent ) {
		$this->m_fltPassbookRatePercent = $fltPassbookRatePercent;
	}

	public function setLease( $objLease ) {
		$this->m_objLease = $objLease;
	}

	public function setIsCreatedByGrossRentChange( $boolIsCreatedByGrossRentChange ) {
		$this->m_boolIsCreatedByGrossRentChange = $boolIsCreatedByGrossRentChange;
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->m_intUnitSpaceId = $intUnitSpaceId;
	}

	public function setApplicationDetail( $objApplicationDetail ) {
		$this->m_objApplicationDetail = $objApplicationDetail;
	}

	/**
	 * get Functions
	 *
	 */

	public function getCustomerRecord() {
		return $this->m_objCustomerRecord;
	}

	public function getBasicRecord() {
		return $this->m_objBasicRecord;
	}

	public function getCustomerData() {
		return $this->m_arrobjCustomerData;
	}

	public function getCustomerAssets() {
		return $this->m_arrobjCustomerAssets;
	}

	public function getCustomerIncomes() {
		return $this->m_arrobjCustomerIncomes;
	}

	public function getIsRentOverriden() {
		return $this->m_boolIsRentOverridden;
	}

	public function getIsSecurityDepositOverriden() {
		return  $this->m_boolIsSecurityDepositOverridden;
	}

	public function getIsSignatureException() {
		return $this->m_boolIsSignatureException;
	}

	public function getSubsidyContractTypeId() {
		return $this->m_intSubsidyContractTypeId;
	}

	public function getHeadOfHouseholdName() {
		return $this->m_strHeadOfHouseholdName;
	}

	public function getSubsidyCertificationTypeName() {
		if( true == isset( $this->m_strSubsidyCertificationTypeName ) ) {
			return $this->m_strSubsidyCertificationTypeName;
		} else {
			return CSubsidyCertificationType::getTypeNameByTypeId( $this->getSubsidyCertificationTypeId() );
		}

	}

	public function getLatestLeaseId() {
		return $this->m_intLatestLeaseId;
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function getRentCalculation() {
		return $this->m_objRentCalculation;
	}

	public function getPreviousRentCalculation() {
		return $this->m_objPreviousRentCalculation;
	}

	public function getSequencePosition() {
		return $this->m_intSequencePosition;
	}

	public function getApplication() {
		return $this->m_objApplication;
	}

	public function getNeedDependentUpdates() {
		return $this->m_boolNeedDependentUpdates;
	}

	public function getPassbookRatePercent() {
		return $this->m_fltPassbookRatePercent;
	}

	public function getLease() {
		return $this->m_objLease;
	}

	public function getIsCreatedByGrossRentChange() {
		return $this->m_boolIsCreatedByGrossRentChange;
	}

	public function getApplicationDetail() {
		return $this->m_objApplicationDetail;
	}

	/**
	 * @return mixed
	 */
	public function getLeaseStatusType() {
		return $this->m_strLeaseStatusType;
	}

	/**
	 * @param mixed $strLeaseStatusType
	 */
	public function setLeaseStatusType( $strLeaseStatusType ) {
		$this->m_strLeaseStatusType = $strLeaseStatusType;
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyContractId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSetAsideId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrimaryCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyCertificationTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidySubCertificationTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyCertificationStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyCertificationReasonTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSubsidyCertificationReasonTypeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_certification_reason_type', 'Please select a termination reason.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valSubsidyIncomeLimitVersionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyCertificationCorrectionTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getSubsidyCertificationCorrectionTypeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_correction_type_id', 'Please select a correction reason for current certification ID:' . $this->getId() ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valSubsidySignatureExceptionTypeId() {
		$boolIsValid = true;

		if( true == $this->getIsSignatureException() && true == is_null( $this->getSubsidySignatureExceptionTypeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_signature_exception_type_id', 'Exception reason is required.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valCorrectedSubsidyCertificationId() {
		$boolIsValid = true;

		if( false == valId( $this->getCorrectedSubsidyCertificationId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'corrected_subsidy_certification_id', 'Unable to associate corrected certification with the current certification ID:' . $this->getId() ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valResultOfSubsidyCertificationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyGrossRentChangeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHmfaCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMatData() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRentOverrideAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validateReQualifyHouseholdCertificationEffectiveDate() {
		$boolIsValid = true;

		$arrmixSubsidyCertificationDates    = ( array ) \Psi\Eos\Entrata\CSubsidyCertifications::createService()->fetchAllSubsidyCertificationsDateSpansExcludingGRCAndCorrectedByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $this->getDatabase() );

		if( true == valArr( $arrmixSubsidyCertificationDates ) ) {
			$strMinEffectiveDate = reset( $arrmixSubsidyCertificationDates['effective_date'] );
			$strMaxEffectiveDate = end( $arrmixSubsidyCertificationDates['effective_through_date'] );

			if( true == in_array( date( 'm/d/Y', strtotime( $this->getEffectiveDate() ) ), $arrmixSubsidyCertificationDates['effective_date'] ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', 'A certification with the effective date ' . date( 'm/d/Y', strtotime( $this->getEffectiveDate() ) ) . ' already exists on this lease. Please correct the existing certification to make these changes.' ) );
			} elseif( new DateTime( $this->getEffectiveDate() ) <= new DateTime( $strMinEffectiveDate ) || new DateTime( $this->getEffectiveDate() ) >= new DateTime( $strMaxEffectiveDate ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', 'Please select the effective date in between ' . $strMinEffectiveDate . ' and ' . $strMaxEffectiveDate . ' to requalify the household.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validateInterimCertificationEffectiveDate() {
		$boolIsValid = true;

		if( self::$c_intFirstDayOfMonth != date( 'd', strtotime( $this->getEffectiveDate() ) ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', 'Interim recertification effective date should be 1st day of month.' ) );
		}

		$arrmixSubsidyCertificationDates    = ( array ) \Psi\Eos\Entrata\CSubsidyCertifications::createService()->fetchAllSubsidyCertificationsDateSpansExcludingGRCAndCorrectedByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $this->getDatabase() );
		if( true == in_array( date( 'm/d/Y', strtotime( $this->getEffectiveDate() ) ), $arrmixSubsidyCertificationDates['effective_date'] ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', 'Interim recertification with the effective date ' . date( 'm/d/Y', strtotime( $this->getEffectiveDate() ) ) . ' already exists on this lease. Please correct the existing certification to make these changes.' ) );
		}
		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getEffectiveDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', 'Please enter valid effective date.' ) );
			$boolIsValid &= false;
		} elseif( $this->getSubsidyCertificationTypeId() == CSubsidyCertificationType::INTERIM_RECERTIFICATION ) {
			if( true == $this->isTaxCreditSubsidyCertification() ) {
				$boolIsValid &= $this->validateReQualifyHouseholdCertificationEffectiveDate();
			} else {
				$boolIsValid &= $this->validateInterimCertificationEffectiveDate();
			}
		} elseif( $this->getSubsidyCertificationTypeId() == CSubsidyCertificationType::TERMINATION && false == is_null( $this->getEffectiveDate() ) && new DateTime( $this->getEffectiveDate() ) > new DateTime() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', 'Future Termination date is not allowed.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valEffectiveThroughDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessedDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDatePrimaryApplicantSigned() {
		$boolIsValid = true;

		$mixValidatedStartDate = CValidation::checkISODateFormat( $this->getDatePrimaryApplicantSigned(), $boolFormat = true );
		$strSignatureName = ( true == $this->getIsSignatureException() ) ? 'Anticipated ' : 'Tenant';
		if( true == is_null( $this->m_strDatePrimaryApplicantSigned ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_primary_applicant_signed',  $strSignatureName . ' signature date is required.' ) );
			return false;
		} elseif( false === $mixValidatedStartDate ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_primary_applicant_signed',  $strSignatureName . ' signature date must be in mm/dd/yyyy format.' ) );
			$boolIsValid = false;
		} else {
			$this->setDatePrimaryApplicantSigned( date( 'm/d/Y', strtotime( $mixValidatedStartDate ) ) );
		}

		return $boolIsValid;
	}

	public function valDateAgentSigned() {

		$boolIsValid = true;
		$mixValidatedStartDate = CValidation::checkISODateFormat( $this->getDateAgentSigned(), $boolFormat = true );

		if( true == is_null( $this->m_strDateAgentSigned ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_agent_signed', 'Owner/Agent signature date is required.' ) );
			return false;
		} elseif( false === $mixValidatedStartDate ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_agent_signed', 'Owner/Agent signature date must be in mm/dd/yyyy format.' ) );
			$boolIsValid = false;
		} else {
			$this->setDateAgentSigned( date( 'm/d/Y', strtotime( $mixValidatedStartDate ) ) );
		}

		return $boolIsValid;
	}

	public function valAnticipatedVoucherDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFirstVoucherDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsTrial() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsHistorical() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsResultOfEiv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEligibilityCheckNotRequired() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasBeenCorrected() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyCertificationDates() {
		$boolIsValid = true;

		if( false == CValidation::isValidDate( $this->getEffectiveDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', 'A valid effective date is required.' ) );
		}

		if( false == CValidation::isValidDate( $this->getEffectiveThroughDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_through_date', 'A valid effective through date is required.' ) );
		}

		if( true == CValidation::isValidDate( $this->getEffectiveDate() ) && true == CValidation::isValidDate( $this->getEffectiveThroughDate() ) ) {
			if( strtotime( $this->getEffectiveThroughDate() ) <= strtotime( $this->getEffectiveDate() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'certification_dates', 'Effective through date cannot be less than or equal to effective date.' ) );
				return false;
			}

			// Validate overlapping certifications
			$arrobjSubsidyCertifications = \Psi\Eos\Entrata\CSubsidyCertifications::createService()->fetchSubsidyCertificationsByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $this->getDatabase(), false );
			foreach( $arrobjSubsidyCertifications as $intSubsidyCertificationId => $objSubsidyCertification ) {
				if( $this->getId() != $intSubsidyCertificationId
				&& ( ( strtotime( $this->getEffectiveDate() ) >= strtotime( $objSubsidyCertification->getEffectiveDate() ) && strtotime( $this->getEffectiveDate() ) <= strtotime( $objSubsidyCertification->getEffectiveThroughDate() ) )
				|| ( strtotime( $this->getEffectiveThroughDate() ) >= strtotime( $objSubsidyCertification->getEffectiveDate() ) && strtotime( $this->getEffectiveThroughDate() ) <= strtotime( $objSubsidyCertification->getEffectiveThroughDate() ) ) ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'certification_dates_overlap', 'Certification dates cannot overlap.' ) );
					return false;
				} elseif( $this->getId() != $intSubsidyCertificationId
				&& ( ( strtotime( $objSubsidyCertification->getEffectiveDate() ) >= strtotime( $this->getEffectiveDate() ) && strtotime( $objSubsidyCertification->getEffectiveDate() ) <= strtotime( $this->getEffectiveThroughDate() ) )
				|| ( strtotime( $objSubsidyCertification->getEffectiveThroughDate() ) >= strtotime( $this->getEffectiveDate() ) && strtotime( $objSubsidyCertification->getEffectiveThroughDate() ) <= strtotime( $this->getEffectiveThroughDate() ) ) ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'certification_dates_overlap', 'Certification dates cannot overlap.' ) );
					return false;
				}
			}

			// Validate sequence of certifications
			$arrintOldSubsidyCertificationIds = array_keys( $arrobjSubsidyCertifications );
			$arrobjSubsidyCertifications[$this->getId()]->setEffectiveDate( $this->getEffectiveDate() );
			$arrobjSubsidyCertifications[$this->getId()]->setEffectiveThroughDate( $this->getEffectiveThroughDate() );
			usort( $arrobjSubsidyCertifications, function ( $objSubsidyCertificationOne, $objSubsidyCertificationTwo ) {
				return strtotime( $objSubsidyCertificationOne->getEffectiveDate() ) < strtotime( $objSubsidyCertificationTwo->getEffectiveDate() );
			} );
			$arrobjSubsidyCertifications      = rekeyObjects( 'Id', $arrobjSubsidyCertifications );
			$arrintNewSubsidyCertificationIds = array_keys( $arrobjSubsidyCertifications );
			if( $arrintOldSubsidyCertificationIds !== $arrintNewSubsidyCertificationIds ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'certification_dates_overlap', 'Sequence of certifications cannot change.' ) );
				return false;
			}
		}

		return $boolIsValid;
	}

	public function valVoucherDate() {

		if( false == valStr( $this->getAnticipatedVoucherDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'voucher_date', 'Voucher date is required.' ) );
			return false;
		}

		$this->setAnticipatedVoucherDate( date( 'm/01/Y', strtotime( $this->getAnticipatedVoucherDate() ) ) );

		if( false == CValidation::isValidDate( $this->getAnticipatedVoucherDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'voucher_date', 'Voucher date is invalid.' ) );
			return false;
		}

		if( ( date( 'm/01/Y', strtotime( $this->getAnticipatedVoucherDate() ) ) == date( 'm/01/Y', strtotime( $this->getEffectiveDate() ) ) ) || ( date( 'm/01/Y', strtotime( $this->getAnticipatedVoucherDate() ) ) == date( 'm/01/Y', strtotime( $this->getEffectiveThroughDate() ) ) ) ) {
			return true;
		}

		if( strtotime( date( 'm/01/Y', strtotime( $this->getAnticipatedVoucherDate() ) ) ) < strtotime( date( 'm/01/Y', strtotime( $this->getEffectiveDate() ) ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'voucher_date', 'Voucher date cannot be less than effective date.' ) );
			return false;
		}

		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_lease_signature':
				$boolIsValid &= $this->valDateAgentSigned();
				$boolIsValid &= $this->valSubsidySignatureExceptionTypeId();
				$boolIsValid &= $this->valDatePrimaryApplicantSigned();
				break;

			case 'validate_termination':
				$boolIsValid &= $this->valEffectiveDate();
				$boolIsValid &= $this->valSubsidyCertificationReasonTypeId();
				break;

			case 'validate_correction':
				$boolIsValid &= $this->valSubsidyCertificationCorrectionTypeId();
				$boolIsValid &= $this->valCorrectedSubsidyCertificationId();
				break;

			case 'validate_subsidy_certification_dates':
				$boolIsValid &= $this->valSubsidyCertificationDates();
				break;

			case 'validate_voucher_date':
				$boolIsValid &= $this->valVoucherDate();
				break;

			case 'validate_subsidy_certification_data':
				$boolIsValid &= $this->valEffectiveDate();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	// Other functions

	public function fetchRecertificationNoticeByFileTypeId( $intFileTypeId, $objDatabase ) {
		return CFiles::fetchFileBySubsidyCertificationIdByFileTypeIdByCid( $this->getId(), $intFileTypeId, $this->getCid(), $objDatabase );
	}

	public function getOrFetchLease( $objDatabase ) {
		if( true == valObj( $this->getLease(), \CLease::class ) ) {
			$objLease = $this->getLease();
		} else {
			$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCidBySchema( $this->getLeaseId(), $this->getCid(), $objDatabase, $boolFetchFromView = false );
			$this->setLease( $objLease );
		}

		return $objLease;
	}

	public function getOrFetchApplication( $objDatabase ) {
		if( true == valObj( $this->getApplication(), \CApplication::class ) ) {
			$objApplication = $this->getApplication();
		} else {
			$objApplication = CApplications::fetchApplicationByIdByCid( $this->getApplicationId(), $this->getCid(), $objDatabase );
			$this->setApplication( $objApplication );
		}
		return $objApplication;
	}

	public function getOrFetchSubsidyContract( CDatabase $objDatabase ) {
		if( true == valObj( $this->getSubsidyContract(), \CSubsidyContract::class ) ) {
			$objSubsidyContract = $this->getSubsidyContract();
		} else {
			$objSubsidyContract = CSubsidyContracts::createService()->fetchSubsidyContractByIdByCid( $this->getSubsidyContractId(), $this->getCid(), $objDatabase );
			$this->setSubsidyContract( $objSubsidyContract );
		}
		return $objSubsidyContract;
	}

	public function getOrFetchSetAside( CDatabase $objDatabase ) {
		if( true == valObj( $this->getSetAside(), \CSetAside::class ) ) {
			$objSetAside = $this->getSetAside();
		} else {
			$objSetAside = CSetAsides::fetchSetAsideByIdByCid( $this->getSetAsideId(), $this->getCid(), $objDatabase );
			$this->setSetAside( $objSetAside );
		}
		return $objSetAside;
	}

	/**
	 * @return array
	 */

	public function getApplicableSubsidyTypeIds() {

		$arrintApplicableSubsidyTypeIds = [];
		$arrobjPropertySubsidyTypes = rekeyObjects( 'SubsidyTypeId', ( array ) \Psi\Eos\Entrata\CPropertySubsidyTypes::createService()->fetchActivePropertySubsidyTypesByPropertyIdByCId( $this->getPropertyId(), $this->getCid(), $this->m_objDatabase ) );

		if( true == array_key_exists( CSubsidyType::HUD, $arrobjPropertySubsidyTypes ) && true == valId( $this->getSubsidyContractId() ) ) {
			$arrintApplicableSubsidyTypeIds[CSubsidyType::HUD] = CSubsidyType::HUD;
		}

		if( true == array_key_exists( CSubsidyType::TAX_CREDIT, $arrobjPropertySubsidyTypes ) && true == valId( $this->getSetAsideId() ) ) {
			$arrintApplicableSubsidyTypeIds[CSubsidyType::TAX_CREDIT] = CSubsidyType::TAX_CREDIT;
		}

		return $arrintApplicableSubsidyTypeIds;

	}

	public function getCertificationStatusTypeName() {
		return CSubsidyCertificationStatusType::getTypeNameByTypeId( $this->getSubsidyCertificationStatusTypeId() );
	}

	public function toArray() {

		$arrmixAdditionalFields = [
			'customer_data'                   => $this->getCustomerData(),
			'is_signature_exception'          => $this->getIsSignatureException(),
			'customer_assets'                 => $this->getCustomerAssets(),
			'customer_incomes'                => $this->getCustomerIncomes(),
			'basic_record'                    => $this->getBasicRecord(),
			'customer_record'                 => $this->getCustomerRecord(),
			'subsidy_contract_type_id'        => $this->getSubsidyContractTypeId(),
			'head_of_household_name'          => $this->getHeadOfHouseholdName(),
			'subsidy_certification_type_name' => $this->getSubsidyCertificationTypeName(),
			'latest_lease_id'                 => $this->getLatestLeaseId(),
			'unit_type_id'                    => $this->getUnitTypeId(),
			'sequence_position'               => $this->getSequencePosition(),
			'passbook_rate_percent'           => $this->getPassbookRatePercent(),
			'case_ref'                        => $this->getCaseRef(),
			'need_dependent_updates'          => $this->getNeedDependentUpdates()
		];

		return array_merge( parent::toArray(), $arrmixAdditionalFields );
	}

	public function setCaseRef( $strCaseRef ) {
		$this->m_strCaseRef = $strCaseRef;
	}

	public function getCaseRef() {
		return $this->m_strCaseRef;
	}

	public function isDocumentGenerationRequired( $intSubsidyTypeId ) {
		switch( $intSubsidyTypeId ) {
			case CSubsidyType::HUD:
				return ( true == valId( $this->getSubsidyContractId() ) );
				break;

			case CSubsidyType::TAX_CREDIT:
				return ( true == valId( $this->getSetAsideId() ) && true == in_array( $this->getSubsidyCertificationTypeId(), CSubsidyCertificationType::$c_arrintLayeredSitesCertificationTypeIds ) );
				break;

			default:
				// do something
				break;
		}

		return false;
	}

	public function showDownloadOrReprintDocumentButton() {
		return ( true == $this->isDocumentGenerationRequired( CSubsidyType::HUD ) || true == $this->isDocumentGenerationRequired( CSubsidyType::TAX_CREDIT ) );
	}

	public function isCorrectOptionRequired() {
		return ( false == in_array( $this->getSubsidyCertificationTypeId(), CSubsidyCertificationType::$c_arrintPartialSubsidyCertificationTypeIds ) && false == $this->getHasBeenCorrected()
		         && ( ( true == valId( $this->getSubsidyContractId() && true == in_array( $this->getSubsidyCertificationStatusTypeId(), CSubsidyCertificationStatusType::$c_arrintHudCorrectionSubsidyCertificationStatusTypeIds ) ) )
		              || ( true == is_null( $this->getSubsidyContractId() ) && CSubsidyCertificationStatusType::FINALIZED == $this->getSubsidyCertificationStatusTypeId() ) ) );
	}

	public function isTaxCreditSubsidyCertification() : bool {
		if( NULL == $this->getSubsidyContractId() && true == valId( $this->getSetAsideId() ) ) {
			return true;
		}
		return false;
	}

	public function isAllowChangeToHouseholdComposition( $objDatabase ) {
		$objPropertySubsidyDetails              = \Psi\Eos\Entrata\CPropertySubsidyDetails::createService()->fetchPropertySubsidyDetailByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		if( true == $this->isTaxCreditSubsidyCertification() && false == $objPropertySubsidyDetails->getTaxCreditHouseholdCompositionChangeAllowed() ) {
			$objLease                           = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );
			$boolIsMoveinWithinSixMonthOfMovein = ( valObj( $objLease, CLease::class ) && strtotime( __( '{%t,0,DATE_NUMERIC_STANDARD}', [ $this->getEffectiveDate() ] ) ) >= strtotime( $objLease->getMoveInDate() ) && strtotime( __( '{%t,0,DATE_NUMERIC_STANDARD}', [ $this->getEffectiveDate() ] ) ) <= strtotime( date( 'm/d/Y ', strtotime( $objLease->getMoveInDate() . '+6 MONTH' ) ) ) ) ? true : false;
			return true != $boolIsMoveinWithinSixMonthOfMovein;
		}
		return true;
	}

	public function showActionButtons( $boolIsPsiUser = false ) {
		return ( ( true == in_array( $this->getSubsidyCertificationStatusTypeId(), CSubsidyCertificationStatusType::$c_arrintCurrentSubsidyCertificationStatusTypeIds ) )
		         && ( true == $this->isCorrectOptionRequired() || true == $this->showDownloadOrReprintDocumentButton() || ( true == $boolIsPsiUser && true == valId( $this->getSubsidyContractId() ) && true == in_array( $this->getSubsidyCertificationStatusTypeId(), [ CSubsidyCertificationStatusType::FINALIZED, CSubsidyCertificationStatusType::TRANSMITTED, CSubsidyCertificationStatusType::UNHANDLED_EXCEPTIONS ] ) ) ) );
	}

	public function showMoreActionButtons() {

		// For displaying option => Correction
		if( true == $this->isCorrectOptionRequired() ) return true;

		// For Edit Dates in certifications tab.
		if( false == valId( $this->getSubsidyContractId() ) && true == valId( $this->getSetAsideId() ) ) return true;

		// For displaying options on certifications tab for HUD leases => Mark As Transmitted, Mark As Finalized, Mark As Acknowledged, Resend
		if( true == valId( $this->getSubsidyContractId() ) && true == in_array( $this->getSubsidyCertificationStatusTypeId(), CSubsidyCertificationStatusType::$c_arrintCurrentSubsidyCertificationStatusTypeIds ) ) return true;

		return false;
	}

	public function allowEffectiveDateToEdit() {

		$boolAllowEffectiveDateToEdit = true;
		if( true == in_array( $this->getSubsidyCertificationTypeId(), array( CSubsidyCertificationType::MOVE_IN, CSubsidyCertificationType::INITIAL_CERTIFICATION ) ) && true == valId( $this->getCorrectedSubsidyCertificationId() ) ) {
			$intSubsidyCertificationCount = \Psi\Eos\Entrata\CSubsidyCertifications::createService()->fetchCountOfSubsidyCertificationsByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $this->getDatabase() );
			if( 1 <= $intSubsidyCertificationCount ) {
				$boolAllowEffectiveDateToEdit = false;
			}
		} elseif( $this->getSubsidyCertificationTypeId() == CSubsidyCertificationType::INTERIM_RECERTIFICATION && true == $this->isTaxCreditSubsidyCertification() ) {
			return false;
		}
		return $boolAllowEffectiveDateToEdit;
	}

	public function logActivity( $intEventSubTypeId, $objCompanyUser = NULL, $objOldSubsidyCertification = NULL, $objDatabase, $intLeaseIntervalId = NULL ) {
		$objEventLibrary 			= new CEventLibrary();
		$objEventLibraryDataObject 	= $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $objDatabase );

		$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::SUBSIDY_CERTIFICATION_UPDATED, $intEventSubTypeId );
		$objEvent->setCid( $this->getCid() );
		$objEvent->setPropertyId( $this->getPropertyId() );
		$objEvent->setLeaseIntervalId( $intLeaseIntervalId );

		if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			$objEvent->setCompanyUser( $objCompanyUser );
			$intUserId = $objCompanyUser->getId();

			if( false == is_null( $objCompanyUser->getCompanyEmployeeId() ) ) {
				$objCompanyEmployee = \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchCompanyEmployeeByIdByCid( $objCompanyUser->getCompanyEmployeeId(), $this->getCid(), $objDatabase );
				if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
					$objEvent->setCompanyEmployee( $objCompanyEmployee );
				}
			}
		} else {
			$intUserId = SYSTEM_USER_ID;
			$objEvent->setCompanyUser( NULL );
		}

		$objEvent->setEventDatetime( 'NOW()' );

		if( false == is_null( $this->getPrimaryCustomerId() ) ) {
			$objEvent->setCustomerId( $this->getPrimaryCustomerId() );
		} else {
			$objEvent->setCustomerId( NULL );
		}

		if( false == is_null( $this->getLeaseId() ) ) {
			$objEvent->setLeaseId( $this->getLeaseId() );
		}

		$objEvent->setDataReferenceId( $this->getId() );
		$objEvent->setReference( $this );

		$objEvent->setOldReferenceData( $objOldSubsidyCertification );
		$objEventLibrary->buildEventDescription( $this );

		if( false == $objEventLibrary->insertEvent( $intUserId, $objDatabase ) ) {
			$this->addErrorMsgs( $objEventLibrary->getErrorMsgs() );
			$objDatabase->rollback();
			return false;
		}
		return true;
	}

	/**
	 * @return mixed
	 */
	public function getTruncateCharges() {
		return $this->m_boolTruncateCharges;
	}

	/**
	 * @param mixed $boolTruncateCharges
	 */
	public function setTruncateCharges( $boolTruncateCharges ) {
		$this->m_boolTruncateCharges = $boolTruncateCharges;
	}

	/**
	 * @return mixed
	 */
	public function getAddNewCharges() {
		return $this->m_boolAddNewCharges;
	}

	/**
	 * @param mixed $boolAddNewCharges
	 */
	public function setAddNewCharges( $boolAddNewCharges ) {
		$this->m_boolAddNewCharges = $boolAddNewCharges;
	}

	/**
	 * @return mixed
	 */
	public function getRewindCharges() {
		return $this->m_boolRewindCharges;
	}

	/**
	 * @param mixed $boolRewindCharges
	 */
	public function setRewindCharges( $boolRewindCharges ) {
		$this->m_boolRewindCharges = $boolRewindCharges;
	}

	/**
	 * @return mixed
	 */
	public function getSetAside() {
		return $this->m_objSetAside;
	}

	/**
	 * @param mixed $objSetAside
	 */
	public function setSetAside( $objSetAside ) {
		$this->m_objSetAside = $objSetAside;
	}

	/**
	 * @return mixed
	 */
	public function getSubsidyContract() {
		return $this->m_objSubsidyContract;
	}

	/**
	 * @param mixed $objSubsidyContract
	 */
	public function setSubsidyContract( $objSubsidyContract ) {
		$this->m_objSubsidyContract = $objSubsidyContract;
	}

	/**
	 * @return mixed
	 */
	public function getUnitNumberCache() {
		return $this->m_strUnitNumberCache;
	}

	/**
	 * @param mixed $strUnitNumberCache
	 */
	public function setUnitNumberCache( $strUnitNumberCache ) {
		$this->m_strUnitNumberCache = $strUnitNumberCache;
	}

	/**
	 * @return mixed
	 */
	public function getIsLastCertification() {
		return $this->m_boolIsLastCertification;
	}

	/**
	 * @param mixed $boolIsLastCertification
	 */
	public function setIsLastCertification( $boolIsLastCertification ) {
		$this->m_boolIsLastCertification = $boolIsLastCertification;
	}

	public function setProjectMoveInDate( $strProjectMoveInDate ) {
		$this->setDetailsField( [ 'subsidy', 'project_move_in_date' ], $strProjectMoveInDate );
	}

	public function getProjectMoveInDate() {
		return $this->getDetailsField( [ 'subsidy', 'project_move_in_date' ] );
	}

	public function setMatDataField( $strFieldName, $strValue, $strSection = NULL ) {
		$arrmixMatData                = json_decode( $this->getMatdata(), true );
		if( false == is_null( $strSection ) ) {
			$arrmixMatData[$strSection][$strFieldName] = $strValue;
		} else {
			$arrmixMatData[$strFieldName] = $strValue;
		}
		$this->setMatData( json_encode( $arrmixMatData ) );

	}

	public function setMatDataProjectMoveInDate( $strProjectMoveInDate ) {
		$arrmixMatData = json_decode( $this->getMatdata(), true );
		$arrmixMatData['Section2']['project_move_in_date'] = $strProjectMoveInDate;
		$this->setMatData( json_encode( $arrmixMatData ) );

	}

	// TODO: Entire Anticipated Voucher date calculation logic ( which is distributed right now ) should be moved inside this setter method.

	public function setAnticipatedVoucherDate( $strAnticipatedVoucherDate, $boolIsOverrideRequired = false, $objDatabase = NULL ) {
		if( true == $boolIsOverrideRequired && ( true == valId( $this->getCorrectedSubsidyCertificationId() ) || true == valObj( $objDatabase, 'CDatabase' ) ) ) {
			$objOpenSubsidyHapRequest  = \Psi\Eos\Entrata\CSubsidyHapRequests::createService()->fetchSubsidyHapRequestsBySubsidyContractIdByVoucherDateByPropertyIdByCid( $this->getSubsidyContractId(), $strAnticipatedVoucherDate, $this->getPropertyId(), $this->getCid(), $objDatabase );
			$strAnticipatedVoucherDate = ( true == valObj( $objOpenSubsidyHapRequest, CSubsidyHapRequest::class ) ) ? date( 'm/01/Y', strtotime( $objOpenSubsidyHapRequest->getVoucherDate() ) ) : $strAnticipatedVoucherDate;
		}

		parent::setAnticipatedVoucherDate( $strAnticipatedVoucherDate );
	}

}
?>