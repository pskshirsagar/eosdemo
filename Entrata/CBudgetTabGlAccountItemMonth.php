<?php

class CBudgetTabGlAccountItemMonth extends CBaseBudgetTabGlAccountItemMonth {

	protected $m_strFormula;
	protected $m_fltUnitPrice;
	protected $m_intBudgetTabId;
	protected $m_intBudgetTabGlAccountMonthId;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBudgetId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBudgetTabGlAccountItemId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valQuantity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginalQuantity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOverridden() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDirty() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	// Get methods

	public function getFormula() {
		return $this->m_strFormula;
	}

	public function getUnitPrice() {
		return $this->m_fltUnitPrice;
	}

	public function getBudgetTabGlAccountMonthId() {
		return $this->m_intBudgetTabGlAccountMonthId;
	}

	public function getBudgetTabId() {
		return $this->m_intBudgetTabId;
	}

	// Set methods

	public function setFormula( $strFormula ) {
		$this->m_strFormula = $strFormula;
	}

	public function setUnitPrice( $fltUnitPrice ) {
		$this->m_fltUnitPrice = $fltUnitPrice;
	}

	public function setBudgetTabGlAccountMonthId( $intBudgetTabGlAccountMonthId ) {
		$this->m_intBudgetTabGlAccountMonthId = $intBudgetTabGlAccountMonthId;
	}

	public function setBudgetTabId( $intBudgetTabId ) {
		$this->m_intBudgetTabId = $intBudgetTabId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['formula'] ) ) {
			$this->setFormula( $arrmixValues['formula'] );
		}

		if( isset( $arrmixValues['unit_price'] ) ) {
			$this->setUnitPrice( $arrmixValues['unit_price'] );
		}

		if( isset( $arrmixValues['budget_tab_gl_account_month_id'] ) ) {
			$this->setBudgetTabGlAccountMonthId( $arrmixValues['budget_tab_gl_account_month_id'] );
		}

		if( isset( $arrmixValues['budget_tab_id'] ) ) {
			$this->setBudgetTabId( $arrmixValues['budget_tab_id'] );
		}

	}

}
?>