<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeCategories
 * Do not add any new functions to this class.
 */

class CApPayeeCategories extends CBaseApPayeeCategories {

    public static function fetchApPayeeCategories( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CApPayeeCategory', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchApPayeeCategory( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CApPayeeCategory', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

}
?>