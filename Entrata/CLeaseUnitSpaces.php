<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseUnitSpaces
 * Do not add any new functions to this class.
 */

class CLeaseUnitSpaces extends CBaseLeaseUnitSpaces {

	public static function fetchActiveLeaseUnitSpacesByLeaseIdByPropertyIdByCid( $intLeaseId, $intPropertyId, $intCid, $objDatabase, $boolIsMoveOutDateSet = false, $boolSkipPrimaryUnitSpace = false, $intPropertyUnitId = NULL ) {

		$strSql = 'SELECT
						lus.*,
						us.unit_number_cache
					FROM
						lease_unit_spaces lus
						JOIN unit_spaces us ON ( us.cid = lus.cid AND us.property_id = lus.property_id AND us.id = lus.unit_space_id )
					WHERE
						lus.cid = ' . ( int ) $intCid . '
						AND lus.property_id = ' . ( int ) $intPropertyId . '
						AND lus.lease_id = ' . ( int ) $intLeaseId . '
						AND lus.deleted_by IS NULL
						AND lus.deleted_on IS NULL ';

		if( true == $boolIsMoveOutDateSet ) {
			$strSql .= ' AND move_out_date IS NOT NULL ';
		}

		if( true == $boolSkipPrimaryUnitSpace ) {
			$strSql .= ' AND lus.slot_number > 0 ';
		}

		if( true == valId( $intPropertyUnitId ) ) {
			$strSql .= ' AND us.property_unit_id = ' . ( int ) $intPropertyUnitId;
		}

		$strSql .= ' ORDER BY
						lus.slot_number';

		return parent::fetchLeaseUnitSpaces( $strSql, $objDatabase );
	}

	public static function fetchActiveLeaseUnitSpacesByLeaseIdsByPropertyIdsByCid( $arrintLeaseIds, $arrintPropertyIds, $intCid, $objDatabase, $boolIsMoveOutDateSet = false, $strSelectedFields = NULL, $boolIsUnitSpaceStatusType = false, $intPropertyUnitId = NULL ) {

		if( false == valArr( $arrintLeaseIds ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSelectSql = ' lus.* ';
		if( true == valStr( $strSelectedFields ) ) {
			$strSelectSql = $strSelectedFields;
		}

		$strSql = 'SELECT
						' . $strSelectSql . ',
						l.primary_customer_id,
						l.property_unit_id,
						us.unit_space_status_type_id
					FROM
						lease_unit_spaces lus
						JOIN leases l ON ( l.cid = lus.cid AND l.id = lus.lease_id )
						LEFT JOIN unit_spaces us ON( us.cid = lus.cid AND us.id = lus.unit_space_id )
					WHERE
						lus.cid = ' . ( int ) $intCid . '
						AND lus.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND lus.lease_id IN( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND lus.deleted_by IS NULL
						AND lus.deleted_on IS NULL ';

		if( true == $boolIsMoveOutDateSet ) {
			$strSql .= ' AND move_out_date IS NOT NULL ';
		}

		if( true == $boolIsUnitSpaceStatusType ) {
			$strSql .= ' AND us.unit_space_status_type_id IN ( ' . implode( ',', ( array ) CUnitSpaceStatusType::$c_arrintMakeReadyUnitSpaceStatusTypes ) . ' )';
		}

		if( true == valId( $intPropertyUnitId ) ) {
			$strSql .= ' AND us.property_unit_id = ' . ( int ) $intPropertyUnitId;
		}

		$strSql .= ' ORDER BY
						lus.slot_number';

		return parent::fetchLeaseUnitSpaces( $strSql, $objDatabase );
	}

	public static function fetchLeaseUnitSpaceNumbersByLeaseIdByPropertyIdByCid( $intLeaseId, $intPropertyId, $intCid, $objDatabase, $boolIncludeDeletedUnitSpaces = false ) {

		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		if( false == is_numeric( $intLeaseId ) && false == valId( $intPropertyId ) ) return false;

			$strSql = 'SELECT
						array_to_string ( array_agg ( us.space_number ORDER BY lus.slot_number ASC ), \',\' ) AS unit_space_numbers
					FROM
						lease_unit_spaces lus
						JOIN unit_spaces us ON ( us.cid = lus.cid AND us.property_id = lus.property_id AND us.id = lus.unit_space_id ' . $strCheckDeletedUnitSpacesSql . ' )
					WHERE
						lus.cid = ' . ( int ) $intCid . '
						AND lus.property_id = ' . ( int ) $intPropertyId . '
						AND lus.lease_id = ' . ( int ) $intLeaseId . '
						AND lus.deleted_by IS NULL
						AND lus.deleted_on IS NULL
					GROUP BY
						lus.cid,
						lus.property_id,
						lus.lease_id';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchActiveLeaseUnitSpaceByUnitSpaceIdByLeaseStatusTypeIdsByPropertyIdByCid( $intUnitSpaceId, $arrintLeaseStatusTypeIds, $intPropertyId, $intCid, $objDatabase ) {

		if( true == is_null( $intUnitSpaceId ) ) return NULL;

		$strSql = 'SELECT
						lus.*
					FROM
						lease_unit_spaces lus
						JOIN leases l ON ( l.cid = lus.cid AND l.id = lus.lease_id AND l.unit_space_id <> lus.unit_space_id )
					  	JOIN lease_customers lc ON  ( lc.cid = l.cid AND lc.lease_id = l.id AND lc.lease_status_type_id IN( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' ) )
					WHERE
						lus.cid = ' . ( int ) $intCid . '
						AND lus.property_id = ' . ( int ) $intPropertyId . '
						AND lus.unit_space_id = ' . ( int ) $intUnitSpaceId . '
						AND lus.deleted_on IS NULL';

		return self::fetchLeaseUnitSpaces( $strSql, $objDatabase );
	}

	public static function fetchActiveLeaseUnitSpaceByUnitSpaceIdsByLeaseStatusTypeIdsByPropertyIdByCid( $arrintUnitSpaceIds, $arrintLeaseStatusTypeIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintUnitSpaceIds ) ) return NULL;

		$strSql = 'SELECT
						lus.*
					FROM
						lease_unit_spaces lus
						JOIN leases l ON ( l.cid = lus.cid AND l.id = lus.lease_id AND l.unit_space_id <> lus.unit_space_id )
					  	JOIN lease_customers lc ON  ( lc.cid = l.cid AND lc.lease_id = l.id AND lc.lease_status_type_id IN( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' ) )
					WHERE
						lus.cid = ' . ( int ) $intCid . '
						AND lus.property_id = ' . ( int ) $intPropertyId . '
						AND lus.unit_space_id IN( ' . implode( ',', $arrintUnitSpaceIds ) . ')';

		return self::fetchLeaseUnitSpaces( $strSql, $objDatabase );
	}

	public static function updateLeaseUnitSpaces( $intCid, $intLeaseId, $intUnitSpaceId, $intPropertyId, $intActiveLeaseIntervalid, $strMoveInDate, $objDatabase ) {

		if( true == valId( $intLeaseId ) && false == valId( $intCid ) ) return false;

		$arrstrReponse = fetchData( 'SELECT lease_unit_spaces_insert_update( ' . ( int ) $intCid . '::INTEGER, ' . ( int ) $intLeaseId . '::INTEGER,  ' . ( int ) $intUnitSpaceId . '::INTEGER,' . ( int ) $intPropertyId . '::INTEGER, ' . ( int ) $intActiveLeaseIntervalid . '::INTEGER, ' . '\'' . $strMoveInDate . '\'' . '::DATE );', $objDatabase );

		if( true == valArr( $arrstrReponse ) ) {
			trigger_error( $arrstrReponse[0]['lease_unit_spaces_insert_update'], E_USER_WARNING );
			return false;
		}

		return true;
	}

   	public static function fetchLeaseUnitSpacesCountByUnitSpaceIdsByLeaseStatusTypeIdsByCid( $arrintUnitSpaceIds, $arrintLeaseStatusTypeIds, $intCid, $objDatabase ) {

   		if( false == valArr( $arrintLeaseStatusTypeIds ) || false == valArr( $arrintUnitSpaceIds ) ) return NULL;

   		$strSql = 'SELECT
						count( lus.id ),
						lus.unit_space_id,
						lc.lease_status_type_id
					FROM
						leases l
    					JOIN lease_customers lc ON ( lc.cid = l.cid AND lc.lease_id = l.id AND lc.customer_id = l.primary_customer_id )
    					JOIN lease_unit_spaces lus ON ( lc.cid = lus.cid AND lc.lease_id = lus.lease_id )
					WHERE
						lus.unit_space_id IN( ' . implode( ',', $arrintUnitSpaceIds ) . ')
						AND lc.cid = ' . ( int ) $intCid . '
						AND lc.lease_status_type_id IN( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ')
						AND lus.deleted_on IS NULL
					GROUP BY
						lus.unit_space_id,
						lc.lease_status_type_id';

	    $arrmixLeasesCount = fetchData( $strSql, $objDatabase );

	    $arrintLeasesCount = [];
	    if( true == valArr( $arrmixLeasesCount ) ) {
		    foreach( $arrmixLeasesCount as $arrmixLeaseCount ) {
			    $arrintLeasesCount[$arrmixLeaseCount['unit_space_id']][$arrmixLeaseCount['lease_status_type_id']] = $arrmixLeaseCount['count'];
		    }
	    }

	    return $arrintLeasesCount;
   	}

   	public static function fetchUnitSpaceCountByLeaseIdByPropertyIdByCid( $intLeaseId, $intPropertyId, $intCid, $objDatabase ) {

   		$strWhere = 'WHERE
						lease_id = ' . ( int ) $intLeaseId . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND unit_space_id IS NOT NULL
						AND deleted_on IS NULL
						AND cid  = ' . ( int ) $intCid;

   		return parent::fetchLeaseUnitSpaceCount( $strWhere, $objDatabase );
   	}

   	public static function fetchAvaialbleUnitSpacesSqlByLeaseStartWindowIdByLeaseIdByPropertyIdByCid( $intLeaseStartWindowId, $intLeaseId, $intPropertyId, $intCid, $objDatabase, $boolIncludeMonthtoMonthUnitSpaces = false, $intPropertyUnitId = NULL ) {

	    $arrintLeaseStatusTypeIds = [ CLeaseStatusType::CURRENT, CLeaseStatusType::FUTURE ];
	    $arrmixLeaseStartWindows = CLeaseStartWindows::fetchAssociatedLeaseStartWindowIdsByIdByPropertyIdByCid( $intLeaseStartWindowId, $intPropertyId, $intCid, $objDatabase );

	    $strIncludeMonthtoMonthUnitSpaces = '';
	    if( true == $boolIncludeMonthtoMonthUnitSpaces ) {
		    $strIncludeMonthtoMonthUnitSpaces = ', ' . ( int ) CLeaseIntervalType::MONTH_TO_MONTH;
	    }

	    $strWhereCondition = ( true == is_numeric( $intPropertyUnitId ) ) ? ' AND cl.property_unit_id = ' . ( int ) $intPropertyUnitId : '';

	    $strWhereSql = ' us.id NOT IN (
										SELECT
											lus.unit_space_id
										FROM
											cached_leases cl
											JOIN lease_unit_spaces lus ON ( lus.lease_id = cl.id AND lus.cid = cl.cid AND lus.deleted_on IS NULL )
											JOIN lease_intervals li ON ( li.cid = cl.cid AND li.lease_id = cl.id AND li.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' ) )
										WHERE
											cl.cid = ' . ( int ) $intCid . '
											AND cl.property_id = ' . ( int ) $intPropertyId . '
										    AND cl.id != ' . ( int ) $intLeaseId . $strWhereCondition . '
										    AND cl.unit_space_id IS NOT NULL
										    AND cl.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )
										    AND li.lease_interval_type_id NOT IN ( ' . CLeaseIntervalType::LEASE_MODIFICATION . $strIncludeMonthtoMonthUnitSpaces . ' )';

	    $strWhereSql .= ( true == valArr( $arrmixLeaseStartWindows ) ) ? ' AND li.lease_start_window_id IN ( ' . implode( ',', array_keys( rekeyArray( 'id', $arrmixLeaseStartWindows ) ) ) . ' ) ' : ' AND li.lease_start_window_id = ' . ( int ) $intLeaseStartWindowId;
	    $strWhereSql .= ' GROUP BY cl.cid, lus.unit_space_id )';

		return $strWhereSql;
    }

	public static function fetchLeaseUnitSpaceNumbersByLeaseIdsByPropertyIdByCid( $arrintLeaseIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseIds ) ) {
			return NULL;
		}

		$strSql = ' 
					SELECT
						string_agg ( us.space_number, \',\' ) AS space_number,
						string_agg ( us.id::character VARYING, \',\' ) AS id,
						lus.lease_id
					FROM
						lease_unit_spaces lus
						JOIN unit_spaces us ON ( us.cid = lus.cid AND us.property_id = lus.property_id AND us.id = lus.unit_space_id )
					WHERE
						lus.cid = ' . ( int ) $intCid . '
						AND lus.property_id = ' . ( int ) $intPropertyId . '
						AND lus.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND lus.deleted_by IS NULL
						AND lus.deleted_on IS NULL
						AND us.deleted_on IS NULL
					GROUP BY
						lus.lease_id';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchActiveUnitSpaceIdsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase, $boolIsMoveOutDateSet = false ) {

		if( false == valId( $intLeaseId ) ) return NULL;

		$strSql = '	SELECT
						lus.unit_space_id
					FROM
						lease_unit_spaces lus
					WHERE
						lus.cid = ' . ( int ) $intCid . '
						AND lus.lease_id = ' . ( int ) $intLeaseId . '
						AND lus.deleted_by IS NULL
						AND lus.deleted_on IS NULL ';

		if( true == $boolIsMoveOutDateSet ) {
			$strSql .= ' AND move_out_date IS NOT NULL ';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveUnitSpaceIdsByLeaseIdsByPropertyIdByCid( $arrintLeaseIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseIds ) ) return NULL;

		$strSql = 'SELECT
						lease_id,
						unit_space_id
					FROM
						lease_unit_spaces
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND lease_id IN( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND deleted_by IS NULL
						AND deleted_on IS NULL 
					ORDER BY
						slot_number';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSumOfSuitesAreaByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		if( false === valId( $intLeaseId ) || false === valId( $intCid ) || false === valObj( $objDatabase, 'CDatabase' ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						SUM( cs.square_feet ) AS occupied_square_feet
					FROM 
						lease_unit_spaces lus
						JOIN unit_spaces us ON ( lus.unit_space_id = us.id AND lus.cid = us.cid )
						JOIN commercial_suites cs ON ( cs.property_unit_id = us.property_unit_id AND cs.cid = us.cid )
					WHERE 
						lus.cid = ' . $intCid . '
						AND lus.lease_id = ' . $intLeaseId . '
						AND lus.deleted_by IS NULL
						AND us.deleted_by IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMaxSlotNumberByLeaseIdByPropertyIdByCid( $intLeaseId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						MAX( lus.slot_number ) AS max_slot_number
					FROM
						lease_unit_spaces lus
					WHERE
						lus.cid = ' . ( int ) $intCid . '
						AND lus.property_id = ' . ( int ) $intPropertyId . '
						AND lus.lease_id = ' . ( int ) $intLeaseId . '
						AND lus.deleted_by IS NULL';

		return self::fetchColumn( $strSql, 'max_slot_number', $objDatabase );

	}

}
?>