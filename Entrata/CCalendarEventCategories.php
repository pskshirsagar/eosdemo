<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCalendarEventCategories
 * Do not add any new functions to this class.
 */

class CCalendarEventCategories extends CBaseCalendarEventCategories {

    public static function fetchCalendarEventCategories( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CCalendarEventCategory', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchCalendarEventCategory( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CCalendarEventCategory', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>