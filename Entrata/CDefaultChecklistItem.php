<?php

class CDefaultChecklistItem extends CBaseDefaultChecklistItem {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultChecklistId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChecklistItemTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChecklistItemTypeOptionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valItemTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valShowOnPortals() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRequired() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>