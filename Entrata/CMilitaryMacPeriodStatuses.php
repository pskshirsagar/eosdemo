<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMilitaryMacPeriodStatuses
 * Do not add any new functions to this class.
 */

class CMilitaryMacPeriodStatuses extends CBaseMilitaryMacPeriodStatuses {

	public static function fetchMilitaryMacPeriodStatuses( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMilitaryMacPeriodStatus', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchMilitaryMacPeriodStatus( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMilitaryMacPeriodStatus', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedMilitaryMacPeriodStatuses( $objDatabase ) {
		return self::fetchMilitaryMacPeriodStatuses( 'SELECT * FROM military_mac_period_statuses WHERE is_published = TRUE ORDER BY order_num', $objDatabase );
	}

}
?>