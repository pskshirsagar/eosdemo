<?php

class CSubsidyIncomeExceptionType extends CBaseSubsidyIncomeExceptionType {

	const SUBSIDY_INCOME_EXCEPTION_TYPE_CONVERTED 	 = 1;
	const SUBSIDY_INCOME_EXCEPTION_TYPE_DISPLACEMENT = 2;
	const SUBSIDY_INCOME_EXCEPTION_TYPE_EIT 	     = 3;
	const SUBSIDY_INCOME_EXCEPTION_TYPE_EAT 	     = 4;
	const SUBSIDY_INCOME_EXCEPTION_TYPE_OTHER 		 = 6;

	public static $c_arrmixSubsidyIncomeExceptionTypes = [
		self::SUBSIDY_INCOME_EXCEPTION_TYPE_CONVERTED,
		self::SUBSIDY_INCOME_EXCEPTION_TYPE_DISPLACEMENT,
		self::SUBSIDY_INCOME_EXCEPTION_TYPE_OTHER
	];

	public static $c_arrmixSubsidyIncomeExceptionTypesForHap = [
		self::SUBSIDY_INCOME_EXCEPTION_TYPE_DISPLACEMENT,
		self::SUBSIDY_INCOME_EXCEPTION_TYPE_EIT,
		self::SUBSIDY_INCOME_EXCEPTION_TYPE_EAT,
		self::SUBSIDY_INCOME_EXCEPTION_TYPE_OTHER
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>