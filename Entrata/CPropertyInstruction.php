<?php

class CPropertyInstruction extends CBasePropertyInstruction {

	protected $m_arrobjCompanyEmployeeContactDetails;
	protected $m_arrmixCompanyEmployeeContacts;
	protected $m_arrintCompanyEmployeeContactIds;
	protected $m_strPropertyInstructionActionType;
	protected $m_boolIsDefault;
	protected $m_boolIsClientLevel;

	const GENERAL_MAINTENANCE_PROTOCOL							= 1;
	const MAINTENANCE_REQUEST_911_EMERGENCY						= 2;
	const MAINTENANCE_REQUEST_911_EMERGENCY_INSTRUCTIONS		= 3;
	const NON_911_EMERGENCY										= 4;
	const NON_911_EMERGENCY_INSTRUCTIONS						= 5;
	const SECURITY_ISSUES_AND_DISTURBANCES						= 6;
	const SECURITY_ISSUES_AND_DISTURBANCES_INSTRUCTIONS			= 7;
	const GENERAL_COMPLAINTS									= 8;
	const GENERAL_COMPLAINT_INSTRUCTIONS						= 9;
	const PARKING_AND_TOWING									= 10;
	const PARKING_AND_TOWING_GENERAL_COMPLAINT_INSTRUCTIONS		= 11;
	const GENERAL_VENDOR_PROTOCOL								= 12;
	const GENERAL_SOLICITOR_PROTOCOL							= 13;
	const MARKET_SURVEY_PROTOCOL								= 14;
	const TEST_CALLS_PROTOCOL									= 15;
	const LOCKOUTS												= 16;
	const LOCKOUT_INSTRUCTIONS									= 17;
	const GENERAL_CORPORATE_CARE_PROTOCOL						= 18;
	const ACCOUNTING_PROTOCOL									= 19;
	const LEASING_PROTOCOL										= 20;
	const MAINTENANCE_PROTOCOL									= 21;
	const MANAGEMENT_PROTOCOL									= 22;
	const OTHER_PROTOCOL										= 23;
	const CALL_PROPERTY_STAFF									= 'call_property_staff';
	const SUBMIT_WORK_ORDER										= 'submit_work_order';

	public static $c_arrmixPropertyInstructionActionTypes = [
		'Call property staff'		=> self::CALL_PROPERTY_STAFF,
		'Submit work order'			=> self::SUBMIT_WORK_ORDER
	];

	/**
	* Setter Getter Functions
	*/

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['is_visible'] ) ) $this->setIsVisible( $arrmixValues['is_visible'] );
		if( true == isset( $arrmixValues['is_client_level'] ) ) $this->setIsClientLevel( $arrmixValues['is_client_level'] );
		if( true == isset( $arrmixValues['is_default'] ) ) $this->setIsDefault( $arrmixValues['is_default'] );
		if( true == isset( $arrmixValues['company_employee_contact_ids'] ) ) $this->setCompanyEmployeeContactIds( array_unique( $arrmixValues['company_employee_contact_ids'] ) );
		if( true == isset( $arrmixValues['property_instruction_action_type'] ) ) {
			$this->setPropertyInstructionActionType( $arrmixValues['property_instruction_action_type'] );
			$arrmixIsActionRequired = [ 'call_property_staff' => 0, 'submit_work_order' => 0 ];

			if( CPropertyInstruction::CALL_PROPERTY_STAFF == $this->getPropertyInstructionActionType() ) {
				$arrmixIsActionRequired = [ 'call_property_staff' => 1, 'submit_work_order' => 0 ];
			} elseif( CPropertyInstruction::SUBMIT_WORK_ORDER == $this->getPropertyInstructionActionType() ) {
				$arrmixIsActionRequired = [ 'call_property_staff' => 0, 'submit_work_order' => 1 ];
				$this->setCompanyEmployeeContactIds( NULL );
			}

			$this->setActionRequired( $arrmixIsActionRequired );
		}

		return true;
	}

	public function setIsDefault( $boolIsDefault ) {
		return $this->m_boolIsDefault = $boolIsDefault;
	}

	public function getIsDefault() {
		return $this->m_boolIsDefault;
	}

	public function setCompanyEmployeeContactDetails( $arrobjCompanyEmployeeContactDetails ) {
		return $this->m_arrobjCompanyEmployeeContactDetails = ( array ) $arrobjCompanyEmployeeContactDetails;
	}

	public function getCompanyEmployeeContactDetails() {
		return $this->m_arrobjCompanyEmployeeContactDetails;
	}

	public function setIsClientLevel( $boolIsClientLevel ) {
		return $this->m_boolIsClientLevel = $boolIsClientLevel;
	}

	public function getIsClientLevel() {
		return $this->m_boolIsClientLevel;
	}

	public function getIsVisibleCssClass() {
		return ( true == $this->getIsVisible() ) ? 'on' : 'off';
	}

	public function getIsVisibleCustomText() {
		return ( true == $this->getIsVisible() ) ? __( 'Yes' ) : __( 'No' );
	}

	public function getIsClientLevelCssClass() {
		return ( true == $this->getIsClientLevel() ) ? 'on' : 'off';
	}

	public function getIsClientLevelCustomText() {
		return ( true == $this->getIsClientLevel() ) ? __( 'Yes' ) : __( 'No' );
	}

	public function setPropertyInstructionActionType( $strPropertyInstructionActionType ) {
		return $this->m_strPropertyInstructionActionType = $strPropertyInstructionActionType;
	}

	public function getPropertyInstructionActionType() {
		return $this->m_strPropertyInstructionActionType;
	}

	public function setCompanyEmployeeContacts( $arrmixCompanyEmployeeContacts ) {
		return $this->m_arrmixCompanyEmployeeContacts = $arrmixCompanyEmployeeContacts;
	}

	public function getCompanyEmployeeContacts() {
		return $this->m_arrmixCompanyEmployeeContacts;
	}

	public function setCompanyEmployeeContactIds( $arrintCompanyEmployeeContactIds ) {
		return $this->m_arrintCompanyEmployeeContactIds = $arrintCompanyEmployeeContactIds;
	}

	public function getCompanyEmployeeContactIds() {
		return $this->m_arrintCompanyEmployeeContactIds;
	}

	/**
	* Validate Functions
	*/

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == valId( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Client id does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( false == $this->getIsClientLevel() && false == valId( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property id does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyInstructionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstruction() {
		$boolIsValid = true;

		if( false == valStr( $this->getInstruction() ) ) {
			$boolIsValid = false;

			if( true == in_array( $this->getPropertyInstructionId(), [ CPropertyInstruction::NON_911_EMERGENCY, CPropertyInstruction::MAINTENANCE_REQUEST_911_EMERGENCY ] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'instruction', __( 'Please enter event.' ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'instruction', __( 'Please enter protocol.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valInstructionDetails() {
		$boolIsValid = true;

		if( false == valStr( $this->getInstructionDetails() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'instruction_details', __( 'Please enter protocol.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCompanyEmployeeContactTypeId() {
		$boolIsValid = true;

		if( ( false == valId( $this->getCompanyEmployeeContactTypeId() ) || 1 > $this->getCompanyEmployeeContactTypeId() ) && self::CALL_PROPERTY_STAFF == $this->getPropertyInstructionActionType() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_employee_contact_type_ids', __( 'Please select contact category.' ) ) );
		}

		return $boolIsValid;
	}

	public function valProprtyInstructionCompanyEmployeeContacts() {
		$boolIsValid = true;

		if( false == is_null( $this->getCompanyEmployeeContactIds() ) ) {
			if( self::CALL_PROPERTY_STAFF == $this->getPropertyInstructionActionType() && CCompanyEmployeeContactType::CUSTOM == $this->getCompanyEmployeeContactTypeId() && false == valStr( implode( ',', $this->getCompanyEmployeeContactIds() ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'instruction_details', __( 'Please select custom contacts.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valIsVisible() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valInstruction();

				if( CPropertyInstruction::GENERAL_MAINTENANCE_PROTOCOL == $this->getPropertyInstructionId() ) {
					$boolIsValid &= $this->valCompanyEmployeeContactTypeId();
					$boolIsValid &= $this->valProprtyInstructionCompanyEmployeeContacts();
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	* Other Functions
	*/

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsSoftDelete = true ) {
		if( false == $boolIsSoftDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( 'NOW()' );
		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( 'NOW()' );

		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>