<?php

class CCompanyUserReportHistory extends CBaseCompanyUserReportHistory {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyUserId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportHistoryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyGroupIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsArchived() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valViewedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function getCanonicalPath( $strExtension ) {
		return 'historical_reports/' . $this->m_intReportHistoryId . '/' . md5( $this->m_strPropertyGroupIds ) . '.' . $strExtension;
	}

}
