<?php

class CApPaymentPrintingOrderType extends CBaseApPaymentPrintingOrderType {

	const PROPERTY_AND_VENDOR					= 1;
	const VENDOR_AND_PROPERTY					= 2;
	const BANK_ACCOUNT_AND_VENDOR				= 3;
	const BANK_ACCOUNT_AND_PROPERTY				= 4;
	const PROPERTY_AND_BUILDING_UNIT_AND_VENDOR	= 5;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
