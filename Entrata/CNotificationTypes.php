<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CNotificationTypes
 * Do not add any new functions to this class.
 */

class CNotificationTypes extends CBaseNotificationTypes {

    public static function fetchNotificationTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CNotificationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchNotificationType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CNotificationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchPublishedNotificationTypes( $objDatabase ) {

    	return parent::fetchNotificationTypes( 'SELECT * FROM notification_types WHERE is_published = 1', $objDatabase );
    }

}
?>