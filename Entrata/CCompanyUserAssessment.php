<?php

class CCompanyUserAssessment extends CBaseCompanyUserAssessment {

	const ASSESSEMENT_INCOMPLETE = 0;
	const ASSESSEMENT_COMPLETE   = 1;
	const NOT_STARTED_COURSES    = 0;
	const IN_PROGRESS_COURSES    = 1;
	const FAILED_COURSES         = 2;
	const PASSED_COURSES         = 3;
	const UNASSIGNED_COURSES     = 4;

	const IN_PROGRESS_TEXT = 'in_progress';
	const NOT_STARTED_TEXT = 'not_started';
	const FAILED_TEXT      = 'failed';
	const COMPLETED_TEXT   = 'completed';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyUserId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHelpResourceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyGroupAssessmentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAssignedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDueDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTrainingTeamAssessmentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>