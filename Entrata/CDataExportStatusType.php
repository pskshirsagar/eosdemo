<?php

class CDataExportStatusType extends CBaseDataExportStatusType {

	const UNUSED 					= 1;
	const IDLE						= 2;
	const QUEUED					= 3;
	const BACKUP_CREATED			= 4;
	const READY_TO_DOWNLOAD			= 5;
	const COMPLETE					= 6;
	const UPLOAD_FAILED				= 7;
	const FAILED					= 8;
	const DATA_EXPORT_CREATING_CSV_FILES					= 9;
	const DATA_EXPORT_CSV_FILES_CREATED						= 10;
	const DATA_EXPORT_CREATING_CSV_ARCHIVE					= 11;
	const DATA_EXPORT_CSV_ARCHIVE_CREATED					= 12;
	const DATA_EXPORT_GENERATING_SQL_BACKUP					= 13;
	const DATA_EXPORT_GENERATING_INSERT_STATEMENTS			= 14;
	const DATA_EXPORT_INSERT_STATEMENTS_GENERATED			= 15;
	const DATA_EXPORT_CONNECTING_TO_SQL_SERVER				= 16;
	const DATA_EXPORT_CONNECTED_TO_SQL_SERVER				= 17;
	const DATA_EXPORT_CREATING_NEW_DATABASE					= 18;
	const DATA_EXPORT_NEW_DATABASE_CREATED					= 19;
	const DATA_EXPORT_EXECUTING_SQL_STATEMENTS				= 20;
	const DATA_EXPORT_ALL_SQL_EXECUTED_SUCCESSFULLY			= 21;
	const DATA_EXPORT_GENERATING_BACKUP_OF_NEW_SQL_DATABASE	= 22;
	const DATA_EXPORT_NEW_SQL_BACKUP_GENERATED				= 23;
	const DATA_EXPORT_UPLOADING_BACKUP_TO_FTP				= 24;
	const DATA_EXPORT_CONNECTING_TO_FTP						= 25;
	const DATA_EXPORT_ATTEMPTING_FTP_LOGIN					= 26;
	const DATA_EXPORT_FTP_LOGIN_SUCCESSFUL					= 27;
	const DATA_EXPORT_UPLOADING_BACKUP						= 28;
	const DATA_EXPORT_BACKUP_UPLOADED_SUCCESSFULLY			= 29;
	const DATA_EXPORT_DELETING_UPLOADED_ZIP_FILE			= 30;
	const DATA_EXPORT_DELETED_UPLOADED_ZIP_FILE				= 31;
	const DATA_EXPORT_DELETING_WEEK_OLD_FILES				= 32;
	const DATA_EXPORT_DELETED_WEEK_OLD_FILES				= 33;
	const DATA_EXPORT_FAILED_TO_CREATE_CSV					= 34;
	const DATA_EXPORT_FAILED_TO_CREATE_CSV_ARCHIVE			= 35;
	const DATA_EXPORT_FAILED_TO_GENERATE_INSERT_STATEMENTS	= 36;
	const DATA_EXPORT_FAILED_TO_CONNECT_TO_SQL_SERVER		= 37;
	const DATA_EXPORT_FAILED_TO_CREATE_NEW_DATABASE			= 38;
	const DATA_EXPORT_FAILED_TO_EXECUTE_CREATE_TABLE		= 39;
	const DATA_EXPORT_FAILED_TO_EXECUTE_INSERT_STATEMENTS	= 40;
	const DATA_EXPORT_FAILED_TO_GENERATE_SQL_BACKUP			= 41;
	const DATA_EXPORT_FAILED_TO_CONNECT_TO_FTP				= 42;
	const DATA_EXPORT_FAILED_TO_LOGIN_FTP					= 43;
	const DATA_EXPORT_FAILED_TO_DELETE_ZIP_FILE				= 44;
	const DATA_EXPORT_FAILED_TO_DELETE_WEEK_OLD_FILES		= 45;
	const DATA_EXPORT_FAILED_TO_UPLOAD_BACKUP_TO_FTP		= 46;
	const DATA_EXPORT_DELETING_SQL_DATABASE					= 47;
	const DATA_EXPORT_DELETED_SQL_DATABASE					= 48;
	const DATA_EXPORT_FAILED_TO_DELETE_SQL_DATABASE			= 49;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
        }

        return $boolIsValid;
    }
}
?>