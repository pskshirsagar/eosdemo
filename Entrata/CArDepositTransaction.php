<?php

class CArDepositTransaction extends CBaseArDepositTransaction {

    public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id is required' ) ) );
        }

        return $boolIsValid;
    }

    public function valArDepositId( $boolValidateArDepositId ) {

         $boolIsValid = true;

        if( true == is_null( $this->getArDepositId() ) && true == $boolValidateArDepositId ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_deposit_id', __( 'Ar deposit is required' ) ) );
        }

        return $boolIsValid;
    }

    public function valArTransactionId() {
        $boolIsValid = true;

        if( true == is_null( $this->getArTransactionId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_transaction_id', __( 'Ar transaction is required' ) ) );
        }

        return $boolIsValid;
    }

    public function valDuplicity( $objDatabase ) {

    	if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			trigger_error( __( 'Database did not load.' ), E_USER_ERROR );
			exit;
    	}

    	$strFrom = ' ar_deposit_transactions adt
					JOIN ar_deposits ad ON ( adt.cid = ad.cid AND adt.ar_deposit_id = ad.id )';

		$strWhere = ' WHERE
						ad.is_deleted = 0
						AND adt.ar_transaction_id = ' . ( int ) $this->getArTransactionId() . '
						AND ad.cid = ' . ( int ) $this->getCid();

    	if( 0 != \Psi\Eos\Entrata\CArDepositTransactions::createService()->fetchArDepositTransactionCount( $strWhere, $objDatabase, $strFrom ) ) {
    		$this->addErrorMsg( new CErrorMsg( NULL, '', __( 'Transaction #{%s, 0} is already contained in another deposit.', [ $this->getArTransactionId() ] ) ) );
    		return false;
    	}

    	return true;
    }

    public function validate( $strAction, $objDatabase, $boolValidateArDepositId = true ) {

        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valArTransactionId();
            	$boolIsValid &= $this->valArDepositId( $boolValidateArDepositId );
            	$boolIsValid &= $this->valDuplicity( $objDatabase );
            	break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>