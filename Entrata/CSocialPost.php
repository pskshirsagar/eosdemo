<?php

class CSocialPost extends CBaseSocialPost {

	protected $m_arrintSocialPostTypes;

	protected $m_strNote;
	protected $m_strPostedDate;
	protected $m_strNoteAddedOn;
	protected $m_strPropertyName;
	protected $m_strRemoteUserKey;
	protected $m_strRemoteUsername;
	protected $m_strExternalPostId;
	protected $m_strQuestionAnswer;
	protected $m_strPostedAgo;

	protected $m_intPropertyId;
	protected $m_intLikesCount;
	protected $m_intShareCount;
	protected $m_intViewsCount;
	protected $m_intClicksCount;
	protected $m_intNoteAddedBy;
	protected $m_intCommentCount;
	protected $m_strRemoteMediaUri;
	protected $m_strCompanyMediaUrl;
	protected $m_intSocialPostStatusId;
	protected $m_strMediaDetails;
	protected $m_strErrorMessage;
	protected $m_strAccountName;

	protected $m_boolIsDeletedFromThirdParty;
	protected $m_arrstrSocialPostCompanyMediaFileFullSizeUrls;

	// INSTAGRAM
	const POST_CATEGORY_STORIES          = 'STORIES';
	const POST_CATEGORY_MEDIA            = 'MEDIA';
	const POST_CATEGORY_CAROUSEL_ALBUM   = 'CAROUSEL_ALBUM';

	// GOOGLE QA
	const GOOGLE_QA_AUTHOR_TYPE_MERCHANT = 'MERCHANT';
	const GOOGLE_QA_ANSWERED             = 'answered';
	const GOOGLE_QA_IGNORED              = 'ignored';
	const GOOGLE_QA_NEW_QUESTIONS        = 'new_questions';

	// GOOGLE Post category
	const GOOGLE_POST_CATEGORY_WHATS_NEW = 'whats_new';
	const GOOGLE_POST_CATEGORY_OFFER     = 'offer';
	const GOOGLE_POST_CATEGORY_EVENT     = 'event';

	/**
	 * Create Functions
	 *
	 **/

	public function createSocialPostStatusAssociation() {
		$objSocialPostStatusAssociation = new CSocialPostStatusAssociation();
		$objSocialPostStatusAssociation->setCid( $this->m_intCid );
		$objSocialPostStatusAssociation->setSocialPostId( $this->getId() );

		return $objSocialPostStatusAssociation;
	}

	public function createSocialPostShareContent() {
		$objSocialPostShareContent = new CSocialPostShareContent();
		$objSocialPostShareContent->setPropertyId( $this->getPropertyId() );
		$objSocialPostShareContent->setCid( $this->getCid() );
		$objSocialPostShareContent->setSocialPostId( $this->getId() );

		return $objSocialPostShareContent;
	}

	/**
	 * Set Functions
	 *
	 **/

	public function setSocialPostCompanyMediaFileFullSizeUrls( $arrstrSocialPostCompanyMediaFileFullSizeUrls ) {
		if( true == valArr( $arrstrSocialPostCompanyMediaFileFullSizeUrls ) ) {
			$this->m_arrstrSocialPostCompanyMediaFileFullSizeUrls = $arrstrSocialPostCompanyMediaFileFullSizeUrls;
		}
	}

	public function setExternalPostId( $strExternalPostId ) {
		$this->m_strExternalPostId = CStrings::strTrimDef( $strExternalPostId, 250, NULL, true );
	}

	public function setRemoteUserKey( $strRemoteUserKey ) {
		$this->m_strRemoteUserKey = CStrings::strTrimDef( $strRemoteUserKey, 250, NULL, true );
	}

	public function setRemoteUsername( $strRemoteUsername ) {
		$this->m_strRemoteUsername = CStrings::strTrimDef( $strRemoteUsername, 250, NULL, true );
	}

	public function setNote( $strNote ) {
		$this->m_strNote = $strNote;
	}

	public function setNoteAddedOn( $strNoteAddedOn ) {
		$this->m_strNoteAddedOn = $strNoteAddedOn;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
	}

	public function setLikesCount( $intLikesCount ) {
		$this->m_intLikesCount = CStrings::strToIntDef( $intLikesCount, NULL, false );
	}

	public function setViewsCount( $intViewsCount ) {
		$this->m_intViewsCount = CStrings::strToIntDef( $intViewsCount, NULL, false );
	}

	public function setClicksCount( $intClicksCount ) {
		$this->m_intClicksCount = CStrings::strToIntDef( $intClicksCount, NULL, false );
	}

	public function setCommentCount( $intCommentCount ) {
		$this->m_intCommentCount = CStrings::strToIntDef( $intCommentCount, NULL, false );
	}

	public function setShareCount( $intShareCount ) {
		$this->m_intShareCount = CStrings::strToIntDef( $intShareCount, NULL, false );
	}

	public function setRemoteMediaUri( $strRemoteMediaUri ) {
		$this->m_strRemoteMediaUri = CStrings::strTrimDef( $strRemoteMediaUri, NULL, NULL, true );
	}

	public function setCompanyMediaUrl( $strCompanyMediaUrl ) {
		$this->m_strCompanyMediaUrl = CStrings::strTrimDef( $strCompanyMediaUrl, NULL, NULL, true );
	}

	public function setNoteAddedBy( $intNoteAddedBy ) {
		$this->m_intNoteAddedBy = $intNoteAddedBy;
	}

	public function setPostedDate( $strPostedDate ) {
		$this->m_strPostedDate = $strPostedDate;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setSocialPostStatusId( $intSocialPostStatusId ) {
		$this->m_intSocialPostStatusId = CStrings::strToIntDef( $intSocialPostStatusId, NULL, false );
	}

	public function setQuestionAnswer( $strQuestionAnswer ) {
		$this->m_strQuestionAnswer = $strQuestionAnswer;
	}

	public function setPostedAgo( $strPostedOnAgo ) {
		$this->m_strPostedAgo = $strPostedOnAgo;
	}

	public function setMediaDetails( $strMediaDetails ) {
		$this->m_strMediaDetails = $strMediaDetails;
	}

	public function setErrorMessage( $strErrorMessage ) {
		$this->m_strErrorMessage = $strErrorMessage;
	}

	public function setAccountName( $strAccountName ) {
		$this->m_strAccountName = $strAccountName;
	}

	public function setIsDeletedFromThirdParty( $boolIsDeletedFromThirdParty ) {
		$this->m_boolIsDeletedFromThirdParty = $boolIsDeletedFromThirdParty;
	}

	/**
	 * Get Functions
	 *
	 **/

	public function getSocialPostCompanyMediaFileFullSizeUrls() {
		return $this->m_arrstrSocialPostCompanyMediaFileFullSizeUrls;
	}

	public function getExternalPostId() {
		return $this->m_strExternalPostId;
	}

	public function getRemoteUsername() {
		return $this->m_strRemoteUsername;
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function getNoteAddedOn() {
		return $this->m_strNoteAddedOn;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getLikesCount() {
		return $this->m_intLikesCount;
	}

	public function getViewsCount() {
		return $this->m_intViewsCount;
	}

	public function getClicksCount() {
		return $this->m_intClicksCount;
	}

	public function getCommentCount() {
		return $this->m_intCommentCount;
	}

	public function getShareCount() {
		return $this->m_intShareCount;
	}

	public function getRemoteMediaUri() {
		return $this->m_strRemoteMediaUri;
	}

	public function getCompanyMediaUrl() {
		return $this->m_strCompanyMediaUrl;
	}

	public function getNoteAddedBy() {
		return $this->m_intNoteAddedBy;
	}

	public function getPostedDate() {
		return $this->m_strPostedDate;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getSocialPostStatusId() {
		return $this->m_intSocialPostStatusId;
	}

	public function getQuestionAnswer() {
		return $this->m_strQuestionAnswer;
	}

	public function getPostedAgo() {
		return $this->m_strPostedAgo;
	}

	public function getMediaDetails() {
		if( true == isset( $this->m_strMediaDetails ) ) {
			$this->m_jsonDetails = CStrings::strToJson( $this->m_strMediaDetails );
			unset( $this->m_strMediaDetails );
		}
		return $this->m_jsonDetails;
	}

	public function getErrorMessage() {
		return $this->m_strErrorMessage;
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function getIsDeletedFromThirdParty() {
		return $this->m_boolIsDeletedFromThirdParty;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['posted_on'] ) ) $this->setPostedOn( $arrmixValues['posted_on'] );
		if( true == isset( $arrmixValues['external_post_id'] ) ) $this->setExternalPostId( $arrmixValues['external_post_id'] );
		if( true == isset( $arrmixValues['remote_user_key'] ) ) $this->setRemoteUserKey( $arrmixValues['remote_user_key'] );
		if( true == isset( $arrmixValues['remote_username'] ) ) $this->setRemoteUsername( $arrmixValues['remote_username'] );
		if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['likes_count'] ) ) $this->setLikesCount( $arrmixValues['likes_count'] );
		if( true == isset( $arrmixValues['comment_count'] ) ) $this->setCommentCount( $arrmixValues['comment_count'] );
		if( true == isset( $arrmixValues['share_count'] ) ) $this->setShareCount( $arrmixValues['share_count'] );
		if( true == isset( $arrmixValues['social_post_status_id'] ) ) $this->setSocialPostStatusId( $arrmixValues['social_post_status_id'] );
		if( true == isset( $arrmixValues['note'] ) ) $this->setNote( $arrmixValues['note'] );
		if( true == isset( $arrmixValues['note_added_on'] ) ) $this->setNoteAddedOn( $arrmixValues['note_added_on'] );
		if( true == isset( $arrmixValues['note_added_by'] ) ) $this->setNoteAddedBy( $arrmixValues['note_added_by'] );
		if( true == isset( $arrmixValues['posted_date'] ) ) $this->setPostedDate( $arrmixValues['posted_date'] );
		if( true == isset( $arrmixValues['remote_media_uri'] ) ) $this->setRemoteMediaUri( $arrmixValues['remote_media_uri'] );
		if( true == isset( $arrmixValues['company_media_url'] ) ) $this->setCompanyMediaUrl( $arrmixValues['company_media_url'] );
		if( true == isset( $arrmixValues['media_details'] ) ) $this->setMediaDetails( $arrmixValues['media_details'] );
		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['question_answer'] ) ) $this->setQuestionAnswer( $arrmixValues['question_answer'] );
		if( true == isset( $arrmixValues['views_count'] ) ) $this->setViewsCount( $arrmixValues['views_count'] );
		if( true == isset( $arrmixValues['clicks_count'] ) ) $this->setClicksCount( $arrmixValues['clicks_count'] );
	}

	/**
	 * Validation Functions
	 *
	 **/

	public function valPostedMessage() {
		$boolIsValid = true;

		if( false == valStr( $this->getPostedMessage() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'posted_message', __( 'Post can not be empty.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valScheduledOn( $strPropertyTimeZone ) {
		$boolIsValid = true;
		$strScheduledOn = $this->getScheduledOn();
		date_default_timezone_set( $strPropertyTimeZone );
		$strTime = date( 'm/d/Y H:i:s' );

		if( true == empty( $strScheduledOn ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_on', 'Schedule date/time is required.' ) );
			$boolIsValid = false;
		} elseif( strtotime( $strScheduledOn ) <= strtotime( $strTime ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_on', 'Please enter future date/time.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valSocialPostType() {
		return true;
	}

	public function valCategory( $strSocialPostCategoryName, $strPostType ) {
		$boolIsValid = true;

		if( false == $this->getSocialPostCategoryId() && false == valStr( $strSocialPostCategoryName ) && 1 == $strPostType ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Category', 'Select a category.' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $strPropertyTimeZone = CTimeZone::DEFAULT_TIME_ZONE_NAME, $intSocialPostStatusId = NULL ) {

		$boolIsValid = true;

		switch( $strAction ) {

			case 'validate_scheduled':
				$boolIsValid &= $this->valScheduledOn( $strPropertyTimeZone );
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valSocialPostType();
				$boolIsValid &= $this->valPostedMessage();
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_post_url':
				$boolIsValid &= $this->valSocialPostType();
				$boolIsValid &= $this->valPostedMessage();
				if( CSocialPostStatus::DRAFT != $intSocialPostStatusId ) {
					$boolIsValid &= $this->valScheduledOn( $strPropertyTimeZone );
				}
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>