<?php

class CDefaultArRules extends CBaseDefaultArRules {

    public static function fetchDefaultArRules( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CDefaultArRule', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchDefaultArRule( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CDefaultArRule', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>