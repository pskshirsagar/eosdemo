<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyApplicationAddOnGroups
 * Do not add any new functions to this class.
 */

class CCompanyApplicationAddOnGroups extends CBaseCompanyApplicationAddOnGroups {

    public static function fetchCustomCompanyApplicationAddOnGroupsByPropertyIdByAddOnGroupIdsByCid( $intPropertyId, $arrintAddOnGroupIds, $intCid, $objDatabase ) {

    	if( false == valArr( $arrintAddOnGroupIds ) ) {
    		return NULL;
    	}

    	$strSql = 'SELECT
				    	caaog.*,
				    	ca.title
    			    FROM
				    	company_application_add_on_groups caaog
				    	JOIN company_applications ca ON ca.id = caaog.company_application_id AND caaog.cid = ca.cid
    				WHERE
    					caaog.property_id = ' . ( int ) $intPropertyId . '
				    	AND caaog.add_on_group_id IN ( ' . implode( ', ', $arrintAddOnGroupIds ) . ' )
				    	AND caaog.cid = ' . ( int ) $intCid;

    	return self::fetchCompanyApplicationAddOnGroups( $strSql, $objDatabase );
    }

    public static function fetchAddOnGroupsByCompanyApplicationIdsByPropertyIdByCid( $arrintCompanyApplicationIds, $intPropertyId, $intCid, $objDatabase ) {

    	if( false == valArr( $arrintCompanyApplicationIds ) ) return NULL;

		$strSql = '	SELECT
						aog.id,' .
						( false == empty( CLocaleContainer::createService()->getTargetLocaleCode() ) ?
						'util_get_translated( \'name\',aog.name, aog.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' )' : 'aog.name' ) . ' AS name,
						caag.company_application_id
					FROM
						add_on_groups aog
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id = aog.property_id OR aoc.property_id IS NULL ) )
						JOIN company_application_add_on_groups caag ON ( caag.add_on_group_id = aog.id and caag.cid = aog.cid AND caag.company_application_id IN ( ' . implode( ',', $arrintCompanyApplicationIds ) . ' ) )
					WHERE
						aog.cid = ' . ( int ) $intCid . '
						AND aog.property_id = ' . ( int ) $intPropertyId . '
						AND aog.add_on_type_id = ' . CAddOnType::SERVICES . '
						AND aoc.default_add_on_category_id = ' . CDefaultAddOnCategory::MEAL_PLANS . '
						AND aog.deleted_on IS NULL';

    	return fetchData( $strSql, $objDatabase );
    }

	public function fetchAddOnGroupsByCompanyApplicationIdsByAddOnCategoryIdByPropertyIdByCid( $arrintCompanyApplicationIds, $intAddOnCategoryId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCompanyApplicationIds ) ) return NULL;

		$strSql = '	SELECT
						aog.id,' .
				  ( false == empty( CLocaleContainer::createService()->getTargetLocaleCode() ) ?
					  'util_get_translated( \'name\',aog.name, aog.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' )' : 'aog.name' ) . ' AS name,
						caag.company_application_id
					FROM
						add_on_groups aog
						JOIN company_application_add_on_groups caag ON ( caag.add_on_group_id = aog.id and caag.cid = aog.cid AND caag.company_application_id IN ( ' . implode( ',', $arrintCompanyApplicationIds ) . ' ) )
					WHERE
						aog.cid = ' . ( int ) $intCid . '
						AND aog.property_id = ' . ( int ) $intPropertyId . '
						AND aog.add_on_type_id = ' . CAddOnType::SERVICES . '
						AND aog.add_on_category_id = ' . ( int ) $intAddOnCategoryId . '
						AND aog.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

}
?>