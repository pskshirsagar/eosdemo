<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitSpaceStartWindowSettings
 * Do not add any new functions to this class.
 */

class CUnitSpaceStartWindowSettings extends CBaseUnitSpaceStartWindowSettings {

	public static function fetchUnitSpaceStartWindowSettingByUnitSpaceIdByLeaseStartWindowIdByPropertyIdByCid( $intUnitSpaceId, $intLeaseStartWindowId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ussws.*
					FROM 
						unit_space_start_window_settings ussws
					WHERE 
						ussws.unit_space_id = ' . ( int ) $intUnitSpaceId . '
						AND  ussws.lease_start_window_id = ' . ( int ) $intLeaseStartWindowId . '
						AND USSWS.property_id = ' . ( int ) $intPropertyId . '
						AND  ussws.cid = ' . ( int ) $intCid;

		return self::fetchUnitSpaceStartWindowSetting( $strSql, $objDatabase );

	}

	public static function fetchUnitSpaceStartWindowSettingsByUnitSpaceIdByLeaseStartWindowIdByPropertyIdByCid( $intUnitSpaceId, $intLeaseStartWindowId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ussws.notes,
						ussws.updated_on,
						cu.id as company_user_id,
						COALESCE (ce.name_first || \' \' || ce.name_last,cu.username)AS username
					FROM
						unit_space_start_window_settings ussws
						JOIN company_users cu on ( cu.cid = ussws.cid AND cu.id = ussws.updated_by )
						LEFT JOIN company_employees ce on (cu.company_employee_id = ce.id AND cu.cid = ce.cid)
					WHERE 
						ussws.unit_space_id = ' . ( int ) $intUnitSpaceId . '
						AND ussws.lease_start_window_id = ' . ( int ) $intLeaseStartWindowId . '
						AND USSWS.property_id =' . ( int ) $intPropertyId . '
						AND ussws.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );

	}

}

?>