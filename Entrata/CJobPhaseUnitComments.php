<?php

namespace Psi\Eos\Entrata;

/**
 * EOS PLURAL CLASS
 * Warning: This is a composite key based class. If you are regenerating the this
 * class again, please do so by checking the composite key checkbox.
 */
class CJobPhaseUnitComments extends \Psi\Eos\CEosPluralBase {

	/**
	 * @param string $strSql
	 * @param \CDatabase $objDatabase
	 * @param bool $boolIsReturnKeyedArray When true array keyed by id is returned
	 * @return \CJobPhaseUnitComment[]
	 */
	public function fetchJobPhaseUnitComments( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return $this->fetchObjects( $strSql, \CJobPhaseUnitComment::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @param string $strSql
	 * @param \CDatabase $objDatabase
	 * @return \CJobPhaseUnitComment
	 */
	public function fetchJobPhaseUnitComment( $strSql, $objDatabase ) {
		return $this->fetchObject( $strSql, \CJobPhaseUnitComment::class, $objDatabase );
	}

	/**
	 * @param string $strWhere Specify the where clause
	 * @param \CDatabase $objDatabase
	 * @return int Row count
	 */
	public function fetchJobPhaseUnitCommentCount( $strWhere, $objDatabase ) {
		return $this->fetchRowCount( $strWhere, 'job_phase_unit_comments', $objDatabase );
	}

	/**
	 * @param int $intId
	 * @param int $intCid
	 * @param \CDatabase $objDatabase
	 * @return \CJobPhaseUnitComment
	 */
	public function fetchJobPhaseUnitCommentByIdByCid( $intId, $intCid, $objDatabase ) {
		return $this->fetchJobPhaseUnitComment( sprintf( 'SELECT * FROM job_phase_unit_comments WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	/**
	 * @param int $intCid
	 * @param \CDatabase $objDatabase
	 * @return \CJobPhaseUnitComment[]
	 */
	public function fetchJobPhaseUnitCommentsByCid( $intCid, $objDatabase ) {
		return $this->fetchJobPhaseUnitComments( sprintf( 'SELECT * FROM job_phase_unit_comments WHERE cid = %d', $intCid ), $objDatabase );
	}

	/**
	 * @param int $intJobPhaseId
	 * @param int $intCid
	 * @param \CDatabase $objDatabase
	 * @return \CJobPhaseUnitComment[]
	 */
	public function fetchJobPhaseUnitCommentsByJobPhaseIdByCid( $intJobPhaseId, $intCid, $objDatabase ) {
		return $this->fetchJobPhaseUnitComments( sprintf( 'SELECT * FROM job_phase_unit_comments WHERE job_phase_id = %d AND cid = %d', $intJobPhaseId, $intCid ), $objDatabase );
	}

	/**
	 * @param int $intJobPhaseUnitId
	 * @param int $intCid
	 * @param \CDatabase $objDatabase
	 * @return \CJobPhaseUnitComment[]
	 */
	public function fetchJobPhaseUnitCommentsByJobPhaseUnitIdByCid( $intJobPhaseUnitId, $intCid, $objDatabase ) {
		return $this->fetchJobPhaseUnitComments( sprintf( 'SELECT * FROM job_phase_unit_comments WHERE job_phase_unit_id = %d AND cid = %d', $intJobPhaseUnitId, $intCid ), $objDatabase );
	}

}
?>