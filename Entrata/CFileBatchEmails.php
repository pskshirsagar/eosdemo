<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileBatchEmails
 * Do not add any new functions to this class.
 */

class CFileBatchEmails extends CBaseFileBatchEmails {

	public static function fetchFileBatchEmailsByFileBatchIdByCid( $intFileBatchId, $intCid, $objDatabase ) {
		return self::fetchFileBatchEmail( sprintf( 'SELECT * FROM file_batch_emails WHERE file_batch_id = %d AND cid = %d ORDER BY id desc LIMIT 1', ( int ) $intFileBatchId, ( int ) $intCid ), $objDatabase );
	}
}
?>