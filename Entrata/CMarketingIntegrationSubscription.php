<?php

use Psi\Eos\Entrata\CMarketingIntegrationSubscriptions;

class CMarketingIntegrationSubscription extends CBaseMarketingIntegrationSubscription {

	protected $m_strPropertyName;
	protected $m_boolIsFeedEnabled;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInternetListingServiceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOccupancyTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsLimited() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInterruptions() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIlsNotifiedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotificationConfirmedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotificationRejectedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRejectionNote() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChangeNotifiedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSuspendedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	/**
	 * Get Functions
	 */

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getIsFeedEnabled() {
		return $this->m_boolIsFeedEnabled;
	}

	/**
	 * Set Functions
	 */

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setIsFeedEnabled( $boolIsFeedEnabled ) {
		$this->m_boolIsFeedEnabled = $boolIsFeedEnabled;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_name'] ) : $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['is_feed_enabled'] ) ) $this->setIsFeedEnabled( $arrmixValues['is_feed_enabled'] );
	}

	public function validate( $strAction, $objDatabase = NULL ) : bool {
		$boolIsValid = true;

		switch( $strAction ) {
			case 'activation_request':
			case VALIDATE_UPDATE:
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valDuplicateMarketingIntegrationSubscription( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function valDuplicateMarketingIntegrationSubscription( $objDatabase ) : bool {

		$boolIsValid = true;

		$objMarketingIntegrationSubscription = CMarketingIntegrationSubscriptions::createService()->fetchMarketingIntegrationSubscriptionByPropertyIdByInternetListingServiceIdByCid( $this->getPropertyId(), $this->getInternetListingServiceId(), $this->getCid(), $objDatabase, $this->getOccupancyTypeId() );

		if( true == valObj( $objMarketingIntegrationSubscription, 'CMarketingIntegrationSubscription' ) && true == is_null( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'internet_listing_service_id', __( 'Duplicate records with same ILS name are not allowed.' ) ) );
		}

		if( true == valObj( $objMarketingIntegrationSubscription, 'CMarketingIntegrationSubscription' ) && false == is_null( $this->getId() ) && $this->getId() != $objMarketingIntegrationSubscription->getId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'internet_listing_service_id', __( 'Duplicate records with same ILS name are not allowed.' ) ) );
		}

		return $boolIsValid;
	}

	public function validatePropertyForIlsApiNew( $strValidationAction, $arrmixValidate, $boolIsLimited = false, $boolIsShowMissingInfoData = false, $boolIsStudentProperty = false ) {

		// ignore validations if property is student property for now
		if( true == $boolIsStudentProperty ) {
			return true;
		}

		$boolIsValidProperty = true;
		$arrstrMissingInfoData = [];

		switch( $strValidationAction ) {
			case 'property':
				if( true == isset( $arrmixValidate['propertyAddress'] ) ) {
					$objPropertyAddress = $arrmixValidate['propertyAddress'];

					if( true == is_null( $objPropertyAddress )
					    || false == valObj( $objPropertyAddress, 'CPropertyAddress' )
					    || ( 0 == mb_strlen( $objPropertyAddress->getStreetLine1() ) && 0 == mb_strlen( $objPropertyAddress->getStreetLine2() ) && 0 == mb_strlen( $objPropertyAddress->getStreetLine3() ) )
					    || 0 == mb_strlen( $objPropertyAddress->getCity() )
					    || ( 0 == mb_strlen( $objPropertyAddress->getStateCode() ) && 'US' == $objPropertyAddress->getCountryCode() )
					    || 0 == mb_strlen( $objPropertyAddress->getPostalCode() ) ) {
						if( true == $boolIsShowMissingInfoData ) {
							$arrstrMissingInfoData['address'] = 'Property Address is missing.';
						}
						$boolIsValidProperty &= false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'address', __( 'Property Address' ) ) );
					}
				} else {
					if( true == $boolIsShowMissingInfoData ) {
						$arrstrMissingInfoData['address'] = 'Property Address is missing.';
					}
					$boolIsValidProperty &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'address', __( 'Property Address' ) ) );
				}

				if( false == $boolIsLimited && true == isset( $arrmixValidate['PropertyPhotoCompanyMediaFiles'] ) ) {
					$arrobjPropertyPhotoCompanyMediaFiles = $arrmixValidate['PropertyPhotoCompanyMediaFiles'];
					if( false == valArr( $arrobjPropertyPhotoCompanyMediaFiles ) ) {

						if( true == $boolIsShowMissingInfoData ) {
							$arrstrMissingInfoData['photo'] = __( 'Property Photo is missing.' );
						}

						$boolIsValidProperty &= false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_photos', __( 'Property Photos' ) ) );
					}
				}
				break;

			case 'floorPlans':
				if( true == isset( $arrmixValidate['propertyFloorPlan'] ) ) {
					$objPropertyFloorplan = $arrmixValidate['propertyFloorPlan'];

					if( true == $objPropertyFloorplan->getIsPublished() ) {

						// FLOOR PLAN RENT Validation
						if( true == $objPropertyFloorplan->getIsManualRentRange() && ( ( true == is_null( $objPropertyFloorplan->getMinRent() ) || 1 > $objPropertyFloorplan->getMinRent() ) || ( true == is_null( $objPropertyFloorplan->getMaxRent() ) || 1 > $objPropertyFloorplan->getMaxRent() ) ) ) {
							if( true == $boolIsShowMissingInfoData ) {
								$arrstrMissingInfoData['rent'] = 'Floorplan ' . $objPropertyFloorplan->getFloorplanName() . ' : Rent is missing.';
							}
							$boolIsValidProperty &= false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Rent' ), $objPropertyFloorplan->getId() ) );
						}

						// NO OF BATHROOMS Validation
						if( true == is_null( $objPropertyFloorplan->getNumberOfBathrooms() ) || 1 > $objPropertyFloorplan->getNumberOfBathrooms() ) {
							if( true == $boolIsShowMissingInfoData ) {
								$arrstrMissingInfoData['bathroom'] = 'Floorplan ' . $objPropertyFloorplan->getFloorplanName() . ' : No. Of Bathrooms are missing.';
							}
							$boolIsValidProperty &= false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'floorplan_no_of_bathrooms', __( 'No. Of Bathrooms' ), $objPropertyFloorplan->getId() ) );
						}

						// NO OF BEDROOMS Validation
						if( true == is_null( $objPropertyFloorplan->getNumberOfBedrooms() ) ) {
							if( true == $boolIsShowMissingInfoData ) {
								$arrstrMissingInfoData['bedroom'] = 'Floorplan ' . $objPropertyFloorplan->getFloorplanName() . ' : No. Of Bedrooms are missing.';
							}
							$boolIsValidProperty &= false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'floorplan_no_of_bedrooms', __( 'No. Of Bedrooms' ), $objPropertyFloorplan->getId() ) );
						}
					}

				} else {

					if( true == $boolIsShowMissingInfoData ) {
						$arrstrMissingInfoData[] = 'Floorplan is missing.';
					}
					$boolIsValidProperty &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_floorplan', __( 'Missing Floorplans' ) ) );
				}
				break;

			case 'unit':
				break;

			default:
				if( false == $boolIsLimited ) {
					if( true == isset( $arrmixValidate['propertyAddress'] ) ) {
						$objPropertyAddress = $arrmixValidate['propertyAddress'];

						if( true == is_null( $objPropertyAddress )
						    || false == valObj( $objPropertyAddress, 'CPropertyAddress' )
						    || ( 0 == mb_strlen( $objPropertyAddress->getStreetLine1() ) && 0 == mb_strlen( $objPropertyAddress->getStreetLine2() ) && 0 == mb_strlen( $objPropertyAddress->getStreetLine3() ) )
						    || 0 == mb_strlen( $objPropertyAddress->getCity() )
						    || 0 == mb_strlen( $objPropertyAddress->getStateCode() )
						    || 0 == mb_strlen( $objPropertyAddress->getPostalCode() ) ) {

							$boolIsValidProperty &= false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'address', __( 'Property Address' ) ) );
						}

						if( true == isset( $arrmixValidate['PropertyPhotoCompanyMediaFiles'] ) ) {
							$arrobjPropertyPhotoCompanyMediaFiles = $arrmixValidate['PropertyPhotoCompanyMediaFiles'];
							if( false == valArr( $arrobjPropertyPhotoCompanyMediaFiles ) ) {
								$boolIsValidProperty &= false;
								$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_photos', __( 'Property Photos' ) ) );
							}
						}
					} else {
						$boolIsValidProperty &= false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'address', __( 'Property Address' ) ) );
					}
				}
				break;
		}

		if( true == $boolIsShowMissingInfoData ) {
			return $arrstrMissingInfoData;
		}

		return $boolIsValidProperty;
	}

	public function suspendMarketingIntegrationSubscription() {

		$this->setIlsNotifiedOn( NULL );
		$this->setNotificationConfirmedOn( NULL );
		$this->setNotificationRejectedOn( NULL );
		$this->setChangeNotifiedOn( NULL );
		$this->setSuspendedOn( date( 'm/d/Y H:i:s' ) );
	}

}
?>