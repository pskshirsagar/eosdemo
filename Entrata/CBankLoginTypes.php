<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBankLoginTypes
 * Do not add any new functions to this class.
 */

class CBankLoginTypes extends CBaseBankLoginTypes {

	public static function fetchBankLoginTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CBankLoginType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchBankLoginType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CBankLoginType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}
}
?>