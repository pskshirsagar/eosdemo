<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CVehicles
 * Do not add any new functions to this class.
 */

class CVehicles extends CBaseVehicles {

	public static function fetchCompanyVehiclesByCid( $intCid, $objDatabase ) {

 		$strSql = 'SELECT
 						v.*
 					FROM
 						vehicles v
 					WHERE
 						v.cid = ' . ( int ) $intCid . '
 						AND v.is_personal = 0
 						AND v.deleted_on IS NULL 
 					ORDER BY ' . $objDatabase->getCollateSort( 'v.name', true );

		return parent::fetchObjects( $strSql, CVehicle::class, $objDatabase );
	}

	public static function fetchPrivateVehicleByCid( $intCid, $objDatabase ) {
		$strSql = ' SELECT
						v.*
					FROM
						vehicles v
					WHERE
						v.cid = ' . ( int ) $intCid . '
						AND v.is_personal = 1';
		return self::fetchVehicle( $strSql, $objDatabase );
	}

	public static function fetchSimplePrivateVehicleByCid( $intCid, $objDatabase ) {
		$strSql = ' SELECT
						v.*
					FROM
						vehicles v
					WHERE
						v.cid = ' . ( int ) $intCid . '
						AND v.is_personal = 1
						AND v.deleted_on IS NULL ';

		return fetchData( $strSql, $objDatabase );
	}

}
?>