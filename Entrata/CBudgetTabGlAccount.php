<?php

class CBudgetTabGlAccount extends CBaseBudgetTabGlAccount {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBudgetId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBudgetTabId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBudgetDataSourceTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBudgetDataSourceSubTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFormula() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAdjustmentPercent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>