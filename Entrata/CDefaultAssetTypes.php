<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultAssetTypes
 * Do not add any new functions to this class.
 */

class CDefaultAssetTypes extends CBaseDefaultAssetTypes {

	public static function fetchDefaultAssetTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDefaultAssetType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDefaultAssetType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDefaultAssetType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}
}
?>