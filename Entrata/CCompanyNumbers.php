<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyNumbers
 * Do not add any new functions to this class.
 */

class CCompanyNumbers extends CBaseCompanyNumbers {

	public static function fetchNextCompanyNumber( $strColumn, $intCid, $intCompanyUserId, $objDatabase ) {
		return self::fetchCompanyNumber( sprintf( 'SELECT * FROM get_next_company_number_by_column_name(%1$d, \'%2$s\', %3$d)', ( int ) $intCid, ( string ) addslashes( $strColumn ), ( int ) $intCompanyUserId ), $objDatabase );
	}

	public static function fetchNextLeaseNumber( $intCid, $intCompanyUserId, $objDatabase ) {
		return self::fetchNextCompanyNumber( 'lease_number', $intCid, $intCompanyUserId, $objDatabase );
	}

	public static function fetchNextGlHeaderNumber( $intCid, $intCompanyUserId, $objDatabase ) {
		return self::fetchNextCompanyNumber( 'gl_header_number', $intCid, $intCompanyUserId, $objDatabase );
	}

	public static function fetchNextPoNumber( $intCid, $intCompanyUserId, $objDatabase ) {
		return self::fetchNextCompanyNumber( 'po_number', $intCid, $intCompanyUserId, $objDatabase );
	}

	public static function fetchNextScheduledApTransactionHeaderNumber( $intCid, $intCompanyUserId, $objDatabase ) {
		return self::fetchNextCompanyNumber( 'scheduled_ap_transaction_header_number', $intCid, $intCompanyUserId, $objDatabase );
	}
}
?>