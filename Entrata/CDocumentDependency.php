<?php

class CDocumentDependency extends CBaseDocumentDependency {

	const OPTION_LEASE_LENGTH_COMPARISON_GREATER_THAN_EQUAL_TO  = '>=';
	const OPTION_LEASE_LENGTH_COMPARISON_EQUAL_TO               = '=';
	const OPTION_LEASE_LENGTH_COMPARISON_LESS_THAN_EQUAL_TO     = '<=';

	const OPTION_LEASE_LENGTH_UNIT_DAYS   = 'days';
	const OPTION_LEASE_LENGTH_UNIT_WEEKS  = 'weeks';
	const OPTION_LEASE_LENGTH_UNIT_MONTHS = 'months';
	const OPTION_LEASE_LENGTH_UNIT_YEARS  = 'years';

    /**
     * Validate Functions
     */

    public function valCid() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getCid() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required.' ) );
    	}

    	return $boolIsValid;
    }

    public function valDocumentId() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getDocumentId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_id', 'Main document is required.' ) );
    	}

    	return $boolIsValid;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDocumentAddendaId() {
        $boolIsValid = true;

        if( true == is_null( $this->getDocumentAddendaId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_addenda_id', 'Document Addenda is required.' ) );
        }

        return $boolIsValid;
    }

    public function valDocumentDependencyTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getDocumentDependencyTypeId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_dependency_type_id', 'Document Dependency Type is required.' ) );
        }

        return $boolIsValid;
    }

    public function valIncludeDocumentAddendaId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReferenceIds() {
        $boolIsValid = true;
        switch( $this->getDocumentDependencyTypeId() ) {
        	case CDocumentDependencyType::CHARGE_CODE:
        		if( false == valArr( $this->getReferenceIds() ) ) {
        			$boolIsValid = false;
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_applicable_codes_rules', 'Please select charge codes.' ) );
        		}
        		break;

        	case CDocumentDependencyType::PET:
        		if( false == valArr( $this->getReferenceIds() ) ) {
        			$boolIsValid = false;
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_applicable_pet_rules', 'Please select pets.' ) );
        		}
        		break;

        	case CDocumentDependencyType::ADD_ON:
        		if( false == valArr( $this->getReferenceIds() ) ) {
        			$boolIsValid = false;
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_applicable_add_on_rules', 'Please select rentable items.' ) );
        		}
        		break;

        	case CDocumentDependencyType::LEASE_TYPE:
        		if( false == valArr( $this->getReferenceIds() ) ) {
        			$boolIsValid = false;
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_applicable_lease_type_rules', 'Please select lease types.' ) );
        		}
        		break;

        	case CDocumentDependencyType::VEHICLE:
        		break;

        	case CDocumentDependencyType::UNIT_AMENITIES:
        		if( false == valArr( $this->getReferenceIds() ) ) {
        			$boolIsValid = false;
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_applicable_amenities_rules', 'Please select amenities.' ) );
        		}
        		break;

        	case CDocumentDependencyType::PROPERTY_GROUPS:
        		if( false == valArr( $this->getReferenceIds() ) ) {
        			$boolIsValid = false;
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_group', 'Please select property.' ) );
        		}
        		break;

        	default:
        		NULL;
        }

        return $boolIsValid;
    }

    public function valMinLimit() {
        $boolIsValid = true;

        if( true == is_null( $this->getMinLimit() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_limit', 'Minimum age is required.' ) );
        }

        return $boolIsValid;
    }

    public function valMaxLimit() {
        $boolIsValid = true;

        if( true == is_null( $this->getMaxLimit() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_limit', 'Maximum age is required.' ) );
        }
        if( true == $boolIsValid ) {
        	if( $this->getMaxLimit() <= $this->getMinLimit() ) {
        		$boolIsValid = false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_limit', 'Maximum age should be greater than minimum age.' ) );
        	}
        }

        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsInclude() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valLeaseLengthComparison() {
		$boolIsValid = true;

		if( true == is_null( $this->getLeaseLengthComparison() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_length_comparison', 'Lease Length comparison is required.' ) );
		}

		return $boolIsValid;
	}

	public function valLeaseLength() {
		$boolIsValid = true;

		if( true == is_null( $this->getLeaseLength() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_length', 'Lease length is required.' ) );
		}

		return $boolIsValid;
	}

	public function valLeaseLengthUnit() {
		$boolIsValid = true;

		if( true == is_null( $this->getLeaseLengthUnit() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_length_unit', 'Lease length unit is required.' ) );
		}

		return $boolIsValid;
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        		$boolIsValid &= $this->valCid();
        		$boolIsValid &= $this->valDocumentId();
        		$boolIsValid &= $this->valDocumentAddendaId();
        		$boolIsValid &= $this->valDocumentDependencyTypeId();
        		if( CDocumentDependencyType::APPLICANT_AGE == $this->getDocumentDependencyTypeId() ) {
        			$boolIsValid &= $this->valMinLimit();
        			$boolIsValid &= $this->valMaxLimit();
        		}
		        if( CDocumentDependencyType::LEASE_LENGTH == $this->getDocumentDependencyTypeId() ) {
			        $boolIsValid &= $this->valLeaseLengthComparison();
			        $boolIsValid &= $this->valLeaseLength();
			        $boolIsValid &= $this->valLeaseLengthUnit();
		        }
        		$boolIsValid &= $this->valReferenceIds();
        		break;

        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

	public static function getLeaseLengthComparisonOptions( $strKey = '' ) {

		$arrstrLeaseLengthComparisonOptions = [
			SELF::OPTION_LEASE_LENGTH_COMPARISON_GREATER_THAN_EQUAL_TO  => SELF::OPTION_LEASE_LENGTH_COMPARISON_GREATER_THAN_EQUAL_TO,
			SELF::OPTION_LEASE_LENGTH_COMPARISON_EQUAL_TO               => SELF::OPTION_LEASE_LENGTH_COMPARISON_EQUAL_TO,
			SELF::OPTION_LEASE_LENGTH_COMPARISON_LESS_THAN_EQUAL_TO     => SELF::OPTION_LEASE_LENGTH_COMPARISON_LESS_THAN_EQUAL_TO
		];

		return  ( ( true == valStr( $strKey ) ) ?  $arrstrLeaseLengthComparisonOptions[$strKey] : $arrstrLeaseLengthComparisonOptions );
	}

	public static function getLeaseLengthUnitOptions( $strKey = '' ) {

		$arrstrLeaseLengthComparisonOptions = [
			SELF::OPTION_LEASE_LENGTH_UNIT_DAYS   => __( 'Day(s)' ),
			SELF::OPTION_LEASE_LENGTH_UNIT_WEEKS  => __( 'Week(s)' ),
			SELF::OPTION_LEASE_LENGTH_UNIT_MONTHS => __( 'Month(s)' ),
			SELF::OPTION_LEASE_LENGTH_UNIT_YEARS  => __( 'Year(s)' )
		];

		return  ( ( true == valStr( $strKey ) ) ?  $arrstrLeaseLengthComparisonOptions[$strKey] : $arrstrLeaseLengthComparisonOptions );
	}

}
?>