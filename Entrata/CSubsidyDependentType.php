<?php

class CSubsidyDependentType extends CBaseSubsidyDependentType {

	const JOINT_CUSTODY_RECEIVING_ALLOWANCE			= 1;
	const JOINT_CUSTODY_WITHOUT_DEPENDENT_ALLOWANCE	= 2;
	const JOINT_CUSTODY_FULL_TIME_RESIDENT			= 3;

	public static $c_arrintJointCustodyCodes = [
		self::JOINT_CUSTODY_RECEIVING_ALLOWANCE,
		self::JOINT_CUSTODY_FULL_TIME_RESIDENT
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>