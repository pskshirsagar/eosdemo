<?php

class CWebsitesSnippetActivityLog extends CBaseWebsitesSnippetActivityLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWebsiteId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWebsiteSnippetTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSnippetCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insertOrUpdateSnippetLog( $intClientId, $intUserId, $intWebsiteId, $intPropertyId, $intSnippetType, $strSnippetCode, $objDatabase ) {

		$intCount = $this->loadSnippetLogByCIdByWebsiteIdBySnippetType( $intClientId, $intWebsiteId, $intPropertyId, $intSnippetType, $objDatabase );

		$this->setCid( $intClientId );
		$this->setWebsiteId( $intWebsiteId );
		$this->setPropertyId( $intPropertyId );
		$this->setWebsiteSnippetTypeId( $intSnippetType );
		$this->setSnippetCode( $strSnippetCode );
		$this->setUpdatedBy( $intUserId );
		$this->setUpdatedOn( 'now()' );
		$this->setCreatedBy( $intUserId );
		$this->setCreatedOn( 'now()' );

		if( 0 == $intCount ) {
			// insert
			$this->insert( $intUserId, $objDatabase );
		} else {
			// update
			$this->update( $intUserId, $objDatabase );
		}
	}

	public function loadSnippetLogByCIdByWebsiteIdBySnippetType( $intClientId, $intWebsiteId, $intPropertyId, $intSnippetType, $objDatabase ) {

		$strWhere = ' WHERE cid = ' . ( int ) $intClientId . ' AND website_id = ' . ( int ) $intWebsiteId . ' AND website_snippet_type_id = ' . ( int ) $intSnippetType;
		if( NULL == $intPropertyId ) {
			$strWhere .= ' AND property_id IS NULL';
		} else {
			$strWhere .= ' AND property_id = ' . ( int ) $intPropertyId;
		}
		$intCount = \Psi\Eos\Entrata\CWebsitesSnippetActivityLogs::createService()->fetchWebsitesSnippetActivityLogCount( $strWhere, $objDatabase );

		return $intCount;
	}

}
?>