<?php

class CChecklist extends CBaseChecklist {

	const DEFAULT_SUBSIDIZED_HUD_CHECKLIST	= 1;

	public function valDependentInformation( $objDatabase ) {

		$strFromClause = ' property_checklists pc
							JOIN property_gl_settings pgs ON ( pc.cid = pgs.cid AND pc.property_id = pgs.property_id )
							JOIN property_products pp ON ( pc.cid = pp.cid AND ( pc.property_id = pp.property_id OR pp.property_id IS NULL ) )';

		$strWhereClause = ' WHERE
								pc.cid = ' . ( int ) $this->getCid() . '
								AND pc.checklist_id = ' . ( int ) $this->getId() . '
								AND pgs.activate_standard_posting = true
								AND pp.ps_product_id = ' . CPsProduct::ENTRATA;

		if( 0 < ( int ) CPropertyChecklists::fetchRowCount( $strWhereClause, $strFromClause, $objDatabase ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Checklist cannot be deleted because it is associated to one or more Properties.' ), NULL ) );
			return false;
		}

		return true;
	}

	public function valOccupancyTypeIds() {
		$boolIsValid = true;
		if( false == valArr( $this->getOccupancyTypeIds() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'occupancy_types', __( 'Please select at least one occupancy type.' ) ) );
		}
		return $boolIsValid;
	}

	public function valChecklistTriggerId() {
		$boolIsValid = true;

		if( true == is_null( $this->getChecklistTriggerId() ) ) {
			$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'insert_checklist_before', __( 'Please select checklist type.' ) ) );
		}

		return $boolIsValid;
	}

	public function valName() {

		$boolIsValid = true;
		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Checklist title is required.' ) ) );
		}

		if( false == is_null( $this->getName() ) && 200 < \Psi\CStringService::singleton()->strlen( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', __( 'Checklist title should not exceed {%d, 0} characters.', [ 200 ] ) ) );
		}
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', __( 'Please add checklist description.' ) ) );
		}
		return $boolIsValid;
	}

	public function valDefaultChecklist() {
		$boolIsValid = true;
		if( true == $this->getIsSystem() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'checklist', __( 'You cannot delete default checklist.' ) ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valChecklistTriggerId();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valOccupancyTypeIds();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDefaultChecklist();
				$boolIsValid &= $this->valDependentInformation( $objDatabase );
				break;

			default:
				// default case
				break;
		}

		return $boolIsValid;
	}

	public function createChecklistItem() {

		$objChecklistItem = new CChecklistItem();
		$objChecklistItem->setCid( $this->getCid() );
		$objChecklistItem->setChecklistId( $this->getId() );

		return $objChecklistItem;
	}

	public function createPropertyChecklist( $intPropertyId ) {

		$objPropertyChecklist = new CPropertyChecklist();
		$objPropertyChecklist->setCid( $this->getCid() );
		$objPropertyChecklist->setChecklistId( $this->getId() );
		$objPropertyChecklist->setPropertyId( $intPropertyId );

		return $objPropertyChecklist;
	}

	/*
	 * Overriding delete function from base file to always soft delete checklist.
	 */

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsHardDelete = false ) {

		if( true == $boolIsHardDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( 'NOW()' );
			return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

}
?>