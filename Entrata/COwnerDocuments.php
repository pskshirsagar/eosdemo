<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COwnerDocuments
 * Do not add any new functions to this class.
 */

class COwnerDocuments extends CBaseOwnerDocuments {

	public static function fetchOwnerDocumentsByDocumentIdOwnerIdsByCid( $intDocumentId, $arrintOwnerIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintOwnerIds ) ) return NULL;

		$strSql = 'SELECT *
					FROM
						owner_documents
					WHERE
						document_id =' . ( int ) $intDocumentId . '
						AND ap_payee_id IN(' . implode( ',', $arrintOwnerIds ) . ')
						AND cid = ' . ( int ) $intCid;

		return self::fetchOwnerDocuments( $strSql, $objDatabase );
	}

	public static function fetchOwnerDocumentsByCidDocumentIdOwnerIds( $intCid, $arrintOwnerIds, $intDocumentId, $objDatabase ) {
		if( false == valArr( $arrintOwnerIds ) ) return NULL;

		$strDocumentSql = '';

		if( 0 != $intDocumentId ) $strDocumentSql = ' AND document_id =' . ( int ) $intDocumentId . ' ';

		$strSql = 'SELECT *
					FROM
						owner_documents
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strDocumentSql . '
						AND ap_payee_id IN(' . implode( ',', $arrintOwnerIds ) . ')';

		return self::fetchOwnerDocuments( $strSql, $objDatabase );
	}

}
?>