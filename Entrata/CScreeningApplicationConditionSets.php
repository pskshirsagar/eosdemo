<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningApplicationConditionSets
 * Do not add any new functions to this class.
 */
class CScreeningApplicationConditionSets extends CBaseScreeningApplicationConditionSets {

	public static function fetchAppliedConditionsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolFetchSatisfiedConditionsOnly = false, $boolIncludeGuarantorCondition = false ) {
		$strWhere = '';

		if( true == $boolFetchSatisfiedConditionsOnly ) {
			$strWhere = ' AND ( ( sacv.satisfied_on IS NOT NULL AND sacv.satisfied_by IS NOT NULL )';

			if( true == $boolIncludeGuarantorCondition ) {
				$strWhere .= ' OR sacv.condition_type_id = ' . ( int ) CScreeningPackageConditionType::ADD_GUARANTOR;
			}

			$strWhere .= ')';
		}

		$strSql = 'SELECT
						sacs.*,
						sacv.id as condition_value_id,
						sacv.condition_subset_value_id,
						sacv.condition_name,
						sacv.condition_type_id,
						sacv.condition_amount_type_id,
						sacv.base_charge_code_id,
						sacv.apply_to_charge_code_id,
						sacv.ar_trigger_id,
						sacv.condition_value,
						sacv.condition_subset_id,
						sacv.satisfied_on as subset_satisfied_on,
						sacv.ar_transaction_id,
						sacv.scheduled_charge_id,
						sacv.final_value,
						sacv.screening_available_condition_id
					FROM
						screening_application_condition_sets sacs
						JOIN screening_application_condition_values sacv ON ( sacs.id = sacv.screening_application_condition_set_id AND sacv.cid = sacs.cid )
					WHERE
						sacs.application_id = ' . ( int ) $intApplicationId . '
						AND sacv.cid = ' . ( int ) $intCid . '
						AND sacs.is_active = 1
						AND sacv.is_reverted = 0
						' . $strWhere . '
					ORDER BY
						sacv.id';

		return self::fetchScreeningApplicationConditionSets( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchAllConditionsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						sacs.*,
						sacv.id as condition_value_id,
						sacv.condition_subset_value_id,
						sacv.condition_name,
						sacv.condition_type_id,
						sacv.condition_amount_type_id,
						sacv.base_charge_code_id,
						sacv.apply_to_charge_code_id,
						sacv.ar_trigger_id,
						sacv.condition_value,
						sacv.condition_subset_id,
						sacv.satisfied_on as subset_satisfied_on,
						sacv.ar_transaction_id,
						sacv.scheduled_charge_id,
						sacv.final_value,
						sacv.screening_available_condition_id
					FROM
						screening_application_condition_sets sacs
						JOIN screening_application_condition_values sacv ON ( sacs.id = sacv.screening_application_condition_set_id AND sacv.cid = sacs.cid )
					WHERE
						sacs.application_id = ' . ( int ) $intApplicationId . '
						AND sacv.cid = ' . ( int ) $intCid . '
						AND sacs.is_active = 1
					ORDER BY
						sacv.id';

		return self::fetchScreeningApplicationConditionSets( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	// need to change the function name
	// this determines whether the subsets have satisfied their required items and whether we are ready to go to mark the overall condition set satiisfied

	public static function isConditionSubsetsSatisfied( $intConditionSetId, $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						sacs.*,
						sacv.condition_subset_id,
						sacv.satisfied_on AS subset_satisfied_on,
						sacv.satisfied_by AS subset_satisfied_by
					FROM
						screening_application_condition_sets sacs
	 					JOIN screening_application_condition_values sacv ON ( sacs.id = sacv.screening_application_condition_set_id AND sacv.cid = sacs.cid AND sacs.is_active =1
	 																			AND ( sacs.condition_subset_one_id = sacv.condition_subset_id OR sacs.condition_subset_two_id = sacv.condition_subset_id ) )
					WHERE
						sacs.condition_set_id = ' . ( int ) $intConditionSetId . '
						AND sacs.application_id = ' . ( int ) $intApplicationId . '
						AND sacv.is_reverted = 0
						AND sacv.cid = ' . ( int ) $intCid . '
					ORDER BY
						sacv.condition_subset_id';

		return self::fetchScreeningApplicationConditionSets( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchActiveScreeningApplicationConditionSetByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		if( true == is_null( $intApplicationId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						screening_application_condition_sets
					WHERE
						application_id = ' . ( int ) $intApplicationId . '
						AND cid = ' . ( int ) $intCid . '
						AND is_active = 1';

		 return $objScreeningApplicationConditionSet = self::fetchScreeningApplicationConditionSets( $strSql, $objDatabase );

	}

	public static function fetchActiveScreeningApplicationConditionSetsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		if( true == is_null( $intApplicationId ) ) return NULL;

		$strSql = '	SELECT
						*
					FROM
						screening_application_condition_sets
					WHERE
						application_id = ' . ( int ) $intApplicationId . '
						AND cid = ' . ( int ) $intCid . '
						AND is_active = 1';

		return self::fetchScreeningApplicationConditionSets( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );

	}

	public static function fetchScreeningApplicationConditionSetsByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strSql = '	SELECT
						DISTINCT ( application_id ), satisfied_on
					FROM
						screening_application_condition_sets sacs
					WHERE
						is_active = 1 AND
						application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function areApplicationScreeningConditionsSatisfied( $intApplicationId, $intCid, $objDatabase ) {
		if( true == is_null( $intApplicationId ) ) return NULL;

		$strSql = '	SELECT
						id
					FROM
						screening_application_condition_sets
					WHERE
						cid = ' . ( int ) $intCid . '
						AND application_id = ' . ( int ) $intApplicationId . '
						AND satisfied_by IS NULL AND satisfied_on IS NULL
						AND is_active = 1';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		return ( false == valArr( $arrmixResult ) ) ? true : false;

	}

	public static function fetchScreeningConditionSatisfiedApplicationByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		if( true == is_null( $intApplicationId ) ) return NULL;

		$strSql = '	SELECT
						1
					FROM
						screening_application_condition_sets sacs
						JOIN screening_application_requests sap ON ( sacs.cid = sap.cid AND sacs.application_id = sap.application_id AND sacs.screening_application_request_id = sap.id )
						JOIN applicant_applications aa ON ( aa.cid = sacs.cid AND aa.application_id = sacs.application_id )
						JOIN screening_applicant_results sar ON ( sap.cid = sar.cid AND sar.application_id = sap.application_id AND sar.screening_application_request_id = sap.id AND sar.applicant_id = aa.applicant_id )
						JOIN file_associations fa ON ( fa.cid = aa.cid AND fa.application_id = aa.application_id AND fa.applicant_id = aa.applicant_id AND fa.deleted_by IS NULL AND fa.deleted_on IS NULL )
                        JOIN files f ON ( f.cid = fa.cid AND f.id = fa.file_id )
                        JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid AND ft.system_code = \'LP\' )
					WHERE
						sacs.application_id = ' . ( int ) $intApplicationId . '
						AND sacs.cid = ' . ( int ) $intCid . '
						AND sap.screening_decision_type_id = ' . CScreeningDecisionType::APPROVE_WITH_CONDITIONS . '
						AND sacs.satisfied_by IS NULL AND sacs.satisfied_on IS NULL
						AND sacs.is_active = 1
						AND aa.lease_signed_on IS NOT NULL
						AND aa.lease_signature IS NOT NULL
						AND aa.lease_ip_address IS NOT NULL 
						AND f.countersigned_by IS NULL
                        AND f.countersigned_on IS NULL ';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrmixResult ) ) ? true : false;

	}

}
?>