<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerConciergeServices
 * Do not add any new functions to this class.
 */

class CCustomerConciergeServices extends CBaseCustomerConciergeServices {

	public static function fetchPaginatedCustomerConciergeServicesByCidByPropertyIds( $intPageNumber, $intPageSize, $intCid, $arrintPropertyIds, $objDatabase, $boolIsRemovedConciergeServices = false ) {
		if( false == valArr( $arrintPropertyIds ) || false == is_numeric( $intCid ) ) return NULL;

		$intOffset = ( 0 < $intPageNumber ) ? $intPageSize * ( $intPageNumber - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSql = 'SELECT
						ccs.*
					 FROM
						customer_concierge_services ccs
					 WHERE
						ccs.cid = ' . ( int ) $intCid . '
					 	AND ccs.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')';

		$strSql .= ( true == $boolIsRemovedConciergeServices ) ? ' AND ccs.deleted_on IS NOT NULL ORDER BY ccs.updated_on DESC' : ' AND ccs.deleted_on IS NULL ORDER BY ccs.id DESC';

		$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;

		return self::fetchCustomerConciergeServices( $strSql, $objDatabase );
	}

	public static function fetchConciergeServicesCountByCidByPropertyIds( $intCid, $arrintPropertyIds, $objDatabase, $boolIsRemovedConciergeServices = false ) {
		if( false == valArr( $arrintPropertyIds ) || false == is_numeric( $intCid ) ) return NULL;

		$strWhere = ' WHERE cid = ' . ( int ) $intCid . ' AND property_id  IN (' . implode( ',', $arrintPropertyIds ) . ')';
		$strWhere .= ( true == $boolIsRemovedConciergeServices ) ? ' AND deleted_on IS NOT NULL' : ' AND deleted_on IS NULL';

		return self::fetchCustomerConciergeServiceCount( $strWhere, $objDatabase );
	}

	public static function fetchCustomerConciergeServiceByIdByCid( $intId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM customer_concierge_services WHERE id = ' . ( int ) $intId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchCustomerConciergeService( $strSql, $objDatabase );
	}

	public static function fetchCustomerConciergeServiceByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM customer_concierge_services WHERE id IN (' . implode( ',', $arrintIds ) . ') AND cid = ' . ( int ) $intCid;
		return self::fetchCustomerConciergeServices( $strSql, $objDatabase );
	}

	public static function fetchCustomerConciergeServicesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		if( false == is_numeric( $intCustomerId ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						datetime_submitted,
						concierge_specifics
					FROM
						customer_concierge_services
					WHERE
						customer_id = ' . $intCustomerId . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchCustomerConciergeServices( $strSql, $objDatabase );
	}

	public function fetchCustomerConciergeServicesByIdByCustomerIdByPropertyIdByCid( $intConciergeServiceId, $intPropertyId, $intCustomerId, $intCid, $objDatabase ) {
		if( false == valId( $intConciergeServiceId ) || false == valId( $intPropertyId ) || false == valId( $intCustomerId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						customer_concierge_services
					WHERE
						id = ' . ( int ) $intConciergeServiceId . '
						AND customer_id = ' . ( int ) $intCustomerId . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchCustomerConciergeService( $strSql, $objDatabase );
	}

	public static function fetchPaginatedCustomerConciergeServicesByDashboardFilterByCid( $objDashboardFilter, $intCid, $intOffset, $intLimit, $strSortBy, $objDatabase ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSql = 'SELECT
						*
					FROM (
						SELECT
							CASE
								WHEN TRIM( dp.resident_portal_concierge->>\'urgent_concierge_requested_since\' ) != \'\' AND date_part( \'day\', CURRENT_DATE - ccs.datetime_submitted ) >= ( dp.resident_portal_concierge->>\'urgent_concierge_requested_since\' )::int THEN
									3
								WHEN TRIM( dp.resident_portal_concierge->>\'important_concierge_requested_since\' ) != \'\' AND date_part( \'day\', CURRENT_DATE - ccs.datetime_submitted ) >= ( dp.resident_portal_concierge->>\'important_concierge_requested_since\' )::int THEN
									2
								ELSE
									1
							END AS priority,
							ccs.id,
							cl.id AS lease_id,
							ccs.customer_id AS customer_id,
							ccs.datetime_submitted AS requested,
							ccs.concierge_specifics AS concierge_specifics,
							ccs.viewed_on,
							func_format_customer_name( ccs.customer_name_first_encrypted, ccs.customer_name_last_encrypted ) as customer_name,
							cl.property_name AS property,
							MAX( cl.id ) OVER( PARTITION BY ccs.id ORDER BY cl.id DESC ) AS max_lease_id,
							COALESCE ( cl.building_name || \' - \' || cl.unit_number_cache, cl.building_name, cl.unit_number_cache ) AS unit_number
						FROM
							customer_concierge_services ccs
							JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) AS lp ON ( ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? 'lp.is_disabled = 0 AND' : '' ) . ' lp.property_id = ccs.property_id )
							JOIN customers c ON ( c.id = ccs.customer_id AND c.cid = ccs.cid )
							JOIN lease_customers lc ON ( lc.customer_id = c.id AND lc.cid = ccs.cid )
							LEFT JOIN cached_leases cl ON ( cl.id = lc.lease_id AND cl.cid = ccs.cid AND cl.property_id = ccs.property_id )
							LEFT JOIN dashboard_priorities dp ON ( dp.cid = ccs.cid )
						WHERE
							ccs.cid = ' . ( int ) $intCid . '
							AND ccs.customer_id IS NOT NULL
							AND ccs.deleted_on IS NULL
					) subQ
					WHERE
						subQ.lease_id = subQ.max_lease_id
						' . $strPriorityWhere . '
					ORDER BY ' . $strSortBy . '
					OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchConciergeServicesCountByDashboardFilterByCid( $objDashboardFilter, $intCid, $objDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSql = ( 0 != $intMaxExecutionTimeOut )? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';

		$strSql .= 'SELECT
						count( 1 ) AS ' .
						( false == $boolIsGroupByProperties ?
							'concierge_services_count,
							MAX'
						:
							'resident_concierge,
							property_id,
							property_name,
						' ) .
						'( priority ) AS priority
					FROM (
						SELECT
							CASE
								WHEN TRIM( dp.resident_portal_concierge->>\'urgent_concierge_requested_since\' ) != \'\' AND date_part( \'day\', CURRENT_DATE - ccs.datetime_submitted ) >= ( dp.resident_portal_concierge->>\'urgent_concierge_requested_since\' )::int THEN
									3
								WHEN TRIM( dp.resident_portal_concierge->>\'important_concierge_requested_since\' ) != \'\' AND date_part( \'day\', CURRENT_DATE - ccs.datetime_submitted ) >= ( dp.resident_portal_concierge->>\'important_concierge_requested_since\' )::int THEN
									2
								ELSE
									1
							END AS priority,
							cl.id as lease_id,
							cl.property_id,
							p.property_name,
							MAX( cl.id ) OVER ( PARTITION BY ccs.id ORDER BY cl.id DESC ) AS max_lease_id
						FROM
							customer_concierge_services ccs
							JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) AS lp ON ( lp.property_id = ccs.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
							JOIN properties p ON ( p.cid = ccs.cid AND p.id = ccs.property_id )
							JOIN customers c ON ( c.id = ccs.customer_id AND c.cid = ccs.cid )
							JOIN lease_customers lc ON ( lc.customer_id = c.id AND lc.cid = ccs.cid )
							LEFT JOIN cached_leases cl ON ( cl.id = lc.lease_id AND cl.cid = ccs.cid AND cl.property_id = ccs.property_id )
							LEFT JOIN dashboard_priorities dp ON ( dp.cid = ccs.cid )
						WHERE
							ccs.cid = ' . ( int ) $intCid . '
							AND ccs.customer_id IS NOT NULL
							AND ccs.deleted_on IS NULL
						GROUP BY
							cl.id,
							ccs.id,
							ccs.datetime_submitted,
							dp.resident_portal_concierge->>\'urgent_concierge_requested_since\',
							dp.resident_portal_concierge->>\'important_concierge_requested_since\',
							cl.property_id,
							p.property_name
						) subQ
					WHERE
						subQ.lease_id = subQ.max_lease_id
						' . $strPriorityWhere . ' ' .
					( true == $boolIsGroupByProperties ?
						' GROUP BY
							property_id,
							property_name,
							priority'
					: '' );

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchPaginatedCustomerConciergeServiceByCompanyConciergeServiceIdByPropertyIdByCid( $intConciergeServiceId, $intPropertyId, $intCid, $objPagination, $objDatabase ) {
		if( false == is_numeric( $intConciergeServiceId ) || false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;
		$strOffsetLimit = ( false == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) ? '' : 'OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();

		$strSql = 'SELECT
  						DISTINCT ccs.id,
   						ccs.*
					FROM
    					customer_concierge_services ccs
    					JOIN company_concierge_services ccse ON( ccs.cid = ccse.cid )
    					JOIN property_concierge_services pcs ON ( ccs.property_id = pcs.property_id  AND ccs.cid = pcs.cid )
					WHERE
    					pcs.property_id = ' . $intPropertyId . '
    					AND pcs.company_concierge_service_id =' . $intConciergeServiceId . '
    					AND ccse.id = ' . $intConciergeServiceId . '
    					AND ccs.cid = ' . $intCid . '
    					AND ccs.deleted_on IS NULL
    					AND ccs.deleted_by IS NULL
    					AND ccs.concierge_specifics LIKE \'%' . $intConciergeServiceId . '%\'
    				ORDER BY ccs.datetime_submitted DESC ' . $strOffsetLimit;

		return self::fetchCustomerConciergeServices( $strSql, $objDatabase );
	}

	public static function fetchPaginatedCustomerConciergeServiceByFilterByConciergeServiceIdsByCid( $objCustomerServicesFilter, $arrintConciergeServiceIds, $intCid, $objPagination, $objDatabase ) {

		if( false == valArr( $arrintConciergeServiceIds ) ) return false;

		$intSelectedProperty = NULL;
		$strWhereCondition = NULL;
		$strConciergeServiceIdCondition = NULL;

		if( true == valId( $objCustomerServicesFilter->getSelectedPropertyId() ) ) {
			$intSelectedProperty = $objCustomerServicesFilter->getSelectedPropertyId();
		} else {
			$intSelectedProperty = reset( $objCustomerServicesFilter->getPropertyIds() );
		}

		if( false == valId( $intSelectedProperty ) ) return false;

		$strOffsetLimit = ( false == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) ? '' : 'OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();

		if( true == valStr( $objCustomerServicesFilter->getResidentName() ) ) {
			$arrstrExplodedResidentNames = explode( ' ', trim( $objCustomerServicesFilter->getResidentName() ) );
			if( true == valArr( $arrstrExplodedResidentNames ) && 1 < \Psi\Libraries\UtilFunctions\count( $arrstrExplodedResidentNames ) ) {
				$arrstrOrParameters = [];
				foreach( $arrstrExplodedResidentNames as $strExplodedResidentName ) {
					if( true === strrchr( $strExplodedResidentName, '\'' ) ) {
						$strFilteredExplodedResidentName = $strExplodedResidentName;
					} else {
						$strFilteredExplodedResidentName = '\' || \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strExplodedResidentName ) ) . '\' || \'';
					}
					array_push( $arrstrOrParameters, $strFilteredExplodedResidentName );
				}

				$arrstrOrParameters = array_filter( $arrstrOrParameters );

				if( true == valArr( $arrstrOrParameters ) ) {
					$strWhereCondition = 'AND ( c.name_first ILIKE E\'%' . implode( '%\' OR c.name_first ILIKE E\'%', $arrstrOrParameters ) . '%\' )';
					$strWhereCondition .= ' AND ( c.name_last ILIKE E\'%' . implode( '%\' OR c.name_last ILIKE E\'%', $arrstrOrParameters ) . '%\' )';
				}
			} else {
				$strWhereCondition = 'AND ( lower( c.name_first ) LIKE E\'%' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $objCustomerServicesFilter->getResidentName() ) ) ) . '%\' OR lower( c.name_last ) LIKE E\'%' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $objCustomerServicesFilter->getResidentName() ) ) ) . '%\' OR lower( l.unit_number_cache ) LIKE E\'%' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $objCustomerServicesFilter->getResidentName() ) ) ) . '%\' )';
			}
		}

		$strSql = 'SELECT
  						DISTINCT ON ( ccs.id )
						ccs.*
					FROM
    					customer_concierge_services ccs
    					JOIN company_concierge_services ccse ON( ccs.cid = ccse.cid )
						LEFT JOIN customers c ON( ccs.customer_id = c.id AND ccs.cid = c.cid )
						LEFT JOIN lease_customers as lc ON( c.id = lc.customer_id AND c.cid = lc.cid )
						LEFT JOIN cached_leases as l ON( lc.lease_id = l.id  AND lc.cid = l.cid )
    					JOIN property_concierge_services pcs ON ( ccs.property_id = pcs.property_id  AND ccs.cid = pcs.cid )
					WHERE
    					pcs.property_id = ' . $intSelectedProperty . '
    					AND ccs.cid = ' . ( int ) $intCid . '
    					AND ccs.deleted_on IS NULL
    					AND ccs.deleted_by IS NULL
    					AND rtrim( substring( ccs.concierge_specifics, \'d*([0-9]{1,6})\' ) )::int IN( ' . implode( ',', $arrintConciergeServiceIds ) . ' )
    					' . $strWhereCondition . '
    				ORDER BY
    					ccs.id DESC,
    					ccs.datetime_submitted DESC ' . $strOffsetLimit;

		return self::fetchCustomerConciergeServices( $strSql, $objDatabase );
	}

	public static function fetchCountCustomerConciergeServiceByCompanyConciergeServiceIdsByPropertyIdByCid( $arrintConciergeServiceIds, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintConciergeServiceIds ) || false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ccs.id,
						rtrim( substring( ccs.concierge_specifics, \'d*([0-9]{1,6})\' ) ) as count
					FROM
						customer_concierge_services ccs
					WHERE
						ccs.property_id = ' . $intPropertyId . '
						AND ccs.cid = ' . ( int ) $intCid . '
						AND ccs.deleted_on IS NULL
						AND ccs.deleted_by IS NULL
						AND rtrim( substring( ccs.concierge_specifics, \'d*([0-9]{1,6})\' ) )::int IN( ' . implode( ',', $arrintConciergeServiceIds ) . ' )
					ORDER BY
						rtrim( substring( ccs.concierge_specifics, \'d*([0-9]{1,6})\') )';

		return fetchData( $strSql, $objDatabase );
	}

	public function fetchSimpleCustomerConciergeServicesByCustomerIdByPropertyIdByCid( $intCustomerId, $intPropertyId, $intCid, $objDatabase, $boolIsReturnLastRecord = false ) {
		if( false == valId( $intCustomerId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						id,
						concierge_specifics,
						datetime_submitted,
						customer_email_address_encrypted,
						customer_phone_number_encrypted
					FROM
						customer_concierge_services
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND customer_id = ' . ( int ) $intCustomerId . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL';

		if( true == $boolIsReturnLastRecord ) {
			$strSql .= ' ORDER BY id DESC LIMIT 1';
		}

		return fetchData( $strSql, $objDatabase );
	}

}
?>
