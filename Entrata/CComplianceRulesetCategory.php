<?php

class CComplianceRulesetCategory extends CBaseComplianceRulesetCategory {

	protected $m_strVendorCategoryTypeIds;

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valComplianceRulesetId() {
		return true;
	}

	public function valVendorCategoryTypeId() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setVendorCategoryTypeIds( $strVendorCategoryTypeIds ) {
		$this->m_strVendorCategoryTypeIds = $strVendorCategoryTypeIds;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['vendor_category_type_ids'] ) ) {
			$this->setVendorCategoryTypeIds( $arrmixValues['vendor_category_type_ids'] );
		}
	}

	public function getVendorCategoryTypeIds() {
		return $this->m_strVendorCategoryTypeIds;
	}

	// @TODO: Temprory code, once done with migration in Entrata will remove this code.

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( CWorker::USER_ID_FLAG_TO_MIGRATE_COMPLIANCE_DATA != $intCurrentUserId ) {
			return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false );
		} else {
			$this->setDatabase( $objDatabase );

			$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

			$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, compliance_ruleset_id, vendor_category_type_id, created_by, created_on )
					VALUES ( ' .
							$strId . ', ' .
							$this->sqlCid() . ', ' .
							$this->sqlComplianceRulesetId() . ', ' .
							$this->sqlVendorCategoryTypeId() . ', ' .
							$this->sqlCreatedBy() . ', ' .
							$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

			if( true == $boolReturnSqlOnly ) {
				return $strSql;
			} else {
				return $this->executeSql( $strSql, $this, $objDatabase );
			}
		}
	}

}
?>