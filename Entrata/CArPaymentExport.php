<?php

class CArPaymentExport extends CBaseArPaymentExport {

	protected $m_intPaymentCount;
	protected $m_intSumPaymentAmount;

    /**
     * Set Functions
     */

    public function getPaymentCount() {
		return $this->m_intPaymentCount;
	}

	public function getSumPaymentAmount() {
		return $this->m_intSumPaymentAmount;
	}

	/**
	 * Set Functions
	 */

	public function setPaymentCount( $intPaymentCount ) {
		$this->m_intPaymentCount = $intPaymentCount;
	}

	public function setSumPaymentAmount( $intSumPaymentAmount ) {
		$this->m_intSumPaymentAmount = $intSumPaymentAmount;
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['payment_count'] ) )				$this->setPaymentCount( $arrValues['payment_count'] );
		if( true == isset( $arrValues['sum_payment_amount'] ) )			$this->setSumPaymentAmount( $arrValues['sum_payment_amount'] );

		return;
	}

	/**
	 * Validate Functions
	 */

    public function valId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        if( true == is_null( $this->getId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Ar payment export id is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        if( true == is_null( $this->getCid() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id is requird.' ) ) );
        }

        return $boolIsValid;
    }

    public function valCompanyMerchantAccountId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        if( true == is_null( $this->getCompanyMerchantAccountId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_merchant_account_id', __( 'Company merchant account id is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valExportDatetime() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        if( true == is_null( $this->getExportDatetime() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'export_datetime', __( 'Export datetime is required.' ) ) );
        }

        if( 0 < strlen( trim( $this->getExportDatetime() ) ) && 1 != CValidation::validateDate( $this->getExportDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'export_datetime', __( 'Export datetime is not formatted correctly.' ) ) );
		}

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {

            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valCompanyMerchantAccountId();
            	$boolIsValid &= $this->valExportDatetime();
				break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valId();
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>