<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CEntityTypes
 * Do not add any new functions to this class.
 */

class CEntityTypes extends CBaseEntityTypes {

	public static function fetchEntityTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEntityType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchEntityType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEntityType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSimpleEntityTypes( $objDatabase ) {
		$strSql = ' SELECT * FROM entity_types WHERE is_published = 1 ORDER BY ' . $objDatabase->getCollateSort( 'name', true, true );
		return self::fetchEntityTypes( $strSql, $objDatabase );
	}

	public static function fetchEntityTypesForModulePermissions( $objDatabase, $boolIsDocManagement = true ) {

		if( false == $boolIsDocManagement ) {
			$strSql = 'SELECT 				
			   CASE
			        WHEN name = \'Lead\' THEN \'Lead/Applicant\'
			        WHEN name = \'Resident\' THEN \'Application/Resident\'
			        ELSE name
			   END as name,
			   CASE
					WHEN name = \'Resident\' THEN \'' . __( 'Application/Resident' ) . '\'
					ELSE util_get_system_translated( \'name\', name, details )
				END AS translated_entity_name';
		} else {
			$strSql = 'SELECT 				
			   CASE
			        WHEN name = \'Lead\' THEN \'Lead/Applicant\'
			        ELSE name
			   END as name,
			   CASE
					WHEN name = \'Resident\' THEN \'' . __( 'Resident/Tenant' ) . '\'
					WHEN name = \'Lead\' THEN \'' . __( 'Lead/Applicant' ) . '\'
					ELSE util_get_system_translated( \'name\', name, details )
				END AS translated_entity_name';
		}

		$strSql .= '
			FROM 
				entity_types 
			WHERE 
				is_published = 1 ORDER BY ' . $objDatabase->getCollateSort( 'name', true, true );

		return fetchData( $strSql, $objDatabase );
	}

}
?>