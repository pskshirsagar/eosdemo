<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMilitaryOfficerStructures
 * Do not add any new functions to this class.
 */

class CMilitaryOfficerStructures extends CBaseMilitaryOfficerStructures {

	public static function fetchMilitaryOfficerStructures( $strSql, $objDatabase ) {
		return self::fetchObjects( $strSql, CMilitaryOfficerStructure::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMilitaryOfficerStructure( $strSql, $objDatabase ) {
		return self::fetchObject( $strSql, CMilitaryOfficerStructure::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllMilitaryOfficerStructures( $objDatabase ) {

		$strSql = 'SELECT
					 *
					FROM
					    military_officer_structures
					WHERE
					    is_published = TRUE
					ORDER BY
					    id';
		return self::fetchMilitaryOfficerStructures( $strSql, $objDatabase );
	}

}
?>