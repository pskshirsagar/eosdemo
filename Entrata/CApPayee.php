<?php

use Psi\Eos\Entrata\CJobFundingSources;

class CApPayee extends CBaseApPayee {

	protected $m_boolIsPrimary;
	protected $m_boolIsSelectAllProperties;
	protected $m_boolReceives1099;
	protected $m_boolIsTransactionAssociated;

	protected $m_objApPayeeDetail;
	protected $m_objApPayeeContact;

	protected $m_intForm1099TypeId;
	protected $m_intApPaymentTypeId;
	protected $m_intApLegalEntityId;
	protected $m_intApRemittanceId;
	protected $m_intApPayeeAccountId;
	protected $m_intApPayeeLocationId;
	protected $m_intForm1099BoxTypeId;
	protected $m_intApPayeeAccountNumber;
	protected $m_intPropertyId;
	protected $m_intRecipientId;
	protected $m_intLocationsApRemittanceId;
	protected $m_intApPayeeContactId;
	protected $m_intIntercompanyPropertyId;
	protected $m_intApRoutingTagId;
	protected $m_intOwnerId;

	protected $m_intApPayeeContactLocationId;

	protected $m_fltPaymentAmount;
	protected $m_fltPreviousBalance;

	protected $m_strCity;
	protected $m_strPayeeName;
	protected $m_strStateCode;
	protected $m_strCountryCode;
	protected $m_strPostalCode;
	protected $m_strVendorCode;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strStreetLine3;
	protected $m_strLocationName;
	protected $m_strApPrimaryContact;
	protected $m_strApPaymentTypeName;
	protected $m_strApPayeeStatusType;
	protected $m_strDefaultGlAccountName;
	protected $m_strEmailAddress;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strPropertyName;
	protected $m_strPhoneNumber;
	protected $m_strApPayeeName;
	protected $m_strApPayeeAddress;
	protected $m_strRemittanceName;
	protected $m_strRemittanceType;
	protected $m_strRecipientTin;
	protected $m_strPostDate;
	protected $m_strPostMonth;
	protected $m_strOwnerName;

	protected $m_arrobjApPayeeProperties;
	protected $m_arrobjApPayeePropertyGroups;
	protected $m_arrobjApPayee1099Adjustments;

	protected $m_arrmixInsurancePolicyDetails;
	protected $m_arrstrMergeFields;
	protected $m_arrmixVendorInformationEvent;
	protected $m_arrmixVendorStatusEvent;
	protected $m_arrmixApPayeeCustomData;

	const LOOKUP_TYPE_AP_CODE_VENDOR									= 'ap_code_vendor';
	const LOOKUP_TYPE_VENDORS_WITH_WARRANTY_CONTACTS					= 'vendors_with_warranty_contacts';
	const LOOKUP_TYPE_VENDORS_WITHOUT_WARRANTY_CONTACTS					= 'vendors_without_warranty_contacts';
	const LOOKUP_TYPE_AP_CODE_VENDORS_ASSOCIATED_WITH_CATALOG_ITEMS		= 'ap_code_vendors_associated_with_catalog_items';
	const IMPORT_AP_PAYEES												= 'import_ap_payees';
	const IMPORT_AP_PAYEE_REMITTANCES									= 'import_ap_payee_remittance';

	const IS_REMOVE_FROM_INVITE_VENDORS = 'is_remove_from_invite_vendors';

	public function __construct() {
		parent::__construct();
		$this->m_boolIsTransactionAssociated    = false;
		return;
	}

	public function valApPayeeTermId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApPayeeTermId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_term_id', 'Payment term is required.' ) );
		}

		return $boolIsValid;
	}

	public function valGlAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->getGlAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', 'Gl account is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyName( $boolIsMandatory = false ) {
		$boolIsValid = true;

		if( true == $boolIsMandatory && true == is_null( $this->getCompanyName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_name', 'Business name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valVendorId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPayee1099Adjustments() {
		$boolIsValid = true;
		$arrobjTempApPayee1099Adjustments = $this->getApPayee1099Adjustments();
		if( true == valArr( $this->getApPayee1099Adjustments() ) ) {
			foreach( $this->getApPayee1099Adjustments() as $intKey => $objApPayee1099Adjustment ) {
				foreach( $arrobjTempApPayee1099Adjustments as $intTempKey  => $objTempApPayee1099Adjustment ) {
					if( $intKey != $intTempKey ) {
						if( $objApPayee1099Adjustment->getPropertyId() == $objTempApPayee1099Adjustment->getPropertyId() && false == is_null( $objApPayee1099Adjustment->getYear() ) && $objApPayee1099Adjustment->getYear() == $objTempApPayee1099Adjustment->getYear() ) {
							$boolIsValid = false;
							$objApPayee1099Adjustment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_name', 'Property can not be used for same year.' ) );
						}
					}
				}

				if( true == valArr( $this->getApPayeeProperties() ) && false == array_key_exists( $objApPayee1099Adjustment->getPropertyId(), $this->getApPayeeProperties() ) && false == is_null( $objApPayee1099Adjustment->getPropertyId() ) ) {
					$boolIsValid = false;
					$objApPayee1099Adjustment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Please select approved property.' ) );
				}
			}
		}
		return $boolIsValid;
	}

	public function valWebsiteUrl() {
		$boolIsValid = true;
		$strUrlValidatorRegEx = '@^(http\:\/\/|https\:\/\/)?([a-z0-9][a-z0-9\-]*\.)+[a-z0-9][a-z0-9\-]*[\/a-z0-9\-]*$@i';

		if( false == is_null( $this->getWebsiteUrl() ) && false == preg_match( $strUrlValidatorRegEx, $this->getWebsiteUrl() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'website_url', 'Invalid website url format.' ) );
		}

		return $boolIsValid;
	}

	public function valApPayeePropertyGroups() {
		$boolIsValid = true;

		if( false == valArr( $this->getApPayeePropertyGroups() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property', 'At least one property group is required.' ) );
		}

		return $boolIsValid;
	}

	public function valVendorType( $objClientDatabase ) {
		$boolIsValid			= true;
		$arrstrFeeTemplateName	= [];

		$strWhere = 'WHERE ap_payee_type_id =' . CApPayeeType::INTERCOMPANY . ' AND ap_payee_status_type_id =' . CApPayeeStatusType::ACTIVE . ' AND cid =' . $this->getCid();

		$intClientVendorCount = CApPayees::fetchApPayeeCount( $strWhere, $objClientDatabase );

		if( 1 == $intClientVendorCount && CApPayeeStatusType::ACTIVE == $this->getApPayeeStatusTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_id', 'Vendor type cannot be changed, at least one client vendor must be active.' ) );
			$boolIsValid &= false;
		}

		$arrobjActiveFeeTemplates = ( array ) \Psi\Eos\Entrata\CFeeTemplates::createService()->fetchActiveFeeTemplatesByApPayeeLocationIdByCid( $this->getApPayeeLocationId(), $this->getCid(), $objClientDatabase );

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrobjActiveFeeTemplates ) ) {
			$arrstrFeeTemplateName = rekeyObjects( 'Name', $arrobjActiveFeeTemplates );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_id', 'Vendor type cannot be changed, it is associated with management fee or owner distribution template(s) \'' . implode( '\', \'', array_keys( $arrstrFeeTemplateName ) ) . '\'.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;

	}

	public function valStatus( $objClientDatabase ) {

		$boolIsValid			= true;
		$arrstrFeeTemplateName	= [];

		$strWhere = 'WHERE ap_payee_type_id =' . CApPayeeType::INTERCOMPANY . ' AND ap_payee_status_type_id =' . CApPayeeStatusType::ACTIVE . ' AND cid =' . $this->getCid();

		$intClientVendorCount = CApPayees::fetchApPayeeCount( $strWhere, $objClientDatabase );

		if( 1 == $intClientVendorCount && CApPayeeStatusType::ACTIVE == $this->getApPayeeStatusTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_id', 'Vendor status cannot be changed, at least one client vendor must be active.' ) );
			$boolIsValid &= false;
		}

		$arrobjActiveFeeTemplates = ( array ) \Psi\Eos\Entrata\CFeeTemplates::createService()->fetchActiveFeeTemplatesByApPayeeLocationIdByCid( $this->getApPayeeLocationId(), $this->getCid(), $objClientDatabase );

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrobjActiveFeeTemplates ) ) {
			$arrstrFeeTemplateName = rekeyObjects( 'Name', $arrobjActiveFeeTemplates );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_id', 'Vendor status cannot be changed, it is associated with management fee or owner distribution template(s) \'' . implode( '\', \'', array_keys( $arrstrFeeTemplateName ) ) . '\'.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valForm1099BoxType() {

		$boolIsValid = true;

		if( true == valId( $this->getForm1099TypeId() ) && false == valId( $this->getForm1099BoxTypeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'box_number', 'Box Number is required if a 1099 Type is selected.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valDuplicateCustomDataKey( $boolIsKeyExists ) {
		$boolIsValid = true;
		if( true == $boolIsKeyExists ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'details', 'Key already exists.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase = NULL, $boolIsRequired = true, $arrmixDetailsToValidate = [] ) {
		$boolIsValid = true;
		$boolIsKeyExists = ( true == isset( $arrmixDetailsToValidate['is_key_exists'] ) ) ? $arrmixDetailsToValidate['is_key_exists'] : false;
		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCompanyName( true );
				$boolIsValid &= $this->valApPayeeTermId();
				if( true == $boolIsRequired ) {
					$boolIsValid &= $this->valGlAccountId();
				}
				break;

			case VALIDATE_DELETE:
				break;

			case 'vendor_insert':
				$boolIsValid &= $this->valCompanyName( true );
				$boolIsValid &= $this->valWebsiteUrl();
				$boolIsValid &= $this->valApPayee1099Adjustments();
				$boolIsValid &= $this->valApPayeeTermId();
				break;

			case 'vendor_update':
				$boolIsValid &= $this->valCompanyName( true );
				$boolIsValid &= $this->valApPayee1099Adjustments();
				break;

			case 'validate_owner':
				$boolIsValid &= $this->valCompanyName( true );
				break;

			case 'validate_ap_payee_type':
				$boolIsValid &= $this->valVendorType( $objClientDatabase );
				break;

			case 'validate_vendor_status':
				$boolIsValid &= $this->valStatus( $objClientDatabase );
				break;

			case 'validate_approved_properties':
				$boolIsValid &= $this->valApPayeePropertyGroups();
				break;

			case 'update_ventor':
				$boolIsValid &= $this->valCompanyName( true );
				$boolIsValid &= $this->valWebsiteUrl();
				$boolIsValid &= $this->valApPayeeTermId();
				break;

			case 'insert_custom_data':
				$boolIsValid &= $this->valDuplicateCustomDataKey( $boolIsKeyExists );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 */

	public function createApLegalEntity() {

		$objApLegalEntity = new CApLegalEntity();
		$objApLegalEntity->setCid( $this->getCid() );
		$objApLegalEntity->setApPayeeId( $this->getId() );

		return $objApLegalEntity;
	}

	public function createApPayeeContactLocation() {

		$objApPayeeContactLocation = new CApPayeeContactLocation();
		$objApPayeeContactLocation->setCid( $this->getCid() );

		return $objApPayeeContactLocation;
	}

	public function createApPayeeAccount() {

		$objApPayeeAccount = new CApPayeeAccount();
		$objApPayeeAccount->setApPayeeId( $this->getId() );
		$objApPayeeAccount->setCid( $this->getCid() );

		return $objApPayeeAccount;
	}

	public function createApRemittance() {

		$objApRemittance = new CApRemittance();
		$objApRemittance->setApPayeeId( $this->getId() );
		$objApRemittance->setCid( $this->getCid() );

		return $objApRemittance;
	}

	public function createApPayeeContact() {

		$objApPayeeContact = new CApPayeeContact();
		$objApPayeeContact->setApPayeeId( $this->getId() );
		$objApPayeeContact->setCid( $this->getCid() );
		$objApPayeeContact->setIsPrimary( true );

		return $objApPayeeContact;
	}

	public function createApPayeeLocation() {

		$objApPayeeLocation = new CApPayeeLocation();
		$objApPayeeLocation->setDefaults();
		$objApPayeeLocation->setApPayeeId( $this->getId() );
		$objApPayeeLocation->setCid( $this->getCid() );
		$objApPayeeLocation->setLocationDatetime( 'NOW()' );

		return $objApPayeeLocation;
	}

	public function createApHeader() {

		$objApHeader = new CApHeader();
		$objApHeader->setDefaults();
		$objApHeader->setCid( $this->getCid() );
		$objApHeader->setApPayeeId( $this->getId() );
		$objApHeader->setApPayeeRequirePoForInvoice( $this->getRequirePoForInvoice() );
		$objApHeader->setGlTransactionTypeId( CGlTransactionType::AP_CHARGE );
		$objApHeader->setTransactionDatetime( 'NOW()' );
		$objApHeader->setPostDate( date( 'm/d/Y' ) );
		$objApHeader->setPostMonth( date( 'm/1/Y' ) );
		return $objApHeader;
	}

	public function createScheduledApTransactionHeader() {

		$objScheduledApTransactionHeader = new CScheduledApTransactionHeader();

		$objScheduledApTransactionHeader->setApPayeeId( $this->getId() );
		$objScheduledApTransactionHeader->setCid( $this->getCid() );

		return $objScheduledApTransactionHeader;
	}

	public function createApDetail() {

		$objApDetail = new CApDetail();
		$objApDetail->setDatetime( date( 'm/d/Y h:i:s', mktime() ) );

		return $objApDetail;
	}

	public function createComplianceDoc() {

		$objComplianceDoc = new CComplianceDoc();
		$objComplianceDoc->setCid( $this->getCid() );
		$objComplianceDoc->setApPayeeId( $this->getId() );
		$objComplianceDoc->setDocDatetime( 'NOW()' );

		return $objComplianceDoc;
	}

	public function createFileAssociation() {

		$objFileAssociation = new CFileAssociation();

		$objFileAssociation->setCid( $this->getCid() );
		$objFileAssociation->setApPayeeId( $this->getId() );

		return $objFileAssociation;
	}

	public function createApPayees1099Adjustment() {

		$objApPayee1099Adjustment = new CApPayee1099Adjustment();
		$objApPayee1099Adjustment->setDefaults();
		$objApPayee1099Adjustment->setCid( $this->getCid() );
		$objApPayee1099Adjustment->setApPayeeId( $this->getId() );

		return $objApPayee1099Adjustment;
	}

	public function createApPayeePropertyGroup() {

		$objApPayeePropertyGroup = new CApPayeePropertyGroup();
		$objApPayeePropertyGroup->setCid( $this->getCid() );
		$objApPayeePropertyGroup->setApPayeeId( $this->getId() );

		return $objApPayeePropertyGroup;
	}

	public function createOwner() {

		$objOwner = new COwner();
		$objOwner->setDefaults();
		$objOwner->setCid( $this->getCid() );
		$objOwner->setApPayeeId( $this->getId() );

		return $objOwner;
	}

	public function createApPayee1099Adjustment() {
		$objApPayee1099Adjustment = new CApPayee1099Adjustment();

		$objApPayee1099Adjustment->setApPayeeId( $this->getId() );
		$objApPayee1099Adjustment->setCid( $this->getCid() );

		return $objApPayee1099Adjustment;
	}

	private function createTempTableApHeadersByCidByPropertyIds( $arrmixParameters, $intCid, $objClientDatabase ) {

		$boolIsConfidential         = getArrayElementByKey( 'is_confidential',          $arrmixParameters );
		$arrintPropertyIds          = getArrayElementByKey( 'property_ids',             $arrmixParameters );
		$objApTransactionsFilter    = getArrayElementByKey( 'ap_transactions_filter',   $arrmixParameters );
		$arrintApHeaderSubTypeIds   = getArrayElementByKey( 'ap_header_sub_type_ids',   $arrmixParameters );

		if( false == valArr( $arrintApHeaderSubTypeIds ) ) {
			return false;
		}

		if( 't' == fetchData( 'SELECT util.util_temp_table_exists(\'temp_ap_headers\') AS result;', $objClientDatabase )[0]['result'] ) {
			return;
		}

		$strConditions				= NULL;
		$arrstrAndConditions		= [];

		$arrintApFinancialStatusTypeIds = [ CApFinancialStatusType::PENDING, CApFinancialStatusType::APPROVED, CApFinancialStatusType::PARTIALLY_INVOICED, CApFinancialStatusType::CLOSED, CApFinancialStatusType::CANCELLED, CApFinancialStatusType::REJECTED ];

		if( false == $boolIsConfidential ) {
			$strConditions 				.= ' AND ad.is_confidential = false';
			$strTransactionAmountLabel	= ' SUM( ad.transaction_amount ) OVER ( PARTITION BY ah.id )';
		} else {
			$strTransactionAmountLabel	= ' SUM( ad.transaction_amount ) OVER ( PARTITION BY ah.id )';
		}

		if( true == valObj( $objApTransactionsFilter, CApTransactionsFilter::class ) ) {

			if( true == valArr( $objApTransactionsFilter->getPropertyIds() ) ) {
				$arrstrAndConditions[] = 'ad.property_id IN( ' . implode( ',', $objApTransactionsFilter->getPropertyIds() ) . ' )';
			} else {
				$arrstrAndConditions[] = 'ad.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )';
			}

			if( true == valArr( $objApTransactionsFilter->getApPayeeLocationIds() ) ) {
				$arrstrAndConditions[] = 'apl.id IN( ' . implode( ',', $objApTransactionsFilter->getApPayeeLocationIds() ) . ')';
			}

			if( true == valStr( $objApTransactionsFilter->getHeaderNumber() ) ) {
				$arrstrAndConditions[] = 'ah.header_number ILIKE \'%' . addslashes( trim( $objApTransactionsFilter->getHeaderNumber() ) ) . '%\' ';
			}

			if( true == is_numeric( $objApTransactionsFilter->getCreatedBy() ) ) {
				$arrstrAndConditions[] = 'ah.created_by = ' . ( int ) $objApTransactionsFilter->getCreatedBy();
			}

			if( true == valStr( $objApTransactionsFilter->getCreatedOnFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getCreatedOnFrom() ) ) {
				$arrstrAndConditions[] = 'ah.transaction_datetime >= \'' . date( 'm/d/Y', strtotime( $objApTransactionsFilter->getCreatedOnFrom() ) ) . ' 00:00:00\'';
			}

			if( true == valStr( $objApTransactionsFilter->getCreatedOnTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getCreatedOnTo() ) ) {
				$arrstrAndConditions[] = 'ah.transaction_datetime <= \'' . date( 'm/d/Y', strtotime( $objApTransactionsFilter->getCreatedOnTo() ) ) . ' 23:59:59\'';
			}

			if( true == valArr( $objApTransactionsFilter->getApPhysicalStatusTypeIds() ) ) {
				$arrstrAndConditions[] = ' ah.ap_physical_status_type_id IN( ' . sqlIntImplode( $objApTransactionsFilter->getApPhysicalStatusTypeIds() ) . ' )';
			}

			if( true == valArr( $objApTransactionsFilter->getApFinancialStatusTypeIds() ) ) {
				$arrstrAndConditions[] = ' ah.ap_financial_status_type_id IN( ' . sqlIntImplode( $objApTransactionsFilter->getApFinancialStatusTypeIds() ) . ' )';
			}

		}

		if( true == valArr( $objApTransactionsFilter->getApPoTypeIds() ) ) {
			if( true == valIntArr( $objApTransactionsFilter->getJobIds() ) && true == in_array( CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER, $objApTransactionsFilter->getApPoTypeIds() ) ) {
				$arrintPoTypeIds = array_flip( $objApTransactionsFilter->getApPoTypeIds() );
				unset( $arrintPoTypeIds[CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER] );
				if( 1 == \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApPoTypeIds() ) ) {
					$arrstrAndConditions[] = ' ( ah.ap_header_sub_type_id IN ( ' . implode( ',', $objApTransactionsFilter->getApPoTypeIds() ) . ' ) AND jp.job_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobIds() ) . ' ) )';
				} else {
					$arrstrAndConditions[] = ' ( ah.ap_header_sub_type_id IN ( ' . implode( ',', array_flip( $arrintPoTypeIds ) ) . ' ) OR ( ah.ap_header_sub_type_id = ' . CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER . ' AND jp.job_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobIds() ) . ' ) ) )';
				}
			} else {
				$arrstrAndConditions[] = ' ah.ap_header_sub_type_id IN ( ' . implode( ',', $objApTransactionsFilter->getApPoTypeIds() ) . ' )';
			}
		}

		$arrstrAndConditions[] = ' ah.ap_header_sub_type_id IN ( ' . implode( ',', $arrintApHeaderSubTypeIds ) . ' )';

		if( true == valArr( $arrstrAndConditions ) ) {
			$strConditions .= ' AND ' . implode( ' AND ', $arrstrAndConditions );
		}

		$strOrderBy		= 'ah.created_on DESC, ah.header_number::integer DESC';
		$arrstrSortBy	= [ 'ah.header_number', 'apl.location_name', 'created_on', 'property_name', 'ap_financial_status_type_id', 'ap_physical_status_type_id','transaction_amount', 'apl.vendor_code' ];

		switch( $objApTransactionsFilter->getSortBy() ) {

			case 'header_number':
				$objApTransactionsFilter->setSortBy( 'ah.header_number' );
				break;

			case 'location_name':
				$objApTransactionsFilter->setSortBy( 'apl.location_name' );
				break;

			case 'vendor_code':
				$objApTransactionsFilter->setSortBy( 'apl.vendor_code' );
				break;

			default:
				NULL;
				break;
		}

		if( true == valStr( $objApTransactionsFilter->getSortBy() ) && true == valStr( $objApTransactionsFilter->getSortDirection() )
		    && true == in_array( $objApTransactionsFilter->getSortBy(), $arrstrSortBy ) ) {

			if( 'ah.header_number' == $objApTransactionsFilter->getSortBy() ) {
				$strSortBy		= 'ah.header_number::integer';
			} else {
				if( in_array( $objApTransactionsFilter->getSortBy(), [ 'property_name', 'apl.location_name', 'apl.vendor_code', 'transaction_amount' ] ) ) {
					$strSortBy		= $objApTransactionsFilter->getSortBy();
				} else {
					$strSortBy		= 'ah.' . $objApTransactionsFilter->getSortBy();
				}
			}

			$strOrderBy	= $strSortBy . ' ' . $objApTransactionsFilter->getSortDirection();
		}

		$strSql = 'DROP TABLE IF EXISTS pg_temp.temp_ap_headers;
					CREATE TEMP TABLE temp_ap_headers AS (
						SELECT
							DISTINCT ah.id,
							ah.cid,
							ah.header_number::INTEGER,
							ah.ap_payee_id,
							apl.location_name,
							apl.vendor_code,
							ah.ap_physical_status_type_id,
							ah.ap_financial_status_type_id,
							ah.created_on,
							apl.id AS ap_payee_location_id,
							' . $strTransactionAmountLabel . ' AS transaction_amount,
							(
								SELECT
									CASE
										WHEN 1 < COUNT ( DISTINCT ap_sub_select.property_id ) THEN \'Multiple\'
										ELSE p.property_name
									END AS property_name
								FROM
									ap_details ap_sub_select
								WHERE
									ap_sub_select.ap_header_id = ah.id
									AND ap_sub_select.cid = ah.cid
									AND ap_sub_select.cid = ' . ( int ) $intCid . '
									AND ap_sub_select.deleted_by IS NULL
									AND ap_sub_select.deleted_on IS NULL
							) AS property_name
						FROM
							ap_headers ah
							JOIN ap_details ad ON ( ah.id = ad.ap_header_id AND ah.cid = ad.cid )
							JOIN properties p ON ( p.id = ad.property_id AND p.cid = ad.cid )
							LEFT JOIN ap_payee_locations apl ON ( apl.id = ah.ap_payee_location_id AND apl.cid = ah.cid )
							LEFT JOIN job_phases jp ON ( ad.cid = jp.cid AND ad.job_phase_id = jp.id )
						WHERE
							ah.cid = ' . ( int ) $intCid . '
							AND ah.ap_payee_id = ' . ( int ) $this->getId() . '
							AND ah.ap_header_type_id = ' . $objApTransactionsFilter->getApHeaderTypeId() . '
							AND ah.ap_financial_status_type_id IN ( ' . implode( ',', $arrintApFinancialStatusTypeIds ) . ' )
							' . $strConditions . '
							AND ah.deleted_by IS NULL
							AND ah.deleted_on IS NULL
							AND ah.is_template = false
							AND ad.deleted_by IS NULL
							AND ad.deleted_on IS NULL
						ORDER BY ' . $strOrderBy . ' 
					);
					ANALYZE pg_temp.temp_ap_headers;';

		fetchData( $strSql, $objClientDatabase );

		return true;
	}

	private function createTempTableApTransactionsByPropertyIds( $arrintPropertyIds, $objClientDatabase, $arrintInvoiceTypePermissions, $objApTransactionsFilter ) {

		if( 't' == fetchData( 'SELECT util.util_temp_table_exists(\'temp_ap_transactions\') AS result;', $objClientDatabase )[0]['result'] ) {
			return;
		}

		if( false == valArr( $arrintInvoiceTypePermissions ) ) {
			return NULL;
		}

		$strOrderBy				= 'ap_header_id DESC';
		$arrstrSortBy			= [ 'header_number', 'location_name', 'vendor_account_number', 'vendor_code', 'post_date', 'due_date', 'is_posted', 'transaction_amount', 'transaction_amount_paid' ];
		$strConditions			= '';
		$arrstrAndConditions 	= [];
		$arrstrOrConditions     = [];

		if( true == valStr( $objApTransactionsFilter->getSortBy() ) && true == in_array( $objApTransactionsFilter->getSortBy(), $arrstrSortBy ) ) {

			$strOrderBy = $objApTransactionsFilter->getSortBy() . ' ' . $objApTransactionsFilter->getSortDirection() . ', LOWER( ah.header_number ) ' . $objApTransactionsFilter->getSortDirection();

			if( 'header_number' == $objApTransactionsFilter->getSortBy() ) {
				$strOrderBy = 'LOWER( ah.header_number ) ' . $objApTransactionsFilter->getSortDirection();
			}

			if( 'is_posted' == $objApTransactionsFilter->getSortBy() ) {
				$strOrderBy = ( ( 'ASC' == $objApTransactionsFilter->getSortDirection() ) ?
					'CASE 
						WHEN ah.ap_financial_status_type_id = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_REJECTED . ' 
						THEN \'Rejected\' END, 
					 CASE 
						WHEN ah.is_posted = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_POSTED . ' ::BOOLEAN
						THEN \'Posted\' END, 
					CASE 
						WHEN ah.is_posted = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_NOT_POSTED . ' ::BOOLEAN 
						THEN \'Not Posted\' END ' . $objApTransactionsFilter->getSortDirection() :
					'CASE
						WHEN ah.is_posted = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_NOT_POSTED . ' ::BOOLEAN 
						THEN \'Not Posted\' END, 
					CASE
						WHEN ah.is_posted = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_POSTED . ' ::BOOLEAN 
						THEN \'Posted\' END, 
					CASE 
						WHEN ah.ap_financial_status_type_id = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_REJECTED . ' 
						THEN \'Rejected\' END ' . $objApTransactionsFilter->getSortDirection() );
			}
		}

		if( true == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) ) {
			if( true == valArr( $objApTransactionsFilter->getPropertyIds() ) ) {
				$arrintPropertyIds      = $objApTransactionsFilter->getPropertyIds();
				$arrstrAndConditions[]  = 'ad.property_id IN ( SELECT property_id FROM pg_temp.load_properties_info ) ';
			} else {
				$arrstrAndConditions[] = ' CASE
											WHEN ah.control_total = 0
											THEN ah.id IN (
															SELECT
																ap_header_id
															FROM
																ap_details
															WHERE
	 															cid = ' . ( int ) $this->getCid() . '
																AND property_id IN ( SELECT property_id FROM pg_temp.load_properties_info )
																AND deleted_by IS NULL
																AND deleted_on IS NULL
														)
											ELSE
												ah.id = ah.id
												AND ah.cid = ah.cid
										END ';
			}
		}

		if( true == valStr( $objApTransactionsFilter->getHeaderNumber() ) ) {
			$arrstrAndConditions[] = 'ah.header_number ILIKE \'%' . trim( $objApTransactionsFilter->getHeaderNumber() ) . '%\' ';
		}

		if( true == valId( $objApTransactionsFilter->getApHeaderTypeId() ) ) {
			$arrstrAndConditions[] = 'ah.ap_header_type_id = ' . $objApTransactionsFilter->getApHeaderTypeId();
		}

		if( true == valArr( $objApTransactionsFilter->getApInvoiceTypeIds() ) ) {

			if( true == in_array( CApTransactionsFilter::AP_INVOICE_SUB_TYPE_REFUND_INVOICE, $objApTransactionsFilter->getApInvoiceTypeIds() ) && 1 == \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApInvoiceTypeIds() ) ) {

				$arrstrAndConditions[] = ' ah.lease_customer_id IS NOT NULL';
			} elseif( true == valIntArr( $objApTransactionsFilter->getJobIds() ) && true == in_array( CApTransactionsFilter::AP_INVOICE_SUB_TYPE_STANDARD_JOB_OR_CONTRACT, $objApTransactionsFilter->getApInvoiceTypeIds() ) ) {

				$arrintInvoiceTypeIds = array_flip( $objApTransactionsFilter->getApInvoiceTypeIds() );
				unset( $arrintInvoiceTypeIds[CApTransactionsFilter::AP_INVOICE_SUB_TYPE_STANDARD_JOB_OR_CONTRACT] );
				if( true == in_array( CApTransactionsFilter::AP_INVOICE_SUB_TYPE_REFUND_INVOICE, $objApTransactionsFilter->getApInvoiceTypeIds() ) && true == in_array( CApTransactionsFilter::AP_INVOICE_SUB_TYPE_STANDARD_JOB_OR_CONTRACT, $objApTransactionsFilter->getApInvoiceTypeIds() ) ) {

					$arrstrAndConditions[] = ' ( ah.ap_header_sub_type_id IN ( ' . implode( ',', array_flip( $arrintInvoiceTypeIds ) ) . ' ) OR ( ah.ap_header_sub_type_id = ' . CApTransactionsFilter::AP_INVOICE_SUB_TYPE_STANDARD_JOB_OR_CONTRACT . ' AND jp.job_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobIds() ) . ' ) ) OR ah.lease_customer_id IS NOT NULL )';
				} elseif( true == in_array( CApTransactionsFilter::AP_INVOICE_SUB_TYPE_STANDARD_JOB_OR_CONTRACT, $objApTransactionsFilter->getApInvoiceTypeIds() ) && 1 == \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApInvoiceTypeIds() ) ) {

					$arrstrAndConditions[] = ' ( ah.ap_header_sub_type_id IN ( ' . implode( ',', $objApTransactionsFilter->getApInvoiceTypeIds() ) . ' ) AND jp.job_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobIds() ) . ' ) )';
				} elseif( false == in_array( CApTransactionsFilter::AP_INVOICE_SUB_TYPE_REFUND_INVOICE, $objApTransactionsFilter->getApInvoiceTypeIds() ) && true == in_array( CApTransactionsFilter::AP_INVOICE_SUB_TYPE_STANDARD_JOB_OR_CONTRACT, $objApTransactionsFilter->getApInvoiceTypeIds() ) ) {

					$arrstrAndConditions[] = ' ( ah.ap_header_sub_type_id IN ( ' . implode( ',', array_flip( $arrintInvoiceTypeIds ) ) . ' ) OR ( ah.ap_header_sub_type_id = ' . CApTransactionsFilter::AP_INVOICE_SUB_TYPE_STANDARD_JOB_OR_CONTRACT . ' AND jp.job_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobIds() ) . ' ) ) )';
				}
			} elseif( true == in_array( CApTransactionsFilter::AP_INVOICE_SUB_TYPE_REFUND_INVOICE, $objApTransactionsFilter->getApInvoiceTypeIds() ) ) {

				$arrstrAndConditions[] = ' ( ah.ap_header_sub_type_id IN ( ' . implode( ',', $objApTransactionsFilter->getApInvoiceTypeIds() ) . ' ) OR ah.lease_customer_id IS NOT NULL )';
			} else {

				$arrstrAndConditions[] = ' ah.ap_header_sub_type_id IN ( ' . implode( ',', $objApTransactionsFilter->getApInvoiceTypeIds() ) . ' ) AND ah.lease_customer_id IS NULL ';
			}
		}

		if( true == valArr( $objApTransactionsFilter->getApPayeeLocationIds() ) ) {
			$arrstrAndConditions[] = 'apl.id IN ( ' . implode( ',', $objApTransactionsFilter->getApPayeeLocationIds() ) . ' )';
		}

		if( true == valStr( $objApTransactionsFilter->getPostDateFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getPostDateFrom() ) ) {
			$arrstrAndConditions[] = 'ah.post_date >= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getPostDateFrom() ) ) . ' 00:00:00\'';
		}

		if( true == valStr( $objApTransactionsFilter->getPostDateTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getPostDateTo() ) ) {
			$arrstrAndConditions[] = 'ah.post_date <= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getPostDateTo() ) ) . ' 23:59:59\'';
		}

		if( true == valStr( $objApTransactionsFilter->getDueDateFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getDueDateFrom() ) ) {
			$arrstrAndConditions[] = 'ah.due_date >= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDueDateFrom() ) ) . ' 00:00:00\'';
		}

		if( true == valStr( $objApTransactionsFilter->getDueDateTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getDueDateTo() ) ) {
			$arrstrAndConditions[] = 'ah.due_date <= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDueDateTo() ) ) . ' 23:59:59\'';
		}

		if( true == valStr( $objApTransactionsFilter->getInvoicePostMonthFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getInvoicePostMonthFrom() ) ) {
			$arrstrAndConditions[] = 'ah.post_month >= \'' . addslashes( $objApTransactionsFilter->getInvoicePostMonthFrom() ) . '\'';
		}

		if( true == valStr( $objApTransactionsFilter->getInvoicePostMonthTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getInvoicePostMonthTo() ) ) {
			$arrstrAndConditions[] = 'ah.post_month <= \'' . addslashes( $objApTransactionsFilter->getInvoicePostMonthTo() ) . '\'';
		}

		if( true == valStr( $objApTransactionsFilter->getPaymentPostMonthFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getPaymentPostMonthFrom() ) ) {
			$arrstrAndConditions[] = 'aa.post_month >= \'' . addslashes( $objApTransactionsFilter->getPaymentPostMonthFrom() ) . '\'';
		}

		if( true == valStr( $objApTransactionsFilter->getPaymentPostMonthTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getPaymentPostMonthTo() ) ) {
			$arrstrAndConditions[] = 'aa.post_month <= \'' . addslashes( $objApTransactionsFilter->getPaymentPostMonthTo() ) . '\'';
		}

		if( CApPayeeType::RESIDENT == ( int ) $this->getApPayeeTypeId() ) {
			$arrstrAndConditions[] = 'ah.lease_customer_id IS NOT NULL';
		}

		if( true == valArr( $objApTransactionsFilter->getApPostStatusTypeIds() ) ) {

			if( true == in_array( CApTransactionsFilter::AP_POST_STATUS_TYPE_POSTED, $objApTransactionsFilter->getApPostStatusTypeIds() ) ) {
				$arrstrOrConditions[] = '( ah.is_posted = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_POSTED . ' ::BOOLEAN AND ( ah.ap_financial_status_type_id IS NULL OR ah.ap_financial_status_type_id <> ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_REJECTED . ' ) )';
			}

			if( true == in_array( CApTransactionsFilter::AP_POST_STATUS_TYPE_NOT_POSTED, $objApTransactionsFilter->getApPostStatusTypeIds() ) ) {
				$arrstrOrConditions[] = '( ah.is_posted = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_NOT_POSTED . ' ::BOOLEAN AND ( ah.ap_financial_status_type_id IS NULL OR ( COALESCE( ah.ap_financial_status_type_id, 0 ) <> ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_REJECTED . ' AND COALESCE( ah.ap_financial_status_type_id, 0 ) <> ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_DELETED . ' ) ) )';
			}

			if( true == in_array( CApTransactionsFilter::AP_POST_STATUS_TYPE_REJECTED, $objApTransactionsFilter->getApPostStatusTypeIds() ) ) {
				$arrstrOrConditions[] = '( ah.ap_financial_status_type_id = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_REJECTED . ' )';
			}

			if( true == in_array( CApTransactionsFilter::AP_POST_STATUS_TYPE_DELETED, $objApTransactionsFilter->getApPostStatusTypeIds() ) ) {
				$arrstrOrConditions[] = '( ah.ap_financial_status_type_id = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_DELETED . ' )';
			}

			if( true == valArr( $arrstrOrConditions ) ) {
				$arrstrAndConditions[] = ' ( ' . implode( ' OR ', $arrstrOrConditions ) . ' )';
			}
		} else {
			$arrstrAndConditions[] = '( ( ah.is_posted = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_POSTED . ' ::BOOLEAN  OR ah.is_posted = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_NOT_POSTED . ' ::BOOLEAN ) AND ( ah.ap_financial_status_type_id IS NULL OR ( COALESCE( ah.ap_financial_status_type_id, 0 ) <> ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_REJECTED . ' AND COALESCE( ah.ap_financial_status_type_id, 0 ) <> ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_DELETED . ' ) ) )';
		}

		if( true == valArr( $objApTransactionsFilter->getApPayeeAccountNumberIds() )
			&& 1 == \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApPayeeAccountNumberIds() )
			&& false == valStr( current( $objApTransactionsFilter->getApPayeeAccountNumberIds() ) ) ) {
			$arrstrAndConditions[] = 'apa.account_number IS NULL';
		} else if( true == valArr( $objApTransactionsFilter->getApPayeeAccountNumberIds() )
			&& 1 < \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApPayeeAccountNumberIds() )
			&& false == valStr( current( $objApTransactionsFilter->getApPayeeAccountNumberIds() ) ) ) {
			$arrstrAndConditions[] = ' ( apa.account_number IS NULL OR apa.id IN ( ' . implode( ',', array_filter( $objApTransactionsFilter->getApPayeeAccountNumberIds() ) ) . ' ) )';
		} else if( true == valArr( $objApTransactionsFilter->getApPayeeAccountNumberIds() ) ) {
			$arrstrAndConditions[] = ' apa.id IN ( ' . implode( ',', array_filter( $objApTransactionsFilter->getApPayeeAccountNumberIds() ) ) . ' )';
		}

		if( true == valArr( $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
			$arrstrPaymentStatusConditions = [
				CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_PAID				=> ' transaction_amount_paid = ah.transaction_amount ',
				CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_PARTIALLY_PAID	=> ' ( transaction_amount_paid < ah.transaction_amount AND transaction_amount_paid > 0 ) ',
				CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNPAID			=> ' COALESCE( transaction_amount_paid, 0 ) = 0 '
			];

			$arrstrTempPaymentStatusConditions = [];
			foreach( $objApTransactionsFilter->getApPaymentStatusTypeIds() as $intApPaymentStatusTypeId ) {
				$arrstrTempPaymentStatusConditions[] = $arrstrPaymentStatusConditions[$intApPaymentStatusTypeId];
			}
			$arrstrTempPaymentStatusConditions = array_filter( $arrstrTempPaymentStatusConditions );
			if( \Psi\Libraries\UtilFunctions\count( $arrstrPaymentStatusConditions ) != \Psi\Libraries\UtilFunctions\count( $arrstrTempPaymentStatusConditions ) && true == valArr( $arrstrTempPaymentStatusConditions ) ) {
				$arrstrAndConditions[] = ' ( ' . implode( ' OR ', $arrstrTempPaymentStatusConditions ) . ' )';
			}
		}

		$arrstrAndConditions[] = 'ah.ap_header_sub_type_id IN ( ' . sqlIntImplode( $arrintInvoiceTypePermissions ) . ' ) ';

		if( true == valArr( $arrstrAndConditions ) ) {
			$strConditions .= ' AND ' . implode( ' AND ', $arrstrAndConditions );
		}

		$strSql = 'DROP TABLE IF EXISTS pg_temp.load_properties_info;
					CREATE TEMP TABLE load_properties_info AS (
						SELECT
							lp.property_id
						FROM
							load_properties( ARRAY[' . ( int ) $this->getCid() . '], ARRAY[' . implode( ',', $arrintPropertyIds ) . '], ARRAY[' . CPsProduct::ENTRATA . '], true ) lp
						WHERE 
							lp.is_disabled = 0
					);
					ANALYZE pg_temp.load_properties_info;

					DROP TABLE IF EXISTS pg_temp.temp_ap_transactions;
					CREATE TEMP TABLE temp_ap_transactions AS (
						SELECT
							ah.id AS ap_header_id,
							ah.header_number,
							ah.post_date,
							ah.due_date,
							ah.is_posted::integer,
							ah.ap_financial_status_type_id,
							apl.vendor_code,
							apl.location_name,
							apl.id AS ap_payee_location_id,
							(
								SELECT
									COALESCE( SUM ( ad.transaction_amount ), 0 )
								FROM
									ap_details ad
								WHERE
									ah.cid = ad.cid
									AND ad.cid = ' . ( int ) $this->getCid() . '
									AND ah.id = ad.ap_header_id
									AND ah.gl_transaction_type_id = ad.gl_transaction_type_id
									AND ah.post_month = ad.post_month
									AND ad.deleted_by IS NULL
									AND ad.deleted_on IS NULL
							) AS transaction_amount,
							( COALESCE( ad2.transaction_amount_paid, 0 ) ) AS transaction_amount_paid,
							apa.account_number as vendor_account_number,
							CASE WHEN COALESCE( transaction_amount_paid, 0 ) = 0 THEN \'Unpaid\'
								WHEN transaction_amount_paid = ah.transaction_amount THEN \'Paid\'
								WHEN transaction_amount_paid < ah.transaction_amount AND transaction_amount_paid > 0 THEN \'Partially Paid\'
							END As payment_status
						FROM
							ap_headers ah
							LEFT JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month AND ad.deleted_by IS NULL AND ad.deleted_on IS NULL )
							LEFT JOIN ap_allocations aa ON ( aa.cid = ad.cid AND ( aa.charge_ap_detail_id = ad.id OR aa.credit_ap_detail_id = ad.id ) AND aa.is_deleted = false )
							LEFT JOIN LATERAL
							(
								SELECT
									ap_header_id,
									cid,
									SUM ( paid_amount ) AS transaction_amount_paid
								FROM
								(
									SELECT
										ad.ap_header_id,
										ad.cid,
										CASE
											WHEN ad.transaction_amount >= 0::numeric THEN ABS ( COALESCE ( SUM ( aa.allocation_amount ), 0::NUMERIC ) )
											ELSE COALESCE ( SUM ( aa.allocation_amount ), 0::NUMERIC )
										END AS paid_amount
									FROM
										ap_details ad
										JOIN ap_allocations aa ON ( aa.cid = ad.cid AND ( aa.charge_ap_detail_id = ad.id OR aa.credit_ap_detail_id = ad.id ) AND aa.is_deleted = false )
									WHERE
										ad.cid = ' . ( int ) $this->getCid() . '
										AND aa.cid = ' . ( int ) $this->getCid() . '
									GROUP BY
										ad.ap_header_id,
										ad.cid,
										ad.transaction_amount
								) aa2
								WHERE
									aa2.cid = ah.cid 
									AND aa2.ap_header_id = ah.id
								GROUP BY
									ap_header_id,
									cid
							) ad2 ON TRUE
							LEFT JOIN properties p ON ( ad.cid = p.cid AND p.id = ad.property_id )
							LEFT JOIN ap_payees ap ON ( ah.cid = ap.cid AND ah.ap_payee_id = ap.id )
							LEFT JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id )
							LEFT JOIN ap_payee_accounts apa ON ( apa.cid = ah.cid AND apa.id = ah.ap_payee_account_id )
							LEFT JOIN job_phases jp ON ( ad.cid = jp.cid AND ad.job_phase_id = jp.id )
						WHERE
							ah.is_batching = FALSE
							AND ah.cid = ' . ( int ) $this->getCid() . '
							AND ah.ap_payee_id = ' . ( int ) $this->getId() . '
							AND ah.reversal_ap_header_id IS NULL
							AND ah.is_template = false
							AND ah.ap_payment_id IS NULL ' . $strConditions . '
							AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
						GROUP BY
							ah.id,
							ah.cid,
							ah.header_number,
							ah.post_date,
							ah.due_date,
							ah.is_posted,
							ah.post_month,
							apl.id,
							apl.vendor_code,
							apl.location_name,
							transaction_amount_paid,
							vendor_account_number
						ORDER BY ' . $strOrderBy . '
					);
					ANALYZE pg_temp.temp_ap_transactions;';

		fetchData( $strSql, $objClientDatabase );

		return true;
	}

	private function createTempTableApPayments( $intCid, $objClientDatabase, $objApTransactionsFilter = NULL, $arrintCompanyUserPropertyIds = NULL ) {

		if( 't' == fetchData( 'SELECT util.util_temp_table_exists(\'temp_ap_payments\') AS result;', $objClientDatabase )[0]['result'] ) {
			return;
		}

		$strCondition				= '';
		$strConditionSub			= '';
		$strPaymentStatusCondition	= '';
		$arrstrSubWhere				= [];
		$strOrderBy					= 'ap.id DESC';

		$arrstrSortBy				= [ 'payment_number', 'payment_date', 'post_month', 'ap_payment_type_name', 'account_name', 'payment_status_type_name', 'payment_amount' ];

		if( true == valArr( $arrintCompanyUserPropertyIds ) ) {
			$strConditionSub = ' AND ad.property_id IN ( ' . implode( ',', $arrintCompanyUserPropertyIds ) . ' )';
		}

		if( true == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) ) {

			if( true == valStr( $objApTransactionsFilter->getSortBy() ) && true == in_array( $objApTransactionsFilter->getSortBy(), $arrstrSortBy ) ) {
				$strOrderBy = $objApTransactionsFilter->getSortBy() . ' ' . $objApTransactionsFilter->getSortDirection();
			}

			if( true == valArr( $objApTransactionsFilter->getBankAccountIds() ) ) {
				$strCondition .= ' AND ap.bank_account_id IN ( ' . implode( ',', $objApTransactionsFilter->getBankAccountIds() ) . ' )';
			}

			if( true == is_numeric( $objApTransactionsFilter->getApPaymentTypeId() ) ) {
				$arrstrSubWhere[] = ' AND ap.ap_payment_type_id = ' . ( int ) $objApTransactionsFilter->getApPaymentTypeId();
			}

			if( ( CApPaymentType::CHECK == $objApTransactionsFilter->getApPaymentTypeId() ) || ( CApPaymentType::CREDIT_CARD == $objApTransactionsFilter->getApPaymentTypeId() ) ) {

				if( true == is_numeric( $objApTransactionsFilter->getCheckMinNumber() ) ) {
					$strCondition .= ' AND ap.payment_number::Integer >= ' . ( int ) $objApTransactionsFilter->getCheckMinNumber();
				}

				if( true == is_numeric( $objApTransactionsFilter->getCheckMaxNumber() ) ) {
					$strCondition .= ' AND ap.payment_number::Integer <= ' . ( int ) $objApTransactionsFilter->getCheckMaxNumber();
				}
			}

			if( true == valStr( $objApTransactionsFilter->getPaymentNumber() ) ) {
				$strCondition .= ' AND ap.payment_number ILIKE \'%' . addslashes( $objApTransactionsFilter->getPaymentNumber() ) . '%\' ';
			}

			if( true == valStr( $objApTransactionsFilter->getDateFrom() ) && false == valStr( $objApTransactionsFilter->getDateTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getDateFrom() ) ) {

				$strCondition .= ' AND ap.payment_date >= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDateFrom() ) ) . ' 00:00:00\' ';

			} else if( true == valStr( $objApTransactionsFilter->getDateTo() ) && false == valStr( $objApTransactionsFilter->getDateFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getDateTo() ) ) {

				$strCondition .= ' AND ap.payment_date <= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDateTo() ) ) . ' 23:59:59\'';

			} else if( true == valStr( $objApTransactionsFilter->getDateFrom() ) && true == valStr( $objApTransactionsFilter->getDateTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getDateFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getDateTo() ) ) {

				$strCondition .= ' AND ap.payment_date >= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDateFrom() ) ) . ' 00:00:00\' ';
				$strCondition .= ' AND ap.payment_date <= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDateTo() ) ) . ' 23:59:59\'';
			}

			if( true == valArr( $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {

				$arrstrPaymentStatusCondition = [];

				if( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					$arrstrPaymentStatusCondition[] = 'gd.gl_reconciliation_id IS NULL';
				}

				if( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					$arrstrPaymentStatusCondition[] = '( gd.gl_reconciliation_id IS NOT NULL AND gr.gl_reconciliation_status_type_id = ' . ( int ) CGlReconciliationStatusType::RECONCILED . ' )';
				}

				if( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					$arrstrPaymentStatusCondition[] = '( gd.gl_reconciliation_id IS NOT NULL AND gr.gl_reconciliation_status_type_id = ' . ( int ) CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED . ' )';
				}

				if( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					$arrstrPaymentStatusCondition[] = 'ap.payment_status_type_id = ' . ( int ) CPaymentStatusType::VOIDED;
				} else if( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() )
				           || true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED, $objApTransactionsFilter->getApPaymentStatusTypeIds() )
				           || true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					// if voided status was not selected then for other statuses this condition is must
					$strPaymentStatusCondition = 'AND ( ap.payment_status_type_id <> ' . ( int ) CPaymentStatusType::VOIDED . ' )';
				}

				if( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RETURNED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					// CPaymentStatusType::RECALL having same value as of Returned in payment_status_types table
					$arrstrPaymentStatusCondition[] = 'ap.payment_status_type_id = ' . ( int ) CPaymentStatusType::RECALL;
				}

				if( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDING, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					$arrstrPaymentStatusCondition[] = 'ap.payment_status_type_id = ' . ( int ) CPaymentStatusType::VOIDING;
				}

				if( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CAPTURING, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					$arrstrPaymentStatusCondition[] = 'ap.payment_status_type_id = ' . ( int ) CPaymentStatusType::CAPTURING;
				}

				if( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CAPTURED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					$arrstrPaymentStatusCondition[] = 'ap.payment_status_type_id = ' . ( int ) CPaymentStatusType::CAPTURED;
				}

				if( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECEIVED, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					$arrstrPaymentStatusCondition[] = '( gd.gl_reconciliation_id IS NOT NULL AND gr.gl_reconciliation_status_type_id IS NULL AND ap.payment_status_type_id = ' . ( int ) CPaymentStatusType::RECEIVED . ' )';
				}

				if( true == in_array( CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_PENDING, $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					$arrstrPaymentStatusCondition[] = 'ap.payment_status_type_id = ' . ( int ) CPaymentStatusType::PENDING;
				}

				if( true == valArr( $arrstrPaymentStatusCondition ) ) {

					if( 1 < \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
						$strPaymentStatusCondition .= ' AND ( ' . implode( ' OR ', $arrstrPaymentStatusCondition ) . ' )';
					} else {
						$strPaymentStatusCondition .= ' AND ( ' . $arrstrPaymentStatusCondition[0] . ' )';
					}
				}
				$arrstrSubWhere[] = $strPaymentStatusCondition;
			}
		}

		$strColumn = ', CASE
							WHEN ap.payment_status_type_id = 8 THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDED] . '\'
							WHEN ap.payment_status_type_id = ' . ( int ) CPaymentStatusType::VOIDING . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDING] . '\'
							WHEN ap.payment_status_type_id = ' . ( int ) CPaymentStatusType::CAPTURING . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CAPTURING] . '\'
							WHEN ap.payment_status_type_id = ' . ( int ) CPaymentStatusType::CAPTURED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CAPTURED] . '\'
							WHEN ap.payment_status_type_id = ' . ( int ) CPaymentStatusType::PENDING . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_PENDING] . '\'
							WHEN ap.payment_status_type_id IN ( 15, 30 ) THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RETURNED] . '\'
							ELSE CASE
								WHEN sub_ad.gl_reconciliation_id IS NOT NULL AND sub_ad.gl_reconciliation_status_type_id =' . CGlReconciliationStatusType::PAUSED . ' THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED] . '\'
								WHEN sub_ad.gl_reconciliation_id IS NULL THEN \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED] . '\'
								ELSE \'' . CApTransactionsFilter::$c_arrintApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED] . '\'
							END
						END AS payment_status_type_name';

		$strSql = 'DROP TABLE IF EXISTS pg_temp.temp_ap_payments;
					CREATE TEMP TABLE temp_ap_payments AS (
						SELECT
							DISTINCT ap.*,
							apt.name AS ap_payment_type_name,
							MAX( ah.post_month ) OVER ( PARTITION BY ap.id ) AS post_month,
							ba.account_name
							' . $strColumn . '
						FROM
							ap_payments ap
							LEFT JOIN ap_payment_types apt ON ( apt.id = ap.ap_payment_type_id )
							JOIN ap_headers ah ON ( ap.cid = ah.cid AND ap.id = ah.ap_payment_id AND ah.deleted_by IS NULL AND ah.deleted_on IS NULL  )
							JOIN bank_accounts ba ON ( ba.cid = ap.cid AND ba.id = ap.bank_account_id )
							JOIN LATERAL (
								SELECT
									gd.gl_reconciliation_id, 
									gr.gl_reconciliation_status_type_id
								FROM
									ap_details ad
									LEFT JOIN ap_allocations aa ON ( aa.cid = ad.cid AND aa.credit_ap_detail_id = ad.id )
									LEFT JOIN gl_headers gh ON ( gh.cid = aa.cid AND gh.reference_id = aa.id AND gh.gl_transaction_type_id = aa.gl_transaction_type_id AND gh.gl_transaction_type_id = ' . CGlTransactionType::AP_ALLOCATION . ' )
									LEFT JOIN gl_details gd ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id AND gd.gl_reconciliation_id IS NOT NULL
												AND gd.amount = CASE
												WHEN aa.origin_ap_allocation_id IS NOT NULL OR aa.lump_ap_header_id IS NOT NULL
												THEN aa.allocation_amount * - 1
													ELSE aa.allocation_amount
												END
												AND gd.property_id = CASE
												WHEN ba.reimbursed_property_id <> aa.property_id THEN ba.reimbursed_property_id
													ELSE aa.property_id
												END )
									LEFT JOIN gl_reconciliations gr ON ( gr.cid = gd.cid AND gr.id = gd.gl_reconciliation_id )
								WHERE
									ah.cid = ' . ( int ) $intCid . '
									AND ah.id = ad.ap_header_id
									AND ah.gl_transaction_type_id = ad.gl_transaction_type_id
									AND ah.post_month = ad.post_month
									AND ad.deleted_on IS NULL ' . $strConditionSub . '
									' . implode( PHP_EOL . ' ', $arrstrSubWhere ) . '
								GROUP BY
									1, 2
								) AS sub_ad ON TRUE
						WHERE
							ap.cid = ' . ( int ) $intCid . '
							AND ah.ap_payee_id = ' . ( int ) $this->getId() . '
							AND ap.is_unclaimed_property = false ' . $strCondition . '
						ORDER BY
							' . $strOrderBy . '
					);
					ANALYZE pg_temp.temp_ap_payments;';

		fetchData( $strSql, $objClientDatabase );

		return true;
	}

	/**
	 * Get Functions
	 */

	public function getOrFetchApPayeeDetail( $objClientDatabase ) {

		if( false == valObj( $this->m_objApPayeeDetail, 'CApPayeeDetail' ) ) {
			$this->m_objApPayeeDetail = $this->fetchApPayeeDetail( $objClientDatabase );
		}

		return $this->m_objApPayeeDetail;
	}

	public function getOrFetchApPayeeContact( $objClientDatabase ) {

		if( false == valObj( $this->m_objApPayeeContact, 'CApPayeeContact' ) ) {
			$this->m_objApPayeeContact = $this->fetchApPayeeContacts( $objClientDatabase );
		}

		return $this->m_objApPayeeContact;
	}

	public function getApPaymentTypeId() {
		return $this->m_intApPaymentTypeId;
	}

	public function getForm1099TypeId() {
		return $this->m_intForm1099TypeId;
	}

	public function getPaymentAmount() {
		return $this->m_fltPaymentAmount;
	}

	public function getApPaymentTypeName() {
		return $this->m_strApPaymentTypeName;
	}

	public function getApPrimaryContact() {
		return $this->m_strApPrimaryContact;
	}

	public function getApPayeeStatusType() {
		return $this->m_strApPayeeStatusType;
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function getStreetLine2() {
		return $this->m_strStreetLine2;
	}

	public function getStreetLine3() {
		return $this->m_strStreetLine3;
	}

	public function getPayeeName() {
		return $this->m_strPayeeName;
	}

	public function getLocationName() {
		return $this->m_strLocationName;
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function getApPayee1099Adjustments() {
		return $this->m_arrobjApPayee1099Adjustments;
	}

	public function getApPayeeProperties() {
		return $this->m_arrobjApPayeeProperties;
	}

	public function getApPayeePropertyGroups() {
		return $this->m_arrobjApPayeePropertyGroups;
	}

	public function getIsSelectAllProperties() {
		return $this->m_boolIsSelectAllProperties;
	}

	public function getReceives1099() {
		return $this->m_boolReceives1099;
	}

	public function getApPayeeLocationId() {
		return $this->m_intApPayeeLocationId;
	}

	public function getApLegalEntityId() {
		return $this->m_intApLegalEntityId;
	}

	public function getApPayeeAccountId() {
	    return $this->m_intApPayeeAccountId;
	}

	public function getForm1099BoxTypeId() {
		return $this->m_intForm1099BoxTypeId;
	}

	public function getApPayeeAccountNumber() {
		return $this->m_intApPayeeAccountNumber;
	}

	public function getDefaultGlAccountName() {
		return $this->m_strDefaultGlAccountName;
	}

	public function getVendorCode() {
		return $this->m_strVendorCode;
	}

	public function getApRemittanceId() {
		return $this->m_intApRemittanceId;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function getApPayeeName() {
		return $this->m_strApPayeeName;
	}

	public function getApPayeeAddress() {
		return $this->m_strApPayeeAddress;
	}

	public function getApPayeeContactLocationId() {
		return $this->m_intApPayeeContactLocationId;
	}

	public function getMergeFields() {
		return $this->m_arrstrMergeFields;
	}

	public function getMergeFieldByKey( $strKey ) {
		if( true == array_key_exists( $strKey, $this->m_arrstrMergeFields ) ) {
			return $this->m_arrstrMergeFields[$strKey];
		}

		return NULL;
	}

	public function getRecipientId() {
		return $this->m_intRecipientId;
	}

	public function getInsurancePolicyDetails() {
		return $this->m_arrmixInsurancePolicyDetails;
	}

	public function getLocationsApRemittanceId() {
		return $this->m_intLocationsApRemittanceId;
	}

	public function getApPayeeContactId() {
		return $this->m_intApPayeeContactId;
	}

	public function getVendorInformationEvent() {
		return $this->m_arrmixVendorInformationEvent;
	}

	public function getVendorStatusEvent() {
		return $this->m_arrmixVendorStatusEvent;
	}

	public function getIntercompanyPropertyId() {
		return $this->m_intIntercompanyPropertyId;
	}

	public function getRemittanceName() {
		return $this->m_strRemittanceName;
	}

	public function getRemittanceType() {
		return $this->m_strRemittanceType;
	}

	public function getRecipientTin() {
		return $this->m_strRecipientTin;
	}

	public function getEntityName() {
		return $this->m_strEntityName;
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function getApRoutingTagId() {
		return $this->m_intApRoutingTagId;
	}

	public function getApPayeeCustomData() {
		return $this->m_arrmixApPayeeCustomData;
	}

	public function getPreviousBalance() {
		return $this->m_fltPreviousBalance;
	}

	public function getIsPrimary() {
		return $this->m_boolIsPrimary;
	}

	public function getOwnerName() {
		return $this->m_strOwnerName;
	}

	public function getOwnerId() {
		return $this->m_intOwnerId;
	}

	/**
	 * Set Functions
	 */

	public function setApRemittanceId( $intApRemittanceId ) {
		$this->m_intApRemittanceId = CStrings::strToIntDef( $intApRemittanceId, NULL, false );
	}

	public function setApPayeeStatusType( $strApPayeeStatusType ) {
		$this->m_strApPayeeStatusType = $strApPayeeStatusType;
	}

	public function setApPaymentTypeId( $intApPaymentTypeId ) {
		$this->m_intApPaymentTypeId = CStrings::strToIntDef( $intApPaymentTypeId, NULL, false );
	}

	public function setForm1099TypeId( $intForm1099TypeId ) {
		$this->m_intForm1099TypeId = CStrings::strToIntDef( $intForm1099TypeId, NULL, false );
	}

	public function setPaymentAmount( $fltPaymentAmount ) {
		$this->m_fltPaymentAmount = CStrings::strToFloatDef( $fltPaymentAmount, NULL, false, 2 );
	}

	public function setApPaymentTypeName( $strApPaymentTypeName ) {
		$this->m_strApPaymentTypeName = $strApPaymentTypeName;
	}

	public function setApPrimaryContact( $strApPrimaryContact ) {
		$this->m_strApPrimaryContact = $strApPrimaryContact;
	}

	public function setCity( $strCity ) {
		$this->m_strCity = $strCity;
	}

	public function setStateCode( $strStateCode ) {
		$this->m_strStateCode = $strStateCode;
	}

	public function setCountryCode( $strCountryCode ) {
		$this->m_strCountryCode = $strCountryCode;
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->m_strStreetLine1 = CStrings::strTrimDef( $strStreetLine1, 100, NULL, true );
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->m_strStreetLine2 = CStrings::strTrimDef( $strStreetLine2, 100, NULL, true );
	}

	public function setStreetLine3( $strStreetLine3 ) {
		$this->m_strStreetLine3 = CStrings::strTrimDef( $strStreetLine3, 100, NULL, true );
	}

	public function setPayeeName( $strPayeeName ) {
		$this->m_strPayeeName = CStrings::strTrimDef( $strPayeeName, 50, NULL, true );
	}

	public function setLocationName( $strLocationName ) {
		$this->m_strLocationName = CStrings::strTrimDef( $strLocationName, 100, NULL, true );
	}

	public function setPostalCode( $strPostalCode ) {
		$this->m_strPostalCode = CStrings::strTrimDef( $strPostalCode, 20, NULL, true );
	}

	public function setApPayee1099Adjustments( $arrobjApPayee1099Adjustments ) {
		$this->m_arrobjApPayee1099Adjustments = ( array ) $arrobjApPayee1099Adjustments;
	}

	public function setApPayeeProperties( $arrobjApPayeeProperties ) {
		$this->m_arrobjApPayeeProperties = ( array ) $arrobjApPayeeProperties;
	}

	public function setApPayeePropertyGroups( $arrobjApPayeePropertyGroups ) {
		$this->m_arrobjApPayeePropertyGroups = ( array ) $arrobjApPayeePropertyGroups;
	}

	public function setIsSelectAllProperties( $boolIsSelectAllProperties ) {
		$this->m_boolIsSelectAllProperties = $boolIsSelectAllProperties;
	}

	public function setReceives1099( $boolReceives1099 ) {
		$this->m_boolReceives1099 = $boolReceives1099;
	}

	public function setApPayeeLocationId( $intApPayeeLocationId ) {
		$this->m_intApPayeeLocationId = CStrings::strToIntDef( $intApPayeeLocationId, NULL, false );
	}

	public function setApLegalEntityId( $intApLegalEntityId ) {
		$this->m_intApLegalEntityId = CStrings::strToIntDef( $intApLegalEntityId, NULL, false );
	}

	public function setApPayeeAccountId( $intApPayeeAccountId ) {
	    $this->m_intApPayeeAccountId = CStrings::strToIntDef( $intApPayeeAccountId, NULL, false );
	}

	public function setForm1099BoxTypeId( $intForm1099BoxTypeId ) {
		$this->m_intForm1099BoxTypeId = CStrings::strToIntDef( $intForm1099BoxTypeId, NULL, false );
	}

	public function setApPayeeAccountNumber( $strAccountNumber ) {
		$this->m_intApPayeeAccountNumber = CStrings::strTrimDef( $strAccountNumber, 50, NULL, true );
	}

	public function setDefaultGlAccountName( $strDefaultGlAccountName ) {
		$this->m_strDefaultGlAccountName = $strDefaultGlAccountName;
	}

	public function setVendorCode( $strVendorCode ) {
		$this->m_strVendorCode = $strVendorCode;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->m_strPhoneNumber = CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true );
	}

	public function setApPayeeName( $strApPayeeName ) {
		$this->m_strApPayeeName = $strApPayeeName;
	}

	public function setApPayeeAddress( $strApPayeeAddress ) {
		$this->m_strApPayeeAddress = $strApPayeeAddress;
	}

	public function setApPayeeContactLocationId( $intApPayeeContactLocationId ) {
		$this->m_intApPayeeContactLocationId = $intApPayeeContactLocationId;
	}

	public function setRemittanceName( $strRemittanceName ) {
		$this->m_strRemittanceName = CStrings::strTrimDef( $strRemittanceName, 100, NULL, true );
	}

	public function setRemittanceType( $strRemittanceType ) {
		$this->m_strRemittanceType = CStrings::strTrimDef( $strRemittanceType, 100, NULL, true );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['payment_amount'] ) ) $this->setPaymentAmount( $arrmixValues['payment_amount'] );
		if( true == isset( $arrmixValues['ap_payment_type_id'] ) ) $this->setApPaymentTypeId( $arrmixValues['ap_payment_type_id'] );
		if( true == isset( $arrmixValues['form_1099_type_id'] ) ) $this->setForm1099TypeId( $arrmixValues['form_1099_type_id'] );
		if( true == isset( $arrmixValues['ap_payee_location_id'] ) ) $this->setApPayeeLocationId( $arrmixValues['ap_payee_location_id'] );
		if( true == isset( $arrmixValues['ap_payee_account_id'] ) ) $this->setApPayeeAccountId( $arrmixValues['ap_payee_account_id'] );
		if( true == isset( $arrmixValues['locations_ap_remittance_id'] ) ) $this->setLocationsApRemittanceId( $arrmixValues['locations_ap_remittance_id'] );
		if( true == isset( $arrmixValues['form_1099_box_type_id'] ) ) $this->setForm1099BoxTypeId( $arrmixValues['form_1099_box_type_id'] );
		if( true == isset( $arrmixValues['ap_payee_account_number'] ) ) $this->setApPayeeAccountNumber( $arrmixValues['ap_payee_account_number'] );
		if( true == isset( $arrmixValues['ap_payment_type_name'] ) ) $this->setApPaymentTypeName( $arrmixValues['ap_payment_type_name'] );
		if( true == isset( $arrmixValues['ap_primary_contact'] ) ) $this->setApPrimaryContact( $arrmixValues['ap_primary_contact'] );
		if( true == isset( $arrmixValues['ap_payee_status_type'] ) ) $this->setApPayeeStatusType( $arrmixValues['ap_payee_status_type'] );
		if( true == isset( $arrmixValues['city'] ) ) $this->setCity( $arrmixValues['city'] );
		if( true == isset( $arrmixValues['state_code'] ) ) $this->setStateCode( $arrmixValues['state_code'] );
		if( true == isset( $arrmixValues['country_code'] ) ) $this->setCountryCode( $arrmixValues['country_code'] );
		if( true == isset( $arrmixValues['street_line1'] ) ) $this->setStreetLine1( $arrmixValues['street_line1'] );
		if( true == isset( $arrmixValues['street_line2'] ) ) $this->setStreetLine2( $arrmixValues['street_line2'] );
		if( true == isset( $arrmixValues['street_line3'] ) ) $this->setStreetLine3( $arrmixValues['street_line3'] );
		if( true == isset( $arrmixValues['postal_code'] ) ) $this->setPostalCode( $arrmixValues['postal_code'] );
		if( true == isset( $arrmixValues['payee_name'] ) ) $this->setPayeeName( $arrmixValues['payee_name'] );
		if( true == isset( $arrmixValues['location_name'] ) ) $this->setLocationName( $arrmixValues['location_name'] );
		if( true == isset( $arrmixValues['vendor_code'] ) ) $this->setVendorCode( $arrmixValues['vendor_code'] );
		if( true == isset( $arrmixValues['ap_remittance_id'] ) ) $this->setApRemittanceId( $arrmixValues['ap_remittance_id'] );
		if( true == isset( $arrmixValues['receives_1099'] ) ) $this->setReceives1099( $arrmixValues['receives_1099'] );
		if( true == isset( $arrmixValues['email_address'] ) ) $this->setEmailAddress( $arrmixValues['email_address'] );
		if( true == isset( $arrmixValues['name_first'] ) ) $this->setNameFirst( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) ) $this->setNameLast( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['ap_payee_name'] ) ) $this->setApPayeeName( $arrmixValues['ap_payee_name'] );
		if( true == isset( $arrmixValues['ap_payee_contact_location_id'] ) ) $this->setApPayeeContactLocationId( $arrmixValues['ap_payee_contact_location_id'] );
		if( true == isset( $arrmixValues['ap_payee_contact_id'] ) ) $this->setApPayeeContactId( $arrmixValues['ap_payee_contact_id'] );
		if( true == isset( $arrmixValues['ap_payee_address'] ) ) $this->setApPayeeAddress( $arrmixValues['ap_payee_address'] );
		if( true == isset( $arrmixValues['intercompany_property_id'] ) ) $this->setIntercompanyPropertyId( $arrmixValues['intercompany_property_id'] );
		if( true == isset( $arrmixValues['remittance_name'] ) ) $this->setRemittanceName( $arrmixValues['remittance_name'] );
		if( true == isset( $arrmixValues['remittance_type'] ) ) $this->setRemittanceType( $arrmixValues['remittance_type'] );
		if( true == isset( $arrmixValues['recipient_tin'] ) ) $this->setRecipientTin( $arrmixValues['recipient_tin'] );
		if( true == isset( $arrmixValues['entity_name'] ) ) $this->setEntityName( $arrmixValues['entity_name'] );
		if( true == isset( $arrmixValues['post_date'] ) ) $this->setPostDate( $arrmixValues['post_date'] );
		if( true == isset( $arrmixValues['post_month'] ) ) $this->setPostMonth( $arrmixValues['post_month'] );
		if( true == isset( $arrmixValues['ap_routing_tag_id'] ) ) $this->setApRoutingTagId( $arrmixValues['ap_routing_tag_id'] );
		if( true == isset( $arrmixValues['is_on_site'] ) ) $this->setApRoutingTagId( $arrmixValues['is_on_site'] );
		if( true == isset( $arrmixValues['ap_legal_entity_id'] ) ) $this->setApLegalEntityId( $arrmixValues['ap_legal_entity_id'] );
		if( true == isset( $arrmixValues['previous_balance'] ) ) $this->setPreviousBalance( $arrmixValues['previous_balance'] );
		if( true == isset( $arrmixValues['is_primary'] ) ) $this->setIsPrimary( $arrmixValues['is_primary'] );
		if( true == isset( $arrmixValues['owner_name'] ) ) $this->setOwnerName( $arrmixValues['owner_name'] );
		if( true == isset( $arrmixValues['owner_id'] ) ) $this->setOwnerId( ( $arrmixValues['owner_id'] ) );
	}

	public function setMergeFields( $arrstrMergeFields ) {
		foreach( $arrstrMergeFields as $strKey => $strValue ) {
			$this->setMergeFieldByKey( $strKey, $strValue );
		}
	}

	public function setMergeFieldByKey( $strKey, $strValue ) {
		$this->m_arrstrMergeFields[$strKey] = $strValue;
	}

	public function setRecipientId( $intRecipientId ) {
		$this->m_intRecipientId = $intRecipientId;
	}

	public function setInsurancePolicyDetails( $arrmixInsurancePolicyDetails ) {
		$this->m_arrmixInsurancePolicyDetails = $arrmixInsurancePolicyDetails;
	}

	public function setLocationsApRemittanceId( $intLocationsApRemittanceId ) {
		$this->m_intLocationsApRemittanceId = CStrings::strToIntDef( $intLocationsApRemittanceId, NULL, false );
	}

	public function setApPayeeContactId( $intApPayeeContactId ) {
		$this->m_intApPayeeContactId = $intApPayeeContactId;
	}

	public function setVendorInformationEvent( $arrmixEvent ) {
		$this->m_arrmixVendorInformationEvent = $arrmixEvent;
	}

	public function setVendorStatusEvent( $arrmixEvent ) {
		$this->m_arrmixVendorStatusEvent = $arrmixEvent;
	}

	public function setIntercompanyPropertyId( $intIntercompanyPropertyId ) {
		$this->m_intIntercompanyPropertyId = $intIntercompanyPropertyId;
	}

	public function setRecipientTin( $strRecipientTin ) {
		$this->m_strRecipientTin = $strRecipientTin;
	}

	public function setEntityName( $strEntityName ) {
		$this->m_strEntityName = $strEntityName;
	}

	public function setPostDate( $strPostDate ) {
		$this->m_strPostDate = $strPostDate;
	}

	public function setPostMonth( $strPostMonth ) {
		$this->m_strPostMonth = $strPostMonth;
	}

	public function setApRoutingTagId( $intApRoutingTagId ) {
		$this->m_intApRoutingTagId = $intApRoutingTagId;
	}

	public function setApPayeeCustomData( $arrmixApPayeeCustomData ) {
		$this->m_arrmixApPayeeCustomData = $arrmixApPayeeCustomData;
	}

	public function setPreviousBalance( $fltPreviousBalance ) {
		$this->m_fltPreviousBalance = CStrings::strToFloatDef( $fltPreviousBalance, NULL, false, 2 );
	}

	public function setIsPrimary( $boolIsPrimary ) {
		$this->m_boolIsPrimary = $boolIsPrimary;
	}

	public function setOwnerName( $strOwnerName ) {
		$this->m_strOwnerName = $strOwnerName;
	}

	public function setOwnerId( $intOwnerId ) {
		$this->m_intOwnerId = $intOwnerId;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchApPayeeDetail( $objClientDatabase ) {
		return CApPayeeDetails::fetchApPayeeDetailByApPayeeIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchApPayeeAccounts( $objClientDatabase ) {
		return CApPayeeAccounts::fetchApPayeeAccountsByApPayeeIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchPrimaryApPayeeContact( $objClientDatabase ) {
		return CApPayeeContacts::fetchApPayeeContactByApPayeeContactTypeIdByApPayeeIdByCid( true, $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchApPayeeContacts( $objClientDatabase ) {
		return CApPayeeContacts::fetchSimpleApPayeeContactsByApPayeeIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchIntegratedApPayeeContacts( $objClientDatabase ) {
		return CApPayeeContacts::fetchIntegratedApPayeeContactsByApPayeeIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchApPayeeTerms( $objClientDatabase ) {
		return CApPayeeTerms::fetchApPayeeTermsByCid( $this->getCid(), $objClientDatabase );
	}

	public function fetchApTransactionsByPropertyIds( $arrintPropertyIds, $objClientDatabase, $arrintInvoiceTypePermissions, $objApTransactionsFilter, $objPagination = NULL ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintInvoiceTypePermissions ) ) {
			return NULL;
		}

		$this->createTempTableApTransactionsByPropertyIds( $arrintPropertyIds, $objClientDatabase, $arrintInvoiceTypePermissions, $objApTransactionsFilter );

		$strSql = 'SELECT * FROM pg_temp.temp_ap_transactions';

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return fetchData( $strSql, $objClientDatabase );
	}

	public function fetchBalance( $objDataBase ) {

		$fltBalance 	= 0;

		$strSql = 'SELECT
						SUM( ad.transaction_amount_due ) AS transaction_amount_due
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
						JOIN properties p ON ( ad.cid = p.cid AND ad.property_id = p.id AND p.disabled_on IS NULL )
					WHERE
						ad.cid = ' . ( int ) $this->getCid() . '
						AND ah.ap_payee_id = ' . ( int ) $this->getId() . '
						AND ah.reversal_ap_header_id IS NULL
						AND ah.ap_header_type_id = 5
						AND ah.ap_payment_id IS NULL
						AND ah.is_batching = FALSE
						AND ad.deleted_by IS NULL
						AND COALESCE( ah.ap_financial_status_type_id, 0 ) <> ' . CApFinancialStatusType::DELETED . '
						AND ad.deleted_on IS NULL';

		$arrfltBalance 	= fetchData( $strSql, $objDataBase );

		if( true == valArr( $arrfltBalance ) ) {
			$fltBalance = $arrfltBalance[0]['transaction_amount_due'];
		}

		return $fltBalance;
	}

	public function fetchApPaymentDetailsByApHeaderIds( $arrintApHeaderIds, $objClientDatabase ) {
		return CApHeaders::fetchApPaymentDetailsByApHeaderIdsByApPayeeIdByCid( $arrintApHeaderIds, $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchIntegratedApHeaders( $objClientDatabase ) {
		return CApHeaders::fetchIntegratedApHeadersByApPayeeIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchApPayeeContact( $intContactId, $objClientDatabase ) {
		return CApPayeeContacts::fetchApPayeeContactByIdByApPayeeIdByCid( $intContactId, $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchIntegratedApDetails( $objClientDatabase ) {
		return CApDetails::fetchIntegratedApDetailsByApPayeeIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchApPayeeProperties( $objClientDatabase ) {
		return CApPayeePropertyGroups::fetchApPayeePropertiesByApPayeeIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchApPayeeLocationsByIsPrimary( $boolIsPrimary, $objClientDatabase ) {
		return CApPayeeLocations::fetchApPayeeLocationsByApPayeeIdByIsPrimaryByCid( $this->getId(), $boolIsPrimary, $this->getCid(), $objClientDatabase );
	}

	public function fetchApHeadersByCidByPropertyIds( $arrmixParameters, $intCid, $objClientDatabase ) {

		if( false == valArr( getArrayElementByKey( 'ap_header_sub_type_ids', $arrmixParameters ) ) ) {
			return NULL;
		}
		$objPagination = getArrayElementByKey( 'pagination',  $arrmixParameters );

		$this->createTempTableApHeadersByCidByPropertyIds( $arrmixParameters, $intCid, $objClientDatabase );

		$strSql = 'SELECT * FROM pg_temp.temp_ap_headers';

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return CApHeaders::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public function fetchApPayeeLocations( $objClientDatabase ) {
		return CApPayeeLocations::fetchApPayeeLocationsByApPayeeIdOrderByLocationNameByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchApPayeePropertiesByApPayeeLocationIdsByPropertyIds( $arrintApPayeeLocationIds, $arrintPropertyIds, $objClientDatabase ) {
		return CApPayeePropertyGroups::fetchApPayeePropertiesByApPayeeIdByApPayeeLocationIdsByPropertyIdsByCid( $this->getId(), $arrintApPayeeLocationIds, $arrintPropertyIds, $this->getCid(), $objClientDatabase );
	}

	public function fetchApPayeeLocationsByPropertyIds( $arrintPropertyIds, $objClientDatabase ) {
		return CApPayeeLocations::fetchApPayeeLocationsByApPayeeIdByPropertyIdsByCid( $this->getId(), $arrintPropertyIds, $this->getCid(), $objClientDatabase );
	}

	public function fetchApPayeeLocationByCidById( $intCid, $intId, $objClientDatabase ) {
		return CApPayeeLocations::fetchApPayeeLocationByIdByApPayeeIdByCid( $intId, $this->getId(), $intCid, $objClientDatabase );
	}

	public function fetchApPayeePropertiesByApPayeeLocationId( $intApPayeeLocationId, $objClientDatabase ) {
		return CApPayeePropertyGroups::fetchApPayeePropertiesByApPayeeIdByApPayeeLocationIdByCid( $this->getId(), $intApPayeeLocationId, $this->getCid(), $objClientDatabase );
	}

	public function fetchApPayeePropertiesByPropertyIdsByApPayeeLocationId( $arrintPropertyIds, $intApPayeeLocationId, $objClientDatabase ) {
		return CApPayeePropertyGroups::fetchApPayeePropertiesByApPayeeIdByApPayeeLocationIdByPropertyIdsByCid( $this->getId(), $intApPayeeLocationId, $arrintPropertyIds, $this->getCid(), $objClientDatabase );
	}

	public function fetchApPayments( $intCid, $objClientDatabase, $objApTransactionsFilter = NULL, $arrintCompanyUserPropertyIds = NULL, $objPagination = NULL ) {

		$this->createTempTableApPayments( $intCid, $objClientDatabase, $objApTransactionsFilter, $arrintCompanyUserPropertyIds );

		$strSql = 'SELECT * FROM pg_temp.temp_ap_payments';

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return CApPayments::fetchApPayments( $strSql, $objClientDatabase );
	}

	public function fetchApRemittancesDetail( $objClientDatabase ) {
		return CApRemittances::fetchApRemittancesDetailByApPayeeIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchApRemittance( $objClientDatabase ) {
		return CApRemittances::fetchApRemittanceByApPayeeIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchApLegalEntity( $objClientDatabase ) {
	    return CApLegalEntities::fetchApLegalEntityByApPayeeIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchApRemittancesDetailByIds( $arrintApRemittanceIds, $objClientDatabase ) {
		return CApRemittances::fetchApRemittancesDetailByIdsByApPayeeIdByCid( $arrintApRemittanceIds, $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchApRemittanceDetailsById( $intApRemittanceId, $objClientDatabase ) {
		return CApRemittances::fetchApRemittanceDetailsByIdByApPayeeIdByCid( $intApRemittanceId, $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchVendorInfoDetails( $objClientDatabase ) {
		return CApPayees::fetchVendorInfoDetailsByIdByCidByApPayeeTermIdByGlAccountId( $this->getId(), $this->getCid(), $this->getApPayeeTermId(), $this->getGlAccountId(), $objClientDatabase );
	}

	public function fetchApPayeeQuickViewDetails( $objClientDatabase ) {
		return CApPayees::fetchApPayeeQuickViewDetailsByIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchApTransactionsCountByPropertyIds( $arrintPropertyIds, $objClientDatabase, $arrintInvoiceTypePermissions, $objApTransactionsFilter ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintInvoiceTypePermissions ) ) {
			return NULL;
		}

		$this->createTempTableApTransactionsByPropertyIds( $arrintPropertyIds, $objClientDatabase, $arrintInvoiceTypePermissions, $objApTransactionsFilter );

		$strSql = 'SELECT count( * ) FROM pg_temp.temp_ap_transactions;';

		return CApHeaders::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public function fetchApHeadersCountByCidByPropertyIds( $arrmixParameters, $intCid, $objClientDatabase ) {
		if( false == valArr( getArrayElementByKey( 'ap_header_sub_type_ids', $arrmixParameters ) ) ) {
			return NULL;
		}
		$this->createTempTableApHeadersByCidByPropertyIds( $arrmixParameters, $intCid, $objClientDatabase );

		$strSql = 'SELECT COUNT( id ) FROM pg_temp.temp_ap_headers';

		return CApHeaders::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public function fetchApPaymentsCount( $intCid, $objClientDatabase, $objApTransactionsFilter = NULL, $arrintCompanyUserPropertyIds = NULL ) {

		$this->createTempTableApPayments( $intCid, $objClientDatabase, $objApTransactionsFilter, $arrintCompanyUserPropertyIds );

		$strSql = 'SELECT COUNT( id ) FROM pg_temp.temp_ap_payments';

		return CApPayments::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchApPayeeContactsBySearchKeywordByCid( $strApPayeeContact, $intCid, $objClientDatabase ) {

		$strSqlApPayeeContact	= '';
		$strSqlLimit			= '';

		if( true == valStr( $strApPayeeContact ) ) {
			$strSqlLimit		= ' LIMIT 15 ';

			$arrstrApPayeeContactNames = explode( ', ', $strApPayeeContact );
			$arrstrApPayeeContactNames = explode( ',', $arrstrApPayeeContactNames[0] );

			if( true == valArr( $arrstrApPayeeContactNames ) && true == valStr( $arrstrApPayeeContactNames[0] ) && true == valStr( $arrstrApPayeeContactNames[1] ) ) {
				$strSqlApPayeeContact .= ' AND ( lower( apc.name_first ) ILIKE \'%' . addslashes( strtolower( $arrstrApPayeeContactNames[1] ) ) . '%\'
											AND
											lower( apc.name_last ) ILIKE \'%' . addslashes( strtolower( $arrstrApPayeeContactNames[0] ) ) . '%\'
											) ';
			} elseif( true == valArr( $arrstrApPayeeContactNames ) && true == valStr( $arrstrApPayeeContactNames[0] ) && false == valStr( $arrstrApPayeeContactNames[1] ) ) {
				$strSqlApPayeeContact .= ' AND ( lower( apc.name_first ) ILIKE \'%' . addslashes( strtolower( $arrstrApPayeeContactNames[0] ) ) . '%\'
											OR
											lower( apc.name_last ) ILIKE \'%' . addslashes( strtolower( $arrstrApPayeeContactNames[0] ) ) . '%\'
											) ';
			}
		}

		$strSql = 'SELECT 
						func_format_customer_name( apc.name_first, apc.name_last, NULL) as contact_name
					FROM
						ap_payee_contacts apc
						JOIN ap_payees ap ON ( apc.cid = ap.cid AND apc.ap_payee_id = ap.id AND apc.disabled_by IS NULL )
					WHERE
						apc.cid = ' . ( int ) $intCid . '
						' . $strSqlApPayeeContact . '
						AND ap.ap_payee_type_id NOT IN ( ' . CApPayeeType::EMPLOYEE . ', ' . CApPayeeType::CORPORATE . ' )
						' . $strSqlLimit . ' ';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeExternalIdsBySearchKeywordByCid( $strApPayeeExternalId, $intCid, $objClientDatabase ) {

		$strSqlApPayeeExternalId	= '';
		$strSqlLimit			= '';

		if( true == valStr( $strApPayeeExternalId ) ) {
			$strSqlLimit		= ' LIMIT 15 ';

			$strSqlApPayeeExternalId	= ' AND lower( ap.secondary_number ) ILIKE \'%' . strtolower( $strApPayeeExternalId ) . '%\'';
		}

		$strSql = 'SELECT 
 					    ap.id,ap.secondary_number
					FROM
						ap_payees ap
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strSqlApPayeeExternalId . '
						AND ap.ap_payee_type_id NOT IN ( ' . CApPayeeType::EMPLOYEE . ', ' . CApPayeeType::CORPORATE . ' )
						' . $strSqlLimit . ' ';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeEmailIdsBySearchKeywordByCid( $strApPayeeEmailId, $intCid, $objClientDatabase ) {

		$strSqlApPayeeEmailId	= '';
		$strSqlLimit			= '';

		if( true == valStr( $strApPayeeEmailId ) ) {
			$strSqlLimit		= ' LIMIT 15 ';

			$strSqlApPayeeEmailId	= ' AND  lower( apc.email_address ) ILIKE \'%' . strtolower( $strApPayeeEmailId ) . '%\'';
		}

		$strSql = ' SELECT 
						apc.email_address
					FROM
						ap_payee_contacts apc
						JOIN ap_payees ap ON ( apc.cid = ap.cid AND apc.ap_payee_id = ap.id AND apc.disabled_by IS NULL )
					WHERE
						apc.cid = ' . ( int ) $intCid . '
						' . $strSqlApPayeeEmailId . '
						AND ap.ap_payee_type_id NOT IN ( ' . CApPayeeType::EMPLOYEE . ', ' . CApPayeeType::CORPORATE . ' )
						' . $strSqlLimit . ' ';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeNamesBySearchKeywordByCid( $strApPayeeName, array $arrintSelectedIds = [], $intCid, $objClientDatabase ) {

		$strSqlApPayeeName	= ' lower(ap.company_name) ILIKE \'%' . strtolower( $strApPayeeName ) . '%\'';

		if( true == valIntArr( $arrintSelectedIds ) && false == valStr( $strApPayeeName ) ) {
			$strSqlApPayeeName = ' AND ( ap.id IN ( ' . sqlIntImplode( $arrintSelectedIds ) . ' ) ) ';
		} elseif( true == valIntArr( $arrintSelectedIds ) && true == valStr( $strApPayeeName ) ) {
			$strSqlApPayeeName = ' AND ( ap.id NOT IN ( ' . sqlIntImplode( $arrintSelectedIds ) . ' ) AND ' . $strSqlApPayeeName . ' ) ';
		} else {
			$strSqlApPayeeName = ' AND ( ' . $strSqlApPayeeName . ' ) ';
		}

		$strSql = 'select 
					DISTINCT ap.id,ap.company_name
				FROM
					ap_payees ap
				JOIN ap_payee_property_groups appg ON ( appg.cid = ap.cid AND appg.ap_payee_id = ap.id )
				WHERE
					ap.cid = ' . ( int ) $intCid . '
					' . $strSqlApPayeeName . '  
					AND ap.ap_payee_type_id NOT IN ( ' . CApPayeeType::EMPLOYEE . ', ' . CApPayeeType::CORPORATE . ', ' . CApPayeeType::PROPERTY_SOLUTIONS . ' )
				ORDER BY 
					ap.company_name asc
				LIMIT 15';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeAddressesBySearchKeywordByCid( $strApPayeeAddress, $intCid, $objClientDatabase ) {

		$strSqlApPayeeAddress	= '';
		$strSqlLimit			= '';

		if( true == valStr( $strApPayeeAddress ) ) {
			$strSqlLimit		= ' LIMIT 15 ';

			$strSqlApPayeeAddress	= ' AND ( lower( apl.street_line1 ) ILIKE \'%' . strtolower( $strApPayeeAddress ) . '%\'
                        	OR lower( apl.street_line2 ) ILIKE \'%' . strtolower( $strApPayeeAddress ) . '%\'
                            OR lower( apl.street_line3 ) ILIKE \'%' . strtolower( $strApPayeeAddress ) . '%\'
                            OR lower( apl.city ) ILIKE \'%' . strtolower( $strApPayeeAddress ) . '%\'
                            OR lower( apl.state_code ) ILIKE \'%' . strtolower( $strApPayeeAddress ) . '%\'
                            OR lower( apl.postal_code ) ILIKE \'%' . strtolower( $strApPayeeAddress ) . '%\' )';
		}

		$strSql = 'SELECT 
						DISTINCT apl.id,
								apl.street_line1,
								apl.street_line2,
								apl.street_line3,
								apl.city, 
								apl.state_code,
								apl.postal_code
					FROM
						ap_payee_locations apl
						JOIN ap_payees ap ON ( apl.cid = ap.cid AND apl.ap_payee_id = ap.id AND apl.deleted_by IS NULL )
					WHERE
						apl.cid = ' . ( int ) $intCid . '
						' . $strSqlApPayeeAddress . ' 
						AND ap.ap_payee_type_id NOT IN ( ' . CApPayeeType::EMPLOYEE . ', ' . CApPayeeType::CORPORATE . ' )
						' . $strSqlLimit . ' ';

		return fetchData( $strSql, $objClientDatabase );

	}

	/**
	 * Other Functions
	 *
	 */

	public function init( $intCurrentUserId, $objClientDatabase ) {
		$objDataset = $objClientDatabase->createDataset();

		$strSql = 'SELECT * ' .
		'FROM ap_payees_init( ' .
		$this->sqlId() . ', ' .
		( int ) $intCurrentUserId . ' ) AS result;';

		if( false == $objDataset->execute( $strSql ) ) {
			// Unset new id on error
			$this->setId( NULL );

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to initialize ap payee record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objClientDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			// Unset new id on error
			$this->setId( NULL );

			while( !$objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function exportUpdateApPayee( $intCompanyUserId, $objDatabase ) {
		if( false == valStr( $this->getRemotePrimaryKey() ) ) return true;

		$objIntegrationClient = \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchIntegrationClientByIntegrationDatabaseIdByServiceIdByCid( $this->getIntegrationDatabaseId(), CIntegrationService::UPDATE_AP_PAYEE, $this->getCid(), $objDatabase );
		if( false == valObj( $objIntegrationClient, 'CIntegrationClient' ) ) return true;

		$objGenericWorker = CIntegrationFactory::createWorkerFromClient( $objIntegrationClient, $intCompanyUserId, $objDatabase );

		$objGenericWorker->setApPayee( $this );
		$objGenericWorker->setDatabase( $objDatabase );

		$boolIsSuccessful = $objGenericWorker->process();

		if( false == $boolIsSuccessful && true == valArr( $objGenericWorker->getErrorMsgs() ) ) {
			foreach( $objGenericWorker->getErrorMsgs() as $objErrorMsg ) {
				$this->addErrorMsg( $objErrorMsg );
			}
		}

		return $boolIsSuccessful;
	}

	public function importApPayee( $intCompanyUserId, $objDatabase ) {
		if( false == valStr( $this->getRemotePrimaryKey() ) ) return true;

		$objIntegrationClient = \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchIntegrationClientByIntegrationDatabaseIdByServiceIdByCid( $this->getIntegrationDatabaseId(), CIntegrationService::RETRIEVE_AP_PAYEE, $this->getCid(), $objDatabase );
		if( false == valObj( $objIntegrationClient, 'CIntegrationClient' ) ) return true;

		$objGenericWorker = CIntegrationFactory::createWorkerFromClient( $objIntegrationClient, $intCompanyUserId, $objDatabase );

		$objGenericWorker->setApPayee( $this );

		$boolIsSuccessful = $objGenericWorker->process();

		if( false == $boolIsSuccessful && true == valArr( $objGenericWorker->getErrorMsgs() ) ) {
			foreach( $objGenericWorker->getErrorMsgs() as $objErrorMsg ) {
				$this->addErrorMsg( $objErrorMsg );
			}
		}

		return $boolIsSuccessful;
	}

	public function importLedger( $intCompanyUserId, $objDatabase, $boolIsHistoricalCall = false ) {
		if( false == valStr( $this->getRemotePrimaryKey() ) ) return true;

		$objIntegrationClient = \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchIntegrationClientByIntegrationDatabaseIdByServiceIdByCid( $this->getIntegrationDatabaseId(), CIntegrationService::RETRIEVE_AP_TRANSACTIONS, $this->getCid(), $objDatabase );

		if( false == valObj( $objIntegrationClient, 'CIntegrationClient' ) ) return true;

		$objGenericWorker = CIntegrationFactory::createWorkerFromClient( $objIntegrationClient, $intCompanyUserId, $objDatabase );

		$objGenericWorker->setApPayee( $this );

		if( true == $boolIsHistoricalCall ) {
			$objGenericWorker->setTransportRules( [ 'is_historical_call' => $boolIsHistoricalCall ] );
		}

		$boolIsSuccessful = $objGenericWorker->process();

		if( false == $boolIsSuccessful && true == valArr( $objGenericWorker->getErrorMsgs() ) ) {
			foreach( $objGenericWorker->getErrorMsgs() as $objErrorMsg ) {
				$this->addErrorMsg( $objErrorMsg );
			}
		}

		return $boolIsSuccessful;
	}

	public function exportLedger( $intCompanyUserId, $objAdminDatabase, $objDatabase, $intApHeaderId ) {
		if( false == valStr( $this->getRemotePrimaryKey() ) ) return true;
		if( false == valStr( $intApHeaderId ) ) return true;

		$objIntegrationClient = \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchIntegrationClientByIntegrationDatabaseIdByServiceIdByCid( $this->getIntegrationDatabaseId(), CIntegrationService::SEND_AP_TRANSACTIONS, $this->getCid(), $objDatabase );
		if( false == valObj( $objIntegrationClient, 'CIntegrationClient' ) ) return true;

		$objGenericWorker = CIntegrationFactory::createWorkerFromClient( $objIntegrationClient, $intCompanyUserId, $objDatabase );

		$objGenericWorker->setApPayee( $this );
		$objGenericWorker->setDatabase( $objDatabase );
		$objGenericWorker->setAdminDatabase( $objAdminDatabase );
		$objGenericWorker->setApHeaderId( $intApHeaderId );

		$boolIsSuccessful = $objGenericWorker->process();

		if( false == $boolIsSuccessful && true == valArr( $objGenericWorker->getErrorMsgs() ) ) {
			foreach( $objGenericWorker->getErrorMsgs() as $objErrorMsg ) {
				$this->addErrorMsg( $objErrorMsg );
			}
		}

		return $boolIsSuccessful;
	}

	public function validateApPayeePolicy( $arrobjApDetails, $strStatusAllowance, $intApLegalEntityId, $objClientDatabase ) {

		if( false == valArr( $arrobjApDetails ) ) {
			return NULL;
		}

		$boolIsValid = true;

		$arrintPropertyIds = array_filter( array_keys( $arrobjRekeyedApPayees = rekeyObjects( 'PropertyId', $arrobjApDetails ) ) );

		$arrmixFilterData['property_ids']        = $arrintPropertyIds;
		$arrmixFilterData['ap_payee_ids']        = [ $this->getId() ];
		$arrmixFilterData['ap_legal_entity_ids'] = [ $intApLegalEntityId ];

		$arrmixStatusAllowancesData = ( array ) \Psi\Eos\Entrata\CApPayeeComplianceStatuses::createService()->fetchStatusAllowancesByCidsByStatusAllowance( [ $this->getCid() ], $strStatusAllowance, $objClientDatabase, $arrmixFilterData );
		$arrmixStatusAllowances     = rekeyArray( 'property_id', $arrmixStatusAllowancesData );

		foreach( $arrmixStatusAllowances as $intPropertyId => $arrmixStatusAllowance ) {

			$boolIsAllow = getArrayElementByKey( 'is_allow_processing', $arrmixStatusAllowance );

			if( true == CStrings::strToBool( $boolIsAllow ) ) {
				continue;
			}

			$strComplianceFor         = str_replace( '_', ' ', $arrmixStatusAllowance['compliance_for'] );
			$intComplianceRulesetId   = $arrmixStatusAllowance['compliance_ruleset_id'];
			$strComplianceRulesetName = $arrmixStatusAllowance['compliance_ruleset_name'];

			$objApDetail = getArrayElementByKey( $intPropertyId, $arrobjRekeyedApPayees );

			$arrstrPropertyName[] = $objApDetail->getPropertyName();

			$arrstrRulesetLink[$arrmixStatusAllowance['compliance_ruleset_id']] = '<a class="view_ruleset" data-ruleset-id="' . ( int ) $intComplianceRulesetId . '" data-ruleset-title="' . $strComplianceRulesetName . '"  href="javascript:void(0)">' . $strComplianceRulesetName . '</a>';

			$boolIsValid &= false;
		}

		if( false == $boolIsValid ) $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'compliance_item', 'You cannot create/approve the ' . $strComplianceFor . ' because the vendor is non-compliant for <u> ' . implode( ', ', $arrstrPropertyName ) . '</u> property(ies) ( View rule set(s): ' . implode( ', ', $arrstrRulesetLink ) . ' ).<br>' ) );

		return $boolIsValid;
	}

	public function createApPayee( $intCid, $intCurrentUserId, $arrmixApPayeeDetails = [], $objDatabase ) {

		$intCount				= 1;
		$intApPayeeId			= $this->fetchNextId( $objDatabase );
		$intApPayeeLocationId	= ( new CApPayeeLocation() )->fetchNextId( $objDatabase );

		$objPropertyGroup		= \Psi\Eos\Entrata\CPropertyGroups::createService()->fetchPropertyGroupBySystemCodeByCid( 'ALL', $intCid, $objDatabase );
		$intPropertyGroupId		= ( ( true == valObj( $objPropertyGroup, 'CPropertyGroup' ) ) ? $objPropertyGroup->getId() : NULL );

		$arrmixDefaultValues	= [        // Dont change order of array items
			'id'								=> $intApPayeeId,
			'cid'								=> $intCid,
			'integration_database_id'			=> NULL,
			'ap_payee_term_id'					=> NULL,
			'gl_account_id'						=> NULL,
			'vendor_id'							=> NULL,
			'ap_payee_type_id'					=> CApPayeeType::EMPLOYEE,
			'ap_payee_status_type_id'			=> CApPayeeStatusType::ACTIVE,
			'categorization_type_id'			=> CCategorizationType::MANUAL,
			'confidentiality_allowance_type_id'	=> 1,
			'cam_allowance_type_id'				=> 1,
			'vendor-category_type_id'			=> NULL,
			'compliance_rulset_id'				=> NULL,
			'reference_id'						=> NULL,
			'customer_id'						=> NULL,
			'secondary_number'					=> NULL,
			'remote_primary_key'				=> NULL,
			'system_code'						=> NULL,
			'company_name'						=> NULL,
			'username'							=> NULL,
			'website_url'						=> NULL,
			'notes'								=> NULL,
			'is_categorization_queued'			=> false,
			'is_consolidated'					=> false,
			'is_system'							=> false,
			'is_on_site'						=> false,
			'ap_payment_type_id'				=> CApPaymentType::CHECK,
			'check_account_type_id'				=> CCheckAccountType::BUSINESS_CHECKING,
			'check_name_on_account'				=> NULL,
			'check_bank_name'					=> NULL,
			'ap_remittance_name'				=> NULL,
			'check_routing_number'				=> NULL,
			'check_account_number_encrypted'	=> NULL,
			'street_line1'						=> NULL,
			'street_line2'						=> NULL,
			'street_line3'						=> NULL,
			'city'								=> NULL,
			'state_code'						=> NULL,
			'postal_code'						=> NULL,
			'province'							=> NULL,
			'form_1099_type_id'					=> 1,
			'form_1099_box_type_id'				=> CForm1099BoxType::FORM_1099_BOX_1,
			'owner_type_id'						=> NULL,
			'entity_name'						=> 'Unknown',
			'receives_1099'						=> true,
			'tax_number_encrypted'				=> NULL,
			'ap_payee_location_id'				=> $intApPayeeLocationId,
			'store_id'							=> NULL,
			'payee_name'						=> NULL,
			'location_name'						=> 'Corporate',
			'location_datetime'					=> 'NOW()',
			'phone_number'						=> NULL,
			'country_code'						=> NULL,
			'is_primary'						=> true,
			'worker_id'							=> NULL,
			'name_first'						=> NULL,
			'name_middle'						=> NULL,
			'name_last'							=> NULL,
			'title'								=> NULL,
			'fax_number'						=> NULL,
			'mobile_number'						=> NULL,
			'email_address'						=> NULL,
			'is_verified'						=> false,
			'disabled_by'						=> NULL,
			'disabled_on'						=> NULL,
			'current_user_id'					=> $intCurrentUserId,
			'duns_number'						=> NULL,
			'property_group_id'					=> $intPropertyGroupId,
			'is_default'						=> true
		];

		$arrmixDefaultValues = array_merge( $arrmixDefaultValues, $arrmixApPayeeDetails );

		foreach( $arrmixDefaultValues as $intKey => $strDefaultValue ) {

			if( true == preg_match( "/'/u", $strDefaultValue ) ) {
				$arrmixDefaultValues['f' . $intCount] = str_replace( "'", "''", $strDefaultValue );
			} else {
				$arrmixDefaultValues['f' . $intCount] = $strDefaultValue;
			}
			unset( $arrmixDefaultValues[$intKey] );

			$intCount++;
		}

		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * FROM ap_payees_insert( \'' . json_encode( $arrmixDefaultValues ) . '\' ) AS result;';

		if( false == $objDataset->execute( $strSql ) ) {
			$objDatabase->rollback();
			return [ 'status' => 'error' ];
		}

		$objApRemittance	= CApRemittances::fetchApRemittanceByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase );
		$objApPayeeAccount	= CApPayeeAccounts::fetchApPayeeAccountByApPayeeIdByApPayeeLocationIdByCid( $intApPayeeId, $intApPayeeLocationId, $intCid, $objDatabase );

		return [
			'status' => 'success',
			'data' => [
				'id'					=> $intApPayeeId,
				'ap_remittance_id'		=> $objApRemittance->getId(),
				'ap_payee_location_id'	=> $intApPayeeLocationId,
				'ap_payee_account_id'	=> $objApPayeeAccount->getId()
			]
		];
	}

	public function createEvent( $intEventTypeId, $intEventSubTypeId = NULL, $strEventHandle = NULL, $strNotes = NULL, $objDatabase = NULL ) {
		if( false == valId( $intEventTypeId ) ) return NULL;

		$objEvent = new CEvent();
		$objEvent->setCid( $this->getCid() );
		$objEvent->setEventTypeId( $intEventTypeId );
		$objEvent->setEventSubTypeId( $intEventSubTypeId );
		$objEvent->setEventDatetime( 'NOW()' );
		$objEvent->setEventHandle( $strEventHandle );
		$objEvent->setNotes( $strNotes );

		if( true == valObj( $objDatabase, 'CDatabase' ) ) {
			$arrintPropertyIds 	= ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchManagerialPropertyIdsByCid( $this->getCid(), $objDatabase );
			if( true == valArr( $arrintPropertyIds ) ) {
				$objEvent->setPropertyId( current( $arrintPropertyIds ) );
			}
		}

		$objEventReference = $objEvent->createEventReference();
		$objEventReference->setEventReferenceTypeId( CEventReferenceType::VENDOR );
		$objEventReference->setReferenceId( $this->getId() );

		$objEvent->setEventReferences( [ $objEventReference ] );

		return $objEvent;
	}

	public function fetchEventsByEventTypeIds( $arrintEventTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchEventsByReferenceIdByReferenceTypeIdByEventTypeIdsByCid( $this->getId(), CEventReferenceType::VENDOR, $arrintEventTypeIds, $this->getCid(), $objDatabase, 'AND e.event_type_id NOT IN ( ' . CEventType::ADD_ON . ' )' );
	}

	public static function deleteApPayee( $intApPayeeId, $objClient, $intCompanyUserId, $objDatabase, $objUtilitiesDatabase ) {

		$objApPayee = CApPayees::fetchApPayeeByIdByCid( $intApPayeeId, $objClient->getId(), $objDatabase );
		if( false == valObj( $objApPayee, CApPayee::class ) ) {
			return [ result => false, message => 'Vendor not found.' ];
		}

		$arrmixJsonData = ( array ) self::isApPayeeTransactionData( $intApPayeeId, $objClient->getId(), $objDatabase, true );

		if( true == $arrmixJsonData['result'] ) return [ result => false, message => $arrmixJsonData['message'] ];

		$arrobjApRemittances				= ( array ) CApRemittances::fetchApRemittancesByApPayeeIdsByCid( [ $intApPayeeId ], $objClient->getId(), $objDatabase, false );
		$arrobjApPayeeAccounts				= ( array ) CApPayeeAccounts::fetchApPayeeAccountsByApPayeeIdsByCid( [ $intApPayeeId ], $objClient->getId(), $objDatabase );
		$arrobjApPayeeContacts				= ( array ) CApPayeeContacts::fetchApPayeeContactsByApPayeeIdByCid( $intApPayeeId, $objClient->getId(), $objDatabase, false );
		$arrobjApLegalEntities				= ( array ) CApLegalEntities::fetchApLegalEntitiesByApPayeeIdByCid( $intApPayeeId, $objClient->getId(), $objDatabase );
		$arrobjFileAssociations				= ( array ) \Psi\Eos\Entrata\CFileAssociations::createService()->fetchFileAssociationsByApPayeeIdByCid( $intApPayeeId, $objClient->getId(), $objDatabase );
		$arrobjApPayeeLocations				= ( array ) CApPayeeLocations::fetchApPayeeLocationsByApPayeeIdsByCid( [ $intApPayeeId ], $objClient->getId(), $objDatabase );
		$arrobjApPayeeProperties			= ( array ) CApPayeeProperties::fetchApPayeePropertiesByApPayeeIdByCid( $intApPayeeId, $objClient->getId(), $objDatabase );
		$arrobjApPayeePropertyGroups		= ( array ) CApPayeePropertyGroups::fetchApPayeePropertyGroupsByApPayeeIdsByCid( [ $intApPayeeId ], $objClient->getId(), $objDatabase );
		$arrobjApPayee1099Adjustments		= ( array ) CApPayee1099Adjustments::fetchApPayee1099AdjustmentsByApPayeeIdByCid( $intApPayeeId, $objClient->getId(), $objDatabase );
		$arrobjApPayeeSubAccounts			= ( array ) CApPayeeSubAccounts::fetchApPayeeSubAccountsByApPayeeAccountIdsByCid( array_keys( $arrobjApPayeeAccounts ), $objClient->getId(), $objDatabase );
		$arrobjApPayeeContactLocations		= ( array ) CApPayeeContactLocations::fetchApPayeeContactLocationsByApPayeeContactIdsByApPayeeLocationIdsByCid( array_keys( $arrobjApPayeeLocations ), array_keys( $arrobjApPayeeContacts ), $objClient->getId(), $objDatabase );
		$arrobjJobFundingSources			= ( array ) CJobFundingSources::createService()->fetchJobFundingSourcesByApPayeeIdByCid( $intApPayeeId, $objClient->getId(), $objDatabase );
		$arrobjOwnerDocuments				= ( array ) COwnerDocuments::fetchOwnerDocumentsByApPayeeIdByCid( $intApPayeeId, $objClient->getId(), $objDatabase );
		$arrobjApPayeeComplianceStatuses	= ( array ) \Psi\Eos\Entrata\CApPayeeComplianceStatuses::createService()->fetchApPayeeComplianceStatusesByApPayeeIdsByCid( [ $intApPayeeId ], $objClient->getId(), $objDatabase );
		$arrobjPropertyGlSettings			= ( array ) CPropertyGlSettings::fetchPropertyGlSettingsByUnclaimedPropertyApPayeeIdByCid( $intApPayeeId, $objClient->getId(), $objDatabase );

		switch( NULL ) {
			default:
				$objDatabase->begin();
				$objUtilitiesDatabase->begin();

				if( false == self::deleteVendorCompliance( $intApPayeeId, $objClient->getId(), $intCompanyUserId, $objUtilitiesDatabase, $objDatabase ) ) {
					$objDatabase->rollback();
					$objUtilitiesDatabase->rollback();
					break;
				}

				foreach( $arrobjFileAssociations as $objFileAssociation ) {
					if( true == valObj( $objFileAssociation, 'CFileAssociation' ) && false == self::deleteVendorDocument( $objFileAssociation->getFileId(), $objApPayee, $intCompanyUserId, $objDatabase, $objClient ) ) {
						$objDatabase->rollback();
						break 2;
					}
				}

				if( true == valArr( $arrobjOwnerDocuments ) && false == COwnerDocuments::bulkDelete( $arrobjOwnerDocuments, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				if( true == valArr( $arrobjApPayeeSubAccounts ) && false == CApPayeeSubAccounts::bulkDelete( $arrobjApPayeeSubAccounts, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				foreach( $arrobjApPayee1099Adjustments as $objApPayee1099Adjustment ) {
					if( true == valObj( $objApPayee1099Adjustment, 'CApPayee1099Adjustment' ) && false == $objApPayee1099Adjustment->delete( $intCompanyUserId, $objDatabase ) ) {
						$objDatabase->rollback();
						break 2;
					}
				}

				if( true == valArr( $arrobjApPayeeAccounts ) && false == CApPayeeAccounts::bulkDelete( $arrobjApPayeeAccounts, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				if( true == valArr( $arrobjApPayeeContactLocations ) && false == CApPayeeContactLocations::bulkDelete( $arrobjApPayeeContactLocations, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				if( true == valArr( $arrobjApPayeePropertyGroups ) && false == CApPayeePropertyGroups::bulkDelete( $arrobjApPayeePropertyGroups, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				if( true == valArr( $arrobjApPayeeProperties ) && false == CApPayeeProperties::bulkDelete( $arrobjApPayeeProperties, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				foreach( $arrobjApLegalEntities as $objApLegalEntities ) {
					$objApLegalEntities->setApRemittanceId( NULL );
				}

				if( false == CApLegalEntities::bulkUpdate( $arrobjApLegalEntities, [ 'ap_remittance_id' ], $intCompanyUserId, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				$objApPayee->setDefaultApRemittanceId( NULL );
				if( false == $objApPayee->update( $intCompanyUserId, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				if( true == valArr( $arrobjApRemittances ) && false == CApRemittances::bulkDelete( $arrobjApRemittances, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				if( true == valArr( $arrobjApPayeeLocations ) && false == CApPayeeLocations::bulkDelete( $arrobjApPayeeLocations, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				if( true == valArr( $arrobjApPayeeComplianceStatuses ) && false == \Psi\Eos\Entrata\CApPayeeComplianceStatuses::createService()->bulkDelete( $arrobjApPayeeComplianceStatuses, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				if( true == valArr( $arrobjApLegalEntities ) && false == CApLegalEntities::bulkDelete( $arrobjApLegalEntities, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				if( true == valArr( $arrobjApPayeeContacts ) && false == CApPayeeContacts::bulkDelete( $arrobjApPayeeContacts, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				if( true == valArr( $arrobjJobFundingSources ) && false == CJobFundingSources::createService()->bulkDelete( $arrobjJobFundingSources, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				foreach( $arrobjPropertyGlSettings as $objPropertyGlSetting ) {
					$objPropertyGlSetting->setUnclaimedPropertyApPayeeId( NULL );
				}

				if( true == valArr( $arrobjPropertyGlSettings ) && false == CPropertyGlSettings::bulkUpdate( $arrobjPropertyGlSettings, [ 'unclaimed_property_ap_payee_id' ], $intCompanyUserId, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				if( false == $objApPayee->delete( $intCompanyUserId, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				$objUtilitiesDatabase->commit();
				$objDatabase->commit();

				return [ 'result' => true ];
		}

		return [ 'result' => false, 'message' => 'Failed to delete vendor.' ];
	}

	public static function isApPayeeTransactionData( $intApPayeeId, $intCId, $objDatabase, $boolIsCalledFromDelete = false, $boolIncludeSkus = false ) {

		$arrobjApCatalogItems        = [];
		$objApPayeeRemittance        = [];
		$objApPayeeLegalEntity       = [];

		if( true == $boolIsCalledFromDelete ) {
			$objApPayeeRemittance  = CApRemittances::fetchApRemittanceByApPayeeIdByCid( $intApPayeeId, $intCId, $objDatabase, true );
			$objApPayeeLegalEntity = CApLegalEntities::fetchApLegalEntityByApPayeeIdByCid( $intApPayeeId, $intCId, $objDatabase, true );
		}

		if( true == $boolIncludeSkus ) {
			$arrobjApCatalogItems = ( array ) CBaseApCatalogItems::fetchApCatalogItemsByApPayeeIdByCid( $intApPayeeId, $intCId, $objDatabase );
		}

		$objApPayeeTransaction				= CApHeaders::fetchApHeaderByApPayeeIdByApHeaderTypeIdsByCid( $intApPayeeId, [ CApHeaderType::PURCHASE_ORDER, CApHeaderType::INVOICE ], $intCId, $objDatabase );
		$objMaintenanceRequest				= \Psi\Eos\Entrata\CMaintenanceRequests::createService()->fetchMaintenanceRequestByApPayeeIdByCid( $intApPayeeId, $intCId, $objDatabase );
		$objScheduledApTransactionHeader	= \Psi\Eos\Entrata\CScheduledApTransactionHeaders::createService()->fetchScheduledApTransactionHeaderByApPayeeIdByCid( $intApPayeeId, $intCId, $objDatabase );
		$arrintDrawRequestsCount            = ( array ) \Psi\Eos\Entrata\CDrawRequests::createService()->fetchDrawRequestsCountByApPayeeIdByCid( $intApPayeeId, $intCId, $objDatabase );
		$intTotalDrawRequestCount           = ( true == valArr( $arrintDrawRequestsCount[0] ) && true == isset( $arrintDrawRequestsCount[0]['total_draw_count'] ) ) ? $arrintDrawRequestsCount[0]['total_draw_count'] : 0;
		$intFinalizeDrawRequestCount        = ( true == valArr( $arrintDrawRequestsCount[0] ) && true == isset( $arrintDrawRequestsCount[0]['finalize_draw_count'] ) ) ? $arrintDrawRequestsCount[0]['finalize_draw_count'] : 0;
		$arrobjAssetDetails					= \Psi\Eos\Entrata\CAssetDetails::createService()->fetchAssetDetailsByApPayeeIdByCid( $intApPayeeId, $intCId, $objDatabase );
		$arrobjApHeaderLogs                 = ( array ) \Psi\Eos\Entrata\CApHeaderLogs::createService()->fetchApHeaderLogsByApPayeeIdByCid( $intApPayeeId, $intCId, $objDatabase );

		if( true == valObj( $objApPayeeTransaction, CApHeader::class )
		    || true == valObj( $objApPayeeLegalEntity, CApLegalEntity::class )
		    || true == valObj( $objApPayeeRemittance, CApRemittance::class )
		    || true == valObj( $objScheduledApTransactionHeader, CScheduledApTransactionHeader::class )
		    || true == valObj( $objMaintenanceRequest, CMaintenanceRequest::class )
		    || true == valArr( $arrobjApHeaderLogs )
		    || true == ( 0 < $intTotalDrawRequestCount )
		    || true == valArr( $arrobjAssetDetails )
		    || ( true == $boolIncludeSkus && true == valArr( $arrobjApCatalogItems ) ) ) {

			$strErrorMsg = 'Failed to delete vendor.';

			if( true == $boolIsCalledFromDelete && ( true == valObj( $objApPayeeLegalEntity, CApLegalEntity::class ) || true == valObj( $objApPayeeRemittance, CApRemittance::class ) ) ) {

				$strErrorMsg = 'Failed to delete vendor, as Vendor has synced transactions present.';
			}

			return [ 'result' => true, 'message' => $strErrorMsg, 'totalDrawRequestCount' => $intTotalDrawRequestCount, 'finalizeDrawRequestCount' => $intFinalizeDrawRequestCount ];
		}
		return [ 'result' => false, 'totalDrawRequestCount' => $intTotalDrawRequestCount, 'finalizeDrawRequestCount' => $intFinalizeDrawRequestCount ];
	}

	public static function deleteVendorCompliance( $intApPayeeId, $intCId, $intCompanyUserId, $objUtilitiesDatabase, $objDatabase ) {

		$arrobjComplianceDocs = ( array ) \Psi\Eos\Utilities\CComplianceDocs::createService()->fetchComplianceDocsByApPayeeIdByCid( $intApPayeeId, $intCId, $objUtilitiesDatabase );
		$arrintComplianceDocIds = array_keys( $arrobjComplianceDocs );
		$arrobjComplianceJobs = ( array ) \Psi\Eos\Utilities\CComplianceJobs::createService()->fetchComplianceJobsByApPayeeIdByCid( $intApPayeeId, $intCId, $objUtilitiesDatabase );
		$arrobjComplianceRuleSetVendors     = ( array ) \Psi\Eos\Entrata\CComplianceRulesetVendors::createService()->fetchComplianceRulesetVendorsByApPayeeIdByCid( $intApPayeeId, $intCId, $objDatabase );
		$arrobjComplianceJobItems           = ( array ) \Psi\Eos\Utilities\CComplianceJobItems::createService()->fetchComplianceJobItemsByComplianceJobIdsByCid( array_keys( $arrobjComplianceJobs ), $intCId, $objUtilitiesDatabase, false );
		$arrobjDocs                         = ( array ) \Psi\Eos\Utilities\CDocs::createService()->fetchDocsByIds( array_filter( array_keys( ( array ) rekeyObjects( 'docId', $arrobjComplianceDocs ) ) ), $objUtilitiesDatabase );
		$arrobjComplianceJobItemDocs        = ( array ) \Psi\Eos\Utilities\CComplianceJobItemDocs::createService()->fetchComplianceJobItemDocsByComplianceDocIdsByCid( $arrintComplianceDocIds, $intCId, $objUtilitiesDatabase );
		$arrobjComplianceDocPolicies        = ( array ) \Psi\Eos\Utilities\CComplianceDocPolicies::createService()->fetchComplianceDocPoliciesByCidByComplianceDocIds( $intCId, $arrintComplianceDocIds, $objUtilitiesDatabase );
		$arrobjComplianceDocPropertyGroups  = ( array ) \Psi\Eos\Utilities\CComplianceDocPropertyGroups::createService()->fetchComplianceDocPropertyGroupsByComplianceDocIdsByCid( $arrintComplianceDocIds, $intCId, $objUtilitiesDatabase );
		$arrobjComplianceDocAssociations    = ( array ) \Psi\Eos\Utilities\CComplianceDocAssociations::createService()->fetchComplianceDocAssociationsByComplianceDocIdsByCid( $arrintComplianceDocIds, $intCId, $objUtilitiesDatabase );
		$arrobjComplianceDocLimits          = ( array ) \Psi\Eos\Utilities\CComplianceDocLimits::createService()->fetchComplianceDocLimitsByCidByComplianceDocPolicyIds( $intCId, array_keys( $arrobjComplianceDocPolicies ), $objUtilitiesDatabase );
		$arrobjVendorInvitations            = ( array ) \Psi\Eos\Utilities\CVendorInvitations::createService()->fetchVendorInvitationsByApPayeeIdByCid( $intApPayeeId, $intCId, $objUtilitiesDatabase );
		$arrobjComplianceDocVerifications	= ( array ) \Psi\Eos\Utilities\CComplianceDocVerifications::createService()->fetchAllComplianceDocVerificationsByCidByComplianceDocIds( $intCId, $arrintComplianceDocIds, $objUtilitiesDatabase );
		$arrobjComplianceDocVerificationAdditionalInsurers = ( array ) \Psi\Eos\Utilities\CComplianceDocVerificationAdditionalInsurers::createService()->fetchComplianceDocVerificationAdditionalInsurersByComplianceDocVerificationIdsByCid( $intCId, array_keys( $arrobjComplianceDocVerifications ), $objUtilitiesDatabase );

		switch( NULL ) {
			default:

				if( true == valArr( $arrobjComplianceDocPropertyGroups ) && false == \Psi\Eos\Utilities\CComplianceDocPropertyGroups::createService()->bulkDelete( $arrobjComplianceDocPropertyGroups, $objUtilitiesDatabase ) ) {
					break;
				}

				if( true == valArr( $arrobjComplianceDocLimits ) && false == \Psi\Eos\Utilities\CComplianceDocLimits::createService()->bulkDelete( $arrobjComplianceDocLimits, $objUtilitiesDatabase ) ) {
					break;
				}

				if( true == valArr( $arrobjComplianceDocPolicies ) && false == \Psi\Eos\Utilities\CComplianceDocPolicies::createService()->bulkDelete( $arrobjComplianceDocPolicies, $objUtilitiesDatabase ) ) {
					break;
				}

				if( true == valArr( $arrobjComplianceJobItemDocs ) && false == \Psi\Eos\Utilities\CComplianceJobItemDocs::createService()->bulkDelete( $arrobjComplianceJobItemDocs, $objUtilitiesDatabase ) ) {
					break;
				}

				if( true == valArr( $arrobjComplianceDocAssociations ) && false == \Psi\Eos\Utilities\CComplianceDocAssociations::createService()->bulkDelete( $arrobjComplianceDocAssociations, $objUtilitiesDatabase ) ) {
					break;
				}

				if( true == valArr( $arrobjComplianceDocVerificationAdditionalInsurers ) && false == \Psi\Eos\Utilities\CComplianceDocVerificationAdditionalInsurers::createService()->bulkDelete( $arrobjComplianceDocVerificationAdditionalInsurers, $objUtilitiesDatabase ) ) {
					break;
				}

				if( true == valArr( $arrobjComplianceDocVerifications ) && false == \Psi\Eos\Utilities\CComplianceDocVerifications::createService()->bulkDelete( $arrobjComplianceDocVerifications, $objUtilitiesDatabase ) ) {
					break;
				}

				foreach( $arrobjComplianceDocs as $objComplianceDoc ) {
					if( true == valObj( $objComplianceDoc, 'CComplianceDoc' ) && false == $objComplianceDoc->delete( $intCompanyUserId, $objUtilitiesDatabase ) ) {
						break 2;
					}
				}

				foreach( $arrobjDocs as $objDoc ) {
					if( true == valObj( $objDoc, 'CDoc' ) && false == $objDoc->delete( $intCompanyUserId, $objUtilitiesDatabase ) ) {
						break 2;
					}
					$strVendorDocPath = CDoc::loadEntrataDocumentPath( $intCId );
					$objDoc->deleteDocument( $strVendorDocPath );
				}

				if( true == valArr( $arrobjComplianceRuleSetVendors ) && false == \Psi\Eos\Entrata\CComplianceRulesetVendors::createService()->bulkDelete( $arrobjComplianceRuleSetVendors, $objDatabase ) ) {
					break;
				}

				if( true == valArr( $arrobjComplianceJobItems ) && false == \Psi\Eos\Utilities\CComplianceJobItems::createService()->bulkDelete( $arrobjComplianceJobItems, $objUtilitiesDatabase ) ) {
					break;
				}

				if( true == valArr( $arrobjComplianceJobs ) && false == \Psi\Eos\Utilities\CComplianceJobs::createService()->bulkDelete( $arrobjComplianceJobs, $objUtilitiesDatabase ) ) {
					break;
				}

				if( true == valArr( $arrobjVendorInvitations ) && false == \Psi\Eos\Utilities\CVendorInvitations::createService()->bulkDelete( $arrobjVendorInvitations, $objUtilitiesDatabase ) ) {
					break;
				}

				return true;
		}

		return false;
	}

	public static function deleteVendorDocument( $intFileId, $objApPayee, $intCompanyUserId, $objDatabase, $objClient ) {

		if( false == valId( $intFileId ) )  return false;

		$objFile = $objClient->fetchFileById( $intFileId, $objDatabase );
		if( false == valObj( $objFile, 'CFile' ) ) return false;

		$objFileAssociation     = $objClient->fetchFileAssociationByFileId( $objFile->getId(), $objDatabase );
		$arrobjFileEvents 		= ( array ) CFileEvents::fetchFileEventsByFileIdByCid( $objFile->getId(), $objFile->getCid(), $objDatabase );
		$arrobjFileMetadatas	= ( array ) CFileMetadatas::fetchFileMetadatasByFileIdByCid( $objFile->getId(), $objFile->getCid(), $objDatabase );

		switch( NULL ) {

			default:
				if( false == $objFileAssociation->delete( $intCompanyUserId, $objDatabase, false ) ) {
					$objDatabase->rollback();
					break;
				}

				if( true == valArr( $arrobjFileEvents ) && false == CFileEvents::bulkDelete( $arrobjFileEvents, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				if( true == valArr( $arrobjFileMetadatas ) && false == CFileMetadatas::bulkDelete( $arrobjFileMetadatas, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				if( false == $objFile->delete( $intCompanyUserId, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				$strFolderPath = ( false == is_null( $objFileAssociation->getVpDocumentId() ) ) ? PATH_MOUNTS_GLOBAL_VENDOR_DOCUMENTS . $objApPayee->getVendorId() . '/' : PATH_MOUNTS_FILES;
				$objFile->deleteFile( $strFolderPath );
				return true;
		}

		return false;
	}

	public function loadRemittanceAddress( $intApPayeeLocationId, $intApRemittanceId, $intLeaseCustomerId, $objDatabase, $boolIsPublished = true, $boolIsBillbackReimbursement = false ) {
		$arrmixRemittanceAddress = \Psi\Eos\Entrata\CApRemittances::createService()->fetchSimpleApRemittanceAddressByApPayeeLocationIdByApRemittanceIdByLeaseCustomerIdByCid( $intApPayeeLocationId, $intApRemittanceId, $intLeaseCustomerId, $this->getCid(), $objDatabase, $boolIsPublished, $boolIsBillbackReimbursement );

		if( false == valArr( $arrmixRemittanceAddress ) ) return;

		$this->setStreetLine1( $arrmixRemittanceAddress['street_line1'] );
		$this->setStreetLine2( $arrmixRemittanceAddress['street_line2'] );
		$this->setStreetLine3( $arrmixRemittanceAddress['street_line3'] );
		$this->setStateCode( $arrmixRemittanceAddress['state_code'] );
		$this->setPostalCode( $arrmixRemittanceAddress['postal_code'] );
		$this->setCity( $arrmixRemittanceAddress['city'] );
		$this->setRemittanceName( $arrmixRemittanceAddress['remittance_name'] );
		$this->setRemittanceType( $arrmixRemittanceAddress['remittance_type'] );

		$strApPayeeAddress = $this->getStreetLine1();
		$strApPayeeAddress .= ( true == valStr( $this->getStreetLine1() ) ? ', ' : '' ) . $this->getStreetLine2();
		$strApPayeeAddress .= ( true == valStr( $this->getStreetLine2() ) ? ', ' : '' ) . $this->getCity();
		$strApPayeeAddress .= ( true == valStr( $this->getCity() ) ? ', ' : '' ) . $this->getStateCode();
		$strApPayeeAddress .= ( true == valStr( $this->getStateCode() ) ? ', ' : '' ) . $this->getPostalCode();

		$this->setApPayeeAddress( $strApPayeeAddress );
	}

	public function loadCustomerAddress( $intLeaseCustomerId, $objDatabase ) {

		$objCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByLeaseCustomerIdByCid( $intLeaseCustomerId, $this->getCid(), $objDatabase );
		$arrintAddressTypeIds = [ CAddressType::FORWARDING, CAddressType::PERMANENT, CAddressType::CURRENT, CAddressType::PRIMARY ];

		$arrobjCustomerAddress = ( array ) \Psi\Eos\Entrata\CCustomerAddresses::createService()->fetchCustomerAddressesByCustomerIdByAddressTypeIdsByCid( $objCustomer->getId(), $arrintAddressTypeIds, $this->getCid(), $objDatabase );
		$arrobjCustomerAddresses = rekeyObjects( 'AddressTypeId', $arrobjCustomerAddress );

		$boolIsForwardingAddress = ( true == valObj( $arrobjCustomerAddresses[CAddressType::FORWARDING], 'CCustomerAddress' ) );
		$boolIsPermanentAddress = ( false == $boolIsForwardingAddress && true == valObj( $arrobjCustomerAddresses[CAddressType::PERMANENT], 'CCustomerAddress' ) );
		$boolIsCurrentAddress = ( false == $boolIsPermanentAddress && true == valObj( $arrobjCustomerAddresses[CAddressType::CURRENT], 'CCustomerAddress' ) );
		$boolIsPrimaryAddress = ( false == $boolIsCurrentAddress && true == valObj( $arrobjCustomerAddresses[CAddressType::PRIMARY], 'CCustomerAddress' ) );

		switch( $arrobjCustomerAddresses ) {
			case ( true == $boolIsForwardingAddress ):
				$objCustomerAddress = $arrobjCustomerAddresses[CAddressType::FORWARDING];
				break;

			case ( true == $boolIsPermanentAddress ):
				$objCustomerAddress = $arrobjCustomerAddresses[CAddressType::PERMANENT];
				break;

			case ( true == $boolIsCurrentAddress ):
				$objCustomerAddress = $arrobjCustomerAddresses[CAddressType::CURRENT];
				break;

			case ( true == $boolIsPrimaryAddress ):
				$objCustomerAddress = $arrobjCustomerAddresses[CAddressType::PRIMARY];
				break;

			default:
		}

		if( false == valObj( $objCustomerAddress, 'CCustomerAddress' ) ) {

			$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $objCustomer->getLeaseId(), $this->getCid(), $objDatabase );
			$objUnitOrPropertyAddress = NULL;
			if( true == valId( $objLease->getPropertyUnitId() ) ) {
				$objUnitOrPropertyAddress = \Psi\Eos\Entrata\CUnitAddresses::createService()->fetchUnitPrimaryAddressByPropertyUnitIdByCid( $objLease->getPropertyUnitId(), $this->getCid(), $objDatabase );
			}

			if( false == valObj( $objUnitOrPropertyAddress, 'CUnitAddress' ) || ( ( true == valObj( $objUnitOrPropertyAddress, 'CUnitAddress' ) && ( false == valStr( $objUnitOrPropertyAddress->getStreetLine1() ) || false == valStr( $objUnitOrPropertyAddress->getCity() ) || false == valStr( $objUnitOrPropertyAddress->getStateCode() ) || false == valStr( $objUnitOrPropertyAddress->getPostalCode() ) ) ) ) ) {
				$objUnitOrPropertyAddress = \Psi\Eos\Entrata\CPropertyAddresses::createService()->fetchPropertyAddressByPropertyIdByAddressTypeIdByCid( $objLease->getPropertyId(), ( array ) CAddressType::PRIMARY, $this->getCid(), $objDatabase );
			}

			if( true == valObj( $objUnitOrPropertyAddress, 'CUnitAddress' ) || true == valObj( $objUnitOrPropertyAddress, 'CPropertyAddress' ) ) {
				$this->setStreetLine1( $objUnitOrPropertyAddress->getStreetLine1() );
				$this->setStreetLine2( $objUnitOrPropertyAddress->getStreetLine2() );
				$this->setStreetLine3( $objUnitOrPropertyAddress->getStreetLine3() );
				$this->setStateCode( $objUnitOrPropertyAddress->getStateCode() );
				$this->setPostalCode( $objUnitOrPropertyAddress->getPostalCode() );
				$this->setCity( $objUnitOrPropertyAddress->getCity() );

			}
		} else {
			$this->setStreetLine1( $objCustomerAddress->getStreetLine1() );
			$this->setStreetLine2( $objCustomerAddress->getStreetLine2() );
			$this->setStreetLine3( $objCustomerAddress->getStreetLine3() );
			$this->setStateCode( $objCustomerAddress->getStateCode() );
			$this->setPostalCode( $objCustomerAddress->getPostalCode() );
			$this->setCity( $objCustomerAddress->getCity() );
		}

	}

	public function getVendorInsurancePolicyExpirationDate() {
		$arrmixComplainceMergeFieldValues = $this->getVendorInsurancePolicyDetails();
		$arrstrPolicyExpirationDates = [];

		foreach( $arrmixComplainceMergeFieldValues as $arrstrData ) {
			foreach( $arrstrData as $strKey => $strValue ) {
				if( $strKey == 'insurance_policy_expiration_date' ) {
					$arrstrPolicyExpirationDates[] = $strValue;
				}
			}
		}
		return implode( ', ', $arrstrPolicyExpirationDates );
	}

	public function getVendorInsurancePolicyType() {
		$arrmixComplainceMergeFieldValues = $this->getVendorInsurancePolicyDetails();
		$arrstrPolicyTypes = [];

		foreach( $arrmixComplainceMergeFieldValues as $arrstrData ) {
			foreach( $arrstrData as $strKey => $strValue ) {
				if( $strKey == 'insurance_policy_type' ) {
					$arrstrPolicyTypes[] = $strValue;
				}
			}
		}
		return implode( ', ', $arrstrPolicyTypes );
	}

	public function getVendorInsurancePolicyDetails() {
		$arrmixComplainceMergeFieldValues = [];
		$objUtilityDatabase = $this->loadUtilitiesDatabase();
		if( true == valObj( $objUtilityDatabase, 'CDatabase' ) ) {
			$objUtilityDatabase->open( false );
			$arrobjComplianceItems   = ( array ) \Psi\Eos\Utilities\CComplianceItems::createService()->fetchParentComplianceItemsByComplianceTypeIds( [ CComplianceType::INSURANCE ], $objUtilityDatabase );
			$arrintComplianceItemIds = array_keys( $arrobjComplianceItems );
			$arrintComplianceDocIds   = ( array ) \Psi\Eos\Utilities\CComplianceDocs::createService()->fetchComplianceDocIdsByApPayeeIdByComplianceItemIdsByCid( $this->getId(), $arrintComplianceItemIds, $this->m_intCid, $objUtilityDatabase );

			if( true == valArr( $arrintComplianceDocIds ) ) {
				$arrintComplianceDocIds = array_keys( rekeyArray( 'id', $arrintComplianceDocIds ) );
				$arrmixComplainceMergeFieldValues = ( array ) \Psi\Eos\Utilities\CComplianceDocs::createService()->fetchComplianceDocsDetailsByApPayeeId( $this->getId(), $arrintComplianceDocIds, $objUtilityDatabase );
			}
			$objUtilityDatabase->close();
		}

		return $arrmixComplainceMergeFieldValues;
	}

	public function loadUtilitiesDatabase() {
		$objUtilitiesDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::UTILITIES, CDatabaseUserType::PS_PROPERTYMANAGER, CDatabaseServerType::POSTGRES, false );
		return $objUtilitiesDatabase;
	}

	public function bindDetails( $arrmixDetails ) {
		$this->setDetails( ( object ) $arrmixDetails );
	}

}
?>