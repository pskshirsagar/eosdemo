<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSetAsides
 * Do not add any new functions to this class.
 */

class CSetAsides extends CBaseSetAsides {

	public static function fetchSimpleSetAsidesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						pb.building_name,
						sa.*
					FROM
						set_asides sa
						JOIN property_buildings pb ON ( pb.cid = sa.cid AND pb.property_id = sa.property_id AND pb.id = sa.property_building_id )
					WHERE
						sa.cid = ' . ( int ) $intCid . '
						AND sa.property_id = ' . ( int ) $intPropertyId . '
						AND sa.deleted_on IS NULL
						AND pb.deleted_on IS NULL
					ORDER BY
						natural_sort( pb.building_name )';

		return self::fetchSetAsides( $strSql, $objDatabase );
	}

	public static function fetchSetAsidesContractsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $strEffectiveDate = 'NOW()', array $arrintSetAsideIds = NULL, $intPropertyBuildingId = NULL ) {
		$strSetAsideIdsCondition = '';
		if( true == valArr( $arrintSetAsideIds ) ) {
			$strSetAsideIdsCondition = ' AND sa.id IN ( ' . sqlIntImplode( $arrintSetAsideIds ) . ' )';
		}
		$strPropertyBuilidingIdCondition = ( true == is_null( $intPropertyBuildingId ) ) ? 'ELSE sa.property_building_id IS NOT NULL' : ' ELSE sa.property_building_id = ' . ( int ) $intPropertyBuildingId . ' ';

		$strSql = '/** @lang PostgreSQL */
					SELECT
						sa.*,
						pst.subsidy_type_id AS property_subsidy_type_id
					FROM
						set_asides sa
						JOIN property_subsidy_types pst ON ( pst.cid = sa.cid AND pst.property_id = sa.property_id AND pst.deleted_on IS NULL AND pst.subsidy_type_id = ' . CSubsidyType::TAX_CREDIT . ' )
						JOIN property_subsidy_details psd ON ( psd.cid = sa.cid AND psd.property_id = sa.property_id )
					WHERE
						sa.cid = ' . ( int ) $intCid . '
						' . $strSetAsideIdsCondition . '
						AND sa.property_id  = ' . ( int ) $intPropertyId . '
						AND sa.placed_in_service_date <=  \'' . $strEffectiveDate . '\'::date
						AND sa.deleted_by IS NULL
						AND sa.deleted_on IS NULL
						AND CASE
							WHEN psd.is_tax_credit_multiple_building_project = FALSE THEN sa.property_building_id IS NULL
							' . $strPropertyBuilidingIdCondition . '
						END
					ORDER BY
						sa.id ASC';

		return self::fetchSetAsides( $strSql, $objDatabase );
	}

	public static function fetchPropertySetAsideByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM set_asides WHERE property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid . ' AND property_building_id IS NULL AND deleted_on IS NULL';

		return parent::fetchSetAside( $strSql, $objDatabase );
	}

	public static function fetchSetAsidesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM set_asides WHERE property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND cid = ' . ( int ) $intCid . ' AND property_building_id IS NOT NULL AND deleted_on IS NULL';

		return parent::fetchSetAsides( $strSql, $objDatabase );
	}

	public static function fetchSetAsideWithBuildingNameByIdByCid( $intSetAsideId, $intCid, $objDatabase ) {

		$strSql = '
			SELECT
				pb.building_name,
				sa.*
			FROM
				set_asides sa
				JOIN property_buildings pb ON ( pb.cid = sa.cid AND pb.property_id = sa.property_id AND pb.id = sa.property_building_id )
			WHERE
				sa.cid = ' . ( int ) $intCid . '
				AND sa.id = ' . ( int ) $intSetAsideId . '
				AND sa.deleted_on IS NULL;';

		return self::fetchSetAside( $strSql, $objDatabase );
	}

	/**
	 * @param $intPropertyId
	 * @param $intCid
	 * @param $objDatabase
	 * @return CSetAside[]
	 */
	public static function fetchActiveSetAsidesByPropertyId( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = '
			SELECT
				psa.id,
				psa.cid,
				psa.property_id,
				--   Treating the property_id as the building id when we have a property-wide set aside, to simplify
				-- logic that is calling this function
				COALESCE( pb.id, p.id ) AS property_building_id,
				psa.subsidy_set_aside_type_id,
				psa.subsidy_income_limit_area_id,
				psa.name,
				psa.building_identification_number,
				psa.placed_in_service_date,
				psa.credit_start_date,
				psa.compliance_start_date,
				psa.extended_use_start_date,
				psa.qualified_units_percent,
				psa.median_income_percent,
				psa.first_year_level_percent,
				psa.deleted_by,
				psa.deleted_on,
				psa.updated_by,
				psa.updated_on,
				psa.created_by,
				psa.created_on,
				COALESCE( pb.building_name, p.property_name ) AS building_name
			FROM
				set_asides psa
				JOIN property_subsidy_details psd ON psd.cid = psa.cid AND psd.property_id = psa.property_id AND psa.property_building_id IS NOT NULL = psd.is_tax_credit_multiple_building_project
				JOIN properties p ON psa.cid = p.cid AND psa.property_id = p.id
				JOIN property_subsidy_types pst ON pst.cid = psa.cid AND psa.property_id = pst.property_id AND pst.subsidy_type_id = ' . CSubsidyType::TAX_CREDIT . ' AND pst.deleted_on IS NULL
				LEFT JOIN property_buildings pb ON pb.cid = psa.cid AND pb.id = psa.property_building_id
			WHERE
				psa.cid = ' . ( int ) $intCid . '
				AND psa.property_id = ' . ( int ) $intPropertyId . '
				AND psa.deleted_on IS NULL
			ORDER BY
				p.property_name,
				natural_sort( pb.building_name )';

		$arrobjSetAsides = self::fetchSetAsides( $strSql, $objDatabase, false );

		return rekeyObjects( 'PropertyBuildingId', $arrobjSetAsides );
	}

	public function fetchSetAsideByApplicationIdByPropertyIdByCid( $intApplicationId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						sa.*
					FROM
						set_asides sa
						JOIN applications a ON ( a.cid = sa.cid AND a.property_id = sa.property_id AND a.id = ' . ( int ) $intApplicationId . ' AND NOT a.is_deleted )
						JOIN property_subsidy_details psd ON ( psd.cid = a.cid AND psd.property_id = a.property_id )
						LEFT JOIN property_units pu ON ( pu.cid = a.cid AND pu.property_id = a.property_id AND pu.id = a.property_unit_id AND pu.deleted_on IS NULL )
					WHERE
						sa.cid = ' . ( int ) $intCid . '
						AND sa.property_id = ' . ( int ) $intPropertyId . '
						AND CASE
								WHEN psd.is_tax_credit_multiple_building_project
								THEN sa.property_building_id IS NOT NULL
								ELSE sa.property_building_id IS NULL
							END
						AND COALESCE( pu.property_building_id, a.property_building_id, 0 ) = COALESCE( sa.property_building_id, 0 )
						AND sa.deleted_on IS NULL
					LIMIT 1';

		return parent::fetchSetAside( $strSql, $objDatabase );
	}

	public static function fetchPropertiesWithActiveSetAsidesByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyGroupIds ) ) {
			return [];
		}

		$strSql = '
			SELECT
				lp.cid,
				lp.property_id
			FROM
				load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[' . implode( ',', $arrintPropertyGroupIds ) . '] ) lp
				JOIN property_subsidy_types pst ON pst.cid = lp.cid AND pst.property_id = lp.property_id AND pst.deleted_on IS NULL AND pst.subsidy_type_id = ' . CSubsidyType::TAX_CREDIT . '
				JOIN set_asides psa ON psa.cid = lp.cid AND psa.property_id = lp.property_id AND psa.deleted_on IS NULL
			GROUP BY
				lp.cid,
				lp.property_id
		';

		return fetchData( $strSql, $objDatabase ) ? : [];
	}

	public static function fetchSetAsideForPropertyBuildingByUnitSpaceIdByPropertyIdByCid( $intPropertyId, $intUnitSpaceId, $intCid, $objDatabase ) {
		if( false == valId( $intUnitSpaceId ) || false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						sa.id, 
						sa.subsidy_set_aside_type_id   
			       FROM unit_spaces us
			            JOIN set_asides sa ON ( ( sa.property_building_id =  us.property_building_id OR sa.property_building_id IS NULL ) AND sa.cid = us.cid )
			       WHERE
			            sa.cid = ' . ( int ) $intCid . '
			            AND us.id = ' . ( int ) $intUnitSpaceId . '
			            AND sa.property_id = ' . ( int ) $intPropertyId . '
			            AND sa.deleted_by IS NULL
			            AND sa.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSubsidySetAsideTypeIdBySetAsideIdByCid( $intSetAsideId, $intCid, $objDatabase ) {
		if( false == valId( $intSetAsideId ) ) {
			return NULL;
		}

		$strSql = '
					SELECT
						sa.subsidy_set_aside_type_id
					FROM
						set_asides sa
					WHERE
						sa.cid = ' . ( int ) $intCid . '
						AND sa.id = ' . ( int ) $intSetAsideId . '
						AND sa.deleted_on IS NULL';

		return self::fetchColumn( $strSql, 'subsidy_set_aside_type_id', $objDatabase );
	}

	public static function fetchMedianIncomePercentageByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $intSetAsideId = NULL ) {
		if( false == valId( $intPropertyId ) ) {
			return NULL;
		}
		$strSetAsideCondition = ( valId( $intSetAsideId ) ) ? ' AND id = ' . ( int ) $intSetAsideId . ' ' : '';

		$strSql = ' SELECT
				       median_income_percent, property_building_id
				    FROM
				        set_asides
				    WHERE
				        property_id = ' . ( int ) $intPropertyId . '
				        ' . $strSetAsideCondition . '
				        AND cid = ' . ( int ) $intCid . '
				        AND deleted_on IS NULL
				        AND deleted_by IS NULL;';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchBuildingIdentificationNumbersByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $intExcludePropertyBuildingId = NULL, $intIncludePropertyBuildingId = NULL, $intSetAsideId = NULL ) {

		$strExcludePropertyBuildingIdCondition = '';
		$strIncludePropertyBuildingIdCondition = '';
		$strSetAsideCondition = '';
		if( true == valId( $intExcludePropertyBuildingId ) ) {
			$strExcludePropertyBuildingIdCondition = ' AND sa.property_building_id <> ' . $intExcludePropertyBuildingId . '';
		}
		if( true == valId( $intIncludePropertyBuildingId ) ) {
			$strIncludePropertyBuildingIdCondition = ' AND sa.property_building_id = ' . $intIncludePropertyBuildingId . '';
		}
		if( true == valId( $intSetAsideId ) ) {
			$strSetAsideCondition = ' AND sa.id <> ' . $intSetAsideId . '';
		}
		$strSql = 'SELECT
						sa.building_identification_number
					FROM
						set_asides sa
						JOIN property_buildings pb ON ( pb.cid = sa.cid AND pb.property_id = sa.property_id AND pb.id = sa.property_building_id )
					WHERE
						sa.cid = ' . ( int ) $intCid . $strIncludePropertyBuildingIdCondition . $strSetAsideCondition . '
						AND sa.property_id = ' . ( int ) $intPropertyId . $strExcludePropertyBuildingIdCondition . '
						AND sa.deleted_on IS NULL
						AND pb.deleted_on IS NULL
					ORDER BY
						natural_sort( pb.building_name )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleSetAsideDataByPropertyBuildingIdByPropertyIdByCid( $intPropertyBuildingId, $intPropertyId, $intCid, $objDatabase, $intExcludeSetAsideId = NULL, $intSetAsideTypeId = NULL ) {

		$strSetAsideCondition = ( true == valId( $intExcludeSetAsideId ) ) ? ' AND sa.id <> ' . ( int ) $intExcludeSetAsideId : '';
		$strSetAsideTypeCondition = ( true == valId( $intSetAsideTypeId ) ) ? ' AND sa.subsidy_set_aside_type_id = ' . ( int ) $intSetAsideTypeId : '';

			$strSql = 'SELECT
						CAST ( sa.median_income_percent AS DOUBLE PRECISION ),
						sa.qualified_units_percent
					FROM
						set_asides sa
						JOIN property_buildings pb ON ( pb.cid = sa.cid AND pb.property_id = sa.property_id AND pb.id = sa.property_building_id )
					WHERE
						sa.cid = ' . ( int ) $intCid . $strSetAsideCondition . '
						AND sa.property_id = ' . ( int ) $intPropertyId . '
						AND sa.property_building_id = ' . ( int ) $intPropertyBuildingId . $strSetAsideTypeCondition . '
						AND sa.deleted_on IS NULL
						AND pb.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSubsidySetAsideTypeIdBySetAsideIdsByCid( $arrintSetAsideIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintSetAsideIds ) ) {
			return NULL;
		}

		$strSql = '
					SELECT
						sa.id,
						sa.subsidy_set_aside_type_id
					FROM
						set_asides sa
					WHERE
						sa.cid = ' . ( int ) $intCid . '
						AND sa.id IN ( ' . sqlIntImplode( $arrintSetAsideIds ) . ' )
						AND sa.deleted_on IS NULL';

		$arrmixSetAsides = fetchData( $strSql, $objDatabase );

		return rekeyArray( 'id', $arrmixSetAsides );

	}

}