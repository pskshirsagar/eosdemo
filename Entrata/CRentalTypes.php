<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRentalTypes
 * Do not add any new functions to this class.
 */

class CRentalTypes extends CBaseRentalTypes {

	public static function fetchRentalTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CRentalType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchRentalType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CRentalType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>