<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CArPaymentTransmissions
 * Do not add any new functions to this class.
 */
class CArPaymentTransmissions extends CBaseArPaymentTransmissions {

	public static function fetchArPaymentTransmissionsByIdByPropertyIdByCid( $intArPaymentTransmissionId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM ar_payment_transmissions WHERE id = ' . ( int ) $intArPaymentTransmissionId . ' AND cid = ' . ( int ) $intCid . ' AND property_id = ' . ( int ) $intPropertyId;
		return self::fetchArPaymentTransmission( $strSql, $objDatabase );
	}

	public static function fetchConfirmedArPaymentTransmissionsCountByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = '	SELECT count(id) FROM (
						SELECT
							apt.id
						FROM
							ar_payment_transmissions apt
							JOIN ar_payments ap ON ( apt.cid = ap.cid AND apt.id = ap.ar_payment_transmission_id )
						WHERE
							ap.payment_status_type_id =' . CPaymentStatusType::BATCHING . '
							AND apt.confirmed_on IS NOT NULL
							AND apt.created_on >= NOW() - INTERVAL \'30 days\'
							AND apt.cid = ' . ( int ) $intCid . '
							AND ap.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						GROUP BY
							apt.id
					) as sub';

		$arrstrData = fetchData( $strSql, $objDatabase );

		return ( true == isset( $arrstrData[0]['count'] ) )? ( int ) $arrstrData[0]['count']: 0;
	}

	// Dashboard Query

	public static function fetchPaginatedConfirmedArPaymentTransmissionsByDashboardFilterByCid( $objDashboardFilter, $intOffset, $intLimit, $strSortBy, $intCid, $objPaymentDatabase ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSql = 'WITH payments AS (

						SELECT
						    DISTINCT ON ( ap.cid, ap.ar_payment_transmission_id )
						    ap.cid,
						    ap.ar_payment_transmission_id,
						    ap.unprocessed_item_count,
						    ap.property_name,
						    COUNT( ap.property_id ) OVER ( PARTITION BY ap.cid, ap.ar_payment_transmission_id ) as property_count
						FROM
						    (
						    SELECT
						        ap.id,
						        ap.cid,
						        ap.property_id,
						        ap.ar_payment_transmission_id,
						        p.property_name,
						        COUNT( ap.id ) OVER ( PARTITION BY ap.cid, ap.ar_payment_transmission_id ) as unprocessed_item_count
						     FROM
						        ar_payments ap
								JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) AS lp ON ( ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? 'lp.is_disabled = 0 AND' : '' ) . ' lp.property_id = ap.property_id )
						        JOIN properties p ON ( p.id = ap.property_id AND p.cid = ap.cid )
						     WHERE
						        ap.cid = ' . ( int ) $intCid . '
						        AND ap.payment_status_type_id = ' . CPaymentStatusType::BATCHING . '
						        AND ap.ar_payment_transmission_id IS NOT NULL
						    ) as ap
					    GROUP BY
					          ap.cid,
					          ap.ar_payment_transmission_id,
					          ap.unprocessed_item_count,
					          ap.property_name,
					          ap.property_id
						)

					SELECT
						*
					FROM (
						SELECT
							CASE
								WHEN TRIM( dp.financial_check21->>\'urgent_unprocessed_payments_greater_than\' ) != \'\' AND ap.unprocessed_item_count >= TRIM( dp.financial_check21->>\'urgent_unprocessed_payments_greater_than\' )::int THEN 3
								WHEN TRIM( dp.financial_check21->>\'urgent_batch_created_since\' ) != \'\' AND get_date_difference( \'day\', NOW(), apt.created_on ) >= TRIM( dp.financial_check21->>\'urgent_batch_created_since\' )::int THEN 3
								WHEN TRIM( dp.financial_check21->>\'important_batch_created_since\' ) != \'\' AND ap.unprocessed_item_count >= TRIM( dp.financial_check21->>\'important_batch_created_since\' )::int THEN 2
								WHEN TRIM( dp.financial_check21->>\'important_unprocessed_payments_greater_than\' ) != \'\' AND get_date_difference( \'day\', NOW(), apt.created_on ) >= TRIM( dp.financial_check21->>\'important_unprocessed_payments_greater_than\' )::int THEN 2
								ELSE 1
							END as priority,
							apt.id,
							apt.cid,
							apt.company_merchant_account_id,
	    					apt.property_id,
							apt.total_amount,
	    					apt.item_count,
	    					apt.auto_associate_count,
	    					apt.transmission_datetime,
	    					apt.confirmed_on,
							ap.unprocessed_item_count,
	    					ap.property_count,
							apt.created_on,
							CASE WHEN 1 = ap.property_count THEN ap.property_name ELSE \'Multiple\' END as property_name
						FROM
		    				ar_payment_transmissions apt
		    				JOIN payments ap ON ( apt.id = ap.ar_payment_transmission_id AND apt.cid = ap.cid )
	     					LEFT JOIN dashboard_priorities dp ON ( dp.cid = apt.cid )
						WHERE
		    				apt.confirmed_on IS NOT NULL
		    				AND apt.created_on >= ( NOW() - INTERVAL \'30 days\' )
		    				AND apt.cid = ' . ( int ) $intCid . '
		    		) apt
		    		WHERE
		    			cid = ' . ( int ) $intCid . '
		    			' . $strPriorityWhere . '
					ORDER BY
	    				' . $strSortBy . '
		    		OFFSET
		    			' . ( int ) $intOffset . '
		    		LIMIT
		    			' . ( int ) $intLimit;

		return fetchData( $strSql, $objPaymentDatabase );
	}

	// Dashboard Query

	public static function fetchConfirmedArPaymentTransmissionCountByDashboardFilterByCid( $objDashboardFilter, $intCid, $objPaymentDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSql = 'WITH payments AS (
						SELECT
						    DISTINCT ON ( ap.cid, ap.ar_payment_transmission_id )
						    ap.cid,
						    ap.ar_payment_transmission_id,
						    ap.unprocessed_item_count,
						    ap.property_name,
							ap.property_id,
						    COUNT( ap.property_id ) OVER ( PARTITION BY ap.cid, ap.ar_payment_transmission_id ) as property_count
						FROM (
							SELECT
						        ap.id,
						        ap.cid,
						        ap.property_id,
						        ap.ar_payment_transmission_id,
						        p.property_name,
						        COUNT( ap.id ) OVER ( PARTITION BY ap.cid, ap.ar_payment_transmission_id ) as unprocessed_item_count
							FROM
						        ar_payments ap
								JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) AS lp ON ( lp.is_disabled = 0 AND lp.property_id = ap.property_id )
						        JOIN properties p ON ( p.id = ap.property_id AND p.cid = ap.cid )
							WHERE
						        ap.cid = ' . ( int ) $intCid . '
						        AND ap.payment_status_type_id = ' . CPaymentStatusType::BATCHING . '
						        AND ap.ar_payment_transmission_id IS NOT NULL
						) as ap
					    GROUP BY
							ap.cid,
							ap.ar_payment_transmission_id,
							ap.unprocessed_item_count,
							ap.property_name,
							ap.property_id
					)

					SELECT
						*
					FROM (
						SELECT
	 						DISTINCT apt.id,
							apt.cid,
							CASE
								WHEN TRIM( dp.financial_check21->>\'urgent_unprocessed_payments_greater_than\' ) != \'\' AND ap.unprocessed_item_count >= TRIM( dp.financial_check21->>\'urgent_unprocessed_payments_greater_than\' )::int THEN 3
								WHEN TRIM( dp.financial_check21->>\'urgent_batch_created_since\' ) != \'\' AND get_date_difference( \'day\', NOW(), apt.created_on ) >= TRIM( dp.financial_check21->>\'urgent_batch_created_since\' )::int THEN 3
								WHEN TRIM( dp.financial_check21->>\'important_batch_created_since\' ) != \'\' AND ap.unprocessed_item_count >= TRIM( dp.financial_check21->>\'important_batch_created_since\' )::int THEN 2
								WHEN TRIM( dp.financial_check21->>\'important_unprocessed_payments_greater_than\' ) != \'\' AND get_date_difference( \'day\', NOW(), apt.created_on ) >= TRIM( dp.financial_check21->>\'important_unprocessed_payments_greater_than\' )::int THEN 2
								ELSE 1
							END as priority,
							ap.property_name,
							ap.property_id
						FROM
		    				ar_payment_transmissions apt
		    				JOIN payments ap ON ( apt.id = ap.ar_payment_transmission_id AND apt.cid = ap.cid )
	     					LEFT JOIN dashboard_priorities dp ON ( dp.cid = apt.cid )
						WHERE
		    				apt.confirmed_on IS NOT NULL
		    				AND apt.created_on >= ( NOW() - INTERVAL \'30 days\' )
		    				AND apt.cid = ' . ( int ) $intCid . '
		    			GROUP BY
	                        apt.id,
		    				apt.cid,
	                        ap.unprocessed_item_count,
	                        apt.created_on,
	                        dp.financial_check21->>\'urgent_unprocessed_payments_greater_than\',
	                        dp.financial_check21->>\'urgent_batch_created_since\',
	                        dp.financial_check21->>\'important_batch_created_since\',
	                        dp.financial_check21->>\'important_unprocessed_payments_greater_than\',
		    				ap.property_name,
							ap.property_id
		    		) check_21
		    		WHERE
		    			cid = ' . ( int ) $intCid . '
		    			' . $strPriorityWhere . '
	    			ORDER BY
	    				priority DESC ' .
		    		( false == $boolIsGroupByProperties ?
		    			'OFFSET 0
		    			LIMIT 100'
					: '' );

		if( false == $boolIsGroupByProperties ) {

			$strSql = ( ( 0 != $intMaxExecutionTimeOut )? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '' ) . 'SELECT COUNT(1) || \'-\' || COALESCE( MAX( priority ), 1 ) as count FROM ( ' . $strSql . ' ) as subQ';
			return parent::fetchColumn( $strSql, 'count', $objPaymentDatabase );
		} else {

			$strSql = ' SELECT
							COUNT(1) AS financial_check_21,
							priority,
							property_name,
							property_id
						FROM
							( ' . $strSql . ' ) AS subQ
						GROUP BY
							priority,
							property_name,
							property_id';

			return fetchData( $strSql, $objPaymentDatabase );
		}

	}

	public static function fetchPaginatedConfirmedArPaymentTransmissionsByPropertyIdsByCid( $arrintPropertyIds, $intOffset, $intLimit, $intCid, $objPaymentDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
 						DISTINCT ON ( apt.id )
						apt.id,
						apt.cid,
						apt.company_merchant_account_id,
    					apt.property_id,
						apt.total_amount,
    					apt.item_count,
    					apt.auto_associate_count,
    					apt.transmission_datetime,
    					apt.confirmed_on,
						CASE
        					WHEN (ap.ar_payment_transmission_id = apt.id AND ap.cid = apt.cid AND ap.payment_status_type_id = ' . CPaymentStatusType::BATCHING . ' ) THEN
        						count( ap.id )
        					ELSE
        						0
    					END as unprocessed_item_count,
    					CASE
        					WHEN (ap.ar_payment_transmission_id = apt.id AND ap.cid = apt.cid ) THEN
        						count( DISTINCT ( ap.property_id ) )
        					ELSE
        						0
    					END as property_item_count
				FROM
    				ar_payment_transmissions apt
    				JOIN ar_payments ap ON apt.id = ap.ar_payment_transmission_id AND apt.cid = ap.cid
				WHERE
    				ap.payment_status_type_id = ' . CPaymentStatusType::BATCHING . '
    				AND apt.confirmed_on IS NOT NULL
    				AND apt.created_on >= now ( ) - INTERVAL \'30 days\'
    				AND apt.cid = ' . ( int ) $intCid . '
    				AND ap.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
				GROUP BY
      				apt.id,
      				apt.cid,
    				apt.company_merchant_account_id,
    				apt.property_id,
    				apt.total_amount,
    				apt.item_count,
    				apt.auto_associate_count,
    				apt.transmission_datetime,
    				apt.confirmed_on,
      				ap.ar_payment_transmission_id,
      				ap.payment_status_type_id,
      				ap.cid
				ORDER BY
    				apt.id';

		if( false == is_null( $intOffset ) && false == is_null( $intLimit ) ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		return self::fetchArPaymentTransmissions( $strSql, $objPaymentDatabase );
	}

	public static function fetchUnprocessedConfirmedArPaymentTransmissionsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
// Changing previouse Query to match the Entrata Query written in above Function 'fetchPaginatedConfirmedArPaymentTransmissionsByPropertyIdsByCid;
		$strSql = ' SELECT
						DISTINCT ON ( apt.id )
						apt.id,
						apt.cid,
						apt.company_merchant_account_id,
						apt.property_id,
						apt.total_amount,
						apt.item_count,
						apt.auto_associate_count,
						apt.transmission_datetime,
						apt.confirmed_on,
						CASE
							WHEN ( ap.ar_payment_transmission_id = apt.id AND ap.cid = apt.cid AND ap.payment_status_type_id = ' . CPaymentStatusType::BATCHING . ' ) THEN
								count ( ap.id )
							ELSE
								0
						END AS unprocessed_item_count,
						CASE
							WHEN ( ap.ar_payment_transmission_id = apt.id AND ap.cid = apt.cid AND ap.payment_status_type_id = ' . CPaymentStatusType::BATCHING . ' ) THEN
								sum ( payment_amount )
							ELSE
								0
						END AS unprocessed_amount,
						CASE
							WHEN ( ap.ar_payment_transmission_id = apt.id AND ap.cid = apt.cid AND ap.payment_status_type_id = ' . CPaymentStatusType::BATCHING . ' ) THEN
								count( DISTINCT ( ap.property_id ) )
							ELSE
								0
						END as property_item_count
					FROM
						ar_payment_transmissions apt
						JOIN ar_payments ap ON apt.id = ap.ar_payment_transmission_id
						AND apt.cid = ap.cid
					WHERE
						ap.payment_status_type_id =' . CPaymentStatusType::BATCHING . '
						AND apt.confirmed_on IS NOT NULL
						AND apt.created_on >= NOW() - INTERVAL \'30 days\'
						AND apt.cid = ' . ( int ) $intCid . '
						AND ap.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					GROUP BY
						apt.id,
						apt.cid,
						apt.company_merchant_account_id,
						apt.property_id,
						apt.total_amount,
						apt.item_count,
						apt.auto_associate_count,
						apt.transmission_datetime,
						ap.ar_payment_transmission_id,
						ap.payment_status_type_id,
						ap.cid,
						apt.confirmed_on
					ORDER BY apt.id';

		return self::fetchArPaymentTransmissions( $strSql, $objDatabase );
	}

	public static function fetchOpenArPaymentTransmissionsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( apt.transmission_datetime, apt.id )
						apt.*,
					    ( 	SELECT
					        	count(id)
					    	FROM
					        	ar_payments ap
							WHERE
					        	ap.ar_payment_transmission_id = apt.id
								AND ap.cid = apt.cid
								AND ap.cid = ' . ( int ) $intCid . '
								AND ap.payment_status_type_id = ' . CPaymentStatusType::BATCHING . '

						) as unprocessed_item_count
					FROM
						ar_payment_transmissions apt
					    INNER JOIN properties cp ON apt.property_id = cp.id
						AND apt.cid = cp.cid
					    LEFT JOIN company_users cu ON ( cp.cid = cu.cid AND cu.cid = ' . ( int ) $intCid . ' AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
					    LEFT JOIN view_company_user_properties cup ON cp.id = cup.property_id AND cp.cid = cup.cid AND cu.id = cup.company_user_id AND ( cu.cid = cup.cid )
					WHERE
					   	apt.confirmed_on IS NULL
						AND apt.cid = ' . ( int ) $intCid . '
					   	AND apt.total_amount > 0
                        AND apt.item_count > 0
                        AND (
                        	SELECT
					        	count(id)
					    	FROM
					        	ar_payments ap
							WHERE
					        	ap.ar_payment_transmission_id = apt.id
								AND ap.cid = apt.cid
								AND ap.cid = ' . ( int ) $intCid . '
								AND ap.payment_status_type_id = ' . CPaymentStatusType::BATCHING . '

						) BETWEEN 1 and apt.item_count  -- we require an unprocessed count between 1 and the number of items specified
					    AND (
					    	cup.company_user_id = ' . ( int ) $intCompanyUserId . '
					    	OR ( cu.id = ' . ( int ) $intCompanyUserId . '  AND cu.cid = ' . ( int ) $intCid . ' AND cu.is_administrator = 1 ) )
					ORDER BY
						apt.transmission_datetime desc,
					    apt.id
					LIMIT 10;';

		return self::fetchArPaymentTransmissions( $strSql, $objDatabase );
	}

	public static function fetchArPaymentTransmissionsByIdByCompanyMerchantAccountIdByCid( $intArPaymentTransmissionId, $intCompanyMerchantAccountId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM ar_payment_transmissions WHERE id = ' . ( int ) $intArPaymentTransmissionId . ' AND cid = ' . ( int ) $intCid . ' AND company_merchant_account_id = ' . ( int ) $intCompanyMerchantAccountId;
		return self::fetchArPaymentTransmission( $strSql, $objDatabase );
	}

	public static function fetchCustomConfirmedArPaymentTransmissionCountByDashboardFilterByCid( $objDashboardFilter, $intCid, $objDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strSql = 'WITH payments AS (
						SELECT
								ap.cid,
								ap.ar_payment_transmission_id
							FROM
								ar_payments ap
								JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) AS lp ON ( lp.property_id = ap.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
								JOIN properties p ON ( p.id = ap.property_id AND p.cid = ap.cid )
							WHERE
								ap.cid = ' . ( int ) $intCid . '
								AND ap.payment_status_type_id = ' . CPaymentStatusType::BATCHING . '
								AND ap.ar_payment_transmission_id IS NOT NULL
						) 
						SELECT
	 						count ( DISTINCT apt.id ) 
						FROM
		    				ar_payment_transmissions apt
		    				JOIN payments ap ON ( apt.id = ap.ar_payment_transmission_id AND apt.cid = ap.cid )
						WHERE
		    				apt.confirmed_on IS NOT NULL
		    				AND apt.created_on >= ( NOW() - INTERVAL \'30 days\' )
		    				AND apt.cid = ' . ( int ) $intCid;

		if( false == $boolIsGroupByProperties ) {
			$strSql = ( ( 0 != $intMaxExecutionTimeOut )? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '' ) . $strSql;
			return parent::fetchColumn( $strSql, 'count', $objDatabase );
		}
	}

}
?>