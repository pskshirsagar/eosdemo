<?php

class CArRuleConditionType extends CBaseArRuleConditionType {

	const OCCUPANCY_TYPE_IS = 1;
	const MOVE_OUT_TYPE_IS = 2;
	const MOVE_OUT_REASON_IS = 3;
	const CANCELATION_REASON_IS = 4;
	const TERMINATION_IS_DURING = 5;
	const MOVE_OUT_DATE_IS_BEFORE = 6;
	const MOVE_OUT_DATE_IS_AFTER = 7;
	const MOVE_OUT_DATE_IS_BETWEEN = 8;
	const MOVE_OUT_DATE_IS_MORE_THAN_DAYS_BEFORE_LEASE_END_DATE = 9;
	const CANCELATION_IS_DAYS_BEFORE_MOVE_IN_DATE = 10;
	const CANCELATION_IS_DAYS_AFTER_LEASE_SIGN_DATE = 11;
	const NOTICE_DATE_IS_LESS_THAN_BEFORE_MOVE_OUT_DATE = 12;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystemTranslated() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>