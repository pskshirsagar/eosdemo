<?php

class COccupancyType extends CBaseOccupancyType {

	const CONVENTIONAL	= 1;
	const COMMERCIAL	= 2;
	const OTHER_INCOME	= 4;
	const PROPERTY		= 5;
	const AFFORDABLE	= 6;
	const OWNER 		= 7;
	const LENDER 		= 8;
	const MILITARY 		= 9;
	const STUDENT 		= 10;
	const HOSPITALITY   = 12;

	// Used to exclude occupancy type PROPERTY and in future can add more types which will be needed to exclude
	public static $c_arrintExcludedOccupancyTypes = [ self::PROPERTY ];

	public static $c_arrintExcludedQuickSearchOccupancyTypes = [ self::OWNER, self::LENDER ];

	public static $c_arrintIncludedOccupancyTypesInReports = [ COccupancyType::CONVENTIONAL, COccupancyType::COMMERCIAL, COccupancyType::OTHER_INCOME, COccupancyType::AFFORDABLE, COccupancyType::MILITARY, COccupancyType::STUDENT ];

	public static $c_arrintIncludedOccupancyTypes = [ self::CONVENTIONAL, self::COMMERCIAL, self::OTHER_INCOME, self::AFFORDABLE, self::MILITARY, self::STUDENT, self::HOSPITALITY ];

	public static $c_arrintTenantOccupancyTypes = [ self::COMMERCIAL ];

	public static $c_arrintOccupancyTypes = [ self::CONVENTIONAL, self::OTHER_INCOME, self::STUDENT, self::MILITARY, self::HOSPITALITY ];

	public static $c_arrintApplicationStatusOverviewOccupancyTypes = [ self::CONVENTIONAL, self::STUDENT ];

	public static $c_arrintCorporateOccupancyTypes = [ self::COMMERCIAL, self::PROPERTY, self::OTHER_INCOME, self::OWNER, self::LENDER ];

	public static $c_arrintExcludedOccupancyTypesForQuotes = [ self::AFFORDABLE, self::MILITARY, self::STUDENT ];

	public static $c_arrstrIlsPortalOccupancyTypes = [ self::CONVENTIONAL => 'conventional', self::STUDENT => 'student', self::AFFORDABLE => 'affordable' ];

	public static $c_arrintMTMLeaseTermOccupancyTypes = [ self::CONVENTIONAL, self::MILITARY ];

	public static $c_arrintFlexibleOccupancyTypeAccessClient = [ 235, 15731, 15705, 16227, 16350, 16480, 16534, 16506, 16643, 16587, 16649, 14566, 16510, 16641, 16616, 15334, 16138, 15149, 15193, 15142 ];

	public static $c_arrintAllLeaseTermOccupancyTypes = [ self::CONVENTIONAL, self::COMMERCIAL, self::MILITARY, self::STUDENT, self::HOSPITALITY ];

	// IMPORTANT: used for associating default ledger and every new default ledger must be added here.
	public static $c_arrintPsProductIdByOccupancyTypeId = [
		self::AFFORDABLE    => CPsProduct::ENTRATA_AFFORDABLE,
		self::MILITARY      => CPsProduct::ENTRATA_MILITARY
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getOccupancyTypeNameById( $intOccupancyTypeId ) {
		$strOccupancyTypeName = NULL;

		switch( $intOccupancyTypeId ) {
			case self::COMMERCIAL:
				$strOccupancyTypeName = __( 'Commercial' );
				break;

			case self::PROPERTY:
				$strOccupancyTypeName = __( 'Property' );
				break;

			case self::OTHER_INCOME:
				$strOccupancyTypeName = __( 'Other Income' );
				break;

			case self::OWNER:
				$strOccupancyTypeName = __( 'Owner' );
				break;

			case self::LENDER:
				$strOccupancyTypeName = __( 'Lender' );
				break;

			case self::MILITARY:
				$strOccupancyTypeName = __( 'Military' );
				break;

			case self::STUDENT:
				$strOccupancyTypeName = __( 'Student' );
				break;

			case self::CONVENTIONAL:
				$strOccupancyTypeName = __( 'Conventional' );
				break;

			case self::AFFORDABLE:
				$strOccupancyTypeName = __( 'Affordable' );
				break;

			case self::HOSPITALITY:
				$strOccupancyTypeName = __( 'Flexible' );
				break;

			default:
				// This is default
				break;
		}

		return $strOccupancyTypeName;
	}

	public static function getOccupancyTypeNamesByIds( $arrintOccupancyTypeIds, $boolReturnText = false ) {
		$arrstrTempName = [];
		if( false == valArr( $arrintOccupancyTypeIds ) )
			return '';
		foreach( $arrintOccupancyTypeIds as $intOccupancyId ) {
			$arrstrTempName[$intOccupancyId] = self::getOccupancyTypeNameById( $intOccupancyId );
		}
		return $boolReturnText ? implode( ', ', $arrstrTempName ) : $arrstrTempName;
	}

	public static function getCustomerLabel( $intOccupancyTypeId ) {

		switch( $intOccupancyTypeId ) {
			case self::COMMERCIAL:
				$strCustomerLabel = __( 'Tenant' );
				break;

			case self::CONVENTIONAL:
			default:
				$strCustomerLabel = __( 'Resident' );
				break;
		}

		return $strCustomerLabel;
	}

	public static function getApplicationOccupancyTypes() {
		$arrstrApplicationOccupancyTypes = [
			COccupancyType::AFFORDABLE   => __( 'Add Member' ),
			COccupancyType::CONVENTIONAL => __( 'Add Applicant' ),
			COccupancyType::STUDENT      => __( 'Add Applicant' ),
			COccupancyType::MILITARY     => __( 'Add Applicant' ),
			NULL                         => __( 'Add Applicant' )
		];

		return $arrstrApplicationOccupancyTypes;
	}

	public static function getViolationOccupancyTypes() {
		return [
			SELF::CONVENTIONAL => [ 'id' => '1', 'name' => __( 'Conventional' ) ],
			SELF::COMMERCIAL   => [ 'id' => '2', 'name' => __( 'Commercial' ) ],
			SELF::AFFORDABLE   => [ 'id' => '6', 'name' => __( 'Affordable' ) ],
			SELF::MILITARY     => [ 'id' => '9', 'name' => __( 'Military' ) ],
			SELF::STUDENT      => [ 'id' => '10', 'name' => __( 'Student' ) ]
		];
	}

	public static function getOccupancyTypeCount( $objProperty, $objWebsiteProperty ) {

		if( true == valObj( $objWebsiteProperty, 'CWebsiteProperty' ) ) {
			$arrintWebsiteJourneys = $objWebsiteProperty->getWebsiteJourneys();
		}
		if( true == valObj( $objProperty, 'CProperty' ) ) {
			$arrintPropertyOccupancyTypeIds = $objProperty->getOccupancyTypeIds();
		}

		if( true == valArr( $arrintWebsiteJourneys ) && true == valArr( $arrintPropertyOccupancyTypeIds ) ) {
			$arrintOccupancyTypeIds = array_intersect( $arrintPropertyOccupancyTypeIds, $arrintWebsiteJourneys );
		} elseif( true == valObj( $objProperty, 'CProperty' ) && false == is_null( $objProperty->getOccupancyTypeIds() ) ) {
			$arrintOccupancyTypeIds = $objProperty->getOccupancyTypeIds();
		} else {
			$arrintOccupancyTypeIds = $objProperty->getDefaultOccupancyTypeId();
		}

		return \Psi\Libraries\UtilFunctions\count( $arrintOccupancyTypeIds );
	}

	public static function getOccupancyTypeIdByOccupancyName( $strOccupancyType ) {

		switch( $strOccupancyType ) {
			case 'conventional':
				return \COccupancyType::CONVENTIONAL;
				break;

			case 'student':
				return \COccupancyType::STUDENT;
				break;

			case 'military':
				return \COccupancyType::MILITARY;
				break;

			case 'short-stay':
				return \CWebsiteJourneyType::FLEXIBLE_LEASING;
				break;

			default:
				// Do Nothing
				break;
		}

	}

}
?>