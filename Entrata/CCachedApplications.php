<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCachedApplications
 * Do not add any new functions to this class.
 */
class CCachedApplications extends CBaseCachedApplications {

	public static function fetchPaginatedApplicationsByPropertyIdsByPsProductIdsByApplicationsFilterByCid( $arrintPropertyIds, $arrintPsProductIds, $objApplicationsFilter, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strCheckDeletedUnitsSql 	    = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql   = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesAsSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND deleted_on IS NULL' : '';

		$arrstrWhereParameters = [];

		// -1 means outside the core query, 1 means inside the core query, and 0 means no join at all
		$intJoinLeases = ( true == in_array( CLeaseIntervalType::RENEWAL, ( array ) $objApplicationsFilter->getLeaseIntervalTypeIds() ) ) ? - 1 : 0;
		$intJoinLeases = ( -1 == $intJoinLeases && 'lease_end_date' == $objApplicationsFilter->getSortBy() ) ? 1 : - 1;

		$intJoinPropertyUnits 		= -1;
		$intJoinPropertyFloorplans 	= 0;

		$intJoinEvents = ( true == valArr( $arrintPsProductIds ) && true == in_array( CPsProduct::LEAD_MANAGEMENT, $arrintPsProductIds ) ) ? - 1 : 0;

		$intOffset 	= ( 0 < ( int ) $objApplicationsFilter->getPageNumber() ) ? $objApplicationsFilter->getCountPerPage() * ( $objApplicationsFilter->getPageNumber() - 1 ) : 0;
		$intLimit 	= ( 0 < $objApplicationsFilter->getCountPerPage() ) ? $objApplicationsFilter->getCountPerPage() : 25;
		$strQuickSearch = trim( $objApplicationsFilter->getQuickSearch() );
		if( false == empty( $strQuickSearch ) ) {
			$arrstrQuickSearchWhereSqls = [];

			$arrstrQuickSearchItems	= explode( ' ', trim( $objApplicationsFilter->getQuickSearch() ) );
			foreach( $arrstrQuickSearchItems as $strQuickSearchItem ) {
				if( true == valStr( $strQuickSearchItem ) ) {
					$arrstrQuickSearchWhereSqls[] = ' ( lower( appt.name_last ) LIKE lower( \'%' . addslashes( $strQuickSearchItem ) . '%\' ) OR lower( appt.name_first ) LIKE lower( \'%' . addslashes( $strQuickSearchItem ) . '%\' ) OR CAST( ca.id AS text ) LIKE \'%' . addslashes( $strQuickSearchItem ) . '%\' OR lower( appt.phone_number ) LIKE lower( \'%' . addslashes( $strQuickSearchItem ) . '%\' ) OR lower( appt.username ) LIKE lower( \'%' . addslashes( $strQuickSearchItem ) . '%\' ) ) ';
				}
			}

			if( true == valArr( $arrstrQuickSearchWhereSqls ) ) {
				$arrstrWhereParameters[] = ' ( ' . implode( 'OR', $arrstrQuickSearchWhereSqls ) . ' ) ';
			}
		}

		if( true == valArr( $objApplicationsFilter->getApplicationIds() ) ) 			$arrstrWhereParameters[] = 'ca.id IN ( ' . implode( ',', $objApplicationsFilter->getApplicationIds() ) . ' ) ';
		if( true == valArr( $objApplicationsFilter->getLeadSourceIds() ) ) 				$arrstrWhereParameters[] = 'ca.lead_source_id IN ( ' . implode( ',', $objApplicationsFilter->getLeadSourceIds() ) . ' ) ';
		if( true == valArr( $objApplicationsFilter->getPsProductIds() ) ) 				$arrstrWhereParameters[] = 'ca.ps_product_id IN ( ' . implode( ',', $objApplicationsFilter->getPsProductIds() ) . ' ) ';
		if( true == valArr( $objApplicationsFilter->getCompanyEmployeeIds() ) ) 		$arrstrWhereParameters[] = 'ca.leasing_agent_id IN ( ' . implode( ',', $objApplicationsFilter->getCompanyEmployeeIds() ) . ' ) ';
		if( true == valArr( $objApplicationsFilter->getPropertyFloorplanIds() ) ) 		$arrstrWhereParameters[] = 'ca.property_floorplan_id IN ( ' . implode( ',', $objApplicationsFilter->getPropertyFloorplanIds() ) . ' ) ';
		if( true == valArr( $objApplicationsFilter->getLeaseIntervalTypeIds() ) ) 		$arrstrWhereParameters[] = 'ca.lease_interval_type_id IN ( ' . implode( ',', $objApplicationsFilter->getLeaseIntervalTypeIds() ) . ' ) ';
		if( true == valArr( $objApplicationsFilter->getCompanyapplicationIds() ) ) 		$arrstrWhereParameters[] = 'ca.company_application_id IN( ' . implode( ',', $objApplicationsFilter->getCompanyapplicationIds() ) . ' )';

		if( 0 < ( int ) $objApplicationsFilter->getDesiredRentMin() ) 					$arrstrWhereParameters[] = 'ca.desired_rent_min >= ' . ( int ) $objApplicationsFilter->getDesiredRentMin() . ' ';
		if( 0 < ( int ) $objApplicationsFilter->getDesiredRentMax() ) 					$arrstrWhereParameters[] = 'ca.desired_rent_max <= ' . ( int ) $objApplicationsFilter->getDesiredRentMax() . ' ';

		if( true == valArr( $objApplicationsFilter->getPropertyIds() ) ) {
			$arrstrWhereParameters[] = 'ca.property_id IN ( ' . implode( ',', $objApplicationsFilter->getPropertyIds() ) . ' ) ';
		} else {
			$arrstrWhereParameters[] = 'ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}

		if( 'Min' != $objApplicationsFilter->getApplicationStartDate() && true == CValidation::validateDate( $objApplicationsFilter->getApplicationStartDate() ) ) 	$arrstrWhereParameters[] = 'DATE_TRUNC ( \'day\', ca.application_datetime ) >= \'' . $objApplicationsFilter->getApplicationStartDate() . '\' ';
		if( 'Max' != $objApplicationsFilter->getApplicationEndDate() && true == CValidation::validateDate( $objApplicationsFilter->getApplicationEndDate() ) ) 		$arrstrWhereParameters[] = 'DATE_TRUNC ( \'day\', ca.application_datetime ) <= \'' . $objApplicationsFilter->getApplicationEndDate() . '\' ';

		if( 'Min' != $objApplicationsFilter->getMoveInDateMin() && true == CValidation::validateDate( $objApplicationsFilter->getMoveInDateMin() ) )	$arrstrWhereParameters[] = 'DATE_TRUNC ( \'day\', ca.lease_start_date ) >= \'' . $objApplicationsFilter->getMoveInDateMin() . '\' ';
		if( 'Max' != $objApplicationsFilter->getMoveInDateMax() && true == CValidation::validateDate( $objApplicationsFilter->getMoveInDateMax() ) ) 	$arrstrWhereParameters[] = 'DATE_TRUNC ( \'day\', ca.lease_start_date ) <= \'' . $objApplicationsFilter->getMoveInDateMax() . '\' ';

		if( true == valArr( $objApplicationsFilter->getApplicationStageStatusIds() ) ) {
			$arrstrWhereParameters[] = 'ass.id IN ( ' . implode( ',', $objApplicationsFilter->getApplicationStageStatusIds() ) . ' ) ';
		} else {
			$arrstrWhereParameters[] = '(application_stage_id,application_status_id) NOT IN ( ' . sqlIntMultiImplode( CApplicationStageStatus::$c_arrintNonActiveApplicationStatusIds ) . ' ) ';
		}

		$strCompanyEmployees = trim( $objApplicationsFilter->getCompanyEmployees() );
		if( true == valArr( $objApplicationsFilter->getCompanyEmployeeIds() ) && true == $objApplicationsFilter->getIncludeLeadsWithNullLeasingAgent() ) {
			$arrstrWhereParameters[] = ' ( ca.leasing_agent_id IN ( ' . $objApplicationsFilter->getCompanyEmployees() . ' ) OR ca.leasing_agent_id IS NULL ) ';
		} elseif( false == empty( $strCompanyEmployees ) ) {
			$arrstrWhereParameters[] = ' ca.leasing_agent_id IN ( ' . $objApplicationsFilter->getCompanyEmployees() . ' ) ';
		} elseif( true == $objApplicationsFilter->getIncludeLeadsWithNullLeasingAgent() ) {
			$arrstrWhereParameters[] = ' ca.leasing_agent_id IS NULL ';
		}

		// We may require this code in future when we enhance the search leads.
		if( false == is_null( $objApplicationsFilter->getLastContactMinDays() ) && '' != trim( $objApplicationsFilter->getLastContactMinDays() ) && '0' != trim( $objApplicationsFilter->getLastContactMinDays() ) && -1 == $intJoinEvents ) {
			$arrstrWhereParameters[] = 'date_part( \'day\', ( current_timestamp  - e.event_datetime )) >= ' . ( int ) $objApplicationsFilter->getLastContactMinDays() . ' ';
			$intJoinEvents = 1;
		}

		if( false == is_null( $objApplicationsFilter->getLastContactMaxDays() ) && '' != trim( $objApplicationsFilter->getLastContactMaxDays() ) && '0' != trim( $objApplicationsFilter->getLastContactMaxDays() ) && -1 == $intJoinEvents ) {
			$arrstrWhereParameters[] = 'date_part( \'day\', ( current_timestamp  - e.event_datetime )) <= ' . ( int ) $objApplicationsFilter->getLastContactMaxDays() . ' ';
			$intJoinEvents = 1;
		}

		if( false == is_null( $objApplicationsFilter->getDesiredBedrooms() ) && '' != trim( $objApplicationsFilter->getDesiredBedrooms() ) ) {
			$intJoinPropertyFloorplans 	= 1;

			$arrstrBedroomsWhereSqls 	= [];
			$arrintDesiredBedrooms 		= explode( ',', $objApplicationsFilter->getDesiredBedrooms() );

			if( true == valArr( $arrintDesiredBedrooms ) ) 	$arrstrBedroomsWhereSqls[] = ' ca.desired_bedrooms IN ( ' . implode( ',', array_keys( $arrintDesiredBedrooms ) ) . ' ) ';
			if( 5 < \Psi\Libraries\UtilFunctions\count( $arrintDesiredBedrooms ) ) 		$arrstrBedroomsWhereSqls[] = ' ca.desired_bedrooms > 5 ';

			if( true == valArr( $arrstrBedroomsWhereSqls ) ) $arrstrWhereParameters[] = ' ( ' . implode( ' OR ', $arrstrBedroomsWhereSqls ) . ' ) ';
		}
		$strDesiredBathrooms = trim( $objApplicationsFilter->getDesiredBathrooms() );
		if( false == empty( $strDesiredBathrooms ) ) {
			$intJoinPropertyFloorplans 	= 1;
			$arrintDesiredBathrooms		= explode( ',', $objApplicationsFilter->getDesiredBathrooms() );

			$arrstrBathroomsWhereSqls = [];
			if( true == valArr( $arrintDesiredBathrooms ) ) 	$arrstrBathroomsWhereSqls[] = ' ca.desired_bathrooms IN ( ' . implode( ',', array_keys( $arrintDesiredBathrooms ) ) . ' ) ';
			if( 5 < \Psi\Libraries\UtilFunctions\count( $arrintDesiredBathrooms ) ) 			$arrstrBathroomsWhereSqls[] = ' ca.desired_bathrooms > 5 ';

			if( true == valArr( $arrstrBathroomsWhereSqls ) ) 	$arrstrWhereParameters[] = ' ( ' . implode( ' OR ', $arrstrBathroomsWhereSqls ) . ' ) ';
		}
		$strDesiredRentMin = trim( $objApplicationsFilter->getDesiredRentMin() );
		$strDesiredRentMax = trim( $objApplicationsFilter->getDesiredRentMax() );

		$strDesiredOccupants = trim( $objApplicationsFilter->getDesiredOccupants() );
		if( false == empty( $strDesiredOccupants ) ) {
			$intJoinPropertyUnits = 1;
			$arrstrWhereParameters[] = 'pu.max_occupants = ' . ( int ) $objApplicationsFilter->getDesiredOccupants() . ' ';
		}
		$intUnitNumbers = trim( $objApplicationsFilter->getUnitNumbers() );
		if( false == empty( $intUnitNumbers ) ) {
			$intJoinPropertyUnits = 1;
			$arrstrWhereParameters[] = 'lower( pu.unit_number ) LIKE lower( \'%' . addslashes( $objApplicationsFilter->getUnitNumbers() ) . '%\' ) ';
		}
		$strLeaseIntervalTypes = trim( $objApplicationsFilter->getLeaseIntervalTypes() );
		if( false == empty( $strLeaseIntervalTypes ) ) {
			$arrstrWhereParameters[] = 'ca.lease_interval_type_id IN ( ' . $objApplicationsFilter->getLeaseIntervalTypes() . ' ) ';
		} elseif( true == valArr( $objApplicationsFilter->getLeaseIntervalTypeIds() ) ) {
			$arrstrWhereParameters[] = 'ca.lease_interval_type_id IN ( ' . implode( ',', $objApplicationsFilter->getLeaseIntervalTypeIds() ) . ' ) ';
		}

		if( true == is_numeric( $objApplicationsFilter->getIsIntegrated() ) ) {
			if( CApplicationFilter::INTEGRATED == $objApplicationsFilter->getIsIntegrated() ) {

				$arrstrWhereParameters[] = '( ( ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' AND ca.app_remote_primary_key IS NOT NULL ) ) ';

			} elseif( CApplicationFilter::NON_INTEGRATED == $objApplicationsFilter->getIsIntegrated() ) {

				$arrstrWhereParameters[] = '( ( ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' AND ca.app_remote_primary_key IS NULL ) ) ';
			}
		}

		$strEventsJoinSql = ' LEFT JOIN events e ON ( e.cid = ' . ( int ) $intCid . ' AND e.id = ca.last_event_id AND e.cid = ca.cid ) ';

		$strEventFields = 'e.id as event_id,
							e.event_datetime,
							e.event_type_id,
							DATE_TRUNC(\'day\',  NOW() - e.event_datetime ) as last_contact_days,';

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						ca.*,
						CASE WHEN us.space_number IS NOT NULL AND ( 1 < ( SELECT COUNT(id) FROM unit_spaces WHERE property_unit_id = ca.property_unit_id AND cid = ca.cid ' . $strCheckDeletedUnitSpacesAsSql . ' ) )
							THEN pu.unit_number || \'-\' || us.space_number
							ELSE pu.unit_number
						END AS unit_number,
						' . ( ( -1 == $intJoinEvents ) ? $strEventFields : '' ) . '
						' . ( ( 1 == $intJoinLeases ) ? 'ca.lease_end_date,' : '' ) . '
						' . ( ( -1 == $intJoinLeases ) ? 'li.lease_end_date,' : '' ) . '
						CASE WHEN TRUE = ca.is_pet_policy_conflicted
							AND ( 0 = ( SELECT
											count(pap.id)
										FROM
											property_application_preferences as pap
										WHERE
											pap.cid = ' . ( int ) $intCid . '
											AND pap.property_id = ca.property_id
											AND pap.company_application_id = ca.company_application_id
											AND pap.cid = ca.cid
											AND pap.key = \'HIDE_OPTION_PET\' LIMIT 1 ) ) THEN 1 ELSE 0
						END AS is_pet_policy_conflicted
					FROM
						(
							SELECT
								appt.id AS applicant_id,
								appt.name_last,
								appt.name_first,
								appt.phone_number,
								appt.mobile_number,
								appt.work_number,
								appt.username as email_address,
								ca.id as id,
								ca.cid,
								ca.property_id,
								ca.company_application_id,
								ca.application_datetime,
								ca.property_floorplan_id,
								ca.property_unit_id,
								ca.unit_space_id,
								ca.lead_source_id,
								ca.leasing_agent_id,
								ca.application_stage_id,
								ca.application_status_id,
								ca.last_event_id,
								ca.is_pet_policy_conflicted,
								ca.lease_id,
								ca.lease_interval_type_id,
								ca.ps_product_id,
								ca.screening_id,
								ca.guest_remote_primary_key,
								ca.app_remote_primary_key,
								ca.created_on,
								ca.application_completed_on,
								ca.lease_completed_on,
								ca.application_approved_on,
								aa.application_step_id,
								aa.application_document_id,
								' . ( ( 1 == $intJoinEvents ) ? $strEventFields : '' ) . '
								' . ( ( 1 == $intJoinLeases ) ? 'li.lease_end_date as lease_end_date,' : '' ) . '
								aa.application_form_id,
								aa.lease_generated_on ';

		$strSql .= ' FROM
						cached_applications ca
						JOIN application_stage_statuses ass ON ( ass.application_stage_id = ca.application_stage_id AND ass.application_status_id = ca.application_status_id AND ass.lease_interval_type_id = ca.lease_interval_type_id)
						JOIN applicant_applications aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . $strCheckDeletedAASql . ' )
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid ) ';

		$strSql .= ( 1 == $intJoinLeases ) ? ' LEFT JOIN leases l ON ( ca.lease_id = l.id AND ca.cid = l.cid )
											LEFT JOIN lease_intervals li ON ( li.lease_id = l.id AND li.cid = l.cid AND li.id = l.active_lease_interval_id ) ' : '';

		$strSql .= ( 1 == $intJoinPropertyUnits ) ? ' LEFT JOIN property_units pu ON ( pu.id = ca.property_unit_id AND pu.cid = ca.cid ' . $strCheckDeletedUnitsSql . ' ) ' : '';
		$strSql .= ( 1 == $intJoinEvents ) ? $strEventsJoinSql : '';

		$strSql .= ' WHERE  ca.cid = ' . ( int ) $intCid;

		if( 0 == $intJoinLeases ) {
			$strSql .= ' AND li.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED;
		}

		if( true == valArr( $arrstrWhereParameters ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhereParameters ) . ' ';
		}

		$arrstrOrderBy = [
			'lead_id'					=> 'appt.id',
			'name'						=> 'appt.name_last || appt.name_first',
			'status'					=> 'ca.application_status_id',
			'property'					=> 'ca.property_id',
			'source'					=> 'ca.lead_source_id',
			'application_datetime'		=> 'ca.application_datetime',
			'application_approved_on'	=> 'ca.application_approved_on',
			'application_completed_on'	=> 'ca.application_completed_on',
			'lease_completed_on'		=> 'ca.lease_completed_on',
			'lease_interval_type'		=> 'ca.lease_interval_type_id',
			'lease_end_date'			=> 'li.lease_end_date'
		];

		$strOrderBy 		= ( true == isset( $arrstrOrderBy[$objApplicationsFilter->getSortBy()] ) ? $arrstrOrderBy[$objApplicationsFilter->getSortBy()] : 'ca.application_datetime' );
		$strSortDirection 	= ( false == is_null( $objApplicationsFilter->getSortDirection() ) ) ? $objApplicationsFilter->getSortDirection() : 'DESC';

		$strSql .= ' ORDER BY ' . addslashes( $strOrderBy ) . ' ' . addslashes( $strSortDirection );

		if( false == is_null( $objApplicationsFilter->getPageNumber() ) && false == is_null( $objApplicationsFilter->getCountPerPage() ) ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		$strSql .= ' ) AS ca
					LEFT JOIN unit_spaces us ON ( us.id = ca.unit_space_id AND us.cid = ca.cid ' . $strCheckDeletedUnitSpacesSql . ' )
					LEFT JOIN property_units pu ON ( pu.id = ca.property_unit_id AND pu.cid = ca.cid ' . $strCheckDeletedUnitsSql . ' ) ';

		$strSql .= ( -1 == $intJoinLeases ) ? ' LEFT JOIN leases l ON ( ca.lease_id = l.id AND ca.cid = l.cid )
												LEFT JOIN lease_intervals li ON ( li.lease_id = l.id AND li.id = l.active_lease_interval_id AND li.cid = l.cid ) ' : '';

		$strSql .= ( -1 == $intJoinEvents ) ? $strEventsJoinSql : '';

		$strSql .= ' WHERE ca.cid = ' . ( int ) $intCid;

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchTotalApplicationsCountByPropertyIdsByPsProductIdsByApplicationFilterByCid( $arrintPropertyIds, $arrintPsProductIds, $objApplicationFilter, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $boolIncludeDeletedUnits = false, $boolIncludeDeletedFloorPlans = false ) {
		if( false == isset( $arrintPropertyIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) return NULL;

		$strCheckDeletedUnitsSql 	  = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlansSql = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pfp.deleted_on IS NULL' : '';

		$arrstrWhereParameters 			= [];

		$boolJoinApplicantSql 			= false;
		$boolJoinPropertyFloorplanSql	= false;
		$boolJoinPropertyUnitSql		= false;
		$boolIsRenewal					= false;
		$boolJoinEvents					= false;

		if( true == valArr( $objApplicationFilter->getLeaseIntervalTypeIds() ) && true == in_array( CLeaseIntervalType::RENEWAL, $objApplicationFilter->getLeaseIntervalTypeIds() ) ) {
			$boolIsRenewal = true;
		}
		$strQuickSearch = trim( $objApplicationFilter->getQuickSearch() );
		if( false == empty( $strQuickSearch ) ) {
			$arrstrQuickSearchWhereSqls = [];
			$boolJoinApplicantSql 		= true;

			$arrstrQuickSearchItems	= explode( ' ', trim( $objApplicationFilter->getQuickSearch() ) );
			foreach( $arrstrQuickSearchItems as $strQuickSearchItem ) {
				if( true == valStr( $strQuickSearchItem ) ) {
					$arrstrQuickSearchWhereSqls[] = ' ( lower( appt.name_last ) LIKE lower( \'%' . addslashes( $strQuickSearchItem ) . '%\' ) OR lower( appt.name_first ) LIKE lower( \'%' . addslashes( $strQuickSearchItem ) . '%\' ) OR CAST( ca.id AS text ) LIKE \'%' . addslashes( $strQuickSearchItem ) . '%\' OR lower( appt.phone_number ) LIKE lower( \'%' . addslashes( $strQuickSearchItem ) . '%\' ) OR lower( appt.username ) LIKE lower( \'%' . addslashes( $strQuickSearchItem ) . '%\' ) ) ';
				}
			}

			if( true == valArr( $arrstrQuickSearchWhereSqls ) ) {
				$arrstrWhereParameters[] = ' ( ' . implode( 'OR', $arrstrQuickSearchWhereSqls ) . ' ) ';
			}
		}

		if( true == valArr( $objApplicationFilter->getApplicationIds() ) ) 			$arrstrWhereParameters[] = 'ca.id IN ( ' . implode( ',', $objApplicationFilter->getApplicationIds() ) . ' ) ';
		if( true == valArr( $objApplicationFilter->getLeadSourceIds() ) ) 			$arrstrWhereParameters[] = 'ca.lead_source_id IN ( ' . implode( ',', $objApplicationFilter->getLeadSourceIds() ) . ' ) ';
		if( true == valArr( $objApplicationFilter->getPsProductIds() ) ) 			$arrstrWhereParameters[] = 'ca.ps_product_id IN ( ' . implode( ',', $objApplicationFilter->getPsProductIds() ) . ' ) ';
		if( true == valArr( $objApplicationFilter->getCompanyEmployeeIds() ) ) 		$arrstrWhereParameters[] = 'ca.leasing_agent_id IN ( ' . implode( ',', $objApplicationFilter->getCompanyEmployeeIds() ) . ' ) ';
		if( true == valArr( $objApplicationFilter->getPropertyFloorplanIds() ) ) 	$arrstrWhereParameters[] = 'ca.property_floorplan_id IN ( ' . implode( ',', $objApplicationFilter->getPropertyFloorplanIds() ) . ' ) ';
		if( true == valArr( $objApplicationFilter->getLeaseIntervalTypeIds() ) ) 			$arrstrWhereParameters[] = 'ca.lease_interval_type_id IN ( ' . implode( ',', $objApplicationFilter->getLeaseIntervalTypeIds() ) . ' ) ';
		if( true == valArr( $objApplicationFilter->getCompanyApplicationIds() ) ) 	$arrstrWhereParameters[] = 'ca.company_application_id IN( ' . implode( ',', $objApplicationFilter->getCompanyApplicationIds() ) . ' )';

		if( 0 < ( int ) $objApplicationFilter->getDesiredRentMin() ) 					$arrstrWhereParameters[] = 'ca.desired_rent_min >= ' . ( int ) $objApplicationFilter->getDesiredRentMin() . ' ';
		if( 0 < ( int ) $objApplicationFilter->getDesiredRentMax() ) 					$arrstrWhereParameters[] = 'ca.desired_rent_max <= ' . ( int ) $objApplicationFilter->getDesiredRentMax() . ' ';

		if( true == valArr( $objApplicationFilter->getPropertyIds() ) ) {
			$arrstrWhereParameters[] = 'ca.property_id IN ( ' . implode( ',', $objApplicationFilter->getPropertyIds() ) . ' ) ';
		} else {
			$arrstrWhereParameters[] = 'ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}

		if( 'Min' != $objApplicationFilter->getApplicationStartDate() && true == CValidation::validateDate( $objApplicationFilter->getApplicationStartDate() ) ) 	$arrstrWhereParameters[] = 'DATE_TRUNC ( \'day\', ca.application_datetime ) >= \'' . $objApplicationFilter->getApplicationStartDate() . '\' ';
		if( 'Max' != $objApplicationFilter->getApplicationEndDate() && true == CValidation::validateDate( $objApplicationFilter->getApplicationEndDate() ) ) 		$arrstrWhereParameters[] = 'DATE_TRUNC ( \'day\', ca.application_datetime ) <= \'' . $objApplicationFilter->getApplicationEndDate() . '\' ';

		if( 'Min' != $objApplicationFilter->getMoveInDateMin() && true == CValidation::validateDate( $objApplicationFilter->getMoveInDateMin() ) )	$arrstrWhereParameters[] = 'DATE_TRUNC ( \'day\', ca.lease_start_date ) >= \'' . $objApplicationFilter->getMoveInDateMin() . '\' ';
		if( 'Max' != $objApplicationFilter->getMoveInDateMax() && true == CValidation::validateDate( $objApplicationFilter->getMoveInDateMax() ) ) 	$arrstrWhereParameters[] = 'DATE_TRUNC ( \'day\', ca.lease_start_date ) <= \'' . $objApplicationFilter->getMoveInDateMax() . '\' ';

		if( true == valArr( $objApplicationFilter->getApplicationStageStatusIds() ) ) {
			$arrstrWhereParameters[] = 'ass.id IN ( ' . implode( ',', $objApplicationFilter->getApplicationStageStatusIds() ) . ' ) ';
		} else {
			$arrstrWhereParameters[] = '( application_stage_id, application_status_id ) NOT IN ( ' . sqlIntMultiImplode( CApplicationStageStatus::$c_arrintNonActiveApplicationStatusIds ) . ' ) ';
		}

		$strCompanyEmployees = trim( $objApplicationFilter->getCompanyEmployees() );
		if( true == valArr( $objApplicationFilter->getCompanyEmployeeIds() ) && true == $objApplicationFilter->getIncludeLeadsWithNullLeasingAgent() ) {
			$arrstrWhereParameters[] = ' ( ca.leasing_agent_id IN ( ' . $objApplicationFilter->getCompanyEmployees() . ' ) OR ca.leasing_agent_id IS NULL ) ';
		} elseif( false == empty( $strCompanyEmployees ) ) {
			$arrstrWhereParameters[] = ' ca.leasing_agent_id IN ( ' . $objApplicationFilter->getCompanyEmployees() . ' ) ';
		} elseif( true == $objApplicationFilter->getIncludeLeadsWithNullLeasingAgent() ) {
			$arrstrWhereParameters[] = ' ca.leasing_agent_id IS NULL ';
		}

		// We may require this code in future when we enhance the search leads.
		if( true == valArr( $arrintPsProductIds ) && true == in_array( CPsProduct::LEAD_MANAGEMENT, $arrintPsProductIds ) ) {
			if( false == is_null( $objApplicationFilter->getLastContactMinDays() ) && '' != trim( $objApplicationFilter->getLastContactMinDays() ) && '0' != trim( $objApplicationFilter->getLastContactMinDays() ) && -1 == $intJoinEvents ) {
				$boolJoinEvents = true;
				$arrstrWhereParameters[] = 'date_part( \'day\', ( current_timestamp  - e.event_datetime )) >= ' . ( int ) $objApplicationFilter->getLastContactMinDays() . ' ';
			}

			if( false == is_null( $objApplicationFilter->getLastContactMaxDays() ) && '' != trim( $objApplicationFilter->getLastContactMaxDays() ) && '0' != trim( $objApplicationFilter->getLastContactMaxDays() ) && -1 == $intJoinEvents ) {
				$boolJoinEvents = true;
				$arrstrWhereParameters[] = 'date_part( \'day\', ( current_timestamp  - e.event_datetime )) <= ' . ( int ) $objApplicationFilter->getLastContactMaxDays() . ' ';
			}
		}

		$strDesiredBedrooms = trim( $objApplicationFilter->getDesiredBedrooms() );
		if( false == empty( $strDesiredBedrooms ) ) {
			$intJoinPropertyFloorplans 	= 1;

			$arrstrBedroomsWhereSqls 	= [];
			$arrintDesiredBedrooms		= explode( ',', $objApplicationFilter->getDesiredBedrooms() );

			if( true == valArr( $arrintDesiredBedrooms ) ) $arrstrBedroomsWhereSqls[] = ' ca.desired_bedrooms IN ( ' . implode( ',', array_keys( $arrintDesiredBedrooms ) ) . ' ) ';
			if( 5 < \Psi\Libraries\UtilFunctions\count( $arrintDesiredBedrooms ) ) 		$arrstrBedroomsWhereSqls[] = ' ca.desired_bedrooms > 5 ';

			if( true == valArr( $arrstrBedroomsWhereSqls ) ) $arrstrWhereParameters[] = ' ( ' . implode( ' OR ', $arrstrBedroomsWhereSqls ) . ' ) ';
		}

		$strDesiredBathrooms = trim( $objApplicationFilter->getDesiredBathrooms() );
		if( false == empty( $strDesiredBathrooms ) ) {
			$intJoinPropertyFloorplans 	= 1;
			$arrintDesiredBathrooms		= explode( ',', $objApplicationFilter->getDesiredBathrooms() );

			$arrstrBathroomsWhereSqls = [];
			if( true == valArr( $arrintDesiredBathrooms ) ) 	$arrstrBathroomsWhereSqls[] = ' ca.desired_bathrooms IN ( ' . implode( ',', array_keys( $arrintDesiredBathrooms ) ) . ' ) ';
			if( 5 < \Psi\Libraries\UtilFunctions\count( $arrintDesiredBathrooms ) ) 			$arrstrBathroomsWhereSqls[] = ' ca.desired_bathrooms > 5 ';

			if( true == valArr( $arrstrBathroomsWhereSqls ) ) 	$arrstrWhereParameters[] = ' ( ' . implode( ' OR ', $arrstrBathroomsWhereSqls ) . ' ) ';
		}

		$strDesiredRentMin = trim( $objApplicationFilter->getDesiredRentMin() );
		$strDesiredRentMax = trim( $objApplicationFilter->getDesiredRentMax() );

		$strDesiredOccupants = trim( $objApplicationFilter->getDesiredOccupants() );
		if( false == empty( $strDesiredOccupants ) ) {
			$intJoinPropertyUnits = 1;
			$arrstrWhereParameters[] = 'pu.max_occupants = ' . ( int ) $objApplicationFilter->getDesiredOccupants() . ' ';
		}

		$strPropertyFloorplans = trim( $objApplicationFilter->getPropertyFloorplans() );
		if( false == empty( $strPropertyFloorplans ) ) {
			$arrstrWhereParameters[] = 'ca.property_floorplan_id IN ( ' . $objApplicationFilter->getPropertyFloorplans() . ' ) ';
		}
		$intUnitNumbers = trim( $objApplicationFilter->getUnitNumbers() );
		if( false == empty( $intUnitNumbers ) ) {
			$intJoinPropertyUnits = 1;
			$arrstrWhereParameters[] = 'lower( pu.unit_number ) LIKE lower( \'%' . addslashes( $objApplicationFilter->getUnitNumbers() ) . '%\' ) ';
		}

		$strLeaseIntervalTypes = trim( $objApplicationFilter->getLeaseIntervalTypes() );
		if( false == empty( $strLeaseIntervalTypes ) ) {
			$arrstrWhereParameters[] = 'ca.lease_interval_type_id IN ( ' . $objApplicationFilter->getLeaseIntervalTypes() . ' ) ';
		} elseif( true == valArr( $objApplicationFilter->getLeaseIntervalTypeIds() ) ) {
			$arrstrWhereParameters[] = 'ca.lease_interval_type_id IN ( ' . implode( ',', $objApplicationFilter->getLeaseIntervalTypeIds() ) . ' ) ';
		}

		if( true == is_numeric( $objApplicationFilter->getIsIntegrated() ) ) {
			if( CApplicationFilter::INTEGRATED == $objApplicationFilter->getIsIntegrated() ) {

				$arrstrWhereParameters[] = '( ( ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' AND ca.app_remote_primary_key IS NOT NULL ) ) ';

			} elseif( CApplicationFilter::NON_INTEGRATED == $objApplicationFilter->getIsIntegrated() ) {

				$arrstrWhereParameters[] = '( ( ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' AND ca.app_remote_primary_key IS NULL ) ) ';
			}
		}

		$strJoinSql = '';

		if( true == $boolJoinApplicantSql ) 							$strJoinSql .= ' INNER JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid ) ';
		if( true == $boolJoinApplicantSql && true == $boolIsRenewal ) 	$strJoinSql .= ' LEFT JOIN leases l ON ( ca.lease_id = l.id AND ca.cid = l.cid ) ';
		if( true == $boolJoinPropertyFloorplanSql )						$strJoinSql .= ' LEFT JOIN property_floorplans pfp ON ( pfp.id = ca.property_floorplan_id AND pfp.cid = ca.cid ' . $strCheckDeletedFloorPlansSql . '  ) ';
		if( true == $boolJoinPropertyUnitSql )							$strJoinSql .= ' LEFT JOIN property_units pu ON ( pu.id = ca.property_unit_id AND pu.cid = ca.cid ' . $strCheckDeletedUnitsSql . ' ) ';
		if( true == $boolJoinEvents )									$strJoinSql .= ' LEFT JOIN events e ON ( e.cid = ' . ( int ) $intCid . ' AND e.id = ca.last_event_id AND e.cid = ca.cid ) ';

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						COUNT( ca.id )
					FROM
						cached_applications ca
						JOIN application_stage_statuses ass ON ( ass.application_stage_id = ca.application_stage_id AND ass.application_status_id = ca.application_status_id AND ass.lease_interval_type_id = ca.lease_interval_type_id)
						INNER JOIN applicant_applications aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . $strCheckDeletedAASql . ' ) '
				. $strJoinSql . '
					WHERE
						ca.cid = ' . ( int ) $intCid;

		if( true == valArr( $arrstrWhereParameters ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhereParameters ) . ' ';
		}

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;
	}

	public static function fetchCustomApplicationByIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						ca.*,
						ap.id as applicant_id,
						aa.id as applicant_application_id,
						pfp.floorplan_name
					FROM
						cached_applications ca
						JOIN applicant_applications aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid ' . $strCheckDeletedAASql . ' )
						JOIN applicants ap ON ( ap.id = aa.applicant_id AND ap.cid = aa.cid )
						LEFT JOIN property_floorplans pfp ON ( pfp.id = ca.property_floorplan_id AND pfp.cid = ca.cid )
					WHERE
						ca.id = ' . ( int ) $intApplicationId . '
						AND aa.customer_type_id = ' . CCustomerType::PRIMARY . '
						AND ca.cid = ' . ( int ) $intCid;

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationByLeaseIntervalIdByCid( $intLeaseIntervalId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM cached_applications WHERE cid = ' . ( int ) $intCid . ' AND lease_interval_id = ' . ( int ) $intLeaseIntervalId;

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByApplicantIdByPropertyIdsByHistoricalDaysByApplicationStageStatusIdsByLeaseIntervalTypeIdsByCid( $intApplicantId, $arrintPropertyIds, $intHistoricalDays, $arrintApplicationStageStatusIds, $arrintLeaseIntervalTypeIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrintPropertyIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) return;
		if( false == valArr( $arrintLeaseIntervalTypeIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintLeaseIntervalTypeIds ) ) return;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						capp.*
					FROM
						cached_applications capp
						JOIN applicant_applications aa ON ( aa.application_id = capp.id AND aa.cid = capp.cid ' . $strCheckDeletedAASql . ' )
						JOIN applicants a ON ( a.id = aa.applicant_id AND a.cid = aa.cid )
						JOIN properties p ON ( ( p.id IN( ' . implode( ',', $arrintPropertyIds ) . ' ) AND p.cid = ' . ( int ) $intCid . ') OR  ( p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )  AND p.cid = ' . ( int ) $intCid . '))
						LEFT JOIN company_applications ca ON ( capp.company_application_id = ca.id AND capp.cid = ca.cid)
						LEFT JOIN property_applications pa ON ( ( ( pa.property_id = p.id AND pa.cid = p.cid )  OR ( pa.property_id = p.property_id AND pa.cid = p.cid ) ) AND pa.company_application_id = ca.id AND pa.cid = ca.cid )
					WHERE
						a.id::int = ' . ( int ) $intApplicantId . '
						AND a.cid::int = ' . ( int ) $intCid . '
						AND capp.property_id = p.id
						AND capp.cid = p.cid
						AND capp.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
						AND to_char( COALESCE( capp.application_datetime, capp.created_on ), \'MM/DD/YYYY\' )::date >= ( CURRENT_DATE - integer \'' . ( int ) $intHistoricalDays . '\' )::date
						AND
						(
								capp.application_stage_id = ' . CApplicationStage::LEASE . '
								OR
								(
										( capp.application_stage_id, capp.application_status_id ) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )
										AND
										(
												ca.id IS NULL
												OR
												( ca.is_published = 1 AND pa.id IS NOT NULL )
										)
								)
						) ';

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationByApplicantIdByHistoricalDaysByLeaseIntervalTypeIdsByCid( $intApplicantId, $intHistoricalDays, $arrintLeaseIntervalTypeIds, $arrintPropertyIds, $intCid, $objDatabase, $intPropertyId = NULL, $boolFetchDeniedApplications = false, $boolIncludeDeletedAA = false ) {

		if( false == valArr( $arrintLeaseIntervalTypeIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintLeaseIntervalTypeIds ) ) return;
		if( false == valArr( $arrintPropertyIds ) ) return;

		if( false == $boolFetchDeniedApplications ) {
		    $strFetchDeniedApplications = ' AND ( ca.cancellation_list_type_id IS NULL OR ca.cancellation_list_type_id <> ' . CListType::LEAD_DENIAL_REASONS . ' ) ';
		}

	    $strFetchCancelledArchivedApplications = ' AND ( ( ca.application_status_id IN ( ' . CApplicationStatus::CANCELLED . ', ' . CApplicationStatus::ON_HOLD . ' ) ' . $strFetchDeniedApplications . '
	    													AND CASE WHEN \'BLOCK_REOPEN_ALL_LEADS\' = 	pp.value THEN ca.application_status_id NOT IN ( ' . CApplicationStatus::CANCELLED . ', ' . CApplicationStatus::ON_HOLD . ' )

																	 WHEN \'BLOCK_REOPEN_APPLICATION\' = pp.value
																		THEN CASE WHEN ca.application_stage_id = ' . CApplicationStage::APPLICATION . '
																			THEN ca.application_status_id NOT IN ( ' . CApplicationStatus::CANCELLED . ', ' . CApplicationStatus::ON_HOLD . ' )
																		ELSE TRUE
																	 END

						                                             WHEN \'BLOCK_REOPEN_GUEST_CARD\' = pp.value
						                                             THEN CASE WHEN ca.application_stage_id = ' . CApplicationStage::PRE_APPLICATION . '
						                                                       THEN ca.application_status_id NOT IN ( ' . CApplicationStatus::CANCELLED . ', ' . CApplicationStatus::ON_HOLD . ' )
						                                                       ELSE TRUE
						                                                  END
						                                              ELSE TRUE
						                                          END
				                                     	  ) OR ca.application_status_id NOT IN ( ' . CApplicationStatus::CANCELLED . ', ' . CApplicationStatus::ON_HOLD . ' )
				                                     	)';

		$strCheckDeletedAASql		= ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		// we need to load the application for which the applicant has already applied and order those - NRW
		if( 0 < $intPropertyId ) {

			$strSql = '	SELECT
							ca.*,
							CASE WHEN ' . ( int ) $intPropertyId . ' = ca.property_id THEN 1 ELSE 0 END AS display_order
						FROM
							cached_applications ca
							JOIN applicant_applications aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid ' . $strCheckDeletedAASql . ' )
							JOIN applicants a ON ( a.id = aa.applicant_id AND a.cid = aa.cid )
							JOIN properties p ON ( p.id = ca.property_id AND p.cid = ca.cid )
							LEFT JOIN property_preferences pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.key = \'BLOCK_AUTO_REOPENING_OF_CANCELLED_ARCHIVED_LEADS\' )
						WHERE
							a.id = ' . ( int ) $intApplicantId . '
							AND a.cid = ' . ( int ) $intCid . '
							AND ca.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
							' . $strFetchCancelledArchivedApplications . '
							AND to_char( COALESCE( ca.application_datetime, ca.created_on ), \'MM/DD/YYYY\' )::date >= ( CURRENT_DATE - integer \'' . ( int ) $intHistoricalDays . '\' )::date
							AND ( p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ') OR p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
						ORDER BY display_order DESC, ca.created_on DESC LIMIT 1';

		} else {

			$strSql = '	SELECT
							ca.*
						FROM
							cached_applications ca
							JOIN applicant_applications aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid ' . $strCheckDeletedAASql . ' )
							JOIN applicants a ON ( a.id = aa.applicant_id AND a.cid = aa.cid )
							JOIN properties p ON ( p.id = ca.property_id AND p.cid = ca.cid )
							LEFT JOIN property_preferences pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.key = \'BLOCK_AUTO_REOPENING_OF_CANCELLED_ARCHIVED_LEADS\' )
						WHERE
								a.id = ' . ( int ) $intApplicantId . '
							AND	a.cid = ' . ( int ) $intCid . '
							AND ca.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
							' . $strFetchCancelledArchivedApplications . '
							AND to_char( COALESCE( ca.application_datetime, ca.created_on ), \'MM/DD/YYYY\' )::date >= ( CURRENT_DATE - integer \'' . ( int ) $intHistoricalDays . '\' )::date
							AND ( p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ') OR p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
						ORDER BY ca.created_on DESC LIMIT 1';
		}

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchActiveApplicationByApplicantIdByHistoricalDaysByLeaseIntervalTypeIdsByCid( $intApplicantId, $intHistoricalDays, $arrintLeaseIntervalTypeIds, $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrintLeaseIntervalTypeIds ) || false == valArr( $arrintPropertyIds ) ) return;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						ca.*
					FROM
						cached_applications ca
						JOIN applicant_applications aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid AND aa.applicant_id = ' . ( int ) $intApplicantId . $strCheckDeletedAASql . ' )
						JOIN properties p ON ( p.id = ca.property_id AND p.cid = ca.cid )
					WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND ca.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
						AND ca.application_status_id NOT IN ( ' . CApplicationStatus::CANCELLED . ', ' . CApplicationStatus::ON_HOLD . ' )
						AND to_char( COALESCE( ca.application_datetime, ca.created_on ), \'MM/DD/YYYY\' )::date >= ( CURRENT_DATE - integer \'' . ( int ) $intHistoricalDays . '\' )::date
						AND ( p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ') OR p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
					ORDER BY ca.created_on DESC LIMIT 1';

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationByIdByApplicantIdByCid( $intApplicationId, $intApplicantId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						ca.*
					FROM
						cached_applications ca,
						applicants ap,
						applicant_applications aa
					WHERE
						ca.id = aa.application_id
						AND ca.cid = aa.cid
						AND ap.id = aa.applicant_id
						AND ap.cid = aa.cid
						AND ap.id::int = ' . ( int ) $intApplicantId . '
						AND ca.id = ' . ( int ) $intApplicationId . '
						AND ca.cid = ' . ( int ) $intCid . $strCheckDeletedAASql;

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationByApplicantIdByPropertyIdByCid( $intApplicantId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = '	SELECT
						ca.*
					FROM
						cached_applications ca
						JOIN applicants a ON ( a.id = ca.primary_applicant_id )
					WHERE
						a.id = ' . ( int ) $intApplicantId . '
						AND ca.cid = ' . ( int ) $intCid . '
						AND ca.property_id = ' . ( int ) $intPropertyId;

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchLatestApplicationByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						ca.*
					FROM
						cached_applications ca,
						applicants appt,
						applicant_applications aa
					WHERE
						ca.cid = aa.cid
						AND ca.id = aa.application_id
						AND appt.id = aa.applicant_id
						AND appt.cid = aa.cid
						AND appt.customer_id::int = ' . ( int ) $intCustomerId . '
						AND ca.cid::int = ' . ( int ) $intCid . $strCheckDeletedAASql . '
						ORDER BY id DESC LIMIT 1';

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchLatestApplicationByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		$strSql = '	SELECT
						ca.*
					FROM
						cached_applications ca
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.lease_id::int = ' . ( int ) $intLeaseId . '
						ORDER BY id DESC LIMIT 1';

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchTotalApplicationsCountByPropertyIdsByApplicationFilterByLeaseIntervalTypeIdsByCid( $arrintPropertyIds, $arrintLeaseIntervalTypeIds, $objApplicationsFilter, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $boolIncludeDeletedUnits = false, $boolIncludeDeletedFloorPlans = false ) {
		if( false == isset( $arrintPropertyIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) return NULL;

		$strCheckDeletedUnitsSql 	  = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlansSql = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pfp.deleted_on IS NULL' : '';

		$arrstrWhere = [];

		$boolJoinApplicantSql 			= false;
		$boolJoinPropertyFloorplanSql	= false;
		$boolJoinPropertyUnitSql		= false;
		$boolIsRenewal					= false;
		$boolJoinEvents					= false;

		if( true == valArr( $arrintLeaseIntervalTypeIds ) && true == in_array( CLeaseIntervalType::RENEWAL, $arrintLeaseIntervalTypeIds ) ) {
			$boolIsRenewal = true;
		}
		$strQuickSearch = trim( $objApplicationsFilter->getQuickSearch() );
		if( false == empty( $strQuickSearch ) && ( 'Find a Lead ...' != trim( $objApplicationsFilter->getQuickSearch() ) ) ) {

			$boolJoinApplicantSql = true;

			$arrstrLeadNames = explode( ' ', trim( $objApplicationsFilter->getQuickSearch() ) );

			if( true == valArr( $arrstrLeadNames ) && 1 < \Psi\Libraries\UtilFunctions\count( $arrstrLeadNames ) ) {
				foreach( $arrstrLeadNames as $strLeadName ) {
					$strName = addslashes( $strLeadName );
					if( true == valStr( $strName ) ) {
						$arrstrNameFirstConditions[] 	= ' lower( appt.name_first ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) ';
						$arrstrNameLastConditions[] 	= ' lower( appt.name_last ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) ';
					}
				}

				$strCondition = ' ( ( ' . implode( ' OR ', $arrstrNameFirstConditions ) . ' ) AND ( ' . implode( ' OR ', $arrstrNameLastConditions ) . ' ) ) ';
			} else {
				$strName = array_pop( $arrstrLeadNames );
				$strCondition = ' ( lower( appt.name_last ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) OR lower( appt.name_first ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) OR CAST( ca.id AS text ) LIKE \'%' . addslashes( $strName ) . '%\' OR lower( appt.phone_number ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) OR lower( appt.username ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) ) ';
			}

			$arrstrWhere[] = $strCondition;
		}

		if( true == is_null( $objApplicationsFilter->getPageNo() ) && true == is_null( $objApplicationsFilter->getCountPerPage() ) && false == is_null( $objApplicationsFilter->getLeadIds() ) ) {
			$arrstrWhere[] = 'ca.id IN ( ' . $objApplicationsFilter->getLeadIds() . ' ) ';// used only for selected downloads
		}

		if( false == is_null( $objApplicationsFilter->getExcludingApplicationIds() ) ) {
			$arrstrWhere[] = 'ca.id NOT IN ( ' . implode( ',', $objApplicationsFilter->getExcludingApplicationIds() ) . ' ) ';
		}

		if( true == is_numeric( $objApplicationsFilter->getApplicationId() ) ) {
			$arrstrWhere[] = 'ca.id = ' . ( int ) $objApplicationsFilter->getApplicationId();
		}
		$strProperties = trim( $objApplicationsFilter->getProperties() );
		if( false == empty( $strProperties ) ) {
			$arrstrWhere[] = 'ca.property_id IN ( ' . $objApplicationsFilter->getProperties() . ' ) ';
		} else {
			$arrstrWhere[] = 'ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}

		if( 99 != $objApplicationsFilter->getApplicationStageStatuses() ) {
			$strApplicationStageStatuses = trim( $objApplicationsFilter->getApplicationStageStatuses() );
			if( false == empty( $strApplicationStageStatuses ) ) {
				$arrstrWhere[] = 'ass.id IN ( ' . $strApplicationStageStatuses . ' ) ';
			} elseif( true == is_null( $objApplicationsFilter->getApplicationId() ) ) {
			    // && ( true == is_null( $objApplicationsFilter->getQuickSearch() ) || 'Find a Lead ...' == trim( $objApplicationsFilter->getQuickSearch() ) || '' == trim( $objApplicationsFilter->getQuickSearch() ))
				$arrstrWhereParameters[] = '(application_stage_id,application_status_id) NOT IN ( ' . sqlIntMultiImplode( CApplicationStageStatus::$c_arrintNonActiveApplicationStatusIds ) . ' ) ';
			}
		}
		$strLeadSources = trim( $objApplicationsFilter->getLeadSources() );
		if( false == empty( $strLeadSources ) ) {
			$arrstrWhere[] = 'ca.lead_source_id IN ( ' . $objApplicationsFilter->getLeadSources() . ' ) ';
		}
		$strPsProducts = trim( $objApplicationsFilter->getPsProducts() );
		if( false == empty( $strPsProducts ) ) {
			$arrstrWhere[] = 'ca.ps_product_id IN ( ' . $objApplicationsFilter->getPsProducts() . ' ) ';
		}

		// We may require this code in future when we enhance the search leads .
		$strLastContactMinDays = trim( $objApplicationsFilter->getLastContactMinDays() );
		if( false == empty( $strLastContactMinDays ) && true == valArr( $objApplicationsFilter->getCompanyAssociatedPsProductIds() ) && true == in_array( CPsProduct::LEAD_MANAGEMENT, $objApplicationsFilter->getCompanyAssociatedPsProductIds() ) ) {
			$boolJoinEvents = true;
			$arrstrWhere[] = 'date_part( \'day\', ( current_timestamp  - e.event_datetime )) >= ' . ( int ) $objApplicationsFilter->getLastContactMinDays() . ' ';
		}

		$strLastContactMaxDays = trim( $objApplicationsFilter->getLastContactMaxDays() );
		if( false == empty( $strLastContactMaxDays ) && true == valArr( $objApplicationsFilter->getCompanyAssociatedPsProductIds() ) && true == in_array( CPsProduct::LEAD_MANAGEMENT, $objApplicationsFilter->getCompanyAssociatedPsProductIds() ) ) {
			$boolJoinEvents = true;
			$arrstrWhere[] = 'date_part( \'day\', ( current_timestamp  - e.event_datetime )) <= ' . ( int ) $objApplicationsFilter->getLastContactMaxDays() . ' ';
		}
		$strCompanyEmployees = trim( $objApplicationsFilter->getCompanyEmployees() );

		if( true == $objApplicationsFilter->getConsiderLeasingAgent() ) {

			if( false == empty( $strCompanyEmployees ) && true == $objApplicationsFilter->getLeasingAgentIsNull() ) {
				$arrstrWhere[] = ' ( ca.leasing_agent_id IN ( ' . $objApplicationsFilter->getCompanyEmployees() . ' ) OR ca.leasing_agent_id IS NULL ) ';
			} elseif( false == empty( $strCompanyEmployees ) ) {
				$arrstrWhere[] = ' ca.leasing_agent_id IN ( ' . $objApplicationsFilter->getCompanyEmployees() . ' ) ';
			} elseif( true == $objApplicationsFilter->getLeasingAgentIsNull() ) {
				$arrstrWhere[] = ' ( ca.leasing_agent_id IS NULL ) ';
			}
		}

		if( false == is_null( $objApplicationsFilter->getMoveInDateMin() ) && false == is_null( $objApplicationsFilter->getMoveInDateMax() ) && 'Min' != $objApplicationsFilter->getMoveInDateMin() && 'Max' != $objApplicationsFilter->getMoveInDateMax() ) {
			$intMoveInDateMin = strtotime( $objApplicationsFilter->getMoveInDateMin() );
			$intMoveInDateMax = strtotime( $objApplicationsFilter->getMoveInDateMax() );

			if( $intMoveInDateMin <= $intMoveInDateMax ) {
				$arrstrWhere[] = 'ca.lease_start_date >= \'' . date( 'Y-m-d', $intMoveInDateMin ) . '\'';
				$arrstrWhere[] = 'ca.lease_start_date <= \'' . date( 'Y-m-d', $intMoveInDateMax ) . '\'';
			}
		}
		$strDesiredBedrooms = trim( $objApplicationsFilter->getDesiredBedrooms() );
		if( false == empty( $strDesiredBedrooms ) ) {

			$boolJoinPropertyFloorplanSql = true;

			$boolGreaterThanSix 	= false;
			$arrintDesiredBedrooms	= [];
			$arrintDesiredBedrooms 	= explode( ',', $objApplicationsFilter->getDesiredBedrooms() );

			if( true == valArr( $arrintDesiredBedrooms ) ) {
				$arrintDesiredBedrooms = array_flip( $arrintDesiredBedrooms );
			}

			// if 6 is in DesiredBedrooms array then we need to look for 6 or greater than 6 bedrooms
			if( true == valArr( $arrintDesiredBedrooms ) && true == array_key_exists( '6', $arrintDesiredBedrooms ) ) {

				$boolGreaterThanSix = true;

				unset( $arrintDesiredBedrooms['6'] );
			}

			$strCondition = '( ';
			if( true == valArr( $arrintDesiredBedrooms ) ) {
				$strCondition .= ' pfp.number_of_bedrooms IN ( ' . implode( ',', array_keys( $arrintDesiredBedrooms ) ) . ' ) ';

				if( true == $boolGreaterThanSix ) {
					$strCondition .= ' OR ';
				}
			}

			if( true == $boolGreaterThanSix ) {
				$strCondition .= 'pfp.number_of_bedrooms > 5 ';
			}
			$strCondition .= ')';

			$arrstrWhere[] = $strCondition;

		}
		$strDesiredBathrooms = trim( $objApplicationsFilter->getDesiredBathrooms() );
		if( false == empty( $strDesiredBathrooms ) ) {

			$boolJoinPropertyFloorplanSql = true;

			$boolGreaterThanSix 	= false;
			$arrintDesiredBathrooms	= [];
			$arrintDesiredBathrooms = explode( ',', $objApplicationsFilter->getDesiredBathrooms() );

			if( true == valArr( $arrintDesiredBathrooms ) ) {
				$arrintDesiredBathrooms = array_flip( $arrintDesiredBathrooms );
			}

			// if 6 is in DesiredBathrooms array then we need to look for 6 or greater than 6 bathrooms
			if( true == valArr( $arrintDesiredBathrooms ) && true == array_key_exists( '6', $arrintDesiredBathrooms ) ) {

				$boolGreaterThanSix = true;

				unset( $arrintDesiredBathrooms['6'] );
			}

			$strCondition = '( ';
			if( true == valArr( $arrintDesiredBathrooms ) ) {
				$strCondition .= ' pfp.number_of_bathrooms IN ( ' . implode( ',', array_keys( $arrintDesiredBathrooms ) ) . ' ) ';

				if( true == $boolGreaterThanSix ) {
					$strCondition .= ' OR ';
				}
			}

			if( true == $boolGreaterThanSix ) {
				$strCondition .= 'pfp.number_of_bathrooms > 5 ';
			}
			$strCondition .= ')';

			$arrstrWhere[] = $strCondition;
		}

		// Removing join from property_units because we are not getting min/max rent from property units table.
		$strDesiredRentMin = trim( $objApplicationsFilter->getDesiredRentMin() );
		$strDesiredRentMax = trim( $objApplicationsFilter->getDesiredRentMax() );

		if( false == empty( $strDesiredRentMax ) && $strDesiredRentMin < $strDesiredRentMax ) {
			$arrstrWhere[] = 'ca.desired_rent_min >= ' . ( int ) $objApplicationsFilter->getDesiredRentMin() . ' ';
			$arrstrWhere[] = 'ca.desired_rent_max <= ' . ( int ) $objApplicationsFilter->getDesiredRentMax() . ' ';
		} elseif( false == empty( $strDesiredRentMin ) && true == empty( $strDesiredRentMax ) ) {
			$arrstrWhere[] = 'ca.desired_rent_min >= ' . ( int ) $objApplicationsFilter->getDesiredRentMin() . ' ';
		} elseif( $strDesiredRentMin != 0 && $strDesiredRentMin == $strDesiredRentMax ) {
			$arrstrWhere[] = 'ca.desired_rent_min = ' . ( int ) $objApplicationsFilter->getDesiredRentMin() . ' ';
			$arrstrWhere[] = 'ca.desired_rent_max = ' . ( int ) $objApplicationsFilter->getDesiredRentMax() . ' ';
		}
		$strDesiredOccupants = trim( $objApplicationsFilter->getDesiredOccupants() );
		if( false == empty( $strDesiredOccupants ) ) {
			$boolJoinPropertyUnitSql = true;

			$arrstrWhere[] = 'pu.max_occupants = ' . ( int ) $objApplicationsFilter->getDesiredOccupants() . ' ';
		}
		$strPropertyFloorplans = trim( $objApplicationsFilter->getPropertyFloorplans() );
		if( false == empty( $strPropertyFloorplans ) ) {
			$arrstrWhere[] = 'ca.property_floorplan_id IN ( ' . $objApplicationsFilter->getPropertyFloorplans() . ' ) ';
		}
		$intUnitNumbers = trim( $objApplicationsFilter->getUnitNumbers() );
		if( false == empty( $intUnitNumbers ) ) {
			$boolJoinPropertyUnitSql = true;

			$arrstrWhere[] = 'lower( pu.unit_number ) LIKE lower( \'%' . addslashes( $objApplicationsFilter->getUnitNumbers() ) . '%\' ) ';
		}
		$strLeaseIntervalTypes = trim( $objApplicationsFilter->getLeaseIntervalTypes() );
		if( false == empty( $strLeaseIntervalTypes ) ) {
			$arrstrWhere[] = 'ca.lease_interval_type_id IN ( ' . $objApplicationsFilter->getLeaseIntervalTypes() . ' ) ';
		} elseif( true == isset( $arrintLeaseIntervalTypeIds ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrintLeaseIntervalTypeIds ) ) {
			$arrstrWhere[] = 'ca.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' ) ';
		}

		if( false == is_null( $objApplicationsFilter->getIsIntegrated() ) ) {
			if( CApplicationFilter::INTEGRATED == $objApplicationsFilter->getIsIntegrated() ) {

				if( CLeaseIntervalType::APPLICATION == $objApplicationsFilter->getLeaseIntervalTypes() ) {
					$arrstrWhere[] = 'ca.app_remote_primary_key is NOT NULL';
				} elseif( NULL == $objApplicationsFilter->getLeaseIntervalTypes() ) {
					$arrstrWhere[] = '( ca.app_remote_primary_key is NOT NULL OR ca.guest_remote_primary_key is NOT NULL )';
				}
			} elseif( CApplicationFilter::NON_INTEGRATED == $objApplicationsFilter->getIsIntegrated() ) {

				if( CLeaseIntervalType::APPLICATION == $objApplicationsFilter->getLeaseIntervalTypes() ) {
					$arrstrWhere[] = 'ca.app_remote_primary_key is NULL AND ca.guest_remote_primary_key is NULL';
				} else {
					$arrstrWhere[] = '( ca.app_remote_primary_key is NULL AND ca.guest_remote_primary_key is NULL )';
				}
			}
		}

		if( false == is_null( $objApplicationsFilter->getApplicationStartDate() ) && false == is_null( $objApplicationsFilter->getApplicationEndDate() ) && 'Min' != $objApplicationsFilter->getApplicationStartDate() && 'Max' != $objApplicationsFilter->getApplicationEndDate() ) {
			$intApplicationStartDate = strtotime( $objApplicationsFilter->getApplicationStartDate() );
			$intApplicationEndDate = strtotime( $objApplicationsFilter->getApplicationEndDate() );
			if( $intApplicationStartDate <= $intApplicationEndDate ) {
				$arrstrWhere[] = 'ca.application_datetime >= \'' . date( 'Y-m-d', strtotime( $objApplicationsFilter->getApplicationStartDate() ) ) . ' 00:00:00\'';
				$arrstrWhere[] = 'ca.application_datetime <= \'' . date( 'Y-m-d', strtotime( $objApplicationsFilter->getApplicationEndDate() ) ) . ' 23:59:59\'';
			}
		}

		if( false == is_null( $objApplicationsFilter->getIsCompletedOnIsNotNull() ) && true == $objApplicationsFilter->getIsCompletedOnIsNotNull() ) {
			$arrstrWhere[] = 'ca.application_completed_on IS NOT NULL';
		}

		if( false == is_null( $objApplicationsFilter->getCompanyapplicationIds() ) ) {
			$arrstrWhere[] = 'ca.company_application_id IN( ' . implode( ',', $objApplicationsFilter->getCompanyapplicationIds() ) . ' )';
		}

		$strJoinSql = '';
		if( true == $boolJoinApplicantSql || true == $objApplicationsFilter->getIsFollowup() ) {
			$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

			$strJoinSql .= ' INNER JOIN applicant_applications aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . $strCheckDeletedAASql . ' )
							INNER JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid ) ';
		}

		if( true == $boolJoinApplicantSql && true == $boolIsRenewal ) {
			$strJoinSql	.= ' LEFT JOIN leases l ON ( ca.lease_id = l.id AND ca.cid = l.cid ) ';
		}

		if( true == $boolJoinPropertyFloorplanSql ) {
			$strJoinSql .= ' LEFT JOIN property_floorplans pfp ON ( pfp.id = ca.property_floorplan_id AND pfp.cid = ca.cid ' . $strCheckDeletedFloorPlansSql . ' ) ';
		}

		if( true == $boolJoinPropertyUnitSql ) {
			$strJoinSql .= ' LEFT JOIN property_units pu ON ( pu.id = ca.property_unit_id AND pu.cid = ca.cid ' . $strCheckDeletedUnitsSql . ' ) ';
		}

		// There's no reason to
		if( true == $boolJoinEvents ) {
			$strJoinSql .= ' LEFT JOIN events e ON ( e.cid = ' . ( int ) $intCid . ' AND e.id = ca.last_event_id AND e.cid = ca.cid ) ';
		}

		$strSql = '	SELECT
						COUNT( ca.id )
					FROM
						cached_applications ca
						JOIN application_stage_statuses ass ON ( ass.application_stage_id = ca.application_stage_id AND ass.application_status_id = ca.application_status_id AND ass.lease_interval_type_id = ca.lease_interval_type_id)'
						. $strJoinSql . '
						WHERE
							ca.cid = ' . ( int ) $intCid;

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;
	}

	public static function fetchPaginatedApplicationsByPropertyIdsByLeaseIntervalTypeIdsByCid( $arrintPropertyIds, $arrintLeaseIntervalTypeIds, $objApplicationsFilter, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strCheckDeletedUnitsSql 		 = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql 	 = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesAsSql	 = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND deleted_on IS NULL' : '';

		// -1 means outside the core query, 1 means inside the core query, and 0 means no join at all
		$intJoinLeases = ( true == in_array( CLeaseIntervalType::RENEWAL, ( array ) $arrintLeaseIntervalTypeIds ) ) ? - 1 : 0;
		$intJoinLeases = ( - 1 == $intJoinLeases && 'lease_end_date' == $objApplicationsFilter->getSortBy() ) ? 1 : - 1;

		$intJoinPropertyUnits = - 1;
		$intJoinPropertyFloorplans = 0;
		$intJoinEvents = ( true == valArr( $objApplicationsFilter->getCompanyAssociatedPsProductIds() ) && ( true == in_array( CPsProduct::LEAD_MANAGEMENT, $objApplicationsFilter->getCompanyAssociatedPsProductIds() ) || true == in_array( CPsProduct::ENTRATA, $objApplicationsFilter->getCompanyAssociatedPsProductIds() ) ) ) ? - 1 : 0;

		$arrstrWhere = [];

		if( false == is_null( $objApplicationsFilter->getPageNo() ) && false == is_null( $objApplicationsFilter->getCountPerPage() ) ) {
			$intOffset 	= ( 0 < $objApplicationsFilter->getPageNo() ) ? $objApplicationsFilter->getCountPerPage() * ( $objApplicationsFilter->getPageNo() - 1 ) : 0;
			$intLimit 	= ( int ) $objApplicationsFilter->getCountPerPage();
		}

		$strQuickSearch = trim( $objApplicationsFilter->getQuickSearch() );

		if( false == empty( $strQuickSearch ) ) {
			$arrstrLeadNames = explode( ' ', trim( $objApplicationsFilter->getQuickSearch() ) );

			if( true == valArr( $arrstrLeadNames ) && 1 < \Psi\Libraries\UtilFunctions\count( $arrstrLeadNames ) ) {
				foreach( $arrstrLeadNames as $strLeadName ) {
					$strName = addslashes( $strLeadName );
					if( true == valStr( $strName ) ) {
						$arrstrNameFirstConditions[] 	= ' lower( appt.name_first ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) ';
						$arrstrNameLastConditions[] 	= ' lower( appt.name_last ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) ';
					}
				}

				$strCondition = ' ( ( ' . implode( ' OR ', $arrstrNameFirstConditions ) . ' ) AND ( ' . implode( ' OR ', $arrstrNameLastConditions ) . ' ) ) ';
			} else {
				$strName = array_pop( $arrstrLeadNames );
				$strCondition = ' ( lower( appt.name_last ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) OR lower( appt.name_first ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) OR CAST( ca.id AS text ) LIKE \'%' . addslashes( $strName ) . '%\' OR appt.phone_number LIKE \'%' . addslashes( $strName ) . '%\' OR lower( appt.username ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) OR CAST( ca.lease_id AS text ) LIKE ( \'%' . addslashes( $strName ) . '%\' ) ) ';
			}

			$arrstrWhere[] = $strCondition;
		}

		if( true == is_null( $objApplicationsFilter->getPageNo() ) && true == is_null( $objApplicationsFilter->getCountPerPage() ) && false == is_null( $objApplicationsFilter->getLeadIds() ) ) {
			$arrstrWhere[] = 'ca.id IN ( ' . $objApplicationsFilter->getLeadIds() . ' ) ';// used only for selected downloads
		}

		if( true == is_numeric( $objApplicationsFilter->getApplicationId() ) ) {
			$arrstrWhere[] = 'ca.id = ' . ( int ) $objApplicationsFilter->getApplicationId();
		}

		if( false == is_null( $objApplicationsFilter->getExcludingApplicationIds() ) ) {
			$arrstrWhere[] = 'ca.id NOT IN ( ' . implode( ',', $objApplicationsFilter->getExcludingApplicationIds() ) . ' ) ';
		}
		$strProperties = trim( $objApplicationsFilter->getProperties() );
		if( false == empty( $strProperties ) ) {
			$arrstrWhere[] = 'ca.property_id IN ( ' . $objApplicationsFilter->getProperties() . ' ) ';
			$strEventProperties = $objApplicationsFilter->getProperties();
		} else {
			$arrstrWhere[] = 'ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
			$strEventProperties = implode( ',', $arrintPropertyIds );
		}

		if( ( true == is_null( $objApplicationsFilter->getQuickSearch() ) || false == is_numeric( $objApplicationsFilter->getQuickSearch() ) ) && 99 != ( int ) $objApplicationsFilter->getApplicationStageStatuses() ) {
			$strApplicationStageStatuses = trim( $objApplicationsFilter->getApplicationStageStatuses() );

			if( true == $objApplicationsFilter->getIsPartiallyGeneratedLease() ) {

				$strPartiallyGeneratedLeaseWhere = '( (ca.application_stage_id,ca.application_status_id) IN ( ' . sqlIntMultiImplode( [ CApplicationStage::APPLICATION => [ CApplicationStatus::APPROVED ] ] ) . ' ) ';
				$strPartiallyGeneratedLeaseWhere .= ' OR ( (ca.application_stage_id,ca.application_status_id) IN ( ' . sqlIntMultiImplode( [ CApplicationStage::LEASE => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED ] ] ) . ' ) ';
				$strPartiallyGeneratedLeaseWhere .= ' AND 0 < ( SELECT count ( fa.id ) FROM file_associations fa JOIN files f ON ( fa.file_id = f.id AND f.cid = fa.cid AND fa.cid = ' . ( int ) $intCid . ' AND f.file_upload_date IS NULL ) JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid AND ft.system_code = \'LEASE\') WHERE fa.application_id = ca.id ) )';
				$strPartiallyGeneratedLeaseWhere .= ' )';
				$arrstrWhere[] = $strPartiallyGeneratedLeaseWhere;
			} elseif( false == empty( $strApplicationStageStatuses ) ) {
				$arrstrWhere[] = 'ass.id IN ( ' . $objApplicationsFilter->getApplicationStageStatuses() . ' ) ';
			} elseif( true == is_null( $objApplicationsFilter->getApplicationId() ) ) {
			    // && ( true == is_null( $objApplicationsFilter->getQuickSearch() ) || 'Find a Lead ...' == trim( $objApplicationsFilter->getQuickSearch() ) || '' == trim( $objApplicationsFilter->getQuickSearch() ))
				$arrstrWhereParameters[] = '(application_stage_id,application_status_id) NOT IN ( ' . sqlIntMultiImplode( CApplicationStageStatus::$c_arrintNonActiveApplicationStatusIds ) . ' ) ';
			}
		}
		$strLeadSources = trim( $objApplicationsFilter->getLeadSources() );
		if( false == empty( $strLeadSources ) ) {
			$arrstrWhere[] = 'ca.lead_source_id IN ( ' . $objApplicationsFilter->getLeadSources() . ' ) ';
		}
		$strPsProducts = trim( $objApplicationsFilter->getPsProducts() );
		if( false == empty( $strPsProducts ) ) {
			$arrstrWhere[] = 'ca.ps_product_id IN ( ' . $objApplicationsFilter->getPsProducts() . ' ) ';
		}

		// We may require this code in future when we enhance the search leads .
		$strLastContactMinDays = trim( $objApplicationsFilter->getLastContactMinDays() );
		if( false == empty( $strLastContactMinDays ) ) {
			$arrstrWhere[] = 'date_part( \'day\', ( current_timestamp  - e.event_datetime )) >= ' . ( int ) $objApplicationsFilter->getLastContactMinDays() . ' ';
			$intJoinEvents = 1;
		}

		$strLastContactMaxDays = trim( $objApplicationsFilter->getLastContactMaxDays() );
		if( false == empty( $strLastContactMaxDays ) ) {
			$arrstrWhere[] = 'date_part( \'day\', ( current_timestamp  - e.event_datetime )) <= ' . ( int ) $objApplicationsFilter->getLastContactMaxDays() . ' ';
			$intJoinEvents = 1;
		}

		if( 'contact_days' == $objApplicationsFilter->getSortBy() ) {
			$intJoinEvents = 1;
		}

		if( false == is_null( $objApplicationsFilter->getMoveInDateMin() ) && false == is_null( $objApplicationsFilter->getMoveInDateMax() ) && 'Min' != $objApplicationsFilter->getMoveInDateMin() && 'Max' != $objApplicationsFilter->getMoveInDateMax() ) {
			$intMoveInDateMin = strtotime( $objApplicationsFilter->getMoveInDateMin() );
			$intMoveInDateMax = strtotime( $objApplicationsFilter->getMoveInDateMax() );

			if( $intMoveInDateMin <= $intMoveInDateMax ) {
				$arrstrWhere[] = 'ca.lease_start_date >= \'' . date( 'Y-m-d', $intMoveInDateMin ) . '\'';
				$arrstrWhere[] = 'ca.lease_start_date <= \'' . date( 'Y-m-d', $intMoveInDateMax ) . '\'';
			}
		}

		$strCompanyEmployees = trim( $objApplicationsFilter->getCompanyEmployees() );

		if( true == $objApplicationsFilter->getConsiderLeasingAgent() ) {

			if( false == empty( $strCompanyEmployees ) && true == $objApplicationsFilter->getLeasingAgentIsNull() ) {
				$arrstrWhere[] = ' ( ca.leasing_agent_id IN ( ' . $objApplicationsFilter->getCompanyEmployees() . ' ) OR ca.leasing_agent_id IS NULL ) ';
			} elseif( false == empty( $strCompanyEmployees ) ) {
				$arrstrWhere[] = ' ca.leasing_agent_id IN ( ' . $objApplicationsFilter->getCompanyEmployees() . ' ) ';
			} elseif( true == $objApplicationsFilter->getLeasingAgentIsNull() ) {
				$arrstrWhere[] = ' ( ca.leasing_agent_id IS NULL ) ';
			}
		}

		$strDesiredBedrooms = trim( $objApplicationsFilter->getDesiredBedrooms() );
		if( false == empty( $strDesiredBedrooms ) ) {

			$intJoinPropertyFloorplans 		= 1;
			$boolGreaterThanSix 	= false;
			$arrintDesiredBedrooms	= [];
			$arrintDesiredBedrooms 	= explode( ',', $objApplicationsFilter->getDesiredBedrooms() );

			if( true == valArr( $arrintDesiredBedrooms ) ) {
				$arrintDesiredBedrooms = array_flip( $arrintDesiredBedrooms );
			}

			// if 6 is in DesiredBedrooms array then we need to look for 6 or greater than 6 bedrooms
			if( true == valArr( $arrintDesiredBedrooms ) && true == array_key_exists( '6', $arrintDesiredBedrooms ) ) {

				$boolGreaterThanSix = true;

				unset( $arrintDesiredBedrooms['6'] );
			}

			$strCondition = '( ';
			if( true == valArr( $arrintDesiredBedrooms ) ) {
				$strCondition .= ' ca.desired_bedrooms IN ( ' . implode( ',', array_keys( $arrintDesiredBedrooms ) ) . ' ) ';

				if( true == $boolGreaterThanSix ) {
					$strCondition .= ' OR ';
				}
			}

			if( true == $boolGreaterThanSix ) {
				$strCondition .= ' ca.desired_bedrooms > 5 ';
			}
			$strCondition .= ')';

			$arrstrWhere[] = $strCondition;
		}

		$strDesiredBathrooms = trim( $objApplicationsFilter->getDesiredBathrooms() );
		if( false == empty( $strDesiredBathrooms ) ) {

			$intJoinPropertyFloorplans 	= 1;
			$boolGreaterThanSix 		= false;
			$arrintDesiredBathrooms		= [];
			$arrintDesiredBathrooms 	= explode( ',', $objApplicationsFilter->getDesiredBathrooms() );

			if( true == valArr( $arrintDesiredBathrooms ) ) {
				$arrintDesiredBathrooms = array_flip( $arrintDesiredBathrooms );
			}

			// if 6 is in DesiredBathrooms array the we need to look for 6 or greater than 6 bathrooms
			if( true == valArr( $arrintDesiredBathrooms ) && true == array_key_exists( '6', $arrintDesiredBathrooms ) ) {

				$boolGreaterThanSix = true;

				unset( $arrintDesiredBathrooms['6'] );
			}

			$strCondition = '( ';
			if( true == valArr( $arrintDesiredBathrooms ) ) {
				$strCondition .= ' ca.desired_bathrooms IN ( ' . implode( ',', array_keys( $arrintDesiredBathrooms ) ) . ' ) ';

				if( true == $boolGreaterThanSix ) {
					$strCondition .= ' OR ';
				}
			}

			if( true == $boolGreaterThanSix ) {
				$strCondition .= 'ca.desired_bathrooms > 5 ';
			}
			$strCondition .= ')';

			$arrstrWhere[] = $strCondition;
		}

		// Removing join from property_units table because we are not getting min/max rent from property units table.
		$strDesiredRentMin = trim( $objApplicationsFilter->getDesiredRentMin() );
		$strDesiredRentMax = trim( $objApplicationsFilter->getDesiredRentMax() );

		if( false == empty( $strDesiredRentMax ) && $strDesiredRentMin < $strDesiredRentMax ) {
			$arrstrWhere[] = 'ca.desired_rent_min >= ' . ( int ) $objApplicationsFilter->getDesiredRentMin() . ' ';
			$arrstrWhere[] = 'ca.desired_rent_max <= ' . ( int ) $objApplicationsFilter->getDesiredRentMax() . ' ';
		} elseif( false == empty( $strDesiredRentMin ) && true == empty( $strDesiredRentMax ) ) {
			$arrstrWhere[] = 'ca.desired_rent_min >= ' . ( int ) $objApplicationsFilter->getDesiredRentMin() . ' ';
		} elseif( $strDesiredRentMin != 0 && $strDesiredRentMin == $strDesiredRentMax ) {
			$arrstrWhere[] = 'ca.desired_rent_min = ' . ( int ) $objApplicationsFilter->getDesiredRentMin() . ' ';
			$arrstrWhere[] = 'ca.desired_rent_max = ' . ( int ) $objApplicationsFilter->getDesiredRentMax() . ' ';
		}
		$strDesiredOccupants = trim( $objApplicationsFilter->getDesiredOccupants() );
		if( false == empty( $strDesiredOccupants ) ) {
			$intJoinPropertyUnits = 1;
			$arrstrWhere[] = 'pu.max_occupants = ' . ( int ) $objApplicationsFilter->getDesiredOccupants() . ' ';
		}
		$strPropertyFloorplans = trim( $objApplicationsFilter->getPropertyFloorplans() );
		if( false == empty( $strPropertyFloorplans ) ) {
			$arrstrWhere[] = 'ca.property_floorplan_id IN ( ' . $objApplicationsFilter->getPropertyFloorplans() . ' ) ';
		}
		$strUnitNumbers = trim( $objApplicationsFilter->getUnitNumbers() );
		if( false == empty( $strUnitNumbers ) ) {
			$intJoinPropertyUnits = 1;
			$arrstrWhere[] = 'lower( pu.unit_number ) LIKE lower( \'%' . addslashes( $objApplicationsFilter->getUnitNumbers() ) . '%\' ) ';
		}
		$strLeaseIntervalTypes = trim( $objApplicationsFilter->getLeaseIntervalTypes() );
		if( false == empty( $strLeaseIntervalTypes ) ) {
			$arrstrWhere[] = 'ca.lease_interval_type_id IN ( ' . $objApplicationsFilter->getLeaseIntervalTypes() . ' ) ';
		} elseif( true == isset( $arrintLeaseIntervalTypeIds ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrintLeaseIntervalTypeIds ) ) {
			$arrstrWhere[] = 'ca.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' ) ';
		}

		if( false == is_null( $objApplicationsFilter->getIsIntegrated() ) ) {
			if( CApplicationFilter::INTEGRATED == $objApplicationsFilter->getIsIntegrated() ) {

				if( CLeaseIntervalType::APPLICATION == $objApplicationsFilter->getLeaseIntervalTypes() ) {
					$arrstrWhere[] = 'ca.app_remote_primary_key is NOT NULL';
				} elseif( NULL == $objApplicationsFilter->getLeaseIntervalTypes() ) {
					$arrstrWhere[] = '( ca.app_remote_primary_key is NOT NULL OR ca.guest_remote_primary_key is NOT NULL )';
				}
			} elseif( CApplicationFilter::NON_INTEGRATED == $objApplicationsFilter->getIsIntegrated() ) {

				if( CLeaseIntervalType::APPLICATION == $objApplicationsFilter->getLeaseIntervalTypes() ) {
					$arrstrWhere[] = 'ca.app_remote_primary_key is NULL AND ca.guest_remote_primary_key is NULL';
				} else {
					$arrstrWhere[] = '( ca.app_remote_primary_key is NULL AND ca.guest_remote_primary_key is NULL )';
				}
			}
		}

		if( false == is_null( $objApplicationsFilter->getApplicationStartDate() ) && false == is_null( $objApplicationsFilter->getApplicationEndDate() ) && 'Min' != $objApplicationsFilter->getApplicationStartDate() && 'Max' != $objApplicationsFilter->getApplicationEndDate() ) {
			$intApplicationStartDate = strtotime( $objApplicationsFilter->getApplicationStartDate() );
			$intApplicationEndDate = strtotime( $objApplicationsFilter->getApplicationEndDate() );
			if( $intApplicationStartDate <= $intApplicationEndDate ) {
				$arrstrWhere[] = 'ca.application_datetime >= \'' . date( 'Y-m-d', strtotime( $objApplicationsFilter->getApplicationStartDate() ) ) . ' 00:00:00\'';
				$arrstrWhere[] = 'ca.application_datetime <= \'' . date( 'Y-m-d', strtotime( $objApplicationsFilter->getApplicationEndDate() ) ) . ' 23:59:59\'';

			} elseif( false == is_null( $objApplicationsFilter->getPageNo() ) && false == is_null( $objApplicationsFilter->getCountPerPage() ) ) {

				$objApplicationsFilter->validate( 'validate_dates' );

				$objApplicationsFilter->changeDateFormat();
			}
		}

		if( false == is_null( $objApplicationsFilter->getIsCompletedOnIsNotNull() ) && true == $objApplicationsFilter->getIsCompletedOnIsNotNull() ) {
			$arrstrWhere[] = 'ca.application_completed_on IS NOT NULL';
		}

		if( true == valArr( $objApplicationsFilter->getApplicationIds() ) ) {
			$arrstrWhere[] = 'ca.id IN ( ' . implode( ',', $objApplicationsFilter->getApplicationIds() ) . ')';
		}

		if( false == is_null( $objApplicationsFilter->getCompanyapplicationIds() ) ) {
			$arrstrWhere[] = 'ca.company_application_id IN( ' . implode( ',', $objApplicationsFilter->getCompanyapplicationIds() ) . ' )';
		}

		$strEventsJoinSql = ' LEFT JOIN events e ON ( e.cid = ' . ( int ) $intCid . ' AND e.id = ca.last_event_id AND e.cid = ca.cid ) ';

		$strEventFields = 'e.id as event_id,
							e.event_datetime,
							e.event_type_id,
							DATE_TRUNC(\'day\',  NOW() - e.event_datetime ) as last_contact_days,';

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						ca.*,
						CASE WHEN us.space_number IS NOT NULL AND ( 1 < ( SELECT COUNT(id) FROM unit_spaces WHERE property_unit_id = ca.property_unit_id AND cid = ca.cid  AND cid = ' . ( int ) $intCid . $strCheckDeletedUnitSpacesAsSql . ' ) )
							THEN pu.unit_number || \'-\' || us.space_number
							ELSE pu.unit_number
						END AS unit_number,
						' . ( ( -1 == $intJoinEvents ) ? $strEventFields : '' ) . '
						' . ( ( 1 == $intJoinLeases ) ? 'ca.lease_end_date,' : '' ) . '
						' . ( ( -1 == $intJoinLeases ) ? 'li.lease_end_date,' : '' ) . '

						CASE
							WHEN TRUE = ca.is_pet_policy_conflicted
							AND ( 0 = (	SELECT
												count(pap.id)
											FROM
												property_application_preferences AS pap
											WHERE
												pap.cid = ' . ( int ) $intCid . '
												AND pap.property_id = ca.property_id
												AND pap.company_application_id = ca.company_application_id
												AND pap.cid = ca.cid
												AND pap.key = \'HIDE_OPTION_PET\' LIMIT 1 ) ) THEN 1 ELSE 0
						END AS is_pet_policy_conflicted
					FROM
						(
							SELECT
								appt.id AS applicant_id,
								appt.name_last,
								appt.name_first,
								appt.phone_number,
								appt.mobile_number,
								appt.work_number,
								appt.username as email_address,
								ca.id as id,
								ca.cid,
								ca.property_id,
								ca.company_application_id,
								ca.application_datetime,
								ca.property_floorplan_id,
								ca.property_unit_id,
								ca.unit_space_id,
								ca.lead_source_id,
								ca.leasing_agent_id,
								ca.application_stage_id,
								ca.application_status_id,
								ca.last_event_id,
								ca.is_pet_policy_conflicted,
								ca.lease_id,
								ca.lease_interval_type_id,
								ca.ps_product_id,
								ca.screening_id,
								ca.guest_remote_primary_key,
								ca.app_remote_primary_key,
								ca.created_on,
								ca.application_completed_on,
								ca.lease_completed_on,
								ca.application_approved_on,
								aa.application_step_id,
								aa.application_document_id,
								' . ( ( 1 == $intJoinEvents ) ? $strEventFields : '' ) . '
								' . ( ( 1 == $intJoinLeases ) ? 'li.lease_end_date as lease_end_date,' : '' ) . '
								aa.application_form_id,
								aa.lease_generated_on,
								CASE
									WHEN ad.primary_phone_number_type_id = ' . CPhoneNumberType::HOME . ' THEN appt.phone_number
									WHEN ad.primary_phone_number_type_id = ' . CPhoneNumberType::OFFICE . ' THEN appt.work_number
									WHEN ad.primary_phone_number_type_id = ' . CPhoneNumberType::MOBILE . ' THEN appt.mobile_number
								END AS primary_phone_number,
								ad.primary_phone_number_type_id';

		$strSql .= ' FROM
						cached_applications ca
						JOIN application_stage_statuses ass ON ( ass.application_stage_id = ca.application_stage_id AND ass.application_status_id = ca.application_status_id AND ass.lease_interval_type_id = ca.lease_interval_type_id)
						JOIN applicant_applications aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . $strCheckDeletedAASql . ' )
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid )
						JOIN applicant_details ad ON ( ad.cid = appt.cid AND ad.applicant_id = appt.id ) ';

		$strSql .= ( 1 == $intJoinLeases ) ? ' LEFT JOIN leases l ON ( ca.lease_id = l.id AND ca.cid = l.cid )
											LEFT JOIN lease_intervals li ON ( li.lease_id = l.id AND li.id = l.active_lease_interval_id AND li.cid = l.cid ) ' : '';

		$strSql .= ( 1 == $intJoinPropertyUnits ) ? ' LEFT JOIN property_units pu ON ( pu.id = ca.property_unit_id AND pu.cid = ca.cid ' . $strCheckDeletedUnitsSql . ' ) ' : '';
		$strSql .= ( 1 == $intJoinEvents ) ? $strEventsJoinSql : '';

		$strSql .= ' WHERE ca.cid = ' . ( int ) $intCid;
		if( 0 == $intJoinLeases ) {
			$strSql .= ' AND li.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED;
		}

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		$arrstrOrderBy = [
			'lead_id'					=> 'appt.id',
			'name'						=> 'appt.name_last || appt.name_first',
			'status'					=> 'ca.application_status_id',
			'property'					=> 'ca.property_id',
			'source'					=> 'ca.lead_source_id',
			'application_datetime'		=> 'ca.application_datetime',
			'application_approved_on'	=> 'ca.application_approved_on',
			'application_completed_on'	=> 'ca.application_completed_on',
			'lease_completed_on'		=> 'ca.lease_completed_on',
			'lease_interval_type'		=> 'ca.lease_interval_type_id',
			'lease_end_date'			=> 'li.lease_end_date',
			'contact_days'				=> 'last_contact_days'
		];

		$strOrderBy = ( true == isset( $arrstrOrderBy[$objApplicationsFilter->getSortBy()] ) ? $arrstrOrderBy[$objApplicationsFilter->getSortBy()] : 'ca.application_datetime' );

		$strSortDirection = ( false == is_null( $objApplicationsFilter->getSortDirection() ) ) ? $objApplicationsFilter->getSortDirection() : 'DESC';

		$strSql .= ' ORDER BY ' . addslashes( $strOrderBy ) . ' ' . addslashes( $strSortDirection );

		if( false == is_null( $objApplicationsFilter->getPageNo() ) && false == is_null( $objApplicationsFilter->getCountPerPage() ) ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		$strSql .= ' ) AS ca
					LEFT JOIN unit_spaces us ON ( us.id = ca.unit_space_id AND us.cid = ca.cid ' . $strCheckDeletedUnitSpacesSql . ' )
					LEFT JOIN property_units pu ON ( pu.id = ca.property_unit_id AND pu.cid = ca.cid ' . $strCheckDeletedUnitsSql . ' ) ';

		$strSql .= ( -1 == $intJoinLeases ) ? ' LEFT JOIN leases l ON ( ca.lease_id = l.id AND ca.cid = l.cid )
												LEFT JOIN lease_intervals li ON ( li.lease_id = l.id AND li.id = l.active_lease_interval_id AND li.cid = l.cid ) ' : '';

		$strSql .= ( -1 == $intJoinEvents ) ? $strEventsJoinSql : '';

		$strSql .= ' WHERE ca.cid = ' . ( int ) $intCid;

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationStageStatusWiseOpenApplicationCountByPropertyIdsByLeaseIntervalTypeIdsByLeasingAgentIdsByCid( $arrintPropertyIds, $arrintLeaseIntervalTypeIds, $arrintCompanyEmployeeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strCompanyEmployeeSql = ( true == valArr( $arrintCompanyEmployeeIds ) ) ? ' AND leasing_agent_id IN ( ' . implode( ',', $arrintCompanyEmployeeIds ) . ' ) ' : '';

		$strSql = '	SELECT
						ass.id as application_stage_status_id,
						count(ca.id)
					FROM
						cached_applications ca
						JOIN application_stage_statuses ass ON( ca.lease_interval_type_id = ass.lease_interval_type_id AND ca.application_stage_id = ass.application_stage_id AND ca.application_status_id =ass.application_status_id )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ca.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
						AND ass.id NOT IN ( ' . implode( ',', CApplicationStageStatus::$c_arrintNonActiveApplicationStageStatusIds ) . ' ) ' . $strCompanyEmployeeSql . '
					GROUP BY
						ass.id';

		$arrmixValues = fetchData( $strSql, $objDatabase );

		$arrintApplicationStatuesTypesData = [];

		if( true == valArr( $arrmixValues ) ) {
			foreach( $arrmixValues as $arrmixValue ) {
				$arrintApplicationStatuesTypesData[$arrmixValue['application_stage_status_id']] = $arrmixValue['count'];
			}
		}

		return $arrintApplicationStatuesTypesData;
	}

	public static function fetchEventStatisticsByPropertyIdsByLeaseIntervalTypeIdsByLeasingAgentIdsByCid( $arrintPropertyIds, $arrintLeaseIntervalTypeIds, $arrintCompanyEmployeeIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strCompanyEmployeeSql = ( true == valArr( $arrintCompanyEmployeeIds ) ) ? ' AND leasing_agent_id IN ( ' . implode( ',', $arrintCompanyEmployeeIds ) . ' ) ' : '';

		$strSql = '	SELECT
						count(id) as total_lead_count,
						sum ( is_lease_approved ) as lease_approved_count,
						sum ( has_site_visit ) as site_visit_count,
						( sum ( time_to_first_contact ) / count(id)) as time_to_first_contact,
						sum ( is_contacted ) as contacted_count
					FROM (
							SELECT
								ca.id,
								EXTRACT( EPOCH FROM COALESCE( first_contact.event_datetime, NOW() ) - ca.application_datetime ) AS time_to_first_contact,
								CASE
									WHEN first_site_visit.lease_interval_id IS NOT NULL
									THEN 1
									ELSE 0
								END AS has_site_visit,
								CASE
									WHEN ' . CApplicationStage::LEASE . '  = ca.application_stage_id AND ' . CApplicationStatus::STARTED . '  = ca.application_status_id
									THEN 1
									ELSE 0
								END AS is_lease_approved,
								CASE
									WHEN first_contact.lease_interval_id IS NOT NULL
									THEN 1
									ELSE 0
								END AS is_contacted
							FROM
								cached_applications ca
								LEFT JOIN (
											SELECT
												e.event_datetime,
												e.lease_interval_id,
												e.cid,
												RANK() OVER( PARTITION BY e.lease_interval_id ORDER BY e.event_datetime ASC )
											FROM
												events e
											WHERE
												e.cid = ' . ( int ) $intCid . '
												AND e.event_datetime > ( CURRENT_DATE - 31 )
												AND e.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
												AND e.lease_interval_id IS NOT NULL
												AND e.event_type_id IN ( ' . implode( ',', [ CEventType::EMAIL_OUTGOING, CEventType::CALL_OUTGOING, CEventType::SMS_OUTGOING, CEventType::ONLINE_CHAT, CEventType::ONSITE_VISIT ] ) . ' )
								) first_contact ON ( first_contact.lease_interval_id = ca.lease_interval_id AND first_contact.cid = ca.cid AND first_contact.rank = 1 )
								LEFT JOIN (
											SELECT
												e.event_datetime,
												e.lease_interval_id,
												e.cid,
												RANK() OVER ( PARTITION BY e.lease_interval_id ORDER BY e.event_datetime ASC )
											FROM
												events e
											WHERE
												e.cid = ' . ( int ) $intCid . '
												AND e.event_datetime > ( CURRENT_DATE - 31 )
												AND e.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
												AND e.lease_interval_id IS NOT NULL
												AND e.event_type_id = ' . CEventType::ONSITE_VISIT . '
											) first_site_visit ON ( first_site_visit.lease_interval_id = ca.lease_interval_id AND first_site_visit.cid = ca.cid AND first_site_visit.rank = 1 )
							WHERE
								ca.cid = ' . ( int ) $intCid . '
								AND ca.application_datetime > ( CURRENT_DATE - 31 )
								AND ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
								AND ca.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
							' . $strCompanyEmployeeSql . '
						) as sub';
		$arrmixValues = fetchData( $strSql, $objDatabase );

		$arrmixEventStatisticsData = [];

		$arrmixEventStatisticsData['total_lead_count'] 				= ( true == isset ( $arrmixValues[0]['total_lead_count'] ) ) ? $arrmixValues[0]['total_lead_count'] : 0;
		$arrmixEventStatisticsData['lease_approved_count'] 			= ( true == isset ( $arrmixValues[0]['lease_approved_count'] ) ) ? $arrmixValues[0]['lease_approved_count'] : 0;
		$arrmixEventStatisticsData['site_visit_count'] 				= ( true == isset ( $arrmixValues[0]['site_visit_count'] ) ) ? $arrmixValues[0]['site_visit_count'] : 0;
		$arrmixEventStatisticsData['contacted_count'] 				= ( true == isset ( $arrmixValues[0]['contacted_count'] ) ) ? $arrmixValues[0]['contacted_count'] : 0;

		if( true == is_numeric( $arrmixValues[0]['time_to_first_contact'] ) ) {
			$intDays = 0;
			$intHours = ( $arrmixValues[0]['time_to_first_contact'] / 3600 );
			if( $intHours > 23 ) {
				$intDays = $intHours / 24;
				$intHours = $intHours % 24;
			}

			$strDaysLabel = ( 1 == $intDays ) ? ' Day ' : ' Days ';
			$strHoursLabel = ( 1 == $intHours ) ? ' Hour ' : ' Hours ';

			if( 0 == $intDays ) {
				$arrmixEventStatisticsData['time_to_first_contact'] = number_format( $intHours, 0 ) . 'h';
				$arrmixEventStatisticsData['time_to_first_contact_long'] = number_format( $intHours, 0 ) . $strHoursLabel;
			} else {
				$arrmixEventStatisticsData['time_to_first_contact'] = number_format( $intDays, 0 ) . 'd';
				$arrmixEventStatisticsData['time_to_first_contact_long'] = number_format( $intDays, 0 ) . $strDaysLabel . number_format( $intHours, 0 ) . $strHoursLabel;
			}
		} else {
			$arrmixEventStatisticsData['time_to_first_contact'] = NULL;
			$arrmixEventStatisticsData['time_to_first_contact_long'] = NULL;
		}

		return $arrmixEventStatisticsData;
	}

	public static function fetchApplicationsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == isset( $arrintPropertyIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						DISTINCT ( ca.id ),
						appt.id as applicant_id,
						appt.name_last,
						appt.name_first,
						appt.phone_number,
						ca.property_id,
						ca.property_unit_id,
						ca.originating_lead_source_id,
						ca.converting_lead_source_id,
						ca.leasing_agent_id,
						ca.lease_id,
						ca.lease_interval_id,
						ca.application_stage_id,
						ca.application_status_id,
						ca.lease_interval_type_id,
						ca.guest_remote_primary_key,
						ca.app_remote_primary_key,
						ca.created_on,
						aa.application_step_id,
						aa.application_document_id,
						aa.application_form_id,
						aa.cid
					FROM
						cached_applications ca,
						applicants appt,
						applicant_applications aa
					WHERE
						appt.id = aa.applicant_id
						AND appt.cid = aa.cid
						AND ca.id = aa.application_id
						AND ca.cid = aa.cid
						AND aa.customer_type_id = ' . CCustomerType::PRIMARY . '
						AND ca.cid = ' . ( int ) $intCid . '
						AND ca.lease_interval_type_id is NOT NULL
						AND ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )'
						. $strCheckDeletedAASql;

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByPropertyIdByIdsByCid( $intPropertyId, $arrintApplicationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strSql = '	SELECT
						*
					FROM
						cached_applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND property_id = ' . ( int ) $intPropertyId;

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchFirstPrimaryApplicationByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == isset( $arrintPropertyIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						DISTINCT ( ca.id ), appt.id as applicant_id, appt.name_last, appt.name_first, ca.lease_interval_type_id
					FROM
						cached_applications ca,
						applicants appt,
						applicant_applications aa
					WHERE
						appt.id = aa.applicant_id
						AND appt.cid = aa.cid
						AND ca.id = aa.application_id
						AND ca.cid = aa.cid
						AND aa.customer_type_id = ' . CCustomerType::PRIMARY . '
						AND ca.cid = ' . ( int ) $intCid . '
						AND ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						' . $strCheckDeletedAASql . '
					ORDER BY ca.id DESC limit 1';

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						ca.id,ca.created_on,ca.lease_interval_type_id,aa.additional_info, acnt.id as applicant_id, ca.lease_id, ca.primary_applicant_id
					FROM
						applicants acnt
						JOIN applicant_applications aa ON (acnt.id = aa.applicant_id AND acnt.cid = aa.cid ' . $strCheckDeletedAASql . ' )
						JOIN cached_applications ca ON (aa.application_id = ca.id AND aa.cid = ca.cid )
					WHERE
						acnt.customer_id = ' . ( int ) $intCustomerId . '
						AND acnt.cid = ' . ( int ) $intCid . '
					ORDER BY aa.created_on DESC';

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchLeaseAgreementIssuedApplicationByCustomerIdByPropertyIdsByCid( $intCustomerId, $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						ca.*
					FROM
						cached_applications ca
						JOIN applicant_applications aa ON ( ca.id = aa.application_id AND ca.cid = aa.cid ' . $strCheckDeletedAASql . ' )
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid )
						JOIN file_associations fa ON( fa.application_id = ca.id AND fa.applicant_id = appt.id AND fa.cid = ca.cid )
						JOIN files f ON ( f.id = fa.file_id AND fa.cid = f.cid )
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.system_code = \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\' AND fa.cid = ft.cid )
						JOIN lease_intervals li ON( ca.cid = li.cid AND ca.lease_id = li.lease_id AND li.id = ca.lease_interval_id AND li.lease_status_type_id <> ' . CLeaseStatusType::PAST . ' )
					WHERE
						aa.lease_generated_on IS NOT NULL
						AND fa.file_signed_on IS NULL
						AND fa.deleted_on IS NULL
						AND fa.deleted_by IS NULL
						AND appt.customer_id = ' . ( int ) $intCustomerId . '
						AND appt.cid = ' . ( int ) $intCid . '
						AND ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ca.lease_interval_type_id IN ( ' . CLeaseIntervalType::RENEWAL . ',' . CLeaseIntervalType::LEASE_MODIFICATION . ',' . CLeaseIntervalType::TRANSFER . ',' . CLeaseIntervalType::APPLICATION . ' )

						AND CASE WHEN ca.application_stage_id = ' . CApplicationStage::LEASE . ' AND ca.application_status_id = ' . CApplicationStatus::APPROVED . ' THEN
							ca.lease_approved_on > TIMESTAMP \'2014-05-07\'
						ELSE
							true
						END
							ORDER BY ca.created_on DESC
					LIMIT 1';

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchSimpleLeaseAgreementIssuedApplicationsByCustomerIdByPropertyIdsByCid( $intCustomerId, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = '	SELECT
						ca.id,
						ca.cid,
						ca.lease_interval_type_id,
						ca.application_datetime
					FROM
						cached_applications ca
						JOIN applicant_applications aa ON ( ca.cid = aa.cid AND ca.id = aa.application_id AND aa.deleted_on IS NULL )
						JOIN applicants appt ON ( appt.cid = aa.cid AND appt.id = aa.applicant_id )
						JOIN file_associations fa ON(  fa.cid = ca.cid AND fa.application_id = ca.id AND fa.applicant_id = appt.id )
						JOIN files f ON ( fa.cid = f.cid AND f.id = fa.file_id )
						JOIN file_types ft ON ( fa.cid = ft.cid AND ft.id = f.file_type_id AND ft.system_code = \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\' )
						JOIN lease_intervals li ON( ca.cid = li.cid AND ca.lease_id = li.lease_id AND li.id = ca.lease_interval_id )
						JOIN file_signatures fs ON( fa.cid = fs.cid AND fa.id = fs.file_association_id )
					WHERE
						appt.cid = ' . ( int ) $intCid . '
						AND f.file_upload_date IS NOT NULL
						AND aa.lease_generated_on IS NOT NULL
						AND fa.file_signed_on IS NULL
						AND fa.deleted_on IS NULL
						AND fa.deleted_by IS NULL
						AND appt.customer_id = ' . ( int ) $intCustomerId . '
						AND ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ca.lease_interval_type_id IN ( ' . implode( ',', CLeaseIntervalType::$c_arrintLeaseSigningLeaseIntervalTypes ) . ' )
						AND li.lease_status_type_id IN ( ' . implode( ',', CLeaseStatusType::$c_arrintLeaseSigningLeaseStatusTypes ) . ' )
						AND CASE WHEN ca.application_stage_id = ' . CApplicationStage::LEASE . ' AND ca.application_status_id = ' . CApplicationStatus::APPROVED . ' THEN
							ca.lease_approved_on > TIMESTAMP \'2014-05-07\'
						ELSE
							TRUE
						END
						ORDER BY ca.created_on DESC';

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchUnexportedApplicationsByInternetListingServiceIdByCid( $intInternetListingServiceId, $intCid, $objDatabase, $intHistoricalDays = NULL, $boolIncludeDeletedAA = false ) {

		if ( false == is_numeric( $intInternetListingServiceId ) ) return NULL;
		if ( false == is_numeric( $intCid ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						DISTINCT ON (ca.id)
						ca.*,
						aa.application_step_id
					FROM
						cached_applications ca
						LEFT OUTER JOIN export_records er ON ( ca.id::int = er.reference_number::int AND ca.cid = er.cid AND er.table_key = \'applications\' AND er.export_type_key = \'' . ( string ) $intInternetListingServiceId . '\' )
						LEFT OUTER JOIN applicant_applications aa ON ( ca.id = aa.application_id AND ca.cid = aa.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . $strCheckDeletedAASql . ' )
					WHERE
						er.id IS NULL
						AND ca.internet_listing_service_id::int = ' . ( int ) $intInternetListingServiceId . '::int
						AND ca.cid::int = ' . ( int ) $intCid . '::int';

		if( false == is_null( $intHistoricalDays ) && true == is_numeric( $intHistoricalDays ) ) {
			$strSql .= ' AND to_char( COALESCE( ca.application_datetime, ca.created_on ), \'MM/DD/YYYY\' )::date = ( CURRENT_DATE - integer \'' . ( int ) $intHistoricalDays . '\' )::date ';
		}
		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchUnexportedApplicationsByInternetListingServiceIdByApplicationDatetimeByCid( $intInternetListingServiceId, $strApplicationDatetime = NULL, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		if ( false == is_numeric( $intInternetListingServiceId ) ) return NULL;
		if ( false == is_numeric( $intCid ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						DISTINCT ON (ca.id)
						ca.*,
						aa.application_step_id
					FROM
						cached_applications ca
						LEFT OUTER JOIN export_records er ON ( ca.id::int = er.reference_number::int AND ca.cid::int = er.cid AND er.table_key = \'applications\' AND er.export_type_key = \'' . ( string ) $intInternetListingServiceId . '\' )
						LEFT OUTER JOIN applicant_applications aa ON ( ca.id = aa.application_id AND ca.cid::int = aa.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . $strCheckDeletedAASql . ' )
					WHERE
						er.id IS NULL
						AND ca.internet_listing_service_id::int = ' . ( int ) $intInternetListingServiceId . '::int
						AND ca.cid::int = ' . ( int ) $intCid . '::int';
					if( false == is_null( $strApplicationDatetime ) ) {
							$strSql .= ' AND to_char( COALESCE( ca.application_datetime, ca.created_on ), \'MM/DD/YYYY\' )::date = \'' . $strApplicationDatetime . '\'::date ';
					}

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchPaginatedApartmentsComApplicationsByCid( $intPageNo, $intPageSize, $objDatabase, $objApplicationsFilter, $boolIncludeDeletedAA = false ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT DISTINCT ON ( ca.created_on, mc.company_name, ca.id )
						ca.*,
						appt.name_last,
						appt.name_first
					FROM
						cached_applications ca,
						applicants appt,
						applicant_applications aa,
						clients mc
					WHERE
						appt.id = aa.applicant_id
						AND appt.cid = aa.cid
						AND ca.id = aa.application_id
						AND ca.cid = aa.cid
						AND aa.customer_type_id = ' . CCustomerType::PRIMARY . '
						AND ca.cid = mc.id
						AND mc.id = ' . ( int ) $objApplicationsFilter->getCid() . ' AND ca.cid = ' . ( int ) $objApplicationsFilter->getCid() . $strCheckDeletedAASql;

		$strSql .= ' ORDER BY
						ca.created_on DESC,
						mc.company_name ASC,
						ca.id ASC

						OFFSET ' . ( int ) $intOffset . '
						LIMIT ' . ( int ) $intLimit;

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchPaginatedApartmentsComApplicationsCountByCid( $objDatabase, $objApplicationsFilter, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						count( apps.id )
					FROM
						applications apps,
						applicants appt,
						applicant_applications aa,
						clients mc
					WHERE
						appt.id = aa.applicant_id
						AND appt.cid = aa.cid
						AND apps.id = aa.application_id
						AND apps.cid = aa.cid
						AND aa.customer_type_id = ' . CCustomerType::PRIMARY . '
						AND apps.cid = mc.id
						AND mc.id = ' . ( int ) $objApplicationsFilter->getCid() . '
						AND apps.cid = ' . ( int ) $objApplicationsFilter->getCid() . $strCheckDeletedAASql;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if ( true == isset ( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchApplicationsByIdsByCid( $arrintApplicationIds, $intCid, $objDatabase, $boolIncludePrimaryApplicantDetails = false, $boolIncludeLeaseDocument = false, $strOrderBy = '', $boolIncludeDeletedAA = false ) {

		$arrintApplicationIds = array_filter( ( array ) $arrintApplicationIds, 'is_numeric' );
		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						ca.*';

		if( true == $boolIncludePrimaryApplicantDetails ) {
			$strSql .= ' , appt.id as applicant_id
						, appt.name_last
						, appt.name_first
						, appt.name_last_matronymic
						, appt.username as email_address
						, appt.phone_number AS phone_number
						, appt.mobile_number AS mobile_number
						, appt.work_number AS work_number ';
		}

		if( true == $boolIncludePrimaryApplicantDetails || true == $boolIncludeLeaseDocument ) {
			$strSql .= ' , aa.id AS applicant_application_id
						, aa.lease_document_id ';
		}

		$strSql .= ' FROM
						cached_applications ca ';

		if( true == $boolIncludePrimaryApplicantDetails || true == $boolIncludeLeaseDocument ) {
			$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

			$strSql .= ' INNER JOIN applicant_applications aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . $strCheckDeletedAASql . ' ) ';
		}

		if( true == $boolIncludePrimaryApplicantDetails ) {
			$strSql .= ' INNER JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid ) ';
		}

		$strSql .= ' WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.id IN ( ' . sqlIntImplode( $arrintApplicationIds ) . ')';

		if( true == $boolIncludePrimaryApplicantDetails ) {
			if( 'name' == $strOrderBy ) {
				$strSql .= ' ORDER BY appt.name_first, appt.name_last';
			} elseif( 'name DESC' == $strOrderBy ) {
				$strSql .= ' ORDER BY name_last || name_first DESC';
			} elseif( 'name ASC' == $strOrderBy ) {
				$strSql .= ' ORDER BY name_last || name_first ASC';
			} elseif( false == empty( $strOrderBy ) ) {
				$strSql .= ' ORDER BY ca. ' . $strOrderBy;
			} else {
				$strSql .= ' ORDER BY ca.id DESC';
			}
		}

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByIntegrationDatabaseIdByPropertyIdsByCid( $intIntegrationDatabaseId, $arrintPropertyIds, $intCid, $objDatabase ) {
		$strSql = '	SELECT
						ca.*
					FROM
						cached_applications ca
						JOIN property_integration_databases pid ON( ca.property_id = pid.property_id AND ca.cid = pid.cid )
					WHERE
						pid.integration_database_id = ' . ( int ) $intIntegrationDatabaseId . '
						AND pid.cid = ' . ( int ) $intCid .
						( ( true == valArr( $arrintPropertyIds ) ) ? ' AND ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' : '' );

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationStatusCountsByPropertyIdsByLeasingAgentIdByCid( $arrintPropertyIds, $arrintCompanyEmployeeIds, $intCid, $objDatabase, $intCurrentEmployeeId = NULL ) {

		if ( false == valArr( $arrintPropertyIds ) ) return 0;

		$arrintLeaseIntervalTypes = [ CLeaseIntervalType::APPLICATION ];

		$strSql = '	SELECT
						ass.id as application_stage_status_id,
						is_lease_partially_generated,
						count(ca.id)
					FROM
						cached_applications ca
						JOIN application_stage_statuses ass ON( ca.lease_interval_type_id = ass.lease_interval_type_id AND ca.application_stage_id = ass.application_stage_id AND ca.application_status_id =ass.application_status_id )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND (ca.application_stage_id,ca.application_status_id) NOT IN ( ' . sqlIntMultiImplode( [ CApplicationStage::APPLICATION => [ CApplicationStatus::CANCELLED ], CApplicationStage::LEASE => [ CApplicationStatus::APPROVED ] ] ) . ' )
						AND ( CASE WHEN ass.id = ' . CApplicationStageStatus::PROSPECT_STAGE3_COMPLETED . ' THEN
									ca.lease_interval_type_id in ( ' . implode( ',', $arrintLeaseIntervalTypes ) . ' )
							ELSE
									ca.lease_interval_type_id <> 7
							END ) ';

					if( false == valArr( $arrintCompanyEmployeeIds ) ) {

						$strSql .= ' AND( CASE WHEN ass.id NOT IN ( ' . CApplicationStageStatus::PROSPECT_STAGE4_COMPLETED . ' )
										THEN leasing_agent_id IS NULL
										ELSE true
									END ) ';

					} elseif( 1 == \Psi\Libraries\UtilFunctions\count( $arrintCompanyEmployeeIds ) && false == is_null( $intCurrentEmployeeId ) && true == array_key_exists( $intCurrentEmployeeId, $arrintCompanyEmployeeIds ) ) {

						$strSql .= ' AND( CASE WHEN ass.id NOT IN ( ' . CApplicationStageStatus::PROSPECT_STAGE4_COMPLETED . ' )
										THEN leasing_agent_id IS NULL OR leasing_agent_id IN ( ' . implode( ',', $arrintCompanyEmployeeIds ) . ' )
										ELSE true
									END ) ';
					} else {

						$strSql .= ' AND ( leasing_agent_id IN ( ' . implode( ',', $arrintCompanyEmployeeIds ) . ' ) OR leasing_agent_id IS NULL )';
					}

		$strSql .= ' GROUP BY
					ass.id, is_lease_partially_generated
				ORDER BY
					ass.id';

		$arrstrOpenApplicationCounts = fetchData( $strSql, $objDatabase );

		$arrstrFormattedOpenApplicationCounts = [];
		if( true == valArr( $arrstrOpenApplicationCounts ) ) {
			foreach( $arrstrOpenApplicationCounts as $arrstrOpenApplicationCount ) {

				if( 1 == $arrstrOpenApplicationCount['is_lease_partially_generated'] ) {
					$arrstrFormattedOpenApplicationCounts[CApplicationStageStatus::PROSPECT_STAGE3_APPROVED]['count'] += $arrstrOpenApplicationCount['count'];
					continue;
				}

				$arrstrFormattedOpenApplicationCounts[$arrstrOpenApplicationCount['application_stage_status_id']] = [ 'count' => $arrstrOpenApplicationCount['count'], 'status' => '' ];
			}
		}

		return $arrstrFormattedOpenApplicationCounts;
	}

	public static function fetchApplicationByPropertyIdByApplicantApplicationIdByCid( $intPropertyId, $intApplicantApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						ca.*
					FROM
						cached_applications ca,
						applicant_applications aa
					WHERE
						aa.id = ' . ( int ) $intApplicantApplicationId . '
						AND aa.cid = ' . ( int ) $intCid . '
						AND ca.id = aa.application_id
						AND ca.cid = aa.cid
						AND ca.property_id = ' . ( int ) $intPropertyId . $strCheckDeletedAASql;

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchSimpleApplicationsByApplicantApplicationIdsByPsProductIdByUserIdsByCids( $arrintApplicantApplicationIds, $intPsProductId, $arrintUserIds, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintApplicantApplicationIds ) || false == valId( $intPsProductId ) || false == valArr( $arrintUserIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						ca.id,
						ca.cid,
						ca.property_id,
						ca.created_by,
						aa.id AS applicant_application_id,
						ca.lease_id,
						ca.lease_generated_on
					FROM
						cached_applications ca
						JOIN applicant_applications aa ON ( ca.id = aa.application_id AND ca.cid = aa.cid )
					WHERE
						aa.id IN ( ' . implode( ',', $arrintApplicantApplicationIds ) . ' )
						AND aa.created_by IN ( ' . implode( ',', $arrintUserIds ) . ' )
						AND ca.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND ca.ps_product_id = ' . ( int ) $intPsProductId . '
						AND aa.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyIdByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						property_id
					FROM
						applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intApplicationId;

		$arrintResponse = fetchData( $strSql, $objDatabase );
		return $arrintResponse[0]['property_id'];
	}

	public static function fetchNonIntegratedApplicationsByPropertyIdsByLeaseIntervalTypeIdByApplicationStageStatusIdsByIntervalByCid( $arrintPropertyIds, $intLeaseIntervalTypeId, $arrintApplicationStageStatusIds, $strInterval, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicationStageStatusIds ) || \Psi\Libraries\UtilFunctions\count( $arrintApplicationStageStatusIds, COUNT_NORMAL ) == \Psi\Libraries\UtilFunctions\count( $arrintApplicationStageStatusIds, COUNT_RECURSIVE ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = '	SELECT
						*
					FROM
						cached_applications
					WHERE
						cid =' . ( int ) $intCid . '
						AND property_id IN (' . implode( ',', $arrintPropertyIds ) . ' )
						And lease_interval_type_id=' . ( int ) $intLeaseIntervalTypeId . '
						AND ( application_stage_id, application_status_id ) IN (' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ')
						AND app_remote_primary_key IS NULL
						AND created_on > ( NOW() - INTERVAL \'' . $strInterval . '\' )';

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchNonIntegratedApplicationsIdsByPropertyIdsByLeaseIntervalTypeIdByApplicationStageStatusIdsByIntervalByCid( $arrintPropertyIds, $intLeaseIntervalTypeId, $arrintApplicationStageStatusIds, $strInterval, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicationStageStatusIds ) || \Psi\Libraries\UtilFunctions\count( $arrintApplicationStageStatusIds, COUNT_NORMAL ) == \Psi\Libraries\UtilFunctions\count( $arrintApplicationStageStatusIds, COUNT_RECURSIVE ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = ' SELECT
						property_unit_id
					FROM
						cached_applications
					WHERE
						cid =' . ( int ) $intCid . '
						AND property_id IN (' . implode( ',', $arrintPropertyIds ) . ' )
						And lease_interval_type_id=' . ( int ) $intLeaseIntervalTypeId . '
						AND ( application_stage_id, application_status_id ) IN (' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ')
						AND app_remote_primary_key IS NULL
						AND created_on > ( NOW() - INTERVAL \'' . $strInterval . '\' )';

		$arrmixApplicationIds = fetchData( $strSql, $objDatabase );

		$arrmixTempApplicationIds = [];

		if( true == valArr( $arrmixApplicationIds ) ) {
			foreach( $arrmixApplicationIds as $arrmixApplicationId ) {
				$arrmixTempApplicationIds[$arrmixApplicationId['property_unit_id']] = $arrmixApplicationId['property_unit_id'];
			}
		}

		return $arrmixTempApplicationIds;
	}

	public static function fetchNonIntegratedApplicationsByPropertyIdByPropertyUnitIdByLeaseIntervalTypeIdByApplicationStageStatusIdsByIntervalByCid( $intPropertyId, $intPropertyUnitId,  $intLeaseIntervalTypeId, $arrintApplicationStageStatusIds, $strInterval, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicationStageStatusIds ) || \Psi\Libraries\UtilFunctions\count( $arrintApplicationStageStatusIds, COUNT_NORMAL ) == \Psi\Libraries\UtilFunctions\count( $arrintApplicationStageStatusIds, COUNT_RECURSIVE ) ) return NULL;

		$strSql = '	SELECT
						*
					FROM
						cached_applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND property_unit_id = ' . ( int ) $intPropertyUnitId . '
						And lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
						AND ( application_stage_id, application_status_id ) IN (' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ')
						AND app_remote_primary_key IS NULL
						AND created_on > ( NOW() - INTERVAL \'' . $strInterval . '\' )';

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strSql = '	SELECT
						*
					FROM
						cached_applications
					WHERE
						id::int IN ( ' . implode( ',', $arrintApplicationIds ) . ')
					AND cid::int = ' . ( int ) $intCid;

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchCustomApplicationsByApplicationIdsByCids( $arrintApplicationIds, $arrintCids, $objDatabase, $boolIsReturnKeyedArray = true ) {
		if( false == valArr( $arrintApplicationIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = '	SELECT
						*
					FROM
						cached_applications
					WHERE
						id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND cid IN ( ' . implode( ',', $arrintCids ) . ' )';

		return self::fetchCachedApplications( $strSql, $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchApplicationByLeadSourceIdByCid( $intLeadSourceId, $intCid, $objClientDatabase ) {
		$strSql = '	SELECT
						id
					FROM
						applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ( originating_lead_source_id = ' . ( int ) $intLeadSourceId . ' OR converting_lead_source_id = ' . ( int ) $intLeadSourceId . ' )
					LIMIT 1';

		$arrmixApplications = fetchData( $strSql, $objClientDatabase );

		return ( 0 == \Psi\Libraries\UtilFunctions\count( $arrmixApplications ) ) ? false : true;
	}

	public static function fetchApplicationsByUsernameByCid( $strUsername, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( true == empty( $strUsername ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						*
					FROM
						applicant_applications aa
						INNER JOIN cached_applications ca ON ( aa.cid = ca.cid AND aa.application_id = ca.id )
					WHERE
						aa.applicant_id = ( SELECT id FROM applicants WHERE lower(username) = \'' . addslashes( trim( \Psi\CStringService::singleton()->strtolower( $strUsername ) ) ) . '\' AND cid = ' . ( int ) $intCid . ' ORDER BY id DESC LIMIT 1 ) AND aa.cid = ' . ( int ) $intCid . $strCheckDeletedAASql;

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchLatestApplicationByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						ca.*
					FROM
						cached_applications ca,
						applicants appt,
						applicant_applications aa
					WHERE
						ca.id = aa.application_id
						AND ca.cid = aa.cid
						AND appt.id = aa.applicant_id
						AND appt.cid = aa.cid
						AND appt.id::int = ' . ( int ) $intApplicantId . '
						AND ca.cid::int = ' . ( int ) $intCid . $strCheckDeletedAASql . '
						ORDER BY id DESC LIMIT 1';

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchLatestApplicationIdByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		$strSql = '	SELECT
						ca.id
					FROM
						cached_applications ca,
						applicants appt,
						applicant_applications aa
					WHERE
						ca.id = aa.application_id
						AND ca.cid = aa.cid
						AND appt.id = aa.applicant_id
						AND appt.cid = aa.cid
						AND appt.id::int = ' . ( int ) $intApplicantId . '
						AND ca.cid::int = ' . ( int ) $intCid . '
						ORDER BY id DESC LIMIT 1';

		$arrmixData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['id'];

		return NULL;
	}

	public static function fetchApplicationsByCallIdsByCid( $arrintCallIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCallIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintCallIds ) ) return NULL;

		$strSql = '	SELECT
						*
					FROM
						cached_applications
					WHERE
						call_id IN ( ' . implode( ',', $arrintCallIds ) . ' )
						AND cid::int = ' . ( int ) $intCid;

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchTotalGuestCardsCountByStatisticsEmailFilterByCid( $objStastisticsEmailFilter, $objClientDatabase ) {

		if( ( false == valArr( $objStastisticsEmailFilter->getPropertyIds() ) && 0 == \Psi\Libraries\UtilFunctions\count( $objStastisticsEmailFilter->getPropertyIds() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getStartDate() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getEndDate() ) ) ) {
			return NULL;
		}

		$arrintPropertyIds = $objStastisticsEmailFilter->getPropertyIds();

		$strSql = '	SELECT
						ca.property_id,
						ca.originating_lead_source_id AS lead_source_id,
						ca.ps_product_id,
						count ( DISTINCT( ca.id )) lead_count
					FROM
						cached_applications ca
					WHERE
						ca.cid = ' . ( int ) $objStastisticsEmailFilter->getCid() . '
					AND
						ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '
					AND
						ca.ps_product_id = ' . CPsProduct::PROSPECT_PORTAL . '
					AND
						ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					AND
						ca.application_datetime::date >= \'' . $objStastisticsEmailFilter->getStartDate() . '\'::date
					AND
						ca.application_datetime::date <= \'' . $objStastisticsEmailFilter->getEndDate() . '\'::date
					GROUP BY
						ca.property_id,
						ca.originating_lead_source_id,
						ca.ps_product_id
					ORDER BY
						ca.property_id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchTotalApplicationsCountByStatisticsEmailFilterByCid( $objStastisticsEmailFilter, $objClientDatabase ) {

		if( ( false == valArr( $objStastisticsEmailFilter->getPropertyIds() ) && 0 == \Psi\Libraries\UtilFunctions\count( $objStastisticsEmailFilter->getPropertyIds() ) || ( 0 == strlen( $objStastisticsEmailFilter->getStartDate() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getEndDate() ) ) ) ) {
			return NULL;
		}

		$arrintPropertyIds = $objStastisticsEmailFilter->getPropertyIds();

		$strSql = '	SELECT
						ca.property_id,
						ca.application_stage_id,
						ca.application_status_id,
						ca.application_completed_on,
						count ( DISTINCT ( ca.id ) ) as applications_count
					FROM
						cached_applications ca
					WHERE
						ca.cid = ' . ( int ) $objStastisticsEmailFilter->getCid() . '
						AND ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ca.lease_interval_type_id = ' . ( int ) $objStastisticsEmailFilter->getLeaseIntervalTypeId() . '
						AND ca.ps_product_id = ' . CPsProduct::PROSPECT_PORTAL . '
						AND ca.application_datetime::date >= \'' . $objStastisticsEmailFilter->getStartDate() . '\'::date
						AND ca.application_datetime::date <= \'' . $objStastisticsEmailFilter->getEndDate() . '\'::date
					GROUP BY
						ca.property_id,
						ca.application_stage_id,
						ca.application_status_id,
						ca.application_completed_on
					ORDER BY
						ca.property_id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchTotalApplicationsCountByStatisticsEmailFilterByPsProductIdsByCid( $objStastisticsEmailFilter, $arrintPsProductIds, $objClientDatabase ) {

		if( ( false == valArr( $objStastisticsEmailFilter->getPropertyIds() ) && 0 == \Psi\Libraries\UtilFunctions\count( $objStastisticsEmailFilter->getPropertyIds() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getStartDate() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getEndDate() ) ) ) {
			return NULL;
		}

		$arrintPropertyIds = $objStastisticsEmailFilter->getPropertyIds();

		$strSql = '	SELECT
						ca.property_id,
						to_char(ca.application_datetime, \'MM/DD/YYYY\') as xaxis,
						ca.lease_interval_type_id,
						count ( DISTINCT ( ca.id ) ) as application_count
					FROM
						cached_applications ca
					WHERE
						ca.cid = ' . ( int ) $objStastisticsEmailFilter->getCid() . ' AND
						ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )	AND
						ca.application_status_id <> ' . CApplicationStatus::CANCELLED . ' AND
						ca.internet_listing_service_id = ' . CInternetListingService::CRAIGS_LIST . ' AND
						ca.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' ) AND
						ca.craigslist_post_id IS NOT NULL AND
						ca.lease_interval_type_id IN ( ' . implode( ',', $objStastisticsEmailFilter->getLeaseIntervalTypeIds() ) . ' ) AND
						ca.application_datetime::date >= \'' . $objStastisticsEmailFilter->getStartDate() . '\'::date AND
						ca.application_datetime::date <= \'' . $objStastisticsEmailFilter->getEndDate() . '\'::date
					GROUP BY
						ca.property_id,
						xaxis,
						ca.lease_interval_type_id
					ORDER BY
						ca.property_id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApplicationsByApplicationIdsByCallFilterByCid( $arrintApplicationIds, $objCallsFilter, $intCid, $objClientDatabase, $boolIncludeDeletedAA = false ) {

		if( false == valArr( $arrintApplicationIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintApplicationIds ) ) return;

		$arrstrWhere 	= [];
		$strGenericData = trim( $objCallsFilter->getGenericData() );

		if( ( false == empty( $strGenericData ) ) && ( 'Find a Call ...' != trim( $objCallsFilter->getGenericData() ) ) ) {
			$arrstrApplicantNames = explode( ' ', trim( $objCallsFilter->getGenericData() ) );

			if( true == valArr( $arrstrApplicantNames ) && 1 < \Psi\Libraries\UtilFunctions\count( $arrstrApplicantNames ) ) {
				foreach( $arrstrApplicantNames as $arrstrApplicantName ) {
					$strName					= addslashes( $arrstrApplicantName );
					if( true == valStr( $strName ) ) {
						$arrstrNameFirstConditions[] 	= ' lower( appt.name_first ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) ';
						$arrstrNameLastConditions[] 	= ' lower( appt.name_last ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) ';
					}
				}

				$arrstrWhere[] = ' ( ( ' . implode( ' OR ', $arrstrNameFirstConditions ) . ' ) AND ( ' . implode( ' OR ', $arrstrNameLastConditions ) . ' ) ) ';
			} else {
				$strName		= array_pop( $arrstrApplicantNames );
				$arrstrWhere[]	= '( lower( appt.name_last ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) OR lower( appt.name_first ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) OR CAST( ca.call_id AS text ) LIKE \'%' . addslashes( $strName ) . '%\' OR lower( cpn.phone_number ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) OR lower( appt.username ) LIKE lower( \'%' . addslashes( $strName ) . '%\' ) ) ';
			}
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						appt.id AS applicant_id, appt.name_last, appt.name_first, cpn.phone_number,
						ca.id as id, ca.property_id, ca.property_unit_id,
				--ca.lead_source_id,
				ca.lease_id, ca.lease_interval_type_id, ca.ps_product_id, ca.call_id,
						aa.id AS applicant_application_id
					FROM
						cached_applications ca
						INNER JOIN applicant_applications aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid ' . $strCheckDeletedAASql . ' )
						INNER JOIN applicants appt ON ( aa.applicant_id = appt.id AND aa.cid = appt.cid )
						LEFT JOIN customer_phone_numbers cpn ON ( cpn.customer_id = appt.customer_id AND cpn.cid = appt.cid )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.id IN (' . implode( ',', $arrintApplicationIds ) . ')
						AND aa.customer_type_id = ' . CCustomerType::PRIMARY . '
						AND cpn.deleted_by IS NULL';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		return self::fetchCachedApplications( $strSql, $objClientDatabase );
	}

	public static function fetchApplicationsByApplicantApplicationIdsByCid( $arrintApplicantApplicationIds, $intCid, $objClientDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrintApplicantApplicationIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintApplicantApplicationIds ) ) return;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						appt.id AS applicant_id, appt.name_last, appt.name_first, cpn.phone_number,
						ca.id as id, ca.property_id, ca.property_unit_id,
				--ca.lead_source_id,
				ca.lease_id,	ca.lease_interval_type_id, ca.ps_product_id, ca.call_id,
						aa.id AS applicant_application_id
					FROM
						cached_applications ca
						INNER JOIN applicant_applications aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid ' . $strCheckDeletedAASql . ' )
						INNER JOIN applicants appt ON ( aa.applicant_id = appt.id AND aa.cid = appt.cid )
						LEFT JOIN customer_phone_numbers cpn ON ( cpn.customer_id = appt.customer_id AND cpn.is_primary = true )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND aa.id IN (' . implode( ',', $arrintApplicantApplicationIds ) . ')
						AND aa.customer_type_id = ' . CCustomerType::PRIMARY;

		return self::fetchCachedApplications( $strSql, $objClientDatabase );
	}

	public static function fetchApplicationsByCustomerIdByLeaseIdsByCid( $intCustomerId, $arrintLeaseIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrintLeaseIds ) ) return false;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						ca.*
					FROM
						cached_applications ca
						JOIN applicant_applications aa ON ( ca.id = aa.application_id AND ca.cid = aa.cid ' . $strCheckDeletedAASql . ' )
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid AND appt.customer_id = ' . ( int ) $intCustomerId . ' AND appt.cid = ' . ( int ) $intCid . ' )
					WHERE
						ca.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND ( ca.lease_interval_type_id, ca.application_stage_id, ca.application_status_id ) NOT IN ( ( ' . CLeaseIntervalType::TRANSFER . ' ,' . CApplicationStage::APPLICATION . ' , ' . CApplicationStatus::STARTED . ' ) )
						AND ca.cid = ' . ( int ) $intCid;

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsIdsByCustomerIdByLeaseIdsByCid( $intCustomerId, $arrintLeaseIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintLeaseIds ) ) return false;

		$arrmixUnusedParameters = [ $intCustomerId ];

		$strSql = '	SELECT
						id
					FROM
						cached_applications
					WHERE
						lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		$arrintApplicationIds = ( array ) fetchData( $strSql, $objDatabase );
		$arrintNewApplicationIds = [];
		foreach( $arrintApplicationIds as $arrintApplicationId ) {
			$arrintNewApplicationIds[] = $arrintApplicationId['id'];
		}

		return $arrintNewApplicationIds;
	}

	public static function fetchApplicationsByIntegrationDatabaseIdByLeaseRemotePrimaryKeysByLeaseIntervalTypeIdsByLeaseStatusTypeIdsByCid( $intIntegrationDatabaseId, $arrstrLeaseRemotePrimaryKeys, $intCid, $arrintLeaseIntervalTypeIds, $arrintLeaseStatusTypeIds, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrstrLeaseRemotePrimaryKeys ) || false == valArr( $arrintLeaseIntervalTypeIds ) || false == valArr( $arrintLeaseStatusTypeIds ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						ca.*,
						aa.offer_sent_on,
						l.remote_primary_key
					FROM
						cached_applications ca
						JOIN applicant_applications aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' )
						JOIN integration_databases id ON ( ca.cid = id.cid AND id.id = ' . ( int ) $intIntegrationDatabaseId . ' )
						JOIN leases l ON( l.id = ca.lease_id AND l.cid = ca.cid AND l.remote_primary_key IN( \'' . implode( "','", $arrstrLeaseRemotePrimaryKeys ) . '\' ) )
						JOIN lease_intervals li ON( li.id = ca.lease_interval_id and li.cid = ca.cid AND li.lease_status_type_id IN( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' ) AND li.lease_interval_type_id IN( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' ) )
					WHERE
						ca.cid = ' . ( int ) $intCid
						. $strCheckDeletedAASql;

		return CCachedApplications::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function loadScheduledEmailFilterSearchSql( $objScheduledEmailFilter, $arrintPropertyIds ) {

		if( false == valObj( $objScheduledEmailFilter, 'CScheduledEmailFilter' ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$arrstrTableJoins = [
			'leases'						=> 'LEFT JOIN leases AS l ON ( l.id = ca.lease_id AND l.cid = ca.cid AND l.property_id = ca.property_id )',
			'application_stage_statuses'	=> 'JOIN application_stage_statuses ass ON ( ass.lease_interval_type_id = ca.lease_interval_type_id AND ass.application_stage_id = ca.application_stage_id AND ass.application_status_id = ca.application_status_id )'

		];

		$arrmixSqlFilters 	= [
			'select'	=> [],
			'join' 		=> [],
			'where' 	=> []
		];

		$strSqlCondition = '';
		$strProperties = trim( $objScheduledEmailFilter->getProperties() );

		if( true == valArr( $arrintPropertyIds ) ) {
			$arrmixSqlFilters['where'][] = 'ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		} elseif( true == valStr( $strProperties ) ) {
			$arrmixSqlFilters['where'][] = 'ca.property_id IN ( ' . trim( $objScheduledEmailFilter->getProperties(), ',' ) . ' )';
		}

		$strStatusTypes = trim( $objScheduledEmailFilter->getStatusTypes() );
		if( true == valStr( $strStatusTypes ) && 99 != ( int ) $strStatusTypes ) {
			$arrmixSqlFilters['where'][]							= 'ass.id IN ( ' . $objScheduledEmailFilter->getStatusTypes() . ' ) ';
			$arrmixSqlFilters['join']['application_stage_statuses']	= $arrstrTableJoins['application_stage_statuses'];
		} elseif( 99 != ( int ) $strStatusTypes ) {
			$arrmixSqlFilters['where'][]							= 'ass.id IN ( ' . implode( ',', $objScheduledEmailFilter->getAllActiveStatusTypeIds() ) . ' )';
			$arrmixSqlFilters['join']['application_stage_statuses']	= $arrstrTableJoins['application_stage_statuses'];
		} elseif( 99 == ( int ) $strStatusTypes ) {
			$strSql = 'SELECT id FROM application_stage_statuses WHERE application_stage_id IN ( ' . CApplicationStage::LEASE . ' ) AND application_status_id IN ( ' . CApplicationStatus::APPROVED . ' ) AND lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION;
			$arrmixSqlFilters['where'][]							= 'ass.id NOT IN ( ' . $strSql . ' )';
			$arrmixSqlFilters['join']['application_stage_statuses']	= $arrstrTableJoins['application_stage_statuses'];
		}

		// Building filter
		$strBuildings = trim( $objScheduledEmailFilter->getPropertyBuildings() );
		if( true == valStr( $strBuildings ) ) {
			$arrmixSqlFilters['select'][] = 'pb.building_name';
			$arrmixSqlFilters['where'][] = 'pb.id IN ( ' . $objScheduledEmailFilter->getPropertyBuildings() . ' ) ';
			$arrmixSqlFilters['join']['property_buildings'] = $arrstrTableJoins['property_buildings'];
		}

		$strLeasingAgents = trim( $objScheduledEmailFilter->getLeasingAgents() );
		if( true == valStr( $strLeasingAgents ) ) {
			$arrmixSqlFilters['where'][] = 'ca.leasing_agent_id IN ( ' . $objScheduledEmailFilter->getLeasingAgents() . ' ) ';
		}

		// floor plan filter
		$strPropertyFloorplans = trim( $objScheduledEmailFilter->getPropertyFloorplans() );
		if( true == valStr( $strPropertyFloorplans ) ) {
			$arrmixSqlFilters['where'][] = 'ca.property_floorplan_id IN ( ' . $objScheduledEmailFilter->getPropertyFloorplans() . ' ) ';
		}

		// floor filter
		$strPropertyFloors = trim( $objScheduledEmailFilter->getPropertyFloors() );
		if( true == valStr( $strPropertyFloors ) ) {
			$arrmixSqlFilters['where'][] = 'pu.property_floor_id IN ( ' . $strPropertyFloors . ' ) ';
		}

		$strPetTypes = trim( $objScheduledEmailFilter->getPetTypes() );
		if( true == valStr( $strPetTypes ) ) {
			$arrmixSqlFilters['where'][] = 'aap.pet_type_id IN ( ' . $objScheduledEmailFilter->getPetTypes() . ' )';
		}

		$strLeaseIntervalTypes = trim( $objScheduledEmailFilter->getLeaseIntervalTypes() );
		if( true == valStr( $strLeaseIntervalTypes ) ) {
			$arrmixLeaseIntervalTypes = explode( ',', $strLeaseIntervalTypes );

			$arrmixAllLeaseIntervalTypes = [];

			foreach( $arrmixLeaseIntervalTypes as &$arrmixLeaseIntervalType ) {
				if( CLeaseIntervalType::RENEWAL == $arrmixLeaseIntervalType || CLeaseIntervalType::LEASE_MODIFICATION == $arrmixLeaseIntervalType ) {
					continue;
				} else {
					$arrmixAllLeaseIntervalTypes[] = $arrmixLeaseIntervalType;
				}
			}

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrmixAllLeaseIntervalTypes ) ) {
				$arrmixSqlFilters['where'][] = 'ca.lease_interval_type_id IN ( ' . implode( ',', $arrmixAllLeaseIntervalTypes ) . ' )';
			} else {
				$arrmixSqlFilters['where'][] = 'ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' ';
			}
		} else {
			$arrmixSqlFilters['where'][] = 'ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' ';
		}

		$strUnitNumber = trim( $objScheduledEmailFilter->getUnitNumber() );

		if( true == valStr( $strUnitNumber ) ) {
			$arrstrUnitNumberWhere 	= [];

			if( 3 == substr_count( $strUnitNumber, '-' ) ) {
				$arrstrUnitNumbers = [];
				$strUnitCounter = '';

				$arrstrUnitNumbersplit = array_chunk( explode( '-', $strUnitNumber ), 2 );

				$strFirstMinRange	= trim( $arrstrUnitNumbersplit[0][0] );
				$strFirstMaxRange	= trim( $arrstrUnitNumbersplit[1][0] );
				$strSecondMinRange	= trim( $arrstrUnitNumbersplit[0][1] );
				$strSecondMaxRange	= trim( $arrstrUnitNumbersplit[1][1] );

				if( $strFirstMinRange <= $strFirstMaxRange && $strSecondMinRange <= $strSecondMaxRange ) {
					++$strFirstMaxRange;

					for( $strUnitCounter = $strFirstMinRange; $strUnitCounter !== $strFirstMaxRange; ++$strUnitCounter ) {
						$arrstrFirstSplit[] = $strUnitCounter;
					}

					++$strSecondMaxRange;
					unset( $strUnitCounter );

					for( $strUnitCounter = $strSecondMinRange; $strUnitCounter !== $strSecondMaxRange; ++$strUnitCounter ) {
						$arrstrSecondSplit[] = $strUnitCounter;
					}

					foreach( $arrstrFirstSplit as $strFirstSplit ) {
						foreach( $arrstrSecondSplit as $strSecondSplit ) {
							$arrstrUnitNumbers[] = '\'' . $strFirstSplit . '-' . $strSecondSplit . '\'';
						}
					}

					$arrstrUnitNumberWhere[] = 'pu.unit_number IN ( ' . implode( ',', $arrstrUnitNumbers ) . ' )';
				} else {
					$arrstrUnitNumberWhere[] = 'pu.unit_number = \'#$#$\' ';
				}

			} else {

				$arrmixUnitNumbers 		= str_replace( '\\', '',  $objScheduledEmailFilter->getUnitNumber() );
				$arrmixUnitNumbers		= explode( ',', str_replace( '*', '%', str_replace( '%', '\%', $arrmixUnitNumbers ) ) );

				foreach( $arrmixUnitNumbers as $mixUnitNumber ) {
					$mixUnitNumber 		= trim( $mixUnitNumber );

					if( false !== \Psi\CStringService::singleton()->strpos( $mixUnitNumber, '-' ) ) {
						if( 1 == substr_count( $mixUnitNumber, '-' ) ) {
							list( $strRangeStart, $strRangeEnd ) 	= explode( '-', $mixUnitNumber, 2 );
							$strRangeStart 							= preg_replace( '/[^a-zA-Z0-9\.]+/', '', $strRangeStart );
							$strRangeEnd 							= preg_replace( '/[^a-zA-Z0-9\.]+/', '', $strRangeEnd );
						} else {
							$strRangeStart = '';
							$strRangeEnd = '';
						}

						if( true == valStr( $strRangeStart ) && true == valStr( $strRangeEnd ) ) {
							if( true == is_numeric( $strRangeStart ) && true == is_numeric( $strRangeEnd ) ) {
								$arrstrUnitNumberWhere[] = 'regexp_replace( pu.unit_number, E\'[^0-9.\-]\', \'\', \'g\' ) = pu.unit_number AND CAST ( regexp_replace( pu.unit_number, E\'[^0-9.]\', \'\', \'g\' ) AS numeric ) BETWEEN ' . $strRangeStart . ' AND ' . $strRangeEnd . ' ';
							} else {
								$intMinSearchLength = min( \Psi\CStringService::singleton()->strlen( $strRangeStart ), \Psi\CStringService::singleton()->strlen( $strRangeEnd ) );
								$intMaxSearchLength = max( \Psi\CStringService::singleton()->strlen( $strRangeStart ), \Psi\CStringService::singleton()->strlen( $strRangeEnd ) );

								$arrmixRangeStart = preg_split( '#(?<=\d)(?=[a-z])#i', $strRangeStart );
								$arrmixRangeEnd = preg_split( '#(?<=\d)(?=[a-z])#i', $strRangeEnd );

								if( true == is_numeric( $arrmixRangeStart[0] ) && true == is_numeric( $arrmixRangeEnd[0] ) && true == isset( $arrmixRangeStart[1] ) && true == isset( $arrmixRangeEnd[1] ) ) {
									$arrstrUnitNumberWhere[] = 'char_length( replace( pu.unit_number, \'-\', \'\' ) ) >= ' . ( int ) $intMinSearchLength . ' AND char_length( replace( pu.unit_number, \'-\', \'\' ) ) <= ' . ( int ) $intMaxSearchLength . ' AND replace( pu.unit_number, \'-\', \'\' ) BETWEEN \'' . $arrmixRangeStart[0] . '\' AND \'' . $arrmixRangeEnd[0] . '\' AND replace( pu.unit_number, \'-\', \'\' ) ILIKE COALESCE ( \'%' . $arrmixRangeStart[1] . '%\', \'%' . $arrmixRangeEnd[1] . '%\')';
								} else {
									$arrstrUnitNumberWhere[] = 'char_length( replace( pu.unit_number, \'-\', \'\' ) ) >= ' . ( int ) $intMinSearchLength . ' AND char_length( replace( pu.unit_number, \'-\', \'\' ) ) <= ' . ( int ) $intMaxSearchLength . ' AND replace( pu.unit_number, \'-\', \'\' ) BETWEEN \'' . $strRangeStart . '\' AND \'' . $strRangeEnd . '\'	';
								}
							}
							continue;
						}

						if( true == valStr( $strRangeStart ) ) {
							$arrstrUnitNumberWhere[] = 'replace( pu.unit_number, \'-\', \'\' ) ILIKE \'' . addslashes( $strRangeStart ) . '\' ';
						}

						if( true == valStr( $strRangeEnd ) ) {
							$arrstrUnitNumberWhere[] = 'replace( pu.unit_number, \'-\', \'\' ) ILIKE \'' . addslashes( $strRangeEnd ) . '\' ';
						}

						// if both are blank show no result
						if( false == valStr( $strRangeStart ) && false == valStr( $strRangeEnd ) ) {
							$arrstrUnitNumberWhere[] = 'pu.unit_number = \'#$#$\' AND us.space_number = \'#$#$\' ';
						}
					} else {
						$mixUnitNumber = addslashes( $mixUnitNumber );
						$mixUnitNumber = str_replace( '-', '', $mixUnitNumber );

						if( true == is_numeric( $mixUnitNumber ) ) {
							$mixUnitNumber = preg_replace( '/[^0-9\.]+/', '', $mixUnitNumber );
							$arrstrUnitNumberWhere[] = 'regexp_replace( pu.unit_number, E\'[^0-9.\-]\', \'\', \'g\' ) = pu.unit_number AND CAST ( regexp_replace( pu.unit_number, E\'[^0-9.]\', \'\', \'g\' ) AS numeric ) = ' . $mixUnitNumber;
						} else {
							$arrstrUnitNumberWhere[] = 'replace( pu.unit_number, \'-\', \'\' ) ILIKE \'' . addslashes( $mixUnitNumber ) . '\' ';
						}

					}
				}
			}

			if( true == valArr( $arrstrUnitNumberWhere ) ) {
				$arrmixSqlFilters['where'][] = 'pu.unit_number <> \'\'';
				$arrmixSqlFilters['where'][] = ' ( ' . implode( ' OR ', $arrstrUnitNumberWhere ) . ' ) ';
			}

		}

		$strLeadSources = trim( $objScheduledEmailFilter->getLeadSources() );
		if( true == valStr( $strLeadSources ) ) {
			$arrmixSqlFilters['where'][] = 'ca.originating_lead_source_id IN ( ' . $objScheduledEmailFilter->getLeadSources() . ' )';
		}

		$strProducts = trim( $objScheduledEmailFilter->getProducts() );
		if( true == valStr( $strProducts ) ) {
			$arrmixSqlFilters['where'][] = 'ca.ps_product_id IN ( ' . $objScheduledEmailFilter->getProducts() . ' )';
		}

		if( true == valStr( $objScheduledEmailFilter->getDesiredRentMin() ) ) {
			$arrmixSqlFilters['where'][] = 'ca.desired_rent_min >= ' . ( int ) $objScheduledEmailFilter->getDesiredRentMin();
		}

		if( true == valStr( $objScheduledEmailFilter->getDesiredRentMax() ) ) {
			$arrmixSqlFilters['where'][] = 'ca.desired_rent_max <= ' . ( int ) $objScheduledEmailFilter->getDesiredRentMax();
		}

		if( true == valStr( $objScheduledEmailFilter->getApplicationTypes() ) ) {
			$arrmixSqlFilters['where'][] = 'ca.company_application_id IN ( ' . $objScheduledEmailFilter->getApplicationTypes() . ' )';
		}

		if( true == $objScheduledEmailFilter->getShowBlockedRecipients() && true == valArr( $objScheduledEmailFilter->getBlockedRecipientIds() ) ) {
			$arrmixSqlFilters['where'][] = 'appt.id NOT IN ( ' . implode( ', ', $objScheduledEmailFilter->getBlockedRecipientIds() ) . ' )';
		}

		if( true == valStr( $objScheduledEmailFilter->getCustomerTypes() ) ) {
			$arrmixSqlFilters['where'][] = 'aa.customer_type_id IN ( ' . $objScheduledEmailFilter->getCustomerTypes() . ' ) ';
		} else {
			$arrmixSqlFilters['where'][] = 'aa.customer_type_id NOT IN ( ' . CCustomerType::GUARANTOR . ' )';
		}

		if( true == valStr( $objScheduledEmailFilter->getMoveInDateMin() ) && true == valStr( $objScheduledEmailFilter->getMoveInDateMax() ) ) {
			$intMoveInDateMin	= strtotime( $objScheduledEmailFilter->getMoveInDateMin() );
			$intMoveInDateMax	= strtotime( $objScheduledEmailFilter->getMoveInDateMax() );

			if( $intMoveInDateMin <= $intMoveInDateMax ) {
				$arrmixSqlFilters['where'][] = 'ca.lease_start_date >= \'' . date( 'Y-m-d', strtotime( $objScheduledEmailFilter->getMoveInDateMin() ) ) . ' 00:00:00\'';
				$arrmixSqlFilters['where'][] = 'ca.lease_start_date <= \'' . date( 'Y-m-d', strtotime( $objScheduledEmailFilter->getMoveInDateMax() ) ) . ' 23:59:59\'';
			} else {
				$objScheduledEmailFilter->validate( 'validate_move_in_dates' );
			}
		}

		if( true == valStr( $objScheduledEmailFilter->getDaysCreated() ) && 0 < $objScheduledEmailFilter->getDaysCreated() ) {
			$arrmixSqlFilters['where'][] = ' DATE( ca.application_datetime ) BETWEEN ( DATE ( CURRENT_DATE - INTERVAL \'' . $objScheduledEmailFilter->getDaysCreated() . ' day\' ) ) AND CURRENT_DATE';
		} else {
			if( true == valStr( $objScheduledEmailFilter->getApplicationCreatedOnDateMin() ) && true == valStr( $objScheduledEmailFilter->getApplicationCreatedOnDateMax() ) ) {
				$intApplicationCreatedOnDateMin	= strtotime( $objScheduledEmailFilter->getApplicationCreatedOnDateMin() );
				$intApplicationCreatedOnDateMax	= strtotime( $objScheduledEmailFilter->getApplicationCreatedOnDateMax() );

				if( $intApplicationCreatedOnDateMin <= $intApplicationCreatedOnDateMax ) {
					$arrmixSqlFilters['where'][] = 'ca.application_datetime >= \'' . date( 'Y-m-d', $intApplicationCreatedOnDateMin ) . ' 00:00:00\'';
					$arrmixSqlFilters['where'][] = 'ca.application_datetime <= \'' . date( 'Y-m-d', $intApplicationCreatedOnDateMax ) . ' 23:59:59\'';
				}
			}
		}

		$strDesiredBedrooms = trim( $objScheduledEmailFilter->getDesiredBedrooms() );
		if( true == valStr( $strDesiredBedrooms ) ) {

			$boolGreaterThanSix 	= false;
			$arrintDesiredBedrooms	= [];
			$arrintDesiredBedrooms 	= explode( ',', $objScheduledEmailFilter->getDesiredBedrooms() );

			if( true == valArr( $arrintDesiredBedrooms ) ) {
				$arrintDesiredBedrooms = array_flip( $arrintDesiredBedrooms );
			}

			// if 6 is in DesiredBedrooms array then we need to look for 6 or greater than 6 bedrooms
			if( true == valArr( $arrintDesiredBedrooms ) && true == array_key_exists( '6', $arrintDesiredBedrooms ) ) {

				$boolGreaterThanSix = true;

				unset( $arrintDesiredBedrooms['6'] );
			}

			$strCondition = '( ';

			if( true == valArr( $arrintDesiredBedrooms ) ) {
				$strCondition .= ' pfp.number_of_bedrooms IN ( ' . implode( ',', array_keys( $arrintDesiredBedrooms ) ) . ' ) ';

				if( true == $boolGreaterThanSix ) {
					$strCondition .= ' OR ';
				}
			}

			if( true == $boolGreaterThanSix ) {
				$strCondition .= ' pfp.number_of_bedrooms > 5 ';
			}
			$strCondition .= ')';

			$arrmixSqlFilters['where'][] = $strCondition;
		}

		$strDesiredBathrooms = trim( $objScheduledEmailFilter->getDesiredBathrooms() );
		if( true == valStr( $strDesiredBathrooms ) ) {

			$boolGreaterThanSix 	= false;
			$arrintDesiredBathrooms	= [];
			$arrintDesiredBathrooms	= explode( ',', $objScheduledEmailFilter->getDesiredBathrooms() );

			if( true == valArr( $arrintDesiredBathrooms ) ) {
				$arrintDesiredBathrooms = array_flip( $arrintDesiredBathrooms );
			}

			// if 6 is in DesiredBathrooms array then we need to look for 6 or greater than 6 bathrooms
			if( true == valArr( $arrintDesiredBathrooms ) && true == array_key_exists( '6', $arrintDesiredBathrooms ) ) {

				$boolGreaterThanSix = true;

				unset( $arrintDesiredBathrooms['6'] );
			}

			$strCondition = '( ';

			if( true == valArr( $arrintDesiredBathrooms ) ) {
				$strCondition .= ' pfp.number_of_bathrooms IN ( ' . implode( ',', array_keys( $arrintDesiredBathrooms ) ) . ' ) ';
				if( true == $boolGreaterThanSix ) {
					$strCondition .= ' OR ';
				}
			}

			if( true == $boolGreaterThanSix ) {
				$strCondition .= ' pfp.number_of_bathrooms > 5 ';
			}
			$strCondition .= ')';

			$arrmixSqlFilters['where'][] = $strCondition;
		}

		if( true == $objScheduledEmailFilter->getShowEventRecipients() && false == is_null( $objScheduledEmailFilter->getScheduledEmailEventTypeId() ) ) {

			switch( $objScheduledEmailFilter->getScheduledEmailEventTypeId() ) {
				case CScheduledEmailEventType::BIRTHDAY:
					if( 1 == $objScheduledEmailFilter->getIsAfterEvent() ) {
						$arrmixSqlFilters['where'][] = 'date_part( \'day\', appt.birth_date + INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = date_part(\'day\', CURRENT_DATE )
										AND date_part( \'month\', appt.birth_date + INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = date_part( \'month\', CURRENT_DATE )';
					} else {
						$arrmixSqlFilters['where'][] = 'date_part( \'day\', appt.birth_date - INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = date_part(\'day\', CURRENT_DATE )
										AND date_part( \'month\', appt.birth_date - INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = date_part( \'month\', CURRENT_DATE )';
					}
					break;

				case CScheduledEmailEventType::MOVE_IN_DATE:
					if( 1 == $objScheduledEmailFilter->getIsAfterEvent() ) {
						$arrmixSqlFilters['where'][] = 'ca.lease_start_date IS NOT NULL AND ( DATE_TRUNC( \'day\', ca.lease_start_date ) + INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = DATE_TRUNC( \'day\', NOW() )';
					} else {
						$arrmixSqlFilters['where'][] = 'ca.lease_start_date IS NOT NULL AND ( DATE_TRUNC( \'day\', ca.lease_start_date ) - INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = DATE_TRUNC( \'day\', NOW() )';
					}
					break;

				case CScheduledEmailEventType::LEAD_CREATED_DATE:
					if( 0 < $objScheduledEmailFilter->getDaysFromEvent() ) {
						if( 1 == $objScheduledEmailFilter->getIsAfterEvent() ) {
							$arrmixSqlFilters['where'][] = 'ca.application_datetime IS NOT NULL AND ( DATE_TRUNC( \'day\', ca.application_datetime ) + INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = DATE_TRUNC( \'day\', NOW() )';
						} else {
							$arrmixSqlFilters['where'][] = 'ca.application_datetime IS NOT NULL AND ( DATE_TRUNC( \'day\', ca.application_datetime ) - INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = DATE_TRUNC( \'day\', NOW() )';
						}
					} else {
						if( false == is_null( $objScheduledEmailFilter->getLastSentOn() ) ) {
							$arrmixSqlFilters['where'][] = ' ( ca.application_datetime BETWEEN ( TIMESTAMP \'' . date( 'Y-m-d H:i:s', strtotime( $objScheduledEmailFilter->getLastSentOn() ) ) . '\' ) AND ( TIMESTAMP \'' . date( 'Y-m-d H:i:s', strtotime( $objScheduledEmailFilter->getLastSentOn() ) ) . '\' + INTERVAL \'200 minutes\' ) )';
						} else {
							$arrmixSqlFilters['where'][] = ' ( ca.application_datetime BETWEEN ( TIMESTAMP \'' . date( 'Y-m-d H:i:s', strtotime( $objScheduledEmailFilter->getConfirmedOn() ) ) . '\' ) AND ( TIMESTAMP \'' . date( 'Y-m-d H:i:s', strtotime( $objScheduledEmailFilter->getConfirmedOn() ) ) . '\' + INTERVAL \'200 minutes\' ) )';
						}
					}
					break;

				case CScheduledEmailEventType::APPLICATION_LAST_UPDATE:
					if( 1 == $objScheduledEmailFilter->getIsAfterEvent() ) {
						$arrmixSqlFilters['where'][] = ' ( DATE_TRUNC( \'day\', ca.updated_on ) + INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = DATE_TRUNC( \'day\', NOW() )';
					}
					break;

				case CScheduledEmailEventType::LEASE_LAST_UPDATE:
					if( 1 == $objScheduledEmailFilter->getIsAfterEvent() ) {
						$arrmixSqlFilters['where'][] = 'l.updated_on IS NOT NULL AND ( DATE_TRUNC( \'day\', l.updated_on ) + INTERVAL \'' . $objScheduledEmailFilter->getDaysFromEvent() . ' day\' ) = DATE_TRUNC( \'day\', NOW() )';
						$arrmixSqlFilters['join']['leases'] = $arrstrTableJoins['leases'];
					}
					break;

				default:
					$boolIsValid = false;
					break;
			}
		}

		return $arrmixSqlFilters;
	}

	public static function fetchSimpleRecipientsByPropertyIdsByScheduledEmailFilterByCid( $arrintPropertyIds, $objScheduledEmailFilter, $objDatabase, $strSelectedFields, $boolIncludeDeletedUnits = false, $boolIncludeDeletedFloorPlans = false ) {

		if( false == valObj( $objScheduledEmailFilter, 'CScheduledEmailFilter' ) || false == valArr( $arrintPropertyIds ) || false == valStr( $strSelectedFields ) ) return NULL;

		$strCheckDeletedUnitsSql 	  = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlansSql = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pfp.deleted_on IS NULL' : '';

		$strSelectColumns = '';
		switch( $strSelectedFields ) {
			case 'recipients':
				$strSelectColumns = 'DISTINCT appt.id AS recipients';
				break;

			case 'count':
				$strSelectColumns = 'COUNT ( DISTINCT appt.id )';
				break;

			default:
				return NULL;
		}

		if( false == valStr( $strSelectColumns ) ) return NULL;

		$arrmixSqlFilters = self::loadScheduledEmailFilterSearchSql( $objScheduledEmailFilter, $arrintPropertyIds );

		$strSql = '	SELECT
						' . $strSelectColumns . '
					FROM
						cached_applications ca
						JOIN applicant_applications AS aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid AND aa.deleted_on IS NULL )
						JOIN applicants As appt ON ( aa.applicant_id = appt.id AND aa.cid = appt.cid )
						LEFT JOIN applicant_details AS aptd ON ( aptd.applicant_id = appt.id AND aptd.cid = appt.cid )
						LEFT JOIN property_units AS pu ON ( pu.id = ca.property_unit_id AND pu.cid = ca.cid ' . $strCheckDeletedUnitsSql . ' )
						LEFT JOIN company_employees AS ce ON ( ce.id = ca.leasing_agent_id AND ce.cid = ca.cid )
						LEFT JOIN property_buildings pb ON ( pu.cid = pb.cid AND pu.property_building_id = pb.id AND pb.deleted_on IS NULL )
						LEFT JOIN property_floorplans AS pfp ON ( pfp.id = ca.property_floorplan_id AND pfp.cid = ca.cid ' . $strCheckDeletedFloorPlansSql . ' )
						LEFT JOIN scheduled_email_address_filters AS seaf ON ( seaf.applicant_id = appt.id AND seaf.cid = appt.cid AND seaf.cid = ca.cid )
						' . ( ( true == valArr( $arrmixSqlFilters['join'] ) ) ? ' ' . implode( ' ', $arrmixSqlFilters['join'] ) : '' ) . '
					WHERE
						ca.cid = ' . ( int ) $objScheduledEmailFilter->getCid() . '
						AND seaf.applicant_id IS NULL ' . ( ( true == valArr( $arrmixSqlFilters['where'] ) ) ? ' AND ' . implode( ' AND ', $arrmixSqlFilters['where'] ) : '' );

		$arrmixResponseData = ( array ) fetchData( $strSql, $objDatabase );

		switch( $strSelectedFields ) {
			case 'recipients':
				$arrmixAllRecipients = [];
				foreach( $arrmixResponseData as $arrmixReponse ) {
					$arrmixAllRecipients[] = $arrmixReponse['recipients'];
				}
				return $arrmixAllRecipients;

			case 'count':
				if( true == isset( $arrmixResponseData[0]['count'] ) )
					return $arrmixResponseData[0]['count'];

				return 0;

			default:
				return NULL;
		}
	}

	public static function fetchLatestApplicationByLeaseIdByLeaseIntervalTypeIdByCid( $intLeaseId, $intLeaseIntervalTypeId, $intCid, $objDatabase ) {

		$strSql = '	SELECT
						a.*,
						pf.floorplan_name as floorplan_name
					FROM
						applications a
						JOIN lease_intervals li ON ( a.lease_interval_id = li.id AND li.cid = a.cid )
						LEFT JOIN property_floorplans pf ON ( a.cid = pf.cid  AND pf.id = a.property_floorplan_id )
					WHERE
						a.lease_id = ' . ( int ) $intLeaseId . '
						AND li.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
						AND a.cid = ' . ( int ) $intCid . '
						AND pf.cid = ' . ( int ) $intCid . '
					ORDER BY
						a.id DESC
					LIMIT 1 ';

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByPropertyIdByScheduledEmailFilterByCid( $intPropertyId, $objScheduledEmailFilter, $intCid, $objDatabase ) {
		if( false == valObj( $objScheduledEmailFilter, 'CScheduledEmailFilter' ) ) return NULL;

		$arrmixSqlFilters = self::loadScheduledEmailFilterSearchSql( $objScheduledEmailFilter, [ $intPropertyId ] );

		$strSql = 'SELECT
						ca.id,
						ca.lease_id,
						appt.id AS recipient_id,
						aa.application_id,
						appt.name_first,
						appt.name_last
					FROM
						cached_applications ca
						JOIN applicant_applications AS aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid AND aa.deleted_on IS NULL )
						JOIN applicants As appt ON ( aa.applicant_id = appt.id AND aa.cid = appt.cid )
						JOIN properties AS p ON ( p.id = ca.property_id AND p.cid = ca.cid )
						LEFT JOIN applicant_details AS aptd ON ( aptd.applicant_id = appt.id AND aptd.cid = appt.cid )
						LEFT JOIN property_units AS pu ON ( pu.id = ca.property_unit_id AND pu.cid = ca.cid AND pu.deleted_on IS NULL )
						LEFT JOIN company_employees AS ce ON ( ce.id = ca.leasing_agent_id AND ce.cid = ca.cid )
						LEFT JOIN property_buildings pb ON ( pu.cid = pb.cid AND pu.property_building_id = pb.id AND pb.deleted_on IS NULL )
						LEFT JOIN property_floorplans AS pfp ON ( pfp.id = ca.property_floorplan_id AND pfp.cid = ca.cid AND pfp.deleted_on IS NULL )
						LEFT JOIN scheduled_email_address_filters AS seaf ON ( seaf.applicant_id = appt.id AND seaf.cid = appt.cid AND seaf.cid = ca.cid )
						' . ( ( true == valArr( $arrmixSqlFilters['join'] ) ) ? ' ' . implode( ' ', $arrmixSqlFilters['join'] ) : '' ) . '
					WHERE
						seaf.applicant_id IS NULL
						AND ca.cid = ' . ( int ) $intCid . ( ( true == valArr( $arrmixSqlFilters['where'] ) ) ? ' AND ' . implode( ' AND ', $arrmixSqlFilters['where'] ) : '' );

		if( 0 < $objScheduledEmailFilter->getRecipientCount() ) {
			$strSql .= ' LIMIT ' . $objScheduledEmailFilter->getRecipientCount();
		}

		return ( array ) fetchData( $strSql, $objDatabase, false );
	}

	public static function fetchApplicationsByPropertyIdsByScheduledEmailFilterByCid( $arrintPropertyIds, $objScheduledEmailFilter, $intCid, $objDatabase, $boolShowAllRecords = true, $boolIncludeDeletedUnits = false, $boolIncludeDeletedFloorPlans = false ) {
		if( false == valObj( $objScheduledEmailFilter, 'CScheduledEmailFilter' ) ) return NULL;

		$strCheckDeletedUnitsSql 		 = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlansSql    = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pfp.deleted_on IS NULL' : '';

		if( false == is_null( $objScheduledEmailFilter->getPageNo() ) && false == is_null( $objScheduledEmailFilter->getCountPerPage() ) ) {
			$intOffset 	= ( 0 < $objScheduledEmailFilter->getPageNo() ) ? $objScheduledEmailFilter->getCountPerPage() * ( $objScheduledEmailFilter->getPageNo() - 1 ) : 0;
			$intLimit 	= ( int ) $objScheduledEmailFilter->getCountPerPage();
		}

		$arrmixSqlFilters = self::loadScheduledEmailFilterSearchSql( $objScheduledEmailFilter, $arrintPropertyIds );

		$strSql = 'SELECT
						DISTINCT ON ( appt.id )
						ca.*,
						appt.id AS applicant_id,
						appt.name_last,
						appt.name_first,
						appt.username as email_address,
						appt.phone_number AS phone_number,
						appt.mobile_number AS mobile_number,
						appt.work_number AS work_number,
						p.property_name,
						aptd.primary_phone_number_type_id,
						ca.application_stage_id,
						ca.application_status_id,
						ca.originating_lead_source_id,
						ca.property_id,
						ca.application_datetime as created_on,
						ca.lease_start_date,
						ce.name_first AS leasing_agent_first_name,
						ce.name_last AS leasing_agent_last_name,
						pfp.floorplan_name,
						pb.building_name ' . ( ( true == valArr( $arrmixSqlFilters['select'] ) ) ? ', ' . implode( ',', $arrmixSqlFilters['select'] ) : '' ) . '
					FROM
						cached_applications ca
						JOIN applicant_applications AS aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid AND aa.deleted_on IS NULL )
						JOIN applicants As appt ON ( aa.applicant_id = appt.id AND aa.cid = appt.cid )
						JOIN properties AS p ON ( p.id = ca.property_id AND p.cid = ca.cid )
						LEFT JOIN applicant_details AS aptd ON ( aptd.applicant_id = appt.id AND aptd.cid = appt.cid )
						LEFT JOIN property_units AS pu ON ( pu.id = ca.property_unit_id AND pu.cid = ca.cid ' . $strCheckDeletedUnitsSql . ' )
						LEFT JOIN company_employees AS ce ON ( ce.id = ca.leasing_agent_id AND ce.cid = ca.cid )
						LEFT JOIN property_buildings pb ON ( pu.cid = pb.cid AND pu.property_building_id = pb.id AND pb.deleted_on IS NULL )
						LEFT JOIN property_floorplans AS pfp ON ( pfp.id = ca.property_floorplan_id AND pfp.cid = ca.cid ' . $strCheckDeletedFloorPlansSql . ' )
						LEFT JOIN scheduled_email_address_filters AS seaf ON ( seaf.applicant_id = appt.id AND seaf.cid = appt.cid AND seaf.cid = ca.cid )
						' . ( ( true == valArr( $arrmixSqlFilters['join'] ) ) ? ' ' . implode( ' ', $arrmixSqlFilters['join'] ) : '' ) . '
					WHERE
						seaf.applicant_id IS NULL
						AND ca.cid = ' . ( int ) $intCid . ( ( true == valArr( $arrmixSqlFilters['where'] ) ) ? ' AND ' . implode( ' AND ', $arrmixSqlFilters['where'] ) : '' );

		$arrstrOrderBy = [
			'lead_id'					=> 'appt.id',
			'customer_name_last'		=> 'appt.name_last',
			'customer_email_address'	=> 'appt.username',
			'status'					=> 'ca.application_status_id',
			'property'					=> 'ca.property_id',
			'property_name'				=> 'ca.property_name',
			'source'					=> 'ca.originating_lead_source_id',
			'created_on'				=> 'ca.created_on'
		];

		$strSortBy			= ( true == isset( $arrstrOrderBy[$objScheduledEmailFilter->getSortBy()] ) ? $arrstrOrderBy[$objScheduledEmailFilter->getSortBy()] : 'appt.id' );
		$strSortDirection	= ( false == is_null( $objScheduledEmailFilter->getSortDirection() ) ) ? $objScheduledEmailFilter->getSortDirection() : 'DESC';

		if( false == is_null( $strSortBy ) && false == is_null( $strSortDirection ) ) {
			$strSql .= ' ORDER BY ' . addslashes( $strSortBy ) . ' ' . addslashes( $strSortDirection );
		}

		if( false == $boolShowAllRecords ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		return self::fetchCachedApplications( $strSql, $objDatabase, false );
	}

	public static function fetchCountApplicationsAsResidentByPropertyIdsByScheduledEmailFilterByCid( $arrintPropertyIds, $objScheduledEmailFilter, $intCid, $objDatabase, $boolIncludeDeletedUnits = false, $boolIncludeDeletedFloorPlans = false ) {
		if( false == valObj( $objScheduledEmailFilter, 'CScheduledEmailFilter' ) ) return NULL;

		$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlansSql = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pf.deleted_on IS NULL' : '';

		$arrmixSqlFilters = self::loadScheduledEmailFilterSearchSql( $objScheduledEmailFilter, $arrintPropertyIds );

		$strSql = 'SELECT
						COUNT( DISTINCT( appt.username ) ) as count
					FROM
						cached_applications ca
						JOIN applicant_applications AS aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid AND aa.deleted_on IS NULL )
						JOIN applicants As appt ON ( aa.applicant_id = appt.id AND aa.cid = appt.cid )
						JOIN properties AS p ON ( p.id = ca.property_id AND p.cid = ca.cid )
						LEFT JOIN applicant_details AS aptd ON ( aptd.applicant_id = appt.id AND aptd.cid = appt.cid )
						LEFT JOIN property_units AS pu ON ( pu.id = ca.property_unit_id AND pu.cid = ca.cid ' . $strCheckDeletedUnitsSql . ' )
						LEFT JOIN company_employees AS ce ON ( ce.id = ca.leasing_agent_id AND ce.cid = ca.cid )
						LEFT JOIN property_buildings pb ON ( pu.cid = pb.cid AND pu.property_building_id = pb.id AND pb.deleted_on IS NULL )
						LEFT JOIN property_floorplans AS pfp ON ( pfp.id = ca.property_floorplan_id AND pfp.cid = ca.cid ' . $strCheckDeletedFloorPlansSql . ' )
						LEFT JOIN scheduled_email_address_filters AS seaf ON ( seaf.applicant_id = appt.id AND seaf.cid = appt.cid AND seaf.cid = ca.cid )
						JOIN customers AS c ON ( c.email_address = appt.username AND c.cid = appt.cid )
						JOIN customer_portal_settings AS cps ON ( cps.username = appt.username AND appt.cid = cps.cid )
						JOIN lease_customers lc ON ( c.id = lc.customer_id AND c.cid = lc.cid )
						' . ( ( true == valArr( $arrmixSqlFilters['join'] ) ) ? ' ' . implode( ' ', $arrmixSqlFilters['join'] ) : '' ) . '
					WHERE
						seaf.applicant_id IS NULL
						AND appt.username IS NOT NULL
						AND lc.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND lc.lease_status_type_id IN ( ' . CLeaseStatusType::CURRENT . ' , ' . CLeaseStatusType::NOTICE . ' )
						AND ca.cid = ' . ( int ) $intCid . ( ( true == valArr( $arrmixSqlFilters['where'] ) ) ? ' AND ' . implode( ' AND ', $arrmixSqlFilters['where'] ) : '' );

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;
	}

	public static function fetchApplicationByPropertyIdByIdByCid( $intPropertyId, $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM cached_applications WHERE property_id = ' . ( int ) $intPropertyId . ' AND id = ' . ( int ) $intApplicationId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByPropertyIdByLeaseIdsByLeaseIntervalTypeIdByCid( $intPropertyId, $arrintLeaseIds, $intLeaseIntervalTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintLeaseIds ) ) return NULL;

		$strSql = ' SELECT * FROM cached_applications WHERE property_id = ' . ( int ) $intPropertyId . ' AND lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' ) AND lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . ' AND cid = ' . ( int ) $intCid;

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByPropertyIdByLeaseIdsByLeaseIntervalTypeIdsByCid( $intPropertyId, $arrintLeaseIds, $arrintLeaseIntervalTypeIds, $intCid, $objDatabase ) {
		if( false == valStr( $intPropertyId ) || false == valArr( $arrintLeaseIds ) || false == valArr( $arrintLeaseIntervalTypeIds ) ) return NULL;

		$strSql = ' SELECT * FROM cached_applications WHERE property_id = ' . ( int ) $intPropertyId . ' AND lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' ) AND lease_interval_type_id IN( ' . implode( ', ', $arrintLeaseIntervalTypeIds ) . ' ) AND cid = ' . ( int ) $intCid;

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationByGuestRemotePrimaryKeyByPropertyIdByByCid( $strRemotePrimaryKey, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valStr( $intPropertyId ) || false == valStr( $strRemotePrimaryKey ) ) return NULL;

		$strSql = 'SELECT * FROM cached_applications WHERE lease_id IS NULL AND guest_remote_primary_key = \'' . $strRemotePrimaryKey . '\' AND property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid;

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationByEventTypeIdByDataReferenceIdByCid( $intEventTypeId, $intDataReferenceId, $intCid, $objDatabase ) {
		$strSql = '	SELECT
						ca.*
					FROM
						cached_applications ca
						JOIN events e ON ( e.cid = ca.cid AND e.lease_interval_id = ca.lease_interval_id AND e.event_type_id = ' . ( int ) $intEventTypeId . ' )
					WHERE
						e.lease_interval_id IS NOT NULL
						AND e.data_reference_id = ' . ( int ) $intDataReferenceId . '
						AND ca.cid = ' . ( int ) $intCid;

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchSimpleApplicationsByUsernameByCid( $strUsername, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql  = 'SELECT
						ap.id as applicant_id,
						ap.name_first,
						ap.name_last,
						aa.application_id,
						ass.stage_name || ass.status_name as application_status ,
						ca.property_id,
						ct.name as customer_type
					FROM
						applicant_applications aa
						JOIN cached_applications ca ON ( aa.cid = ca.cid AND ca.id = aa.application_id )
						JOIN applicants ap ON ( aa.cid = ap.cid AND ap.id = aa.applicant_id )
						JOIN application_stage_statuses ass ON ( ass.lease_interval_type_id = ca.lease_interval_type_id AND ass.application_stage_id = ca.application_stage_id AND ass.application_status_id = ca.application_status_id )
						JOIN customer_types ct ON ( aa.customer_type_id = ct.id )
					WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '
						AND ass.id <> ' . CApplicationStageStatus::PROSPECT_STAGE4_APPROVED . '
						AND lower( ap.username ) LIKE \'' . addslashes( trim( \Psi\CStringService::singleton()->strtolower( $strUsername ) ) ) . '%\'
						AND aa.customer_type_id IN ( ' . implode( ',', CCustomerType::$c_arrintApplicationRequiredCustomerTypeIds ) . ' )' . $strCheckDeletedAASql . ' LIMIT 2';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByApplicationFilterByCid( $objApplicationFilter, $intCid, $objDatabase, $boolIsReturnId = false, $boolIncludeDeletedAA = false, $boolIncludeDeletedUnits = false ) {

		$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';

		$intOffset 				= NULL;
		$intMaxSelectionLimit 	= 1000;
		$intLimit  				= NULL;

		if( false == is_null( $objApplicationFilter->getPageNo() ) && false == is_null( $objApplicationFilter->getCountPerPage() ) ) {
			$intOffset 	= ( 0 < $objApplicationFilter->getPageNo() ) ? $objApplicationFilter->getCountPerPage() * ( $objApplicationFilter->getPageNo() - 1 ) : 0;
			$intLimit 	= $objApplicationFilter->getCountPerPage();
		}
		$strApplicationStageStatuses = trim( $objApplicationFilter->getApplicationStageStatuses() );
		if( false == empty( $strApplicationStageStatuses ) ) {
			$arrstrWhere[] = 'ass.id IN ( ' . $strApplicationStageStatuses . ' ) ';
		}
		$strLeaseIntervalTypes = trim( $objApplicationFilter->getLeaseIntervalTypes() );
		if( false == empty( $strLeaseIntervalTypes ) ) {
			$arrstrWhere[] = 'ca.lease_interval_type_id IN ( ' . $objApplicationFilter->getLeaseIntervalTypes() . ' ) ';
		}
		$strProperties = trim( $objApplicationFilter->getProperties() );
		if( false == empty( $strProperties ) ) {
			$arrstrWhere[] = 'ca.property_id IN ( ' . $objApplicationFilter->getProperties() . ' ) ';
		}

		if( false == is_null( $objApplicationFilter->getCompanyEmployees() ) ) {
			$arrstrWhere[] = ' ( ca.leasing_agent_id IN ( ' . $objApplicationFilter->getCompanyEmployees() . ' ) OR ca.leasing_agent_id IS NULL ) ';
		} else {
			$arrstrWhere[] = ' ( ca.leasing_agent_id IS NULL ) ';
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSelect = 'appt.id AS applicant_id,

						appt.name_last,
						appt.name_first,
						appt.phone_number,
						appt.mobile_number,
						appt.work_number,
						appt.username AS email_address,
						ca.id AS id,
						ca.property_id,
						ca.application_datetime,
						ca.application_stage_id,
						ca.application_status_id,
						ca.lease_id,
						ca.created_on,
						ca.application_completed_on,
						li.lease_end_date,
						pu.unit_number AS unit_number,
						CASE
							WHEN ( lc.lease_status_type_id IN  ( ' . CLeaseStatusType::CURRENT . ' , ' . CLeaseStatusType::NOTICE . '  ) AND li.lease_end_date  <= CURRENT_TIMESTAMP  ) THEN
								1
							ELSE
								li.lease_interval_type_id
						END::integer AS month_to_month';

		if( true == $boolIsReturnId ) {

			$intLimit	= $intMaxSelectionLimit;
			$strSelect	= ' ca.id AS id ';

		}

		$strSql  = 'SELECT
						' . $strSelect . '
					FROM
						cached_applications ca
						JOIN application_stage_statuses ass ON ( ass.application_stage_id = ca.application_stage_id AND ass.application_status_id = ca.application_status_id AND ass.lease_interval_type_id = ca.lease_interval_type_id)
						JOIN properties p ON ( ca.property_id = p.id AND ca.cid = p.cid )
						JOIN applicant_applications aa ON ( aa.application_id = ca.id
															AND aa.customer_type_id = ' . CCustomerType::PRIMARY . '
															AND aa.cid = ca.cid ' . $strCheckDeletedAASql . ' )

						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid AND appt.cid = ca.cid )
						JOIN applicant_details ad ON ( appt.id = ad.applicant_id AND ad.cid = appt.cid )

						LEFT JOIN leases l ON ( ca.lease_id = l.id AND l.cid = ca.cid )
						LEFT JOIN lease_customers lc ON ( l.cid = lc.cid
														AND l.id = lc.lease_id
														AND lc.customer_id = l.primary_customer_id )

						LEFT JOIN lease_intervals li ON ( li.lease_id = l.id AND li.id = l.active_lease_interval_id AND li.cid = l.cid )

						LEFT JOIN property_units pu ON ( ca.property_unit_id = pu.id AND pu.cid = ca.cid ' . $strCheckDeletedUnitsSql . ' )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND li.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED;

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		$arrstrOrderBy = [
			'name_last'			=> 'appt.name_last',
			'property_name'		=> 'p.property_name',
			'month_to_month'	=> 'month_to_month'
		];

		$strOrderBy = ( true == isset( $arrstrOrderBy[$objApplicationFilter->getSortBy()] ) ? $arrstrOrderBy[$objApplicationFilter->getSortBy()] : '' );
		$strSortDirection = ( false == is_null( $objApplicationFilter->getSortDirection() ) ) ? $objApplicationFilter->getSortDirection() : 'ASC';

		if( true == valStr( $strOrderBy ) ) {
			if( 'p.property_name' == $strOrderBy ) {

				$strSql .= ' ORDER BY ' . addslashes( $strOrderBy ) . ' ' . addslashes( $strSortDirection ) . ' ,pu.unit_number ' . addslashes( $strSortDirection );

			} else {
				$strSql .= ' ORDER BY ' . addslashes( $strOrderBy ) . ' ' . addslashes( $strSortDirection );
			}
		}

		if( false == is_null( $intOffset ) && false == is_null( $intLimit ) ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}
		$arrmixApplications = fetchData( $strSql, $objDatabase );
		return $arrmixApplications;

	}

	public static function fetchApplicationsCountByApplicationFilterByCid( $objApplicationFilter, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strApplicationStageStatuses = trim( $objApplicationFilter->getApplicationStageStatuses() );
		if( false == empty( $strApplicationStageStatuses ) ) {
			$arrstrWhere[] = 'ass.id IN ( ' . $strApplicationStageStatuses . ' ) ';
		}
		$strLeaseIntervalTypes = trim( $objApplicationFilter->getLeaseIntervalTypes() );
		if( false == empty( $strLeaseIntervalTypes ) ) {
			$arrstrWhere[] = 'ca.lease_interval_type_id IN ( ' . $objApplicationFilter->getLeaseIntervalTypes() . ' ) ';
		}
		$strProperties = trim( $objApplicationFilter->getProperties() );
		if( false == empty( $strProperties ) ) {
			$arrstrWhere[] = 'ca.property_id IN ( ' . $objApplicationFilter->getProperties() . ' ) ';
		}

		if( false == is_null( $objApplicationFilter->getCompanyEmployees() ) ) {
			$arrstrWhere[] = ' ( ca.leasing_agent_id IN ( ' . $objApplicationFilter->getCompanyEmployees() . ' ) OR ca.leasing_agent_id IS NULL ) ';
		} else {
			$arrstrWhere[] = ' ( ca.leasing_agent_id IS NULL ) ';
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql  = 'SELECT
						COUNT( appt.id )
					FROM
						cached_applications ca
						JOIN application_stage_statuses ass ON ( ass.application_stage_id = ca.application_stage_id AND ass.application_status_id = ca.application_status_id AND ass.lease_interval_type_id = ca.lease_interval_type_id)
						JOIN applicant_applications aa ON (	aa.application_id = ca.id AND aa.cid = ca.cid AND aa.customer_type_id = 1 ' . $strCheckDeletedAASql . ' )
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid )
						JOIN applicant_details ad ON ( appt.id = ad.applicant_id AND appt.cid = ad.cid )
						LEFT JOIN leases l ON ( ca.lease_id = l.id AND ca.cid = l.cid )
					WHERE
						ca.cid = ' . ( int ) $intCid;

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;
	}

	public static function fetchCustomCCPaymentPendingApplications( $objDatabase, $intApplicationId = NULL, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						ca.*
					FROM
						cached_applications ca
						JOIN application_payments ap ON ( ap.application_id = ca.id AND ap.cid = ca.cid )
						JOIN ar_payments arp ON ( arp.id = ap.ar_payment_id AND arp.cid = ap.cid )
						JOIN applicant_applications aa ON ( aa.applicant_id = ap.applicant_id AND aa.application_id = ap.application_id AND aa.cid = ap.cid ' . $strCheckDeletedAASql . ' )
						JOIN clients mc ON ca.cid = mc.id
					WHERE
						arp.payment_type_id IN ( ' . implode( ',', CPaymentType::$c_arrintCreditCardPaymentTypes ) . ' )
						AND aa.completed_on IS NULL
						AND ca.application_completed_on IS NULL
						AND ( (ca.application_stage_id,ca.application_status_id) IN ( ' . sqlIntMultiImplode( [ CApplicationStage::APPLICATION => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED ] ] ) . ') OR ca.requires_capture = TRUE )
						AND to_char( ca.updated_on, \'MM/DD/YYYY\' )::date >= ( CURRENT_DATE - integer \'60\' )::date
						AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT;

		$strSql .= ( 0 < $intApplicationId ) ? ' AND ca.id = ' . ( int ) $intApplicationId : '';

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchCustomMoneygramPaymentPendingApplications( $objDatabase, $intApplicationId = NULL, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						ca.*
					FROM
						cached_applications ca
						JOIN applicant_applications aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid ' . $strCheckDeletedAASql . ' )
						JOIN applicants app ON app.id = aa.applicant_id AND app.cid = ca.cid
						RIGHT JOIN ar_payments arp ON arp.lease_id = ca.lease_id AND arp.customer_id = app.customer_id AND arp.cid = ca.cid
						JOIN clients mc ON ca.cid = mc.id
					WHERE
						arp.payment_type_id = ' . CPaymentType::EMONEY_ORDER . '
						AND arp.payment_status_type_id = ' . CPaymentStatusType::CAPTURED . '
						AND (
							--If application is in Application Approved / Lease Approved status then completed_on would be set
							( aa.completed_on IS NOT NULL AND ca.application_completed_on IS NOT NULL AND (ca.application_stage_id,ca.application_status_id) IN ( ' . sqlIntMultiImplode( [ CApplicationStage::APPLICATION => [ CApplicationStatus::APPROVED ], CApplicationStage::LEASE => [ CApplicationStatus::APPROVED ] ] ) . ' ) )
							OR ( aa.completed_on IS NULL AND ca.application_completed_on IS NULL AND ( ( ca.application_stage_id = ' . CApplicationStage::APPLICATION . ' AND ca.application_status_id = ' . CApplicationStatus::PARTIALLY_COMPLETED . ' ) OR ca.requires_capture = TRUE ) )
						)
						AND to_char( ca.updated_on, \'MM/DD/YYYY\' )::date >= ( CURRENT_DATE - integer \'60\' )::date
						AND mc.company_status_type_id = ' . CCompanyStatusType::CLIENT;

		$strSql .= ( 0 < $intApplicationId ) ? ' AND ca.id = ' . ( int ) $intApplicationId : '';

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchFutureRenewalApplicationByLeaseIdByLeaseEndDateByCid( $intLeaseId, $strLeaseEndDate, $intCid, $objDatabase ) {

		if( false == valStr( $strLeaseEndDate ) ) return NULL;

		$strSql = ' SELECT
						ca.*
					FROM
						cached_applications ca
						JOIN lease_intervals li ON( ca.cid = li.cid AND ca.lease_id = li.lease_id AND li.id = ca.lease_interval_id AND li.lease_status_type_id = ' . CLeaseStatusType::FUTURE . ' )
					WHERE
						ca.lease_id = ' . ( int ) $intLeaseId . '
						AND ca.cid = ' . ( int ) $intCid . '
						AND ca.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::RENEWAL . '
						AND ca.application_stage_id = ' . ( int ) CApplicationStage::LEASE . '
						AND ca.application_status_id = ' . ( int ) CApplicationStatus::APPROVED . '
						AND ca.lease_start_date > \'' . date( 'Y-m-d', strtotime( $strLeaseEndDate ) ) . ' 00:00:00\'
						AND ca.lease_approved_on IS NOT NULL
					LIMIT 1';

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ca.*
					FROM
						cached_applications ca
						JOIN file_associations fa ON( ca.id = fa.application_id AND ca.cid = fa.cid AND fa.cid = ' . ( int ) $intCid . ' AND fa.file_id = ' . ( int ) $intFileId . ' ) LIMIT 1';
		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchLatestApplicationByApplicantIdByPropertyIdsByCid( $intApplicantId, $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = '	SELECT
						ca.*
					FROM cached_applications ca
						INNER JOIN applicant_applications aa ON ( ca.id = aa.application_id AND ca.cid = aa.cid ' . $strCheckDeletedAASql . ' )
					WHERE ca.cid = ' . ( int ) $intCid . '
						AND aa.applicant_id = ' . ( int ) $intApplicantId . '
						AND aa.customer_type_id = ' . CCustomerType::PRIMARY . '
						AND ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					ORDER BY ca.id DESC LIMIT 1';

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchExtendedRenewalOfferCountByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		$strSql = '	SELECT
						COUNT(*)
					FROM
						cached_applications
					WHERE
						lease_id = ' . ( int ) $intLeaseId . '
						AND cid = ' . ( int ) $intCid . '
						AND lease_interval_type_id = ' . ( int ) CLeaseIntervalType::RENEWAL . '
						AND (application_stage_id, application_status_id) NOT IN ( ' . sqlIntMultiImplode( [ CApplicationStage::PRE_APPLICATION => [ CApplicationStatus::ON_HOLD ], CApplicationStage::APPLICATION => [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ], CApplicationStage::LEASE => [ CApplicationStatus::APPROVED, CApplicationStatus::CANCELLED ] ] ) . ')';

		$arrmixQueryResult = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixQueryResult ) ) return $arrmixQueryResult[0]['count'];
		return 0;
	}

	public static function fetchOtherGuestCardCountsByStatisticsEmailFilter( $objStastisticsEmailFilter, $objClientDatabase ) {

		if( ( false == valArr( $objStastisticsEmailFilter->getPropertyIds() ) && 0 == \Psi\Libraries\UtilFunctions\count( $objStastisticsEmailFilter->getPropertyIds() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getStartDate() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getEndDate() ) ) ) {
			return NULL;
		}

		$arrintPropertyIds = $objStastisticsEmailFilter->getPropertyIds();

		$strSql = '	SELECT
						ca.property_id,
						ca.originating_lead_source_id AS lead_source_id,
						ca.ps_product_id,
						count ( DISTINCT( ca.id )) lead_count
					FROM
						cached_applications ca
					WHERE
						ca.cid = ' . ( int ) $objStastisticsEmailFilter->getCid() . '	AND
						ca.application_status_id <> ' . CApplicationStatus::CANCELLED . ' AND
						ca.application_stage_id = ' . CApplicationStage::PRE_APPLICATION . '	AND
						ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '	AND
						ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND
						ca.created_on::date >= \'' . $objStastisticsEmailFilter->getStartDate() . '\'::date AND
						ca.created_on::date <= \'' . $objStastisticsEmailFilter->getEndDate() . '\'::date
					GROUP BY
						ca.property_id,
						ca.originating_lead_source_id,
						ca.ps_product_id
					ORDER BY
						ca.property_id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchInsuranceMissingApplicationsByApplicationFilterByCid( $objApplicationsFilter, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false ) {

		$strCheckDeletedUnitsSql 	  = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		$strCompanyEmployees = trim( $objApplicationsFilter->getCompanyEmployees() );
		if( false == empty( $strCompanyEmployees ) && true == $objApplicationsFilter->getLeasingAgentIsNull() ) {
			$arrstrWhere[] = ' ( ca.leasing_agent_id IN ( ' . $objApplicationsFilter->getCompanyEmployees() . ' ) OR ca.leasing_agent_id IS NULL ) ';
		} elseif( false == empty( $strCompanyEmployees ) ) {
			$arrstrWhere[] = ' ca.leasing_agent_id IN ( ' . $objApplicationsFilter->getCompanyEmployees() . ' ) ';
		} elseif( true == $objApplicationsFilter->getLeasingAgentIsNull() ) {
			$arrstrWhere[] = ' ( ca.leasing_agent_id IS NULL ) ';
		}
		$strProperties = trim( $objApplicationsFilter->getProperties() );
		if( false == empty( $strProperties ) ) {
			$arrstrWhere[] = 'ca.property_id IN ( ' . $objApplicationsFilter->getProperties() . ' ) ';
		}
		$strLeaseIntervalTypes = trim( $objApplicationsFilter->getLeaseIntervalTypes() );
		if( false == empty( $strLeaseIntervalTypes ) ) {
			$arrstrWhere[] = 'ca.lease_interval_type_id IN ( ' . $objApplicationsFilter->getLeaseIntervalTypes() . ' ) ';
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = ' SELECT
						ca.*,
						appt.name_last,
						appt.name_first,
						appt.email_address,
						appt.phone_number,
						p.property_name,
						pu.unit_number,
						us.space_number,
						rip.id as policy_id
					FROM
						cached_applications ca
						JOIN applicant_applications aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid AND aa.customer_type_id = ' . ( int ) CCustomerType::PRIMARY . $strCheckDeletedAASql . ' )
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid )
						LEFT JOIN properties p ON ( ca.property_id = p.id AND ca.cid = p.cid )
						LEFT JOIN property_units pu ON ( ca.property_unit_id = pu.id AND ca.cid = pu.cid ' . $strCheckDeletedUnitsSql . ' )
						LEFT JOIN unit_spaces us ON ( ca.unit_space_id = us.id AND ca.cid = us.cid ' . $strCheckDeletedUnitSpacesSql . ' )
						LEFT JOIN insurance_policy_customers ipc ON( ca.lease_id = ipc.lease_id AND ca.id = ipc.application_id AND ca.cid = ipc.cid )
						LEFT JOIN resident_insurance_policies rip ON( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )';
		$strSql .= ' WHERE
						rip.confirmed_on IS NULL
						AND ca.application_stage_id = ' . ( int ) CApplicationStage::LEASE . '
						AND ca.application_status_id = ' . ( int ) CApplicationStatus::COMPLETED . '
						AND ca.cid = ' . ( int ) $intCid;

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchPaginatedApplicationsByPropertyIdsByLeaseIntervalTypeIdsByTransmissionVendorIdByApplicationsFilterByCid( $arrintPropertyIds, $arrintLeaseIntervalTypeIds, $intTransmissionVendorId, $objApplicationsFilter, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$arrstrWhere = [];

		if( false == is_null( $objApplicationsFilter->getPageNo() ) && false == is_null( $objApplicationsFilter->getCountPerPage() ) ) {
			$intOffset 	= ( 0 < $objApplicationsFilter->getPageNo() ) ? $objApplicationsFilter->getCountPerPage() * ( $objApplicationsFilter->getPageNo() - 1 ) : 0;
			$intLimit 	= ( int ) $objApplicationsFilter->getCountPerPage();
		}

		$strProperties = trim( $objApplicationsFilter->getProperties() );
		if( false == empty( $strProperties ) ) {
			$arrstrWhere[] = 'ca.property_id IN ( ' . $objApplicationsFilter->getProperties() . ' ) ';
		} else {
			$arrstrWhere[] = 'ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}

		if( false == is_null( $objApplicationsFilter->getIsCompletedOnIsNotNull() ) && true == $objApplicationsFilter->getIsCompletedOnIsNotNull() ) {
			$arrstrWhere[] = 'ca.application_completed_on IS NOT NULL';
		}

		$strLeaseIntervalTypes = trim( $objApplicationsFilter->getLeaseIntervalTypes() );
		if( false == empty( $strLeaseIntervalTypes ) ) {
			$arrstrWhere[] = 'ca.lease_interval_type_id IN ( ' . $objApplicationsFilter->getLeaseIntervalTypes() . ' ) ';
		} elseif( true == isset( $arrintLeaseIntervalTypeIds ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrintLeaseIntervalTypeIds ) ) {
			$arrstrWhere[] = 'ca.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' ) ';
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						*
					FROM
						(
						SELECT
							DISTINCT ON ( TEMP . id ) TEMP . *
						FROM
							(
								SELECT
									ca.id,
									aat.transmission_response_type_id,
									appt.id AS applicant_id,
									appt.name_last,
									appt.name_first,
									appt.phone_number,
									appt.mobile_number,
									appt.work_number,
									appt.username AS email_address,
									ca.cid,
									ca.property_id,
									ca.application_datetime,
									ca.created_on,
									ca.application_completed_on,
									ca.application_approved_on,
									ca.application_stage_id,
									ca.application_status_id,
									sar.screening_decision_type_id,
									aa.customer_type_id,
									CASE
										WHEN ad.primary_phone_number_type_id = ' . CPhoneNumberType::HOME . ' THEN appt.phone_number
										WHEN ad.primary_phone_number_type_id = ' . CPhoneNumberType::OFFICE . ' THEN appt.work_number
										WHEN ad.primary_phone_number_type_id = ' . CPhoneNumberType::MOBILE . ' THEN appt.mobile_number
									END AS primary_phone_number,
									ad.primary_phone_number_type_id
								FROM
									cached_applications ca
									JOIN applicant_applications aa ON ( ca.id = aa.application_id AND aa.cid = ca.cid ' . $strCheckDeletedAASql . ' )
									JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid )
									JOIN applicant_details ad ON ( ad.cid = appt.cid AND ad.applicant_id = appt.id )
									JOIN applicant_application_transmissions aat ON ( aa.id = aat.applicant_application_id AND aa.cid = aat.cid AND aat.transmission_vendor_id = ' . ( int ) $intTransmissionVendorId . ' )
									JOIN screening_application_requests sar ON ( ca.id = sar.application_id AND ca.cid = sar.cid AND sar.screening_decision_type_id ISNULL )';

		$strSql .= ' WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.application_stage_id = ' . CApplicationStage::APPLICATION . '
						AND ca.application_status_id = ' . CApplicationStatus::COMPLETED . '
						AND ( (
								SELECT
									SUM( CASE WHEN customer_type_id = ' . CCustomerType::GUARANTOR . ' THEN CASE WHEN ( SELECT MAX( id ) FROM applicant_application_transmissions WHERE application_id = ca.id AND cid = ' . ( int ) $intCid . ' AND applicant_application_id = aa1.id AND  transmission_response_type_id = ' . CTransmissionResponseType::IN_PROGRESS . ' ) IS null THEN 0 ELSE 1 END ELSE 1 END ) as cnt
								FROM
									applicant_applications aa1
								WHERE
									aa1.application_id = ca.id
									AND aa1.cid = ' . ( int ) $intCid . '
									AND aa1.customer_type_id != ' . CCustomerType::NOT_RESPONSIBLE . '
						) =
						(
						SELECT count( id ) cnt
							FROM (
									SELECT aat.id,aat.transmission_response_type_id,
											MAX(aat.id) OVER( PARTITION BY applicant_application_id ) as
											max_application_id
									FROM applicant_application_transmissions aat
											JOIN applicant_applications aa ON (aa.id = aat.applicant_application_id AND aa.cid = aat.cid ' . $strCheckDeletedAASql . ' )
									WHERE
										aat.application_id = ca.id
										AND aat.cid = ' . ( int ) $intCid . '
										AND customer_type_id != ' . CCustomerType::NOT_RESPONSIBLE . '
							) as sub
							WHERE id = max_application_id  AND transmission_response_type_id IN (' . CTransmissionResponseType::APPROVED . ',' . CTransmissionResponseType::CONDITIONALLY_APPROVED . ',' . CTransmissionResponseType::DENIED . ' )
						) ) ';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ' . ') TEMP ';
		}

		$strSql .= ' WHERE
						customer_type_id = ' . CCustomerType::PRIMARY . '
						AND transmission_response_type_id IN (' . CTransmissionResponseType::APPROVED . ',' . CTransmissionResponseType::CONDITIONALLY_APPROVED . ',' . CTransmissionResponseType::DENIED . ' )
						AND application_approved_on IS NULL
						AND cid = ' . ( int ) $intCid;

		$arrstrOrderBy = [
			'lead_id'					=> 'id',
			'completed_on'				=> 'completed_on',
			'name'						=> 'name_last || name_first',
			'application_datetime'		=> 'application_datetime'
		];

		$strOrderBy = ( true == isset( $arrstrOrderBy[$objApplicationsFilter->getSortBy()] ) ? $arrstrOrderBy[$objApplicationsFilter->getSortBy()] : 'ca.application_datetime' );
		$strSortDirection = ( false == is_null( $objApplicationsFilter->getSortDirection() ) ) ? $objApplicationsFilter->getSortDirection() : 'DESC';
		$strSql .= ') temp_final ORDER BY ' . addslashes( $strOrderBy ) . ' ' . addslashes( $strSortDirection );

		if( false == is_null( $objApplicationsFilter->getPageNo() ) && false == is_null( $objApplicationsFilter->getCountPerPage() ) ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		return self::fetchCachedApplications( $strSql, $objDatabase );

	}

	public static function fetchResidentVerifyTransmissionVendorApplicationsByApplicationFilterByCid( $objApplicationsFilter, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		$arrstrWhere = [];

		$strProperties = trim( $objApplicationsFilter->getProperties() );
		if( false == empty( $strProperties ) ) {
			$arrstrWhere[] = 'ca.property_id IN ( ' . $objApplicationsFilter->getProperties() . ' ) ';
		}
		if( false == is_null( $objApplicationsFilter->getIsCompletedOnIsNotNull() ) && true == $objApplicationsFilter->getIsCompletedOnIsNotNull() ) {
			$arrstrWhere[] = 'ca.application_completed_on IS NOT NULL';
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						count ( DISTINCT ( TEMP . id ) ) AS count
					FROM
						(
						SELECT
							ca.id,
							aat.transmission_response_type_id,
							ca.application_approved_on,
							aa.customer_type_id,
							ca.cid
						FROM
							cached_applications ca
							JOIN applicant_applications aa ON ( ca.id = aa.application_id AND aa.cid = ca.cid ' . $strCheckDeletedAASql . ' )
							JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid )
							JOIN applicant_application_transmissions aat ON ( aa.id = aat.applicant_application_id AND aa.cid = aat.cid AND aat.transmission_vendor_id = 21 )
							JOIN screening_application_requests sar ON ( ca.id = sar.application_id AND ca.cid = sar.cid AND sar.screening_decision_type_id ISNULL )';

		$strSql .= ' WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.application_stage_id = ' . CApplicationStage::APPLICATION . '
						AND ca.application_status_id = ' . CApplicationStatus::COMPLETED . '
						AND ( (
								SELECT
									SUM( CASE WHEN customer_type_id = ' . CCustomerType::GUARANTOR . ' THEN CASE WHEN ( SELECT MAX( id ) FROM applicant_application_transmissions aat WHERE aat.cid = ' . ( int ) $intCid . ' AND aat.application_id = ca.id AND aat.applicant_application_id = aa1.id AND  aat.transmission_response_type_id = ' . CTransmissionResponseType::IN_PROGRESS . ' ) IS null THEN 0 ELSE 1 END ELSE 1 END ) as cnt
								FROM
									applicant_applications aa1
								WHERE
									aa1.application_id = ca.id
									AND aa1.cid = ' . ( int ) $intCid . '
									AND aa1.customer_type_id != ' . CCustomerType::NOT_RESPONSIBLE . '
						) =
						(
						SELECT count( id ) cnt

							FROM (
									SELECT aat.id,aat.transmission_response_type_id,
											MAX(aat.id) OVER( PARTITION BY applicant_application_id ) as
											max_application_id
									FROM applicant_application_transmissions aat
											JOIN applicant_applications aa ON (aa.id = aat.applicant_application_id AND aa.cid = aat.cid ' . $strCheckDeletedAASql . ' )
									WHERE
										aat.application_id = ca.id
										AND aat.cid = ' . ( int ) $intCid . '
										AND customer_type_id != ' . CCustomerType::NOT_RESPONSIBLE . '
							) as sub
							WHERE id = max_application_id  AND transmission_response_type_id IN (' . CTransmissionResponseType::APPROVED . ',' . CTransmissionResponseType::CONDITIONALLY_APPROVED . ',' . CTransmissionResponseType::DENIED . ' )
						) ) ';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ' . ' AND lease_interval_type_id = ' . CCustomerType::PRIMARY . ' ) TEMP ';
		}

		$strSql .= ' WHERE
						cid = ' . ( int ) $intCid . '
						AND customer_type_id = ' . CCustomerType::PRIMARY . '
						AND transmission_response_type_id IN (' . CTransmissionResponseType::APPROVED . ',' . CTransmissionResponseType::CONDITIONALLY_APPROVED . ',' . CTransmissionResponseType::DENIED . ' )
						AND application_approved_on IS NULL ';

		$arrstrData = fetchData( $strSql, $objDatabase );
		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];
		return 0;

	}

	public static function fetchExentdedRenewalOfferApplicationsByPropertyIdByLeaseIdsByCid( $intPropertyId, $arrintLeaseIds = [], $intCid, $objDatabase ) {

		if( 0 >= $intCid ) return NULL;
		if( 0 >= $intPropertyId ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						cached_applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND lease_interval_type_id = ' . ( int ) CLeaseIntervalType::RENEWAL . '
						AND application_stage_id = ' . CApplicationStage::APPLICATION . '
						AND application_status_id = ' . CApplicationStatus::STARTED;

						if( false != valArr( $arrintLeaseIds ) ) {
							$strSql .= ' AND lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )';
						}

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationByCallIdByCid( $intCallId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM cached_applications WHERE cid = ' . ( int ) $intCid . ' AND call_id = ' . ( int ) $intCallId . ' LIMIT 1';

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchLatestApplicationByLeaseIdByApplicationStageStatusIdsByCid( $intLeaseId, $intCid, $objDatabase, $arrintApplicationStageStatusIds = NULL ) {

		if( 0 >= $intCid ) return NULL;
		if( 0 >= $intLeaseId ) return NULL;
		if( true == is_null( $arrintApplicationStageStatusIds ) )  return NULL;

		$strSql = 'SELECT
						ca.*
					FROM
						cached_applications ca
						JOIN application_stage_statuses ass ON ( ca.application_stage_id = ass.application_stage_id AND ca.application_status_id = ass.application_status_id AND ca.lease_interval_type_id = ass.lease_interval_type_id  )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.lease_id = ' . ( int ) $intLeaseId . '
						AND ass.id IN ( ' . implode( ',', $arrintApplicationStageStatusIds ) . ' )
					ORDER BY
						created_on DESC
					LIMIT 1';

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchRenewalApplicationsByLeaseIdsByApplicationStageStatusIdsByCid( $arrintLeaseIds, $arrintApplicationStageStatusIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseIds ) || false == valArr( $arrintApplicationStageStatusIds ) ) return NULL;

		$strSql = 'SELECT
						ca.*,
						pf.floorplan_name
					FROM
						cached_applications ca
						LEFT JOIN property_floorplans pf ON ( ca.cid = pf.cid AND pf.property_id = ca.property_id AND ca.property_floorplan_id = pf.id )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::RENEWAL . '
						AND (ca.application_stage_id,ca.application_status_id) IN( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )
						AND ca.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )';

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationByRoommateIdByCid( $intRoommateId, $intCid, $objDatabase ) {

		if( false == is_numeric( $intRoommateId ) ) return NULL;

		$strSql = 'SELECT
						ca.*
					FROM
						roommates r
						JOIN roommate_groups rg ON ( r.roommate_group_id = rg.id AND r.cid = rg.cid )
						JOIN cached_applications ca ON (rg.application_id = ca.id AND rg.cid = ca.cid )
					WHERE
						r.id =' . ( int ) $intRoommateId . '
						AND ca.cid = ' . ( int ) $intCid;

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchLatestCompletedApplicationByApplicantIdByLeaseTermIdByPropertyFloorplanIdBySpaceConfigurationIdByPropertyIdByCid( $intApplicantId, $intLeaseTermId, $intPropertyFloorplanId, $intSpaceConfigurationId, $intPropertyId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == is_numeric( $intApplicantId ) || false == is_numeric( $intLeaseTermId ) || false == is_numeric( $intPropertyFloorplanId ) || false == is_numeric( $intPropertyId ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						ca.*
					FROM
						cached_applications ca,
						applicants appt,
						applicant_applications aa
					WHERE
						ca.id = aa.application_id
						AND ca.cid = aa.cid
						AND appt.id = aa.applicant_id
						AND appt.cid = aa.cid
						AND appt.id =' . ( int ) $intApplicantId . '
						AND ca.cid =' . ( int ) $intCid . '
						AND ( ( ca.application_stage_id = ' . CApplicationStage::LEASE . ' ) OR ( ca.application_stage_id = ' . CApplicationStage::APPLICATION . ' AND ca.application_status_id IN( ' . CApplicationStatus::COMPLETED . ',' . CApplicationStatus::APPROVED . ') ) )
						AND ca.lease_term_id =' . ( int ) $intLeaseTermId . '
						AND ca.property_id =' . ( int ) $intPropertyId . '
						AND ca.property_floorplan_id =' . ( int ) $intPropertyFloorplanId . $strCheckDeletedAASql;

		if( true == is_numeric( $intSpaceConfigurationId ) ) {
			$strSql .= ' AND ca.space_configuration_id = ' . ( int ) $intSpaceConfigurationId;
		}

		$strSql .= ' ORDER BY id DESC LIMIT 1';

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByLeaseIntervalTypeIdByLeaseIdByApplicationStageStatusIdsByCid( $intLeaseIntervalTypeId, $intLeaseId, $arrintApplicationStageStatusIds, $intCid, $objDatabase, $boolIsFromMidLease = false ) {

		if( false == valArr( $arrintApplicationStageStatusIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						cached_applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
						AND lease_id =' . ( int ) $intLeaseId . '
						AND (application_stage_id,application_status_id) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )';

		if( true == $boolIsFromMidLease ) {
			$strSql .= ' ORDER BY id DESC LIMIT 1';
		}

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByLeaseIntervalTypeIdByLeaseIdsByApplicationStageStatusIdsByCid( $intLeaseIntervalTypeId, $arrintLeaseIds, $arrintApplicationStageStatusIds, $intCid, $objDatabase, $boolIsFromMidLease = false ) {

		if( false == valArr( $arrintApplicationStageStatusIds ) || false == valArr( $arrintLeaseIds ) ) return NULL;

		$strSql = 'SELECT
            *
          FROM
            cached_applications
          WHERE
            cid = ' . ( int ) $intCid . '
            AND lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
            AND lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
            AND (application_stage_id,application_status_id) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )';

		if( true == $boolIsFromMidLease ) {
			$strSql .= ' ORDER BY id DESC LIMIT 1';
		}

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchRenewalOpenApplicationsByLeaseIdsByCid( $arrintLeaseIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseIds ) ) return NULL;

		$strSql = 'SELECT
						ca.*,
						COUNT( sc.id ) AS scheduled_charges_count
					FROM
						cached_applications ca
						LEFT JOIN scheduled_charges sc ON( sc.cid = ca.cid AND sc.property_id = ca.property_id AND sc.lease_id = ca.lease_id AND sc.lease_interval_id = ca.lease_interval_id )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . '
						AND ca.lease_id IN (' . implode( ',', $arrintLeaseIds ) . ' )
						AND ca.application_stage_id =' . CApplicationStage::APPLICATION . '
						AND ca.application_status_id =' . CApplicationStatus::STARTED . '
					GROUP BY
						ca.id,
						ca.cid
					HAVING
						COUNT( sc.id ) = 0';

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchRenewalOpenApplicationByIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						cached_applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . '
						AND id = ' . ( int ) $intApplicationId . '
						AND application_stage_id =' . CApplicationStage::APPLICATION . '
						AND application_status_id =' . CApplicationStatus::STARTED;

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchTransferOpenApplicationsByIdsByCid( $arrintApplicationIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						cached_applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND lease_interval_type_id = ' . CLeaseIntervalType::TRANSFER . '
						AND id IN ( ' . sqlIntImplode( $arrintApplicationIds ) . ' )
						AND application_stage_id =' . CApplicationStage::APPLICATION . '
						AND application_status_id =' . CApplicationStatus::STARTED;

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsCountByLeaseIntervalTypeIdByLeaseIdByApplicationStageStatusIdsByCid( $intLeaseIntervalTypeId, $intLeaseId, $arrintApplicationStageStatusIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicationStageStatusIds ) ) return NULL;

		$strSql = 'SELECT
						count( id )
					FROM
						cached_applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
						AND lease_id =' . ( int ) $intLeaseId . '
						AND (application_stage_id,application_status_id) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )';

		$arrmixApplicationCount = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixApplicationCount ) ) return $arrmixApplicationCount[0]['count'];

		return 0;
	}

	public static function fetchApplicationsWithNonAcceptedRenewalOffersByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ca.*
					FROM
						cached_applications ca
						JOIN renewal_tiers rt ON ( rt.cid = ca.cid AND rt.application_id = ca.id )
					WHERE
						ca.cid =' . ( int ) $intCid . '
						AND (application_stage_id,application_status_id) IN ( ' . sqlIntMultiImplode( [ CApplicationStage::PRE_APPLICATION => [ CApplicationStatus::COMPLETED ], CApplicationStage::APPLICATION => [ CApplicationStatus::STARTED ] ] ) . ' )
						AND ca.property_id =' . ( int ) $intPropertyId . '
						AND rt.id IS NULL';

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByLeaseIdsByPropertyIdByCid( $arrintLeaseIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						cached_applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND property_id = ' . ( int ) $intPropertyId;

		return self::fetchCachedApplications( $strSql, $objDatabase );

	}

	/**
	 * Function used for searching and filtering the waitlist by applicant
	 *
	 * @param int $intApplicationId
	 * @param array $arrintPropertyFloorplanIds
	 * @param array $arrintPropertyIds
	 * @param int $intCid
	 * @param CDatabase $objDatabase
	 * @param bool $boolIncludeDeletedAA
	 * @param bool $boolIncludeDeletedFloorPlans
	 * @return array
	 */

	public static function fetchApplicationsByApplicationIdByPropertyFloorplanIdsByPropertyIdsByCid( $intApplicationId, $arrintPropertyFloorplanIds, $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $boolIncludeDeletedFloorPlans = false ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlansSql = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pf.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						apl.id applicant_id,
						ca.id application_id,
						apl.name_first,
						apl.name_last,
						ca.lease_id,
						ca.screening_approved_on,
						ca.application_completed_on,
						ca.lease_start_date,
						apl.username AS email_address,
						COALESCE ( apl.phone_number, apl.mobile_number, apl.work_number, NULL ) AS phone_number,
						pf.floorplan_name,
						af.property_floorplan_id af_property_floorplan_id,
						ca.property_floorplan_id ap_property_floorplan_id,
						ca.desired_bedrooms,
						ca.desired_bathrooms,
						COALESCE ( sub_art.balance, 0 ) AS balance,
						CASE
						WHEN 0 >= COALESCE ( sub_art.balance, 0 ) THEN 0
						ELSE sub_art.balance
						END balance_for_sorting
					FROM
						cached_applications ca
						JOIN application_stage_statuses ass ON ( ass.application_stage_id = ca.application_stage_id AND ass.application_status_id = ca.application_status_id AND ass.lease_interval_type_id = ca.lease_interval_type_id)
						JOIN wait_lists wl ON ( wl.cid = ca.cid AND wl.property_group_id = ca.property_id AND wl.is_active IS TRUE )
						JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( $arrintPropertyIds, ',' ) . ' ]::INTEGER[] ) lp ON ( wl.cid = lp.cid AND lp.property_id = wl.property_group_id AND lp.is_disabled = 0 )
						LEFT JOIN property_floorplans pf ON ( pf.cid = ca.cid ' . $strCheckDeletedFloorPlansSql . ' )
						LEFT JOIN application_floorplans af ON ( af.cid = ca.cid AND af.application_id = ca.id AND af.property_floorplan_id = pf.id )
						JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id ' . $strCheckDeletedAASql . ' )
						JOIN applicants apl ON ( aa.cid = apl.cid AND apl.id = aa.applicant_id )
						JOIN lease_customers lc ON ( lc.cid = apl.cid AND lc.customer_id = apl.customer_id AND lc.lease_id = ca.lease_id )
						LEFT JOIN
								(
								SELECT
									COALESCE ( sum ( art.transaction_amount ), 0 ) AS balance,
									art.cid,
									art.lease_id
								FROM
									ar_transactions art
								WHERE
									art.cid = ' . ( int ) $intCid . '
									AND art.ar_code_type_id IN ( ' . implode( ', ', array_merge( CArCodeType::$c_arrintRentAndOtherFeesArCodeTypes, [ CArCodeType::PAYMENT ] ) ) . ' )
									AND art.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
								GROUP BY
									art.cid,
									art.lease_id
								) sub_art ON ( sub_art.cid = ca.cid AND sub_art.lease_id = ca.lease_id )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ass.id >= wl.application_stage_status_id
						AND ca.application_status_id NOT IN ( ' . CApplicationStatus::ON_HOLD . ',' . CApplicationStatus::CANCELLED . ' )
						AND ca.application_completed_on IS NOT NULL
						AND ca.property_unit_id IS NULL
						AND lc.customer_type_id = ' . ( int ) CCustomerType::PRIMARY . '
						AND lc.lease_status_type_id = ' . ( int ) CLeaseStatusType::APPLICANT . '';

		if( true == valArr( $arrintPropertyFloorplanIds ) ) {

			$strSql .= 'AND ( ca.property_floorplan_id IN ( ' . implode( ',', $arrintPropertyFloorplanIds ) . ' )
								OR af.property_floorplan_id IN ( ' . implode( ',', $arrintPropertyFloorplanIds ) . ' ) )
							AND ( ( ca.property_floorplan_id IS NOT NULL OR af.property_floorplan_id IS NOT NULL )
								OR ( ca.desired_bathrooms IS NOT NULL AND ca.desired_bedrooms IS NOT NULL ) )
							AND ( ca.property_floorplan_id = pf.id OR af.property_floorplan_id = pf.id )';

		} elseif( false == is_null( $intApplicationId ) ) {

			$strSql .= 'AND ca.id = ' . ( int ) $intApplicationId;
		}

		$strSql .= '
					ORDER BY
						balance_for_sorting,
						ca.application_completed_on,
						ca.lease_start_date,
						apl.name_first,
						apl.name_last';

		return fetchData( $strSql, $objDatabase );
	}

	/**
	 * Function used for searching and filtering the waitlist by floorplan
	 *
	 * @param array $arrintRequiredPropertyFloorPlanIds
	 * @param array $arrintPropertyIds
	 * @param int $intCid
	 * @param CDatabase $objDatabase
	 * @param bool $boolIsFromBulkAssignUnits
	 * @param array $arrintParentChildPropertyIds
	 * @param bool $boolIncludeDeletedAA
	 * @param bool $boolIncludeDeletedFloorPlans
	 * @return array
	 */

	public static function fetchApplicationsByPropertyFloorPlanIdsByPropertyIdsByCid( $arrintRequiredPropertyFloorPlanIds, $arrintPropertyIds, $intCid, $objDatabase, $boolIsFromBulkAssignUnits = false, $arrintParentChildPropertyIds = NULL, $boolIncludeDeletedAA = false, $boolIncludeDeletedFloorPlans = false ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlansSql = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pf.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						ca.id AS application_id,
						apl.id AS applicant_id,
						ca.lease_id,
						ca.screening_approved_on,
						COALESCE ( apl.name_first || \' \', \'\' ) || COALESCE ( apl.name_middle || \' \', \'\' ) || apl.name_last applicant_name,
						ca.application_completed_on,
						ca.lease_start_date,
						apl.username AS email_address,
						COALESCE ( apl.phone_number, apl.mobile_number, apl.work_number, NULL ) AS phone_number,
						CASE
						WHEN ca.property_floorplan_id IS NULL THEN af.property_floorplan_id
						ELSE ca.property_floorplan_id
						END AS property_floorplan_id,
						pf.floorplan_name,
						ca.desired_bedrooms,
						ca.desired_bathrooms,
						COALESCE ( sub_at.balance, 0 ) AS balance,
						CASE
						WHEN 0 >= COALESCE ( sub_at.balance, 0 ) THEN 0
						ELSE sub_at.balance
						END balance_for_sorting
					FROM
						cached_applications ca
						JOIN application_stage_statuses ass ON ( ass.application_stage_id = ca.application_stage_id AND ass.application_status_id = ca.application_status_id AND ass.lease_interval_type_id = ca.lease_interval_type_id)
						JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id ' . $strCheckDeletedAASql . ' )
						JOIN applicants apl ON ( aa.cid = apl.cid AND apl.id = aa.applicant_id )
						JOIN lease_customers lc ON ( lc.cid = apl.cid AND lc.customer_id = apl.customer_id AND lc.lease_id = ca.lease_id )
						JOIN wait_lists wl ON ( wl.cid = ca.cid AND wl.property_group_id = ca.property_id AND wl.is_active IS TRUE )
						JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INTEGER[], ARRAY[ ' . implode( $arrintPropertyIds, ',' ) . ' ]::INTEGER[] ) lp ON ( wl.cid = lp.cid AND lp.property_id = wl.property_group_id AND lp.is_disabled = 0 )
						LEFT JOIN application_floorplans af ON ( af.cid = ca.cid AND af.application_id = ca.id )
						LEFT JOIN property_floorplans pf ON ( pf.cid = ca.cid AND ( pf.id = ca.property_floorplan_id OR pf.id = af.property_floorplan_id ) ' . $strCheckDeletedFloorPlansSql . ' )';
						if( true == $boolIsFromBulkAssignUnits ) {
								$strSql .= 'LEFT JOIN
											(
											SELECT
												ai.application_id,
												ai.cid,
												ai.offer_submitted_on,
												ai.event_type_id,
												rank () OVER ( PARTITION BY ai.application_id ORDER BY ai.offer_submitted_on DESC ) AS rank
											FROM
												application_interests ai
											WHERE
													ai.cid = ' . ( int ) $intCid . '
													AND ai.property_id IN ( ' . implode( ',', $arrintParentChildPropertyIds ) . ' )
											) sub_ai ON ( sub_ai.cid = ca.cid AND sub_ai.application_id = ca.id AND sub_ai.rank = 1 )';
						}
						$strSql .= 'LEFT JOIN
								(
								SELECT
									COALESCE ( sum ( at.transaction_amount ), 0 ) AS balance,
									at.cid,
									at.lease_id
								FROM
									ar_transactions at
								WHERE
									at.cid = ' . ( int ) $intCid . '
									AND at.ar_code_type_id IN ( ' . implode( ', ', array_merge( CArCodeType::$c_arrintRentAndOtherFeesArCodeTypes, [ CArCodeType::PAYMENT ] ) ) . ' )
									AND at.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
								GROUP BY
									at.cid,
									at.lease_id
								) sub_at ON ( sub_at.cid = ca.cid AND sub_at.lease_id = ca.lease_id )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ass.id >= wl.application_stage_status_id
						AND ca.application_status_id NOT IN ( ' . CApplicationStatus::ON_HOLD . ',' . CApplicationStatus::CANCELLED . ' )
						AND ca.application_completed_on IS NOT NULL
						AND ca.property_unit_id IS NULL
						AND lc.customer_type_id = ' . ( int ) CCustomerType::PRIMARY . '
						AND lc.lease_status_type_id = ' . ( int ) CLeaseStatusType::APPLICANT . '
						AND ( ca.property_floorplan_id IN ( ' . implode( ',', $arrintRequiredPropertyFloorPlanIds ) . ' )
						OR af.property_floorplan_id IN ( ' . implode( ',', $arrintRequiredPropertyFloorPlanIds ) . ' ) )
						AND ( ( ca.property_floorplan_id IS NOT NULL
							OR af.property_floorplan_id IS NOT NULL )
							OR ( ca.desired_bathrooms IS NOT NULL
							AND ca.desired_bedrooms IS NOT NULL ) )';

						if( true == $boolIsFromBulkAssignUnits ) {
							$strSql .= ' AND ( sub_ai.event_type_id IS NULL
										OR sub_ai.event_type_id <> ' . ( int ) CEventType::GENERATE_WAITLIST_OFFER . ' ) ';
						}
					$strSql .= 'ORDER BY
						balance_for_sorting,
						ca.application_completed_on,
						ca.lease_start_date,
						apl.name_first,
						apl.name_last';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningCriteriaMissingApplicationIdsByPropertyIdsByModulePermissionByCid( $arrintPropertyIds, $arrintSelectedCompanyEmployeeIds, $intCid, $objDatabase, $intCurrentEmployeeId = NULL ) {

	if( false == valArr( $arrintPropertyIds ) ) return [];

		$arrstrWhere = [];

		if( true == ValArr( $arrintPropertyIds ) ) {
			$arrstrWhere[] = 'ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}

		$strSql = ' SELECT
						DISTINCT( ca.id )
					FROM
						cached_applications ca
						JOIN property_transmission_vendors ptv ON( ptv.property_id = ca.property_id AND ptv.cid = ca.cid )
						JOIN company_transmission_vendors ctv ON( ctv.id = ptv.company_transmission_vendor_id AND ctv.cid = ptv.cid )';
		$strSql .= 'WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ctv.transmission_vendor_id = ' . CTransmissionVendor::RESIDENT_VERIFY;

		if( false == valArr( $arrintSelectedCompanyEmployeeIds ) ) {

			$strSql .= ' AND( CASE WHEN ca.application_stage_id <> ' . CApplicationStage::LEASE . ' AND ca.application_status_id <> ' . CApplicationStatus::COMPLETED . '
							THEN ca.leasing_agent_id IS NULL
							ELSE true
						END ) ';

		} elseif( 1 == \Psi\Libraries\UtilFunctions\count( $arrintSelectedCompanyEmployeeIds ) && false == is_null( $intCurrentEmployeeId ) && true == array_key_exists( $intCurrentEmployeeId, $arrintSelectedCompanyEmployeeIds ) ) {
			$strSql .= ' AND( CASE WHEN ca.application_stage_id <> ' . CApplicationStage::LEASE . ' AND ca.application_status_id <> ' . CApplicationStatus::COMPLETED . '
							THEN ca.leasing_agent_id IS NULL OR ca.leasing_agent_id IN ( ' . implode( ',', $arrintSelectedCompanyEmployeeIds ) . ' )
							ELSE true
						END ) ';
		} else {
			$strSql .= ' AND ( ca.leasing_agent_id IN ( ' . implode( ',', $arrintSelectedCompanyEmployeeIds ) . ' ) OR ca.leasing_agent_id IS NULL AND ca.lease_interval_type_id IN ( ' . CLeaseIntervalType::APPLICATION . ', ' . CLeaseIntervalType::RENEWAL . ', ' . CLeaseIntervalType::LEASE_MODIFICATION . ' ))';
		}

		$strSql .= ' AND ca.application_stage_id = ' . CApplicationStage::LEASE . ' AND ca.application_status_id = ' . CApplicationStatus::COMPLETED;

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		$arrintApplicationIds = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintApplicationIds ) ) return [];

		$arrintRekeyedApplicationIds = rekeyArray( 'id', $arrintApplicationIds );

		$arrintScreeningCriteriaMissingApplicantionIds 		  = [];
		$intTotalLatestApplicantApplicationTransmissionsCount = 0;
		$intTotalApplicantsCount							  = 0;
		$arrobjTransmissionConditions						  = [];
		$arrobjRekeyedTransmissionConditions				  = [];

		$arrintTotalLatestApplicantApplicationTransmissionCounts = ( array ) CApplicantApplicationTransmissions::fetchTotalLatestApplicantApplicationTransmissionsCountByApplicationIdsByCustomerTypeIdsByCid( array_keys( $arrintRekeyedApplicationIds ), CCustomerType::$c_arrintApplicationRequiredCustomerTypeIds, $intCid, $objDatabase );
		$arrintTotalApplicantCounts								 = ( array ) CApplicants::fetchTotalApplicantsCountByApplicationIdsByCustomerTypeIdsByCid( array_keys( $arrintRekeyedApplicationIds ), CCustomerType::$c_arrintApplicationRequiredCustomerTypeIds, $intCid, $objDatabase );
		$arrobjTransmissionConditions							 = ( array ) CTransmissionConditions::fetchTransmissionConditionsByApplicationIdsByCid( array_keys( $arrintRekeyedApplicationIds ), $intCid, $objDatabase );

		$arrstrEvents 		 									 = \Psi\Eos\Entrata\CEvents::createService()->fetchLatestScreeningEventsByEventTypeIdByApplicationIdsByCid( CEventType::SCREENING_DECISION, array_keys( $arrintRekeyedApplicationIds ), $intCid, $objDatabase );
		$arrstrRekeyedEvents 									 = ( true == valArr( $arrstrEvents ) ) ? rekeyArray( 'event_reference_id', $arrstrEvents ) : [];

		$arrobjRekeyedTransmissionConditions					 = rekeyObjects( 'ApplicationId', $arrobjTransmissionConditions, true );

		$arrmixApplicationConditionSetDetails 				 	 = rekeyArray( 'application_id', ( array ) CScreeningApplicationConditionSets::fetchScreeningApplicationConditionSetsByApplicationIdsByCid( array_keys( $arrintRekeyedApplicationIds ), $intCid, $objDatabase ) );

		$arrintApplicationIds = array_keys( $arrintRekeyedApplicationIds );

		foreach( $arrintApplicationIds as $intApplicationId ) {

			$intTotalLatestApplicantApplicationTransmissionsCount = ( true == array_key_exists( $intApplicationId, $arrintTotalLatestApplicantApplicationTransmissionCounts ) ) ? $arrintTotalLatestApplicantApplicationTransmissionCounts[$intApplicationId]['count'] : 0;
			$intTotalApplicantsCount							  = ( true == array_key_exists( $intApplicationId, $arrintTotalApplicantCounts ) ) ? $arrintTotalApplicantCounts[$intApplicationId]['applicant_count'] : 0;

			if( 0 != $intTotalLatestApplicantApplicationTransmissionsCount ) {

				if( $intTotalLatestApplicantApplicationTransmissionsCount != $intTotalApplicantsCount ) {
					$arrintScreeningCriteriaMissingApplicantionIds[] = $intApplicationId;

				} else {
					if( true == array_key_exists( $intApplicationId, $arrmixApplicationConditionSetDetails ) ) {
						$boolIsConditionSetSatisfied = ( true == array_key_exists( 'satisfied_on', $arrmixApplicationConditionSetDetails[$intApplicationId] ) && false == is_null( $arrmixApplicationConditionSetDetails[$intApplicationId]['satisfied_on'] ) ) ? true : false;

						if( false == $boolIsConditionSetSatisfied ) $arrintScreeningCriteriaMissingApplicantionIds[] = $intApplicationId;

					} else {
						$arrobjTransmissionConditions = ( true == valArr( $arrobjRekeyedTransmissionConditions ) && true == array_key_exists( $intApplicationId, $arrobjRekeyedTransmissionConditions ) ) ? $arrobjRekeyedTransmissionConditions[$intApplicationId] : [];

						if( true == valArr( $arrobjTransmissionConditions ) ) {
							$objApplication = new CApplication();

							$boolIsTransmissionConditionsSatisfied = $objApplication->getIsTransmissionConditionsSatisfied( $arrobjTransmissionConditions );

							// if transmission conditions are selected, but not satisfied then skip that application from approving its lease

							if( false == $boolIsTransmissionConditionsSatisfied ) {
								$arrintScreeningCriteriaMissingApplicantionIds[] = $intApplicationId;
							}
 						}
					}
				}
			}
		}

		return $arrintScreeningCriteriaMissingApplicantionIds;
	}

	public static function fetchActiveApplicationByIdByApplicantIdByHistoricalDaysByLeaseIntervalTypeIdsByCid( $intApplicationId, $intApplicantId, $intHistoricalDays, $arrintLeaseIntervalTypeIds, $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {

		if( false == valArr( $arrintLeaseIntervalTypeIds ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = ' SELECT
						ca.*
					FROM
						cached_applications ca
						JOIN applicant_applications aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid AND aa.applicant_id = ' . ( int ) $intApplicantId . $strCheckDeletedAASql . ' )
						JOIN properties p ON ( p.id = ca.property_id AND p.cid = ca.cid )
					WHERE
						aa.cid = ' . ( int ) $intCid . '
						AND ca.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
						AND ca.application_status_id NOT IN ( ' . CApplicationStatus::CANCELLED . ', ' . CApplicationStatus::ON_HOLD . ' )
						AND to_char( COALESCE( ca.application_datetime, ca.created_on ), \'MM/DD/YYYY\' )::date >= ( CURRENT_DATE - integer \'' . ( int ) $intHistoricalDays . '\' )::date
						AND ( p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ') OR p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
						AND ca.id = ' . ( int ) $intApplicationId . '
					ORDER BY ca.created_on DESC LIMIT 1';

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchLatestApprovedApplicationByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		if( 0 >= $intCid ) return NULL;
		if( 0 >= $intLeaseId ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						cached_applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND lease_id = ' . ( int ) $intLeaseId . '
						AND application_status_id != ' . CApplicationStatus::CANCELLED . '
					ORDER BY
						created_on DESC
					LIMIT 1';

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		if( 0 >= $intCid ) return NULL;
		if( 0 >= $intLeaseId ) return NULL;

		$strSql = 'SELECT
						ca.*
					FROM
						cached_applications ca
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.lease_id = ' . ( int ) $intLeaseId . '
					ORDER BY
						ca.created_on DESC
					LIMIT 1';

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationByLeaseIdByLeaseIntervalIdByCid( $intLeaseId, $intLeaseIntervalId, $intCid, $objDatabase ) {

		if( 0 >= $intCid ) return NULL;
		if( 0 >= $intLeaseId ) return NULL;
		if( 0 >= $intLeaseIntervalId ) return NULL;

		$strSql = 'SELECT
						ca.*
					FROM
						cached_applications ca
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.lease_id = ' . ( int ) $intLeaseId . '
						AND ca.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
					ORDER BY
						ca.created_on DESC
					LIMIT 1';

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationPropertyFloorplanIdByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						property_floorplan_id
					FROM
						applications
					WHERE cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intApplicationId;

		return self::fetchColumn( $strSql, 'property_floorplan_id', $objDatabase );
	}

	public static function fetchApplicationByLeaseIdByLeaseIntervalTypeIdByApplicationStageStatusIdsByCid( $intLeaseId, $intLeaseIntervalTypeId, $arrintApplicationStageStatusIds, $intCid, $objDatabase ) {

		if( false == is_numeric( $intCid ) || false == is_numeric( $intLeaseId ) || false == is_numeric( $intLeaseIntervalTypeId ) || false == valArr( $arrintApplicationStageStatusIds ) ) return NULL;

		$strSql = 'SELECT
						ca.*
					FROM
						cached_applications ca
					WHERE
						ca.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
						AND ca.cid = ' . ( int ) $intCid . '
						AND ca.lease_id = ' . ( int ) $intLeaseId . '
						AND (ca.application_stage_id,ca.application_status_id) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )
					ORDER BY
						ca.created_on DESC
					LIMIT 1';

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByLeaseIdsByLeaseIntervalTypeIdByApplicationStageStatusIdsByCid( $arrintLeaseIds, $intLeaseIntervalTypeId, $arrintApplicationStageStatusIds, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valArr( $arrintLeaseIds ) || false == valId( $intLeaseIntervalTypeId ) || false == valArr( $arrintApplicationStageStatusIds ) ) return NULL;

		$strSql = 'SELECT
						ca.*
					FROM
						cached_applications ca
					WHERE
						ca.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
						AND ca.cid = ' . ( int ) $intCid . '
						AND ca.lease_id IN ( ' . sqlIntImplode( $arrintLeaseIds ) . ' )
						AND ( ca.application_stage_id, ca.application_status_id ) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' ) ';

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsFieldsByIdsByCid( $arrintApplicationIds, $intCid, $objClientDatabase, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false, $boolIncludeDeletedFloorPlans = false ) {
		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strCheckDeletedUnitsSql 	  = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlansSql = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pfp.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						ca.id,
						ce.name_first || \' \' || ce.name_last as leasing_agent_name,
						pu.unit_number,
						pb.building_name,
						pfp.floorplan_name,
						ca.lease_start_date
					FROM
						cached_applications ca
						LEFT JOIN company_employees ce ON ( ca.cid = ce.cid AND ca.leasing_agent_id = ce.id )
						LEFT JOIN unit_spaces us ON ( ca.cid = us.cid AND ca.unit_space_id = us.id ' . $strCheckDeletedUnitSpacesSql . ' )
						LEFT JOIN property_units pu ON ( us.cid = pu.cid AND us.property_unit_id = pu.id ' . $strCheckDeletedUnitsSql . ' )
						LEFT JOIN property_buildings pb ON ( pu.cid = pb.cid AND pu.property_building_id = pb.id AND pb.deleted_on IS NULL )
						LEFT JOIN property_floorplans AS pfp ON ( ca.property_floorplan_id = pfp.id AND ca.cid = pfp.cid ' . $strCheckDeletedFloorPlansSql . ' )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.id IN ( ' . implode( ', ', $arrintApplicationIds ) . ' )';

		$arrmixData			= fetchData( $strSql, $objClientDatabase );
		$arrmixFinalData	= [];

		foreach( $arrmixData as $arrmixChunk ) {
			$arrmixFinalData[$arrmixChunk['id']] = $arrmixChunk;
		}

		return $arrmixFinalData;
	}

	public static function fetchApplicationsByLeaseIdsByCid( $arrintLeaseIds, $intCid, $objDatabase ) {

		if ( false == valArr( $arrintLeaseIds ) ) return;

		$strSql = 'SELECT
						apps.*
					FROM
						leases l
						JOIN
							(
							SELECT
								rank() OVER ( PARTITION BY ca.lease_id ORDER BY ca.id ),
								ca.*
							FROM
								cached_applications ca
							WHERE
								ca.cid = ' . ( int ) $intCid . '
								AND ca.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
							) apps ON ( apps.cid = l.cid AND apps.lease_id = l.id AND apps.rank = 1 )
					WHERE
						l.cid = ' . ( int ) $intCid . '
						AND l.id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )';

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByLeaseIdsByLeaseIntervalTypeIdsByCid( $arrintLeaseIds, $arrintLeaseIntervalTypeIds, $intCid, $objDatabase ) {

		if ( false == valArr( $arrintLeaseIds ) ) return;

		$strSql = 'SELECT
						apps.*
					FROM
						leases l
						JOIN
							(
							SELECT
								rank() OVER ( PARTITION BY ca.lease_id ORDER BY ca.id ),
								ca.*
							FROM
								cached_applications ca
							WHERE
								ca.cid = ' . ( int ) $intCid . '
								AND ca.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
								AND ca.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
							) apps ON ( apps.cid = l.cid AND apps.lease_id = l.id AND apps.rank = 1 )
					WHERE
						l.cid = ' . ( int ) $intCid . '
						AND l.id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )';

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function filterScreeningFailedApplicationIdsByApplicationIdsByModulePermissionByCid( $arrintApplicationIds, $boolCanApproveApplicationWithoutScreening, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicationIds ) ) return [];

		$strSql = 'SELECT
						a.id
					FROM
						applications a
						JOIN property_transmission_vendors ptv ON ( ptv.property_id = a.property_id AND ptv.cid = a.cid )
						JOIN company_transmission_vendors ctv ON ( ctv.id = ptv.company_transmission_vendor_id AND ctv.cid = ptv.cid )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND ctv.transmission_vendor_id = ' . CTransmissionVendor::RESIDENT_VERIFY . '
						AND a.id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )';

		$arrintRvEnabledApplicationIds = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintRvEnabledApplicationIds ) ) return [];

		$arrintRvEnabledApplicationIds = array_keys( rekeyArray( 'id', $arrintRvEnabledApplicationIds ) );

		$arrintScreeningFailedApplicationIds = [];

		$intTotalLatestApplicantApplicationTransmissionsCount = 0;
		$intTotalApplicantsCount							  = 0;
		$arrobjTransmissionConditions						  = [];
		$arrobjRekeyedTransmissionConditions				  = [];

		$arrobjLatestApplicantApplicationTransmissions 	= rekeyObjects( 'ApplicationId', ( array ) CApplicantApplicationTransmissions::fetchLatestApplicantApplicationTransmissionsByApplicationIdsByCustomerTypeIdsByCid( $arrintRvEnabledApplicationIds, [ CCustomerType::PRIMARY, CCustomerType::RESPONSIBLE, CCustomerType::NON_LEASING_OCCUPANT, CCustomerType::GUARANTOR ], $intCid, $objDatabase ), true );
		$arrintTotalApplicantCounts 					= ( array ) CApplicants::fetchTotalApplicantsCountByApplicationIdsByCustomerTypeIdsByCid( $arrintRvEnabledApplicationIds, CCustomerType::$c_arrintApplicationRequiredCustomerTypeIds, $intCid, $objDatabase );
		$arrobjTransmissionConditions					= ( array ) CTransmissionConditions::fetchTransmissionConditionsByApplicationIdsByCid( $arrintRvEnabledApplicationIds, $intCid, $objDatabase );

		$arrmixApplicationConditionSetDetails 			= rekeyArray( 'application_id', ( array ) CScreeningApplicationConditionSets::fetchScreeningApplicationConditionSetsByApplicationIdsByCid( array_keys( $arrintRvEnabledApplicationIds ), $intCid, $objDatabase ) );

		$arrobjRekeyedTransmissionConditions			= rekeyObjects( 'ApplicationId', $arrobjTransmissionConditions, true );

		foreach( $arrintRvEnabledApplicationIds as $intApplicationId ) {

			$intTotalLatestApplicantApplicationTransmissionsCount = ( true == array_key_exists( $intApplicationId, $arrobjLatestApplicantApplicationTransmissions ) ) ? \Psi\Libraries\UtilFunctions\count( $arrobjLatestApplicantApplicationTransmissions[$intApplicationId] ) : 0;
			$intTotalApplicantsCount							  = ( true == array_key_exists( $intApplicationId, $arrintTotalApplicantCounts ) ) ? $arrintTotalApplicantCounts[$intApplicationId]['applicant_count'] : 0;

			if( 0 == $intTotalLatestApplicantApplicationTransmissionsCount ) {
				// if not screened then check for permission approve application without screening
				if( false == $boolCanApproveApplicationWithoutScreening ) {
					$arrintScreeningFailedApplicationIds[] = $intApplicationId;
				}

			} else {
				if( $intTotalLatestApplicantApplicationTransmissionsCount != $intTotalApplicantsCount ) {
					$arrintScreeningFailedApplicationIds[] = $intApplicationId;

				} else {
					if( true == array_key_exists( $intApplicationId, $arrmixApplicationConditionSetDetails ) ) {
						$boolIsConditionSetSatisfied = ( true == array_key_exists( 'satisfied_on', $arrmixApplicationConditionSetDetails[$intApplicationId] ) && false == is_null( $arrmixApplicationConditionSetDetails[$intApplicationId]['satisfied_on'] ) ) ? true : false;

						if( false == $boolIsConditionSetSatisfied ) $arrintScreeningFailedApplicationIds[] = $intApplicationId;

					} else {
						$arrobjTransmissionConditions = ( true == valArr( $arrobjRekeyedTransmissionConditions ) && true == array_key_exists( $intApplicationId, $arrobjRekeyedTransmissionConditions ) ) ? $arrobjRekeyedTransmissionConditions[$intApplicationId] : [];

						if( true == valArr( $arrobjTransmissionConditions ) ) {
							$objApplication = new CApplication();

							$boolIsTransmissionConditionsSatisfied = $objApplication->getIsTransmissionConditionsSatisfied( $arrobjTransmissionConditions );

							// if transmission conditions are selected, but not satisfied then skip that application from approving
							if( false == $boolIsTransmissionConditionsSatisfied ) {
								$arrintScreeningFailedApplicationIds[] = $intApplicationId;
							}
						} else {

							// If screening recommendation is not pass, then skip application from approving
							$arrobjPrimaryCoApplicantsLatestApplicantApplicationTransmissions = ( true == array_key_exists( $intApplicationId, $arrobjLatestApplicantApplicationTransmissions ) ) ? $arrobjLatestApplicantApplicationTransmissions[$intApplicationId] : [];
							if( CScreeningRecommendationType::PASS != CResidentVerifyScreeningController::loadPrimaryCoapplicantsApplicationScreeningStatusId( $arrobjPrimaryCoApplicantsLatestApplicantApplicationTransmissions ) ) {
								$arrintScreeningFailedApplicationIds[] = $intApplicationId;
							}
						}
					}
				}
			}
		}
		return $arrintScreeningFailedApplicationIds;
	}

	public static function fetchApplicationsByEventTypeIdByLeaseIntervalTypeIdByPsProductIdByPropertyIdsByCidByFormFilter( $intEventTypeId, $intLeaseIntervalTypeId, $intProductId, $arrintPropertyIds, $intCid, $objFormFilter, $objDatabase ) {
		if ( false == valId( $intLeaseIntervalTypeId ) || false == valArr( $arrintPropertyIds ) || false == valObj( $objFormFilter, 'CFormFilter' ) ) return NULL;

		$strSql = 'SELECT
						ca.id
					FROM
						cached_applications ca
						JOIN events e ON ( e.cid = ca.cid AND e.lease_interval_id = ca.lease_interval_id AND e.event_type_id = ' . ( int ) $intEventTypeId . ' )
						JOIN properties p ON ( p.cid = ca.cid AND p.id = ca.property_id )
						JOIN time_zones tz ON ( tz.id = p.time_zone_id )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ca.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
						AND ca.ps_product_id = ' . ( int ) $intProductId . '
						' . CReportCommonQueries::getWhereConditionForArchivedLeadsSql( ( int ) $intCid, 'ca' ) . '
						AND ca.occupancy_type_id <> ' . COccupancyType::OTHER_INCOME . '
						AND e.created_by < \' 10000 \'
						AND ' . $objFormFilter->getFilterObject( 'period' )->getDateComparisonSql( 'DATE_TRUNC( \'day\', ca.application_datetime ) AT TIME ZONE tz.time_zone_name ', NULL, 'property_id' );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByCallIdsByUserIdsByLeaseIntervalTypeIdByPsProductIdByPropertyIdsByCidByDate( $arrintPropertyCallIds, $arrintUserIds, $intLeaseIntervalTypeId, $intProductId, $intCid, $objFormFilter, $objDatabase, $arrintApplicantApplicationIds = [] ) {
		if ( false == valId( $intLeaseIntervalTypeId ) || false == valArr( $arrintUserIds ) || false == valArr( $arrintPropertyCallIds ) ) return NULL;

		$strJoinSql            = $strWhereSql = '';
		if( true == valArr( $arrintApplicantApplicationIds ) ) {
			$strJoinSql            = '
				JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id AND aa.customer_type_id = ' . ( int ) CCustomerType::PRIMARY . ' )
				JOIN applicants a ON ( a.cid = aa.cid AND a.id = aa.applicant_id )
			';
			$strWhereSql           = ' aa.id IN ( ' . implode( ',', $arrintApplicantApplicationIds ) . ' ) ';
		}

		foreach( $arrintPropertyCallIds as $arrintPropertyCallId ) {
			$arrmixPropertyCalls[] 	= '( ' . $arrintPropertyCallId['property_id'] . ', ' . $arrintPropertyCallId['call_id'] . ' )';
		}

		$strJoinSql .= '
					JOIN ( VALUES ' . implode( ', ', $arrmixPropertyCalls ) . ' ) AS calls_data( property_id, call_id )
						ON ca.property_id = calls_data.property_id AND ca.call_id = calls_data.call_id';

		$strSql = '
			SELECT
				DISTINCT( ca.id ) AS application_id,
				ca.call_id,
				ca.property_id
			FROM
				cached_applications ca
				' . $strJoinSql . '
				JOIN properties p ON ( p.cid = ca.cid AND p.id = ca.property_id )
				JOIN time_zones tz ON ( tz.id = p.time_zone_id )
			WHERE
				ca.cid = ' . ( int ) $intCid . '
				AND ca.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
				' . ( true == valId( $intProductId ) ? ' AND ca.ps_product_id = ' . ( int ) $intProductId . ' ' : '' ) . '
				-- AND ca.created_by IN ( ' . implode( ',', $arrintUserIds ) . ' ) // Leasing center team is working on why guest cards are not created by leasing agent.
				AND ' . $strWhereSql . '
				' . CReportCommonQueries::getWhereConditionForArchivedLeadsSql( ( int ) $intCid, 'ca' ) . '
				AND ca.occupancy_type_id <> ' . COccupancyType::OTHER_INCOME . '
				AND ca.application_datetime AT TIME ZONE tz.time_zone_name >= \'' . $objFormFilter->getFilterObject( 'period' )->getStartDate() . ' 00:00:00 \'
				AND ca.application_datetime AT TIME ZONE tz.time_zone_name <= \'' . $objFormFilter->getFilterObject( 'period' )->getEndDate() . ' 23:59:59 \'
			';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationsLeaseIntervalTypeIdByCallFilterByProductId( $intLeaseIntervalTypeId, $objCallFilter, $intProductId, $objDatabase, $arrintApplicantApplicationIds = [] ) {
		if ( false == valObj( $objCallFilter, 'CCallFilter' ) || false == valId( $intLeaseIntervalTypeId ) || false == valStr( $objCallFilter->getCallIds() ) ) return NULL;

		$strJoinSql = $strWhereSql = '';

		if( true == valArr( $arrintApplicantApplicationIds ) ) {
			$strJoinSql		= 'JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id AND aa.customer_type_id = ' . ( int ) CCustomerType::PRIMARY . ' )
							JOIN applicants a ON ( a.cid = aa.cid AND a.id = aa.applicant_id )';
			$strWhereSql	= ' OR aa.id IN ( ' . implode( ',', $arrintApplicantApplicationIds ) . ' ) ';
		}

		$strSql = '
			SELECT
				DISTINCT( ca.id ) AS application_id,
				ca.call_id,
				ca.property_id
			FROM
				cached_applications ca
				JOIN properties p ON ( p.cid = ca.cid AND p.id = ca.property_id )
				' . $strJoinSql . '
				JOIN time_zones tz ON ( tz.id = p.time_zone_id )
			WHERE
				ca.cid IN ( ' . $objCallFilter->getCids() . ' )
				AND ca.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
				' . ( true == valId( $intProductId ) ? ' AND ca.ps_product_id = ' . ( int ) $intProductId . ' ' : '' ) . '
				AND ( ca.call_id IN ( ' . $objCallFilter->getCallIds() . ' ) ' . $strWhereSql . ' )
				AND ca.application_datetime AT TIME ZONE tz.time_zone_name >= \'' . $objCallFilter->getStartDate() . ' 00:00:00 \'
				AND ca.application_datetime AT TIME ZONE tz.time_zone_name <= \'' . $objCallFilter->getEndDate() . ' 23:59:59 \'
			';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByLeaseIntervalTypeIdsByCallFilter( $arrintLeaseIntervalTypeIds, $objCallFilter, $objDatabase ) {
		if ( false == valArr( $arrintLeaseIntervalTypeIds ) || false == valStr( $objCallFilter->getCids() ) || false == valStr( $objCallFilter->getCallIds() ) || false == valObj( $objCallFilter, 'CCallFilter' ) ) return NULL;

		if( CCallFilter::LEASING_CENTER_REPORT_INTERVAL_WEEKLY == $objCallFilter->getIntervalTypeId() ) {
			$strSelect = ' DATE_TRUNC( \'week\', ca.application_datetime + INTERVAL \'6 days\' )::date AS application_datetime_format, ';
		} elseif( CCallFilter::LEASING_CENTER_REPORT_INTERVAL_DAILY == $objCallFilter->getIntervalTypeId() ) {
			$strSelect = ' DATE_TRUNC( \'day\', ca.application_datetime )::date AS application_datetime_format, ';
		} elseif( CCallFilter::LEASING_CENTER_REPORT_INTERVAL_MONTHLY == $objCallFilter->getIntervalTypeId() ) {
			$strSelect = ' DATE_TRUNC( \'month\', ca.application_datetime )::date AS application_datetime_format, ';
		}

		$strSql = 'SELECT
						DISTINCT( ca.id ),
						ca.lease_interval_id,
						' . $strSelect . '
						 COUNT( ca.id ) AS application_count
					FROM
						cached_applications ca
					WHERE
						ca.cid IN ( ' . $objCallFilter->getCids() . ' )
						AND ca.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
						AND ca.ps_product_id IN ( ' . $objCallFilter->getPsProductIds() . ' )
						AND ca.call_id IN ( ' . $objCallFilter->getCallIds() . ' )
						AND ca.application_datetime BETWEEN \'' . $objCallFilter->getStartDate() . ' 00:00:00\'
						AND \'' . $objCallFilter->getEndDate() . ' 23:59:59\'
					GROUP BY
						application_datetime_format,
						ca.id,
						ca.lease_interval_id
					ORDER BY
						application_datetime_format ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationCountByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase ) {
		if ( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						a.property_id,
						COUNT( id ) AS guest_cards_inserted_count
					FROM
						applications a
					WHERE
						a.id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND a.cid = ' . ( int ) $intCid . '
					GROUP BY a.property_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationCountByApplicationIdsByApplicationStageStatusIdsByCid( $arrintApplicationIds, $arrintApplicationStageStatusIds, $intCid, $objDatabase ) {
		if ( false == valArr( $arrintApplicationIds ) || false == valArr( $arrintApplicationStageStatusIds ) ) return NULL;

		$strSql = 'SELECT
						ca.property_id,
						COUNT( id ) AS leases_generated_count
					FROM
						cached_applications ca
					WHERE
						ca.id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND ca.cid = ' . ( int ) $intCid . '
						AND (ca.application_stage_id,ca.application_status_id) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )
					GROUP BY ca.property_id';

		return ( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationByLeaseIdByLeaseIntervalTypeIdByExcludingApplicationStageStatusIdsByCid( $intLeaseId, $intLeaseIntervalTypeId, $arrintExcludingApplicationStageStatusIds, $intCid, $objDatabase ) {

		if( false == is_numeric( $intCid ) || false == is_numeric( $intLeaseId ) || false == is_numeric( $intLeaseIntervalTypeId ) || false == valArr( $arrintExcludingApplicationStageStatusIds ) ) return NULL;

		$strSql = 'SELECT
						ca.*
					FROM
						cached_applications ca
					WHERE
						ca.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
						AND ca.cid = ' . ( int ) $intCid . '
						AND ca.lease_id = ' . ( int ) $intLeaseId . '
						AND (ca.application_stage_id,ca.application_status_id) NOT IN ( ' . sqlIntMultiImplode( $arrintExcludingApplicationStageStatusIds ) . ' )
					ORDER BY
						ca.created_on DESC
					LIMIT 1';

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationByAppRemotePrimaryKeyOrLeaseIntervalIdByPropertyIdByCid( $strAppRemotePrimaryKey, $intLeaseIntervalId, $intPropertyId, $intCid, $objDatabase ) {

		$strCondition = 'lower( ca.app_remote_primary_key ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strAppRemotePrimaryKey ) ) . '\'';

		if( true == valStr( $intLeaseIntervalId ) ) $strCondition = '( lower( ca.app_remote_primary_key ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strAppRemotePrimaryKey ) ) . '\' OR ca.lease_interval_id = ' . ( int ) $intLeaseIntervalId . ')';

		$strSql = 'SELECT
						ca.*
					FROM
						cached_applications ca
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.property_id = ' . ( int ) $intPropertyId . '
						AND ' . $strCondition;

		$strSql .= ' ORDER BY ca.id DESC LIMIT 1';

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationByGuestRemotePrimaryKeyOrLeaseIntervalIdByPropertyIdByCid( $strGuestRemotePrimaryKey, $intLeaseIntervalId, $intPropertyId, $intCid, $objDatabase ) {

		$strCondition = 'lower( ca.guest_remote_primary_key ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strGuestRemotePrimaryKey ) ) . '\'';

		if( true == valStr( $intLeaseIntervalId ) ) $strCondition = '( lower( ca.guest_remote_primary_key ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strGuestRemotePrimaryKey ) ) . '\' OR ca.lease_interval_id = ' . ( int ) $intLeaseIntervalId . ')';

		$strSql = 'SELECT
						ca.*
					FROM
						cached_applications ca
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.property_id = ' . ( int ) $intPropertyId . '
						AND ' . $strCondition;

		$strSql .= ' ORDER BY ca.id DESC LIMIT 1';

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByLeaseIntervalTypeIdByLeaseIdsByCid( $intLeaseIntervalTypeId, $arrintLeaseIds, $intCid, $objDatabase, $arrintLeaseIntervalIds = NULL ) {

		if ( false == valArr( $arrintLeaseIds ) ) return;

		$strSql = 'SELECT
						ca.*
					FROM
						cached_applications ca
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
						AND ca.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' ) ' .
						( ( true == valArr( $arrintLeaseIntervalIds ) ) ? 'AND ca.lease_interval_id IN ( ' . implode( ',', $arrintLeaseIntervalIds ) . ')' : '' ) . '
						AND ca.application_status_id NOT IN ( ' . CApplicationStatus::CANCELLED . ' )';

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	// This function is written for new dashboard

	public static function fetchPaginatedRenewalOffersByDashboardFilterByCid( $objDashboardFilter, $intCid, $intOffset, $intPageSize, $strSortBy, $objClientDatabase ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strLeasingAgentCondition = ( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) ? ' AND ( ca.leasing_agent_id IN ( ' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ' ) OR ca.leasing_agent_id IS NULL ) ' : '';

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSql = 'SELECT
						*
					FROM (
						SELECT
							ca.id,
							ca.cid,
							ca.lease_id,
							lp.property_id,
							func_format_customer_name ( appt.name_first, appt.name_last ) AS customer_full_name,
							la.lease_end_date AS lease_end_date,
							COALESCE( la.building_name || \' - \' || la.unit_number_cache, la.building_name, la.unit_number_cache ) as unit_number,
							la.property_name,
							la.primary_customer_id,
							CASE
								WHEN li.lease_interval_type_id <> ' . CLeaseIntervalType::MONTH_TO_MONTH . ' THEN lst.name
								ELSE \'Month-to-Month\'
							END AS lease_status_type_name,
							CASE WHEN TRIM( dp.approvals_renewal_offers->>\'urgent_lease_expires_within\' ) <> \'\' AND CURRENT_DATE >= ( la.lease_end_date + ( dp.approvals_renewal_offers->>\'urgent_lease_expires_within\' )::int ) THEN
								3
							WHEN TRIM( dp.approvals_renewal_offers->>\'urgent_lease_interval_type_id\' ) <> \'\' AND TRIM( dp.approvals_renewal_offers->>\'urgent_lease_interval_type_id\' )::int = 1 AND li.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . ' THEN
								3
							WHEN TRIM( dp.approvals_renewal_offers->>\'urgent_lease_status_type_ids\' ) <> \'\' AND la.lease_status_type_id = ANY( ( dp.approvals_renewal_offers->>\'urgent_lease_status_type_ids\' )::integer[] ) AND li.lease_interval_type_id <> ' . CLeaseIntervalType::MONTH_TO_MONTH . ' THEN
								3
							WHEN TRIM( dp.approvals_renewal_offers->>\'important_lease_expires_within\' ) <> \'\' AND CURRENT_DATE >= ( la.lease_end_date + ( dp.approvals_renewal_offers->>\'important_lease_expires_within\' )::int ) THEN
								2
							WHEN TRIM( dp.approvals_renewal_offers->>\'important_lease_interval_type_id\' ) <> \'\' AND TRIM( dp.approvals_renewal_offers->>\'important_lease_interval_type_id\' )::int = 1 AND li.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . ' THEN
								2
							WHEN TRIM( dp.approvals_renewal_offers->>\'important_lease_status_type_ids\' ) <> \'\' AND la.lease_status_type_id = ANY( ( dp.approvals_renewal_offers->>\'important_lease_status_type_ids\' )::integer[] ) AND li.lease_interval_type_id <> ' . CLeaseIntervalType::MONTH_TO_MONTH . ' THEN
								2
							ELSE
								1
							END as priority
						FROM
							cached_applications ca
							JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) lp ON ( lp.property_id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ?  ' AND lp.is_disabled = 0 ' : '' ) . ' )
							JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' AND aa.deleted_on IS NULL )
							JOIN applicants appt ON ( appt.cid = ca.cid AND appt.id = aa.applicant_id AND appt.cid = aa.cid  )
							LEFT JOIN cached_leases la ON ( la.cid = ca.cid AND la.id = ca.lease_id )
							JOIN lease_intervals li ON ( li.cid = la.cid AND li.id = la.active_lease_interval_id )
							JOIN lease_status_types lst ON ( lst.id = la.lease_status_type_id )
							LEFT JOIN dashboard_priorities dp ON dp.cid = ca.cid
						WHERE
							ca.cid = ' . ( int ) $intCid . '
							' . $strLeasingAgentCondition . '
							AND la.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
							AND ca.application_stage_id IN ( ' . CApplicationStage::PRE_APPLICATION . ' , ' . CApplicationStage::APPLICATION . ' )
							AND	ca.application_status_id  = ' . CApplicationStatus::COMPLETED . '
							AND ca.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . '
					) a_reversals
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere . '
					ORDER BY
						' . $strSortBy . '
					OFFSET
						' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPaginatedTransferCompletedApplicationsByDashboardFilterByCid( $objDashboardFilter, $intCid, $intOffset, $intPageSize, $strSortBy, $objClientDatabase ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strLeasingAgentCondition = ( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) ? ' AND ( ca.leasing_agent_id IN ( ' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ' ) OR ca.leasing_agent_id IS NULL ) ' : '';

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSql = 'SELECT
						*
					FROM (
						SELECT
							ca.id,
							ca.cid,
							ca.lease_id,
							lp.property_id,
							func_format_customer_name ( appt.name_first, appt.name_last ) AS customer_full_name,
							cl.lease_end_date AS lease_end_date,
							COALESCE( cl.building_name || \' - \' || cl.unit_number_cache, cl.building_name, cl.unit_number_cache ) as unit_number,
							cl.property_name,
							cl.primary_customer_id,
							cl.id as notice_lease_id,
							CASE WHEN TRIM( dp.approvals_transfers->>\'urgent_transfer_request_scheduled\' ) <> \'\' AND (dp.approvals_transfers->>\'urgent_transfer_request_scheduled\' )::int <= ( CURRENT_DATE - ( ca.application_datetime::date ) ) THEN
								3
							WHEN TRIM( dp.approvals_transfers->>\'urgent_transfer_request_accepted\' ) <> \'\' AND ca.application_status_id = ' . CApplicationStatus::COMPLETED . ' AND (dp.approvals_transfers->>\'urgent_transfer_request_accepted\' )::int <= ( CURRENT_DATE - ( ca.application_completed_on::date ) ) THEN
								3
							WHEN TRIM( dp.approvals_transfers->>\'important_transfer_request_scheduled\' ) <> \'\' AND (dp.approvals_transfers->>\'important_transfer_request_scheduled\' )::int <= ( CURRENT_DATE - ( ca.application_datetime::date ) ) THEN
								2
							WHEN TRIM( dp.approvals_transfers->>\'important_transfer_request_accepted\' ) <> \'\' AND ca.application_status_id = ' . CApplicationStatus::COMPLETED . ' AND (dp.approvals_transfers->>\'important_transfer_request_accepted\' )::int <= ( CURRENT_DATE - ( ca.application_completed_on::date ) ) THEN
								2
							ELSE
								1
							END as priority
						FROM
							cached_applications ca
							JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) lp ON ( lp.property_id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ?  ' AND lp.is_disabled = 0 ' : '' ) . ' )
							JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' AND aa.deleted_on IS NULL )
							JOIN applicants appt ON ( appt.cid = ca.cid AND appt.id = aa.applicant_id AND appt.cid = aa.cid  )
							JOIN lease_processes lpt ON ( ca.cid = lpt.cid AND lpt.transfer_lease_id = ca.lease_id )
							JOIN cached_leases cl ON ( ca.cid = cl.cid AND lpt.lease_id = cl.id )
							LEFT JOIN dashboard_priorities dp ON dp.cid = ca.cid
						WHERE
							ca.cid = ' . ( int ) $intCid . '
							' . $strLeasingAgentCondition . '
							AND cl.lease_status_type_id NOT IN ( ' . CLeaseStatusType::CANCELLED . ' , ' . CLeaseStatusType::PAST . ' )
							AND ca.application_stage_id = ' . CApplicationStage::APPLICATION . '
							AND	ca.application_status_id = ' . CApplicationStatus::COMPLETED . ' 
							AND ca.lease_interval_type_id = ' . CLeaseIntervalType::TRANSFER . '
					) application_transfers
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere . '
					ORDER BY
						' . $strSortBy . '
					OFFSET
						' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;

		return fetchData( $strSql, $objClientDatabase );
	}

	// This function is written for new dashboard

	public static function fetchTransferCompletedApplicationsCountByDashboardFilterByCid( $objDashboardFilter, $intCid, $objClientDatabase, $boolIsDashboardAuditReport = false, $intMaxExecutionTimeOut = 0 ) {

		if( false === valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strLeasingAgentCondition = '';
		$strPriorityWhere = '';

		if( true === valArr( $objDashboardFilter->getTaskPriorities() ) ) {
			$strPriorityWhere = ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )';
		}

		if( true === valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) {
			$strLeasingAgentCondition = ' AND ( ca.leasing_agent_id IN ( ' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ' ) OR ca.leasing_agent_id IS NULL ) ';
		}

		$strSql = ( 0 != $intMaxExecutionTimeOut )? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\';' : '';

		$strSql = 'SELECT
						COUNT( id ) AS' .
		          ( false === $boolIsDashboardAuditReport ? ' count, MAX ' : ' approvals_transfers, property_id, property_name, ' ) . '
						( priority ) AS priority
					FROM (
						SELECT
							ca.id,
							ca.cid,
							ca.lease_id,
							lp.property_id,
							func_format_customer_name ( appt.name_first, appt.name_last ) AS customer_full_name,
							cl.lease_end_date AS lease_end_date,
							COALESCE( cl.building_name || \' - \' || cl.unit_number_cache, cl.building_name, cl.unit_number_cache ) as unit_number,
							cl.property_name,
							cl.primary_customer_id,
							cl.id as notice_lease_id,
							CASE WHEN TRIM( dp.approvals_transfers->>\'urgent_transfer_request_scheduled\' ) <> \'\' AND (dp.approvals_transfers->>\'urgent_transfer_request_scheduled\' )::int <= ( CURRENT_DATE - ( ca.application_datetime::date ) ) THEN
								3
							WHEN TRIM( dp.approvals_transfers->>\'urgent_transfer_request_accepted\' ) <> \'\' AND ca.application_status_id = ' . CApplicationStatus::COMPLETED . ' AND (dp.approvals_transfers->>\'urgent_transfer_request_accepted\' )::int <= ( CURRENT_DATE - ( ca.application_completed_on::date ) ) THEN
								3
							WHEN TRIM( dp.approvals_transfers->>\'important_transfer_request_scheduled\' ) <> \'\' AND (dp.approvals_transfers->>\'important_transfer_request_scheduled\' )::int <= ( CURRENT_DATE - ( ca.application_datetime::date ) ) THEN
								2
							WHEN TRIM( dp.approvals_transfers->>\'important_transfer_request_accepted\' ) <> \'\' AND ca.application_status_id = ' . CApplicationStatus::COMPLETED . ' AND (dp.approvals_transfers->>\'important_transfer_request_accepted\' )::int <= ( CURRENT_DATE - ( ca.application_completed_on::date ) ) THEN
								2
							ELSE
								1
							END as priority
						FROM
							cached_applications ca
							JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) lp ON ( lp.property_id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ?  ' AND lp.is_disabled = 0 ' : '' ) . ' )
							JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' AND aa.deleted_on IS NULL )
							JOIN applicants appt ON ( appt.cid = ca.cid AND appt.id = aa.applicant_id AND appt.cid = aa.cid  )
							JOIN lease_processes lpt ON ( ca.cid = lpt.cid AND lpt.transfer_lease_id = ca.lease_id )
							JOIN cached_leases cl ON ( ca.cid = cl.cid AND lpt.lease_id = cl.id )
							LEFT JOIN dashboard_priorities dp ON dp.cid = ca.cid
						WHERE
							ca.cid = ' . ( int ) $intCid . '
							' . $strLeasingAgentCondition . '
							AND cl.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
							AND ca.application_stage_id = ' . CApplicationStage::APPLICATION . '
							AND	ca.application_status_id = ' . CApplicationStatus::COMPLETED . ' 
							AND ca.lease_interval_type_id = ' . CLeaseIntervalType::TRANSFER . '
					) application_transfers
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere . ' ' .
		          ( true === $boolIsDashboardAuditReport ?
			          'GROUP BY
							property_id,
							property_name,
							priority'
					: '' );

		if( true === $boolIsDashboardAuditReport ) {
			return fetchData( $strSql, $objClientDatabase );
		}

		return fetchOrCacheData( $strSql, $intCacheLifetime = 900, NULL, $objClientDatabase );
	}

	public static function fetchPaginatedPendingRenewalOffersByDashboardFilterByCid( CDashboardFilter $objDashboardFilter, int $intCid, int $intPageSize, string $strSortBy, CDatabase $objClientDatabase, $intOffset ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strLeasingAgentCondition = ( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) ? ' AND ( ca.leasing_agent_id IN ( ' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ' ) OR ca.leasing_agent_id IS NULL ) ' : '';

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSql = 'SELECT
						*
					FROM (
						SELECT
							ca.id,
							ca.cid,
							ca.lease_id,
							ca.property_name,
							lp.property_id,
							appt.customer_id As customer_id,
							util_get_translated( \'name\',lst.name, lst.details ) AS lease_status_type_name,
							CASE WHEN appt.name_last_matronymic IS NOT NULL AND length( appt.name_last_matronymic ) > 0 THEN 
								func_format_customer_name ( appt.name_first, CONCAT ( COALESCE ( appt.name_last, \'\'), \' \', appt.name_last_matronymic ) )
							ELSE
								func_format_customer_name ( appt.name_first, appt.name_last )
							END AS customer_full_name,
							COALESCE( ca.building_name || \' - \' || ca.unit_number_cache, ca.building_name, ca.unit_number_cache, \' - \' ) as unit_number,
							CASE WHEN TRIM( dp.approvals_renewal_offers->>\'urgent_lease_expires_within\' ) <> \'\' AND CURRENT_DATE >= ( ca.lease_end_date + ( dp.approvals_renewal_offers->>\'urgent_lease_expires_within\' )::int ) THEN
								3
							WHEN TRIM( dp.approvals_renewal_offers->>\'urgent_lease_status_type_ids\' ) <> \'\' AND ca.lease_status_type_id = ANY( ( dp.approvals_renewal_offers->>\'urgent_lease_status_type_ids\' )::integer[] ) THEN
								3
							WHEN TRIM( dp.approvals_renewal_offers->>\'important_lease_expires_within\' ) <> \'\' AND CURRENT_DATE >= ( ca.lease_end_date + ( dp.approvals_renewal_offers->>\'important_lease_expires_within\' )::int ) THEN
								2
							WHEN TRIM( dp.approvals_renewal_offers->>\'important_lease_status_type_ids\' ) <> \'\' AND ca.lease_status_type_id = ANY( ( dp.approvals_renewal_offers->>\'important_lease_status_type_ids\' )::integer[] ) THEN
								2
							ELSE
								1
							END as priority,
							q.expires_on AS lease_end_date,
                            RANK() OVER ( PARTITION BY ca.id ORDER BY q.expires_on ASC ) as quote_rank 
						FROM
							cached_applications ca
							JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) lp ON ( lp.property_id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
							JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' AND aa.deleted_on IS NULL )
							JOIN applicants appt ON ( appt.cid = aa.cid AND appt.id = aa.applicant_id )
							JOIN lease_status_types lst ON ( lst.id = ca.lease_status_type_id )
							JOIN quotes q ON ( q.cid = ca.cid AND q.application_id = ca.id ) 
							LEFT JOIN dashboard_priorities dp ON dp.cid = ca.cid
						WHERE
							ca.cid = ' . ( int ) $intCid . '
							' . $strLeasingAgentCondition . '
							AND ca.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . '
							AND ca.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
							AND ca.application_stage_id IN ( ' . CApplicationStage::PRE_APPLICATION . ' , ' . CApplicationStage::APPLICATION . ' )
							AND	ca.application_status_id IN ( ' . CApplicationStatus::PARTIALLY_COMPLETED . ' , ' . CApplicationStatus::STARTED . ' )
					) a_reversals
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere . '
						AND quote_rank = 1 
					ORDER BY
						' . $strSortBy . '
					OFFSET
						' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;

		return fetchData( $strSql, $objClientDatabase );
	}

	// This function is written for new dashboard

	public static function fetchRenewalOffersCountByDashboardFilterByCid( $objDashboardFilter, $intCid, $objClientDatabase, $boolIsDashboardAuditReport = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strLeasingAgentCondition = $strPriorityWhere = '';

		if( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) {
			$strPriorityWhere = ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )';
		}

		if( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) {
			$strLeasingAgentCondition = ' AND ( ca.leasing_agent_id IN ( ' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ' ) OR ca.leasing_agent_id IS NULL ) ';
		}

		$strSql = ( 0 != $intMaxExecutionTimeOut )? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\';' : '';

		$strSql .= 'SELECT
						COUNT( id ) AS' .
						( false == $boolIsDashboardAuditReport ? ' count, MAX ' : ' approvals_renewals, property_id, property_name, ' ) . '
						( priority ) AS priority
					FROM (
						SELECT
							ca.id,
							ca.cid,
							ca.property_id,
							ca.property_name,
							CASE WHEN TRIM( dp.approvals_renewal_offers->>\'urgent_lease_expires_within\' ) <> \'\' AND CURRENT_DATE >= ( la.lease_end_date + ( dp.approvals_renewal_offers->>\'urgent_lease_expires_within\' )::int ) THEN
								3
							WHEN TRIM( dp.approvals_renewal_offers->>\'urgent_lease_interval_type_id\' ) <> \'\' AND TRIM( dp.approvals_renewal_offers->>\'urgent_lease_interval_type_id\' )::int = 1 AND la.is_month_to_month = 1 THEN
								3
							WHEN TRIM( dp.approvals_renewal_offers->>\'urgent_lease_status_type_ids\' ) <> \'\' AND la.lease_status_type_id = ANY( ( dp.approvals_renewal_offers->>\'urgent_lease_status_type_ids\' )::integer[] ) AND la.is_month_to_month <> 1 THEN
								3
							WHEN TRIM( dp.approvals_renewal_offers->>\'important_lease_expires_within\' ) <> \'\' AND CURRENT_DATE >= ( la.lease_end_date + ( dp.approvals_renewal_offers->>\'important_lease_expires_within\' )::int ) THEN
								2
							WHEN TRIM( dp.approvals_renewal_offers->>\'important_lease_interval_type_id\' ) <> \'\' AND TRIM( dp.approvals_renewal_offers->>\'important_lease_interval_type_id\' )::int = 1 AND la.is_month_to_month = 1 THEN
								2
							WHEN TRIM( dp.approvals_renewal_offers->>\'important_lease_status_type_ids\' ) <> \'\' AND la.lease_status_type_id = ANY( ( dp.approvals_renewal_offers->>\'important_lease_status_type_ids\' )::integer[] ) AND la.is_month_to_month <> 1 THEN
								2
							ELSE
								1
							END AS priority
						FROM
							cached_applications ca
							JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) lp ON ( lp.property_id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ?  ' AND lp.is_disabled = 0 ' : '' ) . ' )
							JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' AND aa.deleted_on IS NULL )
							LEFT JOIN cached_leases la ON ( la.cid = ca.cid AND la.id = ca.lease_id )
							LEFT JOIN dashboard_priorities dp ON dp.cid = ca.cid
						WHERE
							ca.cid = ' . ( int ) $intCid . '
							' . $strLeasingAgentCondition . '
							AND la.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
							AND ca.application_stage_id  IN ( ' . CApplicationStage::PRE_APPLICATION . ' , ' . CApplicationStage::APPLICATION . ' )
							AND	ca.application_status_id  = ' . CApplicationStatus::COMPLETED . '
							AND ca.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . '
					) a_renewals
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere . '									' .
					( true == $boolIsDashboardAuditReport ?
						'GROUP BY
							property_id,
							property_name,
							priority'
					: '' );
		if( true == $boolIsDashboardAuditReport ) {
			return fetchData( $strSql, $objClientDatabase );
		}

		return fetchOrCacheData( $strSql, $intCacheLifetime = 900, NULL, $objClientDatabase );
	}

	public static function fetchPendingRenewalOffersCountByDashboardFilterByCid( $objDashboardFilter, $intCid, $objClientDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strLeasingAgentCondition = '';
		$strPriorityWhere = '';

		if( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) {
			$strPriorityWhere = ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )';
		}

		if( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) {
			$strLeasingAgentCondition = ' AND ( a.leasing_agent_id IN ( ' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ' ) OR a.leasing_agent_id IS NULL ) ';
		}

		$strSql = ( 0 != $intMaxExecutionTimeOut )? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\';' : '';

		$strSql .= 'DROP TABLE IF EXISTS pg_temp.loaded_properties;
					CREATE TEMP TABLE loaded_properties AS (
						SELECT property_id
						FROM load_properties( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] )
						' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? 'WHERE is_disabled = 0' : '' ) . ' 
					);
					ANALYZE pg_temp.loaded_properties;
					SELECT
						COUNT( id ) AS ' .
				   ( false == $boolIsGroupByProperties ? 'count, MAX ' : ' residents_renewals, property_id, property_name, ' ) . '
						( priority ) AS priority
					FROM (
						SELECT
							a.id,
							a.cid,
							a.property_id,
							a.property_name,
							CASE WHEN TRIM( dp.approvals_renewal_offers->>\'urgent_lease_expires_within\' ) <> \'\' AND CURRENT_DATE >= ( a.lease_end_date + ( dp.approvals_renewal_offers->>\'urgent_lease_expires_within\' )::int ) THEN
								3
							WHEN TRIM( dp.approvals_renewal_offers->>\'urgent_lease_interval_type_id\' ) <> \'\' AND TRIM( dp.approvals_renewal_offers->>\'urgent_lease_interval_type_id\' )::int = 1 AND a.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . ' THEN
								3
							WHEN TRIM( dp.approvals_renewal_offers->>\'urgent_lease_status_type_ids\' ) <> \'\' AND a.lease_status_type_id = ANY( ( dp.approvals_renewal_offers->>\'urgent_lease_status_type_ids\' )::integer[] ) AND a.lease_interval_type_id <> ' . CLeaseIntervalType::MONTH_TO_MONTH . ' THEN
								3
							WHEN TRIM( dp.approvals_renewal_offers->>\'important_lease_expires_within\' ) <> \'\' AND CURRENT_DATE >= ( a.lease_end_date + ( dp.approvals_renewal_offers->>\'important_lease_expires_within\' )::int ) THEN
								2
							WHEN TRIM( dp.approvals_renewal_offers->>\'important_lease_interval_type_id\' ) <> \'\' AND TRIM( dp.approvals_renewal_offers->>\'important_lease_interval_type_id\' )::int = 1 AND a.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . ' THEN
								2
							WHEN TRIM( dp.approvals_renewal_offers->>\'important_lease_status_type_ids\' ) <> \'\' AND a.lease_status_type_id = ANY( ( dp.approvals_renewal_offers->>\'important_lease_status_type_ids\' )::integer[] ) AND a.lease_interval_type_id <> ' . CLeaseIntervalType::MONTH_TO_MONTH . ' THEN
								2
							ELSE
								1
							END AS priority,
							RANK() OVER ( PARTITION BY a.id ORDER BY q.expires_on ASC ) as quote_rank
						FROM
							cached_applications a
							JOIN loaded_properties lp ON lp.property_id = a.property_id
							JOIN applicant_applications aa ON ( aa.cid = a.cid AND aa.application_id = a.id AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' AND aa.deleted_on IS NULL )
							LEFT JOIN dashboard_priorities dp ON dp.cid = a.cid
							JOIN quotes q ON a.cid = q.cid AND a.id = q.application_id
						WHERE
							a.cid = ' . ( int ) $intCid . '
							' . $strLeasingAgentCondition . '
							AND a.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
							AND a.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . '
							AND a.application_stage_id IN ( ' . CApplicationStage::PRE_APPLICATION . ' , ' . CApplicationStage::APPLICATION . ' )
							AND	a.application_status_id IN ( ' . CApplicationStatus::STARTED . ' ,  ' . CApplicationStatus::PARTIALLY_COMPLETED . ' )
					) a_renewals
					WHERE
						cid = ' . ( int ) $intCid . '
						AND quote_rank = 1
						' . $strPriorityWhere . '									' .
				   ( true == $boolIsGroupByProperties ?
					   'GROUP BY
							property_id,
							property_name,
							priority'
					   : '' );

		return fetchData( $strSql, $objClientDatabase );
	}

	// This function is written for new dashboard

	public static function fetchPaginatedRejectedRenewalOffersByDashboardFilterByCid( $objDashboardFilter, $intCid, $intOffset, $intPageSize, $strSortBy, $objClientDatabase ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strLeasingAgentCondition = '';

		if( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) {
			$strLeasingAgentCondition = ' AND ( ca.leasing_agent_id IN ( ' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ' ) OR ca.leasing_agent_id IS NULL ) ';
		}

		$strSql = '	SELECT *
					FROM (
						SELECT
							ca.id AS application_id,
							CASE WHEN appt.name_last_matronymic IS NOT NULL AND length( appt.name_last_matronymic ) > 0 THEN 
								func_format_customer_name ( appt.name_first, CONCAT ( COALESCE ( appt.name_last, \'\'), \' \', appt.name_last_matronymic ) )
							ELSE
								func_format_customer_name ( appt.name_first, appt.name_last )
							END AS customer_full_name,
							CASE
								WHEN la.lease_end_date = DATE_TRUNC ( \'day\', NOW ( ) ) THEN \'Today\'
								ELSE to_char ( la.lease_end_date, \'MM/DD/YY\' )
							END AS lease_end_date,
							COALESCE( la.building_name || \' - \' || la.unit_number_cache, la.building_name, la.unit_number_cache ) as unit_number,
							la.property_name,
							CASE
								WHEN li.lease_interval_type_id <> ' . CLeaseIntervalType::MONTH_TO_MONTH . '
								THEN util_get_translated( \'name\', lst.name, lst.details , \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' )
								ELSE \'' . __( 'Month-to-Month' ) . '\'
							END AS lease_status_type_name,
							ca.cid,
							CASE WHEN TRIM( dp.residents_renewals->>\'urgent_lease_expires_within\' ) != \'\' AND ( CURRENT_DATE + ( dp.residents_renewals->>\'urgent_lease_expires_within\' )::int ) > la.lease_end_date THEN
								3
							WHEN TRIM( dp.residents_renewals->>\'urgent_lease_status_type_ids\' ) != \'\' AND lst.id = ANY ( ( dp.residents_renewals->>\'urgent_lease_status_type_ids\' )::integer[] ) AND la.is_month_to_month <> 1 THEN
								3
							WHEN TRIM( dp.residents_renewals->>\'urgent_lease_interval_type_id\' ) <> \'\' AND TRIM( dp.residents_renewals->>\'urgent_lease_interval_type_id\' )::int = 1 AND li.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . ' THEN
								3
							WHEN TRIM( dp.residents_renewals->>\'important_lease_expires_within\' ) != \'\' AND ( CURRENT_DATE + ( dp.residents_renewals->>\'important_lease_expires_within\' )::int ) > la.lease_end_date THEN
								2
							WHEN TRIM( dp.residents_renewals->>\'important_lease_status_type_ids\' ) != \'\' AND lst.id = ANY ( ( dp.residents_renewals->>\'important_lease_status_type_ids\' )::integer[] ) AND la.is_month_to_month <> 1 THEN
								2
							WHEN TRIM( dp.residents_renewals->>\'important_lease_interval_type_id\' ) <> \'\' AND TRIM( dp.residents_renewals->>\'important_lease_interval_type_id\' )::int = 1 AND li.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . ' THEN
								2
							ELSE
								1
							END as priority
						FROM
							cached_applications ca
							JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ], array[  ' . CPsProduct::ENTRATA . ', ' . CPsProduct::LEASE_EXECUTION . ' ] ) lp ON ( lp.property_id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
							JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' AND aa.deleted_on IS NULL )
							JOIN applicants appt ON ( appt.cid = ca.cid AND appt.id = aa.applicant_id AND appt.cid = aa.cid  )
							JOIN cached_leases la ON ( la.cid = ca.cid AND la.id = ca.lease_id )
							JOIN lease_intervals li ON ( li.cid = la.cid AND li.id = la.active_lease_interval_id )
							JOIN lease_status_types lst ON ( lst.id = la.lease_status_type_id )
							LEFT JOIN dashboard_priorities dp ON dp.cid = ca.cid
						WHERE
							ca.cid = ' . ( int ) $intCid . '
							' . $strLeasingAgentCondition . '
							AND la.occupancy_type_id IN ( ' . implode( ',', COccupancyType::$c_arrintIncludedOccupancyTypes ) . ' )
							AND la.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
							AND ca.application_stage_id  = ' . CApplicationStage::PRE_APPLICATION . '
							AND	ca.application_status_id  = ' . CApplicationStatus::CANCELLED . '
							AND ca.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . '
							AND ca.lease_status_type_id = ' . CLeaseStatusType::APPLICANT . '
					) AS applications
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere . '
					ORDER BY
						' . $strSortBy . '
					OFFSET
						' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;

		return fetchData( $strSql, $objClientDatabase );
	}

	// This function is written for new dashboard

	public static function fetchRejectedRenewalOffersCountByDashboardFilterByCid( $objDashboardFilter, $intCid, $objClientDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strLeasingAgentCondition = '';

		if( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) {
			$strLeasingAgentCondition = ' AND ( ca.leasing_agent_id IN ( ' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ' ) OR ca.leasing_agent_id IS NULL ) ';
		}

		$strSql = ( 0 != $intMaxExecutionTimeOut )? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';

		$strSql .= 'SELECT
						COUNT( id ) AS ' .
							( false == $boolIsGroupByProperties ?
									' count, MAX'
									:
									' residents_renewals,
									  property_id,
									  property_name,
						' ) . '
						(priority) AS priority
					FROM (
						SELECT *
						FROM (
								SELECT ' .
									( true == $boolIsGroupByProperties ? '
										ca.property_id,
										ca.property_name, '
									: '' ) . '
									ca.id,
									ca.cid,
									CASE
										WHEN TRIM( dp.residents_renewals->>\'urgent_lease_expires_within\' ) != \'\' AND ( CURRENT_DATE + ( dp.residents_renewals->>\'urgent_lease_expires_within\' )::int ) > cl.lease_end_date THEN
											3
										WHEN TRIM( dp.residents_renewals->>\'urgent_lease_status_type_ids\' ) != \'\' AND cl.lease_status_type_id = ANY ( ( dp.residents_renewals->>\'urgent_lease_status_type_ids\' )::integer[] ) AND cl.is_month_to_month <> 1 THEN
											3
										WHEN TRIM( dp.residents_renewals->>\'urgent_lease_interval_type_id\' ) <> \'\' AND TRIM( dp.residents_renewals->>\'urgent_lease_interval_type_id\' )::int = 1 AND ca.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . ' THEN
											3
										WHEN TRIM( dp.residents_renewals->>\'important_lease_expires_within\' ) != \'\' AND ( CURRENT_DATE + ( dp.residents_renewals->>\'important_lease_expires_within\' )::int ) > cl.lease_end_date THEN
											2
										WHEN TRIM( dp.residents_renewals->>\'important_lease_status_type_ids\' ) != \'\' AND cl.lease_status_type_id = ANY ( ( dp.residents_renewals->>\'important_lease_status_type_ids\' )::integer[] ) AND cl.is_month_to_month <> 1 THEN
											2
										WHEN TRIM( dp.residents_renewals->>\'important_lease_interval_type_id\' ) <> \'\' AND TRIM( dp.residents_renewals->>\'important_lease_interval_type_id\' )::int = 1 AND li.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . ' THEN
											2
										ELSE
											1
									END AS priority
								FROM
									cached_applications ca
									JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ], array[  ' . CPsProduct::ENTRATA . ', ' . CPsProduct::LEASE_EXECUTION . ' ] ) lp ON ( lp.property_id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
									JOIN cached_leases cl ON ( cl.cid = ca.cid AND cl.id = ca.lease_id )
									JOIN lease_intervals li ON ( li.cid = cl.cid AND li.id = cl.active_lease_interval_id )
									LEFT JOIN dashboard_priorities dp ON dp.cid = ca.cid
								WHERE
									ca.cid = ' . ( int ) $intCid . '
									' . $strLeasingAgentCondition . '
									AND cl.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
									AND ca.application_stage_id  = ' . CApplicationStage::PRE_APPLICATION . '
									AND ca.application_status_id  = ' . CApplicationStatus::CANCELLED . '
									AND ca.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . '
									AND ca.lease_status_type_id = ' . CLeaseStatusType::APPLICANT . '
							) applications
						WHERE
							cid = ' . ( int ) $intCid . '
							' . $strPriorityWhere .
						( false == $boolIsGroupByProperties ? '
						ORDER BY
							priority DESC
						LIMIT
							100 '
						: '' ) . '
					) rejected_offers
					WHERE
						cid = ' . ( int ) $intCid .
					( true == $boolIsGroupByProperties ? '
						GROUP BY
							property_id,
							property_name,
							priority'
						: '' );

		return fetchData( $strSql, $objClientDatabase );
	}

	// This function is written for entrata new dashboard

	public static function fetchPaginatedMixedApplicationsByDashboardFilterByCid( $objDashboardFilter, $intCid, $intOffset, $intLimit, $strSortBy, $objDatabase ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) {
			return NULL;
		}

		$arrintLeaseIntervalTypes = [ CLeaseIntervalType::APPLICATION, CLeaseIntervalType::LEASE_MODIFICATION ];

		$strLeasingAgentCondition = '';
		$strPriorityWhere         = '';

		if( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) {
			$strLeasingAgentCondition .= ' AND ( ce.id IS NULL OR ce.id IN (' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ') )';
		}

		if( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) {
			$strPriorityWhere = ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )';
		}

		$strPropertyPrefs = 'temp_property_prefs_' . rand( 1, 99999 );
		$strPropertyTransmissionVendors = 'temp_property_transmission_vendors_' . rand( 1, 99999 );
		$strCompanyAppPrefs = 'temp_company_app_prefs_' . rand( 1, 99999 );

		$strSql = '
			CREATE TEMP TABLE lp AS(
				SELECT
					cid,
					property_id
				FROM
					load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ']' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ', NULL, true ' : '' ) . ' )
			);
			
			ANALYZE pg_temp.lp;
			
			CREATE TEMP TABLE ' . $strPropertyPrefs . ' ON COMMIT DROP AS(

				SELECT
                    p.cid,
					p.id,
					p.property_name,
					p.remote_primary_key,
                    ( SELECT pp_1.id FROM property_preferences pp_1 WHERE  pp_1.cid = ' . ( int ) $intCid . ' AND pp_1.property_id = lp.property_id AND pp_1.value IS NOT NULL AND pp_1.key = \'DONT_EXPORT_APPLICATION_CHARGES\' ) AS pp_1_id,
					( SELECT pp_2.id FROM property_preferences pp_2 WHERE  pp_2.cid = ' . ( int ) $intCid . ' AND pp_2.property_id = lp.property_id AND pp_2.value IS NOT NULL AND pp_2.key = \'DONT_EXPORT_APPLICATIONS\' ) AS pp_2_id,
					( SELECT pp_3.id FROM property_preferences pp_3 WHERE  pp_3.cid = ' . ( int ) $intCid . ' AND pp_3.property_id = lp.property_id AND pp_3.value IS NOT NULL AND pp_3.key = \'DONT_EXPORT_GUEST_CARDS\' ) AS pp_3_id,
                    ( SELECT pp_4.id FROM property_preferences pp_4 WHERE  pp_4.cid = ' . ( int ) $intCid . ' AND pp_4.property_id = lp.property_id AND pp_4.value IS NOT NULL AND pp_4.key = \'SCREEN_AFTER_LEASE_COMPLETED\' ) AS pp_4_id
				FROM
					pg_temp.lp
                    JOIN properties p ON p.id = lp.property_id
			);
			
			ANALYZE pg_temp.' . $strPropertyPrefs . ';
			
			CREATE TEMP TABLE ' . $strPropertyTransmissionVendors . ' ON COMMIT DROP AS(
            
				SELECT 
                	DISTINCT ON ( ptv.property_id )
					ptv.cid,
					ptv.property_id,
					CASE WHEN ctv.transmission_vendor_id = ' . CTransmissionVendor::RESIDENT_VERIFY . ' THEN 1 ELSE 0 END AS is_resident_verify,
					ptv.id
				FROM
					company_transmission_vendors ctv
					JOIN property_transmission_vendors ptv ON ptv.cid = ctv.cid AND ptv.company_transmission_vendor_id = ctv.id
				WHERE
					ctv.cid = ' . ( int ) $intCid . '
					AND ctv.transmission_type_id = ' . CTransmissionType::SCREENING . '
					AND EXISTS ( SELECT 1 FROM pg_temp.lp WHERE property_id = ptv.property_id )
				ORDER BY
					ptv.property_id,
					is_resident_verify DESC,
					id DESC
			);
			
			ANALYZE pg_temp.' . $strPropertyTransmissionVendors . ';
			
			CREATE TEMP TABLE ' . $strCompanyAppPrefs . ' ON COMMIT DROP AS(
			
				SELECT
					pap.cid,
					pap.property_id,
					pap.company_application_id
				FROM
					property_application_preferences AS pap
				WHERE
					pap.cid = ' . ( int ) $intCid . '
                    AND EXISTS ( SELECT 1 FROM pg_temp.lp WHERE property_id = pap.property_id )
					AND pap.key = \'HIDE_OPTION_PET\'
			);
			
			ANALYZE pg_temp.' . $strCompanyAppPrefs . ';
			
			SELECT
				apps.*
			FROM (
				SELECT DISTINCT ON ( ca.id )
					ca.id AS application_id,
					p.cid,
					appt.id AS applicant_id,
					func_format_customer_name( appt.name_first, appt.name_last ) AS applicant_name,
                    ( SELECT cpn.phone_number FROM customer_phone_numbers cpn WHERE cpn.cid = appt.cid AND appt.customer_id = cpn.customer_id AND cpn.phone_number_type_id = 2 AND cpn.deleted_by IS NULL and cpn.deleted_on IS NULL ORDER BY cpn.created_on DESC LIMIT 1 ) AS phone_number,
					ca.lease_start_date AS move_in_date,
					func_format_unit_number( NULL, NULL, ca.building_name, ca.unit_number_cache ) AS unit_number,
					ca.application_completed_on,
					ca.application_datetime,
					ca.lease_id,
					ca.guest_remote_primary_key,
					ca.app_remote_primary_key,
					ca.lease_interval_type_id,
					aa.completed_on AS primary_applicant_completed_on,
					p.property_name,
					p.id AS property_id,
					p.remote_primary_key AS property_remote_primary_key,
                    ( SELECT concat_ws( \'\', ce.name_first, ce.name_last ) FROM company_employees ce WHERE ce.cid = ca.cid AND ce.id = ca.leasing_agent_id ) AS agent_name,
					screening_sub.transmission_response_type_id,
					screening_sub.screening_result,
					p.pp_1_id AS dont_export_application_charges,
					p.pp_2_id AS dont_export_applications,
					p.pp_3_id AS dont_export_guest_cards,
					p.pp_4_id AS screen_after_lease_completed,
					ac.id AS ar_transaction_id,
					ptv.property_id AS has_screening_vendor,
					COALESCE( ptv.is_resident_verify, 0 ) AS is_resident_verify,
					sapr.screening_recommendation_type_id,
					CASE
						WHEN TRUE = ca.is_pet_policy_conflicted AND NOT EXISTS(
							SELECT
								NULL
							FROM
								' . $strCompanyAppPrefs . ' cte
							WHERE
								cte.cid = ca.cid
								AND cte.property_id = ca.property_id
								AND cte.company_application_id = ca.company_application_id
						)
						THEN 1
						ELSE 0
					END AS is_pet_policy_conflicted,
					
					CASE
						WHEN TRIM( dp.approvals_applications ->> \'urgent_application_completed_since\') <> \'\' AND CURRENT_DATE >= ( ca.application_completed_on::DATE + ( dp.approvals_applications->>\'urgent_application_completed_since\' )::INT ) THEN
							3
						WHEN TRIM( dp.approvals_applications ->> \'urgent_transmission_response_type_ids\') <> \'\' AND transmission_response_type_id = ANY ( ( dp.approvals_applications->>\'urgent_transmission_response_type_ids\' )::INT[] ) THEN
							3
						WHEN TRIM( dp.approvals_applications ->> \'urgent_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.approvals_applications->>\'urgent_move_in_date_within\' )::INT ) >= ca.lease_start_date THEN
							3
						WHEN TRIM( dp.approvals_applications ->> \'important_transmission_response_type_ids\') <> \'\' AND transmission_response_type_id = ANY ( ( dp.approvals_applications->>\'important_transmission_response_type_ids\' )::INT[] ) THEN
							2
						WHEN TRIM( dp.approvals_applications ->> \'important_application_completed_since\') <> \'\' AND NOW() >= ( ca.application_completed_on::DATE + ( dp.approvals_applications->>\'important_application_completed_since\' )::INT ) THEN
							2
						WHEN TRIM( dp.approvals_applications ->> \'important_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.approvals_applications->>\'important_move_in_date_within\' )::INT ) >= ca.lease_start_date THEN
							2
						ELSE
							1
					END AS priority,
						CASE 
							WHEN ca.occupancy_type_id = ' . \COccupancyType::AFFORDABLE . '
							THEN 
								1
							ELSE 
								0
							END as is_affordable_application
					FROM
					cached_applications ca
					JOIN applicant_applications aa ON aa.cid = ca.cid AND aa.application_id = ca.id AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' AND aa.deleted_on IS NULL
					JOIN applicants appt ON appt.cid = aa.cid AND appt.id = aa.applicant_id
					
					JOIN ' . $strPropertyPrefs . ' p ON p.cid = ca.cid AND p.id = ca.property_id

					/* need to figure out if ar_transactions and ar_codes joins are actually needed */
					LEFT JOIN ar_transactions at ON ( at.cid = ca.cid AND at.lease_id = ca.lease_id AND at.remote_primary_key IS NULL AND at.customer_id IS NOT NULL AND at.property_id = ca.property_id )
					LEFT JOIN ar_codes ac ON ac.cid = at.cid AND ac.id = at.ar_code_id AND ac.default_ar_code_id IS DISTINCT FROM ' . CDefaultArCode::PAYMENT . '
					LEFT JOIN dashboard_priorities dp ON dp.cid = ca.cid
					LEFT JOIN screening_application_requests sapr ON sapr.cid = ca.cid AND sapr.application_id = ca.id
				    LEFT JOIN screening_application_condition_sets sacs ON sacs.cid = sapr.cid AND sacs.screening_application_request_id = sapr.id AND sacs.application_id = ca.id AND sacs.is_active = 1
					LEFT JOIN ' . $strPropertyTransmissionVendors . ' ptv ON ptv.cid = ca.cid AND ptv.property_id = ca.property_id
					LEFT JOIN LATERAL (
						SELECT
							aat.applicant_application_id,
							aat.transmission_response_type_id,
							util_get_translated( \'name\', trt.name, trt.details, \'' . \CLocaleContainer::createService()->getLocaleCode() . '\' ) AS screening_result
						FROM
							applicant_application_transmissions aat
							JOIN transmission_response_types trt ON trt.id = aat.transmission_response_type_id
						WHERE
							aat.cid = ' . ( int ) $intCid . '
							AND aat.applicant_application_id = aa.id
							AND aat.application_id = aa.application_id
						ORDER BY
							 aat.id DESC
						LIMIT 1
					) screening_sub ON true
				WHERE					
					ca.cid = ' . ( int ) $intCid . '
					AND ca.application_stage_id = ' . CApplicationStage::APPLICATION . '
					AND ca.application_status_id = ' . CApplicationStatus::COMPLETED . '
					AND ca.lease_interval_type_id <> ' . CLeaseIntervalType::TRANSFER . '
					' . $strLeasingAgentCondition . '
					AND CASE
                            WHEN ptv.id IS NULL THEN true
                            WHEN ptv.is_resident_verify <> 1 AND p.pp_4_id IS NULL THEN
                                screening_sub.applicant_application_id IS NOT NULL
                                AND ca.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::APPLICATION . '
                            WHEN ptv.is_resident_verify = 1 AND sapr.screening_vendor_id = ' . CTransmissionVendor::RESIDENT_VERIFY . ' AND p.pp_4_id IS NULL THEN
                                sapr.id IS NOT NULL
                                AND ca.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypes ) . ' )
                                AND CASE 
                                    WHEN sacs.id IS NOT NULL AND sacs.is_active = 1 THEN
                                       sacs.satisfied_on IS NOT NULL                                
                                AND sapr.screening_decision_type_id = ' . CScreeningDecisionType::APPROVE_WITH_CONDITIONS . '
                                    END
                            WHEN ptv.is_resident_verify <> 1 AND p.pp_4_id IS NOT NULL THEN
                                screening_sub.applicant_application_id IS NULL
                                AND ca.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::APPLICATION . '
                            WHEN ptv.is_resident_verify = 1 AND p.pp_4_id IS NOT NULL THEN
                                sapr.id IS NULL
                                AND ca.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypes ) . ' )   
                         END
					) apps
				WHERE
					cid = ' . ( int ) $intCid . '
					' . $strPriorityWhere . '
				ORDER BY ' . $strSortBy . '
				OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	// This function is written for entrata new dashboard

	public static function fetchApplicationsCountByDashboardFilterByCidByPropertyTypes( $objDashboardFilter, $intCid, $objDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) {
			return 0;
		}

		$strLeasingAgentCondition = '';
		$strPriorityWhere         = '';

		if( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) {
			$strLeasingAgentCondition .= ' AND ( ca.leasing_agent_id IS NULL OR ca.leasing_agent_id IN (' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ') )';
		}

		if( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) {
			$strPriorityWhere = ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )';
		}

		$arrintLeaseIntervalTypes = [ CLeaseIntervalType::APPLICATION, CLeaseIntervalType::LEASE_MODIFICATION ];

		$strSql = ( 0 != $intMaxExecutionTimeOut ) ? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\';' : '';

		$strSql = 'SELECT
						COUNT( id ) ' .
		          ( false == $boolIsGroupByProperties ? ', MAX' : 'AS approvals_applications, property_id, property_name, ' ) . '
						( priority ) AS priority
					FROM (
						SELECT
							*
						FROM (
							SELECT
								DISTINCT ON ( ca.id )
								ca.id,
								ca.cid,
								CASE
									WHEN TRIM( dp.approvals_applications ->> \'urgent_application_completed_since\') <> \'\' AND CURRENT_DATE >= ( ca.application_completed_on::date + ( dp.approvals_applications->>\'urgent_application_completed_since\' )::int ) THEN
										3
									WHEN TRIM( dp.approvals_applications ->> \'urgent_transmission_response_type_ids\') <> \'\' AND transmission_response_type_id = ANY ( ( dp.approvals_applications->>\'urgent_transmission_response_type_ids\' )::integer[] ) THEN
										3
									WHEN TRIM( dp.approvals_applications ->> \'urgent_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.approvals_applications->>\'urgent_move_in_date_within\' )::int ) >= ca.lease_start_date THEN
										3
									WHEN TRIM( dp.approvals_applications ->> \'important_transmission_response_type_ids\') <> \'\' AND transmission_response_type_id = ANY ( ( dp.approvals_applications->>\'important_transmission_response_type_ids\' )::integer[] ) THEN
										2
									WHEN TRIM( dp.approvals_applications ->> \'important_application_completed_since\') <> \'\' AND NOW() >= ( ca.application_completed_on::date + ( dp.approvals_applications->>\'important_application_completed_since\' )::int ) THEN
										2
									WHEN TRIM( dp.approvals_applications ->> \'important_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.approvals_applications->>\'important_move_in_date_within\' )::int ) >= ca.lease_start_date THEN
										2
									ELSE
										1
								END AS priority,
								ca.property_id,
								ca.property_name
							FROM
								cached_applications ca
								JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . '] ) lp ON lp.property_id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . '
								JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' AND aa.deleted_on IS NULL )
								JOIN applicants appt ON ( appt.cid = aa.cid AND appt.id = aa.applicant_id )
								LEFT JOIN dashboard_priorities dp ON dp.cid = ca.cid
								LEFT JOIN (
											SELECT
												ptv.id,
												ctv.cid,
												ptv.property_id,
												ptv.company_transmission_vendor_id,
												CASE 
												WHEN ctv.transmission_vendor_id = ' . CTransmissionVendor::RESIDENT_VERIFY . ' THEN 1
												ELSE 0
												END as is_resident_verify
											FROM
												company_transmission_vendors ctv
												JOIN property_transmission_vendors ptv ON ( ptv.cid = ctv.cid AND ptv.company_transmission_vendor_id = ctv.id )
											WHERE
												ctv.cid = ' . ( int ) $intCid . '
												AND  ctv.transmission_type_id = ' . CTransmissionType::SCREENING . '
								) ptv ON ( ptv.cid = ca.cid AND ptv.property_id = ca.property_id )
								LEFT JOIN property_preferences pp ON ( pp.cid = ca.cid AND pp.property_id = ca.property_id AND pp.value <> \'0\' AND pp.key = \'SCREEN_AFTER_LEASE_COMPLETED\' )
								LEFT JOIN screening_application_requests sapr ON ( ca.cid = sapr.cid AND ca.id = sapr.application_id )
						        LEFT JOIN screening_applicant_results sar ON ( ca.cid = sar.cid AND appt.id = sar.applicant_id AND ca.id = sar.application_id )
						        LEFT JOIN screening_application_condition_sets sacs ON ( ca.cid = sacs.cid AND sacs.screening_application_request_id = sapr.id AND ca.id = sacs.application_id AND sacs.is_active = 1)
                                LEFT JOIN applicant_application_transmissions aat ON ( aat.cid = aa.cid AND aat.applicant_application_id = aa.id AND aat.application_id = ca.id )								
							WHERE
								ca.cid = ' . ( int ) $intCid . '
								AND ca.application_stage_id  = ' . CApplicationStage::APPLICATION . '
								AND	ca.application_status_id  = ' . CApplicationStatus::COMPLETED . '
							AND (
                            CASE 
                            	WHEN ptv.id IS NULL THEN
                                    ptv.id IS NULL
                                    AND ca.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::APPLICATION . '
                                WHEN ptv.id IS NOT NULL AND ptv.is_resident_verify <> 1 AND pp.id IS NULL THEN
                                    aat.applicant_application_id IS NOT NULL
                                    AND ca.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::APPLICATION . '
                                WHEN ptv.id IS NOT NULL AND ptv.is_resident_verify = 1 AND sapr.screening_vendor_id = ' . CTransmissionVendor::RESIDENT_VERIFY . ' AND pp.id IS NULL THEN
                                    sapr.id IS NOT NULL
                                    AND ca.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypes ) . ' )
                                    AND CASE 
                                        WHEN sacs.id IS NOT NULL AND sacs.is_active = 1 THEN
                                           sacs.satisfied_on IS NOT NULL
                                           AND sapr.screening_decision_type_id = ' . CScreeningDecisionType::APPROVE_WITH_CONDITIONS . '
                                        END
                                WHEN ptv.id IS NOT NULL AND ptv.is_resident_verify <> 1 AND pp.id IS NOT NULL  THEN
                                    aat.applicant_application_id IS NULL
                                    AND ca.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::APPLICATION . '
                                WHEN ptv.id IS NOT NULL AND ptv.is_resident_verify = 1 AND pp.id IS NOT NULL THEN
                                    sapr.id IS NULL
                                    AND ca.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypes ) . ' )   
                             END  
								)
								' . $strLeasingAgentCondition . '
						) applications
						WHERE
							cid = ' . ( int ) $intCid . '
							' . $strPriorityWhere . ' ' .
		          ( false == $boolIsGroupByProperties ?
			          ' ORDER BY
							priority DESC
						LIMIT 100'
			          :
			          '' ) . '
					) application_count
					WHERE
						cid = ' . ( int ) $intCid . ' ' .

		          ( true == $boolIsGroupByProperties ?
			          ' GROUP BY
						property_id,
						property_name,
						priority'
			          :
			          '' );

		if( true == $boolIsGroupByProperties ) {
			return fetchData( $strSql, $objDatabase );
		}

		$arrmixData = fetchOrCacheData( $strSql, $intCacheLifetime = 900, NULL, $objDatabase );
		return ( true == isset( $arrmixData[0] ) ) ? $arrmixData[0] : [ 'count' => 0, 'priority' => 1 ];
	}

	// This function is written for entrata new dashboard

	public static function fetchPaginatedInsuranceMissingApplicationsByDashboardFilterByCid( $objDashboardFilter, $intCid, $intOffset, $intLimit, $strSortBy, $objDatabase ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strLeasingAgentCondition = ( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) ? ' AND ( ca.leasing_agent_id IS NULL OR ca.leasing_agent_id IN (' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ') )' : '';

		$strSql = 'SELECT
						*
					FROM (
						SELECT
							ca.id,
							ca.cid,
							ca.lease_start_date as move_in_date,
							COALESCE( ca.building_name || \' - \' || ca.unit_number_cache, ca.building_name, ca.unit_number_cache ) as unit_number,
							ca.lease_id,
							func_format_customer_name( appt.name_first, appt.name_last ) as applicant_name,
							appt.username AS email_address,
							cpn.phone_number,
							p.id as property_id,
							p.property_name,
							rip.id as policy_id,
							rip.policy_number,
							rip.insurance_carrier_name,
							CASE
								WHEN TRIM( dp.applicants_missing_insurance->>\'urgent_resident_movedin\') <> \'\' AND 1 = TRIM( dp.applicants_missing_insurance ->> \'urgent_resident_movedin\')::int AND CURRENT_DATE > ca.lease_start_date THEN
									3
								WHEN TRIM( dp.applicants_missing_insurance->>\'urgent_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.applicants_missing_insurance->>\'urgent_move_in_date_within\' )::int ) >= ca.lease_start_date THEN
									3
								WHEN TRIM( dp.applicants_missing_insurance->>\'urgent_lease_completed_since\') <> \'\' AND ( CURRENT_DATE - ( dp.applicants_missing_insurance->>\'urgent_lease_completed_since\' )::int ) >= ca.lease_completed_on::date THEN
									3
								WHEN TRIM( dp.applicants_missing_insurance->>\'important_resident_movedin\') <> \'\' AND 1 = TRIM( dp.applicants_missing_insurance ->> \'important_resident_movedin\')::int AND CURRENT_DATE > ca.lease_start_date THEN
									2
								WHEN TRIM( dp.applicants_missing_insurance->>\'important_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.applicants_missing_insurance->>\'important_move_in_date_within\' )::int ) >= ca.lease_start_date THEN
									2
								WHEN TRIM( dp.applicants_missing_insurance->>\'important_lease_completed_since\') <> \'\' AND ( CURRENT_DATE - ( dp.applicants_missing_insurance->>\'important_lease_completed_since\' )::int ) >= ca.lease_completed_on::date THEN
									2
								ELSE
									1
							END as priority,
							CASE
					            WHEN pp1.key = \'PAID_VERIFICATION\' AND pp1.value = \'1\' THEN TRUE
					            ELSE FALSE
				            END AS is_paid_verifcation_on
						FROM
							cached_applications ca
							JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . '] ) lp ON lp.property_id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ?  ' AND lp.is_disabled = 0 ' : '' ) . '
							JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' AND aa.deleted_on IS NULL )
							JOIN applicants appt ON ( appt.cid = aa.cid AND appt.id = aa.applicant_id )
							JOIN properties p ON ( p.cid = ca.cid AND p.id = ca.property_id )
							JOIN property_preferences pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND key = \'REQUIRE_RENTERS_INSURANCE_AFTER_LEASE_SIGNED\' AND value IS NOT NULL )
							LEFT JOIN property_preferences pp1 ON ( pp1.cid = p.cid AND pp1.property_id = p.id AND pp1.key = \'PAID_VERIFICATION\' )
							LEFT JOIN insurance_policy_customers ipc ON ( ca.cid = ipc.cid AND ca.lease_id = ipc.lease_id )
							LEFT JOIN resident_insurance_policies rip ON ( rip.cid = ipc.cid AND rip.id = ipc.resident_insurance_policy_id )
							LEFT JOIN dashboard_priorities dp ON dp.cid = ca.cid
							LEFT JOIN customer_phone_numbers cpn ON (appt.cid = cpn.cid AND appt.customer_id = cpn.customer_id AND cpn.is_primary = true AND cpn.deleted_by IS NULL )
						WHERE
							ca.cid = ' . ( int ) $intCid . '
							AND rip.confirmed_on IS NULL
							AND ca.application_stage_id  = ' . CApplicationStage::LEASE . '
							AND	ca.application_status_id  = ' . CApplicationStatus::COMPLETED . '
							AND ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '
							' . $strLeasingAgentCondition . '
					) app_missing_insurance
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere . '
					ORDER BY ' . $strSortBy . '
					OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	// This function is written for entrata new dashboard

	public static function fetchInsuranceMissingApplicationsCountByDashboardFilterByCid( $objDashboardFilter, $intCid, $objDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strLeasingAgentCondition = ( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) ? ' AND ( ca.leasing_agent_id IS NULL OR ca.leasing_agent_id IN (' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ') )' : '';

		if( 0 != $intMaxExecutionTimeOut ) {

			executeSql( 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ', $objDatabase );
		}

		$strSql = 'SELECT
						COUNT( 1 ) ' .
						( true == $boolIsGroupByProperties ?
							' AS applicants_missing_insurance,
							apps.property_id, apps.property_name, '
						:
							', MAX ' ) . '
						( apps.priority ) as priority
					FROM (
						SELECT
							*
						FROM (
							SELECT
								DISTINCT ON ( ca.id )
								ca.id,
								ca.cid,
								ca.property_id,
								ca.property_name,
								CASE
									WHEN TRIM( dp.applicants_missing_insurance->>\'urgent_resident_movedin\') <> \'\' AND 1 = TRIM( dp.applicants_missing_insurance ->> \'urgent_resident_movedin\')::int AND CURRENT_DATE > ca.lease_start_date THEN
										3
									WHEN TRIM( dp.applicants_missing_insurance->>\'urgent_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.applicants_missing_insurance->>\'urgent_move_in_date_within\' )::int ) >= ca.lease_start_date THEN
										3
									WHEN TRIM( dp.applicants_missing_insurance->>\'urgent_lease_completed_since\') <> \'\' AND ( CURRENT_DATE - ( dp.applicants_missing_insurance->>\'urgent_lease_completed_since\' )::int ) >= ca.lease_completed_on::date THEN
										3
									WHEN TRIM( dp.applicants_missing_insurance->>\'important_resident_movedin\') <> \'\' AND 1 = TRIM( dp.applicants_missing_insurance ->> \'important_resident_movedin\')::int AND CURRENT_DATE > ca.lease_start_date THEN
										2
									WHEN TRIM( dp.applicants_missing_insurance->>\'important_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.applicants_missing_insurance->>\'important_move_in_date_within\' )::int ) >= ca.lease_start_date THEN
										2
									WHEN TRIM( dp.applicants_missing_insurance->>\'important_lease_completed_since\') <> \'\' AND ( CURRENT_DATE - ( dp.applicants_missing_insurance->>\'important_lease_completed_since\' )::int ) >= ca.lease_completed_on::date THEN
										2
									ELSE
										1
								END as priority
							FROM
								cached_applications ca
								JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . '] ) lp ON lp.property_id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ?  ' AND lp.is_disabled = 0 ' : '' ) . '
								JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' AND aa.deleted_on IS NULL )
								JOIN property_preferences pp ON ( pp.cid = ca.cid AND pp.property_id = ca.property_id AND key = \'REQUIRE_RENTERS_INSURANCE_AFTER_LEASE_SIGNED\' AND value IS NOT NULL )
								LEFT JOIN insurance_policy_customers ipc ON ( ca.cid = ipc.cid AND ca.lease_id = ipc.lease_id )
								LEFT JOIN resident_insurance_policies rip ON ( rip.cid = ipc.cid AND rip.id = ipc.resident_insurance_policy_id )
								LEFT JOIN dashboard_priorities dp ON dp.cid = ca.cid
							WHERE
								ca.cid = ' . ( int ) $intCid . '
								AND rip.confirmed_on IS NULL
								AND ca.application_stage_id  = ' . CApplicationStage::LEASE . '
								AND	ca.application_status_id  = ' . CApplicationStatus::COMPLETED . '
								AND ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '
								' . $strLeasingAgentCondition . '
						) app_missing_insurance
						WHERE
							cid = ' . ( int ) $intCid . '
							' . $strPriorityWhere . '
						ORDER BY
							priority DESC ' .
						( false == $boolIsGroupByProperties ? ' LIMIT 100' : '' ) . '
					) apps' .
					( true == $boolIsGroupByProperties ? '
						GROUP BY
							property_id,
							property_name,
							priority'
					: '' ) . '';

		$arrmixData = fetchData( $strSql, $objDatabase );

		return ( false == $boolIsGroupByProperties ? ( ( true == isset( $arrmixData[0] ) ) ? $arrmixData[0] : [ 'count' => 0, 'priority' => 1 ] ) : $arrmixData );
	}

	public static function fetchApplicationsByIdsByCustomerTypeIdsByPropertyIdByCid( $arrintApplicationIds, $arrintCustomerTypeIds, $intPropertyId, $intCid, $boolIncludeOnlyPendingSigners = false, $objDatabase ) {
		if( false == valArr( $arrintApplicationIds ) || false == valArr( $arrintCustomerTypeIds ) ) return NULL;

		$strSql = 'SELECT
						apps.*,
						aa.id as applicant_application_id,
						a.id as applicant_id,
						a.name_first,
						a.name_last,
						a.username as email_address
					FROM
						applications apps
						JOIN applicant_applications aa ON ( aa.application_id = apps.id AND aa.cid = apps.cid )
						JOIN applicants a ON ( a.id = aa.applicant_id AND a.cid = aa.cid )
					WHERE
						apps.cid = ' . ( int ) $intCid . '
						AND apps.property_id = ' . ( int ) $intPropertyId . '
						AND aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' )
						AND apps.id IN ( ' . implode( ',', $arrintApplicationIds ) . ' ) ';

		if( true == $boolIncludeOnlyPendingSigners ) {
			$strSql .= ' AND aa.lease_signed_on IS NULL';
		}

		return self::fetchCachedApplications( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	// This function is written for entrata new dashboard

	public static function fetchPaginatedGenerateLeasesApplicationsByDashboardFilterByCid( $objDashboardFilter, $intCid, $intOffset, $intLimit, $strSortBy, $objDatabase, $boolShowOnlyRenewals = false, $boolIsFromRenewal = false ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';
		$strSqlWhere = 'AND (ca.application_stage_id,ca.application_status_id) IN( ' . sqlIntMultiImplode( [ CApplicationStage::APPLICATION => [ CApplicationStatus::APPROVED ], CApplicationStage::LEASE => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED ] ] ) . ' )
						AND ( CASE WHEN ( ca.application_stage_id,ca.application_status_id) IN( ' . sqlIntMultiImplode( [ CApplicationStage::LEASE => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED ] ] ) . ' ) THEN ca.is_lease_partially_generated IS TRUE ELSE TRUE END 
          		             OR CASE WHEN pp_4.id IS NOT NULL THEN aa1.id IS NOT NULL ELSE FALSE END ) ';

		if( true == $boolShowOnlyRenewals ) {
			$strSqlWhere .= ' AND ca.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL;
		} else {
			$strSqlWhere .= ' AND ca.lease_interval_type_id IN ( ' . CLeaseIntervalType::APPLICATION . ', ' . CLeaseIntervalType::LEASE_MODIFICATION . ' )';
		}

		if( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) {
			$strSqlWhere .= ' AND ( ce.id IS NULL OR ce.id IN (' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ') )';
		}

		$strCustomerName = ' func_format_customer_name( appt.name_first, appt.name_last ) as applicant_name, ';
		if( true == $boolIsFromRenewal ) {
			$strCustomerName = ' CASE WHEN appt.name_last_matronymic IS NOT NULL AND length( appt.name_last_matronymic ) > 0 THEN 
								func_format_customer_name ( appt.name_first, CONCAT ( COALESCE ( appt.name_last, \'\'), \' \', appt.name_last_matronymic ) )
							ELSE
								func_format_customer_name ( appt.name_first, appt.name_last )
							END AS applicant_name, ';
		}
		$strSql = 'SELECT
						*
					FROM (
						SELECT
							DISTINCT ON ( ca.id ) ca.id AS application_id,
							ca.cid,
							appt.id as applicant_id,
							' . $strCustomerName . '
							appt.customer_id,
							ca.lease_start_date as move_in_date,
							COALESCE( ca.building_name || \' - \' || ca.unit_number_cache, ca.building_name, ca.unit_number_cache ) as unit_number,
							ca.application_completed_on,
							ca.application_datetime,
							ca.lease_id,
							ca.guest_remote_primary_key,
							ca.app_remote_primary_key,
							ca.lease_interval_type_id,
							ca.application_stage_id,
							ca.application_status_id,
							aa.lease_generated_on,
							CASE
								WHEN ' . CLeaseIntervalType::APPLICATION . ' = ca.lease_interval_type_id THEN \'New Lease\'
								WHEN ' . CLeaseIntervalType::RENEWAL . ' = ca.lease_interval_type_id THEN \'Renewal\'
								WHEN ' . CLeaseIntervalType::LEASE_MODIFICATION . ' = ca.lease_interval_type_id THEN \'Lease Modification\'
								WHEN ' . CLeaseIntervalType::TRANSFER . ' = ca.lease_interval_type_id THEN \'Unit Transfer\'
								ELSE \'\'
							END as lease_interval_type,
							aa.completed_on as primary_applicant_completed_on,
							p.property_name,
							p.id as property_id,
							p.remote_primary_key as property_remote_primary_key,
							ce.name_first || \' \' || ce.name_last as agent_name,
							CASE
								WHEN TRUE = ca.is_pet_policy_conflicted AND ( 0 =
									(
										SELECT
											count( pap.id )
										FROM
											property_application_preferences as pap
										WHERE
											pap.cid = ' . ( int ) $intCid . '
											AND pap.property_id = ca.property_id
											AND pap.company_application_id = ca.company_application_id
											AND pap.cid = ca.cid
											AND pap.key = \'HIDE_OPTION_PET\'
										LIMIT 1
									) ) THEN 1
								ELSE 0
							END AS is_pet_policy_conflicted,
							pp_1.id AS dont_export_application_charges,
							pp_2.id AS dont_export_applications,
							pp_3.id AS dont_export_guest_cards,
							(
								SELECT
									count( 1 )
								FROM
									ar_transactions at
									JOIN ar_codes ac ON ( ac.cid = at.cid AND ( ac.default_ar_code_id IS NULL OR ac.default_ar_code_id <> ' . CDefaultArCode::PAYMENT . ' ) AND ac.id = at.ar_code_id )
								WHERE
									at.cid = ca.cid
									AND at.lease_id = ca.lease_id
									AND at.remote_primary_key IS NULL
									AND at.customer_id IS NOT NULL
								LIMIT 1 ) as ar_transaction_id,
							CASE WHEN pp.ps_product_id IS NOT NULL THEN 1 ELSE 0 END as is_lease_execution,
							CASE
								WHEN TRIM( dp.applicants_generate_leases ->> \'urgent_application_approved_since\') <> \'\' AND ( ca.application_approved_on IS NOT NULL AND NOW() >= ( ca.application_approved_on::date + ( dp.applicants_generate_leases->>\'urgent_application_approved_since\' )::int ) ) THEN
									3
								WHEN TRIM ( dp.applicants_generate_leases->>\'urgent_move_in_date_within_days\' ) != \'\' AND ( \'past\' = TRIM( dp.applicants_generate_leases->>\'urgent_move_in_date_within\' ) )
									AND ( CURRENT_DATE - TRIM( dp.applicants_generate_leases->>\'urgent_move_in_date_within_days\' )::int ) >= ca.lease_start_date THEN
									3
								WHEN TRIM ( dp.applicants_generate_leases->>\'urgent_move_in_date_within_days\' ) != \'\' AND ( \'within\' = TRIM( dp.applicants_generate_leases->>\'urgent_move_in_date_within\' ) )
									AND (
											( ca.lease_start_date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.applicants_generate_leases ->> \'urgent_move_in_date_within_days\' ) ::int ) )
											OR ( ( \'past\' = TRIM( dp.applicants_generate_leases->>\'important_move_in_date_within\' ) ) AND ( ca.lease_start_date BETWEEN ( CURRENT_DATE - TRIM ( dp.applicants_generate_leases ->> \'urgent_move_in_date_within_days\' ) ::int ) AND CURRENT_DATE ) )
										) THEN
									3
								WHEN TRIM( dp.applicants_generate_leases ->> \'important_application_approved_since\') <> \'\' AND ( ca.application_approved_on IS NOT NULL AND NOW() >= ( ca.application_approved_on::date + ( dp.applicants_generate_leases->>\'important_application_approved_since\' )::int ) ) THEN
									2
								WHEN TRIM ( dp.applicants_generate_leases->>\'important_move_in_date_within_days\' ) != \'\' AND ( \'past\' = TRIM( dp.applicants_generate_leases->>\'important_move_in_date_within\' ) )
									AND ( CURRENT_DATE - TRIM( dp.applicants_generate_leases->>\'important_move_in_date_within_days\' )::int ) >= ca.lease_start_date THEN
									2
								WHEN TRIM ( dp.applicants_generate_leases->>\'important_move_in_date_within_days\' ) != \'\' AND ( \'within\' = TRIM( dp.applicants_generate_leases->>\'important_move_in_date_within\' ) )
									AND (
											( ca.lease_start_date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.applicants_generate_leases ->> \'important_move_in_date_within_days\' ) ::int ) )
											OR ( ( \'past\' = TRIM( dp.applicants_generate_leases->>\'urgent_move_in_date_within\' ) ) AND ( ca.lease_start_date BETWEEN ( CURRENT_DATE - TRIM ( dp.applicants_generate_leases ->> \'important_move_in_date_within_days\' ) ::int ) AND CURRENT_DATE ) )
										) THEN
									2
								ELSE
									1
							END as priority
						FROM
							cached_applications ca
							JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . '], array[  ' . CPsProduct::ENTRATA . ', ' . CPsProduct::LEASE_EXECUTION . ' ] ) lp ON lp.property_id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ?  ' AND lp.is_disabled = 0 ' : '' ) . '
							JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' AND aa.deleted_on IS NULL )
							LEFT JOIN applicant_applications aa1 ON ( aa1.cid = ca.cid AND aa1.application_id = ca.id AND aa1.deleted_on IS NULL AND aa1.approved_on IS NOT NULL AND aa1.lease_generated_on IS NULL AND aa1.lease_skipped_on IS NULL AND aa1.customer_type_id IN ( \'' . implode( '\',\'', CCustomerType::$c_arrintResponsibleCustomerTypeIds ) . '\' ) )
							JOIN applicants appt ON ( appt.cid = aa.cid AND appt.id = aa.applicant_id )
							JOIN properties p ON ( p.cid = ca.cid AND p.id = ca.property_id )
							LEFT JOIN property_products pp ON ( pp.cid = p.cid AND ( pp.property_id = p.id OR pp.property_id IS NULL ) AND pp.ps_product_id = ' . CPsProduct::LEASE_EXECUTION . ' )
							LEFT JOIN company_employees ce ON ( ce.cid = ca.cid AND ce.id = ca.leasing_agent_id )
							LEFT JOIN property_preferences pp_1 ON ( pp_1.cid = ca.cid AND pp_1.property_id = ca.property_id AND pp_1.value IS NOT NULL AND pp_1.key = \'DONT_EXPORT_APPLICATION_CHARGES\' )
							LEFT JOIN property_preferences pp_2 ON ( pp_2.cid = ca.cid AND pp_2.property_id = ca.property_id AND pp_2.value IS NOT NULL AND pp_2.key = \'DONT_EXPORT_APPLICATIONS\' )
							LEFT JOIN property_preferences pp_3 ON ( pp_3.cid = ca.cid AND pp_3.property_id = ca.property_id AND pp_3.value IS NOT NULL AND pp_3.key = \'DONT_EXPORT_GUEST_CARDS\' )
							LEFT JOIN property_preferences pp_4 ON ( pp_4.cid = ca.cid AND pp_4.property_id = ca.property_id AND pp_4.value IS NOT NULL AND pp_4.key = \'ALLOW_INDEPENDENT_APPLICANT_LEASE_PROGRESSION\' )
							LEFT JOIN dashboard_priorities dp ON dp.cid = ca.cid
						WHERE
							ca.cid = ' . ( int ) $intCid . '
							' . $strSqlWhere . '
					) generate_leases
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere . '
					ORDER BY ' . $strSortBy . '
					OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	// This function is written for entrata new dashboard

	public static function fetchGenerateLeasesApplicationsCountByDashboardFilterByCid( $objDashboardFilter, $intCid, $objDatabase, $boolIsGroupByProperties = false, $boolShowOnlyRenewals = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) {
			return NULL;
		}

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSqlWhere = ( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) ? ' AND ( ca.leasing_agent_id IS NULL OR ca.leasing_agent_id IN (' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ') )' : '';

		$strSqlWhere .= 'AND (ca.application_stage_id,ca.application_status_id) IN( ' . sqlIntMultiImplode( [ CApplicationStage::APPLICATION => [ CApplicationStatus::APPROVED ], CApplicationStage::LEASE => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED ] ] ) . ' )
						AND ( CASE WHEN ( ca.application_stage_id,ca.application_status_id) IN( ' . sqlIntMultiImplode( [ CApplicationStage::LEASE => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED ] ] ) . ' ) THEN ca.is_lease_partially_generated IS TRUE ELSE TRUE END 
          		             OR CASE WHEN pp_4.id IS NOT NULL THEN aa1.id IS NOT NULL ELSE FALSE END ) ';

		if( true == $boolShowOnlyRenewals ) {
			$strSqlWhere .= ' AND ca.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL;
		} else {
			$strSqlWhere .= ' AND ca.lease_interval_type_id IN ( ' . CLeaseIntervalType::APPLICATION . ', ' . CLeaseIntervalType::LEASE_MODIFICATION . ' )';
		}

		if( 0 != $intMaxExecutionTimeOut ) {

			executeSql( 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ', $objDatabase );
		}

		$strSql = 'SELECT
						COUNT( 0 ) ' .
						( true == $boolIsGroupByProperties ? true == $boolShowOnlyRenewals ?
							' AS residents_renewals,
							property_id, property_name, priority '
						:
							' AS applicants_generate_leases,
							property_id, property_name, priority '
						:
							', MAX( priority ) AS priority ' ) . '
					FROM (
						SELECT
							*
						FROM (
							SELECT 	DISTINCT ON ( ca.id ) ca.id AS application_id,
								CASE
									WHEN TRIM( dp.applicants_generate_leases ->> \'urgent_application_approved_since\') <> \'\' AND ( ca.application_approved_on IS NOT NULL AND NOW() >= ( ca.application_approved_on::date + ( dp.applicants_generate_leases->>\'urgent_application_approved_since\' )::int ) ) THEN
										3
									WHEN TRIM ( dp.applicants_generate_leases->>\'urgent_move_in_date_within_days\' ) != \'\' AND ( \'past\' = TRIM( dp.applicants_generate_leases->>\'urgent_move_in_date_within\' ) )
										AND ( CURRENT_DATE - TRIM( dp.applicants_generate_leases->>\'urgent_move_in_date_within_days\' )::int ) >= ca.lease_start_date THEN
										3
									WHEN TRIM ( dp.applicants_generate_leases->>\'urgent_move_in_date_within_days\' ) != \'\' AND ( \'within\' = TRIM( dp.applicants_generate_leases->>\'urgent_move_in_date_within\' ) )
										AND (
												( ca.lease_start_date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.applicants_generate_leases ->> \'urgent_move_in_date_within_days\' ) ::int ) )
												OR ( ( \'past\' = TRIM( dp.applicants_generate_leases->>\'important_move_in_date_within\' ) ) AND ( ca.lease_start_date BETWEEN ( CURRENT_DATE - TRIM ( dp.applicants_generate_leases ->> \'urgent_move_in_date_within_days\' ) ::int ) AND CURRENT_DATE ) )
											) THEN
										3
									WHEN TRIM( dp.applicants_generate_leases ->> \'important_application_approved_since\') <> \'\' AND ( ca.application_approved_on IS NOT NULL AND NOW() >= ( ca.application_approved_on::date + ( dp.applicants_generate_leases->>\'important_application_approved_since\' )::int ) ) THEN
										2
									WHEN TRIM ( dp.applicants_generate_leases->>\'important_move_in_date_within_days\' ) != \'\' AND ( \'past\' = TRIM( dp.applicants_generate_leases->>\'important_move_in_date_within\' ) )
										AND ( CURRENT_DATE - TRIM( dp.applicants_generate_leases->>\'important_move_in_date_within_days\' )::int ) >= ca.lease_start_date THEN
										2
									WHEN TRIM ( dp.applicants_generate_leases->>\'important_move_in_date_within_days\' ) != \'\' AND ( \'within\' = TRIM( dp.applicants_generate_leases->>\'important_move_in_date_within\' ) )
										AND (
												( ca.lease_start_date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.applicants_generate_leases ->> \'important_move_in_date_within_days\' ) ::int ) )
												OR ( ( \'past\' = TRIM( dp.applicants_generate_leases->>\'urgent_move_in_date_within\' ) ) AND ( ca.lease_start_date BETWEEN ( CURRENT_DATE - TRIM ( dp.applicants_generate_leases ->> \'important_move_in_date_within_days\' ) ::int ) AND CURRENT_DATE ) )
											) THEN
										2
									ELSE
										1
								END AS priority,
								ca.property_id,
								p.property_name,
								ca.cid
							FROM
								cached_applications ca
								JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . '], array[  ' . CPsProduct::ENTRATA . ', ' . CPsProduct::LEASE_EXECUTION . ' ] ) lp ON lp.property_id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . '
								JOIN applicant_applications aa ON aa.cid = ca.cid AND aa.application_id = ca.id AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' AND aa.deleted_on IS NULL
								LEFT JOIN applicant_applications aa1 ON aa1.cid = ca.cid AND aa1.application_id = ca.id AND aa1.deleted_on IS NULL AND aa1.approved_on IS NOT NULL AND aa1.lease_generated_on IS NULL AND aa1.lease_skipped_on IS NULL AND aa1.customer_type_id IN ( \'' . implode( '\',\'', CCustomerType::$c_arrintResponsibleCustomerTypeIds ) . '\' )
								JOIN applicants appt ON appt.cid = aa.cid AND appt.id = aa.applicant_id
								JOIN properties p ON p.cid = ca.cid AND p.id = ca.property_id
								LEFT JOIN property_products pp ON pp.cid = p.cid AND ( pp.property_id = p.id OR pp.property_id IS NULL ) AND pp.ps_product_id = ' . CPsProduct::LEASE_EXECUTION . '
								LEFT JOIN company_employees ce ON ce.cid = ca.cid AND ce.id = ca.leasing_agent_id
								LEFT JOIN property_preferences pp_1 ON pp_1.cid = ca.cid AND pp_1.property_id = ca.property_id AND pp_1.value IS NOT NULL AND pp_1.key = \'DONT_EXPORT_APPLICATION_CHARGES\'
								LEFT JOIN property_preferences pp_2 ON pp_2.cid = ca.cid AND pp_2.property_id = ca.property_id AND pp_2.value IS NOT NULL AND pp_2.key = \'DONT_EXPORT_APPLICATIONS\'
								LEFT JOIN property_preferences pp_3 ON pp_3.cid = ca.cid AND pp_3.property_id = ca.property_id AND pp_3.value IS NOT NULL AND pp_3.key = \'DONT_EXPORT_GUEST_CARDS\'
								LEFT JOIN property_preferences pp_4 ON pp_4.cid = ca.cid AND pp_4.property_id = ca.property_id AND pp_4.value IS NOT NULL AND pp_4.key = \'ALLOW_INDEPENDENT_APPLICANT_LEASE_PROGRESSION\'
								LEFT JOIN dashboard_priorities dp ON dp.cid = ca.cid
							WHERE
								ca.cid = ' . ( int ) $intCid .
								$strSqlWhere . '
						) sub
						WHERE
							cid = ' . ( int ) $intCid . '
							' . $strPriorityWhere .
						( false == $boolIsGroupByProperties ?
						'ORDER BY
							priority DESC
						LIMIT 100'
						:
						'' ) . '
					) generate_leases' .
					( true == $boolIsGroupByProperties ? ' GROUP BY property_id, property_name, priority' : '' );

		$arrmixData = fetchData( $strSql, $objDatabase );

		return ( false == $boolIsGroupByProperties ? ( ( true == isset( $arrmixData[0] ) ) ? $arrmixData[0] : [ 'count' => 0, 'priority' => 1 ] ) : $arrmixData );
	}

	public static function fetchGenerateLeasesApplicationsCountByDashboardFilterByCidNew( $objDashboardFilter, $intCid, $objDatabase, $boolIsGroupByProperties = false, $boolShowOnlyRenewals = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) {
			return NULL;
		}

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSqlWhere = ( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) ? ' AND ( ca.leasing_agent_id IS NULL OR ca.leasing_agent_id IN (' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ') )' : '';

		$strSqlWhere .= 'AND ( (ca.application_stage_id,ca.application_status_id) = (' . CApplicationStage::APPLICATION . ',' . CApplicationStatus::APPROVED . ')
							OR ( (ca.application_stage_id,ca.application_status_id) IN( ' . sqlIntMultiImplode( [ CApplicationStage::LEASE => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED ] ] ) . ' )
									AND ca.is_lease_partially_generated = true ) ) ';

		if( true == $boolShowOnlyRenewals ) {
			$strSqlWhere .= ' AND ca.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL;
		} else {
			$strSqlWhere .= ' AND ca.lease_interval_type_id IN ( ' . CLeaseIntervalType::APPLICATION . ', ' . CLeaseIntervalType::LEASE_MODIFICATION . ' )';
		}

		$strSqlSetMaxTime = ( 0 != $intMaxExecutionTimeOut )? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';
		executeSql( $strSqlSetMaxTime, $objDatabase );

		$strSql = 'SELECT
						COUNT( 0 ) ' .
						( true == $boolIsGroupByProperties ? true == $boolShowOnlyRenewals ?
							' AS residents_renewals,
							property_id, property_name, priority '
						:
							' AS applicants_generate_leases,
							property_id, property_name, priority '
						:
						', MAX( priority ) AS priority ' ) . '
					FROM (
						SELECT
							*
						FROM (
							SELECT
								cdp.priority_id AS priority,
								ca.property_id,
								ca.property_name,
								ca.cid
							FROM
								cached_dashboard_priorities cdp
								JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . '], array[  ' . CPsProduct::ENTRATA . ', ' . CPsProduct::LEASE_EXECUTION . ' ] ) lp ON lp.property_id = cdp.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . '
								JOIN cached_applications ca ON ( cdp.cid = ca.cid AND cdp.property_id = ca.property_id AND cdp.reference_id = ca.id )
								JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' AND aa.deleted_on IS NULL )
							WHERE
								ca.cid = ' . ( int ) $intCid . '
								AND cdp.module_id = ' . CModule::DASHBOARD_APPLICANTS_GENERATE_LEASES .
								$strSqlWhere . '
						) sub
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere .
					( false == $boolIsGroupByProperties ?
		                'ORDER BY
							priority DESC
						LIMIT 100'
					:
						'' ) . '
					) generate_leases' .
					( true == $boolIsGroupByProperties ? ' GROUP BY property_id, property_name, priority' : '' );

		$arrmixData = fetchData( $strSql, $objDatabase );

		return ( false == $boolIsGroupByProperties ? ( ( true == isset( $arrmixData[0] ) ) ? $arrmixData[0] : [ 'count' => 0, 'priority' => 1 ] ) : $arrmixData );
	}

	public static function fetchApplicationCountByPropertyGroupIdsByLeaseIntervalTypeIdsByLeasingAgentIdsByCidGroupedByApplicationStageStatusId( $arrintPropertyGroupIds, $arrintLeaseIntervalTypeIds, $arrintCompanyEmployeeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strCompanyEmployeeSql = ( true == valArr( $arrintCompanyEmployeeIds ) ) ? ' AND a.leasing_agent_id IN ( ' . implode( ',', $arrintCompanyEmployeeIds ) . ' ) ' : '';

        $arrintNonActiveApplicationStatusIds = [
            CApplicationStage::PRE_APPLICATION 		=> [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ],
            CApplicationStage::APPLICATION 			=> [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ],
            CApplicationStage::LEASE                => [ CApplicationStatus::PARTIALLY_COMPLETED ],
        ];

        $strSql = 'SELECT
						ass.id as application_stage_status_id,
						count( ca.id ) as total_prospects,
						count( ca_1.id ) as new_prospects
					FROM
						cached_applications ca
						JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $arrintPropertyGroupIds ) . ' ] ) as lp ON ( lp.property_id = ca.property_id AND lp.is_disabled = 0 )
						JOIN application_stage_statuses ass ON ( ca.application_stage_id = ass.application_stage_id AND ca.application_status_id = ass.application_status_id AND ca.lease_interval_type_id = ass.lease_interval_type_id  )
						LEFT JOIN cached_applications ca_1 ON ( ca.id = ca_1.id AND ca_1.cid = ca.cid AND DATE_TRUNC ( \'day\', ca.application_datetime ) BETWEEN ( CURRENT_DATE - 30 ) AND CURRENT_DATE )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
						AND DATE_TRUNC( \'day\', ca.application_datetime ) BETWEEN \'' . '01/01/2004' . '\'  AND \'' . '12/31/2024' . '\'
						AND (ca.application_stage_id,ca.application_status_id) NOT IN ( ' . sqlIntMultiImplode( $arrintNonActiveApplicationStatusIds ) . ' )
						' . $strCompanyEmployeeSql . '
					GROUP BY
						ass.id';

		$arrmixApplicationCounts = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixApplicationCounts ) ) {

			$arrmixApplicationCounts = rekeyArray( 'application_stage_status_id', $arrmixApplicationCounts );

		}

		return $arrmixApplicationCounts;
	}

	// This function is written for entrata new dashboard

	public static function fetchDashboardApplicationsCountsWithMissingLeasingAgentsByDashboardFilterByCid( $objDashboardFilter, $intCid, $objDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		if( 0 != $intMaxExecutionTimeOut ) {

			executeSql( 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ', $objDatabase );
		}

		$strSql = 'SELECT
					COUNT( * ) ' .
					( true == $boolIsGroupByProperties ?
						' AS leads_assign_agents,
						property_id,
						property_name, '
					:
						', MAX' ) . '
					( priority ) as priority
				FROM (
					SELECT
						*
					FROM (
						SELECT
							ca.id,
							ca.cid,
							CASE
								WHEN TRIM( dp.leads_assign_agents->>\'urgent_lead_created_since\' ) != \'\' AND NOW() >= ( ca.created_on::timestamp + INTERVAL \'1 hour\' * ( dp.leads_assign_agents->>\'urgent_lead_created_since\' )::int ) THEN
									3
								WHEN TRIM( dp.leads_assign_agents->>\'urgent_application_progressed_to_stage_status_id\' ) != \'\' AND ass.id >= ( dp.leads_assign_agents->>\'urgent_application_progressed_to_stage_status_id\' )::int THEN
									3
								WHEN TRIM( dp.leads_assign_agents->>\'important_application_progressed_to_stage_status_id\' ) != \'\' AND ass.id >= ( dp.leads_assign_agents->>\'important_application_progressed_to_stage_status_id\' )::int THEN
									2
								WHEN TRIM( dp.leads_assign_agents->>\'important_lead_created_since\' ) != \'\' AND NOW() >= ( ca.created_on::timestamp + INTERVAL \'1 hour\' * ( dp.leads_assign_agents->>\'important_lead_created_since\' )::int ) THEN
									2
								ELSE
									1
							END as priority,
							ca.property_id,
							ca.property_name
						FROM
							cached_applications ca
							JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) lp ON ( ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? 'lp.is_disabled = 0 AND' : '' ) . ' lp.property_id = ca.property_id )
							JOIN properties p ON ( p.cid = ca.cid AND p.id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? 'AND p.is_disabled = 0' : '' ) . ' )
							JOIN application_stage_statuses ass ON ( ass.lease_interval_type_id = ca.lease_interval_type_id AND ass.application_stage_id = ca.application_stage_id AND ass.application_status_id = ca.application_status_id )
							JOIN applications a ON ( a.cid = ca.cid AND a.id = ca.id )
							LEFT JOIN dashboard_priorities dp ON dp.cid = ca.cid
						WHERE
							ca.cid = ' . ( int ) $intCid . '
							AND ca.is_deleted IS FALSE
							AND ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '
							AND ca.leasing_agent_id IS NULL
							AND (ca.application_stage_id,ca.application_status_id) NOT IN ( ' . sqlIntMultiImplode( CApplicationStageStatus::$c_arrintDashboardNonActiveApplicationStatusIds ) . ' )' . '
						UNION ALL
						SELECT
							ca.id,
							ca.cid,
							CASE
								WHEN TRIM( dp.leads_assign_agents->>\'urgent_lead_created_since\' ) != \'\' AND NOW() >= ( ca.created_on::timestamp + INTERVAL \'1 hour\' * ( dp.leads_assign_agents->>\'urgent_lead_created_since\' )::int ) THEN
									3
								WHEN TRIM( dp.leads_assign_agents->>\'urgent_application_progressed_to_stage_status_id\' ) != \'\' AND ass.id >= ( dp.leads_assign_agents->>\'urgent_application_progressed_to_stage_status_id\' )::int THEN
									3
								WHEN TRIM( dp.leads_assign_agents->>\'important_application_progressed_to_stage_status_id\' ) != \'\' AND ass.id >= ( dp.leads_assign_agents->>\'important_application_progressed_to_stage_status_id\' )::int THEN
									2
								WHEN TRIM( dp.leads_assign_agents->>\'important_lead_created_since\' ) != \'\' AND NOW() >= ( ca.created_on::timestamp + INTERVAL \'1 hour\' * ( dp.leads_assign_agents->>\'important_lead_created_since\' )::int ) THEN
									2
								ELSE
									1
							END as priority,
							ca.property_id,
							ca.property_name
						FROM
							cached_applications ca
							JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) lp ON ( lp.property_id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
							JOIN properties p ON ( p.cid = ca.cid AND p.id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND p.is_disabled = 0 ' : '' ) . ' )
							JOIN application_stage_statuses ass ON ( ass.lease_interval_type_id = ca.lease_interval_type_id AND ass.application_stage_id = ca.application_stage_id AND ass.application_status_id = ca.application_status_id )
							JOIN applications a ON ( a.cid = ca.cid AND a.id = ca.id )
							JOIN property_preferences pp ON ( pp.cid = ca.cid AND pp.property_id = ca.property_id AND pp.key = \'COUNT_DEFAULT_LEASING_AGENT_LEADS_AS_UNCLAIMED\' )
							JOIN property_leasing_agents pla ON ( pla.cid = ca.cid AND pla.property_id = ca.property_id AND pla.company_employee_id = ca.leasing_agent_id AND is_default = 1 )
							LEFT JOIN dashboard_priorities dp ON dp.cid = ca.cid
						WHERE
							ca.cid = ' . ( int ) $intCid . '
							AND ca.is_deleted IS FALSE
							AND ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '
							AND ca.leasing_agent_id IS NOT NULL
							AND (ca.application_stage_id,ca.application_status_id) NOT IN ( ' . sqlIntMultiImplode( CApplicationStageStatus::$c_arrintDashboardNonActiveApplicationStatusIds ) . ' )' . '
					) leads_assign_agent
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere . ' ' .
					( false == $boolIsGroupByProperties ?
					'ORDER BY
						priority DESC
					LIMIT 100' : '' ) . '
				) as subq ' .
				( true == $boolIsGroupByProperties ?
					' GROUP BY
							property_id,
							property_name,
							priority'
				: '' );

		return fetchData( $strSql, $objDatabase );
	}

	// This function is written for entrata new dashboard

	public static function fetchDashboardPaginatedApplicationsWithMissingLeasingAgentsByDashboardFilterByCid( $objDashboardFilter, $intCid, $objDatabase, $intPageOffset = 0, $intPageSize = 10, $strSortBy, $boolIsIntentToApply = false ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityCase = 'CASE
					      WHEN trim ( dp.leads_assign_agents ->> \'urgent_lead_created_since\' ) != \'\' AND now ( ) >= ( leads_assign_agent.application_datetime::TIMESTAMP + INTERVAL \'1 hour\' * ( dp.leads_assign_agents ->> \'urgent_lead_created_since\' )::int ) THEN 3
					      WHEN trim ( dp.leads_assign_agents ->> \'urgent_application_progressed_to_stage_status_id\' ) != \'\' AND ass.id >= ( dp.leads_assign_agents ->> \'urgent_application_progressed_to_stage_status_id\' )::int THEN 3
					      WHEN trim ( dp.leads_assign_agents ->> \'important_application_progressed_to_stage_status_id\' ) != \'\' AND ass.id >= ( dp.leads_assign_agents ->> \'important_application_progressed_to_stage_status_id\' )::int THEN 2
					      WHEN trim ( dp.leads_assign_agents ->> \'important_lead_created_since\' ) != \'\' AND now ( ) >= ( leads_assign_agent.application_datetime::TIMESTAMP + INTERVAL \'1 hour\' * ( dp.leads_assign_agents ->> \'important_lead_created_since\' )::int ) THEN 2
					      ELSE 1
					    END';
		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND ' . $strPriorityCase . ' IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSelectSqlApplicationIntentToApply = '';

		if( true == $boolIsIntentToApply ) {
			$strSelectSqlApplicationIntentToApply = ', ca.details::jsonb ->>\'agent_priority\' AS agent_priority ';
		}

		$strSql = 'SELECT
					    leads_assign_agent.*,
					    util_get_system_translated ( \'stage_name\', ass.stage_name, ass.details ) || \' \' || util_get_system_translated ( \'status_name\', ass.status_name, ass.details ) AS application_status,
					    ' . $strPriorityCase . ' AS priority,
					    ROUND ( ( leads_assign_agent.lead_score_1 )::NUMERIC, 1 ) AS lead_score
					FROM
					    ( WITH properties AS (
					                           SELECT
					                               p.*
					                           FROM
					                               load_properties ( array [ ' . ( int ) $intCid . ' ], array[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) AS lp
					                               JOIN properties AS p ON p.cid = lp.cid AND p.id = lp.property_id
					                           WHERE
					                               lp.is_disabled = 0
					    )
					SELECT
					    ca.id, ca.cid, ca.lease_interval_type_id, ca.application_datetime, ca.application_stage_id, ca.application_status_id, p.id AS property_id, p.property_name, func_format_customer_name ( ca.name_first, ca.name_last ) AS ap_name, ca.primary_phone_number AS phone_number, ca.lease_start_date AS move_in_date, ca.details, 
					     CASE 
							WHEN (cp.key)::text = \'ENABLE_LEAD_SCORING\' AND (cp.value)::numeric = 2 THEN 
								ROUND( util.util_mapto_range(( ca.details::json->>\'lead_score\' )::text::numeric, ( ca.details::json->>\'lead_score_min\' )::text::numeric, ( ca.details::json->>\'lead_score_max\' )::text::numeric, 0.5, 0.0, 1.0), 1 )
							ELSE
								ROUND( util.util_mapto_range(( ca.details::json->>\'lead_score\' )::text::numeric, 0.1, 5.0, 0.5, 0.0, 1.0), 1 )
							END
					     AS lead_score_1 ' . $strSelectSqlApplicationIntentToApply . '
					FROM
					    cached_applications AS ca
					    JOIN properties AS p ON ca.cid = p.cid AND ca.property_id = p.id
					    LEFT JOIN company_preferences cp ON (cp.cid = ca.cid AND cp.key = \'ENABLE_LEAD_SCORING\' )
					WHERE
					    ca.cid = ' . ( int ) $intCid . '
					    AND ca.is_deleted IS FALSE
					    AND ca.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::APPLICATION . '
					    AND ( ca.application_stage_id, ca.application_status_id ) NOT IN ( ' . sqlIntMultiImplode( CApplicationStageStatus::$c_arrintDashboardNonActiveApplicationStatusIds ) . ' )
					    AND ca.leasing_agent_id IS NULL
					UNION ALL
					SELECT
					    ca.id, ca.cid, ca.lease_interval_type_id, ca.application_datetime, ca.application_stage_id, ca.application_status_id, p.id AS property_id, p.property_name, func_format_customer_name ( ca.name_first, ca.name_last ) AS ap_name, ca.primary_phone_number AS phone_number, ca.lease_start_date AS move_in_date, ca.details,
					     CASE 
							WHEN (cp.key)::text = \'ENABLE_LEAD_SCORING\' AND (cp.value)::numeric = 2 THEN 
								ROUND( util.util_mapto_range(( ca.details::json->>\'lead_score\' )::text::numeric, ( ca.details::json->>\'lead_score_min\' )::text::numeric, ( ca.details::json->>\'lead_score_max\' )::text::numeric, 0.5, 0.0, 1.0), 1 )
							ELSE
								ROUND( util.util_mapto_range(( ca.details::json->>\'lead_score\' )::text::numeric, 0.1, 5.0, 0.5, 0.0, 1.0), 1 )
							END
					     AS lead_score_1 ' . $strSelectSqlApplicationIntentToApply . '
					FROM
					    cached_applications AS ca
					    JOIN properties AS p ON ca.cid = p.cid AND ca.property_id = p.id
					    JOIN property_preferences AS pp ON ( pp.cid = ca.cid AND pp.property_id = ca.property_id AND pp.key = \'COUNT_DEFAULT_LEASING_AGENT_LEADS_AS_UNCLAIMED\' )
					    JOIN property_leasing_agents AS pla ON ( pla.cid = ca.cid AND pla.property_id = ca.property_id AND pla.company_employee_id = ca.leasing_agent_id AND is_default = 1 )
						LEFT JOIN company_preferences cp ON (cp.cid = ca.cid AND cp.key = \'ENABLE_LEAD_SCORING\' )
					WHERE
					    ca.cid = ' . ( int ) $intCid . '
					    AND ca.is_deleted IS FALSE
					    AND ca.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::APPLICATION . '
					    AND ( ca.application_stage_id, ca.application_status_id ) NOT IN ( ' . sqlIntMultiImplode( CApplicationStageStatus::$c_arrintDashboardNonActiveApplicationStatusIds ) . ' )
					    AND ca.leasing_agent_id IS NOT NULL ) AS leads_assign_agent JOIN application_stage_statuses AS ass ON ass.lease_interval_type_id = leads_assign_agent.lease_interval_type_id
					    AND ass.application_stage_id = leads_assign_agent.application_stage_id
					    AND ass.application_status_id = leads_assign_agent.application_status_id JOIN dashboard_priorities AS dp ON dp.cid = leads_assign_agent.cid
					WHERE
					    leads_assign_agent.cid = ' . ( int ) $intCid . '
					    ' . $strPriorityWhere . '    
					ORDER BY ' . $strSortBy . '
					LIMIT ' . ( int ) $intPageSize . '
					OFFSET ' . ( int ) $intPageOffset;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationWithPropertyFloorPlansByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedFloorPlans = false ) {

		$strCheckDeletedFloorPlansSql = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pf.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						ca.id,
						aa.id as applicant_application_id,
						ca.cid,
						a.id as applicant_id,
						a.name_first,
						a.name_last,
						a.username as email_address,
						ca.desired_bedrooms AS desired_bedrooms,
						ca.desired_bathrooms AS desired_bathrooms,
						ca.lease_start_date AS move_in_date,
						ca.term_month AS lease_term,
						ca.created_on AS created_on,
						appt.name_first AS name_first,
						appt.name_last AS name_last,
						appt.phone_number AS phone_number,
						appt.email_address AS email_address,
						pf.floorplan_name AS floorplan_name
					FROM
						cached_applications ca
						JOIN applicant_applications aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid )
						JOIN applicants a ON ( a.id = aa.applicant_id AND a.cid = aa.cid )
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid )
						LEFT JOIN property_floorplans pf ON ( ca.property_floorplan_id = pf.id AND ca.cid = pf.cid ' . $strCheckDeletedFloorPlansSql . ' )
					WHERE
						ca.cid	= ' . ( int ) $intCid . '
						AND ca.id = ' . ( int ) $intApplicationId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationByLeaseIdByLeaseIntervalIdByFieldNamesByCid( $intLeaseId, $intLeaseIntervalId, $arrstrFieldNames, $intCid, $objDatabase, $boolOnlyPrimaryApplicant = false ) {
		if( false == valArr( $arrstrFieldNames ) ) return NULL;

		$strSql = 'SELECT
						' . implode( ', ', $arrstrFieldNames ) . '
					FROM
						cached_applications ca
						JOIN applicant_applications aa ON ( ca.cid = aa.cid AND ca.id = aa.application_id )
						JOIN lease_terms lt ON ( ca.cid = lt.cid AND ca.lease_term_id = lt.id )
						LEFT JOIN lease_start_windows lsw ON ( ca.cid = lsw.cid AND ca.lease_start_window_id = lsw.id AND lsw.lease_term_id = lt.id )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.lease_id = ' . ( int ) $intLeaseId . '
						AND ca.lease_interval_id = ' . ( int ) $intLeaseIntervalId;
		$strSql .= ( true == $boolOnlyPrimaryApplicant ) ? ' AND aa.customer_type_id = 1 ' : '';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchApplicationsByLeaseIdByLeaseIntervalIdsByCid( $intLeaseId, $arrintLeaseIntervalIds, $intCid, $objDatabase ) {

		if( false == is_numeric( $intCid ) || false == is_numeric( $intLeaseId ) || false == valArr( $arrintLeaseIntervalIds ) ) return NULL;

		$strSql = '	SELECT
						ca.*
					FROM
						cached_applications ca
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.lease_id = ' . ( int ) $intLeaseId . '
						AND ca.lease_interval_id IN ( ' . implode( ',', $arrintLeaseIntervalIds ) . ' )
					ORDER BY
						ca.created_on DESC';

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsWithoutLeaseRenewalPdfsByApplicationStageStatusIdsByCid( $arrintApplicationStageStatusIds, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valArr( $arrintApplicationStageStatusIds ) || false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;

		$strSql = '	SELECT
						ca.*,
						aa.offer_sent_on
					FROM
						cached_applications ca
						JOIN quotes q ON ( ca.cid = q.cid AND ca.id = q.application_id AND q.cancelled_on IS NULL )
						JOIN applicant_applications aa ON ( ca.cid = aa.cid AND aa.application_id = ca.id AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' AND aa.cid = ' . ( int ) $intCid . ' )
						LEFT JOIN file_associations fa ON ( fa.cid = ca.cid AND fa.application_id = ca.id AND fa.cid = ' . ( int ) $intCid . ' AND fa.deleted_by IS NULL AND fa.quote_id = q.id )
						LEFT JOIN files f ON ( f.cid = fa.cid AND f.id = fa.file_id AND f.cid = ' . ( int ) $intCid . ' )
						LEFT JOIN file_types ft ON ( ft.cid = f.cid AND ft.id = f.file_type_id AND ft.system_code = \'ROS\' AND ft.cid = ' . ( int ) $intCid . ' )
					WHERE
						ca.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . '
						AND (ca.application_stage_id,ca.application_status_id) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )
						AND fa.id IS NULL
						AND ca.cid = ' . ( int ) $intCid;

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchLatestScheduleTransferApplicationByTransferLeaseIdLeaseIntervalTypeIdByByCid( $intTransferLeaseId, $intCid, $objDatabase ) {

		if( 0 >= $intCid ) return NULL;
		if( 0 >= $intTransferLeaseId ) return NULL;

		$strSql = '
					SELECT
						cap.*
					FROM
						cached_applications cap
					WHERE
						cap.cid = ' . ( int ) $intCid . '
						AND cap.lease_id = ' . ( int ) $intTransferLeaseId . '
						AND cap.lease_interval_type_id = ' . CLeaseIntervalType::TRANSFER . '
					ORDER BY
						cap.created_on DESC
					LIMIT 1';

		return self::fetchCachedApplication( $strSql, $objDatabase );

	}

	public static function fetchLatestRenewalApplicationIdByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		$strSql = '	SELECT
						ca.id AS application_id
					FROM
						cached_applications ca
						JOIN lease_intervals li ON ( li.id = ca.lease_interval_id AND li.cid = ca.cid )

					WHERE
						ca.lease_id = ' . ( int ) $intLeaseId . '
						AND ca.cid = ' . ( int ) $intCid . '
						AND ca.application_status_id <> ' . CApplicationStatus::CANCELLED . '
						AND ca.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . '
						AND li.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
					ORDER BY
						ca.id  DESC
					LIMIT
						1';

		return self::fetchColumn( $strSql, 'application_id', $objDatabase );

	}

	public static function fetchApplicationByLeaseIdByLeaseIntervalTypeIdByLeaseStatusTypeIdsByExcludingApplicationStageStatusIds( $intLeaseId, $intLeaseIntervalTypeId, $arrintLeaseStatusTypeIds, $arrintApplicationStageStatusIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ca.id
					FROM
						cached_applications ca
						JOIN lease_intervals li ON ( ca.cid = li.cid AND ca.lease_interval_id = li.id )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND li.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
						AND ca.lease_id = ' . ( int ) $intLeaseId . '
						AND li.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )
						AND (ca.application_stage_id,ca.application_status_id) NOT IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )
					LIMIT
						1';

		return self::fetchCachedApplication( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByPropertyUnitIdByApplicationStageStatusIdsByCid( $intPropertyUnitId, $arrintApplicationStageStatusIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicationStageStatusIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						cached_applications ca
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.property_unit_id = ' . ( int ) $intPropertyUnitId . '
						AND (ca.application_stage_id,ca.application_status_id) NOT IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' )';

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsWithPropertyFloorplanIdsByLeaseIdsByCid( $arrintLeaseIds, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						cached_applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND property_floorplan_id IS NOT NULL';

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationStageStausIdsByLeasesIdsByPropertyIdByCid( $arrintLeaseIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseIds ) || false == valId( $intCid ) ) return false;

		$strSql	= '	SELECT
						ca.lease_id,
						ass.id,
						ass.application_stage_id as stage_id
					FROM
						cached_applications ca
						JOIN leases l ON ( l.cid = ca.cid AND l.id = ca.lease_id AND l.active_lease_interval_id = ca.lease_interval_id )
						JOIN application_stage_statuses ass ON( ca.lease_interval_type_id = ass.lease_interval_type_id AND ca.application_stage_id = ass.application_stage_id AND ca.application_status_id =ass.application_status_id )
					WHERE
						ca.cid	= ' . ( int ) $intCid . '
						AND ca.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByLeaseIdsByLeaseIntervalTypeIdByCid( $arrintTransferLeaseIds, $intLeaseIntervalTypeId, $intCid, $objDatabase ) {

		if( 0 >= $intCid || false == valArr( $arrintTransferLeaseIds ) ) return NULL;

		$strSql = ' SELECT
						cap.*
					FROM
						cached_applications cap
					WHERE
						cap.cid = ' . ( int ) $intCid . '
						AND cap.lease_id IN ( ' . implode( ',', $arrintTransferLeaseIds ) . ' )
						AND cap.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
					ORDER BY
						cap.created_on DESC';

		return self::fetchCachedApplications( $strSql, $objDatabase );

	}

	public static function fetchApplicationsByLeaseIdsByLeaseIntervalIdsByCid( $arrintLeaseIds, $arrintLeaseIntervalIds, $intCid, $objDatabase ) {

		if( 0 >= $intCid || false == valArr( $arrintLeaseIds ) || false == valArr( $arrintLeaseIntervalIds ) ) return NULL;

		$strSql = 'SELECT
						ca.*
					FROM
						cached_applications ca
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND ca.lease_interval_id IN ( ' . implode( ',', $arrintLeaseIntervalIds ) . ' )
					ORDER BY
						ca.created_on DESC';

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByLeaseIntervalIdsByCid( $arrintLeaseIntervalIds, $intCid, $objDatabase ) {

		if( 0 >= $intCid || false == valArr( $arrintLeaseIntervalIds ) ) return NULL;

		$strSql = 'SELECT
						ca.*
					FROM
						cached_applications ca
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.lease_interval_id IN ( ' . implode( ',', $arrintLeaseIntervalIds ) . ' )
					ORDER BY
						ca.created_on DESC';

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationStageIdIdByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						application_stage_id
					FROM
						applications
					WHERE cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intApplicationId;

		return self::fetchColumn( $strSql, 'application_stage_id', $objDatabase );
	}

	public static function fetchRenewalApplicationsByIdsByLeaseIdsByCid( $arrintApplicationIds, $arrintLeaseIds, $intCid, $objDatabase ) {
		if ( false == valArr( $arrintLeaseIds ) || false == valArr( $arrintApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						ca.*
					FROM
						cached_applications ca
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND ca.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND ca.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL;

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationStatusIdIdByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						application_status_id
					FROM
						applications
					WHERE cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intApplicationId;

		return self::fetchColumn( $strSql, 'application_status_id', $objDatabase );
	}

	// Dashboard Never Contacted Leads

	public static function fetchPaginatedNeverContactedApplicationsByDashboardFilterByCid( $objDashboardFilter, $intCid, $intOffset, $intLimit, $strSortBy, $objDatabase, $boolIsLead = 1, $boolIsDashboardAuditReport = false, $boolIsIntentToApply = false ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strLeasingAgentCondition = ( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) ? ' AND ( ca.leasing_agent_id IN ( ' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ' ) OR ca.leasing_agent_id IS NULL ) ' : '';

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND COALESCE( cdp.priority_id, 1 ) IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';
		$intModuleId = $boolIsLead ? CModule::DASHBOARD_LEADS_NEVER_CONTACTED : CModule::DASHBOARD_APPLICANTS_NEVER_CONTACTED;

		$strSelectSqlApplicationIntentToApply = '';

		if( true == $boolIsIntentToApply ) {
			$strSelectSqlApplicationIntentToApply = ', ca.details::jsonb ->>\'agent_priority\' AS agent_priority ';
		}

		$strSelectSql = '
						ca.id AS application_id,
						ca.cid,
						ca.application_stage_id,
						ca.application_status_id,
						ca.lease_start_date AS move_in_date,
						aa.id AS applicant_id,
						func_format_customer_name ( ca.name_first, ca.name_last ) AS applicant_name,
						ca.primary_phone_number,
						ca.email_address,
						COALESCE ( ce.name_first || \' \' || ce.name_last, ce.name_first, ce.name_last ) AS agent_name,
						util_get_system_translated( \'stage_name\', ass.stage_name, ass.details )|| \' \'||util_get_system_translated( \'status_name\', ass.status_name, ass.details ) AS application_status,
						a.created_on,
						ca.property_name,
						ca.property_id,
						pp1.id AS dont_export_application_charges,
						pp2.id AS dont_export_applications,
						pp3.id AS dont_export_guest_cards,
						COALESCE ( cdp.priority_id, 1 ) AS priority,
						CASE 
							WHEN (cp.key)::text = \'ENABLE_LEAD_SCORING\' AND (cp.value)::numeric = 2 THEN 
								ROUND( util.util_mapto_range(( ca.details::json->>\'lead_score\' )::text::numeric, ( ca.details::json->>\'lead_score_min\' )::text::numeric, ( ca.details::json->>\'lead_score_max\' )::text::numeric, 0.5, 0.0, 1.0), 1 )
							ELSE
								ROUND( util.util_mapto_range(( ca.details::json->>\'lead_score\' )::text::numeric, 0.1, 5.0, 0.5, 0.0, 1.0), 1 )
							END
					     AS lead_score ' . $strSelectSqlApplicationIntentToApply;

		$strGroupBySql = '';
		$strOrderBySql = '
				ORDER BY ' . $strSortBy . '
				OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		if( true == $boolIsDashboardAuditReport ) {
			$strSelectSql = '
					COUNT( 1 ) AS applicants_never_contacted,
					ca.property_id,
					ca.property_name,
					COALESCE ( cdp.priority_id, 1 ) AS priority';
			$strGroupBySql	= '
					GROUP BY
						ca.property_id,
						ca.property_name,
						priority';
			$strOrderBySql = '';
		}

		$strSql = '
					SELECT
						' . $strSelectSql . '
					FROM
						cached_applications ca
						JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY[' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . '] )  AS lp ON ( lp.property_id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ?  ' AND lp.is_disabled = 0 ' : '' ) . ' )
						JOIN applicant_applications aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid AND aa.deleted_on IS NULL AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' )
						JOIN application_stage_statuses ass ON ( ass.application_stage_id = ca.application_stage_id AND ass.application_status_id = ca.application_status_id AND ass.lease_interval_type_id = ca.lease_interval_type_id )
						JOIN applications a ON ( a.cid = ca.cid AND a.id = ca.id )
						JOIN application_display_details add ON ( add.cid = ca.cid AND add.application_id = ca.id )
						LEFT JOIN company_employees ce ON ( ce.cid = ca.cid AND ce.id = ca.leasing_agent_id )
						LEFT JOIN property_preferences pp1 ON ( pp1.cid = ca.cid AND pp1.property_id = ca.property_id AND pp1.value IS NOT NULL AND pp1.key= \'DONT_EXPORT_APPLICATION_CHARGES\' )
						LEFT JOIN property_preferences pp2 ON ( pp2.cid = ca.cid AND pp2.property_id = ca.property_id AND pp2.value IS NOT NULL AND pp2.key= \'DONT_EXPORT_APPLICATIONS\' )
						LEFT JOIN property_preferences pp3 ON ( pp3.cid = ca.cid AND pp3.property_id = ca.property_id AND pp3.value IS NOT NULL AND pp3.key= \'DONT_EXPORT_GUEST_CARDS\' )
						LEFT JOIN cached_leases cl ON ( ca.cid = cl.cid AND ca.lease_id = cl.id )
						LEFT JOIN cached_dashboard_priorities cdp ON cdp.cid = ca.cid AND cdp.property_id = ca.property_id AND cdp.module_id = ' . ( int ) $intModuleId . ' AND cdp.reference_id = ca.id
						LEFT JOIN company_preferences cp ON (cp.cid = ca.cid AND cp.key = \'ENABLE_LEAD_SCORING\' )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.is_deleted IS FALSE
						AND ass.id NOT IN( ' . implode( ',', CApplicationStageStatus::$c_arrintDashboardProspectNonActiveApplicationStageStatusIds ) . ' )
						AND ca.lease_interval_type_id IN ( ' . CLeaseIntervalType::APPLICATION . ' )
						AND ca.last_event_id IS NULL
						AND ( cl.id IS NULL OR cl.lease_status_type_id = 1 )
						AND add.is_lead = ' . $boolIsLead . '::BOOL
						' . $strLeasingAgentCondition . '
						' . $strPriorityWhere . '
					' . $strGroupBySql . $strOrderBySql;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchNeverContactedApplicationsCountByDashboardFilterByCid( $objDashboardFilter, $intCid, $objDatabase, $boolIsDashboardAuditReport = false, $intMaxExecutionTimeOut = 0, $boolIsLead = true ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strLeasingAgentCondition = ( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) ? ' AND ( ca.leasing_agent_id IN ( ' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ' ) OR ca.leasing_agent_id IS NULL ) ' : '';

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND COALESCE( cdp.priority_id, 1 ) IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';
		$intModuleId = $boolIsLead ? CModule::DASHBOARD_LEADS_NEVER_CONTACTED : CModule::DASHBOARD_APPLICANTS_NEVER_CONTACTED;

		if( 0 != $intMaxExecutionTimeOut ) {

			executeSql( 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ', $objDatabase );
		}

		$strSql = 'SELECT
						COUNT( id ) ' . ( true == $boolIsDashboardAuditReport ? ' AS leads_never_contacted, ca.property_id, ca.property_name, ' : ', MAX ' ) . '
						( priority ) AS priority
					FROM (
							SELECT
								ca.id,
								ca.cid,
								ca.property_id,
								ca.property_name,
								COALESCE ( cdp.priority_id, 1 ) AS priority
							FROM
								cached_applications ca
								JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY[' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . '] )  AS lp ON ( lp.property_id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ?  ' AND lp.is_disabled = 0 ' : '' ) . ' )
								JOIN applicant_applications aa ON ( aa.application_id = ca.id AND aa.cid = ca.cid AND aa.deleted_on IS NULL AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' )
								JOIN application_stage_statuses ass ON ( ass.application_stage_id = ca.application_stage_id AND ass.application_status_id = ca.application_status_id AND ass.lease_interval_type_id = ca.lease_interval_type_id )
								JOIN application_display_details add ON ( add.cid = ca.cid AND add.application_id = ca.id )
								LEFT JOIN cached_dashboard_priorities cdp ON cdp.cid = ca.cid AND cdp.property_id = ca.property_id AND cdp.module_id = ' . ( int ) $intModuleId . ' AND cdp.reference_id = ca.id
								LEFT JOIN cached_leases cl ON ( ca.cid = cl.cid AND ca.lease_id = cl.id )
							WHERE
								ca.cid = ' . ( int ) $intCid . '
								AND ca.is_deleted IS FALSE
								AND ass.id NOT IN( ' . implode( ',', CApplicationStageStatus::$c_arrintDashboardProspectNonActiveApplicationStageStatusIds ) . ' )
								AND ca.lease_interval_type_id IN ( ' . CLeaseIntervalType::APPLICATION . ' )
								AND ca.last_event_id IS NULL
								AND ( cl.id IS NULL OR cl.lease_status_type_id = 1 )
								AND add.is_lead = ' . $boolIsLead . '::BOOL
								' . $strLeasingAgentCondition . '
								' . $strPriorityWhere . '
							ORDER BY
								priority DESC' .
						( false == $boolIsDashboardAuditReport ? ' LIMIT 100' : '' ) . '
					) AS ca
					' . ( true == $boolIsDashboardAuditReport ? ' GROUP BY property_id, property_name, priority' : '' ) . '';

		$arrmixData = fetchData( $strSql, $objDatabase );

		return ( false == $boolIsDashboardAuditReport ? ( ( true == isset( $arrmixData[0] ) ) ? $arrmixData[0] : [ 'count' => 0, 'priority' => 1 ] ) : $arrmixData );
	}

	public static function fetchPaginatedGenerateLeasesApplicationsByDashboardFilterByCidNew( $objDashboardFilter, $intCid, $intOffset, $intLimit, $strSortBy, $objDatabase, $boolShowOnlyRenewals = false ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSqlWhere = 'AND (ca.application_stage_id,ca.application_status_id) IN( ' . sqlIntMultiImplode( [ CApplicationStage::APPLICATION => [ CApplicationStatus::APPROVED ], CApplicationStage::LEASE => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED ] ] ) . ' )
						AND ( CASE WHEN ( ca.application_stage_id,ca.application_status_id) IN( ' . sqlIntMultiImplode( [ CApplicationStage::LEASE => [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED ] ] ) . ' ) THEN ca.is_lease_partially_generated IS TRUE ELSE TRUE END 
          		             OR CASE WHEN pp_4.id IS NOT NULL THEN aa1.id IS NOT NULL ELSE FALSE END ) ';

		if( true == $boolShowOnlyRenewals ) {
			$strSqlWhere .= ' AND ca.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL;
		} else {
			$strSqlWhere .= ' AND ca.lease_interval_type_id IN ( ' . CLeaseIntervalType::APPLICATION . ', ' . CLeaseIntervalType::LEASE_MODIFICATION . ' )';
		}

		if( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) {
			$strSqlWhere .= ' AND ( ce.id IS NULL OR ce.id IN (' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ') )';
		}

		$strSql = 'SELECT
						*
					FROM (
						SELECT
							DISTINCT ON ( ca.id ) ca.id AS application_id,
							ca.cid,
							appt.id as applicant_id,
							func_format_customer_name( appt.name_first, appt.name_last ) as applicant_name,
							appt.customer_id,
							ca.lease_start_date as move_in_date,
							COALESCE( ca.building_name || \' - \' || ca.unit_number_cache, ca.building_name, ca.unit_number_cache ) as unit_number,
							ca.application_completed_on,
							ca.application_datetime,
							ca.lease_id,
							ca.guest_remote_primary_key,
							ca.app_remote_primary_key,
							ca.lease_interval_type_id,
							ca.application_stage_id,
							ca.application_status_id,
							aa.lease_generated_on,
							CASE
								WHEN ' . CLeaseIntervalType::APPLICATION . ' = ca.lease_interval_type_id THEN \'New Lease\'
								WHEN ' . CLeaseIntervalType::RENEWAL . ' = ca.lease_interval_type_id THEN \'Renewal\'
								WHEN ' . CLeaseIntervalType::LEASE_MODIFICATION . ' = ca.lease_interval_type_id THEN \'Lease Modification\'
								WHEN ' . CLeaseIntervalType::TRANSFER . ' = ca.lease_interval_type_id THEN \'Unit Transfer\'
								ELSE \'\'
							END as lease_interval_type,
							aa.completed_on as primary_applicant_completed_on,
							p.property_name,
							p.id as property_id,
							p.remote_primary_key as property_remote_primary_key,
							ce.name_first || \' \' || ce.name_last as agent_name,
							CASE
								WHEN TRUE = ca.is_pet_policy_conflicted AND ( 0 =
									(
										SELECT
											count( pap.id )
										FROM
											property_application_preferences as pap
										WHERE
											pap.cid = ' . ( int ) $intCid . '
											AND pap.property_id = ca.property_id
											AND pap.company_application_id = ca.company_application_id
											AND pap.cid = ca.cid
											AND pap.key = \'HIDE_OPTION_PET\'
										LIMIT 1
									) ) THEN 1
								ELSE 0
							END AS is_pet_policy_conflicted,
							pp_1.id AS dont_export_application_charges,
							pp_2.id AS dont_export_applications,
							pp_3.id AS dont_export_guest_cards,
							(
								SELECT
									count( 1 )
								FROM
									ar_transactions at
									JOIN ar_codes ac ON ( ac.cid = at.cid AND ( ac.default_ar_code_id IS NULL OR ac.default_ar_code_id <> ' . CDefaultArCode::PAYMENT . ' ) AND ac.id = at.ar_code_id )
								WHERE
									at.cid = ca.cid
									AND at.lease_id = ca.lease_id
									AND at.remote_primary_key IS NULL
									AND at.customer_id IS NOT NULL
								LIMIT 1 ) as ar_transaction_id,
							CASE WHEN pp.ps_product_id IS NOT NULL THEN 1 ELSE 0 END as is_lease_execution,
							cdp.priority_id as priority
						FROM
							cached_dashboard_priorities cdp
							JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . '], array[  ' . CPsProduct::ENTRATA . ', ' . CPsProduct::LEASE_EXECUTION . ' ] ) lp ON lp.property_id = cdp.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ?  ' AND lp.is_disabled = 0 ' : '' ) . '
							JOIN cached_applications ca ON ( cdp.cid = ca.cid AND cdp.property_id = ca.property_id AND cdp.reference_id = ca.id )
							JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.id = ( cdp.details ->>\'applicant_application_id\')::INTEGER AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' AND aa.deleted_on IS NULL )
							LEFT JOIN applicant_applications aa1 ON ( aa1.cid = ca.cid AND aa1.application_id = ca.id AND aa1.deleted_on IS NULL AND aa1.approved_on IS NOT NULL AND aa1.lease_generated_on IS NULL AND aa1.lease_skipped_on IS NULL AND aa1.customer_type_id IN ( \'' . implode( '\',\'', CCustomerType::$c_arrintResponsibleCustomerTypeIds ) . '\' ) )
							JOIN applicants appt ON ( appt.cid = aa.cid AND appt.id = (cdp.details ->>\'applicant_id\')::INTEGER )
							JOIN properties p ON ( p.cid = ca.cid AND p.id = ca.property_id )
							LEFT JOIN property_products pp ON ( pp.cid = p.cid AND ( pp.property_id = p.id OR pp.property_id IS NULL ) AND pp.ps_product_id = ' . CPsProduct::LEASE_EXECUTION . ' )
							LEFT JOIN company_employees ce ON ( ce.cid = ca.cid AND ce.id = ca.leasing_agent_id )
							LEFT JOIN property_preferences pp_1 ON ( pp_1.cid = ca.cid AND pp_1.property_id = ca.property_id AND pp_1.value IS NOT NULL AND pp_1.key = \'DONT_EXPORT_APPLICATION_CHARGES\' )
							LEFT JOIN property_preferences pp_2 ON ( pp_2.cid = ca.cid AND pp_2.property_id = ca.property_id AND pp_2.value IS NOT NULL AND pp_2.key = \'DONT_EXPORT_APPLICATIONS\' )
							LEFT JOIN property_preferences pp_3 ON ( pp_3.cid = ca.cid AND pp_3.property_id = ca.property_id AND pp_3.value IS NOT NULL AND pp_3.key = \'DONT_EXPORT_GUEST_CARDS\' )
							LEFT JOIN property_preferences pp_4 ON ( pp_4.cid = ca.cid AND pp_4.property_id = ca.property_id AND pp_4.value IS NOT NULL AND pp_4.key = \'ALLOW_INDEPENDENT_APPLICANT_LEASE_PROGRESSION\' )
						WHERE
							ca.cid = ' . ( int ) $intCid . '
							AND cdp.module_id = ' . CModule::DASHBOARD_APPLICANTS_GENERATE_LEASES . '
							' . $strSqlWhere . '
					) generate_leases
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere . '
					ORDER BY ' . $strSortBy . '
					OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicantsByApplicatonIdsFloorplanIdByUnitSpaceIdByCid( $arrintApplicationIds, $intPropertyFloorplanId, $intUnitSpaceId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$arrintArCodeTypeIds = array_merge( CArCodeType::$c_arrintRentAndOtherFeesArCodeTypes, [ CArCodeType::PAYMENT, CArCodeType::REFUND ] );

		$strSql = 'SELECT
						ca.cid,
						ROW_NUMBER() OVER (ORDER BY ca.wait_list_points DESC NULLS LAST, wla.wait_start_on) AS number,
						ca.id application_id,
						ca.lease_id,
						ca.property_id,
						ca.created_on,
						lp.move_in_date,
						ca.screening_approved_on,
						apl.id AS applicant_id,
						apl.name_first || \' \' || COALESCE ( apl.name_middle, \'\' ) || apl.name_last AS applicant_name,
						COALESCE ( apl.phone_number, apl.mobile_number, apl.work_number, NULL ) AS contact_number,
						pf.id AS property_floorplan_id,
						pf.floorplan_name,
						pf.number_of_bedrooms,
						pf.number_of_bathrooms,
						COALESCE ( sub_art.balance, 0 ) AS actual_balance
					FROM
						cached_applications ca
						LEFT JOIN lease_processes lp ON ( ca.cid = lp.cid AND ca.lease_id = lp.lease_id )
						JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id )
						JOIN applicants apl ON ( aa.cid = apl.cid AND apl.id = aa.applicant_id )
						JOIN wait_lists wl ON ( wl.cid = ca.cid AND wl.occupancy_type_id = ca.occupancy_type_id AND wl.is_active IS TRUE )
						JOIN wait_list_applications wla ON ( wl.cid = wla.cid AND wl.id = wla.wait_list_id AND wla.application_id = ca.id )
						LEFT JOIN LATERAL ( SELECT COUNT(*) AS floorplan_count FROM application_floorplans WHERE cid = ca.cid AND application_id = ca.id ) AS af ON TRUE
						LEFT JOIN property_floorplans pf ON ( pf.cid = ca.cid AND ( af.floorplan_count > 0 OR pf.id = ca.property_floorplan_id ) AND pf.deleted_on IS NULL )
						LEFT JOIN
						(
							SELECT
								sum ( art.transaction_amount ) AS balance,
								art.cid,
								art.lease_id
							FROM
								ar_transactions art
							WHERE
								art.cid = ' . ( int ) $intCid . '
								AND art.ar_code_type_id IN  ( ' . implode( ',', $arrintArCodeTypeIds ) . ' )
							GROUP BY
								art.cid,
								art.lease_id
						) sub_art ON ( sub_art.cid = ca.cid AND sub_art.lease_id = ca.lease_id )
						LEFT JOIN
						(
							SELECT
								ai.application_id,
								ai.cid,
								ai.event_type_id,
								rank() OVER ( PARTITION BY ai.application_id ORDER BY ai.offer_submitted_on DESC NULLS LAST ) AS rank
							FROM
								application_interests ai
							WHERE
								ai.cid = ' . ( int ) $intCid . '
								AND ai.unit_space_id = ' . ( int ) $intUnitSpaceId . '
						) sub_ai ON ( sub_ai.cid = ca.cid AND sub_ai.application_id = ca.id AND sub_ai.rank = 1 )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND ca.application_status_id NOT IN ( ' . CApplicationStatus::ON_HOLD . ',' . CApplicationStatus::CANCELLED . ' )
						AND pf.id = ' . ( int ) $intPropertyFloorplanId . '
						AND ca.unit_space_id IS NULL
						AND aa.customer_type_id = ' . ( int ) CCustomerType::PRIMARY . '
						AND sub_ai.event_type_id IS NULL
						AND ca.lease_status_type_id = ' . ( int ) CLeaseStatusType::APPLICANT . '
					ORDER BY
						ca.wait_list_points DESC NULLS LAST, 
						wla.wait_start_on';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByPropertyIdByUnitSpaceIdByIntervalByIdByCid( $intPropertyId, $intUnitSpaceId, $intApplicationId, $strInterval, $intCid, $objDatabase ) {

		$strSql = ' SELECT
    					*
					FROM
    					cached_applications
					WHERE
    					CID = ' . ( int ) $intCid . '
    					AND property_id = ' . ( int ) $intPropertyId . '
    					AND unit_space_id = ' . ( int ) $intUnitSpaceId . '
    					AND id <> ' . ( int ) $intApplicationId . '
    					AND created_on > ( NOW ( ) - INTERVAL \'' . $strInterval . '\' )';

		return self::fetchCachedApplications( $strSql, $objDatabase );
	}

	public static function fetchApplicationsByLeaseIntervalTypeIdByCallFilter( $intLeaseIntervalTypeId, $objCallFilter, $objDatabase ) {
		if( false == valId( $intLeaseIntervalTypeId ) || false == valObj( $objCallFilter, 'CCallFilter' ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT( ca.id )
					FROM
						cached_applications ca
					WHERE
						ca.lease_interval_type_id =  ' . ( int ) $intLeaseIntervalTypeId . '
						AND ca.ps_product_id = ' . ( int ) $objCallFilter->getPsProductIds() . ' ';

		if( true == valStr( $objCallFilter->getCids() ) && false == valId( $objCallFilter->getCid() ) ) {
			$strSql .= ' AND cid IN ( ' . $objCallFilter->getCids() . ' ) ';
		} elseif( false == valStr( $objCallFilter->getCids() ) && true == valId( $objCallFilter->getCid() ) ) {
			$strSql .= ' AND cid = ' . ( int ) $objCallFilter->getCid() . ' ';
		}

		if( true == valStr( $objCallFilter->getCallIds() ) ) {
			$strSql .= ' AND ca.call_id IN ( ' . $objCallFilter->getCallIds() . ' )';
		}

		if( true == valId( $objCallFilter->getPropertyId() ) ) {
			$strSql .= ' AND ca.property_id = ' . ( int ) $objCallFilter->getPropertyId();
		}

		if( true == valStr( $objCallFilter->getStartDate() ) && true == valStr( $objCallFilter->getEndDate() ) ) {
			$strSql .= ' AND DATE_TRUNC( \'day\', ca.application_datetime ) BETWEEN \'' . $objCallFilter->getStartDate() . ' 00:00:00\'
						 AND \'' . $objCallFilter->getEndDate() . ' 23:59:59\'';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomApplicationsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						aa.application_id
					FROM
						applicant_applications aa
						JOIN applicants a ON aa.applicant_id = a.id AND aa.cid = a.cid
					WHERE
						a.customer_id = ' . ( int ) $intCustomerId . '
						AND a.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRenewalApplicationsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchCachedApplications( sprintf( 'SELECT * FROM cached_applications ca WHERE ca.lease_id = %d AND ca.cid = %d AND ca.lease_interval_type_id IN ( ' . CLeaseIntervalType::RENEWAL . ')', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationByCustomFieldsByCustomerIdByPropertyIdsByCid( $arrmixCustomFields, $intCustomerId, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrmixCustomFields ) ) return NULL;

		$strSql = '	SELECT
						ca.' . implode( ',', $arrmixCustomFields ) . '
					FROM
						cached_applications ca
						JOIN applicant_applications aa ON ( ca.id = aa.application_id AND ca.cid = aa.cid )
						JOIN applicants appt ON ( appt.id = aa.applicant_id AND appt.cid = aa.cid )
						JOIN file_associations fa ON( fa.application_id = ca.id AND fa.applicant_id = appt.id AND fa.cid = ca.cid )
						JOIN files f ON ( f.id = fa.file_id AND fa.cid = f.cid )
						JOIN file_types ft ON ( ft.id = f.file_type_id AND ft.system_code = \'' . CFileType::SYSTEM_CODE_LEASE_ADDENDUM . '\' AND fa.cid = ft.cid )
					WHERE
						aa.lease_generated_on IS NOT NULL
						AND fa.file_signed_on IS NULL
						AND fa.deleted_on IS NULL
						AND fa.deleted_by IS NULL
						AND appt.customer_id = ' . ( int ) $intCustomerId . '
						AND appt.cid = ' . ( int ) $intCid . '
						AND ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ca.lease_interval_type_id IN ( ' . CLeaseIntervalType::RENEWAL . ',' . CLeaseIntervalType::LEASE_MODIFICATION . ',' . CLeaseIntervalType::TRANSFER . ',' . CLeaseIntervalType::APPLICATION . ' )
						AND CASE WHEN ca.application_stage_id = ' . CApplicationStage::LEASE . ' AND ca.application_status_id = ' . CApplicationStatus::APPROVED . ' THEN
							ca.lease_approved_on > TIMESTAMP \'2014-05-07\'
						ELSE
							true
						END
							ORDER BY ca.created_on DESC
					LIMIT 1 ';

		$arrstrData = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrstrData ) ) ? $arrstrData[0]['id'] : '';

	}

	public static function fetchLatestApplicationByCustomFieldsByByCustomerIdByCid( $arrmixCustomFields, $intCustomerId, $intCid, $objDatabase ) {

		if( false == valId( $intCustomerId ) || false == valArr( $arrmixCustomFields ) )  return NULL;

		$strSql = '	SELECT
						ca.' . implode( ',', $arrmixCustomFields ) . '
					FROM
						cached_applications ca,
						applicants appt,
						applicant_applications aa
					WHERE
						ca.cid = aa.cid
						AND ca.id = aa.application_id
						AND appt.id = aa.applicant_id
						AND appt.cid = aa.cid
						AND appt.customer_id::int = ' . ( int ) $intCustomerId . '
						AND ca.cid::int = ' . ( int ) $intCid . '
						ORDER BY ca.id DESC LIMIT 1 ';

		$arrstrData = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrstrData ) ) ? $arrstrData[0]['lease_id'] : '';

	}

	public static function fetchApplicationWithPropertyFloorplanIdByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		if( false == valId( $intLeaseId ) ) return NULL;

		$strSql = 'SELECT
						property_floorplan_id
					FROM
						applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND lease_id = ' . ( int ) $intLeaseId . '
						AND property_floorplan_id IS NOT NULL
					ORDER BY id ASC LIMIT 1';

		$arrstrData = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrstrData ) ) ? $arrstrData[0]['property_floorplan_id'] : NULL;
	}

	public static function fetchApplicationDataByLeaseIdByActiveLeaseIntervalIdByCid( $intLeaseId, $intLeaseIntervalId, $intCid, $objDatabase ) {

		if( false == valId( $intLeaseId ) ) return NULL;

		$strSql = 'SELECT
						ca.id,
						ca.primary_applicant_id
					FROM
						cached_applications ca
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.lease_id = ' . ( int ) $intLeaseId . '
						AND ca.lease_interval_id = ' . ( int ) $intLeaseIntervalId;
		$arrstrData = fetchData( $strSql, $objDatabase );
		return ( true == valArr( $arrstrData ) ) ? $arrstrData[0] : NULL;
	}

	public static function fetchCustomerIdsByLeaseIntervalIdByCustomerTypeIdsByPropertyIdByCid( $intLeaseIntervalId, $arrintCustomerTypeIds, $intPropertyId, $intCid, $objDatabase ) {

	    if( false == valArr( $arrintCustomerTypeIds ) ) return NULL;

	    $strSql = ' SELECT
					   a.customer_id
    				FROM
    					applications ap
    					JOIN applicant_applications aa ON ( aa.cid = ap.cid AND aa.application_id = ap.id )
    					JOIN applicants a ON ( aa.cid = a.cid AND aa.applicant_id = a.id )
    				WHERE
    					ap.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
    					AND ap.cid = ' . ( int ) $intCid . '
    					AND ap.property_id = ' . ( int ) $intPropertyId . '
    					AND aa.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ' );';

	    $arrmixValues = ( array ) fetchData( $strSql, $objDatabase );

	    $arrintCustomerIds = [];

	    foreach( $arrmixValues as $arrmixValue ) {
	        $arrintCustomerIds[] = $arrmixValue['customer_id'];
	    }

	    return $arrintCustomerIds;
	}

	public static function fetchApplicationDataByApplicationIdByCid( $intApplicationId, $intCid, $objClientDatabase ) {

		if( false == valId( $intApplicationId ) ) return NULL;

		$strSql = 'SELECT
						lease_id,
						property_id
					FROM
						applications
					WHERE
						id = ' . ( int ) $intApplicationId . '
						AND cid = ' . ( int ) $intCid;
		$arrstrData = fetchData( $strSql, $objClientDatabase );
		return ( true == valArr( $arrstrData ) ) ? $arrstrData[0] : NULL;
	}

	public static function fetchApplicationDetailsByLeaseIntervalIdByCid( $arrintLeaseIntervalIds, $intCid, $objDatabase ) {

	    if( false == valArr( $arrintLeaseIntervalIds ) ) return NULL;

	    $strSql = 'SELECT
	                   ca.property_floorplan_id,
	                   ca.lease_start_window_id,
	                   ca.space_configuration_id,
	                   ca.lease_interval_id
	               FROM
	                   cached_applications ca
	               WHERE
	                   ca.cid = ' . ( int ) $intCid . '
	                   AND ca.lease_interval_id IN ( ' . implode( ',', $arrintLeaseIntervalIds ) . ' )';

	    return rekeyArray( 'lease_interval_id', fetchData( $strSql, $objDatabase ) );
	}

	public static function fetchPreviousFiveWeekApplicationsCountByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'WITH week_date_range AS (
						SELECT
							row_number() OVER(),
							week_start::date,
							( week_start + INTERVAL \'6 days\' )::date AS week_end_date
						FROM
							generate_series( date_trunc( \'day\', CURRENT_TIMESTAMP - \'34 days\'::interval )::date, date_trunc( \'day\', CURRENT_TIMESTAMP)::date, \'1 week\'::interval ) week_start
						WHERE
							week_start <= date_trunc( \'week\', CURRENT_TIMESTAMP)::date
						ORDER BY
							week_start DESC )
					SELECT
						wdr.week_end_date,
						count( CASE WHEN ca.created_on::date BETWEEN wdr.week_start::date AND wdr.week_end_date::date THEN wdr.week_end_date ELSE NULL END )
					FROM
						week_date_range wdr
						LEFT JOIN cached_applications ca ON ( ( ca.created_on::date BETWEEN wdr.week_start::date AND wdr.week_end_date::date  OR ca.created_on::date NOT BETWEEN wdr.week_start::date AND wdr.week_end_date::date )
						AND ca.cid = ' . ( int ) $intCid . ' AND ca.property_id = ' . ( int ) $intPropertyId . ' )
					GROUP BY wdr.week_end_date
					ORDER BY wdr.week_end_date DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationsCountsForCurrentDaylastSeventDaysByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						count( CASE WHEN ca.created_on::Date = CURRENT_TIMESTAMP::Date THEN 1 ELSE NULL END ) as created_today,
						count( CASE WHEN ca.created_on::Date = date_trunc( \'day\', CURRENT_TIMESTAMP - \'1 days\'::interval )::Date THEN 1 ELSE NULL END ) as created_yesterday,
						count( CASE WHEN ( ca.created_on::Date > date_trunc( \'day\', CURRENT_TIMESTAMP - \'7 days\'::interval )::Date ) THEN 1 ELSE NULL END ) as created_seven_days,
						count( CASE WHEN ca.created_on::Date > date_trunc( \'day\', CURRENT_TIMESTAMP - \'14 days\'::interval )::date
									AND ca.created_on::Date < date_trunc ( \'day\', CURRENT_TIMESTAMP - \'7 days\'::interval )::date
									THEN 1
									ELSE NULL
								END ) as created_seven_days_previous
					FROM
						cached_applications ca
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.property_id = ' . ( int ) $intPropertyId . '
					GROUP BY
						ca.cid,
						ca.property_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApproveLeasesCountByDashboardFilterByCid( $objDashboardFilter, $intCid, $arrintScreeningCriteriaFailedApplicationIds, $objDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return;

		$strWhere = '';
		$strPriorityWhere = '';

		if( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) {
			$strPriorityWhere = ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )';
		}

		if( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) {
			$strWhere .= ' AND ( ca.leasing_agent_id IN ( ' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ' )
							OR ca.leasing_agent_id IS NULL ) ';
		}

		if( true == valArr( $arrintScreeningCriteriaFailedApplicationIds ) ) {
			$strWhere .= ' AND ca.id NOT IN ( ' . implode( ',', $arrintScreeningCriteriaFailedApplicationIds ) . ' )';
		}

		$strSql = ( 0 != $intMaxExecutionTimeOut )? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';

		$strSql .= ( false == $boolIsGroupByProperties ? '
					SELECT
						COALESCE( SUM( count ), 0 ) AS count,
						COALESCE( MAX( priority ), 1 ) AS priority
					FROM (
						SELECT
							*
						FROM (
							SELECT
								COUNT( ca.id ) AS count,'
				:
					'SELECT
						*
					 FROM (
						SELECT
							COUNT( ca.id ) AS approvals_esign_docs,
							ca.property_id,
							ca.property_name, ' ) . '
							(
								CASE
								WHEN TRIM( dp.approvals_esign_docs->>\'urgent_move_in_date_within\' ) != \'\' AND ca.lease_start_date <= ( CURRENT_DATE + ( dp.approvals_esign_docs->>\'urgent_move_in_date_within\' )::INT ) THEN
									3
								WHEN TRIM( dp.approvals_esign_docs->>\'urgent_lease_type_ids\' ) != \'\' AND ca.lease_interval_type_id = ANY ( ( dp.approvals_esign_docs->>\'urgent_lease_type_ids\' )::INTEGER[] ) THEN
									3
								WHEN TRIM( dp.approvals_esign_docs->>\'important_lease_type_ids\' ) != \'\' AND ca.lease_interval_type_id = ANY ( ( dp.approvals_esign_docs->>\'important_lease_type_ids\' )::INTEGER[] ) THEN
									2
								WHEN TRIM( dp.approvals_esign_docs->>\'important_move_in_date_within\' ) != \'\' AND ca.lease_start_date <= ( CURRENT_DATE + ( dp.approvals_esign_docs->>\'important_move_in_date_within\' )::INT ) THEN
									2
								ELSE
									1
								END
							) AS priority,
							ca.cid
						FROM
							cached_applications ca
							JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) lp ON ( ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? 'lp.is_disabled = 0 AND' : '' ) . ' lp.property_id = ca.property_id )
							LEFT JOIN property_preferences pp ON ( pp.cid = ca.cid AND pp.property_id = lp.property_id AND pp.key = \'REQUIRE_RENTERS_INSURANCE_AFTER_LEASE_SIGNED\' AND pp.value IS NOT NULL )
							LEFT JOIN property_preferences pp_1 ON ( pp_1.cid = ca.cid AND pp_1.property_id = lp.property_id AND lp.is_disabled = 0 AND pp_1.key = \'SCREEN_AFTER_LEASE_COMPLETED\' AND pp_1.value IS NOT NULL )
							LEFT JOIN insurance_policy_customers ipc ON ( ca.cid = ipc.cid AND ca.lease_id = ipc.lease_id AND ca.id = ipc.application_id )
							LEFT JOIN resident_insurance_policies rip ON ( rip.cid = ca.cid AND rip.id = ipc.resident_insurance_policy_id )
							LEFT JOIN dashboard_priorities dp ON ( dp.cid = ca.cid )
							LEFT JOIN screening_application_requests sar ON ( ca.cid = sar.cid AND ca.id = sar.application_id )
							LEFT JOIN (
								SELECT
									DISTINCT ctv.cid,
									ptv.property_id,
									ptv.company_transmission_vendor_id,
									ctv.transmission_vendor_id,
									1 as is_resident_verify
								FROM
									company_transmission_vendors ctv
									JOIN property_transmission_vendors ptv ON ( ptv.cid = ctv.cid AND ptv.company_transmission_vendor_id = ctv.id )
									JOIN property_products p_prod ON ( ctv.cid = p_prod.cid AND ptv.property_id = p_prod.property_id )
								WHERE
									ctv.cid = ' . ( int ) $intCid . '
									AND  ctv.transmission_vendor_id = ' . CTransmissionVendor::RESIDENT_VERIFY . '
									AND ( p_prod.ps_product_id = ' . CPsProduct::RESIDENT_VERIFY . ' OR p_prod.ps_product_id IS NULL )
							) ptv ON ( ptv.cid = ca.cid AND ptv.property_id = ca.property_id )
						WHERE
							ca.cid = ' . ( int ) $intCid . '
							AND ca.cancelled_on IS NULL
							AND ca.application_stage_id = ' . CApplicationStage::LEASE . '
							AND ca.application_status_id = ' . CApplicationStatus::COMPLETED . '
							AND
								CASE WHEN
									ptv.is_resident_verify = 1
									AND sar.id IS NOT NULL
								THEN
									sar.request_status_type_id = ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED . '
								ELSE
									true
								END
							AND ( ( ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' AND rip.confirmed_on IS NOT NULL AND pp.id IS NOT NULL ) OR ca.lease_interval_type_id IN ( ' . CLeaseIntervalType::RENEWAL . ', ' . CLeaseIntervalType::LEASE_MODIFICATION . ', ' . CLeaseIntervalType::TRANSFER . ' ) OR pp.id IS NULL )
							' . $strWhere . '
							AND ( sar.screening_decision_type_id IS NULL OR sar.screening_decision_type_id <> ' . CScreeningDecisionType::DENIED . ' )
							AND NOT EXISTS (
								SELECT
									1
								FROM
									applicant_applications aa
								WHERE
									aa.cid = ' . ( int ) $intCid . '
									AND aa.cid = ca.cid
									AND aa.application_id = ca.id
									AND aa.customer_type_id = ' . CCustomerType::PRIMARY . '
									AND aa.deleted_on IS NOT NULL
							)
						GROUP BY
							dp.approvals_esign_docs ->> \'urgent_move_in_date_within\',
							dp.approvals_esign_docs ->> \'urgent_lease_type_ids\',
							dp.approvals_esign_docs ->> \'important_lease_type_ids\',
							dp.approvals_esign_docs ->> \'important_move_in_date_within\',
							ca.lease_start_date,
							ca.cid,
							ca.lease_interval_type_id ' .
							( true == $boolIsGroupByProperties ? '
								, ca.property_id,
								ca.property_name,
								priority'
							: '' ) . '
					) approve_leases
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere . '
					ORDER BY
						priority DESC ' .
				( false == $boolIsGroupByProperties ? ') a_leases' : '' );

		return fetchData( $strSql, $objDatabase );
	}

    public static function fetchPaginatedMixedScreeningApplicationsByDashboardFilterByCid( $objDashboardFilter, $intCid, $intOffset, $intLimit, $strSortBy, $arrintPropertyIds, $arrmixUserPermissions, $objDatabase, $boolIsDashboardAuditReport = false ) {

        if( true == $boolIsDashboardAuditReport ) {
	        $arrintPropertyIds = $objDashboardFilter->getPropertyGroupIds();
        }

        if( false == valArr( $arrintPropertyIds ) || '' === $strSortBy || NULL === $intOffset || NULL === $intLimit || false == valObj( $objDashboardFilter, 'CDashboardFilter' ) ) {
            return NULL;
        }

        $strLeasingAgentCondition = $strPriorityWhere = $strJoin = $strWhere = '';

        if( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) {
            $strLeasingAgentCondition .= ' AND ( ce.id IS NULL OR ce.id IN (' . sqlIntImplode( $objDashboardFilter->getCompanyEmployeeIds() ) . ') )';
        }

        if( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) {
            $strPriorityWhere = ' AND priority IN ( ' . sqlIntImplode( $objDashboardFilter->getTaskPriorities() ) . ' )';
        }

        $arrintNonRVLeaseIntevalTypeIds[] = $arrintAllRVLeaseIntervalTypeIds[] = CLeaseIntervalType::APPLICATION;

        if( true == $arrmixUserPermissions['Renewal'] ) {
            $strJoin .= ' LEFT JOIN pp AS pp1 ON
                            CASE WHEN pp1.key in (\'REQUIRE_SCREENING_AFTER_RENEWAL\') THEN
                            	( pp1.cid = ca.cid AND pp1.property_id = p.id AND pp1.key = \'REQUIRE_SCREENING_AFTER_RENEWAL\' )  
                            ELSE
                            	( pp1.cid = ca.cid AND pp1.property_id = p.id AND ( ( pp1.key = \'REMOVED_APPLICANT_SCREEN_REMAINING_APPLICANTS\' AND  pp1.value = \'' . CPropertyPreference::SCREENING_REQUIRED . '\')  or ( pp1.key = \'REQUIRE_SCREENING_FOR_ADDED_APPLICANTS\' and  pp1.value = \'' . CPropertyPreference::SCREENING_REQUIRED . '\' ) ) )
                            END ';
	        $arrintRVLeaseIntevalTypeIds[] = $arrintNonRVLeaseIntevalTypeIds[] = $arrintAllRVLeaseIntervalTypeIds[] = CLeaseIntervalType::RENEWAL;
            $strWhere .= 'CASE
								WHEN ca.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::RENEWAL . ' THEN
                                	    pp1.id IS NOT NULL';
        }

        if( true == $arrmixUserPermissions['Transfer'] ) {

            $strJoin .= ' LEFT JOIN pp AS pp2 ON 
                            CASE WHEN pp2.key in (\'REQUIRE_SCREENING\') THEN
                            	( pp2.cid = ca.cid AND pp2.property_id = p.id AND pp2.key = \'REQUIRE_SCREENING\' )  
                            ELSE
                            	( pp2.cid = ca.cid AND pp2.property_id = p.id AND ( ( pp2.key = \'REMOVED_APPLICANT_SCREEN_REMAINING_APPLICANTS\' AND  pp2.value = \'' . CPropertyPreference::SCREENING_REQUIRED . '\')  or ( pp2.key = \'REQUIRE_SCREENING_FOR_ADDED_APPLICANTS\' and pp2.value = \'' . CPropertyPreference::SCREENING_REQUIRED . '\' ) ) )
                            END ';
            $arrintRVLeaseIntevalTypeIds[] = $arrintNonRVLeaseIntevalTypeIds[] = $arrintAllRVLeaseIntervalTypeIds[] = CLeaseIntervalType::TRANSFER;

            $strWhere .= ( '' == $strWhere ) ? ' CASE
								WHEN ca.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::TRANSFER . ' THEN
                                	     pp2.id IS NOT NULL ' : '
								WHEN ca.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::TRANSFER . ' THEN
                                	     pp2.id IS NOT NULL ';
        }

        if( true == $arrmixUserPermissions['Lease_Modification'] ) {
            $arrintRVLeaseIntevalTypeIds[] = $arrintAllRVLeaseIntervalTypeIds[] = CLeaseIntervalType::LEASE_MODIFICATION;
        }

        $strNonRVLeaseIntervalTypeIdsConditionSql = valArr( $arrintNonRVLeaseIntevalTypeIds ) ? ' AND ca.lease_interval_type_id IN ( ' . sqlIntImplode( $arrintNonRVLeaseIntevalTypeIds ) . ' ) ' : '';

        $strCaseStatement = '';
        $strScreeningRequirement = 'CASE 
	                                    WHEN ca.lease_type_id = ' . ( int ) CLeaseType::CORPORATE . ' THEN 
	                                        sum( CASE WHEN spctm.lease_type_id = ' . ( int ) CLeaseType::CORPORATE . ' AND spctm.screening_applicant_requirement_type_id = ' . ( int ) CScreeningApplicantRequirementType::REQUIRED . ' AND spctm.customer_type_id <> ' . ( int ) CCustomerType::PRIMARY . ' THEN 1 ELSE 0 END ) OVER (PARTITION BY aa.cid, aa.application_id)
	                                    ELSE
	                                        sum( CASE WHEN spctm.lease_type_id = CASE WHEN ca.lease_type_id IS NULL THEN ' . ( int ) CLeaseType::STANDARD . ' ELSE ca.lease_type_id END AND spctm.screening_applicant_requirement_type_id = ' . ( int ) CScreeningApplicantRequirementType::REQUIRED . ' THEN 1 ELSE 0 END ) OVER (PARTITION BY aa.cid, aa.application_id) 
	                                    END AS total_required_screening_applicants';

        if( true == valArr( $arrintRVLeaseIntevalTypeIds ) ) {
            $strCaseStatement = 'WHEN ptv.is_resident_verify = 1
	                                AND pp.value IS NOT NULL
	                                AND ca.lease_interval_type_id IN ( ' . sqlIntImplode( $arrintRVLeaseIntevalTypeIds ) . ' )
	                            THEN aa.require_screening IN ( ' . sqlIntImplode( [ CScreeningUtils::REQUIRE_SCREENING_APPLICANT_SETTING_OPTIONAL, CScreeningUtils::REQUIRE_SCREENING_APPLICANT_SETTING_YES ] ) . ')
                                WHEN ptv.is_resident_verify = 1 
	                                AND ca.lease_interval_type_id IN ( ' . sqlIntImplode( $arrintRVLeaseIntevalTypeIds ) . ' )  
	                            THEN aa.require_screening IN ( ' . sqlIntImplode( [ CScreeningUtils::REQUIRE_SCREENING_APPLICANT_SETTING_OPTIONAL, CScreeningUtils::REQUIRE_SCREENING_APPLICANT_SETTING_YES ] ) . ') ';

            $strScreeningRequirement = 'CASE 
			                                WHEN ca.lease_interval_type_id IN ( ' . sqlIntImplode( $arrintRVLeaseIntevalTypeIds ) . ' ) THEN
          	                                    sum( CASE WHEN aa.require_screening = ' . ( int ) CScreeningUtils::REQUIRE_SCREENING_APPLICANT_SETTING_YES . ' THEN 1 ELSE 0 END ) OVER( PARTITION BY aa.cid, aa.application_id )
          	                                ELSE 
          	                                    CASE
           		                                    WHEN ca.lease_type_id = ' . ( int ) CLeaseType::CORPORATE . ' THEN 
	                                                    sum( CASE WHEN spctm.lease_type_id = ' . ( int ) CLeaseType::CORPORATE . ' AND spctm.screening_applicant_requirement_type_id = ' . ( int ) CScreeningApplicantRequirementType::REQUIRED . ' AND spctm.customer_type_id <> ' . ( int ) CCustomerType::PRIMARY . ' THEN 1 ELSE 0 END ) OVER (PARTITION BY aa.cid, aa.application_id)
	                                                ELSE
	                                                    sum( CASE WHEN spctm.lease_type_id = CASE WHEN ca.lease_type_id IS NULL THEN ' . ( int ) CLeaseType::STANDARD . ' ELSE ca.lease_type_id END AND spctm.screening_applicant_requirement_type_id = ' . ( int ) CScreeningApplicantRequirementType::REQUIRED . ' THEN 1 ELSE 0 END ) OVER (PARTITION BY aa.cid, aa.application_id)
                                                END 
	                                    END AS total_required_screening_applicants ';
        }

        $strScreeningPackageCustomerTypesMappingCondition = '';

        $arrmixScreeningPackageCustomerTypeMapping      = CScreeningPackageCustomerTypesMappings::fetchActiveScreeningPackageCustomerTypesMappingsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase );
        if( !valArr( $arrmixScreeningPackageCustomerTypeMapping ) && valArr( $arrintRVLeaseIntevalTypeIds ) ) {
            $strScreeningRequirement = 'CASE 
			                                WHEN ca.lease_interval_type_id IN ( ' . sqlIntImplode( $arrintRVLeaseIntevalTypeIds ) . ' ) THEN
          	                                    sum( CASE WHEN aa.require_screening = ' . ( int ) CScreeningUtils::REQUIRE_SCREENING_APPLICANT_SETTING_YES . ' THEN 1 ELSE 0 END ) OVER( PARTITION BY aa.cid, aa.application_id )
	                                    END AS total_required_screening_applicants ';

            $strScreeningPackageCustomerTypesMappingJoin = '';
            $strScreeningPackageCustomerTypesMappingCondition = '';

        }

        $strAllRVLeaseIntervalTypeIds = valArr( $arrintAllRVLeaseIntervalTypeIds ) ?' AND ca.lease_interval_type_id IN ( ' . sqlIntImplode( $arrintAllRVLeaseIntervalTypeIds ) . ' ) ' : '';

        $strSelectSql	= 'apps.*';
        $strGroupBySql	= '';
        $strOrderBySql	= 'ORDER BY ' . $strSortBy . '
							OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

        if( true == $boolIsDashboardAuditReport ) {

            $strSelectSql = '
					COUNT( 1 ) AS applicants_process_screening,
					property_id,
					property_name,
					priority';

            $strGroupBySql	= '
					GROUP BY
						property_id,
						property_name,
						priority';

            $strOrderBySql = '';
        }

        $strWhere .= ( '' == $strWhere ) ? ' TRUE ' : ' ELSE TRUE END ';

        $strWithPtvCondition = 'WITH ptv AS (
										SELECT
											ctv.cid,
											ptv.property_id,
											COALESCE ( ptv.company_transmission_vendor_id, 0 ) AS company_transmission_vendor_id,
											CASE 
											WHEN ctv.transmission_vendor_id = ' . CTransmissionVendor::RESIDENT_VERIFY . ' THEN 1
											ELSE 0
											END as is_resident_verify
										FROM
											company_transmission_vendors ctv
											JOIN property_transmission_vendors ptv ON ( ptv.cid = ctv.cid AND ptv.company_transmission_vendor_id = ctv.id )
										WHERE
											ctv.cid = ' . ( int ) $intCid . '
											AND  ctv.transmission_type_id = ' . CTransmissionType::SCREENING . '
							),
							 pp AS (
                                                    SELECT
                                                        *
                                                    FROM
                                                        property_preferences pp
                                                    WHERE
                                                        pp.cid = ' . ( int ) $intCid . '
                                                        AND pp.property_id = ANY ( ARRAY[' . sqlIntImplode( $arrintPropertyIds ) . '] )
                                                        AND pp.value <> \'0\'
                                                        AND CASE WHEN pp.key in (\'REQUIRE_SCREENING_AFTER_RENEWAL\') THEN
                                                        	pp.key IN ( \'SCREEN_AFTER_LEASE_COMPLETED\', \'REQUIRE_SCREENING\',\'REQUIRE_SCREENING_AFTER_RENEWAL\' )
                                                        ELSE pp.key IN ( \'SCREEN_AFTER_LEASE_COMPLETED\', \'REQUIRE_SCREENING\',\'REMOVED_APPLICANT_SCREEN_REMAINING_APPLICANTS\',\'REQUIRE_SCREENING_FOR_ADDED_APPLICANTS\' )
                                                        END)';

        $strSql = $strWithPtvCondition . 'SELECT
						' . $strSelectSql . '
					FROM (
						SELECT
							DISTINCT aa.id,
							ca.id as application_id, 
							ca.cid,
							ca.lease_id,
							ca.lease_type_id,
							func_format_customer_name( appt.name_first, appt.name_last ) as applicant_name,
							ca.lease_start_date as move_in_date,
							COALESCE( ca.building_name || \' - \' || ca.unit_number_cache, ca.building_name, ca.unit_number_cache ) as unit_number,
							aa.completed_on as applicant_completed_on,
							p.property_name,
							p.id as property_id,
							ce.name_first || \' \' || ce.name_last as agent_name,
							pp.id AS screen_after_lease_completed,
							aa.require_screening,
							ca.lease_interval_type_id,
							aa.customer_type_id,
							COALESCE( ptv.is_resident_verify, 0 ) as is_resident_verify,
							 (
                                SELECT
                                    sapr.id
                                FROM
                                    screening_application_requests sapr
                                WHERE
                                    ca.cid = sapr.cid
                                    AND ca.id = sapr.application_id
                              ) AS screening_application_request_id,
							CASE
								WHEN TRIM( dp.approvals_applications ->> \'urgent_application_completed_since\') <> \'\' AND CURRENT_DATE >= ( ca.application_completed_on::date + ( dp.approvals_applications->>\'urgent_application_completed_since\' )::int ) THEN
									3
								WHEN TRIM( dp.approvals_applications ->> \'urgent_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.approvals_applications->>\'urgent_move_in_date_within\' )::int ) >= ca.lease_start_date THEN
									3
								WHEN TRIM( dp.approvals_applications ->> \'important_application_completed_since\') <> \'\' AND NOW() >= ( ca.application_completed_on::date + ( dp.approvals_applications->>\'important_application_completed_since\' )::int ) THEN
									2
								WHEN TRIM( dp.approvals_applications ->> \'important_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.approvals_applications->>\'important_move_in_date_within\' )::int ) >= ca.lease_start_date THEN
									2
								ELSE
									1
							END as priority,
							CASE 
							WHEN pp.id IS NOT NULL THEN
								( SELECT
						                      f.id
						                  FROM
						                      files f
						                      JOIN file_types ft ON ( f.file_type_id = ft.id AND f.cid = ft.cid AND ft.system_code = \'' . CFileType::SYSTEM_CODE_LEASE_PACKET . '\' AND f.countersigned_by IS NULL )
						                      JOIN file_associations fa ON ( f.id = fa.file_id AND f.cid = fa.cid AND fa.deleted_on IS NULL AND fa.lease_id IS NOT NULL )
						                  WHERE
						                      f.cid = ' . ( int ) $intCid . '
						                      AND f.property_id = pp.property_id
						                      AND fa.applicant_id = appt.id
						                      AND fa.customer_id IS NOT NULL
						                      AND fa.approved_by IS NULL
						                      AND ( fa.application_id IS NOT NULL OR f.require_countersign = 1 )
						                      ORDER BY f.id DESC LIMIT 1 )
							END AS file_id,
							CASE 
							WHEN ca.occupancy_type_id = ' . \COccupancyType::AFFORDABLE . '
							THEN 
								1
							ELSE 
								0
							END as is_affordable_application,
							 ' . $strScreeningRequirement . '
						FROM
							cached_applications ca
							JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[' . sqlIntImplode( $arrintPropertyIds ) . '] ) lp ON lp.is_disabled = 0 AND lp.property_id = ca.property_id
							JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id AND aa.customer_type_id IN ( ' . sqlIntImplode( CCustomerType::$c_arrintAllCustomerTypeIds ) . ' ) AND aa.deleted_on IS NULL )
							JOIN applicants appt ON ( appt.cid = aa.cid AND appt.id = aa.applicant_id )
							JOIN properties p ON ( p.cid = ca.cid AND p.id = ca.property_id )
							LEFT JOIN company_employees ce ON ( ce.cid = ca.cid AND ce.id = ca.leasing_agent_id )
							LEFT JOIN pp ON ( pp.cid = ca.cid AND pp.property_id = p.id AND pp.key = \'SCREEN_AFTER_LEASE_COMPLETED\' )
							' . $strJoin . '
							LEFT JOIN dashboard_priorities dp ON dp.cid = ca.cid
							  JOIN LATERAL 
                              (
                                SELECT
                                    *
                                FROM
                                    ptv
                                WHERE
                                    ptv.cid = ca.cid
                                    AND ptv.property_id = ca.property_id
                                LIMIT
                                    1
                              ) AS ptv ON TRUE  
							LEFT JOIN screening_applicant_results sar ON ( ca.cid = sar.cid AND sar.applicant_id = aa.applicant_id AND appt.id = sar.applicant_id AND ca.id = sar.application_id )
							LEFT JOIN screening_package_customer_types_mappings spctm ON ( ca.cid = spctm.cid AND ca.property_id = spctm.property_id AND aa.customer_type_id = spctm.customer_type_id AND aa.customer_relationship_id = spctm.customer_relationship_id AND spctm.is_active = 1 )
							LEFT JOIN applicant_application_transmissions aat ON ( aat.cid = aa.cid AND aat.applicant_application_id = aa.id AND aat.application_id = ca.id )
						WHERE
							ca.cid = ' . ( int ) $intCid . '
							AND 
								CASE WHEN ptv.is_resident_verify = 1 and ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '
									THEN 
										spctm.lease_type_id = CASE WHEN ca.lease_type_id IS NULL THEN ' . CLeaseType::STANDARD . ' ELSE ca.lease_type_id END
								ELSE
									TRUE
								END
							AND
								CASE
                                    WHEN ptv.is_resident_verify = 1
                                THEN
									CASE 
										WHEN ca.lease_type_id = ' . ( int ) CLeaseType::CORPORATE . ' 
									THEN
	                                    spctm.customer_type_id <> ' . ( int ) CCustomerType::PRIMARY . '
									ELSE
										spctm.screening_applicant_requirement_type_id IN (' . CScreeningApplicantRequirementType::REQUIRED . ', ' . CScreeningApplicantRequirementType::OPTIONAL . ' )
									END
								ELSE
                                    TRUE
                                END
							AND CASE
							    WHEN spctm.id IS NOT NULL THEN
							            spctm.screening_applicant_requirement_type_id <> ' . CScreeningApplicantRequirementType::NOT_REQUIRED . ' 
							    ELSE
							            TRUE
								END
							AND CASE 
							        WHEN pp.id <> 0 AND pp.id IS NOT NULL THEN 
							            ca.application_stage_id = ' . CApplicationStage::LEASE . '
							            AND ca.application_status_id IN ( ' . CApplicationStatus::COMPLETED . ' ) AND aa.completed_on IS NOT NULL
							        ELSE         
										ca.application_stage_id = ' . CApplicationStage::APPLICATION . '
										AND ca.application_status_id = ' . CApplicationStatus::COMPLETED . '
								END	
							' . $strLeasingAgentCondition . '
							AND CASE
								WHEN ptv.is_resident_verify = 1 THEN
                                	 sar.applicant_id IS NULL
								' . $strAllRVLeaseIntervalTypeIds . ' 
                                ELSE
                                	 aat.applicant_application_id IS NULL
								' . $strNonRVLeaseIntervalTypeIdsConditionSql . '
                                END	 	 
                           AND CASE
                                WHEN ptv.is_resident_verify = 1 
	                                AND ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' 
                                THEN aa.require_screening IN ( ' . CScreeningUtils::REQUIRE_SCREENING_APPLICANT_SETTING_NO . ' )
                                 ' . $strCaseStatement . '
								ELSE 
                                      TRUE       	
                                END
							AND ' . $strWhere . '
							AND CASE
                                    WHEN ptv.is_resident_verify = 1 
                                THEN
                                    aa.customer_type_id IN ( ' . sqlIntImplode( CCustomerType::$c_arrintAllCustomerTypeIds ) . ' )
                                ELSE 
                                    aa.customer_type_id IN ( ' . sqlIntImplode( CCustomerType::$c_arrintApplicationRequiredCustomerTypeIds ) . ' )
       	                        END
    	                         
       	                         
						) apps
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere . ' 
						' . $strGroupBySql . ' 
						' . $strOrderBySql;

        return fetchData( $strSql, $objDatabase );
    }

	public static function fetchRVProcessScreeningApplicationsCountByDashboardFilterByCidByPropertyTypes( $objDashboardFilter, $intCid, $boolIsGroupByProperties = false, $arrintRVEnabledPropertyIds, $arrmixUserPermissions, $objDatabase ) {

	if( false == valArr( $arrintRVEnabledPropertyIds ) || false == valObj( $objDashboardFilter, 'CDashboardFilter' ) ) {
		return 0;
	}

	$strLeasingAgentCondition = $strPriorityWhere = $strJoin = $strWhere = '';

	if( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) {
		$strLeasingAgentCondition .= ' AND ( ca.leasing_agent_id IS NULL OR ca.leasing_agent_id IN (' . sqlIntImplode( $objDashboardFilter->getCompanyEmployeeIds() ) . ') )';
	}

	if( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) {
		$strPriorityWhere = ' AND priority IN ( ' . sqlIntImplode( $objDashboardFilter->getTaskPriorities() ) . ' )';
	}

		$arrintAllRVLeaseIntervalTypeIds[] = CLeaseIntervalType::APPLICATION;

		if( true == $arrmixUserPermissions['Renewal'] ) {
			$strJoin .= ' LEFT JOIN pp AS pp1 ON
                            CASE WHEN pp1.key in (\'REQUIRE_SCREENING_AFTER_RENEWAL\') THEN
                            	( pp1.cid = ca.cid AND pp1.property_id = lp.property_id AND pp1.key = \'REQUIRE_SCREENING_AFTER_RENEWAL\' )  
                            ELSE
                            	( pp1.cid = ca.cid AND pp1.property_id = lp.property_id AND ( pp1.key = \'REMOVED_APPLICANT_SCREEN_REMAINING_APPLICANTS\' AND  pp1.value = \'' . CPropertyPreference::SCREENING_REQUIRED . '\')  or ( pp1.key = \'REQUIRE_SCREENING_FOR_ADDED_APPLICANTS\' and  pp1.value = \'' . CPropertyPreference::SCREENING_REQUIRED . '\') )
                            END ';
			$arrintRVLeaseIntevalTypeIds[] = $arrintAllRVLeaseIntervalTypeIds[] = CLeaseIntervalType::RENEWAL;
			$strWhere .= 'CASE
								WHEN ca.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::RENEWAL . ' THEN
                                	    pp1.id IS NOT NULL';
		}

		if( true == $arrmixUserPermissions['Transfer'] ) {
			$strJoin .= ' LEFT JOIN pp AS pp2 ON
                            CASE WHEN pp2.key in (\'REQUIRE_SCREENING\') THEN
                            	( pp2.cid = ca.cid AND pp2.property_id = lp.property_id AND pp2.key = \'REQUIRE_SCREENING\' )
                            ELSE
                            	( pp2.cid = ca.cid AND pp2.property_id = lp.property_id AND ( ( pp2.key = \'REMOVED_APPLICANT_SCREEN_REMAINING_APPLICANTS\' AND  pp2.value = \'' . CPropertyPreference::SCREENING_REQUIRED . '\')  or ( pp2.key = \'REQUIRE_SCREENING_FOR_ADDED_APPLICANTS\' and pp2.value = \'' . CPropertyPreference::SCREENING_REQUIRED . '\' ) ) )
                            END ';
			$arrintRVLeaseIntevalTypeIds[] = $arrintAllRVLeaseIntervalTypeIds[] = CLeaseIntervalType::TRANSFER;

			$strWhere .= ( '' == $strWhere ) ? ' CASE
								WHEN ca.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::TRANSFER . ' THEN
                                	     pp2.id IS NOT NULL ' : '
								WHEN ca.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::TRANSFER . ' THEN
                                	     pp2.id IS NOT NULL ';
		}

		if( true == $arrmixUserPermissions['Lease_Modification'] ) {
			$arrintRVLeaseIntevalTypeIds[] = $arrintAllRVLeaseIntervalTypeIds[] = CLeaseIntervalType::LEASE_MODIFICATION;
		}

		$strCaseStatement = '';
		if( true == valArr( $arrintRVLeaseIntevalTypeIds ) ) {
			$strCaseStatement = 'WHEN pp.value IS NOT NULL
	                                AND ca.lease_interval_type_id IN ( ' . sqlIntImplode( $arrintRVLeaseIntevalTypeIds ) . ' )
	                            THEN aa.require_screening IN ( ' . sqlIntImplode( [ CScreeningUtils::REQUIRE_SCREENING_APPLICANT_SETTING_OPTIONAL, CScreeningUtils::REQUIRE_SCREENING_APPLICANT_SETTING_YES ] ) . ')
                                WHEN ca.lease_interval_type_id IN ( ' . sqlIntImplode( $arrintRVLeaseIntevalTypeIds ) . ' )  
	                            THEN aa.require_screening IN ( ' . sqlIntImplode( [ CScreeningUtils::REQUIRE_SCREENING_APPLICANT_SETTING_OPTIONAL, CScreeningUtils::REQUIRE_SCREENING_APPLICANT_SETTING_YES ] ) . ') ';
		}

	$strWhere .= ( '' == $strWhere ) ? ' TRUE ' : ' ELSE TRUE END ';

		$strWithPtvCondition = 'WITH pp AS (
                                                    SELECT
                                                        *
                                                    FROM
                                                        property_preferences pp
                                                    WHERE
                                                        pp.cid = ' . ( int ) $intCid . '
                                                        AND pp.property_id = ANY ( ARRAY[' . sqlIntImplode( $arrintRVEnabledPropertyIds ) . '] )
                                                        AND pp.value <> \'0\'
                                                        AND CASE WHEN pp.key in (\'REQUIRE_SCREENING_AFTER_RENEWAL\') THEN
                                                        	pp.key IN ( \'SCREEN_AFTER_LEASE_COMPLETED\', \'REQUIRE_SCREENING\',\'REQUIRE_SCREENING_AFTER_RENEWAL\' )
                                                        ELSE pp.key IN ( \'SCREEN_AFTER_LEASE_COMPLETED\', \'REQUIRE_SCREENING\',\'REMOVED_APPLICANT_SCREEN_REMAINING_APPLICANTS\',\'REQUIRE_SCREENING_FOR_ADDED_APPLICANTS\' )
                                                        END)';

	$strSql = $strWithPtvCondition . 'SELECT
					COUNT( id ), 
					COALESCE( max(priority), 1 ) AS priority
				FROM (
						SELECT
							DISTINCT ON( aa.id )
							aa.id,
							ca.cid,
							CASE
								WHEN TRIM( dp.approvals_applications ->> \'urgent_application_completed_since\') <> \'\' AND CURRENT_DATE >= ( ca.application_completed_on::date + ( dp.approvals_applications->>\'urgent_application_completed_since\' )::int ) THEN
									3
								WHEN TRIM( dp.approvals_applications ->> \'urgent_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.approvals_applications->>\'urgent_move_in_date_within\' )::int ) >= ca.lease_start_date THEN
									3
								WHEN TRIM( dp.approvals_applications ->> \'important_application_completed_since\') <> \'\' AND NOW() >= ( ca.application_completed_on::date + ( dp.approvals_applications->>\'important_application_completed_since\' )::int ) THEN
									2
								WHEN TRIM( dp.approvals_applications ->> \'important_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.approvals_applications->>\'important_move_in_date_within\' )::int ) >= ca.lease_start_date THEN
									2
								ELSE
									1
							END AS priority
						FROM
							cached_applications ca
							JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[' . sqlIntImplode( $arrintRVEnabledPropertyIds ) . '] ) lp ON lp.property_id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . '
							JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id AND aa.customer_type_id IN (  ' . sqlIntImplode( CCustomerType::$c_arrintAllCustomerTypeIds ) . ' ) AND aa.deleted_on IS NULL )
							LEFT JOIN pp ON ( pp.cid = ca.cid AND pp.property_id = lp.property_id AND pp.value <> \'0\' AND pp.key = \'SCREEN_AFTER_LEASE_COMPLETED\' )
							' . $strJoin . '
							LEFT JOIN dashboard_priorities dp ON dp.cid = ca.cid
							LEFT JOIN screening_applicant_results sar ON ( ca.cid = sar.cid AND aa.applicant_id = sar.applicant_id AND ca.id = sar.application_id )
							LEFT JOIN screening_package_customer_types_mappings spctm ON ( ca.cid = spctm.cid AND ca.property_id = spctm.property_id AND aa.customer_type_id = spctm.customer_type_id AND aa.customer_relationship_id = spctm.customer_relationship_id AND spctm.is_active = 1 )
						WHERE
							ca.cid = ' . ( int ) $intCid . '
							AND 
								CASE WHEN ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '
									THEN 
										spctm.lease_type_id = CASE WHEN ca.lease_type_id IS NULL THEN ' . CLeaseType::STANDARD . ' ELSE ca.lease_type_id END
								ELSE
									TRUE
								END
							AND CASE 
									WHEN ca.lease_type_id = ' . ( int ) CLeaseType::CORPORATE . ' 
								THEN
                                    spctm.customer_type_id <> ' . ( int ) CCustomerType::PRIMARY . '
								ELSE
									spctm.screening_applicant_requirement_type_id IN (' . CScreeningApplicantRequirementType::REQUIRED . ', ' . CScreeningApplicantRequirementType::OPTIONAL . ' )
								END
							AND
							CASE
								WHEN pp.id <> 0 AND pp.id IS NOT NULL THEN
									ca.application_stage_id = ' . CApplicationStage::LEASE . '
									AND ca.application_status_id IN ( ' . CApplicationStatus::COMPLETED . ' ) AND aa.completed_on IS NOT NULL
								ELSE         
									ca.application_stage_id = ' . CApplicationStage::APPLICATION . '
									AND ca.application_status_id = ' . CApplicationStatus::COMPLETED . '
							END
							AND CASE
							WHEN spctm.id IS NOT NULL THEN
									spctm.screening_applicant_requirement_type_id <> ' . CScreeningApplicantRequirementType::NOT_REQUIRED . ' 
							ELSE
									TRUE
							END
							AND sar.applicant_id IS NULL
							AND ca.lease_interval_type_id IN ( ' . sqlIntImplode( $arrintAllRVLeaseIntervalTypeIds ) . ' )
							AND CASE
                                WHEN ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' 
                                THEN aa.require_screening IN ( ' . CScreeningUtils::REQUIRE_SCREENING_APPLICANT_SETTING_NO . ' )
                                 ' . $strCaseStatement . '
								ELSE 
                                      TRUE       	
                                END
							AND ' . $strWhere . '
							' . $strLeasingAgentCondition . '
					) applications
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere;
	$arrmixData = fetchData( $strSql, $objDatabase );

	if( true == $boolIsGroupByProperties ) {
		return $arrmixData;
	}

	return ( true == isset( $arrmixData[0] ) ) ? $arrmixData[0] : [ 'count' => 0, 'priority' => 1 ];
	}

	public static function fetchNonRVProcessScreeningApplicationsCountByDashboardFilterByCidByPropertyTypes( $objDashboardFilter, $intCid, $boolIsGroupByProperties = false, $arrintNonRVPropertyIds, $arrmixUserPermissions, $objDatabase ) {

		if( false == valArr( $arrintNonRVPropertyIds ) || false == valObj( $objDashboardFilter, 'CDashboardFilter' ) ) {
			return 0;
		}

		$strLeasingAgentCondition = $strPriorityWhere = $strJoin = $strWhere = '';

		if( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) {
			$strLeasingAgentCondition .= ' AND ( ca.leasing_agent_id IS NULL OR ca.leasing_agent_id IN (' . sqlIntImplode( $objDashboardFilter->getCompanyEmployeeIds() ) . ') )';
		}

		if( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) {
			$strPriorityWhere = ' AND priority IN ( ' . sqlIntImplode( $objDashboardFilter->getTaskPriorities() ) . ' )';
		}

		$arrintNonRVLeaseIntevalTypeIds[] = CLeaseIntervalType::APPLICATION;

		if( true == $arrmixUserPermissions['Renewal'] ) {
			$strJoin .= ' LEFT JOIN property_preferences pp1 ON ( pp1.cid = ca.cid AND pp1.property_id = ca.property_id AND pp1.value <> \'0\' AND pp1.key = \'REQUIRE_SCREENING_AFTER_RENEWAL\' ) ';
			$arrintNonRVLeaseIntevalTypeIds[] = CLeaseIntervalType::RENEWAL;
			$strWhere .= 'CASE
								WHEN ca.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::RENEWAL . ' THEN
                                	     pp1.id IS NOT NULL';
		}

		if( true == $arrmixUserPermissions['Transfer'] ) {
			$strJoin .= ' LEFT JOIN property_preferences pp2 ON ( pp2.cid = ca.cid AND pp2.property_id = ca.property_id AND pp2.value <> \'0\' AND pp2.key = \'REQUIRE_SCREENING\' ) ';
			$arrintNonRVLeaseIntevalTypeIds[] = CLeaseIntervalType::TRANSFER;

			$strWhere .= ( '' == $strWhere ) ? ' CASE
								WHEN ca.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::TRANSFER . ' THEN
                                	     pp2.id IS NOT NULL ' : '
								WHEN ca.lease_interval_type_id = ' . ( int ) CLeaseIntervalType::TRANSFER . ' THEN
                                	     pp2.id IS NOT NULL ';
		}

		$strWhere .= ( '' == $strWhere ) ? ' TRUE ' : ' ELSE TRUE END ';

		$strSql = 'SELECT
						COUNT( id ), 
						COALESCE( max(priority), 1 ) AS priority
					FROM (
							SELECT
								aa.id,
								ca.cid,
								CASE
									WHEN TRIM( dp.approvals_applications ->> \'urgent_application_completed_since\') <> \'\' AND CURRENT_DATE >= ( ca.application_completed_on::date + ( dp.approvals_applications->>\'urgent_application_completed_since\' )::int ) THEN
										3
									WHEN TRIM( dp.approvals_applications ->> \'urgent_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.approvals_applications->>\'urgent_move_in_date_within\' )::int ) >= ca.lease_start_date THEN
										3
									WHEN TRIM( dp.approvals_applications ->> \'important_application_completed_since\') <> \'\' AND NOW() >= ( ca.application_completed_on::date + ( dp.approvals_applications->>\'important_application_completed_since\' )::int ) THEN
										2
									WHEN TRIM( dp.approvals_applications ->> \'important_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.approvals_applications->>\'important_move_in_date_within\' )::int ) >= ca.lease_start_date THEN
										2
									ELSE
										1
								END AS priority
							FROM
								cached_applications ca
								JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[' . sqlIntImplode( $arrintNonRVPropertyIds ) . '] ) lp ON lp.property_id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . '
								JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id AND aa.customer_type_id IN (  ' . sqlIntImplode( CCustomerType::$c_arrintApplicationRequiredCustomerTypeIds ) . ' ) AND aa.deleted_on IS NULL )
								LEFT JOIN property_preferences pp ON ( pp.cid = ca.cid AND pp.property_id = ca.property_id AND pp.value <> \'0\' AND pp.key = \'SCREEN_AFTER_LEASE_COMPLETED\' )
								' . $strJoin . '
								LEFT JOIN dashboard_priorities dp ON dp.cid = ca.cid
								LEFT JOIN applicant_application_transmissions aat ON ( aat.cid = aa.cid AND aat.applicant_application_id = aa.id AND aat.application_id = ca.id )
							WHERE
								ca.cid = ' . ( int ) $intCid . '
								AND CASE 
							        WHEN pp.id <> 0 AND pp.id IS NOT NULL THEN 
							            ca.application_stage_id = ' . CApplicationStage::LEASE . '
							            AND ca.application_status_id IN ( ' . CApplicationStatus::PARTIALLY_COMPLETED . ', ' . CApplicationStatus::COMPLETED . ' ) AND aa.completed_on IS NOT NULL
							        ELSE         
										ca.application_stage_id = ' . CApplicationStage::APPLICATION . '
										AND ca.application_status_id = ' . CApplicationStatus::COMPLETED . '
								END
								AND aat.applicant_application_id IS NULL
                                AND ca.lease_interval_type_id IN ( ' . sqlIntImplode( $arrintNonRVLeaseIntevalTypeIds ) . ' )
								AND  ' . $strWhere . '
								' . $strLeasingAgentCondition . '
								
						) applications
						WHERE
							cid = ' . ( int ) $intCid . '
							' . $strPriorityWhere;
		$arrmixData = fetchData( $strSql, $objDatabase );

		if( true == $boolIsGroupByProperties ) {
			return $arrmixData;
		}

		return ( true == isset( $arrmixData[0] ) ) ? $arrmixData[0] : [ 'count' => 0, 'priority' => 1 ];
	}

	public static function fetchScreeningDecisionPendingApplicationsCountByDashboardFilterByCompanyUserByLeaseIntervalTypesByCid( $objDashboardFilter, $objCompanyUser, $arrintLeaseIntervalTypeIds, $intCid, $objDatabase, $intMaxExecutionTimeOut = 0, $boolIsDashboardAuditReport = false ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) {
			return;
		}

		$strWhere         = '';
		$strPriorityWhere = '';

		if( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) {
			$strPriorityWhere = ' WHERE screening_data.priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )';
		}

		if( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) {
			$strWhere .= ' AND ( ca.leasing_agent_id IN ( ' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ' ) OR ca.leasing_agent_id IS NULL ) ';
		}

		$strSql = ( 0 != $intMaxExecutionTimeOut ) ? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';

		if( true == $boolIsDashboardAuditReport ) {
			$strSql .= 'SELECT 
						COUNT(cnt) AS approvals_screening,
						property_id,
						property_name,
						COALESCE( MAX(priority), 1 ) AS priority';
		} else {
			$strSql .= 'SELECT 
						COUNT(cnt),
						COALESCE( MAX(priority), 1 ) AS priority';
		}

		$strSql .= '
			FROM (
				SELECT
                     DISTINCT(ca.id) as cnt,
                       CASE
                           WHEN TRIM( dp.approvals_applications ->> \'urgent_application_completed_since\') <> \'\' AND CURRENT_DATE >= ( ca.application_completed_on::date + ( dp.approvals_applications->>\'urgent_application_completed_since\' )::int ) THEN
                        3
                           WHEN TRIM( dp.approvals_applications ->> \'urgent_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.approvals_applications->>\'urgent_move_in_date_within\' )::int ) >= ca.lease_start_date THEN
                        3
                           WHEN TRIM( dp.approvals_applications ->> \'important_application_completed_since\') <> \'\' AND NOW() >= ( ca.application_completed_on::date + ( dp.approvals_applications->>\'important_application_completed_since\' )::int ) THEN 
                       2
                       WHEN TRIM( dp.approvals_applications ->> \'important_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.approvals_applications->>\'important_move_in_date_within\' )::int ) >= ca.lease_start_date THEN
                       2
                       ELSE
                       1
                       END as priority,
                       p.id AS property_id,
                       p.property_name
				FROM
					cached_applications ca
					JOIN screening_application_requests sar ON( sar.application_id = ca.id AND sar.cid = ca.cid )
					JOIN properties p ON( p.cid = ca.cid AND p.id = ca.property_id )
					JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . '] ) lp ON lp.property_id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0' : '' ) . '
					JOIN applicant_applications aa ON(aa.application_id = sar.application_id AND aa.cid = sar.cid AND aa.applicant_id = ca.primary_applicant_id )
					JOIN applicant_application_transmissions aat ON( aat.applicant_application_id = aa.id AND aat.cid = aa.cid)
					JOIN screening_recommendation_types srt ON( srt.id = sar.screening_recommendation_type_id )
					JOIN applicants appt ON ( appt.cid = aa.cid AND appt.id = aa.applicant_id )
					LEFT JOIN company_users cu ON ( sar.created_by = cu.id AND cu.cid = sar.cid AND cu.id = ' . ( int ) $objCompanyUser->getId() . ' AND cu.company_user_type_id = 2 )
					LEFT JOIN company_employees ce ON( ce.cid = ca.cid AND ce.id = ca.leasing_agent_id )
					LEFT JOIN dashboard_priorities dp ON ( dp.cid = ca.cid )
					LEFT JOIN property_preferences pp ON ( pp.cid = ca.cid AND pp.property_id = ca.property_id AND pp.value <> \'0\' AND pp.key = \'SCREEN_AFTER_LEASE_COMPLETED\' )
				WHERE
                        sar.cid =  ' . ( int ) $intCid . '
                        AND sar.screening_vendor_id IN ( ' . sqlintImplode( CScreeningApplicationRequest::$c_arrintScreeningDecisionEnabledVendorIds ) . ' )
                         AND CASE 
                          WHEN pp.id <> 0 AND pp.id IS NOT NULL THEN 
                              ca.application_stage_id = ' . CApplicationStage::LEASE . '
                              AND ca.application_status_id = ' . CApplicationStatus::COMPLETED . '
                          ELSE         
                            ca.application_stage_id = ' . CApplicationStage::APPLICATION . '
                           AND ca.application_status_id = ' . CApplicationStatus::COMPLETED . '
                     END    
                        AND sar.application_type_id = ' . CScreeningApplicationType::TENANT . '
                          AND sar.screening_decision_type_id IS NULL
                        AND sar.request_status_type_id = ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED . '
                        AND ca.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' ) ' . $strWhere . '                        
                  ) AS screening_data ' . $strPriorityWhere;

		if( true == $boolIsDashboardAuditReport ) {
			$strSql .= 'GROUP BY
							property_id,
							property_name';
		}

		return fetchOrCacheData( $strSql, $intCacheLifetime = 900, NULL, $objDatabase );
	}

	public static function fetchPaginatedScreeningDecisionPendingApplicationsByDashboardFilterByCompanyUserByLeaseIntervalTypesByCid( $objDashboardFilter, $objCompanyUser, $arrintLeaseIntervalTypeIds, $intCid, $intOffset, $intPageLimit, $strSortBy, $strSortDirection, $objDatabase ) {

		if( false !== \Psi\CStringService::singleton()->strpos( $strSortBy, 'move_in_date' ) || false !== \Psi\CStringService::singleton()->strpos( $strSortBy, 'lease_end_date' ) || false !== \Psi\CStringService::singleton()->strpos( $strSortBy, 'unit_number' ) ) {
			$strSortField = 'priority DESC';
		} else {
			$strSortField = $strSortBy;
		}

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) {
			return NULL;
		}

		$strLeasingAgentCondition = '';
		$strPriorityWhere = '';

		if( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) {
			$strLeasingAgentCondition .= ' AND ( ce.id IS NULL OR ce.id IN (' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ') )';
		}

		if( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) {
			$strPriorityWhere = ' WHERE priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )';
		}

		$strSql = 'SELECT 
               *
            FROM (
                  SELECT
                                   DISTINCT ON( ca.id )
                        ca.id as application_id,
                        ca.cid,
                        func_format_customer_name( appt.name_first, appt.name_last ) as applicant_name,
                      
                        COALESCE( ce.name_first || \' - \' || ce.name_last ) as agent_name,
                        ca.application_completed_on as app_completed_on,
                        ca.application_datetime,
                        ca.lease_id,
                        p.property_name,
                        p.id as property_id,
                        sar.screening_recommendation_type_id as recommendation_id,
                        sar.screening_vendor_id,
                        srt.name as recommendation,
                                   CASE
                         WHEN TRIM( dp.approvals_applications ->> \'urgent_application_completed_since\') <> \'\' AND CURRENT_DATE >= ( ca.application_completed_on::date + ( dp.approvals_applications->>\'urgent_application_completed_since\' )::int ) THEN
                         3
                         WHEN TRIM( dp.approvals_applications ->> \'urgent_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.approvals_applications->>\'urgent_move_in_date_within\' )::int ) >= ca.lease_start_date THEN
                         3
                         WHEN TRIM( dp.approvals_applications ->> \'important_application_completed_since\') <> \'\' AND NOW() >= ( ca.application_completed_on::date + ( dp.approvals_applications->>\'important_application_completed_since\' )::int ) THEN 
                         2
                         WHEN TRIM( dp.approvals_applications ->> \'important_move_in_date_within\') <> \'\' AND ( CURRENT_DATE + ( dp.approvals_applications->>\'important_move_in_date_within\' )::int ) >= ca.lease_start_date THEN
                         2
                         ELSE
                         1
                                   END as priority
                  FROM
                       cached_applications ca
			            JOIN screening_application_requests sar ON( sar.application_id = ca.id AND sar.cid = ca.cid )
						JOIN properties p ON( p.cid = ca.cid AND p.id = ca.property_id )
						JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . '] ) lp ON lp.property_id = ca.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0' : '' ) . '
						JOIN applicant_applications aa ON(aa.application_id = sar.application_id AND aa.cid = sar.cid )
						JOIN applicant_application_transmissions aat ON( aat.applicant_application_id = aa.id AND aat.cid = aa.cid)
						JOIN screening_recommendation_types srt ON( srt.id = sar.screening_recommendation_type_id )
						JOIN applicants appt ON ( appt.cid = aa.cid AND appt.id = aa.applicant_id )
						LEFT JOIN company_users cu ON ( sar.created_by = cu.id AND cu.cid = sar.cid AND cu.id = ' . ( int ) $objCompanyUser->getId() . ' AND cu.company_user_type_id = 2 )
						LEFT JOIN company_employees ce ON( ce.cid = ca.cid AND ce.id = ca.leasing_agent_id )
						LEFT JOIN dashboard_priorities dp ON ( dp.cid = ca.cid )
						LEFT JOIN property_preferences pp ON ( pp.cid = ca.cid AND pp.property_id = ca.property_id AND pp.value <> \'0\' AND pp.key = \'SCREEN_AFTER_LEASE_COMPLETED\' )
               WHERE
               sar.cid =  ' . ( int ) $intCid . '
               AND sar.screening_vendor_id IN ( ' . sqlintImplode( CScreeningApplicationRequest::$c_arrintScreeningDecisionEnabledVendorIds ) . ' )
               AND CASE
                        WHEN ca.lease_type_id = ' . ( int ) CLeaseType::CORPORATE . ' THEN 
                            aa.customer_type_id IN ( ' . implode( ',', CCustomerType::$c_arrintResponsibleCustomerTypeIds ) . ' )
						ELSE 
							aa.applicant_id = ca.primary_applicant_id
						END
               AND CASE
                          WHEN pp.id <> 0 AND pp.id IS NOT NULL THEN
                              ca.application_stage_id = ' . CApplicationStage::LEASE . '
                              AND ca.application_status_id = ' . CApplicationStatus::COMPLETED . '
                          ELSE         
                            ca.application_stage_id = ' . CApplicationStage::APPLICATION . '
                           AND ca.application_status_id = ' . CApplicationStatus::COMPLETED . '
            END    
               AND sar.application_type_id = ' . CScreeningApplicationType::TENANT . '
                   AND sar.screening_decision_type_id IS NULL
               AND sar.request_status_type_id = ' . CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED . '
               AND ca.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ' )
                ' . $strLeasingAgentCondition . '
           ) AS screening_data -- correct data name to valuable
       ' . $strPriorityWhere . '
      ORDER BY
         ' . $strSortField . '
         OFFSET ' . ( int ) $intOffset . '
         LIMIT ' . ( int ) $intPageLimit;

		$arrmixScreeningApplicationDetails = fetchData( $strSql, $objDatabase );
		if( false == valArr( $arrmixScreeningApplicationDetails ) ) return [];
		$arrmixScreeningApplicationDetails = rekeyArray( 'lease_id', $arrmixScreeningApplicationDetails );
		$arrintLeaseIds = array_keys( $arrmixScreeningApplicationDetails );

		if( false !== \Psi\CStringService::singleton()->strpos( $strSortBy, 'move_in_date' ) ) {
			$strSortDirection 	= ( 'ASC' == $strSortDirection ) ? $strSortDirection . ' NULLS FIRST' : $strSortDirection . ' NULLS LAST';
			$strSortBy = ' ORDER BY move_in_date' . ' ' . $strSortDirection;
		} elseif( false !== \Psi\CStringService::singleton()->strpos( $strSortBy, 'lease_end_date' ) ) {
			$strSortDirection = ( 'ASC' == $strSortDirection ) ? $strSortDirection . ' NULLS FIRST' : $strSortDirection . ' NULLS LAST';
			$strSortBy        = ' ORDER BY lease_end_date' . ' ' . $strSortDirection;
		} elseif( 0 == \Psi\CStringService::singleton()->strpos( $strSortBy, 'priority' ) ) {
				$strSortBy = ' ORDER BY unit_number DESC';
		} elseif( false !== \Psi\CStringService::singleton()->strpos( $strSortBy, 'unit_number' ) ) {
				$strSortBy = ' ORDER BY COALESCE( SUBSTRING( unit_number FROM \' ^ ( \\\\d +)\' )::NUMERIC, 99999999  ) ' . $strSortDirection . ',
									SUBSTRING( unit_number FROM \' ^ \\\\d * *(.*?)( \\\\d +)?$\' ) ' . $strSortDirection . ',
									COALESCE( SUBSTRING( unit_number FROM \' ( \\\\d +)$\' )::NUMERIC, 0 ) ' . $strSortDirection . ',
									unit_number ' . $strSortDirection;
		} else {
			$strSortBy = '';
		}

		$strSql = ' SELECT 
							* 
					FROM
						(
							SELECT
								cl.id,
								li.lease_start_date as move_in_date,
								li.lease_end_date as lease_end_date,
								COALESCE( cl.building_name || \' - \' || cl.unit_number_cache, cl.building_name, cl.unit_number_cache ) as unit_number
							FROM
								cached_leases cl
								LEFT JOIN lease_intervals li ON ( li.lease_id = cl.id AND li.cid = cl.cid )
							WHERE
								cl.id In ( ' . implode( ',', $arrintLeaseIds ) . ')
							AND cl.cid = ' . ( int ) $intCid . '
							AND li.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypeIds ) . ')
						) AS subquery
								' . $strSortBy;

		$arrmixScreeningApplicationDateDetails = fetchData( $strSql, $objDatabase );
		$arrmixScreeningApplicationDateDetails = rekeyArray( 'id', $arrmixScreeningApplicationDateDetails );
		if( false == valArr( $arrmixScreeningApplicationDateDetails ) ) return [];

		if( false !== \Psi\CStringService::singleton()->strpos( $strSortBy, 'move_in_date' ) || false !== \Psi\CStringService::singleton()->strpos( $strSortBy, 'lease_end_date' ) || false !== \Psi\CStringService::singleton()->strpos( $strSortBy, 'unit_number' ) ) {
			foreach( $arrmixScreeningApplicationDateDetails as $arrmixScreeningApplicationDateDetail ) {
				$arrmixResult[$arrmixScreeningApplicationDateDetail['id']] = $arrmixScreeningApplicationDetails[$arrmixScreeningApplicationDateDetail['id']] + $arrmixScreeningApplicationDateDetail;
			}
		} else {
			foreach( $arrmixScreeningApplicationDetails as $arrmixScreeningApplicationDetail ) {
				$arrmixResult[$arrmixScreeningApplicationDetail['lease_id']] = $arrmixScreeningApplicationDetail + $arrmixScreeningApplicationDateDetails[$arrmixScreeningApplicationDetail['lease_id']];
			}
		}

		return $arrmixResult;
	}

	public static function fetchScreeningCriteriaMissingApplicantIdsByPropertyIdsByModulePermissionByCid( $arrintPropertyIds, $arrintSelectedCompanyEmployeeIds, $intCid, $objDatabase, $intCurrentEmployeeId = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return [];
		}

		$arrstrWhere = [];

		if( true == valArr( $arrintPropertyIds ) ) {
			$arrstrWhere[] = 'ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}
		$arrintRVLeaseIntevalTypeIds = [ CLeaseIntervalType::RENEWAL, CLeaseIntervalType::LEASE_MODIFICATION, CLeaseIntervalType::TRANSFER ];

		$strWhere = '( sapr.applicant_id IS NULL
									OR ( ap.customer_type_id = ' . CCustomerType::GUARANTOR . ' AND sapr.applicant_id = ap.applicant_id AND sar.screening_decision_type_id = ' . CScreeningDecisionType::APPROVE_WITH_CONDITIONS . '
									AND sapr.screening_recommendation_type_id = ' . CScreeningRecommendationType::FAIL . '
									AND  1 = (
											SELECT
											    1
											 FROM screening_application_condition_sets sacs
											 JOIN screening_application_condition_values sacv ON ( sacs.id = sacv.screening_application_condition_set_id AND sacs.cid = sacv.cid AND sacs.condition_subset_one_id = sacv.condition_subset_id AND sacv.condition_type_id = ' . CScreeningPackageConditionType::ADD_GUARANTOR . ' )
											  WHERE
											    sacs.application_id = ca.id
											    AND sacs.cid = ca.cid
											    AND sacs.is_active = 1
											    LIMIT 1
									        )
									    )
								)	         
						';

		$strSql = 'WITH pp AS (
                                SELECT
                                    *
                                FROM
                                    property_preferences pp
                                WHERE
                                    pp.cid = ' . ( int ) $intCid . '
                                    AND pp.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )  
                                    AND pp.value <> \'0\'
                                    AND pp.key IN ( \'SCREEN_AFTER_LEASE_COMPLETED\', \'REQUIRE_SCREENING\', \'REQUIRE_SCREENING_AFTER_RENEWAL\' ))			    
					 SELECT
						DISTINCT( ap.applicant_id )
					FROM
						cached_applications ca
		                JOIN applicant_applications ap ON ( ap.application_id = ca.id AND ap.cid = ca.cid AND ap.deleted_on IS NULL )
						JOIN property_transmission_vendors ptv ON( ptv.property_id = ca.property_id AND ptv.cid = ca.cid )
						LEFT JOIN screening_application_requests sar ON ( ca.cid = sar.cid AND ca.id = sar.application_id AND sar.screening_decision_type_id IN ( ' . sqlIntImplode( [ CScreeningDecisionType::APPROVE, CScreeningDecisionType::APPROVE_WITH_CONDITIONS ] ) . ' ) )
						LEFT JOIN screening_applicant_results sapr ON ( ca.cid = sapr.cid AND ca.id = sapr.application_id AND sapr.applicant_id = ap.applicant_id )
						LEFT JOIN pp ON ( pp.cid = ca.cid AND pp.property_id = ca.property_id AND pp.key = \'SCREEN_AFTER_LEASE_COMPLETED\' AND pp.value IS NOT NULL )
						LEFT JOIN pp AS pp1 ON ( pp1.cid = ca.cid AND pp1.property_id = ca.property_id AND pp1.key = \'REQUIRE_SCREENING_AFTER_RENEWAL\' AND pp1.value IS NOT NULL )
						LEFT JOIN pp AS pp2 ON ( pp2.cid = ca.cid AND pp2.property_id = ca.property_id AND pp2.key = \'REQUIRE_SCREENING\' AND pp2.value IS NOT NULL )
						JOIN company_transmission_vendors ctv ON( ctv.id = ptv.company_transmission_vendor_id AND ctv.cid = ptv.cid )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND 
						   CASE WHEN (pp.id IS NOT NULL AND ( ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' OR ( ca.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . ' AND pp1.id IS NOT NULL ) OR ( ca.lease_interval_type_id = ' . CLeaseIntervalType::TRANSFER . ' AND pp2.id IS NOT NULL ) ) ) THEN 
						            ' . $strWhere . '        	
						   END       
						AND ctv.transmission_vendor_id = ' . CTransmissionVendor::RESIDENT_VERIFY;

		if( false == valArr( $arrintSelectedCompanyEmployeeIds ) ) {

			$strSql .= ' AND( CASE WHEN ca.application_stage_id <> ' . CApplicationStage::LEASE . ' AND ca.application_status_id <> ' . CApplicationStatus::COMPLETED . '
							THEN ca.leasing_agent_id IS NULL
							ELSE true
						END ) ';

		} elseif( 1 == \Psi\Libraries\UtilFunctions\count( $arrintSelectedCompanyEmployeeIds ) && false == is_null( $intCurrentEmployeeId ) && true == array_key_exists( $intCurrentEmployeeId, $arrintSelectedCompanyEmployeeIds ) ) {
			$strSql .= ' AND( CASE WHEN ca.application_stage_id <> ' . CApplicationStage::LEASE . ' AND ca.application_status_id <> ' . CApplicationStatus::COMPLETED . '
							THEN ca.leasing_agent_id IS NULL OR ca.leasing_agent_id IN ( ' . sqlIntImplode( $arrintSelectedCompanyEmployeeIds ) . ' )
							ELSE true
						END ) ';
		} else {
			$strSql .= ' AND ( ca.leasing_agent_id IN ( ' . sqlIntImplode( $arrintSelectedCompanyEmployeeIds ) . ' ) OR ca.leasing_agent_id IS NULL AND ca.lease_interval_type_id IN ( ' . CLeaseIntervalType::APPLICATION . ', ' . CLeaseIntervalType::RENEWAL . ', ' . CLeaseIntervalType::LEASE_MODIFICATION . ' ))';
		}

		$strSql .= ' AND ca.application_stage_id = ' . CApplicationStage::LEASE . ' AND ca.application_status_id IN ( ' . CApplicationStatus::PARTIALLY_COMPLETED . ', ' . CApplicationStatus::COMPLETED . ' )';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		$arrintApplicantdataIds = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintApplicantdataIds ) ) {
			return [];
		}
		$arrintApplicantIds = [];

		foreach( $arrintApplicantdataIds as $arrintApplicantId ) {
			$arrintApplicantIds[] = $arrintApplicantId['applicant_id'];
		}
		return $arrintApplicantIds;
	}

	public static function fetchWaitlistApplicationsWithWaitlistFilterByPropertyIdsByCid( $objWaitlistFilter, $objDatabase ) {

		if( false == valArr( $objWaitlistFilter->getParentChildPropertyIds() ) || ( true == is_null( $objWaitlistFilter->getCid() ) ) || true == is_null( $objWaitlistFilter->getOccupancyTypeId() ) ) {
			return NULL;
		}

		$strFloorplanWhereSql = ( COccupancyType::AFFORDABLE != $objWaitlistFilter->getOccupancyTypeId() ) ? ' AND ( ( af.floorplan_count > 0 AND wla.wait_start_on IS NOT NULL ) OR ( ca.property_floorplan_id IS NOT NULL ) ) AND wla.wait_end_on IS NULL' : '';

		$strSql = 'SELECT
						ca.cid,
						ca.id application_id,
						ca.lease_id,
						ca.property_id,
						ca.application_completed_on,
						CASE WHEN sar.id IS NOT NULL THEN
								sar.screening_recommendation_type_id
							WHEN aat.id is NOT NULL THEN
								aat.transmission_response_type_id
							ELSE
									0
						END AS screening_recommendation_type_id,
						ca.lease_start_date,
						ca.term_month,
                        ca.desired_bedrooms,
                        ca.desired_bathrooms,
                        ca.occupancy_type_id,
						apl.id AS applicant_id,
						apl.name_first || \' \' || COALESCE ( apl.name_middle, \'\' ) || apl.name_last AS applicant_name,
						COALESCE ( apl.phone_number, apl.mobile_number, apl.work_number, NULL ) AS contact_number,
						apl.username AS email_address,
						ca.property_floorplan_id,
						sub_csdt.disabilities,
						ca.wait_list_position,
						af.floorplan_count,
						array_to_string ( array( SELECT property_floorplan_id FROM application_floorplans WHERE cid = ca.cid AND application_id = ca.id ) , \',\' ) AS application_floorplan_ids,
						sub_ai.unit_space_id as offered_unit_space_id,
						sub_ai.offer_submitted_on,
						rejected_ai.unit_space_id as rejected_unit_space_id,
						array_to_string ( array( SELECT distinct(property_floorplan_id) FROM application_interests WHERE cid = ca.cid AND application_id = ca.id AND unit_space_id IS NULL AND ( event_type_id IS NULL OR event_type_id = ' . ( int ) CEventType::APPLICANT_DECLINED_WAITLIST_OFFER . ' OR event_type_id = ' . ( int ) CEventType::WAITLIST_APPLICATION_REMOVED_BY_PROPERTY . ' ) ) , \',\' ) as rejected_property_floorplan_id
					FROM
						cached_applications ca
						JOIN application_stage_statuses ass ON ( ass.application_stage_id = ca.application_stage_id AND ass.application_status_id = ca.application_status_id AND ass.lease_interval_type_id = ca.lease_interval_type_id )
						JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id AND aa.customer_type_id = ' . ( int ) CCustomerType::PRIMARY . ' )
						JOIN applicants apl ON ( aa.cid = apl.cid AND apl.id = aa.applicant_id )
						JOIN wait_lists wl ON ( wl.cid = ca.cid AND wl.occupancy_type_id = ca.occupancy_type_id AND wl.is_active IS TRUE AND wl.property_group_id IN ( ' . implode( ',', $objWaitlistFilter->getParentChildPropertyIds() ) . ' ) )
						JOIN wait_list_applications wla ON ( wl.cid = wla.cid AND wl.id = wla.wait_list_id AND wla.application_id = ca.id AND wla.wait_end_on IS NULL )
						JOIN load_properties( ARRAY[ ' . ( int ) $objWaitlistFilter->getCid() . ' ]::INTEGER[], ARRAY[ ' . implode( $objWaitlistFilter->getParentChildPropertyIds(), ',' ) . ' ]::INTEGER[] ) lp ON ( wl.cid = lp.cid AND lp.property_id = wl.property_group_id AND lp.is_disabled = 0 )
						LEFT JOIN screening_application_requests sar ON ( sar.cid = ca.cid AND ca.id = sar.application_id AND sar.screening_recommendation_type_id IS NOT NULL )
						LEFT JOIN LATERAL ( SELECT MAX( t.id ) as id FROM transmissions t WHERE t.cid = ca.cid AND ca.id = t.application_id AND t.transmission_type_id = ' . CTransmissionType::SCREENING . ' ) AS t ON TRUE
                        LEFT JOIN LATERAL ( SELECT MAX( aat.id ) as id, aat.transmission_response_type_id FROM applicant_application_transmissions aat WHERE aat.cid = ca.cid AND ca.id = aat.application_id AND aat.applicant_application_id = aa.id AND t.id = aat.transmission_id GROUP BY aat.id, aat.transmission_response_type_id ) AS aat ON TRUE
						LEFT JOIN LATERAL ( SELECT COUNT(*) AS floorplan_count FROM application_floorplans WHERE cid = ca.cid AND application_id = ca.id ) AS af ON TRUE
						LEFT JOIN 
                        (
                            SELECT 
                                cid, customer_id, array_to_string( array_agg( sdt.name ), \',\') AS disabilities
                            FROM
                                customer_subsidy_disability_types csdt
                            JOIN 
                                subsidy_disability_types sdt ON ( csdt.subsidy_disability_type_id = sdt.id )
                            WHERE
                                csdt.cid = ' . ( int ) $objWaitlistFilter->getCid() . '
                            GROUP BY 
                                customer_id, cid
                        ) sub_csdt ON ( sub_csdt.cid = ca.cid and sub_csdt.customer_id = apl.customer_id ) 
                        LEFT JOIN
						(
							SELECT
								ai.application_id,
								ai.unit_space_id,
								ai.cid,
								ai.offer_submitted_on,
								ai.event_type_id,
								rank () OVER ( PARTITION BY ai.application_id, ai.unit_space_id ORDER BY ai.offer_submitted_on DESC ) AS rank
							FROM
								application_interests ai
							WHERE
								ai.cid = ' . ( int ) $objWaitlistFilter->getCid() . '
								AND ai.property_id IN ( ' . implode( ',', $objWaitlistFilter->getParentChildPropertyIds() ) . ' )
								AND ( ai.event_type_id IS NULL OR ai.event_type_id = ' . ( int ) CEventType::GENERATE_WAITLIST_OFFER . ' )
						) sub_ai ON ( sub_ai.cid = aa.cid AND sub_ai.application_id = aa.application_id AND sub_ai.rank = 1 )
						LEFT JOIN
						(
							SELECT
								ai.application_id,
								ai.unit_space_id,
								ai.cid,
								ai.offer_submitted_on,
								ai.event_type_id,
								rank () OVER ( PARTITION BY ai.application_id ORDER BY ai.offer_submitted_on DESC ) AS rank
							FROM
								application_interests ai
							WHERE
								ai.cid = ' . ( int ) $objWaitlistFilter->getCid() . '
								AND ai.property_id IN ( ' . implode( ',', $objWaitlistFilter->getParentChildPropertyIds() ) . ' )
								AND ( ai.event_type_id IS NULL OR ai.event_type_id = ' . ( int ) CEventType::APPLICANT_DECLINED_WAITLIST_OFFER . ' OR ai.event_type_id = ' . ( int ) CEventType::WAITLIST_APPLICATION_REMOVED_BY_PROPERTY . ' )
						) rejected_ai ON ( rejected_ai.cid = aa.cid AND rejected_ai.application_id = aa.application_id AND rejected_ai.rank = 1 )
					WHERE
						ca.cid = ' . ( int ) $objWaitlistFilter->getCid() . '
						AND ca.property_id IN ( ' . implode( ',', $objWaitlistFilter->getParentChildPropertyIds() ) . ' )
						AND ca.occupancy_type_id = ' . ( int ) $objWaitlistFilter->getOccupancyTypeId() . ' 
						AND ca.lease_status_type_id = ' . ( int ) CLeaseStatusType::APPLICANT . '
						AND ( CASE WHEN ass.id = ' . CApplicationStageStatus::PROSPECT_STAGE3_PARTIALLY_COMPLETED . ' THEN ' . CApplicationStageStatus::PROSPECT_STAGE3_COMPLETED . ' ELSE ass.id END >= wl.application_stage_status_id OR wla.is_exception IS TRUE )
						AND ca.unit_space_id IS NULL ' . $strFloorplanWhereSql . '
						AND ca.unit_space_id IS NULL 
						AND CASE WHEN aat.id IS NOT NULL AND sar.id IS NULL THEN
                             aat.id = ( 
	                            SELECT
							          MAX ( aat1.id ) OVER ( PARTITION BY aat1.applicant_application_id ) AS max_transmission_id
							      FROM
							          applicant_application_transmissions aat1
						      WHERE
							          aat1.cid = ca.cid
							          AND aat1.application_id = ca.id
							          AND aat1.applicant_application_id = aa.id
                                      LIMIT 1
	                             )
                           ELSE
                                TRUE
                           END
						AND ca.application_status_id NOT IN ( ' . CApplicationStatus::ON_HOLD . ',' . CApplicationStatus::CANCELLED . ' )
						ORDER BY ca.wait_list_position NULLS LAST, wla.wait_start_on';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCachedApplicationDetailsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT 
						ca.name_first,
                        ca.name_last,
                        ca.property_name,
                        ca.lease_start_date,
                        ce.name_first as agent_first_name,
                        ce.name_last as agent_last_name,
					    ls.name as lead_source_name,
					    pfp.floorplan_name
					FROM
						cached_applications ca
						LEFT JOIN property_floorplans pfp ON ( ca.property_floorplan_id = pfp.id AND ca.cid = pfp.cid )
						LEFT JOIN lead_sources as ls ON ( ca.originating_lead_source_id = ls.id AND ca.cid = ls.cid )
						LEFT JOIN company_employees ce ON (ca.leasing_agent_id = ce.id AND ca.cid = ce.cid )
					WHERE
						ca.id = ' . ( int ) $intApplicationId . '
						AND ca.cid = ' . ( int ) $intCid . '
					';

		$arrstrData = fetchData( $strSql, $objDatabase );
		return ( true == valArr( $arrstrData ) ) ? $arrstrData[0] : NULL;
	}

	public static function fetchLeadConversionCountByPropertyGroupIdsByCid( array $arrintPropertyGroupIds, int $intCid, CDatabase $objDatabase, $boolShowDisabledData = false ) : array {

		if( !valArr( $arrintPropertyGroupIds ) ) {
			return [];
		}

		$arrintDashboardEventTypes = [ CEventType::ONSITE_VISIT, CEventType::TOUR, CEventType::SELF_GUIDED_TOUR, CEventType::VIRTUAL_TOUR ];

		$objTodayDateTime	= new DateTime();
		$objMondayDateTime	= clone $objTodayDateTime->modify( ( 'Sunday' == $objTodayDateTime->format( 'l' ) ) ? 'previous Monday' : 'Monday this week' );
		$objSundayDateTime	= clone $objTodayDateTime->modify( 'Sunday this week' );

		$strStartDateTime	= $objMondayDateTime->format( 'm/d/Y 00:00:00' );
		$strEndDateTime		= $objSundayDateTime->format( 'm/d/Y 23:59:59' );
		$strStartDate		= $objMondayDateTime->format( 'm/d/Y' );
		$strEndDate			= $objSundayDateTime->format( 'm/d/Y' );

		$strSql				= '
			SELECT
				COUNT( DISTINCT CASE 
					WHEN cal.application_datetime >= \'' . $strStartDateTime . '\' AND cal.application_datetime <= \'' . $strEndDateTime . '\'
						THEN cal.application_id
				END ) AS new_leads,
				COUNT( DISTINCT CASE
					WHEN events_before_start_date.event_lease_interval_id IS NULL AND a.event_type_id IN ( ' . sqlIntImplode( $arrintDashboardEventTypes ) . ' )
						AND ' . CDefaultEventResult::COMPLETED . ' = ANY ( a.default_event_result_ids )
						AND a.event_datetime <= \'' . $strEndDateTime . '\'
							THEN cal.application_id
				END ) AS first_visit_tour,
				COUNT( DISTINCT CASE
					WHEN ( 10 * cal_prior.max_application_stage_id + cal_prior.max_application_status_id ) < ( 10 * ' . CApplicationStage::APPLICATION . ' + ' . CApplicationStatus::PARTIALLY_COMPLETED . ' )
						AND ( 10 * cal.max_application_stage_id + cal.max_application_status_id ) >= ( 10 * ' . CApplicationStage::APPLICATION . ' + ' . CApplicationStatus::PARTIALLY_COMPLETED . ' )
							THEN cal.application_id
					WHEN ( 10 * cal_prior.max_application_stage_id + cal_prior.max_application_status_id ) BETWEEN ( 10 * ' . CApplicationStage::APPLICATION . ' + ' . CApplicationStatus::PARTIALLY_COMPLETED . ' ) AND ( 10 * ' . CApplicationStage::APPLICATION . ' + ' . CApplicationStatus::COMPLETED . ' )
						AND cal_prior.application_status_id IN ( ' . CApplicationStatus::ON_HOLD . ', ' . CApplicationStatus::CANCELLED . ' )
						AND cal.application_status_id NOT IN ( ' . CApplicationStatus::ON_HOLD . ', ' . CApplicationStatus::CANCELLED . ' )
							THEN cal.application_id
				END ) AS applications_completed
			FROM
				cached_application_logs AS cal
				JOIN load_properties( ARRAY[ ' . $intCid . ' ], ARRAY[ ' . sqlIntImplode( $arrintPropertyGroupIds ) . ' ] ) AS lp ON lp.property_id = cal.property_id AND lp.is_disabled = 0 AND lp.is_test = 0
				LEFT JOIN (
					SELECT
						cal_inner.cid,
						cal_inner.application_id,
						cal_inner.id,
						cal_inner.max_application_stage_id,
						cal_inner.max_application_status_id,
						cal_inner.application_status_id
					FROM
						cached_application_logs AS cal_inner
						JOIN load_properties( ARRAY[ ' . $intCid . ' ], ARRAY[ ' . sqlIntImplode( $arrintPropertyGroupIds ) . ' ] ) AS lp ON lp.property_id = cal_inner.property_id AND lp.is_disabled = 0 AND lp.is_test = 0
					WHERE
						cal_inner.cid = ' . $intCid . '
						AND cal_inner.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '
						AND cal_inner.occupancy_type_id IS DISTINCT FROM ' . COccupancyType::OTHER_INCOME . '
						AND cal_inner.is_post_date_ignored = false
						AND \'' . $strStartDate . '\'::DATE - INTERVAL \'1 DAY\' BETWEEN cal_inner.reporting_post_date AND cal_inner.apply_through_post_date
				) AS cal_prior ON cal_prior.cid = cal.cid AND cal_prior.application_id = cal.application_id AND cal_prior.id <> cal.id
				LEFT JOIN (
					SELECT
						e.cid,
						e.lease_interval_id,
						e.event_type_id,
						COALESCE( er.default_event_result_ids, \'{}\' ) AS default_event_result_ids,
						e.event_datetime
					FROM
						events AS e
						JOIN load_properties ( ARRAY[ ' . $intCid . ' ], ARRAY [ ' . sqlIntImplode( $arrintPropertyGroupIds ) . ' ] ) AS lp ON lp.property_id = e.property_id AND lp.is_disabled = 0 AND lp.is_test = 0
						LEFT JOIN event_results er ON er.cid = e.cid AND er.id = e.event_result_id
					WHERE
						e.cid = ' . $intCid . '
						AND e.event_datetime BETWEEN \'' . $strStartDateTime . '\' AND \'' . $strEndDateTime . '\'
						AND e.event_type_id IN ( ' . sqlIntImplode( $arrintDashboardEventTypes ) . ' )
						AND e.lease_interval_id IS NOT NULL
						AND e.is_deleted = FALSE
				) AS a ON a.cid = cal.cid AND a.lease_interval_id = cal.lease_interval_id AND ( a.event_datetime <= cal.lease_approved_on OR cal.lease_approved_on IS NULL )
				LEFT JOIN (
					SELECT
						e.cid,
						e.lease_interval_id AS event_lease_interval_id
					FROM
						events AS e
						JOIN load_properties ( ARRAY[ ' . $intCid . ' ], ARRAY [ ' . sqlIntImplode( $arrintPropertyGroupIds ) . ' ] ) AS lp ON lp.property_id = e.property_id AND lp.is_disabled = 0 AND lp.is_test = 0
						JOIN event_results er ON er.cid = e.cid AND er.id = e.event_result_id
					WHERE
						e.cid = ' . $intCid . '
						AND e.event_datetime < \'' . $strStartDateTime . '\'
						AND e.lease_interval_id IS NOT NULL
						AND e.event_type_id IN ( ' . sqlIntImplode( $arrintDashboardEventTypes ) . ' )
						AND ' . CDefaultEventResult::COMPLETED . ' = ANY( er.default_event_result_ids )
					GROUP BY
						e.cid,
						e.lease_interval_id
				) AS events_before_start_date ON events_before_start_date.event_lease_interval_id = cal.lease_interval_id
			WHERE
				cal.cid = ' . $intCid . '
				AND cal.reporting_post_date < ( \'' . $strEndDate . '\'::DATE + INTERVAL \'1 DAY\' ) AND cal.apply_through_post_date >= \'' . $strEndDate . '\' AND cal.is_post_date_ignored = false
				AND cal.occupancy_type_id <> ' . COccupancyType::OTHER_INCOME . '
				AND cal.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '
				' . CReportCommonQueries::getWhereConditionForArchivedLeadsSql( $intCid, 'cal' ) . '
				AND cal.effective_date >= \'' . $strStartDate . '\'
				AND CASE
					WHEN cal.application_datetime >= \'' . $strStartDateTime . '\' AND cal.application_datetime <= \'' . $strEndDateTime . '\'
						THEN TRUE
					WHEN events_before_start_date.event_lease_interval_id IS NULL AND a.event_type_id IN ( ' . sqlIntImplode( $arrintDashboardEventTypes ) . ' )
						AND ' . CDefaultEventResult::COMPLETED . ' = ANY ( a.default_event_result_ids )
						AND a.event_datetime <= \'' . $strEndDateTime . '\'
							THEN TRUE
					WHEN ( 10 * cal_prior.max_application_stage_id + cal_prior.max_application_status_id ) < ( 10 * ' . CApplicationStage::APPLICATION . ' + ' . CApplicationStatus::PARTIALLY_COMPLETED . ' )
						AND ( 10 * cal.max_application_stage_id + cal.max_application_status_id ) >= ( 10 * ' . CApplicationStage::APPLICATION . ' + ' . CApplicationStatus::PARTIALLY_COMPLETED . ' )
							THEN TRUE
					WHEN ( 10 * cal_prior.max_application_stage_id + cal_prior.max_application_status_id ) BETWEEN ( 10 * ' . CApplicationStage::APPLICATION . ' + ' . CApplicationStatus::PARTIALLY_COMPLETED . ' ) AND ( 10 * ' . CApplicationStage::APPLICATION . ' + ' . CApplicationStatus::COMPLETED . ' )
						AND cal_prior.application_status_id IN ( ' . CApplicationStatus::ON_HOLD . ', ' . CApplicationStatus::CANCELLED . ' )
						AND cal.application_status_id NOT IN ( ' . CApplicationStatus::ON_HOLD . ', ' . CApplicationStatus::CANCELLED . ' )
							THEN TRUE
				END
		';

		$arrstrData = fetchData( $strSql, $objDatabase );
		return valArr( $arrstrData ) ? $arrstrData : [];
	}

	public static function fetchPreLeaseVelocityAndRentByLeaseStartWindowIdsByPropertyIdByCid( $strLeaseStartWindowIds, $intPropertyId, $intCid, $objDatabase, $objStudentDashboardFilter ) {

		if( false == valStr( $strLeaseStartWindowIds ) ) {
			$strLeaseStartWindowIds = '0';
		}
		$strSpaceConfigurationCnd = '';
		if( true == valArr( $objStudentDashboardFilter->getSpaceConfigurationIds() ) ) {
			$strSpaceConfigurationCnd = ' AND ca.space_configuration_id IN (' . implode( ',', $objStudentDashboardFilter->getSpaceConfigurationIds() ) . ')';
		}

		$strFloorPlanIdsCnd = '';

		if( true == valArr( $objStudentDashboardFilter->getFloorPlanIds() ) || true == valArr( $objStudentDashboardFilter->getBedroomCounts() ) ) {
			$strFloorPlanIdsCnd = 'JOIN property_floorplans pf on ca.property_floorplan_id = pf.id and ca.cid = pf.cid and ca.property_id = pf.property_id ';
			if( true == valArr( $objStudentDashboardFilter->getFloorPlanIds() ) ) {
				$strFloorPlanIdsCnd = $strFloorPlanIdsCnd . ' AND pf.id IN (' . implode( ',', $objStudentDashboardFilter->getFloorPlanIds() ) . ')';
			}
			if( true == valArr( $objStudentDashboardFilter->getBedroomCounts() ) ) {
				$strFloorPlanIdsCnd = $strFloorPlanIdsCnd . ' AND pf.number_of_bedrooms IN (' . implode( ',', $objStudentDashboardFilter->getBedroomCounts() ) . ')';
			}
		}

		$strUnitTypeCnd = '';
		$strTotalUnitSpacesUnitTypeCnd = '';
		if( true == valArr( $objStudentDashboardFilter->getUnitTypeIds() ) ) {
			$strUnitTypeAllCnd = ( 1 < \Psi\Libraries\UtilFunctions\count( $objStudentDashboardFilter->getUnitTypeIds() ) ? 'OR ca.unit_type_id IS NULL' : '' );
			$strUnitTypeCnd = ' AND ( ca.unit_type_id IN (' . implode( ',', $objStudentDashboardFilter->getUnitTypeIds() ) . ') ' . $strUnitTypeAllCnd . ' )';
			$strTotalUnitSpacesUnitTypeCnd = ' us.unit_type_id IN(' . implode( ',', $objStudentDashboardFilter->getUnitTypeIds() ) . ') AND ';
		}

		$strLeaseApproveCondition = ' AND ca.application_stage_id = ' . $objStudentDashboardFilter->getApplicationStageId() . ' AND ca.application_status_id = ' . $objStudentDashboardFilter->getApplicationStatusId();
		$strLeaseCondition        = '';
		if( 'ca.lease_approved_on' != trim( $objStudentDashboardFilter->getLeaseRecognizeAt() ) ) {
			$strLeaseApproveCondition = '';
			$strLeaseCondition = ' AND ca.application_stage_id = ' . $objStudentDashboardFilter->getApplicationStageId() . ' AND ca.application_status_id = ' . $objStudentDashboardFilter->getApplicationStatusId();
		}

		$strSql = sprintf( 'WITH lease_terms AS (
		            SELECT 
							lt.cid,
							lsw.property_id,
							lsw.id as current_lease_start_window_id,
							lsw.start_date as current_window_start_date,
							lsw.end_date as current_window_end_date,
							CASE 
								WHEN (row_number() OVER(PARTITION BY lt.cid, lsw.property_id, pre_lsw.id ) ) <> 1 THEN NULL
								ELSE pre_lsw.id
							END AS past_lease_start_window_id,
							pre_lsw.start_date as past_window_start_date,
							pre_lsw.end_date as past_window_end_date,
							CASE 
								WHEN (row_number() OVER(PARTITION BY lt.cid, lsw.property_id, ppre_lsw.id ) ) <> 1 THEN NULL
								ELSE ppre_lsw.id
							END AS past_past_lease_window_id,
							ppre_lsw.start_date as past_past_start_date,
							ppre_lsw.end_date as past_past_end_date,
							pp1.value::DATE + INTERVAL \'1 year\' AS acedemic_year_start_date,
							pp2.value::DATE + INTERVAL \'1 year\' AS acedemic_year_end_date,
							pp1.value::DATE AS past_acedemic_year_start_date,
							pp2.value::DATE AS past_acedemic_year_end_date,
							pp1.value::DATE - INTERVAL \'1 year\' AS past_past_acedemic_year_start_date,
							pp2.value::DATE - INTERVAL \'1 year\' AS past_past_acedemic_year_end_date
						FROM 
							lease_terms lt
							JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id )
							LEFT JOIN LATERAL
							(
								SELECT 
									p_lsw.*
								FROM 
									lease_start_windows p_lsw
								WHERE 
									p_lsw.cid = lt.cid 
									AND p_lsw.property_id = lsw.property_id 
									AND p_lsw.start_date < lsw.start_date
									AND extract(MONTH FROM p_lsw.start_date)::INTEGER = extract(MONTH FROM lsw.start_date)::INTEGER
									AND extract(YEAR FROM p_lsw.start_date)::INTEGER = extract(YEAR FROM lsw.start_date)::INTEGER - 1
								ORDER BY 
									CASE 
										WHEN p_lsw.lease_term_id = lt.id THEN TRUE
										WHEN ( abs( (lsw.end_date::DATE - lsw.start_date::DATE) - (p_lsw.end_date::DATE - p_lsw.start_date::DATE) ) <= 5 )
											AND ( abs( extract(DAY FROM p_lsw.start_date)::INTEGER - extract(DAY FROM lsw.start_date)::INTEGER ) <= 10 ) THEN TRUE
									END,
									p_lsw.start_date ASC
								LIMIT 1
							) AS pre_lsw ON pre_lsw.cid = lt.cid
							LEFT JOIN LATERAL
				           (
				             SELECT p_lsw.cid,
				                    p_lsw.id,
				                    p_lsw.start_date,
				                    p_lsw.end_date
				             FROM lease_start_windows p_lsw
				             WHERE p_lsw.cid = lt.cid AND
				                   p_lsw.property_id = lsw.property_id AND
				                   p_lsw.start_date < lsw.start_date AND
				                   extract(MONTH
				             FROM p_lsw.start_date)::INTEGER = extract(MONTH
				             FROM lsw.start_date)::INTEGER AND extract(YEAR
				             FROM p_lsw.start_date)::INTEGER = extract(YEAR
				             FROM lsw.start_date)::INTEGER - 2
				             ORDER BY CASE
				                        WHEN p_lsw.lease_term_id = lt.id THEN TRUE
				                        WHEN (abs((lsw.end_date::DATE - lsw.start_date::DATE) -(
				                          p_lsw.end_date::DATE - p_lsw.start_date::DATE)) <= 5)
				                          AND (abs(extract(DAY
				             FROM p_lsw.start_date)::INTEGER - extract(DAY
				             FROM lsw.start_date)::INTEGER) <= 10) THEN TRUE
				                      END,
				                  p_lsw.start_date ASC
				             LIMIT 1
				           ) AS ppre_lsw ON ppre_lsw.cid = lt.cid
							JOIN properties p ON ( p.cid = lsw.cid AND p.id = lsw.property_id )
							JOIN load_properties( ARRAY [ %1$d ], ARRAY [ %2$d ] ) lp ON lp.is_disabled = 0 AND lp.property_id = p.id
							JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id AND lsw.property_id = pcs.property_id )
							JOIN load_property_preferences( %1$d, ARRAY[ %2$d ], ARRAY[\'PRICING_STUDENT_PRELEASE_START_DAY\'] ) AS pp1 ON true
							JOIN load_property_preferences( %1$d, ARRAY[ %2$d ], ARRAY[\'PRICING_STUDENT_PRELEASE_END_DAY\'] ) AS pp2 ON true
						WHERE 
							lt.cid = %1$d
							AND lsw.id IN( %10$s ) 
							AND lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . '
							AND ( ( lsw.start_date <= CURRENT_DATE AND ( lsw.end_date >= CURRENT_DATE OR lsw.end_date IS NULL ) ) OR ( lsw.start_date > CURRENT_DATE AND ( lsw.end_date >= CURRENT_DATE OR lsw.end_date IS NULL ) ) )
						ORDER BY (lsw.start_date, lsw.end_date) ASC
					),
					lease_term AS (
						SELECT 
							CASE
								WHEN
									current_window_end_date <= acedemic_year_end_date::DATE AND current_window_end_date >= acedemic_year_start_date
								THEN current_lease_start_window_id
								ELSE NULL
							END AS current_lease_start_window_id_1,
							CASE
								WHEN
									current_window_end_date <= acedemic_year_end_date::DATE AND current_window_end_date >= acedemic_year_start_date
								THEN current_window_start_date
								ELSE NULL
							END AS current_window_start_date_1,
							CASE
								WHEN
									current_window_end_date <= past_acedemic_year_end_date::DATE AND current_window_end_date >= past_acedemic_year_start_date
								THEN current_lease_start_window_id
								WHEN
									past_window_end_date <= past_acedemic_year_end_date::DATE AND past_window_end_date >= past_acedemic_year_start_date
								THEN past_lease_start_window_id
								ELSE NULL
							END AS past_lease_start_window_id_1,
							CASE
								WHEN
									current_window_end_date <= past_acedemic_year_end_date::DATE AND current_window_end_date >= past_acedemic_year_start_date
								THEN current_window_start_date
								WHEN
									past_window_end_date <= past_acedemic_year_end_date::DATE AND past_window_end_date >= past_acedemic_year_start_date
								THEN past_window_start_date
								ELSE NULL
							END AS past_window_start_date_1,
							CASE
								WHEN
									current_window_end_date <= past_past_acedemic_year_end_date::DATE AND current_window_end_date >= past_past_acedemic_year_start_date
								THEN current_lease_start_window_id
								WHEN
									past_window_end_date <= past_past_acedemic_year_end_date::DATE AND past_window_end_date >= past_past_acedemic_year_start_date
								THEN past_lease_start_window_id
								WHEN
									past_past_end_date <= past_past_acedemic_year_end_date::DATE AND past_past_end_date >= past_past_acedemic_year_start_date
								THEN past_past_lease_window_id
								ELSE NULL
							END AS past_past_lease_window_id_1,
							CASE
								WHEN
									current_window_end_date <= past_past_acedemic_year_end_date::DATE AND current_window_end_date >= past_past_acedemic_year_start_date
								THEN current_window_start_date
								WHEN
									past_window_end_date <= past_past_acedemic_year_end_date::DATE AND past_window_end_date >= past_past_acedemic_year_start_date
								THEN past_window_start_date
								WHEN
									past_past_end_date <= past_past_acedemic_year_end_date::DATE AND past_past_end_date >= past_past_acedemic_year_start_date
								THEN past_past_start_date
								ELSE NULL
							END AS past_past_start_date_1,
							*
						FROM 
							lease_terms
					),
					advertise_dates AS (
						SELECT
							ad.advertised_date as current_advertised_date,
							ad.past_advertised_date_range as past_advertised_date,
							ad.past_past_advertised_date_range as past_past_advertised_date,
							*
						FROM (
							SELECT 
								generate_series(last_day_of_week.last_date_of_week, CASE WHEN CURRENT_DATE > pp2.value::DATE THEN CURRENT_DATE ELSE pp2.value::DATE END , \'1 week\'::INTERVAL)::DATE AS advertised_date,
								generate_series(last_day_of_week.last_date_of_week - INTERVAL \'1 year\', CASE WHEN CURRENT_DATE > pp2.value::DATE THEN CURRENT_DATE - INTERVAL \'1 year\' ELSE pp2.value::DATE - INTERVAL \'1 year\' END , \'1 week\'::INTERVAL)::DATE AS past_advertised_date_range,
								generate_series(last_day_of_week.last_date_of_week - INTERVAL \'2 year\', CASE WHEN CURRENT_DATE > pp2.value::DATE THEN CURRENT_DATE - INTERVAL \'2 year\' ELSE pp2.value::DATE - INTERVAL \'2 year\' END , \'1 week\'::INTERVAL)::DATE AS past_past_advertised_date_range,
								lt.cid,
								lt.property_id,
								current_lease_start_window_id_1 AS current_lease_start_window_id,
								current_window_start_date_1 AS current_window_start_date,
								past_lease_start_window_id_1 AS past_lease_start_window_id,
								current_window_end_date,
								past_window_start_date_1 AS past_window_start_date,
								past_window_end_date,
								past_past_lease_window_id_1 AS past_past_lease_window_id,
								past_past_start_date_1 AS past_past_start_date,
								past_past_end_date 
							FROM lease_term lt
								JOIN load_property_preferences( %1$d, ARRAY[ %2$d ], ARRAY[\'PRICING_STUDENT_PRELEASE_END_DAY\'] ) AS pp2 ON true
								LEFT JOIN LATERAL
					            (
					                SELECT 
					                    MIN(sub.start_date) AS last_date_of_week
					                FROM 
					                    (
					                    SELECT 
					                        generate_series(
					                        ad.start_date,
					                        CASE
					                            WHEN ( \'08/01/\' || extract(YEAR FROM ad.prelease_start_date)::INTEGER )::DATE > ad.start_date THEN ad.start_date
					                            ELSE ( \'08/01/\' || extract(YEAR FROM ad.prelease_start_date)::INTEGER )::DATE
					                        END, 
					                        \'-1 week\'::INTERVAL) AS start_date
					                    FROM
					                        (
					                            SELECT
					                                pp2.value::DATE + ( ( CASE
					                                           WHEN pp1.value = \'Sunday\' THEN 7
					                                           WHEN pp1.value = \'Monday\' THEN 8
					                                           WHEN pp1.value = \'Tuesday\' THEN 9
					                                           WHEN pp1.value = \'Wednesday\' THEN 10
					                                           WHEN pp1.value = \'Thursday\' THEN 11
					                                           WHEN pp1.value = \'Friday\' THEN 12
					                                           WHEN pp1.value = \'Saturday\' THEN 13
					                                         END - extract ( DOW FROM pp2.value::DATE )::int ) %% 7 ) AS start_date,
					                                pp2.value::DATE AS prelease_start_date
					                            FROM
					                                load_property_preferences ( %1$d, ARRAY [ %2$d ], ARRAY [ \'PRICING_STUDENT_LAST_DAY_OF_WEEK\' ] ) AS pp1
					                                JOIN load_property_preferences ( %1$d, ARRAY [ %2$d ], ARRAY [ \'PRICING_STUDENT_PRELEASE_START_DAY\' ] ) AS pp2 ON TRUE
					                        ) AS ad
					                ) AS sub
					            ) AS last_day_of_week ON TRUE
						) as ad
					),
					cached_applications_temp AS (
						SELECT 
							ca.*,
							sc.unit_space_count
                        FROM    
							cached_applications ca
						JOIN cached_leases cl ON cl.cid = ca.cid AND cl.property_id = ca.property_id AND cl.id = ca.lease_id
						JOIN lease_intervals li ON cl.cid = li.cid AND cl.property_id = li.property_id AND cl.id = li.lease_id AND ca.lease_interval_id = li.id
						JOIN space_configurations sc on ca.space_configuration_id = sc.id AND sc.cid = ca.cid  and sc.unit_space_count > 0
						%8$s
						WHERE
							ca.cid = %1$d  AND ca.property_id = %2$d
							%11$s							
                            %6$s %7$s AND ca.lease_interval_type_id IN ( %9$s )
					),
				total_unit_spaces AS (
						SELECT
							us.cid,
							us.property_id,
							count(us.id) AS unit_space_count
						FROM
							unit_spaces us
						WHERE
							us.cid = %1$d  AND us.property_id = %2$d AND ' . $strTotalUnitSpacesUnitTypeCnd . ' 
							us.deleted_on IS NULL
						GROUP BY
							us.cid,
							us.property_id
					),
					pre_lease_temp AS ( 
					SELECT 
						sub.cid,
						sub.property_id,
						sub.current_advertised_date,
						sub.past_advertised_date,
						sub.past_past_advertised_date,
						to_char(sub.current_advertised_date, \'Mon-dd\') AS month_name,
						ROUND(AVG(current_rent)) AS weekly_current_rent,
						SUM(current_lease_velocity) as current_leased_count,
						ROUND(AVG(past_rent)) AS weekly_past_rent,
						SUM(past_lease_velocity) as past_leased_count,
						SUM(current_lease_velocity) * 12 AS current_rented_space_months,
						SUM(past_lease_velocity) * 12 AS past_rented_space_months,
						ROUND(AVG(past_past_rent)) AS weekly_past_past_rent,
                        SUM(past_past_lease_velocity) as past_past_leased_count,
                        SUM(past_past_lease_velocity) * 12 AS past_past_rented_space_months
					FROM (
						SELECT 
							ad.*,
							CASE
								WHEN 
									ca.lease_start_window_id = ad.current_lease_start_window_id 
									AND %3$s >= ad.current_advertised_date - INTERVAL \'1 week\' AND %3$s < ad.current_advertised_date
									AND ca.lease_status_type_id NOT IN ( ' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . ' ) 
									AND ca.occupancy_type_id NOT IN ( ' . implode( ',', array_merge( COccupancyType::$c_arrintExcludedOccupancyTypes, [ COccupancyType::OTHER_INCOME ] ) ) . ' )
									AND ( ca.lease_start_date <= ( ( \'09/30/\' || extract(YEAR FROM ad.current_window_start_date)::INTEGER )::DATE ) AND ( ca.lease_end_date >= ( \'09/30/\' || extract(YEAR FROM ad.current_window_start_date)::INTEGER )::DATE OR ca.lease_end_date IS NULL ) )
								THEN ca.executed_monthly_rent_base/ca.unit_space_count
								ELSE NULL
							END AS current_rent,
							CASE
								WHEN
									ca.lease_start_window_id = ad.current_lease_start_window_id 
									AND %3$s >= ad.current_advertised_date - INTERVAL \'1 week\' AND %3$s < ad.current_advertised_date 
									AND ca.lease_status_type_id NOT IN ( ' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . ' ) 
									AND ca.occupancy_type_id NOT IN ( ' . implode( ',', array_merge( COccupancyType::$c_arrintExcludedOccupancyTypes, [ COccupancyType::OTHER_INCOME ] ) ) . ' )
									AND ( ca.lease_start_date <= ( ( \'09/30/\' || extract(YEAR FROM ad.current_window_start_date)::INTEGER )::DATE ) AND ( ca.lease_end_date >= ( \'09/30/\' || extract(YEAR FROM ad.current_window_start_date)::INTEGER )::DATE OR ca.lease_end_date IS NULL ) )
								THEN 1
								ELSE 0
							END AS current_lease_velocity,
							CASE
								WHEN ca.lease_start_window_id = ad.past_lease_start_window_id 
									AND %3$s >= ad.past_advertised_date - INTERVAL \'1 week\' AND %3$s < ad.past_advertised_date 
									AND 1 = ( CASE
										WHEN extract(YEAR FROM ad.past_window_start_date ) = extract(YEAR FROM CURRENT_DATE )
											AND ca.lease_status_type_id NOT IN( ' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . ' ) 
										THEN 1
										WHEN extract(YEAR FROM ad.past_window_start_date ) <> extract(YEAR FROM CURRENT_DATE )
											AND ca.lease_status_type_id NOT IN( ' . CLeaseStatusType::CANCELLED . ' )
										THEN 1
										ELSE 0
										END
									)
									AND ca.occupancy_type_id NOT IN ( ' . implode( ',', array_merge( COccupancyType::$c_arrintExcludedOccupancyTypes, [ COccupancyType::OTHER_INCOME ] ) ) . ' )
									AND ( ca.lease_start_date <= ( ( \'09/30/\' || extract(YEAR FROM ad.past_window_start_date)::INTEGER )::DATE ) AND ( ca.lease_end_date >= ( \'09/30/\' || extract(YEAR FROM ad.past_window_start_date)::INTEGER )::DATE OR ca.lease_end_date IS NULL ) ) 
								THEN ca.executed_monthly_rent_base/ca.unit_space_count
								ELSE NULL
							END AS past_rent,
							CASE
								WHEN ca.lease_start_window_id = ad.past_lease_start_window_id 
									AND %3$s >= ad.past_advertised_date - INTERVAL \'1 week\' AND %3$s < ad.past_advertised_date
									AND 1 = ( CASE
										WHEN extract(YEAR FROM ad.past_window_start_date ) = extract(YEAR FROM CURRENT_DATE )
											AND ca.lease_status_type_id NOT IN( ' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . ' )
										THEN 1
										WHEN extract(YEAR FROM ad.past_window_start_date ) <> extract(YEAR FROM CURRENT_DATE )
											AND ca.lease_status_type_id NOT IN( ' . CLeaseStatusType::CANCELLED . ' )
										THEN 1
										ELSE 0
										END
									) 
									AND ca.occupancy_type_id NOT IN ( ' . implode( ',', array_merge( COccupancyType::$c_arrintExcludedOccupancyTypes, [ COccupancyType::OTHER_INCOME ] ) ) . ' )
									AND ( ca.lease_start_date <= ( ( \'09/30/\' || extract(YEAR FROM ad.past_window_start_date)::INTEGER )::DATE ) AND ( ca.lease_end_date >= ( \'09/30/\' || extract(YEAR FROM ad.past_window_start_date)::INTEGER )::DATE OR ca.lease_end_date IS NULL ) )
								THEN 1
								ELSE 0
							END AS past_lease_velocity,
							CASE
                            	WHEN
                                	ca.lease_start_window_id = ad.past_past_lease_window_id
                                	AND %3$s >= ad.past_past_advertised_date - INTERVAL \'1 week\' AND %3$s < ad.past_past_advertised_date
                                	AND 1 = ( CASE
										WHEN extract(YEAR FROM ad.past_past_start_date ) = extract(YEAR FROM CURRENT_DATE )
											AND ca.lease_status_type_id NOT IN( ' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . ' )
										THEN 1
										WHEN extract(YEAR FROM ad.past_past_start_date ) <> extract(YEAR FROM CURRENT_DATE )
											AND ca.lease_status_type_id NOT IN( ' . CLeaseStatusType::CANCELLED . ' )
										THEN 1
										ELSE 0
										END
									) 
									AND ca.occupancy_type_id NOT IN ( ' . implode( ',', array_merge( COccupancyType::$c_arrintExcludedOccupancyTypes, [ COccupancyType::OTHER_INCOME ] ) ) . ' )
									AND ( ca.lease_start_date <= ( ( \'09/30/\' || extract(YEAR FROM ad.past_past_start_date)::INTEGER )::DATE ) AND ( ca.lease_end_date >= ( \'09/30/\' || extract(YEAR FROM ad.past_past_start_date)::INTEGER )::DATE OR ca.lease_end_date IS NULL ) )
                                THEN ca.executed_monthly_rent_base/ca.unit_space_count
                                ELSE NULL
                               END AS past_past_rent,
                            CASE
								WHEN ca.lease_start_window_id = ad.past_past_lease_window_id
									AND %3$s >= ad.past_past_advertised_date - INTERVAL \'1 week\' AND %3$s < ad.past_past_advertised_date
									AND 1 = ( CASE
										WHEN extract(YEAR FROM ad.past_past_start_date ) = extract(YEAR FROM CURRENT_DATE )
											AND ca.lease_status_type_id NOT IN( ' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . ' )
										THEN 1
										WHEN extract(YEAR FROM ad.past_past_start_date ) <> extract(YEAR FROM CURRENT_DATE )
											AND ca.lease_status_type_id NOT IN( ' . CLeaseStatusType::CANCELLED . ' )
										THEN 1
										ELSE 0
										END
									) 
									AND ca.occupancy_type_id NOT IN ( ' . implode( ',', array_merge( COccupancyType::$c_arrintExcludedOccupancyTypes, [ COccupancyType::OTHER_INCOME ] ) ) . ' )
									AND ( ca.lease_start_date <= ( ( \'09/30/\' || extract(YEAR FROM ad.past_past_start_date)::INTEGER )::DATE ) AND ( ca.lease_end_date >= ( \'09/30/\' || extract(YEAR FROM ad.past_past_start_date)::INTEGER )::DATE OR ca.lease_end_date IS NULL ) )
								THEN 1
								ELSE 0
              				END AS past_past_lease_velocity  
						FROM 
							advertise_dates as ad
							JOIN cached_applications_temp ca ON ( true )
					) as sub
					GROUP BY 
						sub.cid,
						sub.property_id,
						sub.current_advertised_date,
						sub.past_advertised_date,
						sub.past_past_advertised_date
					ORDER BY 
						cid,
						property_id,
						current_advertised_date
					),
					pre_lease_data AS (
					SELECT 
						*,
						round( ( current_rented_space_months::NUMERIC / (tus.unit_space_count * 12)) * 100 ) As weekly_pre_leased_percentage,
						round( SUM( round( ( current_rented_space_months::NUMERIC / (tus.unit_space_count * 12)) * 100 , 4 ) ) OVER( ORDER BY current_advertised_date ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW), 2) AS cumulative_lease_velocity,
						round( ( past_rented_space_months::NUMERIC / (tus.unit_space_count * 12)) * 100 ) As past_weekly_pre_leased_percentage,
						round( SUM( round( ( past_rented_space_months::NUMERIC / (tus.unit_space_count * 12)) * 100 , 4 ) ) OVER( ORDER BY current_advertised_date ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW), 2) AS past_cumulative_lease_velocity,
						round( ( past_past_rented_space_months::NUMERIC / (tus.unit_space_count * 12)) * 100 ) As past_past_weekly_pre_leased_percentage,
						round( SUM( round( ( past_past_rented_space_months::NUMERIC / (tus.unit_space_count * 12)) * 100 , 4 ) ) OVER( ORDER BY current_advertised_date ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW), 2) AS past_past_cumulative_lease_velocity
					FROM 
						pre_lease_temp as plt
					JOIN total_unit_spaces AS tus ON plt.cid = tus.cid AND plt.property_id = tus.property_id
					)
					SELECT 
						plt.*,
						extract(Year from pp1.value::DATE)::INT AS current_pre_lease_year_start,
						extract(Year from pp2.value::DATE)::INT AS current_pre_lease_year_end
					FROM 
						pre_lease_data AS plt
						JOIN load_property_preferences( %1$d, ARRAY[ %2$d ], ARRAY[\'PRICING_STUDENT_PRELEASE_START_DAY\'] ) AS pp1 ON true
						JOIN load_property_preferences( %1$d, ARRAY[ %2$d ], ARRAY[\'PRICING_STUDENT_PRELEASE_END_DAY\'] ) AS pp2 ON true
					WHERE plt.current_advertised_date::DATE  BETWEEN pp1.value::DATE AND pp2.value::DATE
					', $intCid, $intPropertyId, $objStudentDashboardFilter->getLeaseRecognizeAt(), $objStudentDashboardFilter->getApplicationStageId(), $objStudentDashboardFilter->getApplicationStatusId(), $strSpaceConfigurationCnd, $strUnitTypeCnd, $strFloorPlanIdsCnd, $objStudentDashboardFilter->getLeaseType(), $strLeaseStartWindowIds, $strLeaseApproveCondition, $strLeaseCondition, CApplicationStage::LEASE, CApplicationStatus::APPROVED );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationTotalContractValueByLeaseIdByLeaseIntervalIdByCid( array $arrstrFieldNames, int $intLeaseId, int $intLeaseIntervalId, int $intCid, CDatabase $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intLeaseId ) || false == valId( $intLeaseIntervalId ) || false == valArr( $arrstrFieldNames ) ) return NULL;

		$strSql = 'SELECT
						 COALESCE( ' . implode( ' + ', $arrstrFieldNames ) . ', 0 ) as total_contract_value
					FROM
						cached_applications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND lease_id = ' . ( int ) $intLeaseId . '
						AND lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
					LIMIT 1';

		return fetchData( $strSql, $objDatabase )[0]['total_contract_value'] ?? 0;
	}

	public static function fetchCachedApplicationByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ca.id,
						ca.primary_applicant_id,
						ca.lease_interval_id
					FROM
						cached_applications ca
						JOIN cached_leases cl ON ( ca.lease_id = cl.id AND ca.cid = cl.cid )
					WHERE
						ca.lease_interval_id = cl.active_lease_interval_id
						AND cl.cid = ' . ( int ) $intCid . '
						AND ca.lease_id = ' . ( int ) $intLeaseId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCachedApplicationDataByPropertyIdByCidByDate( $intPropertyId, $intCid, $strDate, $objDatabase ) {
		$strSql = sprintf( 'WITH date_series AS (
						SELECT generate_series(\'%1$s\'::date, CURRENT_DATE, \'1 day\'::INTERVAL)::DATE AS effective_date
					)
					SELECT 
						ds.effective_date,
						ut.cid,
						ut.property_id,
						ut.id AS unit_type_id,
						ca.lease_term_id,
						ca.lease_start_window_id,
						ca.lease_start_window_start_date,
						ca.lease_start_window_end_date,
						tus.unit_space_count AS total_unit_space_count,
						tus.bedroom_count,
						count(ca.id) as total_application_count,
						ROUND(avg(ca.executed_monthly_rent_total)) as rent,
						ROUND(avg(ca.executed_monthly_rent_base)) as base_rent,
						ROUND(avg(ca.executed_monthly_rent_amenity)) as amenity_rent,
						ROUND(avg(ca.executed_monthly_rent_special)) as special_rent
					FROM
						date_series AS ds
						JOIN unit_types AS ut ON ut.cid = %2$d AND ut.property_id = %3$d
						JOIN (
							SELECT
								us.cid,
								us.property_id,
								us.unit_type_id,
								count(us.id) AS unit_space_count,
								max(bedroom_count) as bedroom_count
							FROM
								unit_spaces us
								LEFT JOIN view_unit_standardized_room_counts v ON v.cid = us.cid AND v.unit_id = us.property_unit_id
							WHERE
								us.cid = %2$d AND us.property_id = %3$d AND us.deleted_on IS NULL
							GROUP BY
								us.cid,
								us.property_id,
								us.unit_type_id,
								bedroom_count
						) AS tus ON tus.cid = ut.cid AND tus.property_id = ut.property_id AND tus.unit_type_id = ut.id
						LEFT JOIN cached_applications ca ON ut.cid = ca.cid AND ut.property_id = ca.property_id AND ut.id = ca.unit_type_id
						AND ca.application_stage_id = %4$d AND	ca.application_status_id = %5$d AND ca.lease_approved_on::DATE = ds.effective_date 
						AND ca.executed_monthly_rent_total > 0
					GROUP BY 
						ds.effective_date,
						ut.cid,
						ut.property_id,
						ca.lease_term_id,
						lease_start_window_id,
						tus.unit_space_count,
						ut.id,
						ca.lease_start_window_start_date,
						ca.lease_start_window_end_date,
						bedroom_count', $strDate, $intCid, $intPropertyId, CApplicationStage::LEASE, CApplicationStatus::APPROVED );
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchStudentMonthIntervalDates( $intPropertyId, $intCid, $objDatabase, $boolIsReturnSqlOnly = false ) {
		$strSql = sprintf( 'SELECT
				                     pp1.value::DATE AS start_date,
                                     pp1.value::DATE + INTERVAL \'11 Months \'  AS end_date
				                  FROM
				                      load_property_preferences ( %d , ARRAY [ %d ], ARRAY [ \'PRICING_STUDENT_PRELEASE_START_DAY\' ] ) AS pp1', $intCid, $intPropertyId );

		if( true == $boolIsReturnSqlOnly ) {
			return $strSql;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchStudentWeeklyIntervalDates( $intPropertyId, $intCid, $objDatabase, $boolIsReturnSqlOnly = false ) {
		$strSql = sprintf( 'SELECT
							   sub.start_date,
							   sub.end_date AS end_date
							FROM
							   (
							     SELECT
							         pp2.value::DATE + ( ( CASE
							                                 WHEN pp1.value = \'Sunday\' THEN 7
							                                 WHEN pp1.value = \'Monday\' THEN 8
							                                 WHEN pp1.value = \'Tuesday\' THEN 9
							                                 WHEN pp1.value = \'Wednesday\' THEN 10
							                                 WHEN pp1.value = \'Thursday\' THEN 11
							                                 WHEN pp1.value = \'Friday\' THEN 12
							                                 WHEN pp1.value = \'Saturday\' THEN 13
							                               END - extract ( DOW
							     FROM
							         pp2.value::DATE )::int ) %% 7 ) AS start_date,
							         pp3.value::DATE AS end_date
							     FROM
							         load_property_preferences ( %d, ARRAY [ %d ], ARRAY [ \'PRICING_STUDENT_LAST_DAY_OF_WEEK\' ] ) AS pp1
							         JOIN load_property_preferences ( %d, ARRAY [ %d ], ARRAY [ \'PRICING_STUDENT_PRELEASE_START_DAY\' ] ) AS pp2 ON TRUE
							         JOIN load_property_preferences( %1$d, ARRAY[ %2$d ], ARRAY[\'PRICING_STUDENT_PRELEASE_END_DAY\'] ) AS pp3 ON true
                            ) AS sub', $intCid, $intPropertyId, $intCid, $intPropertyId );

		if( true == $boolIsReturnSqlOnly ) {
			return $strSql;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCurrentAcademicYearStartEndDatesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		$strSql = sprintf( 'SELECT
										property_id,
										str_date  AS start_date,
										end_date
									FROM
									(
										SELECT
											wk_month.property_id,
											to_date( SUBSTRING (wk_month.month_name, 1, 3) || \' \' || extract(YEAR FROM wk_month.start_date::DATE)::TEXT , \'Mon YYYY\') + (
											CASE 
												WHEN wk_month.week_day = \'Sunday\' THEN 7    
												WHEN wk_month.week_day = \'Monday\' THEN 8
												WHEN wk_month.week_day = \'Tuesday\' THEN 9
												WHEN wk_month.week_day = \'Wednesday\' THEN 10
												WHEN wk_month.week_day = \'Thursday\' THEN 11
												WHEN wk_month.week_day = \'Friday\' THEN 12
												WHEN wk_month.week_day = \'Saturday\' THEN 13
												END - extract(dow from to_date(SUBSTRING (wk_month.month_name, 1, 3) || \' \' || extract(YEAR FROM wk_month.start_date::DATE)::TEXT, \'Mon YYYY\')))::integer %% 7 AS str_date,
											wk_month.start_date as month_date,
											end_date
										FROM 
										(
											SELECT
												pp1.property_id,
												pp1.value AS week_day,
												to_char(to_date(pp2.value, \'YYYY-MM-DD\'), \'Month\') month_name,
												DATE ( to_date( SUBSTRING (to_char(to_date(pp2.value, \'YYYY-MM-DD\'), \'Month\'), 1, 3) || \' \' || ( extract( YEAR FROM pp2.value::DATE ) + 1 )::TEXT , \'Mon YYYY\') ) AS start_date,
												pp3.value::date + INTERVAL \'1 year\' AS end_date
											FROM
												load_property_preferences ( %1$d, ARRAY[ %2$s ], ARRAY [ \'PRICING_STUDENT_LAST_DAY_OF_WEEK\' ] ) AS pp1
												JOIN load_property_preferences ( %1$d, ARRAY[ %2$s ], ARRAY [ \'PRICING_STUDENT_PRELEASE_START_DAY\' ] ) AS pp2 ON pp1.property_id = pp2.property_id
												JOIN load_property_preferences ( %1$d, ARRAY[ %2$s ], ARRAY [ \'PRICING_STUDENT_PRELEASE_END_DAY\' ] ) AS pp3 ON pp2.property_id = pp3.property_id
										) AS wk_month
									) AS setting_dates', $intCid, implode( ',', $arrintPropertyIds ) );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchExecutedOccupancyDataByPropertyIdsByCids( $objRentInsights, $intCid, $objDatabase, $objStudentDashboardFilter, $strInterval = 'MONTH' ) {

		if( false == $intCid || false == $objRentInsights->property_id ) {
			return [];
		}

		if( 'MONTH' == $strInterval ) {
			$strIntervalSql = self::fetchStudentMonthIntervalDates( $objRentInsights->property_id, $intCid, $objDatabase, true );
			$strLeaseApprovedCondition = 'EXTRACT ( MONTH FROM ' . $objStudentDashboardFilter->getLeaseRecognizeAt() . ' ) = interval_num';
		} else {
			$strIntervalSql = self::fetchStudentWeeklyIntervalDates( $objRentInsights->property_id, $intCid, $objDatabase, true );
			$strLeaseApprovedCondition = $objStudentDashboardFilter->getLeaseRecognizeAt() . ' BETWEEN interval_key - INTERVAL \'1 WEEK\' AND interval_key';
		}

		$strBedroomCondition = 'AND 0 <= pf.number_of_bedrooms';
		if( false != valArr( $objRentInsights->selected_bedrooms ) ) {
			if( false == array_search( 4, $objRentInsights->selected_bedrooms ) ) {
				$strBedroomCondition = 'AND pf.number_of_bedrooms IN ( ' . sqlIntImplode( $objRentInsights->selected_bedrooms ) . ' )';
			} else {
				$strBedroomCondition = 'AND ( pf.number_of_bedrooms IN ( ' . sqlIntImplode( $objRentInsights->selected_bedrooms ) . ' ) OR 4 < pf.number_of_bedrooms )';
			}
		}

		$strLeaseApproveCondition = ' AND ca.application_stage_id = ' . $objStudentDashboardFilter->getApplicationStageId() . ' AND ca.application_status_id = ' . $objStudentDashboardFilter->getApplicationStatusId();
		$strLeaseCondition        = '';
		if( 'ca.lease_approved_on' != trim( $objStudentDashboardFilter->getLeaseRecognizeAt() ) ) {
			$strLeaseApproveCondition = '';
		}

		$strSql = sprintf( 'WITH student_academic AS (
										              SELECT
										                  interval_key::date,
										                  EXTRACT ( %1$s FROM interval_key ) AS interval_num,
										                  EXTRACT ( YEAR FROM interval_key ) AS interval_year
										              FROM
										                  (
										                    %2$s
										                  ) academic_dates,
									                  generate_series ( academic_dates.start_date, academic_dates.end_date, \'1 %1$s\'::INTERVAL ) AS interval_key
			                                )
					                    SELECT
										    sa.interval_key::DATE AS post_month,
										    CASE
										      WHEN 0 < avg ( ca.rent ) THEN round ( avg ( ca.rent ) )
										      ELSE NULL
										    END AS rent,
										    tus.unit_space_count AS unit_space_count,
										   sum( ca.total_applications ) as total_applications   
										FROM
										    student_academic sa
										    LEFT JOIN 
										    (
										      SELECT
										          us.cid,
										          us.property_id,
										          count ( us.id ) AS unit_space_count
										      FROM
										          unit_spaces us
										          LEFT JOIN property_floorplans pf ON pf.cid = us.cid AND pf.property_id = us.property_id AND  pf.id = us.property_floorplan_id
										      WHERE
										          us.cid = %3$d
										          AND us.property_id = %4$d
										          AND us.deleted_on IS NULL
										          %5$s
										      GROUP BY
										          us.cid,
										          us.property_id
										    ) as tus ON TRUE
										    LEFT JOIN 
										    (
										      SELECT
										          avg ( ca.executed_monthly_rent_base/sc.unit_space_count ) AS rent,
										          count ( ca.id ) AS total_applications,
										          %8$s
										      FROM
										          cached_applications AS ca
										          LEFT JOIN property_floorplans pf ON pf.cid = ca.cid AND pf.property_id = ca.property_id AND pf.id = ca.property_floorplan_id
										          JOIN space_configurations sc on ca.space_configuration_id = sc.id AND sc.cid = ca.cid  and sc.unit_space_count > 0
										          LEFT JOIN unit_types ut ON  ut.cid = ca.cid AND ut.property_id = ca.property_id AND ( COALESCE( ut.id, NULL ) = ca.unit_type_id )
										          LEFT JOIN (
														SELECT ( \'10/01/\' || CASE
																					WHEN EXTRACT(MONTH FROM CURRENT_DATE) >=8
																					THEN EXTRACT( YEAR FROM CURRENT_DATE + INTERVAL \'1 Year\')
																					ELSE EXTRACT( YEAR FROM CURRENT_DATE )
																				END )::DATE AS prelease_date
												  ) as lease_date ON TRUE
										      WHERE
										          ca.cid = %3$d
										          AND ca.property_id = %4$d
										          AND ut.deleted_on IS NULL
										          AND ca.lease_term_id IS NOT NULL
										          %5$s
										          %9$s
												  AND ca.lease_status_type_id NOT IN ( ' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . ' ) 
												  AND ca.lease_interval_type_id NOT IN ( ' . CLeaseIntervalType::MONTH_TO_MONTH . ', ' . CLeaseIntervalType::LEASE_MODIFICATION . ' )
												  AND ca.occupancy_type_id NOT IN ( ' . implode( ',', array_merge( COccupancyType::$c_arrintExcludedOccupancyTypes, [ COccupancyType::OTHER_INCOME ] ) ) . ' )
												  AND ca.lease_start_window_id IN ( %7$s )
												  AND ( ca.lease_start_date <= lease_date.prelease_date AND ( ca.lease_end_date >= lease_date.prelease_date OR ca.lease_end_date IS NULL ) )
										      GROUP BY
										        %8$s
										    ) as ca ON %6$s
										GROUP BY 
										    sa.interval_key,
											tus.unit_space_count
										ORDER BY
										    sa.interval_key', $strInterval, $strIntervalSql, $intCid, $objRentInsights->property_id, $strBedroomCondition, $strLeaseApprovedCondition, sqlIntImplode( $objRentInsights->current_lease_start_window_ids ), $objStudentDashboardFilter->getLeaseRecognizeAt(), $strLeaseApproveCondition );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLeadsCurrentStatusByPropertyId( array $arrintPropertyGroupIds, int $intCid, CDatabase $objDatabase ) : array {

		if( !valArr( $arrintPropertyGroupIds ) ) {
			return [];
		}

		$strSql = '
			SELECT
				COUNT( lease_customer_details.customer_id ) FILTER ( 
					WHERE 
						( lease_customer_details.application_stage_id, lease_customer_details.application_status_id ) = ( ' . CApplicationStage::PRE_APPLICATION . ', ' . CApplicationStatus::COMPLETED . ' )
				) AS guest_card_completed,
				COUNT( lease_customer_details.customer_id ) FILTER (
					WHERE
						( lease_customer_details.application_stage_id, lease_customer_details.application_status_id ) = ( ' . CApplicationStage::APPLICATION . ', ' . CApplicationStatus::STARTED . ' )
				) AS application_started,
				COUNT( lease_customer_details.customer_id ) FILTER ( 
					WHERE
						( lease_customer_details.application_stage_id, lease_customer_details.application_status_id ) = ( ' . CApplicationStage::APPLICATION . ', ' . CApplicationStatus::PARTIALLY_COMPLETED . ' )
				) AS application_partially_completed,
				COUNT( lease_customer_details.customer_id ) FILTER ( 
					WHERE
						( lease_customer_details.application_stage_id, lease_customer_details.application_status_id ) = ( ' . CApplicationStage::APPLICATION . ', ' . CApplicationStatus::COMPLETED . ' )  
				) AS application_completed,
				COUNT( lease_customer_details.customer_id ) FILTER (
					WHERE 
						( lease_customer_details.application_stage_id, lease_customer_details.application_status_id ) = ( ' . CApplicationStage::APPLICATION . ', ' . CApplicationStatus::APPROVED . ' )
				) AS application_approved,
				COUNT( lease_customer_details.customer_id ) FILTER (
					WHERE
						( lease_customer_details.application_stage_id, lease_customer_details.application_status_id ) = ( ' . CApplicationStage::LEASE . ', ' . CApplicationStatus::STARTED . ' )
				) AS lease_started,
				COUNT( lease_customer_details.customer_id ) FILTER (
					WHERE
						( lease_customer_details.application_stage_id, lease_customer_details.application_status_id ) = ( ' . CApplicationStage::LEASE . ', ' . CApplicationStatus::PARTIALLY_COMPLETED . ' )
				) AS lease_partially_completed,
				COUNT( lease_customer_details.customer_id ) FILTER (
					WHERE
						( lease_customer_details.application_stage_id, lease_customer_details.application_status_id ) = ( ' . CApplicationStage::LEASE . ', ' . CApplicationStatus::COMPLETED . ' )
				) AS lease_completed
			FROM
				(
				SELECT
					c.id AS customer_id,
					ca.cid,
					ca.property_id,
					ca.application_stage_id,
					ca.application_status_id,
					ROW_NUMBER() OVER( PARTITION BY ca.lease_id, c.id, COALESCE( aa.customer_type_id, ' . CCustomerType::PRIMARY . ' ) )
				FROM
					cached_applications ca
					JOIN load_properties( ARRAY[' . ( int ) $intCid . '] , ARRAY[' . sqlIntImplode( $arrintPropertyGroupIds ) . '] ) lp ON lp.property_id = ca.property_id AND lp.is_disabled = 0 AND lp.is_test = 0
					JOIN applicant_applications aa ON aa.cid = ca.cid AND aa.application_id = ca.id AND aa.deleted_on IS NULL AND aa.customer_type_id = ' . CCustomerType::PRIMARY . '
					JOIN applicants ap ON ap.cid = aa.cid AND ap.id = aa.applicant_id 
					JOIN customers c ON c.cid = ap.cid AND c.id = ap.customer_id
				WHERE
					ca.cid = ' . ( int ) $intCid . '
					AND ca.occupancy_type_id <> ' . COccupancyType::OTHER_INCOME . '
					AND ca.lease_interval_type_id IN ( ' . CLeaseIntervalType::APPLICATION . ' )
					AND ( ( ca.application_stage_id, ca.application_status_id ) IN ( 
							( ' . CApplicationStage::PRE_APPLICATION . ', ' . CApplicationStatus::COMPLETED . ' ),
							( ' . CApplicationStage::APPLICATION . ', ' . CApplicationStatus::STARTED . ' ),
							( ' . CApplicationStage::APPLICATION . ', ' . CApplicationStatus::PARTIALLY_COMPLETED . ' ),
							( ' . CApplicationStage::APPLICATION . ', ' . CApplicationStatus::COMPLETED . ' ),
							( ' . CApplicationStage::APPLICATION . ', ' . CApplicationStatus::APPROVED . ' ),
							( ' . CApplicationStage::LEASE . ', ' . CApplicationStatus::STARTED . ' ),
							( ' . CApplicationStage::LEASE . ', ' . CApplicationStatus::PARTIALLY_COMPLETED . ' ),
							( ' . CApplicationStage::LEASE . ', ' . CApplicationStatus::COMPLETED . ' ),
							( ' . CApplicationStage::PRE_QUALIFICATION . ', ' . CApplicationStatus::STARTED . ' ),
							( ' . CApplicationStage::PRE_QUALIFICATION . ', ' . CApplicationStatus::COMPLETED . ' ),
							( ' . CApplicationStage::PRE_QUALIFICATION . ', ' . CApplicationStatus::APPROVED . ' )
						)
					)
				) lease_customer_details
				WHERE
				lease_customer_details.row_number = 1
				';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchWeeklyApprovedCachedApplicationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $objStudentFilter ) {

		$strFloorPlanIdsCnd = ' ';
		if( true == valArr( $objStudentFilter->getFloorPlanIds() ) ) {
			$strFloorPlanIdsCnd = ' AND pfp.id IN (' . implode( ',', $objStudentFilter->getFloorPlanIds() ) . ')';
		}
		if( true == valArr( $objStudentFilter->getBedroomCounts() ) ) {
			$strFloorPlanIdsCnd = $strFloorPlanIdsCnd . ' AND pfp.number_of_bedrooms IN (' . implode( ',', $objStudentFilter->getBedroomCounts() ) . ')';
		}

		$strUnitTypeCnd = '';
		if( true == valArr( $objStudentFilter->getUnitTypeIds() ) ) {
			$strUnitTypeCnd = ' AND ca.unit_type_id IN (' . implode( ',', $objStudentFilter->getUnitTypeIds() ) . ')';
		}

		$strLeaseCondition = ' AND ca.application_stage_id = ' . $objStudentFilter->getApplicationStageId() . ' AND ca.application_status_id = ' . $objStudentFilter->getApplicationStatusId();

		if( 'ca.lease_approved_on' != trim( $objStudentFilter->getLeaseRecognizeAt() ) ) {
			$strLeaseCondition    = '';
		}

        $strSpaceOptionsCnd = ' ';
        if( true == valArr( $objStudentFilter->getSpaceConfigurationIds() ) ) {
            $strSpaceOptionsCnd = ' AND sc.id IN (' . implode( ',', $objStudentFilter->getSpaceConfigurationIds() ) . ')';
        }

		$strRecognizeLeaseAt = trim( $objStudentFilter->getLeaseRecognizeAt() );
		$strRecognizeLeaseAt = substr( $strRecognizeLeaseAt, 3 );
		$strSql = sprintf( 'with date_series AS (
				SELECT ar.current_advertised_date as advertised_date
				FROM (
					SELECT generate_series(\'%1$s\'::date, (\'09/30/\' || EXTRACT( YEAR from (\'%1$s\'::date + INTERVAL \'1 year\' ) ) )::date, \'1 week\'::INTERVAL)::DATE AS current_advertised_date
				) AS ar
			)
			SELECT json_agg(wcapp.applications) AS weekly_records,
				wcapp.property_id,
				wcapp.lease_term_id,
				wcapp.lease_start_window_id,
				wcapp.advertised_date
			FROM (
				 SELECT 
						json_build_object(
						\'application_id\', ca.id,
						  \'unit_type\', ut.name,
						  \'unit\', COALESCE (us.unit_number, \'&mdash;\'),
						  \'floorplan\', pfp.floorplan_name,
						  \'space_option\', COALESCE (sc.name, \'&mdash;\'),
						  \'unit_space_count\', sc.unit_space_count,
						  \'resident_name\', ca.name_first || \' \' || ca.name_last,
                          \'lease_signed_date\', TO_CHAR(COALESCE( aa.lease_signed_on::DATE, ca.lease_completed_on::DATE ),\'mm/dd/yyyy\'),
						  \'%13$s\', TO_CHAR(%12$s::date,\'mm/dd/yyyy\'),
						  \'market_rent\', round(ca.executed_monthly_rent_base),
						  \'amenity_rent\', round(ca.executed_monthly_rent_amenity),
						  \'special_rent\', round(ca.executed_monthly_rent_special),
						  \'effective_rent\', ( round(ca.executed_monthly_rent_base) + round(ca.executed_monthly_rent_amenity) + round(ca.executed_monthly_rent_special) ),
						  \'lease_term\', lt.name || \'(\' || lsw.start_date || \' - \' || lsw.end_date || \')\'
						) AS applications,
						%13$s::date,
						ca.cid,
						ca.property_id,
						ca.lease_term_id,
						ca.lease_start_window_id,
						TO_CHAR(ds.advertised_date::DATE, \'mm/dd\') as advertised_date
				FROM
					date_series ds 
					JOIN cached_applications ca ON true
					JOIN unit_types ut ON ut.cid = ca.cid AND ut.property_id = ca.property_id AND ut.id = ca.unit_type_id
					JOIN property_floorplans pfp ON pfp.cid = ca.cid AND pfp.property_id = ca.property_id AND ut.property_floorplan_id = pfp.id %2$s
					JOIN lease_terms lt ON lt.id = ca.lease_term_id AND lt.cid = ca.cid
					JOIN lease_start_windows lsw ON lsw.cid = lt.cid AND lsw.property_id = ca.property_id AND lsw.lease_term_id = lt.id AND lsw.id = ca.lease_start_window_id
					JOIN space_configurations sc ON sc.cid = ca.cid AND sc.id = space_configuration_id %14$s
					JOIN applicant_applications aa ON aa.cid = ca.cid AND aa.application_id = ca.id AND aa.customer_type_id = 1
					LEFT JOIN unit_spaces us ON us.cid = ca.cid AND us.property_id = ca.property_id AND us.unit_type_id = ca.unit_type_id AND us.id = ca.unit_space_id
			    WHERE
					%3$s >= ds.advertised_date - INTERVAL \'1 week\' AND %3$s < ds.advertised_date
					AND ca.cid = %4$d 
					AND ca.property_id = %5$d
					AND ca.lease_start_window_id IN ( %6$s )
					%7$s
					%11$s
					AND ca.lease_interval_type_id IN ( %10$s )
					AND 1 = ( CASE
								WHEN extract(YEAR FROM lsw.start_date ) = extract(YEAR FROM CURRENT_DATE )
									AND ca.lease_status_type_id NOT IN( ' . \CLeaseStatusType::CANCELLED . ', ' . \CLeaseStatusType::PAST . ' )
								THEN 1
								WHEN extract(YEAR FROM lsw.start_date ) <> extract(YEAR FROM CURRENT_DATE )
									AND ca.lease_status_type_id NOT IN( ' . \CLeaseStatusType::CANCELLED . ' )
								THEN 1
								ELSE 0
								END
							)
					AND ca.occupancy_type_id NOT IN ( ' . sqlIntImplode( array_merge( \COccupancyType::$c_arrintExcludedOccupancyTypes, [ \COccupancyType::OTHER_INCOME ] ) ) . ')
					AND ( ca.lease_start_date <= ( ( \'09/30/\' || extract(YEAR FROM lsw.start_date)::INTEGER )::DATE ) AND ( ca.lease_end_date >= ( \'09/30/\' || extract(YEAR FROM lsw.start_date)::INTEGER )::DATE OR ca.lease_end_date IS NULL ) )
				) AS wcapp
			GROUP BY 
				wcapp.cid,
				wcapp.property_id,
				wcapp.lease_term_id,
				wcapp.lease_start_window_id,
				wcapp.advertised_date
				ORDER BY wcapp.advertised_date ASC', $objStudentFilter->getStartDate(), $strFloorPlanIdsCnd, $objStudentFilter->getLeaseRecognizeAt(), $intCid, $intPropertyId, $objStudentFilter->getLeaseStartWinodwId(), $strUnitTypeCnd, $objStudentFilter->getApplicationStageId(), $objStudentFilter->getApplicationStatusId(), $objStudentFilter->getLeaseType(), $strLeaseCondition, $objStudentFilter->getLeaseRecognizeAt(), $strRecognizeLeaseAt, $strSpaceOptionsCnd );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMarketBaselineDataByCidByDate( $intCid, $strStartDate, $strEndDate, $objDatabase ) {

		$strSql = sprintf( 'SELECT d.start_date AS effective_date,
								       sub.cid AS entrata_cid,
								       sub.company_name,
								       sub.property_id AS entrata_property_id,
								       sub.property_name,
								       sub.property_type AS occupancy_types,
								       SUM(sub.property_unit_count) OVER(PARTITION BY sub.cid) AS company_unit_count,
								       sub.property_unit_count,
								       sub.entrata_pricing_unit_count,
								       sub.lro_integrated_unit_count,
								       sub.yieldstar_integrated_unit_count,
								       app.new_leases,
								       app.renewal_leases AS new_renewal_leases,
								       ROUND(rents.rent_base + rents.rent_amenities + rents.rent_specials) AS effective_income,
								       ROUND(rents.rent_base) AS base_rent,
								       ROUND(rents.rent_amenities) AS amenity_rent,
								       ROUND(rents.rent_specials) AS special_rent,
								       sub.property_latitude,
								       sub.property_longitude
								FROM ( SELECT GENERATE_SERIES(DATE_TRUNC(\'month\', \'%1$s\'::DATE), DATE_TRUNC(\'month\', \'%2$s\'::DATE) - INTERVAL \'1 month\',\'1 month\')::DATE AS start_date ) AS d
								     CROSS JOIN 
								     (
								       SELECT c.id AS cid,
								              c.company_name,
								              p.id AS property_id,
								              p.property_name,
								              pt.name AS property_type,
								              COALESCE(MAX(p.number_of_units), 0) AS property_unit_count,
								              COALESCE(SUM(CASE
								                             WHEN pp.id IS NOT NULL THEN p.number_of_units
								                           END), 0) AS entrata_pricing_unit_count,
								              COALESCE(SUM(CASE
								                             WHEN ctv.transmission_vendor_id = ' . CTransmissionVendor::LRO . ' THEN
								                               p.number_of_units
								                           END), 0) AS lro_integrated_unit_count,
								              COALESCE(SUM(CASE
								                             WHEN ctv.transmission_vendor_id IN ( ' . CTransmissionVendor::YIELDSTAR . ', ' . CTransmissionVendor::YIELDSTAR_EXPORT . ' ) THEN
								                               p.number_of_units
								                           END), 0) AS yieldstar_integrated_unit_count,
								              pa.latitude AS property_latitude,
								              pa.longitude AS property_longitude
								       FROM clients AS c
								            JOIN properties AS p ON p.cid = c.id AND p.is_disabled = 0 AND
								              p.is_test = 0 AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' AND p.cid = %3$d
								            JOIN property_types AS pt ON pt.id = p.property_type_id AND pt.occupancy_type_id IN ( ' . COccupancyType::CONVENTIONAL . ', ' . COccupancyType::STUDENT . ' )
								            LEFT JOIN LATERAL
								            (
								              SELECT pa.latitude,
								                     pa.longitude
								              FROM property_addresses AS pa
								              WHERE pa.cid = p.cid AND
								                    pa.property_id = p.id AND
								                    pa.address_type_id = ' . CAddressType::PRIMARY . ' AND
								                    pa.latitude IS NOT NULL AND
								                    pa.longitude IS NOT NULL AND
								                    pa.is_alternate = false
								              ORDER BY pa.id ASC
								              LIMIT 1
								            ) as pa ON true
								            LEFT JOIN property_products AS pp ON pp.ps_product_id IN ( ' . CPsProduct::PRICING . ',' . CPsProduct::PRICING_STUDENT . ' ) AND
								              pp.property_id = p.id
								            LEFT JOIN property_transmission_vendors AS ptv ON ptv.cid = c.id AND
								              ptv.property_id = p.id
								            LEFT JOIN company_transmission_vendors AS ctv ON ctv.id =
								              ptv.company_transmission_vendor_id
								       GROUP BY c.id,
								                c.company_name,
								                p.id,
								                p.property_name,
								                pt.id,
								                pt.name,
								                pa.latitude,
								                pa.longitude
								     ) AS sub
								     JOIN periods p ON sub.cid = p.cid AND sub.property_id = p.property_id AND
								       d.start_date BETWEEN p.ar_start_date AND p.ar_end_date AND p.post_month = d.start_date
								     LEFT JOIN lateral
								     (
								       SELECT SUM(CASE
								                    WHEN ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . ' THEN 1
								                    ELSE 0
								                  END) new_leases,
								              SUM(CASE
								                    WHEN ca.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . ' THEN 1
								                    ELSE 0
								                  END) renewal_leases
								       FROM cached_applications ca
								       WHERE ca.cid = %3$d AND
								             ca.property_id = sub.property_id AND
								             ca.application_stage_id = ' . CApplicationStage::LEASE . ' AND
								             ca.application_status_id = ' . CApplicationStatus::APPROVED . ' AND
								             ca.lease_approved_on BETWEEN p.ar_start_date AND p.ar_end_date
								     ) AS app ON true
								     LEFT JOIN LATERAL
								     (
								       SELECT SUM(cusp.rent_base) AS rent_base,
								              SUM(cusp.rent_amenities) AS rent_amenities,
								              SUM(cusp.rent_specials) AS rent_specials
								       FROM cached_unit_space_periods cusp
								       WHERE cusp.cid = %3$d AND
								             cusp.property_id = sub.property_id AND
								             cusp.period_id = p.id
								     ) rents ON true', $strStartDate, $strEndDate, $intCid );
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedApplicationsByApplicationFiltersByCid( $arrmixApplicationFilter, $objPagination, $intCid, $objDatabase, $boolFetchCount = false ) {
		if( false == valArr( $arrmixApplicationFilter ) ) {
			return NULL;
		}
		if( false == $boolFetchCount ) {
			$strSelectSql = ' DISTINCT ON (ca.id) ap.id AS applicant_id,
								ca.property_id AS property_id,
								ca.lease_interval_type_id AS lease_interval_type_id,
								ca.lease_interval_id,
								ca.ps_product_id AS ps_product_id,
								ca.id AS application_id,
								ca.application_datetime,
								ca.property_unit_id AS property_unit_id,
								ca.desired_rent_min,
								ca.desired_rent_max,
								ca.desired_bedrooms,
								ca.desired_bathrooms,
								ca.term_month,
								ca.desired_occupants,
								ca.leasing_agent_id,
								ca.unit_type_id,
								ca.originating_lead_source_id,
								ca.property_floorplan_id,
								ca.property_unit_id,
								ca.cancellation_list_item_id,
								ca.internet_listing_service_id,
								ca.updated_on,
								ca.created_on,
								ca.lease_term,
								ap.customer_id,
								ap.name_prefix,
								ap.name_first,
								ap.name_middle,
								ap.name_last,
								ap.name_maiden,
								ap.name_suffix,
								ap.fax_number,
								ap.username,
								ap.tax_number_encrypted,
								ap.tax_number_masked,
								ap.birth_date,
								ap.notes,
								COALESCE (ap.app_remote_primary_key, ap.guest_remote_primary_key) AS applicant_remote_primary_key,
								aa.id AS applicant_application_id,
								CASE
									WHEN cl.lease_start_date IS NOT NULL THEN cl.lease_start_date
									ELSE ca.lease_start_date
								END AS lease_start_date,
								cl.lease_status_type,
								cl.id AS lease_id,
								cl.move_in_date,
								cl.move_out_date,
								CASE 
									WHEN cl.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . ' THEN cl.lease_end_date 
									ELSE NULL 
								END AS lease_end_date,
								cl.unit_number_cache AS marketing_name,
								cl.building_name,
								cl.unit_space_id,
								cl.unit_number_cache,
								concat_ws( \' \', ce.name_first, ce.name_last ) AS leasing_agent_name,
								concat_ws( \' \', ass.stage_name, ass.status_name ) AS application_stage_status_name,
								ls.name AS lead_source,
								pf.floorplan_name,
								li1.name AS lead_cancellation_reason,
								ils.name AS internet_listing_service_name,
								ca.desired_space_configuration_id';
		} else {
			$strSelectSql = ' COUNT(1) ';
		}
		$strSql = 'SELECT
						' . $strSelectSql . '
					FROM
						cached_applications ca
						JOIN applicant_applications aa ON ( aa.application_id = ca.id AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' AND aa.cid = ca.cid AND aa.deleted_on IS NULL AND ca.property_id = ' . ( int ) $arrmixApplicationFilter['property_id'] . ' )
						JOIN applicants ap ON ( ap.id = aa.applicant_id AND ap.cid = aa.cid )
						JOIN application_stage_statuses ass ON ( ca.lease_interval_type_id = ass.lease_interval_type_id AND ca.application_stage_id = ass.application_stage_id AND ca.application_status_id = ass.application_status_id)
						LEFT JOIN internet_listing_services ils ON ( ils.id = ca.internet_listing_service_id )
						LEFT JOIN company_employees ce ON ( ce.cid = ca.cid AND ce.id = ca.leasing_agent_id )
						LEFT JOIN lead_sources ls ON ( ls.id = ca.originating_lead_source_id AND ls.cid = ca.cid )
						LEFT JOIN cached_leases cl ON ( cl.id = ca.lease_id AND cl.cid = ca.cid )
						LEFT JOIN property_floorplans pf ON ( pf.id = ca.property_floorplan_id AND pf.cid = ca.cid AND pf.deleted_on IS NULL )
						LEFT JOIN list_items li1 ON ( li1.id = ca.cancellation_list_item_id and ca.cid = li1.cid )
						LEFT JOIN customer_phone_numbers cpn ON ( cpn.cid = ap.cid AND cpn.customer_id = ap.customer_id AND cpn.deleted_by IS NULL )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.property_id = ' . ( int ) $arrmixApplicationFilter['property_id'];
		if( false != valArr( $arrmixApplicationFilter['ps_productIds'] ) ) {
			$strSql .= ' AND ca.ps_product_id IN ( ' . implode( ',', $arrmixApplicationFilter['ps_productIds'] ) . ') ';
		}
		if( false != valArr( $arrmixApplicationFilter['lease_status_ids'] ) ) {
			$strSql .= ' AND ass.id IN (' . implode( ',', $arrmixApplicationFilter['lease_status_ids'] ) . ') ';
		}
		if( false != valArr( $arrmixApplicationFilter['lead_ids'] ) ) {
			$strSql .= ' AND ca.id IN ( ' . implode( ',', $arrmixApplicationFilter['lead_ids'] ) . ') ';
		}
		if( false == is_null( $arrmixApplicationFilter['from_date'] ) && false == is_null( $arrmixApplicationFilter['to_date'] ) ) {
			$strSql .= ' AND ca.application_datetime BETWEEN \'' . $arrmixApplicationFilter['from_date'] . ' 00:00:00\' AND \'' . $arrmixApplicationFilter['to_date'] . ' 23:59:59\'';
		}
		if( false != isset( $arrmixApplicationFilter['create_on_date_from'] ) && false == is_null( $arrmixApplicationFilter['create_on_date_from'] ) ) {
			$strSql .= ' AND ca.created_on >= \'' . $arrmixApplicationFilter['create_on_date_from'] . ' 00:00:00' . '\'';
		}

		if( false != isset( $arrmixApplicationFilter['create_on_date_to'] ) && false == is_null( $arrmixApplicationFilter['create_on_date_to'] ) ) {
			$strSql .= ' AND ca.created_on <= \'' . $arrmixApplicationFilter['create_on_date_to'] . ' 23:59:59' . '\'';
		}

		$strSqlSearchFilters = '';
		if( false != valStr( $arrmixApplicationFilter['name_first'] ) ) {
			$strSqlSearchFilters .= ' lower( ap.name_first ) = ' . \Psi\CStringService::singleton()->strtolower( pg_escape_literal( $objDatabase->getHandle(), $arrmixApplicationFilter['name_first'] ) );
		}
		if( false != valStr( $arrmixApplicationFilter['name_last'] ) ) {
			$strSqlSearchFilters .= ' AND lower( ap.name_last ) = ' . \Psi\CStringService::singleton()->strtolower( pg_escape_literal( $objDatabase->getHandle(), $arrmixApplicationFilter['name_last'] ) );
		}
		if( false != valStr( $arrmixApplicationFilter['telephone'] ) ) {
			if( false != valStr( $strSqlSearchFilters ) ) {
				$strSqlSearchFilters .= ' OR ';
			}
			$strSqlSearchFilters .= ' ( regexp_replace( cpn.phone_number, \'[^0-9]\', \'\', \'g\') like \'%' . $arrmixApplicationFilter['telephone'] . '%\' OR
			 regexp_replace( ap.fax_number, \'[^0-9]\', \'\', \'g\') like \'%' . $arrmixApplicationFilter['telephone'] . '%\' )';
		}
		if( false != valStr( $arrmixApplicationFilter['email'] ) ) {
			if( false != valStr( $strSqlSearchFilters ) ) {
				$strSqlSearchFilters .= ' OR ';
			}
			$strSqlSearchFilters .= ' lower ( ap.username ) = \'' . \Psi\CStringService::singleton()->strtolower( $arrmixApplicationFilter['email'] ) . '\'';
		}
		if( false != valStr( $strSqlSearchFilters ) ) {
			$strSqlSearchFilters = ' AND ( ' . $strSqlSearchFilters . ' ) ';
		}
		$strSql .= $strSqlSearchFilters . ' AND aa.deleted_on IS NULL';
		if( false != valArr( $arrmixApplicationFilter['event_type_ids'] ) || ( false != valStr( $arrmixApplicationFilter['event_date_from'] ) && false != valStr( $arrmixApplicationFilter['event_date_to'] ) ) ) {
			$strSql       .= ' AND EXISTS ( ';
			$strCommonSql = ' SELECT
							1
						FROM
							events
						WHERE
							cid = ca.cid
							AND is_deleted = FALSE';
			if( false != valArr( $arrmixApplicationFilter['event_type_ids'] ) ) {
				$strCommonSql .= ' AND event_type_id IN ( ' . implode( ',', $arrmixApplicationFilter['event_type_ids'] ) . ' )';
			}
			$strCommonSql .= ' AND property_id = ca.property_id';
			if( false != valStr( $arrmixApplicationFilter['event_date_from'] ) && false != valStr( $arrmixApplicationFilter['event_date_to'] ) ) {
				$strCommonSql .= ' AND event_datetime BETWEEN \'' . $arrmixApplicationFilter['event_date_from'] . ' 00:00:00\' AND \'' . $arrmixApplicationFilter['event_date_to'] . ' 23:59:59\'';
			}
			$strInnerSql  = $strCommonSql . ' AND lease_interval_id = ca.lease_interval_id ';
			$strInnerSql1 = $strCommonSql . ' AND customer_id = ap.customer_id ';
			$strInnerSql2 = $strCommonSql . ' AND ( lease_id = ca.lease_id AND event_type_id IN ( ' . implode( ',', \CEventType::$c_arrintDepositEventTypeIds ) . ' ) )';
			$strSql       .= $strInnerSql . ' UNION ALL' . $strInnerSql1 . ' UNION ALL' . $strInnerSql2 . ')';
		}
		if( false == $boolFetchCount && false != valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . $objPagination->getPageSize();
		}
		$arrmixLeadDetails = fetchData( $strSql, $objDatabase );
		if( false != $boolFetchCount && false != isset( $arrmixLeadDetails[0]['count'] ) ) {
			return ( int ) $arrmixLeadDetails[0]['count'];
		}
		return $arrmixLeadDetails;
	}

}
