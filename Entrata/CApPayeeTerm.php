<?php

class CApPayeeTerm extends CBaseApPayeeTerm {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objClientDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
		}

		$strWhereSql = 'WHERE cid = ' . ( int ) $this->getCid() . ' AND lower(name) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->getName() ) ) . '\' AND deleted_by IS NULL AND deleted_on IS NULL';

		if( 0 < $this->getId() ) {
			$strWhereSql .= ' AND id <> ' . ( int ) $this->getId();
		}

		if( 0 < CApPayeeTerms::fetchApPayeeTermCount( $strWhereSql, $objClientDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name already exists.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSystemCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPercentage() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsFromInvoiceDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDaysForIsFromInvoiceDate() {
		$boolIsValid = true;

		if( 1 != $this->getIsFromInvoiceDate() && ( 0 >= $this->getDays() || 31 < $this->getDays() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days', __( 'Due in full should be in between {%d, 0} to {%d, 1} if day of the month is selected.', [ 1, 31 ] ) ) );
		}

		if( false == is_null( $this->getDays() ) && 0 > $this->getDays() && 1 == $this->getIsFromInvoiceDate() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days', __( 'Invalid Days' ) ) );
		}

		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;

		if( false == is_null( $this->getIsSystem() ) && 1 == $this->getIsSystem() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_system', __( 'System payment term cannot be deleted.' ) ) );
		}

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUpdatedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUpdatedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCreatedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCreatedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDelete( $objClientDatabase ) {
		$boolIsValid = true;

		$strWhere = 'WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND ap_payee_term_id = ' . ( int ) $this->getId() . '
						AND reversal_ap_header_id IS NULL';

		$intApHeaderCount = CApHeaders::fetchApHeaderCount( $strWhere, $objClientDatabase );

		if( 0 < $intApHeaderCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delete', __( '&nbsp;Cannot be deleted, it is being used in \' invoice section \'.' ) ) );
			$boolIsValid = false;
		}

		$strWhere = 'WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND ap_payee_term_id = ' . ( int ) $this->getId() . '
						AND system_code <> \'PS\'';

		$intApPayeeCount = CApPayees::fetchApPayeeCount( $strWhere, $objClientDatabase );

		if( 0 < $intApPayeeCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delete', __( '&nbsp;Cannot be deleted, it is being used in \'vendor section \'.' ) ) );
			$boolIsValid = false;
		}

		$strWhere = 'WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND ap_payee_term_id = ' . ( int ) $this->getId() . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		$intScheduledApTransactionHeaderCount = ( int ) \Psi\Eos\Entrata\CScheduledApTransactionHeaders::createService()->fetchScheduledApTransactionHeaderCount( $strWhere, $objClientDatabase );

		if( 0 < $intScheduledApTransactionHeaderCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delete', __( '&nbsp;Cannot be deleted, it is being used in \'Recurring transactions section \'.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objClientDatabase );
				$boolIsValid &= $this->valDaysForIsFromInvoiceDate();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valIsSystem();
				$boolIsValid &= $this->valDelete( $objClientDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>