<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CConstraintVariables
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseConstraintVariables extends CEosPluralBase {

	/**
	 * @return CConstraintVariable[]
	 */
	public static function fetchConstraintVariables( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CConstraintVariable', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CConstraintVariable
	 */
	public static function fetchConstraintVariable( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CConstraintVariable', $objDatabase );
	}

	public static function fetchConstraintVariableCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'constraint_variables', $objDatabase );
	}

	public static function fetchConstraintVariableByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchConstraintVariable( sprintf( 'SELECT * FROM constraint_variables WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchConstraintVariablesByCid( $intCid, $objDatabase ) {
		return self::fetchConstraintVariables( sprintf( 'SELECT * FROM constraint_variables WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchConstraintVariablesByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchConstraintVariables( sprintf( 'SELECT * FROM constraint_variables WHERE unit_type_id = %d AND cid = %d', ( int ) $intUnitTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>