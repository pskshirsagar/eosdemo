<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePlaidRequestLog extends CEosSingularBase {

	const TABLE_NAME = 'public.plaid_request_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerPlaidItemId;
	protected $m_intCustomerId;
	protected $m_intPropertyId;
	protected $m_intLeaseId;
	protected $m_intPsProductId;
	protected $m_intPsProductOptionId;
	protected $m_intPlaidRequestTypeId;
	protected $m_strPlaidRequestId;
	protected $m_strLinkSessionId;
	protected $m_intHttpResponseCode;
	protected $m_strRawRequest;
	protected $m_jsonRawRequest;
	protected $m_strRawResponse;
	protected $m_jsonRawResponse;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strErrorCode;
	protected $m_strErrorType;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_plaid_item_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerPlaidItemId', trim( $arrValues['customer_plaid_item_id'] ) ); elseif( isset( $arrValues['customer_plaid_item_id'] ) ) $this->setCustomerPlaidItemId( $arrValues['customer_plaid_item_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['ps_product_option_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductOptionId', trim( $arrValues['ps_product_option_id'] ) ); elseif( isset( $arrValues['ps_product_option_id'] ) ) $this->setPsProductOptionId( $arrValues['ps_product_option_id'] );
		if( isset( $arrValues['plaid_request_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPlaidRequestTypeId', trim( $arrValues['plaid_request_type_id'] ) ); elseif( isset( $arrValues['plaid_request_type_id'] ) ) $this->setPlaidRequestTypeId( $arrValues['plaid_request_type_id'] );
		if( isset( $arrValues['plaid_request_id'] ) && $boolDirectSet ) $this->set( 'm_strPlaidRequestId', trim( $arrValues['plaid_request_id'] ) ); elseif( isset( $arrValues['plaid_request_id'] ) ) $this->setPlaidRequestId( $arrValues['plaid_request_id'] );
		if( isset( $arrValues['link_session_id'] ) && $boolDirectSet ) $this->set( 'm_strLinkSessionId', trim( $arrValues['link_session_id'] ) ); elseif( isset( $arrValues['link_session_id'] ) ) $this->setLinkSessionId( $arrValues['link_session_id'] );
		if( isset( $arrValues['http_response_code'] ) && $boolDirectSet ) $this->set( 'm_intHttpResponseCode', trim( $arrValues['http_response_code'] ) ); elseif( isset( $arrValues['http_response_code'] ) ) $this->setHttpResponseCode( $arrValues['http_response_code'] );
		if( isset( $arrValues['raw_request'] ) ) $this->set( 'm_strRawRequest', trim( $arrValues['raw_request'] ) );
		if( isset( $arrValues['raw_response'] ) ) $this->set( 'm_strRawResponse', trim( $arrValues['raw_response'] ) );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['error_code'] ) && $boolDirectSet ) $this->set( 'm_strErrorCode', trim( $arrValues['error_code'] ) ); elseif( isset( $arrValues['error_code'] ) ) $this->setErrorCode( $arrValues['error_code'] );
		if( isset( $arrValues['error_type'] ) && $boolDirectSet ) $this->set( 'm_strErrorType', trim( $arrValues['error_type'] ) ); elseif( isset( $arrValues['error_type'] ) ) $this->setErrorType( $arrValues['error_type'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerPlaidItemId( $intCustomerPlaidItemId ) {
		$this->set( 'm_intCustomerPlaidItemId', CStrings::strToIntDef( $intCustomerPlaidItemId, NULL, false ) );
	}

	public function getCustomerPlaidItemId() {
		return $this->m_intCustomerPlaidItemId;
	}

	public function sqlCustomerPlaidItemId() {
		return ( true == isset( $this->m_intCustomerPlaidItemId ) ) ? ( string ) $this->m_intCustomerPlaidItemId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setPsProductOptionId( $intPsProductOptionId ) {
		$this->set( 'm_intPsProductOptionId', CStrings::strToIntDef( $intPsProductOptionId, NULL, false ) );
	}

	public function getPsProductOptionId() {
		return $this->m_intPsProductOptionId;
	}

	public function sqlPsProductOptionId() {
		return ( true == isset( $this->m_intPsProductOptionId ) ) ? ( string ) $this->m_intPsProductOptionId : 'NULL';
	}

	public function setPlaidRequestTypeId( $intPlaidRequestTypeId ) {
		$this->set( 'm_intPlaidRequestTypeId', CStrings::strToIntDef( $intPlaidRequestTypeId, NULL, false ) );
	}

	public function getPlaidRequestTypeId() {
		return $this->m_intPlaidRequestTypeId;
	}

	public function sqlPlaidRequestTypeId() {
		return ( true == isset( $this->m_intPlaidRequestTypeId ) ) ? ( string ) $this->m_intPlaidRequestTypeId : 'NULL';
	}

	public function setPlaidRequestId( $strPlaidRequestId ) {
		$this->set( 'm_strPlaidRequestId', CStrings::strTrimDef( $strPlaidRequestId, 100, NULL, true ) );
	}

	public function getPlaidRequestId() {
		return $this->m_strPlaidRequestId;
	}

	public function sqlPlaidRequestId() {
		return ( true == isset( $this->m_strPlaidRequestId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPlaidRequestId ) : '\'' . addslashes( $this->m_strPlaidRequestId ) . '\'' ) : 'NULL';
	}

	public function setLinkSessionId( $strLinkSessionId ) {
		$this->set( 'm_strLinkSessionId', CStrings::strTrimDef( $strLinkSessionId, -1, NULL, true ) );
	}

	public function getLinkSessionId() {
		return $this->m_strLinkSessionId;
	}

	public function sqlLinkSessionId() {
		return ( true == isset( $this->m_strLinkSessionId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLinkSessionId ) : '\'' . addslashes( $this->m_strLinkSessionId ) . '\'' ) : 'NULL';
	}

	public function setHttpResponseCode( $intHttpResponseCode ) {
		$this->set( 'm_intHttpResponseCode', CStrings::strToIntDef( $intHttpResponseCode, NULL, false ) );
	}

	public function getHttpResponseCode() {
		return $this->m_intHttpResponseCode;
	}

	public function sqlHttpResponseCode() {
		return ( true == isset( $this->m_intHttpResponseCode ) ) ? ( string ) $this->m_intHttpResponseCode : 'NULL';
	}

	public function setRawRequest( $jsonRawRequest ) {
		if( true == valObj( $jsonRawRequest, 'stdClass' ) ) {
			$this->set( 'm_jsonRawRequest', $jsonRawRequest );
		} elseif( true == valJsonString( $jsonRawRequest ) ) {
			$this->set( 'm_jsonRawRequest', CStrings::strToJson( $jsonRawRequest ) );
		} else {
			$this->set( 'm_jsonRawRequest', NULL );
		}
		unset( $this->m_strRawRequest );
	}

	public function getRawRequest() {
		if( true == isset( $this->m_strRawRequest ) ) {
			$this->m_jsonRawRequest = CStrings::strToJson( $this->m_strRawRequest );
			unset( $this->m_strRawRequest );
		}
		return $this->m_jsonRawRequest;
	}

	public function sqlRawRequest() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getRawRequest() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getRawRequest() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getRawRequest() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setRawResponse( $jsonRawResponse ) {
		if( true == valObj( $jsonRawResponse, 'stdClass' ) ) {
			$this->set( 'm_jsonRawResponse', $jsonRawResponse );
		} elseif( true == valJsonString( $jsonRawResponse ) ) {
			$this->set( 'm_jsonRawResponse', CStrings::strToJson( $jsonRawResponse ) );
		} else {
			$this->set( 'm_jsonRawResponse', NULL );
		}
		unset( $this->m_strRawResponse );
	}

	public function getRawResponse() {
		if( true == isset( $this->m_strRawResponse ) ) {
			$this->m_jsonRawResponse = CStrings::strToJson( $this->m_strRawResponse );
			unset( $this->m_strRawResponse );
		}
		return $this->m_jsonRawResponse;
	}

	public function sqlRawResponse() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getRawResponse() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getRawResponse() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getRawResponse() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function setErrorCode( $strErrorCode ) {
		$this->set( 'm_strErrorCode', CStrings::strTrimDef( $strErrorCode, 100, NULL, true ) );
	}

	public function getErrorCode() {
		return $this->m_strErrorCode;
	}

	public function sqlErrorCode() {
		return ( true == isset( $this->m_strErrorCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strErrorCode ) : '\'' . addslashes( $this->m_strErrorCode ) . '\'' ) : 'NULL';
	}

	public function setErrorType( $strErrorType ) {
		$this->set( 'm_strErrorType', CStrings::strTrimDef( $strErrorType, 100, NULL, true ) );
	}

	public function getErrorType() {
		return $this->m_strErrorType;
	}

	public function sqlErrorType() {
		return ( true == isset( $this->m_strErrorType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strErrorType ) : '\'' . addslashes( $this->m_strErrorType ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_plaid_item_id, customer_id, property_id, lease_id, ps_product_id, ps_product_option_id, plaid_request_type_id, plaid_request_id, link_session_id, http_response_code, raw_request, raw_response, created_by, created_on, error_code, error_type )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCustomerPlaidItemId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlPsProductOptionId() . ', ' .
						$this->sqlPlaidRequestTypeId() . ', ' .
						$this->sqlPlaidRequestId() . ', ' .
						$this->sqlLinkSessionId() . ', ' .
						$this->sqlHttpResponseCode() . ', ' .
						$this->sqlRawRequest() . ', ' .
						$this->sqlRawResponse() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlErrorCode() . ', ' .
						$this->sqlErrorType() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_plaid_item_id = ' . $this->sqlCustomerPlaidItemId(). ',' ; } elseif( true == array_key_exists( 'CustomerPlaidItemId', $this->getChangedColumns() ) ) { $strSql .= ' customer_plaid_item_id = ' . $this->sqlCustomerPlaidItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId(). ',' ; } elseif( true == array_key_exists( 'PsProductOptionId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' plaid_request_type_id = ' . $this->sqlPlaidRequestTypeId(). ',' ; } elseif( true == array_key_exists( 'PlaidRequestTypeId', $this->getChangedColumns() ) ) { $strSql .= ' plaid_request_type_id = ' . $this->sqlPlaidRequestTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' plaid_request_id = ' . $this->sqlPlaidRequestId(). ',' ; } elseif( true == array_key_exists( 'PlaidRequestId', $this->getChangedColumns() ) ) { $strSql .= ' plaid_request_id = ' . $this->sqlPlaidRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' link_session_id = ' . $this->sqlLinkSessionId(). ',' ; } elseif( true == array_key_exists( 'LinkSessionId', $this->getChangedColumns() ) ) { $strSql .= ' link_session_id = ' . $this->sqlLinkSessionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' http_response_code = ' . $this->sqlHttpResponseCode(). ',' ; } elseif( true == array_key_exists( 'HttpResponseCode', $this->getChangedColumns() ) ) { $strSql .= ' http_response_code = ' . $this->sqlHttpResponseCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' raw_request = ' . $this->sqlRawRequest(). ',' ; } elseif( true == array_key_exists( 'RawRequest', $this->getChangedColumns() ) ) { $strSql .= ' raw_request = ' . $this->sqlRawRequest() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' raw_response = ' . $this->sqlRawResponse(). ',' ; } elseif( true == array_key_exists( 'RawResponse', $this->getChangedColumns() ) ) { $strSql .= ' raw_response = ' . $this->sqlRawResponse() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_code = ' . $this->sqlErrorCode(). ',' ; } elseif( true == array_key_exists( 'ErrorCode', $this->getChangedColumns() ) ) { $strSql .= ' error_code = ' . $this->sqlErrorCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_type = ' . $this->sqlErrorType() ; } elseif( true == array_key_exists( 'ErrorType', $this->getChangedColumns() ) ) { $strSql .= ' error_type = ' . $this->sqlErrorType() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_plaid_item_id' => $this->getCustomerPlaidItemId(),
			'customer_id' => $this->getCustomerId(),
			'property_id' => $this->getPropertyId(),
			'lease_id' => $this->getLeaseId(),
			'ps_product_id' => $this->getPsProductId(),
			'ps_product_option_id' => $this->getPsProductOptionId(),
			'plaid_request_type_id' => $this->getPlaidRequestTypeId(),
			'plaid_request_id' => $this->getPlaidRequestId(),
			'link_session_id' => $this->getLinkSessionId(),
			'http_response_code' => $this->getHttpResponseCode(),
			'raw_request' => $this->getRawRequest(),
			'raw_response' => $this->getRawResponse(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'error_code' => $this->getErrorCode(),
			'error_type' => $this->getErrorType()
		);
	}

}
?>
