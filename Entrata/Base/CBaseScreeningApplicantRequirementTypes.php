<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningApplicantRequirementTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningApplicantRequirementTypes extends CEosPluralBase {

	/**
	 * @return CScreeningApplicantRequirementType[]
	 */
	public static function fetchScreeningApplicantRequirementTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningApplicantRequirementType::class, $objDatabase );
	}

	/**
	 * @return CScreeningApplicantRequirementType
	 */
	public static function fetchScreeningApplicantRequirementType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningApplicantRequirementType::class, $objDatabase );
	}

	public static function fetchScreeningApplicantRequirementTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_applicant_requirement_types', $objDatabase );
	}

	public static function fetchScreeningApplicantRequirementTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningApplicantRequirementType( sprintf( 'SELECT * FROM screening_applicant_requirement_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>