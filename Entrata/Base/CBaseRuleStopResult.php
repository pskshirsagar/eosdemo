<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRuleStopResult extends CEosSingularBase {

	const TABLE_NAME = 'public.rule_stop_results';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intRuleStopId;
	protected $m_intRuleStopStatusTypeId;
	protected $m_intReferenceId;
	protected $m_intRouteTypeId;
	protected $m_intCompanyUserId;
	protected $m_strResultDatetime;
	protected $m_strNotes;
	protected $m_strIpAddress;
	protected $m_boolIsArchived;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsArchived = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['rule_stop_id'] ) && $boolDirectSet ) $this->set( 'm_intRuleStopId', trim( $arrValues['rule_stop_id'] ) ); elseif( isset( $arrValues['rule_stop_id'] ) ) $this->setRuleStopId( $arrValues['rule_stop_id'] );
		if( isset( $arrValues['rule_stop_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRuleStopStatusTypeId', trim( $arrValues['rule_stop_status_type_id'] ) ); elseif( isset( $arrValues['rule_stop_status_type_id'] ) ) $this->setRuleStopStatusTypeId( $arrValues['rule_stop_status_type_id'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		if( isset( $arrValues['route_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRouteTypeId', trim( $arrValues['route_type_id'] ) ); elseif( isset( $arrValues['route_type_id'] ) ) $this->setRouteTypeId( $arrValues['route_type_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['result_datetime'] ) && $boolDirectSet ) $this->set( 'm_strResultDatetime', trim( $arrValues['result_datetime'] ) ); elseif( isset( $arrValues['result_datetime'] ) ) $this->setResultDatetime( $arrValues['result_datetime'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( stripcslashes( $arrValues['ip_address'] ) ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ip_address'] ) : $arrValues['ip_address'] );
		if( isset( $arrValues['is_archived'] ) && $boolDirectSet ) $this->set( 'm_boolIsArchived', trim( stripcslashes( $arrValues['is_archived'] ) ) ); elseif( isset( $arrValues['is_archived'] ) ) $this->setIsArchived( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_archived'] ) : $arrValues['is_archived'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setRuleStopId( $intRuleStopId ) {
		$this->set( 'm_intRuleStopId', CStrings::strToIntDef( $intRuleStopId, NULL, false ) );
	}

	public function getRuleStopId() {
		return $this->m_intRuleStopId;
	}

	public function sqlRuleStopId() {
		return ( true == isset( $this->m_intRuleStopId ) ) ? ( string ) $this->m_intRuleStopId : 'NULL';
	}

	public function setRuleStopStatusTypeId( $intRuleStopStatusTypeId ) {
		$this->set( 'm_intRuleStopStatusTypeId', CStrings::strToIntDef( $intRuleStopStatusTypeId, NULL, false ) );
	}

	public function getRuleStopStatusTypeId() {
		return $this->m_intRuleStopStatusTypeId;
	}

	public function sqlRuleStopStatusTypeId() {
		return ( true == isset( $this->m_intRuleStopStatusTypeId ) ) ? ( string ) $this->m_intRuleStopStatusTypeId : 'NULL';
	}

	public function setReferenceId( $intReferenceId ) {
		$this->set( 'm_intReferenceId', CStrings::strToIntDef( $intReferenceId, NULL, false ) );
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_intReferenceId ) ) ? ( string ) $this->m_intReferenceId : 'NULL';
	}

	public function setRouteTypeId( $intRouteTypeId ) {
		$this->set( 'm_intRouteTypeId', CStrings::strToIntDef( $intRouteTypeId, NULL, false ) );
	}

	public function getRouteTypeId() {
		return $this->m_intRouteTypeId;
	}

	public function sqlRouteTypeId() {
		return ( true == isset( $this->m_intRouteTypeId ) ) ? ( string ) $this->m_intRouteTypeId : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setResultDatetime( $strResultDatetime ) {
		$this->set( 'm_strResultDatetime', CStrings::strTrimDef( $strResultDatetime, -1, NULL, true ) );
	}

	public function getResultDatetime() {
		return $this->m_strResultDatetime;
	}

	public function sqlResultDatetime() {
		return ( true == isset( $this->m_strResultDatetime ) ) ? '\'' . $this->m_strResultDatetime . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 24, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? '\'' . addslashes( $this->m_strIpAddress ) . '\'' : 'NULL';
	}

	public function setIsArchived( $boolIsArchived ) {
		$this->set( 'm_boolIsArchived', CStrings::strToBool( $boolIsArchived ) );
	}

	public function getIsArchived() {
		return $this->m_boolIsArchived;
	}

	public function sqlIsArchived() {
		return ( true == isset( $this->m_boolIsArchived ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsArchived ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, rule_stop_id, rule_stop_status_type_id, reference_id, route_type_id, company_user_id, result_datetime, notes, ip_address, is_archived, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlRuleStopId() . ', ' .
 						$this->sqlRuleStopStatusTypeId() . ', ' .
 						$this->sqlReferenceId() . ', ' .
 						$this->sqlRouteTypeId() . ', ' .
 						$this->sqlCompanyUserId() . ', ' .
 						$this->sqlResultDatetime() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlIpAddress() . ', ' .
 						$this->sqlIsArchived() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rule_stop_id = ' . $this->sqlRuleStopId() . ','; } elseif( true == array_key_exists( 'RuleStopId', $this->getChangedColumns() ) ) { $strSql .= ' rule_stop_id = ' . $this->sqlRuleStopId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rule_stop_status_type_id = ' . $this->sqlRuleStopStatusTypeId() . ','; } elseif( true == array_key_exists( 'RuleStopStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' rule_stop_status_type_id = ' . $this->sqlRuleStopStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' route_type_id = ' . $this->sqlRouteTypeId() . ','; } elseif( true == array_key_exists( 'RouteTypeId', $this->getChangedColumns() ) ) { $strSql .= ' route_type_id = ' . $this->sqlRouteTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' result_datetime = ' . $this->sqlResultDatetime() . ','; } elseif( true == array_key_exists( 'ResultDatetime', $this->getChangedColumns() ) ) { $strSql .= ' result_datetime = ' . $this->sqlResultDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived() . ','; } elseif( true == array_key_exists( 'IsArchived', $this->getChangedColumns() ) ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'rule_stop_id' => $this->getRuleStopId(),
			'rule_stop_status_type_id' => $this->getRuleStopStatusTypeId(),
			'reference_id' => $this->getReferenceId(),
			'route_type_id' => $this->getRouteTypeId(),
			'company_user_id' => $this->getCompanyUserId(),
			'result_datetime' => $this->getResultDatetime(),
			'notes' => $this->getNotes(),
			'ip_address' => $this->getIpAddress(),
			'is_archived' => $this->getIsArchived(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>