<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAddOnGroup extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.add_on_groups';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intAddOnTypeId;
	protected $m_intAddOnCategoryId;
	protected $m_intCategorizationTypeId;
	protected $m_intCompanyMediaFileId;
	protected $m_intSitePlanCompanyMediaFileId;
	protected $m_strRemotePrimaryKey;
	protected $m_strLookupCode;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_boolLockToLeaseDates;
	protected $m_boolHasInventory;
	protected $m_boolIsOptional;
	protected $m_boolIsMarketed;
	protected $m_boolShowOnWebsite;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intMappingPropertyAmenityId;
	protected $m_intMappingUnitAmenityId;
	protected $m_intMappingUnitSpaceAmenityId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_boolShowInEntrata;
	protected $m_boolShowInApplication;
	protected $m_boolShowInResidentPortal;

	public function __construct() {
		parent::__construct();

		$this->m_intCategorizationTypeId = '3';
		$this->m_boolLockToLeaseDates = false;
		$this->m_boolHasInventory = false;
		$this->m_boolIsOptional = true;
		$this->m_boolIsMarketed = false;
		$this->m_boolShowOnWebsite = true;
		$this->m_intOrderNum = '0';
		$this->m_boolShowInEntrata = true;
		$this->m_boolShowInApplication = true;
		$this->m_boolShowInResidentPortal = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['add_on_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAddOnTypeId', trim( $arrValues['add_on_type_id'] ) ); elseif( isset( $arrValues['add_on_type_id'] ) ) $this->setAddOnTypeId( $arrValues['add_on_type_id'] );
		if( isset( $arrValues['add_on_category_id'] ) && $boolDirectSet ) $this->set( 'm_intAddOnCategoryId', trim( $arrValues['add_on_category_id'] ) ); elseif( isset( $arrValues['add_on_category_id'] ) ) $this->setAddOnCategoryId( $arrValues['add_on_category_id'] );
		if( isset( $arrValues['categorization_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCategorizationTypeId', trim( $arrValues['categorization_type_id'] ) ); elseif( isset( $arrValues['categorization_type_id'] ) ) $this->setCategorizationTypeId( $arrValues['categorization_type_id'] );
		if( isset( $arrValues['company_media_file_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMediaFileId', trim( $arrValues['company_media_file_id'] ) ); elseif( isset( $arrValues['company_media_file_id'] ) ) $this->setCompanyMediaFileId( $arrValues['company_media_file_id'] );
		if( isset( $arrValues['site_plan_company_media_file_id'] ) && $boolDirectSet ) $this->set( 'm_intSitePlanCompanyMediaFileId', trim( $arrValues['site_plan_company_media_file_id'] ) ); elseif( isset( $arrValues['site_plan_company_media_file_id'] ) ) $this->setSitePlanCompanyMediaFileId( $arrValues['site_plan_company_media_file_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['lookup_code'] ) && $boolDirectSet ) $this->set( 'm_strLookupCode', trim( $arrValues['lookup_code'] ) ); elseif( isset( $arrValues['lookup_code'] ) ) $this->setLookupCode( $arrValues['lookup_code'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['lock_to_lease_dates'] ) && $boolDirectSet ) $this->set( 'm_boolLockToLeaseDates', trim( stripcslashes( $arrValues['lock_to_lease_dates'] ) ) ); elseif( isset( $arrValues['lock_to_lease_dates'] ) ) $this->setLockToLeaseDates( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['lock_to_lease_dates'] ) : $arrValues['lock_to_lease_dates'] );
		if( isset( $arrValues['has_inventory'] ) && $boolDirectSet ) $this->set( 'm_boolHasInventory', trim( stripcslashes( $arrValues['has_inventory'] ) ) ); elseif( isset( $arrValues['has_inventory'] ) ) $this->setHasInventory( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_inventory'] ) : $arrValues['has_inventory'] );
		if( isset( $arrValues['is_optional'] ) && $boolDirectSet ) $this->set( 'm_boolIsOptional', trim( stripcslashes( $arrValues['is_optional'] ) ) ); elseif( isset( $arrValues['is_optional'] ) ) $this->setIsOptional( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_optional'] ) : $arrValues['is_optional'] );
		if( isset( $arrValues['is_marketed'] ) && $boolDirectSet ) $this->set( 'm_boolIsMarketed', trim( stripcslashes( $arrValues['is_marketed'] ) ) ); elseif( isset( $arrValues['is_marketed'] ) ) $this->setIsMarketed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_marketed'] ) : $arrValues['is_marketed'] );
		if( isset( $arrValues['show_on_website'] ) && $boolDirectSet ) $this->set( 'm_boolShowOnWebsite', trim( stripcslashes( $arrValues['show_on_website'] ) ) ); elseif( isset( $arrValues['show_on_website'] ) ) $this->setShowOnWebsite( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_on_website'] ) : $arrValues['show_on_website'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['mapping_property_amenity_id'] ) && $boolDirectSet ) $this->set( 'm_intMappingPropertyAmenityId', trim( $arrValues['mapping_property_amenity_id'] ) ); elseif( isset( $arrValues['mapping_property_amenity_id'] ) ) $this->setMappingPropertyAmenityId( $arrValues['mapping_property_amenity_id'] );
		if( isset( $arrValues['mapping_unit_amenity_id'] ) && $boolDirectSet ) $this->set( 'm_intMappingUnitAmenityId', trim( $arrValues['mapping_unit_amenity_id'] ) ); elseif( isset( $arrValues['mapping_unit_amenity_id'] ) ) $this->setMappingUnitAmenityId( $arrValues['mapping_unit_amenity_id'] );
		if( isset( $arrValues['mapping_unit_space_amenity_id'] ) && $boolDirectSet ) $this->set( 'm_intMappingUnitSpaceAmenityId', trim( $arrValues['mapping_unit_space_amenity_id'] ) ); elseif( isset( $arrValues['mapping_unit_space_amenity_id'] ) ) $this->setMappingUnitSpaceAmenityId( $arrValues['mapping_unit_space_amenity_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['show_in_entrata'] ) && $boolDirectSet ) $this->set( 'm_boolShowInEntrata', trim( stripcslashes( $arrValues['show_in_entrata'] ) ) ); elseif( isset( $arrValues['show_in_entrata'] ) ) $this->setShowInEntrata( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_in_entrata'] ) : $arrValues['show_in_entrata'] );
		if( isset( $arrValues['show_in_application'] ) && $boolDirectSet ) $this->set( 'm_boolShowInApplication', trim( stripcslashes( $arrValues['show_in_application'] ) ) ); elseif( isset( $arrValues['show_in_application'] ) ) $this->setShowInApplication( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_in_application'] ) : $arrValues['show_in_application'] );
		if( isset( $arrValues['show_in_resident_portal'] ) && $boolDirectSet ) $this->set( 'm_boolShowInResidentPortal', trim( stripcslashes( $arrValues['show_in_resident_portal'] ) ) ); elseif( isset( $arrValues['show_in_resident_portal'] ) ) $this->setShowInResidentPortal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_in_resident_portal'] ) : $arrValues['show_in_resident_portal'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setAddOnTypeId( $intAddOnTypeId ) {
		$this->set( 'm_intAddOnTypeId', CStrings::strToIntDef( $intAddOnTypeId, NULL, false ) );
	}

	public function getAddOnTypeId() {
		return $this->m_intAddOnTypeId;
	}

	public function sqlAddOnTypeId() {
		return ( true == isset( $this->m_intAddOnTypeId ) ) ? ( string ) $this->m_intAddOnTypeId : 'NULL';
	}

	public function setAddOnCategoryId( $intAddOnCategoryId ) {
		$this->set( 'm_intAddOnCategoryId', CStrings::strToIntDef( $intAddOnCategoryId, NULL, false ) );
	}

	public function getAddOnCategoryId() {
		return $this->m_intAddOnCategoryId;
	}

	public function sqlAddOnCategoryId() {
		return ( true == isset( $this->m_intAddOnCategoryId ) ) ? ( string ) $this->m_intAddOnCategoryId : 'NULL';
	}

	public function setCategorizationTypeId( $intCategorizationTypeId ) {
		$this->set( 'm_intCategorizationTypeId', CStrings::strToIntDef( $intCategorizationTypeId, NULL, false ) );
	}

	public function getCategorizationTypeId() {
		return $this->m_intCategorizationTypeId;
	}

	public function sqlCategorizationTypeId() {
		return ( true == isset( $this->m_intCategorizationTypeId ) ) ? ( string ) $this->m_intCategorizationTypeId : '3';
	}

	public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
		$this->set( 'm_intCompanyMediaFileId', CStrings::strToIntDef( $intCompanyMediaFileId, NULL, false ) );
	}

	public function getCompanyMediaFileId() {
		return $this->m_intCompanyMediaFileId;
	}

	public function sqlCompanyMediaFileId() {
		return ( true == isset( $this->m_intCompanyMediaFileId ) ) ? ( string ) $this->m_intCompanyMediaFileId : 'NULL';
	}

	public function setSitePlanCompanyMediaFileId( $intSitePlanCompanyMediaFileId ) {
		$this->set( 'm_intSitePlanCompanyMediaFileId', CStrings::strToIntDef( $intSitePlanCompanyMediaFileId, NULL, false ) );
	}

	public function getSitePlanCompanyMediaFileId() {
		return $this->m_intSitePlanCompanyMediaFileId;
	}

	public function sqlSitePlanCompanyMediaFileId() {
		return ( true == isset( $this->m_intSitePlanCompanyMediaFileId ) ) ? ( string ) $this->m_intSitePlanCompanyMediaFileId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setLookupCode( $strLookupCode ) {
		$this->set( 'm_strLookupCode', CStrings::strTrimDef( $strLookupCode, 64, NULL, true ) );
	}

	public function getLookupCode() {
		return $this->m_strLookupCode;
	}

	public function sqlLookupCode() {
		return ( true == isset( $this->m_strLookupCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLookupCode ) : '\'' . addslashes( $this->m_strLookupCode ) . '\'' ) : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 2000, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setLockToLeaseDates( $boolLockToLeaseDates ) {
		$this->set( 'm_boolLockToLeaseDates', CStrings::strToBool( $boolLockToLeaseDates ) );
	}

	public function getLockToLeaseDates() {
		return $this->m_boolLockToLeaseDates;
	}

	public function sqlLockToLeaseDates() {
		return ( true == isset( $this->m_boolLockToLeaseDates ) ) ? '\'' . ( true == ( bool ) $this->m_boolLockToLeaseDates ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasInventory( $boolHasInventory ) {
		$this->set( 'm_boolHasInventory', CStrings::strToBool( $boolHasInventory ) );
	}

	public function getHasInventory() {
		return $this->m_boolHasInventory;
	}

	public function sqlHasInventory() {
		return ( true == isset( $this->m_boolHasInventory ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasInventory ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsOptional( $boolIsOptional ) {
		$this->set( 'm_boolIsOptional', CStrings::strToBool( $boolIsOptional ) );
	}

	public function getIsOptional() {
		return $this->m_boolIsOptional;
	}

	public function sqlIsOptional() {
		return ( true == isset( $this->m_boolIsOptional ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOptional ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsMarketed( $boolIsMarketed ) {
		$this->set( 'm_boolIsMarketed', CStrings::strToBool( $boolIsMarketed ) );
	}

	public function getIsMarketed() {
		return $this->m_boolIsMarketed;
	}

	public function sqlIsMarketed() {
		return ( true == isset( $this->m_boolIsMarketed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsMarketed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowOnWebsite( $boolShowOnWebsite ) {
		$this->set( 'm_boolShowOnWebsite', CStrings::strToBool( $boolShowOnWebsite ) );
	}

	public function getShowOnWebsite() {
		return $this->m_boolShowOnWebsite;
	}

	public function sqlShowOnWebsite() {
		return ( true == isset( $this->m_boolShowOnWebsite ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowOnWebsite ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setMappingPropertyAmenityId( $intMappingPropertyAmenityId ) {
		$this->set( 'm_intMappingPropertyAmenityId', CStrings::strToIntDef( $intMappingPropertyAmenityId, NULL, false ) );
	}

	public function getMappingPropertyAmenityId() {
		return $this->m_intMappingPropertyAmenityId;
	}

	public function sqlMappingPropertyAmenityId() {
		return ( true == isset( $this->m_intMappingPropertyAmenityId ) ) ? ( string ) $this->m_intMappingPropertyAmenityId : 'NULL';
	}

	public function setMappingUnitAmenityId( $intMappingUnitAmenityId ) {
		$this->set( 'm_intMappingUnitAmenityId', CStrings::strToIntDef( $intMappingUnitAmenityId, NULL, false ) );
	}

	public function getMappingUnitAmenityId() {
		return $this->m_intMappingUnitAmenityId;
	}

	public function sqlMappingUnitAmenityId() {
		return ( true == isset( $this->m_intMappingUnitAmenityId ) ) ? ( string ) $this->m_intMappingUnitAmenityId : 'NULL';
	}

	public function setMappingUnitSpaceAmenityId( $intMappingUnitSpaceAmenityId ) {
		$this->set( 'm_intMappingUnitSpaceAmenityId', CStrings::strToIntDef( $intMappingUnitSpaceAmenityId, NULL, false ) );
	}

	public function getMappingUnitSpaceAmenityId() {
		return $this->m_intMappingUnitSpaceAmenityId;
	}

	public function sqlMappingUnitSpaceAmenityId() {
		return ( true == isset( $this->m_intMappingUnitSpaceAmenityId ) ) ? ( string ) $this->m_intMappingUnitSpaceAmenityId : 'NULL';
	}

	public function setShowInEntrata( $boolShowInEntrata ) {
		$this->set( 'm_boolShowInEntrata', CStrings::strToBool( $boolShowInEntrata ) );
	}

	public function getShowInEntrata() {
		return $this->m_boolShowInEntrata;
	}

	public function sqlShowInEntrata() {
		return ( true == isset( $this->m_boolShowInEntrata ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowInEntrata ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowInApplication( $boolShowInApplication ) {
		$this->set( 'm_boolShowInApplication', CStrings::strToBool( $boolShowInApplication ) );
	}

	public function getShowInApplication() {
		return $this->m_boolShowInApplication;
	}

	public function sqlShowInApplication() {
		return ( true == isset( $this->m_boolShowInApplication ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowInApplication ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowInResidentPortal( $boolShowInResidentPortal ) {
		$this->set( 'm_boolShowInResidentPortal', CStrings::strToBool( $boolShowInResidentPortal ) );
	}

	public function getShowInResidentPortal() {
		return $this->m_boolShowInResidentPortal;
	}

	public function sqlShowInResidentPortal() {
		return ( true == isset( $this->m_boolShowInResidentPortal ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowInResidentPortal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, add_on_type_id, add_on_category_id, categorization_type_id, company_media_file_id, site_plan_company_media_file_id, remote_primary_key, lookup_code, name, description, lock_to_lease_dates, has_inventory, is_optional, is_marketed, show_on_website, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, mapping_property_amenity_id, mapping_unit_amenity_id, mapping_unit_space_amenity_id, details, show_in_entrata, show_in_application, show_in_resident_portal )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlAddOnTypeId() . ', ' .
		          $this->sqlAddOnCategoryId() . ', ' .
		          $this->sqlCategorizationTypeId() . ', ' .
		          $this->sqlCompanyMediaFileId() . ', ' .
		          $this->sqlSitePlanCompanyMediaFileId() . ', ' .
		          $this->sqlRemotePrimaryKey() . ', ' .
		          $this->sqlLookupCode() . ', ' .
		          $this->sqlName() . ', ' .
		          $this->sqlDescription() . ', ' .
		          $this->sqlLockToLeaseDates() . ', ' .
		          $this->sqlHasInventory() . ', ' .
		          $this->sqlIsOptional() . ', ' .
		          $this->sqlIsMarketed() . ', ' .
		          $this->sqlShowOnWebsite() . ', ' .
		          $this->sqlOrderNum() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlMappingPropertyAmenityId() . ', ' .
		          $this->sqlMappingUnitAmenityId() . ', ' .
		          $this->sqlMappingUnitSpaceAmenityId() . ', ' .
		          $this->sqlDetails() . ', ' .
		          $this->sqlShowInEntrata() . ', ' .
		          $this->sqlShowInApplication() . ', ' .
		          $this->sqlShowInResidentPortal() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_type_id = ' . $this->sqlAddOnTypeId(). ',' ; } elseif( true == array_key_exists( 'AddOnTypeId', $this->getChangedColumns() ) ) { $strSql .= ' add_on_type_id = ' . $this->sqlAddOnTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_category_id = ' . $this->sqlAddOnCategoryId(). ',' ; } elseif( true == array_key_exists( 'AddOnCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' add_on_category_id = ' . $this->sqlAddOnCategoryId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' categorization_type_id = ' . $this->sqlCategorizationTypeId(). ',' ; } elseif( true == array_key_exists( 'CategorizationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' categorization_type_id = ' . $this->sqlCategorizationTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId(). ',' ; } elseif( true == array_key_exists( 'CompanyMediaFileId', $this->getChangedColumns() ) ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' site_plan_company_media_file_id = ' . $this->sqlSitePlanCompanyMediaFileId(). ',' ; } elseif( true == array_key_exists( 'SitePlanCompanyMediaFileId', $this->getChangedColumns() ) ) { $strSql .= ' site_plan_company_media_file_id = ' . $this->sqlSitePlanCompanyMediaFileId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lookup_code = ' . $this->sqlLookupCode(). ',' ; } elseif( true == array_key_exists( 'LookupCode', $this->getChangedColumns() ) ) { $strSql .= ' lookup_code = ' . $this->sqlLookupCode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lock_to_lease_dates = ' . $this->sqlLockToLeaseDates(). ',' ; } elseif( true == array_key_exists( 'LockToLeaseDates', $this->getChangedColumns() ) ) { $strSql .= ' lock_to_lease_dates = ' . $this->sqlLockToLeaseDates() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_inventory = ' . $this->sqlHasInventory(). ',' ; } elseif( true == array_key_exists( 'HasInventory', $this->getChangedColumns() ) ) { $strSql .= ' has_inventory = ' . $this->sqlHasInventory() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_optional = ' . $this->sqlIsOptional(). ',' ; } elseif( true == array_key_exists( 'IsOptional', $this->getChangedColumns() ) ) { $strSql .= ' is_optional = ' . $this->sqlIsOptional() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_marketed = ' . $this->sqlIsMarketed(). ',' ; } elseif( true == array_key_exists( 'IsMarketed', $this->getChangedColumns() ) ) { $strSql .= ' is_marketed = ' . $this->sqlIsMarketed() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite(). ',' ; } elseif( true == array_key_exists( 'ShowOnWebsite', $this->getChangedColumns() ) ) { $strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mapping_property_amenity_id = ' . $this->sqlMappingPropertyAmenityId(). ',' ; } elseif( true == array_key_exists( 'MappingPropertyAmenityId', $this->getChangedColumns() ) ) { $strSql .= ' mapping_property_amenity_id = ' . $this->sqlMappingPropertyAmenityId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mapping_unit_amenity_id = ' . $this->sqlMappingUnitAmenityId(). ',' ; } elseif( true == array_key_exists( 'MappingUnitAmenityId', $this->getChangedColumns() ) ) { $strSql .= ' mapping_unit_amenity_id = ' . $this->sqlMappingUnitAmenityId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mapping_unit_space_amenity_id = ' . $this->sqlMappingUnitSpaceAmenityId(). ',' ; } elseif( true == array_key_exists( 'MappingUnitSpaceAmenityId', $this->getChangedColumns() ) ) { $strSql .= ' mapping_unit_space_amenity_id = ' . $this->sqlMappingUnitSpaceAmenityId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_in_entrata = ' . $this->sqlShowInEntrata(). ',' ; } elseif( true == array_key_exists( 'ShowInEntrata', $this->getChangedColumns() ) ) { $strSql .= ' show_in_entrata = ' . $this->sqlShowInEntrata() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_in_application = ' . $this->sqlShowInApplication(). ',' ; } elseif( true == array_key_exists( 'ShowInApplication', $this->getChangedColumns() ) ) { $strSql .= ' show_in_application = ' . $this->sqlShowInApplication() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_in_resident_portal = ' . $this->sqlShowInResidentPortal(). ',' ; } elseif( true == array_key_exists( 'ShowInResidentPortal', $this->getChangedColumns() ) ) { $strSql .= ' show_in_resident_portal = ' . $this->sqlShowInResidentPortal() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'add_on_type_id' => $this->getAddOnTypeId(),
			'add_on_category_id' => $this->getAddOnCategoryId(),
			'categorization_type_id' => $this->getCategorizationTypeId(),
			'company_media_file_id' => $this->getCompanyMediaFileId(),
			'site_plan_company_media_file_id' => $this->getSitePlanCompanyMediaFileId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'lookup_code' => $this->getLookupCode(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'lock_to_lease_dates' => $this->getLockToLeaseDates(),
			'has_inventory' => $this->getHasInventory(),
			'is_optional' => $this->getIsOptional(),
			'is_marketed' => $this->getIsMarketed(),
			'show_on_website' => $this->getShowOnWebsite(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'mapping_property_amenity_id' => $this->getMappingPropertyAmenityId(),
			'mapping_unit_amenity_id' => $this->getMappingUnitAmenityId(),
			'mapping_unit_space_amenity_id' => $this->getMappingUnitSpaceAmenityId(),
			'details' => $this->getDetails(),
			'show_in_entrata' => $this->getShowInEntrata(),
			'show_in_application' => $this->getShowInApplication(),
			'show_in_resident_portal' => $this->getShowInResidentPortal()
		);
	}

}
?>