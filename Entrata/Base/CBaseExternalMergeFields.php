<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CExternalMergeFields
 * Do not add any new functions to this class.
 */

class CBaseExternalMergeFields extends CEosPluralBase {

	/**
	 * @return CExternalMergeField[]
	 */
	public static function fetchExternalMergeFields( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CExternalMergeField', $objDatabase );
	}

	/**
	 * @return CExternalMergeField
	 */
	public static function fetchExternalMergeField( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CExternalMergeField', $objDatabase );
	}

	public static function fetchExternalMergeFieldCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'external_merge_fields', $objDatabase );
	}

	public static function fetchExternalMergeFieldById( $intId, $objDatabase ) {
		return self::fetchExternalMergeField( sprintf( 'SELECT * FROM external_merge_fields WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchExternalMergeFieldsByTransmissionVendorId( $intTransmissionVendorId, $objDatabase ) {
		return self::fetchExternalMergeFields( sprintf( 'SELECT * FROM external_merge_fields WHERE transmission_vendor_id = %d', ( int ) $intTransmissionVendorId ), $objDatabase );
	}

	public static function fetchExternalMergeFieldsByBlockDefaultMergeFieldId( $intBlockDefaultMergeFieldId, $objDatabase ) {
		return self::fetchExternalMergeFields( sprintf( 'SELECT * FROM external_merge_fields WHERE block_default_merge_field_id = %d', ( int ) $intBlockDefaultMergeFieldId ), $objDatabase );
	}

	public static function fetchExternalMergeFieldsByMergeFieldGroupId( $intMergeFieldGroupId, $objDatabase ) {
		return self::fetchExternalMergeFields( sprintf( 'SELECT * FROM external_merge_fields WHERE merge_field_group_id = %d', ( int ) $intMergeFieldGroupId ), $objDatabase );
	}

}
?>