<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CComparableRoomTypes
 * Do not add any new functions to this class.
 */

class CBaseComparableRoomTypes extends CEosPluralBase {

	/**
	 * @return CComparableRoomType[]
	 */
	public static function fetchComparableRoomTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CComparableRoomType::class, $objDatabase );
	}

	/**
	 * @return CComparableRoomType
	 */
	public static function fetchComparableRoomType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CComparableRoomType::class, $objDatabase );
	}

	public static function fetchComparableRoomTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'comparable_room_types', $objDatabase );
	}

	public static function fetchComparableRoomTypeById( $intId, $objDatabase ) {
		return self::fetchComparableRoomType( sprintf( 'SELECT * FROM comparable_room_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>