<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyMaintenanceExceptions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyMaintenanceExceptions extends CEosPluralBase {

	/**
	 * @return CPropertyMaintenanceException[]
	 */
	public static function fetchPropertyMaintenanceExceptions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPropertyMaintenanceException::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyMaintenanceException
	 */
	public static function fetchPropertyMaintenanceException( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyMaintenanceException::class, $objDatabase );
	}

	public static function fetchPropertyMaintenanceExceptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_maintenance_exceptions', $objDatabase );
	}

	public static function fetchPropertyMaintenanceExceptionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceException( sprintf( 'SELECT * FROM property_maintenance_exceptions WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceExceptionsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceExceptions( sprintf( 'SELECT * FROM property_maintenance_exceptions WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceExceptionsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceExceptions( sprintf( 'SELECT * FROM property_maintenance_exceptions WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyMaintenanceExceptionsByMaintenanceExceptionIdByCid( $intMaintenanceExceptionId, $intCid, $objDatabase ) {
		return self::fetchPropertyMaintenanceExceptions( sprintf( 'SELECT * FROM property_maintenance_exceptions WHERE maintenance_exception_id = %d AND cid = %d', $intMaintenanceExceptionId, $intCid ), $objDatabase );
	}

}
?>