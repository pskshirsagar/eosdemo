<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerExpense extends CEosSingularBase {

	use TEosDetails;

	use TEosPostalAddresses;

	const TABLE_NAME = 'public.customer_expenses';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerId;
	protected $m_intExpenseTypeId;
	protected $m_intFrequencyId;
	protected $m_arrintEnabledCustomerIncomeIds;
	protected $m_strDescription;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strInstitutionName;
	protected $m_strInstitutionStreetLine1;
	protected $m_strInstitutionStreetLine2;
	protected $m_strInstitutionStreetLine3;
	protected $m_strInstitutionCity;
	protected $m_strInstitutionStateCode;
	protected $m_strInstitutionProvince;
	protected $m_strInstitutionPostalCode;
	protected $m_strInstitutionCountryCode;
	protected $m_strContactName;
	protected $m_strContactPhoneNumber;
	protected $m_strContactPhoneExtension;
	protected $m_strContactFaxNumber;
	protected $m_strContactEmail;
	protected $m_fltAmount;
	protected $m_strExpenseEffectiveDate;
	protected $m_strDateEnded;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	private $m_arrstrPostalAddressFields;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_arrstrPostalAddressFields = [
			 'default' => [
				'm_strInstitutionStreetLine1' => 'addressLine1',
				'm_strInstitutionStreetLine2' => 'addressLine2',
				'm_strInstitutionStreetLine3' => 'addressLine3',
				'm_strInstitutionCity' => 'locality',
				'm_strInstitutionStateCode' => 'administrativeArea',
				'm_strInstitutionPostalCode' => 'postalCode',
				'm_strInstitutionCountryCode' => 'country'
			]
		 ];

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['expense_type_id'] ) && $boolDirectSet ) $this->set( 'm_intExpenseTypeId', trim( $arrValues['expense_type_id'] ) ); elseif( isset( $arrValues['expense_type_id'] ) ) $this->setExpenseTypeId( $arrValues['expense_type_id'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['enabled_customer_income_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintEnabledCustomerIncomeIds', trim( $arrValues['enabled_customer_income_ids'] ) ); elseif( isset( $arrValues['enabled_customer_income_ids'] ) ) $this->setEnabledCustomerIncomeIds( $arrValues['enabled_customer_income_ids'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['institution_name'] ) && $boolDirectSet ) $this->set( 'm_strInstitutionName', trim( stripcslashes( $arrValues['institution_name'] ) ) ); elseif( isset( $arrValues['institution_name'] ) ) $this->setInstitutionName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_name'] ) : $arrValues['institution_name'] );
		if( isset( $arrValues['institution_street_line1'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInstitutionStreetLine1', trim( stripcslashes( $arrValues['institution_street_line1'] ) ) ); elseif( isset( $arrValues['institution_street_line1'] ) ) $this->setInstitutionStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_street_line1'] ) : $arrValues['institution_street_line1'] );
		if( isset( $arrValues['institution_street_line2'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInstitutionStreetLine2', trim( stripcslashes( $arrValues['institution_street_line2'] ) ) ); elseif( isset( $arrValues['institution_street_line2'] ) ) $this->setInstitutionStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_street_line2'] ) : $arrValues['institution_street_line2'] );
		if( isset( $arrValues['institution_street_line3'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInstitutionStreetLine3', trim( stripcslashes( $arrValues['institution_street_line3'] ) ) ); elseif( isset( $arrValues['institution_street_line3'] ) ) $this->setInstitutionStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_street_line3'] ) : $arrValues['institution_street_line3'] );
		if( isset( $arrValues['institution_city'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInstitutionCity', trim( stripcslashes( $arrValues['institution_city'] ) ) ); elseif( isset( $arrValues['institution_city'] ) ) $this->setInstitutionCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_city'] ) : $arrValues['institution_city'] );
		if( isset( $arrValues['institution_state_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInstitutionStateCode', trim( stripcslashes( $arrValues['institution_state_code'] ) ) ); elseif( isset( $arrValues['institution_state_code'] ) ) $this->setInstitutionStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_state_code'] ) : $arrValues['institution_state_code'] );
		if( isset( $arrValues['institution_province'] ) && $boolDirectSet ) $this->set( 'm_strInstitutionProvince', trim( stripcslashes( $arrValues['institution_province'] ) ) ); elseif( isset( $arrValues['institution_province'] ) ) $this->setInstitutionProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_province'] ) : $arrValues['institution_province'] );
		if( isset( $arrValues['institution_postal_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInstitutionPostalCode', trim( stripcslashes( $arrValues['institution_postal_code'] ) ) ); elseif( isset( $arrValues['institution_postal_code'] ) ) $this->setInstitutionPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_postal_code'] ) : $arrValues['institution_postal_code'] );
		if( isset( $arrValues['institution_country_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strInstitutionCountryCode', trim( stripcslashes( $arrValues['institution_country_code'] ) ) ); elseif( isset( $arrValues['institution_country_code'] ) ) $this->setInstitutionCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['institution_country_code'] ) : $arrValues['institution_country_code'] );
		if( isset( $arrValues['contact_name'] ) && $boolDirectSet ) $this->set( 'm_strContactName', trim( stripcslashes( $arrValues['contact_name'] ) ) ); elseif( isset( $arrValues['contact_name'] ) ) $this->setContactName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_name'] ) : $arrValues['contact_name'] );
		if( isset( $arrValues['contact_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strContactPhoneNumber', trim( stripcslashes( $arrValues['contact_phone_number'] ) ) ); elseif( isset( $arrValues['contact_phone_number'] ) ) $this->setContactPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_phone_number'] ) : $arrValues['contact_phone_number'] );
		if( isset( $arrValues['contact_phone_extension'] ) && $boolDirectSet ) $this->set( 'm_strContactPhoneExtension', trim( stripcslashes( $arrValues['contact_phone_extension'] ) ) ); elseif( isset( $arrValues['contact_phone_extension'] ) ) $this->setContactPhoneExtension( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_phone_extension'] ) : $arrValues['contact_phone_extension'] );
		if( isset( $arrValues['contact_fax_number'] ) && $boolDirectSet ) $this->set( 'm_strContactFaxNumber', trim( stripcslashes( $arrValues['contact_fax_number'] ) ) ); elseif( isset( $arrValues['contact_fax_number'] ) ) $this->setContactFaxNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_fax_number'] ) : $arrValues['contact_fax_number'] );
		if( isset( $arrValues['contact_email'] ) && $boolDirectSet ) $this->set( 'm_strContactEmail', trim( stripcslashes( $arrValues['contact_email'] ) ) ); elseif( isset( $arrValues['contact_email'] ) ) $this->setContactEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_email'] ) : $arrValues['contact_email'] );
		if( isset( $arrValues['amount'] ) && $boolDirectSet ) $this->set( 'm_fltAmount', trim( $arrValues['amount'] ) ); elseif( isset( $arrValues['amount'] ) ) $this->setAmount( $arrValues['amount'] );
		if( isset( $arrValues['expense_effective_date'] ) && $boolDirectSet ) $this->set( 'm_strExpenseEffectiveDate', trim( $arrValues['expense_effective_date'] ) ); elseif( isset( $arrValues['expense_effective_date'] ) ) $this->setExpenseEffectiveDate( $arrValues['expense_effective_date'] );
		if( isset( $arrValues['date_ended'] ) && $boolDirectSet ) $this->set( 'm_strDateEnded', trim( $arrValues['date_ended'] ) ); elseif( isset( $arrValues['date_ended'] ) ) $this->setDateEnded( $arrValues['date_ended'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( $this->m_boolInitialized ) {
			$this->setPostalAddressValues( $arrValues, $this->m_arrstrPostalAddressFields );
		}

		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setExpenseTypeId( $intExpenseTypeId ) {
		$this->set( 'm_intExpenseTypeId', CStrings::strToIntDef( $intExpenseTypeId, NULL, false ) );
	}

	public function getExpenseTypeId() {
		return $this->m_intExpenseTypeId;
	}

	public function sqlExpenseTypeId() {
		return ( true == isset( $this->m_intExpenseTypeId ) ) ? ( string ) $this->m_intExpenseTypeId : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : 'NULL';
	}

	public function setEnabledCustomerIncomeIds( $arrintEnabledCustomerIncomeIds ) {
		$this->set( 'm_arrintEnabledCustomerIncomeIds', CStrings::strToArrIntDef( $arrintEnabledCustomerIncomeIds, NULL ) );
	}

	public function getEnabledCustomerIncomeIds() {
		return $this->m_arrintEnabledCustomerIncomeIds;
	}

	public function sqlEnabledCustomerIncomeIds() {
		return ( true == isset( $this->m_arrintEnabledCustomerIncomeIds ) && true == valArr( $this->m_arrintEnabledCustomerIncomeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintEnabledCustomerIncomeIds, NULL ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setInstitutionName( $strInstitutionName ) {
		$this->set( 'm_strInstitutionName', CStrings::strTrimDef( $strInstitutionName, 100, NULL, true ) );
	}

	public function getInstitutionName() {
		return $this->m_strInstitutionName;
	}

	public function sqlInstitutionName() {
		return ( true == isset( $this->m_strInstitutionName ) ) ? '\'' . addslashes( $this->m_strInstitutionName ) . '\'' : 'NULL';
	}

	public function setInstitutionStreetLine1( $strInstitutionStreetLine1 ) {
		$this->setPostalAddressField( 'addressLine1', $strInstitutionStreetLine1, $strAddressKey = 'default', 'm_strInstitutionStreetLine1' );
	}

	public function getInstitutionStreetLine1() {
		return $this->getPostalAddressField( 'addressLine1', 'default', 'm_strInstitutionStreetLine1' );
	}

	public function sqlInstitutionStreetLine1() {
		return ( true == isset( $this->m_strInstitutionStreetLine1 ) ) ? '\'' . addslashes( $this->m_strInstitutionStreetLine1 ) . '\'' : 'NULL';
	}

	public function setInstitutionStreetLine2( $strInstitutionStreetLine2 ) {
		$this->setPostalAddressField( 'addressLine2', $strInstitutionStreetLine2, $strAddressKey = 'default', 'm_strInstitutionStreetLine2' );
	}

	public function getInstitutionStreetLine2() {
		return $this->getPostalAddressField( 'addressLine2', 'default', 'm_strInstitutionStreetLine2' );
	}

	public function sqlInstitutionStreetLine2() {
		return ( true == isset( $this->m_strInstitutionStreetLine2 ) ) ? '\'' . addslashes( $this->m_strInstitutionStreetLine2 ) . '\'' : 'NULL';
	}

	public function setInstitutionStreetLine3( $strInstitutionStreetLine3 ) {
		$this->setPostalAddressField( 'addressLine3', $strInstitutionStreetLine3, $strAddressKey = 'default', 'm_strInstitutionStreetLine3' );
	}

	public function getInstitutionStreetLine3() {
		return $this->getPostalAddressField( 'addressLine3', 'default', 'm_strInstitutionStreetLine3' );
	}

	public function sqlInstitutionStreetLine3() {
		return ( true == isset( $this->m_strInstitutionStreetLine3 ) ) ? '\'' . addslashes( $this->m_strInstitutionStreetLine3 ) . '\'' : 'NULL';
	}

	public function setInstitutionCity( $strInstitutionCity ) {
		$this->setPostalAddressField( 'locality', $strInstitutionCity, $strAddressKey = 'default', 'm_strInstitutionCity' );
	}

	public function getInstitutionCity() {
		return $this->getPostalAddressField( 'locality', 'default', 'm_strInstitutionCity' );
	}

	public function sqlInstitutionCity() {
		return ( true == isset( $this->m_strInstitutionCity ) ) ? '\'' . addslashes( $this->m_strInstitutionCity ) . '\'' : 'NULL';
	}

	public function setInstitutionStateCode( $strInstitutionStateCode ) {
		$this->setPostalAddressField( 'administrativeArea', $strInstitutionStateCode, $strAddressKey = 'default', 'm_strInstitutionStateCode' );
	}

	public function getInstitutionStateCode() {
		return $this->getPostalAddressField( 'administrativeArea', 'default', 'm_strInstitutionStateCode' );
	}

	public function sqlInstitutionStateCode() {
		return ( true == isset( $this->m_strInstitutionStateCode ) ) ? '\'' . addslashes( $this->m_strInstitutionStateCode ) . '\'' : 'NULL';
	}

	public function setInstitutionProvince( $strInstitutionProvince ) {
		$this->set( 'm_strInstitutionProvince', CStrings::strTrimDef( $strInstitutionProvince, 50, NULL, true ) );
	}

	public function getInstitutionProvince() {
		return $this->m_strInstitutionProvince;
	}

	public function sqlInstitutionProvince() {
		return ( true == isset( $this->m_strInstitutionProvince ) ) ? '\'' . addslashes( $this->m_strInstitutionProvince ) . '\'' : 'NULL';
	}

	public function setInstitutionPostalCode( $strInstitutionPostalCode ) {
		$this->setPostalAddressField( 'postalCode', $strInstitutionPostalCode, $strAddressKey = 'default', 'm_strInstitutionPostalCode' );
	}

	public function getInstitutionPostalCode() {
		return $this->getPostalAddressField( 'postalCode', 'default', 'm_strInstitutionPostalCode' );
	}

	public function sqlInstitutionPostalCode() {
		return ( true == isset( $this->m_strInstitutionPostalCode ) ) ? '\'' . addslashes( $this->m_strInstitutionPostalCode ) . '\'' : 'NULL';
	}

	public function setInstitutionCountryCode( $strInstitutionCountryCode ) {
		$this->setPostalAddressField( 'country', $strInstitutionCountryCode, $strAddressKey = 'default', 'm_strInstitutionCountryCode' );
	}

	public function getInstitutionCountryCode() {
		return $this->getPostalAddressField( 'country', 'default', 'm_strInstitutionCountryCode' );
	}

	public function sqlInstitutionCountryCode() {
		return ( true == isset( $this->m_strInstitutionCountryCode ) ) ? '\'' . addslashes( $this->m_strInstitutionCountryCode ) . '\'' : 'NULL';
	}

	public function setContactName( $strContactName ) {
		$this->set( 'm_strContactName', CStrings::strTrimDef( $strContactName, 100, NULL, true ) );
	}

	public function getContactName() {
		return $this->m_strContactName;
	}

	public function sqlContactName() {
		return ( true == isset( $this->m_strContactName ) ) ? '\'' . addslashes( $this->m_strContactName ) . '\'' : 'NULL';
	}

	public function setContactPhoneNumber( $strContactPhoneNumber ) {
		$this->set( 'm_strContactPhoneNumber', CStrings::strTrimDef( $strContactPhoneNumber, 30, NULL, true ) );
	}

	public function getContactPhoneNumber() {
		return $this->m_strContactPhoneNumber;
	}

	public function sqlContactPhoneNumber() {
		return ( true == isset( $this->m_strContactPhoneNumber ) ) ? '\'' . addslashes( $this->m_strContactPhoneNumber ) . '\'' : 'NULL';
	}

	public function setContactPhoneExtension( $strContactPhoneExtension ) {
		$this->set( 'm_strContactPhoneExtension', CStrings::strTrimDef( $strContactPhoneExtension, 6, NULL, true ) );
	}

	public function getContactPhoneExtension() {
		return $this->m_strContactPhoneExtension;
	}

	public function sqlContactPhoneExtension() {
		return ( true == isset( $this->m_strContactPhoneExtension ) ) ? '\'' . addslashes( $this->m_strContactPhoneExtension ) . '\'' : 'NULL';
	}

	public function setContactFaxNumber( $strContactFaxNumber ) {
		$this->set( 'm_strContactFaxNumber', CStrings::strTrimDef( $strContactFaxNumber, 30, NULL, true ) );
	}

	public function getContactFaxNumber() {
		return $this->m_strContactFaxNumber;
	}

	public function sqlContactFaxNumber() {
		return ( true == isset( $this->m_strContactFaxNumber ) ) ? '\'' . addslashes( $this->m_strContactFaxNumber ) . '\'' : 'NULL';
	}

	public function setContactEmail( $strContactEmail ) {
		$this->set( 'm_strContactEmail', CStrings::strTrimDef( $strContactEmail, 240, NULL, true ) );
	}

	public function getContactEmail() {
		return $this->m_strContactEmail;
	}

	public function sqlContactEmail() {
		return ( true == isset( $this->m_strContactEmail ) ) ? '\'' . addslashes( $this->m_strContactEmail ) . '\'' : 'NULL';
	}

	public function setAmount( $fltAmount ) {
		$this->set( 'm_fltAmount', CStrings::strToFloatDef( $fltAmount, NULL, false, 4 ) );
	}

	public function getAmount() {
		return $this->m_fltAmount;
	}

	public function sqlAmount() {
		return ( true == isset( $this->m_fltAmount ) ) ? ( string ) $this->m_fltAmount : 'NULL';
	}

	public function setExpenseEffectiveDate( $strExpenseEffectiveDate ) {
		$this->set( 'm_strExpenseEffectiveDate', CStrings::strTrimDef( $strExpenseEffectiveDate, -1, NULL, true ) );
	}

	public function getExpenseEffectiveDate() {
		return $this->m_strExpenseEffectiveDate;
	}

	public function sqlExpenseEffectiveDate() {
		return ( true == isset( $this->m_strExpenseEffectiveDate ) ) ? '\'' . $this->m_strExpenseEffectiveDate . '\'' : 'NOW()';
	}

	public function setDateEnded( $strDateEnded ) {
		$this->set( 'm_strDateEnded', CStrings::strTrimDef( $strDateEnded, -1, NULL, true ) );
	}

	public function getDateEnded() {
		return $this->m_strDateEnded;
	}

	public function sqlDateEnded() {
		return ( true == isset( $this->m_strDateEnded ) ) ? '\'' . $this->m_strDateEnded . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_id, expense_type_id, frequency_id, enabled_customer_income_ids, description, details, institution_name, institution_street_line1, institution_street_line2, institution_street_line3, institution_city, institution_state_code, institution_province, institution_postal_code, institution_country_code, contact_name, contact_phone_number, contact_phone_extension, contact_fax_number, contact_email, amount, expense_effective_date, date_ended, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlExpenseTypeId() . ', ' .
						$this->sqlFrequencyId() . ', ' .
						$this->sqlEnabledCustomerIncomeIds() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlInstitutionName() . ', ' .
						$this->sqlInstitutionStreetLine1() . ', ' .
						$this->sqlInstitutionStreetLine2() . ', ' .
						$this->sqlInstitutionStreetLine3() . ', ' .
						$this->sqlInstitutionCity() . ', ' .
						$this->sqlInstitutionStateCode() . ', ' .
						$this->sqlInstitutionProvince() . ', ' .
						$this->sqlInstitutionPostalCode() . ', ' .
						$this->sqlInstitutionCountryCode() . ', ' .
						$this->sqlContactName() . ', ' .
						$this->sqlContactPhoneNumber() . ', ' .
						$this->sqlContactPhoneExtension() . ', ' .
						$this->sqlContactFaxNumber() . ', ' .
						$this->sqlContactEmail() . ', ' .
						$this->sqlAmount() . ', ' .
						$this->sqlExpenseEffectiveDate() . ', ' .
						$this->sqlDateEnded() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expense_type_id = ' . $this->sqlExpenseTypeId(). ',' ; } elseif( true == array_key_exists( 'ExpenseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' expense_type_id = ' . $this->sqlExpenseTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId(). ',' ; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' enabled_customer_income_ids = ' . $this->sqlEnabledCustomerIncomeIds(). ',' ; } elseif( true == array_key_exists( 'EnabledCustomerIncomeIds', $this->getChangedColumns() ) ) { $strSql .= ' enabled_customer_income_ids = ' . $this->sqlEnabledCustomerIncomeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_name = ' . $this->sqlInstitutionName(). ',' ; } elseif( true == array_key_exists( 'InstitutionName', $this->getChangedColumns() ) ) { $strSql .= ' institution_name = ' . $this->sqlInstitutionName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_street_line1 = ' . $this->sqlInstitutionStreetLine1(). ',' ; } elseif( true == array_key_exists( 'InstitutionStreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' institution_street_line1 = ' . $this->sqlInstitutionStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_street_line2 = ' . $this->sqlInstitutionStreetLine2(). ',' ; } elseif( true == array_key_exists( 'InstitutionStreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' institution_street_line2 = ' . $this->sqlInstitutionStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_street_line3 = ' . $this->sqlInstitutionStreetLine3(). ',' ; } elseif( true == array_key_exists( 'InstitutionStreetLine3', $this->getChangedColumns() ) ) { $strSql .= ' institution_street_line3 = ' . $this->sqlInstitutionStreetLine3() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_city = ' . $this->sqlInstitutionCity(). ',' ; } elseif( true == array_key_exists( 'InstitutionCity', $this->getChangedColumns() ) ) { $strSql .= ' institution_city = ' . $this->sqlInstitutionCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_state_code = ' . $this->sqlInstitutionStateCode(). ',' ; } elseif( true == array_key_exists( 'InstitutionStateCode', $this->getChangedColumns() ) ) { $strSql .= ' institution_state_code = ' . $this->sqlInstitutionStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_province = ' . $this->sqlInstitutionProvince(). ',' ; } elseif( true == array_key_exists( 'InstitutionProvince', $this->getChangedColumns() ) ) { $strSql .= ' institution_province = ' . $this->sqlInstitutionProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_postal_code = ' . $this->sqlInstitutionPostalCode(). ',' ; } elseif( true == array_key_exists( 'InstitutionPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' institution_postal_code = ' . $this->sqlInstitutionPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' institution_country_code = ' . $this->sqlInstitutionCountryCode(). ',' ; } elseif( true == array_key_exists( 'InstitutionCountryCode', $this->getChangedColumns() ) ) { $strSql .= ' institution_country_code = ' . $this->sqlInstitutionCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_name = ' . $this->sqlContactName(). ',' ; } elseif( true == array_key_exists( 'ContactName', $this->getChangedColumns() ) ) { $strSql .= ' contact_name = ' . $this->sqlContactName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_phone_number = ' . $this->sqlContactPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'ContactPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' contact_phone_number = ' . $this->sqlContactPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_phone_extension = ' . $this->sqlContactPhoneExtension(). ',' ; } elseif( true == array_key_exists( 'ContactPhoneExtension', $this->getChangedColumns() ) ) { $strSql .= ' contact_phone_extension = ' . $this->sqlContactPhoneExtension() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_fax_number = ' . $this->sqlContactFaxNumber(). ',' ; } elseif( true == array_key_exists( 'ContactFaxNumber', $this->getChangedColumns() ) ) { $strSql .= ' contact_fax_number = ' . $this->sqlContactFaxNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_email = ' . $this->sqlContactEmail(). ',' ; } elseif( true == array_key_exists( 'ContactEmail', $this->getChangedColumns() ) ) { $strSql .= ' contact_email = ' . $this->sqlContactEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount = ' . $this->sqlAmount(). ',' ; } elseif( true == array_key_exists( 'Amount', $this->getChangedColumns() ) ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expense_effective_date = ' . $this->sqlExpenseEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'ExpenseEffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' expense_effective_date = ' . $this->sqlExpenseEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_ended = ' . $this->sqlDateEnded(). ',' ; } elseif( true == array_key_exists( 'DateEnded', $this->getChangedColumns() ) ) { $strSql .= ' date_ended = ' . $this->sqlDateEnded() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_id' => $this->getCustomerId(),
			'expense_type_id' => $this->getExpenseTypeId(),
			'frequency_id' => $this->getFrequencyId(),
			'enabled_customer_income_ids' => $this->getEnabledCustomerIncomeIds(),
			'description' => $this->getDescription(),
			'details' => $this->getDetails(),
			'institution_name' => $this->getInstitutionName(),
			'institution_street_line1' => $this->getInstitutionStreetLine1(),
			'institution_street_line2' => $this->getInstitutionStreetLine2(),
			'institution_street_line3' => $this->getInstitutionStreetLine3(),
			'institution_city' => $this->getInstitutionCity(),
			'institution_state_code' => $this->getInstitutionStateCode(),
			'institution_province' => $this->getInstitutionProvince(),
			'institution_postal_code' => $this->getInstitutionPostalCode(),
			'institution_country_code' => $this->getInstitutionCountryCode(),
			'contact_name' => $this->getContactName(),
			'contact_phone_number' => $this->getContactPhoneNumber(),
			'contact_phone_extension' => $this->getContactPhoneExtension(),
			'contact_fax_number' => $this->getContactFaxNumber(),
			'contact_email' => $this->getContactEmail(),
			'amount' => $this->getAmount(),
			'expense_effective_date' => $this->getExpenseEffectiveDate(),
			'date_ended' => $this->getDateEnded(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>