<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CEntrataTabletModules
 * Do not add any new functions to this class.
 */

class CBaseEntrataTabletModules extends CEosPluralBase {

	/**
	 * @return CEntrataTabletModule[]
	 */
	public static function fetchEntrataTabletModules( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEntrataTabletModule::class, $objDatabase );
	}

	/**
	 * @return CEntrataTabletModule
	 */
	public static function fetchEntrataTabletModule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEntrataTabletModule::class, $objDatabase );
	}

	public static function fetchEntrataTabletModuleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'entrata_tablet_modules', $objDatabase );
	}

	public static function fetchEntrataTabletModuleById( $intId, $objDatabase ) {
		return self::fetchEntrataTabletModule( sprintf( 'SELECT * FROM entrata_tablet_modules WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchEntrataTabletModulesByEntrataTabletModuleId( $intEntrataTabletModuleId, $objDatabase ) {
		return self::fetchEntrataTabletModules( sprintf( 'SELECT * FROM entrata_tablet_modules WHERE entrata_tablet_module_id = %d', $intEntrataTabletModuleId ), $objDatabase );
	}

	public static function fetchEntrataTabletModulesByModuleId( $intModuleId, $objDatabase ) {
		return self::fetchEntrataTabletModules( sprintf( 'SELECT * FROM entrata_tablet_modules WHERE module_id = %d', $intModuleId ), $objDatabase );
	}

	public static function fetchEntrataTabletModulesByEntrataTabletModuleTypeId( $intEntrataTabletModuleTypeId, $objDatabase ) {
		return self::fetchEntrataTabletModules( sprintf( 'SELECT * FROM entrata_tablet_modules WHERE entrata_tablet_module_type_id = %d', $intEntrataTabletModuleTypeId ), $objDatabase );
	}

}
?>