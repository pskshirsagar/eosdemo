<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyWebsiteStat extends CEosSingularBase {

	const TABLE_NAME = 'public.property_website_stats';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_strStatDate;
	protected $m_intStandardUniques;
	protected $m_intStandardViews;
	protected $m_intMobileUniques;
	protected $m_intMobileViews;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intStandardUniques = '0';
		$this->m_intStandardViews = '0';
		$this->m_intMobileUniques = '0';
		$this->m_intMobileViews = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['stat_date'] ) && $boolDirectSet ) $this->set( 'm_strStatDate', trim( $arrValues['stat_date'] ) ); elseif( isset( $arrValues['stat_date'] ) ) $this->setStatDate( $arrValues['stat_date'] );
		if( isset( $arrValues['standard_uniques'] ) && $boolDirectSet ) $this->set( 'm_intStandardUniques', trim( $arrValues['standard_uniques'] ) ); elseif( isset( $arrValues['standard_uniques'] ) ) $this->setStandardUniques( $arrValues['standard_uniques'] );
		if( isset( $arrValues['standard_views'] ) && $boolDirectSet ) $this->set( 'm_intStandardViews', trim( $arrValues['standard_views'] ) ); elseif( isset( $arrValues['standard_views'] ) ) $this->setStandardViews( $arrValues['standard_views'] );
		if( isset( $arrValues['mobile_uniques'] ) && $boolDirectSet ) $this->set( 'm_intMobileUniques', trim( $arrValues['mobile_uniques'] ) ); elseif( isset( $arrValues['mobile_uniques'] ) ) $this->setMobileUniques( $arrValues['mobile_uniques'] );
		if( isset( $arrValues['mobile_views'] ) && $boolDirectSet ) $this->set( 'm_intMobileViews', trim( $arrValues['mobile_views'] ) ); elseif( isset( $arrValues['mobile_views'] ) ) $this->setMobileViews( $arrValues['mobile_views'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setStatDate( $strStatDate ) {
		$this->set( 'm_strStatDate', CStrings::strTrimDef( $strStatDate, -1, NULL, true ) );
	}

	public function getStatDate() {
		return $this->m_strStatDate;
	}

	public function sqlStatDate() {
		return ( true == isset( $this->m_strStatDate ) ) ? '\'' . $this->m_strStatDate . '\'' : 'NOW()';
	}

	public function setStandardUniques( $intStandardUniques ) {
		$this->set( 'm_intStandardUniques', CStrings::strToIntDef( $intStandardUniques, NULL, false ) );
	}

	public function getStandardUniques() {
		return $this->m_intStandardUniques;
	}

	public function sqlStandardUniques() {
		return ( true == isset( $this->m_intStandardUniques ) ) ? ( string ) $this->m_intStandardUniques : '0';
	}

	public function setStandardViews( $intStandardViews ) {
		$this->set( 'm_intStandardViews', CStrings::strToIntDef( $intStandardViews, NULL, false ) );
	}

	public function getStandardViews() {
		return $this->m_intStandardViews;
	}

	public function sqlStandardViews() {
		return ( true == isset( $this->m_intStandardViews ) ) ? ( string ) $this->m_intStandardViews : '0';
	}

	public function setMobileUniques( $intMobileUniques ) {
		$this->set( 'm_intMobileUniques', CStrings::strToIntDef( $intMobileUniques, NULL, false ) );
	}

	public function getMobileUniques() {
		return $this->m_intMobileUniques;
	}

	public function sqlMobileUniques() {
		return ( true == isset( $this->m_intMobileUniques ) ) ? ( string ) $this->m_intMobileUniques : '0';
	}

	public function setMobileViews( $intMobileViews ) {
		$this->set( 'm_intMobileViews', CStrings::strToIntDef( $intMobileViews, NULL, false ) );
	}

	public function getMobileViews() {
		return $this->m_intMobileViews;
	}

	public function sqlMobileViews() {
		return ( true == isset( $this->m_intMobileViews ) ) ? ( string ) $this->m_intMobileViews : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, stat_date, standard_uniques, standard_views, mobile_uniques, mobile_views, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlStatDate() . ', ' .
 						$this->sqlStandardUniques() . ', ' .
 						$this->sqlStandardViews() . ', ' .
 						$this->sqlMobileUniques() . ', ' .
 						$this->sqlMobileViews() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' stat_date = ' . $this->sqlStatDate() . ','; } elseif( true == array_key_exists( 'StatDate', $this->getChangedColumns() ) ) { $strSql .= ' stat_date = ' . $this->sqlStatDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' standard_uniques = ' . $this->sqlStandardUniques() . ','; } elseif( true == array_key_exists( 'StandardUniques', $this->getChangedColumns() ) ) { $strSql .= ' standard_uniques = ' . $this->sqlStandardUniques() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' standard_views = ' . $this->sqlStandardViews() . ','; } elseif( true == array_key_exists( 'StandardViews', $this->getChangedColumns() ) ) { $strSql .= ' standard_views = ' . $this->sqlStandardViews() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mobile_uniques = ' . $this->sqlMobileUniques() . ','; } elseif( true == array_key_exists( 'MobileUniques', $this->getChangedColumns() ) ) { $strSql .= ' mobile_uniques = ' . $this->sqlMobileUniques() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mobile_views = ' . $this->sqlMobileViews() . ','; } elseif( true == array_key_exists( 'MobileViews', $this->getChangedColumns() ) ) { $strSql .= ' mobile_views = ' . $this->sqlMobileViews() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'stat_date' => $this->getStatDate(),
			'standard_uniques' => $this->getStandardUniques(),
			'standard_views' => $this->getStandardViews(),
			'mobile_uniques' => $this->getMobileUniques(),
			'mobile_views' => $this->getMobileViews(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>