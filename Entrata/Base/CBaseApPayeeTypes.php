<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeTypes
 * Do not add any new functions to this class.
 */

class CBaseApPayeeTypes extends CEosPluralBase {

	/**
	 * @return CApPayeeType[]
	 */
	public static function fetchApPayeeTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CApPayeeType::class, $objDatabase );
	}

	/**
	 * @return CApPayeeType
	 */
	public static function fetchApPayeeType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApPayeeType::class, $objDatabase );
	}

	public static function fetchApPayeeTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_payee_types', $objDatabase );
	}

	public static function fetchApPayeeTypeById( $intId, $objDatabase ) {
		return self::fetchApPayeeType( sprintf( 'SELECT * FROM ap_payee_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>