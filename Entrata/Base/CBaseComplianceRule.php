<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseComplianceRule extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.compliance_rules';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intComplianceRulesetId;
	protected $m_intComplianceTypeId;
	protected $m_intComplianceLevelId;
	protected $m_intComplianceItemId;
	protected $m_intScreeningPackageId;
	protected $m_strName;
	protected $m_fltRequiredAmount;
	protected $m_intScreeningFrequencyMonths;
	protected $m_boolValidateExpiration;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_fltRequiredAmount = '0';
		$this->m_intScreeningFrequencyMonths = '12';
		$this->m_boolValidateExpiration = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['compliance_ruleset_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceRulesetId', trim( $arrValues['compliance_ruleset_id'] ) ); elseif( isset( $arrValues['compliance_ruleset_id'] ) ) $this->setComplianceRulesetId( $arrValues['compliance_ruleset_id'] );
		if( isset( $arrValues['compliance_type_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceTypeId', trim( $arrValues['compliance_type_id'] ) ); elseif( isset( $arrValues['compliance_type_id'] ) ) $this->setComplianceTypeId( $arrValues['compliance_type_id'] );
		if( isset( $arrValues['compliance_level_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceLevelId', trim( $arrValues['compliance_level_id'] ) ); elseif( isset( $arrValues['compliance_level_id'] ) ) $this->setComplianceLevelId( $arrValues['compliance_level_id'] );
		if( isset( $arrValues['compliance_item_id'] ) && $boolDirectSet ) $this->set( 'm_intComplianceItemId', trim( $arrValues['compliance_item_id'] ) ); elseif( isset( $arrValues['compliance_item_id'] ) ) $this->setComplianceItemId( $arrValues['compliance_item_id'] );
		if( isset( $arrValues['screening_package_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningPackageId', trim( $arrValues['screening_package_id'] ) ); elseif( isset( $arrValues['screening_package_id'] ) ) $this->setScreeningPackageId( $arrValues['screening_package_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['required_amount'] ) && $boolDirectSet ) $this->set( 'm_fltRequiredAmount', trim( $arrValues['required_amount'] ) ); elseif( isset( $arrValues['required_amount'] ) ) $this->setRequiredAmount( $arrValues['required_amount'] );
		if( isset( $arrValues['screening_frequency_months'] ) && $boolDirectSet ) $this->set( 'm_intScreeningFrequencyMonths', trim( $arrValues['screening_frequency_months'] ) ); elseif( isset( $arrValues['screening_frequency_months'] ) ) $this->setScreeningFrequencyMonths( $arrValues['screening_frequency_months'] );
		if( isset( $arrValues['validate_expiration'] ) && $boolDirectSet ) $this->set( 'm_boolValidateExpiration', trim( stripcslashes( $arrValues['validate_expiration'] ) ) ); elseif( isset( $arrValues['validate_expiration'] ) ) $this->setValidateExpiration( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['validate_expiration'] ) : $arrValues['validate_expiration'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setComplianceRulesetId( $intComplianceRulesetId ) {
		$this->set( 'm_intComplianceRulesetId', CStrings::strToIntDef( $intComplianceRulesetId, NULL, false ) );
	}

	public function getComplianceRulesetId() {
		return $this->m_intComplianceRulesetId;
	}

	public function sqlComplianceRulesetId() {
		return ( true == isset( $this->m_intComplianceRulesetId ) ) ? ( string ) $this->m_intComplianceRulesetId : 'NULL';
	}

	public function setComplianceTypeId( $intComplianceTypeId ) {
		$this->set( 'm_intComplianceTypeId', CStrings::strToIntDef( $intComplianceTypeId, NULL, false ) );
	}

	public function getComplianceTypeId() {
		return $this->m_intComplianceTypeId;
	}

	public function sqlComplianceTypeId() {
		return ( true == isset( $this->m_intComplianceTypeId ) ) ? ( string ) $this->m_intComplianceTypeId : 'NULL';
	}

	public function setComplianceLevelId( $intComplianceLevelId ) {
		$this->set( 'm_intComplianceLevelId', CStrings::strToIntDef( $intComplianceLevelId, NULL, false ) );
	}

	public function getComplianceLevelId() {
		return $this->m_intComplianceLevelId;
	}

	public function sqlComplianceLevelId() {
		return ( true == isset( $this->m_intComplianceLevelId ) ) ? ( string ) $this->m_intComplianceLevelId : 'NULL';
	}

	public function setComplianceItemId( $intComplianceItemId ) {
		$this->set( 'm_intComplianceItemId', CStrings::strToIntDef( $intComplianceItemId, NULL, false ) );
	}

	public function getComplianceItemId() {
		return $this->m_intComplianceItemId;
	}

	public function sqlComplianceItemId() {
		return ( true == isset( $this->m_intComplianceItemId ) ) ? ( string ) $this->m_intComplianceItemId : 'NULL';
	}

	public function setScreeningPackageId( $intScreeningPackageId ) {
		$this->set( 'm_intScreeningPackageId', CStrings::strToIntDef( $intScreeningPackageId, NULL, false ) );
	}

	public function getScreeningPackageId() {
		return $this->m_intScreeningPackageId;
	}

	public function sqlScreeningPackageId() {
		return ( true == isset( $this->m_intScreeningPackageId ) ) ? ( string ) $this->m_intScreeningPackageId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 2000, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setRequiredAmount( $fltRequiredAmount ) {
		$this->set( 'm_fltRequiredAmount', CStrings::strToFloatDef( $fltRequiredAmount, NULL, false, 2 ) );
	}

	public function getRequiredAmount() {
		return $this->m_fltRequiredAmount;
	}

	public function sqlRequiredAmount() {
		return ( true == isset( $this->m_fltRequiredAmount ) ) ? ( string ) $this->m_fltRequiredAmount : '0';
	}

	public function setScreeningFrequencyMonths( $intScreeningFrequencyMonths ) {
		$this->set( 'm_intScreeningFrequencyMonths', CStrings::strToIntDef( $intScreeningFrequencyMonths, NULL, false ) );
	}

	public function getScreeningFrequencyMonths() {
		return $this->m_intScreeningFrequencyMonths;
	}

	public function sqlScreeningFrequencyMonths() {
		return ( true == isset( $this->m_intScreeningFrequencyMonths ) ) ? ( string ) $this->m_intScreeningFrequencyMonths : '12';
	}

	public function setValidateExpiration( $boolValidateExpiration ) {
		$this->set( 'm_boolValidateExpiration', CStrings::strToBool( $boolValidateExpiration ) );
	}

	public function getValidateExpiration() {
		return $this->m_boolValidateExpiration;
	}

	public function sqlValidateExpiration() {
		return ( true == isset( $this->m_boolValidateExpiration ) ) ? '\'' . ( true == ( bool ) $this->m_boolValidateExpiration ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, compliance_ruleset_id, compliance_type_id, compliance_level_id, compliance_item_id, screening_package_id, name, required_amount, screening_frequency_months, validate_expiration, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlComplianceRulesetId() . ', ' .
						$this->sqlComplianceTypeId() . ', ' .
						$this->sqlComplianceLevelId() . ', ' .
						$this->sqlComplianceItemId() . ', ' .
						$this->sqlScreeningPackageId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlRequiredAmount() . ', ' .
						$this->sqlScreeningFrequencyMonths() . ', ' .
						$this->sqlValidateExpiration() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_ruleset_id = ' . $this->sqlComplianceRulesetId(). ',' ; } elseif( true == array_key_exists( 'ComplianceRulesetId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_ruleset_id = ' . $this->sqlComplianceRulesetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_type_id = ' . $this->sqlComplianceTypeId(). ',' ; } elseif( true == array_key_exists( 'ComplianceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_type_id = ' . $this->sqlComplianceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_level_id = ' . $this->sqlComplianceLevelId(). ',' ; } elseif( true == array_key_exists( 'ComplianceLevelId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_level_id = ' . $this->sqlComplianceLevelId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' compliance_item_id = ' . $this->sqlComplianceItemId(). ',' ; } elseif( true == array_key_exists( 'ComplianceItemId', $this->getChangedColumns() ) ) { $strSql .= ' compliance_item_id = ' . $this->sqlComplianceItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_package_id = ' . $this->sqlScreeningPackageId(). ',' ; } elseif( true == array_key_exists( 'ScreeningPackageId', $this->getChangedColumns() ) ) { $strSql .= ' screening_package_id = ' . $this->sqlScreeningPackageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' required_amount = ' . $this->sqlRequiredAmount(). ',' ; } elseif( true == array_key_exists( 'RequiredAmount', $this->getChangedColumns() ) ) { $strSql .= ' required_amount = ' . $this->sqlRequiredAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_frequency_months = ' . $this->sqlScreeningFrequencyMonths(). ',' ; } elseif( true == array_key_exists( 'ScreeningFrequencyMonths', $this->getChangedColumns() ) ) { $strSql .= ' screening_frequency_months = ' . $this->sqlScreeningFrequencyMonths() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' validate_expiration = ' . $this->sqlValidateExpiration(). ',' ; } elseif( true == array_key_exists( 'ValidateExpiration', $this->getChangedColumns() ) ) { $strSql .= ' validate_expiration = ' . $this->sqlValidateExpiration() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'compliance_ruleset_id' => $this->getComplianceRulesetId(),
			'compliance_type_id' => $this->getComplianceTypeId(),
			'compliance_level_id' => $this->getComplianceLevelId(),
			'compliance_item_id' => $this->getComplianceItemId(),
			'screening_package_id' => $this->getScreeningPackageId(),
			'name' => $this->getName(),
			'required_amount' => $this->getRequiredAmount(),
			'screening_frequency_months' => $this->getScreeningFrequencyMonths(),
			'validate_expiration' => $this->getValidateExpiration(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>