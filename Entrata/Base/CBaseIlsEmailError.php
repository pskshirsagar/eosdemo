<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseIlsEmailError extends CEosSingularBase {

	const TABLE_NAME = 'public.ils_email_errors';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intIlsEmailId;
	protected $m_intIlsEmailErrorTypeId;
	protected $m_strErrorDate;
	protected $m_strErrorDescription;
	protected $m_intIsFixed;
	protected $m_intIlsEmailReparseLogId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsFixed = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ils_email_id'] ) && $boolDirectSet ) $this->set( 'm_intIlsEmailId', trim( $arrValues['ils_email_id'] ) ); elseif( isset( $arrValues['ils_email_id'] ) ) $this->setIlsEmailId( $arrValues['ils_email_id'] );
		if( isset( $arrValues['ils_email_error_type_id'] ) && $boolDirectSet ) $this->set( 'm_intIlsEmailErrorTypeId', trim( $arrValues['ils_email_error_type_id'] ) ); elseif( isset( $arrValues['ils_email_error_type_id'] ) ) $this->setIlsEmailErrorTypeId( $arrValues['ils_email_error_type_id'] );
		if( isset( $arrValues['error_date'] ) && $boolDirectSet ) $this->set( 'm_strErrorDate', trim( $arrValues['error_date'] ) ); elseif( isset( $arrValues['error_date'] ) ) $this->setErrorDate( $arrValues['error_date'] );
		if( isset( $arrValues['error_description'] ) && $boolDirectSet ) $this->set( 'm_strErrorDescription', trim( stripcslashes( $arrValues['error_description'] ) ) ); elseif( isset( $arrValues['error_description'] ) ) $this->setErrorDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['error_description'] ) : $arrValues['error_description'] );
		if( isset( $arrValues['is_fixed'] ) && $boolDirectSet ) $this->set( 'm_intIsFixed', trim( $arrValues['is_fixed'] ) ); elseif( isset( $arrValues['is_fixed'] ) ) $this->setIsFixed( $arrValues['is_fixed'] );
		if( isset( $arrValues['ils_email_reparse_log_id'] ) && $boolDirectSet ) $this->set( 'm_intIlsEmailReparseLogId', trim( $arrValues['ils_email_reparse_log_id'] ) ); elseif( isset( $arrValues['ils_email_reparse_log_id'] ) ) $this->setIlsEmailReparseLogId( $arrValues['ils_email_reparse_log_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setIlsEmailId( $intIlsEmailId ) {
		$this->set( 'm_intIlsEmailId', CStrings::strToIntDef( $intIlsEmailId, NULL, false ) );
	}

	public function getIlsEmailId() {
		return $this->m_intIlsEmailId;
	}

	public function sqlIlsEmailId() {
		return ( true == isset( $this->m_intIlsEmailId ) ) ? ( string ) $this->m_intIlsEmailId : 'NULL';
	}

	public function setIlsEmailErrorTypeId( $intIlsEmailErrorTypeId ) {
		$this->set( 'm_intIlsEmailErrorTypeId', CStrings::strToIntDef( $intIlsEmailErrorTypeId, NULL, false ) );
	}

	public function getIlsEmailErrorTypeId() {
		return $this->m_intIlsEmailErrorTypeId;
	}

	public function sqlIlsEmailErrorTypeId() {
		return ( true == isset( $this->m_intIlsEmailErrorTypeId ) ) ? ( string ) $this->m_intIlsEmailErrorTypeId : 'NULL';
	}

	public function setErrorDate( $strErrorDate ) {
		$this->set( 'm_strErrorDate', CStrings::strTrimDef( $strErrorDate, -1, NULL, true ) );
	}

	public function getErrorDate() {
		return $this->m_strErrorDate;
	}

	public function sqlErrorDate() {
		return ( true == isset( $this->m_strErrorDate ) ) ? '\'' . $this->m_strErrorDate . '\'' : 'NOW()';
	}

	public function setErrorDescription( $strErrorDescription ) {
		$this->set( 'm_strErrorDescription', CStrings::strTrimDef( $strErrorDescription, 2000, NULL, true ) );
	}

	public function getErrorDescription() {
		return $this->m_strErrorDescription;
	}

	public function sqlErrorDescription() {
		return ( true == isset( $this->m_strErrorDescription ) ) ? '\'' . addslashes( $this->m_strErrorDescription ) . '\'' : 'NULL';
	}

	public function setIsFixed( $intIsFixed ) {
		$this->set( 'm_intIsFixed', CStrings::strToIntDef( $intIsFixed, NULL, false ) );
	}

	public function getIsFixed() {
		return $this->m_intIsFixed;
	}

	public function sqlIsFixed() {
		return ( true == isset( $this->m_intIsFixed ) ) ? ( string ) $this->m_intIsFixed : '0';
	}

	public function setIlsEmailReparseLogId( $intIlsEmailReparseLogId ) {
		$this->set( 'm_intIlsEmailReparseLogId', CStrings::strToIntDef( $intIlsEmailReparseLogId, NULL, false ) );
	}

	public function getIlsEmailReparseLogId() {
		return $this->m_intIlsEmailReparseLogId;
	}

	public function sqlIlsEmailReparseLogId() {
		return ( true == isset( $this->m_intIlsEmailReparseLogId ) ) ? ( string ) $this->m_intIlsEmailReparseLogId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ils_email_id, ils_email_error_type_id, error_date, error_description, is_fixed, ils_email_reparse_log_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlIlsEmailId() . ', ' .
 						$this->sqlIlsEmailErrorTypeId() . ', ' .
 						$this->sqlErrorDate() . ', ' .
 						$this->sqlErrorDescription() . ', ' .
 						$this->sqlIsFixed() . ', ' .
 						$this->sqlIlsEmailReparseLogId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ils_email_id = ' . $this->sqlIlsEmailId() . ','; } elseif( true == array_key_exists( 'IlsEmailId', $this->getChangedColumns() ) ) { $strSql .= ' ils_email_id = ' . $this->sqlIlsEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ils_email_error_type_id = ' . $this->sqlIlsEmailErrorTypeId() . ','; } elseif( true == array_key_exists( 'IlsEmailErrorTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ils_email_error_type_id = ' . $this->sqlIlsEmailErrorTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_date = ' . $this->sqlErrorDate() . ','; } elseif( true == array_key_exists( 'ErrorDate', $this->getChangedColumns() ) ) { $strSql .= ' error_date = ' . $this->sqlErrorDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_description = ' . $this->sqlErrorDescription() . ','; } elseif( true == array_key_exists( 'ErrorDescription', $this->getChangedColumns() ) ) { $strSql .= ' error_description = ' . $this->sqlErrorDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_fixed = ' . $this->sqlIsFixed() . ','; } elseif( true == array_key_exists( 'IsFixed', $this->getChangedColumns() ) ) { $strSql .= ' is_fixed = ' . $this->sqlIsFixed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ils_email_reparse_log_id = ' . $this->sqlIlsEmailReparseLogId() . ','; } elseif( true == array_key_exists( 'IlsEmailReparseLogId', $this->getChangedColumns() ) ) { $strSql .= ' ils_email_reparse_log_id = ' . $this->sqlIlsEmailReparseLogId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ils_email_id' => $this->getIlsEmailId(),
			'ils_email_error_type_id' => $this->getIlsEmailErrorTypeId(),
			'error_date' => $this->getErrorDate(),
			'error_description' => $this->getErrorDescription(),
			'is_fixed' => $this->getIsFixed(),
			'ils_email_reparse_log_id' => $this->getIlsEmailReparseLogId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>