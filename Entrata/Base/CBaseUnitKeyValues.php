<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitKeyValues
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitKeyValues extends CEosPluralBase {

	/**
	 * @return CUnitKeyValue[]
	 */
	public static function fetchUnitKeyValues( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CUnitKeyValue', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CUnitKeyValue
	 */
	public static function fetchUnitKeyValue( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUnitKeyValue', $objDatabase );
	}

	public static function fetchUnitKeyValueCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'unit_key_values', $objDatabase );
	}

	public static function fetchUnitKeyValueByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchUnitKeyValue( sprintf( 'SELECT * FROM unit_key_values WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitKeyValuesByCid( $intCid, $objDatabase ) {
		return self::fetchUnitKeyValues( sprintf( 'SELECT * FROM unit_key_values WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitKeyValuesByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchUnitKeyValues( sprintf( 'SELECT * FROM unit_key_values WHERE property_unit_id = %d AND cid = %d', ( int ) $intPropertyUnitId, ( int ) $intCid ), $objDatabase );
	}

}
?>