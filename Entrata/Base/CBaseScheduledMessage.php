<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledMessage extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.scheduled_messages';

	protected $m_intId;
	protected $m_intCid;
	protected $m_strScheduledMessageType;
	protected $m_arrintScheduledMessageFilterIds;
	protected $m_intFrequencyId;
	protected $m_intSystemMessageTemplateId;
	protected $m_intMessageOwnerId;
	protected $m_strStartDatetime;
	protected $m_strEndDatetime;
	protected $m_strLocaleCode;
	protected $m_strTitle;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_boolIsPublished;
	protected $m_strPublishedOn;
	protected $m_intPublishedBy;
	protected $m_strLastQueuedOn;
	protected $m_strDeletedOn;
	protected $m_intDeletedBy;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strLocaleCode = '\'en_US\'::text';
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['scheduled_message_type'] ) && $boolDirectSet ) $this->set( 'm_strScheduledMessageType', trim( stripcslashes( $arrValues['scheduled_message_type'] ) ) ); elseif( isset( $arrValues['scheduled_message_type'] ) ) $this->setScheduledMessageType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['scheduled_message_type'] ) : $arrValues['scheduled_message_type'] );
		if( isset( $arrValues['scheduled_message_filter_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintScheduledMessageFilterIds', trim( $arrValues['scheduled_message_filter_ids'] ) ); elseif( isset( $arrValues['scheduled_message_filter_ids'] ) ) $this->setScheduledMessageFilterIds( $arrValues['scheduled_message_filter_ids'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['system_message_template_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemMessageTemplateId', trim( $arrValues['system_message_template_id'] ) ); elseif( isset( $arrValues['system_message_template_id'] ) ) $this->setSystemMessageTemplateId( $arrValues['system_message_template_id'] );
		if( isset( $arrValues['message_owner_id'] ) && $boolDirectSet ) $this->set( 'm_intMessageOwnerId', trim( $arrValues['message_owner_id'] ) ); elseif( isset( $arrValues['message_owner_id'] ) ) $this->setMessageOwnerId( $arrValues['message_owner_id'] );
		if( isset( $arrValues['start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStartDatetime', trim( $arrValues['start_datetime'] ) ); elseif( isset( $arrValues['start_datetime'] ) ) $this->setStartDatetime( $arrValues['start_datetime'] );
		if( isset( $arrValues['end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEndDatetime', trim( $arrValues['end_datetime'] ) ); elseif( isset( $arrValues['end_datetime'] ) ) $this->setEndDatetime( $arrValues['end_datetime'] );
		if( isset( $arrValues['locale_code'] ) && $boolDirectSet ) $this->set( 'm_strLocaleCode', trim( $arrValues['locale_code'] ) ); elseif( isset( $arrValues['locale_code'] ) ) $this->setLocaleCode( $arrValues['locale_code'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( $arrValues['title'] ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( $arrValues['title'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['published_on'] ) && $boolDirectSet ) $this->set( 'm_strPublishedOn', trim( $arrValues['published_on'] ) ); elseif( isset( $arrValues['published_on'] ) ) $this->setPublishedOn( $arrValues['published_on'] );
		if( isset( $arrValues['published_by'] ) && $boolDirectSet ) $this->set( 'm_intPublishedBy', trim( $arrValues['published_by'] ) ); elseif( isset( $arrValues['published_by'] ) ) $this->setPublishedBy( $arrValues['published_by'] );
		if( isset( $arrValues['last_queued_on'] ) && $boolDirectSet ) $this->set( 'm_strLastQueuedOn', trim( $arrValues['last_queued_on'] ) ); elseif( isset( $arrValues['last_queued_on'] ) ) $this->setLastQueuedOn( $arrValues['last_queued_on'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScheduledMessageType( $strScheduledMessageType ) {
		$this->set( 'm_strScheduledMessageType', CStrings::strTrimDef( $strScheduledMessageType, -1, NULL, true ) );
	}

	public function getScheduledMessageType() {
		return $this->m_strScheduledMessageType;
	}

	public function sqlScheduledMessageType() {
		return ( true == isset( $this->m_strScheduledMessageType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strScheduledMessageType ) : '\'' . addslashes( $this->m_strScheduledMessageType ) . '\'' ) : 'NULL';
	}

	public function setScheduledMessageFilterIds( $arrintScheduledMessageFilterIds ) {
		$this->set( 'm_arrintScheduledMessageFilterIds', CStrings::strToArrIntDef( $arrintScheduledMessageFilterIds, NULL ) );
	}

	public function getScheduledMessageFilterIds() {
		return $this->m_arrintScheduledMessageFilterIds;
	}

	public function sqlScheduledMessageFilterIds() {
		return ( true == isset( $this->m_arrintScheduledMessageFilterIds ) && true == valArr( $this->m_arrintScheduledMessageFilterIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintScheduledMessageFilterIds, NULL ) . '\'' : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : 'NULL';
	}

	public function setSystemMessageTemplateId( $intSystemMessageTemplateId ) {
		$this->set( 'm_intSystemMessageTemplateId', CStrings::strToIntDef( $intSystemMessageTemplateId, NULL, false ) );
	}

	public function getSystemMessageTemplateId() {
		return $this->m_intSystemMessageTemplateId;
	}

	public function sqlSystemMessageTemplateId() {
		return ( true == isset( $this->m_intSystemMessageTemplateId ) ) ? ( string ) $this->m_intSystemMessageTemplateId : 'NULL';
	}

	public function setMessageOwnerId( $intMessageOwnerId ) {
		$this->set( 'm_intMessageOwnerId', CStrings::strToIntDef( $intMessageOwnerId, NULL, false ) );
	}

	public function getMessageOwnerId() {
		return $this->m_intMessageOwnerId;
	}

	public function sqlMessageOwnerId() {
		return ( true == isset( $this->m_intMessageOwnerId ) ) ? ( string ) $this->m_intMessageOwnerId : 'NULL';
	}

	public function setStartDatetime( $strStartDatetime ) {
		$this->set( 'm_strStartDatetime', CStrings::strTrimDef( $strStartDatetime, -1, NULL, true ) );
	}

	public function getStartDatetime() {
		return $this->m_strStartDatetime;
	}

	public function sqlStartDatetime() {
		return ( true == isset( $this->m_strStartDatetime ) ) ? '\'' . $this->m_strStartDatetime . '\'' : 'NULL';
	}

	public function setEndDatetime( $strEndDatetime ) {
		$this->set( 'm_strEndDatetime', CStrings::strTrimDef( $strEndDatetime, -1, NULL, true ) );
	}

	public function getEndDatetime() {
		return $this->m_strEndDatetime;
	}

	public function sqlEndDatetime() {
		return ( true == isset( $this->m_strEndDatetime ) ) ? '\'' . $this->m_strEndDatetime . '\'' : 'NULL';
	}

	public function setLocaleCode( $strLocaleCode ) {
		$this->set( 'm_strLocaleCode', CStrings::strTrimDef( $strLocaleCode, -1, NULL, true ) );
	}

	public function getLocaleCode() {
		return $this->m_strLocaleCode;
	}

	public function sqlLocaleCode() {
		return ( true == isset( $this->m_strLocaleCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLocaleCode ) : '\'' . addslashes( $this->m_strLocaleCode ) . '\'' ) : '\'en_US\'';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, -1, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTitle ) : '\'' . addslashes( $this->m_strTitle ) . '\'' ) : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPublishedOn( $strPublishedOn ) {
		$this->set( 'm_strPublishedOn', CStrings::strTrimDef( $strPublishedOn, -1, NULL, true ) );
	}

	public function getPublishedOn() {
		return $this->m_strPublishedOn;
	}

	public function sqlPublishedOn() {
		return ( true == isset( $this->m_strPublishedOn ) ) ? '\'' . $this->m_strPublishedOn . '\'' : 'NULL';
	}

	public function setPublishedBy( $intPublishedBy ) {
		$this->set( 'm_intPublishedBy', CStrings::strToIntDef( $intPublishedBy, NULL, false ) );
	}

	public function getPublishedBy() {
		return $this->m_intPublishedBy;
	}

	public function sqlPublishedBy() {
		return ( true == isset( $this->m_intPublishedBy ) ) ? ( string ) $this->m_intPublishedBy : 'NULL';
	}

	public function setLastQueuedOn( $strLastQueuedOn ) {
		$this->set( 'm_strLastQueuedOn', CStrings::strTrimDef( $strLastQueuedOn, -1, NULL, true ) );
	}

	public function getLastQueuedOn() {
		return $this->m_strLastQueuedOn;
	}

	public function sqlLastQueuedOn() {
		return ( true == isset( $this->m_strLastQueuedOn ) ) ? '\'' . $this->m_strLastQueuedOn . '\'' : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, scheduled_message_type, scheduled_message_filter_ids, frequency_id, system_message_template_id, message_owner_id, start_datetime, end_datetime, locale_code, title, details, is_published, published_on, published_by, last_queued_on, deleted_on, deleted_by, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlScheduledMessageType() . ', ' .
						$this->sqlScheduledMessageFilterIds() . ', ' .
						$this->sqlFrequencyId() . ', ' .
						$this->sqlSystemMessageTemplateId() . ', ' .
						$this->sqlMessageOwnerId() . ', ' .
						$this->sqlStartDatetime() . ', ' .
						$this->sqlEndDatetime() . ', ' .
						$this->sqlLocaleCode() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlPublishedOn() . ', ' .
						$this->sqlPublishedBy() . ', ' .
						$this->sqlLastQueuedOn() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_message_type = ' . $this->sqlScheduledMessageType(). ',' ; } elseif( true == array_key_exists( 'ScheduledMessageType', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_message_type = ' . $this->sqlScheduledMessageType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_message_filter_ids = ' . $this->sqlScheduledMessageFilterIds(). ',' ; } elseif( true == array_key_exists( 'ScheduledMessageFilterIds', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_message_filter_ids = ' . $this->sqlScheduledMessageFilterIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId(). ',' ; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_message_template_id = ' . $this->sqlSystemMessageTemplateId(). ',' ; } elseif( true == array_key_exists( 'SystemMessageTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' system_message_template_id = ' . $this->sqlSystemMessageTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_owner_id = ' . $this->sqlMessageOwnerId(). ',' ; } elseif( true == array_key_exists( 'MessageOwnerId', $this->getChangedColumns() ) ) { $strSql .= ' message_owner_id = ' . $this->sqlMessageOwnerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_datetime = ' . $this->sqlStartDatetime(). ',' ; } elseif( true == array_key_exists( 'StartDatetime', $this->getChangedColumns() ) ) { $strSql .= ' start_datetime = ' . $this->sqlStartDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime(). ',' ; } elseif( true == array_key_exists( 'EndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locale_code = ' . $this->sqlLocaleCode(). ',' ; } elseif( true == array_key_exists( 'LocaleCode', $this->getChangedColumns() ) ) { $strSql .= ' locale_code = ' . $this->sqlLocaleCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' published_on = ' . $this->sqlPublishedOn(). ',' ; } elseif( true == array_key_exists( 'PublishedOn', $this->getChangedColumns() ) ) { $strSql .= ' published_on = ' . $this->sqlPublishedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' published_by = ' . $this->sqlPublishedBy(). ',' ; } elseif( true == array_key_exists( 'PublishedBy', $this->getChangedColumns() ) ) { $strSql .= ' published_by = ' . $this->sqlPublishedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_queued_on = ' . $this->sqlLastQueuedOn(). ',' ; } elseif( true == array_key_exists( 'LastQueuedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_queued_on = ' . $this->sqlLastQueuedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'scheduled_message_type' => $this->getScheduledMessageType(),
			'scheduled_message_filter_ids' => $this->getScheduledMessageFilterIds(),
			'frequency_id' => $this->getFrequencyId(),
			'system_message_template_id' => $this->getSystemMessageTemplateId(),
			'message_owner_id' => $this->getMessageOwnerId(),
			'start_datetime' => $this->getStartDatetime(),
			'end_datetime' => $this->getEndDatetime(),
			'locale_code' => $this->getLocaleCode(),
			'title' => $this->getTitle(),
			'details' => $this->getDetails(),
			'is_published' => $this->getIsPublished(),
			'published_on' => $this->getPublishedOn(),
			'published_by' => $this->getPublishedBy(),
			'last_queued_on' => $this->getLastQueuedOn(),
			'deleted_on' => $this->getDeletedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>