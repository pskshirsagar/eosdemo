<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyCamBudgets
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyCamBudgets extends CEosPluralBase {

	/**
	 * @return CPropertyCamBudget[]
	 */
	public static function fetchPropertyCamBudgets( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyCamBudget', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyCamBudget
	 */
	public static function fetchPropertyCamBudget( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyCamBudget', $objDatabase );
	}

	public static function fetchPropertyCamBudgetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_cam_budgets', $objDatabase );
	}

	public static function fetchPropertyCamBudgetByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyCamBudget( sprintf( 'SELECT * FROM property_cam_budgets WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCamBudgetsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyCamBudgets( sprintf( 'SELECT * FROM property_cam_budgets WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCamBudgetsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyCamBudgets( sprintf( 'SELECT * FROM property_cam_budgets WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCamBudgetsByPropertyCamPoolIdByCid( $intPropertyCamPoolId, $intCid, $objDatabase ) {
		return self::fetchPropertyCamBudgets( sprintf( 'SELECT * FROM property_cam_budgets WHERE property_cam_pool_id = %d AND cid = %d', ( int ) $intPropertyCamPoolId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCamBudgetsByPropertyCamPeriodIdByCid( $intPropertyCamPeriodId, $intCid, $objDatabase ) {
		return self::fetchPropertyCamBudgets( sprintf( 'SELECT * FROM property_cam_budgets WHERE property_cam_period_id = %d AND cid = %d', ( int ) $intPropertyCamPeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCamBudgetsByApCodeIdByCid( $intApCodeId, $intCid, $objDatabase ) {
		return self::fetchPropertyCamBudgets( sprintf( 'SELECT * FROM property_cam_budgets WHERE ap_code_id = %d AND cid = %d', ( int ) $intApCodeId, ( int ) $intCid ), $objDatabase );
	}

}
?>