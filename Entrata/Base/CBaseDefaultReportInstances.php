<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultReportInstances
 * Do not add any new functions to this class.
 */

class CBaseDefaultReportInstances extends CEosPluralBase {

	/**
	 * @return CDefaultReportInstance[]
	 */
	public static function fetchDefaultReportInstances( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDefaultReportInstance', $objDatabase );
	}

	/**
	 * @return CDefaultReportInstance
	 */
	public static function fetchDefaultReportInstance( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDefaultReportInstance', $objDatabase );
	}

	public static function fetchDefaultReportInstanceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_report_instances', $objDatabase );
	}

	public static function fetchDefaultReportInstanceById( $intId, $objDatabase ) {
		return self::fetchDefaultReportInstance( sprintf( 'SELECT * FROM default_report_instances WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDefaultReportInstancesByDefaultReportGroupId( $intDefaultReportGroupId, $objDatabase ) {
		return self::fetchDefaultReportInstances( sprintf( 'SELECT * FROM default_report_instances WHERE default_report_group_id = %d', ( int ) $intDefaultReportGroupId ), $objDatabase );
	}

	public static function fetchDefaultReportInstancesByDefaultReportId( $intDefaultReportId, $objDatabase ) {
		return self::fetchDefaultReportInstances( sprintf( 'SELECT * FROM default_report_instances WHERE default_report_id = %d', ( int ) $intDefaultReportId ), $objDatabase );
	}

	public static function fetchDefaultReportInstancesByDefaultReportVersionId( $intDefaultReportVersionId, $objDatabase ) {
		return self::fetchDefaultReportInstances( sprintf( 'SELECT * FROM default_report_instances WHERE default_report_version_id = %d', ( int ) $intDefaultReportVersionId ), $objDatabase );
	}

}
?>