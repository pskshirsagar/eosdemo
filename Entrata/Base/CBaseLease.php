<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLease extends CEosSingularBase {

	const TABLE_NAME = 'public.leases';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPrimaryCustomerId;
	protected $m_intPropertyId;
	protected $m_intReimbursingPropertyId;
	protected $m_intOwnerId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_intOccupancyTypeId;
	protected $m_intIntegrationDatabaseId;
	protected $m_intPaymentAllowanceTypeId;
	protected $m_intActiveLeaseIntervalId;
	protected $m_intM2mLeaseIntervalId;
	protected $m_intLeaseStatusTypeId;
	protected $m_intLeaseIntervalTypeId;
	protected $m_intLeaseTermId;
	protected $m_intLeaseStartWindowId;
	protected $m_intLateFeeFormulaId;
	protected $m_intLeaseTypeId;
	protected $m_intTerminationListTypeId;
	protected $m_intLastDelinquencyNoteEventId;
	protected $m_strRemotePrimaryKey;
	protected $m_strPropertyName;
	protected $m_strBuildingName;
	protected $m_strUnitNumberCache;
	protected $m_intDisplayNumber;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strLeaseStartDate;
	protected $m_strLeaseEndDate;
	protected $m_strMoveInDate;
	protected $m_strMoveOutDate;
	protected $m_strNoticeDate;
	protected $m_strTerminationStartDate;
	protected $m_strFmoStartedOn;
	protected $m_strFmoApprovedOn;
	protected $m_strFmoProcessedOn;
	protected $m_strCollectionsStartDate;
	protected $m_strCollectionsBlockedReason;
	protected $m_intCollectionsBlockedBy;
	protected $m_strCollectionsBlockedOn;
	protected $m_strLedgerLastSyncOn;
	protected $m_intReturnedPaymentsCount;
	protected $m_intDontChargeLateFees;
	protected $m_intLateFeePostedBy;
	protected $m_strLateFeePostedOn;
	protected $m_intIsView;
	protected $m_intIsRenewalBlacklist;
	protected $m_boolHasRepaymentAgreement;
	protected $m_intRebuildLeaseCache;
	protected $m_intDelinquencyPropertyInterestFormulaId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intOrganizationContractId;
	protected $m_boolSelectedDepositAlternative;

	public function __construct() {
		parent::__construct();

		$this->m_boolHasRepaymentAgreement = false;
		$this->m_intRebuildLeaseCache = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['primary_customer_id'] ) && $boolDirectSet ) $this->set( 'm_intPrimaryCustomerId', trim( $arrValues['primary_customer_id'] ) ); elseif( isset( $arrValues['primary_customer_id'] ) ) $this->setPrimaryCustomerId( $arrValues['primary_customer_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['reimbursing_property_id'] ) && $boolDirectSet ) $this->set( 'm_intReimbursingPropertyId', trim( $arrValues['reimbursing_property_id'] ) ); elseif( isset( $arrValues['reimbursing_property_id'] ) ) $this->setReimbursingPropertyId( $arrValues['reimbursing_property_id'] );
		if( isset( $arrValues['owner_id'] ) && $boolDirectSet ) $this->set( 'm_intOwnerId', trim( $arrValues['owner_id'] ) ); elseif( isset( $arrValues['owner_id'] ) ) $this->setOwnerId( $arrValues['owner_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['integration_database_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationDatabaseId', trim( $arrValues['integration_database_id'] ) ); elseif( isset( $arrValues['integration_database_id'] ) ) $this->setIntegrationDatabaseId( $arrValues['integration_database_id'] );
		if( isset( $arrValues['payment_allowance_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentAllowanceTypeId', trim( $arrValues['payment_allowance_type_id'] ) ); elseif( isset( $arrValues['payment_allowance_type_id'] ) ) $this->setPaymentAllowanceTypeId( $arrValues['payment_allowance_type_id'] );
		if( isset( $arrValues['active_lease_interval_id'] ) && $boolDirectSet ) $this->set( 'm_intActiveLeaseIntervalId', trim( $arrValues['active_lease_interval_id'] ) ); elseif( isset( $arrValues['active_lease_interval_id'] ) ) $this->setActiveLeaseIntervalId( $arrValues['active_lease_interval_id'] );
		if( isset( $arrValues['m2m_lease_interval_id'] ) && $boolDirectSet ) $this->set( 'm_intM2mLeaseIntervalId', trim( $arrValues['m2m_lease_interval_id'] ) ); elseif( isset( $arrValues['m2m_lease_interval_id'] ) ) $this->setM2mLeaseIntervalId( $arrValues['m2m_lease_interval_id'] );
		if( isset( $arrValues['lease_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStatusTypeId', trim( $arrValues['lease_status_type_id'] ) ); elseif( isset( $arrValues['lease_status_type_id'] ) ) $this->setLeaseStatusTypeId( $arrValues['lease_status_type_id'] );
		if( isset( $arrValues['lease_interval_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseIntervalTypeId', trim( $arrValues['lease_interval_type_id'] ) ); elseif( isset( $arrValues['lease_interval_type_id'] ) ) $this->setLeaseIntervalTypeId( $arrValues['lease_interval_type_id'] );
		if( isset( $arrValues['lease_term_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTermId', trim( $arrValues['lease_term_id'] ) ); elseif( isset( $arrValues['lease_term_id'] ) ) $this->setLeaseTermId( $arrValues['lease_term_id'] );
		if( isset( $arrValues['lease_start_window_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStartWindowId', trim( $arrValues['lease_start_window_id'] ) ); elseif( isset( $arrValues['lease_start_window_id'] ) ) $this->setLeaseStartWindowId( $arrValues['lease_start_window_id'] );
		if( isset( $arrValues['late_fee_formula_id'] ) && $boolDirectSet ) $this->set( 'm_intLateFeeFormulaId', trim( $arrValues['late_fee_formula_id'] ) ); elseif( isset( $arrValues['late_fee_formula_id'] ) ) $this->setLateFeeFormulaId( $arrValues['late_fee_formula_id'] );
		if( isset( $arrValues['lease_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTypeId', trim( $arrValues['lease_type_id'] ) ); elseif( isset( $arrValues['lease_type_id'] ) ) $this->setLeaseTypeId( $arrValues['lease_type_id'] );
		if( isset( $arrValues['termination_list_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTerminationListTypeId', trim( $arrValues['termination_list_type_id'] ) ); elseif( isset( $arrValues['termination_list_type_id'] ) ) $this->setTerminationListTypeId( $arrValues['termination_list_type_id'] );
		if( isset( $arrValues['last_delinquency_note_event_id'] ) && $boolDirectSet ) $this->set( 'm_intLastDelinquencyNoteEventId', trim( $arrValues['last_delinquency_note_event_id'] ) ); elseif( isset( $arrValues['last_delinquency_note_event_id'] ) ) $this->setLastDelinquencyNoteEventId( $arrValues['last_delinquency_note_event_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['property_name'] ) && $boolDirectSet ) $this->set( 'm_strPropertyName', trim( $arrValues['property_name'] ) ); elseif( isset( $arrValues['property_name'] ) ) $this->setPropertyName( $arrValues['property_name'] );
		if( isset( $arrValues['building_name'] ) && $boolDirectSet ) $this->set( 'm_strBuildingName', trim( $arrValues['building_name'] ) ); elseif( isset( $arrValues['building_name'] ) ) $this->setBuildingName( $arrValues['building_name'] );
		if( isset( $arrValues['unit_number_cache'] ) && $boolDirectSet ) $this->set( 'm_strUnitNumberCache', trim( $arrValues['unit_number_cache'] ) ); elseif( isset( $arrValues['unit_number_cache'] ) ) $this->setUnitNumberCache( $arrValues['unit_number_cache'] );
		if( isset( $arrValues['display_number'] ) && $boolDirectSet ) $this->set( 'm_intDisplayNumber', trim( $arrValues['display_number'] ) ); elseif( isset( $arrValues['display_number'] ) ) $this->setDisplayNumber( $arrValues['display_number'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( $arrValues['name_first'] ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( $arrValues['name_first'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( $arrValues['name_last'] ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( $arrValues['name_last'] );
		if( isset( $arrValues['lease_start_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStartDate', trim( $arrValues['lease_start_date'] ) ); elseif( isset( $arrValues['lease_start_date'] ) ) $this->setLeaseStartDate( $arrValues['lease_start_date'] );
		if( isset( $arrValues['lease_end_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseEndDate', trim( $arrValues['lease_end_date'] ) ); elseif( isset( $arrValues['lease_end_date'] ) ) $this->setLeaseEndDate( $arrValues['lease_end_date'] );
		if( isset( $arrValues['move_in_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveInDate', trim( $arrValues['move_in_date'] ) ); elseif( isset( $arrValues['move_in_date'] ) ) $this->setMoveInDate( $arrValues['move_in_date'] );
		if( isset( $arrValues['move_out_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutDate', trim( $arrValues['move_out_date'] ) ); elseif( isset( $arrValues['move_out_date'] ) ) $this->setMoveOutDate( $arrValues['move_out_date'] );
		if( isset( $arrValues['notice_date'] ) && $boolDirectSet ) $this->set( 'm_strNoticeDate', trim( $arrValues['notice_date'] ) ); elseif( isset( $arrValues['notice_date'] ) ) $this->setNoticeDate( $arrValues['notice_date'] );
		if( isset( $arrValues['termination_start_date'] ) && $boolDirectSet ) $this->set( 'm_strTerminationStartDate', trim( $arrValues['termination_start_date'] ) ); elseif( isset( $arrValues['termination_start_date'] ) ) $this->setTerminationStartDate( $arrValues['termination_start_date'] );
		if( isset( $arrValues['fmo_started_on'] ) && $boolDirectSet ) $this->set( 'm_strFmoStartedOn', trim( $arrValues['fmo_started_on'] ) ); elseif( isset( $arrValues['fmo_started_on'] ) ) $this->setFmoStartedOn( $arrValues['fmo_started_on'] );
		if( isset( $arrValues['fmo_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strFmoApprovedOn', trim( $arrValues['fmo_approved_on'] ) ); elseif( isset( $arrValues['fmo_approved_on'] ) ) $this->setFmoApprovedOn( $arrValues['fmo_approved_on'] );
		if( isset( $arrValues['fmo_processed_on'] ) && $boolDirectSet ) $this->set( 'm_strFmoProcessedOn', trim( $arrValues['fmo_processed_on'] ) ); elseif( isset( $arrValues['fmo_processed_on'] ) ) $this->setFmoProcessedOn( $arrValues['fmo_processed_on'] );
		if( isset( $arrValues['collections_start_date'] ) && $boolDirectSet ) $this->set( 'm_strCollectionsStartDate', trim( $arrValues['collections_start_date'] ) ); elseif( isset( $arrValues['collections_start_date'] ) ) $this->setCollectionsStartDate( $arrValues['collections_start_date'] );
		if( isset( $arrValues['collections_blocked_reason'] ) && $boolDirectSet ) $this->set( 'm_strCollectionsBlockedReason', trim( $arrValues['collections_blocked_reason'] ) ); elseif( isset( $arrValues['collections_blocked_reason'] ) ) $this->setCollectionsBlockedReason( $arrValues['collections_blocked_reason'] );
		if( isset( $arrValues['collections_blocked_by'] ) && $boolDirectSet ) $this->set( 'm_intCollectionsBlockedBy', trim( $arrValues['collections_blocked_by'] ) ); elseif( isset( $arrValues['collections_blocked_by'] ) ) $this->setCollectionsBlockedBy( $arrValues['collections_blocked_by'] );
		if( isset( $arrValues['collections_blocked_on'] ) && $boolDirectSet ) $this->set( 'm_strCollectionsBlockedOn', trim( $arrValues['collections_blocked_on'] ) ); elseif( isset( $arrValues['collections_blocked_on'] ) ) $this->setCollectionsBlockedOn( $arrValues['collections_blocked_on'] );
		if( isset( $arrValues['ledger_last_sync_on'] ) && $boolDirectSet ) $this->set( 'm_strLedgerLastSyncOn', trim( $arrValues['ledger_last_sync_on'] ) ); elseif( isset( $arrValues['ledger_last_sync_on'] ) ) $this->setLedgerLastSyncOn( $arrValues['ledger_last_sync_on'] );
		if( isset( $arrValues['returned_payments_count'] ) && $boolDirectSet ) $this->set( 'm_intReturnedPaymentsCount', trim( $arrValues['returned_payments_count'] ) ); elseif( isset( $arrValues['returned_payments_count'] ) ) $this->setReturnedPaymentsCount( $arrValues['returned_payments_count'] );
		if( isset( $arrValues['dont_charge_late_fees'] ) && $boolDirectSet ) $this->set( 'm_intDontChargeLateFees', trim( $arrValues['dont_charge_late_fees'] ) ); elseif( isset( $arrValues['dont_charge_late_fees'] ) ) $this->setDontChargeLateFees( $arrValues['dont_charge_late_fees'] );
		if( isset( $arrValues['late_fee_posted_by'] ) && $boolDirectSet ) $this->set( 'm_intLateFeePostedBy', trim( $arrValues['late_fee_posted_by'] ) ); elseif( isset( $arrValues['late_fee_posted_by'] ) ) $this->setLateFeePostedBy( $arrValues['late_fee_posted_by'] );
		if( isset( $arrValues['late_fee_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strLateFeePostedOn', trim( $arrValues['late_fee_posted_on'] ) ); elseif( isset( $arrValues['late_fee_posted_on'] ) ) $this->setLateFeePostedOn( $arrValues['late_fee_posted_on'] );
		if( isset( $arrValues['is_view'] ) && $boolDirectSet ) $this->set( 'm_intIsView', trim( $arrValues['is_view'] ) ); elseif( isset( $arrValues['is_view'] ) ) $this->setIsView( $arrValues['is_view'] );
		if( isset( $arrValues['is_renewal_blacklist'] ) && $boolDirectSet ) $this->set( 'm_intIsRenewalBlacklist', trim( $arrValues['is_renewal_blacklist'] ) ); elseif( isset( $arrValues['is_renewal_blacklist'] ) ) $this->setIsRenewalBlacklist( $arrValues['is_renewal_blacklist'] );
		if( isset( $arrValues['has_repayment_agreement'] ) && $boolDirectSet ) $this->set( 'm_boolHasRepaymentAgreement', trim( stripcslashes( $arrValues['has_repayment_agreement'] ) ) ); elseif( isset( $arrValues['has_repayment_agreement'] ) ) $this->setHasRepaymentAgreement( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_repayment_agreement'] ) : $arrValues['has_repayment_agreement'] );
		if( isset( $arrValues['rebuild_lease_cache'] ) && $boolDirectSet ) $this->set( 'm_intRebuildLeaseCache', trim( $arrValues['rebuild_lease_cache'] ) ); elseif( isset( $arrValues['rebuild_lease_cache'] ) ) $this->setRebuildLeaseCache( $arrValues['rebuild_lease_cache'] );
		if( isset( $arrValues['delinquency_property_interest_formula_id'] ) && $boolDirectSet ) $this->set( 'm_intDelinquencyPropertyInterestFormulaId', trim( $arrValues['delinquency_property_interest_formula_id'] ) ); elseif( isset( $arrValues['delinquency_property_interest_formula_id'] ) ) $this->setDelinquencyPropertyInterestFormulaId( $arrValues['delinquency_property_interest_formula_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['organization_contract_id'] ) && $boolDirectSet ) $this->set( 'm_intOrganizationContractId', trim( $arrValues['organization_contract_id'] ) ); elseif( isset( $arrValues['organization_contract_id'] ) ) $this->setOrganizationContractId( $arrValues['organization_contract_id'] );
		if( isset( $arrValues['selected_deposit_alternative'] ) && $boolDirectSet ) $this->set( 'm_boolSelectedDepositAlternative', trim( stripcslashes( $arrValues['selected_deposit_alternative'] ) ) ); elseif( isset( $arrValues['selected_deposit_alternative'] ) ) $this->setSelectedDepositAlternative( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['selected_deposit_alternative'] ) : $arrValues['selected_deposit_alternative'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPrimaryCustomerId( $intPrimaryCustomerId ) {
		$this->set( 'm_intPrimaryCustomerId', CStrings::strToIntDef( $intPrimaryCustomerId, NULL, false ) );
	}

	public function getPrimaryCustomerId() {
		return $this->m_intPrimaryCustomerId;
	}

	public function sqlPrimaryCustomerId() {
		return ( true == isset( $this->m_intPrimaryCustomerId ) ) ? ( string ) $this->m_intPrimaryCustomerId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setReimbursingPropertyId( $intReimbursingPropertyId ) {
		$this->set( 'm_intReimbursingPropertyId', CStrings::strToIntDef( $intReimbursingPropertyId, NULL, false ) );
	}

	public function getReimbursingPropertyId() {
		return $this->m_intReimbursingPropertyId;
	}

	public function sqlReimbursingPropertyId() {
		return ( true == isset( $this->m_intReimbursingPropertyId ) ) ? ( string ) $this->m_intReimbursingPropertyId : 'NULL';
	}

	public function setOwnerId( $intOwnerId ) {
		$this->set( 'm_intOwnerId', CStrings::strToIntDef( $intOwnerId, NULL, false ) );
	}

	public function getOwnerId() {
		return $this->m_intOwnerId;
	}

	public function sqlOwnerId() {
		return ( true == isset( $this->m_intOwnerId ) ) ? ( string ) $this->m_intOwnerId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : 'NULL';
	}

	public function setIntegrationDatabaseId( $intIntegrationDatabaseId ) {
		$this->set( 'm_intIntegrationDatabaseId', CStrings::strToIntDef( $intIntegrationDatabaseId, NULL, false ) );
	}

	public function getIntegrationDatabaseId() {
		return $this->m_intIntegrationDatabaseId;
	}

	public function sqlIntegrationDatabaseId() {
		return ( true == isset( $this->m_intIntegrationDatabaseId ) ) ? ( string ) $this->m_intIntegrationDatabaseId : 'NULL';
	}

	public function setPaymentAllowanceTypeId( $intPaymentAllowanceTypeId ) {
		$this->set( 'm_intPaymentAllowanceTypeId', CStrings::strToIntDef( $intPaymentAllowanceTypeId, NULL, false ) );
	}

	public function getPaymentAllowanceTypeId() {
		return $this->m_intPaymentAllowanceTypeId;
	}

	public function sqlPaymentAllowanceTypeId() {
		return ( true == isset( $this->m_intPaymentAllowanceTypeId ) ) ? ( string ) $this->m_intPaymentAllowanceTypeId : 'NULL';
	}

	public function setActiveLeaseIntervalId( $intActiveLeaseIntervalId ) {
		$this->set( 'm_intActiveLeaseIntervalId', CStrings::strToIntDef( $intActiveLeaseIntervalId, NULL, false ) );
	}

	public function getActiveLeaseIntervalId() {
		return $this->m_intActiveLeaseIntervalId;
	}

	public function sqlActiveLeaseIntervalId() {
		return ( true == isset( $this->m_intActiveLeaseIntervalId ) ) ? ( string ) $this->m_intActiveLeaseIntervalId : 'NULL';
	}

	public function setM2mLeaseIntervalId( $intM2mLeaseIntervalId ) {
		$this->set( 'm_intM2mLeaseIntervalId', CStrings::strToIntDef( $intM2mLeaseIntervalId, NULL, false ) );
	}

	public function getM2mLeaseIntervalId() {
		return $this->m_intM2mLeaseIntervalId;
	}

	public function sqlM2mLeaseIntervalId() {
		return ( true == isset( $this->m_intM2mLeaseIntervalId ) ) ? ( string ) $this->m_intM2mLeaseIntervalId : 'NULL';
	}

	public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$this->set( 'm_intLeaseStatusTypeId', CStrings::strToIntDef( $intLeaseStatusTypeId, NULL, false ) );
	}

	public function getLeaseStatusTypeId() {
		return $this->m_intLeaseStatusTypeId;
	}

	public function sqlLeaseStatusTypeId() {
		return ( true == isset( $this->m_intLeaseStatusTypeId ) ) ? ( string ) $this->m_intLeaseStatusTypeId : 'NULL';
	}

	public function setLeaseIntervalTypeId( $intLeaseIntervalTypeId ) {
		$this->set( 'm_intLeaseIntervalTypeId', CStrings::strToIntDef( $intLeaseIntervalTypeId, NULL, false ) );
	}

	public function getLeaseIntervalTypeId() {
		return $this->m_intLeaseIntervalTypeId;
	}

	public function sqlLeaseIntervalTypeId() {
		return ( true == isset( $this->m_intLeaseIntervalTypeId ) ) ? ( string ) $this->m_intLeaseIntervalTypeId : 'NULL';
	}

	public function setLeaseTermId( $intLeaseTermId ) {
		$this->set( 'm_intLeaseTermId', CStrings::strToIntDef( $intLeaseTermId, NULL, false ) );
	}

	public function getLeaseTermId() {
		return $this->m_intLeaseTermId;
	}

	public function sqlLeaseTermId() {
		return ( true == isset( $this->m_intLeaseTermId ) ) ? ( string ) $this->m_intLeaseTermId : 'NULL';
	}

	public function setLeaseStartWindowId( $intLeaseStartWindowId ) {
		$this->set( 'm_intLeaseStartWindowId', CStrings::strToIntDef( $intLeaseStartWindowId, NULL, false ) );
	}

	public function getLeaseStartWindowId() {
		return $this->m_intLeaseStartWindowId;
	}

	public function sqlLeaseStartWindowId() {
		return ( true == isset( $this->m_intLeaseStartWindowId ) ) ? ( string ) $this->m_intLeaseStartWindowId : 'NULL';
	}

	public function setLateFeeFormulaId( $intLateFeeFormulaId ) {
		$this->set( 'm_intLateFeeFormulaId', CStrings::strToIntDef( $intLateFeeFormulaId, NULL, false ) );
	}

	public function getLateFeeFormulaId() {
		return $this->m_intLateFeeFormulaId;
	}

	public function sqlLateFeeFormulaId() {
		return ( true == isset( $this->m_intLateFeeFormulaId ) ) ? ( string ) $this->m_intLateFeeFormulaId : 'NULL';
	}

	public function setLeaseTypeId( $intLeaseTypeId ) {
		$this->set( 'm_intLeaseTypeId', CStrings::strToIntDef( $intLeaseTypeId, NULL, false ) );
	}

	public function getLeaseTypeId() {
		return $this->m_intLeaseTypeId;
	}

	public function sqlLeaseTypeId() {
		return ( true == isset( $this->m_intLeaseTypeId ) ) ? ( string ) $this->m_intLeaseTypeId : 'NULL';
	}

	public function setTerminationListTypeId( $intTerminationListTypeId ) {
		$this->set( 'm_intTerminationListTypeId', CStrings::strToIntDef( $intTerminationListTypeId, NULL, false ) );
	}

	public function getTerminationListTypeId() {
		return $this->m_intTerminationListTypeId;
	}

	public function sqlTerminationListTypeId() {
		return ( true == isset( $this->m_intTerminationListTypeId ) ) ? ( string ) $this->m_intTerminationListTypeId : 'NULL';
	}

	public function setLastDelinquencyNoteEventId( $intLastDelinquencyNoteEventId ) {
		$this->set( 'm_intLastDelinquencyNoteEventId', CStrings::strToIntDef( $intLastDelinquencyNoteEventId, NULL, false ) );
	}

	public function getLastDelinquencyNoteEventId() {
		return $this->m_intLastDelinquencyNoteEventId;
	}

	public function sqlLastDelinquencyNoteEventId() {
		return ( true == isset( $this->m_intLastDelinquencyNoteEventId ) ) ? ( string ) $this->m_intLastDelinquencyNoteEventId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setPropertyName( $strPropertyName ) {
		$this->set( 'm_strPropertyName', CStrings::strTrimDef( $strPropertyName, 50, NULL, true ) );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function sqlPropertyName() {
		return ( true == isset( $this->m_strPropertyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPropertyName ) : '\'' . addslashes( $this->m_strPropertyName ) . '\'' ) : 'NULL';
	}

	public function setBuildingName( $strBuildingName ) {
		$this->set( 'm_strBuildingName', CStrings::strTrimDef( $strBuildingName, 50, NULL, true ) );
	}

	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	public function sqlBuildingName() {
		return ( true == isset( $this->m_strBuildingName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBuildingName ) : '\'' . addslashes( $this->m_strBuildingName ) . '\'' ) : 'NULL';
	}

	public function setUnitNumberCache( $strUnitNumberCache ) {
		$this->set( 'm_strUnitNumberCache', CStrings::strTrimDef( $strUnitNumberCache, 50, NULL, true ) );
	}

	public function getUnitNumberCache() {
		return $this->m_strUnitNumberCache;
	}

	public function sqlUnitNumberCache() {
		return ( true == isset( $this->m_strUnitNumberCache ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUnitNumberCache ) : '\'' . addslashes( $this->m_strUnitNumberCache ) . '\'' ) : 'NULL';
	}

	public function setDisplayNumber( $intDisplayNumber ) {
		$this->set( 'm_intDisplayNumber', CStrings::strToIntDef( $intDisplayNumber, NULL, false ) );
	}

	public function getDisplayNumber() {
		return $this->m_intDisplayNumber;
	}

	public function sqlDisplayNumber() {
		return ( true == isset( $this->m_intDisplayNumber ) ) ? ( string ) $this->m_intDisplayNumber : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 100, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameFirst ) : '\'' . addslashes( $this->m_strNameFirst ) . '\'' ) : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 50, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameLast ) : '\'' . addslashes( $this->m_strNameLast ) . '\'' ) : 'NULL';
	}

	public function setLeaseStartDate( $strLeaseStartDate ) {
		$this->set( 'm_strLeaseStartDate', CStrings::strTrimDef( $strLeaseStartDate, -1, NULL, true ) );
	}

	public function getLeaseStartDate() {
		return $this->m_strLeaseStartDate;
	}

	public function sqlLeaseStartDate() {
		return ( true == isset( $this->m_strLeaseStartDate ) ) ? '\'' . $this->m_strLeaseStartDate . '\'' : 'NULL';
	}

	public function setLeaseEndDate( $strLeaseEndDate ) {
		$this->set( 'm_strLeaseEndDate', CStrings::strTrimDef( $strLeaseEndDate, -1, NULL, true ) );
	}

	public function getLeaseEndDate() {
		return $this->m_strLeaseEndDate;
	}

	public function sqlLeaseEndDate() {
		return ( true == isset( $this->m_strLeaseEndDate ) ) ? '\'' . $this->m_strLeaseEndDate . '\'' : 'NULL';
	}

	public function setMoveInDate( $strMoveInDate ) {
		$this->set( 'm_strMoveInDate', CStrings::strTrimDef( $strMoveInDate, -1, NULL, true ) );
	}

	public function getMoveInDate() {
		return $this->m_strMoveInDate;
	}

	public function sqlMoveInDate() {
		return ( true == isset( $this->m_strMoveInDate ) ) ? '\'' . $this->m_strMoveInDate . '\'' : 'NULL';
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->set( 'm_strMoveOutDate', CStrings::strTrimDef( $strMoveOutDate, -1, NULL, true ) );
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function sqlMoveOutDate() {
		return ( true == isset( $this->m_strMoveOutDate ) ) ? '\'' . $this->m_strMoveOutDate . '\'' : 'NULL';
	}

	public function setNoticeDate( $strNoticeDate ) {
		$this->set( 'm_strNoticeDate', CStrings::strTrimDef( $strNoticeDate, -1, NULL, true ) );
	}

	public function getNoticeDate() {
		return $this->m_strNoticeDate;
	}

	public function sqlNoticeDate() {
		return ( true == isset( $this->m_strNoticeDate ) ) ? '\'' . $this->m_strNoticeDate . '\'' : 'NULL';
	}

	public function setTerminationStartDate( $strTerminationStartDate ) {
		$this->set( 'm_strTerminationStartDate', CStrings::strTrimDef( $strTerminationStartDate, -1, NULL, true ) );
	}

	public function getTerminationStartDate() {
		return $this->m_strTerminationStartDate;
	}

	public function sqlTerminationStartDate() {
		return ( true == isset( $this->m_strTerminationStartDate ) ) ? '\'' . $this->m_strTerminationStartDate . '\'' : 'NULL';
	}

	public function setFmoStartedOn( $strFmoStartedOn ) {
		$this->set( 'm_strFmoStartedOn', CStrings::strTrimDef( $strFmoStartedOn, -1, NULL, true ) );
	}

	public function getFmoStartedOn() {
		return $this->m_strFmoStartedOn;
	}

	public function sqlFmoStartedOn() {
		return ( true == isset( $this->m_strFmoStartedOn ) ) ? '\'' . $this->m_strFmoStartedOn . '\'' : 'NULL';
	}

	public function setFmoApprovedOn( $strFmoApprovedOn ) {
		$this->set( 'm_strFmoApprovedOn', CStrings::strTrimDef( $strFmoApprovedOn, -1, NULL, true ) );
	}

	public function getFmoApprovedOn() {
		return $this->m_strFmoApprovedOn;
	}

	public function sqlFmoApprovedOn() {
		return ( true == isset( $this->m_strFmoApprovedOn ) ) ? '\'' . $this->m_strFmoApprovedOn . '\'' : 'NULL';
	}

	public function setFmoProcessedOn( $strFmoProcessedOn ) {
		$this->set( 'm_strFmoProcessedOn', CStrings::strTrimDef( $strFmoProcessedOn, -1, NULL, true ) );
	}

	public function getFmoProcessedOn() {
		return $this->m_strFmoProcessedOn;
	}

	public function sqlFmoProcessedOn() {
		return ( true == isset( $this->m_strFmoProcessedOn ) ) ? '\'' . $this->m_strFmoProcessedOn . '\'' : 'NULL';
	}

	public function setCollectionsStartDate( $strCollectionsStartDate ) {
		$this->set( 'm_strCollectionsStartDate', CStrings::strTrimDef( $strCollectionsStartDate, -1, NULL, true ) );
	}

	public function getCollectionsStartDate() {
		return $this->m_strCollectionsStartDate;
	}

	public function sqlCollectionsStartDate() {
		return ( true == isset( $this->m_strCollectionsStartDate ) ) ? '\'' . $this->m_strCollectionsStartDate . '\'' : 'NULL';
	}

	public function setCollectionsBlockedReason( $strCollectionsBlockedReason ) {
		$this->set( 'm_strCollectionsBlockedReason', CStrings::strTrimDef( $strCollectionsBlockedReason, -1, NULL, true ) );
	}

	public function getCollectionsBlockedReason() {
		return $this->m_strCollectionsBlockedReason;
	}

	public function sqlCollectionsBlockedReason() {
		return ( true == isset( $this->m_strCollectionsBlockedReason ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCollectionsBlockedReason ) : '\'' . addslashes( $this->m_strCollectionsBlockedReason ) . '\'' ) : 'NULL';
	}

	public function setCollectionsBlockedBy( $intCollectionsBlockedBy ) {
		$this->set( 'm_intCollectionsBlockedBy', CStrings::strToIntDef( $intCollectionsBlockedBy, NULL, false ) );
	}

	public function getCollectionsBlockedBy() {
		return $this->m_intCollectionsBlockedBy;
	}

	public function sqlCollectionsBlockedBy() {
		return ( true == isset( $this->m_intCollectionsBlockedBy ) ) ? ( string ) $this->m_intCollectionsBlockedBy : 'NULL';
	}

	public function setCollectionsBlockedOn( $strCollectionsBlockedOn ) {
		$this->set( 'm_strCollectionsBlockedOn', CStrings::strTrimDef( $strCollectionsBlockedOn, -1, NULL, true ) );
	}

	public function getCollectionsBlockedOn() {
		return $this->m_strCollectionsBlockedOn;
	}

	public function sqlCollectionsBlockedOn() {
		return ( true == isset( $this->m_strCollectionsBlockedOn ) ) ? '\'' . $this->m_strCollectionsBlockedOn . '\'' : 'NULL';
	}

	public function setLedgerLastSyncOn( $strLedgerLastSyncOn ) {
		$this->set( 'm_strLedgerLastSyncOn', CStrings::strTrimDef( $strLedgerLastSyncOn, -1, NULL, true ) );
	}

	public function getLedgerLastSyncOn() {
		return $this->m_strLedgerLastSyncOn;
	}

	public function sqlLedgerLastSyncOn() {
		return ( true == isset( $this->m_strLedgerLastSyncOn ) ) ? '\'' . $this->m_strLedgerLastSyncOn . '\'' : 'NULL';
	}

	public function setReturnedPaymentsCount( $intReturnedPaymentsCount ) {
		$this->set( 'm_intReturnedPaymentsCount', CStrings::strToIntDef( $intReturnedPaymentsCount, NULL, false ) );
	}

	public function getReturnedPaymentsCount() {
		return $this->m_intReturnedPaymentsCount;
	}

	public function sqlReturnedPaymentsCount() {
		return ( true == isset( $this->m_intReturnedPaymentsCount ) ) ? ( string ) $this->m_intReturnedPaymentsCount : 'NULL';
	}

	public function setDontChargeLateFees( $intDontChargeLateFees ) {
		$this->set( 'm_intDontChargeLateFees', CStrings::strToIntDef( $intDontChargeLateFees, NULL, false ) );
	}

	public function getDontChargeLateFees() {
		return $this->m_intDontChargeLateFees;
	}

	public function sqlDontChargeLateFees() {
		return ( true == isset( $this->m_intDontChargeLateFees ) ) ? ( string ) $this->m_intDontChargeLateFees : 'NULL';
	}

	public function setLateFeePostedBy( $intLateFeePostedBy ) {
		$this->set( 'm_intLateFeePostedBy', CStrings::strToIntDef( $intLateFeePostedBy, NULL, false ) );
	}

	public function getLateFeePostedBy() {
		return $this->m_intLateFeePostedBy;
	}

	public function sqlLateFeePostedBy() {
		return ( true == isset( $this->m_intLateFeePostedBy ) ) ? ( string ) $this->m_intLateFeePostedBy : 'NULL';
	}

	public function setLateFeePostedOn( $strLateFeePostedOn ) {
		$this->set( 'm_strLateFeePostedOn', CStrings::strTrimDef( $strLateFeePostedOn, -1, NULL, true ) );
	}

	public function getLateFeePostedOn() {
		return $this->m_strLateFeePostedOn;
	}

	public function sqlLateFeePostedOn() {
		return ( true == isset( $this->m_strLateFeePostedOn ) ) ? '\'' . $this->m_strLateFeePostedOn . '\'' : 'NULL';
	}

	public function setIsView( $intIsView ) {
		$this->set( 'm_intIsView', CStrings::strToIntDef( $intIsView, NULL, false ) );
	}

	public function getIsView() {
		return $this->m_intIsView;
	}

	public function sqlIsView() {
		return ( true == isset( $this->m_intIsView ) ) ? ( string ) $this->m_intIsView : 'NULL';
	}

	public function setIsRenewalBlacklist( $intIsRenewalBlacklist ) {
		$this->set( 'm_intIsRenewalBlacklist', CStrings::strToIntDef( $intIsRenewalBlacklist, NULL, false ) );
	}

	public function getIsRenewalBlacklist() {
		return $this->m_intIsRenewalBlacklist;
	}

	public function sqlIsRenewalBlacklist() {
		return ( true == isset( $this->m_intIsRenewalBlacklist ) ) ? ( string ) $this->m_intIsRenewalBlacklist : 'NULL';
	}

	public function setHasRepaymentAgreement( $boolHasRepaymentAgreement ) {
		$this->set( 'm_boolHasRepaymentAgreement', CStrings::strToBool( $boolHasRepaymentAgreement ) );
	}

	public function getHasRepaymentAgreement() {
		return $this->m_boolHasRepaymentAgreement;
	}

	public function sqlHasRepaymentAgreement() {
		return ( true == isset( $this->m_boolHasRepaymentAgreement ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasRepaymentAgreement ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRebuildLeaseCache( $intRebuildLeaseCache ) {
		$this->set( 'm_intRebuildLeaseCache', CStrings::strToIntDef( $intRebuildLeaseCache, NULL, false ) );
	}

	public function getRebuildLeaseCache() {
		return $this->m_intRebuildLeaseCache;
	}

	public function sqlRebuildLeaseCache() {
		return ( true == isset( $this->m_intRebuildLeaseCache ) ) ? ( string ) $this->m_intRebuildLeaseCache : '0';
	}

	public function setDelinquencyPropertyInterestFormulaId( $intDelinquencyPropertyInterestFormulaId ) {
		$this->set( 'm_intDelinquencyPropertyInterestFormulaId', CStrings::strToIntDef( $intDelinquencyPropertyInterestFormulaId, NULL, false ) );
	}

	public function getDelinquencyPropertyInterestFormulaId() {
		return $this->m_intDelinquencyPropertyInterestFormulaId;
	}

	public function sqlDelinquencyPropertyInterestFormulaId() {
		return ( true == isset( $this->m_intDelinquencyPropertyInterestFormulaId ) ) ? ( string ) $this->m_intDelinquencyPropertyInterestFormulaId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setOrganizationContractId( $intOrganizationContractId ) {
		$this->set( 'm_intOrganizationContractId', CStrings::strToIntDef( $intOrganizationContractId, NULL, false ) );
	}

	public function getOrganizationContractId() {
		return $this->m_intOrganizationContractId;
	}

	public function sqlOrganizationContractId() {
		return ( true == isset( $this->m_intOrganizationContractId ) ) ? ( string ) $this->m_intOrganizationContractId : 'NULL';
	}

	public function setSelectedDepositAlternative( $boolSelectedDepositAlternative ) {
		$this->set( 'm_boolSelectedDepositAlternative', CStrings::strToBool( $boolSelectedDepositAlternative ) );
	}

	public function getSelectedDepositAlternative() {
		return $this->m_boolSelectedDepositAlternative;
	}

	public function sqlSelectedDepositAlternative() {
		return ( true == isset( $this->m_boolSelectedDepositAlternative ) ) ? '\'' . ( true == ( bool ) $this->m_boolSelectedDepositAlternative ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$objDataset = $objDatabase->createDataset();

		$intId = $this->getId();

		if( true == is_null( $intId ) ) {
			$strSql = 'SELECT nextval( \'' . $this->getSequenceName() . '\'::text ) AS id';

			if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert lease record. The following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

				$objDataset->cleanup();
				return false;
			}

			$arrValues = $objDataset->fetchArray();
			$this->setId( $arrValues['id'] );

			$objDataset->cleanup();
		}

		$strSql = 'SELECT * ' .
					'FROM ' . static::TABLE_NAME . '_insert( row_to_json ( ROW ( ' .
						$this->sqlId() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPrimaryCustomerId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlReimbursingPropertyId() . ', ' .
						$this->sqlOwnerId() . ', ' .
						$this->sqlPropertyUnitId() . ', ' .
						$this->sqlUnitSpaceId() . ', ' .
						$this->sqlOccupancyTypeId() . ', ' .
						$this->sqlIntegrationDatabaseId() . ', ' .
						$this->sqlPaymentAllowanceTypeId() . ', ' .
						$this->sqlActiveLeaseIntervalId() . ', ' .
						$this->sqlM2mLeaseIntervalId() . ', ' .
						$this->sqlLeaseStatusTypeId() . ', ' .
						$this->sqlLeaseIntervalTypeId() . ', ' .
						$this->sqlLeaseTermId() . ', ' .
						$this->sqlLeaseStartWindowId() . ', ' .
						$this->sqlLateFeeFormulaId() . ', ' .
						$this->sqlLeaseTypeId() . ', ' .
						$this->sqlTerminationListTypeId() . ', ' .
						$this->sqlLastDelinquencyNoteEventId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlPropertyName() . ', ' .
						$this->sqlBuildingName() . ', ' .
						$this->sqlUnitNumberCache() . ', ' .
						$this->sqlDisplayNumber() . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlLeaseStartDate() . ', ' .
						$this->sqlLeaseEndDate() . ', ' .
						$this->sqlMoveInDate() . ', ' .
						$this->sqlMoveOutDate() . ', ' .
						$this->sqlNoticeDate() . ', ' .
						$this->sqlTerminationStartDate() . ', ' .
						$this->sqlFmoStartedOn() . ', ' .
						$this->sqlFmoApprovedOn() . ', ' .
						$this->sqlFmoProcessedOn() . ', ' .
						$this->sqlCollectionsStartDate() . ', ' .
						$this->sqlCollectionsBlockedReason() . ', ' .
						$this->sqlCollectionsBlockedBy() . ', ' .
						$this->sqlCollectionsBlockedOn() . ', ' .
						$this->sqlLedgerLastSyncOn() . ', ' .
						$this->sqlReturnedPaymentsCount() . ', ' .
						$this->sqlDontChargeLateFees() . ', ' .
						$this->sqlLateFeePostedBy() . ', ' .
						$this->sqlLateFeePostedOn() . ', ' .
						$this->sqlIsView() . ', ' .
						$this->sqlIsRenewalBlacklist() . ', ' .
						$this->sqlHasRepaymentAgreement() . ', ' .
						$this->sqlRebuildLeaseCache() . ', ' .
						$this->sqlDelinquencyPropertyInterestFormulaId() . ', ' .
						$this->sqlOrganizationContractId() . ', ' .
						$this->sqlSelectedDepositAlternative() . ', ' .
						( int ) $intCurrentUserId . ' ) ) ) AS result;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {

			//Reset id on error
			$this->setId( $intId );

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert lease record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();
			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert lease record. The following error was reported.' ) );
			//Reset id on error
			$this->setId( $intId );

			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * ' . 
					'FROM ' . static::TABLE_NAME . '_update( row_to_json ( ROW ( ' .
						$this->sqlId() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPrimaryCustomerId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlReimbursingPropertyId() . ', ' .
						$this->sqlOwnerId() . ', ' .
						$this->sqlPropertyUnitId() . ', ' .
						$this->sqlUnitSpaceId() . ', ' .
						$this->sqlOccupancyTypeId() . ', ' .
						$this->sqlIntegrationDatabaseId() . ', ' .
						$this->sqlPaymentAllowanceTypeId() . ', ' .
						$this->sqlActiveLeaseIntervalId() . ', ' .
						$this->sqlM2mLeaseIntervalId() . ', ' .
						$this->sqlLeaseStatusTypeId() . ', ' .
						$this->sqlLeaseIntervalTypeId() . ', ' .
						$this->sqlLeaseTermId() . ', ' .
						$this->sqlLeaseStartWindowId() . ', ' .
						$this->sqlLateFeeFormulaId() . ', ' .
						$this->sqlLeaseTypeId() . ', ' .
						$this->sqlTerminationListTypeId() . ', ' .
						$this->sqlLastDelinquencyNoteEventId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlPropertyName() . ', ' .
						$this->sqlBuildingName() . ', ' .
						$this->sqlUnitNumberCache() . ', ' .
						$this->sqlDisplayNumber() . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlLeaseStartDate() . ', ' .
						$this->sqlLeaseEndDate() . ', ' .
						$this->sqlMoveInDate() . ', ' .
						$this->sqlMoveOutDate() . ', ' .
						$this->sqlNoticeDate() . ', ' .
						$this->sqlTerminationStartDate() . ', ' .
						$this->sqlFmoStartedOn() . ', ' .
						$this->sqlFmoApprovedOn() . ', ' .
						$this->sqlFmoProcessedOn() . ', ' .
						$this->sqlCollectionsStartDate() . ', ' .
						$this->sqlCollectionsBlockedReason() . ', ' .
						$this->sqlCollectionsBlockedBy() . ', ' .
						$this->sqlCollectionsBlockedOn() . ', ' .
						$this->sqlLedgerLastSyncOn() . ', ' .
						$this->sqlReturnedPaymentsCount() . ', ' .
						$this->sqlDontChargeLateFees() . ', ' .
						$this->sqlLateFeePostedBy() . ', ' .
						$this->sqlLateFeePostedOn() . ', ' .
						$this->sqlIsView() . ', ' .
						$this->sqlIsRenewalBlacklist() . ', ' .
						$this->sqlHasRepaymentAgreement() . ', ' .
						$this->sqlRebuildLeaseCache() . ', ' .
						$this->sqlDelinquencyPropertyInterestFormulaId() . ', ' .
						$this->sqlOrganizationContractId() . ', ' .
						$this->sqlSelectedDepositAlternative() . ', ' .
						 ( int ) $intCurrentUserId . ' ) ) ) AS result;';

		if( true == $boolReturnSqlOnly ) {
			 return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update lease record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update lease record. The following error was reported.' ) );

			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$objDataset = $objDatabase->createDataset();

		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * ' .
					'FROM ' . static::TABLE_NAME . '_delete( ' .
		$this->sqlId() . ', ' .
		$this->sqlCid() . ', ' .
					 ( int ) $intCurrentUserId . ' ) AS result;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to delete lease record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to delete lease record. The following error was reported.' ) );

			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'primary_customer_id' => $this->getPrimaryCustomerId(),
			'property_id' => $this->getPropertyId(),
			'reimbursing_property_id' => $this->getReimbursingPropertyId(),
			'owner_id' => $this->getOwnerId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'integration_database_id' => $this->getIntegrationDatabaseId(),
			'payment_allowance_type_id' => $this->getPaymentAllowanceTypeId(),
			'active_lease_interval_id' => $this->getActiveLeaseIntervalId(),
			'm2m_lease_interval_id' => $this->getM2mLeaseIntervalId(),
			'lease_status_type_id' => $this->getLeaseStatusTypeId(),
			'lease_interval_type_id' => $this->getLeaseIntervalTypeId(),
			'lease_term_id' => $this->getLeaseTermId(),
			'lease_start_window_id' => $this->getLeaseStartWindowId(),
			'late_fee_formula_id' => $this->getLateFeeFormulaId(),
			'lease_type_id' => $this->getLeaseTypeId(),
			'termination_list_type_id' => $this->getTerminationListTypeId(),
			'last_delinquency_note_event_id' => $this->getLastDelinquencyNoteEventId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'property_name' => $this->getPropertyName(),
			'building_name' => $this->getBuildingName(),
			'unit_number_cache' => $this->getUnitNumberCache(),
			'display_number' => $this->getDisplayNumber(),
			'name_first' => $this->getNameFirst(),
			'name_last' => $this->getNameLast(),
			'lease_start_date' => $this->getLeaseStartDate(),
			'lease_end_date' => $this->getLeaseEndDate(),
			'move_in_date' => $this->getMoveInDate(),
			'move_out_date' => $this->getMoveOutDate(),
			'notice_date' => $this->getNoticeDate(),
			'termination_start_date' => $this->getTerminationStartDate(),
			'fmo_started_on' => $this->getFmoStartedOn(),
			'fmo_approved_on' => $this->getFmoApprovedOn(),
			'fmo_processed_on' => $this->getFmoProcessedOn(),
			'collections_start_date' => $this->getCollectionsStartDate(),
			'collections_blocked_reason' => $this->getCollectionsBlockedReason(),
			'collections_blocked_by' => $this->getCollectionsBlockedBy(),
			'collections_blocked_on' => $this->getCollectionsBlockedOn(),
			'ledger_last_sync_on' => $this->getLedgerLastSyncOn(),
			'returned_payments_count' => $this->getReturnedPaymentsCount(),
			'dont_charge_late_fees' => $this->getDontChargeLateFees(),
			'late_fee_posted_by' => $this->getLateFeePostedBy(),
			'late_fee_posted_on' => $this->getLateFeePostedOn(),
			'is_view' => $this->getIsView(),
			'is_renewal_blacklist' => $this->getIsRenewalBlacklist(),
			'has_repayment_agreement' => $this->getHasRepaymentAgreement(),
			'rebuild_lease_cache' => $this->getRebuildLeaseCache(),
			'delinquency_property_interest_formula_id' => $this->getDelinquencyPropertyInterestFormulaId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'organization_contract_id' => $this->getOrganizationContractId(),
			'selected_deposit_alternative' => $this->getSelectedDepositAlternative()
		);
	}

}
?>