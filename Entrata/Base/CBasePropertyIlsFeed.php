<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyIlsFeed extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.property_ils_feeds';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intInternetListingServiceId;
	protected $m_strKey;
	protected $m_strRejectionNote;
	protected $m_intIsLimited;
	protected $m_intIsFeedIlsLeadTrackerEmail;
	protected $m_intSuspendFeeds;
	protected $m_strIlsNotifiedOn;
	protected $m_intIsCriticalInfoMissing;
	protected $m_strCriticalInfoMissing;
	protected $m_strNotificationConfirmedOn;
	protected $m_strNotificationRejectedOn;
	protected $m_strChangeNotifiedOn;
	protected $m_strChangeRejectedOn;
	protected $m_strSuspendedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intOccupancyTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_intIsLimited = '0';
		$this->m_intIsFeedIlsLeadTrackerEmail = '0';
		$this->m_intSuspendFeeds = '0';
		$this->m_intIsCriticalInfoMissing = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['internet_listing_service_id'] ) && $boolDirectSet ) $this->set( 'm_intInternetListingServiceId', trim( $arrValues['internet_listing_service_id'] ) ); elseif( isset( $arrValues['internet_listing_service_id'] ) ) $this->setInternetListingServiceId( $arrValues['internet_listing_service_id'] );
		if( isset( $arrValues['key'] ) && $boolDirectSet ) $this->set( 'm_strKey', trim( $arrValues['key'] ) ); elseif( isset( $arrValues['key'] ) ) $this->setKey( $arrValues['key'] );
		if( isset( $arrValues['rejection_note'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strRejectionNote', trim( $arrValues['rejection_note'] ) ); elseif( isset( $arrValues['rejection_note'] ) ) $this->setRejectionNote( $arrValues['rejection_note'] );
		if( isset( $arrValues['is_limited'] ) && $boolDirectSet ) $this->set( 'm_intIsLimited', trim( $arrValues['is_limited'] ) ); elseif( isset( $arrValues['is_limited'] ) ) $this->setIsLimited( $arrValues['is_limited'] );
		if( isset( $arrValues['is_feed_ils_lead_tracker_email'] ) && $boolDirectSet ) $this->set( 'm_intIsFeedIlsLeadTrackerEmail', trim( $arrValues['is_feed_ils_lead_tracker_email'] ) ); elseif( isset( $arrValues['is_feed_ils_lead_tracker_email'] ) ) $this->setIsFeedIlsLeadTrackerEmail( $arrValues['is_feed_ils_lead_tracker_email'] );
		if( isset( $arrValues['suspend_feeds'] ) && $boolDirectSet ) $this->set( 'm_intSuspendFeeds', trim( $arrValues['suspend_feeds'] ) ); elseif( isset( $arrValues['suspend_feeds'] ) ) $this->setSuspendFeeds( $arrValues['suspend_feeds'] );
		if( isset( $arrValues['ils_notified_on'] ) && $boolDirectSet ) $this->set( 'm_strIlsNotifiedOn', trim( $arrValues['ils_notified_on'] ) ); elseif( isset( $arrValues['ils_notified_on'] ) ) $this->setIlsNotifiedOn( $arrValues['ils_notified_on'] );
		if( isset( $arrValues['is_critical_info_missing'] ) && $boolDirectSet ) $this->set( 'm_intIsCriticalInfoMissing', trim( $arrValues['is_critical_info_missing'] ) ); elseif( isset( $arrValues['is_critical_info_missing'] ) ) $this->setIsCriticalInfoMissing( $arrValues['is_critical_info_missing'] );
		if( isset( $arrValues['critical_info_missing'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCriticalInfoMissing', trim( $arrValues['critical_info_missing'] ) ); elseif( isset( $arrValues['critical_info_missing'] ) ) $this->setCriticalInfoMissing( $arrValues['critical_info_missing'] );
		if( isset( $arrValues['notification_confirmed_on'] ) && $boolDirectSet ) $this->set( 'm_strNotificationConfirmedOn', trim( $arrValues['notification_confirmed_on'] ) ); elseif( isset( $arrValues['notification_confirmed_on'] ) ) $this->setNotificationConfirmedOn( $arrValues['notification_confirmed_on'] );
		if( isset( $arrValues['notification_rejected_on'] ) && $boolDirectSet ) $this->set( 'm_strNotificationRejectedOn', trim( $arrValues['notification_rejected_on'] ) ); elseif( isset( $arrValues['notification_rejected_on'] ) ) $this->setNotificationRejectedOn( $arrValues['notification_rejected_on'] );
		if( isset( $arrValues['change_notified_on'] ) && $boolDirectSet ) $this->set( 'm_strChangeNotifiedOn', trim( $arrValues['change_notified_on'] ) ); elseif( isset( $arrValues['change_notified_on'] ) ) $this->setChangeNotifiedOn( $arrValues['change_notified_on'] );
		if( isset( $arrValues['change_rejected_on'] ) && $boolDirectSet ) $this->set( 'm_strChangeRejectedOn', trim( $arrValues['change_rejected_on'] ) ); elseif( isset( $arrValues['change_rejected_on'] ) ) $this->setChangeRejectedOn( $arrValues['change_rejected_on'] );
		if( isset( $arrValues['suspended_on'] ) && $boolDirectSet ) $this->set( 'm_strSuspendedOn', trim( $arrValues['suspended_on'] ) ); elseif( isset( $arrValues['suspended_on'] ) ) $this->setSuspendedOn( $arrValues['suspended_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setInternetListingServiceId( $intInternetListingServiceId ) {
		$this->set( 'm_intInternetListingServiceId', CStrings::strToIntDef( $intInternetListingServiceId, NULL, false ) );
	}

	public function getInternetListingServiceId() {
		return $this->m_intInternetListingServiceId;
	}

	public function sqlInternetListingServiceId() {
		return ( true == isset( $this->m_intInternetListingServiceId ) ) ? ( string ) $this->m_intInternetListingServiceId : 'NULL';
	}

	public function setKey( $strKey ) {
		$this->set( 'm_strKey', CStrings::strTrimDef( $strKey, 64, NULL, true ) );
	}

	public function getKey() {
		return $this->m_strKey;
	}

	public function sqlKey() {
		return ( true == isset( $this->m_strKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strKey ) : '\'' . addslashes( $this->m_strKey ) . '\'' ) : 'NULL';
	}

	public function setRejectionNote( $strRejectionNote, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strRejectionNote', CStrings::strTrimDef( $strRejectionNote, 2000, NULL, true ), $strLocaleCode );
	}

	public function getRejectionNote( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strRejectionNote', $strLocaleCode );
	}

	public function sqlRejectionNote() {
		return ( true == isset( $this->m_strRejectionNote ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRejectionNote ) : '\'' . addslashes( $this->m_strRejectionNote ) . '\'' ) : 'NULL';
	}

	public function setIsLimited( $intIsLimited ) {
		$this->set( 'm_intIsLimited', CStrings::strToIntDef( $intIsLimited, NULL, false ) );
	}

	public function getIsLimited() {
		return $this->m_intIsLimited;
	}

	public function sqlIsLimited() {
		return ( true == isset( $this->m_intIsLimited ) ) ? ( string ) $this->m_intIsLimited : '0';
	}

	public function setIsFeedIlsLeadTrackerEmail( $intIsFeedIlsLeadTrackerEmail ) {
		$this->set( 'm_intIsFeedIlsLeadTrackerEmail', CStrings::strToIntDef( $intIsFeedIlsLeadTrackerEmail, NULL, false ) );
	}

	public function getIsFeedIlsLeadTrackerEmail() {
		return $this->m_intIsFeedIlsLeadTrackerEmail;
	}

	public function sqlIsFeedIlsLeadTrackerEmail() {
		return ( true == isset( $this->m_intIsFeedIlsLeadTrackerEmail ) ) ? ( string ) $this->m_intIsFeedIlsLeadTrackerEmail : '0';
	}

	public function setSuspendFeeds( $intSuspendFeeds ) {
		$this->set( 'm_intSuspendFeeds', CStrings::strToIntDef( $intSuspendFeeds, NULL, false ) );
	}

	public function getSuspendFeeds() {
		return $this->m_intSuspendFeeds;
	}

	public function sqlSuspendFeeds() {
		return ( true == isset( $this->m_intSuspendFeeds ) ) ? ( string ) $this->m_intSuspendFeeds : '0';
	}

	public function setIlsNotifiedOn( $strIlsNotifiedOn ) {
		$this->set( 'm_strIlsNotifiedOn', CStrings::strTrimDef( $strIlsNotifiedOn, -1, NULL, true ) );
	}

	public function getIlsNotifiedOn() {
		return $this->m_strIlsNotifiedOn;
	}

	public function sqlIlsNotifiedOn() {
		return ( true == isset( $this->m_strIlsNotifiedOn ) ) ? '\'' . $this->m_strIlsNotifiedOn . '\'' : 'NULL';
	}

	public function setIsCriticalInfoMissing( $intIsCriticalInfoMissing ) {
		$this->set( 'm_intIsCriticalInfoMissing', CStrings::strToIntDef( $intIsCriticalInfoMissing, NULL, false ) );
	}

	public function getIsCriticalInfoMissing() {
		return $this->m_intIsCriticalInfoMissing;
	}

	public function sqlIsCriticalInfoMissing() {
		return ( true == isset( $this->m_intIsCriticalInfoMissing ) ) ? ( string ) $this->m_intIsCriticalInfoMissing : '0';
	}

	public function setCriticalInfoMissing( $strCriticalInfoMissing, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strCriticalInfoMissing', CStrings::strTrimDef( $strCriticalInfoMissing, -1, NULL, true ), $strLocaleCode );
	}

	public function getCriticalInfoMissing( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strCriticalInfoMissing', $strLocaleCode );
	}

	public function sqlCriticalInfoMissing() {
		return ( true == isset( $this->m_strCriticalInfoMissing ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCriticalInfoMissing ) : '\'' . addslashes( $this->m_strCriticalInfoMissing ) . '\'' ) : 'NULL';
	}

	public function setNotificationConfirmedOn( $strNotificationConfirmedOn ) {
		$this->set( 'm_strNotificationConfirmedOn', CStrings::strTrimDef( $strNotificationConfirmedOn, -1, NULL, true ) );
	}

	public function getNotificationConfirmedOn() {
		return $this->m_strNotificationConfirmedOn;
	}

	public function sqlNotificationConfirmedOn() {
		return ( true == isset( $this->m_strNotificationConfirmedOn ) ) ? '\'' . $this->m_strNotificationConfirmedOn . '\'' : 'NULL';
	}

	public function setNotificationRejectedOn( $strNotificationRejectedOn ) {
		$this->set( 'm_strNotificationRejectedOn', CStrings::strTrimDef( $strNotificationRejectedOn, -1, NULL, true ) );
	}

	public function getNotificationRejectedOn() {
		return $this->m_strNotificationRejectedOn;
	}

	public function sqlNotificationRejectedOn() {
		return ( true == isset( $this->m_strNotificationRejectedOn ) ) ? '\'' . $this->m_strNotificationRejectedOn . '\'' : 'NULL';
	}

	public function setChangeNotifiedOn( $strChangeNotifiedOn ) {
		$this->set( 'm_strChangeNotifiedOn', CStrings::strTrimDef( $strChangeNotifiedOn, -1, NULL, true ) );
	}

	public function getChangeNotifiedOn() {
		return $this->m_strChangeNotifiedOn;
	}

	public function sqlChangeNotifiedOn() {
		return ( true == isset( $this->m_strChangeNotifiedOn ) ) ? '\'' . $this->m_strChangeNotifiedOn . '\'' : 'NULL';
	}

	public function setChangeRejectedOn( $strChangeRejectedOn ) {
		$this->set( 'm_strChangeRejectedOn', CStrings::strTrimDef( $strChangeRejectedOn, -1, NULL, true ) );
	}

	public function getChangeRejectedOn() {
		return $this->m_strChangeRejectedOn;
	}

	public function sqlChangeRejectedOn() {
		return ( true == isset( $this->m_strChangeRejectedOn ) ) ? '\'' . $this->m_strChangeRejectedOn . '\'' : 'NULL';
	}

	public function setSuspendedOn( $strSuspendedOn ) {
		$this->set( 'm_strSuspendedOn', CStrings::strTrimDef( $strSuspendedOn, -1, NULL, true ) );
	}

	public function getSuspendedOn() {
		return $this->m_strSuspendedOn;
	}

	public function sqlSuspendedOn() {
		return ( true == isset( $this->m_strSuspendedOn ) ) ? '\'' . $this->m_strSuspendedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, internet_listing_service_id, key, rejection_note, is_limited, is_feed_ils_lead_tracker_email, suspend_feeds, ils_notified_on, is_critical_info_missing, critical_info_missing, notification_confirmed_on, notification_rejected_on, change_notified_on, change_rejected_on, suspended_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details, occupancy_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlInternetListingServiceId() . ', ' .
						$this->sqlKey() . ', ' .
						$this->sqlRejectionNote() . ', ' .
						$this->sqlIsLimited() . ', ' .
						$this->sqlIsFeedIlsLeadTrackerEmail() . ', ' .
						$this->sqlSuspendFeeds() . ', ' .
						$this->sqlIlsNotifiedOn() . ', ' .
						$this->sqlIsCriticalInfoMissing() . ', ' .
						$this->sqlCriticalInfoMissing() . ', ' .
						$this->sqlNotificationConfirmedOn() . ', ' .
						$this->sqlNotificationRejectedOn() . ', ' .
						$this->sqlChangeNotifiedOn() . ', ' .
						$this->sqlChangeRejectedOn() . ', ' .
						$this->sqlSuspendedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlOccupancyTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internet_listing_service_id = ' . $this->sqlInternetListingServiceId(). ',' ; } elseif( true == array_key_exists( 'InternetListingServiceId', $this->getChangedColumns() ) ) { $strSql .= ' internet_listing_service_id = ' . $this->sqlInternetListingServiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' key = ' . $this->sqlKey(). ',' ; } elseif( true == array_key_exists( 'Key', $this->getChangedColumns() ) ) { $strSql .= ' key = ' . $this->sqlKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejection_note = ' . $this->sqlRejectionNote(). ',' ; } elseif( true == array_key_exists( 'RejectionNote', $this->getChangedColumns() ) ) { $strSql .= ' rejection_note = ' . $this->sqlRejectionNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_limited = ' . $this->sqlIsLimited(). ',' ; } elseif( true == array_key_exists( 'IsLimited', $this->getChangedColumns() ) ) { $strSql .= ' is_limited = ' . $this->sqlIsLimited() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_feed_ils_lead_tracker_email = ' . $this->sqlIsFeedIlsLeadTrackerEmail(). ',' ; } elseif( true == array_key_exists( 'IsFeedIlsLeadTrackerEmail', $this->getChangedColumns() ) ) { $strSql .= ' is_feed_ils_lead_tracker_email = ' . $this->sqlIsFeedIlsLeadTrackerEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' suspend_feeds = ' . $this->sqlSuspendFeeds(). ',' ; } elseif( true == array_key_exists( 'SuspendFeeds', $this->getChangedColumns() ) ) { $strSql .= ' suspend_feeds = ' . $this->sqlSuspendFeeds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ils_notified_on = ' . $this->sqlIlsNotifiedOn(). ',' ; } elseif( true == array_key_exists( 'IlsNotifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' ils_notified_on = ' . $this->sqlIlsNotifiedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_critical_info_missing = ' . $this->sqlIsCriticalInfoMissing(). ',' ; } elseif( true == array_key_exists( 'IsCriticalInfoMissing', $this->getChangedColumns() ) ) { $strSql .= ' is_critical_info_missing = ' . $this->sqlIsCriticalInfoMissing() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' critical_info_missing = ' . $this->sqlCriticalInfoMissing(). ',' ; } elseif( true == array_key_exists( 'CriticalInfoMissing', $this->getChangedColumns() ) ) { $strSql .= ' critical_info_missing = ' . $this->sqlCriticalInfoMissing() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notification_confirmed_on = ' . $this->sqlNotificationConfirmedOn(). ',' ; } elseif( true == array_key_exists( 'NotificationConfirmedOn', $this->getChangedColumns() ) ) { $strSql .= ' notification_confirmed_on = ' . $this->sqlNotificationConfirmedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notification_rejected_on = ' . $this->sqlNotificationRejectedOn(). ',' ; } elseif( true == array_key_exists( 'NotificationRejectedOn', $this->getChangedColumns() ) ) { $strSql .= ' notification_rejected_on = ' . $this->sqlNotificationRejectedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' change_notified_on = ' . $this->sqlChangeNotifiedOn(). ',' ; } elseif( true == array_key_exists( 'ChangeNotifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' change_notified_on = ' . $this->sqlChangeNotifiedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' change_rejected_on = ' . $this->sqlChangeRejectedOn(). ',' ; } elseif( true == array_key_exists( 'ChangeRejectedOn', $this->getChangedColumns() ) ) { $strSql .= ' change_rejected_on = ' . $this->sqlChangeRejectedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' suspended_on = ' . $this->sqlSuspendedOn(). ',' ; } elseif( true == array_key_exists( 'SuspendedOn', $this->getChangedColumns() ) ) { $strSql .= ' suspended_on = ' . $this->sqlSuspendedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'internet_listing_service_id' => $this->getInternetListingServiceId(),
			'key' => $this->getKey(),
			'rejection_note' => $this->getRejectionNote(),
			'is_limited' => $this->getIsLimited(),
			'is_feed_ils_lead_tracker_email' => $this->getIsFeedIlsLeadTrackerEmail(),
			'suspend_feeds' => $this->getSuspendFeeds(),
			'ils_notified_on' => $this->getIlsNotifiedOn(),
			'is_critical_info_missing' => $this->getIsCriticalInfoMissing(),
			'critical_info_missing' => $this->getCriticalInfoMissing(),
			'notification_confirmed_on' => $this->getNotificationConfirmedOn(),
			'notification_rejected_on' => $this->getNotificationRejectedOn(),
			'change_notified_on' => $this->getChangeNotifiedOn(),
			'change_rejected_on' => $this->getChangeRejectedOn(),
			'suspended_on' => $this->getSuspendedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'occupancy_type_id' => $this->getOccupancyTypeId()
		);
	}

}
?>