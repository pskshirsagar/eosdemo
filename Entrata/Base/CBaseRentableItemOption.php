<?php

class CBaseRentableItemOption extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intDefaultCid;
    protected $m_intPropertyRentableItemOptionTypeId;
    protected $m_intCompanyMediaFileId;
    protected $m_intSitePlanCompanyMediaFileId;
    protected $m_strRemotePrimaryKey;
    protected $m_strLookupCode;
    protected $m_strName;
    protected $m_strDescription;
    protected $m_intAllowFlexibleDates;
    protected $m_intHasInventory;
    protected $m_intIsPublished;
    protected $m_intOrderNum;
    protected $m_intDeletedBy;
    protected $m_strDeletedOn;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        $this->m_intAllowFlexibleDates = '0';
        $this->m_intHasInventory = '0';
        $this->m_intIsPublished = '1';
        $this->m_intOrderNum = '0';

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if ( isset( $arrValues['id'] )) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if ( isset( $arrValues['cid'] )) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['default_cid'] ) && $boolDirectSet ) $this->m_intDefaultCid = trim( $arrValues['default_cid'] ); else if ( isset( $arrValues['default_cid'] )) $this->setDefaultCid( $arrValues['default_cid'] );
        if( isset( $arrValues['property_rentable_item_option_type_id'] ) && $boolDirectSet ) $this->m_intPropertyRentableItemOptionTypeId = trim( $arrValues['property_rentable_item_option_type_id'] ); else if ( isset( $arrValues['property_rentable_item_option_type_id'] )) $this->setPropertyRentableItemOptionTypeId( $arrValues['property_rentable_item_option_type_id'] );
        if( isset( $arrValues['company_media_file_id'] ) && $boolDirectSet ) $this->m_intCompanyMediaFileId = trim( $arrValues['company_media_file_id'] ); else if ( isset( $arrValues['company_media_file_id'] )) $this->setCompanyMediaFileId( $arrValues['company_media_file_id'] );
        if( isset( $arrValues['site_plan_company_media_file_id'] ) && $boolDirectSet ) $this->m_intSitePlanCompanyMediaFileId = trim( $arrValues['site_plan_company_media_file_id'] ); else if ( isset( $arrValues['site_plan_company_media_file_id'] )) $this->setSitePlanCompanyMediaFileId( $arrValues['site_plan_company_media_file_id'] );
        if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->m_strRemotePrimaryKey = trim( stripcslashes( $arrValues['remote_primary_key'] ) ); else if ( isset( $arrValues['remote_primary_key'] )) $this->setRemotePrimaryKey(( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
        if( isset( $arrValues['lookup_code'] ) && $boolDirectSet ) $this->m_strLookupCode = trim( stripcslashes( $arrValues['lookup_code'] ) ); else if ( isset( $arrValues['lookup_code'] )) $this->setLookupCode(( true == $boolStripSlashes ) ? stripslashes( $arrValues['lookup_code'] ) : $arrValues['lookup_code'] );
        if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->m_strName = trim( stripcslashes( $arrValues['name'] ) ); else if ( isset( $arrValues['name'] )) $this->setName(( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
        if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->m_strDescription = trim( stripcslashes( $arrValues['description'] ) ); else if ( isset( $arrValues['description'] )) $this->setDescription(( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
        if( isset( $arrValues['allow_flexible_dates'] ) && $boolDirectSet ) $this->m_intAllowFlexibleDates = trim( $arrValues['allow_flexible_dates'] ); else if ( isset( $arrValues['allow_flexible_dates'] )) $this->setAllowFlexibleDates( $arrValues['allow_flexible_dates'] );
        if( isset( $arrValues['has_inventory'] ) && $boolDirectSet ) $this->m_intHasInventory = trim( $arrValues['has_inventory'] ); else if ( isset( $arrValues['has_inventory'] )) $this->setHasInventory( $arrValues['has_inventory'] );
        if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->m_intIsPublished = trim( $arrValues['is_published'] ); else if ( isset( $arrValues['is_published'] )) $this->setIsPublished( $arrValues['is_published'] );
        if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->m_intOrderNum = trim( $arrValues['order_num'] ); else if ( isset( $arrValues['order_num'] )) $this->setOrderNum( $arrValues['order_num'] );
        if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->m_intDeletedBy = trim( $arrValues['deleted_by'] ); else if ( isset( $arrValues['deleted_by'] )) $this->setDeletedBy( $arrValues['deleted_by'] );
        if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->m_strDeletedOn = trim( $arrValues['deleted_on'] ); else if ( isset( $arrValues['deleted_on'] )) $this->setDeletedOn( $arrValues['deleted_on'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if ( isset( $arrValues['updated_by'] )) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if ( isset( $arrValues['updated_on'] )) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if ( isset( $arrValues['created_by'] )) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if ( isset( $arrValues['created_on'] )) $this->setCreatedOn( $arrValues['created_on'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return  ( true == isset( $this->m_intId )) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return  ( true == isset( $this->m_intCid )) ? (string) $this->m_intCid : 'NULL';
    }

    public function setDefaultCid( $intDefaultCid ) {
        $this->m_intDefaultCid = CStrings::strToIntDef( $intDefaultCid, NULL, false );
    }

    public function getDefaultCid() {
        return $this->m_intDefaultCid;
    }

    public function sqlDefaultCid() {
        return  ( true == isset( $this->m_intDefaultCid )) ? (string) $this->m_intDefaultCid : 'NULL';
    }

    public function setPropertyRentableItemOptionTypeId( $intPropertyRentableItemOptionTypeId ) {
        $this->m_intPropertyRentableItemOptionTypeId = CStrings::strToIntDef( $intPropertyRentableItemOptionTypeId, NULL, false );
    }

    public function getPropertyRentableItemOptionTypeId() {
        return $this->m_intPropertyRentableItemOptionTypeId;
    }

    public function sqlPropertyRentableItemOptionTypeId() {
        return  ( true == isset( $this->m_intPropertyRentableItemOptionTypeId )) ? (string) $this->m_intPropertyRentableItemOptionTypeId : 'NULL';
    }

    public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
        $this->m_intCompanyMediaFileId = CStrings::strToIntDef( $intCompanyMediaFileId, NULL, false );
    }

    public function getCompanyMediaFileId() {
        return $this->m_intCompanyMediaFileId;
    }

    public function sqlCompanyMediaFileId() {
        return  ( true == isset( $this->m_intCompanyMediaFileId )) ? (string) $this->m_intCompanyMediaFileId : 'NULL';
    }

    public function setSitePlanCompanyMediaFileId( $intSitePlanCompanyMediaFileId ) {
        $this->m_intSitePlanCompanyMediaFileId = CStrings::strToIntDef( $intSitePlanCompanyMediaFileId, NULL, false );
    }

    public function getSitePlanCompanyMediaFileId() {
        return $this->m_intSitePlanCompanyMediaFileId;
    }

    public function sqlSitePlanCompanyMediaFileId() {
        return  ( true == isset( $this->m_intSitePlanCompanyMediaFileId )) ? (string) $this->m_intSitePlanCompanyMediaFileId : 'NULL';
    }

    public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
        $this->m_strRemotePrimaryKey = CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true );
    }

    public function getRemotePrimaryKey() {
        return $this->m_strRemotePrimaryKey;
    }

    public function sqlRemotePrimaryKey() {
        return  ( true == isset( $this->m_strRemotePrimaryKey )) ? '\'' . addslashes( $this->m_strRemotePrimaryKey) . '\'' : 'NULL';
    }

    public function setLookupCode( $strLookupCode ) {
        $this->m_strLookupCode = CStrings::strTrimDef( $strLookupCode, 64, NULL, true );
    }

    public function getLookupCode() {
        return $this->m_strLookupCode;
    }

    public function sqlLookupCode() {
        return  ( true == isset( $this->m_strLookupCode )) ? '\'' . addslashes( $this->m_strLookupCode) . '\'' : 'NULL';
    }

    public function setName( $strName ) {
        $this->m_strName = CStrings::strTrimDef( $strName, 50, NULL, true );
    }

    public function getName() {
        return $this->m_strName;
    }

    public function sqlName() {
        return  ( true == isset( $this->m_strName )) ? '\'' . addslashes( $this->m_strName) . '\'' : 'NULL';
    }

    public function setDescription( $strDescription ) {
        $this->m_strDescription = CStrings::strTrimDef( $strDescription, 240, NULL, true );
    }

    public function getDescription() {
        return $this->m_strDescription;
    }

    public function sqlDescription() {
        return  ( true == isset( $this->m_strDescription )) ? '\'' . addslashes( $this->m_strDescription) . '\'' : 'NULL';
    }

    public function setAllowFlexibleDates( $intAllowFlexibleDates ) {
        $this->m_intAllowFlexibleDates = CStrings::strToIntDef( $intAllowFlexibleDates, NULL, false );
    }

    public function getAllowFlexibleDates() {
        return $this->m_intAllowFlexibleDates;
    }

    public function sqlAllowFlexibleDates() {
        return  ( true == isset( $this->m_intAllowFlexibleDates )) ? (string) $this->m_intAllowFlexibleDates : '0';
    }

    public function setHasInventory( $intHasInventory ) {
        $this->m_intHasInventory = CStrings::strToIntDef( $intHasInventory, NULL, false );
    }

    public function getHasInventory() {
        return $this->m_intHasInventory;
    }

    public function sqlHasInventory() {
        return  ( true == isset( $this->m_intHasInventory )) ? (string) $this->m_intHasInventory : '0';
    }

    public function setIsPublished( $intIsPublished ) {
        $this->m_intIsPublished = CStrings::strToIntDef( $intIsPublished, NULL, false );
    }

    public function getIsPublished() {
        return $this->m_intIsPublished;
    }

    public function sqlIsPublished() {
        return  ( true == isset( $this->m_intIsPublished )) ? (string) $this->m_intIsPublished : '1';
    }

    public function setOrderNum( $intOrderNum ) {
        $this->m_intOrderNum = CStrings::strToIntDef( $intOrderNum, NULL, false );
    }

    public function getOrderNum() {
        return $this->m_intOrderNum;
    }

    public function sqlOrderNum() {
        return  ( true == isset( $this->m_intOrderNum )) ? (string) $this->m_intOrderNum : '0';
    }

    public function setDeletedBy( $intDeletedBy ) {
        $this->m_intDeletedBy = CStrings::strToIntDef( $intDeletedBy, NULL, false );
    }

    public function getDeletedBy() {
        return $this->m_intDeletedBy;
    }

    public function sqlDeletedBy() {
        return  ( true == isset( $this->m_intDeletedBy )) ? (string) $this->m_intDeletedBy : 'NULL';
    }

    public function setDeletedOn( $strDeletedOn ) {
        $this->m_strDeletedOn = CStrings::strTrimDef( $strDeletedOn, -1, NULL, true );
    }

    public function getDeletedOn() {
        return $this->m_strDeletedOn;
    }

    public function sqlDeletedOn() {
        return  ( true == isset( $this->m_strDeletedOn )) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return  ( true == isset( $this->m_intUpdatedBy )) ? (string) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return  ( true == isset( $this->m_strUpdatedOn )) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return  ( true == isset( $this->m_intCreatedBy )) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return  ( true == isset( $this->m_strCreatedOn )) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() )) ?  'nextval( \'public.rentable_item_options_id_seq\' )' : (int) $this->m_intId;

        $strSql = 'INSERT INTO
					  public.rentable_item_options
					VALUES ( ' .
	                    $strId . ', ' .
	                    $this->sqlCid() . ', ' .
	                    $this->sqlDefaultCid() . ', ' .
	                    $this->sqlPropertyRentableItemOptionTypeId() . ', ' .
	                    $this->sqlCompanyMediaFileId() . ', ' .
	                    $this->sqlSitePlanCompanyMediaFileId() . ', ' .
	                    $this->sqlRemotePrimaryKey() . ', ' .
	                    $this->sqlLookupCode() . ', ' .
	                    $this->sqlName() . ', ' .
	                    $this->sqlDescription() . ', ' .
	                    $this->sqlAllowFlexibleDates() . ', ' .
	                    $this->sqlHasInventory() . ', ' .
	                    $this->sqlIsPublished() . ', ' .
	                    $this->sqlOrderNum() . ', ' .
	                    $this->sqlDeletedBy() . ', ' .
	                    $this->sqlDeletedOn() . ', ' .
	                    (int) $intCurrentUserId . ', ' .
	                    $this->sqlUpdatedOn() . ', ' .
	                    (int) $intCurrentUserId . ', ' .
	                    $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if ( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate()) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }

        $strSql = 'UPDATE
                      public.rentable_item_options
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid()) != $this->getOriginalValueByFieldName ( 'cid' )) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_cid = ' . $this->sqlDefaultCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDefaultCid()) != $this->getOriginalValueByFieldName ( 'default_cid' )) { $arrstrOriginalValueChanges['default_cid'] = $this->sqlDefaultCid(); $strSql .= ' default_cid = ' . $this->sqlDefaultCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_rentable_item_option_type_id = ' . $this->sqlPropertyRentableItemOptionTypeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPropertyRentableItemOptionTypeId()) != $this->getOriginalValueByFieldName ( 'property_rentable_item_option_type_id' )) { $arrstrOriginalValueChanges['property_rentable_item_option_type_id'] = $this->sqlPropertyRentableItemOptionTypeId(); $strSql .= ' property_rentable_item_option_type_id = ' . $this->sqlPropertyRentableItemOptionTypeId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCompanyMediaFileId()) != $this->getOriginalValueByFieldName ( 'company_media_file_id' )) { $arrstrOriginalValueChanges['company_media_file_id'] = $this->sqlCompanyMediaFileId(); $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' site_plan_company_media_file_id = ' . $this->sqlSitePlanCompanyMediaFileId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlSitePlanCompanyMediaFileId()) != $this->getOriginalValueByFieldName ( 'site_plan_company_media_file_id' )) { $arrstrOriginalValueChanges['site_plan_company_media_file_id'] = $this->sqlSitePlanCompanyMediaFileId(); $strSql .= ' site_plan_company_media_file_id = ' . $this->sqlSitePlanCompanyMediaFileId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlRemotePrimaryKey()) != $this->getOriginalValueByFieldName ( 'remote_primary_key' )) { $arrstrOriginalValueChanges['remote_primary_key'] = $this->sqlRemotePrimaryKey(); $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lookup_code = ' . $this->sqlLookupCode() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlLookupCode()) != $this->getOriginalValueByFieldName ( 'lookup_code' )) { $arrstrOriginalValueChanges['lookup_code'] = $this->sqlLookupCode(); $strSql .= ' lookup_code = ' . $this->sqlLookupCode() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlName()) != $this->getOriginalValueByFieldName ( 'name' )) { $arrstrOriginalValueChanges['name'] = $this->sqlName(); $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDescription()) != $this->getOriginalValueByFieldName ( 'description' )) { $arrstrOriginalValueChanges['description'] = $this->sqlDescription(); $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_flexible_dates = ' . $this->sqlAllowFlexibleDates() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlAllowFlexibleDates()) != $this->getOriginalValueByFieldName ( 'allow_flexible_dates' )) { $arrstrOriginalValueChanges['allow_flexible_dates'] = $this->sqlAllowFlexibleDates(); $strSql .= ' allow_flexible_dates = ' . $this->sqlAllowFlexibleDates() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_inventory = ' . $this->sqlHasInventory() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlHasInventory()) != $this->getOriginalValueByFieldName ( 'has_inventory' )) { $arrstrOriginalValueChanges['has_inventory'] = $this->sqlHasInventory(); $strSql .= ' has_inventory = ' . $this->sqlHasInventory() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsPublished()) != $this->getOriginalValueByFieldName ( 'is_published' )) { $arrstrOriginalValueChanges['is_published'] = $this->sqlIsPublished(); $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlOrderNum()) != $this->getOriginalValueByFieldName ( 'order_num' )) { $arrstrOriginalValueChanges['order_num'] = $this->sqlOrderNum(); $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDeletedBy()) != $this->getOriginalValueByFieldName ( 'deleted_by' )) { $arrstrOriginalValueChanges['deleted_by'] = $this->sqlDeletedBy(); $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDeletedOn()) != $this->getOriginalValueByFieldName ( 'deleted_on' )) { $arrstrOriginalValueChanges['deleted_on'] = $this->sqlDeletedOn(); $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
                      $strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
                      $strSql .= ' updated_on = \'NOW()\'
					WHERE
					    id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
        } else {
			if( true == $boolUpdate ){
			    if( true == $this->executeSql( $strSql, $this, $objDatabase )){
				    if( true == $this->getAllowDifferentialUpdate() ) {
				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );return true;
				    }
			    } else {
			        return false;
				}
			}
			return true;
		}
	}

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.rentable_item_options WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if ( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase ) {
        return parent::fetchNextId( 'public.rentable_item_options_id_seq', $objDatabase );
    }
}
?>