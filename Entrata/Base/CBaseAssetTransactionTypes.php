<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAssetTransactionTypes
 * Do not add any new functions to this class.
 */

class CBaseAssetTransactionTypes extends CEosPluralBase {

	/**
	 * @return CAssetTransactionType[]
	 */
	public static function fetchAssetTransactionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CAssetTransactionType::class, $objDatabase );
	}

	/**
	 * @return CAssetTransactionType
	 */
	public static function fetchAssetTransactionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CAssetTransactionType::class, $objDatabase );
	}

	public static function fetchAssetTransactionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'asset_transaction_types', $objDatabase );
	}

	public static function fetchAssetTransactionTypeById( $intId, $objDatabase ) {
		return self::fetchAssetTransactionType( sprintf( 'SELECT * FROM asset_transaction_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>