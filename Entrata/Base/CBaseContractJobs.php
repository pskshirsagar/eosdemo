<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseContractJobs extends CEosPluralBase {

	public static function fetchContractJobs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CContractJob', $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchContractJob( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CContractJob', $objDatabase );
	}

	public static function fetchContractJobCount( $strWhere = NULL, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'contract_jobs', $objDatabase );
	}

	public static function fetchContractJobByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchContractJob( sprintf( 'SELECT * FROM contract_jobs WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
	}

	public static function fetchContractJobsByCid( $intCid, $objDatabase ) {
		return self::fetchContractJobs( sprintf( 'SELECT * FROM contract_jobs WHERE cid = %d', (int) $intCid ), $objDatabase );
	}

	public static function fetchContractJobsByContractIdByCid( $intContractId, $intCid, $objDatabase ) {
		return self::fetchContractJobs( sprintf( 'SELECT * FROM contract_jobs WHERE contract_id = %d AND cid = %d', (int) $intContractId, (int) $intCid ), $objDatabase );
	}

	public static function fetchContractJobsByJobIdByCid( $intJobId, $intCid, $objDatabase ) {
		return self::fetchContractJobs( sprintf( 'SELECT * FROM contract_jobs WHERE job_id = %d AND cid = %d', (int) $intJobId, (int) $intCid ), $objDatabase );
	}

	public static function fetchContractJobsByJobPhaseIdByCid( $intJobPhaseId, $intCid, $objDatabase ) {
		return self::fetchContractJobs( sprintf( 'SELECT * FROM contract_jobs WHERE job_phase_id = %d AND cid = %d', (int) $intJobPhaseId, (int) $intCid ), $objDatabase );
	}

}
?>