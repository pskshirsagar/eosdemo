<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidySpecialClaim extends CEosSingularBase {

	const TABLE_NAME = 'public.subsidy_special_claims';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSubsidySpecialClaimTypeId;
	protected $m_intSubsidySpecialClaimStatusTypeId;
	protected $m_intSubsidyHapRequestId;
	protected $m_strData;
	protected $m_strRemotePrimaryKey;
	protected $m_strNotes;
	protected $m_fltTotalAmount;
	protected $m_fltMaxAllowableClaim;
	protected $m_fltAdjustedAmount;
	protected $m_strAdjustmentReason;
	protected $m_strDenialReason;
	protected $m_strApprovedDate;
	protected $m_strViewedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intSubsidySpecialClaimStatusTypeId = '1';
		$this->m_strCreatedOn = 'now()';
		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['subsidy_special_claim_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidySpecialClaimTypeId', trim( $arrValues['subsidy_special_claim_type_id'] ) ); elseif( isset( $arrValues['subsidy_special_claim_type_id'] ) ) $this->setSubsidySpecialClaimTypeId( $arrValues['subsidy_special_claim_type_id'] );
		if( isset( $arrValues['subsidy_special_claim_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidySpecialClaimStatusTypeId', trim( $arrValues['subsidy_special_claim_status_type_id'] ) ); elseif( isset( $arrValues['subsidy_special_claim_status_type_id'] ) ) $this->setSubsidySpecialClaimStatusTypeId( $arrValues['subsidy_special_claim_status_type_id'] );
		if( isset( $arrValues['subsidy_hap_request_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyHapRequestId', trim( $arrValues['subsidy_hap_request_id'] ) ); elseif( isset( $arrValues['subsidy_hap_request_id'] ) ) $this->setSubsidyHapRequestId( $arrValues['subsidy_hap_request_id'] );
		if( isset( $arrValues['data'] ) && $boolDirectSet ) $this->set( 'm_strData', trim( stripcslashes( $arrValues['data'] ) ) ); elseif( isset( $arrValues['data'] ) ) $this->setData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['data'] ) : $arrValues['data'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['total_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalAmount', trim( $arrValues['total_amount'] ) ); elseif( isset( $arrValues['total_amount'] ) ) $this->setTotalAmount( $arrValues['total_amount'] );
		if( isset( $arrValues['max_allowable_claim'] ) && $boolDirectSet ) $this->set( 'm_fltMaxAllowableClaim', trim( $arrValues['max_allowable_claim'] ) ); elseif( isset( $arrValues['max_allowable_claim'] ) ) $this->setMaxAllowableClaim( $arrValues['max_allowable_claim'] );
		if( isset( $arrValues['adjusted_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAdjustedAmount', trim( $arrValues['adjusted_amount'] ) ); elseif( isset( $arrValues['adjusted_amount'] ) ) $this->setAdjustedAmount( $arrValues['adjusted_amount'] );
		if( isset( $arrValues['adjustment_reason'] ) && $boolDirectSet ) $this->set( 'm_strAdjustmentReason', trim( stripcslashes( $arrValues['adjustment_reason'] ) ) ); elseif( isset( $arrValues['adjustment_reason'] ) ) $this->setAdjustmentReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['adjustment_reason'] ) : $arrValues['adjustment_reason'] );
		if( isset( $arrValues['denial_reason'] ) && $boolDirectSet ) $this->set( 'm_strDenialReason', trim( stripcslashes( $arrValues['denial_reason'] ) ) ); elseif( isset( $arrValues['denial_reason'] ) ) $this->setDenialReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['denial_reason'] ) : $arrValues['denial_reason'] );
		if( isset( $arrValues['approved_date'] ) && $boolDirectSet ) $this->set( 'm_strApprovedDate', trim( $arrValues['approved_date'] ) ); elseif( isset( $arrValues['approved_date'] ) ) $this->setApprovedDate( $arrValues['approved_date'] );
		if( isset( $arrValues['viewed_on'] ) && $boolDirectSet ) $this->set( 'm_strViewedOn', trim( $arrValues['viewed_on'] ) ); elseif( isset( $arrValues['viewed_on'] ) ) $this->setViewedOn( $arrValues['viewed_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSubsidySpecialClaimTypeId( $intSubsidySpecialClaimTypeId ) {
		$this->set( 'm_intSubsidySpecialClaimTypeId', CStrings::strToIntDef( $intSubsidySpecialClaimTypeId, NULL, false ) );
	}

	public function getSubsidySpecialClaimTypeId() {
		return $this->m_intSubsidySpecialClaimTypeId;
	}

	public function sqlSubsidySpecialClaimTypeId() {
		return ( true == isset( $this->m_intSubsidySpecialClaimTypeId ) ) ? ( string ) $this->m_intSubsidySpecialClaimTypeId : 'NULL';
	}

	public function setSubsidySpecialClaimStatusTypeId( $intSubsidySpecialClaimStatusTypeId ) {
		$this->set( 'm_intSubsidySpecialClaimStatusTypeId', CStrings::strToIntDef( $intSubsidySpecialClaimStatusTypeId, NULL, false ) );
	}

	public function getSubsidySpecialClaimStatusTypeId() {
		return $this->m_intSubsidySpecialClaimStatusTypeId;
	}

	public function sqlSubsidySpecialClaimStatusTypeId() {
		return ( true == isset( $this->m_intSubsidySpecialClaimStatusTypeId ) ) ? ( string ) $this->m_intSubsidySpecialClaimStatusTypeId : '1';
	}

	public function setSubsidyHapRequestId( $intSubsidyHapRequestId ) {
		$this->set( 'm_intSubsidyHapRequestId', CStrings::strToIntDef( $intSubsidyHapRequestId, NULL, false ) );
	}

	public function getSubsidyHapRequestId() {
		return $this->m_intSubsidyHapRequestId;
	}

	public function sqlSubsidyHapRequestId() {
		return ( true == isset( $this->m_intSubsidyHapRequestId ) ) ? ( string ) $this->m_intSubsidyHapRequestId : 'NULL';
	}

	public function setData( $strData ) {
		$this->set( 'm_strData', CStrings::strTrimDef( $strData, -1, NULL, true ) );
	}

	public function getData() {
		return $this->m_strData;
	}

	public function sqlData() {
		return ( true == isset( $this->m_strData ) ) ? '\'' . addslashes( $this->m_strData ) . '\'' : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 20, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setTotalAmount( $fltTotalAmount ) {
		$this->set( 'm_fltTotalAmount', CStrings::strToFloatDef( $fltTotalAmount, NULL, false, 2 ) );
	}

	public function getTotalAmount() {
		return $this->m_fltTotalAmount;
	}

	public function sqlTotalAmount() {
		return ( true == isset( $this->m_fltTotalAmount ) ) ? ( string ) $this->m_fltTotalAmount : 'NULL';
	}

	public function setMaxAllowableClaim( $fltMaxAllowableClaim ) {
		$this->set( 'm_fltMaxAllowableClaim', CStrings::strToFloatDef( $fltMaxAllowableClaim, NULL, false, 2 ) );
	}

	public function getMaxAllowableClaim() {
		return $this->m_fltMaxAllowableClaim;
	}

	public function sqlMaxAllowableClaim() {
		return ( true == isset( $this->m_fltMaxAllowableClaim ) ) ? ( string ) $this->m_fltMaxAllowableClaim : 'NULL';
	}

	public function setAdjustedAmount( $fltAdjustedAmount ) {
		$this->set( 'm_fltAdjustedAmount', CStrings::strToFloatDef( $fltAdjustedAmount, NULL, false, 2 ) );
	}

	public function getAdjustedAmount() {
		return $this->m_fltAdjustedAmount;
	}

	public function sqlAdjustedAmount() {
		return ( true == isset( $this->m_fltAdjustedAmount ) ) ? ( string ) $this->m_fltAdjustedAmount : 'NULL';
	}

	public function setAdjustmentReason( $strAdjustmentReason ) {
		$this->set( 'm_strAdjustmentReason', CStrings::strTrimDef( $strAdjustmentReason, -1, NULL, true ) );
	}

	public function getAdjustmentReason() {
		return $this->m_strAdjustmentReason;
	}

	public function sqlAdjustmentReason() {
		return ( true == isset( $this->m_strAdjustmentReason ) ) ? '\'' . addslashes( $this->m_strAdjustmentReason ) . '\'' : 'NULL';
	}

	public function setDenialReason( $strDenialReason ) {
		$this->set( 'm_strDenialReason', CStrings::strTrimDef( $strDenialReason, -1, NULL, true ) );
	}

	public function getDenialReason() {
		return $this->m_strDenialReason;
	}

	public function sqlDenialReason() {
		return ( true == isset( $this->m_strDenialReason ) ) ? '\'' . addslashes( $this->m_strDenialReason ) . '\'' : 'NULL';
	}

	public function setApprovedDate( $strApprovedDate ) {
		$this->set( 'm_strApprovedDate', CStrings::strTrimDef( $strApprovedDate, -1, NULL, true ) );
	}

	public function getApprovedDate() {
		return $this->m_strApprovedDate;
	}

	public function sqlApprovedDate() {
		return ( true == isset( $this->m_strApprovedDate ) ) ? '\'' . $this->m_strApprovedDate . '\'' : 'NULL';
	}

	public function setViewedOn( $strViewedOn ) {
		$this->set( 'm_strViewedOn', CStrings::strTrimDef( $strViewedOn, -1, NULL, true ) );
	}

	public function getViewedOn() {
		return $this->m_strViewedOn;
	}

	public function sqlViewedOn() {
		return ( true == isset( $this->m_strViewedOn ) ) ? '\'' . $this->m_strViewedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, subsidy_special_claim_type_id, subsidy_special_claim_status_type_id, subsidy_hap_request_id, data, remote_primary_key, notes, total_amount, max_allowable_claim, adjusted_amount, adjustment_reason, denial_reason, approved_date, viewed_on, created_by, created_on, updated_by, updated_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlSubsidySpecialClaimTypeId() . ', ' .
 						$this->sqlSubsidySpecialClaimStatusTypeId() . ', ' .
 						$this->sqlSubsidyHapRequestId() . ', ' .
 						$this->sqlData() . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlTotalAmount() . ', ' .
 						$this->sqlMaxAllowableClaim() . ', ' .
 						$this->sqlAdjustedAmount() . ', ' .
 						$this->sqlAdjustmentReason() . ', ' .
 						$this->sqlDenialReason() . ', ' .
 						$this->sqlApprovedDate() . ', ' .
 						$this->sqlViewedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_special_claim_type_id = ' . $this->sqlSubsidySpecialClaimTypeId() . ','; } elseif( true == array_key_exists( 'SubsidySpecialClaimTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_special_claim_type_id = ' . $this->sqlSubsidySpecialClaimTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_special_claim_status_type_id = ' . $this->sqlSubsidySpecialClaimStatusTypeId() . ','; } elseif( true == array_key_exists( 'SubsidySpecialClaimStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_special_claim_status_type_id = ' . $this->sqlSubsidySpecialClaimStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_hap_request_id = ' . $this->sqlSubsidyHapRequestId() . ','; } elseif( true == array_key_exists( 'SubsidyHapRequestId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_hap_request_id = ' . $this->sqlSubsidyHapRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data = ' . $this->sqlData() . ','; } elseif( true == array_key_exists( 'Data', $this->getChangedColumns() ) ) { $strSql .= ' data = ' . $this->sqlData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_amount = ' . $this->sqlTotalAmount() . ','; } elseif( true == array_key_exists( 'TotalAmount', $this->getChangedColumns() ) ) { $strSql .= ' total_amount = ' . $this->sqlTotalAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_allowable_claim = ' . $this->sqlMaxAllowableClaim() . ','; } elseif( true == array_key_exists( 'MaxAllowableClaim', $this->getChangedColumns() ) ) { $strSql .= ' max_allowable_claim = ' . $this->sqlMaxAllowableClaim() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' adjusted_amount = ' . $this->sqlAdjustedAmount() . ','; } elseif( true == array_key_exists( 'AdjustedAmount', $this->getChangedColumns() ) ) { $strSql .= ' adjusted_amount = ' . $this->sqlAdjustedAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' adjustment_reason = ' . $this->sqlAdjustmentReason() . ','; } elseif( true == array_key_exists( 'AdjustmentReason', $this->getChangedColumns() ) ) { $strSql .= ' adjustment_reason = ' . $this->sqlAdjustmentReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' denial_reason = ' . $this->sqlDenialReason() . ','; } elseif( true == array_key_exists( 'DenialReason', $this->getChangedColumns() ) ) { $strSql .= ' denial_reason = ' . $this->sqlDenialReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_date = ' . $this->sqlApprovedDate() . ','; } elseif( true == array_key_exists( 'ApprovedDate', $this->getChangedColumns() ) ) { $strSql .= ' approved_date = ' . $this->sqlApprovedDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' viewed_on = ' . $this->sqlViewedOn() . ','; } elseif( true == array_key_exists( 'ViewedOn', $this->getChangedColumns() ) ) { $strSql .= ' viewed_on = ' . $this->sqlViewedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'subsidy_special_claim_type_id' => $this->getSubsidySpecialClaimTypeId(),
			'subsidy_special_claim_status_type_id' => $this->getSubsidySpecialClaimStatusTypeId(),
			'subsidy_hap_request_id' => $this->getSubsidyHapRequestId(),
			'data' => $this->getData(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'notes' => $this->getNotes(),
			'total_amount' => $this->getTotalAmount(),
			'max_allowable_claim' => $this->getMaxAllowableClaim(),
			'adjusted_amount' => $this->getAdjustedAmount(),
			'adjustment_reason' => $this->getAdjustmentReason(),
			'denial_reason' => $this->getDenialReason(),
			'approved_date' => $this->getApprovedDate(),
			'viewed_on' => $this->getViewedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn()
		);
	}

}
?>