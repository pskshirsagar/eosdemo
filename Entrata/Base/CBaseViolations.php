<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CViolations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseViolations extends CEosPluralBase {

	/**
	 * @return CViolation[]
	 */
	public static function fetchViolations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CViolation::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CViolation
	 */
	public static function fetchViolation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CViolation::class, $objDatabase );
	}

	public static function fetchViolationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'violations', $objDatabase );
	}

	public static function fetchViolationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchViolation( sprintf( 'SELECT * FROM violations WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchViolationsByCid( $intCid, $objDatabase ) {
		return self::fetchViolations( sprintf( 'SELECT * FROM violations WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchViolationsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchViolations( sprintf( 'SELECT * FROM violations WHERE lease_id = %d AND cid = %d', $intLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchViolationsByViolationTemplateIdByCid( $intViolationTemplateId, $intCid, $objDatabase ) {
		return self::fetchViolations( sprintf( 'SELECT * FROM violations WHERE violation_template_id = %d AND cid = %d', $intViolationTemplateId, $intCid ), $objDatabase );
	}

	public static function fetchViolationsByViolationTypeIdByCid( $intViolationTypeId, $intCid, $objDatabase ) {
		return self::fetchViolations( sprintf( 'SELECT * FROM violations WHERE violation_type_id = %d AND cid = %d', $intViolationTypeId, $intCid ), $objDatabase );
	}

	public static function fetchViolationsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchViolations( sprintf( 'SELECT * FROM violations WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

}
?>