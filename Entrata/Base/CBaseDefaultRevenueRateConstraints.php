<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultRevenueRateConstraints
 * Do not add any new functions to this class.
 */

class CBaseDefaultRevenueRateConstraints extends CEosPluralBase {

	/**
	 * @return CDefaultRevenueRateConstraint[]
	 */
	public static function fetchDefaultRevenueRateConstraints( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDefaultRevenueRateConstraint', $objDatabase );
	}

	/**
	 * @return CDefaultRevenueRateConstraint
	 */
	public static function fetchDefaultRevenueRateConstraint( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDefaultRevenueRateConstraint', $objDatabase );
	}

	public static function fetchDefaultRevenueRateConstraintCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_revenue_rate_constraints', $objDatabase );
	}

	public static function fetchDefaultRevenueRateConstraintById( $intId, $objDatabase ) {
		return self::fetchDefaultRevenueRateConstraint( sprintf( 'SELECT * FROM default_revenue_rate_constraints WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDefaultRevenueRateConstraintsByRevenueRateConstraintTypeId( $intRevenueRateConstraintTypeId, $objDatabase ) {
		return self::fetchDefaultRevenueRateConstraints( sprintf( 'SELECT * FROM default_revenue_rate_constraints WHERE revenue_rate_constraint_type_id = %d', ( int ) $intRevenueRateConstraintTypeId ), $objDatabase );
	}

}
?>