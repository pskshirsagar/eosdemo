<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CExportActivityLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseExportActivityLogs extends CEosPluralBase {

	/**
	 * @return CExportActivityLog[]
	 */
	public static function fetchExportActivityLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CExportActivityLog::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CExportActivityLog
	 */
	public static function fetchExportActivityLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CExportActivityLog::class, $objDatabase );
	}

	public static function fetchExportActivityLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'export_activity_logs', $objDatabase );
	}

	public static function fetchExportActivityLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchExportActivityLog( sprintf( 'SELECT * FROM export_activity_logs WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchExportActivityLogsByCid( $intCid, $objDatabase ) {
		return self::fetchExportActivityLogs( sprintf( 'SELECT * FROM export_activity_logs WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchExportActivityLogsByAccountingExportBatchIdByCid( $intAccountingExportBatchId, $intCid, $objDatabase ) {
		return self::fetchExportActivityLogs( sprintf( 'SELECT * FROM export_activity_logs WHERE accounting_export_batch_id = %d AND cid = %d', $intAccountingExportBatchId, $intCid ), $objDatabase );
	}

}
?>