<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningEventTypes
 * Do not add any new functions to this class.
 */

class CBaseScreeningEventTypes extends CEosPluralBase {

	/**
	 * @return CScreeningEventType[]
	 */
	public static function fetchScreeningEventTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScreeningEventType::class, $objDatabase );
	}

	/**
	 * @return CScreeningEventType
	 */
	public static function fetchScreeningEventType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningEventType::class, $objDatabase );
	}

	public static function fetchScreeningEventTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_event_types', $objDatabase );
	}

	public static function fetchScreeningEventTypeById( $intId, $objDatabase ) {
		return self::fetchScreeningEventType( sprintf( 'SELECT * FROM screening_event_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>