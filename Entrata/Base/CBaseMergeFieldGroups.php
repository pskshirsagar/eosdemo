<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMergeFieldGroups
 * Do not add any new functions to this class.
 */

class CBaseMergeFieldGroups extends CEosPluralBase {

	/**
	 * @return CMergeFieldGroup[]
	 */
	public static function fetchMergeFieldGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMergeFieldGroup::class, $objDatabase );
	}

	/**
	 * @return CMergeFieldGroup
	 */
	public static function fetchMergeFieldGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMergeFieldGroup::class, $objDatabase );
	}

	public static function fetchMergeFieldGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'merge_field_groups', $objDatabase );
	}

	public static function fetchMergeFieldGroupById( $intId, $objDatabase ) {
		return self::fetchMergeFieldGroup( sprintf( 'SELECT * FROM merge_field_groups WHERE id = %d', $intId ), $objDatabase );
	}

}
?>