<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COffers
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOffers extends CEosPluralBase {

	/**
	 * @return COffer[]
	 */
	public static function fetchOffers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, COffer::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return COffer
	 */
	public static function fetchOffer( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, COffer::class, $objDatabase );
	}

	public static function fetchOfferCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'offers', $objDatabase );
	}

	public static function fetchOfferByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchOffer( sprintf( 'SELECT * FROM offers WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOffersByCid( $intCid, $objDatabase ) {
		return self::fetchOffers( sprintf( 'SELECT * FROM offers WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOffersByOfferTemplateIdByCid( $intOfferTemplateId, $intCid, $objDatabase ) {
		return self::fetchOffers( sprintf( 'SELECT * FROM offers WHERE offer_template_id = %d AND cid = %d', ( int ) $intOfferTemplateId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOffersByImportOfferIdByCid( $intImportOfferId, $intCid, $objDatabase ) {
		return self::fetchOffers( sprintf( 'SELECT * FROM offers WHERE import_offer_id = %d AND cid = %d', ( int ) $intImportOfferId, ( int ) $intCid ), $objDatabase );
	}

}
?>