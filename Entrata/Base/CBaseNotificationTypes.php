<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CNotificationTypes
 * Do not add any new functions to this class.
 */

class CBaseNotificationTypes extends CEosPluralBase {

	/**
	 * @return CNotificationType[]
	 */
	public static function fetchNotificationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CNotificationType::class, $objDatabase );
	}

	/**
	 * @return CNotificationType
	 */
	public static function fetchNotificationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CNotificationType::class, $objDatabase );
	}

	public static function fetchNotificationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'notification_types', $objDatabase );
	}

	public static function fetchNotificationTypeById( $intId, $objDatabase ) {
		return self::fetchNotificationType( sprintf( 'SELECT * FROM notification_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>