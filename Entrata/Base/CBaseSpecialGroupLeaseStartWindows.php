<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSpecialGroupLeaseStartWindows
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSpecialGroupLeaseStartWindows extends CEosPluralBase {

	/**
	 * @return CSpecialGroupLeaseStartWindow[]
	 */
	public static function fetchSpecialGroupLeaseStartWindows( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CSpecialGroupLeaseStartWindow', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSpecialGroupLeaseStartWindow
	 */
	public static function fetchSpecialGroupLeaseStartWindow( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSpecialGroupLeaseStartWindow', $objDatabase );
	}

	public static function fetchSpecialGroupLeaseStartWindowCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'special_group_lease_start_windows', $objDatabase );
	}

	public static function fetchSpecialGroupLeaseStartWindowByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSpecialGroupLeaseStartWindow( sprintf( 'SELECT * FROM special_group_lease_start_windows WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSpecialGroupLeaseStartWindowsByCid( $intCid, $objDatabase ) {
		return self::fetchSpecialGroupLeaseStartWindows( sprintf( 'SELECT * FROM special_group_lease_start_windows WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSpecialGroupLeaseStartWindowsBySpecialGroupIdByCid( $intSpecialGroupId, $intCid, $objDatabase ) {
		return self::fetchSpecialGroupLeaseStartWindows( sprintf( 'SELECT * FROM special_group_lease_start_windows WHERE special_group_id = %d AND cid = %d', ( int ) $intSpecialGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSpecialGroupLeaseStartWindowsByLeaseTermIdByCid( $intLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchSpecialGroupLeaseStartWindows( sprintf( 'SELECT * FROM special_group_lease_start_windows WHERE lease_term_id = %d AND cid = %d', ( int ) $intLeaseTermId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSpecialGroupLeaseStartWindowsByLeaseStartWindowIdByCid( $intLeaseStartWindowId, $intCid, $objDatabase ) {
		return self::fetchSpecialGroupLeaseStartWindows( sprintf( 'SELECT * FROM special_group_lease_start_windows WHERE lease_start_window_id = %d AND cid = %d', ( int ) $intLeaseStartWindowId, ( int ) $intCid ), $objDatabase );
	}

}
?>