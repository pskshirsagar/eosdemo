<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApContracts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApContracts extends CEosPluralBase {

	/**
	 * @return CApContract[]
	 */
	public static function fetchApContracts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApContract', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApContract
	 */
	public static function fetchApContract( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApContract', $objDatabase );
	}

	public static function fetchApContractCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_contracts', $objDatabase );
	}

	public static function fetchApContractByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApContract( sprintf( 'SELECT * FROM ap_contracts WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApContractsByCid( $intCid, $objDatabase ) {
		return self::fetchApContracts( sprintf( 'SELECT * FROM ap_contracts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApContractsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchApContracts( sprintf( 'SELECT * FROM ap_contracts WHERE ap_payee_id = %d AND cid = %d', ( int ) $intApPayeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApContractsByApContractStatusIdByCid( $intApContractStatusId, $intCid, $objDatabase ) {
		return self::fetchApContracts( sprintf( 'SELECT * FROM ap_contracts WHERE ap_contract_status_id = %d AND cid = %d', ( int ) $intApContractStatusId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApContractsByProposalIdByCid( $intProposalId, $intCid, $objDatabase ) {
		return self::fetchApContracts( sprintf( 'SELECT * FROM ap_contracts WHERE proposal_id = %d AND cid = %d', ( int ) $intProposalId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApContractsByJobIdByCid( $intJobId, $intCid, $objDatabase ) {
		return self::fetchApContracts( sprintf( 'SELECT * FROM ap_contracts WHERE job_id = %d AND cid = %d', ( int ) $intJobId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApContractsByBudgetSummaryApHeaderIdByCid( $intBudgetSummaryApHeaderId, $intCid, $objDatabase ) {
		return self::fetchApContracts( sprintf( 'SELECT * FROM ap_contracts WHERE budget_summary_ap_header_id = %d AND cid = %d', ( int ) $intBudgetSummaryApHeaderId, ( int ) $intCid ), $objDatabase );
	}

}
?>