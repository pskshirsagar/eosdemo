<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyMergeFieldExceptions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyMergeFieldExceptions extends CEosPluralBase {

	/**
	 * @return CCompanyMergeFieldException[]
	 */
	public static function fetchCompanyMergeFieldExceptions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCompanyMergeFieldException::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyMergeFieldException
	 */
	public static function fetchCompanyMergeFieldException( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyMergeFieldException::class, $objDatabase );
	}

	public static function fetchCompanyMergeFieldExceptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_merge_field_exceptions', $objDatabase );
	}

	public static function fetchCompanyMergeFieldExceptionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyMergeFieldException( sprintf( 'SELECT * FROM company_merge_field_exceptions WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyMergeFieldExceptionsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyMergeFieldExceptions( sprintf( 'SELECT * FROM company_merge_field_exceptions WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCompanyMergeFieldExceptionsByCompanyMergeFieldIdByCid( $intCompanyMergeFieldId, $intCid, $objDatabase ) {
		return self::fetchCompanyMergeFieldExceptions( sprintf( 'SELECT * FROM company_merge_field_exceptions WHERE company_merge_field_id = %d AND cid = %d', $intCompanyMergeFieldId, $intCid ), $objDatabase );
	}

}
?>