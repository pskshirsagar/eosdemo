<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CProposalDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseProposalDetails extends CEosPluralBase {

	/**
	 * @return CProposalDetail[]
	 */
	public static function fetchProposalDetails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CProposalDetail', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CProposalDetail
	 */
	public static function fetchProposalDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CProposalDetail', $objDatabase );
	}

	public static function fetchProposalDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'proposal_details', $objDatabase );
	}

	public static function fetchProposalDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchProposalDetail( sprintf( 'SELECT * FROM proposal_details WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchProposalDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchProposalDetails( sprintf( 'SELECT * FROM proposal_details WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchProposalDetailsByProposalIdByCid( $intProposalId, $intCid, $objDatabase ) {
		return self::fetchProposalDetails( sprintf( 'SELECT * FROM proposal_details WHERE proposal_id = %d AND cid = %d', ( int ) $intProposalId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchProposalDetailsByApCodeIdByCid( $intApCodeId, $intCid, $objDatabase ) {
		return self::fetchProposalDetails( sprintf( 'SELECT * FROM proposal_details WHERE ap_code_id = %d AND cid = %d', ( int ) $intApCodeId, ( int ) $intCid ), $objDatabase );
	}

}
?>