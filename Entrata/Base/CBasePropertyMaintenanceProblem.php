<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyMaintenanceProblem extends CEosSingularBase {

	const TABLE_NAME = 'public.property_maintenance_problems';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intMaintenanceProblemId;
	protected $m_intPropertyMaintenanceProblemId;
	protected $m_intForcedMaintenancePriorityId;
	protected $m_intEmployeeId;
	protected $m_strRemotePrimaryKey;
	protected $m_intSendEmail;
	protected $m_intSendSms;
	protected $m_intShowInResidentPortal;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intSendEmail = '0';
		$this->m_intSendSms = '0';
		$this->m_intShowInResidentPortal = '1';
		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['maintenance_problem_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceProblemId', trim( $arrValues['maintenance_problem_id'] ) ); elseif( isset( $arrValues['maintenance_problem_id'] ) ) $this->setMaintenanceProblemId( $arrValues['maintenance_problem_id'] );
		if( isset( $arrValues['property_maintenance_problem_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyMaintenanceProblemId', trim( $arrValues['property_maintenance_problem_id'] ) ); elseif( isset( $arrValues['property_maintenance_problem_id'] ) ) $this->setPropertyMaintenanceProblemId( $arrValues['property_maintenance_problem_id'] );
		if( isset( $arrValues['forced_maintenance_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intForcedMaintenancePriorityId', trim( $arrValues['forced_maintenance_priority_id'] ) ); elseif( isset( $arrValues['forced_maintenance_priority_id'] ) ) $this->setForcedMaintenancePriorityId( $arrValues['forced_maintenance_priority_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['send_email'] ) && $boolDirectSet ) $this->set( 'm_intSendEmail', trim( $arrValues['send_email'] ) ); elseif( isset( $arrValues['send_email'] ) ) $this->setSendEmail( $arrValues['send_email'] );
		if( isset( $arrValues['send_sms'] ) && $boolDirectSet ) $this->set( 'm_intSendSms', trim( $arrValues['send_sms'] ) ); elseif( isset( $arrValues['send_sms'] ) ) $this->setSendSms( $arrValues['send_sms'] );
		if( isset( $arrValues['show_in_resident_portal'] ) && $boolDirectSet ) $this->set( 'm_intShowInResidentPortal', trim( $arrValues['show_in_resident_portal'] ) ); elseif( isset( $arrValues['show_in_resident_portal'] ) ) $this->setShowInResidentPortal( $arrValues['show_in_resident_portal'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setMaintenanceProblemId( $intMaintenanceProblemId ) {
		$this->set( 'm_intMaintenanceProblemId', CStrings::strToIntDef( $intMaintenanceProblemId, NULL, false ) );
	}

	public function getMaintenanceProblemId() {
		return $this->m_intMaintenanceProblemId;
	}

	public function sqlMaintenanceProblemId() {
		return ( true == isset( $this->m_intMaintenanceProblemId ) ) ? ( string ) $this->m_intMaintenanceProblemId : 'NULL';
	}

	public function setPropertyMaintenanceProblemId( $intPropertyMaintenanceProblemId ) {
		$this->set( 'm_intPropertyMaintenanceProblemId', CStrings::strToIntDef( $intPropertyMaintenanceProblemId, NULL, false ) );
	}

	public function getPropertyMaintenanceProblemId() {
		return $this->m_intPropertyMaintenanceProblemId;
	}

	public function sqlPropertyMaintenanceProblemId() {
		return ( true == isset( $this->m_intPropertyMaintenanceProblemId ) ) ? ( string ) $this->m_intPropertyMaintenanceProblemId : 'NULL';
	}

	public function setForcedMaintenancePriorityId( $intForcedMaintenancePriorityId ) {
		$this->set( 'm_intForcedMaintenancePriorityId', CStrings::strToIntDef( $intForcedMaintenancePriorityId, NULL, false ) );
	}

	public function getForcedMaintenancePriorityId() {
		return $this->m_intForcedMaintenancePriorityId;
	}

	public function sqlForcedMaintenancePriorityId() {
		return ( true == isset( $this->m_intForcedMaintenancePriorityId ) ) ? ( string ) $this->m_intForcedMaintenancePriorityId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setSendEmail( $intSendEmail ) {
		$this->set( 'm_intSendEmail', CStrings::strToIntDef( $intSendEmail, NULL, false ) );
	}

	public function getSendEmail() {
		return $this->m_intSendEmail;
	}

	public function sqlSendEmail() {
		return ( true == isset( $this->m_intSendEmail ) ) ? ( string ) $this->m_intSendEmail : '0';
	}

	public function setSendSms( $intSendSms ) {
		$this->set( 'm_intSendSms', CStrings::strToIntDef( $intSendSms, NULL, false ) );
	}

	public function getSendSms() {
		return $this->m_intSendSms;
	}

	public function sqlSendSms() {
		return ( true == isset( $this->m_intSendSms ) ) ? ( string ) $this->m_intSendSms : '0';
	}

	public function setShowInResidentPortal( $intShowInResidentPortal ) {
		$this->set( 'm_intShowInResidentPortal', CStrings::strToIntDef( $intShowInResidentPortal, NULL, false ) );
	}

	public function getShowInResidentPortal() {
		return $this->m_intShowInResidentPortal;
	}

	public function sqlShowInResidentPortal() {
		return ( true == isset( $this->m_intShowInResidentPortal ) ) ? ( string ) $this->m_intShowInResidentPortal : '1';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, maintenance_problem_id, property_maintenance_problem_id, forced_maintenance_priority_id, employee_id, remote_primary_key, send_email, send_sms, show_in_resident_portal, is_published, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlMaintenanceProblemId() . ', ' .
 						$this->sqlPropertyMaintenanceProblemId() . ', ' .
 						$this->sqlForcedMaintenancePriorityId() . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlSendEmail() . ', ' .
 						$this->sqlSendSms() . ', ' .
 						$this->sqlShowInResidentPortal() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_problem_id = ' . $this->sqlMaintenanceProblemId() . ','; } elseif( true == array_key_exists( 'MaintenanceProblemId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_problem_id = ' . $this->sqlMaintenanceProblemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_maintenance_problem_id = ' . $this->sqlPropertyMaintenanceProblemId() . ','; } elseif( true == array_key_exists( 'PropertyMaintenanceProblemId', $this->getChangedColumns() ) ) { $strSql .= ' property_maintenance_problem_id = ' . $this->sqlPropertyMaintenanceProblemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' forced_maintenance_priority_id = ' . $this->sqlForcedMaintenancePriorityId() . ','; } elseif( true == array_key_exists( 'ForcedMaintenancePriorityId', $this->getChangedColumns() ) ) { $strSql .= ' forced_maintenance_priority_id = ' . $this->sqlForcedMaintenancePriorityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' send_email = ' . $this->sqlSendEmail() . ','; } elseif( true == array_key_exists( 'SendEmail', $this->getChangedColumns() ) ) { $strSql .= ' send_email = ' . $this->sqlSendEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' send_sms = ' . $this->sqlSendSms() . ','; } elseif( true == array_key_exists( 'SendSms', $this->getChangedColumns() ) ) { $strSql .= ' send_sms = ' . $this->sqlSendSms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_in_resident_portal = ' . $this->sqlShowInResidentPortal() . ','; } elseif( true == array_key_exists( 'ShowInResidentPortal', $this->getChangedColumns() ) ) { $strSql .= ' show_in_resident_portal = ' . $this->sqlShowInResidentPortal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'maintenance_problem_id' => $this->getMaintenanceProblemId(),
			'property_maintenance_problem_id' => $this->getPropertyMaintenanceProblemId(),
			'forced_maintenance_priority_id' => $this->getForcedMaintenancePriorityId(),
			'employee_id' => $this->getEmployeeId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'send_email' => $this->getSendEmail(),
			'send_sms' => $this->getSendSms(),
			'show_in_resident_portal' => $this->getShowInResidentPortal(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>