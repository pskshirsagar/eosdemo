<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CContactTypeCategories
 * Do not add any new functions to this class.
 */

class CBaseContactTypeCategories extends CEosPluralBase {

	/**
	 * @return CContactTypeCategory[]
	 */
	public static function fetchContactTypeCategories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CContactTypeCategory::class, $objDatabase );
	}

	/**
	 * @return CContactTypeCategory
	 */
	public static function fetchContactTypeCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CContactTypeCategory::class, $objDatabase );
	}

	public static function fetchContactTypeCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'contact_type_categories', $objDatabase );
	}

	public static function fetchContactTypeCategoryById( $intId, $objDatabase ) {
		return self::fetchContactTypeCategory( sprintf( 'SELECT * FROM contact_type_categories WHERE id = %d', $intId ), $objDatabase );
	}

}
?>