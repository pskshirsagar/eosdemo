<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileMergeValues
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFileMergeValues extends CEosPluralBase {

	/**
	 * @return CFileMergeValue[]
	 */
	public static function fetchFileMergeValues( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CFileMergeValue', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFileMergeValue
	 */
	public static function fetchFileMergeValue( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CFileMergeValue', $objDatabase );
	}

	public static function fetchFileMergeValueCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'file_merge_values', $objDatabase );
	}

	public static function fetchFileMergeValueByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFileMergeValue( sprintf( 'SELECT * FROM file_merge_values WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileMergeValuesByCid( $intCid, $objDatabase ) {
		return self::fetchFileMergeValues( sprintf( 'SELECT * FROM file_merge_values WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileMergeValuesByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchFileMergeValues( sprintf( 'SELECT * FROM file_merge_values WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileMergeValuesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchFileMergeValues( sprintf( 'SELECT * FROM file_merge_values WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileMergeValuesByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		return self::fetchFileMergeValues( sprintf( 'SELECT * FROM file_merge_values WHERE document_id = %d AND cid = %d', ( int ) $intDocumentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileMergeValuesByDocumentAddendaIdByCid( $intDocumentAddendaId, $intCid, $objDatabase ) {
		return self::fetchFileMergeValues( sprintf( 'SELECT * FROM file_merge_values WHERE document_addenda_id = %d AND cid = %d', ( int ) $intDocumentAddendaId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileMergeValuesByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		return self::fetchFileMergeValues( sprintf( 'SELECT * FROM file_merge_values WHERE file_id = %d AND cid = %d', ( int ) $intFileId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileMergeValuesByBlockFileMergeValueIdByCid( $intBlockFileMergeValueId, $intCid, $objDatabase ) {
		return self::fetchFileMergeValues( sprintf( 'SELECT * FROM file_merge_values WHERE block_file_merge_value_id = %d AND cid = %d', ( int ) $intBlockFileMergeValueId, ( int ) $intCid ), $objDatabase );
	}

}
?>