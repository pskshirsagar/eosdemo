<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceRequestLabor extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.maintenance_request_labors';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intMaintenanceRequestId;
	protected $m_intCompanyEmployeeId;
	protected $m_strServices;
	protected $m_strTotalLabor;
	protected $m_fltPerHourRate;
	protected $m_intMaintenanceRequestLaborTypeId;
	protected $m_fltRateMultiplier;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strLaborStartDatetime;
	protected $m_strLaborEndDatetime;

	public function __construct() {
		parent::__construct();

		$this->m_strTotalLabor = '00:00:00';
		$this->m_fltPerHourRate = '0';
		$this->m_intMaintenanceRequestLaborTypeId = '1';
		$this->m_fltRateMultiplier = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['maintenance_request_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceRequestId', trim( $arrValues['maintenance_request_id'] ) ); elseif( isset( $arrValues['maintenance_request_id'] ) ) $this->setMaintenanceRequestId( $arrValues['maintenance_request_id'] );
		if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
		if( isset( $arrValues['services'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strServices', trim( $arrValues['services'] ) ); elseif( isset( $arrValues['services'] ) ) $this->setServices( $arrValues['services'] );
		if( isset( $arrValues['total_labor'] ) && $boolDirectSet ) $this->set( 'm_strTotalLabor', trim( $arrValues['total_labor'] ) ); elseif( isset( $arrValues['total_labor'] ) ) $this->setTotalLabor( $arrValues['total_labor'] );
		if( isset( $arrValues['per_hour_rate'] ) && $boolDirectSet ) $this->set( 'm_fltPerHourRate', trim( $arrValues['per_hour_rate'] ) ); elseif( isset( $arrValues['per_hour_rate'] ) ) $this->setPerHourRate( $arrValues['per_hour_rate'] );
		if( isset( $arrValues['maintenance_request_labor_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceRequestLaborTypeId', trim( $arrValues['maintenance_request_labor_type_id'] ) ); elseif( isset( $arrValues['maintenance_request_labor_type_id'] ) ) $this->setMaintenanceRequestLaborTypeId( $arrValues['maintenance_request_labor_type_id'] );
		if( isset( $arrValues['rate_multiplier'] ) && $boolDirectSet ) $this->set( 'm_fltRateMultiplier', trim( $arrValues['rate_multiplier'] ) ); elseif( isset( $arrValues['rate_multiplier'] ) ) $this->setRateMultiplier( $arrValues['rate_multiplier'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['labor_start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLaborStartDatetime', trim( $arrValues['labor_start_datetime'] ) ); elseif( isset( $arrValues['labor_start_datetime'] ) ) $this->setLaborStartDatetime( $arrValues['labor_start_datetime'] );
		if( isset( $arrValues['labor_end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLaborEndDatetime', trim( $arrValues['labor_end_datetime'] ) ); elseif( isset( $arrValues['labor_end_datetime'] ) ) $this->setLaborEndDatetime( $arrValues['labor_end_datetime'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setMaintenanceRequestId( $intMaintenanceRequestId ) {
		$this->set( 'm_intMaintenanceRequestId', CStrings::strToIntDef( $intMaintenanceRequestId, NULL, false ) );
	}

	public function getMaintenanceRequestId() {
		return $this->m_intMaintenanceRequestId;
	}

	public function sqlMaintenanceRequestId() {
		return ( true == isset( $this->m_intMaintenanceRequestId ) ) ? ( string ) $this->m_intMaintenanceRequestId : 'NULL';
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function sqlCompanyEmployeeId() {
		return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
	}

	public function setServices( $strServices, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strServices', CStrings::strTrimDef( $strServices, -1, NULL, true ), $strLocaleCode );
	}

	public function getServices( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strServices', $strLocaleCode );
	}

	public function sqlServices() {
		return ( true == isset( $this->m_strServices ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strServices ) : '\'' . addslashes( $this->m_strServices ) . '\'' ) : 'NULL';
	}

	public function setTotalLabor( $strTotalLabor ) {
		$this->set( 'm_strTotalLabor', CStrings::strTrimDef( $strTotalLabor, NULL, NULL, true ) );
	}

	public function getTotalLabor() {
		return $this->m_strTotalLabor;
	}

	public function sqlTotalLabor() {
		return ( true == isset( $this->m_strTotalLabor ) ) ? '\'' . addslashes( $this->m_strTotalLabor ) . '\'' : 'NULL';
	}

	public function setPerHourRate( $fltPerHourRate ) {
		$this->set( 'm_fltPerHourRate', CStrings::strToFloatDef( $fltPerHourRate, NULL, false, 2 ) );
	}

	public function getPerHourRate() {
		return $this->m_fltPerHourRate;
	}

	public function sqlPerHourRate() {
		return ( true == isset( $this->m_fltPerHourRate ) ) ? ( string ) $this->m_fltPerHourRate : '0';
	}

	public function setMaintenanceRequestLaborTypeId( $intMaintenanceRequestLaborTypeId ) {
		$this->set( 'm_intMaintenanceRequestLaborTypeId', CStrings::strToIntDef( $intMaintenanceRequestLaborTypeId, NULL, false ) );
	}

	public function getMaintenanceRequestLaborTypeId() {
		return $this->m_intMaintenanceRequestLaborTypeId;
	}

	public function sqlMaintenanceRequestLaborTypeId() {
		return ( true == isset( $this->m_intMaintenanceRequestLaborTypeId ) ) ? ( string ) $this->m_intMaintenanceRequestLaborTypeId : '1';
	}

	public function setRateMultiplier( $fltRateMultiplier ) {
		$this->set( 'm_fltRateMultiplier', CStrings::strToFloatDef( $fltRateMultiplier, NULL, false, 2 ) );
	}

	public function getRateMultiplier() {
		return $this->m_fltRateMultiplier;
	}

	public function sqlRateMultiplier() {
		return ( true == isset( $this->m_fltRateMultiplier ) ) ? ( string ) $this->m_fltRateMultiplier : '1';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setLaborStartDatetime( $strLaborStartDatetime ) {
		$this->set( 'm_strLaborStartDatetime', CStrings::strTrimDef( $strLaborStartDatetime, -1, NULL, true ) );
	}

	public function getLaborStartDatetime() {
		return $this->m_strLaborStartDatetime;
	}

	public function sqlLaborStartDatetime() {
		return ( true == isset( $this->m_strLaborStartDatetime ) ) ? '\'' . $this->m_strLaborStartDatetime . '\'' : 'NULL';
	}

	public function setLaborEndDatetime( $strLaborEndDatetime ) {
		$this->set( 'm_strLaborEndDatetime', CStrings::strTrimDef( $strLaborEndDatetime, -1, NULL, true ) );
	}

	public function getLaborEndDatetime() {
		return $this->m_strLaborEndDatetime;
	}

	public function sqlLaborEndDatetime() {
		return ( true == isset( $this->m_strLaborEndDatetime ) ) ? '\'' . $this->m_strLaborEndDatetime . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, maintenance_request_id, company_employee_id, services, total_labor, per_hour_rate, maintenance_request_labor_type_id, rate_multiplier, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details, labor_start_datetime, labor_end_datetime )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlMaintenanceRequestId() . ', ' .
		          $this->sqlCompanyEmployeeId() . ', ' .
		          $this->sqlServices() . ', ' .
		          $this->sqlTotalLabor() . ', ' .
		          $this->sqlPerHourRate() . ', ' .
		          $this->sqlMaintenanceRequestLaborTypeId() . ', ' .
		          $this->sqlRateMultiplier() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlDetails() . ', ' .
		          $this->sqlLaborStartDatetime() . ', ' .
		          $this->sqlLaborEndDatetime() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceRequestId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId(). ',' ; } elseif( true == array_key_exists( 'CompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' services = ' . $this->sqlServices(). ',' ; } elseif( true == array_key_exists( 'Services', $this->getChangedColumns() ) ) { $strSql .= ' services = ' . $this->sqlServices() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_labor = ' . $this->sqlTotalLabor(). ',' ; } elseif( true == array_key_exists( 'TotalLabor', $this->getChangedColumns() ) ) { $strSql .= ' total_labor = ' . $this->sqlTotalLabor() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' per_hour_rate = ' . $this->sqlPerHourRate(). ',' ; } elseif( true == array_key_exists( 'PerHourRate', $this->getChangedColumns() ) ) { $strSql .= ' per_hour_rate = ' . $this->sqlPerHourRate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_request_labor_type_id = ' . $this->sqlMaintenanceRequestLaborTypeId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceRequestLaborTypeId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_request_labor_type_id = ' . $this->sqlMaintenanceRequestLaborTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate_multiplier = ' . $this->sqlRateMultiplier(). ',' ; } elseif( true == array_key_exists( 'RateMultiplier', $this->getChangedColumns() ) ) { $strSql .= ' rate_multiplier = ' . $this->sqlRateMultiplier() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' labor_start_datetime = ' . $this->sqlLaborStartDatetime(). ',' ; } elseif( true == array_key_exists( 'LaborStartDatetime', $this->getChangedColumns() ) ) { $strSql .= ' labor_start_datetime = ' . $this->sqlLaborStartDatetime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' labor_end_datetime = ' . $this->sqlLaborEndDatetime(). ',' ; } elseif( true == array_key_exists( 'LaborEndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' labor_end_datetime = ' . $this->sqlLaborEndDatetime() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'maintenance_request_id' => $this->getMaintenanceRequestId(),
			'company_employee_id' => $this->getCompanyEmployeeId(),
			'services' => $this->getServices(),
			'total_labor' => $this->getTotalLabor(),
			'per_hour_rate' => $this->getPerHourRate(),
			'maintenance_request_labor_type_id' => $this->getMaintenanceRequestLaborTypeId(),
			'rate_multiplier' => $this->getRateMultiplier(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'labor_start_datetime' => $this->getLaborStartDatetime(),
			'labor_end_datetime' => $this->getLaborEndDatetime()
		);
	}

}
?>