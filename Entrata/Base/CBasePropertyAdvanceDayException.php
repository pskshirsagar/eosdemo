<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyAdvanceDayException extends CEosSingularBase {

	const TABLE_NAME = 'public.property_advance_day_exceptions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intMonth;
	protected $m_intArAdvanceDay;
	protected $m_intApAdvanceDay;
	protected $m_intGlAdvanceDay;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['month'] ) && $boolDirectSet ) $this->set( 'm_intMonth', trim( $arrValues['month'] ) ); elseif( isset( $arrValues['month'] ) ) $this->setMonth( $arrValues['month'] );
		if( isset( $arrValues['ar_advance_day'] ) && $boolDirectSet ) $this->set( 'm_intArAdvanceDay', trim( $arrValues['ar_advance_day'] ) ); elseif( isset( $arrValues['ar_advance_day'] ) ) $this->setArAdvanceDay( $arrValues['ar_advance_day'] );
		if( isset( $arrValues['ap_advance_day'] ) && $boolDirectSet ) $this->set( 'm_intApAdvanceDay', trim( $arrValues['ap_advance_day'] ) ); elseif( isset( $arrValues['ap_advance_day'] ) ) $this->setApAdvanceDay( $arrValues['ap_advance_day'] );
		if( isset( $arrValues['gl_advance_day'] ) && $boolDirectSet ) $this->set( 'm_intGlAdvanceDay', trim( $arrValues['gl_advance_day'] ) ); elseif( isset( $arrValues['gl_advance_day'] ) ) $this->setGlAdvanceDay( $arrValues['gl_advance_day'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setMonth( $intMonth ) {
		$this->set( 'm_intMonth', CStrings::strToIntDef( $intMonth, NULL, false ) );
	}

	public function getMonth() {
		return $this->m_intMonth;
	}

	public function sqlMonth() {
		return ( true == isset( $this->m_intMonth ) ) ? ( string ) $this->m_intMonth : 'NULL';
	}

	public function setArAdvanceDay( $intArAdvanceDay ) {
		$this->set( 'm_intArAdvanceDay', CStrings::strToIntDef( $intArAdvanceDay, NULL, false ) );
	}

	public function getArAdvanceDay() {
		return $this->m_intArAdvanceDay;
	}

	public function sqlArAdvanceDay() {
		return ( true == isset( $this->m_intArAdvanceDay ) ) ? ( string ) $this->m_intArAdvanceDay : 'NULL';
	}

	public function setApAdvanceDay( $intApAdvanceDay ) {
		$this->set( 'm_intApAdvanceDay', CStrings::strToIntDef( $intApAdvanceDay, NULL, false ) );
	}

	public function getApAdvanceDay() {
		return $this->m_intApAdvanceDay;
	}

	public function sqlApAdvanceDay() {
		return ( true == isset( $this->m_intApAdvanceDay ) ) ? ( string ) $this->m_intApAdvanceDay : 'NULL';
	}

	public function setGlAdvanceDay( $intGlAdvanceDay ) {
		$this->set( 'm_intGlAdvanceDay', CStrings::strToIntDef( $intGlAdvanceDay, NULL, false ) );
	}

	public function getGlAdvanceDay() {
		return $this->m_intGlAdvanceDay;
	}

	public function sqlGlAdvanceDay() {
		return ( true == isset( $this->m_intGlAdvanceDay ) ) ? ( string ) $this->m_intGlAdvanceDay : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, month, ar_advance_day, ap_advance_day, gl_advance_day, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlMonth() . ', ' .
 						$this->sqlArAdvanceDay() . ', ' .
 						$this->sqlApAdvanceDay() . ', ' .
 						$this->sqlGlAdvanceDay() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; } elseif( true == array_key_exists( 'Month', $this->getChangedColumns() ) ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_advance_day = ' . $this->sqlArAdvanceDay() . ','; } elseif( true == array_key_exists( 'ArAdvanceDay', $this->getChangedColumns() ) ) { $strSql .= ' ar_advance_day = ' . $this->sqlArAdvanceDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_advance_day = ' . $this->sqlApAdvanceDay() . ','; } elseif( true == array_key_exists( 'ApAdvanceDay', $this->getChangedColumns() ) ) { $strSql .= ' ap_advance_day = ' . $this->sqlApAdvanceDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_advance_day = ' . $this->sqlGlAdvanceDay() . ','; } elseif( true == array_key_exists( 'GlAdvanceDay', $this->getChangedColumns() ) ) { $strSql .= ' gl_advance_day = ' . $this->sqlGlAdvanceDay() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'month' => $this->getMonth(),
			'ar_advance_day' => $this->getArAdvanceDay(),
			'ap_advance_day' => $this->getApAdvanceDay(),
			'gl_advance_day' => $this->getGlAdvanceDay(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>