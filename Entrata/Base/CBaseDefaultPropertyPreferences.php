<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultPropertyPreferences
 * Do not add any new functions to this class.
 */

class CBaseDefaultPropertyPreferences extends CEosPluralBase {

	/**
	 * @return CDefaultPropertyPreference[]
	 */
	public static function fetchDefaultPropertyPreferences( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDefaultPropertyPreference', $objDatabase );
	}

	/**
	 * @return CDefaultPropertyPreference
	 */
	public static function fetchDefaultPropertyPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDefaultPropertyPreference', $objDatabase );
	}

	public static function fetchDefaultPropertyPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_property_preferences', $objDatabase );
	}

	public static function fetchDefaultPropertyPreferenceById( $intId, $objDatabase ) {
		return self::fetchDefaultPropertyPreference( sprintf( 'SELECT * FROM default_property_preferences WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>