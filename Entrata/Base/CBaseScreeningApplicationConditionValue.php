<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningApplicationConditionValue extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_application_condition_values';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScreeningApplicationConditionSetId;
	protected $m_intConditionSubsetId;
	protected $m_intConditionSubsetValueId;
	protected $m_intScreeningAvailableConditionId;
	protected $m_strConditionName;
	protected $m_intConditionTypeId;
	protected $m_intConditionValueTypeId;
	protected $m_intConditionAmountTypeId;
	protected $m_boolProcessAutomatically;
	protected $m_intArTransactionId;
	protected $m_intApplicationRateId;
	protected $m_intScheduledChargeId;
	protected $m_strBaseChargeCodeId;
	protected $m_strApplyToChargeCodeId;
	protected $m_intArTriggerId;
	protected $m_strConditionValue;
	protected $m_strFinalValue;
	protected $m_intIsReverted;
	protected $m_intSatisfiedBy;
	protected $m_strSatisfiedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolProcessAutomatically = false;
		$this->m_intIsReverted = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['screening_application_condition_set_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningApplicationConditionSetId', trim( $arrValues['screening_application_condition_set_id'] ) ); elseif( isset( $arrValues['screening_application_condition_set_id'] ) ) $this->setScreeningApplicationConditionSetId( $arrValues['screening_application_condition_set_id'] );
		if( isset( $arrValues['condition_subset_id'] ) && $boolDirectSet ) $this->set( 'm_intConditionSubsetId', trim( $arrValues['condition_subset_id'] ) ); elseif( isset( $arrValues['condition_subset_id'] ) ) $this->setConditionSubsetId( $arrValues['condition_subset_id'] );
		if( isset( $arrValues['condition_subset_value_id'] ) && $boolDirectSet ) $this->set( 'm_intConditionSubsetValueId', trim( $arrValues['condition_subset_value_id'] ) ); elseif( isset( $arrValues['condition_subset_value_id'] ) ) $this->setConditionSubsetValueId( $arrValues['condition_subset_value_id'] );
		if( isset( $arrValues['screening_available_condition_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningAvailableConditionId', trim( $arrValues['screening_available_condition_id'] ) ); elseif( isset( $arrValues['screening_available_condition_id'] ) ) $this->setScreeningAvailableConditionId( $arrValues['screening_available_condition_id'] );
		if( isset( $arrValues['condition_name'] ) && $boolDirectSet ) $this->set( 'm_strConditionName', trim( stripcslashes( $arrValues['condition_name'] ) ) ); elseif( isset( $arrValues['condition_name'] ) ) $this->setConditionName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['condition_name'] ) : $arrValues['condition_name'] );
		if( isset( $arrValues['condition_type_id'] ) && $boolDirectSet ) $this->set( 'm_intConditionTypeId', trim( $arrValues['condition_type_id'] ) ); elseif( isset( $arrValues['condition_type_id'] ) ) $this->setConditionTypeId( $arrValues['condition_type_id'] );
		if( isset( $arrValues['condition_value_type_id'] ) && $boolDirectSet ) $this->set( 'm_intConditionValueTypeId', trim( $arrValues['condition_value_type_id'] ) ); elseif( isset( $arrValues['condition_value_type_id'] ) ) $this->setConditionValueTypeId( $arrValues['condition_value_type_id'] );
		if( isset( $arrValues['condition_amount_type_id'] ) && $boolDirectSet ) $this->set( 'm_intConditionAmountTypeId', trim( $arrValues['condition_amount_type_id'] ) ); elseif( isset( $arrValues['condition_amount_type_id'] ) ) $this->setConditionAmountTypeId( $arrValues['condition_amount_type_id'] );
		if( isset( $arrValues['process_automatically'] ) && $boolDirectSet ) $this->set( 'm_boolProcessAutomatically', trim( stripcslashes( $arrValues['process_automatically'] ) ) ); elseif( isset( $arrValues['process_automatically'] ) ) $this->setProcessAutomatically( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['process_automatically'] ) : $arrValues['process_automatically'] );
		if( isset( $arrValues['ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intArTransactionId', trim( $arrValues['ar_transaction_id'] ) ); elseif( isset( $arrValues['ar_transaction_id'] ) ) $this->setArTransactionId( $arrValues['ar_transaction_id'] );
		if( isset( $arrValues['application_rate_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationRateId', trim( $arrValues['application_rate_id'] ) ); elseif( isset( $arrValues['application_rate_id'] ) ) $this->setApplicationRateId( $arrValues['application_rate_id'] );
		if( isset( $arrValues['scheduled_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledChargeId', trim( $arrValues['scheduled_charge_id'] ) ); elseif( isset( $arrValues['scheduled_charge_id'] ) ) $this->setScheduledChargeId( $arrValues['scheduled_charge_id'] );
		if( isset( $arrValues['base_charge_code_id'] ) && $boolDirectSet ) $this->set( 'm_strBaseChargeCodeId', trim( stripcslashes( $arrValues['base_charge_code_id'] ) ) ); elseif( isset( $arrValues['base_charge_code_id'] ) ) $this->setBaseChargeCodeId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['base_charge_code_id'] ) : $arrValues['base_charge_code_id'] );
		if( isset( $arrValues['apply_to_charge_code_id'] ) && $boolDirectSet ) $this->set( 'm_strApplyToChargeCodeId', trim( stripcslashes( $arrValues['apply_to_charge_code_id'] ) ) ); elseif( isset( $arrValues['apply_to_charge_code_id'] ) ) $this->setApplyToChargeCodeId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['apply_to_charge_code_id'] ) : $arrValues['apply_to_charge_code_id'] );
		if( isset( $arrValues['ar_trigger_id'] ) && $boolDirectSet ) $this->set( 'm_intArTriggerId', trim( $arrValues['ar_trigger_id'] ) ); elseif( isset( $arrValues['ar_trigger_id'] ) ) $this->setArTriggerId( $arrValues['ar_trigger_id'] );
		if( isset( $arrValues['condition_value'] ) && $boolDirectSet ) $this->set( 'm_strConditionValue', trim( stripcslashes( $arrValues['condition_value'] ) ) ); elseif( isset( $arrValues['condition_value'] ) ) $this->setConditionValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['condition_value'] ) : $arrValues['condition_value'] );
		if( isset( $arrValues['final_value'] ) && $boolDirectSet ) $this->set( 'm_strFinalValue', trim( stripcslashes( $arrValues['final_value'] ) ) ); elseif( isset( $arrValues['final_value'] ) ) $this->setFinalValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['final_value'] ) : $arrValues['final_value'] );
		if( isset( $arrValues['is_reverted'] ) && $boolDirectSet ) $this->set( 'm_intIsReverted', trim( $arrValues['is_reverted'] ) ); elseif( isset( $arrValues['is_reverted'] ) ) $this->setIsReverted( $arrValues['is_reverted'] );
		if( isset( $arrValues['satisfied_by'] ) && $boolDirectSet ) $this->set( 'm_intSatisfiedBy', trim( $arrValues['satisfied_by'] ) ); elseif( isset( $arrValues['satisfied_by'] ) ) $this->setSatisfiedBy( $arrValues['satisfied_by'] );
		if( isset( $arrValues['satisfied_on'] ) && $boolDirectSet ) $this->set( 'm_strSatisfiedOn', trim( $arrValues['satisfied_on'] ) ); elseif( isset( $arrValues['satisfied_on'] ) ) $this->setSatisfiedOn( $arrValues['satisfied_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScreeningApplicationConditionSetId( $intScreeningApplicationConditionSetId ) {
		$this->set( 'm_intScreeningApplicationConditionSetId', CStrings::strToIntDef( $intScreeningApplicationConditionSetId, NULL, false ) );
	}

	public function getScreeningApplicationConditionSetId() {
		return $this->m_intScreeningApplicationConditionSetId;
	}

	public function sqlScreeningApplicationConditionSetId() {
		return ( true == isset( $this->m_intScreeningApplicationConditionSetId ) ) ? ( string ) $this->m_intScreeningApplicationConditionSetId : 'NULL';
	}

	public function setConditionSubsetId( $intConditionSubsetId ) {
		$this->set( 'm_intConditionSubsetId', CStrings::strToIntDef( $intConditionSubsetId, NULL, false ) );
	}

	public function getConditionSubsetId() {
		return $this->m_intConditionSubsetId;
	}

	public function sqlConditionSubsetId() {
		return ( true == isset( $this->m_intConditionSubsetId ) ) ? ( string ) $this->m_intConditionSubsetId : 'NULL';
	}

	public function setConditionSubsetValueId( $intConditionSubsetValueId ) {
		$this->set( 'm_intConditionSubsetValueId', CStrings::strToIntDef( $intConditionSubsetValueId, NULL, false ) );
	}

	public function getConditionSubsetValueId() {
		return $this->m_intConditionSubsetValueId;
	}

	public function sqlConditionSubsetValueId() {
		return ( true == isset( $this->m_intConditionSubsetValueId ) ) ? ( string ) $this->m_intConditionSubsetValueId : 'NULL';
	}

	public function setScreeningAvailableConditionId( $intScreeningAvailableConditionId ) {
		$this->set( 'm_intScreeningAvailableConditionId', CStrings::strToIntDef( $intScreeningAvailableConditionId, NULL, false ) );
	}

	public function getScreeningAvailableConditionId() {
		return $this->m_intScreeningAvailableConditionId;
	}

	public function sqlScreeningAvailableConditionId() {
		return ( true == isset( $this->m_intScreeningAvailableConditionId ) ) ? ( string ) $this->m_intScreeningAvailableConditionId : 'NULL';
	}

	public function setConditionName( $strConditionName ) {
		$this->set( 'm_strConditionName', CStrings::strTrimDef( $strConditionName, -1, NULL, true ) );
	}

	public function getConditionName() {
		return $this->m_strConditionName;
	}

	public function sqlConditionName() {
		return ( true == isset( $this->m_strConditionName ) ) ? '\'' . addslashes( $this->m_strConditionName ) . '\'' : 'NULL';
	}

	public function setConditionTypeId( $intConditionTypeId ) {
		$this->set( 'm_intConditionTypeId', CStrings::strToIntDef( $intConditionTypeId, NULL, false ) );
	}

	public function getConditionTypeId() {
		return $this->m_intConditionTypeId;
	}

	public function sqlConditionTypeId() {
		return ( true == isset( $this->m_intConditionTypeId ) ) ? ( string ) $this->m_intConditionTypeId : 'NULL';
	}

	public function setConditionValueTypeId( $intConditionValueTypeId ) {
		$this->set( 'm_intConditionValueTypeId', CStrings::strToIntDef( $intConditionValueTypeId, NULL, false ) );
	}

	public function getConditionValueTypeId() {
		return $this->m_intConditionValueTypeId;
	}

	public function sqlConditionValueTypeId() {
		return ( true == isset( $this->m_intConditionValueTypeId ) ) ? ( string ) $this->m_intConditionValueTypeId : 'NULL';
	}

	public function setConditionAmountTypeId( $intConditionAmountTypeId ) {
		$this->set( 'm_intConditionAmountTypeId', CStrings::strToIntDef( $intConditionAmountTypeId, NULL, false ) );
	}

	public function getConditionAmountTypeId() {
		return $this->m_intConditionAmountTypeId;
	}

	public function sqlConditionAmountTypeId() {
		return ( true == isset( $this->m_intConditionAmountTypeId ) ) ? ( string ) $this->m_intConditionAmountTypeId : 'NULL';
	}

	public function setProcessAutomatically( $boolProcessAutomatically ) {
		$this->set( 'm_boolProcessAutomatically', CStrings::strToBool( $boolProcessAutomatically ) );
	}

	public function getProcessAutomatically() {
		return $this->m_boolProcessAutomatically;
	}

	public function sqlProcessAutomatically() {
		return ( true == isset( $this->m_boolProcessAutomatically ) ) ? '\'' . ( true == ( bool ) $this->m_boolProcessAutomatically ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setArTransactionId( $intArTransactionId ) {
		$this->set( 'm_intArTransactionId', CStrings::strToIntDef( $intArTransactionId, NULL, false ) );
	}

	public function getArTransactionId() {
		return $this->m_intArTransactionId;
	}

	public function sqlArTransactionId() {
		return ( true == isset( $this->m_intArTransactionId ) ) ? ( string ) $this->m_intArTransactionId : 'NULL';
	}

	public function setApplicationRateId( $intApplicationRateId ) {
		$this->set( 'm_intApplicationRateId', CStrings::strToIntDef( $intApplicationRateId, NULL, false ) );
	}

	public function getApplicationRateId() {
		return $this->m_intApplicationRateId;
	}

	public function sqlApplicationRateId() {
		return ( true == isset( $this->m_intApplicationRateId ) ) ? ( string ) $this->m_intApplicationRateId : 'NULL';
	}

	public function setScheduledChargeId( $intScheduledChargeId ) {
		$this->set( 'm_intScheduledChargeId', CStrings::strToIntDef( $intScheduledChargeId, NULL, false ) );
	}

	public function getScheduledChargeId() {
		return $this->m_intScheduledChargeId;
	}

	public function sqlScheduledChargeId() {
		return ( true == isset( $this->m_intScheduledChargeId ) ) ? ( string ) $this->m_intScheduledChargeId : 'NULL';
	}

	public function setBaseChargeCodeId( $strBaseChargeCodeId ) {
		$this->set( 'm_strBaseChargeCodeId', CStrings::strTrimDef( $strBaseChargeCodeId, -1, NULL, true ) );
	}

	public function getBaseChargeCodeId() {
		return $this->m_strBaseChargeCodeId;
	}

	public function sqlBaseChargeCodeId() {
		return ( true == isset( $this->m_strBaseChargeCodeId ) ) ? '\'' . addslashes( $this->m_strBaseChargeCodeId ) . '\'' : 'NULL';
	}

	public function setApplyToChargeCodeId( $strApplyToChargeCodeId ) {
		$this->set( 'm_strApplyToChargeCodeId', CStrings::strTrimDef( $strApplyToChargeCodeId, -1, NULL, true ) );
	}

	public function getApplyToChargeCodeId() {
		return $this->m_strApplyToChargeCodeId;
	}

	public function sqlApplyToChargeCodeId() {
		return ( true == isset( $this->m_strApplyToChargeCodeId ) ) ? '\'' . addslashes( $this->m_strApplyToChargeCodeId ) . '\'' : 'NULL';
	}

	public function setArTriggerId( $intArTriggerId ) {
		$this->set( 'm_intArTriggerId', CStrings::strToIntDef( $intArTriggerId, NULL, false ) );
	}

	public function getArTriggerId() {
		return $this->m_intArTriggerId;
	}

	public function sqlArTriggerId() {
		return ( true == isset( $this->m_intArTriggerId ) ) ? ( string ) $this->m_intArTriggerId : 'NULL';
	}

	public function setConditionValue( $strConditionValue ) {
		$this->set( 'm_strConditionValue', CStrings::strTrimDef( $strConditionValue, -1, NULL, true ) );
	}

	public function getConditionValue() {
		return $this->m_strConditionValue;
	}

	public function sqlConditionValue() {
		return ( true == isset( $this->m_strConditionValue ) ) ? '\'' . addslashes( $this->m_strConditionValue ) . '\'' : 'NULL';
	}

	public function setFinalValue( $strFinalValue ) {
		$this->set( 'm_strFinalValue', CStrings::strTrimDef( $strFinalValue, -1, NULL, true ) );
	}

	public function getFinalValue() {
		return $this->m_strFinalValue;
	}

	public function sqlFinalValue() {
		return ( true == isset( $this->m_strFinalValue ) ) ? '\'' . addslashes( $this->m_strFinalValue ) . '\'' : 'NULL';
	}

	public function setIsReverted( $intIsReverted ) {
		$this->set( 'm_intIsReverted', CStrings::strToIntDef( $intIsReverted, NULL, false ) );
	}

	public function getIsReverted() {
		return $this->m_intIsReverted;
	}

	public function sqlIsReverted() {
		return ( true == isset( $this->m_intIsReverted ) ) ? ( string ) $this->m_intIsReverted : '0';
	}

	public function setSatisfiedBy( $intSatisfiedBy ) {
		$this->set( 'm_intSatisfiedBy', CStrings::strToIntDef( $intSatisfiedBy, NULL, false ) );
	}

	public function getSatisfiedBy() {
		return $this->m_intSatisfiedBy;
	}

	public function sqlSatisfiedBy() {
		return ( true == isset( $this->m_intSatisfiedBy ) ) ? ( string ) $this->m_intSatisfiedBy : 'NULL';
	}

	public function setSatisfiedOn( $strSatisfiedOn ) {
		$this->set( 'm_strSatisfiedOn', CStrings::strTrimDef( $strSatisfiedOn, -1, NULL, true ) );
	}

	public function getSatisfiedOn() {
		return $this->m_strSatisfiedOn;
	}

	public function sqlSatisfiedOn() {
		return ( true == isset( $this->m_strSatisfiedOn ) ) ? '\'' . $this->m_strSatisfiedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, screening_application_condition_set_id, condition_subset_id, condition_subset_value_id, screening_available_condition_id, condition_name, condition_type_id, condition_value_type_id, condition_amount_type_id, process_automatically, ar_transaction_id, application_rate_id, scheduled_charge_id, base_charge_code_id, apply_to_charge_code_id, ar_trigger_id, condition_value, final_value, is_reverted, satisfied_by, satisfied_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlScreeningApplicationConditionSetId() . ', ' .
		          $this->sqlConditionSubsetId() . ', ' .
		          $this->sqlConditionSubsetValueId() . ', ' .
		          $this->sqlScreeningAvailableConditionId() . ', ' .
		          $this->sqlConditionName() . ', ' .
		          $this->sqlConditionTypeId() . ', ' .
		          $this->sqlConditionValueTypeId() . ', ' .
		          $this->sqlConditionAmountTypeId() . ', ' .
		          $this->sqlProcessAutomatically() . ', ' .
		          $this->sqlArTransactionId() . ', ' .
		          $this->sqlApplicationRateId() . ', ' .
		          $this->sqlScheduledChargeId() . ', ' .
		          $this->sqlBaseChargeCodeId() . ', ' .
		          $this->sqlApplyToChargeCodeId() . ', ' .
		          $this->sqlArTriggerId() . ', ' .
		          $this->sqlConditionValue() . ', ' .
		          $this->sqlFinalValue() . ', ' .
		          $this->sqlIsReverted() . ', ' .
		          $this->sqlSatisfiedBy() . ', ' .
		          $this->sqlSatisfiedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_application_condition_set_id = ' . $this->sqlScreeningApplicationConditionSetId() . ','; } elseif( true == array_key_exists( 'ScreeningApplicationConditionSetId', $this->getChangedColumns() ) ) { $strSql .= ' screening_application_condition_set_id = ' . $this->sqlScreeningApplicationConditionSetId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' condition_subset_id = ' . $this->sqlConditionSubsetId() . ','; } elseif( true == array_key_exists( 'ConditionSubsetId', $this->getChangedColumns() ) ) { $strSql .= ' condition_subset_id = ' . $this->sqlConditionSubsetId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' condition_subset_value_id = ' . $this->sqlConditionSubsetValueId() . ','; } elseif( true == array_key_exists( 'ConditionSubsetValueId', $this->getChangedColumns() ) ) { $strSql .= ' condition_subset_value_id = ' . $this->sqlConditionSubsetValueId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_available_condition_id = ' . $this->sqlScreeningAvailableConditionId() . ','; } elseif( true == array_key_exists( 'ScreeningAvailableConditionId', $this->getChangedColumns() ) ) { $strSql .= ' screening_available_condition_id = ' . $this->sqlScreeningAvailableConditionId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' condition_name = ' . $this->sqlConditionName() . ','; } elseif( true == array_key_exists( 'ConditionName', $this->getChangedColumns() ) ) { $strSql .= ' condition_name = ' . $this->sqlConditionName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' condition_type_id = ' . $this->sqlConditionTypeId() . ','; } elseif( true == array_key_exists( 'ConditionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' condition_type_id = ' . $this->sqlConditionTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' condition_value_type_id = ' . $this->sqlConditionValueTypeId() . ','; } elseif( true == array_key_exists( 'ConditionValueTypeId', $this->getChangedColumns() ) ) { $strSql .= ' condition_value_type_id = ' . $this->sqlConditionValueTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' condition_amount_type_id = ' . $this->sqlConditionAmountTypeId() . ','; } elseif( true == array_key_exists( 'ConditionAmountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' condition_amount_type_id = ' . $this->sqlConditionAmountTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' process_automatically = ' . $this->sqlProcessAutomatically() . ','; } elseif( true == array_key_exists( 'ProcessAutomatically', $this->getChangedColumns() ) ) { $strSql .= ' process_automatically = ' . $this->sqlProcessAutomatically() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId() . ','; } elseif( true == array_key_exists( 'ArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_rate_id = ' . $this->sqlApplicationRateId() . ','; } elseif( true == array_key_exists( 'ApplicationRateId', $this->getChangedColumns() ) ) { $strSql .= ' application_rate_id = ' . $this->sqlApplicationRateId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_charge_id = ' . $this->sqlScheduledChargeId() . ','; } elseif( true == array_key_exists( 'ScheduledChargeId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_charge_id = ' . $this->sqlScheduledChargeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_charge_code_id = ' . $this->sqlBaseChargeCodeId() . ','; } elseif( true == array_key_exists( 'BaseChargeCodeId', $this->getChangedColumns() ) ) { $strSql .= ' base_charge_code_id = ' . $this->sqlBaseChargeCodeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_to_charge_code_id = ' . $this->sqlApplyToChargeCodeId() . ','; } elseif( true == array_key_exists( 'ApplyToChargeCodeId', $this->getChangedColumns() ) ) { $strSql .= ' apply_to_charge_code_id = ' . $this->sqlApplyToChargeCodeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId() . ','; } elseif( true == array_key_exists( 'ArTriggerId', $this->getChangedColumns() ) ) { $strSql .= ' ar_trigger_id = ' . $this->sqlArTriggerId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' condition_value = ' . $this->sqlConditionValue() . ','; } elseif( true == array_key_exists( 'ConditionValue', $this->getChangedColumns() ) ) { $strSql .= ' condition_value = ' . $this->sqlConditionValue() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' final_value = ' . $this->sqlFinalValue() . ','; } elseif( true == array_key_exists( 'FinalValue', $this->getChangedColumns() ) ) { $strSql .= ' final_value = ' . $this->sqlFinalValue() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reverted = ' . $this->sqlIsReverted() . ','; } elseif( true == array_key_exists( 'IsReverted', $this->getChangedColumns() ) ) { $strSql .= ' is_reverted = ' . $this->sqlIsReverted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' satisfied_by = ' . $this->sqlSatisfiedBy() . ','; } elseif( true == array_key_exists( 'SatisfiedBy', $this->getChangedColumns() ) ) { $strSql .= ' satisfied_by = ' . $this->sqlSatisfiedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' satisfied_on = ' . $this->sqlSatisfiedOn() . ','; } elseif( true == array_key_exists( 'SatisfiedOn', $this->getChangedColumns() ) ) { $strSql .= ' satisfied_on = ' . $this->sqlSatisfiedOn() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'screening_application_condition_set_id' => $this->getScreeningApplicationConditionSetId(),
			'condition_subset_id' => $this->getConditionSubsetId(),
			'condition_subset_value_id' => $this->getConditionSubsetValueId(),
			'screening_available_condition_id' => $this->getScreeningAvailableConditionId(),
			'condition_name' => $this->getConditionName(),
			'condition_type_id' => $this->getConditionTypeId(),
			'condition_value_type_id' => $this->getConditionValueTypeId(),
			'condition_amount_type_id' => $this->getConditionAmountTypeId(),
			'process_automatically' => $this->getProcessAutomatically(),
			'ar_transaction_id' => $this->getArTransactionId(),
			'application_rate_id' => $this->getApplicationRateId(),
			'scheduled_charge_id' => $this->getScheduledChargeId(),
			'base_charge_code_id' => $this->getBaseChargeCodeId(),
			'apply_to_charge_code_id' => $this->getApplyToChargeCodeId(),
			'ar_trigger_id' => $this->getArTriggerId(),
			'condition_value' => $this->getConditionValue(),
			'final_value' => $this->getFinalValue(),
			'is_reverted' => $this->getIsReverted(),
			'satisfied_by' => $this->getSatisfiedBy(),
			'satisfied_on' => $this->getSatisfiedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>