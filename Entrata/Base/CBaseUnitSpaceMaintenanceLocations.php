<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitSpaceMaintenanceLocations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitSpaceMaintenanceLocations extends CEosPluralBase {

	public static function fetchUnitSpaceMaintenanceLocations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CUnitSpaceMaintenanceLocation', $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchUnitSpaceMaintenanceLocation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUnitSpaceMaintenanceLocation', $objDatabase );
	}

	public static function fetchUnitSpaceMaintenanceLocationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'unit_space_maintenance_locations', $objDatabase );
	}

	public static function fetchUnitSpaceMaintenanceLocationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceMaintenanceLocation( sprintf( 'SELECT * FROM unit_space_maintenance_locations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceMaintenanceLocationsByCid( $intCid, $objDatabase ) {
		return self::fetchUnitSpaceMaintenanceLocations( sprintf( 'SELECT * FROM unit_space_maintenance_locations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceMaintenanceLocationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceMaintenanceLocations( sprintf( 'SELECT * FROM unit_space_maintenance_locations WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceMaintenanceLocationsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceMaintenanceLocations( sprintf( 'SELECT * FROM unit_space_maintenance_locations WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceMaintenanceLocationsByPropertyUnitMaintenanceLocationIdByCid( $intPropertyUnitMaintenanceLocationId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceMaintenanceLocations( sprintf( 'SELECT * FROM unit_space_maintenance_locations WHERE property_unit_maintenance_location_id = %d AND cid = %d', ( int ) $intPropertyUnitMaintenanceLocationId, ( int ) $intCid ), $objDatabase );
	}

}
?>