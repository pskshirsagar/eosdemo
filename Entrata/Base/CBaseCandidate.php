<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCandidate extends CEosSingularBase {

	const TABLE_NAME = 'public.candidates';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCareerApplicationId;
	protected $m_intCareerId;
	protected $m_intDocumentId;
	protected $m_intCandidateStatusTypeId;
	protected $m_intCompanyEmployeeId;
	protected $m_intCareerSourceId;
	protected $m_strSourceDescription;
	protected $m_strNamePrefix;
	protected $m_strNameFirst;
	protected $m_strNameMiddle;
	protected $m_strNameLast;
	protected $m_strNameSuffix;
	protected $m_strPhoneNumber;
	protected $m_strWorkNumber;
	protected $m_strMobileNumber;
	protected $m_strFaxNumber;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strPostalCode;
	protected $m_strTaxNumberEncrypted;
	protected $m_strJobShiftType;
	protected $m_strNotes;
	protected $m_intSubmittedBy;
	protected $m_strSubmittedOn;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intRejectedBy;
	protected $m_strRejectedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['career_application_id'] ) && $boolDirectSet ) $this->set( 'm_intCareerApplicationId', trim( $arrValues['career_application_id'] ) ); elseif( isset( $arrValues['career_application_id'] ) ) $this->setCareerApplicationId( $arrValues['career_application_id'] );
		if( isset( $arrValues['career_id'] ) && $boolDirectSet ) $this->set( 'm_intCareerId', trim( $arrValues['career_id'] ) ); elseif( isset( $arrValues['career_id'] ) ) $this->setCareerId( $arrValues['career_id'] );
		if( isset( $arrValues['document_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentId', trim( $arrValues['document_id'] ) ); elseif( isset( $arrValues['document_id'] ) ) $this->setDocumentId( $arrValues['document_id'] );
		if( isset( $arrValues['candidate_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCandidateStatusTypeId', trim( $arrValues['candidate_status_type_id'] ) ); elseif( isset( $arrValues['candidate_status_type_id'] ) ) $this->setCandidateStatusTypeId( $arrValues['candidate_status_type_id'] );
		if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
		if( isset( $arrValues['career_source_id'] ) && $boolDirectSet ) $this->set( 'm_intCareerSourceId', trim( $arrValues['career_source_id'] ) ); elseif( isset( $arrValues['career_source_id'] ) ) $this->setCareerSourceId( $arrValues['career_source_id'] );
		if( isset( $arrValues['source_description'] ) && $boolDirectSet ) $this->set( 'm_strSourceDescription', trim( stripcslashes( $arrValues['source_description'] ) ) ); elseif( isset( $arrValues['source_description'] ) ) $this->setSourceDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['source_description'] ) : $arrValues['source_description'] );
		if( isset( $arrValues['name_prefix'] ) && $boolDirectSet ) $this->set( 'm_strNamePrefix', trim( stripcslashes( $arrValues['name_prefix'] ) ) ); elseif( isset( $arrValues['name_prefix'] ) ) $this->setNamePrefix( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_prefix'] ) : $arrValues['name_prefix'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( stripcslashes( $arrValues['name_first'] ) ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_first'] ) : $arrValues['name_first'] );
		if( isset( $arrValues['name_middle'] ) && $boolDirectSet ) $this->set( 'm_strNameMiddle', trim( stripcslashes( $arrValues['name_middle'] ) ) ); elseif( isset( $arrValues['name_middle'] ) ) $this->setNameMiddle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_middle'] ) : $arrValues['name_middle'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( stripcslashes( $arrValues['name_last'] ) ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_last'] ) : $arrValues['name_last'] );
		if( isset( $arrValues['name_suffix'] ) && $boolDirectSet ) $this->set( 'm_strNameSuffix', trim( stripcslashes( $arrValues['name_suffix'] ) ) ); elseif( isset( $arrValues['name_suffix'] ) ) $this->setNameSuffix( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_suffix'] ) : $arrValues['name_suffix'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( stripcslashes( $arrValues['phone_number'] ) ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number'] ) : $arrValues['phone_number'] );
		if( isset( $arrValues['work_number'] ) && $boolDirectSet ) $this->set( 'm_strWorkNumber', trim( stripcslashes( $arrValues['work_number'] ) ) ); elseif( isset( $arrValues['work_number'] ) ) $this->setWorkNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['work_number'] ) : $arrValues['work_number'] );
		if( isset( $arrValues['mobile_number'] ) && $boolDirectSet ) $this->set( 'm_strMobileNumber', trim( stripcslashes( $arrValues['mobile_number'] ) ) ); elseif( isset( $arrValues['mobile_number'] ) ) $this->setMobileNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['mobile_number'] ) : $arrValues['mobile_number'] );
		if( isset( $arrValues['fax_number'] ) && $boolDirectSet ) $this->set( 'm_strFaxNumber', trim( stripcslashes( $arrValues['fax_number'] ) ) ); elseif( isset( $arrValues['fax_number'] ) ) $this->setFaxNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['fax_number'] ) : $arrValues['fax_number'] );
		if( isset( $arrValues['street_line1'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine1', trim( stripcslashes( $arrValues['street_line1'] ) ) ); elseif( isset( $arrValues['street_line1'] ) ) $this->setStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line1'] ) : $arrValues['street_line1'] );
		if( isset( $arrValues['street_line2'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine2', trim( stripcslashes( $arrValues['street_line2'] ) ) ); elseif( isset( $arrValues['street_line2'] ) ) $this->setStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line2'] ) : $arrValues['street_line2'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( stripcslashes( $arrValues['postal_code'] ) ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
		if( isset( $arrValues['tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberEncrypted', trim( stripcslashes( $arrValues['tax_number_encrypted'] ) ) ); elseif( isset( $arrValues['tax_number_encrypted'] ) ) $this->setTaxNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_number_encrypted'] ) : $arrValues['tax_number_encrypted'] );
		if( isset( $arrValues['job_shift_type'] ) && $boolDirectSet ) $this->set( 'm_strJobShiftType', trim( stripcslashes( $arrValues['job_shift_type'] ) ) ); elseif( isset( $arrValues['job_shift_type'] ) ) $this->setJobShiftType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['job_shift_type'] ) : $arrValues['job_shift_type'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['submitted_by'] ) && $boolDirectSet ) $this->set( 'm_intSubmittedBy', trim( $arrValues['submitted_by'] ) ); elseif( isset( $arrValues['submitted_by'] ) ) $this->setSubmittedBy( $arrValues['submitted_by'] );
		if( isset( $arrValues['submitted_on'] ) && $boolDirectSet ) $this->set( 'm_strSubmittedOn', trim( $arrValues['submitted_on'] ) ); elseif( isset( $arrValues['submitted_on'] ) ) $this->setSubmittedOn( $arrValues['submitted_on'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['rejected_by'] ) && $boolDirectSet ) $this->set( 'm_intRejectedBy', trim( $arrValues['rejected_by'] ) ); elseif( isset( $arrValues['rejected_by'] ) ) $this->setRejectedBy( $arrValues['rejected_by'] );
		if( isset( $arrValues['rejected_on'] ) && $boolDirectSet ) $this->set( 'm_strRejectedOn', trim( $arrValues['rejected_on'] ) ); elseif( isset( $arrValues['rejected_on'] ) ) $this->setRejectedOn( $arrValues['rejected_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCareerApplicationId( $intCareerApplicationId ) {
		$this->set( 'm_intCareerApplicationId', CStrings::strToIntDef( $intCareerApplicationId, NULL, false ) );
	}

	public function getCareerApplicationId() {
		return $this->m_intCareerApplicationId;
	}

	public function sqlCareerApplicationId() {
		return ( true == isset( $this->m_intCareerApplicationId ) ) ? ( string ) $this->m_intCareerApplicationId : 'NULL';
	}

	public function setCareerId( $intCareerId ) {
		$this->set( 'm_intCareerId', CStrings::strToIntDef( $intCareerId, NULL, false ) );
	}

	public function getCareerId() {
		return $this->m_intCareerId;
	}

	public function sqlCareerId() {
		return ( true == isset( $this->m_intCareerId ) ) ? ( string ) $this->m_intCareerId : 'NULL';
	}

	public function setDocumentId( $intDocumentId ) {
		$this->set( 'm_intDocumentId', CStrings::strToIntDef( $intDocumentId, NULL, false ) );
	}

	public function getDocumentId() {
		return $this->m_intDocumentId;
	}

	public function sqlDocumentId() {
		return ( true == isset( $this->m_intDocumentId ) ) ? ( string ) $this->m_intDocumentId : 'NULL';
	}

	public function setCandidateStatusTypeId( $intCandidateStatusTypeId ) {
		$this->set( 'm_intCandidateStatusTypeId', CStrings::strToIntDef( $intCandidateStatusTypeId, NULL, false ) );
	}

	public function getCandidateStatusTypeId() {
		return $this->m_intCandidateStatusTypeId;
	}

	public function sqlCandidateStatusTypeId() {
		return ( true == isset( $this->m_intCandidateStatusTypeId ) ) ? ( string ) $this->m_intCandidateStatusTypeId : 'NULL';
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function sqlCompanyEmployeeId() {
		return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
	}

	public function setCareerSourceId( $intCareerSourceId ) {
		$this->set( 'm_intCareerSourceId', CStrings::strToIntDef( $intCareerSourceId, NULL, false ) );
	}

	public function getCareerSourceId() {
		return $this->m_intCareerSourceId;
	}

	public function sqlCareerSourceId() {
		return ( true == isset( $this->m_intCareerSourceId ) ) ? ( string ) $this->m_intCareerSourceId : 'NULL';
	}

	public function setSourceDescription( $strSourceDescription ) {
		$this->set( 'm_strSourceDescription', CStrings::strTrimDef( $strSourceDescription, 240, NULL, true ) );
	}

	public function getSourceDescription() {
		return $this->m_strSourceDescription;
	}

	public function sqlSourceDescription() {
		return ( true == isset( $this->m_strSourceDescription ) ) ? '\'' . addslashes( $this->m_strSourceDescription ) . '\'' : 'NULL';
	}

	public function setNamePrefix( $strNamePrefix ) {
		$this->set( 'm_strNamePrefix', CStrings::strTrimDef( $strNamePrefix, 20, NULL, true ) );
	}

	public function getNamePrefix() {
		return $this->m_strNamePrefix;
	}

	public function sqlNamePrefix() {
		return ( true == isset( $this->m_strNamePrefix ) ) ? '\'' . addslashes( $this->m_strNamePrefix ) . '\'' : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 50, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? '\'' . addslashes( $this->m_strNameFirst ) . '\'' : 'NULL';
	}

	public function setNameMiddle( $strNameMiddle ) {
		$this->set( 'm_strNameMiddle', CStrings::strTrimDef( $strNameMiddle, 50, NULL, true ) );
	}

	public function getNameMiddle() {
		return $this->m_strNameMiddle;
	}

	public function sqlNameMiddle() {
		return ( true == isset( $this->m_strNameMiddle ) ) ? '\'' . addslashes( $this->m_strNameMiddle ) . '\'' : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 50, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? '\'' . addslashes( $this->m_strNameLast ) . '\'' : 'NULL';
	}

	public function setNameSuffix( $strNameSuffix ) {
		$this->set( 'm_strNameSuffix', CStrings::strTrimDef( $strNameSuffix, 20, NULL, true ) );
	}

	public function getNameSuffix() {
		return $this->m_strNameSuffix;
	}

	public function sqlNameSuffix() {
		return ( true == isset( $this->m_strNameSuffix ) ) ? '\'' . addslashes( $this->m_strNameSuffix ) . '\'' : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' : 'NULL';
	}

	public function setWorkNumber( $strWorkNumber ) {
		$this->set( 'm_strWorkNumber', CStrings::strTrimDef( $strWorkNumber, 30, NULL, true ) );
	}

	public function getWorkNumber() {
		return $this->m_strWorkNumber;
	}

	public function sqlWorkNumber() {
		return ( true == isset( $this->m_strWorkNumber ) ) ? '\'' . addslashes( $this->m_strWorkNumber ) . '\'' : 'NULL';
	}

	public function setMobileNumber( $strMobileNumber ) {
		$this->set( 'm_strMobileNumber', CStrings::strTrimDef( $strMobileNumber, 30, NULL, true ) );
	}

	public function getMobileNumber() {
		return $this->m_strMobileNumber;
	}

	public function sqlMobileNumber() {
		return ( true == isset( $this->m_strMobileNumber ) ) ? '\'' . addslashes( $this->m_strMobileNumber ) . '\'' : 'NULL';
	}

	public function setFaxNumber( $strFaxNumber ) {
		$this->set( 'm_strFaxNumber', CStrings::strTrimDef( $strFaxNumber, 30, NULL, true ) );
	}

	public function getFaxNumber() {
		return $this->m_strFaxNumber;
	}

	public function sqlFaxNumber() {
		return ( true == isset( $this->m_strFaxNumber ) ) ? '\'' . addslashes( $this->m_strFaxNumber ) . '\'' : 'NULL';
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->set( 'm_strStreetLine1', CStrings::strTrimDef( $strStreetLine1, 100, NULL, true ) );
	}

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function sqlStreetLine1() {
		return ( true == isset( $this->m_strStreetLine1 ) ) ? '\'' . addslashes( $this->m_strStreetLine1 ) . '\'' : 'NULL';
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->set( 'm_strStreetLine2', CStrings::strTrimDef( $strStreetLine2, 100, NULL, true ) );
	}

	public function getStreetLine2() {
		return $this->m_strStreetLine2;
	}

	public function sqlStreetLine2() {
		return ( true == isset( $this->m_strStreetLine2 ) ) ? '\'' . addslashes( $this->m_strStreetLine2 ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 20, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted ) {
		$this->set( 'm_strTaxNumberEncrypted', CStrings::strTrimDef( $strTaxNumberEncrypted, 240, NULL, true ) );
	}

	public function getTaxNumberEncrypted() {
		return $this->m_strTaxNumberEncrypted;
	}

	public function sqlTaxNumberEncrypted() {
		return ( true == isset( $this->m_strTaxNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strTaxNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setJobShiftType( $strJobShiftType ) {
		$this->set( 'm_strJobShiftType', CStrings::strTrimDef( $strJobShiftType, 2000, NULL, true ) );
	}

	public function getJobShiftType() {
		return $this->m_strJobShiftType;
	}

	public function sqlJobShiftType() {
		return ( true == isset( $this->m_strJobShiftType ) ) ? '\'' . addslashes( $this->m_strJobShiftType ) . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setSubmittedBy( $intSubmittedBy ) {
		$this->set( 'm_intSubmittedBy', CStrings::strToIntDef( $intSubmittedBy, NULL, false ) );
	}

	public function getSubmittedBy() {
		return $this->m_intSubmittedBy;
	}

	public function sqlSubmittedBy() {
		return ( true == isset( $this->m_intSubmittedBy ) ) ? ( string ) $this->m_intSubmittedBy : 'NULL';
	}

	public function setSubmittedOn( $strSubmittedOn ) {
		$this->set( 'm_strSubmittedOn', CStrings::strTrimDef( $strSubmittedOn, -1, NULL, true ) );
	}

	public function getSubmittedOn() {
		return $this->m_strSubmittedOn;
	}

	public function sqlSubmittedOn() {
		return ( true == isset( $this->m_strSubmittedOn ) ) ? '\'' . $this->m_strSubmittedOn . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setRejectedBy( $intRejectedBy ) {
		$this->set( 'm_intRejectedBy', CStrings::strToIntDef( $intRejectedBy, NULL, false ) );
	}

	public function getRejectedBy() {
		return $this->m_intRejectedBy;
	}

	public function sqlRejectedBy() {
		return ( true == isset( $this->m_intRejectedBy ) ) ? ( string ) $this->m_intRejectedBy : 'NULL';
	}

	public function setRejectedOn( $strRejectedOn ) {
		$this->set( 'm_strRejectedOn', CStrings::strTrimDef( $strRejectedOn, -1, NULL, true ) );
	}

	public function getRejectedOn() {
		return $this->m_strRejectedOn;
	}

	public function sqlRejectedOn() {
		return ( true == isset( $this->m_strRejectedOn ) ) ? '\'' . $this->m_strRejectedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, career_application_id, career_id, document_id, candidate_status_type_id, company_employee_id, career_source_id, source_description, name_prefix, name_first, name_middle, name_last, name_suffix, phone_number, work_number, mobile_number, fax_number, street_line1, street_line2, city, state_code, postal_code, tax_number_encrypted, job_shift_type, notes, submitted_by, submitted_on, approved_by, approved_on, rejected_by, rejected_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCareerApplicationId() . ', ' .
 						$this->sqlCareerId() . ', ' .
 						$this->sqlDocumentId() . ', ' .
 						$this->sqlCandidateStatusTypeId() . ', ' .
 						$this->sqlCompanyEmployeeId() . ', ' .
 						$this->sqlCareerSourceId() . ', ' .
 						$this->sqlSourceDescription() . ', ' .
 						$this->sqlNamePrefix() . ', ' .
 						$this->sqlNameFirst() . ', ' .
 						$this->sqlNameMiddle() . ', ' .
 						$this->sqlNameLast() . ', ' .
 						$this->sqlNameSuffix() . ', ' .
 						$this->sqlPhoneNumber() . ', ' .
 						$this->sqlWorkNumber() . ', ' .
 						$this->sqlMobileNumber() . ', ' .
 						$this->sqlFaxNumber() . ', ' .
 						$this->sqlStreetLine1() . ', ' .
 						$this->sqlStreetLine2() . ', ' .
 						$this->sqlCity() . ', ' .
 						$this->sqlStateCode() . ', ' .
 						$this->sqlPostalCode() . ', ' .
 						$this->sqlTaxNumberEncrypted() . ', ' .
 						$this->sqlJobShiftType() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlSubmittedBy() . ', ' .
 						$this->sqlSubmittedOn() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
 						$this->sqlRejectedBy() . ', ' .
 						$this->sqlRejectedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' career_application_id = ' . $this->sqlCareerApplicationId() . ','; } elseif( true == array_key_exists( 'CareerApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' career_application_id = ' . $this->sqlCareerApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' career_id = ' . $this->sqlCareerId() . ','; } elseif( true == array_key_exists( 'CareerId', $this->getChangedColumns() ) ) { $strSql .= ' career_id = ' . $this->sqlCareerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; } elseif( true == array_key_exists( 'DocumentId', $this->getChangedColumns() ) ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' candidate_status_type_id = ' . $this->sqlCandidateStatusTypeId() . ','; } elseif( true == array_key_exists( 'CandidateStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' candidate_status_type_id = ' . $this->sqlCandidateStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; } elseif( true == array_key_exists( 'CompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' career_source_id = ' . $this->sqlCareerSourceId() . ','; } elseif( true == array_key_exists( 'CareerSourceId', $this->getChangedColumns() ) ) { $strSql .= ' career_source_id = ' . $this->sqlCareerSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_description = ' . $this->sqlSourceDescription() . ','; } elseif( true == array_key_exists( 'SourceDescription', $this->getChangedColumns() ) ) { $strSql .= ' source_description = ' . $this->sqlSourceDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_prefix = ' . $this->sqlNamePrefix() . ','; } elseif( true == array_key_exists( 'NamePrefix', $this->getChangedColumns() ) ) { $strSql .= ' name_prefix = ' . $this->sqlNamePrefix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle() . ','; } elseif( true == array_key_exists( 'NameMiddle', $this->getChangedColumns() ) ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_suffix = ' . $this->sqlNameSuffix() . ','; } elseif( true == array_key_exists( 'NameSuffix', $this->getChangedColumns() ) ) { $strSql .= ' name_suffix = ' . $this->sqlNameSuffix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' work_number = ' . $this->sqlWorkNumber() . ','; } elseif( true == array_key_exists( 'WorkNumber', $this->getChangedColumns() ) ) { $strSql .= ' work_number = ' . $this->sqlWorkNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mobile_number = ' . $this->sqlMobileNumber() . ','; } elseif( true == array_key_exists( 'MobileNumber', $this->getChangedColumns() ) ) { $strSql .= ' mobile_number = ' . $this->sqlMobileNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fax_number = ' . $this->sqlFaxNumber() . ','; } elseif( true == array_key_exists( 'FaxNumber', $this->getChangedColumns() ) ) { $strSql .= ' fax_number = ' . $this->sqlFaxNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; } elseif( true == array_key_exists( 'StreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; } elseif( true == array_key_exists( 'StreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; } elseif( true == array_key_exists( 'TaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_shift_type = ' . $this->sqlJobShiftType() . ','; } elseif( true == array_key_exists( 'JobShiftType', $this->getChangedColumns() ) ) { $strSql .= ' job_shift_type = ' . $this->sqlJobShiftType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' submitted_by = ' . $this->sqlSubmittedBy() . ','; } elseif( true == array_key_exists( 'SubmittedBy', $this->getChangedColumns() ) ) { $strSql .= ' submitted_by = ' . $this->sqlSubmittedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' submitted_on = ' . $this->sqlSubmittedOn() . ','; } elseif( true == array_key_exists( 'SubmittedOn', $this->getChangedColumns() ) ) { $strSql .= ' submitted_on = ' . $this->sqlSubmittedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejected_by = ' . $this->sqlRejectedBy() . ','; } elseif( true == array_key_exists( 'RejectedBy', $this->getChangedColumns() ) ) { $strSql .= ' rejected_by = ' . $this->sqlRejectedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn() . ','; } elseif( true == array_key_exists( 'RejectedOn', $this->getChangedColumns() ) ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'career_application_id' => $this->getCareerApplicationId(),
			'career_id' => $this->getCareerId(),
			'document_id' => $this->getDocumentId(),
			'candidate_status_type_id' => $this->getCandidateStatusTypeId(),
			'company_employee_id' => $this->getCompanyEmployeeId(),
			'career_source_id' => $this->getCareerSourceId(),
			'source_description' => $this->getSourceDescription(),
			'name_prefix' => $this->getNamePrefix(),
			'name_first' => $this->getNameFirst(),
			'name_middle' => $this->getNameMiddle(),
			'name_last' => $this->getNameLast(),
			'name_suffix' => $this->getNameSuffix(),
			'phone_number' => $this->getPhoneNumber(),
			'work_number' => $this->getWorkNumber(),
			'mobile_number' => $this->getMobileNumber(),
			'fax_number' => $this->getFaxNumber(),
			'street_line1' => $this->getStreetLine1(),
			'street_line2' => $this->getStreetLine2(),
			'city' => $this->getCity(),
			'state_code' => $this->getStateCode(),
			'postal_code' => $this->getPostalCode(),
			'tax_number_encrypted' => $this->getTaxNumberEncrypted(),
			'job_shift_type' => $this->getJobShiftType(),
			'notes' => $this->getNotes(),
			'submitted_by' => $this->getSubmittedBy(),
			'submitted_on' => $this->getSubmittedOn(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'rejected_by' => $this->getRejectedBy(),
			'rejected_on' => $this->getRejectedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>