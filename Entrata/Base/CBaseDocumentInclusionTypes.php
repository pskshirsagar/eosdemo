<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentInclusionTypes
 * Do not add any new functions to this class.
 */

class CBaseDocumentInclusionTypes extends CEosPluralBase {

	/**
	 * @return CDocumentInclusionType[]
	 */
	public static function fetchDocumentInclusionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDocumentInclusionType::class, $objDatabase );
	}

	/**
	 * @return CDocumentInclusionType
	 */
	public static function fetchDocumentInclusionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDocumentInclusionType::class, $objDatabase );
	}

	public static function fetchDocumentInclusionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_inclusion_types', $objDatabase );
	}

	public static function fetchDocumentInclusionTypeById( $intId, $objDatabase ) {
		return self::fetchDocumentInclusionType( sprintf( 'SELECT * FROM document_inclusion_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>