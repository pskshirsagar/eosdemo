<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAssetLocations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAssetLocations extends CEosPluralBase {

	/**
	 * @return CAssetLocation[]
	 */
	public static function fetchAssetLocations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CAssetLocation::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CAssetLocation
	 */
	public static function fetchAssetLocation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CAssetLocation::class, $objDatabase );
	}

	public static function fetchAssetLocationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'asset_locations', $objDatabase );
	}

	public static function fetchAssetLocationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchAssetLocation( sprintf( 'SELECT * FROM asset_locations WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchAssetLocationsByCid( $intCid, $objDatabase ) {
		return self::fetchAssetLocations( sprintf( 'SELECT * FROM asset_locations WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchAssetLocationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchAssetLocations( sprintf( 'SELECT * FROM asset_locations WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

}
?>