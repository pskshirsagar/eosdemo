<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitSpaceAbatements
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitSpaceAbatements extends CEosPluralBase {

	/**
	 * @return CUnitSpaceAbatement[]
	 */
	public static function fetchUnitSpaceAbatements( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CUnitSpaceAbatement', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CUnitSpaceAbatement
	 */
	public static function fetchUnitSpaceAbatement( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUnitSpaceAbatement', $objDatabase );
	}

	public static function fetchUnitSpaceAbatementCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'unit_space_abatements', $objDatabase );
	}

	public static function fetchUnitSpaceAbatementByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceAbatement( sprintf( 'SELECT * FROM unit_space_abatements WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceAbatementsByCid( $intCid, $objDatabase ) {
		return self::fetchUnitSpaceAbatements( sprintf( 'SELECT * FROM unit_space_abatements WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceAbatementsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceAbatements( sprintf( 'SELECT * FROM unit_space_abatements WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceAbatementsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceAbatements( sprintf( 'SELECT * FROM unit_space_abatements WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

}
?>