<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportVersions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReportVersions extends CEosPluralBase {

	public static function fetchReportVersions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CReportVersion', $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchReportVersion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CReportVersion', $objDatabase );
	}

	public static function fetchReportVersionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_versions', $objDatabase );
	}

	public static function fetchReportVersionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchReportVersion( sprintf( 'SELECT * FROM report_versions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportVersionsByCid( $intCid, $objDatabase ) {
		return self::fetchReportVersions( sprintf( 'SELECT * FROM report_versions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportVersionsByDefaultReportVersionIdByCid( $intDefaultReportVersionId, $intCid, $objDatabase ) {
		return self::fetchReportVersions( sprintf( 'SELECT * FROM report_versions WHERE default_report_version_id = %d AND cid = %d', ( int ) $intDefaultReportVersionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchReportVersionsByReportIdByCid( $intReportId, $intCid, $objDatabase ) {
		return self::fetchReportVersions( sprintf( 'SELECT * FROM report_versions WHERE report_id = %d AND cid = %d', ( int ) $intReportId, ( int ) $intCid ), $objDatabase );
	}

}
?>