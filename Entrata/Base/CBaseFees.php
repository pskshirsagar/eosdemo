<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFees
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFees extends CEosPluralBase {

	/**
	 * @return CFee[]
	 */
	public static function fetchFees( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CFee::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFee
	 */
	public static function fetchFee( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFee::class, $objDatabase );
	}

	public static function fetchFeeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'fees', $objDatabase );
	}

	public static function fetchFeeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFee( sprintf( 'SELECT * FROM fees WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchFeesByCid( $intCid, $objDatabase ) {
		return self::fetchFees( sprintf( 'SELECT * FROM fees WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchFeesByFeeTypeIdByCid( $intFeeTypeId, $intCid, $objDatabase ) {
		return self::fetchFees( sprintf( 'SELECT * FROM fees WHERE fee_type_id = %d AND cid = %d', $intFeeTypeId, $intCid ), $objDatabase );
	}

	public static function fetchFeesByFeeTemplateIdByCid( $intFeeTemplateId, $intCid, $objDatabase ) {
		return self::fetchFees( sprintf( 'SELECT * FROM fees WHERE fee_template_id = %d AND cid = %d', $intFeeTemplateId, $intCid ), $objDatabase );
	}

	public static function fetchFeesByPeriodIdByCid( $intPeriodId, $intCid, $objDatabase ) {
		return self::fetchFees( sprintf( 'SELECT * FROM fees WHERE period_id = %d AND cid = %d', $intPeriodId, $intCid ), $objDatabase );
	}

	public static function fetchFeesByArCodeIdByCid( $intArCodeId, $intCid, $objDatabase ) {
		return self::fetchFees( sprintf( 'SELECT * FROM fees WHERE ar_code_id = %d AND cid = %d', $intArCodeId, $intCid ), $objDatabase );
	}

	public static function fetchFeesByFeeCalculationTypeIdByCid( $intFeeCalculationTypeId, $intCid, $objDatabase ) {
		return self::fetchFees( sprintf( 'SELECT * FROM fees WHERE fee_calculation_type_id = %d AND cid = %d', $intFeeCalculationTypeId, $intCid ), $objDatabase );
	}

	public static function fetchFeesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchFees( sprintf( 'SELECT * FROM fees WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

}
?>