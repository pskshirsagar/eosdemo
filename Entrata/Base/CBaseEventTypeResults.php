<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CEventTypeResults
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseEventTypeResults extends CEosPluralBase {

	/**
	 * @return CEventTypeResult[]
	 */
	public static function fetchEventTypeResults( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CEventTypeResult', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CEventTypeResult
	 */
	public static function fetchEventTypeResult( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEventTypeResult', $objDatabase );
	}

	public static function fetchEventTypeResultCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'event_type_results', $objDatabase );
	}

	public static function fetchEventTypeResultByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchEventTypeResult( sprintf( 'SELECT * FROM event_type_results WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEventTypeResultsByCid( $intCid, $objDatabase ) {
		return self::fetchEventTypeResults( sprintf( 'SELECT * FROM event_type_results WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEventTypeResultsByEventTypeIdByCid( $intEventTypeId, $intCid, $objDatabase ) {
		return self::fetchEventTypeResults( sprintf( 'SELECT * FROM event_type_results WHERE event_type_id = %d AND cid = %d', ( int ) $intEventTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEventTypeResultsByEventResultIdByCid( $intEventResultId, $intCid, $objDatabase ) {
		return self::fetchEventTypeResults( sprintf( 'SELECT * FROM event_type_results WHERE event_result_id = %d AND cid = %d', ( int ) $intEventResultId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEventTypeResultsByDefaultEventResultIdByCid( $intDefaultEventResultId, $intCid, $objDatabase ) {
		return self::fetchEventTypeResults( sprintf( 'SELECT * FROM event_type_results WHERE default_event_result_id = %d AND cid = %d', ( int ) $intDefaultEventResultId, ( int ) $intCid ), $objDatabase );
	}

}
?>