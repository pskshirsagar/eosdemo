<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyCharities
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyCharities extends CEosPluralBase {

	/**
	 * @return CCompanyCharity[]
	 */
	public static function fetchCompanyCharities( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyCharity', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyCharity
	 */
	public static function fetchCompanyCharity( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyCharity', $objDatabase );
	}

	public static function fetchCompanyCharityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_charities', $objDatabase );
	}

	public static function fetchCompanyCharityByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyCharity( sprintf( 'SELECT * FROM company_charities WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyCharitiesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyCharities( sprintf( 'SELECT * FROM company_charities WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyCharitiesByCompanyMediaFileIdByCid( $intCompanyMediaFileId, $intCid, $objDatabase ) {
		return self::fetchCompanyCharities( sprintf( 'SELECT * FROM company_charities WHERE company_media_file_id = %d AND cid = %d', ( int ) $intCompanyMediaFileId, ( int ) $intCid ), $objDatabase );
	}

}
?>