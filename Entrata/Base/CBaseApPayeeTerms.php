<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeTerms
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPayeeTerms extends CEosPluralBase {

	/**
	 * @return CApPayeeTerm[]
	 */
	public static function fetchApPayeeTerms( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApPayeeTerm::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApPayeeTerm
	 */
	public static function fetchApPayeeTerm( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApPayeeTerm::class, $objDatabase );
	}

	public static function fetchApPayeeTermCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_payee_terms', $objDatabase );
	}

	public static function fetchApPayeeTermByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApPayeeTerm( sprintf( 'SELECT * FROM ap_payee_terms WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchApPayeeTermsByCid( $intCid, $objDatabase ) {
		return self::fetchApPayeeTerms( sprintf( 'SELECT * FROM ap_payee_terms WHERE cid = %d', $intCid ), $objDatabase );
	}

}
?>