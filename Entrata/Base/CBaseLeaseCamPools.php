<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseCamPools
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseCamPools extends CEosPluralBase {

	/**
	 * @return CLeaseCamPool[]
	 */
	public static function fetchLeaseCamPools( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CLeaseCamPool', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CLeaseCamPool
	 */
	public static function fetchLeaseCamPool( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CLeaseCamPool', $objDatabase );
	}

	public static function fetchLeaseCamPoolCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'lease_cam_pools', $objDatabase );
	}

	public static function fetchLeaseCamPoolByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchLeaseCamPool( sprintf( 'SELECT * FROM lease_cam_pools WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseCamPoolsByCid( $intCid, $objDatabase ) {
		return self::fetchLeaseCamPools( sprintf( 'SELECT * FROM lease_cam_pools WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseCamPoolsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchLeaseCamPools( sprintf( 'SELECT * FROM lease_cam_pools WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseCamPoolsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchLeaseCamPools( sprintf( 'SELECT * FROM lease_cam_pools WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseCamPoolsByLeaseIntervalIdByCid( $intLeaseIntervalId, $intCid, $objDatabase ) {
		return self::fetchLeaseCamPools( sprintf( 'SELECT * FROM lease_cam_pools WHERE lease_interval_id = %d AND cid = %d', ( int ) $intLeaseIntervalId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseCamPoolsByPropertyCamPoolIdByCid( $intPropertyCamPoolId, $intCid, $objDatabase ) {
		return self::fetchLeaseCamPools( sprintf( 'SELECT * FROM lease_cam_pools WHERE property_cam_pool_id = %d AND cid = %d', ( int ) $intPropertyCamPoolId, ( int ) $intCid ), $objDatabase );
	}

}
?>