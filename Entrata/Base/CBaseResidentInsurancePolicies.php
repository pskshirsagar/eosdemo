<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CResidentInsurancePolicies
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseResidentInsurancePolicies extends CEosPluralBase {

	/**
	 * @return CResidentInsurancePolicy[]
	 */
	public static function fetchResidentInsurancePolicies( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CResidentInsurancePolicy', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CResidentInsurancePolicy
	 */
	public static function fetchResidentInsurancePolicy( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CResidentInsurancePolicy', $objDatabase );
	}

	public static function fetchResidentInsurancePolicyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'resident_insurance_policies', $objDatabase );
	}

	public static function fetchResidentInsurancePolicyByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchResidentInsurancePolicy( sprintf( 'SELECT * FROM resident_insurance_policies WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchResidentInsurancePoliciesByEntityIdByCid( $intEntityId, $intCid, $objDatabase ) {
		return self::fetchResidentInsurancePolicies( sprintf( 'SELECT * FROM resident_insurance_policies WHERE entity_id = %d AND cid = %d', ( int ) $intEntityId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchResidentInsurancePoliciesByCid( $intCid, $objDatabase ) {
		return self::fetchResidentInsurancePolicies( sprintf( 'SELECT * FROM resident_insurance_policies WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchResidentInsurancePolicyByInsurancePolicyIdByCid( $intInsurancePolicyId, $intCid, $objDatabase ) {
		return self::fetchResidentInsurancePolicy( sprintf( 'SELECT * FROM resident_insurance_policies WHERE insurance_policy_id = %d AND cid = %d', ( int ) $intInsurancePolicyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchResidentInsurancePoliciesByInsurancePolicyIdByCid( $intInsurancePolicyId, $intCid, $objDatabase ) {
		return self::fetchResidentInsurancePolicies( sprintf( 'SELECT * FROM resident_insurance_policies WHERE insurance_policy_id = %d AND cid = %d', ( int ) $intInsurancePolicyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchResidentInsurancePoliciesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchResidentInsurancePolicies( sprintf( 'SELECT * FROM resident_insurance_policies WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchResidentInsurancePoliciesByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchResidentInsurancePolicies( sprintf( 'SELECT * FROM resident_insurance_policies WHERE property_unit_id = %d AND cid = %d', ( int ) $intPropertyUnitId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchResidentInsurancePoliciesByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchResidentInsurancePolicies( sprintf( 'SELECT * FROM resident_insurance_policies WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchResidentInsurancePoliciesByLeadSourceTypeIdByCid( $intLeadSourceTypeId, $intCid, $objDatabase ) {
		return self::fetchResidentInsurancePolicies( sprintf( 'SELECT * FROM resident_insurance_policies WHERE lead_source_type_id = %d AND cid = %d', ( int ) $intLeadSourceTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchResidentInsurancePoliciesByInsurancePolicyStatusTypeIdByCid( $intInsurancePolicyStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchResidentInsurancePolicies( sprintf( 'SELECT * FROM resident_insurance_policies WHERE insurance_policy_status_type_id = %d AND cid = %d', ( int ) $intInsurancePolicyStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchResidentInsurancePoliciesByInsurancePolicyTypeIdByCid( $intInsurancePolicyTypeId, $intCid, $objDatabase ) {
		return self::fetchResidentInsurancePolicies( sprintf( 'SELECT * FROM resident_insurance_policies WHERE insurance_policy_type_id = %d AND cid = %d', ( int ) $intInsurancePolicyTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchResidentInsurancePoliciesByReplacedInsurancePolicyIdByCid( $intReplacedInsurancePolicyId, $intCid, $objDatabase ) {
		return self::fetchResidentInsurancePolicies( sprintf( 'SELECT * FROM resident_insurance_policies WHERE replaced_insurance_policy_id = %d AND cid = %d', ( int ) $intReplacedInsurancePolicyId, ( int ) $intCid ), $objDatabase );
	}

}
?>