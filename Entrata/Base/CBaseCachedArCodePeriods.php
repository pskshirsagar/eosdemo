<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCachedArCodePeriods
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCachedArCodePeriods extends CEosPluralBase {

    public static function fetchCachedArCodePeriods( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CCachedArCodePeriod', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchCachedArCodePeriod( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CCachedArCodePeriod', $objDatabase );
    }

    public static function fetchCachedArCodePeriodCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'cached_ar_code_periods', $objDatabase );
    }

    public static function fetchCachedArCodePeriodByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchCachedArCodePeriod( sprintf( 'SELECT * FROM cached_ar_code_periods WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchCachedArCodePeriodsByCid( $intCid, $objDatabase ) {
        return self::fetchCachedArCodePeriods( sprintf( 'SELECT * FROM cached_ar_code_periods WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchCachedArCodePeriodsByPeriodIdByCid( $intPeriodId, $intCid, $objDatabase ) {
        return self::fetchCachedArCodePeriods( sprintf( 'SELECT * FROM cached_ar_code_periods WHERE period_id = %d AND cid = %d', (int) $intPeriodId, (int) $intCid ), $objDatabase );
    }

    public static function fetchCachedArCodePeriodsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
        return self::fetchCachedArCodePeriods( sprintf( 'SELECT * FROM cached_ar_code_periods WHERE property_id = %d AND cid = %d', (int) $intPropertyId, (int) $intCid ), $objDatabase );
    }

    public static function fetchCachedArCodePeriodsByGlAccountTypeIdByCid( $intGlAccountTypeId, $intCid, $objDatabase ) {
        return self::fetchCachedArCodePeriods( sprintf( 'SELECT * FROM cached_ar_code_periods WHERE gl_account_type_id = %d AND cid = %d', (int) $intGlAccountTypeId, (int) $intCid ), $objDatabase );
    }

    public static function fetchCachedArCodePeriodsByArCodeTypeIdByCid( $intArCodeTypeId, $intCid, $objDatabase ) {
        return self::fetchCachedArCodePeriods( sprintf( 'SELECT * FROM cached_ar_code_periods WHERE ar_code_type_id = %d AND cid = %d', (int) $intArCodeTypeId, (int) $intCid ), $objDatabase );
    }

    public static function fetchCachedArCodePeriodsByArCodeIdByCid( $intArCodeId, $intCid, $objDatabase ) {
        return self::fetchCachedArCodePeriods( sprintf( 'SELECT * FROM cached_ar_code_periods WHERE ar_code_id = %d AND cid = %d', (int) $intArCodeId, (int) $intCid ), $objDatabase );
    }

    public static function fetchCachedArCodePeriodsByArOriginIdByCid( $intArOriginId, $intCid, $objDatabase ) {
        return self::fetchCachedArCodePeriods( sprintf( 'SELECT * FROM cached_ar_code_periods WHERE ar_origin_id = %d AND cid = %d', (int) $intArOriginId, (int) $intCid ), $objDatabase );
    }

    public static function fetchCachedArCodePeriodsByArTriggerTypeIdByCid( $intArTriggerTypeId, $intCid, $objDatabase ) {
        return self::fetchCachedArCodePeriods( sprintf( 'SELECT * FROM cached_ar_code_periods WHERE ar_trigger_type_id = %d AND cid = %d', (int) $intArTriggerTypeId, (int) $intCid ), $objDatabase );
    }

    public static function fetchCachedArCodePeriodsByArTriggerIdByCid( $intArTriggerId, $intCid, $objDatabase ) {
        return self::fetchCachedArCodePeriods( sprintf( 'SELECT * FROM cached_ar_code_periods WHERE ar_trigger_id = %d AND cid = %d', (int) $intArTriggerId, (int) $intCid ), $objDatabase );
    }

    public static function fetchCachedArCodePeriodsByDebitGlAccountIdByCid( $intDebitGlAccountId, $intCid, $objDatabase ) {
        return self::fetchCachedArCodePeriods( sprintf( 'SELECT * FROM cached_ar_code_periods WHERE debit_gl_account_id = %d AND cid = %d', (int) $intDebitGlAccountId, (int) $intCid ), $objDatabase );
    }

    public static function fetchCachedArCodePeriodsByCreditGlAccountIdByCid( $intCreditGlAccountId, $intCid, $objDatabase ) {
        return self::fetchCachedArCodePeriods( sprintf( 'SELECT * FROM cached_ar_code_periods WHERE credit_gl_account_id = %d AND cid = %d', (int) $intCreditGlAccountId, (int) $intCid ), $objDatabase );
    }

}
?>