<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCommercialSuiteLog extends CEosSingularBase {

	const TABLE_NAME = 'public.commercial_suite_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyBuildingId;
	protected $m_intPropertyFloorId;
	protected $m_intPropertyUnitId;
	protected $m_intCommercialSuiteId;
	protected $m_intPriorCommercialSuiteLogId;
	protected $m_strPostMonth;
	protected $m_strPostDate;
	protected $m_strReportingPostMonth;
	protected $m_strApplyThroughPostMonth;
	protected $m_strReportingPostDate;
	protected $m_strApplyThroughPostDate;
	protected $m_fltSquareFeet;
	protected $m_intIsOpeningLog;
	protected $m_intIsPostMonthIgnored;
	protected $m_intIsPostDateIgnored;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strApplyThroughPostMonth = '12/01/2099';
		$this->m_strApplyThroughPostDate = '12/31/2099';
		$this->m_intIsOpeningLog = '0';
		$this->m_intIsPostMonthIgnored = '0';
		$this->m_intIsPostDateIgnored = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_building_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyBuildingId', trim( $arrValues['property_building_id'] ) ); elseif( isset( $arrValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrValues['property_building_id'] );
		if( isset( $arrValues['property_floor_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorId', trim( $arrValues['property_floor_id'] ) ); elseif( isset( $arrValues['property_floor_id'] ) ) $this->setPropertyFloorId( $arrValues['property_floor_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['commercial_suite_id'] ) && $boolDirectSet ) $this->set( 'm_intCommercialSuiteId', trim( $arrValues['commercial_suite_id'] ) ); elseif( isset( $arrValues['commercial_suite_id'] ) ) $this->setCommercialSuiteId( $arrValues['commercial_suite_id'] );
		if( isset( $arrValues['prior_commercial_suite_log_id'] ) && $boolDirectSet ) $this->set( 'm_intPriorCommercialSuiteLogId', trim( $arrValues['prior_commercial_suite_log_id'] ) ); elseif( isset( $arrValues['prior_commercial_suite_log_id'] ) ) $this->setPriorCommercialSuiteLogId( $arrValues['prior_commercial_suite_log_id'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['reporting_post_month'] ) && $boolDirectSet ) $this->set( 'm_strReportingPostMonth', trim( $arrValues['reporting_post_month'] ) ); elseif( isset( $arrValues['reporting_post_month'] ) ) $this->setReportingPostMonth( $arrValues['reporting_post_month'] );
		if( isset( $arrValues['apply_through_post_month'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostMonth', trim( $arrValues['apply_through_post_month'] ) ); elseif( isset( $arrValues['apply_through_post_month'] ) ) $this->setApplyThroughPostMonth( $arrValues['apply_through_post_month'] );
		if( isset( $arrValues['reporting_post_date'] ) && $boolDirectSet ) $this->set( 'm_strReportingPostDate', trim( $arrValues['reporting_post_date'] ) ); elseif( isset( $arrValues['reporting_post_date'] ) ) $this->setReportingPostDate( $arrValues['reporting_post_date'] );
		if( isset( $arrValues['apply_through_post_date'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostDate', trim( $arrValues['apply_through_post_date'] ) ); elseif( isset( $arrValues['apply_through_post_date'] ) ) $this->setApplyThroughPostDate( $arrValues['apply_through_post_date'] );
		if( isset( $arrValues['square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltSquareFeet', trim( $arrValues['square_feet'] ) ); elseif( isset( $arrValues['square_feet'] ) ) $this->setSquareFeet( $arrValues['square_feet'] );
		if( isset( $arrValues['is_opening_log'] ) && $boolDirectSet ) $this->set( 'm_intIsOpeningLog', trim( $arrValues['is_opening_log'] ) ); elseif( isset( $arrValues['is_opening_log'] ) ) $this->setIsOpeningLog( $arrValues['is_opening_log'] );
		if( isset( $arrValues['is_post_month_ignored'] ) && $boolDirectSet ) $this->set( 'm_intIsPostMonthIgnored', trim( $arrValues['is_post_month_ignored'] ) ); elseif( isset( $arrValues['is_post_month_ignored'] ) ) $this->setIsPostMonthIgnored( $arrValues['is_post_month_ignored'] );
		if( isset( $arrValues['is_post_date_ignored'] ) && $boolDirectSet ) $this->set( 'm_intIsPostDateIgnored', trim( $arrValues['is_post_date_ignored'] ) ); elseif( isset( $arrValues['is_post_date_ignored'] ) ) $this->setIsPostDateIgnored( $arrValues['is_post_date_ignored'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		$this->set( 'm_intPropertyBuildingId', CStrings::strToIntDef( $intPropertyBuildingId, NULL, false ) );
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function sqlPropertyBuildingId() {
		return ( true == isset( $this->m_intPropertyBuildingId ) ) ? ( string ) $this->m_intPropertyBuildingId : 'NULL';
	}

	public function setPropertyFloorId( $intPropertyFloorId ) {
		$this->set( 'm_intPropertyFloorId', CStrings::strToIntDef( $intPropertyFloorId, NULL, false ) );
	}

	public function getPropertyFloorId() {
		return $this->m_intPropertyFloorId;
	}

	public function sqlPropertyFloorId() {
		return ( true == isset( $this->m_intPropertyFloorId ) ) ? ( string ) $this->m_intPropertyFloorId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setCommercialSuiteId( $intCommercialSuiteId ) {
		$this->set( 'm_intCommercialSuiteId', CStrings::strToIntDef( $intCommercialSuiteId, NULL, false ) );
	}

	public function getCommercialSuiteId() {
		return $this->m_intCommercialSuiteId;
	}

	public function sqlCommercialSuiteId() {
		return ( true == isset( $this->m_intCommercialSuiteId ) ) ? ( string ) $this->m_intCommercialSuiteId : 'NULL';
	}

	public function setPriorCommercialSuiteLogId( $intPriorCommercialSuiteLogId ) {
		$this->set( 'm_intPriorCommercialSuiteLogId', CStrings::strToIntDef( $intPriorCommercialSuiteLogId, NULL, false ) );
	}

	public function getPriorCommercialSuiteLogId() {
		return $this->m_intPriorCommercialSuiteLogId;
	}

	public function sqlPriorCommercialSuiteLogId() {
		return ( true == isset( $this->m_intPriorCommercialSuiteLogId ) ) ? ( string ) $this->m_intPriorCommercialSuiteLogId : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setReportingPostMonth( $strReportingPostMonth ) {
		$this->set( 'm_strReportingPostMonth', CStrings::strTrimDef( $strReportingPostMonth, -1, NULL, true ) );
	}

	public function getReportingPostMonth() {
		return $this->m_strReportingPostMonth;
	}

	public function sqlReportingPostMonth() {
		return ( true == isset( $this->m_strReportingPostMonth ) ) ? '\'' . $this->m_strReportingPostMonth . '\'' : 'NOW()';
	}

	public function setApplyThroughPostMonth( $strApplyThroughPostMonth ) {
		$this->set( 'm_strApplyThroughPostMonth', CStrings::strTrimDef( $strApplyThroughPostMonth, -1, NULL, true ) );
	}

	public function getApplyThroughPostMonth() {
		return $this->m_strApplyThroughPostMonth;
	}

	public function sqlApplyThroughPostMonth() {
		return ( true == isset( $this->m_strApplyThroughPostMonth ) ) ? '\'' . $this->m_strApplyThroughPostMonth . '\'' : 'NOW()';
	}

	public function setReportingPostDate( $strReportingPostDate ) {
		$this->set( 'm_strReportingPostDate', CStrings::strTrimDef( $strReportingPostDate, -1, NULL, true ) );
	}

	public function getReportingPostDate() {
		return $this->m_strReportingPostDate;
	}

	public function sqlReportingPostDate() {
		return ( true == isset( $this->m_strReportingPostDate ) ) ? '\'' . $this->m_strReportingPostDate . '\'' : 'NOW()';
	}

	public function setApplyThroughPostDate( $strApplyThroughPostDate ) {
		$this->set( 'm_strApplyThroughPostDate', CStrings::strTrimDef( $strApplyThroughPostDate, -1, NULL, true ) );
	}

	public function getApplyThroughPostDate() {
		return $this->m_strApplyThroughPostDate;
	}

	public function sqlApplyThroughPostDate() {
		return ( true == isset( $this->m_strApplyThroughPostDate ) ) ? '\'' . $this->m_strApplyThroughPostDate . '\'' : 'NOW()';
	}

	public function setSquareFeet( $fltSquareFeet ) {
		$this->set( 'm_fltSquareFeet', CStrings::strToFloatDef( $fltSquareFeet, NULL, false, 4 ) );
	}

	public function getSquareFeet() {
		return $this->m_fltSquareFeet;
	}

	public function sqlSquareFeet() {
		return ( true == isset( $this->m_fltSquareFeet ) ) ? ( string ) $this->m_fltSquareFeet : 'NULL';
	}

	public function setIsOpeningLog( $intIsOpeningLog ) {
		$this->set( 'm_intIsOpeningLog', CStrings::strToIntDef( $intIsOpeningLog, NULL, false ) );
	}

	public function getIsOpeningLog() {
		return $this->m_intIsOpeningLog;
	}

	public function sqlIsOpeningLog() {
		return ( true == isset( $this->m_intIsOpeningLog ) ) ? ( string ) $this->m_intIsOpeningLog : '0';
	}

	public function setIsPostMonthIgnored( $intIsPostMonthIgnored ) {
		$this->set( 'm_intIsPostMonthIgnored', CStrings::strToIntDef( $intIsPostMonthIgnored, NULL, false ) );
	}

	public function getIsPostMonthIgnored() {
		return $this->m_intIsPostMonthIgnored;
	}

	public function sqlIsPostMonthIgnored() {
		return ( true == isset( $this->m_intIsPostMonthIgnored ) ) ? ( string ) $this->m_intIsPostMonthIgnored : '0';
	}

	public function setIsPostDateIgnored( $intIsPostDateIgnored ) {
		$this->set( 'm_intIsPostDateIgnored', CStrings::strToIntDef( $intIsPostDateIgnored, NULL, false ) );
	}

	public function getIsPostDateIgnored() {
		return $this->m_intIsPostDateIgnored;
	}

	public function sqlIsPostDateIgnored() {
		return ( true == isset( $this->m_intIsPostDateIgnored ) ) ? ( string ) $this->m_intIsPostDateIgnored : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_building_id, property_floor_id, property_unit_id, commercial_suite_id, prior_commercial_suite_log_id, post_month, post_date, reporting_post_month, apply_through_post_month, reporting_post_date, apply_through_post_date, square_feet, is_opening_log, is_post_month_ignored, is_post_date_ignored, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyBuildingId() . ', ' .
 						$this->sqlPropertyFloorId() . ', ' .
 						$this->sqlPropertyUnitId() . ', ' .
 						$this->sqlCommercialSuiteId() . ', ' .
 						$this->sqlPriorCommercialSuiteLogId() . ', ' .
 						$this->sqlPostMonth() . ', ' .
 						$this->sqlPostDate() . ', ' .
 						$this->sqlReportingPostMonth() . ', ' .
 						$this->sqlApplyThroughPostMonth() . ', ' .
 						$this->sqlReportingPostDate() . ', ' .
 						$this->sqlApplyThroughPostDate() . ', ' .
 						$this->sqlSquareFeet() . ', ' .
 						$this->sqlIsOpeningLog() . ', ' .
 						$this->sqlIsPostMonthIgnored() . ', ' .
 						$this->sqlIsPostDateIgnored() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; } elseif( true == array_key_exists( 'PropertyBuildingId', $this->getChangedColumns() ) ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floor_id = ' . $this->sqlPropertyFloorId() . ','; } elseif( true == array_key_exists( 'PropertyFloorId', $this->getChangedColumns() ) ) { $strSql .= ' property_floor_id = ' . $this->sqlPropertyFloorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commercial_suite_id = ' . $this->sqlCommercialSuiteId() . ','; } elseif( true == array_key_exists( 'CommercialSuiteId', $this->getChangedColumns() ) ) { $strSql .= ' commercial_suite_id = ' . $this->sqlCommercialSuiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prior_commercial_suite_log_id = ' . $this->sqlPriorCommercialSuiteLogId() . ','; } elseif( true == array_key_exists( 'PriorCommercialSuiteLogId', $this->getChangedColumns() ) ) { $strSql .= ' prior_commercial_suite_log_id = ' . $this->sqlPriorCommercialSuiteLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_post_month = ' . $this->sqlReportingPostMonth() . ','; } elseif( true == array_key_exists( 'ReportingPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' reporting_post_month = ' . $this->sqlReportingPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_month = ' . $this->sqlApplyThroughPostMonth() . ','; } elseif( true == array_key_exists( 'ApplyThroughPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_month = ' . $this->sqlApplyThroughPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate() . ','; } elseif( true == array_key_exists( 'ReportingPostDate', $this->getChangedColumns() ) ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate() . ','; } elseif( true == array_key_exists( 'ApplyThroughPostDate', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' square_feet = ' . $this->sqlSquareFeet() . ','; } elseif( true == array_key_exists( 'SquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' square_feet = ' . $this->sqlSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog() . ','; } elseif( true == array_key_exists( 'IsOpeningLog', $this->getChangedColumns() ) ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_month_ignored = ' . $this->sqlIsPostMonthIgnored() . ','; } elseif( true == array_key_exists( 'IsPostMonthIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_month_ignored = ' . $this->sqlIsPostMonthIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored() . ','; } elseif( true == array_key_exists( 'IsPostDateIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_building_id' => $this->getPropertyBuildingId(),
			'property_floor_id' => $this->getPropertyFloorId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'commercial_suite_id' => $this->getCommercialSuiteId(),
			'prior_commercial_suite_log_id' => $this->getPriorCommercialSuiteLogId(),
			'post_month' => $this->getPostMonth(),
			'post_date' => $this->getPostDate(),
			'reporting_post_month' => $this->getReportingPostMonth(),
			'apply_through_post_month' => $this->getApplyThroughPostMonth(),
			'reporting_post_date' => $this->getReportingPostDate(),
			'apply_through_post_date' => $this->getApplyThroughPostDate(),
			'square_feet' => $this->getSquareFeet(),
			'is_opening_log' => $this->getIsOpeningLog(),
			'is_post_month_ignored' => $this->getIsPostMonthIgnored(),
			'is_post_date_ignored' => $this->getIsPostDateIgnored(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>