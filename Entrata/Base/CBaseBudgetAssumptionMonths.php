<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetAssumptionMonths
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetAssumptionMonths extends CEosPluralBase {

	/**
	 * @return CBudgetAssumptionMonth[]
	 */
	public static function fetchBudgetAssumptionMonths( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CBudgetAssumptionMonth::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBudgetAssumptionMonth
	 */
	public static function fetchBudgetAssumptionMonth( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetAssumptionMonth::class, $objDatabase );
	}

	public static function fetchBudgetAssumptionMonthCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_assumption_months', $objDatabase );
	}

	public static function fetchBudgetAssumptionMonthByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBudgetAssumptionMonth( sprintf( 'SELECT * FROM budget_assumption_months WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetAssumptionMonthsByCid( $intCid, $objDatabase ) {
		return self::fetchBudgetAssumptionMonths( sprintf( 'SELECT * FROM budget_assumption_months WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchBudgetAssumptionMonthsByBudgetIdByCid( $intBudgetId, $intCid, $objDatabase ) {
		return self::fetchBudgetAssumptionMonths( sprintf( 'SELECT * FROM budget_assumption_months WHERE budget_id = %d AND cid = %d', $intBudgetId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetAssumptionMonthsByBudgetAssumptionIdByCid( $intBudgetAssumptionId, $intCid, $objDatabase ) {
		return self::fetchBudgetAssumptionMonths( sprintf( 'SELECT * FROM budget_assumption_months WHERE budget_assumption_id = %d AND cid = %d', $intBudgetAssumptionId, $intCid ), $objDatabase );
	}

}
?>