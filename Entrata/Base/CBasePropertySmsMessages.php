<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertySmsMessages
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertySmsMessages extends CEosPluralBase {

	/**
	 * @return CPropertySmsMessage[]
	 */
	public static function fetchPropertySmsMessages( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertySmsMessage', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertySmsMessage
	 */
	public static function fetchPropertySmsMessage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertySmsMessage', $objDatabase );
	}

	public static function fetchPropertySmsMessageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_sms_messages', $objDatabase );
	}

	public static function fetchPropertySmsMessageByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertySmsMessage( sprintf( 'SELECT * FROM property_sms_messages WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertySmsMessagesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertySmsMessages( sprintf( 'SELECT * FROM property_sms_messages WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertySmsMessagesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertySmsMessages( sprintf( 'SELECT * FROM property_sms_messages WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertySmsMessagesBySettingsTemplateIdByCid( $intSettingsTemplateId, $intCid, $objDatabase ) {
		return self::fetchPropertySmsMessages( sprintf( 'SELECT * FROM property_sms_messages WHERE settings_template_id = %d AND cid = %d', ( int ) $intSettingsTemplateId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertySmsMessagesByMessageTypeIdByCid( $intMessageTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertySmsMessages( sprintf( 'SELECT * FROM property_sms_messages WHERE message_type_id = %d AND cid = %d', ( int ) $intMessageTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>