<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceRequestMaterials
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceRequestMaterials extends CEosPluralBase {

	/**
	 * @return CMaintenanceRequestMaterial[]
	 */
	public static function fetchMaintenanceRequestMaterials( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMaintenanceRequestMaterial::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenanceRequestMaterial
	 */
	public static function fetchMaintenanceRequestMaterial( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceRequestMaterial::class, $objDatabase );
	}

	public static function fetchMaintenanceRequestMaterialCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_request_materials', $objDatabase );
	}

	public static function fetchMaintenanceRequestMaterialByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestMaterial( sprintf( 'SELECT * FROM maintenance_request_materials WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestMaterialsByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestMaterials( sprintf( 'SELECT * FROM maintenance_request_materials WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestMaterialsByMaintenanceRequestIdByCid( $intMaintenanceRequestId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestMaterials( sprintf( 'SELECT * FROM maintenance_request_materials WHERE maintenance_request_id = %d AND cid = %d', $intMaintenanceRequestId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestMaterialsByGlAccountIdByCid( $intGlAccountId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestMaterials( sprintf( 'SELECT * FROM maintenance_request_materials WHERE gl_account_id = %d AND cid = %d', $intGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestMaterialsByApCodeIdByCid( $intApCodeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestMaterials( sprintf( 'SELECT * FROM maintenance_request_materials WHERE ap_code_id = %d AND cid = %d', $intApCodeId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestMaterialsByApDetailIdByCid( $intApDetailId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestMaterials( sprintf( 'SELECT * FROM maintenance_request_materials WHERE ap_detail_id = %d AND cid = %d', $intApDetailId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestMaterialsByAssetLocationIdByCid( $intAssetLocationId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestMaterials( sprintf( 'SELECT * FROM maintenance_request_materials WHERE asset_location_id = %d AND cid = %d', $intAssetLocationId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestMaterialsByAssetIdByCid( $intAssetId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestMaterials( sprintf( 'SELECT * FROM maintenance_request_materials WHERE asset_id = %d AND cid = %d', $intAssetId, $intCid ), $objDatabase );
	}

}
?>