<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCachedApplicationLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCachedApplicationLogs extends CEosPluralBase {

	/**
	 * @return CCachedApplicationLog[]
	 */
	public static function fetchCachedApplicationLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCachedApplicationLog::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCachedApplicationLog
	 */
	public static function fetchCachedApplicationLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCachedApplicationLog::class, $objDatabase );
	}

	public static function fetchCachedApplicationLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'cached_application_logs', $objDatabase );
	}

	public static function fetchCachedApplicationLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLog( sprintf( 'SELECT * FROM cached_application_logs WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByCid( $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE application_id = %d AND cid = %d', $intApplicationId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByPriorCachedApplicationLogIdByCid( $intPriorCachedApplicationLogId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE prior_cached_application_log_id = %d AND cid = %d', $intPriorCachedApplicationLogId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE lease_id = %d AND cid = %d', $intLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByLeaseIntervalIdByCid( $intLeaseIntervalId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE lease_interval_id = %d AND cid = %d', $intLeaseIntervalId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByLeaseIntervalTypeIdByCid( $intLeaseIntervalTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE lease_interval_type_id = %d AND cid = %d', $intLeaseIntervalTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByLeaseStatusTypeIdByCid( $intLeaseStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE lease_status_type_id = %d AND cid = %d', $intLeaseStatusTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByLeaseTypeIdByCid( $intLeaseTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE lease_type_id = %d AND cid = %d', $intLeaseTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByOldApplicationStatusTypeIdByCid( $intOldApplicationStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE old_application_status_type_id = %d AND cid = %d', $intOldApplicationStatusTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByApplicationStageIdByCid( $intApplicationStageId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE application_stage_id = %d AND cid = %d', $intApplicationStageId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByApplicationStatusIdByCid( $intApplicationStatusId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE application_status_id = %d AND cid = %d', $intApplicationStatusId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByApplicationStepIdByCid( $intApplicationStepId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE application_step_id = %d AND cid = %d', $intApplicationStepId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByMaxApplicationStageIdByCid( $intMaxApplicationStageId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE max_application_stage_id = %d AND cid = %d', $intMaxApplicationStageId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByMaxApplicationStatusIdByCid( $intMaxApplicationStatusId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE max_application_status_id = %d AND cid = %d', $intMaxApplicationStatusId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByMaxApplicationStepIdByCid( $intMaxApplicationStepId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE max_application_step_id = %d AND cid = %d', $intMaxApplicationStepId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByCancellationListTypeIdByCid( $intCancellationListTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE cancellation_list_type_id = %d AND cid = %d', $intCancellationListTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByCancellationListItemIdByCid( $intCancellationListItemId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE cancellation_list_item_id = %d AND cid = %d', $intCancellationListItemId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByPrimaryApplicantIdByCid( $intPrimaryApplicantId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE primary_applicant_id = %d AND cid = %d', $intPrimaryApplicantId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByUnitKindIdByCid( $intUnitKindId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE unit_kind_id = %d AND cid = %d', $intUnitKindId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE unit_type_id = %d AND cid = %d', $intUnitTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByPropertyFloorplanIdByCid( $intPropertyFloorplanId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE property_floorplan_id = %d AND cid = %d', $intPropertyFloorplanId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByPropertyBuildingIdByCid( $intPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE property_building_id = %d AND cid = %d', $intPropertyBuildingId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByPropertyFloorIdByCid( $intPropertyFloorId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE property_floor_id = %d AND cid = %d', $intPropertyFloorId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE property_unit_id = %d AND cid = %d', $intPropertyUnitId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE unit_space_id = %d AND cid = %d', $intUnitSpaceId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsBySpaceConfigurationIdByCid( $intSpaceConfigurationId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE space_configuration_id = %d AND cid = %d', $intSpaceConfigurationId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByDesiredSpaceConfigurationIdByCid( $intDesiredSpaceConfigurationId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE desired_space_configuration_id = %d AND cid = %d', $intDesiredSpaceConfigurationId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByScreeningIdByCid( $intScreeningId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE screening_id = %d AND cid = %d', $intScreeningId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByPsProductIdByCid( $intPsProductId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE ps_product_id = %d AND cid = %d', $intPsProductId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByApplicationPsProductIdByCid( $intApplicationPsProductId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE application_ps_product_id = %d AND cid = %d', $intApplicationPsProductId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByLeasePsProductIdByCid( $intLeasePsProductId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE lease_ps_product_id = %d AND cid = %d', $intLeasePsProductId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByLeasingAgentIdByCid( $intLeasingAgentId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE leasing_agent_id = %d AND cid = %d', $intLeasingAgentId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByInternetListingServiceIdByCid( $intInternetListingServiceId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE internet_listing_service_id = %d AND cid = %d', $intInternetListingServiceId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByOriginatingLeadSourceIdByCid( $intOriginatingLeadSourceId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE originating_lead_source_id = %d AND cid = %d', $intOriginatingLeadSourceId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByConvertingLeadSourceIdByCid( $intConvertingLeadSourceId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE converting_lead_source_id = %d AND cid = %d', $intConvertingLeadSourceId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByCompanyApplicationIdByCid( $intCompanyApplicationId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE company_application_id = %d AND cid = %d', $intCompanyApplicationId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByLeaseTermIdByCid( $intLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE lease_term_id = %d AND cid = %d', $intLeaseTermId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByLeaseStartWindowIdByCid( $intLeaseStartWindowId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE lease_start_window_id = %d AND cid = %d', $intLeaseStartWindowId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByLateFeeFormulaIdByCid( $intLateFeeFormulaId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE late_fee_formula_id = %d AND cid = %d', $intLateFeeFormulaId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByCombinedApplicationIdByCid( $intCombinedApplicationId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE combined_application_id = %d AND cid = %d', $intCombinedApplicationId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByQuoteIdByCid( $intQuoteId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE quote_id = %d AND cid = %d', $intQuoteId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsBySemAdGroupIdByCid( $intSemAdGroupId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE sem_ad_group_id = %d AND cid = %d', $intSemAdGroupId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsBySemKeywordIdByCid( $intSemKeywordId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE sem_keyword_id = %d AND cid = %d', $intSemKeywordId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsBySemSourceIdByCid( $intSemSourceId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE sem_source_id = %d AND cid = %d', $intSemSourceId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByCallIdByCid( $intCallId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE call_id = %d AND cid = %d', $intCallId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByWebsiteIdByCid( $intWebsiteId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE website_id = %d AND cid = %d', $intWebsiteId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByCraigslistPostIdByCid( $intCraigslistPostId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE craigslist_post_id = %d AND cid = %d', $intCraigslistPostId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByClAdIdByCid( $intClAdId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE cl_ad_id = %d AND cid = %d', $intClAdId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByTrafficCookieIdByCid( $intTrafficCookieId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE traffic_cookie_id = %d AND cid = %d', $intTrafficCookieId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByFirstEventIdByCid( $intFirstEventId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE first_event_id = %d AND cid = %d', $intFirstEventId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByFirstEventMediumIdByCid( $intFirstEventMediumId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE first_event_medium_id = %d AND cid = %d', $intFirstEventMediumId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByFirstLeasingCenterEventIdByCid( $intFirstLeasingCenterEventId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE first_leasing_center_event_id = %d AND cid = %d', $intFirstLeasingCenterEventId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByResponseEventIdByCid( $intResponseEventId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE response_event_id = %d AND cid = %d', $intResponseEventId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByLastEventIdByCid( $intLastEventId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE last_event_id = %d AND cid = %d', $intLastEventId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByLeaseSignatureTypeIdByCid( $intLeaseSignatureTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE lease_signature_type_id = %d AND cid = %d', $intLeaseSignatureTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByInsurancePolicyIdByCid( $intInsurancePolicyId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE insurance_policy_id = %d AND cid = %d', $intInsurancePolicyId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByInsurancePolicyStatusTypeIdByCid( $intInsurancePolicyStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE insurance_policy_status_type_id = %d AND cid = %d', $intInsurancePolicyStatusTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByOccupancyTypeIdByCid( $intOccupancyTypeId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE occupancy_type_id = %d AND cid = %d', $intOccupancyTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByPeriodIdByCid( $intPeriodId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE period_id = %d AND cid = %d', $intPeriodId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByReportingPeriodIdByCid( $intReportingPeriodId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE reporting_period_id = %d AND cid = %d', $intReportingPeriodId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByEffectivePeriodIdByCid( $intEffectivePeriodId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE effective_period_id = %d AND cid = %d', $intEffectivePeriodId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByOriginalPeriodIdByCid( $intOriginalPeriodId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE original_period_id = %d AND cid = %d', $intOriginalPeriodId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByWaitListIdByCid( $intWaitListId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE wait_list_id = %d AND cid = %d', $intWaitListId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByWaitListApplicationIdByCid( $intWaitListApplicationId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE wait_list_application_id = %d AND cid = %d', $intWaitListApplicationId, $intCid ), $objDatabase );
	}

	public static function fetchCachedApplicationLogsByUnitPriorApplicationIdByCid( $intUnitPriorApplicationId, $intCid, $objDatabase ) {
		return self::fetchCachedApplicationLogs( sprintf( 'SELECT * FROM cached_application_logs WHERE unit_prior_application_id = %d AND cid = %d', $intUnitPriorApplicationId, $intCid ), $objDatabase );
	}

}
?>