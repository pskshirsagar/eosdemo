<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTransmissionConditions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseTransmissionConditions extends CEosPluralBase {

	/**
	 * @return CTransmissionCondition[]
	 */
	public static function fetchTransmissionConditions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CTransmissionCondition', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CTransmissionCondition
	 */
	public static function fetchTransmissionCondition( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTransmissionCondition', $objDatabase );
	}

	public static function fetchTransmissionConditionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transmission_conditions', $objDatabase );
	}

	public static function fetchTransmissionConditionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchTransmissionCondition( sprintf( 'SELECT * FROM transmission_conditions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransmissionConditionsByCid( $intCid, $objDatabase ) {
		return self::fetchTransmissionConditions( sprintf( 'SELECT * FROM transmission_conditions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransmissionConditionsByTransmissionConditionTypeIdByCid( $intTransmissionConditionTypeId, $intCid, $objDatabase ) {
		return self::fetchTransmissionConditions( sprintf( 'SELECT * FROM transmission_conditions WHERE transmission_condition_type_id = %d AND cid = %d', ( int ) $intTransmissionConditionTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransmissionConditionsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchTransmissionConditions( sprintf( 'SELECT * FROM transmission_conditions WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransmissionConditionsByTransmissionIdByCid( $intTransmissionId, $intCid, $objDatabase ) {
		return self::fetchTransmissionConditions( sprintf( 'SELECT * FROM transmission_conditions WHERE transmission_id = %d AND cid = %d', ( int ) $intTransmissionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransmissionConditionsByGuarantorApplicantApplicationTransmissionIdByCid( $intGuarantorApplicantApplicationTransmissionId, $intCid, $objDatabase ) {
		return self::fetchTransmissionConditions( sprintf( 'SELECT * FROM transmission_conditions WHERE guarantor_applicant_application_transmission_id = %d AND cid = %d', ( int ) $intGuarantorApplicantApplicationTransmissionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransmissionConditionsByGuarantorApplicantApplicationIdByCid( $intGuarantorApplicantApplicationId, $intCid, $objDatabase ) {
		return self::fetchTransmissionConditions( sprintf( 'SELECT * FROM transmission_conditions WHERE guarantor_applicant_application_id = %d AND cid = %d', ( int ) $intGuarantorApplicantApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransmissionConditionsByDepositArTransactionIdByCid( $intDepositArTransactionId, $intCid, $objDatabase ) {
		return self::fetchTransmissionConditions( sprintf( 'SELECT * FROM transmission_conditions WHERE deposit_ar_transaction_id = %d AND cid = %d', ( int ) $intDepositArTransactionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransmissionConditionsByRentApplicationRateIdByCid( $intRentApplicationRateId, $intCid, $objDatabase ) {
		return self::fetchTransmissionConditions( sprintf( 'SELECT * FROM transmission_conditions WHERE rent_application_rate_id = %d AND cid = %d', ( int ) $intRentApplicationRateId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransmissionConditionsByRentScheduledChargeIdByCid( $intRentScheduledChargeId, $intCid, $objDatabase ) {
		return self::fetchTransmissionConditions( sprintf( 'SELECT * FROM transmission_conditions WHERE rent_scheduled_charge_id = %d AND cid = %d', ( int ) $intRentScheduledChargeId, ( int ) $intCid ), $objDatabase );
	}

}
?>