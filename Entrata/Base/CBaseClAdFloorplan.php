<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseClAdFloorplan extends CEosSingularBase {

	const TABLE_NAME = 'public.cl_ad_floorplans';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intClAdId;
	protected $m_intPropertyFloorplanId;
	protected $m_fltMinRent;
	protected $m_fltMaxRent;
	protected $m_fltMinDeposit;
	protected $m_fltMaxDeposit;
	protected $m_intFloorplanAvailableUnits;
	protected $m_intFloorplanTotalUnits;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['cl_ad_id'] ) && $boolDirectSet ) $this->set( 'm_intClAdId', trim( $arrValues['cl_ad_id'] ) ); elseif( isset( $arrValues['cl_ad_id'] ) ) $this->setClAdId( $arrValues['cl_ad_id'] );
		if( isset( $arrValues['property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorplanId', trim( $arrValues['property_floorplan_id'] ) ); elseif( isset( $arrValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrValues['property_floorplan_id'] );
		if( isset( $arrValues['min_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMinRent', trim( $arrValues['min_rent'] ) ); elseif( isset( $arrValues['min_rent'] ) ) $this->setMinRent( $arrValues['min_rent'] );
		if( isset( $arrValues['max_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMaxRent', trim( $arrValues['max_rent'] ) ); elseif( isset( $arrValues['max_rent'] ) ) $this->setMaxRent( $arrValues['max_rent'] );
		if( isset( $arrValues['min_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltMinDeposit', trim( $arrValues['min_deposit'] ) ); elseif( isset( $arrValues['min_deposit'] ) ) $this->setMinDeposit( $arrValues['min_deposit'] );
		if( isset( $arrValues['max_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltMaxDeposit', trim( $arrValues['max_deposit'] ) ); elseif( isset( $arrValues['max_deposit'] ) ) $this->setMaxDeposit( $arrValues['max_deposit'] );
		if( isset( $arrValues['floorplan_available_units'] ) && $boolDirectSet ) $this->set( 'm_intFloorplanAvailableUnits', trim( $arrValues['floorplan_available_units'] ) ); elseif( isset( $arrValues['floorplan_available_units'] ) ) $this->setFloorplanAvailableUnits( $arrValues['floorplan_available_units'] );
		if( isset( $arrValues['floorplan_total_units'] ) && $boolDirectSet ) $this->set( 'm_intFloorplanTotalUnits', trim( $arrValues['floorplan_total_units'] ) ); elseif( isset( $arrValues['floorplan_total_units'] ) ) $this->setFloorplanTotalUnits( $arrValues['floorplan_total_units'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setClAdId( $intClAdId ) {
		$this->set( 'm_intClAdId', CStrings::strToIntDef( $intClAdId, NULL, false ) );
	}

	public function getClAdId() {
		return $this->m_intClAdId;
	}

	public function sqlClAdId() {
		return ( true == isset( $this->m_intClAdId ) ) ? ( string ) $this->m_intClAdId : 'NULL';
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->set( 'm_intPropertyFloorplanId', CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false ) );
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function sqlPropertyFloorplanId() {
		return ( true == isset( $this->m_intPropertyFloorplanId ) ) ? ( string ) $this->m_intPropertyFloorplanId : 'NULL';
	}

	public function setMinRent( $fltMinRent ) {
		$this->set( 'm_fltMinRent', CStrings::strToFloatDef( $fltMinRent, NULL, false, 2 ) );
	}

	public function getMinRent() {
		return $this->m_fltMinRent;
	}

	public function sqlMinRent() {
		return ( true == isset( $this->m_fltMinRent ) ) ? ( string ) $this->m_fltMinRent : 'NULL';
	}

	public function setMaxRent( $fltMaxRent ) {
		$this->set( 'm_fltMaxRent', CStrings::strToFloatDef( $fltMaxRent, NULL, false, 2 ) );
	}

	public function getMaxRent() {
		return $this->m_fltMaxRent;
	}

	public function sqlMaxRent() {
		return ( true == isset( $this->m_fltMaxRent ) ) ? ( string ) $this->m_fltMaxRent : 'NULL';
	}

	public function setMinDeposit( $fltMinDeposit ) {
		$this->set( 'm_fltMinDeposit', CStrings::strToFloatDef( $fltMinDeposit, NULL, false, 2 ) );
	}

	public function getMinDeposit() {
		return $this->m_fltMinDeposit;
	}

	public function sqlMinDeposit() {
		return ( true == isset( $this->m_fltMinDeposit ) ) ? ( string ) $this->m_fltMinDeposit : 'NULL';
	}

	public function setMaxDeposit( $fltMaxDeposit ) {
		$this->set( 'm_fltMaxDeposit', CStrings::strToFloatDef( $fltMaxDeposit, NULL, false, 2 ) );
	}

	public function getMaxDeposit() {
		return $this->m_fltMaxDeposit;
	}

	public function sqlMaxDeposit() {
		return ( true == isset( $this->m_fltMaxDeposit ) ) ? ( string ) $this->m_fltMaxDeposit : 'NULL';
	}

	public function setFloorplanAvailableUnits( $intFloorplanAvailableUnits ) {
		$this->set( 'm_intFloorplanAvailableUnits', CStrings::strToIntDef( $intFloorplanAvailableUnits, NULL, false ) );
	}

	public function getFloorplanAvailableUnits() {
		return $this->m_intFloorplanAvailableUnits;
	}

	public function sqlFloorplanAvailableUnits() {
		return ( true == isset( $this->m_intFloorplanAvailableUnits ) ) ? ( string ) $this->m_intFloorplanAvailableUnits : 'NULL';
	}

	public function setFloorplanTotalUnits( $intFloorplanTotalUnits ) {
		$this->set( 'm_intFloorplanTotalUnits', CStrings::strToIntDef( $intFloorplanTotalUnits, NULL, false ) );
	}

	public function getFloorplanTotalUnits() {
		return $this->m_intFloorplanTotalUnits;
	}

	public function sqlFloorplanTotalUnits() {
		return ( true == isset( $this->m_intFloorplanTotalUnits ) ) ? ( string ) $this->m_intFloorplanTotalUnits : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, cl_ad_id, property_floorplan_id, min_rent, max_rent, min_deposit, max_deposit, floorplan_available_units, floorplan_total_units, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlClAdId() . ', ' .
 						$this->sqlPropertyFloorplanId() . ', ' .
 						$this->sqlMinRent() . ', ' .
 						$this->sqlMaxRent() . ', ' .
 						$this->sqlMinDeposit() . ', ' .
 						$this->sqlMaxDeposit() . ', ' .
 						$this->sqlFloorplanAvailableUnits() . ', ' .
 						$this->sqlFloorplanTotalUnits() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cl_ad_id = ' . $this->sqlClAdId() . ','; } elseif( true == array_key_exists( 'ClAdId', $this->getChangedColumns() ) ) { $strSql .= ' cl_ad_id = ' . $this->sqlClAdId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; } elseif( true == array_key_exists( 'PropertyFloorplanId', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_rent = ' . $this->sqlMinRent() . ','; } elseif( true == array_key_exists( 'MinRent', $this->getChangedColumns() ) ) { $strSql .= ' min_rent = ' . $this->sqlMinRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_rent = ' . $this->sqlMaxRent() . ','; } elseif( true == array_key_exists( 'MaxRent', $this->getChangedColumns() ) ) { $strSql .= ' max_rent = ' . $this->sqlMaxRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_deposit = ' . $this->sqlMinDeposit() . ','; } elseif( true == array_key_exists( 'MinDeposit', $this->getChangedColumns() ) ) { $strSql .= ' min_deposit = ' . $this->sqlMinDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_deposit = ' . $this->sqlMaxDeposit() . ','; } elseif( true == array_key_exists( 'MaxDeposit', $this->getChangedColumns() ) ) { $strSql .= ' max_deposit = ' . $this->sqlMaxDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' floorplan_available_units = ' . $this->sqlFloorplanAvailableUnits() . ','; } elseif( true == array_key_exists( 'FloorplanAvailableUnits', $this->getChangedColumns() ) ) { $strSql .= ' floorplan_available_units = ' . $this->sqlFloorplanAvailableUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' floorplan_total_units = ' . $this->sqlFloorplanTotalUnits() . ','; } elseif( true == array_key_exists( 'FloorplanTotalUnits', $this->getChangedColumns() ) ) { $strSql .= ' floorplan_total_units = ' . $this->sqlFloorplanTotalUnits() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'cl_ad_id' => $this->getClAdId(),
			'property_floorplan_id' => $this->getPropertyFloorplanId(),
			'min_rent' => $this->getMinRent(),
			'max_rent' => $this->getMaxRent(),
			'min_deposit' => $this->getMinDeposit(),
			'max_deposit' => $this->getMaxDeposit(),
			'floorplan_available_units' => $this->getFloorplanAvailableUnits(),
			'floorplan_total_units' => $this->getFloorplanTotalUnits(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>