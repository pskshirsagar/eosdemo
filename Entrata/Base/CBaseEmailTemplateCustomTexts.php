<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseEmailTemplateCustomTexts extends CEosPluralBase {

	/**
	 * @return CEmailTemplateCustomText[]
	 */
	public static function fetchEmailTemplateCustomTexts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CEmailTemplateCustomText', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CEmailTemplateCustomText
	 */
	public static function fetchEmailTemplateCustomText( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmailTemplateCustomText', $objDatabase );
	}

	public static function fetchEmailTemplateCustomTextCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'email_template_custom_texts', $objDatabase );
	}

	public static function fetchEmailTemplateCustomTextByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplateCustomText( sprintf( 'SELECT * FROM email_template_custom_texts WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplateCustomTextsByCid( $intCid, $objDatabase ) {
		return self::fetchEmailTemplateCustomTexts( sprintf( 'SELECT * FROM email_template_custom_texts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplateCustomTextsByScheduledEmailIdByCid( $intScheduledEmailId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplateCustomTexts( sprintf( 'SELECT * FROM email_template_custom_texts WHERE scheduled_email_id = %d AND cid = %d', ( int ) $intScheduledEmailId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplateCustomTextsByScheduledTaskEmailIdByCid( $intScheduledTaskEmailId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplateCustomTexts( sprintf( 'SELECT * FROM email_template_custom_texts WHERE scheduled_task_email_id = %d AND cid = %d', ( int ) $intScheduledTaskEmailId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplateCustomTextsByCampaignIdByCid( $intCampaignId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplateCustomTexts( sprintf( 'SELECT * FROM email_template_custom_texts WHERE campaign_id = %d AND cid = %d', ( int ) $intCampaignId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplateCustomTextsByEmailTemplateIdByCid( $intSystemMessageTemplateId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplateCustomTexts( sprintf( 'SELECT * FROM email_template_custom_texts WHERE system_message_template_id = %d AND cid = %d', ( int ) $intSystemMessageTemplateId, ( int ) $intCid ), $objDatabase );
	}

}
?>