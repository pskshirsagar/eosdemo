<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCheckComponent extends CEosSingularBase {

	const TABLE_NAME = 'public.check_components';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCheckTemplateId;
	protected $m_intCheckComponentTypeId;
	protected $m_fltXPosition;
	protected $m_fltYPosition;
	protected $m_intWidth;
	protected $m_intHeight;
	protected $m_intCenterJustify;
	protected $m_intIsVisible;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCenterJustify = '0';
		$this->m_intIsVisible = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['check_template_id'] ) && $boolDirectSet ) $this->set( 'm_intCheckTemplateId', trim( $arrValues['check_template_id'] ) ); elseif( isset( $arrValues['check_template_id'] ) ) $this->setCheckTemplateId( $arrValues['check_template_id'] );
		if( isset( $arrValues['check_component_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCheckComponentTypeId', trim( $arrValues['check_component_type_id'] ) ); elseif( isset( $arrValues['check_component_type_id'] ) ) $this->setCheckComponentTypeId( $arrValues['check_component_type_id'] );
		if( isset( $arrValues['x_position'] ) && $boolDirectSet ) $this->set( 'm_fltXPosition', trim( $arrValues['x_position'] ) ); elseif( isset( $arrValues['x_position'] ) ) $this->setXPosition( $arrValues['x_position'] );
		if( isset( $arrValues['y_position'] ) && $boolDirectSet ) $this->set( 'm_fltYPosition', trim( $arrValues['y_position'] ) ); elseif( isset( $arrValues['y_position'] ) ) $this->setYPosition( $arrValues['y_position'] );
		if( isset( $arrValues['width'] ) && $boolDirectSet ) $this->set( 'm_intWidth', trim( $arrValues['width'] ) ); elseif( isset( $arrValues['width'] ) ) $this->setWidth( $arrValues['width'] );
		if( isset( $arrValues['height'] ) && $boolDirectSet ) $this->set( 'm_intHeight', trim( $arrValues['height'] ) ); elseif( isset( $arrValues['height'] ) ) $this->setHeight( $arrValues['height'] );
		if( isset( $arrValues['center_justify'] ) && $boolDirectSet ) $this->set( 'm_intCenterJustify', trim( $arrValues['center_justify'] ) ); elseif( isset( $arrValues['center_justify'] ) ) $this->setCenterJustify( $arrValues['center_justify'] );
		if( isset( $arrValues['is_visible'] ) && $boolDirectSet ) $this->set( 'm_intIsVisible', trim( $arrValues['is_visible'] ) ); elseif( isset( $arrValues['is_visible'] ) ) $this->setIsVisible( $arrValues['is_visible'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCheckTemplateId( $intCheckTemplateId ) {
		$this->set( 'm_intCheckTemplateId', CStrings::strToIntDef( $intCheckTemplateId, NULL, false ) );
	}

	public function getCheckTemplateId() {
		return $this->m_intCheckTemplateId;
	}

	public function sqlCheckTemplateId() {
		return ( true == isset( $this->m_intCheckTemplateId ) ) ? ( string ) $this->m_intCheckTemplateId : 'NULL';
	}

	public function setCheckComponentTypeId( $intCheckComponentTypeId ) {
		$this->set( 'm_intCheckComponentTypeId', CStrings::strToIntDef( $intCheckComponentTypeId, NULL, false ) );
	}

	public function getCheckComponentTypeId() {
		return $this->m_intCheckComponentTypeId;
	}

	public function sqlCheckComponentTypeId() {
		return ( true == isset( $this->m_intCheckComponentTypeId ) ) ? ( string ) $this->m_intCheckComponentTypeId : 'NULL';
	}

	public function setXPosition( $fltXPosition ) {
		$this->set( 'm_fltXPosition', CStrings::strToFloatDef( $fltXPosition, NULL, false, 4 ) );
	}

	public function getXPosition() {
		return $this->m_fltXPosition;
	}

	public function sqlXPosition() {
		return ( true == isset( $this->m_fltXPosition ) ) ? ( string ) $this->m_fltXPosition : 'NULL';
	}

	public function setYPosition( $fltYPosition ) {
		$this->set( 'm_fltYPosition', CStrings::strToFloatDef( $fltYPosition, NULL, false, 4 ) );
	}

	public function getYPosition() {
		return $this->m_fltYPosition;
	}

	public function sqlYPosition() {
		return ( true == isset( $this->m_fltYPosition ) ) ? ( string ) $this->m_fltYPosition : 'NULL';
	}

	public function setWidth( $intWidth ) {
		$this->set( 'm_intWidth', CStrings::strToIntDef( $intWidth, NULL, false ) );
	}

	public function getWidth() {
		return $this->m_intWidth;
	}

	public function sqlWidth() {
		return ( true == isset( $this->m_intWidth ) ) ? ( string ) $this->m_intWidth : 'NULL';
	}

	public function setHeight( $intHeight ) {
		$this->set( 'm_intHeight', CStrings::strToIntDef( $intHeight, NULL, false ) );
	}

	public function getHeight() {
		return $this->m_intHeight;
	}

	public function sqlHeight() {
		return ( true == isset( $this->m_intHeight ) ) ? ( string ) $this->m_intHeight : 'NULL';
	}

	public function setCenterJustify( $intCenterJustify ) {
		$this->set( 'm_intCenterJustify', CStrings::strToIntDef( $intCenterJustify, NULL, false ) );
	}

	public function getCenterJustify() {
		return $this->m_intCenterJustify;
	}

	public function sqlCenterJustify() {
		return ( true == isset( $this->m_intCenterJustify ) ) ? ( string ) $this->m_intCenterJustify : '0';
	}

	public function setIsVisible( $intIsVisible ) {
		$this->set( 'm_intIsVisible', CStrings::strToIntDef( $intIsVisible, NULL, false ) );
	}

	public function getIsVisible() {
		return $this->m_intIsVisible;
	}

	public function sqlIsVisible() {
		return ( true == isset( $this->m_intIsVisible ) ) ? ( string ) $this->m_intIsVisible : '1';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, check_template_id, check_component_type_id, x_position, y_position, width, height, center_justify, is_visible, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCheckTemplateId() . ', ' .
 						$this->sqlCheckComponentTypeId() . ', ' .
 						$this->sqlXPosition() . ', ' .
 						$this->sqlYPosition() . ', ' .
 						$this->sqlWidth() . ', ' .
 						$this->sqlHeight() . ', ' .
 						$this->sqlCenterJustify() . ', ' .
 						$this->sqlIsVisible() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_template_id = ' . $this->sqlCheckTemplateId() . ','; } elseif( true == array_key_exists( 'CheckTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' check_template_id = ' . $this->sqlCheckTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_component_type_id = ' . $this->sqlCheckComponentTypeId() . ','; } elseif( true == array_key_exists( 'CheckComponentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' check_component_type_id = ' . $this->sqlCheckComponentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' x_position = ' . $this->sqlXPosition() . ','; } elseif( true == array_key_exists( 'XPosition', $this->getChangedColumns() ) ) { $strSql .= ' x_position = ' . $this->sqlXPosition() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' y_position = ' . $this->sqlYPosition() . ','; } elseif( true == array_key_exists( 'YPosition', $this->getChangedColumns() ) ) { $strSql .= ' y_position = ' . $this->sqlYPosition() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' width = ' . $this->sqlWidth() . ','; } elseif( true == array_key_exists( 'Width', $this->getChangedColumns() ) ) { $strSql .= ' width = ' . $this->sqlWidth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' height = ' . $this->sqlHeight() . ','; } elseif( true == array_key_exists( 'Height', $this->getChangedColumns() ) ) { $strSql .= ' height = ' . $this->sqlHeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' center_justify = ' . $this->sqlCenterJustify() . ','; } elseif( true == array_key_exists( 'CenterJustify', $this->getChangedColumns() ) ) { $strSql .= ' center_justify = ' . $this->sqlCenterJustify() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_visible = ' . $this->sqlIsVisible() . ','; } elseif( true == array_key_exists( 'IsVisible', $this->getChangedColumns() ) ) { $strSql .= ' is_visible = ' . $this->sqlIsVisible() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'check_template_id' => $this->getCheckTemplateId(),
			'check_component_type_id' => $this->getCheckComponentTypeId(),
			'x_position' => $this->getXPosition(),
			'y_position' => $this->getYPosition(),
			'width' => $this->getWidth(),
			'height' => $this->getHeight(),
			'center_justify' => $this->getCenterJustify(),
			'is_visible' => $this->getIsVisible(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>