<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlSettings
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlSettings extends CEosPluralBase {

	/**
	 * @return CGlSetting[]
	 */
	public static function fetchGlSettings( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CGlSetting::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CGlSetting
	 */
	public static function fetchGlSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGlSetting::class, $objDatabase );
	}

	public static function fetchGlSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_settings', $objDatabase );
	}

	public static function fetchGlSettingByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchGlSetting( sprintf( 'SELECT * FROM gl_settings WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByCid( $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByApGlAccountIdByCid( $intApGlAccountId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE ap_gl_account_id = %d AND cid = %d', $intApGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByRetainedEarningsGlAccountIdByCid( $intRetainedEarningsGlAccountId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE retained_earnings_gl_account_id = %d AND cid = %d', $intRetainedEarningsGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByMarketRentGlAccountIdByCid( $intMarketRentGlAccountId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE market_rent_gl_account_id = %d AND cid = %d', $intMarketRentGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByGainToLeaseGlAccountIdByCid( $intGainToLeaseGlAccountId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE gain_to_lease_gl_account_id = %d AND cid = %d', $intGainToLeaseGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByLossToLeaseGlAccountIdByCid( $intLossToLeaseGlAccountId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE loss_to_lease_gl_account_id = %d AND cid = %d', $intLossToLeaseGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByVacancyLossGlAccountIdByCid( $intVacancyLossGlAccountId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE vacancy_loss_gl_account_id = %d AND cid = %d', $intVacancyLossGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByInterCompanyArGlAccountIdByCid( $intInterCompanyArGlAccountId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE inter_company_ar_gl_account_id = %d AND cid = %d', $intInterCompanyArGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByInterCompanyApGlAccountIdByCid( $intInterCompanyApGlAccountId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE inter_company_ap_gl_account_id = %d AND cid = %d', $intInterCompanyApGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByFloatingReimbursementsGlAccountIdByCid( $intFloatingReimbursementsGlAccountId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE floating_reimbursements_gl_account_id = %d AND cid = %d', $intFloatingReimbursementsGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByRetentionPayableGlAccountIdByCid( $intRetentionPayableGlAccountId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE retention_payable_gl_account_id = %d AND cid = %d', $intRetentionPayableGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByAccelRentArCodeIdByCid( $intAccelRentArCodeId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE accel_rent_ar_code_id = %d AND cid = %d', $intAccelRentArCodeId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByRentArCodeIdByCid( $intRentArCodeId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE rent_ar_code_id = %d AND cid = %d', $intRentArCodeId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByDepositArCodeIdByCid( $intDepositArCodeId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE deposit_ar_code_id = %d AND cid = %d', $intDepositArCodeId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByMtmRentArCodeIdByCid( $intMtmRentArCodeId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE mtm_rent_ar_code_id = %d AND cid = %d', $intMtmRentArCodeId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByReimbursementArCodeIdByCid( $intReimbursementArCodeId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE reimbursement_ar_code_id = %d AND cid = %d', $intReimbursementArCodeId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByManagementFeeArCodeIdByCid( $intManagementFeeArCodeId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE management_fee_ar_code_id = %d AND cid = %d', $intManagementFeeArCodeId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByOwnerContributionArCodeIdByCid( $intOwnerContributionArCodeId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE owner_contribution_ar_code_id = %d AND cid = %d', $intOwnerContributionArCodeId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByLenderContributionArCodeIdByCid( $intLenderContributionArCodeId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE lender_contribution_ar_code_id = %d AND cid = %d', $intLenderContributionArCodeId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByGlDimensionsAllowanceTypeIdByCid( $intGlDimensionsAllowanceTypeId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE gl_dimensions_allowance_type_id = %d AND cid = %d', $intGlDimensionsAllowanceTypeId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByDepartmentAllowanceTypeIdByCid( $intDepartmentAllowanceTypeId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE department_allowance_type_id = %d AND cid = %d', $intDepartmentAllowanceTypeId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByConfidentialityAllowanceTypeIdByCid( $intConfidentialityAllowanceTypeId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE confidentiality_allowance_type_id = %d AND cid = %d', $intConfidentialityAllowanceTypeId, $intCid ), $objDatabase );
	}

	public static function fetchGlSettingsByApMerchantAccountIdByCid( $intApMerchantAccountId, $intCid, $objDatabase ) {
		return self::fetchGlSettings( sprintf( 'SELECT * FROM gl_settings WHERE ap_merchant_account_id = %d AND cid = %d', $intApMerchantAccountId, $intCid ), $objDatabase );
	}

}
?>