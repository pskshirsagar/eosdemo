<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFileSignature extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.file_signatures';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApplicantApplicationId;
	protected $m_intSignatureFileId;
	protected $m_intFileAssociationId;
	protected $m_strInitials;
	protected $m_intPageNumber;
	protected $m_intScrollPercentage;
	protected $m_strIpAddress;
	protected $m_strRemotePrimaryKey;
	protected $m_strUniqueLayoutKey;
	protected $m_intIsMaster;
	protected $m_intIsPaginated;
	protected $m_intIsViewed;
	protected $m_intIsArchived;
	protected $m_strAgreementDatetime;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intPsProductId;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intIsMaster = '1';
		$this->m_intIsPaginated = '0';
		$this->m_intIsViewed = '0';
		$this->m_intIsArchived = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['applicant_application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantApplicationId', trim( $arrValues['applicant_application_id'] ) ); elseif( isset( $arrValues['applicant_application_id'] ) ) $this->setApplicantApplicationId( $arrValues['applicant_application_id'] );
		if( isset( $arrValues['signature_file_id'] ) && $boolDirectSet ) $this->set( 'm_intSignatureFileId', trim( $arrValues['signature_file_id'] ) ); elseif( isset( $arrValues['signature_file_id'] ) ) $this->setSignatureFileId( $arrValues['signature_file_id'] );
		if( isset( $arrValues['file_association_id'] ) && $boolDirectSet ) $this->set( 'm_intFileAssociationId', trim( $arrValues['file_association_id'] ) ); elseif( isset( $arrValues['file_association_id'] ) ) $this->setFileAssociationId( $arrValues['file_association_id'] );
		if( isset( $arrValues['initials'] ) && $boolDirectSet ) $this->set( 'm_strInitials', trim( stripcslashes( $arrValues['initials'] ) ) ); elseif( isset( $arrValues['initials'] ) ) $this->setInitials( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['initials'] ) : $arrValues['initials'] );
		if( isset( $arrValues['page_number'] ) && $boolDirectSet ) $this->set( 'm_intPageNumber', trim( $arrValues['page_number'] ) ); elseif( isset( $arrValues['page_number'] ) ) $this->setPageNumber( $arrValues['page_number'] );
		if( isset( $arrValues['scroll_percentage'] ) && $boolDirectSet ) $this->set( 'm_intScrollPercentage', trim( $arrValues['scroll_percentage'] ) ); elseif( isset( $arrValues['scroll_percentage'] ) ) $this->setScrollPercentage( $arrValues['scroll_percentage'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( stripcslashes( $arrValues['ip_address'] ) ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ip_address'] ) : $arrValues['ip_address'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['unique_layout_key'] ) && $boolDirectSet ) $this->set( 'm_strUniqueLayoutKey', trim( stripcslashes( $arrValues['unique_layout_key'] ) ) ); elseif( isset( $arrValues['unique_layout_key'] ) ) $this->setUniqueLayoutKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unique_layout_key'] ) : $arrValues['unique_layout_key'] );
		if( isset( $arrValues['is_master'] ) && $boolDirectSet ) $this->set( 'm_intIsMaster', trim( $arrValues['is_master'] ) ); elseif( isset( $arrValues['is_master'] ) ) $this->setIsMaster( $arrValues['is_master'] );
		if( isset( $arrValues['is_paginated'] ) && $boolDirectSet ) $this->set( 'm_intIsPaginated', trim( $arrValues['is_paginated'] ) ); elseif( isset( $arrValues['is_paginated'] ) ) $this->setIsPaginated( $arrValues['is_paginated'] );
		if( isset( $arrValues['is_viewed'] ) && $boolDirectSet ) $this->set( 'm_intIsViewed', trim( $arrValues['is_viewed'] ) ); elseif( isset( $arrValues['is_viewed'] ) ) $this->setIsViewed( $arrValues['is_viewed'] );
		if( isset( $arrValues['is_archived'] ) && $boolDirectSet ) $this->set( 'm_intIsArchived', trim( $arrValues['is_archived'] ) ); elseif( isset( $arrValues['is_archived'] ) ) $this->setIsArchived( $arrValues['is_archived'] );
		if( isset( $arrValues['agreement_datetime'] ) && $boolDirectSet ) $this->set( 'm_strAgreementDatetime', trim( $arrValues['agreement_datetime'] ) ); elseif( isset( $arrValues['agreement_datetime'] ) ) $this->setAgreementDatetime( $arrValues['agreement_datetime'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApplicantApplicationId( $intApplicantApplicationId ) {
		$this->set( 'm_intApplicantApplicationId', CStrings::strToIntDef( $intApplicantApplicationId, NULL, false ) );
	}

	public function getApplicantApplicationId() {
		return $this->m_intApplicantApplicationId;
	}

	public function sqlApplicantApplicationId() {
		return ( true == isset( $this->m_intApplicantApplicationId ) ) ? ( string ) $this->m_intApplicantApplicationId : 'NULL';
	}

	public function setSignatureFileId( $intSignatureFileId ) {
		$this->set( 'm_intSignatureFileId', CStrings::strToIntDef( $intSignatureFileId, NULL, false ) );
	}

	public function getSignatureFileId() {
		return $this->m_intSignatureFileId;
	}

	public function sqlSignatureFileId() {
		return ( true == isset( $this->m_intSignatureFileId ) ) ? ( string ) $this->m_intSignatureFileId : 'NULL';
	}

	public function setFileAssociationId( $intFileAssociationId ) {
		$this->set( 'm_intFileAssociationId', CStrings::strToIntDef( $intFileAssociationId, NULL, false ) );
	}

	public function getFileAssociationId() {
		return $this->m_intFileAssociationId;
	}

	public function sqlFileAssociationId() {
		return ( true == isset( $this->m_intFileAssociationId ) ) ? ( string ) $this->m_intFileAssociationId : 'NULL';
	}

	public function setInitials( $strInitials ) {
		$this->set( 'm_strInitials', CStrings::strTrimDef( $strInitials, 240, NULL, true ) );
	}

	public function getInitials() {
		return $this->m_strInitials;
	}

	public function sqlInitials() {
		return ( true == isset( $this->m_strInitials ) ) ? '\'' . addslashes( $this->m_strInitials ) . '\'' : 'NULL';
	}

	public function setPageNumber( $intPageNumber ) {
		$this->set( 'm_intPageNumber', CStrings::strToIntDef( $intPageNumber, NULL, false ) );
	}

	public function getPageNumber() {
		return $this->m_intPageNumber;
	}

	public function sqlPageNumber() {
		return ( true == isset( $this->m_intPageNumber ) ) ? ( string ) $this->m_intPageNumber : 'NULL';
	}

	public function setScrollPercentage( $intScrollPercentage ) {
		$this->set( 'm_intScrollPercentage', CStrings::strToIntDef( $intScrollPercentage, NULL, false ) );
	}

	public function getScrollPercentage() {
		return $this->m_intScrollPercentage;
	}

	public function sqlScrollPercentage() {
		return ( true == isset( $this->m_intScrollPercentage ) ) ? ( string ) $this->m_intScrollPercentage : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 23, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? '\'' . addslashes( $this->m_strIpAddress ) . '\'' : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 50, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setUniqueLayoutKey( $strUniqueLayoutKey ) {
		$this->set( 'm_strUniqueLayoutKey', CStrings::strTrimDef( $strUniqueLayoutKey, 255, NULL, true ) );
	}

	public function getUniqueLayoutKey() {
		return $this->m_strUniqueLayoutKey;
	}

	public function sqlUniqueLayoutKey() {
		return ( true == isset( $this->m_strUniqueLayoutKey ) ) ? '\'' . addslashes( $this->m_strUniqueLayoutKey ) . '\'' : 'NULL';
	}

	public function setIsMaster( $intIsMaster ) {
		$this->set( 'm_intIsMaster', CStrings::strToIntDef( $intIsMaster, NULL, false ) );
	}

	public function getIsMaster() {
		return $this->m_intIsMaster;
	}

	public function sqlIsMaster() {
		return ( true == isset( $this->m_intIsMaster ) ) ? ( string ) $this->m_intIsMaster : '1';
	}

	public function setIsPaginated( $intIsPaginated ) {
		$this->set( 'm_intIsPaginated', CStrings::strToIntDef( $intIsPaginated, NULL, false ) );
	}

	public function getIsPaginated() {
		return $this->m_intIsPaginated;
	}

	public function sqlIsPaginated() {
		return ( true == isset( $this->m_intIsPaginated ) ) ? ( string ) $this->m_intIsPaginated : '0';
	}

	public function setIsViewed( $intIsViewed ) {
		$this->set( 'm_intIsViewed', CStrings::strToIntDef( $intIsViewed, NULL, false ) );
	}

	public function getIsViewed() {
		return $this->m_intIsViewed;
	}

	public function sqlIsViewed() {
		return ( true == isset( $this->m_intIsViewed ) ) ? ( string ) $this->m_intIsViewed : '0';
	}

	public function setIsArchived( $intIsArchived ) {
		$this->set( 'm_intIsArchived', CStrings::strToIntDef( $intIsArchived, NULL, false ) );
	}

	public function getIsArchived() {
		return $this->m_intIsArchived;
	}

	public function sqlIsArchived() {
		return ( true == isset( $this->m_intIsArchived ) ) ? ( string ) $this->m_intIsArchived : '0';
	}

	public function setAgreementDatetime( $strAgreementDatetime ) {
		$this->set( 'm_strAgreementDatetime', CStrings::strTrimDef( $strAgreementDatetime, -1, NULL, true ) );
	}

	public function getAgreementDatetime() {
		return $this->m_strAgreementDatetime;
	}

	public function sqlAgreementDatetime() {
		return ( true == isset( $this->m_strAgreementDatetime ) ) ? '\'' . $this->m_strAgreementDatetime . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, applicant_application_id, signature_file_id, file_association_id, initials, page_number, scroll_percentage, ip_address, remote_primary_key, unique_layout_key, is_master, is_paginated, is_viewed, is_archived, agreement_datetime, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, ps_product_id, details )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlApplicantApplicationId() . ', ' .
 						$this->sqlSignatureFileId() . ', ' .
 						$this->sqlFileAssociationId() . ', ' .
 						$this->sqlInitials() . ', ' .
 						$this->sqlPageNumber() . ', ' .
 						$this->sqlScrollPercentage() . ', ' .
 						$this->sqlIpAddress() . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlUniqueLayoutKey() . ', ' .
 						$this->sqlIsMaster() . ', ' .
 						$this->sqlIsPaginated() . ', ' .
 						$this->sqlIsViewed() . ', ' .
 						$this->sqlIsArchived() . ', ' .
 						$this->sqlAgreementDatetime() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlPsProductId() . ', ' .
		                $this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_application_id = ' . $this->sqlApplicantApplicationId() . ','; } elseif( true == array_key_exists( 'ApplicantApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_application_id = ' . $this->sqlApplicantApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signature_file_id = ' . $this->sqlSignatureFileId() . ','; } elseif( true == array_key_exists( 'SignatureFileId', $this->getChangedColumns() ) ) { $strSql .= ' signature_file_id = ' . $this->sqlSignatureFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_association_id = ' . $this->sqlFileAssociationId() . ','; } elseif( true == array_key_exists( 'FileAssociationId', $this->getChangedColumns() ) ) { $strSql .= ' file_association_id = ' . $this->sqlFileAssociationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initials = ' . $this->sqlInitials() . ','; } elseif( true == array_key_exists( 'Initials', $this->getChangedColumns() ) ) { $strSql .= ' initials = ' . $this->sqlInitials() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' page_number = ' . $this->sqlPageNumber() . ','; } elseif( true == array_key_exists( 'PageNumber', $this->getChangedColumns() ) ) { $strSql .= ' page_number = ' . $this->sqlPageNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scroll_percentage = ' . $this->sqlScrollPercentage() . ','; } elseif( true == array_key_exists( 'ScrollPercentage', $this->getChangedColumns() ) ) { $strSql .= ' scroll_percentage = ' . $this->sqlScrollPercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unique_layout_key = ' . $this->sqlUniqueLayoutKey() . ','; } elseif( true == array_key_exists( 'UniqueLayoutKey', $this->getChangedColumns() ) ) { $strSql .= ' unique_layout_key = ' . $this->sqlUniqueLayoutKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_master = ' . $this->sqlIsMaster() . ','; } elseif( true == array_key_exists( 'IsMaster', $this->getChangedColumns() ) ) { $strSql .= ' is_master = ' . $this->sqlIsMaster() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_paginated = ' . $this->sqlIsPaginated() . ','; } elseif( true == array_key_exists( 'IsPaginated', $this->getChangedColumns() ) ) { $strSql .= ' is_paginated = ' . $this->sqlIsPaginated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_viewed = ' . $this->sqlIsViewed() . ','; } elseif( true == array_key_exists( 'IsViewed', $this->getChangedColumns() ) ) { $strSql .= ' is_viewed = ' . $this->sqlIsViewed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived() . ','; } elseif( true == array_key_exists( 'IsArchived', $this->getChangedColumns() ) ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' agreement_datetime = ' . $this->sqlAgreementDatetime() . ','; } elseif( true == array_key_exists( 'AgreementDatetime', $this->getChangedColumns() ) ) { $strSql .= ' agreement_datetime = ' . $this->sqlAgreementDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'applicant_application_id' => $this->getApplicantApplicationId(),
			'signature_file_id' => $this->getSignatureFileId(),
			'file_association_id' => $this->getFileAssociationId(),
			'initials' => $this->getInitials(),
			'page_number' => $this->getPageNumber(),
			'scroll_percentage' => $this->getScrollPercentage(),
			'ip_address' => $this->getIpAddress(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'unique_layout_key' => $this->getUniqueLayoutKey(),
			'is_master' => $this->getIsMaster(),
			'is_paginated' => $this->getIsPaginated(),
			'is_viewed' => $this->getIsViewed(),
			'is_archived' => $this->getIsArchived(),
			'agreement_datetime' => $this->getAgreementDatetime(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'ps_product_id' => $this->getPsProductId(),
			'details' => $this->getDetails()
		);
	}

}
?>