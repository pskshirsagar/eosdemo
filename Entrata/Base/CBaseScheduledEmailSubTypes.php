<?php

class CBaseScheduledEmailSubTypes extends CEosPluralBase {

	/**
	 * @return CScheduledEmailSubType[]
	 */
	public static function fetchScheduledEmailSubTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScheduledEmailSubType', $objDatabase );
	}

	/**
	 * @return CScheduledEmailSubType
	 */
	public static function fetchScheduledEmailSubType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScheduledEmailSubType', $objDatabase );
	}

	public static function fetchScheduledEmailSubTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_email_sub_types', $objDatabase );
	}

	public static function fetchScheduledEmailSubTypeById( $intId, $objDatabase ) {
		return self::fetchScheduledEmailSubType( sprintf( 'SELECT * FROM scheduled_email_sub_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScheduledEmailSubTypesByScheduledEmailTypeId( $intScheduledEmailTypeId, $objDatabase ) {
		return self::fetchScheduledEmailSubTypes( sprintf( 'SELECT * FROM scheduled_email_sub_types WHERE scheduled_email_type_id = %d', ( int ) $intScheduledEmailTypeId ), $objDatabase );
	}

}
?>