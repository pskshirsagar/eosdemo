<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPayment extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.ap_payments';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intMerchantAccountId;
	protected $m_intBankAccountId;
	protected $m_intMerchantGatewayId;
	protected $m_intPaymentMediumId;
	protected $m_intApPaymentTypeId;
	protected $m_intPaymentStatusTypeId;
	protected $m_intReturnTypeId;
	protected $m_intApPaymentId;
	protected $m_intCheckAccountTypeId;
	protected $m_intApPaymentBatchId;
	protected $m_intSourceCheckAccountTypeId;
	protected $m_intApRemittanceId;
	protected $m_intEftReference;
	protected $m_strRemotePrimaryKey;
	protected $m_strReturnRemotePrimaryKey;
	protected $m_intSecureReferenceNumber;
	protected $m_strPaymentMemo;
	protected $m_strPaymentDate;
	protected $m_strIssueDatetime;
	protected $m_fltPaymentAmount;
	protected $m_strPayeeName;
	protected $m_strBilltoIpAddress;
	protected $m_strPaymentNumber;
	protected $m_strCheckBankName;
	protected $m_strCheckNameOnAccount;
	protected $m_strCheckNameOnAccountLine2;
	protected $m_strCheckRoutingNumber;
	protected $m_strCheckFractionalRoutingNumber;
	protected $m_strCheckAccountNumberEncrypted;
	protected $m_strSourceCheckBankName;
	protected $m_strSourceCheckNameOnAccount;
	protected $m_strSourceCheckRoutingNumber;
	protected $m_strSourceCheckAccountNumberEncrypted;
	protected $m_intIsReversed;
	protected $m_intIsQuickCheck;
	protected $m_boolIsUnclaimedProperty;
	protected $m_strReturnedOn;
	protected $m_strBatchedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intIsReversed = '0';
		$this->m_intIsQuickCheck = '0';
		$this->m_boolIsUnclaimedProperty = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['merchant_account_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantAccountId', trim( $arrValues['merchant_account_id'] ) ); elseif( isset( $arrValues['merchant_account_id'] ) ) $this->setMerchantAccountId( $arrValues['merchant_account_id'] );
		if( isset( $arrValues['bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBankAccountId', trim( $arrValues['bank_account_id'] ) ); elseif( isset( $arrValues['bank_account_id'] ) ) $this->setBankAccountId( $arrValues['bank_account_id'] );
		if( isset( $arrValues['merchant_gateway_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantGatewayId', trim( $arrValues['merchant_gateway_id'] ) ); elseif( isset( $arrValues['merchant_gateway_id'] ) ) $this->setMerchantGatewayId( $arrValues['merchant_gateway_id'] );
		if( isset( $arrValues['payment_medium_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentMediumId', trim( $arrValues['payment_medium_id'] ) ); elseif( isset( $arrValues['payment_medium_id'] ) ) $this->setPaymentMediumId( $arrValues['payment_medium_id'] );
		if( isset( $arrValues['ap_payment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApPaymentTypeId', trim( $arrValues['ap_payment_type_id'] ) ); elseif( isset( $arrValues['ap_payment_type_id'] ) ) $this->setApPaymentTypeId( $arrValues['ap_payment_type_id'] );
		if( isset( $arrValues['payment_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentStatusTypeId', trim( $arrValues['payment_status_type_id'] ) ); elseif( isset( $arrValues['payment_status_type_id'] ) ) $this->setPaymentStatusTypeId( $arrValues['payment_status_type_id'] );
		if( isset( $arrValues['return_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReturnTypeId', trim( $arrValues['return_type_id'] ) ); elseif( isset( $arrValues['return_type_id'] ) ) $this->setReturnTypeId( $arrValues['return_type_id'] );
		if( isset( $arrValues['ap_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intApPaymentId', trim( $arrValues['ap_payment_id'] ) ); elseif( isset( $arrValues['ap_payment_id'] ) ) $this->setApPaymentId( $arrValues['ap_payment_id'] );
		if( isset( $arrValues['check_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCheckAccountTypeId', trim( $arrValues['check_account_type_id'] ) ); elseif( isset( $arrValues['check_account_type_id'] ) ) $this->setCheckAccountTypeId( $arrValues['check_account_type_id'] );
		if( isset( $arrValues['ap_payment_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intApPaymentBatchId', trim( $arrValues['ap_payment_batch_id'] ) ); elseif( isset( $arrValues['ap_payment_batch_id'] ) ) $this->setApPaymentBatchId( $arrValues['ap_payment_batch_id'] );
		if( isset( $arrValues['source_check_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSourceCheckAccountTypeId', trim( $arrValues['source_check_account_type_id'] ) ); elseif( isset( $arrValues['source_check_account_type_id'] ) ) $this->setSourceCheckAccountTypeId( $arrValues['source_check_account_type_id'] );
		if( isset( $arrValues['ap_remittance_id'] ) && $boolDirectSet ) $this->set( 'm_intApRemittanceId', trim( $arrValues['ap_remittance_id'] ) ); elseif( isset( $arrValues['ap_remittance_id'] ) ) $this->setApRemittanceId( $arrValues['ap_remittance_id'] );
		if( isset( $arrValues['eft_reference'] ) && $boolDirectSet ) $this->set( 'm_intEftReference', trim( $arrValues['eft_reference'] ) ); elseif( isset( $arrValues['eft_reference'] ) ) $this->setEftReference( $arrValues['eft_reference'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['return_remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strReturnRemotePrimaryKey', trim( $arrValues['return_remote_primary_key'] ) ); elseif( isset( $arrValues['return_remote_primary_key'] ) ) $this->setReturnRemotePrimaryKey( $arrValues['return_remote_primary_key'] );
		if( isset( $arrValues['secure_reference_number'] ) && $boolDirectSet ) $this->set( 'm_intSecureReferenceNumber', trim( $arrValues['secure_reference_number'] ) ); elseif( isset( $arrValues['secure_reference_number'] ) ) $this->setSecureReferenceNumber( $arrValues['secure_reference_number'] );
		if( isset( $arrValues['payment_memo'] ) && $boolDirectSet ) $this->set( 'm_strPaymentMemo', trim( $arrValues['payment_memo'] ) ); elseif( isset( $arrValues['payment_memo'] ) ) $this->setPaymentMemo( $arrValues['payment_memo'] );
		if( isset( $arrValues['payment_date'] ) && $boolDirectSet ) $this->set( 'm_strPaymentDate', trim( $arrValues['payment_date'] ) ); elseif( isset( $arrValues['payment_date'] ) ) $this->setPaymentDate( $arrValues['payment_date'] );
		if( isset( $arrValues['issue_datetime'] ) && $boolDirectSet ) $this->set( 'm_strIssueDatetime', trim( $arrValues['issue_datetime'] ) ); elseif( isset( $arrValues['issue_datetime'] ) ) $this->setIssueDatetime( $arrValues['issue_datetime'] );
		if( isset( $arrValues['payment_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPaymentAmount', trim( $arrValues['payment_amount'] ) ); elseif( isset( $arrValues['payment_amount'] ) ) $this->setPaymentAmount( $arrValues['payment_amount'] );
		if( isset( $arrValues['payee_name'] ) && $boolDirectSet ) $this->set( 'm_strPayeeName', trim( $arrValues['payee_name'] ) ); elseif( isset( $arrValues['payee_name'] ) ) $this->setPayeeName( $arrValues['payee_name'] );
		if( isset( $arrValues['billto_ip_address'] ) && $boolDirectSet ) $this->set( 'm_strBilltoIpAddress', trim( $arrValues['billto_ip_address'] ) ); elseif( isset( $arrValues['billto_ip_address'] ) ) $this->setBilltoIpAddress( $arrValues['billto_ip_address'] );
		if( isset( $arrValues['payment_number'] ) && $boolDirectSet ) $this->set( 'm_strPaymentNumber', trim( $arrValues['payment_number'] ) ); elseif( isset( $arrValues['payment_number'] ) ) $this->setPaymentNumber( $arrValues['payment_number'] );
		if( isset( $arrValues['check_bank_name'] ) && $boolDirectSet ) $this->set( 'm_strCheckBankName', trim( $arrValues['check_bank_name'] ) ); elseif( isset( $arrValues['check_bank_name'] ) ) $this->setCheckBankName( $arrValues['check_bank_name'] );
		if( isset( $arrValues['check_name_on_account'] ) && $boolDirectSet ) $this->set( 'm_strCheckNameOnAccount', trim( $arrValues['check_name_on_account'] ) ); elseif( isset( $arrValues['check_name_on_account'] ) ) $this->setCheckNameOnAccount( $arrValues['check_name_on_account'] );
		if( isset( $arrValues['check_name_on_account_line_2'] ) && $boolDirectSet ) $this->set( 'm_strCheckNameOnAccountLine2', trim( $arrValues['check_name_on_account_line_2'] ) ); elseif( isset( $arrValues['check_name_on_account_line_2'] ) ) $this->setCheckNameOnAccountLine2( $arrValues['check_name_on_account_line_2'] );
		if( isset( $arrValues['check_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strCheckRoutingNumber', trim( $arrValues['check_routing_number'] ) ); elseif( isset( $arrValues['check_routing_number'] ) ) $this->setCheckRoutingNumber( $arrValues['check_routing_number'] );
		if( isset( $arrValues['check_fractional_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strCheckFractionalRoutingNumber', trim( $arrValues['check_fractional_routing_number'] ) ); elseif( isset( $arrValues['check_fractional_routing_number'] ) ) $this->setCheckFractionalRoutingNumber( $arrValues['check_fractional_routing_number'] );
		if( isset( $arrValues['check_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCheckAccountNumberEncrypted', trim( $arrValues['check_account_number_encrypted'] ) ); elseif( isset( $arrValues['check_account_number_encrypted'] ) ) $this->setCheckAccountNumberEncrypted( $arrValues['check_account_number_encrypted'] );
		if( isset( $arrValues['source_check_bank_name'] ) && $boolDirectSet ) $this->set( 'm_strSourceCheckBankName', trim( $arrValues['source_check_bank_name'] ) ); elseif( isset( $arrValues['source_check_bank_name'] ) ) $this->setSourceCheckBankName( $arrValues['source_check_bank_name'] );
		if( isset( $arrValues['source_check_name_on_account'] ) && $boolDirectSet ) $this->set( 'm_strSourceCheckNameOnAccount', trim( $arrValues['source_check_name_on_account'] ) ); elseif( isset( $arrValues['source_check_name_on_account'] ) ) $this->setSourceCheckNameOnAccount( $arrValues['source_check_name_on_account'] );
		if( isset( $arrValues['source_check_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strSourceCheckRoutingNumber', trim( $arrValues['source_check_routing_number'] ) ); elseif( isset( $arrValues['source_check_routing_number'] ) ) $this->setSourceCheckRoutingNumber( $arrValues['source_check_routing_number'] );
		if( isset( $arrValues['source_check_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strSourceCheckAccountNumberEncrypted', trim( $arrValues['source_check_account_number_encrypted'] ) ); elseif( isset( $arrValues['source_check_account_number_encrypted'] ) ) $this->setSourceCheckAccountNumberEncrypted( $arrValues['source_check_account_number_encrypted'] );
		if( isset( $arrValues['is_reversed'] ) && $boolDirectSet ) $this->set( 'm_intIsReversed', trim( $arrValues['is_reversed'] ) ); elseif( isset( $arrValues['is_reversed'] ) ) $this->setIsReversed( $arrValues['is_reversed'] );
		if( isset( $arrValues['is_quick_check'] ) && $boolDirectSet ) $this->set( 'm_intIsQuickCheck', trim( $arrValues['is_quick_check'] ) ); elseif( isset( $arrValues['is_quick_check'] ) ) $this->setIsQuickCheck( $arrValues['is_quick_check'] );
		if( isset( $arrValues['is_unclaimed_property'] ) && $boolDirectSet ) $this->set( 'm_boolIsUnclaimedProperty', trim( stripcslashes( $arrValues['is_unclaimed_property'] ) ) ); elseif( isset( $arrValues['is_unclaimed_property'] ) ) $this->setIsUnclaimedProperty( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_unclaimed_property'] ) : $arrValues['is_unclaimed_property'] );
		if( isset( $arrValues['returned_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnedOn', trim( $arrValues['returned_on'] ) ); elseif( isset( $arrValues['returned_on'] ) ) $this->setReturnedOn( $arrValues['returned_on'] );
		if( isset( $arrValues['batched_on'] ) && $boolDirectSet ) $this->set( 'm_strBatchedOn', trim( $arrValues['batched_on'] ) ); elseif( isset( $arrValues['batched_on'] ) ) $this->setBatchedOn( $arrValues['batched_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setMerchantAccountId( $intMerchantAccountId ) {
		$this->set( 'm_intMerchantAccountId', CStrings::strToIntDef( $intMerchantAccountId, NULL, false ) );
	}

	public function getMerchantAccountId() {
		return $this->m_intMerchantAccountId;
	}

	public function sqlMerchantAccountId() {
		return ( true == isset( $this->m_intMerchantAccountId ) ) ? ( string ) $this->m_intMerchantAccountId : 'NULL';
	}

	public function setBankAccountId( $intBankAccountId ) {
		$this->set( 'm_intBankAccountId', CStrings::strToIntDef( $intBankAccountId, NULL, false ) );
	}

	public function getBankAccountId() {
		return $this->m_intBankAccountId;
	}

	public function sqlBankAccountId() {
		return ( true == isset( $this->m_intBankAccountId ) ) ? ( string ) $this->m_intBankAccountId : 'NULL';
	}

	public function setMerchantGatewayId( $intMerchantGatewayId ) {
		$this->set( 'm_intMerchantGatewayId', CStrings::strToIntDef( $intMerchantGatewayId, NULL, false ) );
	}

	public function getMerchantGatewayId() {
		return $this->m_intMerchantGatewayId;
	}

	public function sqlMerchantGatewayId() {
		return ( true == isset( $this->m_intMerchantGatewayId ) ) ? ( string ) $this->m_intMerchantGatewayId : 'NULL';
	}

	public function setPaymentMediumId( $intPaymentMediumId ) {
		$this->set( 'm_intPaymentMediumId', CStrings::strToIntDef( $intPaymentMediumId, NULL, false ) );
	}

	public function getPaymentMediumId() {
		return $this->m_intPaymentMediumId;
	}

	public function sqlPaymentMediumId() {
		return ( true == isset( $this->m_intPaymentMediumId ) ) ? ( string ) $this->m_intPaymentMediumId : 'NULL';
	}

	public function setApPaymentTypeId( $intApPaymentTypeId ) {
		$this->set( 'm_intApPaymentTypeId', CStrings::strToIntDef( $intApPaymentTypeId, NULL, false ) );
	}

	public function getApPaymentTypeId() {
		return $this->m_intApPaymentTypeId;
	}

	public function sqlApPaymentTypeId() {
		return ( true == isset( $this->m_intApPaymentTypeId ) ) ? ( string ) $this->m_intApPaymentTypeId : 'NULL';
	}

	public function setPaymentStatusTypeId( $intPaymentStatusTypeId ) {
		$this->set( 'm_intPaymentStatusTypeId', CStrings::strToIntDef( $intPaymentStatusTypeId, NULL, false ) );
	}

	public function getPaymentStatusTypeId() {
		return $this->m_intPaymentStatusTypeId;
	}

	public function sqlPaymentStatusTypeId() {
		return ( true == isset( $this->m_intPaymentStatusTypeId ) ) ? ( string ) $this->m_intPaymentStatusTypeId : 'NULL';
	}

	public function setReturnTypeId( $intReturnTypeId ) {
		$this->set( 'm_intReturnTypeId', CStrings::strToIntDef( $intReturnTypeId, NULL, false ) );
	}

	public function getReturnTypeId() {
		return $this->m_intReturnTypeId;
	}

	public function sqlReturnTypeId() {
		return ( true == isset( $this->m_intReturnTypeId ) ) ? ( string ) $this->m_intReturnTypeId : 'NULL';
	}

	public function setApPaymentId( $intApPaymentId ) {
		$this->set( 'm_intApPaymentId', CStrings::strToIntDef( $intApPaymentId, NULL, false ) );
	}

	public function getApPaymentId() {
		return $this->m_intApPaymentId;
	}

	public function sqlApPaymentId() {
		return ( true == isset( $this->m_intApPaymentId ) ) ? ( string ) $this->m_intApPaymentId : 'NULL';
	}

	public function setCheckAccountTypeId( $intCheckAccountTypeId ) {
		$this->set( 'm_intCheckAccountTypeId', CStrings::strToIntDef( $intCheckAccountTypeId, NULL, false ) );
	}

	public function getCheckAccountTypeId() {
		return $this->m_intCheckAccountTypeId;
	}

	public function sqlCheckAccountTypeId() {
		return ( true == isset( $this->m_intCheckAccountTypeId ) ) ? ( string ) $this->m_intCheckAccountTypeId : 'NULL';
	}

	public function setApPaymentBatchId( $intApPaymentBatchId ) {
		$this->set( 'm_intApPaymentBatchId', CStrings::strToIntDef( $intApPaymentBatchId, NULL, false ) );
	}

	public function getApPaymentBatchId() {
		return $this->m_intApPaymentBatchId;
	}

	public function sqlApPaymentBatchId() {
		return ( true == isset( $this->m_intApPaymentBatchId ) ) ? ( string ) $this->m_intApPaymentBatchId : 'NULL';
	}

	public function setSourceCheckAccountTypeId( $intSourceCheckAccountTypeId ) {
		$this->set( 'm_intSourceCheckAccountTypeId', CStrings::strToIntDef( $intSourceCheckAccountTypeId, NULL, false ) );
	}

	public function getSourceCheckAccountTypeId() {
		return $this->m_intSourceCheckAccountTypeId;
	}

	public function sqlSourceCheckAccountTypeId() {
		return ( true == isset( $this->m_intSourceCheckAccountTypeId ) ) ? ( string ) $this->m_intSourceCheckAccountTypeId : 'NULL';
	}

	public function setApRemittanceId( $intApRemittanceId ) {
		$this->set( 'm_intApRemittanceId', CStrings::strToIntDef( $intApRemittanceId, NULL, false ) );
	}

	public function getApRemittanceId() {
		return $this->m_intApRemittanceId;
	}

	public function sqlApRemittanceId() {
		return ( true == isset( $this->m_intApRemittanceId ) ) ? ( string ) $this->m_intApRemittanceId : 'NULL';
	}

	public function setEftReference( $intEftReference ) {
		$this->set( 'm_intEftReference', CStrings::strToIntDef( $intEftReference, NULL, false ) );
	}

	public function getEftReference() {
		return $this->m_intEftReference;
	}

	public function sqlEftReference() {
		return ( true == isset( $this->m_intEftReference ) ) ? ( string ) $this->m_intEftReference : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setReturnRemotePrimaryKey( $strReturnRemotePrimaryKey ) {
		$this->set( 'm_strReturnRemotePrimaryKey', CStrings::strTrimDef( $strReturnRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getReturnRemotePrimaryKey() {
		return $this->m_strReturnRemotePrimaryKey;
	}

	public function sqlReturnRemotePrimaryKey() {
		return ( true == isset( $this->m_strReturnRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReturnRemotePrimaryKey ) : '\'' . addslashes( $this->m_strReturnRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setSecureReferenceNumber( $intSecureReferenceNumber ) {
		$this->set( 'm_intSecureReferenceNumber', CStrings::strToIntDef( $intSecureReferenceNumber, NULL, false ) );
	}

	public function getSecureReferenceNumber() {
		return $this->m_intSecureReferenceNumber;
	}

	public function sqlSecureReferenceNumber() {
		return ( true == isset( $this->m_intSecureReferenceNumber ) ) ? ( string ) $this->m_intSecureReferenceNumber : 'NULL';
	}

	public function setPaymentMemo( $strPaymentMemo ) {
		$this->set( 'm_strPaymentMemo', CStrings::strTrimDef( $strPaymentMemo, 2000, NULL, true ) );
	}

	public function getPaymentMemo() {
		return $this->m_strPaymentMemo;
	}

	public function sqlPaymentMemo() {
		return ( true == isset( $this->m_strPaymentMemo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPaymentMemo ) : '\'' . addslashes( $this->m_strPaymentMemo ) . '\'' ) : 'NULL';
	}

	public function setPaymentDate( $strPaymentDate ) {
		$this->set( 'm_strPaymentDate', CStrings::strTrimDef( $strPaymentDate, -1, NULL, true ) );
	}

	public function getPaymentDate() {
		return $this->m_strPaymentDate;
	}

	public function sqlPaymentDate() {
		return ( true == isset( $this->m_strPaymentDate ) ) ? '\'' . $this->m_strPaymentDate . '\'' : 'NOW()';
	}

	public function setIssueDatetime( $strIssueDatetime ) {
		$this->set( 'm_strIssueDatetime', CStrings::strTrimDef( $strIssueDatetime, -1, NULL, true ) );
	}

	public function getIssueDatetime() {
		return $this->m_strIssueDatetime;
	}

	public function sqlIssueDatetime() {
		return ( true == isset( $this->m_strIssueDatetime ) ) ? '\'' . $this->m_strIssueDatetime . '\'' : 'NOW()';
	}

	public function setPaymentAmount( $fltPaymentAmount ) {
		$this->set( 'm_fltPaymentAmount', CStrings::strToFloatDef( $fltPaymentAmount, NULL, false, 2 ) );
	}

	public function getPaymentAmount() {
		return $this->m_fltPaymentAmount;
	}

	public function sqlPaymentAmount() {
		return ( true == isset( $this->m_fltPaymentAmount ) ) ? ( string ) $this->m_fltPaymentAmount : 'NULL';
	}

	public function setPayeeName( $strPayeeName ) {
		$this->set( 'm_strPayeeName', CStrings::strTrimDef( $strPayeeName, 2000, NULL, true ) );
	}

	public function getPayeeName() {
		return $this->m_strPayeeName;
	}

	public function sqlPayeeName() {
		return ( true == isset( $this->m_strPayeeName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPayeeName ) : '\'' . addslashes( $this->m_strPayeeName ) . '\'' ) : 'NULL';
	}

	public function setBilltoIpAddress( $strBilltoIpAddress ) {
		$this->set( 'm_strBilltoIpAddress', CStrings::strTrimDef( $strBilltoIpAddress, 23, NULL, true ) );
	}

	public function getBilltoIpAddress() {
		return $this->m_strBilltoIpAddress;
	}

	public function sqlBilltoIpAddress() {
		return ( true == isset( $this->m_strBilltoIpAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoIpAddress ) : '\'' . addslashes( $this->m_strBilltoIpAddress ) . '\'' ) : 'NULL';
	}

	public function setPaymentNumber( $strPaymentNumber ) {
		$this->set( 'm_strPaymentNumber', CStrings::strTrimDef( $strPaymentNumber, 40, NULL, true ) );
	}

	public function getPaymentNumber() {
		return $this->m_strPaymentNumber;
	}

	public function sqlPaymentNumber() {
		return ( true == isset( $this->m_strPaymentNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPaymentNumber ) : '\'' . addslashes( $this->m_strPaymentNumber ) . '\'' ) : 'NULL';
	}

	public function setCheckBankName( $strCheckBankName ) {
		$this->set( 'm_strCheckBankName', CStrings::strTrimDef( $strCheckBankName, 100, NULL, true ) );
	}

	public function getCheckBankName() {
		return $this->m_strCheckBankName;
	}

	public function sqlCheckBankName() {
		return ( true == isset( $this->m_strCheckBankName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckBankName ) : '\'' . addslashes( $this->m_strCheckBankName ) . '\'' ) : 'NULL';
	}

	public function setCheckNameOnAccount( $strCheckNameOnAccount ) {
		$this->set( 'm_strCheckNameOnAccount', CStrings::strTrimDef( $strCheckNameOnAccount, 50, NULL, true ) );
	}

	public function getCheckNameOnAccount() {
		return $this->m_strCheckNameOnAccount;
	}

	public function sqlCheckNameOnAccount() {
		return ( true == isset( $this->m_strCheckNameOnAccount ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckNameOnAccount ) : '\'' . addslashes( $this->m_strCheckNameOnAccount ) . '\'' ) : 'NULL';
	}

	public function setCheckNameOnAccountLine2( $strCheckNameOnAccountLine2 ) {
		$this->set( 'm_strCheckNameOnAccountLine2', CStrings::strTrimDef( $strCheckNameOnAccountLine2, 50, NULL, true ) );
	}

	public function getCheckNameOnAccountLine2() {
		return $this->m_strCheckNameOnAccountLine2;
	}

	public function sqlCheckNameOnAccountLine2() {
		return ( true == isset( $this->m_strCheckNameOnAccountLine2 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckNameOnAccountLine2 ) : '\'' . addslashes( $this->m_strCheckNameOnAccountLine2 ) . '\'' ) : 'NULL';
	}

	public function setCheckRoutingNumber( $strCheckRoutingNumber ) {
		$this->set( 'm_strCheckRoutingNumber', CStrings::strTrimDef( $strCheckRoutingNumber, 240, NULL, true ) );
	}

	public function getCheckRoutingNumber() {
		return $this->m_strCheckRoutingNumber;
	}

	public function sqlCheckRoutingNumber() {
		return ( true == isset( $this->m_strCheckRoutingNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckRoutingNumber ) : '\'' . addslashes( $this->m_strCheckRoutingNumber ) . '\'' ) : 'NULL';
	}

	public function setCheckFractionalRoutingNumber( $strCheckFractionalRoutingNumber ) {
		$this->set( 'm_strCheckFractionalRoutingNumber', CStrings::strTrimDef( $strCheckFractionalRoutingNumber, 50, NULL, true ) );
	}

	public function getCheckFractionalRoutingNumber() {
		return $this->m_strCheckFractionalRoutingNumber;
	}

	public function sqlCheckFractionalRoutingNumber() {
		return ( true == isset( $this->m_strCheckFractionalRoutingNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckFractionalRoutingNumber ) : '\'' . addslashes( $this->m_strCheckFractionalRoutingNumber ) . '\'' ) : 'NULL';
	}

	public function setCheckAccountNumberEncrypted( $strCheckAccountNumberEncrypted ) {
		$this->set( 'm_strCheckAccountNumberEncrypted', CStrings::strTrimDef( $strCheckAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getCheckAccountNumberEncrypted() {
		return $this->m_strCheckAccountNumberEncrypted;
	}

	public function sqlCheckAccountNumberEncrypted() {
		return ( true == isset( $this->m_strCheckAccountNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckAccountNumberEncrypted ) : '\'' . addslashes( $this->m_strCheckAccountNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setSourceCheckBankName( $strSourceCheckBankName ) {
		$this->set( 'm_strSourceCheckBankName', CStrings::strTrimDef( $strSourceCheckBankName, 100, NULL, true ) );
	}

	public function getSourceCheckBankName() {
		return $this->m_strSourceCheckBankName;
	}

	public function sqlSourceCheckBankName() {
		return ( true == isset( $this->m_strSourceCheckBankName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSourceCheckBankName ) : '\'' . addslashes( $this->m_strSourceCheckBankName ) . '\'' ) : 'NULL';
	}

	public function setSourceCheckNameOnAccount( $strSourceCheckNameOnAccount ) {
		$this->set( 'm_strSourceCheckNameOnAccount', CStrings::strTrimDef( $strSourceCheckNameOnAccount, 50, NULL, true ) );
	}

	public function getSourceCheckNameOnAccount() {
		return $this->m_strSourceCheckNameOnAccount;
	}

	public function sqlSourceCheckNameOnAccount() {
		return ( true == isset( $this->m_strSourceCheckNameOnAccount ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSourceCheckNameOnAccount ) : '\'' . addslashes( $this->m_strSourceCheckNameOnAccount ) . '\'' ) : 'NULL';
	}

	public function setSourceCheckRoutingNumber( $strSourceCheckRoutingNumber ) {
		$this->set( 'm_strSourceCheckRoutingNumber', CStrings::strTrimDef( $strSourceCheckRoutingNumber, 240, NULL, true ) );
	}

	public function getSourceCheckRoutingNumber() {
		return $this->m_strSourceCheckRoutingNumber;
	}

	public function sqlSourceCheckRoutingNumber() {
		return ( true == isset( $this->m_strSourceCheckRoutingNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSourceCheckRoutingNumber ) : '\'' . addslashes( $this->m_strSourceCheckRoutingNumber ) . '\'' ) : 'NULL';
	}

	public function setSourceCheckAccountNumberEncrypted( $strSourceCheckAccountNumberEncrypted ) {
		$this->set( 'm_strSourceCheckAccountNumberEncrypted', CStrings::strTrimDef( $strSourceCheckAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getSourceCheckAccountNumberEncrypted() {
		return $this->m_strSourceCheckAccountNumberEncrypted;
	}

	public function sqlSourceCheckAccountNumberEncrypted() {
		return ( true == isset( $this->m_strSourceCheckAccountNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSourceCheckAccountNumberEncrypted ) : '\'' . addslashes( $this->m_strSourceCheckAccountNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setIsReversed( $intIsReversed ) {
		$this->set( 'm_intIsReversed', CStrings::strToIntDef( $intIsReversed, NULL, false ) );
	}

	public function getIsReversed() {
		return $this->m_intIsReversed;
	}

	public function sqlIsReversed() {
		return ( true == isset( $this->m_intIsReversed ) ) ? ( string ) $this->m_intIsReversed : '0';
	}

	public function setIsQuickCheck( $intIsQuickCheck ) {
		$this->set( 'm_intIsQuickCheck', CStrings::strToIntDef( $intIsQuickCheck, NULL, false ) );
	}

	public function getIsQuickCheck() {
		return $this->m_intIsQuickCheck;
	}

	public function sqlIsQuickCheck() {
		return ( true == isset( $this->m_intIsQuickCheck ) ) ? ( string ) $this->m_intIsQuickCheck : '0';
	}

	public function setIsUnclaimedProperty( $boolIsUnclaimedProperty ) {
		$this->set( 'm_boolIsUnclaimedProperty', CStrings::strToBool( $boolIsUnclaimedProperty ) );
	}

	public function getIsUnclaimedProperty() {
		return $this->m_boolIsUnclaimedProperty;
	}

	public function sqlIsUnclaimedProperty() {
		return ( true == isset( $this->m_boolIsUnclaimedProperty ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsUnclaimedProperty ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setReturnedOn( $strReturnedOn ) {
		$this->set( 'm_strReturnedOn', CStrings::strTrimDef( $strReturnedOn, -1, NULL, true ) );
	}

	public function getReturnedOn() {
		return $this->m_strReturnedOn;
	}

	public function sqlReturnedOn() {
		return ( true == isset( $this->m_strReturnedOn ) ) ? '\'' . $this->m_strReturnedOn . '\'' : 'NULL';
	}

	public function setBatchedOn( $strBatchedOn ) {
		$this->set( 'm_strBatchedOn', CStrings::strTrimDef( $strBatchedOn, -1, NULL, true ) );
	}

	public function getBatchedOn() {
		return $this->m_strBatchedOn;
	}

	public function sqlBatchedOn() {
		return ( true == isset( $this->m_strBatchedOn ) ) ? '\'' . $this->m_strBatchedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, merchant_account_id, bank_account_id, merchant_gateway_id, payment_medium_id, ap_payment_type_id, payment_status_type_id, return_type_id, ap_payment_id, check_account_type_id, ap_payment_batch_id, source_check_account_type_id, ap_remittance_id, eft_reference, remote_primary_key, return_remote_primary_key, secure_reference_number, payment_memo, payment_date, issue_datetime, payment_amount, payee_name, billto_ip_address, payment_number, check_bank_name, check_name_on_account, check_name_on_account_line_2, check_routing_number, check_fractional_routing_number, check_account_number_encrypted, source_check_bank_name, source_check_name_on_account, source_check_routing_number, source_check_account_number_encrypted, is_reversed, is_quick_check, is_unclaimed_property, returned_on, batched_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlMerchantAccountId() . ', ' .
						$this->sqlBankAccountId() . ', ' .
						$this->sqlMerchantGatewayId() . ', ' .
						$this->sqlPaymentMediumId() . ', ' .
						$this->sqlApPaymentTypeId() . ', ' .
						$this->sqlPaymentStatusTypeId() . ', ' .
						$this->sqlReturnTypeId() . ', ' .
						$this->sqlApPaymentId() . ', ' .
						$this->sqlCheckAccountTypeId() . ', ' .
						$this->sqlApPaymentBatchId() . ', ' .
						$this->sqlSourceCheckAccountTypeId() . ', ' .
						$this->sqlApRemittanceId() . ', ' .
						$this->sqlEftReference() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlReturnRemotePrimaryKey() . ', ' .
						$this->sqlSecureReferenceNumber() . ', ' .
						$this->sqlPaymentMemo() . ', ' .
						$this->sqlPaymentDate() . ', ' .
						$this->sqlIssueDatetime() . ', ' .
						$this->sqlPaymentAmount() . ', ' .
						$this->sqlPayeeName() . ', ' .
						$this->sqlBilltoIpAddress() . ', ' .
						$this->sqlPaymentNumber() . ', ' .
						$this->sqlCheckBankName() . ', ' .
						$this->sqlCheckNameOnAccount() . ', ' .
						$this->sqlCheckNameOnAccountLine2() . ', ' .
						$this->sqlCheckRoutingNumber() . ', ' .
						$this->sqlCheckFractionalRoutingNumber() . ', ' .
						$this->sqlCheckAccountNumberEncrypted() . ', ' .
						$this->sqlSourceCheckBankName() . ', ' .
						$this->sqlSourceCheckNameOnAccount() . ', ' .
						$this->sqlSourceCheckRoutingNumber() . ', ' .
						$this->sqlSourceCheckAccountNumberEncrypted() . ', ' .
						$this->sqlIsReversed() . ', ' .
						$this->sqlIsQuickCheck() . ', ' .
						$this->sqlIsUnclaimedProperty() . ', ' .
						$this->sqlReturnedOn() . ', ' .
						$this->sqlBatchedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_account_id = ' . $this->sqlMerchantAccountId(). ',' ; } elseif( true == array_key_exists( 'MerchantAccountId', $this->getChangedColumns() ) ) { $strSql .= ' merchant_account_id = ' . $this->sqlMerchantAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_account_id = ' . $this->sqlBankAccountId(). ',' ; } elseif( true == array_key_exists( 'BankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' bank_account_id = ' . $this->sqlBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_gateway_id = ' . $this->sqlMerchantGatewayId(). ',' ; } elseif( true == array_key_exists( 'MerchantGatewayId', $this->getChangedColumns() ) ) { $strSql .= ' merchant_gateway_id = ' . $this->sqlMerchantGatewayId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_medium_id = ' . $this->sqlPaymentMediumId(). ',' ; } elseif( true == array_key_exists( 'PaymentMediumId', $this->getChangedColumns() ) ) { $strSql .= ' payment_medium_id = ' . $this->sqlPaymentMediumId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payment_type_id = ' . $this->sqlApPaymentTypeId(). ',' ; } elseif( true == array_key_exists( 'ApPaymentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payment_type_id = ' . $this->sqlApPaymentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_status_type_id = ' . $this->sqlPaymentStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'PaymentStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_status_type_id = ' . $this->sqlPaymentStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId(). ',' ; } elseif( true == array_key_exists( 'ReturnTypeId', $this->getChangedColumns() ) ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payment_id = ' . $this->sqlApPaymentId(). ',' ; } elseif( true == array_key_exists( 'ApPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payment_id = ' . $this->sqlApPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId(). ',' ; } elseif( true == array_key_exists( 'CheckAccountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payment_batch_id = ' . $this->sqlApPaymentBatchId(). ',' ; } elseif( true == array_key_exists( 'ApPaymentBatchId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payment_batch_id = ' . $this->sqlApPaymentBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_check_account_type_id = ' . $this->sqlSourceCheckAccountTypeId(). ',' ; } elseif( true == array_key_exists( 'SourceCheckAccountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' source_check_account_type_id = ' . $this->sqlSourceCheckAccountTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_remittance_id = ' . $this->sqlApRemittanceId(). ',' ; } elseif( true == array_key_exists( 'ApRemittanceId', $this->getChangedColumns() ) ) { $strSql .= ' ap_remittance_id = ' . $this->sqlApRemittanceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eft_reference = ' . $this->sqlEftReference(). ',' ; } elseif( true == array_key_exists( 'EftReference', $this->getChangedColumns() ) ) { $strSql .= ' eft_reference = ' . $this->sqlEftReference() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_remote_primary_key = ' . $this->sqlReturnRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'ReturnRemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' return_remote_primary_key = ' . $this->sqlReturnRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' secure_reference_number = ' . $this->sqlSecureReferenceNumber(). ',' ; } elseif( true == array_key_exists( 'SecureReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' secure_reference_number = ' . $this->sqlSecureReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_memo = ' . $this->sqlPaymentMemo(). ',' ; } elseif( true == array_key_exists( 'PaymentMemo', $this->getChangedColumns() ) ) { $strSql .= ' payment_memo = ' . $this->sqlPaymentMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_date = ' . $this->sqlPaymentDate(). ',' ; } elseif( true == array_key_exists( 'PaymentDate', $this->getChangedColumns() ) ) { $strSql .= ' payment_date = ' . $this->sqlPaymentDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' issue_datetime = ' . $this->sqlIssueDatetime(). ',' ; } elseif( true == array_key_exists( 'IssueDatetime', $this->getChangedColumns() ) ) { $strSql .= ' issue_datetime = ' . $this->sqlIssueDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_amount = ' . $this->sqlPaymentAmount(). ',' ; } elseif( true == array_key_exists( 'PaymentAmount', $this->getChangedColumns() ) ) { $strSql .= ' payment_amount = ' . $this->sqlPaymentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payee_name = ' . $this->sqlPayeeName(). ',' ; } elseif( true == array_key_exists( 'PayeeName', $this->getChangedColumns() ) ) { $strSql .= ' payee_name = ' . $this->sqlPayeeName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_ip_address = ' . $this->sqlBilltoIpAddress(). ',' ; } elseif( true == array_key_exists( 'BilltoIpAddress', $this->getChangedColumns() ) ) { $strSql .= ' billto_ip_address = ' . $this->sqlBilltoIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_number = ' . $this->sqlPaymentNumber(). ',' ; } elseif( true == array_key_exists( 'PaymentNumber', $this->getChangedColumns() ) ) { $strSql .= ' payment_number = ' . $this->sqlPaymentNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_bank_name = ' . $this->sqlCheckBankName(). ',' ; } elseif( true == array_key_exists( 'CheckBankName', $this->getChangedColumns() ) ) { $strSql .= ' check_bank_name = ' . $this->sqlCheckBankName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_name_on_account = ' . $this->sqlCheckNameOnAccount(). ',' ; } elseif( true == array_key_exists( 'CheckNameOnAccount', $this->getChangedColumns() ) ) { $strSql .= ' check_name_on_account = ' . $this->sqlCheckNameOnAccount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_name_on_account_line_2 = ' . $this->sqlCheckNameOnAccountLine2(). ',' ; } elseif( true == array_key_exists( 'CheckNameOnAccountLine2', $this->getChangedColumns() ) ) { $strSql .= ' check_name_on_account_line_2 = ' . $this->sqlCheckNameOnAccountLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber(). ',' ; } elseif( true == array_key_exists( 'CheckRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_fractional_routing_number = ' . $this->sqlCheckFractionalRoutingNumber(). ',' ; } elseif( true == array_key_exists( 'CheckFractionalRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' check_fractional_routing_number = ' . $this->sqlCheckFractionalRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'CheckAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_check_bank_name = ' . $this->sqlSourceCheckBankName(). ',' ; } elseif( true == array_key_exists( 'SourceCheckBankName', $this->getChangedColumns() ) ) { $strSql .= ' source_check_bank_name = ' . $this->sqlSourceCheckBankName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_check_name_on_account = ' . $this->sqlSourceCheckNameOnAccount(). ',' ; } elseif( true == array_key_exists( 'SourceCheckNameOnAccount', $this->getChangedColumns() ) ) { $strSql .= ' source_check_name_on_account = ' . $this->sqlSourceCheckNameOnAccount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_check_routing_number = ' . $this->sqlSourceCheckRoutingNumber(). ',' ; } elseif( true == array_key_exists( 'SourceCheckRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' source_check_routing_number = ' . $this->sqlSourceCheckRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_check_account_number_encrypted = ' . $this->sqlSourceCheckAccountNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'SourceCheckAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' source_check_account_number_encrypted = ' . $this->sqlSourceCheckAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reversed = ' . $this->sqlIsReversed(). ',' ; } elseif( true == array_key_exists( 'IsReversed', $this->getChangedColumns() ) ) { $strSql .= ' is_reversed = ' . $this->sqlIsReversed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_quick_check = ' . $this->sqlIsQuickCheck(). ',' ; } elseif( true == array_key_exists( 'IsQuickCheck', $this->getChangedColumns() ) ) { $strSql .= ' is_quick_check = ' . $this->sqlIsQuickCheck() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_unclaimed_property = ' . $this->sqlIsUnclaimedProperty(). ',' ; } elseif( true == array_key_exists( 'IsUnclaimedProperty', $this->getChangedColumns() ) ) { $strSql .= ' is_unclaimed_property = ' . $this->sqlIsUnclaimedProperty() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn(). ',' ; } elseif( true == array_key_exists( 'ReturnedOn', $this->getChangedColumns() ) ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batched_on = ' . $this->sqlBatchedOn(). ',' ; } elseif( true == array_key_exists( 'BatchedOn', $this->getChangedColumns() ) ) { $strSql .= ' batched_on = ' . $this->sqlBatchedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'merchant_account_id' => $this->getMerchantAccountId(),
			'bank_account_id' => $this->getBankAccountId(),
			'merchant_gateway_id' => $this->getMerchantGatewayId(),
			'payment_medium_id' => $this->getPaymentMediumId(),
			'ap_payment_type_id' => $this->getApPaymentTypeId(),
			'payment_status_type_id' => $this->getPaymentStatusTypeId(),
			'return_type_id' => $this->getReturnTypeId(),
			'ap_payment_id' => $this->getApPaymentId(),
			'check_account_type_id' => $this->getCheckAccountTypeId(),
			'ap_payment_batch_id' => $this->getApPaymentBatchId(),
			'source_check_account_type_id' => $this->getSourceCheckAccountTypeId(),
			'ap_remittance_id' => $this->getApRemittanceId(),
			'eft_reference' => $this->getEftReference(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'return_remote_primary_key' => $this->getReturnRemotePrimaryKey(),
			'secure_reference_number' => $this->getSecureReferenceNumber(),
			'payment_memo' => $this->getPaymentMemo(),
			'payment_date' => $this->getPaymentDate(),
			'issue_datetime' => $this->getIssueDatetime(),
			'payment_amount' => $this->getPaymentAmount(),
			'payee_name' => $this->getPayeeName(),
			'billto_ip_address' => $this->getBilltoIpAddress(),
			'payment_number' => $this->getPaymentNumber(),
			'check_bank_name' => $this->getCheckBankName(),
			'check_name_on_account' => $this->getCheckNameOnAccount(),
			'check_name_on_account_line_2' => $this->getCheckNameOnAccountLine2(),
			'check_routing_number' => $this->getCheckRoutingNumber(),
			'check_fractional_routing_number' => $this->getCheckFractionalRoutingNumber(),
			'check_account_number_encrypted' => $this->getCheckAccountNumberEncrypted(),
			'source_check_bank_name' => $this->getSourceCheckBankName(),
			'source_check_name_on_account' => $this->getSourceCheckNameOnAccount(),
			'source_check_routing_number' => $this->getSourceCheckRoutingNumber(),
			'source_check_account_number_encrypted' => $this->getSourceCheckAccountNumberEncrypted(),
			'is_reversed' => $this->getIsReversed(),
			'is_quick_check' => $this->getIsQuickCheck(),
			'is_unclaimed_property' => $this->getIsUnclaimedProperty(),
			'returned_on' => $this->getReturnedOn(),
			'batched_on' => $this->getBatchedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>