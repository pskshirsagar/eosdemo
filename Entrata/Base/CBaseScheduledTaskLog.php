<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledTaskLog extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.scheduled_task_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScheduledTaskId;
	protected $m_intScheduledTaskTypeId;
	protected $m_intPropertyId;
	protected $m_intEventTypeId;
	protected $m_intEventSubTypeId;
	protected $m_intPriorScheduledTaskLogId;
	protected $m_strPostDate;
	protected $m_strApplyThroughPostDate;
	protected $m_strScheduleDetails;
	protected $m_jsonScheduleDetails;
	protected $m_strTaskDetails;
	protected $m_jsonTaskDetails;
	protected $m_strLogDatetime;
	protected $m_intIsPostDateIgnored;
	protected $m_intIsOpeningLog;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_strApplyThroughPostDate = '12/31/2099';
		$this->m_intIsPostDateIgnored = '0';
		$this->m_intIsOpeningLog = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['scheduled_task_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledTaskId', trim( $arrValues['scheduled_task_id'] ) ); elseif( isset( $arrValues['scheduled_task_id'] ) ) $this->setScheduledTaskId( $arrValues['scheduled_task_id'] );
		if( isset( $arrValues['scheduled_task_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledTaskTypeId', trim( $arrValues['scheduled_task_type_id'] ) ); elseif( isset( $arrValues['scheduled_task_type_id'] ) ) $this->setScheduledTaskTypeId( $arrValues['scheduled_task_type_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['event_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEventTypeId', trim( $arrValues['event_type_id'] ) ); elseif( isset( $arrValues['event_type_id'] ) ) $this->setEventTypeId( $arrValues['event_type_id'] );
		if( isset( $arrValues['event_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEventSubTypeId', trim( $arrValues['event_sub_type_id'] ) ); elseif( isset( $arrValues['event_sub_type_id'] ) ) $this->setEventSubTypeId( $arrValues['event_sub_type_id'] );
		if( isset( $arrValues['prior_scheduled_task_log_id'] ) && $boolDirectSet ) $this->set( 'm_intPriorScheduledTaskLogId', trim( $arrValues['prior_scheduled_task_log_id'] ) ); elseif( isset( $arrValues['prior_scheduled_task_log_id'] ) ) $this->setPriorScheduledTaskLogId( $arrValues['prior_scheduled_task_log_id'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['apply_through_post_date'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostDate', trim( $arrValues['apply_through_post_date'] ) ); elseif( isset( $arrValues['apply_through_post_date'] ) ) $this->setApplyThroughPostDate( $arrValues['apply_through_post_date'] );
		if( isset( $arrValues['schedule_details'] ) ) $this->set( 'm_strScheduleDetails', trim( $arrValues['schedule_details'] ) );
		if( isset( $arrValues['task_details'] ) ) $this->set( 'm_strTaskDetails', trim( $arrValues['task_details'] ) );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['is_post_date_ignored'] ) && $boolDirectSet ) $this->set( 'm_intIsPostDateIgnored', trim( $arrValues['is_post_date_ignored'] ) ); elseif( isset( $arrValues['is_post_date_ignored'] ) ) $this->setIsPostDateIgnored( $arrValues['is_post_date_ignored'] );
		if( isset( $arrValues['is_opening_log'] ) && $boolDirectSet ) $this->set( 'm_intIsOpeningLog', trim( $arrValues['is_opening_log'] ) ); elseif( isset( $arrValues['is_opening_log'] ) ) $this->setIsOpeningLog( $arrValues['is_opening_log'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScheduledTaskId( $intScheduledTaskId ) {
		$this->set( 'm_intScheduledTaskId', CStrings::strToIntDef( $intScheduledTaskId, NULL, false ) );
	}

	public function getScheduledTaskId() {
		return $this->m_intScheduledTaskId;
	}

	public function sqlScheduledTaskId() {
		return ( true == isset( $this->m_intScheduledTaskId ) ) ? ( string ) $this->m_intScheduledTaskId : 'NULL';
	}

	public function setScheduledTaskTypeId( $intScheduledTaskTypeId ) {
		$this->set( 'm_intScheduledTaskTypeId', CStrings::strToIntDef( $intScheduledTaskTypeId, NULL, false ) );
	}

	public function getScheduledTaskTypeId() {
		return $this->m_intScheduledTaskTypeId;
	}

	public function sqlScheduledTaskTypeId() {
		return ( true == isset( $this->m_intScheduledTaskTypeId ) ) ? ( string ) $this->m_intScheduledTaskTypeId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setEventTypeId( $intEventTypeId ) {
		$this->set( 'm_intEventTypeId', CStrings::strToIntDef( $intEventTypeId, NULL, false ) );
	}

	public function getEventTypeId() {
		return $this->m_intEventTypeId;
	}

	public function sqlEventTypeId() {
		return ( true == isset( $this->m_intEventTypeId ) ) ? ( string ) $this->m_intEventTypeId : 'NULL';
	}

	public function setEventSubTypeId( $intEventSubTypeId ) {
		$this->set( 'm_intEventSubTypeId', CStrings::strToIntDef( $intEventSubTypeId, NULL, false ) );
	}

	public function getEventSubTypeId() {
		return $this->m_intEventSubTypeId;
	}

	public function sqlEventSubTypeId() {
		return ( true == isset( $this->m_intEventSubTypeId ) ) ? ( string ) $this->m_intEventSubTypeId : 'NULL';
	}

	public function setPriorScheduledTaskLogId( $intPriorScheduledTaskLogId ) {
		$this->set( 'm_intPriorScheduledTaskLogId', CStrings::strToIntDef( $intPriorScheduledTaskLogId, NULL, false ) );
	}

	public function getPriorScheduledTaskLogId() {
		return $this->m_intPriorScheduledTaskLogId;
	}

	public function sqlPriorScheduledTaskLogId() {
		return ( true == isset( $this->m_intPriorScheduledTaskLogId ) ) ? ( string ) $this->m_intPriorScheduledTaskLogId : 'NULL';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setApplyThroughPostDate( $strApplyThroughPostDate ) {
		$this->set( 'm_strApplyThroughPostDate', CStrings::strTrimDef( $strApplyThroughPostDate, -1, NULL, true ) );
	}

	public function getApplyThroughPostDate() {
		return $this->m_strApplyThroughPostDate;
	}

	public function sqlApplyThroughPostDate() {
		return ( true == isset( $this->m_strApplyThroughPostDate ) ) ? '\'' . $this->m_strApplyThroughPostDate . '\'' : 'NOW()';
	}

	public function setScheduleDetails( $jsonScheduleDetails ) {
		if( true == valObj( $jsonScheduleDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonScheduleDetails', $jsonScheduleDetails );
		} elseif( true == valJsonString( $jsonScheduleDetails ) ) {
			$this->set( 'm_jsonScheduleDetails', CStrings::strToJson( $jsonScheduleDetails ) );
		} else {
			$this->set( 'm_jsonScheduleDetails', NULL );
		}
		unset( $this->m_strScheduleDetails );
	}

	public function getScheduleDetails() {
		if( true == isset( $this->m_strScheduleDetails ) ) {
			$this->m_jsonScheduleDetails = CStrings::strToJson( $this->m_strScheduleDetails );
			unset( $this->m_strScheduleDetails );
		}
		return $this->m_jsonScheduleDetails;
	}

	public function sqlScheduleDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getScheduleDetails() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getScheduleDetails() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setTaskDetails( $jsonTaskDetails ) {
		if( true == valObj( $jsonTaskDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonTaskDetails', $jsonTaskDetails );
		} elseif( true == valJsonString( $jsonTaskDetails ) ) {
			$this->set( 'm_jsonTaskDetails', CStrings::strToJson( $jsonTaskDetails ) );
		} else {
			$this->set( 'm_jsonTaskDetails', NULL );
		}
		unset( $this->m_strTaskDetails );
	}

	public function getTaskDetails() {
		if( true == isset( $this->m_strTaskDetails ) ) {
			$this->m_jsonTaskDetails = CStrings::strToJson( $this->m_strTaskDetails );
			unset( $this->m_strTaskDetails );
		}
		return $this->m_jsonTaskDetails;
	}

	public function sqlTaskDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getTaskDetails() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getTaskDetails() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setIsPostDateIgnored( $intIsPostDateIgnored ) {
		$this->set( 'm_intIsPostDateIgnored', CStrings::strToIntDef( $intIsPostDateIgnored, NULL, false ) );
	}

	public function getIsPostDateIgnored() {
		return $this->m_intIsPostDateIgnored;
	}

	public function sqlIsPostDateIgnored() {
		return ( true == isset( $this->m_intIsPostDateIgnored ) ) ? ( string ) $this->m_intIsPostDateIgnored : '0';
	}

	public function setIsOpeningLog( $intIsOpeningLog ) {
		$this->set( 'm_intIsOpeningLog', CStrings::strToIntDef( $intIsOpeningLog, NULL, false ) );
	}

	public function getIsOpeningLog() {
		return $this->m_intIsOpeningLog;
	}

	public function sqlIsOpeningLog() {
		return ( true == isset( $this->m_intIsOpeningLog ) ) ? ( string ) $this->m_intIsOpeningLog : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, scheduled_task_id, scheduled_task_type_id, property_id, event_type_id, event_sub_type_id, prior_scheduled_task_log_id, post_date, apply_through_post_date, schedule_details, task_details, log_datetime, is_post_date_ignored, is_opening_log, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
				  $strId . ', ' .
				  $this->sqlCid() . ', ' .
				  $this->sqlScheduledTaskId() . ', ' .
				  $this->sqlScheduledTaskTypeId() . ', ' .
				  $this->sqlPropertyId() . ', ' .
				  $this->sqlEventTypeId() . ', ' .
				  $this->sqlEventSubTypeId() . ', ' .
				  $this->sqlPriorScheduledTaskLogId() . ', ' .
				  $this->sqlPostDate() . ', ' .
				  $this->sqlApplyThroughPostDate() . ', ' .
				  $this->sqlScheduleDetails() . ', ' .
				  $this->sqlTaskDetails() . ', ' .
				  $this->sqlLogDatetime() . ', ' .
				  $this->sqlIsPostDateIgnored() . ', ' .
				  $this->sqlIsOpeningLog() . ', ' .
				  $this->sqlDeletedBy() . ', ' .
				  $this->sqlDeletedOn() . ', ' .
				  ( int ) $intCurrentUserId . ', ' .
				  $this->sqlUpdatedOn() . ', ' .
				  ( int ) $intCurrentUserId . ', ' .
				  $this->sqlCreatedOn() . ', ' .
				  $this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_task_id = ' . $this->sqlScheduledTaskId(). ',' ; } elseif( true == array_key_exists( 'ScheduledTaskId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_task_id = ' . $this->sqlScheduledTaskId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_task_type_id = ' . $this->sqlScheduledTaskTypeId(). ',' ; } elseif( true == array_key_exists( 'ScheduledTaskTypeId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_task_type_id = ' . $this->sqlScheduledTaskTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_type_id = ' . $this->sqlEventTypeId(). ',' ; } elseif( true == array_key_exists( 'EventTypeId', $this->getChangedColumns() ) ) { $strSql .= ' event_type_id = ' . $this->sqlEventTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_sub_type_id = ' . $this->sqlEventSubTypeId(). ',' ; } elseif( true == array_key_exists( 'EventSubTypeId', $this->getChangedColumns() ) ) { $strSql .= ' event_sub_type_id = ' . $this->sqlEventSubTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prior_scheduled_task_log_id = ' . $this->sqlPriorScheduledTaskLogId(). ',' ; } elseif( true == array_key_exists( 'PriorScheduledTaskLogId', $this->getChangedColumns() ) ) { $strSql .= ' prior_scheduled_task_log_id = ' . $this->sqlPriorScheduledTaskLogId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate(). ',' ; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate(). ',' ; } elseif( true == array_key_exists( 'ApplyThroughPostDate', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' schedule_details = ' . $this->sqlScheduleDetails(). ',' ; } elseif( true == array_key_exists( 'ScheduleDetails', $this->getChangedColumns() ) ) { $strSql .= ' schedule_details = ' . $this->sqlScheduleDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_details = ' . $this->sqlTaskDetails(). ',' ; } elseif( true == array_key_exists( 'TaskDetails', $this->getChangedColumns() ) ) { $strSql .= ' task_details = ' . $this->sqlTaskDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime(). ',' ; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored(). ',' ; } elseif( true == array_key_exists( 'IsPostDateIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog(). ',' ; } elseif( true == array_key_exists( 'IsOpeningLog', $this->getChangedColumns() ) ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'scheduled_task_id' => $this->getScheduledTaskId(),
			'scheduled_task_type_id' => $this->getScheduledTaskTypeId(),
			'property_id' => $this->getPropertyId(),
			'event_type_id' => $this->getEventTypeId(),
			'event_sub_type_id' => $this->getEventSubTypeId(),
			'prior_scheduled_task_log_id' => $this->getPriorScheduledTaskLogId(),
			'post_date' => $this->getPostDate(),
			'apply_through_post_date' => $this->getApplyThroughPostDate(),
			'schedule_details' => $this->getScheduleDetails(),
			'task_details' => $this->getTaskDetails(),
			'log_datetime' => $this->getLogDatetime(),
			'is_post_date_ignored' => $this->getIsPostDateIgnored(),
			'is_opening_log' => $this->getIsOpeningLog(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>