<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCommercialClauseKeys
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCommercialClauseKeys extends CEosPluralBase {

	/**
	 * @return CCommercialClauseKey[]
	 */
	public static function fetchCommercialClauseKeys( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCommercialClauseKey::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCommercialClauseKey
	 */
	public static function fetchCommercialClauseKey( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCommercialClauseKey::class, $objDatabase );
	}

	public static function fetchCommercialClauseKeyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'commercial_clause_keys', $objDatabase );
	}

	public static function fetchCommercialClauseKeyByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCommercialClauseKey( sprintf( 'SELECT * FROM commercial_clause_keys WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCommercialClauseKeysByCid( $intCid, $objDatabase ) {
		return self::fetchCommercialClauseKeys( sprintf( 'SELECT * FROM commercial_clause_keys WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCommercialClauseKeysByDefaultCommercialClauseKeyIdByCid( $intDefaultCommercialClauseKeyId, $intCid, $objDatabase ) {
		return self::fetchCommercialClauseKeys( sprintf( 'SELECT * FROM commercial_clause_keys WHERE default_commercial_clause_key_id = %d AND cid = %d', $intDefaultCommercialClauseKeyId, $intCid ), $objDatabase );
	}

	public static function fetchCommercialClauseKeysByCommercialClauseKeyTypeIdByCid( $intCommercialClauseKeyTypeId, $intCid, $objDatabase ) {
		return self::fetchCommercialClauseKeys( sprintf( 'SELECT * FROM commercial_clause_keys WHERE commercial_clause_key_type_id = %d AND cid = %d', $intCommercialClauseKeyTypeId, $intCid ), $objDatabase );
	}

}
?>