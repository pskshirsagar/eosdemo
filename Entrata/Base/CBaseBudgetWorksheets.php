<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetWorksheets
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetWorksheets extends CEosPluralBase {

	/**
	 * @return CBudgetWorksheet[]
	 */
	public static function fetchBudgetWorksheets( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CBudgetWorksheet::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBudgetWorksheet
	 */
	public static function fetchBudgetWorksheet( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetWorksheet::class, $objDatabase );
	}

	public static function fetchBudgetWorksheetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_worksheets', $objDatabase );
	}

	public static function fetchBudgetWorksheetByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorksheet( sprintf( 'SELECT * FROM budget_worksheets WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorksheetsByCid( $intCid, $objDatabase ) {
		return self::fetchBudgetWorksheets( sprintf( 'SELECT * FROM budget_worksheets WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorksheetsByBudgetWorkbookIdByCid( $intBudgetWorkbookId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorksheets( sprintf( 'SELECT * FROM budget_worksheets WHERE budget_workbook_id = %d AND cid = %d', $intBudgetWorkbookId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorksheetsByBudgetWorksheetTypeIdByCid( $intBudgetWorksheetTypeId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorksheets( sprintf( 'SELECT * FROM budget_worksheets WHERE budget_worksheet_type_id = %d AND cid = %d', $intBudgetWorksheetTypeId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorksheetsByBudgetDataSourceTypeIdByCid( $intBudgetDataSourceTypeId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorksheets( sprintf( 'SELECT * FROM budget_worksheets WHERE budget_data_source_type_id = %d AND cid = %d', $intBudgetDataSourceTypeId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorksheetsByBudgetDataSourceSubTypeIdByCid( $intBudgetDataSourceSubTypeId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorksheets( sprintf( 'SELECT * FROM budget_worksheets WHERE budget_data_source_sub_type_id = %d AND cid = %d', $intBudgetDataSourceSubTypeId, $intCid ), $objDatabase );
	}

}
?>