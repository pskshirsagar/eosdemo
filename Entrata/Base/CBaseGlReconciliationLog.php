<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlReconciliationLog extends CEosSingularBase {

	const TABLE_NAME = 'public.gl_reconciliation_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intGlReconciliationId;
	protected $m_strAction;
	protected $m_strBeginningDate;
	protected $m_strStatementDate;
	protected $m_fltBeginningAmount;
	protected $m_fltEndingAmount;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['gl_reconciliation_id'] ) && $boolDirectSet ) $this->set( 'm_intGlReconciliationId', trim( $arrValues['gl_reconciliation_id'] ) ); elseif( isset( $arrValues['gl_reconciliation_id'] ) ) $this->setGlReconciliationId( $arrValues['gl_reconciliation_id'] );
		if( isset( $arrValues['action'] ) && $boolDirectSet ) $this->set( 'm_strAction', trim( stripcslashes( $arrValues['action'] ) ) ); elseif( isset( $arrValues['action'] ) ) $this->setAction( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action'] ) : $arrValues['action'] );
		if( isset( $arrValues['beginning_date'] ) && $boolDirectSet ) $this->set( 'm_strBeginningDate', trim( $arrValues['beginning_date'] ) ); elseif( isset( $arrValues['beginning_date'] ) ) $this->setBeginningDate( $arrValues['beginning_date'] );
		if( isset( $arrValues['statement_date'] ) && $boolDirectSet ) $this->set( 'm_strStatementDate', trim( $arrValues['statement_date'] ) ); elseif( isset( $arrValues['statement_date'] ) ) $this->setStatementDate( $arrValues['statement_date'] );
		if( isset( $arrValues['beginning_amount'] ) && $boolDirectSet ) $this->set( 'm_fltBeginningAmount', trim( $arrValues['beginning_amount'] ) ); elseif( isset( $arrValues['beginning_amount'] ) ) $this->setBeginningAmount( $arrValues['beginning_amount'] );
		if( isset( $arrValues['ending_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEndingAmount', trim( $arrValues['ending_amount'] ) ); elseif( isset( $arrValues['ending_amount'] ) ) $this->setEndingAmount( $arrValues['ending_amount'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setGlReconciliationId( $intGlReconciliationId ) {
		$this->set( 'm_intGlReconciliationId', CStrings::strToIntDef( $intGlReconciliationId, NULL, false ) );
	}

	public function getGlReconciliationId() {
		return $this->m_intGlReconciliationId;
	}

	public function sqlGlReconciliationId() {
		return ( true == isset( $this->m_intGlReconciliationId ) ) ? ( string ) $this->m_intGlReconciliationId : 'NULL';
	}

	public function setAction( $strAction ) {
		$this->set( 'm_strAction', CStrings::strTrimDef( $strAction, 30, NULL, true ) );
	}

	public function getAction() {
		return $this->m_strAction;
	}

	public function sqlAction() {
		return ( true == isset( $this->m_strAction ) ) ? '\'' . addslashes( $this->m_strAction ) . '\'' : 'NULL';
	}

	public function setBeginningDate( $strBeginningDate ) {
		$this->set( 'm_strBeginningDate', CStrings::strTrimDef( $strBeginningDate, -1, NULL, true ) );
	}

	public function getBeginningDate() {
		return $this->m_strBeginningDate;
	}

	public function sqlBeginningDate() {
		return ( true == isset( $this->m_strBeginningDate ) ) ? '\'' . $this->m_strBeginningDate . '\'' : 'NULL';
	}

	public function setStatementDate( $strStatementDate ) {
		$this->set( 'm_strStatementDate', CStrings::strTrimDef( $strStatementDate, -1, NULL, true ) );
	}

	public function getStatementDate() {
		return $this->m_strStatementDate;
	}

	public function sqlStatementDate() {
		return ( true == isset( $this->m_strStatementDate ) ) ? '\'' . $this->m_strStatementDate . '\'' : 'NOW()';
	}

	public function setBeginningAmount( $fltBeginningAmount ) {
		$this->set( 'm_fltBeginningAmount', CStrings::strToFloatDef( $fltBeginningAmount, NULL, false, 2 ) );
	}

	public function getBeginningAmount() {
		return $this->m_fltBeginningAmount;
	}

	public function sqlBeginningAmount() {
		return ( true == isset( $this->m_fltBeginningAmount ) ) ? ( string ) $this->m_fltBeginningAmount : 'NULL';
	}

	public function setEndingAmount( $fltEndingAmount ) {
		$this->set( 'm_fltEndingAmount', CStrings::strToFloatDef( $fltEndingAmount, NULL, false, 2 ) );
	}

	public function getEndingAmount() {
		return $this->m_fltEndingAmount;
	}

	public function sqlEndingAmount() {
		return ( true == isset( $this->m_fltEndingAmount ) ) ? ( string ) $this->m_fltEndingAmount : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, gl_reconciliation_id, action, beginning_date, statement_date, beginning_amount, ending_amount, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlGlReconciliationId() . ', ' .
 						$this->sqlAction() . ', ' .
 						$this->sqlBeginningDate() . ', ' .
 						$this->sqlStatementDate() . ', ' .
 						$this->sqlBeginningAmount() . ', ' .
 						$this->sqlEndingAmount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_reconciliation_id = ' . $this->sqlGlReconciliationId() . ','; } elseif( true == array_key_exists( 'GlReconciliationId', $this->getChangedColumns() ) ) { $strSql .= ' gl_reconciliation_id = ' . $this->sqlGlReconciliationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action = ' . $this->sqlAction() . ','; } elseif( true == array_key_exists( 'Action', $this->getChangedColumns() ) ) { $strSql .= ' action = ' . $this->sqlAction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' beginning_date = ' . $this->sqlBeginningDate() . ','; } elseif( true == array_key_exists( 'BeginningDate', $this->getChangedColumns() ) ) { $strSql .= ' beginning_date = ' . $this->sqlBeginningDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' statement_date = ' . $this->sqlStatementDate() . ','; } elseif( true == array_key_exists( 'StatementDate', $this->getChangedColumns() ) ) { $strSql .= ' statement_date = ' . $this->sqlStatementDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' beginning_amount = ' . $this->sqlBeginningAmount() . ','; } elseif( true == array_key_exists( 'BeginningAmount', $this->getChangedColumns() ) ) { $strSql .= ' beginning_amount = ' . $this->sqlBeginningAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ending_amount = ' . $this->sqlEndingAmount() . ','; } elseif( true == array_key_exists( 'EndingAmount', $this->getChangedColumns() ) ) { $strSql .= ' ending_amount = ' . $this->sqlEndingAmount() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'gl_reconciliation_id' => $this->getGlReconciliationId(),
			'action' => $this->getAction(),
			'beginning_date' => $this->getBeginningDate(),
			'statement_date' => $this->getStatementDate(),
			'beginning_amount' => $this->getBeginningAmount(),
			'ending_amount' => $this->getEndingAmount(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>