<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSiteTabletStat extends CEosSingularBase {

	const TABLE_NAME = 'public.site_tablet_stats';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_strPlatform;
	protected $m_strVersion;
	protected $m_strBuild;
	protected $m_strScreen;
	protected $m_strEvent;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['platform'] ) && $boolDirectSet ) $this->set( 'm_strPlatform', trim( stripcslashes( $arrValues['platform'] ) ) ); elseif( isset( $arrValues['platform'] ) ) $this->setPlatform( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['platform'] ) : $arrValues['platform'] );
		if( isset( $arrValues['version'] ) && $boolDirectSet ) $this->set( 'm_strVersion', trim( stripcslashes( $arrValues['version'] ) ) ); elseif( isset( $arrValues['version'] ) ) $this->setVersion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['version'] ) : $arrValues['version'] );
		if( isset( $arrValues['build'] ) && $boolDirectSet ) $this->set( 'm_strBuild', trim( stripcslashes( $arrValues['build'] ) ) ); elseif( isset( $arrValues['build'] ) ) $this->setBuild( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['build'] ) : $arrValues['build'] );
		if( isset( $arrValues['screen'] ) && $boolDirectSet ) $this->set( 'm_strScreen', trim( stripcslashes( $arrValues['screen'] ) ) ); elseif( isset( $arrValues['screen'] ) ) $this->setScreen( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['screen'] ) : $arrValues['screen'] );
		if( isset( $arrValues['event'] ) && $boolDirectSet ) $this->set( 'm_strEvent', trim( stripcslashes( $arrValues['event'] ) ) ); elseif( isset( $arrValues['event'] ) ) $this->setEvent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['event'] ) : $arrValues['event'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPlatform( $strPlatform ) {
		$this->set( 'm_strPlatform', CStrings::strTrimDef( $strPlatform, 30, NULL, true ) );
	}

	public function getPlatform() {
		return $this->m_strPlatform;
	}

	public function sqlPlatform() {
		return ( true == isset( $this->m_strPlatform ) ) ? '\'' . addslashes( $this->m_strPlatform ) . '\'' : 'NULL';
	}

	public function setVersion( $strVersion ) {
		$this->set( 'm_strVersion', CStrings::strTrimDef( $strVersion, 10, NULL, true ) );
	}

	public function getVersion() {
		return $this->m_strVersion;
	}

	public function sqlVersion() {
		return ( true == isset( $this->m_strVersion ) ) ? '\'' . addslashes( $this->m_strVersion ) . '\'' : 'NULL';
	}

	public function setBuild( $strBuild ) {
		$this->set( 'm_strBuild', CStrings::strTrimDef( $strBuild, 10, NULL, true ) );
	}

	public function getBuild() {
		return $this->m_strBuild;
	}

	public function sqlBuild() {
		return ( true == isset( $this->m_strBuild ) ) ? '\'' . addslashes( $this->m_strBuild ) . '\'' : 'NULL';
	}

	public function setScreen( $strScreen ) {
		$this->set( 'm_strScreen', CStrings::strTrimDef( $strScreen, 40, NULL, true ) );
	}

	public function getScreen() {
		return $this->m_strScreen;
	}

	public function sqlScreen() {
		return ( true == isset( $this->m_strScreen ) ) ? '\'' . addslashes( $this->m_strScreen ) . '\'' : 'NULL';
	}

	public function setEvent( $strEvent ) {
		$this->set( 'm_strEvent', CStrings::strTrimDef( $strEvent, 40, NULL, true ) );
	}

	public function getEvent() {
		return $this->m_strEvent;
	}

	public function sqlEvent() {
		return ( true == isset( $this->m_strEvent ) ) ? '\'' . addslashes( $this->m_strEvent ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, platform, version, build, screen, event, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPlatform() . ', ' .
 						$this->sqlVersion() . ', ' .
 						$this->sqlBuild() . ', ' .
 						$this->sqlScreen() . ', ' .
 						$this->sqlEvent() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' platform = ' . $this->sqlPlatform() . ','; } elseif( true == array_key_exists( 'Platform', $this->getChangedColumns() ) ) { $strSql .= ' platform = ' . $this->sqlPlatform() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' version = ' . $this->sqlVersion() . ','; } elseif( true == array_key_exists( 'Version', $this->getChangedColumns() ) ) { $strSql .= ' version = ' . $this->sqlVersion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' build = ' . $this->sqlBuild() . ','; } elseif( true == array_key_exists( 'Build', $this->getChangedColumns() ) ) { $strSql .= ' build = ' . $this->sqlBuild() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screen = ' . $this->sqlScreen() . ','; } elseif( true == array_key_exists( 'Screen', $this->getChangedColumns() ) ) { $strSql .= ' screen = ' . $this->sqlScreen() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event = ' . $this->sqlEvent() . ','; } elseif( true == array_key_exists( 'Event', $this->getChangedColumns() ) ) { $strSql .= ' event = ' . $this->sqlEvent() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'platform' => $this->getPlatform(),
			'version' => $this->getVersion(),
			'build' => $this->getBuild(),
			'screen' => $this->getScreen(),
			'event' => $this->getEvent(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>