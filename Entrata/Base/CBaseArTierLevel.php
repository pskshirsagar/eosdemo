<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseArTierLevel extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intArTierId;
    protected $m_intMinItemCount;
    protected $m_intMaxItemCount;
    protected $m_fltRateAmount;
    protected $m_intIsDefault;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        $this->m_intMinItemCount = '0';
        $this->m_intMaxItemCount = '0';
        $this->m_fltRateAmount = '0';
        $this->m_intIsDefault = '0';

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['ar_tier_id'] ) && $boolDirectSet ) $this->m_intArTierId = trim( $arrValues['ar_tier_id'] ); else if( isset( $arrValues['ar_tier_id'] ) ) $this->setArTierId( $arrValues['ar_tier_id'] );
        if( isset( $arrValues['min_item_count'] ) && $boolDirectSet ) $this->m_intMinItemCount = trim( $arrValues['min_item_count'] ); else if( isset( $arrValues['min_item_count'] ) ) $this->setMinItemCount( $arrValues['min_item_count'] );
        if( isset( $arrValues['max_item_count'] ) && $boolDirectSet ) $this->m_intMaxItemCount = trim( $arrValues['max_item_count'] ); else if( isset( $arrValues['max_item_count'] ) ) $this->setMaxItemCount( $arrValues['max_item_count'] );
        if( isset( $arrValues['rate_amount'] ) && $boolDirectSet ) $this->m_fltRateAmount = trim( $arrValues['rate_amount'] ); else if( isset( $arrValues['rate_amount'] ) ) $this->setRateAmount( $arrValues['rate_amount'] );
        if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->m_intIsDefault = trim( $arrValues['is_default'] ); else if( isset( $arrValues['is_default'] ) ) $this->setIsDefault( $arrValues['is_default'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
    }

    public function setArTierId( $intArTierId ) {
        $this->m_intArTierId = CStrings::strToIntDef( $intArTierId, NULL, false );
    }

    public function getArTierId() {
        return $this->m_intArTierId;
    }

    public function sqlArTierId() {
        return ( true == isset( $this->m_intArTierId ) ) ? (string) $this->m_intArTierId : 'NULL';
    }

    public function setMinItemCount( $intMinItemCount ) {
        $this->m_intMinItemCount = CStrings::strToIntDef( $intMinItemCount, NULL, false );
    }

    public function getMinItemCount() {
        return $this->m_intMinItemCount;
    }

    public function sqlMinItemCount() {
        return ( true == isset( $this->m_intMinItemCount ) ) ? (string) $this->m_intMinItemCount : '0';
    }

    public function setMaxItemCount( $intMaxItemCount ) {
        $this->m_intMaxItemCount = CStrings::strToIntDef( $intMaxItemCount, NULL, false );
    }

    public function getMaxItemCount() {
        return $this->m_intMaxItemCount;
    }

    public function sqlMaxItemCount() {
        return ( true == isset( $this->m_intMaxItemCount ) ) ? (string) $this->m_intMaxItemCount : '0';
    }

    public function setRateAmount( $fltRateAmount ) {
        $this->m_fltRateAmount = CStrings::strToFloatDef( $fltRateAmount, NULL, false, 2 );
    }

    public function getRateAmount() {
        return $this->m_fltRateAmount;
    }

    public function sqlRateAmount() {
        return ( true == isset( $this->m_fltRateAmount ) ) ? (string) $this->m_fltRateAmount : '0';
    }

    public function setIsDefault( $intIsDefault ) {
        $this->m_intIsDefault = CStrings::strToIntDef( $intIsDefault, NULL, false );
    }

    public function getIsDefault() {
        return $this->m_intIsDefault;
    }

    public function sqlIsDefault() {
        return ( true == isset( $this->m_intIsDefault ) ) ? (string) $this->m_intIsDefault : '0';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.ar_tier_levels_id_seq\' )' : (int) $this->m_intId;

        $strSql = 'INSERT INTO					  public.ar_tier_levels( id, cid, ar_tier_id, min_item_count, max_item_count, rate_amount, is_default, updated_by, updated_on, created_by, created_on )					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlCid() . ', ' . 		                $this->sqlArTierId() . ', ' . 		                $this->sqlMinItemCount() . ', ' . 		                $this->sqlMaxItemCount() . ', ' . 		                $this->sqlRateAmount() . ', ' . 		                $this->sqlIsDefault() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlUpdatedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }
        $strSql = 'UPDATE
                      public.ar_tier_levels
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_tier_id = ' . $this->sqlArTierId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlArTierId() ) != $this->getOriginalValueByFieldName ( 'ar_tier_id' ) ) { $arrstrOriginalValueChanges['ar_tier_id'] = $this->sqlArTierId(); $strSql .= ' ar_tier_id = ' . $this->sqlArTierId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_item_count = ' . $this->sqlMinItemCount() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlMinItemCount() ) != $this->getOriginalValueByFieldName ( 'min_item_count' ) ) { $arrstrOriginalValueChanges['min_item_count'] = $this->sqlMinItemCount(); $strSql .= ' min_item_count = ' . $this->sqlMinItemCount() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_item_count = ' . $this->sqlMaxItemCount() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlMaxItemCount() ) != $this->getOriginalValueByFieldName ( 'max_item_count' ) ) { $arrstrOriginalValueChanges['max_item_count'] = $this->sqlMaxItemCount(); $strSql .= ' max_item_count = ' . $this->sqlMaxItemCount() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate_amount = ' . $this->sqlRateAmount() . ','; } elseif( ( 0 != bccomp ( CStrings::reverseSqlFormat( $this->sqlRateAmount() ), $this->getOriginalValueByFieldName ( 'rate_amount' ), 2 ) ) ) { $arrstrOriginalValueChanges['rate_amount'] = $this->sqlRateAmount(); $strSql .= ' rate_amount = ' . $this->sqlRateAmount() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsDefault() ) != $this->getOriginalValueByFieldName ( 'is_default' ) ) { $arrstrOriginalValueChanges['is_default'] = $this->sqlIsDefault(); $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
                      $strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
                      $strSql .= ' updated_on = \'NOW()\'					WHERE					    id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.ar_tier_levels WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.ar_tier_levels_id_seq', $objDatabase );
    }

}
?>