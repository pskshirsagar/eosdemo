<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base class again, please do so by checking the composite key checkbox. ***
 */

class CBaseCompanyServices extends CEosPluralBase {

    const TABLE_COMPANY_SERVICES = 'public.company_services';

    public static function fetchCompanyServices( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CCompanyService', $objDatabase );
    }

    public static function fetchCompanyService( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CCompanyService', $objDatabase );
    }

    public static function fetchCompanyServiceCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'company_services', $objDatabase );
    }

    public static function fetchCompanyServiceByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchCompanyService( sprintf( 'SELECT * FROM company_services WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchCompanyServicesByCid( $intCid, $objDatabase ) {
        return self::fetchCompanyServices( sprintf( 'SELECT * FROM company_services WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchCompanyServicesByServiceIdByCid( $intServiceId, $intCid, $objDatabase ) {
        return self::fetchCompanyServices( sprintf( 'SELECT * FROM company_services WHERE service_id = %d AND cid = %d', (int) $intServiceId, (int) $intCid ), $objDatabase );
    }
}
?>