<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceTemplates
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceTemplates extends CEosPluralBase {

	/**
	 * @return CMaintenanceTemplate[]
	 */
	public static function fetchMaintenanceTemplates( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMaintenanceTemplate::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenanceTemplate
	 */
	public static function fetchMaintenanceTemplate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceTemplate::class, $objDatabase );
	}

	public static function fetchMaintenanceTemplateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_templates', $objDatabase );
	}

	public static function fetchMaintenanceTemplateByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceTemplate( sprintf( 'SELECT * FROM maintenance_templates WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceTemplatesByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenanceTemplates( sprintf( 'SELECT * FROM maintenance_templates WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceTemplatesByParentMaintenanceTemplateIdByCid( $intParentMaintenanceTemplateId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceTemplates( sprintf( 'SELECT * FROM maintenance_templates WHERE parent_maintenance_template_id = %d AND cid = %d', $intParentMaintenanceTemplateId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceTemplatesByMaintenancePriorityIdByCid( $intMaintenancePriorityId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceTemplates( sprintf( 'SELECT * FROM maintenance_templates WHERE maintenance_priority_id = %d AND cid = %d', $intMaintenancePriorityId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceTemplatesByMaintenanceLocationIdByCid( $intMaintenanceLocationId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceTemplates( sprintf( 'SELECT * FROM maintenance_templates WHERE maintenance_location_id = %d AND cid = %d', $intMaintenanceLocationId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceTemplatesByMaintenanceProblemIdByCid( $intMaintenanceProblemId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceTemplates( sprintf( 'SELECT * FROM maintenance_templates WHERE maintenance_problem_id = %d AND cid = %d', $intMaintenanceProblemId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceTemplatesByMaintenanceRequestTypeIdByCid( $intMaintenanceRequestTypeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceTemplates( sprintf( 'SELECT * FROM maintenance_templates WHERE maintenance_request_type_id = %d AND cid = %d', $intMaintenanceRequestTypeId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceTemplatesByMaintenanceStatusIdByCid( $intMaintenanceStatusId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceTemplates( sprintf( 'SELECT * FROM maintenance_templates WHERE maintenance_status_id = %d AND cid = %d', $intMaintenanceStatusId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceTemplatesByDueIntervalTypeIdByCid( $intDueIntervalTypeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceTemplates( sprintf( 'SELECT * FROM maintenance_templates WHERE due_interval_type_id = %d AND cid = %d', $intDueIntervalTypeId, $intCid ), $objDatabase );
	}

}
?>