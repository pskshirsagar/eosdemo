<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobFundingSources
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseJobFundingSources extends CEosPluralBase {

	/**
	 * @return CJobFundingSource[]
	 */
	public static function fetchJobFundingSources( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CJobFundingSource::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CJobFundingSource
	 */
	public static function fetchJobFundingSource( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CJobFundingSource::class, $objDatabase );
	}

	public static function fetchJobFundingSourceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_funding_sources', $objDatabase );
	}

	public static function fetchJobFundingSourceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchJobFundingSource( sprintf( 'SELECT * FROM job_funding_sources WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobFundingSourcesByCid( $intCid, $objDatabase ) {
		return self::fetchJobFundingSources( sprintf( 'SELECT * FROM job_funding_sources WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobFundingSourcesByJobIdByCid( $intJobId, $intCid, $objDatabase ) {
		return self::fetchJobFundingSources( sprintf( 'SELECT * FROM job_funding_sources WHERE job_id = %d AND cid = %d', ( int ) $intJobId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobFundingSourcesByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchJobFundingSources( sprintf( 'SELECT * FROM job_funding_sources WHERE ap_payee_id = %d AND cid = %d', ( int ) $intApPayeeId, ( int ) $intCid ), $objDatabase );
	}

}
?>