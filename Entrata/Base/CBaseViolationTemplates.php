<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CViolationTemplates
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseViolationTemplates extends CEosPluralBase {

	/**
	 * @return CViolationTemplate[]
	 */
	public static function fetchViolationTemplates( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CViolationTemplate::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CViolationTemplate
	 */
	public static function fetchViolationTemplate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CViolationTemplate::class, $objDatabase );
	}

	public static function fetchViolationTemplateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'violation_templates', $objDatabase );
	}

	public static function fetchViolationTemplateByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchViolationTemplate( sprintf( 'SELECT * FROM violation_templates WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchViolationTemplatesByCid( $intCid, $objDatabase ) {
		return self::fetchViolationTemplates( sprintf( 'SELECT * FROM violation_templates WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchViolationTemplatesByViolationTypeIdByCid( $intViolationTypeId, $intCid, $objDatabase ) {
		return self::fetchViolationTemplates( sprintf( 'SELECT * FROM violation_templates WHERE violation_type_id = %d AND cid = %d', $intViolationTypeId, $intCid ), $objDatabase );
	}

}
?>