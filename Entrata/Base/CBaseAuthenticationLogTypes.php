<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAuthenticationLogTypes
 * Do not add any new functions to this class.
 */

class CBaseAuthenticationLogTypes extends CEosPluralBase {

	/**
	 * @return CAuthenticationLogType[]
	 */
	public static function fetchAuthenticationLogTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CAuthenticationLogType', $objDatabase );
	}

	/**
	 * @return CAuthenticationLogType
	 */
	public static function fetchAuthenticationLogType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAuthenticationLogType', $objDatabase );
	}

	public static function fetchAuthenticationLogTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'authentication_log_types', $objDatabase );
	}

	public static function fetchAuthenticationLogTypeById( $intId, $objDatabase ) {
		return self::fetchAuthenticationLogType( sprintf( 'SELECT * FROM authentication_log_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>