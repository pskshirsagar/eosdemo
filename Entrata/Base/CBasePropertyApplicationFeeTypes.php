<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyApplicationFeeTypes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyApplicationFeeTypes extends CEosPluralBase {

	/**
	 * @return CPropertyApplicationFeeType[]
	 */
	public static function fetchPropertyApplicationFeeTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyApplicationFeeType', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyApplicationFeeType
	 */
	public static function fetchPropertyApplicationFeeType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyApplicationFeeType', $objDatabase );
	}

	public static function fetchPropertyApplicationFeeTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_application_fee_types', $objDatabase );
	}

	public static function fetchPropertyApplicationFeeTypeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyApplicationFeeType( sprintf( 'SELECT * FROM property_application_fee_types WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyApplicationFeeTypesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyApplicationFeeTypes( sprintf( 'SELECT * FROM property_application_fee_types WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyApplicationFeeTypesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyApplicationFeeTypes( sprintf( 'SELECT * FROM property_application_fee_types WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyApplicationFeeTypesByApplicationFeeTypeIdByCid( $intApplicationFeeTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyApplicationFeeTypes( sprintf( 'SELECT * FROM property_application_fee_types WHERE application_fee_type_id = %d AND cid = %d', ( int ) $intApplicationFeeTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>