<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicantIncome extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intApplicantId;
    protected $m_intIncomeTypeId;
    protected $m_intFrequencyId;
    protected $m_strPayerName;
    protected $m_strPayerEmail;
    protected $m_strPayerPhoneNumber;
    protected $m_strStreetLine1;
    protected $m_strStreetLine2;
    protected $m_strStreetLine3;
    protected $m_strCity;
    protected $m_strStateCode;
    protected $m_strProvince;
    protected $m_strPostalCode;
    protected $m_strCountryCode;
    protected $m_boolIsVerified;
    protected $m_strContactName;
    protected $m_strContactPhoneNumber;
    protected $m_strContactFaxNumber;
    protected $m_strContactEmail;
    protected $m_strPayeeName;
    protected $m_strPosition;
    protected $m_fltAmount;
    protected $m_strDescription;
    protected $m_strDateStarted;
    protected $m_strDateStopped;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        $this->m_boolIsVerified = false;

        return;
    }

    public function setDefaults() {
        return;
    }

    /**
     * @SuppressWarnings( BooleanArgumentFlag )
     */
    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->m_intApplicantId = trim( $arrValues['applicant_id'] ); else if( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
        if( isset( $arrValues['income_type_id'] ) && $boolDirectSet ) $this->m_intIncomeTypeId = trim( $arrValues['income_type_id'] ); else if( isset( $arrValues['income_type_id'] ) ) $this->setIncomeTypeId( $arrValues['income_type_id'] );
        if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->m_intFrequencyId = trim( $arrValues['frequency_id'] ); else if( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
        if( isset( $arrValues['payer_name'] ) && $boolDirectSet ) $this->m_strPayerName = trim( stripcslashes( $arrValues['payer_name'] ) ); else if( isset( $arrValues['payer_name'] ) ) $this->setPayerName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payer_name'] ) : $arrValues['payer_name'] );
        if( isset( $arrValues['payer_email'] ) && $boolDirectSet ) $this->m_strPayerEmail = trim( stripcslashes( $arrValues['payer_email'] ) ); else if( isset( $arrValues['payer_email'] ) ) $this->setPayerEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payer_email'] ) : $arrValues['payer_email'] );
        if( isset( $arrValues['payer_phone_number'] ) && $boolDirectSet ) $this->m_strPayerPhoneNumber = trim( stripcslashes( $arrValues['payer_phone_number'] ) ); else if( isset( $arrValues['payer_phone_number'] ) ) $this->setPayerPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payer_phone_number'] ) : $arrValues['payer_phone_number'] );
        if( isset( $arrValues['street_line1'] ) && $boolDirectSet ) $this->m_strStreetLine1 = trim( stripcslashes( $arrValues['street_line1'] ) ); else if( isset( $arrValues['street_line1'] ) ) $this->setStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line1'] ) : $arrValues['street_line1'] );
        if( isset( $arrValues['street_line2'] ) && $boolDirectSet ) $this->m_strStreetLine2 = trim( stripcslashes( $arrValues['street_line2'] ) ); else if( isset( $arrValues['street_line2'] ) ) $this->setStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line2'] ) : $arrValues['street_line2'] );
        if( isset( $arrValues['street_line3'] ) && $boolDirectSet ) $this->m_strStreetLine3 = trim( stripcslashes( $arrValues['street_line3'] ) ); else if( isset( $arrValues['street_line3'] ) ) $this->setStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line3'] ) : $arrValues['street_line3'] );
        if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->m_strCity = trim( stripcslashes( $arrValues['city'] ) ); else if( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
        if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->m_strStateCode = trim( stripcslashes( $arrValues['state_code'] ) ); else if( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
        if( isset( $arrValues['province'] ) && $boolDirectSet ) $this->m_strProvince = trim( stripcslashes( $arrValues['province'] ) ); else if( isset( $arrValues['province'] ) ) $this->setProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['province'] ) : $arrValues['province'] );
        if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->m_strPostalCode = trim( stripcslashes( $arrValues['postal_code'] ) ); else if( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
        if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->m_strCountryCode = trim( stripcslashes( $arrValues['country_code'] ) ); else if( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
        if( isset( $arrValues['is_verified'] ) && $boolDirectSet ) $this->m_boolIsVerified = trim( stripcslashes( $arrValues['is_verified'] ) ); else if( isset( $arrValues['is_verified'] ) ) $this->setIsVerified( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_verified'] ) : $arrValues['is_verified'] );
        if( isset( $arrValues['contact_name'] ) && $boolDirectSet ) $this->m_strContactName = trim( stripcslashes( $arrValues['contact_name'] ) ); else if( isset( $arrValues['contact_name'] ) ) $this->setContactName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_name'] ) : $arrValues['contact_name'] );
        if( isset( $arrValues['contact_phone_number'] ) && $boolDirectSet ) $this->m_strContactPhoneNumber = trim( stripcslashes( $arrValues['contact_phone_number'] ) ); else if( isset( $arrValues['contact_phone_number'] ) ) $this->setContactPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_phone_number'] ) : $arrValues['contact_phone_number'] );
        if( isset( $arrValues['contact_fax_number'] ) && $boolDirectSet ) $this->m_strContactFaxNumber = trim( stripcslashes( $arrValues['contact_fax_number'] ) ); else if( isset( $arrValues['contact_fax_number'] ) ) $this->setContactFaxNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_fax_number'] ) : $arrValues['contact_fax_number'] );
        if( isset( $arrValues['contact_email'] ) && $boolDirectSet ) $this->m_strContactEmail = trim( stripcslashes( $arrValues['contact_email'] ) ); else if( isset( $arrValues['contact_email'] ) ) $this->setContactEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contact_email'] ) : $arrValues['contact_email'] );
        if( isset( $arrValues['payee_name'] ) && $boolDirectSet ) $this->m_strPayeeName = trim( stripcslashes( $arrValues['payee_name'] ) ); else if( isset( $arrValues['payee_name'] ) ) $this->setPayeeName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payee_name'] ) : $arrValues['payee_name'] );
        if( isset( $arrValues['position'] ) && $boolDirectSet ) $this->m_strPosition = trim( stripcslashes( $arrValues['position'] ) ); else if( isset( $arrValues['position'] ) ) $this->setPosition( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['position'] ) : $arrValues['position'] );
        if( isset( $arrValues['amount'] ) && $boolDirectSet ) $this->m_fltAmount = trim( $arrValues['amount'] ); else if( isset( $arrValues['amount'] ) ) $this->setAmount( $arrValues['amount'] );
        if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->m_strDescription = trim( stripcslashes( $arrValues['description'] ) ); else if( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
        if( isset( $arrValues['date_started'] ) && $boolDirectSet ) $this->m_strDateStarted = trim( $arrValues['date_started'] ); else if( isset( $arrValues['date_started'] ) ) $this->setDateStarted( $arrValues['date_started'] );
        if( isset( $arrValues['date_stopped'] ) && $boolDirectSet ) $this->m_strDateStopped = trim( $arrValues['date_stopped'] ); else if( isset( $arrValues['date_stopped'] ) ) $this->setDateStopped( $arrValues['date_stopped'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
    }

    public function setApplicantId( $intApplicantId ) {
        $this->m_intApplicantId = CStrings::strToIntDef( $intApplicantId, NULL, false );
    }

    public function getApplicantId() {
        return $this->m_intApplicantId;
    }

    public function sqlApplicantId() {
        return ( true == isset( $this->m_intApplicantId ) ) ? (string) $this->m_intApplicantId : 'NULL';
    }

    public function setIncomeTypeId( $intIncomeTypeId ) {
        $this->m_intIncomeTypeId = CStrings::strToIntDef( $intIncomeTypeId, NULL, false );
    }

    public function getIncomeTypeId() {
        return $this->m_intIncomeTypeId;
    }

    public function sqlIncomeTypeId() {
        return ( true == isset( $this->m_intIncomeTypeId ) ) ? (string) $this->m_intIncomeTypeId : 'NULL';
    }

    public function setFrequencyId( $intFrequencyId ) {
        $this->m_intFrequencyId = CStrings::strToIntDef( $intFrequencyId, NULL, false );
    }

    public function getFrequencyId() {
        return $this->m_intFrequencyId;
    }

    public function sqlFrequencyId() {
        return ( true == isset( $this->m_intFrequencyId ) ) ? (string) $this->m_intFrequencyId : 'NULL';
    }

    public function setPayerName( $strPayerName ) {
        $this->m_strPayerName = CStrings::strTrimDef( $strPayerName, 50, NULL, true );
    }

    public function getPayerName() {
        return $this->m_strPayerName;
    }

    public function sqlPayerName() {
        return ( true == isset( $this->m_strPayerName ) ) ? '\'' . addslashes( $this->m_strPayerName ) . '\'' : 'NULL';
    }

    public function setPayerEmail( $strPayerEmail ) {
        $this->m_strPayerEmail = CStrings::strTrimDef( $strPayerEmail, 240, NULL, true );
    }

    public function getPayerEmail() {
        return $this->m_strPayerEmail;
    }

    public function sqlPayerEmail() {
        return ( true == isset( $this->m_strPayerEmail ) ) ? '\'' . addslashes( $this->m_strPayerEmail ) . '\'' : 'NULL';
    }

    public function setPayerPhoneNumber( $strPayerPhoneNumber ) {
        $this->m_strPayerPhoneNumber = CStrings::strTrimDef( $strPayerPhoneNumber, 30, NULL, true );
    }

    public function getPayerPhoneNumber() {
        return $this->m_strPayerPhoneNumber;
    }

    public function sqlPayerPhoneNumber() {
        return ( true == isset( $this->m_strPayerPhoneNumber ) ) ? '\'' . addslashes( $this->m_strPayerPhoneNumber ) . '\'' : 'NULL';
    }

    public function setStreetLine1( $strStreetLine1 ) {
        $this->m_strStreetLine1 = CStrings::strTrimDef( $strStreetLine1, 100, NULL, true );
    }

    public function getStreetLine1() {
        return $this->m_strStreetLine1;
    }

    public function sqlStreetLine1() {
        return ( true == isset( $this->m_strStreetLine1 ) ) ? '\'' . addslashes( $this->m_strStreetLine1 ) . '\'' : 'NULL';
    }

    public function setStreetLine2( $strStreetLine2 ) {
        $this->m_strStreetLine2 = CStrings::strTrimDef( $strStreetLine2, 100, NULL, true );
    }

    public function getStreetLine2() {
        return $this->m_strStreetLine2;
    }

    public function sqlStreetLine2() {
        return ( true == isset( $this->m_strStreetLine2 ) ) ? '\'' . addslashes( $this->m_strStreetLine2 ) . '\'' : 'NULL';
    }

    public function setStreetLine3( $strStreetLine3 ) {
        $this->m_strStreetLine3 = CStrings::strTrimDef( $strStreetLine3, 100, NULL, true );
    }

    public function getStreetLine3() {
        return $this->m_strStreetLine3;
    }

    public function sqlStreetLine3() {
        return ( true == isset( $this->m_strStreetLine3 ) ) ? '\'' . addslashes( $this->m_strStreetLine3 ) . '\'' : 'NULL';
    }

    public function setCity( $strCity ) {
        $this->m_strCity = CStrings::strTrimDef( $strCity, 50, NULL, true );
    }

    public function getCity() {
        return $this->m_strCity;
    }

    public function sqlCity() {
        return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
    }

    public function setStateCode( $strStateCode ) {
        $this->m_strStateCode = CStrings::strTrimDef( $strStateCode, 2, NULL, true );
    }

    public function getStateCode() {
        return $this->m_strStateCode;
    }

    public function sqlStateCode() {
        return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
    }

    public function setProvince( $strProvince ) {
        $this->m_strProvince = CStrings::strTrimDef( $strProvince, 50, NULL, true );
    }

    public function getProvince() {
        return $this->m_strProvince;
    }

    public function sqlProvince() {
        return ( true == isset( $this->m_strProvince ) ) ? '\'' . addslashes( $this->m_strProvince ) . '\'' : 'NULL';
    }

    public function setPostalCode( $strPostalCode ) {
        $this->m_strPostalCode = CStrings::strTrimDef( $strPostalCode, 20, NULL, true );
    }

    public function getPostalCode() {
        return $this->m_strPostalCode;
    }

    public function sqlPostalCode() {
        return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
    }

    public function setCountryCode( $strCountryCode ) {
        $this->m_strCountryCode = CStrings::strTrimDef( $strCountryCode, 2, NULL, true );
    }

    public function getCountryCode() {
        return $this->m_strCountryCode;
    }

    public function sqlCountryCode() {
        return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : 'NULL';
    }

    public function setIsVerified( $boolIsVerified ) {
        $this->m_boolIsVerified = CStrings::strToBool( $boolIsVerified );
    }

    public function getIsVerified() {
        return $this->m_boolIsVerified;
    }

    public function sqlIsVerified() {
        return ( true == isset( $this->m_boolIsVerified ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsVerified ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setContactName( $strContactName ) {
        $this->m_strContactName = CStrings::strTrimDef( $strContactName, 100, NULL, true );
    }

    public function getContactName() {
        return $this->m_strContactName;
    }

    public function sqlContactName() {
        return ( true == isset( $this->m_strContactName ) ) ? '\'' . addslashes( $this->m_strContactName ) . '\'' : 'NULL';
    }

    public function setContactPhoneNumber( $strContactPhoneNumber ) {
        $this->m_strContactPhoneNumber = CStrings::strTrimDef( $strContactPhoneNumber, 30, NULL, true );
    }

    public function getContactPhoneNumber() {
        return $this->m_strContactPhoneNumber;
    }

    public function sqlContactPhoneNumber() {
        return ( true == isset( $this->m_strContactPhoneNumber ) ) ? '\'' . addslashes( $this->m_strContactPhoneNumber ) . '\'' : 'NULL';
    }

    public function setContactFaxNumber( $strContactFaxNumber ) {
        $this->m_strContactFaxNumber = CStrings::strTrimDef( $strContactFaxNumber, 30, NULL, true );
    }

    public function getContactFaxNumber() {
        return $this->m_strContactFaxNumber;
    }

    public function sqlContactFaxNumber() {
        return ( true == isset( $this->m_strContactFaxNumber ) ) ? '\'' . addslashes( $this->m_strContactFaxNumber ) . '\'' : 'NULL';
    }

    public function setContactEmail( $strContactEmail ) {
        $this->m_strContactEmail = CStrings::strTrimDef( $strContactEmail, 240, NULL, true );
    }

    public function getContactEmail() {
        return $this->m_strContactEmail;
    }

    public function sqlContactEmail() {
        return ( true == isset( $this->m_strContactEmail ) ) ? '\'' . addslashes( $this->m_strContactEmail ) . '\'' : 'NULL';
    }

    public function setPayeeName( $strPayeeName ) {
        $this->m_strPayeeName = CStrings::strTrimDef( $strPayeeName, 50, NULL, true );
    }

    public function getPayeeName() {
        return $this->m_strPayeeName;
    }

    public function sqlPayeeName() {
        return ( true == isset( $this->m_strPayeeName ) ) ? '\'' . addslashes( $this->m_strPayeeName ) . '\'' : 'NULL';
    }

    public function setPosition( $strPosition ) {
        $this->m_strPosition = CStrings::strTrimDef( $strPosition, 50, NULL, true );
    }

    public function getPosition() {
        return $this->m_strPosition;
    }

    public function sqlPosition() {
        return ( true == isset( $this->m_strPosition ) ) ? '\'' . addslashes( $this->m_strPosition ) . '\'' : 'NULL';
    }

    public function setAmount( $fltAmount ) {
        $this->m_fltAmount = CStrings::strToFloatDef( $fltAmount, NULL, false, 4 );
    }

    public function getAmount() {
        return $this->m_fltAmount;
    }

    public function sqlAmount() {
        return ( true == isset( $this->m_fltAmount ) ) ? (string) $this->m_fltAmount : 'NULL';
    }

    public function setDescription( $strDescription ) {
        $this->m_strDescription = CStrings::strTrimDef( $strDescription, 240, NULL, true );
    }

    public function getDescription() {
        return $this->m_strDescription;
    }

    public function sqlDescription() {
        return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
    }

    public function setDateStarted( $strDateStarted ) {
        $this->m_strDateStarted = CStrings::strTrimDef( $strDateStarted, -1, NULL, true );
    }

    public function getDateStarted() {
        return $this->m_strDateStarted;
    }

    public function sqlDateStarted() {
        return ( true == isset( $this->m_strDateStarted ) ) ? '\'' . $this->m_strDateStarted . '\'' : 'NULL';
    }

    public function setDateStopped( $strDateStopped ) {
        $this->m_strDateStopped = CStrings::strTrimDef( $strDateStopped, -1, NULL, true );
    }

    public function getDateStopped() {
        return $this->m_strDateStopped;
    }

    public function sqlDateStopped() {
        return ( true == isset( $this->m_strDateStopped ) ) ? '\'' . $this->m_strDateStopped . '\'' : 'NULL';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.applicant_incomes_id_seq\' )' : $this->sqlId();

        $strSql = 'INSERT INTO					  public.applicant_incomes					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlCid() . ', ' . 		                $this->sqlApplicantId() . ', ' . 		                $this->sqlIncomeTypeId() . ', ' . 		                $this->sqlFrequencyId() . ', ' . 		                $this->sqlPayerName() . ', ' . 		                $this->sqlPayerEmail() . ', ' . 		                $this->sqlPayerPhoneNumber() . ', ' . 		                $this->sqlStreetLine1() . ', ' . 		                $this->sqlStreetLine2() . ', ' . 		                $this->sqlStreetLine3() . ', ' . 		                $this->sqlCity() . ', ' . 		                $this->sqlStateCode() . ', ' . 		                $this->sqlProvince() . ', ' . 		                $this->sqlPostalCode() . ', ' . 		                $this->sqlCountryCode() . ', ' . 		                $this->sqlIsVerified() . ', ' . 		                $this->sqlContactName() . ', ' . 		                $this->sqlContactPhoneNumber() . ', ' . 		                $this->sqlContactFaxNumber() . ', ' . 		                $this->sqlContactEmail() . ', ' . 		                $this->sqlPayeeName() . ', ' . 		                $this->sqlPosition() . ', ' . 		                $this->sqlAmount() . ', ' . 		                $this->sqlDescription() . ', ' . 		                $this->sqlDateStarted() . ', ' . 		                $this->sqlDateStopped() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlUpdatedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }

        $strSql = 'UPDATE
                      public.applicant_incomes
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlApplicantId() ) != $this->getOriginalValueByFieldName ( 'applicant_id' ) ) { $arrstrOriginalValueChanges['applicant_id'] = $this->sqlApplicantId(); $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' income_type_id = ' . $this->sqlIncomeTypeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIncomeTypeId() ) != $this->getOriginalValueByFieldName ( 'income_type_id' ) ) { $arrstrOriginalValueChanges['income_type_id'] = $this->sqlIncomeTypeId(); $strSql .= ' income_type_id = ' . $this->sqlIncomeTypeId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlFrequencyId() ) != $this->getOriginalValueByFieldName ( 'frequency_id' ) ) { $arrstrOriginalValueChanges['frequency_id'] = $this->sqlFrequencyId(); $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payer_name = ' . $this->sqlPayerName() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPayerName() ) != $this->getOriginalValueByFieldName ( 'payer_name' ) ) { $arrstrOriginalValueChanges['payer_name'] = $this->sqlPayerName(); $strSql .= ' payer_name = ' . $this->sqlPayerName() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payer_email = ' . $this->sqlPayerEmail() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPayerEmail() ) != $this->getOriginalValueByFieldName ( 'payer_email' ) ) { $arrstrOriginalValueChanges['payer_email'] = $this->sqlPayerEmail(); $strSql .= ' payer_email = ' . $this->sqlPayerEmail() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payer_phone_number = ' . $this->sqlPayerPhoneNumber() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPayerPhoneNumber() ) != $this->getOriginalValueByFieldName ( 'payer_phone_number' ) ) { $arrstrOriginalValueChanges['payer_phone_number'] = $this->sqlPayerPhoneNumber(); $strSql .= ' payer_phone_number = ' . $this->sqlPayerPhoneNumber() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlStreetLine1() ) != $this->getOriginalValueByFieldName ( 'street_line1' ) ) { $arrstrOriginalValueChanges['street_line1'] = $this->sqlStreetLine1(); $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlStreetLine2() ) != $this->getOriginalValueByFieldName ( 'street_line2' ) ) { $arrstrOriginalValueChanges['street_line2'] = $this->sqlStreetLine2(); $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlStreetLine3() ) != $this->getOriginalValueByFieldName ( 'street_line3' ) ) { $arrstrOriginalValueChanges['street_line3'] = $this->sqlStreetLine3(); $strSql .= ' street_line3 = ' . $this->sqlStreetLine3() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCity() ) != $this->getOriginalValueByFieldName ( 'city' ) ) { $arrstrOriginalValueChanges['city'] = $this->sqlCity(); $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlStateCode() ) != $this->getOriginalValueByFieldName ( 'state_code' ) ) { $arrstrOriginalValueChanges['state_code'] = $this->sqlStateCode(); $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' province = ' . $this->sqlProvince() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlProvince() ) != $this->getOriginalValueByFieldName ( 'province' ) ) { $arrstrOriginalValueChanges['province'] = $this->sqlProvince(); $strSql .= ' province = ' . $this->sqlProvince() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPostalCode() ) != $this->getOriginalValueByFieldName ( 'postal_code' ) ) { $arrstrOriginalValueChanges['postal_code'] = $this->sqlPostalCode(); $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCountryCode() ) != $this->getOriginalValueByFieldName ( 'country_code' ) ) { $arrstrOriginalValueChanges['country_code'] = $this->sqlCountryCode(); $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_verified = ' . $this->sqlIsVerified() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsVerified() ) != $this->getOriginalValueByFieldName ( 'is_verified' ) ) { $arrstrOriginalValueChanges['is_verified'] = $this->sqlIsVerified(); $strSql .= ' is_verified = ' . $this->sqlIsVerified() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_name = ' . $this->sqlContactName() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlContactName() ) != $this->getOriginalValueByFieldName ( 'contact_name' ) ) { $arrstrOriginalValueChanges['contact_name'] = $this->sqlContactName(); $strSql .= ' contact_name = ' . $this->sqlContactName() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_phone_number = ' . $this->sqlContactPhoneNumber() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlContactPhoneNumber() ) != $this->getOriginalValueByFieldName ( 'contact_phone_number' ) ) { $arrstrOriginalValueChanges['contact_phone_number'] = $this->sqlContactPhoneNumber(); $strSql .= ' contact_phone_number = ' . $this->sqlContactPhoneNumber() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_fax_number = ' . $this->sqlContactFaxNumber() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlContactFaxNumber() ) != $this->getOriginalValueByFieldName ( 'contact_fax_number' ) ) { $arrstrOriginalValueChanges['contact_fax_number'] = $this->sqlContactFaxNumber(); $strSql .= ' contact_fax_number = ' . $this->sqlContactFaxNumber() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_email = ' . $this->sqlContactEmail() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlContactEmail() ) != $this->getOriginalValueByFieldName ( 'contact_email' ) ) { $arrstrOriginalValueChanges['contact_email'] = $this->sqlContactEmail(); $strSql .= ' contact_email = ' . $this->sqlContactEmail() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payee_name = ' . $this->sqlPayeeName() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPayeeName() ) != $this->getOriginalValueByFieldName ( 'payee_name' ) ) { $arrstrOriginalValueChanges['payee_name'] = $this->sqlPayeeName(); $strSql .= ' payee_name = ' . $this->sqlPayeeName() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' position = ' . $this->sqlPosition() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPosition() ) != $this->getOriginalValueByFieldName ( 'position' ) ) { $arrstrOriginalValueChanges['position'] = $this->sqlPosition(); $strSql .= ' position = ' . $this->sqlPosition() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; } elseif( ( 0 != bccomp ( CStrings::reverseSqlFormat( $this->sqlAmount() ), $this->getOriginalValueByFieldName ( 'amount' ), 4 ) ) ) { $arrstrOriginalValueChanges['amount'] = $this->sqlAmount(); $strSql .= ' amount = ' . $this->sqlAmount() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDescription() ) != $this->getOriginalValueByFieldName ( 'description' ) ) { $arrstrOriginalValueChanges['description'] = $this->sqlDescription(); $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_started = ' . $this->sqlDateStarted() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDateStarted() ) != $this->getOriginalValueByFieldName ( 'date_started' ) ) { $arrstrOriginalValueChanges['date_started'] = $this->sqlDateStarted(); $strSql .= ' date_started = ' . $this->sqlDateStarted() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_stopped = ' . $this->sqlDateStopped() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDateStopped() ) != $this->getOriginalValueByFieldName ( 'date_stopped' ) ) { $arrstrOriginalValueChanges['date_stopped'] = $this->sqlDateStopped(); $strSql .= ' date_stopped = ' . $this->sqlDateStopped() . ','; $boolUpdate = true; }
                      $strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
                      $strSql .= ' updated_on = \'NOW()\'					WHERE					    id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.applicant_incomes WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.applicant_incomes_id_seq', $objDatabase );
    }

    public function toArray() {
        return array(
        	'id' => $this->getId(),
        	'cid' => $this->getCid(),
        	'applicant_id' => $this->getApplicantId(),
        	'income_type_id' => $this->getIncomeTypeId(),
        	'frequency_id' => $this->getFrequencyId(),
        	'payer_name' => $this->getPayerName(),
        	'payer_email' => $this->getPayerEmail(),
        	'payer_phone_number' => $this->getPayerPhoneNumber(),
        	'street_line1' => $this->getStreetLine1(),
        	'street_line2' => $this->getStreetLine2(),
        	'street_line3' => $this->getStreetLine3(),
        	'city' => $this->getCity(),
        	'state_code' => $this->getStateCode(),
        	'province' => $this->getProvince(),
        	'postal_code' => $this->getPostalCode(),
        	'country_code' => $this->getCountryCode(),
        	'is_verified' => $this->getIsVerified(),
        	'contact_name' => $this->getContactName(),
        	'contact_phone_number' => $this->getContactPhoneNumber(),
        	'contact_fax_number' => $this->getContactFaxNumber(),
        	'contact_email' => $this->getContactEmail(),
        	'payee_name' => $this->getPayeeName(),
        	'position' => $this->getPosition(),
        	'amount' => $this->getAmount(),
        	'description' => $this->getDescription(),
        	'date_started' => $this->getDateStarted(),
        	'date_stopped' => $this->getDateStopped(),
        	'updated_by' => $this->getUpdatedBy(),
        	'updated_on' => $this->getUpdatedOn(),
        	'created_by' => $this->getCreatedBy(),
        	'created_on' => $this->getCreatedOn()
        );
    }

}
?>