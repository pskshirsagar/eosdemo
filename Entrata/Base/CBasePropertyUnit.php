<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyUnit extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.property_units';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyBuildingId;
	protected $m_intUnitTypeId;
	protected $m_intUnitKindId;
	protected $m_intPropertyFloorplanId;
	protected $m_intPropertyFloorId;
	protected $m_intGenderId;
	protected $m_strRemotePrimaryKey;
	protected $m_strMarketingUnitNumber;
	protected $m_strUnitNumber;
	protected $m_strStyle;
	protected $m_fltSquareFeet;
	protected $m_intMaxOccupants;
	protected $m_intMaxPets;
	protected $m_intNumberOfBedrooms;
	protected $m_fltNumberOfBathrooms;
	protected $m_strCarpetColor;
	protected $m_strUnitCoordinates;
	protected $m_fltSitePlanXPos;
	protected $m_fltSitePlanYPos;
	protected $m_fltFloorXPos;
	protected $m_fltFloorYPos;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intBlockIntegration;
	protected $m_intBlockPricingIntegration;
	protected $m_intIsFurnished;
	protected $m_boolIsSmart;
	protected $m_intOrderNum;
	protected $m_strImportedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strMilitaryNeighborhoodCode;

	public function __construct() {
		parent::__construct();

		$this->m_intBlockIntegration = '0';
		$this->m_intBlockPricingIntegration = '0';
		$this->m_intIsFurnished = '0';
		$this->m_boolIsSmart = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_building_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyBuildingId', trim( $arrValues['property_building_id'] ) ); elseif( isset( $arrValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrValues['property_building_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['unit_kind_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitKindId', trim( $arrValues['unit_kind_id'] ) ); elseif( isset( $arrValues['unit_kind_id'] ) ) $this->setUnitKindId( $arrValues['unit_kind_id'] );
		if( isset( $arrValues['property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorplanId', trim( $arrValues['property_floorplan_id'] ) ); elseif( isset( $arrValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrValues['property_floorplan_id'] );
		if( isset( $arrValues['property_floor_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorId', trim( $arrValues['property_floor_id'] ) ); elseif( isset( $arrValues['property_floor_id'] ) ) $this->setPropertyFloorId( $arrValues['property_floor_id'] );
		if( isset( $arrValues['gender_id'] ) && $boolDirectSet ) $this->set( 'm_intGenderId', trim( $arrValues['gender_id'] ) ); elseif( isset( $arrValues['gender_id'] ) ) $this->setGenderId( $arrValues['gender_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['marketing_unit_number'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strMarketingUnitNumber', trim( stripcslashes( $arrValues['marketing_unit_number'] ) ) ); elseif( isset( $arrValues['marketing_unit_number'] ) ) $this->setMarketingUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['marketing_unit_number'] ) : $arrValues['marketing_unit_number'] );
		if( isset( $arrValues['unit_number'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strUnitNumber', trim( stripcslashes( $arrValues['unit_number'] ) ) ); elseif( isset( $arrValues['unit_number'] ) ) $this->setUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_number'] ) : $arrValues['unit_number'] );
		if( isset( $arrValues['style'] ) && $boolDirectSet ) $this->set( 'm_strStyle', trim( stripcslashes( $arrValues['style'] ) ) ); elseif( isset( $arrValues['style'] ) ) $this->setStyle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['style'] ) : $arrValues['style'] );
		if( isset( $arrValues['square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltSquareFeet', trim( $arrValues['square_feet'] ) ); elseif( isset( $arrValues['square_feet'] ) ) $this->setSquareFeet( $arrValues['square_feet'] );
		if( isset( $arrValues['max_occupants'] ) && $boolDirectSet ) $this->set( 'm_intMaxOccupants', trim( $arrValues['max_occupants'] ) ); elseif( isset( $arrValues['max_occupants'] ) ) $this->setMaxOccupants( $arrValues['max_occupants'] );
		if( isset( $arrValues['max_pets'] ) && $boolDirectSet ) $this->set( 'm_intMaxPets', trim( $arrValues['max_pets'] ) ); elseif( isset( $arrValues['max_pets'] ) ) $this->setMaxPets( $arrValues['max_pets'] );
		if( isset( $arrValues['number_of_bedrooms'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfBedrooms', trim( $arrValues['number_of_bedrooms'] ) ); elseif( isset( $arrValues['number_of_bedrooms'] ) ) $this->setNumberOfBedrooms( $arrValues['number_of_bedrooms'] );
		if( isset( $arrValues['number_of_bathrooms'] ) && $boolDirectSet ) $this->set( 'm_fltNumberOfBathrooms', trim( $arrValues['number_of_bathrooms'] ) ); elseif( isset( $arrValues['number_of_bathrooms'] ) ) $this->setNumberOfBathrooms( $arrValues['number_of_bathrooms'] );
		if( isset( $arrValues['carpet_color'] ) && $boolDirectSet ) $this->set( 'm_strCarpetColor', trim( stripcslashes( $arrValues['carpet_color'] ) ) ); elseif( isset( $arrValues['carpet_color'] ) ) $this->setCarpetColor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['carpet_color'] ) : $arrValues['carpet_color'] );
		if( isset( $arrValues['unit_coordinates'] ) && $boolDirectSet ) $this->set( 'm_strUnitCoordinates', trim( stripcslashes( $arrValues['unit_coordinates'] ) ) ); elseif( isset( $arrValues['unit_coordinates'] ) ) $this->setUnitCoordinates( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_coordinates'] ) : $arrValues['unit_coordinates'] );
		if( isset( $arrValues['site_plan_x_pos'] ) && $boolDirectSet ) $this->set( 'm_fltSitePlanXPos', trim( $arrValues['site_plan_x_pos'] ) ); elseif( isset( $arrValues['site_plan_x_pos'] ) ) $this->setSitePlanXPos( $arrValues['site_plan_x_pos'] );
		if( isset( $arrValues['site_plan_y_pos'] ) && $boolDirectSet ) $this->set( 'm_fltSitePlanYPos', trim( $arrValues['site_plan_y_pos'] ) ); elseif( isset( $arrValues['site_plan_y_pos'] ) ) $this->setSitePlanYPos( $arrValues['site_plan_y_pos'] );
		if( isset( $arrValues['floor_x_pos'] ) && $boolDirectSet ) $this->set( 'm_fltFloorXPos', trim( $arrValues['floor_x_pos'] ) ); elseif( isset( $arrValues['floor_x_pos'] ) ) $this->setFloorXPos( $arrValues['floor_x_pos'] );
		if( isset( $arrValues['floor_y_pos'] ) && $boolDirectSet ) $this->set( 'm_fltFloorYPos', trim( $arrValues['floor_y_pos'] ) ); elseif( isset( $arrValues['floor_y_pos'] ) ) $this->setFloorYPos( $arrValues['floor_y_pos'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['block_integration'] ) && $boolDirectSet ) $this->set( 'm_intBlockIntegration', trim( $arrValues['block_integration'] ) ); elseif( isset( $arrValues['block_integration'] ) ) $this->setBlockIntegration( $arrValues['block_integration'] );
		if( isset( $arrValues['block_pricing_integration'] ) && $boolDirectSet ) $this->set( 'm_intBlockPricingIntegration', trim( $arrValues['block_pricing_integration'] ) ); elseif( isset( $arrValues['block_pricing_integration'] ) ) $this->setBlockPricingIntegration( $arrValues['block_pricing_integration'] );
		if( isset( $arrValues['is_furnished'] ) && $boolDirectSet ) $this->set( 'm_intIsFurnished', trim( $arrValues['is_furnished'] ) ); elseif( isset( $arrValues['is_furnished'] ) ) $this->setIsFurnished( $arrValues['is_furnished'] );
		if( isset( $arrValues['is_smart'] ) && $boolDirectSet ) $this->set( 'm_boolIsSmart', trim( stripcslashes( $arrValues['is_smart'] ) ) ); elseif( isset( $arrValues['is_smart'] ) ) $this->setIsSmart( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_smart'] ) : $arrValues['is_smart'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['imported_on'] ) && $boolDirectSet ) $this->set( 'm_strImportedOn', trim( $arrValues['imported_on'] ) ); elseif( isset( $arrValues['imported_on'] ) ) $this->setImportedOn( $arrValues['imported_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['military_neighborhood_code'] ) && $boolDirectSet ) $this->set( 'm_strMilitaryNeighborhoodCode', trim( stripcslashes( $arrValues['military_neighborhood_code'] ) ) ); elseif( isset( $arrValues['military_neighborhood_code'] ) ) $this->setMilitaryNeighborhoodCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['military_neighborhood_code'] ) : $arrValues['military_neighborhood_code'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		$this->set( 'm_intPropertyBuildingId', CStrings::strToIntDef( $intPropertyBuildingId, NULL, false ) );
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function sqlPropertyBuildingId() {
		return ( true == isset( $this->m_intPropertyBuildingId ) ) ? ( string ) $this->m_intPropertyBuildingId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setUnitKindId( $intUnitKindId ) {
		$this->set( 'm_intUnitKindId', CStrings::strToIntDef( $intUnitKindId, NULL, false ) );
	}

	public function getUnitKindId() {
		return $this->m_intUnitKindId;
	}

	public function sqlUnitKindId() {
		return ( true == isset( $this->m_intUnitKindId ) ) ? ( string ) $this->m_intUnitKindId : 'NULL';
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->set( 'm_intPropertyFloorplanId', CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false ) );
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function sqlPropertyFloorplanId() {
		return ( true == isset( $this->m_intPropertyFloorplanId ) ) ? ( string ) $this->m_intPropertyFloorplanId : 'NULL';
	}

	public function setPropertyFloorId( $intPropertyFloorId ) {
		$this->set( 'm_intPropertyFloorId', CStrings::strToIntDef( $intPropertyFloorId, NULL, false ) );
	}

	public function getPropertyFloorId() {
		return $this->m_intPropertyFloorId;
	}

	public function sqlPropertyFloorId() {
		return ( true == isset( $this->m_intPropertyFloorId ) ) ? ( string ) $this->m_intPropertyFloorId : 'NULL';
	}

	public function setGenderId( $intGenderId ) {
		$this->set( 'm_intGenderId', CStrings::strToIntDef( $intGenderId, NULL, false ) );
	}

	public function getGenderId() {
		return $this->m_intGenderId;
	}

	public function sqlGenderId() {
		return ( true == isset( $this->m_intGenderId ) ) ? ( string ) $this->m_intGenderId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setMarketingUnitNumber( $strMarketingUnitNumber, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strMarketingUnitNumber', CStrings::strTrimDef( $strMarketingUnitNumber, 50, NULL, true ), $strLocaleCode );
	}

	public function getMarketingUnitNumber( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strMarketingUnitNumber', $strLocaleCode );
	}

	public function sqlMarketingUnitNumber() {
		return ( true == isset( $this->m_strMarketingUnitNumber ) ) ? '\'' . addslashes( $this->m_strMarketingUnitNumber ) . '\'' : 'NULL';
	}

	public function setUnitNumber( $strUnitNumber, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strUnitNumber', CStrings::strTrimDef( $strUnitNumber, 50, NULL, true ), $strLocaleCode );
	}

	public function getUnitNumber( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strUnitNumber', $strLocaleCode );
	}

	public function sqlUnitNumber() {
		return ( true == isset( $this->m_strUnitNumber ) ) ? '\'' . addslashes( $this->m_strUnitNumber ) . '\'' : 'NULL';
	}

	public function getNonTranslatedUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function setStyle( $strStyle ) {
		$this->set( 'm_strStyle', CStrings::strTrimDef( $strStyle, 50, NULL, true ) );
	}

	public function getStyle() {
		return $this->m_strStyle;
	}

	public function sqlStyle() {
		return ( true == isset( $this->m_strStyle ) ) ? '\'' . addslashes( $this->m_strStyle ) . '\'' : 'NULL';
	}

	public function setSquareFeet( $fltSquareFeet ) {
		$this->set( 'm_fltSquareFeet', CStrings::strToFloatDef( $fltSquareFeet, NULL, false, 4 ) );
	}

	public function getSquareFeet() {
		return $this->m_fltSquareFeet;
	}

	public function sqlSquareFeet() {
		return ( true == isset( $this->m_fltSquareFeet ) ) ? ( string ) $this->m_fltSquareFeet : 'NULL';
	}

	public function setMaxOccupants( $intMaxOccupants ) {
		$this->set( 'm_intMaxOccupants', CStrings::strToIntDef( $intMaxOccupants, NULL, false ) );
	}

	public function getMaxOccupants() {
		return $this->m_intMaxOccupants;
	}

	public function sqlMaxOccupants() {
		return ( true == isset( $this->m_intMaxOccupants ) ) ? ( string ) $this->m_intMaxOccupants : 'NULL';
	}

	public function setMaxPets( $intMaxPets ) {
		$this->set( 'm_intMaxPets', CStrings::strToIntDef( $intMaxPets, NULL, false ) );
	}

	public function getMaxPets() {
		return $this->m_intMaxPets;
	}

	public function sqlMaxPets() {
		return ( true == isset( $this->m_intMaxPets ) ) ? ( string ) $this->m_intMaxPets : 'NULL';
	}

	public function setNumberOfBedrooms( $intNumberOfBedrooms ) {
		$this->set( 'm_intNumberOfBedrooms', CStrings::strToIntDef( $intNumberOfBedrooms, NULL, false ) );
	}

	public function getNumberOfBedrooms() {
		return $this->m_intNumberOfBedrooms;
	}

	public function sqlNumberOfBedrooms() {
		return ( true == isset( $this->m_intNumberOfBedrooms ) ) ? ( string ) $this->m_intNumberOfBedrooms : 'NULL';
	}

	public function setNumberOfBathrooms( $fltNumberOfBathrooms ) {
		$this->set( 'm_fltNumberOfBathrooms', CStrings::strToFloatDef( $fltNumberOfBathrooms, NULL, false, 4 ) );
	}

	public function getNumberOfBathrooms() {
		return $this->m_fltNumberOfBathrooms;
	}

	public function sqlNumberOfBathrooms() {
		return ( true == isset( $this->m_fltNumberOfBathrooms ) ) ? ( string ) $this->m_fltNumberOfBathrooms : 'NULL';
	}

	public function setCarpetColor( $strCarpetColor ) {
		$this->set( 'm_strCarpetColor', CStrings::strTrimDef( $strCarpetColor, 50, NULL, true ) );
	}

	public function getCarpetColor() {
		return $this->m_strCarpetColor;
	}

	public function sqlCarpetColor() {
		return ( true == isset( $this->m_strCarpetColor ) ) ? '\'' . addslashes( $this->m_strCarpetColor ) . '\'' : 'NULL';
	}

	public function setUnitCoordinates( $strUnitCoordinates ) {
		$this->set( 'm_strUnitCoordinates', CStrings::strTrimDef( $strUnitCoordinates, 2000, NULL, true ) );
	}

	public function getUnitCoordinates() {
		return $this->m_strUnitCoordinates;
	}

	public function sqlUnitCoordinates() {
		return ( true == isset( $this->m_strUnitCoordinates ) ) ? '\'' . addslashes( $this->m_strUnitCoordinates ) . '\'' : 'NULL';
	}

	public function setSitePlanXPos( $fltSitePlanXPos ) {
		$this->set( 'm_fltSitePlanXPos', CStrings::strToFloatDef( $fltSitePlanXPos, NULL, false, 4 ) );
	}

	public function getSitePlanXPos() {
		return $this->m_fltSitePlanXPos;
	}

	public function sqlSitePlanXPos() {
		return ( true == isset( $this->m_fltSitePlanXPos ) ) ? ( string ) $this->m_fltSitePlanXPos : 'NULL';
	}

	public function setSitePlanYPos( $fltSitePlanYPos ) {
		$this->set( 'm_fltSitePlanYPos', CStrings::strToFloatDef( $fltSitePlanYPos, NULL, false, 4 ) );
	}

	public function getSitePlanYPos() {
		return $this->m_fltSitePlanYPos;
	}

	public function sqlSitePlanYPos() {
		return ( true == isset( $this->m_fltSitePlanYPos ) ) ? ( string ) $this->m_fltSitePlanYPos : 'NULL';
	}

	public function setFloorXPos( $fltFloorXPos ) {
		$this->set( 'm_fltFloorXPos', CStrings::strToFloatDef( $fltFloorXPos, NULL, false, 4 ) );
	}

	public function getFloorXPos() {
		return $this->m_fltFloorXPos;
	}

	public function sqlFloorXPos() {
		return ( true == isset( $this->m_fltFloorXPos ) ) ? ( string ) $this->m_fltFloorXPos : 'NULL';
	}

	public function setFloorYPos( $fltFloorYPos ) {
		$this->set( 'm_fltFloorYPos', CStrings::strToFloatDef( $fltFloorYPos, NULL, false, 4 ) );
	}

	public function getFloorYPos() {
		return $this->m_fltFloorYPos;
	}

	public function sqlFloorYPos() {
		return ( true == isset( $this->m_fltFloorYPos ) ) ? ( string ) $this->m_fltFloorYPos : 'NULL';
	}

	public function setBlockIntegration( $intBlockIntegration ) {
		$this->set( 'm_intBlockIntegration', CStrings::strToIntDef( $intBlockIntegration, NULL, false ) );
	}

	public function getBlockIntegration() {
		return $this->m_intBlockIntegration;
	}

	public function sqlBlockIntegration() {
		return ( true == isset( $this->m_intBlockIntegration ) ) ? ( string ) $this->m_intBlockIntegration : '0';
	}

	public function setBlockPricingIntegration( $intBlockPricingIntegration ) {
		$this->set( 'm_intBlockPricingIntegration', CStrings::strToIntDef( $intBlockPricingIntegration, NULL, false ) );
	}

	public function getBlockPricingIntegration() {
		return $this->m_intBlockPricingIntegration;
	}

	public function sqlBlockPricingIntegration() {
		return ( true == isset( $this->m_intBlockPricingIntegration ) ) ? ( string ) $this->m_intBlockPricingIntegration : '0';
	}

	public function setIsFurnished( $intIsFurnished ) {
		$this->set( 'm_intIsFurnished', CStrings::strToIntDef( $intIsFurnished, NULL, false ) );
	}

	public function getIsFurnished() {
		return $this->m_intIsFurnished;
	}

	public function sqlIsFurnished() {
		return ( true == isset( $this->m_intIsFurnished ) ) ? ( string ) $this->m_intIsFurnished : '0';
	}

	public function setIsSmart( $boolIsSmart ) {
		$this->set( 'm_boolIsSmart', CStrings::strToBool( $boolIsSmart ) );
	}

	public function getIsSmart() {
		return $this->m_boolIsSmart;
	}

	public function sqlIsSmart() {
		return ( true == isset( $this->m_boolIsSmart ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSmart ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setImportedOn( $strImportedOn ) {
		$this->set( 'm_strImportedOn', CStrings::strTrimDef( $strImportedOn, -1, NULL, true ) );
	}

	public function getImportedOn() {
		return $this->m_strImportedOn;
	}

	public function sqlImportedOn() {
		return ( true == isset( $this->m_strImportedOn ) ) ? '\'' . $this->m_strImportedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setMilitaryNeighborhoodCode( $strMilitaryNeighborhoodCode ) {
		$this->set( 'm_strMilitaryNeighborhoodCode', CStrings::strTrimDef( $strMilitaryNeighborhoodCode, 50, NULL, true ) );
	}

	public function getMilitaryNeighborhoodCode() {
		return $this->m_strMilitaryNeighborhoodCode;
	}

	public function sqlMilitaryNeighborhoodCode() {
		return ( true == isset( $this->m_strMilitaryNeighborhoodCode ) ) ? '\'' . addslashes( $this->m_strMilitaryNeighborhoodCode ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_building_id, unit_type_id, unit_kind_id, property_floorplan_id, property_floor_id, gender_id, remote_primary_key, marketing_unit_number, unit_number, style, square_feet, max_occupants, max_pets, number_of_bedrooms, number_of_bathrooms, carpet_color, unit_coordinates, site_plan_x_pos, site_plan_y_pos, floor_x_pos, floor_y_pos, details, block_integration, block_pricing_integration, is_furnished, is_smart, order_num, imported_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, military_neighborhood_code )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyBuildingId() . ', ' .
						$this->sqlUnitTypeId() . ', ' .
						$this->sqlUnitKindId() . ', ' .
						$this->sqlPropertyFloorplanId() . ', ' .
						$this->sqlPropertyFloorId() . ', ' .
						$this->sqlGenderId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlMarketingUnitNumber() . ', ' .
						$this->sqlUnitNumber() . ', ' .
						$this->sqlStyle() . ', ' .
						$this->sqlSquareFeet() . ', ' .
						$this->sqlMaxOccupants() . ', ' .
						$this->sqlMaxPets() . ', ' .
						$this->sqlNumberOfBedrooms() . ', ' .
						$this->sqlNumberOfBathrooms() . ', ' .
						$this->sqlCarpetColor() . ', ' .
						$this->sqlUnitCoordinates() . ', ' .
						$this->sqlSitePlanXPos() . ', ' .
						$this->sqlSitePlanYPos() . ', ' .
						$this->sqlFloorXPos() . ', ' .
						$this->sqlFloorYPos() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlBlockIntegration() . ', ' .
						$this->sqlBlockPricingIntegration() . ', ' .
						$this->sqlIsFurnished() . ', ' .
						$this->sqlIsSmart() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlImportedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlMilitaryNeighborhoodCode() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId(). ',' ; } elseif( true == array_key_exists( 'PropertyBuildingId', $this->getChangedColumns() ) ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_kind_id = ' . $this->sqlUnitKindId(). ',' ; } elseif( true == array_key_exists( 'UnitKindId', $this->getChangedColumns() ) ) { $strSql .= ' unit_kind_id = ' . $this->sqlUnitKindId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId(). ',' ; } elseif( true == array_key_exists( 'PropertyFloorplanId', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floor_id = ' . $this->sqlPropertyFloorId(). ',' ; } elseif( true == array_key_exists( 'PropertyFloorId', $this->getChangedColumns() ) ) { $strSql .= ' property_floor_id = ' . $this->sqlPropertyFloorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gender_id = ' . $this->sqlGenderId(). ',' ; } elseif( true == array_key_exists( 'GenderId', $this->getChangedColumns() ) ) { $strSql .= ' gender_id = ' . $this->sqlGenderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_unit_number = ' . $this->sqlMarketingUnitNumber(). ',' ; } elseif( true == array_key_exists( 'MarketingUnitNumber', $this->getChangedColumns() ) ) { $strSql .= ' marketing_unit_number = ' . $this->sqlMarketingUnitNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber(). ',' ; } elseif( true == array_key_exists( 'UnitNumber', $this->getChangedColumns() ) ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' style = ' . $this->sqlStyle(). ',' ; } elseif( true == array_key_exists( 'Style', $this->getChangedColumns() ) ) { $strSql .= ' style = ' . $this->sqlStyle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' square_feet = ' . $this->sqlSquareFeet(). ',' ; } elseif( true == array_key_exists( 'SquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' square_feet = ' . $this->sqlSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_occupants = ' . $this->sqlMaxOccupants(). ',' ; } elseif( true == array_key_exists( 'MaxOccupants', $this->getChangedColumns() ) ) { $strSql .= ' max_occupants = ' . $this->sqlMaxOccupants() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_pets = ' . $this->sqlMaxPets(). ',' ; } elseif( true == array_key_exists( 'MaxPets', $this->getChangedColumns() ) ) { $strSql .= ' max_pets = ' . $this->sqlMaxPets() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_bedrooms = ' . $this->sqlNumberOfBedrooms(). ',' ; } elseif( true == array_key_exists( 'NumberOfBedrooms', $this->getChangedColumns() ) ) { $strSql .= ' number_of_bedrooms = ' . $this->sqlNumberOfBedrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_bathrooms = ' . $this->sqlNumberOfBathrooms(). ',' ; } elseif( true == array_key_exists( 'NumberOfBathrooms', $this->getChangedColumns() ) ) { $strSql .= ' number_of_bathrooms = ' . $this->sqlNumberOfBathrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' carpet_color = ' . $this->sqlCarpetColor(). ',' ; } elseif( true == array_key_exists( 'CarpetColor', $this->getChangedColumns() ) ) { $strSql .= ' carpet_color = ' . $this->sqlCarpetColor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_coordinates = ' . $this->sqlUnitCoordinates(). ',' ; } elseif( true == array_key_exists( 'UnitCoordinates', $this->getChangedColumns() ) ) { $strSql .= ' unit_coordinates = ' . $this->sqlUnitCoordinates() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' site_plan_x_pos = ' . $this->sqlSitePlanXPos(). ',' ; } elseif( true == array_key_exists( 'SitePlanXPos', $this->getChangedColumns() ) ) { $strSql .= ' site_plan_x_pos = ' . $this->sqlSitePlanXPos() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' site_plan_y_pos = ' . $this->sqlSitePlanYPos(). ',' ; } elseif( true == array_key_exists( 'SitePlanYPos', $this->getChangedColumns() ) ) { $strSql .= ' site_plan_y_pos = ' . $this->sqlSitePlanYPos() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' floor_x_pos = ' . $this->sqlFloorXPos(). ',' ; } elseif( true == array_key_exists( 'FloorXPos', $this->getChangedColumns() ) ) { $strSql .= ' floor_x_pos = ' . $this->sqlFloorXPos() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' floor_y_pos = ' . $this->sqlFloorYPos(). ',' ; } elseif( true == array_key_exists( 'FloorYPos', $this->getChangedColumns() ) ) { $strSql .= ' floor_y_pos = ' . $this->sqlFloorYPos() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block_integration = ' . $this->sqlBlockIntegration(). ',' ; } elseif( true == array_key_exists( 'BlockIntegration', $this->getChangedColumns() ) ) { $strSql .= ' block_integration = ' . $this->sqlBlockIntegration() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block_pricing_integration = ' . $this->sqlBlockPricingIntegration(). ',' ; } elseif( true == array_key_exists( 'BlockPricingIntegration', $this->getChangedColumns() ) ) { $strSql .= ' block_pricing_integration = ' . $this->sqlBlockPricingIntegration() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_furnished = ' . $this->sqlIsFurnished(). ',' ; } elseif( true == array_key_exists( 'IsFurnished', $this->getChangedColumns() ) ) { $strSql .= ' is_furnished = ' . $this->sqlIsFurnished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_smart = ' . $this->sqlIsSmart(). ',' ; } elseif( true == array_key_exists( 'IsSmart', $this->getChangedColumns() ) ) { $strSql .= ' is_smart = ' . $this->sqlIsSmart() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn(). ',' ; } elseif( true == array_key_exists( 'ImportedOn', $this->getChangedColumns() ) ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' military_neighborhood_code = ' . $this->sqlMilitaryNeighborhoodCode(). ',' ; } elseif( true == array_key_exists( 'MilitaryNeighborhoodCode', $this->getChangedColumns() ) ) { $strSql .= ' military_neighborhood_code = ' . $this->sqlMilitaryNeighborhoodCode() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_building_id' => $this->getPropertyBuildingId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'unit_kind_id' => $this->getUnitKindId(),
			'property_floorplan_id' => $this->getPropertyFloorplanId(),
			'property_floor_id' => $this->getPropertyFloorId(),
			'gender_id' => $this->getGenderId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'marketing_unit_number' => $this->getMarketingUnitNumber(),
			'unit_number' => $this->getUnitNumber(),
			'style' => $this->getStyle(),
			'square_feet' => $this->getSquareFeet(),
			'max_occupants' => $this->getMaxOccupants(),
			'max_pets' => $this->getMaxPets(),
			'number_of_bedrooms' => $this->getNumberOfBedrooms(),
			'number_of_bathrooms' => $this->getNumberOfBathrooms(),
			'carpet_color' => $this->getCarpetColor(),
			'unit_coordinates' => $this->getUnitCoordinates(),
			'site_plan_x_pos' => $this->getSitePlanXPos(),
			'site_plan_y_pos' => $this->getSitePlanYPos(),
			'floor_x_pos' => $this->getFloorXPos(),
			'floor_y_pos' => $this->getFloorYPos(),
			'details' => $this->getDetails(),
			'block_integration' => $this->getBlockIntegration(),
			'block_pricing_integration' => $this->getBlockPricingIntegration(),
			'is_furnished' => $this->getIsFurnished(),
			'is_smart' => $this->getIsSmart(),
			'order_num' => $this->getOrderNum(),
			'imported_on' => $this->getImportedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'military_neighborhood_code' => $this->getMilitaryNeighborhoodCode()
		);
	}

}
?>