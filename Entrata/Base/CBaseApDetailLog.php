<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApDetailLog extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.ap_detail_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApDetailId;
	protected $m_intApHeaderLogId;
	protected $m_intApHeaderId;
	protected $m_intApPhysicalStatusTypeId;
	protected $m_intGlTransactionTypeId;
	protected $m_intApTransactionTypeId;
	protected $m_intApPostTypeId;
	protected $m_intBankAccountId;
	protected $m_intPropertyId;
	protected $m_intInterCoPropertyId;
	protected $m_intPropertyUnitId;
	protected $m_intApFormulaId;
	protected $m_intCompanyDepartmentId;
	protected $m_intGlDimensionId;
	protected $m_intJobPhaseId;
	protected $m_intApContractId;
	protected $m_intBudgetApHeaderId;
	protected $m_intApContractApDetailId;
	protected $m_intPoApDetailId;
	protected $m_intReversalApDetailId;
	protected $m_intScheduledApDetailId;
	protected $m_intTemplateApDetailId;
	protected $m_intReimbursementApDetailId;
	protected $m_intUnitOfMeasureId;
	protected $m_intApCatalogItemId;
	protected $m_intApCodeId;
	protected $m_intApPayeeSubAccountId;
	protected $m_intJobApCodeId;
	protected $m_intGlAccountId;
	protected $m_intApGlAccountId;
	protected $m_intWipGlAccountId;
	protected $m_intPurchasesClearingGlAccountId;
	protected $m_intPendingReimbursementsGlAccountId;
	protected $m_intInterCoApGlAccountId;
	protected $m_intBankGlAccountId;
	protected $m_intRetentionPayableGlAccountId;
	protected $m_intAccrualDebitGlAccountId;
	protected $m_intAccrualCreditGlAccountId;
	protected $m_intCashDebitGlAccountId;
	protected $m_intCashCreditGlAccountId;
	protected $m_intInterCoArTransactionId;
	protected $m_intRefundArTransactionId;
	protected $m_intAssetTransactionId;
	protected $m_intApExportBatchId;
	protected $m_intFeeId;
	protected $m_intPeriodId;
	protected $m_intPropertyBuildingId;
	protected $m_strPostMonth;
	protected $m_strRemotePrimaryKey;
	protected $m_strTransactionDatetime;
	protected $m_strLogDatetime;
	protected $m_fltQuantityOrdered;
	protected $m_fltRate;
	protected $m_fltSubtotalAmount;
	protected $m_fltTaxAmount;
	protected $m_fltDiscountAmount;
	protected $m_fltShippingAmount;
	protected $m_fltPreApprovalAmount;
	protected $m_fltTransactionAmount;
	protected $m_fltTransactionAmountDue;
	protected $m_fltInitialAmountDue;
	protected $m_fltDrawnAmount;
	protected $m_strDescription;
	protected $m_boolIsConfidential;
	protected $m_boolIs1099;
	protected $m_boolIsApprovedForPayment;
	protected $m_boolIsDeleted;
	protected $m_boolIsReversed;
	protected $m_intPaymentApprovedBy;
	protected $m_strPaymentApprovedOn;
	protected $m_intExportedBy;
	protected $m_strExportedOn;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intOldPoDetailLogId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intCcApPaymentId;
	protected $m_intReimbursementMethodId;
	protected $m_intGlHeaderId;
	protected $m_fltRetentionAmount;

	public function __construct() {
		parent::__construct();

		$this->m_intApTransactionTypeId = '1';
		$this->m_intApPostTypeId = '4';
		$this->m_fltQuantityOrdered = '1';
		$this->m_fltRate = '0';
		$this->m_fltSubtotalAmount = ( 0 );
		$this->m_fltTaxAmount = ( 0 );
		$this->m_fltDiscountAmount = ( 0 );
		$this->m_fltShippingAmount = ( 0 );
		$this->m_fltPreApprovalAmount = '0';
		$this->m_fltTransactionAmount = '0';
		$this->m_fltTransactionAmountDue = '0';
		$this->m_fltInitialAmountDue = '0';
		$this->m_fltDrawnAmount = '0';
		$this->m_boolIsConfidential = false;
		$this->m_boolIs1099 = false;
		$this->m_boolIsApprovedForPayment = false;
		$this->m_boolIsDeleted = false;
		$this->m_boolIsReversed = false;
		$this->m_intReimbursementMethodId = '1';
		$this->m_fltRetentionAmount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ap_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intApDetailId', trim( $arrValues['ap_detail_id'] ) ); elseif( isset( $arrValues['ap_detail_id'] ) ) $this->setApDetailId( $arrValues['ap_detail_id'] );
		if( isset( $arrValues['ap_header_log_id'] ) && $boolDirectSet ) $this->set( 'm_intApHeaderLogId', trim( $arrValues['ap_header_log_id'] ) ); elseif( isset( $arrValues['ap_header_log_id'] ) ) $this->setApHeaderLogId( $arrValues['ap_header_log_id'] );
		if( isset( $arrValues['ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intApHeaderId', trim( $arrValues['ap_header_id'] ) ); elseif( isset( $arrValues['ap_header_id'] ) ) $this->setApHeaderId( $arrValues['ap_header_id'] );
		if( isset( $arrValues['ap_physical_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApPhysicalStatusTypeId', trim( $arrValues['ap_physical_status_type_id'] ) ); elseif( isset( $arrValues['ap_physical_status_type_id'] ) ) $this->setApPhysicalStatusTypeId( $arrValues['ap_physical_status_type_id'] );
		if( isset( $arrValues['gl_transaction_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlTransactionTypeId', trim( $arrValues['gl_transaction_type_id'] ) ); elseif( isset( $arrValues['gl_transaction_type_id'] ) ) $this->setGlTransactionTypeId( $arrValues['gl_transaction_type_id'] );
		if( isset( $arrValues['ap_transaction_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApTransactionTypeId', trim( $arrValues['ap_transaction_type_id'] ) ); elseif( isset( $arrValues['ap_transaction_type_id'] ) ) $this->setApTransactionTypeId( $arrValues['ap_transaction_type_id'] );
		if( isset( $arrValues['ap_post_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApPostTypeId', trim( $arrValues['ap_post_type_id'] ) ); elseif( isset( $arrValues['ap_post_type_id'] ) ) $this->setApPostTypeId( $arrValues['ap_post_type_id'] );
		if( isset( $arrValues['bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBankAccountId', trim( $arrValues['bank_account_id'] ) ); elseif( isset( $arrValues['bank_account_id'] ) ) $this->setBankAccountId( $arrValues['bank_account_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['inter_co_property_id'] ) && $boolDirectSet ) $this->set( 'm_intInterCoPropertyId', trim( $arrValues['inter_co_property_id'] ) ); elseif( isset( $arrValues['inter_co_property_id'] ) ) $this->setInterCoPropertyId( $arrValues['inter_co_property_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['ap_formula_id'] ) && $boolDirectSet ) $this->set( 'm_intApFormulaId', trim( $arrValues['ap_formula_id'] ) ); elseif( isset( $arrValues['ap_formula_id'] ) ) $this->setApFormulaId( $arrValues['ap_formula_id'] );
		if( isset( $arrValues['company_department_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyDepartmentId', trim( $arrValues['company_department_id'] ) ); elseif( isset( $arrValues['company_department_id'] ) ) $this->setCompanyDepartmentId( $arrValues['company_department_id'] );
		if( isset( $arrValues['gl_dimension_id'] ) && $boolDirectSet ) $this->set( 'm_intGlDimensionId', trim( $arrValues['gl_dimension_id'] ) ); elseif( isset( $arrValues['gl_dimension_id'] ) ) $this->setGlDimensionId( $arrValues['gl_dimension_id'] );
		if( isset( $arrValues['job_phase_id'] ) && $boolDirectSet ) $this->set( 'm_intJobPhaseId', trim( $arrValues['job_phase_id'] ) ); elseif( isset( $arrValues['job_phase_id'] ) ) $this->setJobPhaseId( $arrValues['job_phase_id'] );
		if( isset( $arrValues['ap_contract_id'] ) && $boolDirectSet ) $this->set( 'm_intApContractId', trim( $arrValues['ap_contract_id'] ) ); elseif( isset( $arrValues['ap_contract_id'] ) ) $this->setApContractId( $arrValues['ap_contract_id'] );
		if( isset( $arrValues['budget_ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetApHeaderId', trim( $arrValues['budget_ap_header_id'] ) ); elseif( isset( $arrValues['budget_ap_header_id'] ) ) $this->setBudgetApHeaderId( $arrValues['budget_ap_header_id'] );
		if( isset( $arrValues['ap_contract_ap_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intApContractApDetailId', trim( $arrValues['ap_contract_ap_detail_id'] ) ); elseif( isset( $arrValues['ap_contract_ap_detail_id'] ) ) $this->setApContractApDetailId( $arrValues['ap_contract_ap_detail_id'] );
		if( isset( $arrValues['po_ap_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intPoApDetailId', trim( $arrValues['po_ap_detail_id'] ) ); elseif( isset( $arrValues['po_ap_detail_id'] ) ) $this->setPoApDetailId( $arrValues['po_ap_detail_id'] );
		if( isset( $arrValues['reversal_ap_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intReversalApDetailId', trim( $arrValues['reversal_ap_detail_id'] ) ); elseif( isset( $arrValues['reversal_ap_detail_id'] ) ) $this->setReversalApDetailId( $arrValues['reversal_ap_detail_id'] );
		if( isset( $arrValues['scheduled_ap_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledApDetailId', trim( $arrValues['scheduled_ap_detail_id'] ) ); elseif( isset( $arrValues['scheduled_ap_detail_id'] ) ) $this->setScheduledApDetailId( $arrValues['scheduled_ap_detail_id'] );
		if( isset( $arrValues['template_ap_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intTemplateApDetailId', trim( $arrValues['template_ap_detail_id'] ) ); elseif( isset( $arrValues['template_ap_detail_id'] ) ) $this->setTemplateApDetailId( $arrValues['template_ap_detail_id'] );
		if( isset( $arrValues['reimbursement_ap_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intReimbursementApDetailId', trim( $arrValues['reimbursement_ap_detail_id'] ) ); elseif( isset( $arrValues['reimbursement_ap_detail_id'] ) ) $this->setReimbursementApDetailId( $arrValues['reimbursement_ap_detail_id'] );
		if( isset( $arrValues['unit_of_measure_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitOfMeasureId', trim( $arrValues['unit_of_measure_id'] ) ); elseif( isset( $arrValues['unit_of_measure_id'] ) ) $this->setUnitOfMeasureId( $arrValues['unit_of_measure_id'] );
		if( isset( $arrValues['ap_catalog_item_id'] ) && $boolDirectSet ) $this->set( 'm_intApCatalogItemId', trim( $arrValues['ap_catalog_item_id'] ) ); elseif( isset( $arrValues['ap_catalog_item_id'] ) ) $this->setApCatalogItemId( $arrValues['ap_catalog_item_id'] );
		if( isset( $arrValues['ap_code_id'] ) && $boolDirectSet ) $this->set( 'm_intApCodeId', trim( $arrValues['ap_code_id'] ) ); elseif( isset( $arrValues['ap_code_id'] ) ) $this->setApCodeId( $arrValues['ap_code_id'] );
		if( isset( $arrValues['ap_payee_sub_account_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeSubAccountId', trim( $arrValues['ap_payee_sub_account_id'] ) ); elseif( isset( $arrValues['ap_payee_sub_account_id'] ) ) $this->setApPayeeSubAccountId( $arrValues['ap_payee_sub_account_id'] );
		if( isset( $arrValues['job_ap_code_id'] ) && $boolDirectSet ) $this->set( 'm_intJobApCodeId', trim( $arrValues['job_ap_code_id'] ) ); elseif( isset( $arrValues['job_ap_code_id'] ) ) $this->setJobApCodeId( $arrValues['job_ap_code_id'] );
		if( isset( $arrValues['gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intGlAccountId', trim( $arrValues['gl_account_id'] ) ); elseif( isset( $arrValues['gl_account_id'] ) ) $this->setGlAccountId( $arrValues['gl_account_id'] );
		if( isset( $arrValues['ap_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intApGlAccountId', trim( $arrValues['ap_gl_account_id'] ) ); elseif( isset( $arrValues['ap_gl_account_id'] ) ) $this->setApGlAccountId( $arrValues['ap_gl_account_id'] );
		if( isset( $arrValues['wip_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intWipGlAccountId', trim( $arrValues['wip_gl_account_id'] ) ); elseif( isset( $arrValues['wip_gl_account_id'] ) ) $this->setWipGlAccountId( $arrValues['wip_gl_account_id'] );
		if( isset( $arrValues['purchases_clearing_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intPurchasesClearingGlAccountId', trim( $arrValues['purchases_clearing_gl_account_id'] ) ); elseif( isset( $arrValues['purchases_clearing_gl_account_id'] ) ) $this->setPurchasesClearingGlAccountId( $arrValues['purchases_clearing_gl_account_id'] );
		if( isset( $arrValues['pending_reimbursements_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intPendingReimbursementsGlAccountId', trim( $arrValues['pending_reimbursements_gl_account_id'] ) ); elseif( isset( $arrValues['pending_reimbursements_gl_account_id'] ) ) $this->setPendingReimbursementsGlAccountId( $arrValues['pending_reimbursements_gl_account_id'] );
		if( isset( $arrValues['inter_co_ap_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intInterCoApGlAccountId', trim( $arrValues['inter_co_ap_gl_account_id'] ) ); elseif( isset( $arrValues['inter_co_ap_gl_account_id'] ) ) $this->setInterCoApGlAccountId( $arrValues['inter_co_ap_gl_account_id'] );
		if( isset( $arrValues['bank_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBankGlAccountId', trim( $arrValues['bank_gl_account_id'] ) ); elseif( isset( $arrValues['bank_gl_account_id'] ) ) $this->setBankGlAccountId( $arrValues['bank_gl_account_id'] );
		if( isset( $arrValues['retention_payable_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intRetentionPayableGlAccountId', trim( $arrValues['retention_payable_gl_account_id'] ) ); elseif( isset( $arrValues['retention_payable_gl_account_id'] ) ) $this->setRetentionPayableGlAccountId( $arrValues['retention_payable_gl_account_id'] );
		if( isset( $arrValues['accrual_debit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccrualDebitGlAccountId', trim( $arrValues['accrual_debit_gl_account_id'] ) ); elseif( isset( $arrValues['accrual_debit_gl_account_id'] ) ) $this->setAccrualDebitGlAccountId( $arrValues['accrual_debit_gl_account_id'] );
		if( isset( $arrValues['accrual_credit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccrualCreditGlAccountId', trim( $arrValues['accrual_credit_gl_account_id'] ) ); elseif( isset( $arrValues['accrual_credit_gl_account_id'] ) ) $this->setAccrualCreditGlAccountId( $arrValues['accrual_credit_gl_account_id'] );
		if( isset( $arrValues['cash_debit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCashDebitGlAccountId', trim( $arrValues['cash_debit_gl_account_id'] ) ); elseif( isset( $arrValues['cash_debit_gl_account_id'] ) ) $this->setCashDebitGlAccountId( $arrValues['cash_debit_gl_account_id'] );
		if( isset( $arrValues['cash_credit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCashCreditGlAccountId', trim( $arrValues['cash_credit_gl_account_id'] ) ); elseif( isset( $arrValues['cash_credit_gl_account_id'] ) ) $this->setCashCreditGlAccountId( $arrValues['cash_credit_gl_account_id'] );
		if( isset( $arrValues['inter_co_ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intInterCoArTransactionId', trim( $arrValues['inter_co_ar_transaction_id'] ) ); elseif( isset( $arrValues['inter_co_ar_transaction_id'] ) ) $this->setInterCoArTransactionId( $arrValues['inter_co_ar_transaction_id'] );
		if( isset( $arrValues['refund_ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intRefundArTransactionId', trim( $arrValues['refund_ar_transaction_id'] ) ); elseif( isset( $arrValues['refund_ar_transaction_id'] ) ) $this->setRefundArTransactionId( $arrValues['refund_ar_transaction_id'] );
		if( isset( $arrValues['asset_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intAssetTransactionId', trim( $arrValues['asset_transaction_id'] ) ); elseif( isset( $arrValues['asset_transaction_id'] ) ) $this->setAssetTransactionId( $arrValues['asset_transaction_id'] );
		if( isset( $arrValues['ap_export_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intApExportBatchId', trim( $arrValues['ap_export_batch_id'] ) ); elseif( isset( $arrValues['ap_export_batch_id'] ) ) $this->setApExportBatchId( $arrValues['ap_export_batch_id'] );
		if( isset( $arrValues['fee_id'] ) && $boolDirectSet ) $this->set( 'm_intFeeId', trim( $arrValues['fee_id'] ) ); elseif( isset( $arrValues['fee_id'] ) ) $this->setFeeId( $arrValues['fee_id'] );
		if( isset( $arrValues['period_id'] ) && $boolDirectSet ) $this->set( 'm_intPeriodId', trim( $arrValues['period_id'] ) ); elseif( isset( $arrValues['period_id'] ) ) $this->setPeriodId( $arrValues['period_id'] );
		if( isset( $arrValues['property_building_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyBuildingId', trim( $arrValues['property_building_id'] ) ); elseif( isset( $arrValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrValues['property_building_id'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['transaction_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTransactionDatetime', trim( $arrValues['transaction_datetime'] ) ); elseif( isset( $arrValues['transaction_datetime'] ) ) $this->setTransactionDatetime( $arrValues['transaction_datetime'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['quantity_ordered'] ) && $boolDirectSet ) $this->set( 'm_fltQuantityOrdered', trim( $arrValues['quantity_ordered'] ) ); elseif( isset( $arrValues['quantity_ordered'] ) ) $this->setQuantityOrdered( $arrValues['quantity_ordered'] );
		if( isset( $arrValues['rate'] ) && $boolDirectSet ) $this->set( 'm_fltRate', trim( $arrValues['rate'] ) ); elseif( isset( $arrValues['rate'] ) ) $this->setRate( $arrValues['rate'] );
		if( isset( $arrValues['subtotal_amount'] ) && $boolDirectSet ) $this->set( 'm_fltSubtotalAmount', trim( $arrValues['subtotal_amount'] ) ); elseif( isset( $arrValues['subtotal_amount'] ) ) $this->setSubtotalAmount( $arrValues['subtotal_amount'] );
		if( isset( $arrValues['tax_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTaxAmount', trim( $arrValues['tax_amount'] ) ); elseif( isset( $arrValues['tax_amount'] ) ) $this->setTaxAmount( $arrValues['tax_amount'] );
		if( isset( $arrValues['discount_amount'] ) && $boolDirectSet ) $this->set( 'm_fltDiscountAmount', trim( $arrValues['discount_amount'] ) ); elseif( isset( $arrValues['discount_amount'] ) ) $this->setDiscountAmount( $arrValues['discount_amount'] );
		if( isset( $arrValues['shipping_amount'] ) && $boolDirectSet ) $this->set( 'm_fltShippingAmount', trim( $arrValues['shipping_amount'] ) ); elseif( isset( $arrValues['shipping_amount'] ) ) $this->setShippingAmount( $arrValues['shipping_amount'] );
		if( isset( $arrValues['pre_approval_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPreApprovalAmount', trim( $arrValues['pre_approval_amount'] ) ); elseif( isset( $arrValues['pre_approval_amount'] ) ) $this->setPreApprovalAmount( $arrValues['pre_approval_amount'] );
		if( isset( $arrValues['transaction_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionAmount', trim( $arrValues['transaction_amount'] ) ); elseif( isset( $arrValues['transaction_amount'] ) ) $this->setTransactionAmount( $arrValues['transaction_amount'] );
		if( isset( $arrValues['transaction_amount_due'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionAmountDue', trim( $arrValues['transaction_amount_due'] ) ); elseif( isset( $arrValues['transaction_amount_due'] ) ) $this->setTransactionAmountDue( $arrValues['transaction_amount_due'] );
		if( isset( $arrValues['initial_amount_due'] ) && $boolDirectSet ) $this->set( 'm_fltInitialAmountDue', trim( $arrValues['initial_amount_due'] ) ); elseif( isset( $arrValues['initial_amount_due'] ) ) $this->setInitialAmountDue( $arrValues['initial_amount_due'] );
		if( isset( $arrValues['drawn_amount'] ) && $boolDirectSet ) $this->set( 'm_fltDrawnAmount', trim( $arrValues['drawn_amount'] ) ); elseif( isset( $arrValues['drawn_amount'] ) ) $this->setDrawnAmount( $arrValues['drawn_amount'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['is_confidential'] ) && $boolDirectSet ) $this->set( 'm_boolIsConfidential', trim( stripcslashes( $arrValues['is_confidential'] ) ) ); elseif( isset( $arrValues['is_confidential'] ) ) $this->setIsConfidential( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_confidential'] ) : $arrValues['is_confidential'] );
		if( isset( $arrValues['is_1099'] ) && $boolDirectSet ) $this->set( 'm_boolIs1099', trim( stripcslashes( $arrValues['is_1099'] ) ) ); elseif( isset( $arrValues['is_1099'] ) ) $this->setIs1099( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_1099'] ) : $arrValues['is_1099'] );
		if( isset( $arrValues['is_approved_for_payment'] ) && $boolDirectSet ) $this->set( 'm_boolIsApprovedForPayment', trim( stripcslashes( $arrValues['is_approved_for_payment'] ) ) ); elseif( isset( $arrValues['is_approved_for_payment'] ) ) $this->setIsApprovedForPayment( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_approved_for_payment'] ) : $arrValues['is_approved_for_payment'] );
		if( isset( $arrValues['is_deleted'] ) && $boolDirectSet ) $this->set( 'm_boolIsDeleted', trim( stripcslashes( $arrValues['is_deleted'] ) ) ); elseif( isset( $arrValues['is_deleted'] ) ) $this->setIsDeleted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_deleted'] ) : $arrValues['is_deleted'] );
		if( isset( $arrValues['is_reversed'] ) && $boolDirectSet ) $this->set( 'm_boolIsReversed', trim( stripcslashes( $arrValues['is_reversed'] ) ) ); elseif( isset( $arrValues['is_reversed'] ) ) $this->setIsReversed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_reversed'] ) : $arrValues['is_reversed'] );
		if( isset( $arrValues['payment_approved_by'] ) && $boolDirectSet ) $this->set( 'm_intPaymentApprovedBy', trim( $arrValues['payment_approved_by'] ) ); elseif( isset( $arrValues['payment_approved_by'] ) ) $this->setPaymentApprovedBy( $arrValues['payment_approved_by'] );
		if( isset( $arrValues['payment_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strPaymentApprovedOn', trim( $arrValues['payment_approved_on'] ) ); elseif( isset( $arrValues['payment_approved_on'] ) ) $this->setPaymentApprovedOn( $arrValues['payment_approved_on'] );
		if( isset( $arrValues['exported_by'] ) && $boolDirectSet ) $this->set( 'm_intExportedBy', trim( $arrValues['exported_by'] ) ); elseif( isset( $arrValues['exported_by'] ) ) $this->setExportedBy( $arrValues['exported_by'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['old_po_detail_log_id'] ) && $boolDirectSet ) $this->set( 'm_intOldPoDetailLogId', trim( $arrValues['old_po_detail_log_id'] ) ); elseif( isset( $arrValues['old_po_detail_log_id'] ) ) $this->setOldPoDetailLogId( $arrValues['old_po_detail_log_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['cc_ap_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intCcApPaymentId', trim( $arrValues['cc_ap_payment_id'] ) ); elseif( isset( $arrValues['cc_ap_payment_id'] ) ) $this->setCcApPaymentId( $arrValues['cc_ap_payment_id'] );
		if( isset( $arrValues['reimbursement_method_id'] ) && $boolDirectSet ) $this->set( 'm_intReimbursementMethodId', trim( $arrValues['reimbursement_method_id'] ) ); elseif( isset( $arrValues['reimbursement_method_id'] ) ) $this->setReimbursementMethodId( $arrValues['reimbursement_method_id'] );
		if( isset( $arrValues['gl_header_id'] ) && $boolDirectSet ) $this->set( 'm_intGlHeaderId', trim( $arrValues['gl_header_id'] ) ); elseif( isset( $arrValues['gl_header_id'] ) ) $this->setGlHeaderId( $arrValues['gl_header_id'] );
		if( isset( $arrValues['retention_amount'] ) && $boolDirectSet ) $this->set( 'm_fltRetentionAmount', trim( $arrValues['retention_amount'] ) ); elseif( isset( $arrValues['retention_amount'] ) ) $this->setRetentionAmount( $arrValues['retention_amount'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApDetailId( $intApDetailId ) {
		$this->set( 'm_intApDetailId', CStrings::strToIntDef( $intApDetailId, NULL, false ) );
	}

	public function getApDetailId() {
		return $this->m_intApDetailId;
	}

	public function sqlApDetailId() {
		return ( true == isset( $this->m_intApDetailId ) ) ? ( string ) $this->m_intApDetailId : 'NULL';
	}

	public function setApHeaderLogId( $intApHeaderLogId ) {
		$this->set( 'm_intApHeaderLogId', CStrings::strToIntDef( $intApHeaderLogId, NULL, false ) );
	}

	public function getApHeaderLogId() {
		return $this->m_intApHeaderLogId;
	}

	public function sqlApHeaderLogId() {
		return ( true == isset( $this->m_intApHeaderLogId ) ) ? ( string ) $this->m_intApHeaderLogId : 'NULL';
	}

	public function setApHeaderId( $intApHeaderId ) {
		$this->set( 'm_intApHeaderId', CStrings::strToIntDef( $intApHeaderId, NULL, false ) );
	}

	public function getApHeaderId() {
		return $this->m_intApHeaderId;
	}

	public function sqlApHeaderId() {
		return ( true == isset( $this->m_intApHeaderId ) ) ? ( string ) $this->m_intApHeaderId : 'NULL';
	}

	public function setApPhysicalStatusTypeId( $intApPhysicalStatusTypeId ) {
		$this->set( 'm_intApPhysicalStatusTypeId', CStrings::strToIntDef( $intApPhysicalStatusTypeId, NULL, false ) );
	}

	public function getApPhysicalStatusTypeId() {
		return $this->m_intApPhysicalStatusTypeId;
	}

	public function sqlApPhysicalStatusTypeId() {
		return ( true == isset( $this->m_intApPhysicalStatusTypeId ) ) ? ( string ) $this->m_intApPhysicalStatusTypeId : 'NULL';
	}

	public function setGlTransactionTypeId( $intGlTransactionTypeId ) {
		$this->set( 'm_intGlTransactionTypeId', CStrings::strToIntDef( $intGlTransactionTypeId, NULL, false ) );
	}

	public function getGlTransactionTypeId() {
		return $this->m_intGlTransactionTypeId;
	}

	public function sqlGlTransactionTypeId() {
		return ( true == isset( $this->m_intGlTransactionTypeId ) ) ? ( string ) $this->m_intGlTransactionTypeId : 'NULL';
	}

	public function setApTransactionTypeId( $intApTransactionTypeId ) {
		$this->set( 'm_intApTransactionTypeId', CStrings::strToIntDef( $intApTransactionTypeId, NULL, false ) );
	}

	public function getApTransactionTypeId() {
		return $this->m_intApTransactionTypeId;
	}

	public function sqlApTransactionTypeId() {
		return ( true == isset( $this->m_intApTransactionTypeId ) ) ? ( string ) $this->m_intApTransactionTypeId : '1';
	}

	public function setApPostTypeId( $intApPostTypeId ) {
		$this->set( 'm_intApPostTypeId', CStrings::strToIntDef( $intApPostTypeId, NULL, false ) );
	}

	public function getApPostTypeId() {
		return $this->m_intApPostTypeId;
	}

	public function sqlApPostTypeId() {
		return ( true == isset( $this->m_intApPostTypeId ) ) ? ( string ) $this->m_intApPostTypeId : '4';
	}

	public function setBankAccountId( $intBankAccountId ) {
		$this->set( 'm_intBankAccountId', CStrings::strToIntDef( $intBankAccountId, NULL, false ) );
	}

	public function getBankAccountId() {
		return $this->m_intBankAccountId;
	}

	public function sqlBankAccountId() {
		return ( true == isset( $this->m_intBankAccountId ) ) ? ( string ) $this->m_intBankAccountId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setInterCoPropertyId( $intInterCoPropertyId ) {
		$this->set( 'm_intInterCoPropertyId', CStrings::strToIntDef( $intInterCoPropertyId, NULL, false ) );
	}

	public function getInterCoPropertyId() {
		return $this->m_intInterCoPropertyId;
	}

	public function sqlInterCoPropertyId() {
		return ( true == isset( $this->m_intInterCoPropertyId ) ) ? ( string ) $this->m_intInterCoPropertyId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setApFormulaId( $intApFormulaId ) {
		$this->set( 'm_intApFormulaId', CStrings::strToIntDef( $intApFormulaId, NULL, false ) );
	}

	public function getApFormulaId() {
		return $this->m_intApFormulaId;
	}

	public function sqlApFormulaId() {
		return ( true == isset( $this->m_intApFormulaId ) ) ? ( string ) $this->m_intApFormulaId : 'NULL';
	}

	public function setCompanyDepartmentId( $intCompanyDepartmentId ) {
		$this->set( 'm_intCompanyDepartmentId', CStrings::strToIntDef( $intCompanyDepartmentId, NULL, false ) );
	}

	public function getCompanyDepartmentId() {
		return $this->m_intCompanyDepartmentId;
	}

	public function sqlCompanyDepartmentId() {
		return ( true == isset( $this->m_intCompanyDepartmentId ) ) ? ( string ) $this->m_intCompanyDepartmentId : 'NULL';
	}

	public function setGlDimensionId( $intGlDimensionId ) {
		$this->set( 'm_intGlDimensionId', CStrings::strToIntDef( $intGlDimensionId, NULL, false ) );
	}

	public function getGlDimensionId() {
		return $this->m_intGlDimensionId;
	}

	public function sqlGlDimensionId() {
		return ( true == isset( $this->m_intGlDimensionId ) ) ? ( string ) $this->m_intGlDimensionId : 'NULL';
	}

	public function setJobPhaseId( $intJobPhaseId ) {
		$this->set( 'm_intJobPhaseId', CStrings::strToIntDef( $intJobPhaseId, NULL, false ) );
	}

	public function getJobPhaseId() {
		return $this->m_intJobPhaseId;
	}

	public function sqlJobPhaseId() {
		return ( true == isset( $this->m_intJobPhaseId ) ) ? ( string ) $this->m_intJobPhaseId : 'NULL';
	}

	public function setApContractId( $intApContractId ) {
		$this->set( 'm_intApContractId', CStrings::strToIntDef( $intApContractId, NULL, false ) );
	}

	public function getApContractId() {
		return $this->m_intApContractId;
	}

	public function sqlApContractId() {
		return ( true == isset( $this->m_intApContractId ) ) ? ( string ) $this->m_intApContractId : 'NULL';
	}

	public function setBudgetApHeaderId( $intBudgetApHeaderId ) {
		$this->set( 'm_intBudgetApHeaderId', CStrings::strToIntDef( $intBudgetApHeaderId, NULL, false ) );
	}

	public function getBudgetApHeaderId() {
		return $this->m_intBudgetApHeaderId;
	}

	public function sqlBudgetApHeaderId() {
		return ( true == isset( $this->m_intBudgetApHeaderId ) ) ? ( string ) $this->m_intBudgetApHeaderId : 'NULL';
	}

	public function setApContractApDetailId( $intApContractApDetailId ) {
		$this->set( 'm_intApContractApDetailId', CStrings::strToIntDef( $intApContractApDetailId, NULL, false ) );
	}

	public function getApContractApDetailId() {
		return $this->m_intApContractApDetailId;
	}

	public function sqlApContractApDetailId() {
		return ( true == isset( $this->m_intApContractApDetailId ) ) ? ( string ) $this->m_intApContractApDetailId : 'NULL';
	}

	public function setPoApDetailId( $intPoApDetailId ) {
		$this->set( 'm_intPoApDetailId', CStrings::strToIntDef( $intPoApDetailId, NULL, false ) );
	}

	public function getPoApDetailId() {
		return $this->m_intPoApDetailId;
	}

	public function sqlPoApDetailId() {
		return ( true == isset( $this->m_intPoApDetailId ) ) ? ( string ) $this->m_intPoApDetailId : 'NULL';
	}

	public function setReversalApDetailId( $intReversalApDetailId ) {
		$this->set( 'm_intReversalApDetailId', CStrings::strToIntDef( $intReversalApDetailId, NULL, false ) );
	}

	public function getReversalApDetailId() {
		return $this->m_intReversalApDetailId;
	}

	public function sqlReversalApDetailId() {
		return ( true == isset( $this->m_intReversalApDetailId ) ) ? ( string ) $this->m_intReversalApDetailId : 'NULL';
	}

	public function setScheduledApDetailId( $intScheduledApDetailId ) {
		$this->set( 'm_intScheduledApDetailId', CStrings::strToIntDef( $intScheduledApDetailId, NULL, false ) );
	}

	public function getScheduledApDetailId() {
		return $this->m_intScheduledApDetailId;
	}

	public function sqlScheduledApDetailId() {
		return ( true == isset( $this->m_intScheduledApDetailId ) ) ? ( string ) $this->m_intScheduledApDetailId : 'NULL';
	}

	public function setTemplateApDetailId( $intTemplateApDetailId ) {
		$this->set( 'm_intTemplateApDetailId', CStrings::strToIntDef( $intTemplateApDetailId, NULL, false ) );
	}

	public function getTemplateApDetailId() {
		return $this->m_intTemplateApDetailId;
	}

	public function sqlTemplateApDetailId() {
		return ( true == isset( $this->m_intTemplateApDetailId ) ) ? ( string ) $this->m_intTemplateApDetailId : 'NULL';
	}

	public function setReimbursementApDetailId( $intReimbursementApDetailId ) {
		$this->set( 'm_intReimbursementApDetailId', CStrings::strToIntDef( $intReimbursementApDetailId, NULL, false ) );
	}

	public function getReimbursementApDetailId() {
		return $this->m_intReimbursementApDetailId;
	}

	public function sqlReimbursementApDetailId() {
		return ( true == isset( $this->m_intReimbursementApDetailId ) ) ? ( string ) $this->m_intReimbursementApDetailId : 'NULL';
	}

	public function setUnitOfMeasureId( $intUnitOfMeasureId ) {
		$this->set( 'm_intUnitOfMeasureId', CStrings::strToIntDef( $intUnitOfMeasureId, NULL, false ) );
	}

	public function getUnitOfMeasureId() {
		return $this->m_intUnitOfMeasureId;
	}

	public function sqlUnitOfMeasureId() {
		return ( true == isset( $this->m_intUnitOfMeasureId ) ) ? ( string ) $this->m_intUnitOfMeasureId : 'NULL';
	}

	public function setApCatalogItemId( $intApCatalogItemId ) {
		$this->set( 'm_intApCatalogItemId', CStrings::strToIntDef( $intApCatalogItemId, NULL, false ) );
	}

	public function getApCatalogItemId() {
		return $this->m_intApCatalogItemId;
	}

	public function sqlApCatalogItemId() {
		return ( true == isset( $this->m_intApCatalogItemId ) ) ? ( string ) $this->m_intApCatalogItemId : 'NULL';
	}

	public function setApCodeId( $intApCodeId ) {
		$this->set( 'm_intApCodeId', CStrings::strToIntDef( $intApCodeId, NULL, false ) );
	}

	public function getApCodeId() {
		return $this->m_intApCodeId;
	}

	public function sqlApCodeId() {
		return ( true == isset( $this->m_intApCodeId ) ) ? ( string ) $this->m_intApCodeId : 'NULL';
	}

	public function setApPayeeSubAccountId( $intApPayeeSubAccountId ) {
		$this->set( 'm_intApPayeeSubAccountId', CStrings::strToIntDef( $intApPayeeSubAccountId, NULL, false ) );
	}

	public function getApPayeeSubAccountId() {
		return $this->m_intApPayeeSubAccountId;
	}

	public function sqlApPayeeSubAccountId() {
		return ( true == isset( $this->m_intApPayeeSubAccountId ) ) ? ( string ) $this->m_intApPayeeSubAccountId : 'NULL';
	}

	public function setJobApCodeId( $intJobApCodeId ) {
		$this->set( 'm_intJobApCodeId', CStrings::strToIntDef( $intJobApCodeId, NULL, false ) );
	}

	public function getJobApCodeId() {
		return $this->m_intJobApCodeId;
	}

	public function sqlJobApCodeId() {
		return ( true == isset( $this->m_intJobApCodeId ) ) ? ( string ) $this->m_intJobApCodeId : 'NULL';
	}

	public function setGlAccountId( $intGlAccountId ) {
		$this->set( 'm_intGlAccountId', CStrings::strToIntDef( $intGlAccountId, NULL, false ) );
	}

	public function getGlAccountId() {
		return $this->m_intGlAccountId;
	}

	public function sqlGlAccountId() {
		return ( true == isset( $this->m_intGlAccountId ) ) ? ( string ) $this->m_intGlAccountId : 'NULL';
	}

	public function setApGlAccountId( $intApGlAccountId ) {
		$this->set( 'm_intApGlAccountId', CStrings::strToIntDef( $intApGlAccountId, NULL, false ) );
	}

	public function getApGlAccountId() {
		return $this->m_intApGlAccountId;
	}

	public function sqlApGlAccountId() {
		return ( true == isset( $this->m_intApGlAccountId ) ) ? ( string ) $this->m_intApGlAccountId : 'NULL';
	}

	public function setWipGlAccountId( $intWipGlAccountId ) {
		$this->set( 'm_intWipGlAccountId', CStrings::strToIntDef( $intWipGlAccountId, NULL, false ) );
	}

	public function getWipGlAccountId() {
		return $this->m_intWipGlAccountId;
	}

	public function sqlWipGlAccountId() {
		return ( true == isset( $this->m_intWipGlAccountId ) ) ? ( string ) $this->m_intWipGlAccountId : 'NULL';
	}

	public function setPurchasesClearingGlAccountId( $intPurchasesClearingGlAccountId ) {
		$this->set( 'm_intPurchasesClearingGlAccountId', CStrings::strToIntDef( $intPurchasesClearingGlAccountId, NULL, false ) );
	}

	public function getPurchasesClearingGlAccountId() {
		return $this->m_intPurchasesClearingGlAccountId;
	}

	public function sqlPurchasesClearingGlAccountId() {
		return ( true == isset( $this->m_intPurchasesClearingGlAccountId ) ) ? ( string ) $this->m_intPurchasesClearingGlAccountId : 'NULL';
	}

	public function setPendingReimbursementsGlAccountId( $intPendingReimbursementsGlAccountId ) {
		$this->set( 'm_intPendingReimbursementsGlAccountId', CStrings::strToIntDef( $intPendingReimbursementsGlAccountId, NULL, false ) );
	}

	public function getPendingReimbursementsGlAccountId() {
		return $this->m_intPendingReimbursementsGlAccountId;
	}

	public function sqlPendingReimbursementsGlAccountId() {
		return ( true == isset( $this->m_intPendingReimbursementsGlAccountId ) ) ? ( string ) $this->m_intPendingReimbursementsGlAccountId : 'NULL';
	}

	public function setInterCoApGlAccountId( $intInterCoApGlAccountId ) {
		$this->set( 'm_intInterCoApGlAccountId', CStrings::strToIntDef( $intInterCoApGlAccountId, NULL, false ) );
	}

	public function getInterCoApGlAccountId() {
		return $this->m_intInterCoApGlAccountId;
	}

	public function sqlInterCoApGlAccountId() {
		return ( true == isset( $this->m_intInterCoApGlAccountId ) ) ? ( string ) $this->m_intInterCoApGlAccountId : 'NULL';
	}

	public function setBankGlAccountId( $intBankGlAccountId ) {
		$this->set( 'm_intBankGlAccountId', CStrings::strToIntDef( $intBankGlAccountId, NULL, false ) );
	}

	public function getBankGlAccountId() {
		return $this->m_intBankGlAccountId;
	}

	public function sqlBankGlAccountId() {
		return ( true == isset( $this->m_intBankGlAccountId ) ) ? ( string ) $this->m_intBankGlAccountId : 'NULL';
	}

	public function setRetentionPayableGlAccountId( $intRetentionPayableGlAccountId ) {
		$this->set( 'm_intRetentionPayableGlAccountId', CStrings::strToIntDef( $intRetentionPayableGlAccountId, NULL, false ) );
	}

	public function getRetentionPayableGlAccountId() {
		return $this->m_intRetentionPayableGlAccountId;
	}

	public function sqlRetentionPayableGlAccountId() {
		return ( true == isset( $this->m_intRetentionPayableGlAccountId ) ) ? ( string ) $this->m_intRetentionPayableGlAccountId : 'NULL';
	}

	public function setAccrualDebitGlAccountId( $intAccrualDebitGlAccountId ) {
		$this->set( 'm_intAccrualDebitGlAccountId', CStrings::strToIntDef( $intAccrualDebitGlAccountId, NULL, false ) );
	}

	public function getAccrualDebitGlAccountId() {
		return $this->m_intAccrualDebitGlAccountId;
	}

	public function sqlAccrualDebitGlAccountId() {
		return ( true == isset( $this->m_intAccrualDebitGlAccountId ) ) ? ( string ) $this->m_intAccrualDebitGlAccountId : 'NULL';
	}

	public function setAccrualCreditGlAccountId( $intAccrualCreditGlAccountId ) {
		$this->set( 'm_intAccrualCreditGlAccountId', CStrings::strToIntDef( $intAccrualCreditGlAccountId, NULL, false ) );
	}

	public function getAccrualCreditGlAccountId() {
		return $this->m_intAccrualCreditGlAccountId;
	}

	public function sqlAccrualCreditGlAccountId() {
		return ( true == isset( $this->m_intAccrualCreditGlAccountId ) ) ? ( string ) $this->m_intAccrualCreditGlAccountId : 'NULL';
	}

	public function setCashDebitGlAccountId( $intCashDebitGlAccountId ) {
		$this->set( 'm_intCashDebitGlAccountId', CStrings::strToIntDef( $intCashDebitGlAccountId, NULL, false ) );
	}

	public function getCashDebitGlAccountId() {
		return $this->m_intCashDebitGlAccountId;
	}

	public function sqlCashDebitGlAccountId() {
		return ( true == isset( $this->m_intCashDebitGlAccountId ) ) ? ( string ) $this->m_intCashDebitGlAccountId : 'NULL';
	}

	public function setCashCreditGlAccountId( $intCashCreditGlAccountId ) {
		$this->set( 'm_intCashCreditGlAccountId', CStrings::strToIntDef( $intCashCreditGlAccountId, NULL, false ) );
	}

	public function getCashCreditGlAccountId() {
		return $this->m_intCashCreditGlAccountId;
	}

	public function sqlCashCreditGlAccountId() {
		return ( true == isset( $this->m_intCashCreditGlAccountId ) ) ? ( string ) $this->m_intCashCreditGlAccountId : 'NULL';
	}

	public function setInterCoArTransactionId( $intInterCoArTransactionId ) {
		$this->set( 'm_intInterCoArTransactionId', CStrings::strToIntDef( $intInterCoArTransactionId, NULL, false ) );
	}

	public function getInterCoArTransactionId() {
		return $this->m_intInterCoArTransactionId;
	}

	public function sqlInterCoArTransactionId() {
		return ( true == isset( $this->m_intInterCoArTransactionId ) ) ? ( string ) $this->m_intInterCoArTransactionId : 'NULL';
	}

	public function setRefundArTransactionId( $intRefundArTransactionId ) {
		$this->set( 'm_intRefundArTransactionId', CStrings::strToIntDef( $intRefundArTransactionId, NULL, false ) );
	}

	public function getRefundArTransactionId() {
		return $this->m_intRefundArTransactionId;
	}

	public function sqlRefundArTransactionId() {
		return ( true == isset( $this->m_intRefundArTransactionId ) ) ? ( string ) $this->m_intRefundArTransactionId : 'NULL';
	}

	public function setAssetTransactionId( $intAssetTransactionId ) {
		$this->set( 'm_intAssetTransactionId', CStrings::strToIntDef( $intAssetTransactionId, NULL, false ) );
	}

	public function getAssetTransactionId() {
		return $this->m_intAssetTransactionId;
	}

	public function sqlAssetTransactionId() {
		return ( true == isset( $this->m_intAssetTransactionId ) ) ? ( string ) $this->m_intAssetTransactionId : 'NULL';
	}

	public function setApExportBatchId( $intApExportBatchId ) {
		$this->set( 'm_intApExportBatchId', CStrings::strToIntDef( $intApExportBatchId, NULL, false ) );
	}

	public function getApExportBatchId() {
		return $this->m_intApExportBatchId;
	}

	public function sqlApExportBatchId() {
		return ( true == isset( $this->m_intApExportBatchId ) ) ? ( string ) $this->m_intApExportBatchId : 'NULL';
	}

	public function setFeeId( $intFeeId ) {
		$this->set( 'm_intFeeId', CStrings::strToIntDef( $intFeeId, NULL, false ) );
	}

	public function getFeeId() {
		return $this->m_intFeeId;
	}

	public function sqlFeeId() {
		return ( true == isset( $this->m_intFeeId ) ) ? ( string ) $this->m_intFeeId : 'NULL';
	}

	public function setPeriodId( $intPeriodId ) {
		$this->set( 'm_intPeriodId', CStrings::strToIntDef( $intPeriodId, NULL, false ) );
	}

	public function getPeriodId() {
		return $this->m_intPeriodId;
	}

	public function sqlPeriodId() {
		return ( true == isset( $this->m_intPeriodId ) ) ? ( string ) $this->m_intPeriodId : 'NULL';
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		$this->set( 'm_intPropertyBuildingId', CStrings::strToIntDef( $intPropertyBuildingId, NULL, false ) );
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function sqlPropertyBuildingId() {
		return ( true == isset( $this->m_intPropertyBuildingId ) ) ? ( string ) $this->m_intPropertyBuildingId : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setTransactionDatetime( $strTransactionDatetime ) {
		$this->set( 'm_strTransactionDatetime', CStrings::strTrimDef( $strTransactionDatetime, -1, NULL, true ) );
	}

	public function getTransactionDatetime() {
		return $this->m_strTransactionDatetime;
	}

	public function sqlTransactionDatetime() {
		return ( true == isset( $this->m_strTransactionDatetime ) ) ? '\'' . $this->m_strTransactionDatetime . '\'' : 'NOW()';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setQuantityOrdered( $fltQuantityOrdered ) {
		$this->set( 'm_fltQuantityOrdered', CStrings::strToFloatDef( $fltQuantityOrdered, NULL, false, 6 ) );
	}

	public function getQuantityOrdered() {
		return $this->m_fltQuantityOrdered;
	}

	public function sqlQuantityOrdered() {
		return ( true == isset( $this->m_fltQuantityOrdered ) ) ? ( string ) $this->m_fltQuantityOrdered : '1';
	}

	public function setRate( $fltRate ) {
		$this->set( 'm_fltRate', CStrings::strToFloatDef( $fltRate, NULL, false, 6 ) );
	}

	public function getRate() {
		return $this->m_fltRate;
	}

	public function sqlRate() {
		return ( true == isset( $this->m_fltRate ) ) ? ( string ) $this->m_fltRate : '0';
	}

	public function setSubtotalAmount( $fltSubtotalAmount ) {
		$this->set( 'm_fltSubtotalAmount', CStrings::strToFloatDef( $fltSubtotalAmount, NULL, false, 2 ) );
	}

	public function getSubtotalAmount() {
		return $this->m_fltSubtotalAmount;
	}

	public function sqlSubtotalAmount() {
		return ( true == isset( $this->m_fltSubtotalAmount ) ) ? ( string ) $this->m_fltSubtotalAmount : '( 0 )::numeric';
	}

	public function setTaxAmount( $fltTaxAmount ) {
		$this->set( 'm_fltTaxAmount', CStrings::strToFloatDef( $fltTaxAmount, NULL, false, 2 ) );
	}

	public function getTaxAmount() {
		return $this->m_fltTaxAmount;
	}

	public function sqlTaxAmount() {
		return ( true == isset( $this->m_fltTaxAmount ) ) ? ( string ) $this->m_fltTaxAmount : '( 0 )::numeric';
	}

	public function setDiscountAmount( $fltDiscountAmount ) {
		$this->set( 'm_fltDiscountAmount', CStrings::strToFloatDef( $fltDiscountAmount, NULL, false, 2 ) );
	}

	public function getDiscountAmount() {
		return $this->m_fltDiscountAmount;
	}

	public function sqlDiscountAmount() {
		return ( true == isset( $this->m_fltDiscountAmount ) ) ? ( string ) $this->m_fltDiscountAmount : '( 0 )::numeric';
	}

	public function setShippingAmount( $fltShippingAmount ) {
		$this->set( 'm_fltShippingAmount', CStrings::strToFloatDef( $fltShippingAmount, NULL, false, 2 ) );
	}

	public function getShippingAmount() {
		return $this->m_fltShippingAmount;
	}

	public function sqlShippingAmount() {
		return ( true == isset( $this->m_fltShippingAmount ) ) ? ( string ) $this->m_fltShippingAmount : '( 0 )::numeric';
	}

	public function setPreApprovalAmount( $fltPreApprovalAmount ) {
		$this->set( 'm_fltPreApprovalAmount', CStrings::strToFloatDef( $fltPreApprovalAmount, NULL, false, 2 ) );
	}

	public function getPreApprovalAmount() {
		return $this->m_fltPreApprovalAmount;
	}

	public function sqlPreApprovalAmount() {
		return ( true == isset( $this->m_fltPreApprovalAmount ) ) ? ( string ) $this->m_fltPreApprovalAmount : '0';
	}

	public function setTransactionAmount( $fltTransactionAmount ) {
		$this->set( 'm_fltTransactionAmount', CStrings::strToFloatDef( $fltTransactionAmount, NULL, false, 2 ) );
	}

	public function getTransactionAmount() {
		return $this->m_fltTransactionAmount;
	}

	public function sqlTransactionAmount() {
		return ( true == isset( $this->m_fltTransactionAmount ) ) ? ( string ) $this->m_fltTransactionAmount : '0';
	}

	public function setTransactionAmountDue( $fltTransactionAmountDue ) {
		$this->set( 'm_fltTransactionAmountDue', CStrings::strToFloatDef( $fltTransactionAmountDue, NULL, false, 2 ) );
	}

	public function getTransactionAmountDue() {
		return $this->m_fltTransactionAmountDue;
	}

	public function sqlTransactionAmountDue() {
		return ( true == isset( $this->m_fltTransactionAmountDue ) ) ? ( string ) $this->m_fltTransactionAmountDue : '0';
	}

	public function setInitialAmountDue( $fltInitialAmountDue ) {
		$this->set( 'm_fltInitialAmountDue', CStrings::strToFloatDef( $fltInitialAmountDue, NULL, false, 2 ) );
	}

	public function getInitialAmountDue() {
		return $this->m_fltInitialAmountDue;
	}

	public function sqlInitialAmountDue() {
		return ( true == isset( $this->m_fltInitialAmountDue ) ) ? ( string ) $this->m_fltInitialAmountDue : '0';
	}

	public function setDrawnAmount( $fltDrawnAmount ) {
		$this->set( 'm_fltDrawnAmount', CStrings::strToFloatDef( $fltDrawnAmount, NULL, false, 2 ) );
	}

	public function getDrawnAmount() {
		return $this->m_fltDrawnAmount;
	}

	public function sqlDrawnAmount() {
		return ( true == isset( $this->m_fltDrawnAmount ) ) ? ( string ) $this->m_fltDrawnAmount : '0';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 2000, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setIsConfidential( $boolIsConfidential ) {
		$this->set( 'm_boolIsConfidential', CStrings::strToBool( $boolIsConfidential ) );
	}

	public function getIsConfidential() {
		return $this->m_boolIsConfidential;
	}

	public function sqlIsConfidential() {
		return ( true == isset( $this->m_boolIsConfidential ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsConfidential ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIs1099( $boolIs1099 ) {
		$this->set( 'm_boolIs1099', CStrings::strToBool( $boolIs1099 ) );
	}

	public function getIs1099() {
		return $this->m_boolIs1099;
	}

	public function sqlIs1099() {
		return ( true == isset( $this->m_boolIs1099 ) ) ? '\'' . ( true == ( bool ) $this->m_boolIs1099 ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsApprovedForPayment( $boolIsApprovedForPayment ) {
		$this->set( 'm_boolIsApprovedForPayment', CStrings::strToBool( $boolIsApprovedForPayment ) );
	}

	public function getIsApprovedForPayment() {
		return $this->m_boolIsApprovedForPayment;
	}

	public function sqlIsApprovedForPayment() {
		return ( true == isset( $this->m_boolIsApprovedForPayment ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsApprovedForPayment ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDeleted( $boolIsDeleted ) {
		$this->set( 'm_boolIsDeleted', CStrings::strToBool( $boolIsDeleted ) );
	}

	public function getIsDeleted() {
		return $this->m_boolIsDeleted;
	}

	public function sqlIsDeleted() {
		return ( true == isset( $this->m_boolIsDeleted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDeleted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsReversed( $boolIsReversed ) {
		$this->set( 'm_boolIsReversed', CStrings::strToBool( $boolIsReversed ) );
	}

	public function getIsReversed() {
		return $this->m_boolIsReversed;
	}

	public function sqlIsReversed() {
		return ( true == isset( $this->m_boolIsReversed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReversed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPaymentApprovedBy( $intPaymentApprovedBy ) {
		$this->set( 'm_intPaymentApprovedBy', CStrings::strToIntDef( $intPaymentApprovedBy, NULL, false ) );
	}

	public function getPaymentApprovedBy() {
		return $this->m_intPaymentApprovedBy;
	}

	public function sqlPaymentApprovedBy() {
		return ( true == isset( $this->m_intPaymentApprovedBy ) ) ? ( string ) $this->m_intPaymentApprovedBy : 'NULL';
	}

	public function setPaymentApprovedOn( $strPaymentApprovedOn ) {
		$this->set( 'm_strPaymentApprovedOn', CStrings::strTrimDef( $strPaymentApprovedOn, -1, NULL, true ) );
	}

	public function getPaymentApprovedOn() {
		return $this->m_strPaymentApprovedOn;
	}

	public function sqlPaymentApprovedOn() {
		return ( true == isset( $this->m_strPaymentApprovedOn ) ) ? '\'' . $this->m_strPaymentApprovedOn . '\'' : 'NULL';
	}

	public function setExportedBy( $intExportedBy ) {
		$this->set( 'm_intExportedBy', CStrings::strToIntDef( $intExportedBy, NULL, false ) );
	}

	public function getExportedBy() {
		return $this->m_intExportedBy;
	}

	public function sqlExportedBy() {
		return ( true == isset( $this->m_intExportedBy ) ) ? ( string ) $this->m_intExportedBy : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setOldPoDetailLogId( $intOldPoDetailLogId ) {
		$this->set( 'm_intOldPoDetailLogId', CStrings::strToIntDef( $intOldPoDetailLogId, NULL, false ) );
	}

	public function getOldPoDetailLogId() {
		return $this->m_intOldPoDetailLogId;
	}

	public function sqlOldPoDetailLogId() {
		return ( true == isset( $this->m_intOldPoDetailLogId ) ) ? ( string ) $this->m_intOldPoDetailLogId : 'NULL';
	}

	public function setCcApPaymentId( $intCcApPaymentId ) {
		$this->set( 'm_intCcApPaymentId', CStrings::strToIntDef( $intCcApPaymentId, NULL, false ) );
	}

	public function getCcApPaymentId() {
		return $this->m_intCcApPaymentId;
	}

	public function sqlCcApPaymentId() {
		return ( true == isset( $this->m_intCcApPaymentId ) ) ? ( string ) $this->m_intCcApPaymentId : 'NULL';
	}

	public function setReimbursementMethodId( $intReimbursementMethodId ) {
		$this->set( 'm_intReimbursementMethodId', CStrings::strToIntDef( $intReimbursementMethodId, NULL, false ) );
	}

	public function getReimbursementMethodId() {
		return $this->m_intReimbursementMethodId;
	}

	public function sqlReimbursementMethodId() {
		return ( true == isset( $this->m_intReimbursementMethodId ) ) ? ( string ) $this->m_intReimbursementMethodId : '1';
	}

	public function setGlHeaderId( $intGlHeaderId ) {
		$this->set( 'm_intGlHeaderId', CStrings::strToIntDef( $intGlHeaderId, NULL, false ) );
	}

	public function getGlHeaderId() {
		return $this->m_intGlHeaderId;
	}

	public function sqlGlHeaderId() {
		return ( true == isset( $this->m_intGlHeaderId ) ) ? ( string ) $this->m_intGlHeaderId : 'NULL';
	}

	public function setRetentionAmount( $fltRetentionAmount ) {
		$this->set( 'm_fltRetentionAmount', CStrings::strToFloatDef( $fltRetentionAmount, NULL, false, 2 ) );
	}

	public function getRetentionAmount() {
		return $this->m_fltRetentionAmount;
	}

	public function sqlRetentionAmount() {
		return ( true == isset( $this->m_fltRetentionAmount ) ) ? ( string ) $this->m_fltRetentionAmount : '0';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ap_detail_id, ap_header_log_id, ap_header_id, ap_physical_status_type_id, gl_transaction_type_id, ap_transaction_type_id, ap_post_type_id, bank_account_id, property_id, inter_co_property_id, property_unit_id, ap_formula_id, company_department_id, gl_dimension_id, job_phase_id, ap_contract_id, budget_ap_header_id, ap_contract_ap_detail_id, po_ap_detail_id, reversal_ap_detail_id, scheduled_ap_detail_id, template_ap_detail_id, reimbursement_ap_detail_id, unit_of_measure_id, ap_catalog_item_id, ap_code_id, ap_payee_sub_account_id, job_ap_code_id, gl_account_id, ap_gl_account_id, wip_gl_account_id, purchases_clearing_gl_account_id, pending_reimbursements_gl_account_id, inter_co_ap_gl_account_id, bank_gl_account_id, retention_payable_gl_account_id, accrual_debit_gl_account_id, accrual_credit_gl_account_id, cash_debit_gl_account_id, cash_credit_gl_account_id, inter_co_ar_transaction_id, refund_ar_transaction_id, asset_transaction_id, ap_export_batch_id, fee_id, period_id, property_building_id, post_month, remote_primary_key, transaction_datetime, log_datetime, quantity_ordered, rate, subtotal_amount, tax_amount, discount_amount, shipping_amount, pre_approval_amount, transaction_amount, transaction_amount_due, initial_amount_due, drawn_amount, description, is_confidential, is_1099, is_approved_for_payment, is_deleted, is_reversed, payment_approved_by, payment_approved_on, exported_by, exported_on, approved_by, approved_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, old_po_detail_log_id, details, cc_ap_payment_id, reimbursement_method_id, gl_header_id, retention_amount )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlApDetailId() . ', ' .
						$this->sqlApHeaderLogId() . ', ' .
						$this->sqlApHeaderId() . ', ' .
						$this->sqlApPhysicalStatusTypeId() . ', ' .
						$this->sqlGlTransactionTypeId() . ', ' .
						$this->sqlApTransactionTypeId() . ', ' .
						$this->sqlApPostTypeId() . ', ' .
						$this->sqlBankAccountId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlInterCoPropertyId() . ', ' .
						$this->sqlPropertyUnitId() . ', ' .
						$this->sqlApFormulaId() . ', ' .
						$this->sqlCompanyDepartmentId() . ', ' .
						$this->sqlGlDimensionId() . ', ' .
						$this->sqlJobPhaseId() . ', ' .
						$this->sqlApContractId() . ', ' .
						$this->sqlBudgetApHeaderId() . ', ' .
						$this->sqlApContractApDetailId() . ', ' .
						$this->sqlPoApDetailId() . ', ' .
						$this->sqlReversalApDetailId() . ', ' .
						$this->sqlScheduledApDetailId() . ', ' .
						$this->sqlTemplateApDetailId() . ', ' .
						$this->sqlReimbursementApDetailId() . ', ' .
						$this->sqlUnitOfMeasureId() . ', ' .
						$this->sqlApCatalogItemId() . ', ' .
						$this->sqlApCodeId() . ', ' .
						$this->sqlApPayeeSubAccountId() . ', ' .
						$this->sqlJobApCodeId() . ', ' .
						$this->sqlGlAccountId() . ', ' .
						$this->sqlApGlAccountId() . ', ' .
						$this->sqlWipGlAccountId() . ', ' .
						$this->sqlPurchasesClearingGlAccountId() . ', ' .
						$this->sqlPendingReimbursementsGlAccountId() . ', ' .
						$this->sqlInterCoApGlAccountId() . ', ' .
						$this->sqlBankGlAccountId() . ', ' .
						$this->sqlRetentionPayableGlAccountId() . ', ' .
						$this->sqlAccrualDebitGlAccountId() . ', ' .
						$this->sqlAccrualCreditGlAccountId() . ', ' .
						$this->sqlCashDebitGlAccountId() . ', ' .
						$this->sqlCashCreditGlAccountId() . ', ' .
						$this->sqlInterCoArTransactionId() . ', ' .
						$this->sqlRefundArTransactionId() . ', ' .
						$this->sqlAssetTransactionId() . ', ' .
						$this->sqlApExportBatchId() . ', ' .
						$this->sqlFeeId() . ', ' .
						$this->sqlPeriodId() . ', ' .
						$this->sqlPropertyBuildingId() . ', ' .
						$this->sqlPostMonth() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlTransactionDatetime() . ', ' .
						$this->sqlLogDatetime() . ', ' .
						$this->sqlQuantityOrdered() . ', ' .
						$this->sqlRate() . ', ' .
						$this->sqlSubtotalAmount() . ', ' .
						$this->sqlTaxAmount() . ', ' .
						$this->sqlDiscountAmount() . ', ' .
						$this->sqlShippingAmount() . ', ' .
						$this->sqlPreApprovalAmount() . ', ' .
						$this->sqlTransactionAmount() . ', ' .
						$this->sqlTransactionAmountDue() . ', ' .
						$this->sqlInitialAmountDue() . ', ' .
						$this->sqlDrawnAmount() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlIsConfidential() . ', ' .
						$this->sqlIs1099() . ', ' .
						$this->sqlIsApprovedForPayment() . ', ' .
						$this->sqlIsDeleted() . ', ' .
						$this->sqlIsReversed() . ', ' .
						$this->sqlPaymentApprovedBy() . ', ' .
						$this->sqlPaymentApprovedOn() . ', ' .
						$this->sqlExportedBy() . ', ' .
						$this->sqlExportedOn() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlOldPoDetailLogId() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlCcApPaymentId() . ', ' .
						$this->sqlReimbursementMethodId() . ', ' .
						$this->sqlGlHeaderId() . ', ' .
						$this->sqlRetentionAmount() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_detail_id = ' . $this->sqlApDetailId(). ',' ; } elseif( true == array_key_exists( 'ApDetailId', $this->getChangedColumns() ) ) { $strSql .= ' ap_detail_id = ' . $this->sqlApDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_header_log_id = ' . $this->sqlApHeaderLogId(). ',' ; } elseif( true == array_key_exists( 'ApHeaderLogId', $this->getChangedColumns() ) ) { $strSql .= ' ap_header_log_id = ' . $this->sqlApHeaderLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_header_id = ' . $this->sqlApHeaderId(). ',' ; } elseif( true == array_key_exists( 'ApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' ap_header_id = ' . $this->sqlApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_physical_status_type_id = ' . $this->sqlApPhysicalStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'ApPhysicalStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_physical_status_type_id = ' . $this->sqlApPhysicalStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_transaction_type_id = ' . $this->sqlGlTransactionTypeId(). ',' ; } elseif( true == array_key_exists( 'GlTransactionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_transaction_type_id = ' . $this->sqlGlTransactionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_transaction_type_id = ' . $this->sqlApTransactionTypeId(). ',' ; } elseif( true == array_key_exists( 'ApTransactionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_transaction_type_id = ' . $this->sqlApTransactionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_post_type_id = ' . $this->sqlApPostTypeId(). ',' ; } elseif( true == array_key_exists( 'ApPostTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_post_type_id = ' . $this->sqlApPostTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_account_id = ' . $this->sqlBankAccountId(). ',' ; } elseif( true == array_key_exists( 'BankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' bank_account_id = ' . $this->sqlBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inter_co_property_id = ' . $this->sqlInterCoPropertyId(). ',' ; } elseif( true == array_key_exists( 'InterCoPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' inter_co_property_id = ' . $this->sqlInterCoPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_formula_id = ' . $this->sqlApFormulaId(). ',' ; } elseif( true == array_key_exists( 'ApFormulaId', $this->getChangedColumns() ) ) { $strSql .= ' ap_formula_id = ' . $this->sqlApFormulaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_department_id = ' . $this->sqlCompanyDepartmentId(). ',' ; } elseif( true == array_key_exists( 'CompanyDepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' company_department_id = ' . $this->sqlCompanyDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_dimension_id = ' . $this->sqlGlDimensionId(). ',' ; } elseif( true == array_key_exists( 'GlDimensionId', $this->getChangedColumns() ) ) { $strSql .= ' gl_dimension_id = ' . $this->sqlGlDimensionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_phase_id = ' . $this->sqlJobPhaseId(). ',' ; } elseif( true == array_key_exists( 'JobPhaseId', $this->getChangedColumns() ) ) { $strSql .= ' job_phase_id = ' . $this->sqlJobPhaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_contract_id = ' . $this->sqlApContractId(). ',' ; } elseif( true == array_key_exists( 'ApContractId', $this->getChangedColumns() ) ) { $strSql .= ' ap_contract_id = ' . $this->sqlApContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_ap_header_id = ' . $this->sqlBudgetApHeaderId(). ',' ; } elseif( true == array_key_exists( 'BudgetApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' budget_ap_header_id = ' . $this->sqlBudgetApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_contract_ap_detail_id = ' . $this->sqlApContractApDetailId(). ',' ; } elseif( true == array_key_exists( 'ApContractApDetailId', $this->getChangedColumns() ) ) { $strSql .= ' ap_contract_ap_detail_id = ' . $this->sqlApContractApDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' po_ap_detail_id = ' . $this->sqlPoApDetailId(). ',' ; } elseif( true == array_key_exists( 'PoApDetailId', $this->getChangedColumns() ) ) { $strSql .= ' po_ap_detail_id = ' . $this->sqlPoApDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reversal_ap_detail_id = ' . $this->sqlReversalApDetailId(). ',' ; } elseif( true == array_key_exists( 'ReversalApDetailId', $this->getChangedColumns() ) ) { $strSql .= ' reversal_ap_detail_id = ' . $this->sqlReversalApDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_ap_detail_id = ' . $this->sqlScheduledApDetailId(). ',' ; } elseif( true == array_key_exists( 'ScheduledApDetailId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_ap_detail_id = ' . $this->sqlScheduledApDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_ap_detail_id = ' . $this->sqlTemplateApDetailId(). ',' ; } elseif( true == array_key_exists( 'TemplateApDetailId', $this->getChangedColumns() ) ) { $strSql .= ' template_ap_detail_id = ' . $this->sqlTemplateApDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reimbursement_ap_detail_id = ' . $this->sqlReimbursementApDetailId(). ',' ; } elseif( true == array_key_exists( 'ReimbursementApDetailId', $this->getChangedColumns() ) ) { $strSql .= ' reimbursement_ap_detail_id = ' . $this->sqlReimbursementApDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_of_measure_id = ' . $this->sqlUnitOfMeasureId(). ',' ; } elseif( true == array_key_exists( 'UnitOfMeasureId', $this->getChangedColumns() ) ) { $strSql .= ' unit_of_measure_id = ' . $this->sqlUnitOfMeasureId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_catalog_item_id = ' . $this->sqlApCatalogItemId(). ',' ; } elseif( true == array_key_exists( 'ApCatalogItemId', $this->getChangedColumns() ) ) { $strSql .= ' ap_catalog_item_id = ' . $this->sqlApCatalogItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_code_id = ' . $this->sqlApCodeId(). ',' ; } elseif( true == array_key_exists( 'ApCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_code_id = ' . $this->sqlApCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_sub_account_id = ' . $this->sqlApPayeeSubAccountId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeSubAccountId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_sub_account_id = ' . $this->sqlApPayeeSubAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_ap_code_id = ' . $this->sqlJobApCodeId(). ',' ; } elseif( true == array_key_exists( 'JobApCodeId', $this->getChangedColumns() ) ) { $strSql .= ' job_ap_code_id = ' . $this->sqlJobApCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_account_id = ' . $this->sqlGlAccountId(). ',' ; } elseif( true == array_key_exists( 'GlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' gl_account_id = ' . $this->sqlGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_gl_account_id = ' . $this->sqlApGlAccountId(). ',' ; } elseif( true == array_key_exists( 'ApGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' ap_gl_account_id = ' . $this->sqlApGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wip_gl_account_id = ' . $this->sqlWipGlAccountId(). ',' ; } elseif( true == array_key_exists( 'WipGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' wip_gl_account_id = ' . $this->sqlWipGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchases_clearing_gl_account_id = ' . $this->sqlPurchasesClearingGlAccountId(). ',' ; } elseif( true == array_key_exists( 'PurchasesClearingGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' purchases_clearing_gl_account_id = ' . $this->sqlPurchasesClearingGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pending_reimbursements_gl_account_id = ' . $this->sqlPendingReimbursementsGlAccountId(). ',' ; } elseif( true == array_key_exists( 'PendingReimbursementsGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' pending_reimbursements_gl_account_id = ' . $this->sqlPendingReimbursementsGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inter_co_ap_gl_account_id = ' . $this->sqlInterCoApGlAccountId(). ',' ; } elseif( true == array_key_exists( 'InterCoApGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' inter_co_ap_gl_account_id = ' . $this->sqlInterCoApGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_gl_account_id = ' . $this->sqlBankGlAccountId(). ',' ; } elseif( true == array_key_exists( 'BankGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' bank_gl_account_id = ' . $this->sqlBankGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' retention_payable_gl_account_id = ' . $this->sqlRetentionPayableGlAccountId(). ',' ; } elseif( true == array_key_exists( 'RetentionPayableGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' retention_payable_gl_account_id = ' . $this->sqlRetentionPayableGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accrual_debit_gl_account_id = ' . $this->sqlAccrualDebitGlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccrualDebitGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' accrual_debit_gl_account_id = ' . $this->sqlAccrualDebitGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accrual_credit_gl_account_id = ' . $this->sqlAccrualCreditGlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccrualCreditGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' accrual_credit_gl_account_id = ' . $this->sqlAccrualCreditGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cash_debit_gl_account_id = ' . $this->sqlCashDebitGlAccountId(). ',' ; } elseif( true == array_key_exists( 'CashDebitGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' cash_debit_gl_account_id = ' . $this->sqlCashDebitGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cash_credit_gl_account_id = ' . $this->sqlCashCreditGlAccountId(). ',' ; } elseif( true == array_key_exists( 'CashCreditGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' cash_credit_gl_account_id = ' . $this->sqlCashCreditGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inter_co_ar_transaction_id = ' . $this->sqlInterCoArTransactionId(). ',' ; } elseif( true == array_key_exists( 'InterCoArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' inter_co_ar_transaction_id = ' . $this->sqlInterCoArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' refund_ar_transaction_id = ' . $this->sqlRefundArTransactionId(). ',' ; } elseif( true == array_key_exists( 'RefundArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' refund_ar_transaction_id = ' . $this->sqlRefundArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' asset_transaction_id = ' . $this->sqlAssetTransactionId(). ',' ; } elseif( true == array_key_exists( 'AssetTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' asset_transaction_id = ' . $this->sqlAssetTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_export_batch_id = ' . $this->sqlApExportBatchId(). ',' ; } elseif( true == array_key_exists( 'ApExportBatchId', $this->getChangedColumns() ) ) { $strSql .= ' ap_export_batch_id = ' . $this->sqlApExportBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fee_id = ' . $this->sqlFeeId(). ',' ; } elseif( true == array_key_exists( 'FeeId', $this->getChangedColumns() ) ) { $strSql .= ' fee_id = ' . $this->sqlFeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' period_id = ' . $this->sqlPeriodId(). ',' ; } elseif( true == array_key_exists( 'PeriodId', $this->getChangedColumns() ) ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId(). ',' ; } elseif( true == array_key_exists( 'PropertyBuildingId', $this->getChangedColumns() ) ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth(). ',' ; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime(). ',' ; } elseif( true == array_key_exists( 'TransactionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime(). ',' ; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quantity_ordered = ' . $this->sqlQuantityOrdered(). ',' ; } elseif( true == array_key_exists( 'QuantityOrdered', $this->getChangedColumns() ) ) { $strSql .= ' quantity_ordered = ' . $this->sqlQuantityOrdered() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate = ' . $this->sqlRate(). ',' ; } elseif( true == array_key_exists( 'Rate', $this->getChangedColumns() ) ) { $strSql .= ' rate = ' . $this->sqlRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subtotal_amount = ' . $this->sqlSubtotalAmount(). ',' ; } elseif( true == array_key_exists( 'SubtotalAmount', $this->getChangedColumns() ) ) { $strSql .= ' subtotal_amount = ' . $this->sqlSubtotalAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_amount = ' . $this->sqlTaxAmount(). ',' ; } elseif( true == array_key_exists( 'TaxAmount', $this->getChangedColumns() ) ) { $strSql .= ' tax_amount = ' . $this->sqlTaxAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' discount_amount = ' . $this->sqlDiscountAmount(). ',' ; } elseif( true == array_key_exists( 'DiscountAmount', $this->getChangedColumns() ) ) { $strSql .= ' discount_amount = ' . $this->sqlDiscountAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' shipping_amount = ' . $this->sqlShippingAmount(). ',' ; } elseif( true == array_key_exists( 'ShippingAmount', $this->getChangedColumns() ) ) { $strSql .= ' shipping_amount = ' . $this->sqlShippingAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pre_approval_amount = ' . $this->sqlPreApprovalAmount(). ',' ; } elseif( true == array_key_exists( 'PreApprovalAmount', $this->getChangedColumns() ) ) { $strSql .= ' pre_approval_amount = ' . $this->sqlPreApprovalAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount(). ',' ; } elseif( true == array_key_exists( 'TransactionAmount', $this->getChangedColumns() ) ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_amount_due = ' . $this->sqlTransactionAmountDue(). ',' ; } elseif( true == array_key_exists( 'TransactionAmountDue', $this->getChangedColumns() ) ) { $strSql .= ' transaction_amount_due = ' . $this->sqlTransactionAmountDue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initial_amount_due = ' . $this->sqlInitialAmountDue(). ',' ; } elseif( true == array_key_exists( 'InitialAmountDue', $this->getChangedColumns() ) ) { $strSql .= ' initial_amount_due = ' . $this->sqlInitialAmountDue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' drawn_amount = ' . $this->sqlDrawnAmount(). ',' ; } elseif( true == array_key_exists( 'DrawnAmount', $this->getChangedColumns() ) ) { $strSql .= ' drawn_amount = ' . $this->sqlDrawnAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_confidential = ' . $this->sqlIsConfidential(). ',' ; } elseif( true == array_key_exists( 'IsConfidential', $this->getChangedColumns() ) ) { $strSql .= ' is_confidential = ' . $this->sqlIsConfidential() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_1099 = ' . $this->sqlIs1099(). ',' ; } elseif( true == array_key_exists( 'Is1099', $this->getChangedColumns() ) ) { $strSql .= ' is_1099 = ' . $this->sqlIs1099() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_approved_for_payment = ' . $this->sqlIsApprovedForPayment(). ',' ; } elseif( true == array_key_exists( 'IsApprovedForPayment', $this->getChangedColumns() ) ) { $strSql .= ' is_approved_for_payment = ' . $this->sqlIsApprovedForPayment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted(). ',' ; } elseif( true == array_key_exists( 'IsDeleted', $this->getChangedColumns() ) ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reversed = ' . $this->sqlIsReversed(). ',' ; } elseif( true == array_key_exists( 'IsReversed', $this->getChangedColumns() ) ) { $strSql .= ' is_reversed = ' . $this->sqlIsReversed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_approved_by = ' . $this->sqlPaymentApprovedBy(). ',' ; } elseif( true == array_key_exists( 'PaymentApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' payment_approved_by = ' . $this->sqlPaymentApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_approved_on = ' . $this->sqlPaymentApprovedOn(). ',' ; } elseif( true == array_key_exists( 'PaymentApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' payment_approved_on = ' . $this->sqlPaymentApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy(). ',' ; } elseif( true == array_key_exists( 'ExportedBy', $this->getChangedColumns() ) ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn(). ',' ; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_po_detail_log_id = ' . $this->sqlOldPoDetailLogId(). ',' ; } elseif( true == array_key_exists( 'OldPoDetailLogId', $this->getChangedColumns() ) ) { $strSql .= ' old_po_detail_log_id = ' . $this->sqlOldPoDetailLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_ap_payment_id = ' . $this->sqlCcApPaymentId(). ',' ; } elseif( true == array_key_exists( 'CcApPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' cc_ap_payment_id = ' . $this->sqlCcApPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reimbursement_method_id = ' . $this->sqlReimbursementMethodId(). ',' ; } elseif( true == array_key_exists( 'ReimbursementMethodId', $this->getChangedColumns() ) ) { $strSql .= ' reimbursement_method_id = ' . $this->sqlReimbursementMethodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_header_id = ' . $this->sqlGlHeaderId(). ',' ; } elseif( true == array_key_exists( 'GlHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' gl_header_id = ' . $this->sqlGlHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' retention_amount = ' . $this->sqlRetentionAmount(). ',' ; } elseif( true == array_key_exists( 'RetentionAmount', $this->getChangedColumns() ) ) { $strSql .= ' retention_amount = ' . $this->sqlRetentionAmount() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ap_detail_id' => $this->getApDetailId(),
			'ap_header_log_id' => $this->getApHeaderLogId(),
			'ap_header_id' => $this->getApHeaderId(),
			'ap_physical_status_type_id' => $this->getApPhysicalStatusTypeId(),
			'gl_transaction_type_id' => $this->getGlTransactionTypeId(),
			'ap_transaction_type_id' => $this->getApTransactionTypeId(),
			'ap_post_type_id' => $this->getApPostTypeId(),
			'bank_account_id' => $this->getBankAccountId(),
			'property_id' => $this->getPropertyId(),
			'inter_co_property_id' => $this->getInterCoPropertyId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'ap_formula_id' => $this->getApFormulaId(),
			'company_department_id' => $this->getCompanyDepartmentId(),
			'gl_dimension_id' => $this->getGlDimensionId(),
			'job_phase_id' => $this->getJobPhaseId(),
			'ap_contract_id' => $this->getApContractId(),
			'budget_ap_header_id' => $this->getBudgetApHeaderId(),
			'ap_contract_ap_detail_id' => $this->getApContractApDetailId(),
			'po_ap_detail_id' => $this->getPoApDetailId(),
			'reversal_ap_detail_id' => $this->getReversalApDetailId(),
			'scheduled_ap_detail_id' => $this->getScheduledApDetailId(),
			'template_ap_detail_id' => $this->getTemplateApDetailId(),
			'reimbursement_ap_detail_id' => $this->getReimbursementApDetailId(),
			'unit_of_measure_id' => $this->getUnitOfMeasureId(),
			'ap_catalog_item_id' => $this->getApCatalogItemId(),
			'ap_code_id' => $this->getApCodeId(),
			'ap_payee_sub_account_id' => $this->getApPayeeSubAccountId(),
			'job_ap_code_id' => $this->getJobApCodeId(),
			'gl_account_id' => $this->getGlAccountId(),
			'ap_gl_account_id' => $this->getApGlAccountId(),
			'wip_gl_account_id' => $this->getWipGlAccountId(),
			'purchases_clearing_gl_account_id' => $this->getPurchasesClearingGlAccountId(),
			'pending_reimbursements_gl_account_id' => $this->getPendingReimbursementsGlAccountId(),
			'inter_co_ap_gl_account_id' => $this->getInterCoApGlAccountId(),
			'bank_gl_account_id' => $this->getBankGlAccountId(),
			'retention_payable_gl_account_id' => $this->getRetentionPayableGlAccountId(),
			'accrual_debit_gl_account_id' => $this->getAccrualDebitGlAccountId(),
			'accrual_credit_gl_account_id' => $this->getAccrualCreditGlAccountId(),
			'cash_debit_gl_account_id' => $this->getCashDebitGlAccountId(),
			'cash_credit_gl_account_id' => $this->getCashCreditGlAccountId(),
			'inter_co_ar_transaction_id' => $this->getInterCoArTransactionId(),
			'refund_ar_transaction_id' => $this->getRefundArTransactionId(),
			'asset_transaction_id' => $this->getAssetTransactionId(),
			'ap_export_batch_id' => $this->getApExportBatchId(),
			'fee_id' => $this->getFeeId(),
			'period_id' => $this->getPeriodId(),
			'property_building_id' => $this->getPropertyBuildingId(),
			'post_month' => $this->getPostMonth(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'transaction_datetime' => $this->getTransactionDatetime(),
			'log_datetime' => $this->getLogDatetime(),
			'quantity_ordered' => $this->getQuantityOrdered(),
			'rate' => $this->getRate(),
			'subtotal_amount' => $this->getSubtotalAmount(),
			'tax_amount' => $this->getTaxAmount(),
			'discount_amount' => $this->getDiscountAmount(),
			'shipping_amount' => $this->getShippingAmount(),
			'pre_approval_amount' => $this->getPreApprovalAmount(),
			'transaction_amount' => $this->getTransactionAmount(),
			'transaction_amount_due' => $this->getTransactionAmountDue(),
			'initial_amount_due' => $this->getInitialAmountDue(),
			'drawn_amount' => $this->getDrawnAmount(),
			'description' => $this->getDescription(),
			'is_confidential' => $this->getIsConfidential(),
			'is_1099' => $this->getIs1099(),
			'is_approved_for_payment' => $this->getIsApprovedForPayment(),
			'is_deleted' => $this->getIsDeleted(),
			'is_reversed' => $this->getIsReversed(),
			'payment_approved_by' => $this->getPaymentApprovedBy(),
			'payment_approved_on' => $this->getPaymentApprovedOn(),
			'exported_by' => $this->getExportedBy(),
			'exported_on' => $this->getExportedOn(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'old_po_detail_log_id' => $this->getOldPoDetailLogId(),
			'details' => $this->getDetails(),
			'cc_ap_payment_id' => $this->getCcApPaymentId(),
			'reimbursement_method_id' => $this->getReimbursementMethodId(),
			'gl_header_id' => $this->getGlHeaderId(),
			'retention_amount' => $this->getRetentionAmount()
		);
	}

}
?>