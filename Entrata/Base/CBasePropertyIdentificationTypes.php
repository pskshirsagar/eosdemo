<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyIdentificationTypes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyIdentificationTypes extends CEosPluralBase {

	/**
	 * @return CPropertyIdentificationType[]
	 */
	public static function fetchPropertyIdentificationTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyIdentificationType', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyIdentificationType
	 */
	public static function fetchPropertyIdentificationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyIdentificationType', $objDatabase );
	}

	public static function fetchPropertyIdentificationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_identification_types', $objDatabase );
	}

	public static function fetchPropertyIdentificationTypeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyIdentificationType( sprintf( 'SELECT * FROM property_identification_types WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyIdentificationTypesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyIdentificationTypes( sprintf( 'SELECT * FROM property_identification_types WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyIdentificationTypesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyIdentificationTypes( sprintf( 'SELECT * FROM property_identification_types WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyIdentificationTypesByCompanyIdentificationTypeIdByCid( $intCompanyIdentificationTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyIdentificationTypes( sprintf( 'SELECT * FROM property_identification_types WHERE company_identification_type_id = %d AND cid = %d', ( int ) $intCompanyIdentificationTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>