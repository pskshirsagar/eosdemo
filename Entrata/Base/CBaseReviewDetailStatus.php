<?php

class CBaseReviewDetailStatus extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.review_detail_statuses';

	protected $m_intId;
	protected $m_strStatusName;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['status_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStatusName', trim( stripcslashes( $arrValues['status_name'] ) ) ); elseif( isset( $arrValues['status_name'] ) ) $this->setStatusName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['status_name'] ) : $arrValues['status_name'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setStatusName( $strStatusName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strStatusName', CStrings::strTrimDef( $strStatusName, 255, NULL, true ), $strLocaleCode );
	}

	public function getStatusName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strStatusName', $strLocaleCode );
	}

	public function sqlStatusName() {
		return ( true == isset( $this->m_strStatusName ) ) ? '\'' . addslashes( $this->m_strStatusName ) . '\'' : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'status_name' => $this->getStatusName(),
			'details' => $this->getDetails()
		);
	}

}
?>