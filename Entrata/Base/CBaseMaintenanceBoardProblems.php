<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceBoardProblems
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceBoardProblems extends CEosPluralBase {

	/**
	 * @return CMaintenanceBoardProblem[]
	 */
	public static function fetchMaintenanceBoardProblems( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CMaintenanceBoardProblem', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenanceBoardProblem
	 */
	public static function fetchMaintenanceBoardProblem( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMaintenanceBoardProblem', $objDatabase );
	}

	public static function fetchMaintenanceBoardProblemCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_board_problems', $objDatabase );
	}

	public static function fetchMaintenanceBoardProblemByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceBoardProblem( sprintf( 'SELECT * FROM maintenance_board_problems WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceBoardProblemsByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenanceBoardProblems( sprintf( 'SELECT * FROM maintenance_board_problems WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceBoardProblemsByMaintenanceBoardIdByCid( $intMaintenanceBoardId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceBoardProblems( sprintf( 'SELECT * FROM maintenance_board_problems WHERE maintenance_board_id = %d AND cid = %d', ( int ) $intMaintenanceBoardId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceBoardProblemsByMaintenanceProblemIdByCid( $intMaintenanceProblemId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceBoardProblems( sprintf( 'SELECT * FROM maintenance_board_problems WHERE maintenance_problem_id = %d AND cid = %d', ( int ) $intMaintenanceProblemId, ( int ) $intCid ), $objDatabase );
	}

}
?>