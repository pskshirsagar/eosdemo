<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetTabGlAccountItems
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetTabGlAccountItems extends CEosPluralBase {

	/**
	 * @return CBudgetTabGlAccountItem[]
	 */
	public static function fetchBudgetTabGlAccountItems( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CBudgetTabGlAccountItem::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBudgetTabGlAccountItem
	 */
	public static function fetchBudgetTabGlAccountItem( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetTabGlAccountItem::class, $objDatabase );
	}

	public static function fetchBudgetTabGlAccountItemCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_tab_gl_account_items', $objDatabase );
	}

	public static function fetchBudgetTabGlAccountItemByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBudgetTabGlAccountItem( sprintf( 'SELECT * FROM budget_tab_gl_account_items WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetTabGlAccountItemsByCid( $intCid, $objDatabase ) {
		return self::fetchBudgetTabGlAccountItems( sprintf( 'SELECT * FROM budget_tab_gl_account_items WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchBudgetTabGlAccountItemsByBudgetIdByCid( $intBudgetId, $intCid, $objDatabase ) {
		return self::fetchBudgetTabGlAccountItems( sprintf( 'SELECT * FROM budget_tab_gl_account_items WHERE budget_id = %d AND cid = %d', $intBudgetId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetTabGlAccountItemsByBudgetTabGlAccountIdByCid( $intBudgetTabGlAccountId, $intCid, $objDatabase ) {
		return self::fetchBudgetTabGlAccountItems( sprintf( 'SELECT * FROM budget_tab_gl_account_items WHERE budget_tab_gl_account_id = %d AND cid = %d', $intBudgetTabGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetTabGlAccountItemsByBudgetDataSourceTypeIdByCid( $intBudgetDataSourceTypeId, $intCid, $objDatabase ) {
		return self::fetchBudgetTabGlAccountItems( sprintf( 'SELECT * FROM budget_tab_gl_account_items WHERE budget_data_source_type_id = %d AND cid = %d', $intBudgetDataSourceTypeId, $intCid ), $objDatabase );
	}

}
?>