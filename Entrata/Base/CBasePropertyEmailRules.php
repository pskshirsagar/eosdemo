<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyEmailRules
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyEmailRules extends CEosPluralBase {

	/**
	 * @return CPropertyEmailRule[]
	 */
	public static function fetchPropertyEmailRules( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPropertyEmailRule::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyEmailRule
	 */
	public static function fetchPropertyEmailRule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyEmailRule::class, $objDatabase );
	}

	public static function fetchPropertyEmailRuleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_email_rules', $objDatabase );
	}

	public static function fetchPropertyEmailRuleByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyEmailRule( sprintf( 'SELECT * FROM property_email_rules WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyEmailRulesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyEmailRules( sprintf( 'SELECT * FROM property_email_rules WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchPropertyEmailRulesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyEmailRules( sprintf( 'SELECT * FROM property_email_rules WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyEmailRulesBySystemMessageIdByCid( $intSystemMessageId, $intCid, $objDatabase ) {
		return self::fetchPropertyEmailRules( sprintf( 'SELECT * FROM property_email_rules WHERE system_message_id = %d AND cid = %d', $intSystemMessageId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyEmailRulesBySystemEmailTypeIdByCid( $intSystemEmailTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyEmailRules( sprintf( 'SELECT * FROM property_email_rules WHERE system_email_type_id = %d AND cid = %d', $intSystemEmailTypeId, $intCid ), $objDatabase );
	}

}
?>