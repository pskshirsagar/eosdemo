<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerMessages
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerMessages extends CEosPluralBase {

	/**
	 * @return CCustomerMessage[]
	 */
	public static function fetchCustomerMessages( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerMessage', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerMessage
	 */
	public static function fetchCustomerMessage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerMessage', $objDatabase );
	}

	public static function fetchCustomerMessageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_messages', $objDatabase );
	}

	public static function fetchCustomerMessageByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerMessage( sprintf( 'SELECT * FROM customer_messages WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMessagesByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerMessages( sprintf( 'SELECT * FROM customer_messages WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMessagesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCustomerMessages( sprintf( 'SELECT * FROM customer_messages WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMessagesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerMessages( sprintf( 'SELECT * FROM customer_messages WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMessagesBySenderIdByCid( $intSenderId, $intCid, $objDatabase ) {
		return self::fetchCustomerMessages( sprintf( 'SELECT * FROM customer_messages WHERE sender_id = %d AND cid = %d', ( int ) $intSenderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMessagesBySenderTypeIdByCid( $intSenderTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerMessages( sprintf( 'SELECT * FROM customer_messages WHERE sender_type_id = %d AND cid = %d', ( int ) $intSenderTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerMessagesBySystemEmailIdByCid( $intSystemEmailId, $intCid, $objDatabase ) {
		return self::fetchCustomerMessages( sprintf( 'SELECT * FROM customer_messages WHERE system_email_id = %d AND cid = %d', ( int ) $intSystemEmailId, ( int ) $intCid ), $objDatabase );
	}

}
?>