<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyEmailAddresses
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyEmailAddresses extends CEosPluralBase {

	/**
	 * @return CPropertyEmailAddress[]
	 */
	public static function fetchPropertyEmailAddresses( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPropertyEmailAddress::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyEmailAddress
	 */
	public static function fetchPropertyEmailAddress( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyEmailAddress::class, $objDatabase );
	}

	public static function fetchPropertyEmailAddressCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_email_addresses', $objDatabase );
	}

	public static function fetchPropertyEmailAddressByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyEmailAddress( sprintf( 'SELECT * FROM property_email_addresses WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyEmailAddressesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyEmailAddresses( sprintf( 'SELECT * FROM property_email_addresses WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchPropertyEmailAddressesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyEmailAddresses( sprintf( 'SELECT * FROM property_email_addresses WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyEmailAddressesByEmailAddressTypeIdByCid( $intEmailAddressTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyEmailAddresses( sprintf( 'SELECT * FROM property_email_addresses WHERE email_address_type_id = %d AND cid = %d', $intEmailAddressTypeId, $intCid ), $objDatabase );
	}

}
?>