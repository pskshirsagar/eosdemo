<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyGroupPropertyGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyGroupPropertyGroups extends CEosPluralBase {

	/**
	 * @return CCompanyGroupPropertyGroup[]
	 */
	public static function fetchCompanyGroupPropertyGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyGroupPropertyGroup', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyGroupPropertyGroup
	 */
	public static function fetchCompanyGroupPropertyGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyGroupPropertyGroup', $objDatabase );
	}

	public static function fetchCompanyGroupPropertyGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_group_property_groups', $objDatabase );
	}

	public static function fetchCompanyGroupPropertyGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyGroupPropertyGroup( sprintf( 'SELECT * FROM company_group_property_groups WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyGroupPropertyGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyGroupPropertyGroups( sprintf( 'SELECT * FROM company_group_property_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyGroupPropertyGroupsByCompanyGroupIdByCid( $intCompanyGroupId, $intCid, $objDatabase ) {
		return self::fetchCompanyGroupPropertyGroups( sprintf( 'SELECT * FROM company_group_property_groups WHERE company_group_id = %d AND cid = %d', ( int ) $intCompanyGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyGroupPropertyGroupsByPropertyGroupIdByCid( $intPropertyGroupId, $intCid, $objDatabase ) {
		return self::fetchCompanyGroupPropertyGroups( sprintf( 'SELECT * FROM company_group_property_groups WHERE property_group_id = %d AND cid = %d', ( int ) $intPropertyGroupId, ( int ) $intCid ), $objDatabase );
	}

}
?>