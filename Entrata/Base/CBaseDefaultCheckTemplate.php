<?php

class CBaseDefaultCheckTemplate extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_check_templates';

	protected $m_intId;
	protected $m_strSystemCode;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strCustomText;
	protected $m_fltTopMargin;
	protected $m_fltLeftMargin;
	protected $m_intPrintStubCheckNumber;
	protected $m_intPrintStubBankInfo;
	protected $m_intCenterCompanyInfo;
	protected $m_intCenterBankInfo;
	protected $m_intThreeChecksPerPage;
	protected $m_intIsAboveStubs;
	protected $m_intIsPrePrinted;
	protected $m_intIsDefault;
	protected $m_intIsSystem;
	protected $m_intIsDisabled;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intPrintStubCheckNumber = '0';
		$this->m_intPrintStubBankInfo = '0';
		$this->m_intCenterCompanyInfo = '0';
		$this->m_intCenterBankInfo = '0';
		$this->m_intThreeChecksPerPage = '0';
		$this->m_intIsAboveStubs = '0';
		$this->m_intIsPrePrinted = '0';
		$this->m_intIsDefault = '0';
		$this->m_intIsSystem = '0';
		$this->m_intIsDisabled = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['system_code'] ) && $boolDirectSet ) $this->set( 'm_strSystemCode', trim( stripcslashes( $arrValues['system_code'] ) ) ); elseif( isset( $arrValues['system_code'] ) ) $this->setSystemCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['system_code'] ) : $arrValues['system_code'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['custom_text'] ) && $boolDirectSet ) $this->set( 'm_strCustomText', trim( stripcslashes( $arrValues['custom_text'] ) ) ); elseif( isset( $arrValues['custom_text'] ) ) $this->setCustomText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['custom_text'] ) : $arrValues['custom_text'] );
		if( isset( $arrValues['top_margin'] ) && $boolDirectSet ) $this->set( 'm_fltTopMargin', trim( $arrValues['top_margin'] ) ); elseif( isset( $arrValues['top_margin'] ) ) $this->setTopMargin( $arrValues['top_margin'] );
		if( isset( $arrValues['left_margin'] ) && $boolDirectSet ) $this->set( 'm_fltLeftMargin', trim( $arrValues['left_margin'] ) ); elseif( isset( $arrValues['left_margin'] ) ) $this->setLeftMargin( $arrValues['left_margin'] );
		if( isset( $arrValues['print_stub_check_number'] ) && $boolDirectSet ) $this->set( 'm_intPrintStubCheckNumber', trim( $arrValues['print_stub_check_number'] ) ); elseif( isset( $arrValues['print_stub_check_number'] ) ) $this->setPrintStubCheckNumber( $arrValues['print_stub_check_number'] );
		if( isset( $arrValues['print_stub_bank_info'] ) && $boolDirectSet ) $this->set( 'm_intPrintStubBankInfo', trim( $arrValues['print_stub_bank_info'] ) ); elseif( isset( $arrValues['print_stub_bank_info'] ) ) $this->setPrintStubBankInfo( $arrValues['print_stub_bank_info'] );
		if( isset( $arrValues['center_company_info'] ) && $boolDirectSet ) $this->set( 'm_intCenterCompanyInfo', trim( $arrValues['center_company_info'] ) ); elseif( isset( $arrValues['center_company_info'] ) ) $this->setCenterCompanyInfo( $arrValues['center_company_info'] );
		if( isset( $arrValues['center_bank_info'] ) && $boolDirectSet ) $this->set( 'm_intCenterBankInfo', trim( $arrValues['center_bank_info'] ) ); elseif( isset( $arrValues['center_bank_info'] ) ) $this->setCenterBankInfo( $arrValues['center_bank_info'] );
		if( isset( $arrValues['three_checks_per_page'] ) && $boolDirectSet ) $this->set( 'm_intThreeChecksPerPage', trim( $arrValues['three_checks_per_page'] ) ); elseif( isset( $arrValues['three_checks_per_page'] ) ) $this->setThreeChecksPerPage( $arrValues['three_checks_per_page'] );
		if( isset( $arrValues['is_above_stubs'] ) && $boolDirectSet ) $this->set( 'm_intIsAboveStubs', trim( $arrValues['is_above_stubs'] ) ); elseif( isset( $arrValues['is_above_stubs'] ) ) $this->setIsAboveStubs( $arrValues['is_above_stubs'] );
		if( isset( $arrValues['is_pre_printed'] ) && $boolDirectSet ) $this->set( 'm_intIsPrePrinted', trim( $arrValues['is_pre_printed'] ) ); elseif( isset( $arrValues['is_pre_printed'] ) ) $this->setIsPrePrinted( $arrValues['is_pre_printed'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_intIsDefault', trim( $arrValues['is_default'] ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( $arrValues['is_default'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSystemCode( $strSystemCode ) {
		$this->set( 'm_strSystemCode', CStrings::strTrimDef( $strSystemCode, 10, NULL, true ) );
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	public function sqlSystemCode() {
		return ( true == isset( $this->m_strSystemCode ) ) ? '\'' . addslashes( $this->m_strSystemCode ) . '\'' : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setCustomText( $strCustomText ) {
		$this->set( 'm_strCustomText', CStrings::strTrimDef( $strCustomText, 40, NULL, true ) );
	}

	public function getCustomText() {
		return $this->m_strCustomText;
	}

	public function sqlCustomText() {
		return ( true == isset( $this->m_strCustomText ) ) ? '\'' . addslashes( $this->m_strCustomText ) . '\'' : 'NULL';
	}

	public function setTopMargin( $fltTopMargin ) {
		$this->set( 'm_fltTopMargin', CStrings::strToFloatDef( $fltTopMargin, NULL, false, 4 ) );
	}

	public function getTopMargin() {
		return $this->m_fltTopMargin;
	}

	public function sqlTopMargin() {
		return ( true == isset( $this->m_fltTopMargin ) ) ? ( string ) $this->m_fltTopMargin : 'NULL';
	}

	public function setLeftMargin( $fltLeftMargin ) {
		$this->set( 'm_fltLeftMargin', CStrings::strToFloatDef( $fltLeftMargin, NULL, false, 4 ) );
	}

	public function getLeftMargin() {
		return $this->m_fltLeftMargin;
	}

	public function sqlLeftMargin() {
		return ( true == isset( $this->m_fltLeftMargin ) ) ? ( string ) $this->m_fltLeftMargin : 'NULL';
	}

	public function setPrintStubCheckNumber( $intPrintStubCheckNumber ) {
		$this->set( 'm_intPrintStubCheckNumber', CStrings::strToIntDef( $intPrintStubCheckNumber, NULL, false ) );
	}

	public function getPrintStubCheckNumber() {
		return $this->m_intPrintStubCheckNumber;
	}

	public function sqlPrintStubCheckNumber() {
		return ( true == isset( $this->m_intPrintStubCheckNumber ) ) ? ( string ) $this->m_intPrintStubCheckNumber : '0';
	}

	public function setPrintStubBankInfo( $intPrintStubBankInfo ) {
		$this->set( 'm_intPrintStubBankInfo', CStrings::strToIntDef( $intPrintStubBankInfo, NULL, false ) );
	}

	public function getPrintStubBankInfo() {
		return $this->m_intPrintStubBankInfo;
	}

	public function sqlPrintStubBankInfo() {
		return ( true == isset( $this->m_intPrintStubBankInfo ) ) ? ( string ) $this->m_intPrintStubBankInfo : '0';
	}

	public function setCenterCompanyInfo( $intCenterCompanyInfo ) {
		$this->set( 'm_intCenterCompanyInfo', CStrings::strToIntDef( $intCenterCompanyInfo, NULL, false ) );
	}

	public function getCenterCompanyInfo() {
		return $this->m_intCenterCompanyInfo;
	}

	public function sqlCenterCompanyInfo() {
		return ( true == isset( $this->m_intCenterCompanyInfo ) ) ? ( string ) $this->m_intCenterCompanyInfo : '0';
	}

	public function setCenterBankInfo( $intCenterBankInfo ) {
		$this->set( 'm_intCenterBankInfo', CStrings::strToIntDef( $intCenterBankInfo, NULL, false ) );
	}

	public function getCenterBankInfo() {
		return $this->m_intCenterBankInfo;
	}

	public function sqlCenterBankInfo() {
		return ( true == isset( $this->m_intCenterBankInfo ) ) ? ( string ) $this->m_intCenterBankInfo : '0';
	}

	public function setThreeChecksPerPage( $intThreeChecksPerPage ) {
		$this->set( 'm_intThreeChecksPerPage', CStrings::strToIntDef( $intThreeChecksPerPage, NULL, false ) );
	}

	public function getThreeChecksPerPage() {
		return $this->m_intThreeChecksPerPage;
	}

	public function sqlThreeChecksPerPage() {
		return ( true == isset( $this->m_intThreeChecksPerPage ) ) ? ( string ) $this->m_intThreeChecksPerPage : '0';
	}

	public function setIsAboveStubs( $intIsAboveStubs ) {
		$this->set( 'm_intIsAboveStubs', CStrings::strToIntDef( $intIsAboveStubs, NULL, false ) );
	}

	public function getIsAboveStubs() {
		return $this->m_intIsAboveStubs;
	}

	public function sqlIsAboveStubs() {
		return ( true == isset( $this->m_intIsAboveStubs ) ) ? ( string ) $this->m_intIsAboveStubs : '0';
	}

	public function setIsPrePrinted( $intIsPrePrinted ) {
		$this->set( 'm_intIsPrePrinted', CStrings::strToIntDef( $intIsPrePrinted, NULL, false ) );
	}

	public function getIsPrePrinted() {
		return $this->m_intIsPrePrinted;
	}

	public function sqlIsPrePrinted() {
		return ( true == isset( $this->m_intIsPrePrinted ) ) ? ( string ) $this->m_intIsPrePrinted : '0';
	}

	public function setIsDefault( $intIsDefault ) {
		$this->set( 'm_intIsDefault', CStrings::strToIntDef( $intIsDefault, NULL, false ) );
	}

	public function getIsDefault() {
		return $this->m_intIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_intIsDefault ) ) ? ( string ) $this->m_intIsDefault : '0';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'system_code' => $this->getSystemCode(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'custom_text' => $this->getCustomText(),
			'top_margin' => $this->getTopMargin(),
			'left_margin' => $this->getLeftMargin(),
			'print_stub_check_number' => $this->getPrintStubCheckNumber(),
			'print_stub_bank_info' => $this->getPrintStubBankInfo(),
			'center_company_info' => $this->getCenterCompanyInfo(),
			'center_bank_info' => $this->getCenterBankInfo(),
			'three_checks_per_page' => $this->getThreeChecksPerPage(),
			'is_above_stubs' => $this->getIsAboveStubs(),
			'is_pre_printed' => $this->getIsPrePrinted(),
			'is_default' => $this->getIsDefault(),
			'is_system' => $this->getIsSystem(),
			'is_disabled' => $this->getIsDisabled(),
			'details' => $this->getDetails()
		);
	}

}
?>