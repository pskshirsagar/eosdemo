<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningApplicationConditionSets
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningApplicationConditionSets extends CEosPluralBase {

	/**
	 * @return CScreeningApplicationConditionSet[]
	 */
	public static function fetchScreeningApplicationConditionSets( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CScreeningApplicationConditionSet::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScreeningApplicationConditionSet
	 */
	public static function fetchScreeningApplicationConditionSet( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningApplicationConditionSet::class, $objDatabase );
	}

	public static function fetchScreeningApplicationConditionSetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_application_condition_sets', $objDatabase );
	}

	public static function fetchScreeningApplicationConditionSetByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionSet( sprintf( 'SELECT * FROM screening_application_condition_sets WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationConditionSetsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionSets( sprintf( 'SELECT * FROM screening_application_condition_sets WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationConditionSetsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionSets( sprintf( 'SELECT * FROM screening_application_condition_sets WHERE application_id = %d AND cid = %d', $intApplicationId, $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationConditionSetsByScreeningApplicationRequestIdByCid( $intScreeningApplicationRequestId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionSets( sprintf( 'SELECT * FROM screening_application_condition_sets WHERE screening_application_request_id = %d AND cid = %d', $intScreeningApplicationRequestId, $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationConditionSetsByConditionSetIdByCid( $intConditionSetId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionSets( sprintf( 'SELECT * FROM screening_application_condition_sets WHERE condition_set_id = %d AND cid = %d', $intConditionSetId, $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationConditionSetsByConditionSubsetOneIdByCid( $intConditionSubsetOneId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionSets( sprintf( 'SELECT * FROM screening_application_condition_sets WHERE condition_subset_one_id = %d AND cid = %d', $intConditionSubsetOneId, $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationConditionSetsByConditionSubsetTwoIdByCid( $intConditionSubsetTwoId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionSets( sprintf( 'SELECT * FROM screening_application_condition_sets WHERE condition_subset_two_id = %d AND cid = %d', $intConditionSubsetTwoId, $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationConditionSetsByScreeningConditionOperandTypeIdByCid( $intScreeningConditionOperandTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionSets( sprintf( 'SELECT * FROM screening_application_condition_sets WHERE screening_condition_operand_type_id = %d AND cid = %d', $intScreeningConditionOperandTypeId, $intCid ), $objDatabase );
	}

}
?>