<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseComparableAssociation extends CEosSingularBase {

	const TABLE_NAME = 'public.comparable_associations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intComparableOptionId;
	protected $m_intComparableRoomTypeId;
	protected $m_intDefaultAmenityId;
	protected $m_intComparableReferenceId;
	protected $m_intUtilityTypeId;
	protected $m_strNotes;
	protected $m_boolIsFeatured;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsFeatured = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['comparable_option_id'] ) && $boolDirectSet ) $this->set( 'm_intComparableOptionId', trim( $arrValues['comparable_option_id'] ) ); elseif( isset( $arrValues['comparable_option_id'] ) ) $this->setComparableOptionId( $arrValues['comparable_option_id'] );
		if( isset( $arrValues['comparable_room_type_id'] ) && $boolDirectSet ) $this->set( 'm_intComparableRoomTypeId', trim( $arrValues['comparable_room_type_id'] ) ); elseif( isset( $arrValues['comparable_room_type_id'] ) ) $this->setComparableRoomTypeId( $arrValues['comparable_room_type_id'] );
		if( isset( $arrValues['default_amenity_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultAmenityId', trim( $arrValues['default_amenity_id'] ) ); elseif( isset( $arrValues['default_amenity_id'] ) ) $this->setDefaultAmenityId( $arrValues['default_amenity_id'] );
		if( isset( $arrValues['comparable_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intComparableReferenceId', trim( $arrValues['comparable_reference_id'] ) ); elseif( isset( $arrValues['comparable_reference_id'] ) ) $this->setComparableReferenceId( $arrValues['comparable_reference_id'] );
		if( isset( $arrValues['utility_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityTypeId', trim( $arrValues['utility_type_id'] ) ); elseif( isset( $arrValues['utility_type_id'] ) ) $this->setUtilityTypeId( $arrValues['utility_type_id'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['is_featured'] ) && $boolDirectSet ) $this->set( 'm_boolIsFeatured', trim( stripcslashes( $arrValues['is_featured'] ) ) ); elseif( isset( $arrValues['is_featured'] ) ) $this->setIsFeatured( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_featured'] ) : $arrValues['is_featured'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setComparableOptionId( $intComparableOptionId ) {
		$this->set( 'm_intComparableOptionId', CStrings::strToIntDef( $intComparableOptionId, NULL, false ) );
	}

	public function getComparableOptionId() {
		return $this->m_intComparableOptionId;
	}

	public function sqlComparableOptionId() {
		return ( true == isset( $this->m_intComparableOptionId ) ) ? ( string ) $this->m_intComparableOptionId : 'NULL';
	}

	public function setComparableRoomTypeId( $intComparableRoomTypeId ) {
		$this->set( 'm_intComparableRoomTypeId', CStrings::strToIntDef( $intComparableRoomTypeId, NULL, false ) );
	}

	public function getComparableRoomTypeId() {
		return $this->m_intComparableRoomTypeId;
	}

	public function sqlComparableRoomTypeId() {
		return ( true == isset( $this->m_intComparableRoomTypeId ) ) ? ( string ) $this->m_intComparableRoomTypeId : 'NULL';
	}

	public function setDefaultAmenityId( $intDefaultAmenityId ) {
		$this->set( 'm_intDefaultAmenityId', CStrings::strToIntDef( $intDefaultAmenityId, NULL, false ) );
	}

	public function getDefaultAmenityId() {
		return $this->m_intDefaultAmenityId;
	}

	public function sqlDefaultAmenityId() {
		return ( true == isset( $this->m_intDefaultAmenityId ) ) ? ( string ) $this->m_intDefaultAmenityId : 'NULL';
	}

	public function setComparableReferenceId( $intComparableReferenceId ) {
		$this->set( 'm_intComparableReferenceId', CStrings::strToIntDef( $intComparableReferenceId, NULL, false ) );
	}

	public function getComparableReferenceId() {
		return $this->m_intComparableReferenceId;
	}

	public function sqlComparableReferenceId() {
		return ( true == isset( $this->m_intComparableReferenceId ) ) ? ( string ) $this->m_intComparableReferenceId : 'NULL';
	}

	public function setUtilityTypeId( $intUtilityTypeId ) {
		$this->set( 'm_intUtilityTypeId', CStrings::strToIntDef( $intUtilityTypeId, NULL, false ) );
	}

	public function getUtilityTypeId() {
		return $this->m_intUtilityTypeId;
	}

	public function sqlUtilityTypeId() {
		return ( true == isset( $this->m_intUtilityTypeId ) ) ? ( string ) $this->m_intUtilityTypeId : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setIsFeatured( $boolIsFeatured ) {
		$this->set( 'm_boolIsFeatured', CStrings::strToBool( $boolIsFeatured ) );
	}

	public function getIsFeatured() {
		return $this->m_boolIsFeatured;
	}

	public function sqlIsFeatured() {
		return ( true == isset( $this->m_boolIsFeatured ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFeatured ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, comparable_option_id, comparable_room_type_id, default_amenity_id, comparable_reference_id, utility_type_id, notes, is_featured, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlComparableOptionId() . ', ' .
 						$this->sqlComparableRoomTypeId() . ', ' .
 						$this->sqlDefaultAmenityId() . ', ' .
 						$this->sqlComparableReferenceId() . ', ' .
 						$this->sqlUtilityTypeId() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlIsFeatured() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' comparable_option_id = ' . $this->sqlComparableOptionId() . ','; } elseif( true == array_key_exists( 'ComparableOptionId', $this->getChangedColumns() ) ) { $strSql .= ' comparable_option_id = ' . $this->sqlComparableOptionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' comparable_room_type_id = ' . $this->sqlComparableRoomTypeId() . ','; } elseif( true == array_key_exists( 'ComparableRoomTypeId', $this->getChangedColumns() ) ) { $strSql .= ' comparable_room_type_id = ' . $this->sqlComparableRoomTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_amenity_id = ' . $this->sqlDefaultAmenityId() . ','; } elseif( true == array_key_exists( 'DefaultAmenityId', $this->getChangedColumns() ) ) { $strSql .= ' default_amenity_id = ' . $this->sqlDefaultAmenityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' comparable_reference_id = ' . $this->sqlComparableReferenceId() . ','; } elseif( true == array_key_exists( 'ComparableReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' comparable_reference_id = ' . $this->sqlComparableReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_type_id = ' . $this->sqlUtilityTypeId() . ','; } elseif( true == array_key_exists( 'UtilityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_type_id = ' . $this->sqlUtilityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_featured = ' . $this->sqlIsFeatured() . ','; } elseif( true == array_key_exists( 'IsFeatured', $this->getChangedColumns() ) ) { $strSql .= ' is_featured = ' . $this->sqlIsFeatured() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'comparable_option_id' => $this->getComparableOptionId(),
			'comparable_room_type_id' => $this->getComparableRoomTypeId(),
			'default_amenity_id' => $this->getDefaultAmenityId(),
			'comparable_reference_id' => $this->getComparableReferenceId(),
			'utility_type_id' => $this->getUtilityTypeId(),
			'notes' => $this->getNotes(),
			'is_featured' => $this->getIsFeatured(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>