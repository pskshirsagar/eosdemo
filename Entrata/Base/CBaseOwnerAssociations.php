<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COwnerAssociations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOwnerAssociations extends CEosPluralBase {

	/**
	 * @return COwnerAssociation[]
	 */
	public static function fetchOwnerAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'COwnerAssociation', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return COwnerAssociation
	 */
	public static function fetchOwnerAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'COwnerAssociation', $objDatabase );
	}

	public static function fetchOwnerAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'owner_associations', $objDatabase );
	}

	public static function fetchOwnerAssociationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchOwnerAssociation( sprintf( 'SELECT * FROM owner_associations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOwnerAssociationsByCid( $intCid, $objDatabase ) {
		return self::fetchOwnerAssociations( sprintf( 'SELECT * FROM owner_associations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOwnerAssociationsByOwnerIdByCid( $intOwnerId, $intCid, $objDatabase ) {
		return self::fetchOwnerAssociations( sprintf( 'SELECT * FROM owner_associations WHERE owner_id = %d AND cid = %d', ( int ) $intOwnerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOwnerAssociationsByParentOwnerIdByCid( $intParentOwnerId, $intCid, $objDatabase ) {
		return self::fetchOwnerAssociations( sprintf( 'SELECT * FROM owner_associations WHERE parent_owner_id = %d AND cid = %d', ( int ) $intParentOwnerId, ( int ) $intCid ), $objDatabase );
	}

}
?>