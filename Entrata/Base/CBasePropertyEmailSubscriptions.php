<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyEmailSubscriptions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyEmailSubscriptions extends CEosPluralBase {

	/**
	 * @return CPropertyEmailSubscription[]
	 */
	public static function fetchPropertyEmailSubscriptions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyEmailSubscription', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyEmailSubscription
	 */
	public static function fetchPropertyEmailSubscription( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyEmailSubscription', $objDatabase );
	}

	public static function fetchPropertyEmailSubscriptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_email_subscriptions', $objDatabase );
	}

	public static function fetchPropertyEmailSubscriptionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyEmailSubscription( sprintf( 'SELECT * FROM property_email_subscriptions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyEmailSubscriptionsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyEmailSubscriptions( sprintf( 'SELECT * FROM property_email_subscriptions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyEmailSubscriptionsBySystemEmailTypeIdByCid( $intSystemEmailTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyEmailSubscriptions( sprintf( 'SELECT * FROM property_email_subscriptions WHERE system_email_type_id = %d AND cid = %d', ( int ) $intSystemEmailTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyEmailSubscriptionsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyEmailSubscriptions( sprintf( 'SELECT * FROM property_email_subscriptions WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyEmailSubscriptionsByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchPropertyEmailSubscriptions( sprintf( 'SELECT * FROM property_email_subscriptions WHERE company_employee_id = %d AND cid = %d', ( int ) $intCompanyEmployeeId, ( int ) $intCid ), $objDatabase );
	}

}
?>