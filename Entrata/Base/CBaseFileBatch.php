<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFileBatch extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.file_batches';

	protected $m_intId;
	protected $m_intCid;
	protected $m_strBatchName;
	protected $m_intFileCount;
	protected $m_intArchiveFileSize;
	protected $m_strExportPath;
	protected $m_strSearchFilter;
	protected $m_strArchiveFileName;
	protected $m_strArchiveFilePath;
	protected $m_strArchiveCreatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intFileBatchTypeId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['batch_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBatchName', trim( stripcslashes( $arrValues['batch_name'] ) ) ); elseif( isset( $arrValues['batch_name'] ) ) $this->setBatchName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['batch_name'] ) : $arrValues['batch_name'] );
		if( isset( $arrValues['file_count'] ) && $boolDirectSet ) $this->set( 'm_intFileCount', trim( $arrValues['file_count'] ) ); elseif( isset( $arrValues['file_count'] ) ) $this->setFileCount( $arrValues['file_count'] );
		if( isset( $arrValues['archive_file_size'] ) && $boolDirectSet ) $this->set( 'm_intArchiveFileSize', trim( $arrValues['archive_file_size'] ) ); elseif( isset( $arrValues['archive_file_size'] ) ) $this->setArchiveFileSize( $arrValues['archive_file_size'] );
		if( isset( $arrValues['export_path'] ) && $boolDirectSet ) $this->set( 'm_strExportPath', trim( stripcslashes( $arrValues['export_path'] ) ) ); elseif( isset( $arrValues['export_path'] ) ) $this->setExportPath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['export_path'] ) : $arrValues['export_path'] );
		if( isset( $arrValues['search_filter'] ) && $boolDirectSet ) $this->set( 'm_strSearchFilter', trim( stripcslashes( $arrValues['search_filter'] ) ) ); elseif( isset( $arrValues['search_filter'] ) ) $this->setSearchFilter( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['search_filter'] ) : $arrValues['search_filter'] );
		if( isset( $arrValues['archive_file_name'] ) && $boolDirectSet ) $this->set( 'm_strArchiveFileName', trim( stripcslashes( $arrValues['archive_file_name'] ) ) ); elseif( isset( $arrValues['archive_file_name'] ) ) $this->setArchiveFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['archive_file_name'] ) : $arrValues['archive_file_name'] );
		if( isset( $arrValues['archive_file_path'] ) && $boolDirectSet ) $this->set( 'm_strArchiveFilePath', trim( stripcslashes( $arrValues['archive_file_path'] ) ) ); elseif( isset( $arrValues['archive_file_path'] ) ) $this->setArchiveFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['archive_file_path'] ) : $arrValues['archive_file_path'] );
		if( isset( $arrValues['archive_created_on'] ) && $boolDirectSet ) $this->set( 'm_strArchiveCreatedOn', trim( $arrValues['archive_created_on'] ) ); elseif( isset( $arrValues['archive_created_on'] ) ) $this->setArchiveCreatedOn( $arrValues['archive_created_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['file_batch_type_id'] ) && $boolDirectSet ) $this->set( 'm_intFileBatchTypeId', trim( $arrValues['file_batch_type_id'] ) ); elseif( isset( $arrValues['file_batch_type_id'] ) ) $this->setFileBatchTypeId( $arrValues['file_batch_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setBatchName( $strBatchName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strBatchName', CStrings::strTrimDef( $strBatchName, 200, NULL, true ), $strLocaleCode );
	}

	public function getBatchName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strBatchName', $strLocaleCode );
	}

	public function sqlBatchName() {
		return ( true == isset( $this->m_strBatchName ) ) ? '\'' . addslashes( $this->m_strBatchName ) . '\'' : 'NULL';
	}

	public function setFileCount( $intFileCount ) {
		$this->set( 'm_intFileCount', CStrings::strToIntDef( $intFileCount, NULL, false ) );
	}

	public function getFileCount() {
		return $this->m_intFileCount;
	}

	public function sqlFileCount() {
		return ( true == isset( $this->m_intFileCount ) ) ? ( string ) $this->m_intFileCount : 'NULL';
	}

	public function setArchiveFileSize( $intArchiveFileSize ) {
		$this->set( 'm_intArchiveFileSize', CStrings::strToIntDef( $intArchiveFileSize, NULL, false ) );
	}

	public function getArchiveFileSize() {
		return $this->m_intArchiveFileSize;
	}

	public function sqlArchiveFileSize() {
		return ( true == isset( $this->m_intArchiveFileSize ) ) ? ( string ) $this->m_intArchiveFileSize : 'NULL';
	}

	public function setExportPath( $strExportPath ) {
		$this->set( 'm_strExportPath', CStrings::strTrimDef( $strExportPath, 1000, NULL, true ) );
	}

	public function getExportPath() {
		return $this->m_strExportPath;
	}

	public function sqlExportPath() {
		return ( true == isset( $this->m_strExportPath ) ) ? '\'' . addslashes( $this->m_strExportPath ) . '\'' : 'NULL';
	}

	public function setSearchFilter( $strSearchFilter ) {
		$this->set( 'm_strSearchFilter', CStrings::strTrimDef( $strSearchFilter, -1, NULL, true ) );
	}

	public function getSearchFilter() {
		return $this->m_strSearchFilter;
	}

	public function sqlSearchFilter() {
		return ( true == isset( $this->m_strSearchFilter ) ) ? '\'' . addslashes( $this->m_strSearchFilter ) . '\'' : 'NULL';
	}

	public function setArchiveFileName( $strArchiveFileName ) {
		$this->set( 'm_strArchiveFileName', CStrings::strTrimDef( $strArchiveFileName, 240, NULL, true ) );
	}

	public function getArchiveFileName() {
		return $this->m_strArchiveFileName;
	}

	public function sqlArchiveFileName() {
		return ( true == isset( $this->m_strArchiveFileName ) ) ? '\'' . addslashes( $this->m_strArchiveFileName ) . '\'' : 'NULL';
	}

	public function setArchiveFilePath( $strArchiveFilePath ) {
		$this->set( 'm_strArchiveFilePath', CStrings::strTrimDef( $strArchiveFilePath, 4096, NULL, true ) );
	}

	public function getArchiveFilePath() {
		return $this->m_strArchiveFilePath;
	}

	public function sqlArchiveFilePath() {
		return ( true == isset( $this->m_strArchiveFilePath ) ) ? '\'' . addslashes( $this->m_strArchiveFilePath ) . '\'' : 'NULL';
	}

	public function setArchiveCreatedOn( $strArchiveCreatedOn ) {
		$this->set( 'm_strArchiveCreatedOn', CStrings::strTrimDef( $strArchiveCreatedOn, -1, NULL, true ) );
	}

	public function getArchiveCreatedOn() {
		return $this->m_strArchiveCreatedOn;
	}

	public function sqlArchiveCreatedOn() {
		return ( true == isset( $this->m_strArchiveCreatedOn ) ) ? '\'' . $this->m_strArchiveCreatedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setFileBatchTypeId( $intFileBatchTypeId ) {
		$this->set( 'm_intFileBatchTypeId', CStrings::strToIntDef( $intFileBatchTypeId, NULL, false ) );
	}

	public function getFileBatchTypeId() {
		return $this->m_intFileBatchTypeId;
	}

	public function sqlFileBatchTypeId() {
		return ( true == isset( $this->m_intFileBatchTypeId ) ) ? ( string ) $this->m_intFileBatchTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, batch_name, file_count, archive_file_size, export_path, search_filter, archive_file_name, archive_file_path, archive_created_on, updated_by, updated_on, created_by, created_on, details, file_batch_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlBatchName() . ', ' .
						$this->sqlFileCount() . ', ' .
						$this->sqlArchiveFileSize() . ', ' .
						$this->sqlExportPath() . ', ' .
						$this->sqlSearchFilter() . ', ' .
						$this->sqlArchiveFileName() . ', ' .
						$this->sqlArchiveFilePath() . ', ' .
						$this->sqlArchiveCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlFileBatchTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_name = ' . $this->sqlBatchName(). ',' ; } elseif( true == array_key_exists( 'BatchName', $this->getChangedColumns() ) ) { $strSql .= ' batch_name = ' . $this->sqlBatchName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_count = ' . $this->sqlFileCount(). ',' ; } elseif( true == array_key_exists( 'FileCount', $this->getChangedColumns() ) ) { $strSql .= ' file_count = ' . $this->sqlFileCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' archive_file_size = ' . $this->sqlArchiveFileSize(). ',' ; } elseif( true == array_key_exists( 'ArchiveFileSize', $this->getChangedColumns() ) ) { $strSql .= ' archive_file_size = ' . $this->sqlArchiveFileSize() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_path = ' . $this->sqlExportPath(). ',' ; } elseif( true == array_key_exists( 'ExportPath', $this->getChangedColumns() ) ) { $strSql .= ' export_path = ' . $this->sqlExportPath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' search_filter = ' . $this->sqlSearchFilter(). ',' ; } elseif( true == array_key_exists( 'SearchFilter', $this->getChangedColumns() ) ) { $strSql .= ' search_filter = ' . $this->sqlSearchFilter() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' archive_file_name = ' . $this->sqlArchiveFileName(). ',' ; } elseif( true == array_key_exists( 'ArchiveFileName', $this->getChangedColumns() ) ) { $strSql .= ' archive_file_name = ' . $this->sqlArchiveFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' archive_file_path = ' . $this->sqlArchiveFilePath(). ',' ; } elseif( true == array_key_exists( 'ArchiveFilePath', $this->getChangedColumns() ) ) { $strSql .= ' archive_file_path = ' . $this->sqlArchiveFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' archive_created_on = ' . $this->sqlArchiveCreatedOn(). ',' ; } elseif( true == array_key_exists( 'ArchiveCreatedOn', $this->getChangedColumns() ) ) { $strSql .= ' archive_created_on = ' . $this->sqlArchiveCreatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_batch_type_id = ' . $this->sqlFileBatchTypeId(). ',' ; } elseif( true == array_key_exists( 'FileBatchTypeId', $this->getChangedColumns() ) ) { $strSql .= ' file_batch_type_id = ' . $this->sqlFileBatchTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'batch_name' => $this->getBatchName(),
			'file_count' => $this->getFileCount(),
			'archive_file_size' => $this->getArchiveFileSize(),
			'export_path' => $this->getExportPath(),
			'search_filter' => $this->getSearchFilter(),
			'archive_file_name' => $this->getArchiveFileName(),
			'archive_file_path' => $this->getArchiveFilePath(),
			'archive_created_on' => $this->getArchiveCreatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'file_batch_type_id' => $this->getFileBatchTypeId()
		);
	}

}
?>