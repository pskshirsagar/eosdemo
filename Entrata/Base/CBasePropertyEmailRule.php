<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyEmailRule extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.property_email_rules';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSystemMessageId;
	protected $m_intSystemEmailTypeId;
	protected $m_strCustomHeaderText;
	protected $m_strCustomFooterText;
	protected $m_boolResidentsDisabled;
	protected $m_boolProspectsDisabled;
	protected $m_boolEmployeesDisabled;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_boolResidentsDisabled = false;
		$this->m_boolProspectsDisabled = false;
		$this->m_boolEmployeesDisabled = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['system_message_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemMessageId', trim( $arrValues['system_message_id'] ) ); elseif( isset( $arrValues['system_message_id'] ) ) $this->setSystemMessageId( $arrValues['system_message_id'] );
		if( isset( $arrValues['system_email_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemEmailTypeId', trim( $arrValues['system_email_type_id'] ) ); elseif( isset( $arrValues['system_email_type_id'] ) ) $this->setSystemEmailTypeId( $arrValues['system_email_type_id'] );
		if( isset( $arrValues['custom_header_text'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCustomHeaderText', trim( stripcslashes( $arrValues['custom_header_text'] ) ) ); elseif( isset( $arrValues['custom_header_text'] ) ) $this->setCustomHeaderText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['custom_header_text'] ) : $arrValues['custom_header_text'] );
		if( isset( $arrValues['custom_footer_text'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCustomFooterText', trim( stripcslashes( $arrValues['custom_footer_text'] ) ) ); elseif( isset( $arrValues['custom_footer_text'] ) ) $this->setCustomFooterText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['custom_footer_text'] ) : $arrValues['custom_footer_text'] );
		if( isset( $arrValues['residents_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolResidentsDisabled', trim( stripcslashes( $arrValues['residents_disabled'] ) ) ); elseif( isset( $arrValues['residents_disabled'] ) ) $this->setResidentsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['residents_disabled'] ) : $arrValues['residents_disabled'] );
		if( isset( $arrValues['prospects_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolProspectsDisabled', trim( stripcslashes( $arrValues['prospects_disabled'] ) ) ); elseif( isset( $arrValues['prospects_disabled'] ) ) $this->setProspectsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['prospects_disabled'] ) : $arrValues['prospects_disabled'] );
		if( isset( $arrValues['employees_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolEmployeesDisabled', trim( stripcslashes( $arrValues['employees_disabled'] ) ) ); elseif( isset( $arrValues['employees_disabled'] ) ) $this->setEmployeesDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['employees_disabled'] ) : $arrValues['employees_disabled'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSystemMessageId( $intSystemMessageId ) {
		$this->set( 'm_intSystemMessageId', CStrings::strToIntDef( $intSystemMessageId, NULL, false ) );
	}

	public function getSystemMessageId() {
		return $this->m_intSystemMessageId;
	}

	public function sqlSystemMessageId() {
		return ( true == isset( $this->m_intSystemMessageId ) ) ? ( string ) $this->m_intSystemMessageId : 'NULL';
	}

	public function setSystemEmailTypeId( $intSystemEmailTypeId ) {
		$this->set( 'm_intSystemEmailTypeId', CStrings::strToIntDef( $intSystemEmailTypeId, NULL, false ) );
	}

	public function getSystemEmailTypeId() {
		return $this->m_intSystemEmailTypeId;
	}

	public function sqlSystemEmailTypeId() {
		return ( true == isset( $this->m_intSystemEmailTypeId ) ) ? ( string ) $this->m_intSystemEmailTypeId : 'NULL';
	}

	public function setCustomHeaderText( $strCustomHeaderText, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strCustomHeaderText', CStrings::strTrimDef( $strCustomHeaderText, -1, NULL, true ), $strLocaleCode );
	}

	public function getCustomHeaderText( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strCustomHeaderText', $strLocaleCode );
	}

	public function sqlCustomHeaderText() {
		return ( true == isset( $this->m_strCustomHeaderText ) ) ? '\'' . addslashes( $this->m_strCustomHeaderText ) . '\'' : 'NULL';
	}

	public function setCustomFooterText( $strCustomFooterText, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strCustomFooterText', CStrings::strTrimDef( $strCustomFooterText, -1, NULL, true ), $strLocaleCode );
	}

	public function getCustomFooterText( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strCustomFooterText', $strLocaleCode );
	}

	public function sqlCustomFooterText() {
		return ( true == isset( $this->m_strCustomFooterText ) ) ? '\'' . addslashes( $this->m_strCustomFooterText ) . '\'' : 'NULL';
	}

	public function setResidentsDisabled( $boolResidentsDisabled ) {
		$this->set( 'm_boolResidentsDisabled', CStrings::strToBool( $boolResidentsDisabled ) );
	}

	public function getResidentsDisabled() {
		return $this->m_boolResidentsDisabled;
	}

	public function sqlResidentsDisabled() {
		return ( true == isset( $this->m_boolResidentsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolResidentsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setProspectsDisabled( $boolProspectsDisabled ) {
		$this->set( 'm_boolProspectsDisabled', CStrings::strToBool( $boolProspectsDisabled ) );
	}

	public function getProspectsDisabled() {
		return $this->m_boolProspectsDisabled;
	}

	public function sqlProspectsDisabled() {
		return ( true == isset( $this->m_boolProspectsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolProspectsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setEmployeesDisabled( $boolEmployeesDisabled ) {
		$this->set( 'm_boolEmployeesDisabled', CStrings::strToBool( $boolEmployeesDisabled ) );
	}

	public function getEmployeesDisabled() {
		return $this->m_boolEmployeesDisabled;
	}

	public function sqlEmployeesDisabled() {
		return ( true == isset( $this->m_boolEmployeesDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolEmployeesDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, system_message_id, system_email_type_id, custom_header_text, custom_footer_text, residents_disabled, prospects_disabled, employees_disabled, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlSystemMessageId() . ', ' .
						$this->sqlSystemEmailTypeId() . ', ' .
						$this->sqlCustomHeaderText() . ', ' .
						$this->sqlCustomFooterText() . ', ' .
						$this->sqlResidentsDisabled() . ', ' .
						$this->sqlProspectsDisabled() . ', ' .
						$this->sqlEmployeesDisabled() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_message_id = ' . $this->sqlSystemMessageId(). ',' ; } elseif( true == array_key_exists( 'SystemMessageId', $this->getChangedColumns() ) ) { $strSql .= ' system_message_id = ' . $this->sqlSystemMessageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_email_type_id = ' . $this->sqlSystemEmailTypeId(). ',' ; } elseif( true == array_key_exists( 'SystemEmailTypeId', $this->getChangedColumns() ) ) { $strSql .= ' system_email_type_id = ' . $this->sqlSystemEmailTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' custom_header_text = ' . $this->sqlCustomHeaderText(). ',' ; } elseif( true == array_key_exists( 'CustomHeaderText', $this->getChangedColumns() ) ) { $strSql .= ' custom_header_text = ' . $this->sqlCustomHeaderText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' custom_footer_text = ' . $this->sqlCustomFooterText(). ',' ; } elseif( true == array_key_exists( 'CustomFooterText', $this->getChangedColumns() ) ) { $strSql .= ' custom_footer_text = ' . $this->sqlCustomFooterText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' residents_disabled = ' . $this->sqlResidentsDisabled(). ',' ; } elseif( true == array_key_exists( 'ResidentsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' residents_disabled = ' . $this->sqlResidentsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prospects_disabled = ' . $this->sqlProspectsDisabled(). ',' ; } elseif( true == array_key_exists( 'ProspectsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' prospects_disabled = ' . $this->sqlProspectsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employees_disabled = ' . $this->sqlEmployeesDisabled(). ',' ; } elseif( true == array_key_exists( 'EmployeesDisabled', $this->getChangedColumns() ) ) { $strSql .= ' employees_disabled = ' . $this->sqlEmployeesDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'system_message_id' => $this->getSystemMessageId(),
			'system_email_type_id' => $this->getSystemEmailTypeId(),
			'custom_header_text' => $this->getCustomHeaderText(),
			'custom_footer_text' => $this->getCustomFooterText(),
			'residents_disabled' => $this->getResidentsDisabled(),
			'prospects_disabled' => $this->getProspectsDisabled(),
			'employees_disabled' => $this->getEmployeesDisabled(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>