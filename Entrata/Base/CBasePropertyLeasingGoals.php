<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyLeasingGoals
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyLeasingGoals extends CEosPluralBase {

	/**
	 * @return CPropertyLeasingGoal[]
	 */
	public static function fetchPropertyLeasingGoals( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyLeasingGoal', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyLeasingGoal
	 */
	public static function fetchPropertyLeasingGoal( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyLeasingGoal', $objDatabase );
	}

	public static function fetchPropertyLeasingGoalCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_leasing_goals', $objDatabase );
	}

	public static function fetchPropertyLeasingGoalByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyLeasingGoal( sprintf( 'SELECT * FROM property_leasing_goals WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyLeasingGoalsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyLeasingGoals( sprintf( 'SELECT * FROM property_leasing_goals WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyLeasingGoalsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyLeasingGoals( sprintf( 'SELECT * FROM property_leasing_goals WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyLeasingGoalsByLeaseTermIdByCid( $intLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchPropertyLeasingGoals( sprintf( 'SELECT * FROM property_leasing_goals WHERE lease_term_id = %d AND cid = %d', ( int ) $intLeaseTermId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyLeasingGoalsByLeaseStartWindowIdByCid( $intLeaseStartWindowId, $intCid, $objDatabase ) {
		return self::fetchPropertyLeasingGoals( sprintf( 'SELECT * FROM property_leasing_goals WHERE lease_start_window_id = %d AND cid = %d', ( int ) $intLeaseStartWindowId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyLeasingGoalsByPropertyFloorplanIdByCid( $intPropertyFloorplanId, $intCid, $objDatabase ) {
		return self::fetchPropertyLeasingGoals( sprintf( 'SELECT * FROM property_leasing_goals WHERE property_floorplan_id = %d AND cid = %d', ( int ) $intPropertyFloorplanId, ( int ) $intCid ), $objDatabase );
	}

}
?>