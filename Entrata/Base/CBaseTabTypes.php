<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTabTypes
 * Do not add any new functions to this class.
 */

class CBaseTabTypes extends CEosPluralBase {

	/**
	 * @return CTabType[]
	 */
	public static function fetchTabTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTabType::class, $objDatabase );
	}

	/**
	 * @return CTabType
	 */
	public static function fetchTabType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTabType::class, $objDatabase );
	}

	public static function fetchTabTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'tab_types', $objDatabase );
	}

	public static function fetchTabTypeById( $intId, $objDatabase ) {
		return self::fetchTabType( sprintf( 'SELECT * FROM tab_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>