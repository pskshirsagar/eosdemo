<?php

class CBaseDefaultAddOnCategory extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_add_on_categories';

	protected $m_intId;
	protected $m_intAddOnTypeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_boolIsPublished;
	protected $m_boolIsComparable;
	protected $m_boolIsRequirable;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;
		$this->m_boolIsComparable = false;
		$this->m_boolIsRequirable = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['add_on_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAddOnTypeId', trim( $arrValues['add_on_type_id'] ) ); elseif( isset( $arrValues['add_on_type_id'] ) ) $this->setAddOnTypeId( $arrValues['add_on_type_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['is_comparable'] ) && $boolDirectSet ) $this->set( 'm_boolIsComparable', trim( stripcslashes( $arrValues['is_comparable'] ) ) ); elseif( isset( $arrValues['is_comparable'] ) ) $this->setIsComparable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_comparable'] ) : $arrValues['is_comparable'] );
		if( isset( $arrValues['is_requirable'] ) && $boolDirectSet ) $this->set( 'm_boolIsRequirable', trim( stripcslashes( $arrValues['is_requirable'] ) ) ); elseif( isset( $arrValues['is_requirable'] ) ) $this->setIsRequirable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_requirable'] ) : $arrValues['is_requirable'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setAddOnTypeId( $intAddOnTypeId ) {
		$this->set( 'm_intAddOnTypeId', CStrings::strToIntDef( $intAddOnTypeId, NULL, false ) );
	}

	public function getAddOnTypeId() {
		return $this->m_intAddOnTypeId;
	}

	public function sqlAddOnTypeId() {
		return ( true == isset( $this->m_intAddOnTypeId ) ) ? ( string ) $this->m_intAddOnTypeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, -1, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsComparable( $boolIsComparable ) {
		$this->set( 'm_boolIsComparable', CStrings::strToBool( $boolIsComparable ) );
	}

	public function getIsComparable() {
		return $this->m_boolIsComparable;
	}

	public function sqlIsComparable() {
		return ( true == isset( $this->m_boolIsComparable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsComparable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsRequirable( $boolIsRequirable ) {
		$this->set( 'm_boolIsRequirable', CStrings::strToBool( $boolIsRequirable ) );
	}

	public function getIsRequirable() {
		return $this->m_boolIsRequirable;
	}

	public function sqlIsRequirable() {
		return ( true == isset( $this->m_boolIsRequirable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRequirable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'add_on_type_id' => $this->getAddOnTypeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'is_published' => $this->getIsPublished(),
			'is_comparable' => $this->getIsComparable(),
			'is_requirable' => $this->getIsRequirable(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>