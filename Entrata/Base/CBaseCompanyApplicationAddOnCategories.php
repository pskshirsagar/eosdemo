<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyApplicationAddOnCategories
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyApplicationAddOnCategories extends CEosPluralBase {

	/**
	 * @return CCompanyApplicationAddOnCategory[]
	 */
	public static function fetchCompanyApplicationAddOnCategories( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyApplicationAddOnCategory', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyApplicationAddOnCategory
	 */
	public static function fetchCompanyApplicationAddOnCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyApplicationAddOnCategory', $objDatabase );
	}

	public static function fetchCompanyApplicationAddOnCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_application_add_on_categories', $objDatabase );
	}

	public static function fetchCompanyApplicationAddOnCategoryByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyApplicationAddOnCategory( sprintf( 'SELECT * FROM company_application_add_on_categories WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyApplicationAddOnCategoriesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyApplicationAddOnCategories( sprintf( 'SELECT * FROM company_application_add_on_categories WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyApplicationAddOnCategoriesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCompanyApplicationAddOnCategories( sprintf( 'SELECT * FROM company_application_add_on_categories WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyApplicationAddOnCategoriesByAddOnCategoryIdByCid( $intAddOnCategoryId, $intCid, $objDatabase ) {
		return self::fetchCompanyApplicationAddOnCategories( sprintf( 'SELECT * FROM company_application_add_on_categories WHERE add_on_category_id = %d AND cid = %d', ( int ) $intAddOnCategoryId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyApplicationAddOnCategoriesByAddOnGroupIdByCid( $intAddOnGroupId, $intCid, $objDatabase ) {
		return self::fetchCompanyApplicationAddOnCategories( sprintf( 'SELECT * FROM company_application_add_on_categories WHERE add_on_group_id = %d AND cid = %d', ( int ) $intAddOnGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyApplicationAddOnCategoriesByCompanyApplicationIdByCid( $intCompanyApplicationId, $intCid, $objDatabase ) {
		return self::fetchCompanyApplicationAddOnCategories( sprintf( 'SELECT * FROM company_application_add_on_categories WHERE company_application_id = %d AND cid = %d', ( int ) $intCompanyApplicationId, ( int ) $intCid ), $objDatabase );
	}

}
?>