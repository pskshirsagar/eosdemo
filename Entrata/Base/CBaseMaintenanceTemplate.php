<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceTemplate extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.maintenance_templates';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intParentMaintenanceTemplateId;
	protected $m_intMaintenancePriorityId;
	protected $m_intMaintenanceLocationId;
	protected $m_intMaintenanceProblemId;
	protected $m_intMaintenanceRequestTypeId;
	protected $m_intMaintenanceStatusId;
	protected $m_intDueIntervalTypeId;
	protected $m_strTitle;
	protected $m_strDescription;
	protected $m_intDaysToComplete;
	protected $m_fltDueInterval;
	protected $m_boolExcludeWeekendsHolidays;
	protected $m_intEstimatedHours;
	protected $m_intOrderNum;
	protected $m_intIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_arrintOccupancyTypeIds;
	protected $m_boolIsMoveOutDateAsStartDate;

	public function __construct() {
		parent::__construct();

		$this->m_intDueIntervalTypeId = '3';
		$this->m_boolExcludeWeekendsHolidays = false;
		$this->m_intIsPublished = '1';
		$this->m_boolIsMoveOutDateAsStartDate = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['parent_maintenance_template_id'] ) && $boolDirectSet ) $this->set( 'm_intParentMaintenanceTemplateId', trim( $arrValues['parent_maintenance_template_id'] ) ); elseif( isset( $arrValues['parent_maintenance_template_id'] ) ) $this->setParentMaintenanceTemplateId( $arrValues['parent_maintenance_template_id'] );
		if( isset( $arrValues['maintenance_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenancePriorityId', trim( $arrValues['maintenance_priority_id'] ) ); elseif( isset( $arrValues['maintenance_priority_id'] ) ) $this->setMaintenancePriorityId( $arrValues['maintenance_priority_id'] );
		if( isset( $arrValues['maintenance_location_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceLocationId', trim( $arrValues['maintenance_location_id'] ) ); elseif( isset( $arrValues['maintenance_location_id'] ) ) $this->setMaintenanceLocationId( $arrValues['maintenance_location_id'] );
		if( isset( $arrValues['maintenance_problem_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceProblemId', trim( $arrValues['maintenance_problem_id'] ) ); elseif( isset( $arrValues['maintenance_problem_id'] ) ) $this->setMaintenanceProblemId( $arrValues['maintenance_problem_id'] );
		if( isset( $arrValues['maintenance_request_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceRequestTypeId', trim( $arrValues['maintenance_request_type_id'] ) ); elseif( isset( $arrValues['maintenance_request_type_id'] ) ) $this->setMaintenanceRequestTypeId( $arrValues['maintenance_request_type_id'] );
		if( isset( $arrValues['maintenance_status_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceStatusId', trim( $arrValues['maintenance_status_id'] ) ); elseif( isset( $arrValues['maintenance_status_id'] ) ) $this->setMaintenanceStatusId( $arrValues['maintenance_status_id'] );
		if( isset( $arrValues['due_interval_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDueIntervalTypeId', trim( $arrValues['due_interval_type_id'] ) ); elseif( isset( $arrValues['due_interval_type_id'] ) ) $this->setDueIntervalTypeId( $arrValues['due_interval_type_id'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['days_to_complete'] ) && $boolDirectSet ) $this->set( 'm_intDaysToComplete', trim( $arrValues['days_to_complete'] ) ); elseif( isset( $arrValues['days_to_complete'] ) ) $this->setDaysToComplete( $arrValues['days_to_complete'] );
		if( isset( $arrValues['due_interval'] ) && $boolDirectSet ) $this->set( 'm_fltDueInterval', trim( $arrValues['due_interval'] ) ); elseif( isset( $arrValues['due_interval'] ) ) $this->setDueInterval( $arrValues['due_interval'] );
		if( isset( $arrValues['exclude_weekends_holidays'] ) && $boolDirectSet ) $this->set( 'm_boolExcludeWeekendsHolidays', trim( stripcslashes( $arrValues['exclude_weekends_holidays'] ) ) ); elseif( isset( $arrValues['exclude_weekends_holidays'] ) ) $this->setExcludeWeekendsHolidays( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['exclude_weekends_holidays'] ) : $arrValues['exclude_weekends_holidays'] );
		if( isset( $arrValues['estimated_hours'] ) && $boolDirectSet ) $this->set( 'm_intEstimatedHours', trim( $arrValues['estimated_hours'] ) ); elseif( isset( $arrValues['estimated_hours'] ) ) $this->setEstimatedHours( $arrValues['estimated_hours'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['occupancy_type_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintOccupancyTypeIds', trim( $arrValues['occupancy_type_ids'] ) ); elseif( isset( $arrValues['occupancy_type_ids'] ) ) $this->setOccupancyTypeIds( $arrValues['occupancy_type_ids'] );
		if( isset( $arrValues['is_move_out_date_as_start_date'] ) && $boolDirectSet ) $this->set( 'm_boolIsMoveOutDateAsStartDate', trim( stripcslashes( $arrValues['is_move_out_date_as_start_date'] ) ) ); elseif( isset( $arrValues['is_move_out_date_as_start_date'] ) ) $this->setIsMoveOutDateAsStartDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_move_out_date_as_start_date'] ) : $arrValues['is_move_out_date_as_start_date'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setParentMaintenanceTemplateId( $intParentMaintenanceTemplateId ) {
		$this->set( 'm_intParentMaintenanceTemplateId', CStrings::strToIntDef( $intParentMaintenanceTemplateId, NULL, false ) );
	}

	public function getParentMaintenanceTemplateId() {
		return $this->m_intParentMaintenanceTemplateId;
	}

	public function sqlParentMaintenanceTemplateId() {
		return ( true == isset( $this->m_intParentMaintenanceTemplateId ) ) ? ( string ) $this->m_intParentMaintenanceTemplateId : 'NULL';
	}

	public function setMaintenancePriorityId( $intMaintenancePriorityId ) {
		$this->set( 'm_intMaintenancePriorityId', CStrings::strToIntDef( $intMaintenancePriorityId, NULL, false ) );
	}

	public function getMaintenancePriorityId() {
		return $this->m_intMaintenancePriorityId;
	}

	public function sqlMaintenancePriorityId() {
		return ( true == isset( $this->m_intMaintenancePriorityId ) ) ? ( string ) $this->m_intMaintenancePriorityId : 'NULL';
	}

	public function setMaintenanceLocationId( $intMaintenanceLocationId ) {
		$this->set( 'm_intMaintenanceLocationId', CStrings::strToIntDef( $intMaintenanceLocationId, NULL, false ) );
	}

	public function getMaintenanceLocationId() {
		return $this->m_intMaintenanceLocationId;
	}

	public function sqlMaintenanceLocationId() {
		return ( true == isset( $this->m_intMaintenanceLocationId ) ) ? ( string ) $this->m_intMaintenanceLocationId : 'NULL';
	}

	public function setMaintenanceProblemId( $intMaintenanceProblemId ) {
		$this->set( 'm_intMaintenanceProblemId', CStrings::strToIntDef( $intMaintenanceProblemId, NULL, false ) );
	}

	public function getMaintenanceProblemId() {
		return $this->m_intMaintenanceProblemId;
	}

	public function sqlMaintenanceProblemId() {
		return ( true == isset( $this->m_intMaintenanceProblemId ) ) ? ( string ) $this->m_intMaintenanceProblemId : 'NULL';
	}

	public function setMaintenanceRequestTypeId( $intMaintenanceRequestTypeId ) {
		$this->set( 'm_intMaintenanceRequestTypeId', CStrings::strToIntDef( $intMaintenanceRequestTypeId, NULL, false ) );
	}

	public function getMaintenanceRequestTypeId() {
		return $this->m_intMaintenanceRequestTypeId;
	}

	public function sqlMaintenanceRequestTypeId() {
		return ( true == isset( $this->m_intMaintenanceRequestTypeId ) ) ? ( string ) $this->m_intMaintenanceRequestTypeId : 'NULL';
	}

	public function setMaintenanceStatusId( $intMaintenanceStatusId ) {
		$this->set( 'm_intMaintenanceStatusId', CStrings::strToIntDef( $intMaintenanceStatusId, NULL, false ) );
	}

	public function getMaintenanceStatusId() {
		return $this->m_intMaintenanceStatusId;
	}

	public function sqlMaintenanceStatusId() {
		return ( true == isset( $this->m_intMaintenanceStatusId ) ) ? ( string ) $this->m_intMaintenanceStatusId : 'NULL';
	}

	public function setDueIntervalTypeId( $intDueIntervalTypeId ) {
		$this->set( 'm_intDueIntervalTypeId', CStrings::strToIntDef( $intDueIntervalTypeId, NULL, false ) );
	}

	public function getDueIntervalTypeId() {
		return $this->m_intDueIntervalTypeId;
	}

	public function sqlDueIntervalTypeId() {
		return ( true == isset( $this->m_intDueIntervalTypeId ) ) ? ( string ) $this->m_intDueIntervalTypeId : '3';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 50, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setDaysToComplete( $intDaysToComplete ) {
		$this->set( 'm_intDaysToComplete', CStrings::strToIntDef( $intDaysToComplete, NULL, false ) );
	}

	public function getDaysToComplete() {
		return $this->m_intDaysToComplete;
	}

	public function sqlDaysToComplete() {
		return ( true == isset( $this->m_intDaysToComplete ) ) ? ( string ) $this->m_intDaysToComplete : 'NULL';
	}

	public function setDueInterval( $fltDueInterval ) {
		$this->set( 'm_fltDueInterval', CStrings::strToFloatDef( $fltDueInterval, NULL, false, 0 ) );
	}

	public function getDueInterval() {
		return $this->m_fltDueInterval;
	}

	public function sqlDueInterval() {
		return ( true == isset( $this->m_fltDueInterval ) ) ? ( string ) $this->m_fltDueInterval : 'NULL';
	}

	public function setExcludeWeekendsHolidays( $boolExcludeWeekendsHolidays ) {
		$this->set( 'm_boolExcludeWeekendsHolidays', CStrings::strToBool( $boolExcludeWeekendsHolidays ) );
	}

	public function getExcludeWeekendsHolidays() {
		return $this->m_boolExcludeWeekendsHolidays;
	}

	public function sqlExcludeWeekendsHolidays() {
		return ( true == isset( $this->m_boolExcludeWeekendsHolidays ) ) ? '\'' . ( true == ( bool ) $this->m_boolExcludeWeekendsHolidays ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setEstimatedHours( $intEstimatedHours ) {
		$this->set( 'm_intEstimatedHours', CStrings::strToIntDef( $intEstimatedHours, NULL, false ) );
	}

	public function getEstimatedHours() {
		return $this->m_intEstimatedHours;
	}

	public function sqlEstimatedHours() {
		return ( true == isset( $this->m_intEstimatedHours ) ) ? ( string ) $this->m_intEstimatedHours : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setOccupancyTypeIds( $arrintOccupancyTypeIds ) {
		$this->set( 'm_arrintOccupancyTypeIds', CStrings::strToArrIntDef( $arrintOccupancyTypeIds, NULL ) );
	}

	public function getOccupancyTypeIds() {
		return $this->m_arrintOccupancyTypeIds;
	}

	public function sqlOccupancyTypeIds() {
		return ( true == isset( $this->m_arrintOccupancyTypeIds ) && true == valArr( $this->m_arrintOccupancyTypeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintOccupancyTypeIds, NULL ) . '\'' : 'NULL';
	}

	public function setIsMoveOutDateAsStartDate( $boolIsMoveOutDateAsStartDate ) {
		$this->set( 'm_boolIsMoveOutDateAsStartDate', CStrings::strToBool( $boolIsMoveOutDateAsStartDate ) );
	}

	public function getIsMoveOutDateAsStartDate() {
		return $this->m_boolIsMoveOutDateAsStartDate;
	}

	public function sqlIsMoveOutDateAsStartDate() {
		return ( true == isset( $this->m_boolIsMoveOutDateAsStartDate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsMoveOutDateAsStartDate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, parent_maintenance_template_id, maintenance_priority_id, maintenance_location_id, maintenance_problem_id, maintenance_request_type_id, maintenance_status_id, due_interval_type_id, title, description, days_to_complete, due_interval, exclude_weekends_holidays, estimated_hours, order_num, is_published, updated_by, updated_on, created_by, created_on, details, occupancy_type_ids, is_move_out_date_as_start_date )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlParentMaintenanceTemplateId() . ', ' .
						$this->sqlMaintenancePriorityId() . ', ' .
						$this->sqlMaintenanceLocationId() . ', ' .
						$this->sqlMaintenanceProblemId() . ', ' .
						$this->sqlMaintenanceRequestTypeId() . ', ' .
						$this->sqlMaintenanceStatusId() . ', ' .
						$this->sqlDueIntervalTypeId() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlDaysToComplete() . ', ' .
						$this->sqlDueInterval() . ', ' .
						$this->sqlExcludeWeekendsHolidays() . ', ' .
						$this->sqlEstimatedHours() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlOccupancyTypeIds() . ', ' .
						$this->sqlIsMoveOutDateAsStartDate() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_maintenance_template_id = ' . $this->sqlParentMaintenanceTemplateId(). ',' ; } elseif( true == array_key_exists( 'ParentMaintenanceTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' parent_maintenance_template_id = ' . $this->sqlParentMaintenanceTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_priority_id = ' . $this->sqlMaintenancePriorityId(). ',' ; } elseif( true == array_key_exists( 'MaintenancePriorityId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_priority_id = ' . $this->sqlMaintenancePriorityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_location_id = ' . $this->sqlMaintenanceLocationId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceLocationId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_location_id = ' . $this->sqlMaintenanceLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_problem_id = ' . $this->sqlMaintenanceProblemId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceProblemId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_problem_id = ' . $this->sqlMaintenanceProblemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_request_type_id = ' . $this->sqlMaintenanceRequestTypeId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceRequestTypeId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_request_type_id = ' . $this->sqlMaintenanceRequestTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_status_id = ' . $this->sqlMaintenanceStatusId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceStatusId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_status_id = ' . $this->sqlMaintenanceStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_interval_type_id = ' . $this->sqlDueIntervalTypeId(). ',' ; } elseif( true == array_key_exists( 'DueIntervalTypeId', $this->getChangedColumns() ) ) { $strSql .= ' due_interval_type_id = ' . $this->sqlDueIntervalTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' days_to_complete = ' . $this->sqlDaysToComplete(). ',' ; } elseif( true == array_key_exists( 'DaysToComplete', $this->getChangedColumns() ) ) { $strSql .= ' days_to_complete = ' . $this->sqlDaysToComplete() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_interval = ' . $this->sqlDueInterval(). ',' ; } elseif( true == array_key_exists( 'DueInterval', $this->getChangedColumns() ) ) { $strSql .= ' due_interval = ' . $this->sqlDueInterval() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exclude_weekends_holidays = ' . $this->sqlExcludeWeekendsHolidays(). ',' ; } elseif( true == array_key_exists( 'ExcludeWeekendsHolidays', $this->getChangedColumns() ) ) { $strSql .= ' exclude_weekends_holidays = ' . $this->sqlExcludeWeekendsHolidays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' estimated_hours = ' . $this->sqlEstimatedHours(). ',' ; } elseif( true == array_key_exists( 'EstimatedHours', $this->getChangedColumns() ) ) { $strSql .= ' estimated_hours = ' . $this->sqlEstimatedHours() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_ids = ' . $this->sqlOccupancyTypeIds(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeIds', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_ids = ' . $this->sqlOccupancyTypeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_move_out_date_as_start_date = ' . $this->sqlIsMoveOutDateAsStartDate(). ',' ; } elseif( true == array_key_exists( 'IsMoveOutDateAsStartDate', $this->getChangedColumns() ) ) { $strSql .= ' is_move_out_date_as_start_date = ' . $this->sqlIsMoveOutDateAsStartDate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'parent_maintenance_template_id' => $this->getParentMaintenanceTemplateId(),
			'maintenance_priority_id' => $this->getMaintenancePriorityId(),
			'maintenance_location_id' => $this->getMaintenanceLocationId(),
			'maintenance_problem_id' => $this->getMaintenanceProblemId(),
			'maintenance_request_type_id' => $this->getMaintenanceRequestTypeId(),
			'maintenance_status_id' => $this->getMaintenanceStatusId(),
			'due_interval_type_id' => $this->getDueIntervalTypeId(),
			'title' => $this->getTitle(),
			'description' => $this->getDescription(),
			'days_to_complete' => $this->getDaysToComplete(),
			'due_interval' => $this->getDueInterval(),
			'exclude_weekends_holidays' => $this->getExcludeWeekendsHolidays(),
			'estimated_hours' => $this->getEstimatedHours(),
			'order_num' => $this->getOrderNum(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'occupancy_type_ids' => $this->getOccupancyTypeIds(),
			'is_move_out_date_as_start_date' => $this->getIsMoveOutDateAsStartDate()
		);
	}

}
?>