<?php

class CBaseDocumentTypeLibrary extends CEosSingularBase {

	const TABLE_NAME = 'public.document_type_libraries';

	protected $m_intId;
	protected $m_intDocumentTypeId;
	protected $m_intDocumentSubTypeId;
	protected $m_intDocumentLibraryId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['document_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentTypeId', trim( $arrValues['document_type_id'] ) ); elseif( isset( $arrValues['document_type_id'] ) ) $this->setDocumentTypeId( $arrValues['document_type_id'] );
		if( isset( $arrValues['document_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentSubTypeId', trim( $arrValues['document_sub_type_id'] ) ); elseif( isset( $arrValues['document_sub_type_id'] ) ) $this->setDocumentSubTypeId( $arrValues['document_sub_type_id'] );
		if( isset( $arrValues['document_library_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentLibraryId', trim( $arrValues['document_library_id'] ) ); elseif( isset( $arrValues['document_library_id'] ) ) $this->setDocumentLibraryId( $arrValues['document_library_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDocumentTypeId( $intDocumentTypeId ) {
		$this->set( 'm_intDocumentTypeId', CStrings::strToIntDef( $intDocumentTypeId, NULL, false ) );
	}

	public function getDocumentTypeId() {
		return $this->m_intDocumentTypeId;
	}

	public function sqlDocumentTypeId() {
		return ( true == isset( $this->m_intDocumentTypeId ) ) ? ( string ) $this->m_intDocumentTypeId : 'NULL';
	}

	public function setDocumentSubTypeId( $intDocumentSubTypeId ) {
		$this->set( 'm_intDocumentSubTypeId', CStrings::strToIntDef( $intDocumentSubTypeId, NULL, false ) );
	}

	public function getDocumentSubTypeId() {
		return $this->m_intDocumentSubTypeId;
	}

	public function sqlDocumentSubTypeId() {
		return ( true == isset( $this->m_intDocumentSubTypeId ) ) ? ( string ) $this->m_intDocumentSubTypeId : 'NULL';
	}

	public function setDocumentLibraryId( $intDocumentLibraryId ) {
		$this->set( 'm_intDocumentLibraryId', CStrings::strToIntDef( $intDocumentLibraryId, NULL, false ) );
	}

	public function getDocumentLibraryId() {
		return $this->m_intDocumentLibraryId;
	}

	public function sqlDocumentLibraryId() {
		return ( true == isset( $this->m_intDocumentLibraryId ) ) ? ( string ) $this->m_intDocumentLibraryId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, document_type_id, document_sub_type_id, document_library_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlDocumentTypeId() . ', ' .
 						$this->sqlDocumentSubTypeId() . ', ' .
 						$this->sqlDocumentLibraryId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_type_id = ' . $this->sqlDocumentTypeId() . ','; } elseif( true == array_key_exists( 'DocumentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' document_type_id = ' . $this->sqlDocumentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_sub_type_id = ' . $this->sqlDocumentSubTypeId() . ','; } elseif( true == array_key_exists( 'DocumentSubTypeId', $this->getChangedColumns() ) ) { $strSql .= ' document_sub_type_id = ' . $this->sqlDocumentSubTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_library_id = ' . $this->sqlDocumentLibraryId() . ','; } elseif( true == array_key_exists( 'DocumentLibraryId', $this->getChangedColumns() ) ) { $strSql .= ' document_library_id = ' . $this->sqlDocumentLibraryId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'document_type_id' => $this->getDocumentTypeId(),
			'document_sub_type_id' => $this->getDocumentSubTypeId(),
			'document_library_id' => $this->getDocumentLibraryId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>