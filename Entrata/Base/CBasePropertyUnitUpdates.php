<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyUnitUpdates
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyUnitUpdates extends CEosPluralBase {

	/**
	 * @return CPropertyUnitUpdate[]
	 */
	public static function fetchPropertyUnitUpdates( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPropertyUnitUpdate::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyUnitUpdate
	 */
	public static function fetchPropertyUnitUpdate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyUnitUpdate::class, $objDatabase );
	}

	public static function fetchPropertyUnitUpdateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_unit_updates', $objDatabase );
	}

	public static function fetchPropertyUnitUpdateByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyUnitUpdate( sprintf( 'SELECT * FROM property_unit_updates WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyUnitUpdatesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyUnitUpdates( sprintf( 'SELECT * FROM property_unit_updates WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchPropertyUnitUpdatesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyUnitUpdates( sprintf( 'SELECT * FROM property_unit_updates WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyUnitUpdatesByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchPropertyUnitUpdates( sprintf( 'SELECT * FROM property_unit_updates WHERE unit_space_id = %d AND cid = %d', $intUnitSpaceId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyUnitUpdatesByUnitNumberPatternIdByCid( $intUnitNumberPatternId, $intCid, $objDatabase ) {
		return self::fetchPropertyUnitUpdates( sprintf( 'SELECT * FROM property_unit_updates WHERE unit_number_pattern_id = %d AND cid = %d', $intUnitNumberPatternId, $intCid ), $objDatabase );
	}

}
?>