<?php

class CBaseDefaultArRules extends CEosPluralBase {

    const TABLE_DEFAULT_AR_RULES = 'public.default_ar_rules';

    public static function fetchDefaultArRules( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CDefaultArRule', $objDatabase );
    }

    public static function fetchDefaultArRule( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CDefaultArRule', $objDatabase );
    }

    public static function fetchDefaultArRuleCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'default_ar_rules', $objDatabase );
    }

    public static function fetchDefaultArRuleById( $intId, $objDatabase ) {
        return self::fetchDefaultArRule( sprintf( 'SELECT * FROM default_ar_rules WHERE id = %d', (int) $intId ), $objDatabase );
    }

    public static function fetchDefaultArRulesByArOriginId( $intArOriginId, $objDatabase ) {
        return self::fetchDefaultArRules( sprintf( 'SELECT * FROM default_ar_rules WHERE ar_origin_id = %d', (int) $intArOriginId ), $objDatabase );
    }

    public static function fetchDefaultArRulesByArCodeTypeId( $intArCodeTypeId, $objDatabase ) {
        return self::fetchDefaultArRules( sprintf( 'SELECT * FROM default_ar_rules WHERE ar_code_type_id = %d', (int) $intArCodeTypeId ), $objDatabase );
    }

    public static function fetchDefaultArRulesByArTriggerId( $intArTriggerId, $objDatabase ) {
        return self::fetchDefaultArRules( sprintf( 'SELECT * FROM default_ar_rules WHERE ar_trigger_id = %d', (int) $intArTriggerId ), $objDatabase );
    }

    public static function fetchDefaultArRulesByArActionId( $intArActionId, $objDatabase ) {
        return self::fetchDefaultArRules( sprintf( 'SELECT * FROM default_ar_rules WHERE ar_action_id = %d', (int) $intArActionId ), $objDatabase );
    }

}
?>