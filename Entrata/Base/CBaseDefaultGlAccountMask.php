<?php

class CBaseDefaultGlAccountMask extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_gl_account_masks';

	protected $m_intId;
	protected $m_intDefaultGlChartId;
	protected $m_intDefaultGlTreeId;
	protected $m_intDefaultGlGroupId;
	protected $m_intDefaultGlAccountId;
	protected $m_strName;
	protected $m_strAccountNumber;
	protected $m_strDescription;
	protected $m_intHideIfZero;
	protected $m_intIsSystem;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intHideIfZero = '0';
		$this->m_intIsSystem = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['default_gl_chart_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultGlChartId', trim( $arrValues['default_gl_chart_id'] ) ); elseif( isset( $arrValues['default_gl_chart_id'] ) ) $this->setDefaultGlChartId( $arrValues['default_gl_chart_id'] );
		if( isset( $arrValues['default_gl_tree_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultGlTreeId', trim( $arrValues['default_gl_tree_id'] ) ); elseif( isset( $arrValues['default_gl_tree_id'] ) ) $this->setDefaultGlTreeId( $arrValues['default_gl_tree_id'] );
		if( isset( $arrValues['default_gl_group_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultGlGroupId', trim( $arrValues['default_gl_group_id'] ) ); elseif( isset( $arrValues['default_gl_group_id'] ) ) $this->setDefaultGlGroupId( $arrValues['default_gl_group_id'] );
		if( isset( $arrValues['default_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultGlAccountId', trim( $arrValues['default_gl_account_id'] ) ); elseif( isset( $arrValues['default_gl_account_id'] ) ) $this->setDefaultGlAccountId( $arrValues['default_gl_account_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['account_number'] ) && $boolDirectSet ) $this->set( 'm_strAccountNumber', trim( $arrValues['account_number'] ) ); elseif( isset( $arrValues['account_number'] ) ) $this->setAccountNumber( $arrValues['account_number'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['hide_if_zero'] ) && $boolDirectSet ) $this->set( 'm_intHideIfZero', trim( $arrValues['hide_if_zero'] ) ); elseif( isset( $arrValues['hide_if_zero'] ) ) $this->setHideIfZero( $arrValues['hide_if_zero'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDefaultGlChartId( $intDefaultGlChartId ) {
		$this->set( 'm_intDefaultGlChartId', CStrings::strToIntDef( $intDefaultGlChartId, NULL, false ) );
	}

	public function getDefaultGlChartId() {
		return $this->m_intDefaultGlChartId;
	}

	public function sqlDefaultGlChartId() {
		return ( true == isset( $this->m_intDefaultGlChartId ) ) ? ( string ) $this->m_intDefaultGlChartId : 'NULL';
	}

	public function setDefaultGlTreeId( $intDefaultGlTreeId ) {
		$this->set( 'm_intDefaultGlTreeId', CStrings::strToIntDef( $intDefaultGlTreeId, NULL, false ) );
	}

	public function getDefaultGlTreeId() {
		return $this->m_intDefaultGlTreeId;
	}

	public function sqlDefaultGlTreeId() {
		return ( true == isset( $this->m_intDefaultGlTreeId ) ) ? ( string ) $this->m_intDefaultGlTreeId : 'NULL';
	}

	public function setDefaultGlGroupId( $intDefaultGlGroupId ) {
		$this->set( 'm_intDefaultGlGroupId', CStrings::strToIntDef( $intDefaultGlGroupId, NULL, false ) );
	}

	public function getDefaultGlGroupId() {
		return $this->m_intDefaultGlGroupId;
	}

	public function sqlDefaultGlGroupId() {
		return ( true == isset( $this->m_intDefaultGlGroupId ) ) ? ( string ) $this->m_intDefaultGlGroupId : 'NULL';
	}

	public function setDefaultGlAccountId( $intDefaultGlAccountId ) {
		$this->set( 'm_intDefaultGlAccountId', CStrings::strToIntDef( $intDefaultGlAccountId, NULL, false ) );
	}

	public function getDefaultGlAccountId() {
		return $this->m_intDefaultGlAccountId;
	}

	public function sqlDefaultGlAccountId() {
		return ( true == isset( $this->m_intDefaultGlAccountId ) ) ? ( string ) $this->m_intDefaultGlAccountId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setAccountNumber( $strAccountNumber ) {
		$this->set( 'm_strAccountNumber', CStrings::strTrimDef( $strAccountNumber, 50, NULL, true ) );
	}

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function sqlAccountNumber() {
		return ( true == isset( $this->m_strAccountNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAccountNumber ) : '\'' . addslashes( $this->m_strAccountNumber ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setHideIfZero( $intHideIfZero ) {
		$this->set( 'm_intHideIfZero', CStrings::strToIntDef( $intHideIfZero, NULL, false ) );
	}

	public function getHideIfZero() {
		return $this->m_intHideIfZero;
	}

	public function sqlHideIfZero() {
		return ( true == isset( $this->m_intHideIfZero ) ) ? ( string ) $this->m_intHideIfZero : '0';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'default_gl_chart_id' => $this->getDefaultGlChartId(),
			'default_gl_tree_id' => $this->getDefaultGlTreeId(),
			'default_gl_group_id' => $this->getDefaultGlGroupId(),
			'default_gl_account_id' => $this->getDefaultGlAccountId(),
			'name' => $this->getName(),
			'account_number' => $this->getAccountNumber(),
			'description' => $this->getDescription(),
			'hide_if_zero' => $this->getHideIfZero(),
			'is_system' => $this->getIsSystem(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>