<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserTypes
 * Do not add any new functions to this class.
 */

class CBaseCompanyUserTypes extends CEosPluralBase {

	/**
	 * @return CCompanyUserType[]
	 */
	public static function fetchCompanyUserTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCompanyUserType::class, $objDatabase );
	}

	/**
	 * @return CCompanyUserType
	 */
	public static function fetchCompanyUserType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyUserType::class, $objDatabase );
	}

	public static function fetchCompanyUserTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_user_types', $objDatabase );
	}

	public static function fetchCompanyUserTypeById( $intId, $objDatabase ) {
		return self::fetchCompanyUserType( sprintf( 'SELECT * FROM company_user_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>