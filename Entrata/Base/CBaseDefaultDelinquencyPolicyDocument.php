<?php

class CBaseDefaultDelinquencyPolicyDocument extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_delinquency_policy_documents';

	protected $m_intId;
	protected $m_intDefaultDelinquencyPolicyId;
	protected $m_intDefaultFileTypeId;
	protected $m_intDelinquencyThresholdTypeId;
	protected $m_strDocumentName;
	protected $m_intThresholdOffset;
	protected $m_intResendFrequency;
	protected $m_boolIsAutoSent;
	protected $m_boolIsSetCertifiedFundsOnly;
	protected $m_boolIsStartEviction;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intThresholdOffset = '0';
		$this->m_intResendFrequency = '0';
		$this->m_boolIsAutoSent = false;
		$this->m_boolIsSetCertifiedFundsOnly = false;
		$this->m_boolIsStartEviction = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['default_delinquency_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultDelinquencyPolicyId', trim( $arrValues['default_delinquency_policy_id'] ) ); elseif( isset( $arrValues['default_delinquency_policy_id'] ) ) $this->setDefaultDelinquencyPolicyId( $arrValues['default_delinquency_policy_id'] );
		if( isset( $arrValues['default_file_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultFileTypeId', trim( $arrValues['default_file_type_id'] ) ); elseif( isset( $arrValues['default_file_type_id'] ) ) $this->setDefaultFileTypeId( $arrValues['default_file_type_id'] );
		if( isset( $arrValues['delinquency_threshold_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDelinquencyThresholdTypeId', trim( $arrValues['delinquency_threshold_type_id'] ) ); elseif( isset( $arrValues['delinquency_threshold_type_id'] ) ) $this->setDelinquencyThresholdTypeId( $arrValues['delinquency_threshold_type_id'] );
		if( isset( $arrValues['document_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDocumentName', trim( stripcslashes( $arrValues['document_name'] ) ) ); elseif( isset( $arrValues['document_name'] ) ) $this->setDocumentName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['document_name'] ) : $arrValues['document_name'] );
		if( isset( $arrValues['threshold_offset'] ) && $boolDirectSet ) $this->set( 'm_intThresholdOffset', trim( $arrValues['threshold_offset'] ) ); elseif( isset( $arrValues['threshold_offset'] ) ) $this->setThresholdOffset( $arrValues['threshold_offset'] );
		if( isset( $arrValues['resend_frequency'] ) && $boolDirectSet ) $this->set( 'm_intResendFrequency', trim( $arrValues['resend_frequency'] ) ); elseif( isset( $arrValues['resend_frequency'] ) ) $this->setResendFrequency( $arrValues['resend_frequency'] );
		if( isset( $arrValues['is_auto_sent'] ) && $boolDirectSet ) $this->set( 'm_boolIsAutoSent', trim( stripcslashes( $arrValues['is_auto_sent'] ) ) ); elseif( isset( $arrValues['is_auto_sent'] ) ) $this->setIsAutoSent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_auto_sent'] ) : $arrValues['is_auto_sent'] );
		if( isset( $arrValues['is_set_certified_funds_only'] ) && $boolDirectSet ) $this->set( 'm_boolIsSetCertifiedFundsOnly', trim( stripcslashes( $arrValues['is_set_certified_funds_only'] ) ) ); elseif( isset( $arrValues['is_set_certified_funds_only'] ) ) $this->setIsSetCertifiedFundsOnly( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_set_certified_funds_only'] ) : $arrValues['is_set_certified_funds_only'] );
		if( isset( $arrValues['is_start_eviction'] ) && $boolDirectSet ) $this->set( 'm_boolIsStartEviction', trim( stripcslashes( $arrValues['is_start_eviction'] ) ) ); elseif( isset( $arrValues['is_start_eviction'] ) ) $this->setIsStartEviction( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_start_eviction'] ) : $arrValues['is_start_eviction'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDefaultDelinquencyPolicyId( $intDefaultDelinquencyPolicyId ) {
		$this->set( 'm_intDefaultDelinquencyPolicyId', CStrings::strToIntDef( $intDefaultDelinquencyPolicyId, NULL, false ) );
	}

	public function getDefaultDelinquencyPolicyId() {
		return $this->m_intDefaultDelinquencyPolicyId;
	}

	public function sqlDefaultDelinquencyPolicyId() {
		return ( true == isset( $this->m_intDefaultDelinquencyPolicyId ) ) ? ( string ) $this->m_intDefaultDelinquencyPolicyId : 'NULL';
	}

	public function setDefaultFileTypeId( $intDefaultFileTypeId ) {
		$this->set( 'm_intDefaultFileTypeId', CStrings::strToIntDef( $intDefaultFileTypeId, NULL, false ) );
	}

	public function getDefaultFileTypeId() {
		return $this->m_intDefaultFileTypeId;
	}

	public function sqlDefaultFileTypeId() {
		return ( true == isset( $this->m_intDefaultFileTypeId ) ) ? ( string ) $this->m_intDefaultFileTypeId : 'NULL';
	}

	public function setDelinquencyThresholdTypeId( $intDelinquencyThresholdTypeId ) {
		$this->set( 'm_intDelinquencyThresholdTypeId', CStrings::strToIntDef( $intDelinquencyThresholdTypeId, NULL, false ) );
	}

	public function getDelinquencyThresholdTypeId() {
		return $this->m_intDelinquencyThresholdTypeId;
	}

	public function sqlDelinquencyThresholdTypeId() {
		return ( true == isset( $this->m_intDelinquencyThresholdTypeId ) ) ? ( string ) $this->m_intDelinquencyThresholdTypeId : 'NULL';
	}

	public function setDocumentName( $strDocumentName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDocumentName', CStrings::strTrimDef( $strDocumentName, 200, NULL, true ), $strLocaleCode );
	}

	public function getDocumentName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDocumentName', $strLocaleCode );
	}

	public function sqlDocumentName() {
		return ( true == isset( $this->m_strDocumentName ) ) ? '\'' . addslashes( $this->m_strDocumentName ) . '\'' : 'NULL';
	}

	public function setThresholdOffset( $intThresholdOffset ) {
		$this->set( 'm_intThresholdOffset', CStrings::strToIntDef( $intThresholdOffset, NULL, false ) );
	}

	public function getThresholdOffset() {
		return $this->m_intThresholdOffset;
	}

	public function sqlThresholdOffset() {
		return ( true == isset( $this->m_intThresholdOffset ) ) ? ( string ) $this->m_intThresholdOffset : '0';
	}

	public function setResendFrequency( $intResendFrequency ) {
		$this->set( 'm_intResendFrequency', CStrings::strToIntDef( $intResendFrequency, NULL, false ) );
	}

	public function getResendFrequency() {
		return $this->m_intResendFrequency;
	}

	public function sqlResendFrequency() {
		return ( true == isset( $this->m_intResendFrequency ) ) ? ( string ) $this->m_intResendFrequency : '0';
	}

	public function setIsAutoSent( $boolIsAutoSent ) {
		$this->set( 'm_boolIsAutoSent', CStrings::strToBool( $boolIsAutoSent ) );
	}

	public function getIsAutoSent() {
		return $this->m_boolIsAutoSent;
	}

	public function sqlIsAutoSent() {
		return ( true == isset( $this->m_boolIsAutoSent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAutoSent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSetCertifiedFundsOnly( $boolIsSetCertifiedFundsOnly ) {
		$this->set( 'm_boolIsSetCertifiedFundsOnly', CStrings::strToBool( $boolIsSetCertifiedFundsOnly ) );
	}

	public function getIsSetCertifiedFundsOnly() {
		return $this->m_boolIsSetCertifiedFundsOnly;
	}

	public function sqlIsSetCertifiedFundsOnly() {
		return ( true == isset( $this->m_boolIsSetCertifiedFundsOnly ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSetCertifiedFundsOnly ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsStartEviction( $boolIsStartEviction ) {
		$this->set( 'm_boolIsStartEviction', CStrings::strToBool( $boolIsStartEviction ) );
	}

	public function getIsStartEviction() {
		return $this->m_boolIsStartEviction;
	}

	public function sqlIsStartEviction() {
		return ( true == isset( $this->m_boolIsStartEviction ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsStartEviction ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'default_delinquency_policy_id' => $this->getDefaultDelinquencyPolicyId(),
			'default_file_type_id' => $this->getDefaultFileTypeId(),
			'delinquency_threshold_type_id' => $this->getDelinquencyThresholdTypeId(),
			'document_name' => $this->getDocumentName(),
			'threshold_offset' => $this->getThresholdOffset(),
			'resend_frequency' => $this->getResendFrequency(),
			'is_auto_sent' => $this->getIsAutoSent(),
			'is_set_certified_funds_only' => $this->getIsSetCertifiedFundsOnly(),
			'is_start_eviction' => $this->getIsStartEviction(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>