<?php

class CBaseDefaultGlAccount extends CEosSingularBase {

	const TABLE_NAME = 'public.default_gl_accounts';

	protected $m_intId;
	protected $m_intDefaultGlChartId;
	protected $m_intGlGroupTypeId;
	protected $m_intCashFlowTypeId;
	protected $m_intGlAccountTypeId;
	protected $m_intCamAllowanceTypeId;
	protected $m_strSystemCode;
	protected $m_boolRestrictForAp;
	protected $m_boolRestrictForGl;
	protected $m_boolIsConfidential;
	protected $m_intGlAccountUsageTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_intCamAllowanceTypeId = '1';
		$this->m_boolRestrictForAp = false;
		$this->m_boolRestrictForGl = false;
		$this->m_boolIsConfidential = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['default_gl_chart_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultGlChartId', trim( $arrValues['default_gl_chart_id'] ) ); elseif( isset( $arrValues['default_gl_chart_id'] ) ) $this->setDefaultGlChartId( $arrValues['default_gl_chart_id'] );
		if( isset( $arrValues['gl_group_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlGroupTypeId', trim( $arrValues['gl_group_type_id'] ) ); elseif( isset( $arrValues['gl_group_type_id'] ) ) $this->setGlGroupTypeId( $arrValues['gl_group_type_id'] );
		if( isset( $arrValues['cash_flow_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCashFlowTypeId', trim( $arrValues['cash_flow_type_id'] ) ); elseif( isset( $arrValues['cash_flow_type_id'] ) ) $this->setCashFlowTypeId( $arrValues['cash_flow_type_id'] );
		if( isset( $arrValues['gl_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlAccountTypeId', trim( $arrValues['gl_account_type_id'] ) ); elseif( isset( $arrValues['gl_account_type_id'] ) ) $this->setGlAccountTypeId( $arrValues['gl_account_type_id'] );
		if( isset( $arrValues['cam_allowance_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCamAllowanceTypeId', trim( $arrValues['cam_allowance_type_id'] ) ); elseif( isset( $arrValues['cam_allowance_type_id'] ) ) $this->setCamAllowanceTypeId( $arrValues['cam_allowance_type_id'] );
		if( isset( $arrValues['system_code'] ) && $boolDirectSet ) $this->set( 'm_strSystemCode', trim( $arrValues['system_code'] ) ); elseif( isset( $arrValues['system_code'] ) ) $this->setSystemCode( $arrValues['system_code'] );
		if( isset( $arrValues['restrict_for_ap'] ) && $boolDirectSet ) $this->set( 'm_boolRestrictForAp', trim( stripcslashes( $arrValues['restrict_for_ap'] ) ) ); elseif( isset( $arrValues['restrict_for_ap'] ) ) $this->setRestrictForAp( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['restrict_for_ap'] ) : $arrValues['restrict_for_ap'] );
		if( isset( $arrValues['restrict_for_gl'] ) && $boolDirectSet ) $this->set( 'm_boolRestrictForGl', trim( stripcslashes( $arrValues['restrict_for_gl'] ) ) ); elseif( isset( $arrValues['restrict_for_gl'] ) ) $this->setRestrictForGl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['restrict_for_gl'] ) : $arrValues['restrict_for_gl'] );
		if( isset( $arrValues['is_confidential'] ) && $boolDirectSet ) $this->set( 'm_boolIsConfidential', trim( stripcslashes( $arrValues['is_confidential'] ) ) ); elseif( isset( $arrValues['is_confidential'] ) ) $this->setIsConfidential( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_confidential'] ) : $arrValues['is_confidential'] );
		if( isset( $arrValues['gl_account_usage_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlAccountUsageTypeId', trim( $arrValues['gl_account_usage_type_id'] ) ); elseif( isset( $arrValues['gl_account_usage_type_id'] ) ) $this->setGlAccountUsageTypeId( $arrValues['gl_account_usage_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDefaultGlChartId( $intDefaultGlChartId ) {
		$this->set( 'm_intDefaultGlChartId', CStrings::strToIntDef( $intDefaultGlChartId, NULL, false ) );
	}

	public function getDefaultGlChartId() {
		return $this->m_intDefaultGlChartId;
	}

	public function sqlDefaultGlChartId() {
		return ( true == isset( $this->m_intDefaultGlChartId ) ) ? ( string ) $this->m_intDefaultGlChartId : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function setGlGroupTypeId( $intGlGroupTypeId ) {
		$this->set( 'm_intGlGroupTypeId', CStrings::strToIntDef( $intGlGroupTypeId, NULL, false ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function getGlGroupTypeId() {
		return $this->m_intGlGroupTypeId;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function sqlGlGroupTypeId() {
		return ( true == isset( $this->m_intGlGroupTypeId ) ) ? ( string ) $this->m_intGlGroupTypeId : 'NULL';
	}

	public function setCashFlowTypeId( $intCashFlowTypeId ) {
		$this->set( 'm_intCashFlowTypeId', CStrings::strToIntDef( $intCashFlowTypeId, NULL, false ) );
	}

	public function getCashFlowTypeId() {
		return $this->m_intCashFlowTypeId;
	}

	public function sqlCashFlowTypeId() {
		return ( true == isset( $this->m_intCashFlowTypeId ) ) ? ( string ) $this->m_intCashFlowTypeId : 'NULL';
	}

	public function setGlAccountTypeId( $intGlAccountTypeId ) {
		$this->set( 'm_intGlAccountTypeId', CStrings::strToIntDef( $intGlAccountTypeId, NULL, false ) );
	}

	public function getGlAccountTypeId() {
		return $this->m_intGlAccountTypeId;
	}

	public function sqlGlAccountTypeId() {
		return ( true == isset( $this->m_intGlAccountTypeId ) ) ? ( string ) $this->m_intGlAccountTypeId : 'NULL';
	}

	public function setCamAllowanceTypeId( $intCamAllowanceTypeId ) {
		$this->set( 'm_intCamAllowanceTypeId', CStrings::strToIntDef( $intCamAllowanceTypeId, NULL, false ) );
	}

	public function getCamAllowanceTypeId() {
		return $this->m_intCamAllowanceTypeId;
	}

	public function sqlCamAllowanceTypeId() {
		return ( true == isset( $this->m_intCamAllowanceTypeId ) ) ? ( string ) $this->m_intCamAllowanceTypeId : '1';
	}

	public function setSystemCode( $strSystemCode ) {
		$this->set( 'm_strSystemCode', CStrings::strTrimDef( $strSystemCode, 15, NULL, true ) );
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	public function sqlSystemCode() {
		return ( true == isset( $this->m_strSystemCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSystemCode ) : '\'' . addslashes( $this->m_strSystemCode ) . '\'' ) : 'NULL';
	}

	public function setRestrictForAp( $boolRestrictForAp ) {
		$this->set( 'm_boolRestrictForAp', CStrings::strToBool( $boolRestrictForAp ) );
	}

	public function getRestrictForAp() {
		return $this->m_boolRestrictForAp;
	}

	public function sqlRestrictForAp() {
		return ( true == isset( $this->m_boolRestrictForAp ) ) ? '\'' . ( true == ( bool ) $this->m_boolRestrictForAp ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRestrictForGl( $boolRestrictForGl ) {
		$this->set( 'm_boolRestrictForGl', CStrings::strToBool( $boolRestrictForGl ) );
	}

	public function getRestrictForGl() {
		return $this->m_boolRestrictForGl;
	}

	public function sqlRestrictForGl() {
		return ( true == isset( $this->m_boolRestrictForGl ) ) ? '\'' . ( true == ( bool ) $this->m_boolRestrictForGl ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsConfidential( $boolIsConfidential ) {
		$this->set( 'm_boolIsConfidential', CStrings::strToBool( $boolIsConfidential ) );
	}

	public function getIsConfidential() {
		return $this->m_boolIsConfidential;
	}

	public function sqlIsConfidential() {
		return ( true == isset( $this->m_boolIsConfidential ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsConfidential ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setGlAccountUsageTypeId( $intGlAccountUsageTypeId ) {
		$this->set( 'm_intGlAccountUsageTypeId', CStrings::strToIntDef( $intGlAccountUsageTypeId, NULL, false ) );
	}

	public function getGlAccountUsageTypeId() {
		return $this->m_intGlAccountUsageTypeId;
	}

	public function sqlGlAccountUsageTypeId() {
		return ( true == isset( $this->m_intGlAccountUsageTypeId ) ) ? ( string ) $this->m_intGlAccountUsageTypeId : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'default_gl_chart_id' => $this->getDefaultGlChartId(),
			'gl_group_type_id' => $this->getGlGroupTypeId(),
			'cash_flow_type_id' => $this->getCashFlowTypeId(),
			'gl_account_type_id' => $this->getGlAccountTypeId(),
			'cam_allowance_type_id' => $this->getCamAllowanceTypeId(),
			'system_code' => $this->getSystemCode(),
			'restrict_for_ap' => $this->getRestrictForAp(),
			'restrict_for_gl' => $this->getRestrictForGl(),
			'is_confidential' => $this->getIsConfidential(),
			'gl_account_usage_type_id' => $this->getGlAccountUsageTypeId()
		);
	}

}
?>