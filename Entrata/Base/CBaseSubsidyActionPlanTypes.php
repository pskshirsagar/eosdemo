<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyActionPlanTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyActionPlanTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyActionPlanType[]
	 */
	public static function fetchSubsidyActionPlanTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyActionPlanType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyActionPlanType
	 */
	public static function fetchSubsidyActionPlanType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyActionPlanType::class, $objDatabase );
	}

	public static function fetchSubsidyActionPlanTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_action_plan_types', $objDatabase );
	}

	public static function fetchSubsidyActionPlanTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyActionPlanType( sprintf( 'SELECT * FROM subsidy_action_plan_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>