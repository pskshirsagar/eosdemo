<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledTask extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.scheduled_tasks';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDefaultScheduledTaskId;
	protected $m_intScheduledTaskTypeId;
	protected $m_intScheduleTypeId;
	protected $m_intScheduledTaskId;
	protected $m_intScheduledTaskGroupId;
	protected $m_strTitle;
	protected $m_intPropertyId;
	protected $m_arrintPropertyGroupIds;
	protected $m_intEventTypeId;
	protected $m_intEventSubTypeId;
	protected $m_strScheduleDetails;
	protected $m_jsonScheduleDetails;
	protected $m_strTaskDetails;
	protected $m_jsonTaskDetails;
	protected $m_strStartOn;
	protected $m_strEndOn;
	protected $m_strLastRunOn;
	protected $m_strNextRunOn;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_strTitle = NULL;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['default_scheduled_task_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultScheduledTaskId', trim( $arrValues['default_scheduled_task_id'] ) ); elseif( isset( $arrValues['default_scheduled_task_id'] ) ) $this->setDefaultScheduledTaskId( $arrValues['default_scheduled_task_id'] );
		if( isset( $arrValues['scheduled_task_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledTaskTypeId', trim( $arrValues['scheduled_task_type_id'] ) ); elseif( isset( $arrValues['scheduled_task_type_id'] ) ) $this->setScheduledTaskTypeId( $arrValues['scheduled_task_type_id'] );
		if( isset( $arrValues['schedule_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduleTypeId', trim( $arrValues['schedule_type_id'] ) ); elseif( isset( $arrValues['schedule_type_id'] ) ) $this->setScheduleTypeId( $arrValues['schedule_type_id'] );
		if( isset( $arrValues['scheduled_task_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledTaskId', trim( $arrValues['scheduled_task_id'] ) ); elseif( isset( $arrValues['scheduled_task_id'] ) ) $this->setScheduledTaskId( $arrValues['scheduled_task_id'] );
		if( isset( $arrValues['scheduled_task_group_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledTaskGroupId', trim( $arrValues['scheduled_task_group_id'] ) ); elseif( isset( $arrValues['scheduled_task_group_id'] ) ) $this->setScheduledTaskGroupId( $arrValues['scheduled_task_group_id'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_group_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintPropertyGroupIds', trim( $arrValues['property_group_ids'] ) ); elseif( isset( $arrValues['property_group_ids'] ) ) $this->setPropertyGroupIds( $arrValues['property_group_ids'] );
		if( isset( $arrValues['event_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEventTypeId', trim( $arrValues['event_type_id'] ) ); elseif( isset( $arrValues['event_type_id'] ) ) $this->setEventTypeId( $arrValues['event_type_id'] );
		if( isset( $arrValues['event_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEventSubTypeId', trim( $arrValues['event_sub_type_id'] ) ); elseif( isset( $arrValues['event_sub_type_id'] ) ) $this->setEventSubTypeId( $arrValues['event_sub_type_id'] );
		if( isset( $arrValues['schedule_details'] ) ) $this->set( 'm_strScheduleDetails', trim( $arrValues['schedule_details'] ) );
		if( isset( $arrValues['task_details'] ) ) $this->set( 'm_strTaskDetails', trim( $arrValues['task_details'] ) );
		if( isset( $arrValues['start_on'] ) && $boolDirectSet ) $this->set( 'm_strStartOn', trim( $arrValues['start_on'] ) ); elseif( isset( $arrValues['start_on'] ) ) $this->setStartOn( $arrValues['start_on'] );
		if( isset( $arrValues['end_on'] ) && $boolDirectSet ) $this->set( 'm_strEndOn', trim( $arrValues['end_on'] ) ); elseif( isset( $arrValues['end_on'] ) ) $this->setEndOn( $arrValues['end_on'] );
		if( isset( $arrValues['last_run_on'] ) && $boolDirectSet ) $this->set( 'm_strLastRunOn', trim( $arrValues['last_run_on'] ) ); elseif( isset( $arrValues['last_run_on'] ) ) $this->setLastRunOn( $arrValues['last_run_on'] );
		if( isset( $arrValues['next_run_on'] ) && $boolDirectSet ) $this->set( 'm_strNextRunOn', trim( $arrValues['next_run_on'] ) ); elseif( isset( $arrValues['next_run_on'] ) ) $this->setNextRunOn( $arrValues['next_run_on'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDefaultScheduledTaskId( $intDefaultScheduledTaskId ) {
		$this->set( 'm_intDefaultScheduledTaskId', CStrings::strToIntDef( $intDefaultScheduledTaskId, NULL, false ) );
	}

	public function getDefaultScheduledTaskId() {
		return $this->m_intDefaultScheduledTaskId;
	}

	public function sqlDefaultScheduledTaskId() {
		return ( true == isset( $this->m_intDefaultScheduledTaskId ) ) ? ( string ) $this->m_intDefaultScheduledTaskId : 'NULL';
	}

	public function setScheduledTaskTypeId( $intScheduledTaskTypeId ) {
		$this->set( 'm_intScheduledTaskTypeId', CStrings::strToIntDef( $intScheduledTaskTypeId, NULL, false ) );
	}

	public function getScheduledTaskTypeId() {
		return $this->m_intScheduledTaskTypeId;
	}

	public function sqlScheduledTaskTypeId() {
		return ( true == isset( $this->m_intScheduledTaskTypeId ) ) ? ( string ) $this->m_intScheduledTaskTypeId : 'NULL';
	}

	public function setScheduleTypeId( $intScheduleTypeId ) {
		$this->set( 'm_intScheduleTypeId', CStrings::strToIntDef( $intScheduleTypeId, NULL, false ) );
	}

	public function getScheduleTypeId() {
		return $this->m_intScheduleTypeId;
	}

	public function sqlScheduleTypeId() {
		return ( true == isset( $this->m_intScheduleTypeId ) ) ? ( string ) $this->m_intScheduleTypeId : 'NULL';
	}

	public function setScheduledTaskId( $intScheduledTaskId ) {
		$this->set( 'm_intScheduledTaskId', CStrings::strToIntDef( $intScheduledTaskId, NULL, false ) );
	}

	public function getScheduledTaskId() {
		return $this->m_intScheduledTaskId;
	}

	public function sqlScheduledTaskId() {
		return ( true == isset( $this->m_intScheduledTaskId ) ) ? ( string ) $this->m_intScheduledTaskId : 'NULL';
	}

	public function setScheduledTaskGroupId( $intScheduledTaskGroupId ) {
		$this->set( 'm_intScheduledTaskGroupId', CStrings::strToIntDef( $intScheduledTaskGroupId, NULL, false ) );
	}

	public function getScheduledTaskGroupId() {
		return $this->m_intScheduledTaskGroupId;
	}

	public function sqlScheduledTaskGroupId() {
		return ( true == isset( $this->m_intScheduledTaskGroupId ) ) ? ( string ) $this->m_intScheduledTaskGroupId : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 240, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : '\'NULL\'';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyGroupIds( $arrintPropertyGroupIds ) {
		$this->set( 'm_arrintPropertyGroupIds', CStrings::strToArrIntDef( $arrintPropertyGroupIds, NULL ) );
	}

	public function getPropertyGroupIds() {
		return $this->m_arrintPropertyGroupIds;
	}

	public function sqlPropertyGroupIds() {
		return ( true == isset( $this->m_arrintPropertyGroupIds ) && true == valArr( $this->m_arrintPropertyGroupIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintPropertyGroupIds, NULL ) . '\'' : 'NULL';
	}

	public function setEventTypeId( $intEventTypeId ) {
		$this->set( 'm_intEventTypeId', CStrings::strToIntDef( $intEventTypeId, NULL, false ) );
	}

	public function getEventTypeId() {
		return $this->m_intEventTypeId;
	}

	public function sqlEventTypeId() {
		return ( true == isset( $this->m_intEventTypeId ) ) ? ( string ) $this->m_intEventTypeId : 'NULL';
	}

	public function setEventSubTypeId( $intEventSubTypeId ) {
		$this->set( 'm_intEventSubTypeId', CStrings::strToIntDef( $intEventSubTypeId, NULL, false ) );
	}

	public function getEventSubTypeId() {
		return $this->m_intEventSubTypeId;
	}

	public function sqlEventSubTypeId() {
		return ( true == isset( $this->m_intEventSubTypeId ) ) ? ( string ) $this->m_intEventSubTypeId : 'NULL';
	}

	public function setScheduleDetails( $jsonScheduleDetails ) {
		if( true == valObj( $jsonScheduleDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonScheduleDetails', $jsonScheduleDetails );
		} elseif( true == valJsonString( $jsonScheduleDetails ) ) {
			$this->set( 'm_jsonScheduleDetails', CStrings::strToJson( $jsonScheduleDetails ) );
		} else {
			$this->set( 'm_jsonScheduleDetails', NULL ); 
		}
		unset( $this->m_strScheduleDetails );
	}

	public function getScheduleDetails() {
		if( true == isset( $this->m_strScheduleDetails ) ) {
			$this->m_jsonScheduleDetails = CStrings::strToJson( $this->m_strScheduleDetails );
			unset( $this->m_strScheduleDetails );
		}
		return $this->m_jsonScheduleDetails;
	}

	public function sqlScheduleDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getScheduleDetails() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getScheduleDetails() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setTaskDetails( $jsonTaskDetails ) {
		if( true == valObj( $jsonTaskDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonTaskDetails', $jsonTaskDetails );
		} elseif( true == valJsonString( $jsonTaskDetails ) ) {
			$this->set( 'm_jsonTaskDetails', CStrings::strToJson( $jsonTaskDetails ) );
		} else {
			$this->set( 'm_jsonTaskDetails', NULL ); 
		}
		unset( $this->m_strTaskDetails );
	}

	public function getTaskDetails() {
		if( true == isset( $this->m_strTaskDetails ) ) {
			$this->m_jsonTaskDetails = CStrings::strToJson( $this->m_strTaskDetails );
			unset( $this->m_strTaskDetails );
		}
		return $this->m_jsonTaskDetails;
	}

	public function sqlTaskDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getTaskDetails() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getTaskDetails() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setStartOn( $strStartOn ) {
		$this->set( 'm_strStartOn', CStrings::strTrimDef( $strStartOn, -1, NULL, true ) );
	}

	public function getStartOn() {
		return $this->m_strStartOn;
	}

	public function sqlStartOn() {
		return ( true == isset( $this->m_strStartOn ) ) ? '\'' . $this->m_strStartOn . '\'' : 'NULL';
	}

	public function setEndOn( $strEndOn ) {
		$this->set( 'm_strEndOn', CStrings::strTrimDef( $strEndOn, -1, NULL, true ) );
	}

	public function getEndOn() {
		return $this->m_strEndOn;
	}

	public function sqlEndOn() {
		return ( true == isset( $this->m_strEndOn ) ) ? '\'' . $this->m_strEndOn . '\'' : 'NULL';
	}

	public function setLastRunOn( $strLastRunOn ) {
		$this->set( 'm_strLastRunOn', CStrings::strTrimDef( $strLastRunOn, -1, NULL, true ) );
	}

	public function getLastRunOn() {
		return $this->m_strLastRunOn;
	}

	public function sqlLastRunOn() {
		return ( true == isset( $this->m_strLastRunOn ) ) ? '\'' . $this->m_strLastRunOn . '\'' : 'NULL';
	}

	public function setNextRunOn( $strNextRunOn ) {
		$this->set( 'm_strNextRunOn', CStrings::strTrimDef( $strNextRunOn, -1, NULL, true ) );
	}

	public function getNextRunOn() {
		return $this->m_strNextRunOn;
	}

	public function sqlNextRunOn() {
		return ( true == isset( $this->m_strNextRunOn ) ) ? '\'' . $this->m_strNextRunOn . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, default_scheduled_task_id, scheduled_task_type_id, schedule_type_id, scheduled_task_id, scheduled_task_group_id, title, property_id, property_group_ids, event_type_id, event_sub_type_id, schedule_details, task_details, start_on, end_on, last_run_on, next_run_on, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlDefaultScheduledTaskId() . ', ' .
						$this->sqlScheduledTaskTypeId() . ', ' .
						$this->sqlScheduleTypeId() . ', ' .
						$this->sqlScheduledTaskId() . ', ' .
						$this->sqlScheduledTaskGroupId() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyGroupIds() . ', ' .
						$this->sqlEventTypeId() . ', ' .
						$this->sqlEventSubTypeId() . ', ' .
						$this->sqlScheduleDetails() . ', ' .
						$this->sqlTaskDetails() . ', ' .
						$this->sqlStartOn() . ', ' .
						$this->sqlEndOn() . ', ' .
						$this->sqlLastRunOn() . ', ' .
						$this->sqlNextRunOn() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_scheduled_task_id = ' . $this->sqlDefaultScheduledTaskId(). ',' ; } elseif( true == array_key_exists( 'DefaultScheduledTaskId', $this->getChangedColumns() ) ) { $strSql .= ' default_scheduled_task_id = ' . $this->sqlDefaultScheduledTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_task_type_id = ' . $this->sqlScheduledTaskTypeId(). ',' ; } elseif( true == array_key_exists( 'ScheduledTaskTypeId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_task_type_id = ' . $this->sqlScheduledTaskTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' schedule_type_id = ' . $this->sqlScheduleTypeId(). ',' ; } elseif( true == array_key_exists( 'ScheduleTypeId', $this->getChangedColumns() ) ) { $strSql .= ' schedule_type_id = ' . $this->sqlScheduleTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_task_id = ' . $this->sqlScheduledTaskId(). ',' ; } elseif( true == array_key_exists( 'ScheduledTaskId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_task_id = ' . $this->sqlScheduledTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_task_group_id = ' . $this->sqlScheduledTaskGroupId(). ',' ; } elseif( true == array_key_exists( 'ScheduledTaskGroupId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_task_group_id = ' . $this->sqlScheduledTaskGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_group_ids = ' . $this->sqlPropertyGroupIds(). ',' ; } elseif( true == array_key_exists( 'PropertyGroupIds', $this->getChangedColumns() ) ) { $strSql .= ' property_group_ids = ' . $this->sqlPropertyGroupIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_type_id = ' . $this->sqlEventTypeId(). ',' ; } elseif( true == array_key_exists( 'EventTypeId', $this->getChangedColumns() ) ) { $strSql .= ' event_type_id = ' . $this->sqlEventTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_sub_type_id = ' . $this->sqlEventSubTypeId(). ',' ; } elseif( true == array_key_exists( 'EventSubTypeId', $this->getChangedColumns() ) ) { $strSql .= ' event_sub_type_id = ' . $this->sqlEventSubTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' schedule_details = ' . $this->sqlScheduleDetails(). ',' ; } elseif( true == array_key_exists( 'ScheduleDetails', $this->getChangedColumns() ) ) { $strSql .= ' schedule_details = ' . $this->sqlScheduleDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_details = ' . $this->sqlTaskDetails(). ',' ; } elseif( true == array_key_exists( 'TaskDetails', $this->getChangedColumns() ) ) { $strSql .= ' task_details = ' . $this->sqlTaskDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_on = ' . $this->sqlStartOn(). ',' ; } elseif( true == array_key_exists( 'StartOn', $this->getChangedColumns() ) ) { $strSql .= ' start_on = ' . $this->sqlStartOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_on = ' . $this->sqlEndOn(). ',' ; } elseif( true == array_key_exists( 'EndOn', $this->getChangedColumns() ) ) { $strSql .= ' end_on = ' . $this->sqlEndOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_run_on = ' . $this->sqlLastRunOn(). ',' ; } elseif( true == array_key_exists( 'LastRunOn', $this->getChangedColumns() ) ) { $strSql .= ' last_run_on = ' . $this->sqlLastRunOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' next_run_on = ' . $this->sqlNextRunOn(). ',' ; } elseif( true == array_key_exists( 'NextRunOn', $this->getChangedColumns() ) ) { $strSql .= ' next_run_on = ' . $this->sqlNextRunOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'default_scheduled_task_id' => $this->getDefaultScheduledTaskId(),
			'scheduled_task_type_id' => $this->getScheduledTaskTypeId(),
			'schedule_type_id' => $this->getScheduleTypeId(),
			'scheduled_task_id' => $this->getScheduledTaskId(),
			'scheduled_task_group_id' => $this->getScheduledTaskGroupId(),
			'title' => $this->getTitle(),
			'property_id' => $this->getPropertyId(),
			'property_group_ids' => $this->getPropertyGroupIds(),
			'event_type_id' => $this->getEventTypeId(),
			'event_sub_type_id' => $this->getEventSubTypeId(),
			'schedule_details' => $this->getScheduleDetails(),
			'task_details' => $this->getTaskDetails(),
			'start_on' => $this->getStartOn(),
			'end_on' => $this->getEndOn(),
			'last_run_on' => $this->getLastRunOn(),
			'next_run_on' => $this->getNextRunOn(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>