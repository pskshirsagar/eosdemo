<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBankAccounts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBankAccounts extends CEosPluralBase {

	/**
	 * @return CBankAccount[]
	 */
	public static function fetchBankAccounts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CBankAccount::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBankAccount
	 */
	public static function fetchBankAccount( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBankAccount::class, $objDatabase );
	}

	public static function fetchBankAccountCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'bank_accounts', $objDatabase );
	}

	public static function fetchBankAccountByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBankAccount( sprintf( 'SELECT * FROM bank_accounts WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountsByCid( $intCid, $objDatabase ) {
		return self::fetchBankAccounts( sprintf( 'SELECT * FROM bank_accounts WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchBankAccountsByBankAccountTypeIdByCid( $intBankAccountTypeId, $intCid, $objDatabase ) {
		return self::fetchBankAccounts( sprintf( 'SELECT * FROM bank_accounts WHERE bank_account_type_id = %d AND cid = %d', $intBankAccountTypeId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountsByBankLoginTypeIdByCid( $intBankLoginTypeId, $intCid, $objDatabase ) {
		return self::fetchBankAccounts( sprintf( 'SELECT * FROM bank_accounts WHERE bank_login_type_id = %d AND cid = %d', $intBankLoginTypeId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountsByCheckTemplateIdByCid( $intCheckTemplateId, $intCid, $objDatabase ) {
		return self::fetchBankAccounts( sprintf( 'SELECT * FROM bank_accounts WHERE check_template_id = %d AND cid = %d', $intCheckTemplateId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountsByCheckAccountTypeIdByCid( $intCheckAccountTypeId, $intCid, $objDatabase ) {
		return self::fetchBankAccounts( sprintf( 'SELECT * FROM bank_accounts WHERE check_account_type_id = %d AND cid = %d', $intCheckAccountTypeId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountsByTransmissionVendorIdByCid( $intTransmissionVendorId, $intCid, $objDatabase ) {
		return self::fetchBankAccounts( sprintf( 'SELECT * FROM bank_accounts WHERE transmission_vendor_id = %d AND cid = %d', $intTransmissionVendorId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountsByApAchTypeIdByCid( $intApAchTypeId, $intCid, $objDatabase ) {
		return self::fetchBankAccounts( sprintf( 'SELECT * FROM bank_accounts WHERE ap_ach_type_id = %d AND cid = %d', $intApAchTypeId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountsByReimbursedPropertyIdByCid( $intReimbursedPropertyId, $intCid, $objDatabase ) {
		return self::fetchBankAccounts( sprintf( 'SELECT * FROM bank_accounts WHERE reimbursed_property_id = %d AND cid = %d', $intReimbursedPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountsByImmediateDestinationIdByCid( $intImmediateDestinationId, $intCid, $objDatabase ) {
		return self::fetchBankAccounts( sprintf( 'SELECT * FROM bank_accounts WHERE immediate_destination_id = %d AND cid = %d', $intImmediateDestinationId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountsByImmediateOriginIdByCid( $intImmediateOriginId, $intCid, $objDatabase ) {
		return self::fetchBankAccounts( sprintf( 'SELECT * FROM bank_accounts WHERE immediate_origin_id = %d AND cid = %d', $intImmediateOriginId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountsByCompanyIdByCid( $intCompanyId, $intCid, $objDatabase ) {
		return self::fetchBankAccounts( sprintf( 'SELECT * FROM bank_accounts WHERE company_id = %d AND cid = %d', $intCompanyId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountsByCompanyMediaFileIdByCid( $intCompanyMediaFileId, $intCid, $objDatabase ) {
		return self::fetchBankAccounts( sprintf( 'SELECT * FROM bank_accounts WHERE company_media_file_id = %d AND cid = %d', $intCompanyMediaFileId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountsByWfpmCeoCompanyIdByCid( $intWfpmCeoCompanyId, $intCid, $objDatabase ) {
		return self::fetchBankAccounts( sprintf( 'SELECT * FROM bank_accounts WHERE wfpm_ceo_company_id = %d AND cid = %d', $intWfpmCeoCompanyId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountsByCheckLayoutIdByCid( $intCheckLayoutId, $intCid, $objDatabase ) {
		return self::fetchBankAccounts( sprintf( 'SELECT * FROM bank_accounts WHERE check_layout_id = %d AND cid = %d', $intCheckLayoutId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountsByOriginatingBankIdByCid( $intOriginatingBankId, $intCid, $objDatabase ) {
		return self::fetchBankAccounts( sprintf( 'SELECT * FROM bank_accounts WHERE originating_bank_id = %d AND cid = %d', $intOriginatingBankId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountsByAchCompanyIdByCid( $intAchCompanyId, $intCid, $objDatabase ) {
		return self::fetchBankAccounts( sprintf( 'SELECT * FROM bank_accounts WHERE ach_company_id = %d AND cid = %d', $intAchCompanyId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountsByAchCompanyTransmissionVendorIdByCid( $intAchCompanyTransmissionVendorId, $intCid, $objDatabase ) {
		return self::fetchBankAccounts( sprintf( 'SELECT * FROM bank_accounts WHERE ach_company_transmission_vendor_id = %d AND cid = %d', $intAchCompanyTransmissionVendorId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchBankAccounts( sprintf( 'SELECT * FROM bank_accounts WHERE ap_payee_id = %d AND cid = %d', $intApPayeeId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountsByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objDatabase ) {
		return self::fetchBankAccounts( sprintf( 'SELECT * FROM bank_accounts WHERE ap_payee_location_id = %d AND cid = %d', $intApPayeeLocationId, $intCid ), $objDatabase );
	}

	public static function fetchBankAccountsByApPayeeAccountIdByCid( $intApPayeeAccountId, $intCid, $objDatabase ) {
		return self::fetchBankAccounts( sprintf( 'SELECT * FROM bank_accounts WHERE ap_payee_account_id = %d AND cid = %d', $intApPayeeAccountId, $intCid ), $objDatabase );
	}

}
?>