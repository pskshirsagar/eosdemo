<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceTemplateTypes
 * Do not add any new functions to this class.
 */

class CBaseMaintenanceTemplateTypes extends CEosPluralBase {

	/**
	 * @return CMaintenanceTemplateType[]
	 */
	public static function fetchMaintenanceTemplateTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMaintenanceTemplateType::class, $objDatabase );
	}

	/**
	 * @return CMaintenanceTemplateType
	 */
	public static function fetchMaintenanceTemplateType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceTemplateType::class, $objDatabase );
	}

	public static function fetchMaintenanceTemplateTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_template_types', $objDatabase );
	}

	public static function fetchMaintenanceTemplateTypeById( $intId, $objDatabase ) {
		return self::fetchMaintenanceTemplateType( sprintf( 'SELECT * FROM maintenance_template_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>