<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerEmployer extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intCustomerId;
    protected $m_intIncomeTypeId;
    protected $m_strEmployersName;
    protected $m_strEmployersEmail;
    protected $m_strEmployersPhoneNumber;
    protected $m_strStreetLine1;
    protected $m_strStreetLine2;
    protected $m_strStreetLine3;
    protected $m_strCity;
    protected $m_strStateCode;
    protected $m_strProvince;
    protected $m_strPostalCode;
    protected $m_strCountryCode;
    protected $m_boolEmployerIsVerified;
    protected $m_strSupervisorsName;
    protected $m_strSupervisorsPhoneNumber;
    protected $m_strPosition;
    protected $m_fltGrossSalary;
    protected $m_strDateStarted;
    protected $m_strDateTerminated;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        $this->m_boolEmployerIsVerified = false;

        return;
    }

    public function setDefaults() {
        return;
    }

    /**
     * @SuppressWarnings( BooleanArgumentFlag )
     */
    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->m_intCustomerId = trim( $arrValues['customer_id'] ); else if( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
        if( isset( $arrValues['income_type_id'] ) && $boolDirectSet ) $this->m_intIncomeTypeId = trim( $arrValues['income_type_id'] ); else if( isset( $arrValues['income_type_id'] ) ) $this->setIncomeTypeId( $arrValues['income_type_id'] );
        if( isset( $arrValues['employers_name'] ) && $boolDirectSet ) $this->m_strEmployersName = trim( stripcslashes( $arrValues['employers_name'] ) ); else if( isset( $arrValues['employers_name'] ) ) $this->setEmployersName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['employers_name'] ) : $arrValues['employers_name'] );
        if( isset( $arrValues['employers_email'] ) && $boolDirectSet ) $this->m_strEmployersEmail = trim( stripcslashes( $arrValues['employers_email'] ) ); else if( isset( $arrValues['employers_email'] ) ) $this->setEmployersEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['employers_email'] ) : $arrValues['employers_email'] );
        if( isset( $arrValues['employers_phone_number'] ) && $boolDirectSet ) $this->m_strEmployersPhoneNumber = trim( stripcslashes( $arrValues['employers_phone_number'] ) ); else if( isset( $arrValues['employers_phone_number'] ) ) $this->setEmployersPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['employers_phone_number'] ) : $arrValues['employers_phone_number'] );
        if( isset( $arrValues['street_line1'] ) && $boolDirectSet ) $this->m_strStreetLine1 = trim( stripcslashes( $arrValues['street_line1'] ) ); else if( isset( $arrValues['street_line1'] ) ) $this->setStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line1'] ) : $arrValues['street_line1'] );
        if( isset( $arrValues['street_line2'] ) && $boolDirectSet ) $this->m_strStreetLine2 = trim( stripcslashes( $arrValues['street_line2'] ) ); else if( isset( $arrValues['street_line2'] ) ) $this->setStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line2'] ) : $arrValues['street_line2'] );
        if( isset( $arrValues['street_line3'] ) && $boolDirectSet ) $this->m_strStreetLine3 = trim( stripcslashes( $arrValues['street_line3'] ) ); else if( isset( $arrValues['street_line3'] ) ) $this->setStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line3'] ) : $arrValues['street_line3'] );
        if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->m_strCity = trim( stripcslashes( $arrValues['city'] ) ); else if( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
        if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->m_strStateCode = trim( stripcslashes( $arrValues['state_code'] ) ); else if( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
        if( isset( $arrValues['province'] ) && $boolDirectSet ) $this->m_strProvince = trim( stripcslashes( $arrValues['province'] ) ); else if( isset( $arrValues['province'] ) ) $this->setProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['province'] ) : $arrValues['province'] );
        if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->m_strPostalCode = trim( stripcslashes( $arrValues['postal_code'] ) ); else if( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
        if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->m_strCountryCode = trim( stripcslashes( $arrValues['country_code'] ) ); else if( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
        if( isset( $arrValues['employer_is_verified'] ) && $boolDirectSet ) $this->m_boolEmployerIsVerified = trim( stripcslashes( $arrValues['employer_is_verified'] ) ); else if( isset( $arrValues['employer_is_verified'] ) ) $this->setEmployerIsVerified( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['employer_is_verified'] ) : $arrValues['employer_is_verified'] );
        if( isset( $arrValues['supervisors_name'] ) && $boolDirectSet ) $this->m_strSupervisorsName = trim( stripcslashes( $arrValues['supervisors_name'] ) ); else if( isset( $arrValues['supervisors_name'] ) ) $this->setSupervisorsName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['supervisors_name'] ) : $arrValues['supervisors_name'] );
        if( isset( $arrValues['supervisors_phone_number'] ) && $boolDirectSet ) $this->m_strSupervisorsPhoneNumber = trim( stripcslashes( $arrValues['supervisors_phone_number'] ) ); else if( isset( $arrValues['supervisors_phone_number'] ) ) $this->setSupervisorsPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['supervisors_phone_number'] ) : $arrValues['supervisors_phone_number'] );
        if( isset( $arrValues['position'] ) && $boolDirectSet ) $this->m_strPosition = trim( stripcslashes( $arrValues['position'] ) ); else if( isset( $arrValues['position'] ) ) $this->setPosition( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['position'] ) : $arrValues['position'] );
        if( isset( $arrValues['gross_salary'] ) && $boolDirectSet ) $this->m_fltGrossSalary = trim( $arrValues['gross_salary'] ); else if( isset( $arrValues['gross_salary'] ) ) $this->setGrossSalary( $arrValues['gross_salary'] );
        if( isset( $arrValues['date_started'] ) && $boolDirectSet ) $this->m_strDateStarted = trim( $arrValues['date_started'] ); else if( isset( $arrValues['date_started'] ) ) $this->setDateStarted( $arrValues['date_started'] );
        if( isset( $arrValues['date_terminated'] ) && $boolDirectSet ) $this->m_strDateTerminated = trim( $arrValues['date_terminated'] ); else if( isset( $arrValues['date_terminated'] ) ) $this->setDateTerminated( $arrValues['date_terminated'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
    }

    public function setCustomerId( $intCustomerId ) {
        $this->m_intCustomerId = CStrings::strToIntDef( $intCustomerId, NULL, false );
    }

    public function getCustomerId() {
        return $this->m_intCustomerId;
    }

    public function sqlCustomerId() {
        return ( true == isset( $this->m_intCustomerId ) ) ? (string) $this->m_intCustomerId : 'NULL';
    }

    public function setIncomeTypeId( $intIncomeTypeId ) {
        $this->m_intIncomeTypeId = CStrings::strToIntDef( $intIncomeTypeId, NULL, false );
    }

    public function getIncomeTypeId() {
        return $this->m_intIncomeTypeId;
    }

    public function sqlIncomeTypeId() {
        return ( true == isset( $this->m_intIncomeTypeId ) ) ? (string) $this->m_intIncomeTypeId : 'NULL';
    }

    public function setEmployersName( $strEmployersName ) {
        $this->m_strEmployersName = CStrings::strTrimDef( $strEmployersName, 50, NULL, true );
    }

    public function getEmployersName() {
        return $this->m_strEmployersName;
    }

    public function sqlEmployersName() {
        return ( true == isset( $this->m_strEmployersName ) ) ? '\'' . addslashes( $this->m_strEmployersName ) . '\'' : 'NULL';
    }

    public function setEmployersEmail( $strEmployersEmail ) {
        $this->m_strEmployersEmail = CStrings::strTrimDef( $strEmployersEmail, 240, NULL, true );
    }

    public function getEmployersEmail() {
        return $this->m_strEmployersEmail;
    }

    public function sqlEmployersEmail() {
        return ( true == isset( $this->m_strEmployersEmail ) ) ? '\'' . addslashes( $this->m_strEmployersEmail ) . '\'' : 'NULL';
    }

    public function setEmployersPhoneNumber( $strEmployersPhoneNumber ) {
        $this->m_strEmployersPhoneNumber = CStrings::strTrimDef( $strEmployersPhoneNumber, 30, NULL, true );
    }

    public function getEmployersPhoneNumber() {
        return $this->m_strEmployersPhoneNumber;
    }

    public function sqlEmployersPhoneNumber() {
        return ( true == isset( $this->m_strEmployersPhoneNumber ) ) ? '\'' . addslashes( $this->m_strEmployersPhoneNumber ) . '\'' : 'NULL';
    }

    public function setStreetLine1( $strStreetLine1 ) {
        $this->m_strStreetLine1 = CStrings::strTrimDef( $strStreetLine1, 100, NULL, true );
    }

    public function getStreetLine1() {
        return $this->m_strStreetLine1;
    }

    public function sqlStreetLine1() {
        return ( true == isset( $this->m_strStreetLine1 ) ) ? '\'' . addslashes( $this->m_strStreetLine1 ) . '\'' : 'NULL';
    }

    public function setStreetLine2( $strStreetLine2 ) {
        $this->m_strStreetLine2 = CStrings::strTrimDef( $strStreetLine2, 100, NULL, true );
    }

    public function getStreetLine2() {
        return $this->m_strStreetLine2;
    }

    public function sqlStreetLine2() {
        return ( true == isset( $this->m_strStreetLine2 ) ) ? '\'' . addslashes( $this->m_strStreetLine2 ) . '\'' : 'NULL';
    }

    public function setStreetLine3( $strStreetLine3 ) {
        $this->m_strStreetLine3 = CStrings::strTrimDef( $strStreetLine3, 100, NULL, true );
    }

    public function getStreetLine3() {
        return $this->m_strStreetLine3;
    }

    public function sqlStreetLine3() {
        return ( true == isset( $this->m_strStreetLine3 ) ) ? '\'' . addslashes( $this->m_strStreetLine3 ) . '\'' : 'NULL';
    }

    public function setCity( $strCity ) {
        $this->m_strCity = CStrings::strTrimDef( $strCity, 50, NULL, true );
    }

    public function getCity() {
        return $this->m_strCity;
    }

    public function sqlCity() {
        return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
    }

    public function setStateCode( $strStateCode ) {
        $this->m_strStateCode = CStrings::strTrimDef( $strStateCode, 2, NULL, true );
    }

    public function getStateCode() {
        return $this->m_strStateCode;
    }

    public function sqlStateCode() {
        return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
    }

    public function setProvince( $strProvince ) {
        $this->m_strProvince = CStrings::strTrimDef( $strProvince, 50, NULL, true );
    }

    public function getProvince() {
        return $this->m_strProvince;
    }

    public function sqlProvince() {
        return ( true == isset( $this->m_strProvince ) ) ? '\'' . addslashes( $this->m_strProvince ) . '\'' : 'NULL';
    }

    public function setPostalCode( $strPostalCode ) {
        $this->m_strPostalCode = CStrings::strTrimDef( $strPostalCode, 20, NULL, true );
    }

    public function getPostalCode() {
        return $this->m_strPostalCode;
    }

    public function sqlPostalCode() {
        return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
    }

    public function setCountryCode( $strCountryCode ) {
        $this->m_strCountryCode = CStrings::strTrimDef( $strCountryCode, 2, NULL, true );
    }

    public function getCountryCode() {
        return $this->m_strCountryCode;
    }

    public function sqlCountryCode() {
        return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : 'NULL';
    }

    public function setEmployerIsVerified( $boolEmployerIsVerified ) {
        $this->m_boolEmployerIsVerified = CStrings::strToBool( $boolEmployerIsVerified );
    }

    public function getEmployerIsVerified() {
        return $this->m_boolEmployerIsVerified;
    }

    public function sqlEmployerIsVerified() {
        return ( true == isset( $this->m_boolEmployerIsVerified ) ) ? '\'' . ( true == ( bool ) $this->m_boolEmployerIsVerified ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setSupervisorsName( $strSupervisorsName ) {
        $this->m_strSupervisorsName = CStrings::strTrimDef( $strSupervisorsName, 100, NULL, true );
    }

    public function getSupervisorsName() {
        return $this->m_strSupervisorsName;
    }

    public function sqlSupervisorsName() {
        return ( true == isset( $this->m_strSupervisorsName ) ) ? '\'' . addslashes( $this->m_strSupervisorsName ) . '\'' : 'NULL';
    }

    public function setSupervisorsPhoneNumber( $strSupervisorsPhoneNumber ) {
        $this->m_strSupervisorsPhoneNumber = CStrings::strTrimDef( $strSupervisorsPhoneNumber, 30, NULL, true );
    }

    public function getSupervisorsPhoneNumber() {
        return $this->m_strSupervisorsPhoneNumber;
    }

    public function sqlSupervisorsPhoneNumber() {
        return ( true == isset( $this->m_strSupervisorsPhoneNumber ) ) ? '\'' . addslashes( $this->m_strSupervisorsPhoneNumber ) . '\'' : 'NULL';
    }

    public function setPosition( $strPosition ) {
        $this->m_strPosition = CStrings::strTrimDef( $strPosition, 50, NULL, true );
    }

    public function getPosition() {
        return $this->m_strPosition;
    }

    public function sqlPosition() {
        return ( true == isset( $this->m_strPosition ) ) ? '\'' . addslashes( $this->m_strPosition ) . '\'' : 'NULL';
    }

    public function setGrossSalary( $fltGrossSalary ) {
        $this->m_fltGrossSalary = CStrings::strToFloatDef( $fltGrossSalary, NULL, false, 4 );
    }

    public function getGrossSalary() {
        return $this->m_fltGrossSalary;
    }

    public function sqlGrossSalary() {
        return ( true == isset( $this->m_fltGrossSalary ) ) ? (string) $this->m_fltGrossSalary : 'NULL';
    }

    public function setDateStarted( $strDateStarted ) {
        $this->m_strDateStarted = CStrings::strTrimDef( $strDateStarted, -1, NULL, true );
    }

    public function getDateStarted() {
        return $this->m_strDateStarted;
    }

    public function sqlDateStarted() {
        return ( true == isset( $this->m_strDateStarted ) ) ? '\'' . $this->m_strDateStarted . '\'' : 'NULL';
    }

    public function setDateTerminated( $strDateTerminated ) {
        $this->m_strDateTerminated = CStrings::strTrimDef( $strDateTerminated, -1, NULL, true );
    }

    public function getDateTerminated() {
        return $this->m_strDateTerminated;
    }

    public function sqlDateTerminated() {
        return ( true == isset( $this->m_strDateTerminated ) ) ? '\'' . $this->m_strDateTerminated . '\'' : 'NULL';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.customer_employers_id_seq\' )' : $this->sqlId();

        $strSql = 'INSERT INTO					  public.customer_employers					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlCid() . ', ' . 		                $this->sqlCustomerId() . ', ' . 		                $this->sqlIncomeTypeId() . ', ' . 		                $this->sqlEmployersName() . ', ' . 		                $this->sqlEmployersEmail() . ', ' . 		                $this->sqlEmployersPhoneNumber() . ', ' . 		                $this->sqlStreetLine1() . ', ' . 		                $this->sqlStreetLine2() . ', ' . 		                $this->sqlStreetLine3() . ', ' . 		                $this->sqlCity() . ', ' . 		                $this->sqlStateCode() . ', ' . 		                $this->sqlProvince() . ', ' . 		                $this->sqlPostalCode() . ', ' . 		                $this->sqlCountryCode() . ', ' . 		                $this->sqlEmployerIsVerified() . ', ' . 		                $this->sqlSupervisorsName() . ', ' . 		                $this->sqlSupervisorsPhoneNumber() . ', ' . 		                $this->sqlPosition() . ', ' . 		                $this->sqlGrossSalary() . ', ' . 		                $this->sqlDateStarted() . ', ' . 		                $this->sqlDateTerminated() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlUpdatedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }

        $strSql = 'UPDATE
                      public.customer_employers
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCustomerId() ) != $this->getOriginalValueByFieldName ( 'customer_id' ) ) { $arrstrOriginalValueChanges['customer_id'] = $this->sqlCustomerId(); $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' income_type_id = ' . $this->sqlIncomeTypeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIncomeTypeId() ) != $this->getOriginalValueByFieldName ( 'income_type_id' ) ) { $arrstrOriginalValueChanges['income_type_id'] = $this->sqlIncomeTypeId(); $strSql .= ' income_type_id = ' . $this->sqlIncomeTypeId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employers_name = ' . $this->sqlEmployersName() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlEmployersName() ) != $this->getOriginalValueByFieldName ( 'employers_name' ) ) { $arrstrOriginalValueChanges['employers_name'] = $this->sqlEmployersName(); $strSql .= ' employers_name = ' . $this->sqlEmployersName() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employers_email = ' . $this->sqlEmployersEmail() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlEmployersEmail() ) != $this->getOriginalValueByFieldName ( 'employers_email' ) ) { $arrstrOriginalValueChanges['employers_email'] = $this->sqlEmployersEmail(); $strSql .= ' employers_email = ' . $this->sqlEmployersEmail() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employers_phone_number = ' . $this->sqlEmployersPhoneNumber() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlEmployersPhoneNumber() ) != $this->getOriginalValueByFieldName ( 'employers_phone_number' ) ) { $arrstrOriginalValueChanges['employers_phone_number'] = $this->sqlEmployersPhoneNumber(); $strSql .= ' employers_phone_number = ' . $this->sqlEmployersPhoneNumber() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlStreetLine1() ) != $this->getOriginalValueByFieldName ( 'street_line1' ) ) { $arrstrOriginalValueChanges['street_line1'] = $this->sqlStreetLine1(); $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlStreetLine2() ) != $this->getOriginalValueByFieldName ( 'street_line2' ) ) { $arrstrOriginalValueChanges['street_line2'] = $this->sqlStreetLine2(); $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlStreetLine3() ) != $this->getOriginalValueByFieldName ( 'street_line3' ) ) { $arrstrOriginalValueChanges['street_line3'] = $this->sqlStreetLine3(); $strSql .= ' street_line3 = ' . $this->sqlStreetLine3() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCity() ) != $this->getOriginalValueByFieldName ( 'city' ) ) { $arrstrOriginalValueChanges['city'] = $this->sqlCity(); $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlStateCode() ) != $this->getOriginalValueByFieldName ( 'state_code' ) ) { $arrstrOriginalValueChanges['state_code'] = $this->sqlStateCode(); $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' province = ' . $this->sqlProvince() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlProvince() ) != $this->getOriginalValueByFieldName ( 'province' ) ) { $arrstrOriginalValueChanges['province'] = $this->sqlProvince(); $strSql .= ' province = ' . $this->sqlProvince() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPostalCode() ) != $this->getOriginalValueByFieldName ( 'postal_code' ) ) { $arrstrOriginalValueChanges['postal_code'] = $this->sqlPostalCode(); $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCountryCode() ) != $this->getOriginalValueByFieldName ( 'country_code' ) ) { $arrstrOriginalValueChanges['country_code'] = $this->sqlCountryCode(); $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employer_is_verified = ' . $this->sqlEmployerIsVerified() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlEmployerIsVerified() ) != $this->getOriginalValueByFieldName ( 'employer_is_verified' ) ) { $arrstrOriginalValueChanges['employer_is_verified'] = $this->sqlEmployerIsVerified(); $strSql .= ' employer_is_verified = ' . $this->sqlEmployerIsVerified() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' supervisors_name = ' . $this->sqlSupervisorsName() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlSupervisorsName() ) != $this->getOriginalValueByFieldName ( 'supervisors_name' ) ) { $arrstrOriginalValueChanges['supervisors_name'] = $this->sqlSupervisorsName(); $strSql .= ' supervisors_name = ' . $this->sqlSupervisorsName() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' supervisors_phone_number = ' . $this->sqlSupervisorsPhoneNumber() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlSupervisorsPhoneNumber() ) != $this->getOriginalValueByFieldName ( 'supervisors_phone_number' ) ) { $arrstrOriginalValueChanges['supervisors_phone_number'] = $this->sqlSupervisorsPhoneNumber(); $strSql .= ' supervisors_phone_number = ' . $this->sqlSupervisorsPhoneNumber() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' position = ' . $this->sqlPosition() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPosition() ) != $this->getOriginalValueByFieldName ( 'position' ) ) { $arrstrOriginalValueChanges['position'] = $this->sqlPosition(); $strSql .= ' position = ' . $this->sqlPosition() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gross_salary = ' . $this->sqlGrossSalary() . ','; } elseif( ( 0 != bccomp ( CStrings::reverseSqlFormat( $this->sqlGrossSalary() ), $this->getOriginalValueByFieldName ( 'gross_salary' ), 4 ) ) ) { $arrstrOriginalValueChanges['gross_salary'] = $this->sqlGrossSalary(); $strSql .= ' gross_salary = ' . $this->sqlGrossSalary() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_started = ' . $this->sqlDateStarted() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDateStarted() ) != $this->getOriginalValueByFieldName ( 'date_started' ) ) { $arrstrOriginalValueChanges['date_started'] = $this->sqlDateStarted(); $strSql .= ' date_started = ' . $this->sqlDateStarted() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_terminated = ' . $this->sqlDateTerminated() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDateTerminated() ) != $this->getOriginalValueByFieldName ( 'date_terminated' ) ) { $arrstrOriginalValueChanges['date_terminated'] = $this->sqlDateTerminated(); $strSql .= ' date_terminated = ' . $this->sqlDateTerminated() . ','; $boolUpdate = true; }
                      $strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
                      $strSql .= ' updated_on = \'NOW()\'					WHERE					    id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.customer_employers WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.customer_employers_id_seq', $objDatabase );
    }

    public function toArray() {
        return array(
        	'id' => $this->getId(),
        	'cid' => $this->getCid(),
        	'customer_id' => $this->getCustomerId(),
        	'income_type_id' => $this->getIncomeTypeId(),
        	'employers_name' => $this->getEmployersName(),
        	'employers_email' => $this->getEmployersEmail(),
        	'employers_phone_number' => $this->getEmployersPhoneNumber(),
        	'street_line1' => $this->getStreetLine1(),
        	'street_line2' => $this->getStreetLine2(),
        	'street_line3' => $this->getStreetLine3(),
        	'city' => $this->getCity(),
        	'state_code' => $this->getStateCode(),
        	'province' => $this->getProvince(),
        	'postal_code' => $this->getPostalCode(),
        	'country_code' => $this->getCountryCode(),
        	'employer_is_verified' => $this->getEmployerIsVerified(),
        	'supervisors_name' => $this->getSupervisorsName(),
        	'supervisors_phone_number' => $this->getSupervisorsPhoneNumber(),
        	'position' => $this->getPosition(),
        	'gross_salary' => $this->getGrossSalary(),
        	'date_started' => $this->getDateStarted(),
        	'date_terminated' => $this->getDateTerminated(),
        	'updated_by' => $this->getUpdatedBy(),
        	'updated_on' => $this->getUpdatedOn(),
        	'created_by' => $this->getCreatedBy(),
        	'created_on' => $this->getCreatedOn()
        );
    }

}
?>