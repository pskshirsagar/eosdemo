<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseWebsiteDomain extends CEosSingularBase {

	const TABLE_NAME = 'public.website_domains';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intWebsiteId;
	protected $m_intPropertyId;
	protected $m_strWebsiteDomain;
	protected $m_strAwsZoneReference;
	protected $m_strEmailDomain;
	protected $m_intParentDomainId;
	protected $m_intIsPrimary;
	protected $m_boolIsSecure;
	protected $m_intRenewalAttempts;
	protected $m_strSecurityDeletedOn;
	protected $m_strDnsValidatedOn;
	protected $m_strPrivateKey;
	protected $m_strCertificateSigningRequest;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intParentDomainId = '0';
		$this->m_intIsPrimary = '0';
		$this->m_boolIsSecure = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['website_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteId', trim( $arrValues['website_id'] ) ); elseif( isset( $arrValues['website_id'] ) ) $this->setWebsiteId( $arrValues['website_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['website_domain'] ) && $boolDirectSet ) $this->set( 'm_strWebsiteDomain', trim( $arrValues['website_domain'] ) ); elseif( isset( $arrValues['website_domain'] ) ) $this->setWebsiteDomain( $arrValues['website_domain'] );
		if( isset( $arrValues['aws_zone_reference'] ) && $boolDirectSet ) $this->set( 'm_strAwsZoneReference', trim( $arrValues['aws_zone_reference'] ) ); elseif( isset( $arrValues['aws_zone_reference'] ) ) $this->setAwsZoneReference( $arrValues['aws_zone_reference'] );
		if( isset( $arrValues['email_domain'] ) && $boolDirectSet ) $this->set( 'm_strEmailDomain', trim( $arrValues['email_domain'] ) ); elseif( isset( $arrValues['email_domain'] ) ) $this->setEmailDomain( $arrValues['email_domain'] );
		if( isset( $arrValues['parent_domain_id'] ) && $boolDirectSet ) $this->set( 'm_intParentDomainId', trim( $arrValues['parent_domain_id'] ) ); elseif( isset( $arrValues['parent_domain_id'] ) ) $this->setParentDomainId( $arrValues['parent_domain_id'] );
		if( isset( $arrValues['is_primary'] ) && $boolDirectSet ) $this->set( 'm_intIsPrimary', trim( $arrValues['is_primary'] ) ); elseif( isset( $arrValues['is_primary'] ) ) $this->setIsPrimary( $arrValues['is_primary'] );
		if( isset( $arrValues['is_secure'] ) && $boolDirectSet ) $this->set( 'm_boolIsSecure', trim( stripcslashes( $arrValues['is_secure'] ) ) ); elseif( isset( $arrValues['is_secure'] ) ) $this->setIsSecure( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_secure'] ) : $arrValues['is_secure'] );
		if( isset( $arrValues['renewal_attempts'] ) && $boolDirectSet ) $this->set( 'm_intRenewalAttempts', trim( $arrValues['renewal_attempts'] ) ); elseif( isset( $arrValues['renewal_attempts'] ) ) $this->setRenewalAttempts( $arrValues['renewal_attempts'] );
		if( isset( $arrValues['security_deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strSecurityDeletedOn', trim( $arrValues['security_deleted_on'] ) ); elseif( isset( $arrValues['security_deleted_on'] ) ) $this->setSecurityDeletedOn( $arrValues['security_deleted_on'] );
		if( isset( $arrValues['dns_validated_on'] ) && $boolDirectSet ) $this->set( 'm_strDnsValidatedOn', trim( $arrValues['dns_validated_on'] ) ); elseif( isset( $arrValues['dns_validated_on'] ) ) $this->setDnsValidatedOn( $arrValues['dns_validated_on'] );
		if( isset( $arrValues['private_key'] ) && $boolDirectSet ) $this->set( 'm_strPrivateKey', trim( $arrValues['private_key'] ) ); elseif( isset( $arrValues['private_key'] ) ) $this->setPrivateKey( $arrValues['private_key'] );
		if( isset( $arrValues['certificate_signing_request'] ) && $boolDirectSet ) $this->set( 'm_strCertificateSigningRequest', trim( $arrValues['certificate_signing_request'] ) ); elseif( isset( $arrValues['certificate_signing_request'] ) ) $this->setCertificateSigningRequest( $arrValues['certificate_signing_request'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->set( 'm_intWebsiteId', CStrings::strToIntDef( $intWebsiteId, NULL, false ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function sqlWebsiteId() {
		return ( true == isset( $this->m_intWebsiteId ) ) ? ( string ) $this->m_intWebsiteId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setWebsiteDomain( $strWebsiteDomain ) {
		$this->set( 'm_strWebsiteDomain', CStrings::strTrimDef( $strWebsiteDomain, 240, NULL, true ) );
	}

	public function getWebsiteDomain() {
		return $this->m_strWebsiteDomain;
	}

	public function sqlWebsiteDomain() {
		return ( true == isset( $this->m_strWebsiteDomain ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWebsiteDomain ) : '\'' . addslashes( $this->m_strWebsiteDomain ) . '\'' ) : 'NULL';
	}

	public function setAwsZoneReference( $strAwsZoneReference ) {
		$this->set( 'm_strAwsZoneReference', CStrings::strTrimDef( $strAwsZoneReference, 100, NULL, true ) );
	}

	public function getAwsZoneReference() {
		return $this->m_strAwsZoneReference;
	}

	public function sqlAwsZoneReference() {
		return ( true == isset( $this->m_strAwsZoneReference ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAwsZoneReference ) : '\'' . addslashes( $this->m_strAwsZoneReference ) . '\'' ) : 'NULL';
	}

	public function setEmailDomain( $strEmailDomain ) {
		$this->set( 'm_strEmailDomain', CStrings::strTrimDef( $strEmailDomain, 240, NULL, true ) );
	}

	public function getEmailDomain() {
		return $this->m_strEmailDomain;
	}

	public function sqlEmailDomain() {
		return ( true == isset( $this->m_strEmailDomain ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailDomain ) : '\'' . addslashes( $this->m_strEmailDomain ) . '\'' ) : 'NULL';
	}

	public function setParentDomainId( $intParentDomainId ) {
		$this->set( 'm_intParentDomainId', CStrings::strToIntDef( $intParentDomainId, NULL, false ) );
	}

	public function getParentDomainId() {
		return $this->m_intParentDomainId;
	}

	public function sqlParentDomainId() {
		return ( true == isset( $this->m_intParentDomainId ) ) ? ( string ) $this->m_intParentDomainId : '0';
	}

	public function setIsPrimary( $intIsPrimary ) {
		$this->set( 'm_intIsPrimary', CStrings::strToIntDef( $intIsPrimary, NULL, false ) );
	}

	public function getIsPrimary() {
		return $this->m_intIsPrimary;
	}

	public function sqlIsPrimary() {
		return ( true == isset( $this->m_intIsPrimary ) ) ? ( string ) $this->m_intIsPrimary : '0';
	}

	public function setIsSecure( $boolIsSecure ) {
		$this->set( 'm_boolIsSecure', CStrings::strToBool( $boolIsSecure ) );
	}

	public function getIsSecure() {
		return $this->m_boolIsSecure;
	}

	public function sqlIsSecure() {
		return ( true == isset( $this->m_boolIsSecure ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSecure ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRenewalAttempts( $intRenewalAttempts ) {
		$this->set( 'm_intRenewalAttempts', CStrings::strToIntDef( $intRenewalAttempts, NULL, false ) );
	}

	public function getRenewalAttempts() {
		return $this->m_intRenewalAttempts;
	}

	public function sqlRenewalAttempts() {
		return ( true == isset( $this->m_intRenewalAttempts ) ) ? ( string ) $this->m_intRenewalAttempts : 'NULL';
	}

	public function setSecurityDeletedOn( $strSecurityDeletedOn ) {
		$this->set( 'm_strSecurityDeletedOn', CStrings::strTrimDef( $strSecurityDeletedOn, -1, NULL, true ) );
	}

	public function getSecurityDeletedOn() {
		return $this->m_strSecurityDeletedOn;
	}

	public function sqlSecurityDeletedOn() {
		return ( true == isset( $this->m_strSecurityDeletedOn ) ) ? '\'' . $this->m_strSecurityDeletedOn . '\'' : 'NULL';
	}

	public function setDnsValidatedOn( $strDnsValidatedOn ) {
		$this->set( 'm_strDnsValidatedOn', CStrings::strTrimDef( $strDnsValidatedOn, -1, NULL, true ) );
	}

	public function getDnsValidatedOn() {
		return $this->m_strDnsValidatedOn;
	}

	public function sqlDnsValidatedOn() {
		return ( true == isset( $this->m_strDnsValidatedOn ) ) ? '\'' . $this->m_strDnsValidatedOn . '\'' : 'NULL';
	}

	public function setPrivateKey( $strPrivateKey ) {
		$this->set( 'm_strPrivateKey', CStrings::strTrimDef( $strPrivateKey, -1, NULL, true ) );
	}

	public function getPrivateKey() {
		return $this->m_strPrivateKey;
	}

	public function sqlPrivateKey() {
		return ( true == isset( $this->m_strPrivateKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrivateKey ) : '\'' . addslashes( $this->m_strPrivateKey ) . '\'' ) : 'NULL';
	}

	public function setCertificateSigningRequest( $strCertificateSigningRequest ) {
		$this->set( 'm_strCertificateSigningRequest', CStrings::strTrimDef( $strCertificateSigningRequest, -1, NULL, true ) );
	}

	public function getCertificateSigningRequest() {
		return $this->m_strCertificateSigningRequest;
	}

	public function sqlCertificateSigningRequest() {
		return ( true == isset( $this->m_strCertificateSigningRequest ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCertificateSigningRequest ) : '\'' . addslashes( $this->m_strCertificateSigningRequest ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, website_id, property_id, website_domain, aws_zone_reference, email_domain, parent_domain_id, is_primary, is_secure, renewal_attempts, security_deleted_on, dns_validated_on, private_key, certificate_signing_request, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlWebsiteId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlWebsiteDomain() . ', ' .
						$this->sqlAwsZoneReference() . ', ' .
						$this->sqlEmailDomain() . ', ' .
						$this->sqlParentDomainId() . ', ' .
						$this->sqlIsPrimary() . ', ' .
						$this->sqlIsSecure() . ', ' .
						$this->sqlRenewalAttempts() . ', ' .
						$this->sqlSecurityDeletedOn() . ', ' .
						$this->sqlDnsValidatedOn() . ', ' .
						$this->sqlPrivateKey() . ', ' .
						$this->sqlCertificateSigningRequest() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId(). ',' ; } elseif( true == array_key_exists( 'WebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_domain = ' . $this->sqlWebsiteDomain(). ',' ; } elseif( true == array_key_exists( 'WebsiteDomain', $this->getChangedColumns() ) ) { $strSql .= ' website_domain = ' . $this->sqlWebsiteDomain() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' aws_zone_reference = ' . $this->sqlAwsZoneReference(). ',' ; } elseif( true == array_key_exists( 'AwsZoneReference', $this->getChangedColumns() ) ) { $strSql .= ' aws_zone_reference = ' . $this->sqlAwsZoneReference() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_domain = ' . $this->sqlEmailDomain(). ',' ; } elseif( true == array_key_exists( 'EmailDomain', $this->getChangedColumns() ) ) { $strSql .= ' email_domain = ' . $this->sqlEmailDomain() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_domain_id = ' . $this->sqlParentDomainId(). ',' ; } elseif( true == array_key_exists( 'ParentDomainId', $this->getChangedColumns() ) ) { $strSql .= ' parent_domain_id = ' . $this->sqlParentDomainId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_primary = ' . $this->sqlIsPrimary(). ',' ; } elseif( true == array_key_exists( 'IsPrimary', $this->getChangedColumns() ) ) { $strSql .= ' is_primary = ' . $this->sqlIsPrimary() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_secure = ' . $this->sqlIsSecure(). ',' ; } elseif( true == array_key_exists( 'IsSecure', $this->getChangedColumns() ) ) { $strSql .= ' is_secure = ' . $this->sqlIsSecure() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' renewal_attempts = ' . $this->sqlRenewalAttempts(). ',' ; } elseif( true == array_key_exists( 'RenewalAttempts', $this->getChangedColumns() ) ) { $strSql .= ' renewal_attempts = ' . $this->sqlRenewalAttempts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' security_deleted_on = ' . $this->sqlSecurityDeletedOn(). ',' ; } elseif( true == array_key_exists( 'SecurityDeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' security_deleted_on = ' . $this->sqlSecurityDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dns_validated_on = ' . $this->sqlDnsValidatedOn(). ',' ; } elseif( true == array_key_exists( 'DnsValidatedOn', $this->getChangedColumns() ) ) { $strSql .= ' dns_validated_on = ' . $this->sqlDnsValidatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' private_key = ' . $this->sqlPrivateKey(). ',' ; } elseif( true == array_key_exists( 'PrivateKey', $this->getChangedColumns() ) ) { $strSql .= ' private_key = ' . $this->sqlPrivateKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' certificate_signing_request = ' . $this->sqlCertificateSigningRequest(). ',' ; } elseif( true == array_key_exists( 'CertificateSigningRequest', $this->getChangedColumns() ) ) { $strSql .= ' certificate_signing_request = ' . $this->sqlCertificateSigningRequest() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'website_id' => $this->getWebsiteId(),
			'property_id' => $this->getPropertyId(),
			'website_domain' => $this->getWebsiteDomain(),
			'aws_zone_reference' => $this->getAwsZoneReference(),
			'email_domain' => $this->getEmailDomain(),
			'parent_domain_id' => $this->getParentDomainId(),
			'is_primary' => $this->getIsPrimary(),
			'is_secure' => $this->getIsSecure(),
			'renewal_attempts' => $this->getRenewalAttempts(),
			'security_deleted_on' => $this->getSecurityDeletedOn(),
			'dns_validated_on' => $this->getDnsValidatedOn(),
			'private_key' => $this->getPrivateKey(),
			'certificate_signing_request' => $this->getCertificateSigningRequest(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>