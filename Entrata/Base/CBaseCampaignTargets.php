<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCampaignTargets
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCampaignTargets extends CEosPluralBase {

	/**
	 * @return CCampaignTarget[]
	 */
	public static function fetchCampaignTargets( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCampaignTarget', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCampaignTarget
	 */
	public static function fetchCampaignTarget( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCampaignTarget', $objDatabase );
	}

	public static function fetchCampaignTargetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'campaign_targets', $objDatabase );
	}

	public static function fetchCampaignTargetByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCampaignTarget( sprintf( 'SELECT * FROM campaign_targets WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCampaignTargetsByCid( $intCid, $objDatabase ) {
		return self::fetchCampaignTargets( sprintf( 'SELECT * FROM campaign_targets WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCampaignTargetsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCampaignTargets( sprintf( 'SELECT * FROM campaign_targets WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCampaignTargetsByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchCampaignTargets( sprintf( 'SELECT * FROM campaign_targets WHERE applicant_id = %d AND cid = %d', ( int ) $intApplicantId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCampaignTargetsByCampaignIdByCid( $intCampaignId, $intCid, $objDatabase ) {
		return self::fetchCampaignTargets( sprintf( 'SELECT * FROM campaign_targets WHERE campaign_id = %d AND cid = %d', ( int ) $intCampaignId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCampaignTargetsByCompanyIdentificationTypeIdByCid( $intCompanyIdentificationTypeId, $intCid, $objDatabase ) {
		return self::fetchCampaignTargets( sprintf( 'SELECT * FROM campaign_targets WHERE company_identification_type_id = %d AND cid = %d', ( int ) $intCompanyIdentificationTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>