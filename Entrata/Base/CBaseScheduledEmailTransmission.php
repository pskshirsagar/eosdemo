<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledEmailTransmission extends CEosSingularBase {

	const TABLE_NAME = 'public.scheduled_email_transmissions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScheduledEmailId;
	protected $m_intTotalResidents;
	protected $m_intTotalProspects;
	protected $m_intTotalCompanyEmployees;
	protected $m_intTotalResidentEmails;
	protected $m_intTotalProspectEmails;
	protected $m_intTotalCompanyEmployeeEmails;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['scheduled_email_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledEmailId', trim( $arrValues['scheduled_email_id'] ) ); elseif( isset( $arrValues['scheduled_email_id'] ) ) $this->setScheduledEmailId( $arrValues['scheduled_email_id'] );
		if( isset( $arrValues['total_residents'] ) && $boolDirectSet ) $this->set( 'm_intTotalResidents', trim( $arrValues['total_residents'] ) ); elseif( isset( $arrValues['total_residents'] ) ) $this->setTotalResidents( $arrValues['total_residents'] );
		if( isset( $arrValues['total_prospects'] ) && $boolDirectSet ) $this->set( 'm_intTotalProspects', trim( $arrValues['total_prospects'] ) ); elseif( isset( $arrValues['total_prospects'] ) ) $this->setTotalProspects( $arrValues['total_prospects'] );
		if( isset( $arrValues['total_company_employees'] ) && $boolDirectSet ) $this->set( 'm_intTotalCompanyEmployees', trim( $arrValues['total_company_employees'] ) ); elseif( isset( $arrValues['total_company_employees'] ) ) $this->setTotalCompanyEmployees( $arrValues['total_company_employees'] );
		if( isset( $arrValues['total_resident_emails'] ) && $boolDirectSet ) $this->set( 'm_intTotalResidentEmails', trim( $arrValues['total_resident_emails'] ) ); elseif( isset( $arrValues['total_resident_emails'] ) ) $this->setTotalResidentEmails( $arrValues['total_resident_emails'] );
		if( isset( $arrValues['total_prospect_emails'] ) && $boolDirectSet ) $this->set( 'm_intTotalProspectEmails', trim( $arrValues['total_prospect_emails'] ) ); elseif( isset( $arrValues['total_prospect_emails'] ) ) $this->setTotalProspectEmails( $arrValues['total_prospect_emails'] );
		if( isset( $arrValues['total_company_employee_emails'] ) && $boolDirectSet ) $this->set( 'm_intTotalCompanyEmployeeEmails', trim( $arrValues['total_company_employee_emails'] ) ); elseif( isset( $arrValues['total_company_employee_emails'] ) ) $this->setTotalCompanyEmployeeEmails( $arrValues['total_company_employee_emails'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScheduledEmailId( $intScheduledEmailId ) {
		$this->set( 'm_intScheduledEmailId', CStrings::strToIntDef( $intScheduledEmailId, NULL, false ) );
	}

	public function getScheduledEmailId() {
		return $this->m_intScheduledEmailId;
	}

	public function sqlScheduledEmailId() {
		return ( true == isset( $this->m_intScheduledEmailId ) ) ? ( string ) $this->m_intScheduledEmailId : 'NULL';
	}

	public function setTotalResidents( $intTotalResidents ) {
		$this->set( 'm_intTotalResidents', CStrings::strToIntDef( $intTotalResidents, NULL, false ) );
	}

	public function getTotalResidents() {
		return $this->m_intTotalResidents;
	}

	public function sqlTotalResidents() {
		return ( true == isset( $this->m_intTotalResidents ) ) ? ( string ) $this->m_intTotalResidents : 'NULL';
	}

	public function setTotalProspects( $intTotalProspects ) {
		$this->set( 'm_intTotalProspects', CStrings::strToIntDef( $intTotalProspects, NULL, false ) );
	}

	public function getTotalProspects() {
		return $this->m_intTotalProspects;
	}

	public function sqlTotalProspects() {
		return ( true == isset( $this->m_intTotalProspects ) ) ? ( string ) $this->m_intTotalProspects : 'NULL';
	}

	public function setTotalCompanyEmployees( $intTotalCompanyEmployees ) {
		$this->set( 'm_intTotalCompanyEmployees', CStrings::strToIntDef( $intTotalCompanyEmployees, NULL, false ) );
	}

	public function getTotalCompanyEmployees() {
		return $this->m_intTotalCompanyEmployees;
	}

	public function sqlTotalCompanyEmployees() {
		return ( true == isset( $this->m_intTotalCompanyEmployees ) ) ? ( string ) $this->m_intTotalCompanyEmployees : 'NULL';
	}

	public function setTotalResidentEmails( $intTotalResidentEmails ) {
		$this->set( 'm_intTotalResidentEmails', CStrings::strToIntDef( $intTotalResidentEmails, NULL, false ) );
	}

	public function getTotalResidentEmails() {
		return $this->m_intTotalResidentEmails;
	}

	public function sqlTotalResidentEmails() {
		return ( true == isset( $this->m_intTotalResidentEmails ) ) ? ( string ) $this->m_intTotalResidentEmails : 'NULL';
	}

	public function setTotalProspectEmails( $intTotalProspectEmails ) {
		$this->set( 'm_intTotalProspectEmails', CStrings::strToIntDef( $intTotalProspectEmails, NULL, false ) );
	}

	public function getTotalProspectEmails() {
		return $this->m_intTotalProspectEmails;
	}

	public function sqlTotalProspectEmails() {
		return ( true == isset( $this->m_intTotalProspectEmails ) ) ? ( string ) $this->m_intTotalProspectEmails : 'NULL';
	}

	public function setTotalCompanyEmployeeEmails( $intTotalCompanyEmployeeEmails ) {
		$this->set( 'm_intTotalCompanyEmployeeEmails', CStrings::strToIntDef( $intTotalCompanyEmployeeEmails, NULL, false ) );
	}

	public function getTotalCompanyEmployeeEmails() {
		return $this->m_intTotalCompanyEmployeeEmails;
	}

	public function sqlTotalCompanyEmployeeEmails() {
		return ( true == isset( $this->m_intTotalCompanyEmployeeEmails ) ) ? ( string ) $this->m_intTotalCompanyEmployeeEmails : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, scheduled_email_id, total_residents, total_prospects, total_company_employees, total_resident_emails, total_prospect_emails, total_company_employee_emails, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlScheduledEmailId() . ', ' .
 						$this->sqlTotalResidents() . ', ' .
 						$this->sqlTotalProspects() . ', ' .
 						$this->sqlTotalCompanyEmployees() . ', ' .
 						$this->sqlTotalResidentEmails() . ', ' .
 						$this->sqlTotalProspectEmails() . ', ' .
 						$this->sqlTotalCompanyEmployeeEmails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_email_id = ' . $this->sqlScheduledEmailId() . ','; } elseif( true == array_key_exists( 'ScheduledEmailId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_email_id = ' . $this->sqlScheduledEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_residents = ' . $this->sqlTotalResidents() . ','; } elseif( true == array_key_exists( 'TotalResidents', $this->getChangedColumns() ) ) { $strSql .= ' total_residents = ' . $this->sqlTotalResidents() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_prospects = ' . $this->sqlTotalProspects() . ','; } elseif( true == array_key_exists( 'TotalProspects', $this->getChangedColumns() ) ) { $strSql .= ' total_prospects = ' . $this->sqlTotalProspects() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_company_employees = ' . $this->sqlTotalCompanyEmployees() . ','; } elseif( true == array_key_exists( 'TotalCompanyEmployees', $this->getChangedColumns() ) ) { $strSql .= ' total_company_employees = ' . $this->sqlTotalCompanyEmployees() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_resident_emails = ' . $this->sqlTotalResidentEmails() . ','; } elseif( true == array_key_exists( 'TotalResidentEmails', $this->getChangedColumns() ) ) { $strSql .= ' total_resident_emails = ' . $this->sqlTotalResidentEmails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_prospect_emails = ' . $this->sqlTotalProspectEmails() . ','; } elseif( true == array_key_exists( 'TotalProspectEmails', $this->getChangedColumns() ) ) { $strSql .= ' total_prospect_emails = ' . $this->sqlTotalProspectEmails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_company_employee_emails = ' . $this->sqlTotalCompanyEmployeeEmails() . ','; } elseif( true == array_key_exists( 'TotalCompanyEmployeeEmails', $this->getChangedColumns() ) ) { $strSql .= ' total_company_employee_emails = ' . $this->sqlTotalCompanyEmployeeEmails() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'scheduled_email_id' => $this->getScheduledEmailId(),
			'total_residents' => $this->getTotalResidents(),
			'total_prospects' => $this->getTotalProspects(),
			'total_company_employees' => $this->getTotalCompanyEmployees(),
			'total_resident_emails' => $this->getTotalResidentEmails(),
			'total_prospect_emails' => $this->getTotalProspectEmails(),
			'total_company_employee_emails' => $this->getTotalCompanyEmployeeEmails(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>