<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledTaskLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledTaskLogs extends CEosPluralBase {

	/**
	 * @return CScheduledTaskLog[]
	 */
	public static function fetchScheduledTaskLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CScheduledTaskLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScheduledTaskLog
	 */
	public static function fetchScheduledTaskLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScheduledTaskLog', $objDatabase );
	}

	public static function fetchScheduledTaskLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_task_logs', $objDatabase );
	}

	public static function fetchScheduledTaskLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScheduledTaskLog( sprintf( 'SELECT * FROM scheduled_task_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledTaskLogsByCid( $intCid, $objDatabase ) {
		return self::fetchScheduledTaskLogs( sprintf( 'SELECT * FROM scheduled_task_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledTaskLogsByScheduledTaskIdByCid( $intScheduledTaskId, $intCid, $objDatabase ) {
		return self::fetchScheduledTaskLogs( sprintf( 'SELECT * FROM scheduled_task_logs WHERE scheduled_task_id = %d AND cid = %d', ( int ) $intScheduledTaskId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledTaskLogsByScheduledTaskTypeIdByCid( $intScheduledTaskTypeId, $intCid, $objDatabase ) {
		return self::fetchScheduledTaskLogs( sprintf( 'SELECT * FROM scheduled_task_logs WHERE scheduled_task_type_id = %d AND cid = %d', ( int ) $intScheduledTaskTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledTaskLogsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchScheduledTaskLogs( sprintf( 'SELECT * FROM scheduled_task_logs WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledTaskLogsByEventTypeIdByCid( $intEventTypeId, $intCid, $objDatabase ) {
		return self::fetchScheduledTaskLogs( sprintf( 'SELECT * FROM scheduled_task_logs WHERE event_type_id = %d AND cid = %d', ( int ) $intEventTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledTaskLogsByEventSubTypeIdByCid( $intEventSubTypeId, $intCid, $objDatabase ) {
		return self::fetchScheduledTaskLogs( sprintf( 'SELECT * FROM scheduled_task_logs WHERE event_sub_type_id = %d AND cid = %d', ( int ) $intEventSubTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledTaskLogsByPriorScheduledTaskLogIdByCid( $intPriorScheduledTaskLogId, $intCid, $objDatabase ) {
		return self::fetchScheduledTaskLogs( sprintf( 'SELECT * FROM scheduled_task_logs WHERE prior_scheduled_task_log_id = %d AND cid = %d', ( int ) $intPriorScheduledTaskLogId, ( int ) $intCid ), $objDatabase );
	}

}
?>