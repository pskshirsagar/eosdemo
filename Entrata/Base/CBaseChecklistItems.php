<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChecklistItems
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseChecklistItems extends CEosPluralBase {

	/**
	 * @return CChecklistItem[]
	 */
	public static function fetchChecklistItems( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CChecklistItem::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CChecklistItem
	 */
	public static function fetchChecklistItem( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CChecklistItem::class, $objDatabase );
	}

	public static function fetchChecklistItemCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'checklist_items', $objDatabase );
	}

	public static function fetchChecklistItemByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchChecklistItem( sprintf( 'SELECT * FROM checklist_items WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchChecklistItemsByCid( $intCid, $objDatabase ) {
		return self::fetchChecklistItems( sprintf( 'SELECT * FROM checklist_items WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchChecklistItemsByChecklistIdByCid( $intChecklistId, $intCid, $objDatabase ) {
		return self::fetchChecklistItems( sprintf( 'SELECT * FROM checklist_items WHERE checklist_id = %d AND cid = %d', $intChecklistId, $intCid ), $objDatabase );
	}

	public static function fetchChecklistItemsByChecklistItemTypeIdByCid( $intChecklistItemTypeId, $intCid, $objDatabase ) {
		return self::fetchChecklistItems( sprintf( 'SELECT * FROM checklist_items WHERE checklist_item_type_id = %d AND cid = %d', $intChecklistItemTypeId, $intCid ), $objDatabase );
	}

	public static function fetchChecklistItemsByChecklistItemTypeOptionIdByCid( $intChecklistItemTypeOptionId, $intCid, $objDatabase ) {
		return self::fetchChecklistItems( sprintf( 'SELECT * FROM checklist_items WHERE checklist_item_type_option_id = %d AND cid = %d', $intChecklistItemTypeOptionId, $intCid ), $objDatabase );
	}

	public static function fetchChecklistItemsByDefaultChecklistItemIdByCid( $intDefaultChecklistItemId, $intCid, $objDatabase ) {
		return self::fetchChecklistItems( sprintf( 'SELECT * FROM checklist_items WHERE default_checklist_item_id = %d AND cid = %d', $intDefaultChecklistItemId, $intCid ), $objDatabase );
	}

	public static function fetchChecklistItemsByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		return self::fetchChecklistItems( sprintf( 'SELECT * FROM checklist_items WHERE document_id = %d AND cid = %d', $intDocumentId, $intCid ), $objDatabase );
	}

	public static function fetchChecklistItemsByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		return self::fetchChecklistItems( sprintf( 'SELECT * FROM checklist_items WHERE file_id = %d AND cid = %d', $intFileId, $intCid ), $objDatabase );
	}

	public static function fetchChecklistItemsByFileTypeIdByCid( $intFileTypeId, $intCid, $objDatabase ) {
		return self::fetchChecklistItems( sprintf( 'SELECT * FROM checklist_items WHERE file_type_id = %d AND cid = %d', $intFileTypeId, $intCid ), $objDatabase );
	}

}
?>