<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseJobGroups extends CEosPluralBase {

	/**
	 * @return CJobGroup[]
	 */
	public static function fetchJobGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CJobGroup', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CJobGroup
	 */
	public static function fetchJobGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CJobGroup', $objDatabase );
	}

	public static function fetchJobGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_groups', $objDatabase );
	}

	public static function fetchJobGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchJobGroup( sprintf( 'SELECT * FROM job_groups WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchJobGroups( sprintf( 'SELECT * FROM job_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobGroupsByJobIdByCid( $intJobId, $intCid, $objDatabase ) {
		return self::fetchJobGroups( sprintf( 'SELECT * FROM job_groups WHERE job_id = %d AND cid = %d', ( int ) $intJobId, ( int ) $intCid ), $objDatabase );
	}

}
?>