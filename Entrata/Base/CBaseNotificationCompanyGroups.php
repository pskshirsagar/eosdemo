<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CNotificationCompanyGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseNotificationCompanyGroups extends CEosPluralBase {

	/**
	 * @return CNotificationCompanyGroup[]
	 */
	public static function fetchNotificationCompanyGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CNotificationCompanyGroup', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CNotificationCompanyGroup
	 */
	public static function fetchNotificationCompanyGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CNotificationCompanyGroup', $objDatabase );
	}

	public static function fetchNotificationCompanyGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'notification_company_groups', $objDatabase );
	}

	public static function fetchNotificationCompanyGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchNotificationCompanyGroup( sprintf( 'SELECT * FROM notification_company_groups WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchNotificationCompanyGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchNotificationCompanyGroups( sprintf( 'SELECT * FROM notification_company_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchNotificationCompanyGroupsByNotificationIdByCid( $intNotificationId, $intCid, $objDatabase ) {
		return self::fetchNotificationCompanyGroups( sprintf( 'SELECT * FROM notification_company_groups WHERE notification_id = %d AND cid = %d', ( int ) $intNotificationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchNotificationCompanyGroupsByCompanyGroupIdByCid( $intCompanyGroupId, $intCid, $objDatabase ) {
		return self::fetchNotificationCompanyGroups( sprintf( 'SELECT * FROM notification_company_groups WHERE company_group_id = %d AND cid = %d', ( int ) $intCompanyGroupId, ( int ) $intCid ), $objDatabase );
	}

}
?>