<?php

class CBaseDefaultScheduledTask extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_scheduled_tasks';

	protected $m_intId;
	protected $m_intScheduledTaskTypeId;
	protected $m_intScheduleTypeId;
	protected $m_intDefaultScheduledTaskId;
	protected $m_intScheduledTaskGroupId;
	protected $m_intEventTypeId;
	protected $m_intEventSubTypeId;
	protected $m_strTitle;
	protected $m_strScheduleDetails;
	protected $m_jsonScheduleDetails;
	protected $m_strTaskDetails;
	protected $m_jsonTaskDetails;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_strTitle = NULL;
		$this->m_boolIsPublished = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['scheduled_task_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledTaskTypeId', trim( $arrValues['scheduled_task_type_id'] ) ); elseif( isset( $arrValues['scheduled_task_type_id'] ) ) $this->setScheduledTaskTypeId( $arrValues['scheduled_task_type_id'] );
		if( isset( $arrValues['schedule_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduleTypeId', trim( $arrValues['schedule_type_id'] ) ); elseif( isset( $arrValues['schedule_type_id'] ) ) $this->setScheduleTypeId( $arrValues['schedule_type_id'] );
		if( isset( $arrValues['default_scheduled_task_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultScheduledTaskId', trim( $arrValues['default_scheduled_task_id'] ) ); elseif( isset( $arrValues['default_scheduled_task_id'] ) ) $this->setDefaultScheduledTaskId( $arrValues['default_scheduled_task_id'] );
		if( isset( $arrValues['scheduled_task_group_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledTaskGroupId', trim( $arrValues['scheduled_task_group_id'] ) ); elseif( isset( $arrValues['scheduled_task_group_id'] ) ) $this->setScheduledTaskGroupId( $arrValues['scheduled_task_group_id'] );
		if( isset( $arrValues['event_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEventTypeId', trim( $arrValues['event_type_id'] ) ); elseif( isset( $arrValues['event_type_id'] ) ) $this->setEventTypeId( $arrValues['event_type_id'] );
		if( isset( $arrValues['event_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEventSubTypeId', trim( $arrValues['event_sub_type_id'] ) ); elseif( isset( $arrValues['event_sub_type_id'] ) ) $this->setEventSubTypeId( $arrValues['event_sub_type_id'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['schedule_details'] ) ) $this->set( 'm_strScheduleDetails', trim( $arrValues['schedule_details'] ) );
		if( isset( $arrValues['task_details'] ) ) $this->set( 'm_strTaskDetails', trim( $arrValues['task_details'] ) );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScheduledTaskTypeId( $intScheduledTaskTypeId ) {
		$this->set( 'm_intScheduledTaskTypeId', CStrings::strToIntDef( $intScheduledTaskTypeId, NULL, false ) );
	}

	public function getScheduledTaskTypeId() {
		return $this->m_intScheduledTaskTypeId;
	}

	public function sqlScheduledTaskTypeId() {
		return ( true == isset( $this->m_intScheduledTaskTypeId ) ) ? ( string ) $this->m_intScheduledTaskTypeId : 'NULL';
	}

	public function setScheduleTypeId( $intScheduleTypeId ) {
		$this->set( 'm_intScheduleTypeId', CStrings::strToIntDef( $intScheduleTypeId, NULL, false ) );
	}

	public function getScheduleTypeId() {
		return $this->m_intScheduleTypeId;
	}

	public function sqlScheduleTypeId() {
		return ( true == isset( $this->m_intScheduleTypeId ) ) ? ( string ) $this->m_intScheduleTypeId : 'NULL';
	}

	public function setDefaultScheduledTaskId( $intDefaultScheduledTaskId ) {
		$this->set( 'm_intDefaultScheduledTaskId', CStrings::strToIntDef( $intDefaultScheduledTaskId, NULL, false ) );
	}

	public function getDefaultScheduledTaskId() {
		return $this->m_intDefaultScheduledTaskId;
	}

	public function sqlDefaultScheduledTaskId() {
		return ( true == isset( $this->m_intDefaultScheduledTaskId ) ) ? ( string ) $this->m_intDefaultScheduledTaskId : 'NULL';
	}

	public function setScheduledTaskGroupId( $intScheduledTaskGroupId ) {
		$this->set( 'm_intScheduledTaskGroupId', CStrings::strToIntDef( $intScheduledTaskGroupId, NULL, false ) );
	}

	public function getScheduledTaskGroupId() {
		return $this->m_intScheduledTaskGroupId;
	}

	public function sqlScheduledTaskGroupId() {
		return ( true == isset( $this->m_intScheduledTaskGroupId ) ) ? ( string ) $this->m_intScheduledTaskGroupId : 'NULL';
	}

	public function setEventTypeId( $intEventTypeId ) {
		$this->set( 'm_intEventTypeId', CStrings::strToIntDef( $intEventTypeId, NULL, false ) );
	}

	public function getEventTypeId() {
		return $this->m_intEventTypeId;
	}

	public function sqlEventTypeId() {
		return ( true == isset( $this->m_intEventTypeId ) ) ? ( string ) $this->m_intEventTypeId : 'NULL';
	}

	public function setEventSubTypeId( $intEventSubTypeId ) {
		$this->set( 'm_intEventSubTypeId', CStrings::strToIntDef( $intEventSubTypeId, NULL, false ) );
	}

	public function getEventSubTypeId() {
		return $this->m_intEventSubTypeId;
	}

	public function sqlEventSubTypeId() {
		return ( true == isset( $this->m_intEventSubTypeId ) ) ? ( string ) $this->m_intEventSubTypeId : 'NULL';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 240, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : '\'NULL\'';
	}

	public function setScheduleDetails( $jsonScheduleDetails ) {
		if( true == valObj( $jsonScheduleDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonScheduleDetails', $jsonScheduleDetails );
		} elseif( true == valJsonString( $jsonScheduleDetails ) ) {
			$this->set( 'm_jsonScheduleDetails', CStrings::strToJson( $jsonScheduleDetails ) );
		} else {
			$this->set( 'm_jsonScheduleDetails', NULL ); 
		}
		unset( $this->m_strScheduleDetails );
	}

	public function getScheduleDetails() {
		if( true == isset( $this->m_strScheduleDetails ) ) {
			$this->m_jsonScheduleDetails = CStrings::strToJson( $this->m_strScheduleDetails );
			unset( $this->m_strScheduleDetails );
		}
		return $this->m_jsonScheduleDetails;
	}

	public function sqlScheduleDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getScheduleDetails() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getScheduleDetails() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setTaskDetails( $jsonTaskDetails ) {
		if( true == valObj( $jsonTaskDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonTaskDetails', $jsonTaskDetails );
		} elseif( true == valJsonString( $jsonTaskDetails ) ) {
			$this->set( 'm_jsonTaskDetails', CStrings::strToJson( $jsonTaskDetails ) );
		} else {
			$this->set( 'm_jsonTaskDetails', NULL ); 
		}
		unset( $this->m_strTaskDetails );
	}

	public function getTaskDetails() {
		if( true == isset( $this->m_strTaskDetails ) ) {
			$this->m_jsonTaskDetails = CStrings::strToJson( $this->m_strTaskDetails );
			unset( $this->m_strTaskDetails );
		}
		return $this->m_jsonTaskDetails;
	}

	public function sqlTaskDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getTaskDetails() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getTaskDetails() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'scheduled_task_type_id' => $this->getScheduledTaskTypeId(),
			'schedule_type_id' => $this->getScheduleTypeId(),
			'default_scheduled_task_id' => $this->getDefaultScheduledTaskId(),
			'scheduled_task_group_id' => $this->getScheduledTaskGroupId(),
			'event_type_id' => $this->getEventTypeId(),
			'event_sub_type_id' => $this->getEventSubTypeId(),
			'title' => $this->getTitle(),
			'schedule_details' => $this->getScheduleDetails(),
			'task_details' => $this->getTaskDetails(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>