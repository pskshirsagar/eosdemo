<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CNotifications
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseNotifications extends CEosPluralBase {

	/**
	 * @return CNotification[]
	 */
	public static function fetchNotifications( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CNotification', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CNotification
	 */
	public static function fetchNotification( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CNotification', $objDatabase );
	}

	public static function fetchNotificationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'notifications', $objDatabase );
	}

	public static function fetchNotificationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchNotification( sprintf( 'SELECT * FROM notifications WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchNotificationsByCid( $intCid, $objDatabase ) {
		return self::fetchNotifications( sprintf( 'SELECT * FROM notifications WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchNotificationsByNotificationTypeIdByCid( $intNotificationTypeId, $intCid, $objDatabase ) {
		return self::fetchNotifications( sprintf( 'SELECT * FROM notifications WHERE notification_type_id = %d AND cid = %d', ( int ) $intNotificationTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>