<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMarketingIntegrationSubscription extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.marketing_integration_subscriptions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intInternetListingServiceId;
	protected $m_intOccupancyTypeId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_boolIsLimited;
	protected $m_strInterruptions;
	protected $m_strIlsNotifiedOn;
	protected $m_strNotificationConfirmedOn;
	protected $m_strNotificationRejectedOn;
	protected $m_strRejectionNote;
	protected $m_strChangeNotifiedOn;
	protected $m_strSuspendedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strMissingInformation;
	protected $m_jsonMissingInformation;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsLimited = false;
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['internet_listing_service_id'] ) && $boolDirectSet ) $this->set( 'm_intInternetListingServiceId', trim( $arrValues['internet_listing_service_id'] ) ); elseif( isset( $arrValues['internet_listing_service_id'] ) ) $this->setInternetListingServiceId( $arrValues['internet_listing_service_id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['is_limited'] ) && $boolDirectSet ) $this->set( 'm_boolIsLimited', trim( stripcslashes( $arrValues['is_limited'] ) ) ); elseif( isset( $arrValues['is_limited'] ) ) $this->setIsLimited( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_limited'] ) : $arrValues['is_limited'] );
		if( isset( $arrValues['interruptions'] ) && $boolDirectSet ) $this->set( 'm_strInterruptions', trim( $arrValues['interruptions'] ) ); elseif( isset( $arrValues['interruptions'] ) ) $this->setInterruptions( $arrValues['interruptions'] );
		if( isset( $arrValues['ils_notified_on'] ) && $boolDirectSet ) $this->set( 'm_strIlsNotifiedOn', trim( $arrValues['ils_notified_on'] ) ); elseif( isset( $arrValues['ils_notified_on'] ) ) $this->setIlsNotifiedOn( $arrValues['ils_notified_on'] );
		if( isset( $arrValues['notification_confirmed_on'] ) && $boolDirectSet ) $this->set( 'm_strNotificationConfirmedOn', trim( $arrValues['notification_confirmed_on'] ) ); elseif( isset( $arrValues['notification_confirmed_on'] ) ) $this->setNotificationConfirmedOn( $arrValues['notification_confirmed_on'] );
		if( isset( $arrValues['notification_rejected_on'] ) && $boolDirectSet ) $this->set( 'm_strNotificationRejectedOn', trim( $arrValues['notification_rejected_on'] ) ); elseif( isset( $arrValues['notification_rejected_on'] ) ) $this->setNotificationRejectedOn( $arrValues['notification_rejected_on'] );
		if( isset( $arrValues['rejection_note'] ) && $boolDirectSet ) $this->set( 'm_strRejectionNote', trim( $arrValues['rejection_note'] ) ); elseif( isset( $arrValues['rejection_note'] ) ) $this->setRejectionNote( $arrValues['rejection_note'] );
		if( isset( $arrValues['change_notified_on'] ) && $boolDirectSet ) $this->set( 'm_strChangeNotifiedOn', trim( $arrValues['change_notified_on'] ) ); elseif( isset( $arrValues['change_notified_on'] ) ) $this->setChangeNotifiedOn( $arrValues['change_notified_on'] );
		if( isset( $arrValues['suspended_on'] ) && $boolDirectSet ) $this->set( 'm_strSuspendedOn', trim( $arrValues['suspended_on'] ) ); elseif( isset( $arrValues['suspended_on'] ) ) $this->setSuspendedOn( $arrValues['suspended_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['missing_information'] ) ) $this->set( 'm_strMissingInformation', trim( $arrValues['missing_information'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setInternetListingServiceId( $intInternetListingServiceId ) {
		$this->set( 'm_intInternetListingServiceId', CStrings::strToIntDef( $intInternetListingServiceId, NULL, false ) );
	}

	public function getInternetListingServiceId() {
		return $this->m_intInternetListingServiceId;
	}

	public function sqlInternetListingServiceId() {
		return ( true == isset( $this->m_intInternetListingServiceId ) ) ? ( string ) $this->m_intInternetListingServiceId : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : 'NULL';
	}

	public function setIsLimited( $boolIsLimited ) {
		$this->set( 'm_boolIsLimited', CStrings::strToBool( $boolIsLimited ) );
	}

	public function getIsLimited() {
		return $this->m_boolIsLimited;
	}

	public function sqlIsLimited() {
		return ( true == isset( $this->m_boolIsLimited ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLimited ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setInterruptions( $strInterruptions ) {
		$this->set( 'm_strInterruptions', CStrings::strTrimDef( $strInterruptions, -1, NULL, true ) );
	}

	public function getInterruptions() {
		return $this->m_strInterruptions;
	}

	public function sqlInterruptions() {
		return ( true == isset( $this->m_strInterruptions ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInterruptions ) : '\'' . addslashes( $this->m_strInterruptions ) . '\'' ) : 'NULL';
	}

	public function setIlsNotifiedOn( $strIlsNotifiedOn ) {
		$this->set( 'm_strIlsNotifiedOn', CStrings::strTrimDef( $strIlsNotifiedOn, -1, NULL, true ) );
	}

	public function getIlsNotifiedOn() {
		return $this->m_strIlsNotifiedOn;
	}

	public function sqlIlsNotifiedOn() {
		return ( true == isset( $this->m_strIlsNotifiedOn ) ) ? '\'' . $this->m_strIlsNotifiedOn . '\'' : 'NULL';
	}

	public function setNotificationConfirmedOn( $strNotificationConfirmedOn ) {
		$this->set( 'm_strNotificationConfirmedOn', CStrings::strTrimDef( $strNotificationConfirmedOn, -1, NULL, true ) );
	}

	public function getNotificationConfirmedOn() {
		return $this->m_strNotificationConfirmedOn;
	}

	public function sqlNotificationConfirmedOn() {
		return ( true == isset( $this->m_strNotificationConfirmedOn ) ) ? '\'' . $this->m_strNotificationConfirmedOn . '\'' : 'NULL';
	}

	public function setNotificationRejectedOn( $strNotificationRejectedOn ) {
		$this->set( 'm_strNotificationRejectedOn', CStrings::strTrimDef( $strNotificationRejectedOn, -1, NULL, true ) );
	}

	public function getNotificationRejectedOn() {
		return $this->m_strNotificationRejectedOn;
	}

	public function sqlNotificationRejectedOn() {
		return ( true == isset( $this->m_strNotificationRejectedOn ) ) ? '\'' . $this->m_strNotificationRejectedOn . '\'' : 'NULL';
	}

	public function setRejectionNote( $strRejectionNote ) {
		$this->set( 'm_strRejectionNote', CStrings::strTrimDef( $strRejectionNote, -1, NULL, true ) );
	}

	public function getRejectionNote() {
		return $this->m_strRejectionNote;
	}

	public function sqlRejectionNote() {
		return ( true == isset( $this->m_strRejectionNote ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRejectionNote ) : '\'' . addslashes( $this->m_strRejectionNote ) . '\'' ) : 'NULL';
	}

	public function setChangeNotifiedOn( $strChangeNotifiedOn ) {
		$this->set( 'm_strChangeNotifiedOn', CStrings::strTrimDef( $strChangeNotifiedOn, -1, NULL, true ) );
	}

	public function getChangeNotifiedOn() {
		return $this->m_strChangeNotifiedOn;
	}

	public function sqlChangeNotifiedOn() {
		return ( true == isset( $this->m_strChangeNotifiedOn ) ) ? '\'' . $this->m_strChangeNotifiedOn . '\'' : 'NULL';
	}

	public function setSuspendedOn( $strSuspendedOn ) {
		$this->set( 'm_strSuspendedOn', CStrings::strTrimDef( $strSuspendedOn, -1, NULL, true ) );
	}

	public function getSuspendedOn() {
		return $this->m_strSuspendedOn;
	}

	public function sqlSuspendedOn() {
		return ( true == isset( $this->m_strSuspendedOn ) ) ? '\'' . $this->m_strSuspendedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setMissingInformation( $jsonMissingInformation ) {
		if( true == valObj( $jsonMissingInformation, 'stdClass' ) ) {
			$this->set( 'm_jsonMissingInformation', $jsonMissingInformation );
		} elseif( true == valJsonString( $jsonMissingInformation ) ) {
			$this->set( 'm_jsonMissingInformation', CStrings::strToJson( $jsonMissingInformation ) );
		} else {
			$this->set( 'm_jsonMissingInformation', NULL );
		}
		unset( $this->m_strMissingInformation );
	}

	public function getMissingInformation() {
		if( true == isset( $this->m_strMissingInformation ) ) {
			$this->m_jsonMissingInformation = CStrings::strToJson( $this->m_strMissingInformation );
			unset( $this->m_strMissingInformation );
		}
		return $this->m_jsonMissingInformation;
	}

	public function sqlMissingInformation() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getMissingInformation() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getMissingInformation() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getMissingInformation() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, internet_listing_service_id, occupancy_type_id, details, is_limited, interruptions, ils_notified_on, notification_confirmed_on, notification_rejected_on, rejection_note, change_notified_on, suspended_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, missing_information )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlInternetListingServiceId() . ', ' .
		          $this->sqlOccupancyTypeId() . ', ' .
		          $this->sqlDetails() . ', ' .
		          $this->sqlIsLimited() . ', ' .
		          $this->sqlInterruptions() . ', ' .
		          $this->sqlIlsNotifiedOn() . ', ' .
		          $this->sqlNotificationConfirmedOn() . ', ' .
		          $this->sqlNotificationRejectedOn() . ', ' .
		          $this->sqlRejectionNote() . ', ' .
		          $this->sqlChangeNotifiedOn() . ', ' .
		          $this->sqlSuspendedOn() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlMissingInformation() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internet_listing_service_id = ' . $this->sqlInternetListingServiceId(). ',' ; } elseif( true == array_key_exists( 'InternetListingServiceId', $this->getChangedColumns() ) ) { $strSql .= ' internet_listing_service_id = ' . $this->sqlInternetListingServiceId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_limited = ' . $this->sqlIsLimited(). ',' ; } elseif( true == array_key_exists( 'IsLimited', $this->getChangedColumns() ) ) { $strSql .= ' is_limited = ' . $this->sqlIsLimited() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interruptions = ' . $this->sqlInterruptions(). ',' ; } elseif( true == array_key_exists( 'Interruptions', $this->getChangedColumns() ) ) { $strSql .= ' interruptions = ' . $this->sqlInterruptions() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ils_notified_on = ' . $this->sqlIlsNotifiedOn(). ',' ; } elseif( true == array_key_exists( 'IlsNotifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' ils_notified_on = ' . $this->sqlIlsNotifiedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notification_confirmed_on = ' . $this->sqlNotificationConfirmedOn(). ',' ; } elseif( true == array_key_exists( 'NotificationConfirmedOn', $this->getChangedColumns() ) ) { $strSql .= ' notification_confirmed_on = ' . $this->sqlNotificationConfirmedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notification_rejected_on = ' . $this->sqlNotificationRejectedOn(). ',' ; } elseif( true == array_key_exists( 'NotificationRejectedOn', $this->getChangedColumns() ) ) { $strSql .= ' notification_rejected_on = ' . $this->sqlNotificationRejectedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejection_note = ' . $this->sqlRejectionNote(). ',' ; } elseif( true == array_key_exists( 'RejectionNote', $this->getChangedColumns() ) ) { $strSql .= ' rejection_note = ' . $this->sqlRejectionNote() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' change_notified_on = ' . $this->sqlChangeNotifiedOn(). ',' ; } elseif( true == array_key_exists( 'ChangeNotifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' change_notified_on = ' . $this->sqlChangeNotifiedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' suspended_on = ' . $this->sqlSuspendedOn(). ',' ; } elseif( true == array_key_exists( 'SuspendedOn', $this->getChangedColumns() ) ) { $strSql .= ' suspended_on = ' . $this->sqlSuspendedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' missing_information = ' . $this->sqlMissingInformation(). ',' ; } elseif( true == array_key_exists( 'MissingInformation', $this->getChangedColumns() ) ) { $strSql .= ' missing_information = ' . $this->sqlMissingInformation() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'internet_listing_service_id' => $this->getInternetListingServiceId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'details' => $this->getDetails(),
			'is_limited' => $this->getIsLimited(),
			'interruptions' => $this->getInterruptions(),
			'ils_notified_on' => $this->getIlsNotifiedOn(),
			'notification_confirmed_on' => $this->getNotificationConfirmedOn(),
			'notification_rejected_on' => $this->getNotificationRejectedOn(),
			'rejection_note' => $this->getRejectionNote(),
			'change_notified_on' => $this->getChangeNotifiedOn(),
			'suspended_on' => $this->getSuspendedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'missing_information' => $this->getMissingInformation()
		);
	}

}
?>