<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CEmailTemplateFolders
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseEmailTemplateFolders extends CEosPluralBase {

	/**
	 * @return CEmailTemplateFolder[]
	 */
	public static function fetchEmailTemplateFolders( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CEmailTemplateFolder::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CEmailTemplateFolder
	 */
	public static function fetchEmailTemplateFolder( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEmailTemplateFolder::class, $objDatabase );
	}

	public static function fetchEmailTemplateFolderCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'email_template_folders', $objDatabase );
	}

	public static function fetchEmailTemplateFolderByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplateFolder( sprintf( 'SELECT * FROM email_template_folders WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplateFoldersByCid( $intCid, $objDatabase ) {
		return self::fetchEmailTemplateFolders( sprintf( 'SELECT * FROM email_template_folders WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchEmailTemplateFoldersByEmailTemplateFolderIdByCid( $intEmailTemplateFolderId, $intCid, $objDatabase ) {
		return self::fetchEmailTemplateFolders( sprintf( 'SELECT * FROM email_template_folders WHERE email_template_folder_id = %d AND cid = %d', $intEmailTemplateFolderId, $intCid ), $objDatabase );
	}

}
?>