<?php

class CBaseExternalMergeField extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.external_merge_fields';

	protected $m_intId;
	protected $m_intTransmissionVendorId;
	protected $m_strStateCode;
	protected $m_strApartmentAssociation;
	protected $m_arrstrExternalFormKeys;
	protected $m_strFormType;
	protected $m_strExternalMergeFieldName;
	protected $m_strDescription;
	protected $m_strDataType;
	protected $m_intMaxLength;
	protected $m_arrintDefaultMergeFieldIds;
	protected $m_intBlockDefaultMergeFieldId;
	protected $m_intMergeFieldGroupId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_boolIsPublished;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intMergeFieldSubGroupId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['transmission_vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionVendorId', trim( $arrValues['transmission_vendor_id'] ) ); elseif( isset( $arrValues['transmission_vendor_id'] ) ) $this->setTransmissionVendorId( $arrValues['transmission_vendor_id'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['apartment_association'] ) && $boolDirectSet ) $this->set( 'm_strApartmentAssociation', trim( stripcslashes( $arrValues['apartment_association'] ) ) ); elseif( isset( $arrValues['apartment_association'] ) ) $this->setApartmentAssociation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['apartment_association'] ) : $arrValues['apartment_association'] );
		if( isset( $arrValues['external_form_keys'] ) && $boolDirectSet ) $this->set( 'm_arrstrExternalFormKeys', trim( $arrValues['external_form_keys'] ) ); elseif( isset( $arrValues['external_form_keys'] ) ) $this->setExternalFormKeys( $arrValues['external_form_keys'] );
		if( isset( $arrValues['form_type'] ) && $boolDirectSet ) $this->set( 'm_strFormType', trim( stripcslashes( $arrValues['form_type'] ) ) ); elseif( isset( $arrValues['form_type'] ) ) $this->setFormType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['form_type'] ) : $arrValues['form_type'] );
		if( isset( $arrValues['external_merge_field_name'] ) && $boolDirectSet ) $this->set( 'm_strExternalMergeFieldName', trim( stripcslashes( $arrValues['external_merge_field_name'] ) ) ); elseif( isset( $arrValues['external_merge_field_name'] ) ) $this->setExternalMergeFieldName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['external_merge_field_name'] ) : $arrValues['external_merge_field_name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['data_type'] ) && $boolDirectSet ) $this->set( 'm_strDataType', trim( stripcslashes( $arrValues['data_type'] ) ) ); elseif( isset( $arrValues['data_type'] ) ) $this->setDataType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['data_type'] ) : $arrValues['data_type'] );
		if( isset( $arrValues['max_length'] ) && $boolDirectSet ) $this->set( 'm_intMaxLength', trim( $arrValues['max_length'] ) ); elseif( isset( $arrValues['max_length'] ) ) $this->setMaxLength( $arrValues['max_length'] );
		if( isset( $arrValues['default_merge_field_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintDefaultMergeFieldIds', trim( $arrValues['default_merge_field_ids'] ) ); elseif( isset( $arrValues['default_merge_field_ids'] ) ) $this->setDefaultMergeFieldIds( $arrValues['default_merge_field_ids'] );
		if( isset( $arrValues['block_default_merge_field_id'] ) && $boolDirectSet ) $this->set( 'm_intBlockDefaultMergeFieldId', trim( $arrValues['block_default_merge_field_id'] ) ); elseif( isset( $arrValues['block_default_merge_field_id'] ) ) $this->setBlockDefaultMergeFieldId( $arrValues['block_default_merge_field_id'] );
		if( isset( $arrValues['merge_field_group_id'] ) && $boolDirectSet ) $this->set( 'm_intMergeFieldGroupId', trim( $arrValues['merge_field_group_id'] ) ); elseif( isset( $arrValues['merge_field_group_id'] ) ) $this->setMergeFieldGroupId( $arrValues['merge_field_group_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['merge_field_sub_group_id'] ) && $boolDirectSet ) $this->set( 'm_intMergeFieldSubGroupId', trim( $arrValues['merge_field_sub_group_id'] ) ); elseif( isset( $arrValues['merge_field_sub_group_id'] ) ) $this->setMergeFieldSubGroupId( $arrValues['merge_field_sub_group_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTransmissionVendorId( $intTransmissionVendorId ) {
		$this->set( 'm_intTransmissionVendorId', CStrings::strToIntDef( $intTransmissionVendorId, NULL, false ) );
	}

	public function getTransmissionVendorId() {
		return $this->m_intTransmissionVendorId;
	}

	public function sqlTransmissionVendorId() {
		return ( true == isset( $this->m_intTransmissionVendorId ) ) ? ( string ) $this->m_intTransmissionVendorId : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 10, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setApartmentAssociation( $strApartmentAssociation ) {
		$this->set( 'm_strApartmentAssociation', CStrings::strTrimDef( $strApartmentAssociation, 10, NULL, true ) );
	}

	public function getApartmentAssociation() {
		return $this->m_strApartmentAssociation;
	}

	public function sqlApartmentAssociation() {
		return ( true == isset( $this->m_strApartmentAssociation ) ) ? '\'' . addslashes( $this->m_strApartmentAssociation ) . '\'' : 'NULL';
	}

	public function setExternalFormKeys( $arrstrExternalFormKeys ) {
		$this->set( 'm_arrstrExternalFormKeys', CStrings::strToArrIntDef( $arrstrExternalFormKeys, NULL ) );
	}

	public function getExternalFormKeys() {
		return $this->m_arrstrExternalFormKeys;
	}

	public function sqlExternalFormKeys() {
		return ( true == isset( $this->m_arrstrExternalFormKeys ) && true == valArr( $this->m_arrstrExternalFormKeys ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrstrExternalFormKeys, NULL ) . '\'' : 'NULL';
	}

	public function setFormType( $strFormType ) {
		$this->set( 'm_strFormType', CStrings::strTrimDef( $strFormType, 25, NULL, true ) );
	}

	public function getFormType() {
		return $this->m_strFormType;
	}

	public function sqlFormType() {
		return ( true == isset( $this->m_strFormType ) ) ? '\'' . addslashes( $this->m_strFormType ) . '\'' : 'NULL';
	}

	public function setExternalMergeFieldName( $strExternalMergeFieldName ) {
		$this->set( 'm_strExternalMergeFieldName', CStrings::strTrimDef( $strExternalMergeFieldName, 100, NULL, true ) );
	}

	public function getExternalMergeFieldName() {
		return $this->m_strExternalMergeFieldName;
	}

	public function sqlExternalMergeFieldName() {
		return ( true == isset( $this->m_strExternalMergeFieldName ) ) ? '\'' . addslashes( $this->m_strExternalMergeFieldName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 1000, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setDataType( $strDataType ) {
		$this->set( 'm_strDataType', CStrings::strTrimDef( $strDataType, 25, NULL, true ) );
	}

	public function getDataType() {
		return $this->m_strDataType;
	}

	public function sqlDataType() {
		return ( true == isset( $this->m_strDataType ) ) ? '\'' . addslashes( $this->m_strDataType ) . '\'' : 'NULL';
	}

	public function setMaxLength( $intMaxLength ) {
		$this->set( 'm_intMaxLength', CStrings::strToIntDef( $intMaxLength, NULL, false ) );
	}

	public function getMaxLength() {
		return $this->m_intMaxLength;
	}

	public function sqlMaxLength() {
		return ( true == isset( $this->m_intMaxLength ) ) ? ( string ) $this->m_intMaxLength : 'NULL';
	}

	public function setDefaultMergeFieldIds( $arrintDefaultMergeFieldIds ) {
		$this->set( 'm_arrintDefaultMergeFieldIds', CStrings::strToArrIntDef( $arrintDefaultMergeFieldIds, NULL ) );
	}

	public function getDefaultMergeFieldIds() {
		return $this->m_arrintDefaultMergeFieldIds;
	}

	public function sqlDefaultMergeFieldIds() {
		return ( true == isset( $this->m_arrintDefaultMergeFieldIds ) && true == valArr( $this->m_arrintDefaultMergeFieldIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintDefaultMergeFieldIds, NULL ) . '\'' : 'NULL';
	}

	public function setBlockDefaultMergeFieldId( $intBlockDefaultMergeFieldId ) {
		$this->set( 'm_intBlockDefaultMergeFieldId', CStrings::strToIntDef( $intBlockDefaultMergeFieldId, NULL, false ) );
	}

	public function getBlockDefaultMergeFieldId() {
		return $this->m_intBlockDefaultMergeFieldId;
	}

	public function sqlBlockDefaultMergeFieldId() {
		return ( true == isset( $this->m_intBlockDefaultMergeFieldId ) ) ? ( string ) $this->m_intBlockDefaultMergeFieldId : 'NULL';
	}

	public function setMergeFieldGroupId( $intMergeFieldGroupId ) {
		$this->set( 'm_intMergeFieldGroupId', CStrings::strToIntDef( $intMergeFieldGroupId, NULL, false ) );
	}

	public function getMergeFieldGroupId() {
		return $this->m_intMergeFieldGroupId;
	}

	public function sqlMergeFieldGroupId() {
		return ( true == isset( $this->m_intMergeFieldGroupId ) ) ? ( string ) $this->m_intMergeFieldGroupId : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setMergeFieldSubGroupId( $intMergeFieldSubGroupId ) {
		$this->set( 'm_intMergeFieldSubGroupId', CStrings::strToIntDef( $intMergeFieldSubGroupId, NULL, false ) );
	}

	public function getMergeFieldSubGroupId() {
		return $this->m_intMergeFieldSubGroupId;
	}

	public function sqlMergeFieldSubGroupId() {
		return ( true == isset( $this->m_intMergeFieldSubGroupId ) ) ? ( string ) $this->m_intMergeFieldSubGroupId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, transmission_vendor_id, state_code, apartment_association, external_form_keys, form_type, external_merge_field_name, description, data_type, max_length, default_merge_field_ids, block_default_merge_field_id, merge_field_group_id, details, is_published, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, merge_field_sub_group_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlTransmissionVendorId() . ', ' .
						$this->sqlStateCode() . ', ' .
						$this->sqlApartmentAssociation() . ', ' .
						$this->sqlExternalFormKeys() . ', ' .
						$this->sqlFormType() . ', ' .
						$this->sqlExternalMergeFieldName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlDataType() . ', ' .
						$this->sqlMaxLength() . ', ' .
						$this->sqlDefaultMergeFieldIds() . ', ' .
						$this->sqlBlockDefaultMergeFieldId() . ', ' .
						$this->sqlMergeFieldGroupId() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlMergeFieldSubGroupId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId(). ',' ; } elseif( true == array_key_exists( 'TransmissionVendorId', $this->getChangedColumns() ) ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode(). ',' ; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apartment_association = ' . $this->sqlApartmentAssociation(). ',' ; } elseif( true == array_key_exists( 'ApartmentAssociation', $this->getChangedColumns() ) ) { $strSql .= ' apartment_association = ' . $this->sqlApartmentAssociation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_form_keys = ' . $this->sqlExternalFormKeys(). ',' ; } elseif( true == array_key_exists( 'ExternalFormKeys', $this->getChangedColumns() ) ) { $strSql .= ' external_form_keys = ' . $this->sqlExternalFormKeys() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' form_type = ' . $this->sqlFormType(). ',' ; } elseif( true == array_key_exists( 'FormType', $this->getChangedColumns() ) ) { $strSql .= ' form_type = ' . $this->sqlFormType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_merge_field_name = ' . $this->sqlExternalMergeFieldName(). ',' ; } elseif( true == array_key_exists( 'ExternalMergeFieldName', $this->getChangedColumns() ) ) { $strSql .= ' external_merge_field_name = ' . $this->sqlExternalMergeFieldName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_type = ' . $this->sqlDataType(). ',' ; } elseif( true == array_key_exists( 'DataType', $this->getChangedColumns() ) ) { $strSql .= ' data_type = ' . $this->sqlDataType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_length = ' . $this->sqlMaxLength(). ',' ; } elseif( true == array_key_exists( 'MaxLength', $this->getChangedColumns() ) ) { $strSql .= ' max_length = ' . $this->sqlMaxLength() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_merge_field_ids = ' . $this->sqlDefaultMergeFieldIds(). ',' ; } elseif( true == array_key_exists( 'DefaultMergeFieldIds', $this->getChangedColumns() ) ) { $strSql .= ' default_merge_field_ids = ' . $this->sqlDefaultMergeFieldIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block_default_merge_field_id = ' . $this->sqlBlockDefaultMergeFieldId(). ',' ; } elseif( true == array_key_exists( 'BlockDefaultMergeFieldId', $this->getChangedColumns() ) ) { $strSql .= ' block_default_merge_field_id = ' . $this->sqlBlockDefaultMergeFieldId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merge_field_group_id = ' . $this->sqlMergeFieldGroupId(). ',' ; } elseif( true == array_key_exists( 'MergeFieldGroupId', $this->getChangedColumns() ) ) { $strSql .= ' merge_field_group_id = ' . $this->sqlMergeFieldGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merge_field_sub_group_id = ' . $this->sqlMergeFieldSubGroupId(). ',' ; } elseif( true == array_key_exists( 'MergeFieldSubGroupId', $this->getChangedColumns() ) ) { $strSql .= ' merge_field_sub_group_id = ' . $this->sqlMergeFieldSubGroupId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'transmission_vendor_id' => $this->getTransmissionVendorId(),
			'state_code' => $this->getStateCode(),
			'apartment_association' => $this->getApartmentAssociation(),
			'external_form_keys' => $this->getExternalFormKeys(),
			'form_type' => $this->getFormType(),
			'external_merge_field_name' => $this->getExternalMergeFieldName(),
			'description' => $this->getDescription(),
			'data_type' => $this->getDataType(),
			'max_length' => $this->getMaxLength(),
			'default_merge_field_ids' => $this->getDefaultMergeFieldIds(),
			'block_default_merge_field_id' => $this->getBlockDefaultMergeFieldId(),
			'merge_field_group_id' => $this->getMergeFieldGroupId(),
			'details' => $this->getDetails(),
			'is_published' => $this->getIsPublished(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'merge_field_sub_group_id' => $this->getMergeFieldSubGroupId()
		);
	}

}
?>