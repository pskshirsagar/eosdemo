<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseProcess extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.lease_processes';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intLeaseId;
	protected $m_intCustomerId;
	protected $m_intCustomerTypeId;
	protected $m_intCustomerRelationshipId;
	protected $m_intRefundCustomerPaymentAccountId;
	protected $m_intRefundPaymentTypeId;
	protected $m_intTransferLeaseId;
	protected $m_intCombinedLeaseId;
	protected $m_intTransferUnitSpaceId;
	protected $m_intMoveInFormId;
	protected $m_intMoveOutFormId;
	protected $m_intMoveOutReasonListItemId;
	protected $m_intTransferReasonListItemId;
	protected $m_intRefundMethodId;
	protected $m_strTerminationStartDate;
	protected $m_strReasonForLeaving;
	protected $m_strMoveInDate;
	protected $m_strMoveOutDate;
	protected $m_strNoticeDate;
	protected $m_strFmoStartedOn;
	protected $m_strFmoApprovedOn;
	protected $m_strFmoProcessedOn;
	protected $m_strRefundDate;
	protected $m_strTransferredOn;
	protected $m_strCollectionsStartDate;
	protected $m_strCollectionsBlockedReason;
	protected $m_intCollectionsBlockedBy;
	protected $m_strCollectionsBlockedOn;
	protected $m_strMoveInReviewCompletedOn;
	protected $m_strMoveOutReviewCompletedOn;
	protected $m_boolPostAcceleratedRent;
	protected $m_intIsTransferringIn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intTransferFromLeaseId;

	public function __construct() {
		parent::__construct();

		$this->m_boolPostAcceleratedRent = false;
		$this->m_intIsTransferringIn = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['customer_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerTypeId', trim( $arrValues['customer_type_id'] ) ); elseif( isset( $arrValues['customer_type_id'] ) ) $this->setCustomerTypeId( $arrValues['customer_type_id'] );
		if( isset( $arrValues['customer_relationship_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerRelationshipId', trim( $arrValues['customer_relationship_id'] ) ); elseif( isset( $arrValues['customer_relationship_id'] ) ) $this->setCustomerRelationshipId( $arrValues['customer_relationship_id'] );
		if( isset( $arrValues['refund_customer_payment_account_id'] ) && $boolDirectSet ) $this->set( 'm_intRefundCustomerPaymentAccountId', trim( $arrValues['refund_customer_payment_account_id'] ) ); elseif( isset( $arrValues['refund_customer_payment_account_id'] ) ) $this->setRefundCustomerPaymentAccountId( $arrValues['refund_customer_payment_account_id'] );
		if( isset( $arrValues['refund_payment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRefundPaymentTypeId', trim( $arrValues['refund_payment_type_id'] ) ); elseif( isset( $arrValues['refund_payment_type_id'] ) ) $this->setRefundPaymentTypeId( $arrValues['refund_payment_type_id'] );
		if( isset( $arrValues['transfer_lease_id'] ) && $boolDirectSet ) $this->set( 'm_intTransferLeaseId', trim( $arrValues['transfer_lease_id'] ) ); elseif( isset( $arrValues['transfer_lease_id'] ) ) $this->setTransferLeaseId( $arrValues['transfer_lease_id'] );
		if( isset( $arrValues['combined_lease_id'] ) && $boolDirectSet ) $this->set( 'm_intCombinedLeaseId', trim( $arrValues['combined_lease_id'] ) ); elseif( isset( $arrValues['combined_lease_id'] ) ) $this->setCombinedLeaseId( $arrValues['combined_lease_id'] );
		if( isset( $arrValues['transfer_unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intTransferUnitSpaceId', trim( $arrValues['transfer_unit_space_id'] ) ); elseif( isset( $arrValues['transfer_unit_space_id'] ) ) $this->setTransferUnitSpaceId( $arrValues['transfer_unit_space_id'] );
		if( isset( $arrValues['move_in_form_id'] ) && $boolDirectSet ) $this->set( 'm_intMoveInFormId', trim( $arrValues['move_in_form_id'] ) ); elseif( isset( $arrValues['move_in_form_id'] ) ) $this->setMoveInFormId( $arrValues['move_in_form_id'] );
		if( isset( $arrValues['move_out_form_id'] ) && $boolDirectSet ) $this->set( 'm_intMoveOutFormId', trim( $arrValues['move_out_form_id'] ) ); elseif( isset( $arrValues['move_out_form_id'] ) ) $this->setMoveOutFormId( $arrValues['move_out_form_id'] );
		if( isset( $arrValues['move_out_reason_list_item_id'] ) && $boolDirectSet ) $this->set( 'm_intMoveOutReasonListItemId', trim( $arrValues['move_out_reason_list_item_id'] ) ); elseif( isset( $arrValues['move_out_reason_list_item_id'] ) ) $this->setMoveOutReasonListItemId( $arrValues['move_out_reason_list_item_id'] );
		if( isset( $arrValues['transfer_reason_list_item_id'] ) && $boolDirectSet ) $this->set( 'm_intTransferReasonListItemId', trim( $arrValues['transfer_reason_list_item_id'] ) ); elseif( isset( $arrValues['transfer_reason_list_item_id'] ) ) $this->setTransferReasonListItemId( $arrValues['transfer_reason_list_item_id'] );
		if( isset( $arrValues['refund_method_id'] ) && $boolDirectSet ) $this->set( 'm_intRefundMethodId', trim( $arrValues['refund_method_id'] ) ); elseif( isset( $arrValues['refund_method_id'] ) ) $this->setRefundMethodId( $arrValues['refund_method_id'] );
		if( isset( $arrValues['termination_start_date'] ) && $boolDirectSet ) $this->set( 'm_strTerminationStartDate', trim( $arrValues['termination_start_date'] ) ); elseif( isset( $arrValues['termination_start_date'] ) ) $this->setTerminationStartDate( $arrValues['termination_start_date'] );
		if( isset( $arrValues['reason_for_leaving'] ) && $boolDirectSet ) $this->set( 'm_strReasonForLeaving', trim( $arrValues['reason_for_leaving'] ) ); elseif( isset( $arrValues['reason_for_leaving'] ) ) $this->setReasonForLeaving( $arrValues['reason_for_leaving'] );
		if( isset( $arrValues['move_in_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveInDate', trim( $arrValues['move_in_date'] ) ); elseif( isset( $arrValues['move_in_date'] ) ) $this->setMoveInDate( $arrValues['move_in_date'] );
		if( isset( $arrValues['move_out_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutDate', trim( $arrValues['move_out_date'] ) ); elseif( isset( $arrValues['move_out_date'] ) ) $this->setMoveOutDate( $arrValues['move_out_date'] );
		if( isset( $arrValues['notice_date'] ) && $boolDirectSet ) $this->set( 'm_strNoticeDate', trim( $arrValues['notice_date'] ) ); elseif( isset( $arrValues['notice_date'] ) ) $this->setNoticeDate( $arrValues['notice_date'] );
		if( isset( $arrValues['fmo_started_on'] ) && $boolDirectSet ) $this->set( 'm_strFmoStartedOn', trim( $arrValues['fmo_started_on'] ) ); elseif( isset( $arrValues['fmo_started_on'] ) ) $this->setFmoStartedOn( $arrValues['fmo_started_on'] );
		if( isset( $arrValues['fmo_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strFmoApprovedOn', trim( $arrValues['fmo_approved_on'] ) ); elseif( isset( $arrValues['fmo_approved_on'] ) ) $this->setFmoApprovedOn( $arrValues['fmo_approved_on'] );
		if( isset( $arrValues['fmo_processed_on'] ) && $boolDirectSet ) $this->set( 'm_strFmoProcessedOn', trim( $arrValues['fmo_processed_on'] ) ); elseif( isset( $arrValues['fmo_processed_on'] ) ) $this->setFmoProcessedOn( $arrValues['fmo_processed_on'] );
		if( isset( $arrValues['refund_date'] ) && $boolDirectSet ) $this->set( 'm_strRefundDate', trim( $arrValues['refund_date'] ) ); elseif( isset( $arrValues['refund_date'] ) ) $this->setRefundDate( $arrValues['refund_date'] );
		if( isset( $arrValues['transferred_on'] ) && $boolDirectSet ) $this->set( 'm_strTransferredOn', trim( $arrValues['transferred_on'] ) ); elseif( isset( $arrValues['transferred_on'] ) ) $this->setTransferredOn( $arrValues['transferred_on'] );
		if( isset( $arrValues['collections_start_date'] ) && $boolDirectSet ) $this->set( 'm_strCollectionsStartDate', trim( $arrValues['collections_start_date'] ) ); elseif( isset( $arrValues['collections_start_date'] ) ) $this->setCollectionsStartDate( $arrValues['collections_start_date'] );
		if( isset( $arrValues['collections_blocked_reason'] ) && $boolDirectSet ) $this->set( 'm_strCollectionsBlockedReason', trim( $arrValues['collections_blocked_reason'] ) ); elseif( isset( $arrValues['collections_blocked_reason'] ) ) $this->setCollectionsBlockedReason( $arrValues['collections_blocked_reason'] );
		if( isset( $arrValues['collections_blocked_by'] ) && $boolDirectSet ) $this->set( 'm_intCollectionsBlockedBy', trim( $arrValues['collections_blocked_by'] ) ); elseif( isset( $arrValues['collections_blocked_by'] ) ) $this->setCollectionsBlockedBy( $arrValues['collections_blocked_by'] );
		if( isset( $arrValues['collections_blocked_on'] ) && $boolDirectSet ) $this->set( 'm_strCollectionsBlockedOn', trim( $arrValues['collections_blocked_on'] ) ); elseif( isset( $arrValues['collections_blocked_on'] ) ) $this->setCollectionsBlockedOn( $arrValues['collections_blocked_on'] );
		if( isset( $arrValues['move_in_review_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strMoveInReviewCompletedOn', trim( $arrValues['move_in_review_completed_on'] ) ); elseif( isset( $arrValues['move_in_review_completed_on'] ) ) $this->setMoveInReviewCompletedOn( $arrValues['move_in_review_completed_on'] );
		if( isset( $arrValues['move_out_review_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutReviewCompletedOn', trim( $arrValues['move_out_review_completed_on'] ) ); elseif( isset( $arrValues['move_out_review_completed_on'] ) ) $this->setMoveOutReviewCompletedOn( $arrValues['move_out_review_completed_on'] );
		if( isset( $arrValues['post_accelerated_rent'] ) && $boolDirectSet ) $this->set( 'm_boolPostAcceleratedRent', trim( stripcslashes( $arrValues['post_accelerated_rent'] ) ) ); elseif( isset( $arrValues['post_accelerated_rent'] ) ) $this->setPostAcceleratedRent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['post_accelerated_rent'] ) : $arrValues['post_accelerated_rent'] );
		if( isset( $arrValues['is_transferring_in'] ) && $boolDirectSet ) $this->set( 'm_intIsTransferringIn', trim( $arrValues['is_transferring_in'] ) ); elseif( isset( $arrValues['is_transferring_in'] ) ) $this->setIsTransferringIn( $arrValues['is_transferring_in'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['transfer_from_lease_id'] ) && $boolDirectSet ) $this->set( 'm_intTransferFromLeaseId', trim( $arrValues['transfer_from_lease_id'] ) ); elseif( isset( $arrValues['transfer_from_lease_id'] ) ) $this->setTransferFromLeaseId( $arrValues['transfer_from_lease_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setCustomerTypeId( $intCustomerTypeId ) {
		$this->set( 'm_intCustomerTypeId', CStrings::strToIntDef( $intCustomerTypeId, NULL, false ) );
	}

	public function getCustomerTypeId() {
		return $this->m_intCustomerTypeId;
	}

	public function sqlCustomerTypeId() {
		return ( true == isset( $this->m_intCustomerTypeId ) ) ? ( string ) $this->m_intCustomerTypeId : 'NULL';
	}

	public function setCustomerRelationshipId( $intCustomerRelationshipId ) {
		$this->set( 'm_intCustomerRelationshipId', CStrings::strToIntDef( $intCustomerRelationshipId, NULL, false ) );
	}

	public function getCustomerRelationshipId() {
		return $this->m_intCustomerRelationshipId;
	}

	public function sqlCustomerRelationshipId() {
		return ( true == isset( $this->m_intCustomerRelationshipId ) ) ? ( string ) $this->m_intCustomerRelationshipId : 'NULL';
	}

	public function setRefundCustomerPaymentAccountId( $intRefundCustomerPaymentAccountId ) {
		$this->set( 'm_intRefundCustomerPaymentAccountId', CStrings::strToIntDef( $intRefundCustomerPaymentAccountId, NULL, false ) );
	}

	public function getRefundCustomerPaymentAccountId() {
		return $this->m_intRefundCustomerPaymentAccountId;
	}

	public function sqlRefundCustomerPaymentAccountId() {
		return ( true == isset( $this->m_intRefundCustomerPaymentAccountId ) ) ? ( string ) $this->m_intRefundCustomerPaymentAccountId : 'NULL';
	}

	public function setRefundPaymentTypeId( $intRefundPaymentTypeId ) {
		$this->set( 'm_intRefundPaymentTypeId', CStrings::strToIntDef( $intRefundPaymentTypeId, NULL, false ) );
	}

	public function getRefundPaymentTypeId() {
		return $this->m_intRefundPaymentTypeId;
	}

	public function sqlRefundPaymentTypeId() {
		return ( true == isset( $this->m_intRefundPaymentTypeId ) ) ? ( string ) $this->m_intRefundPaymentTypeId : 'NULL';
	}

	public function setTransferLeaseId( $intTransferLeaseId ) {
		$this->set( 'm_intTransferLeaseId', CStrings::strToIntDef( $intTransferLeaseId, NULL, false ) );
	}

	public function getTransferLeaseId() {
		return $this->m_intTransferLeaseId;
	}

	public function sqlTransferLeaseId() {
		return ( true == isset( $this->m_intTransferLeaseId ) ) ? ( string ) $this->m_intTransferLeaseId : 'NULL';
	}

	public function setCombinedLeaseId( $intCombinedLeaseId ) {
		$this->set( 'm_intCombinedLeaseId', CStrings::strToIntDef( $intCombinedLeaseId, NULL, false ) );
	}

	public function getCombinedLeaseId() {
		return $this->m_intCombinedLeaseId;
	}

	public function sqlCombinedLeaseId() {
		return ( true == isset( $this->m_intCombinedLeaseId ) ) ? ( string ) $this->m_intCombinedLeaseId : 'NULL';
	}

	public function setTransferUnitSpaceId( $intTransferUnitSpaceId ) {
		$this->set( 'm_intTransferUnitSpaceId', CStrings::strToIntDef( $intTransferUnitSpaceId, NULL, false ) );
	}

	public function getTransferUnitSpaceId() {
		return $this->m_intTransferUnitSpaceId;
	}

	public function sqlTransferUnitSpaceId() {
		return ( true == isset( $this->m_intTransferUnitSpaceId ) ) ? ( string ) $this->m_intTransferUnitSpaceId : 'NULL';
	}

	public function setMoveInFormId( $intMoveInFormId ) {
		$this->set( 'm_intMoveInFormId', CStrings::strToIntDef( $intMoveInFormId, NULL, false ) );
	}

	public function getMoveInFormId() {
		return $this->m_intMoveInFormId;
	}

	public function sqlMoveInFormId() {
		return ( true == isset( $this->m_intMoveInFormId ) ) ? ( string ) $this->m_intMoveInFormId : 'NULL';
	}

	public function setMoveOutFormId( $intMoveOutFormId ) {
		$this->set( 'm_intMoveOutFormId', CStrings::strToIntDef( $intMoveOutFormId, NULL, false ) );
	}

	public function getMoveOutFormId() {
		return $this->m_intMoveOutFormId;
	}

	public function sqlMoveOutFormId() {
		return ( true == isset( $this->m_intMoveOutFormId ) ) ? ( string ) $this->m_intMoveOutFormId : 'NULL';
	}

	public function setMoveOutReasonListItemId( $intMoveOutReasonListItemId ) {
		$this->set( 'm_intMoveOutReasonListItemId', CStrings::strToIntDef( $intMoveOutReasonListItemId, NULL, false ) );
	}

	public function getMoveOutReasonListItemId() {
		return $this->m_intMoveOutReasonListItemId;
	}

	public function sqlMoveOutReasonListItemId() {
		return ( true == isset( $this->m_intMoveOutReasonListItemId ) ) ? ( string ) $this->m_intMoveOutReasonListItemId : 'NULL';
	}

	public function setTransferReasonListItemId( $intTransferReasonListItemId ) {
		$this->set( 'm_intTransferReasonListItemId', CStrings::strToIntDef( $intTransferReasonListItemId, NULL, false ) );
	}

	public function getTransferReasonListItemId() {
		return $this->m_intTransferReasonListItemId;
	}

	public function sqlTransferReasonListItemId() {
		return ( true == isset( $this->m_intTransferReasonListItemId ) ) ? ( string ) $this->m_intTransferReasonListItemId : 'NULL';
	}

	public function setRefundMethodId( $intRefundMethodId ) {
		$this->set( 'm_intRefundMethodId', CStrings::strToIntDef( $intRefundMethodId, NULL, false ) );
	}

	public function getRefundMethodId() {
		return $this->m_intRefundMethodId;
	}

	public function sqlRefundMethodId() {
		return ( true == isset( $this->m_intRefundMethodId ) ) ? ( string ) $this->m_intRefundMethodId : 'NULL';
	}

	public function setTerminationStartDate( $strTerminationStartDate ) {
		$this->set( 'm_strTerminationStartDate', CStrings::strTrimDef( $strTerminationStartDate, -1, NULL, true ) );
	}

	public function getTerminationStartDate() {
		return $this->m_strTerminationStartDate;
	}

	public function sqlTerminationStartDate() {
		return ( true == isset( $this->m_strTerminationStartDate ) ) ? '\'' . $this->m_strTerminationStartDate . '\'' : 'NULL';
	}

	public function setReasonForLeaving( $strReasonForLeaving ) {
		$this->set( 'm_strReasonForLeaving', CStrings::strTrimDef( $strReasonForLeaving, -1, NULL, true ) );
	}

	public function getReasonForLeaving() {
		return $this->m_strReasonForLeaving;
	}

	public function sqlReasonForLeaving() {
		return ( true == isset( $this->m_strReasonForLeaving ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReasonForLeaving ) : '\'' . addslashes( $this->m_strReasonForLeaving ) . '\'' ) : 'NULL';
	}

	public function setMoveInDate( $strMoveInDate ) {
		$this->set( 'm_strMoveInDate', CStrings::strTrimDef( $strMoveInDate, -1, NULL, true ) );
	}

	public function getMoveInDate() {
		return $this->m_strMoveInDate;
	}

	public function sqlMoveInDate() {
		return ( true == isset( $this->m_strMoveInDate ) ) ? '\'' . $this->m_strMoveInDate . '\'' : 'NULL';
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->set( 'm_strMoveOutDate', CStrings::strTrimDef( $strMoveOutDate, -1, NULL, true ) );
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function sqlMoveOutDate() {
		return ( true == isset( $this->m_strMoveOutDate ) ) ? '\'' . $this->m_strMoveOutDate . '\'' : 'NULL';
	}

	public function setNoticeDate( $strNoticeDate ) {
		$this->set( 'm_strNoticeDate', CStrings::strTrimDef( $strNoticeDate, -1, NULL, true ) );
	}

	public function getNoticeDate() {
		return $this->m_strNoticeDate;
	}

	public function sqlNoticeDate() {
		return ( true == isset( $this->m_strNoticeDate ) ) ? '\'' . $this->m_strNoticeDate . '\'' : 'NULL';
	}

	public function setFmoStartedOn( $strFmoStartedOn ) {
		$this->set( 'm_strFmoStartedOn', CStrings::strTrimDef( $strFmoStartedOn, -1, NULL, true ) );
	}

	public function getFmoStartedOn() {
		return $this->m_strFmoStartedOn;
	}

	public function sqlFmoStartedOn() {
		return ( true == isset( $this->m_strFmoStartedOn ) ) ? '\'' . $this->m_strFmoStartedOn . '\'' : 'NULL';
	}

	public function setFmoApprovedOn( $strFmoApprovedOn ) {
		$this->set( 'm_strFmoApprovedOn', CStrings::strTrimDef( $strFmoApprovedOn, -1, NULL, true ) );
	}

	public function getFmoApprovedOn() {
		return $this->m_strFmoApprovedOn;
	}

	public function sqlFmoApprovedOn() {
		return ( true == isset( $this->m_strFmoApprovedOn ) ) ? '\'' . $this->m_strFmoApprovedOn . '\'' : 'NULL';
	}

	public function setFmoProcessedOn( $strFmoProcessedOn ) {
		$this->set( 'm_strFmoProcessedOn', CStrings::strTrimDef( $strFmoProcessedOn, -1, NULL, true ) );
	}

	public function getFmoProcessedOn() {
		return $this->m_strFmoProcessedOn;
	}

	public function sqlFmoProcessedOn() {
		return ( true == isset( $this->m_strFmoProcessedOn ) ) ? '\'' . $this->m_strFmoProcessedOn . '\'' : 'NULL';
	}

	public function setRefundDate( $strRefundDate ) {
		$this->set( 'm_strRefundDate', CStrings::strTrimDef( $strRefundDate, -1, NULL, true ) );
	}

	public function getRefundDate() {
		return $this->m_strRefundDate;
	}

	public function sqlRefundDate() {
		return ( true == isset( $this->m_strRefundDate ) ) ? '\'' . $this->m_strRefundDate . '\'' : 'NULL';
	}

	public function setTransferredOn( $strTransferredOn ) {
		$this->set( 'm_strTransferredOn', CStrings::strTrimDef( $strTransferredOn, -1, NULL, true ) );
	}

	public function getTransferredOn() {
		return $this->m_strTransferredOn;
	}

	public function sqlTransferredOn() {
		return ( true == isset( $this->m_strTransferredOn ) ) ? '\'' . $this->m_strTransferredOn . '\'' : 'NULL';
	}

	public function setCollectionsStartDate( $strCollectionsStartDate ) {
		$this->set( 'm_strCollectionsStartDate', CStrings::strTrimDef( $strCollectionsStartDate, -1, NULL, true ) );
	}

	public function getCollectionsStartDate() {
		return $this->m_strCollectionsStartDate;
	}

	public function sqlCollectionsStartDate() {
		return ( true == isset( $this->m_strCollectionsStartDate ) ) ? '\'' . $this->m_strCollectionsStartDate . '\'' : 'NULL';
	}

	public function setCollectionsBlockedReason( $strCollectionsBlockedReason ) {
		$this->set( 'm_strCollectionsBlockedReason', CStrings::strTrimDef( $strCollectionsBlockedReason, -1, NULL, true ) );
	}

	public function getCollectionsBlockedReason() {
		return $this->m_strCollectionsBlockedReason;
	}

	public function sqlCollectionsBlockedReason() {
		return ( true == isset( $this->m_strCollectionsBlockedReason ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCollectionsBlockedReason ) : '\'' . addslashes( $this->m_strCollectionsBlockedReason ) . '\'' ) : 'NULL';
	}

	public function setCollectionsBlockedBy( $intCollectionsBlockedBy ) {
		$this->set( 'm_intCollectionsBlockedBy', CStrings::strToIntDef( $intCollectionsBlockedBy, NULL, false ) );
	}

	public function getCollectionsBlockedBy() {
		return $this->m_intCollectionsBlockedBy;
	}

	public function sqlCollectionsBlockedBy() {
		return ( true == isset( $this->m_intCollectionsBlockedBy ) ) ? ( string ) $this->m_intCollectionsBlockedBy : 'NULL';
	}

	public function setCollectionsBlockedOn( $strCollectionsBlockedOn ) {
		$this->set( 'm_strCollectionsBlockedOn', CStrings::strTrimDef( $strCollectionsBlockedOn, -1, NULL, true ) );
	}

	public function getCollectionsBlockedOn() {
		return $this->m_strCollectionsBlockedOn;
	}

	public function sqlCollectionsBlockedOn() {
		return ( true == isset( $this->m_strCollectionsBlockedOn ) ) ? '\'' . $this->m_strCollectionsBlockedOn . '\'' : 'NULL';
	}

	public function setMoveInReviewCompletedOn( $strMoveInReviewCompletedOn ) {
		$this->set( 'm_strMoveInReviewCompletedOn', CStrings::strTrimDef( $strMoveInReviewCompletedOn, -1, NULL, true ) );
	}

	public function getMoveInReviewCompletedOn() {
		return $this->m_strMoveInReviewCompletedOn;
	}

	public function sqlMoveInReviewCompletedOn() {
		return ( true == isset( $this->m_strMoveInReviewCompletedOn ) ) ? '\'' . $this->m_strMoveInReviewCompletedOn . '\'' : 'NULL';
	}

	public function setMoveOutReviewCompletedOn( $strMoveOutReviewCompletedOn ) {
		$this->set( 'm_strMoveOutReviewCompletedOn', CStrings::strTrimDef( $strMoveOutReviewCompletedOn, -1, NULL, true ) );
	}

	public function getMoveOutReviewCompletedOn() {
		return $this->m_strMoveOutReviewCompletedOn;
	}

	public function sqlMoveOutReviewCompletedOn() {
		return ( true == isset( $this->m_strMoveOutReviewCompletedOn ) ) ? '\'' . $this->m_strMoveOutReviewCompletedOn . '\'' : 'NULL';
	}

	public function setPostAcceleratedRent( $boolPostAcceleratedRent ) {
		$this->set( 'm_boolPostAcceleratedRent', CStrings::strToBool( $boolPostAcceleratedRent ) );
	}

	public function getPostAcceleratedRent() {
		return $this->m_boolPostAcceleratedRent;
	}

	public function sqlPostAcceleratedRent() {
		return ( true == isset( $this->m_boolPostAcceleratedRent ) ) ? '\'' . ( true == ( bool ) $this->m_boolPostAcceleratedRent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTransferringIn( $intIsTransferringIn ) {
		$this->set( 'm_intIsTransferringIn', CStrings::strToIntDef( $intIsTransferringIn, NULL, false ) );
	}

	public function getIsTransferringIn() {
		return $this->m_intIsTransferringIn;
	}

	public function sqlIsTransferringIn() {
		return ( true == isset( $this->m_intIsTransferringIn ) ) ? ( string ) $this->m_intIsTransferringIn : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setTransferFromLeaseId( $intTransferFromLeaseId ) {
		$this->set( 'm_intTransferFromLeaseId', CStrings::strToIntDef( $intTransferFromLeaseId, NULL, false ) );
	}

	public function getTransferFromLeaseId() {
		return $this->m_intTransferFromLeaseId;
	}

	public function sqlTransferFromLeaseId() {
		return ( true == isset( $this->m_intTransferFromLeaseId ) ) ? ( string ) $this->m_intTransferFromLeaseId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, lease_id, customer_id, customer_type_id, customer_relationship_id, refund_customer_payment_account_id, refund_payment_type_id, transfer_lease_id, combined_lease_id, transfer_unit_space_id, move_in_form_id, move_out_form_id, move_out_reason_list_item_id, transfer_reason_list_item_id, refund_method_id, termination_start_date, reason_for_leaving, move_in_date, move_out_date, notice_date, fmo_started_on, fmo_approved_on, fmo_processed_on, refund_date, transferred_on, collections_start_date, collections_blocked_reason, collections_blocked_by, collections_blocked_on, move_in_review_completed_on, move_out_review_completed_on, post_accelerated_rent, is_transferring_in, updated_by, updated_on, created_by, created_on, details, transfer_from_lease_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlCustomerTypeId() . ', ' .
						$this->sqlCustomerRelationshipId() . ', ' .
						$this->sqlRefundCustomerPaymentAccountId() . ', ' .
						$this->sqlRefundPaymentTypeId() . ', ' .
						$this->sqlTransferLeaseId() . ', ' .
						$this->sqlCombinedLeaseId() . ', ' .
						$this->sqlTransferUnitSpaceId() . ', ' .
						$this->sqlMoveInFormId() . ', ' .
						$this->sqlMoveOutFormId() . ', ' .
						$this->sqlMoveOutReasonListItemId() . ', ' .
						$this->sqlTransferReasonListItemId() . ', ' .
						$this->sqlRefundMethodId() . ', ' .
						$this->sqlTerminationStartDate() . ', ' .
						$this->sqlReasonForLeaving() . ', ' .
						$this->sqlMoveInDate() . ', ' .
						$this->sqlMoveOutDate() . ', ' .
						$this->sqlNoticeDate() . ', ' .
						$this->sqlFmoStartedOn() . ', ' .
						$this->sqlFmoApprovedOn() . ', ' .
						$this->sqlFmoProcessedOn() . ', ' .
						$this->sqlRefundDate() . ', ' .
						$this->sqlTransferredOn() . ', ' .
						$this->sqlCollectionsStartDate() . ', ' .
						$this->sqlCollectionsBlockedReason() . ', ' .
						$this->sqlCollectionsBlockedBy() . ', ' .
						$this->sqlCollectionsBlockedOn() . ', ' .
						$this->sqlMoveInReviewCompletedOn() . ', ' .
						$this->sqlMoveOutReviewCompletedOn() . ', ' .
						$this->sqlPostAcceleratedRent() . ', ' .
						$this->sqlIsTransferringIn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlTransferFromLeaseId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_type_id = ' . $this->sqlCustomerTypeId(). ',' ; } elseif( true == array_key_exists( 'CustomerTypeId', $this->getChangedColumns() ) ) { $strSql .= ' customer_type_id = ' . $this->sqlCustomerTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_relationship_id = ' . $this->sqlCustomerRelationshipId(). ',' ; } elseif( true == array_key_exists( 'CustomerRelationshipId', $this->getChangedColumns() ) ) { $strSql .= ' customer_relationship_id = ' . $this->sqlCustomerRelationshipId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' refund_customer_payment_account_id = ' . $this->sqlRefundCustomerPaymentAccountId(). ',' ; } elseif( true == array_key_exists( 'RefundCustomerPaymentAccountId', $this->getChangedColumns() ) ) { $strSql .= ' refund_customer_payment_account_id = ' . $this->sqlRefundCustomerPaymentAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' refund_payment_type_id = ' . $this->sqlRefundPaymentTypeId(). ',' ; } elseif( true == array_key_exists( 'RefundPaymentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' refund_payment_type_id = ' . $this->sqlRefundPaymentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transfer_lease_id = ' . $this->sqlTransferLeaseId(). ',' ; } elseif( true == array_key_exists( 'TransferLeaseId', $this->getChangedColumns() ) ) { $strSql .= ' transfer_lease_id = ' . $this->sqlTransferLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' combined_lease_id = ' . $this->sqlCombinedLeaseId(). ',' ; } elseif( true == array_key_exists( 'CombinedLeaseId', $this->getChangedColumns() ) ) { $strSql .= ' combined_lease_id = ' . $this->sqlCombinedLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transfer_unit_space_id = ' . $this->sqlTransferUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'TransferUnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' transfer_unit_space_id = ' . $this->sqlTransferUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_form_id = ' . $this->sqlMoveInFormId(). ',' ; } elseif( true == array_key_exists( 'MoveInFormId', $this->getChangedColumns() ) ) { $strSql .= ' move_in_form_id = ' . $this->sqlMoveInFormId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_form_id = ' . $this->sqlMoveOutFormId(). ',' ; } elseif( true == array_key_exists( 'MoveOutFormId', $this->getChangedColumns() ) ) { $strSql .= ' move_out_form_id = ' . $this->sqlMoveOutFormId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_reason_list_item_id = ' . $this->sqlMoveOutReasonListItemId(). ',' ; } elseif( true == array_key_exists( 'MoveOutReasonListItemId', $this->getChangedColumns() ) ) { $strSql .= ' move_out_reason_list_item_id = ' . $this->sqlMoveOutReasonListItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transfer_reason_list_item_id = ' . $this->sqlTransferReasonListItemId(). ',' ; } elseif( true == array_key_exists( 'TransferReasonListItemId', $this->getChangedColumns() ) ) { $strSql .= ' transfer_reason_list_item_id = ' . $this->sqlTransferReasonListItemId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' refund_method_id = ' . $this->sqlRefundMethodId(). ',' ; } elseif( true == array_key_exists( 'RefundMethodId', $this->getChangedColumns() ) ) { $strSql .= ' refund_method_id = ' . $this->sqlRefundMethodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_start_date = ' . $this->sqlTerminationStartDate(). ',' ; } elseif( true == array_key_exists( 'TerminationStartDate', $this->getChangedColumns() ) ) { $strSql .= ' termination_start_date = ' . $this->sqlTerminationStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reason_for_leaving = ' . $this->sqlReasonForLeaving(). ',' ; } elseif( true == array_key_exists( 'ReasonForLeaving', $this->getChangedColumns() ) ) { $strSql .= ' reason_for_leaving = ' . $this->sqlReasonForLeaving() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate(). ',' ; } elseif( true == array_key_exists( 'MoveInDate', $this->getChangedColumns() ) ) { $strSql .= ' move_in_date = ' . $this->sqlMoveInDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate(). ',' ; } elseif( true == array_key_exists( 'MoveOutDate', $this->getChangedColumns() ) ) { $strSql .= ' move_out_date = ' . $this->sqlMoveOutDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notice_date = ' . $this->sqlNoticeDate(). ',' ; } elseif( true == array_key_exists( 'NoticeDate', $this->getChangedColumns() ) ) { $strSql .= ' notice_date = ' . $this->sqlNoticeDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fmo_started_on = ' . $this->sqlFmoStartedOn(). ',' ; } elseif( true == array_key_exists( 'FmoStartedOn', $this->getChangedColumns() ) ) { $strSql .= ' fmo_started_on = ' . $this->sqlFmoStartedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fmo_approved_on = ' . $this->sqlFmoApprovedOn(). ',' ; } elseif( true == array_key_exists( 'FmoApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' fmo_approved_on = ' . $this->sqlFmoApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fmo_processed_on = ' . $this->sqlFmoProcessedOn(). ',' ; } elseif( true == array_key_exists( 'FmoProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' fmo_processed_on = ' . $this->sqlFmoProcessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' refund_date = ' . $this->sqlRefundDate(). ',' ; } elseif( true == array_key_exists( 'RefundDate', $this->getChangedColumns() ) ) { $strSql .= ' refund_date = ' . $this->sqlRefundDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transferred_on = ' . $this->sqlTransferredOn(). ',' ; } elseif( true == array_key_exists( 'TransferredOn', $this->getChangedColumns() ) ) { $strSql .= ' transferred_on = ' . $this->sqlTransferredOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' collections_start_date = ' . $this->sqlCollectionsStartDate(). ',' ; } elseif( true == array_key_exists( 'CollectionsStartDate', $this->getChangedColumns() ) ) { $strSql .= ' collections_start_date = ' . $this->sqlCollectionsStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' collections_blocked_reason = ' . $this->sqlCollectionsBlockedReason(). ',' ; } elseif( true == array_key_exists( 'CollectionsBlockedReason', $this->getChangedColumns() ) ) { $strSql .= ' collections_blocked_reason = ' . $this->sqlCollectionsBlockedReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' collections_blocked_by = ' . $this->sqlCollectionsBlockedBy(). ',' ; } elseif( true == array_key_exists( 'CollectionsBlockedBy', $this->getChangedColumns() ) ) { $strSql .= ' collections_blocked_by = ' . $this->sqlCollectionsBlockedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' collections_blocked_on = ' . $this->sqlCollectionsBlockedOn(). ',' ; } elseif( true == array_key_exists( 'CollectionsBlockedOn', $this->getChangedColumns() ) ) { $strSql .= ' collections_blocked_on = ' . $this->sqlCollectionsBlockedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_review_completed_on = ' . $this->sqlMoveInReviewCompletedOn(). ',' ; } elseif( true == array_key_exists( 'MoveInReviewCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' move_in_review_completed_on = ' . $this->sqlMoveInReviewCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_out_review_completed_on = ' . $this->sqlMoveOutReviewCompletedOn(). ',' ; } elseif( true == array_key_exists( 'MoveOutReviewCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' move_out_review_completed_on = ' . $this->sqlMoveOutReviewCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_accelerated_rent = ' . $this->sqlPostAcceleratedRent(). ',' ; } elseif( true == array_key_exists( 'PostAcceleratedRent', $this->getChangedColumns() ) ) { $strSql .= ' post_accelerated_rent = ' . $this->sqlPostAcceleratedRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_transferring_in = ' . $this->sqlIsTransferringIn(). ',' ; } elseif( true == array_key_exists( 'IsTransferringIn', $this->getChangedColumns() ) ) { $strSql .= ' is_transferring_in = ' . $this->sqlIsTransferringIn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transfer_from_lease_id = ' . $this->sqlTransferFromLeaseId(). ',' ; } elseif( true == array_key_exists( 'TransferFromLeaseId', $this->getChangedColumns() ) ) { $strSql .= ' transfer_from_lease_id = ' . $this->sqlTransferFromLeaseId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'lease_id' => $this->getLeaseId(),
			'customer_id' => $this->getCustomerId(),
			'customer_type_id' => $this->getCustomerTypeId(),
			'customer_relationship_id' => $this->getCustomerRelationshipId(),
			'refund_customer_payment_account_id' => $this->getRefundCustomerPaymentAccountId(),
			'refund_payment_type_id' => $this->getRefundPaymentTypeId(),
			'transfer_lease_id' => $this->getTransferLeaseId(),
			'combined_lease_id' => $this->getCombinedLeaseId(),
			'transfer_unit_space_id' => $this->getTransferUnitSpaceId(),
			'move_in_form_id' => $this->getMoveInFormId(),
			'move_out_form_id' => $this->getMoveOutFormId(),
			'move_out_reason_list_item_id' => $this->getMoveOutReasonListItemId(),
			'transfer_reason_list_item_id' => $this->getTransferReasonListItemId(),
			'refund_method_id' => $this->getRefundMethodId(),
			'termination_start_date' => $this->getTerminationStartDate(),
			'reason_for_leaving' => $this->getReasonForLeaving(),
			'move_in_date' => $this->getMoveInDate(),
			'move_out_date' => $this->getMoveOutDate(),
			'notice_date' => $this->getNoticeDate(),
			'fmo_started_on' => $this->getFmoStartedOn(),
			'fmo_approved_on' => $this->getFmoApprovedOn(),
			'fmo_processed_on' => $this->getFmoProcessedOn(),
			'refund_date' => $this->getRefundDate(),
			'transferred_on' => $this->getTransferredOn(),
			'collections_start_date' => $this->getCollectionsStartDate(),
			'collections_blocked_reason' => $this->getCollectionsBlockedReason(),
			'collections_blocked_by' => $this->getCollectionsBlockedBy(),
			'collections_blocked_on' => $this->getCollectionsBlockedOn(),
			'move_in_review_completed_on' => $this->getMoveInReviewCompletedOn(),
			'move_out_review_completed_on' => $this->getMoveOutReviewCompletedOn(),
			'post_accelerated_rent' => $this->getPostAcceleratedRent(),
			'is_transferring_in' => $this->getIsTransferringIn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'transfer_from_lease_id' => $this->getTransferFromLeaseId()
		);
	}

}
?>