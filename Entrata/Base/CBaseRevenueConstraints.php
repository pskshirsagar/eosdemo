<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueConstraints
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRevenueConstraints extends CEosPluralBase {

	/**
	 * @return CRevenueConstraint[]
	 */
	public static function fetchRevenueConstraints( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CRevenueConstraint', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRevenueConstraint
	 */
	public static function fetchRevenueConstraint( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRevenueConstraint', $objDatabase );
	}

	public static function fetchRevenueConstraintCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_constraints', $objDatabase );
	}

	public static function fetchRevenueConstraintByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRevenueConstraint( sprintf( 'SELECT * FROM revenue_constraints WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueConstraintsByCid( $intCid, $objDatabase ) {
		return self::fetchRevenueConstraints( sprintf( 'SELECT * FROM revenue_constraints WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>