<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTrainingDatabaseUsers
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseTrainingDatabaseUsers extends CEosPluralBase {

	/**
	 * @return CTrainingDatabaseUser[]
	 */
	public static function fetchTrainingDatabaseUsers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CTrainingDatabaseUser::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CTrainingDatabaseUser
	 */
	public static function fetchTrainingDatabaseUser( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTrainingDatabaseUser::class, $objDatabase );
	}

	public static function fetchTrainingDatabaseUserCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'training_database_users', $objDatabase );
	}

	public static function fetchTrainingDatabaseUserByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchTrainingDatabaseUser( sprintf( 'SELECT * FROM training_database_users WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchTrainingDatabaseUsersByCid( $intCid, $objDatabase ) {
		return self::fetchTrainingDatabaseUsers( sprintf( 'SELECT * FROM training_database_users WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchTrainingDatabaseUsersByTrainingDatabaseIdByCid( $intTrainingDatabaseId, $intCid, $objDatabase ) {
		return self::fetchTrainingDatabaseUsers( sprintf( 'SELECT * FROM training_database_users WHERE training_database_id = %d AND cid = %d', $intTrainingDatabaseId, $intCid ), $objDatabase );
	}

	public static function fetchTrainingDatabaseUsersByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchTrainingDatabaseUsers( sprintf( 'SELECT * FROM training_database_users WHERE company_user_id = %d AND cid = %d', $intCompanyUserId, $intCid ), $objDatabase );
	}

}
?>