<?php

class CBaseDefaultChecklistItem extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_checklist_items';

	protected $m_intId;
	protected $m_intDefaultChecklistId;
	protected $m_intChecklistItemTypeId;
	protected $m_intChecklistItemTypeOptionId;
	protected $m_strItemTitle;
	protected $m_boolShowOnPortals;
	protected $m_boolIsRequired;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolShowOnPortals = false;
		$this->m_boolIsRequired = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['default_checklist_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultChecklistId', trim( $arrValues['default_checklist_id'] ) ); elseif( isset( $arrValues['default_checklist_id'] ) ) $this->setDefaultChecklistId( $arrValues['default_checklist_id'] );
		if( isset( $arrValues['checklist_item_type_id'] ) && $boolDirectSet ) $this->set( 'm_intChecklistItemTypeId', trim( $arrValues['checklist_item_type_id'] ) ); elseif( isset( $arrValues['checklist_item_type_id'] ) ) $this->setChecklistItemTypeId( $arrValues['checklist_item_type_id'] );
		if( isset( $arrValues['checklist_item_type_option_id'] ) && $boolDirectSet ) $this->set( 'm_intChecklistItemTypeOptionId', trim( $arrValues['checklist_item_type_option_id'] ) ); elseif( isset( $arrValues['checklist_item_type_option_id'] ) ) $this->setChecklistItemTypeOptionId( $arrValues['checklist_item_type_option_id'] );
		if( isset( $arrValues['item_title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strItemTitle', trim( stripcslashes( $arrValues['item_title'] ) ) ); elseif( isset( $arrValues['item_title'] ) ) $this->setItemTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['item_title'] ) : $arrValues['item_title'] );
		if( isset( $arrValues['show_on_portals'] ) && $boolDirectSet ) $this->set( 'm_boolShowOnPortals', trim( stripcslashes( $arrValues['show_on_portals'] ) ) ); elseif( isset( $arrValues['show_on_portals'] ) ) $this->setShowOnPortals( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_on_portals'] ) : $arrValues['show_on_portals'] );
		if( isset( $arrValues['is_required'] ) && $boolDirectSet ) $this->set( 'm_boolIsRequired', trim( stripcslashes( $arrValues['is_required'] ) ) ); elseif( isset( $arrValues['is_required'] ) ) $this->setIsRequired( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_required'] ) : $arrValues['is_required'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDefaultChecklistId( $intDefaultChecklistId ) {
		$this->set( 'm_intDefaultChecklistId', CStrings::strToIntDef( $intDefaultChecklistId, NULL, false ) );
	}

	public function getDefaultChecklistId() {
		return $this->m_intDefaultChecklistId;
	}

	public function sqlDefaultChecklistId() {
		return ( true == isset( $this->m_intDefaultChecklistId ) ) ? ( string ) $this->m_intDefaultChecklistId : 'NULL';
	}

	public function setChecklistItemTypeId( $intChecklistItemTypeId ) {
		$this->set( 'm_intChecklistItemTypeId', CStrings::strToIntDef( $intChecklistItemTypeId, NULL, false ) );
	}

	public function getChecklistItemTypeId() {
		return $this->m_intChecklistItemTypeId;
	}

	public function sqlChecklistItemTypeId() {
		return ( true == isset( $this->m_intChecklistItemTypeId ) ) ? ( string ) $this->m_intChecklistItemTypeId : 'NULL';
	}

	public function setChecklistItemTypeOptionId( $intChecklistItemTypeOptionId ) {
		$this->set( 'm_intChecklistItemTypeOptionId', CStrings::strToIntDef( $intChecklistItemTypeOptionId, NULL, false ) );
	}

	public function getChecklistItemTypeOptionId() {
		return $this->m_intChecklistItemTypeOptionId;
	}

	public function sqlChecklistItemTypeOptionId() {
		return ( true == isset( $this->m_intChecklistItemTypeOptionId ) ) ? ( string ) $this->m_intChecklistItemTypeOptionId : 'NULL';
	}

	public function setItemTitle( $strItemTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strItemTitle', CStrings::strTrimDef( $strItemTitle, 240, NULL, true ), $strLocaleCode );
	}

	public function getItemTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strItemTitle', $strLocaleCode );
	}

	public function sqlItemTitle() {
		return ( true == isset( $this->m_strItemTitle ) ) ? '\'' . addslashes( $this->m_strItemTitle ) . '\'' : 'NULL';
	}

	public function setShowOnPortals( $boolShowOnPortals ) {
		$this->set( 'm_boolShowOnPortals', CStrings::strToBool( $boolShowOnPortals ) );
	}

	public function getShowOnPortals() {
		return $this->m_boolShowOnPortals;
	}

	public function sqlShowOnPortals() {
		return ( true == isset( $this->m_boolShowOnPortals ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowOnPortals ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsRequired( $boolIsRequired ) {
		$this->set( 'm_boolIsRequired', CStrings::strToBool( $boolIsRequired ) );
	}

	public function getIsRequired() {
		return $this->m_boolIsRequired;
	}

	public function sqlIsRequired() {
		return ( true == isset( $this->m_boolIsRequired ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRequired ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'default_checklist_id' => $this->getDefaultChecklistId(),
			'checklist_item_type_id' => $this->getChecklistItemTypeId(),
			'checklist_item_type_option_id' => $this->getChecklistItemTypeOptionId(),
			'item_title' => $this->getItemTitle(),
			'show_on_portals' => $this->getShowOnPortals(),
			'is_required' => $this->getIsRequired(),
			'details' => $this->getDetails()
		);
	}

}
?>