<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportNewGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReportNewGroups extends CEosPluralBase {

	/**
	 * @return CReportNewGroup[]
	 */
	public static function fetchReportNewGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CReportNewGroup::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CReportNewGroup
	 */
	public static function fetchReportNewGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportNewGroup::class, $objDatabase );
	}

	public static function fetchReportNewGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_new_groups', $objDatabase );
	}

	public static function fetchReportNewGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchReportNewGroup( sprintf( 'SELECT * FROM report_new_groups WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchReportNewGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchReportNewGroups( sprintf( 'SELECT * FROM report_new_groups WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchReportNewGroupsByReportGroupTypeIdByCid( $intReportGroupTypeId, $intCid, $objDatabase ) {
		return self::fetchReportNewGroups( sprintf( 'SELECT * FROM report_new_groups WHERE report_group_type_id = %d AND cid = %d', $intReportGroupTypeId, $intCid ), $objDatabase );
	}

	public static function fetchReportNewGroupsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchReportNewGroups( sprintf( 'SELECT * FROM report_new_groups WHERE company_user_id = %d AND cid = %d', $intCompanyUserId, $intCid ), $objDatabase );
	}

}
?>