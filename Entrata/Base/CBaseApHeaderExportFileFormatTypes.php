<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApHeaderExportFileFormatTypes
 * Do not add any new functions to this class.
 */

class CBaseApHeaderExportFileFormatTypes extends CEosPluralBase {

	/**
	 * @return CApHeaderExportFileFormatType[]
	 */
	public static function fetchApHeaderExportFileFormatTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CApHeaderExportFileFormatType::class, $objDatabase );
	}

	/**
	 * @return CApHeaderExportFileFormatType
	 */
	public static function fetchApHeaderExportFileFormatType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApHeaderExportFileFormatType::class, $objDatabase );
	}

	public static function fetchApHeaderExportFileFormatTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_header_export_file_format_types', $objDatabase );
	}

	public static function fetchApHeaderExportFileFormatTypeById( $intId, $objDatabase ) {
		return self::fetchApHeaderExportFileFormatType( sprintf( 'SELECT * FROM ap_header_export_file_format_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchApHeaderExportFileFormatTypesByApHeaderExportBatchTypeId( $intApHeaderExportBatchTypeId, $objDatabase ) {
		return self::fetchApHeaderExportFileFormatTypes( sprintf( 'SELECT * FROM ap_header_export_file_format_types WHERE ap_header_export_batch_type_id = %d', $intApHeaderExportBatchTypeId ), $objDatabase );
	}

}
?>