<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDelinquencyPolicy extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.delinquency_policies';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDefaultDelinquencyPolicyId;
	protected $m_intOccupancyTypeId;
	protected $m_intDelinquencyThresholdTypeId;
	protected $m_intDelinquencyPolicyTypeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_fltThresholdAmount;
	protected $m_fltSmallBalanceAmount;
	protected $m_fltSendToCollectionsThreshold;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intParentDelinquencyPolicyId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['default_delinquency_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultDelinquencyPolicyId', trim( $arrValues['default_delinquency_policy_id'] ) ); elseif( isset( $arrValues['default_delinquency_policy_id'] ) ) $this->setDefaultDelinquencyPolicyId( $arrValues['default_delinquency_policy_id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['delinquency_threshold_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDelinquencyThresholdTypeId', trim( $arrValues['delinquency_threshold_type_id'] ) ); elseif( isset( $arrValues['delinquency_threshold_type_id'] ) ) $this->setDelinquencyThresholdTypeId( $arrValues['delinquency_threshold_type_id'] );
		if( isset( $arrValues['delinquency_policy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDelinquencyPolicyTypeId', trim( $arrValues['delinquency_policy_type_id'] ) ); elseif( isset( $arrValues['delinquency_policy_type_id'] ) ) $this->setDelinquencyPolicyTypeId( $arrValues['delinquency_policy_type_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['threshold_amount'] ) && $boolDirectSet ) $this->set( 'm_fltThresholdAmount', trim( $arrValues['threshold_amount'] ) ); elseif( isset( $arrValues['threshold_amount'] ) ) $this->setThresholdAmount( $arrValues['threshold_amount'] );
		if( isset( $arrValues['small_balance_amount'] ) && $boolDirectSet ) $this->set( 'm_fltSmallBalanceAmount', trim( $arrValues['small_balance_amount'] ) ); elseif( isset( $arrValues['small_balance_amount'] ) ) $this->setSmallBalanceAmount( $arrValues['small_balance_amount'] );
		if( isset( $arrValues['send_to_collections_threshold'] ) && $boolDirectSet ) $this->set( 'm_fltSendToCollectionsThreshold', trim( $arrValues['send_to_collections_threshold'] ) ); elseif( isset( $arrValues['send_to_collections_threshold'] ) ) $this->setSendToCollectionsThreshold( $arrValues['send_to_collections_threshold'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['parent_delinquency_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intParentDelinquencyPolicyId', trim( $arrValues['parent_delinquency_policy_id'] ) ); elseif( isset( $arrValues['parent_delinquency_policy_id'] ) ) $this->setParentDelinquencyPolicyId( $arrValues['parent_delinquency_policy_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDefaultDelinquencyPolicyId( $intDefaultDelinquencyPolicyId ) {
		$this->set( 'm_intDefaultDelinquencyPolicyId', CStrings::strToIntDef( $intDefaultDelinquencyPolicyId, NULL, false ) );
	}

	public function getDefaultDelinquencyPolicyId() {
		return $this->m_intDefaultDelinquencyPolicyId;
	}

	public function sqlDefaultDelinquencyPolicyId() {
		return ( true == isset( $this->m_intDefaultDelinquencyPolicyId ) ) ? ( string ) $this->m_intDefaultDelinquencyPolicyId : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : 'NULL';
	}

	public function setDelinquencyThresholdTypeId( $intDelinquencyThresholdTypeId ) {
		$this->set( 'm_intDelinquencyThresholdTypeId', CStrings::strToIntDef( $intDelinquencyThresholdTypeId, NULL, false ) );
	}

	public function getDelinquencyThresholdTypeId() {
		return $this->m_intDelinquencyThresholdTypeId;
	}

	public function sqlDelinquencyThresholdTypeId() {
		return ( true == isset( $this->m_intDelinquencyThresholdTypeId ) ) ? ( string ) $this->m_intDelinquencyThresholdTypeId : 'NULL';
	}

	public function setDelinquencyPolicyTypeId( $intDelinquencyPolicyTypeId ) {
		$this->set( 'm_intDelinquencyPolicyTypeId', CStrings::strToIntDef( $intDelinquencyPolicyTypeId, NULL, false ) );
	}

	public function getDelinquencyPolicyTypeId() {
		return $this->m_intDelinquencyPolicyTypeId;
	}

	public function sqlDelinquencyPolicyTypeId() {
		return ( true == isset( $this->m_intDelinquencyPolicyTypeId ) ) ? ( string ) $this->m_intDelinquencyPolicyTypeId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 200, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setThresholdAmount( $fltThresholdAmount ) {
		$this->set( 'm_fltThresholdAmount', CStrings::strToFloatDef( $fltThresholdAmount, NULL, false, 2 ) );
	}

	public function getThresholdAmount() {
		return $this->m_fltThresholdAmount;
	}

	public function sqlThresholdAmount() {
		return ( true == isset( $this->m_fltThresholdAmount ) ) ? ( string ) $this->m_fltThresholdAmount : 'NULL';
	}

	public function setSmallBalanceAmount( $fltSmallBalanceAmount ) {
		$this->set( 'm_fltSmallBalanceAmount', CStrings::strToFloatDef( $fltSmallBalanceAmount, NULL, false, 2 ) );
	}

	public function getSmallBalanceAmount() {
		return $this->m_fltSmallBalanceAmount;
	}

	public function sqlSmallBalanceAmount() {
		return ( true == isset( $this->m_fltSmallBalanceAmount ) ) ? ( string ) $this->m_fltSmallBalanceAmount : 'NULL';
	}

	public function setSendToCollectionsThreshold( $fltSendToCollectionsThreshold ) {
		$this->set( 'm_fltSendToCollectionsThreshold', CStrings::strToFloatDef( $fltSendToCollectionsThreshold, NULL, false, 2 ) );
	}

	public function getSendToCollectionsThreshold() {
		return $this->m_fltSendToCollectionsThreshold;
	}

	public function sqlSendToCollectionsThreshold() {
		return ( true == isset( $this->m_fltSendToCollectionsThreshold ) ) ? ( string ) $this->m_fltSendToCollectionsThreshold : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setParentDelinquencyPolicyId( $intParentDelinquencyPolicyId ) {
		$this->set( 'm_intParentDelinquencyPolicyId', CStrings::strToIntDef( $intParentDelinquencyPolicyId, NULL, false ) );
	}

	public function getParentDelinquencyPolicyId() {
		return $this->m_intParentDelinquencyPolicyId;
	}

	public function sqlParentDelinquencyPolicyId() {
		return ( true == isset( $this->m_intParentDelinquencyPolicyId ) ) ? ( string ) $this->m_intParentDelinquencyPolicyId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, default_delinquency_policy_id, occupancy_type_id, delinquency_threshold_type_id, delinquency_policy_type_id, name, description, threshold_amount, small_balance_amount, send_to_collections_threshold, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details, parent_delinquency_policy_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlDefaultDelinquencyPolicyId() . ', ' .
						$this->sqlOccupancyTypeId() . ', ' .
						$this->sqlDelinquencyThresholdTypeId() . ', ' .
						$this->sqlDelinquencyPolicyTypeId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlThresholdAmount() . ', ' .
						$this->sqlSmallBalanceAmount() . ', ' .
						$this->sqlSendToCollectionsThreshold() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlParentDelinquencyPolicyId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_delinquency_policy_id = ' . $this->sqlDefaultDelinquencyPolicyId(). ',' ; } elseif( true == array_key_exists( 'DefaultDelinquencyPolicyId', $this->getChangedColumns() ) ) { $strSql .= ' default_delinquency_policy_id = ' . $this->sqlDefaultDelinquencyPolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquency_threshold_type_id = ' . $this->sqlDelinquencyThresholdTypeId(). ',' ; } elseif( true == array_key_exists( 'DelinquencyThresholdTypeId', $this->getChangedColumns() ) ) { $strSql .= ' delinquency_threshold_type_id = ' . $this->sqlDelinquencyThresholdTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquency_policy_type_id = ' . $this->sqlDelinquencyPolicyTypeId(). ',' ; } elseif( true == array_key_exists( 'DelinquencyPolicyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' delinquency_policy_type_id = ' . $this->sqlDelinquencyPolicyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' threshold_amount = ' . $this->sqlThresholdAmount(). ',' ; } elseif( true == array_key_exists( 'ThresholdAmount', $this->getChangedColumns() ) ) { $strSql .= ' threshold_amount = ' . $this->sqlThresholdAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' small_balance_amount = ' . $this->sqlSmallBalanceAmount(). ',' ; } elseif( true == array_key_exists( 'SmallBalanceAmount', $this->getChangedColumns() ) ) { $strSql .= ' small_balance_amount = ' . $this->sqlSmallBalanceAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' send_to_collections_threshold = ' . $this->sqlSendToCollectionsThreshold(). ',' ; } elseif( true == array_key_exists( 'SendToCollectionsThreshold', $this->getChangedColumns() ) ) { $strSql .= ' send_to_collections_threshold = ' . $this->sqlSendToCollectionsThreshold() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_delinquency_policy_id = ' . $this->sqlParentDelinquencyPolicyId(). ',' ; } elseif( true == array_key_exists( 'ParentDelinquencyPolicyId', $this->getChangedColumns() ) ) { $strSql .= ' parent_delinquency_policy_id = ' . $this->sqlParentDelinquencyPolicyId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'default_delinquency_policy_id' => $this->getDefaultDelinquencyPolicyId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'delinquency_threshold_type_id' => $this->getDelinquencyThresholdTypeId(),
			'delinquency_policy_type_id' => $this->getDelinquencyPolicyTypeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'threshold_amount' => $this->getThresholdAmount(),
			'small_balance_amount' => $this->getSmallBalanceAmount(),
			'send_to_collections_threshold' => $this->getSendToCollectionsThreshold(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'parent_delinquency_policy_id' => $this->getParentDelinquencyPolicyId()
		);
	}

}
?>