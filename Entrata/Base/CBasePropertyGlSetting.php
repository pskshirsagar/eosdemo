<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyGlSetting extends CEosSingularBase {

    const TABLE_NAME = 'public.property_gl_settings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intGlTreeId;
	protected $m_intRentBankAccountId;
	protected $m_intDepositBankAccountId;
	protected $m_intRefundBankAccountId;
	protected $m_intApBankAccountId;
	protected $m_intApGlAccountId;
	protected $m_intInterCoReimbursementBankAccountId;
	protected $m_intMarketRentGlAccountId;
	protected $m_intGainToLeaseGlAccountId;
	protected $m_intLossToLeaseGlAccountId;
	protected $m_intVacancyLossGlAccountId;
	protected $m_intDelinquentRentGlAccountId;
	protected $m_intPrepaidRentGlAccountId;
	protected $m_intRetainedEarningsGlAccountId;
	protected $m_intInterCoArGlAccountId;
	protected $m_intInterCoApGlAccountId;
	protected $m_intPoBalancesApGlAccountId;
	protected $m_intPurchasesClearingGlAccountId;
	protected $m_intPendingReimbursementsGlAccountId;
	protected $m_intUnclaimedPropertyGlAccountId;
	protected $m_intAccelRentArCodeId;
	protected $m_intSkipAccelRentArCodeId;
	protected $m_intRentArCodeId;
	protected $m_intDepositArCodeId;
	protected $m_intPercentOfSalesArCodeId;
	protected $m_intMarketRentTypeId;
	protected $m_intLeaseExpirationStructureId;
	protected $m_intDrawRequestApHeaderTypeId;
	protected $m_intUnclaimedPropertyApPayeeId;
	protected $m_intApReceivingTypeId;
	protected $m_intIsCashBasis;
	protected $m_intFiscalYearEndMonth;
	protected $m_intArAdvanceDay;
	protected $m_intApAdvanceDay;
	protected $m_intGlAdvanceDay;
	protected $m_intAutoLockArOffsetDays;
	protected $m_intAutoLockApOffsetDays;
	protected $m_intAutoLockGlOffsetDays;
	protected $m_strArPostMonth;
	protected $m_strApPostMonth;
	protected $m_strGlPostMonth;
	protected $m_strArLockMonth;
	protected $m_strApLockMonth;
	protected $m_strGlLockMonth;
	protected $m_strArSoftLockMonth;
	protected $m_strApSoftLockMonth;
	protected $m_strGlSoftLockMonth;
	protected $m_strOpeningPeriod;
	protected $m_strActivationDate;
	protected $m_strTerminationDate;
	protected $m_intArAutoAdvancePostMonth;
	protected $m_intApAutoAdvancePostMonth;
	protected $m_intGlAutoAdvancePostMonth;
	protected $m_boolIsAutoLockAr;
	protected $m_boolIsAutoLockAp;
	protected $m_boolIsAutoLockGl;
	protected $m_intIgnoreHolidaysAndWeekends;
	protected $m_intAutoPostGprAdjustment;
	protected $m_strGprBookBasis;
	protected $m_intIsModifiedAccrualGpr;
	protected $m_intUseMarketRentInGpr;
	protected $m_intUseBeginOfMonthMarketRent;
	protected $m_intGroupExcludedUnitsInGpr;
	protected $m_intCalculateVacancyOnArClose;
	protected $m_boolProrateGprVacancyForTransfers;
	protected $m_intDepositPaymentsInTransit;
	protected $m_boolPostPosToGlDuringMonthEnd;
	protected $m_intAutoPostRefundInvoice;
	protected $m_intAllocateRentFirst;
	protected $m_boolPostingAndCachingDisabled;
	protected $m_intUseCustomArCodes;
	protected $m_boolRequirePosForReceiving;
	protected $m_boolRequirePosForJobs;
	protected $m_boolRequirePosForManualInvoices;
	protected $m_boolRequireVendorCompliance;
	protected $m_boolActivateStandardPosting;
	protected $m_boolActivatePoPosting;
	protected $m_boolActivateAssetPosting;
	protected $m_boolIsArMigrationMode;
	protected $m_boolIsApMigrationMode;
	protected $m_boolIsAssetMigrationMode;
	protected $m_boolAutoUpdateBudgetedRent;
	protected $m_intUserValidationMode;
	protected $m_intScriptValidationMode;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intMilitaryMacBankAccountId;
	protected $m_intJobCostingApBankAccountId;
	protected $m_intCcApBankAccountId;
	protected $m_intStraightLineAdjustmentArCodeId;

	public function __construct() {
		parent::__construct();

		$this->m_intMarketRentTypeId = '1';
		$this->m_intDrawRequestApHeaderTypeId = '1';
		$this->m_intApReceivingTypeId = '1';
		$this->m_intIsCashBasis = '0';
		$this->m_intFiscalYearEndMonth = '12';
		$this->m_intArAdvanceDay = '25';
		$this->m_intApAdvanceDay = '25';
		$this->m_intGlAdvanceDay = '25';
		$this->m_intAutoLockArOffsetDays = '30';
		$this->m_intAutoLockApOffsetDays = '30';
		$this->m_intAutoLockGlOffsetDays = '30';
		$this->m_intArAutoAdvancePostMonth = '0';
		$this->m_intApAutoAdvancePostMonth = '0';
		$this->m_intGlAutoAdvancePostMonth = '0';
		$this->m_boolIsAutoLockAr = false;
		$this->m_boolIsAutoLockAp = false;
		$this->m_boolIsAutoLockGl = false;
		$this->m_intIgnoreHolidaysAndWeekends = '0';
		$this->m_intAutoPostGprAdjustment = '1';
		$this->m_strGprBookBasis = 'accrual';
		$this->m_intIsModifiedAccrualGpr = '0';
		$this->m_intUseMarketRentInGpr = '0';
		$this->m_intUseBeginOfMonthMarketRent = '1';
		$this->m_intGroupExcludedUnitsInGpr = '1';
		$this->m_intCalculateVacancyOnArClose = '1';
		$this->m_boolProrateGprVacancyForTransfers = true;
		$this->m_intDepositPaymentsInTransit = '0';
		$this->m_boolPostPosToGlDuringMonthEnd = true;
		$this->m_intAutoPostRefundInvoice = '0';
		$this->m_intAllocateRentFirst = '0';
		$this->m_boolPostingAndCachingDisabled = false;
		$this->m_intUseCustomArCodes = '0';
		$this->m_boolRequirePosForReceiving = true;
		$this->m_boolRequirePosForJobs = true;
		$this->m_boolRequirePosForManualInvoices = true;
		$this->m_boolRequireVendorCompliance = false;
		$this->m_boolActivateStandardPosting = false;
		$this->m_boolActivatePoPosting = false;
		$this->m_boolActivateAssetPosting = false;
		$this->m_boolIsArMigrationMode = false;
		$this->m_boolIsApMigrationMode = false;
		$this->m_boolIsAssetMigrationMode = false;
		$this->m_boolAutoUpdateBudgetedRent = false;
		$this->m_intUserValidationMode = '0';
		$this->m_intScriptValidationMode = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['gl_tree_id'] ) && $boolDirectSet ) $this->set( 'm_intGlTreeId', trim( $arrValues['gl_tree_id'] ) ); elseif( isset( $arrValues['gl_tree_id'] ) ) $this->setGlTreeId( $arrValues['gl_tree_id'] );
		if( isset( $arrValues['rent_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intRentBankAccountId', trim( $arrValues['rent_bank_account_id'] ) ); elseif( isset( $arrValues['rent_bank_account_id'] ) ) $this->setRentBankAccountId( $arrValues['rent_bank_account_id'] );
		if( isset( $arrValues['deposit_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDepositBankAccountId', trim( $arrValues['deposit_bank_account_id'] ) ); elseif( isset( $arrValues['deposit_bank_account_id'] ) ) $this->setDepositBankAccountId( $arrValues['deposit_bank_account_id'] );
		if( isset( $arrValues['refund_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intRefundBankAccountId', trim( $arrValues['refund_bank_account_id'] ) ); elseif( isset( $arrValues['refund_bank_account_id'] ) ) $this->setRefundBankAccountId( $arrValues['refund_bank_account_id'] );
		if( isset( $arrValues['ap_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intApBankAccountId', trim( $arrValues['ap_bank_account_id'] ) ); elseif( isset( $arrValues['ap_bank_account_id'] ) ) $this->setApBankAccountId( $arrValues['ap_bank_account_id'] );
		if( isset( $arrValues['ap_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intApGlAccountId', trim( $arrValues['ap_gl_account_id'] ) ); elseif( isset( $arrValues['ap_gl_account_id'] ) ) $this->setApGlAccountId( $arrValues['ap_gl_account_id'] );
		if( isset( $arrValues['inter_co_reimbursement_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intInterCoReimbursementBankAccountId', trim( $arrValues['inter_co_reimbursement_bank_account_id'] ) ); elseif( isset( $arrValues['inter_co_reimbursement_bank_account_id'] ) ) $this->setInterCoReimbursementBankAccountId( $arrValues['inter_co_reimbursement_bank_account_id'] );
		if( isset( $arrValues['market_rent_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intMarketRentGlAccountId', trim( $arrValues['market_rent_gl_account_id'] ) ); elseif( isset( $arrValues['market_rent_gl_account_id'] ) ) $this->setMarketRentGlAccountId( $arrValues['market_rent_gl_account_id'] );
		if( isset( $arrValues['gain_to_lease_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intGainToLeaseGlAccountId', trim( $arrValues['gain_to_lease_gl_account_id'] ) ); elseif( isset( $arrValues['gain_to_lease_gl_account_id'] ) ) $this->setGainToLeaseGlAccountId( $arrValues['gain_to_lease_gl_account_id'] );
		if( isset( $arrValues['loss_to_lease_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intLossToLeaseGlAccountId', trim( $arrValues['loss_to_lease_gl_account_id'] ) ); elseif( isset( $arrValues['loss_to_lease_gl_account_id'] ) ) $this->setLossToLeaseGlAccountId( $arrValues['loss_to_lease_gl_account_id'] );
		if( isset( $arrValues['vacancy_loss_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intVacancyLossGlAccountId', trim( $arrValues['vacancy_loss_gl_account_id'] ) ); elseif( isset( $arrValues['vacancy_loss_gl_account_id'] ) ) $this->setVacancyLossGlAccountId( $arrValues['vacancy_loss_gl_account_id'] );
		if( isset( $arrValues['delinquent_rent_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDelinquentRentGlAccountId', trim( $arrValues['delinquent_rent_gl_account_id'] ) ); elseif( isset( $arrValues['delinquent_rent_gl_account_id'] ) ) $this->setDelinquentRentGlAccountId( $arrValues['delinquent_rent_gl_account_id'] );
		if( isset( $arrValues['prepaid_rent_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intPrepaidRentGlAccountId', trim( $arrValues['prepaid_rent_gl_account_id'] ) ); elseif( isset( $arrValues['prepaid_rent_gl_account_id'] ) ) $this->setPrepaidRentGlAccountId( $arrValues['prepaid_rent_gl_account_id'] );
		if( isset( $arrValues['retained_earnings_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intRetainedEarningsGlAccountId', trim( $arrValues['retained_earnings_gl_account_id'] ) ); elseif( isset( $arrValues['retained_earnings_gl_account_id'] ) ) $this->setRetainedEarningsGlAccountId( $arrValues['retained_earnings_gl_account_id'] );
		if( isset( $arrValues['inter_co_ar_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intInterCoArGlAccountId', trim( $arrValues['inter_co_ar_gl_account_id'] ) ); elseif( isset( $arrValues['inter_co_ar_gl_account_id'] ) ) $this->setInterCoArGlAccountId( $arrValues['inter_co_ar_gl_account_id'] );
		if( isset( $arrValues['inter_co_ap_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intInterCoApGlAccountId', trim( $arrValues['inter_co_ap_gl_account_id'] ) ); elseif( isset( $arrValues['inter_co_ap_gl_account_id'] ) ) $this->setInterCoApGlAccountId( $arrValues['inter_co_ap_gl_account_id'] );
		if( isset( $arrValues['po_balances_ap_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intPoBalancesApGlAccountId', trim( $arrValues['po_balances_ap_gl_account_id'] ) ); elseif( isset( $arrValues['po_balances_ap_gl_account_id'] ) ) $this->setPoBalancesApGlAccountId( $arrValues['po_balances_ap_gl_account_id'] );
		if( isset( $arrValues['purchases_clearing_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intPurchasesClearingGlAccountId', trim( $arrValues['purchases_clearing_gl_account_id'] ) ); elseif( isset( $arrValues['purchases_clearing_gl_account_id'] ) ) $this->setPurchasesClearingGlAccountId( $arrValues['purchases_clearing_gl_account_id'] );
		if( isset( $arrValues['pending_reimbursements_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intPendingReimbursementsGlAccountId', trim( $arrValues['pending_reimbursements_gl_account_id'] ) ); elseif( isset( $arrValues['pending_reimbursements_gl_account_id'] ) ) $this->setPendingReimbursementsGlAccountId( $arrValues['pending_reimbursements_gl_account_id'] );
		if( isset( $arrValues['unclaimed_property_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUnclaimedPropertyGlAccountId', trim( $arrValues['unclaimed_property_gl_account_id'] ) ); elseif( isset( $arrValues['unclaimed_property_gl_account_id'] ) ) $this->setUnclaimedPropertyGlAccountId( $arrValues['unclaimed_property_gl_account_id'] );
		if( isset( $arrValues['accel_rent_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intAccelRentArCodeId', trim( $arrValues['accel_rent_ar_code_id'] ) ); elseif( isset( $arrValues['accel_rent_ar_code_id'] ) ) $this->setAccelRentArCodeId( $arrValues['accel_rent_ar_code_id'] );
		if( isset( $arrValues['skip_accel_rent_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intSkipAccelRentArCodeId', trim( $arrValues['skip_accel_rent_ar_code_id'] ) ); elseif( isset( $arrValues['skip_accel_rent_ar_code_id'] ) ) $this->setSkipAccelRentArCodeId( $arrValues['skip_accel_rent_ar_code_id'] );
		if( isset( $arrValues['rent_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intRentArCodeId', trim( $arrValues['rent_ar_code_id'] ) ); elseif( isset( $arrValues['rent_ar_code_id'] ) ) $this->setRentArCodeId( $arrValues['rent_ar_code_id'] );
		if( isset( $arrValues['deposit_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intDepositArCodeId', trim( $arrValues['deposit_ar_code_id'] ) ); elseif( isset( $arrValues['deposit_ar_code_id'] ) ) $this->setDepositArCodeId( $arrValues['deposit_ar_code_id'] );
		if( isset( $arrValues['percent_of_sales_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intPercentOfSalesArCodeId', trim( $arrValues['percent_of_sales_ar_code_id'] ) ); elseif( isset( $arrValues['percent_of_sales_ar_code_id'] ) ) $this->setPercentOfSalesArCodeId( $arrValues['percent_of_sales_ar_code_id'] );
		if( isset( $arrValues['market_rent_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMarketRentTypeId', trim( $arrValues['market_rent_type_id'] ) ); elseif( isset( $arrValues['market_rent_type_id'] ) ) $this->setMarketRentTypeId( $arrValues['market_rent_type_id'] );
		if( isset( $arrValues['lease_expiration_structure_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseExpirationStructureId', trim( $arrValues['lease_expiration_structure_id'] ) ); elseif( isset( $arrValues['lease_expiration_structure_id'] ) ) $this->setLeaseExpirationStructureId( $arrValues['lease_expiration_structure_id'] );
		if( isset( $arrValues['draw_request_ap_header_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDrawRequestApHeaderTypeId', trim( $arrValues['draw_request_ap_header_type_id'] ) ); elseif( isset( $arrValues['draw_request_ap_header_type_id'] ) ) $this->setDrawRequestApHeaderTypeId( $arrValues['draw_request_ap_header_type_id'] );
		if( isset( $arrValues['unclaimed_property_ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intUnclaimedPropertyApPayeeId', trim( $arrValues['unclaimed_property_ap_payee_id'] ) ); elseif( isset( $arrValues['unclaimed_property_ap_payee_id'] ) ) $this->setUnclaimedPropertyApPayeeId( $arrValues['unclaimed_property_ap_payee_id'] );
		if( isset( $arrValues['ap_receiving_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApReceivingTypeId', trim( $arrValues['ap_receiving_type_id'] ) ); elseif( isset( $arrValues['ap_receiving_type_id'] ) ) $this->setApReceivingTypeId( $arrValues['ap_receiving_type_id'] );
		if( isset( $arrValues['is_cash_basis'] ) && $boolDirectSet ) $this->set( 'm_intIsCashBasis', trim( $arrValues['is_cash_basis'] ) ); elseif( isset( $arrValues['is_cash_basis'] ) ) $this->setIsCashBasis( $arrValues['is_cash_basis'] );
		if( isset( $arrValues['fiscal_year_end_month'] ) && $boolDirectSet ) $this->set( 'm_intFiscalYearEndMonth', trim( $arrValues['fiscal_year_end_month'] ) ); elseif( isset( $arrValues['fiscal_year_end_month'] ) ) $this->setFiscalYearEndMonth( $arrValues['fiscal_year_end_month'] );
		if( isset( $arrValues['ar_advance_day'] ) && $boolDirectSet ) $this->set( 'm_intArAdvanceDay', trim( $arrValues['ar_advance_day'] ) ); elseif( isset( $arrValues['ar_advance_day'] ) ) $this->setArAdvanceDay( $arrValues['ar_advance_day'] );
		if( isset( $arrValues['ap_advance_day'] ) && $boolDirectSet ) $this->set( 'm_intApAdvanceDay', trim( $arrValues['ap_advance_day'] ) ); elseif( isset( $arrValues['ap_advance_day'] ) ) $this->setApAdvanceDay( $arrValues['ap_advance_day'] );
		if( isset( $arrValues['gl_advance_day'] ) && $boolDirectSet ) $this->set( 'm_intGlAdvanceDay', trim( $arrValues['gl_advance_day'] ) ); elseif( isset( $arrValues['gl_advance_day'] ) ) $this->setGlAdvanceDay( $arrValues['gl_advance_day'] );
		if( isset( $arrValues['auto_lock_ar_offset_days'] ) && $boolDirectSet ) $this->set( 'm_intAutoLockArOffsetDays', trim( $arrValues['auto_lock_ar_offset_days'] ) ); elseif( isset( $arrValues['auto_lock_ar_offset_days'] ) ) $this->setAutoLockArOffsetDays( $arrValues['auto_lock_ar_offset_days'] );
		if( isset( $arrValues['auto_lock_ap_offset_days'] ) && $boolDirectSet ) $this->set( 'm_intAutoLockApOffsetDays', trim( $arrValues['auto_lock_ap_offset_days'] ) ); elseif( isset( $arrValues['auto_lock_ap_offset_days'] ) ) $this->setAutoLockApOffsetDays( $arrValues['auto_lock_ap_offset_days'] );
		if( isset( $arrValues['auto_lock_gl_offset_days'] ) && $boolDirectSet ) $this->set( 'm_intAutoLockGlOffsetDays', trim( $arrValues['auto_lock_gl_offset_days'] ) ); elseif( isset( $arrValues['auto_lock_gl_offset_days'] ) ) $this->setAutoLockGlOffsetDays( $arrValues['auto_lock_gl_offset_days'] );
		if( isset( $arrValues['ar_post_month'] ) && $boolDirectSet ) $this->set( 'm_strArPostMonth', trim( $arrValues['ar_post_month'] ) ); elseif( isset( $arrValues['ar_post_month'] ) ) $this->setArPostMonth( $arrValues['ar_post_month'] );
		if( isset( $arrValues['ap_post_month'] ) && $boolDirectSet ) $this->set( 'm_strApPostMonth', trim( $arrValues['ap_post_month'] ) ); elseif( isset( $arrValues['ap_post_month'] ) ) $this->setApPostMonth( $arrValues['ap_post_month'] );
		if( isset( $arrValues['gl_post_month'] ) && $boolDirectSet ) $this->set( 'm_strGlPostMonth', trim( $arrValues['gl_post_month'] ) ); elseif( isset( $arrValues['gl_post_month'] ) ) $this->setGlPostMonth( $arrValues['gl_post_month'] );
		if( isset( $arrValues['ar_lock_month'] ) && $boolDirectSet ) $this->set( 'm_strArLockMonth', trim( $arrValues['ar_lock_month'] ) ); elseif( isset( $arrValues['ar_lock_month'] ) ) $this->setArLockMonth( $arrValues['ar_lock_month'] );
		if( isset( $arrValues['ap_lock_month'] ) && $boolDirectSet ) $this->set( 'm_strApLockMonth', trim( $arrValues['ap_lock_month'] ) ); elseif( isset( $arrValues['ap_lock_month'] ) ) $this->setApLockMonth( $arrValues['ap_lock_month'] );
		if( isset( $arrValues['gl_lock_month'] ) && $boolDirectSet ) $this->set( 'm_strGlLockMonth', trim( $arrValues['gl_lock_month'] ) ); elseif( isset( $arrValues['gl_lock_month'] ) ) $this->setGlLockMonth( $arrValues['gl_lock_month'] );
		if( isset( $arrValues['ar_soft_lock_month'] ) && $boolDirectSet ) $this->set( 'm_strArSoftLockMonth', trim( $arrValues['ar_soft_lock_month'] ) ); elseif( isset( $arrValues['ar_soft_lock_month'] ) ) $this->setArSoftLockMonth( $arrValues['ar_soft_lock_month'] );
		if( isset( $arrValues['ap_soft_lock_month'] ) && $boolDirectSet ) $this->set( 'm_strApSoftLockMonth', trim( $arrValues['ap_soft_lock_month'] ) ); elseif( isset( $arrValues['ap_soft_lock_month'] ) ) $this->setApSoftLockMonth( $arrValues['ap_soft_lock_month'] );
		if( isset( $arrValues['gl_soft_lock_month'] ) && $boolDirectSet ) $this->set( 'm_strGlSoftLockMonth', trim( $arrValues['gl_soft_lock_month'] ) ); elseif( isset( $arrValues['gl_soft_lock_month'] ) ) $this->setGlSoftLockMonth( $arrValues['gl_soft_lock_month'] );
		if( isset( $arrValues['opening_period'] ) && $boolDirectSet ) $this->set( 'm_strOpeningPeriod', trim( $arrValues['opening_period'] ) ); elseif( isset( $arrValues['opening_period'] ) ) $this->setOpeningPeriod( $arrValues['opening_period'] );
		if( isset( $arrValues['activation_date'] ) && $boolDirectSet ) $this->set( 'm_strActivationDate', trim( $arrValues['activation_date'] ) ); elseif( isset( $arrValues['activation_date'] ) ) $this->setActivationDate( $arrValues['activation_date'] );
		if( isset( $arrValues['termination_date'] ) && $boolDirectSet ) $this->set( 'm_strTerminationDate', trim( $arrValues['termination_date'] ) ); elseif( isset( $arrValues['termination_date'] ) ) $this->setTerminationDate( $arrValues['termination_date'] );
		if( isset( $arrValues['ar_auto_advance_post_month'] ) && $boolDirectSet ) $this->set( 'm_intArAutoAdvancePostMonth', trim( $arrValues['ar_auto_advance_post_month'] ) ); elseif( isset( $arrValues['ar_auto_advance_post_month'] ) ) $this->setArAutoAdvancePostMonth( $arrValues['ar_auto_advance_post_month'] );
		if( isset( $arrValues['ap_auto_advance_post_month'] ) && $boolDirectSet ) $this->set( 'm_intApAutoAdvancePostMonth', trim( $arrValues['ap_auto_advance_post_month'] ) ); elseif( isset( $arrValues['ap_auto_advance_post_month'] ) ) $this->setApAutoAdvancePostMonth( $arrValues['ap_auto_advance_post_month'] );
		if( isset( $arrValues['gl_auto_advance_post_month'] ) && $boolDirectSet ) $this->set( 'm_intGlAutoAdvancePostMonth', trim( $arrValues['gl_auto_advance_post_month'] ) ); elseif( isset( $arrValues['gl_auto_advance_post_month'] ) ) $this->setGlAutoAdvancePostMonth( $arrValues['gl_auto_advance_post_month'] );
		if( isset( $arrValues['is_auto_lock_ar'] ) && $boolDirectSet ) $this->set( 'm_boolIsAutoLockAr', trim( stripcslashes( $arrValues['is_auto_lock_ar'] ) ) ); elseif( isset( $arrValues['is_auto_lock_ar'] ) ) $this->setIsAutoLockAr( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_auto_lock_ar'] ) : $arrValues['is_auto_lock_ar'] );
		if( isset( $arrValues['is_auto_lock_ap'] ) && $boolDirectSet ) $this->set( 'm_boolIsAutoLockAp', trim( stripcslashes( $arrValues['is_auto_lock_ap'] ) ) ); elseif( isset( $arrValues['is_auto_lock_ap'] ) ) $this->setIsAutoLockAp( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_auto_lock_ap'] ) : $arrValues['is_auto_lock_ap'] );
		if( isset( $arrValues['is_auto_lock_gl'] ) && $boolDirectSet ) $this->set( 'm_boolIsAutoLockGl', trim( stripcslashes( $arrValues['is_auto_lock_gl'] ) ) ); elseif( isset( $arrValues['is_auto_lock_gl'] ) ) $this->setIsAutoLockGl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_auto_lock_gl'] ) : $arrValues['is_auto_lock_gl'] );
		if( isset( $arrValues['ignore_holidays_and_weekends'] ) && $boolDirectSet ) $this->set( 'm_intIgnoreHolidaysAndWeekends', trim( $arrValues['ignore_holidays_and_weekends'] ) ); elseif( isset( $arrValues['ignore_holidays_and_weekends'] ) ) $this->setIgnoreHolidaysAndWeekends( $arrValues['ignore_holidays_and_weekends'] );
		if( isset( $arrValues['auto_post_gpr_adjustment'] ) && $boolDirectSet ) $this->set( 'm_intAutoPostGprAdjustment', trim( $arrValues['auto_post_gpr_adjustment'] ) ); elseif( isset( $arrValues['auto_post_gpr_adjustment'] ) ) $this->setAutoPostGprAdjustment( $arrValues['auto_post_gpr_adjustment'] );
		if( isset( $arrValues['gpr_book_basis'] ) && $boolDirectSet ) $this->set( 'm_strGprBookBasis', trim( $arrValues['gpr_book_basis'] ) ); elseif( isset( $arrValues['gpr_book_basis'] ) ) $this->setGprBookBasis( $arrValues['gpr_book_basis'] );
		if( isset( $arrValues['is_modified_accrual_gpr'] ) && $boolDirectSet ) $this->set( 'm_intIsModifiedAccrualGpr', trim( $arrValues['is_modified_accrual_gpr'] ) ); elseif( isset( $arrValues['is_modified_accrual_gpr'] ) ) $this->setIsModifiedAccrualGpr( $arrValues['is_modified_accrual_gpr'] );
		if( isset( $arrValues['use_market_rent_in_gpr'] ) && $boolDirectSet ) $this->set( 'm_intUseMarketRentInGpr', trim( $arrValues['use_market_rent_in_gpr'] ) ); elseif( isset( $arrValues['use_market_rent_in_gpr'] ) ) $this->setUseMarketRentInGpr( $arrValues['use_market_rent_in_gpr'] );
		if( isset( $arrValues['use_begin_of_month_market_rent'] ) && $boolDirectSet ) $this->set( 'm_intUseBeginOfMonthMarketRent', trim( $arrValues['use_begin_of_month_market_rent'] ) ); elseif( isset( $arrValues['use_begin_of_month_market_rent'] ) ) $this->setUseBeginOfMonthMarketRent( $arrValues['use_begin_of_month_market_rent'] );
		if( isset( $arrValues['group_excluded_units_in_gpr'] ) && $boolDirectSet ) $this->set( 'm_intGroupExcludedUnitsInGpr', trim( $arrValues['group_excluded_units_in_gpr'] ) ); elseif( isset( $arrValues['group_excluded_units_in_gpr'] ) ) $this->setGroupExcludedUnitsInGpr( $arrValues['group_excluded_units_in_gpr'] );
		if( isset( $arrValues['calculate_vacancy_on_ar_close'] ) && $boolDirectSet ) $this->set( 'm_intCalculateVacancyOnArClose', trim( $arrValues['calculate_vacancy_on_ar_close'] ) ); elseif( isset( $arrValues['calculate_vacancy_on_ar_close'] ) ) $this->setCalculateVacancyOnArClose( $arrValues['calculate_vacancy_on_ar_close'] );
		if( isset( $arrValues['prorate_gpr_vacancy_for_transfers'] ) && $boolDirectSet ) $this->set( 'm_boolProrateGprVacancyForTransfers', trim( stripcslashes( $arrValues['prorate_gpr_vacancy_for_transfers'] ) ) ); elseif( isset( $arrValues['prorate_gpr_vacancy_for_transfers'] ) ) $this->setProrateGprVacancyForTransfers( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['prorate_gpr_vacancy_for_transfers'] ) : $arrValues['prorate_gpr_vacancy_for_transfers'] );
		if( isset( $arrValues['deposit_payments_in_transit'] ) && $boolDirectSet ) $this->set( 'm_intDepositPaymentsInTransit', trim( $arrValues['deposit_payments_in_transit'] ) ); elseif( isset( $arrValues['deposit_payments_in_transit'] ) ) $this->setDepositPaymentsInTransit( $arrValues['deposit_payments_in_transit'] );
		if( isset( $arrValues['post_pos_to_gl_during_month_end'] ) && $boolDirectSet ) $this->set( 'm_boolPostPosToGlDuringMonthEnd', trim( stripcslashes( $arrValues['post_pos_to_gl_during_month_end'] ) ) ); elseif( isset( $arrValues['post_pos_to_gl_during_month_end'] ) ) $this->setPostPosToGlDuringMonthEnd( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['post_pos_to_gl_during_month_end'] ) : $arrValues['post_pos_to_gl_during_month_end'] );
		if( isset( $arrValues['auto_post_refund_invoice'] ) && $boolDirectSet ) $this->set( 'm_intAutoPostRefundInvoice', trim( $arrValues['auto_post_refund_invoice'] ) ); elseif( isset( $arrValues['auto_post_refund_invoice'] ) ) $this->setAutoPostRefundInvoice( $arrValues['auto_post_refund_invoice'] );
		if( isset( $arrValues['allocate_rent_first'] ) && $boolDirectSet ) $this->set( 'm_intAllocateRentFirst', trim( $arrValues['allocate_rent_first'] ) ); elseif( isset( $arrValues['allocate_rent_first'] ) ) $this->setAllocateRentFirst( $arrValues['allocate_rent_first'] );
		if( isset( $arrValues['posting_and_caching_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolPostingAndCachingDisabled', trim( stripcslashes( $arrValues['posting_and_caching_disabled'] ) ) ); elseif( isset( $arrValues['posting_and_caching_disabled'] ) ) $this->setPostingAndCachingDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['posting_and_caching_disabled'] ) : $arrValues['posting_and_caching_disabled'] );
		if( isset( $arrValues['use_custom_ar_codes'] ) && $boolDirectSet ) $this->set( 'm_intUseCustomArCodes', trim( $arrValues['use_custom_ar_codes'] ) ); elseif( isset( $arrValues['use_custom_ar_codes'] ) ) $this->setUseCustomArCodes( $arrValues['use_custom_ar_codes'] );
		if( isset( $arrValues['require_pos_for_receiving'] ) && $boolDirectSet ) $this->set( 'm_boolRequirePosForReceiving', trim( stripcslashes( $arrValues['require_pos_for_receiving'] ) ) ); elseif( isset( $arrValues['require_pos_for_receiving'] ) ) $this->setRequirePosForReceiving( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_pos_for_receiving'] ) : $arrValues['require_pos_for_receiving'] );
		if( isset( $arrValues['require_pos_for_jobs'] ) && $boolDirectSet ) $this->set( 'm_boolRequirePosForJobs', trim( stripcslashes( $arrValues['require_pos_for_jobs'] ) ) ); elseif( isset( $arrValues['require_pos_for_jobs'] ) ) $this->setRequirePosForJobs( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_pos_for_jobs'] ) : $arrValues['require_pos_for_jobs'] );
		if( isset( $arrValues['require_pos_for_manual_invoices'] ) && $boolDirectSet ) $this->set( 'm_boolRequirePosForManualInvoices', trim( stripcslashes( $arrValues['require_pos_for_manual_invoices'] ) ) ); elseif( isset( $arrValues['require_pos_for_manual_invoices'] ) ) $this->setRequirePosForManualInvoices( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_pos_for_manual_invoices'] ) : $arrValues['require_pos_for_manual_invoices'] );
		if( isset( $arrValues['require_vendor_compliance'] ) && $boolDirectSet ) $this->set( 'm_boolRequireVendorCompliance', trim( stripcslashes( $arrValues['require_vendor_compliance'] ) ) ); elseif( isset( $arrValues['require_vendor_compliance'] ) ) $this->setRequireVendorCompliance( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_vendor_compliance'] ) : $arrValues['require_vendor_compliance'] );
		if( isset( $arrValues['activate_standard_posting'] ) && $boolDirectSet ) $this->set( 'm_boolActivateStandardPosting', trim( stripcslashes( $arrValues['activate_standard_posting'] ) ) ); elseif( isset( $arrValues['activate_standard_posting'] ) ) $this->setActivateStandardPosting( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['activate_standard_posting'] ) : $arrValues['activate_standard_posting'] );
		if( isset( $arrValues['activate_po_posting'] ) && $boolDirectSet ) $this->set( 'm_boolActivatePoPosting', trim( stripcslashes( $arrValues['activate_po_posting'] ) ) ); elseif( isset( $arrValues['activate_po_posting'] ) ) $this->setActivatePoPosting( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['activate_po_posting'] ) : $arrValues['activate_po_posting'] );
		if( isset( $arrValues['activate_asset_posting'] ) && $boolDirectSet ) $this->set( 'm_boolActivateAssetPosting', trim( stripcslashes( $arrValues['activate_asset_posting'] ) ) ); elseif( isset( $arrValues['activate_asset_posting'] ) ) $this->setActivateAssetPosting( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['activate_asset_posting'] ) : $arrValues['activate_asset_posting'] );
		if( isset( $arrValues['is_ar_migration_mode'] ) && $boolDirectSet ) $this->set( 'm_boolIsArMigrationMode', trim( stripcslashes( $arrValues['is_ar_migration_mode'] ) ) ); elseif( isset( $arrValues['is_ar_migration_mode'] ) ) $this->setIsArMigrationMode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_ar_migration_mode'] ) : $arrValues['is_ar_migration_mode'] );
		if( isset( $arrValues['is_ap_migration_mode'] ) && $boolDirectSet ) $this->set( 'm_boolIsApMigrationMode', trim( stripcslashes( $arrValues['is_ap_migration_mode'] ) ) ); elseif( isset( $arrValues['is_ap_migration_mode'] ) ) $this->setIsApMigrationMode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_ap_migration_mode'] ) : $arrValues['is_ap_migration_mode'] );
		if( isset( $arrValues['is_asset_migration_mode'] ) && $boolDirectSet ) $this->set( 'm_boolIsAssetMigrationMode', trim( stripcslashes( $arrValues['is_asset_migration_mode'] ) ) ); elseif( isset( $arrValues['is_asset_migration_mode'] ) ) $this->setIsAssetMigrationMode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_asset_migration_mode'] ) : $arrValues['is_asset_migration_mode'] );
		if( isset( $arrValues['auto_update_budgeted_rent'] ) && $boolDirectSet ) $this->set( 'm_boolAutoUpdateBudgetedRent', trim( stripcslashes( $arrValues['auto_update_budgeted_rent'] ) ) ); elseif( isset( $arrValues['auto_update_budgeted_rent'] ) ) $this->setAutoUpdateBudgetedRent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['auto_update_budgeted_rent'] ) : $arrValues['auto_update_budgeted_rent'] );
		if( isset( $arrValues['user_validation_mode'] ) && $boolDirectSet ) $this->set( 'm_intUserValidationMode', trim( $arrValues['user_validation_mode'] ) ); elseif( isset( $arrValues['user_validation_mode'] ) ) $this->setUserValidationMode( $arrValues['user_validation_mode'] );
		if( isset( $arrValues['script_validation_mode'] ) && $boolDirectSet ) $this->set( 'm_intScriptValidationMode', trim( $arrValues['script_validation_mode'] ) ); elseif( isset( $arrValues['script_validation_mode'] ) ) $this->setScriptValidationMode( $arrValues['script_validation_mode'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['military_mac_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intMilitaryMacBankAccountId', trim( $arrValues['military_mac_bank_account_id'] ) ); elseif( isset( $arrValues['military_mac_bank_account_id'] ) ) $this->setMilitaryMacBankAccountId( $arrValues['military_mac_bank_account_id'] );
		if( isset( $arrValues['job_costing_ap_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intJobCostingApBankAccountId', trim( $arrValues['job_costing_ap_bank_account_id'] ) ); elseif( isset( $arrValues['job_costing_ap_bank_account_id'] ) ) $this->setJobCostingApBankAccountId( $arrValues['job_costing_ap_bank_account_id'] );
		if( isset( $arrValues['cc_ap_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCcApBankAccountId', trim( $arrValues['cc_ap_bank_account_id'] ) ); elseif( isset( $arrValues['cc_ap_bank_account_id'] ) ) $this->setCcApBankAccountId( $arrValues['cc_ap_bank_account_id'] );
		if( isset( $arrValues['straight_line_adjustment_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intStraightLineAdjustmentArCodeId', trim( $arrValues['straight_line_adjustment_ar_code_id'] ) ); elseif( isset( $arrValues['straight_line_adjustment_ar_code_id'] ) ) $this->setStraightLineAdjustmentArCodeId( $arrValues['straight_line_adjustment_ar_code_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setGlTreeId( $intGlTreeId ) {
		$this->set( 'm_intGlTreeId', CStrings::strToIntDef( $intGlTreeId, NULL, false ) );
	}

	public function getGlTreeId() {
		return $this->m_intGlTreeId;
	}

	public function sqlGlTreeId() {
		return ( true == isset( $this->m_intGlTreeId ) ) ? ( string ) $this->m_intGlTreeId : 'NULL';
	}

	public function setRentBankAccountId( $intRentBankAccountId ) {
		$this->set( 'm_intRentBankAccountId', CStrings::strToIntDef( $intRentBankAccountId, NULL, false ) );
	}

	public function getRentBankAccountId() {
		return $this->m_intRentBankAccountId;
	}

	public function sqlRentBankAccountId() {
		return ( true == isset( $this->m_intRentBankAccountId ) ) ? ( string ) $this->m_intRentBankAccountId : 'NULL';
	}

	public function setDepositBankAccountId( $intDepositBankAccountId ) {
		$this->set( 'm_intDepositBankAccountId', CStrings::strToIntDef( $intDepositBankAccountId, NULL, false ) );
	}

	public function getDepositBankAccountId() {
		return $this->m_intDepositBankAccountId;
	}

	public function sqlDepositBankAccountId() {
		return ( true == isset( $this->m_intDepositBankAccountId ) ) ? ( string ) $this->m_intDepositBankAccountId : 'NULL';
	}

	public function setRefundBankAccountId( $intRefundBankAccountId ) {
		$this->set( 'm_intRefundBankAccountId', CStrings::strToIntDef( $intRefundBankAccountId, NULL, false ) );
	}

	public function getRefundBankAccountId() {
		return $this->m_intRefundBankAccountId;
	}

	public function sqlRefundBankAccountId() {
		return ( true == isset( $this->m_intRefundBankAccountId ) ) ? ( string ) $this->m_intRefundBankAccountId : 'NULL';
	}

	public function setApBankAccountId( $intApBankAccountId ) {
		$this->set( 'm_intApBankAccountId', CStrings::strToIntDef( $intApBankAccountId, NULL, false ) );
	}

	public function getApBankAccountId() {
		return $this->m_intApBankAccountId;
	}

	public function sqlApBankAccountId() {
		return ( true == isset( $this->m_intApBankAccountId ) ) ? ( string ) $this->m_intApBankAccountId : 'NULL';
	}

	public function setApGlAccountId( $intApGlAccountId ) {
		$this->set( 'm_intApGlAccountId', CStrings::strToIntDef( $intApGlAccountId, NULL, false ) );
	}

	public function getApGlAccountId() {
		return $this->m_intApGlAccountId;
	}

	public function sqlApGlAccountId() {
		return ( true == isset( $this->m_intApGlAccountId ) ) ? ( string ) $this->m_intApGlAccountId : 'NULL';
	}

	public function setInterCoReimbursementBankAccountId( $intInterCoReimbursementBankAccountId ) {
		$this->set( 'm_intInterCoReimbursementBankAccountId', CStrings::strToIntDef( $intInterCoReimbursementBankAccountId, NULL, false ) );
	}

	public function getInterCoReimbursementBankAccountId() {
		return $this->m_intInterCoReimbursementBankAccountId;
	}

	public function sqlInterCoReimbursementBankAccountId() {
		return ( true == isset( $this->m_intInterCoReimbursementBankAccountId ) ) ? ( string ) $this->m_intInterCoReimbursementBankAccountId : 'NULL';
	}

	public function setMarketRentGlAccountId( $intMarketRentGlAccountId ) {
		$this->set( 'm_intMarketRentGlAccountId', CStrings::strToIntDef( $intMarketRentGlAccountId, NULL, false ) );
	}

	public function getMarketRentGlAccountId() {
		return $this->m_intMarketRentGlAccountId;
	}

	public function sqlMarketRentGlAccountId() {
		return ( true == isset( $this->m_intMarketRentGlAccountId ) ) ? ( string ) $this->m_intMarketRentGlAccountId : 'NULL';
	}

	public function setGainToLeaseGlAccountId( $intGainToLeaseGlAccountId ) {
		$this->set( 'm_intGainToLeaseGlAccountId', CStrings::strToIntDef( $intGainToLeaseGlAccountId, NULL, false ) );
	}

	public function getGainToLeaseGlAccountId() {
		return $this->m_intGainToLeaseGlAccountId;
	}

	public function sqlGainToLeaseGlAccountId() {
		return ( true == isset( $this->m_intGainToLeaseGlAccountId ) ) ? ( string ) $this->m_intGainToLeaseGlAccountId : 'NULL';
	}

	public function setLossToLeaseGlAccountId( $intLossToLeaseGlAccountId ) {
		$this->set( 'm_intLossToLeaseGlAccountId', CStrings::strToIntDef( $intLossToLeaseGlAccountId, NULL, false ) );
	}

	public function getLossToLeaseGlAccountId() {
		return $this->m_intLossToLeaseGlAccountId;
	}

	public function sqlLossToLeaseGlAccountId() {
		return ( true == isset( $this->m_intLossToLeaseGlAccountId ) ) ? ( string ) $this->m_intLossToLeaseGlAccountId : 'NULL';
	}

	public function setVacancyLossGlAccountId( $intVacancyLossGlAccountId ) {
		$this->set( 'm_intVacancyLossGlAccountId', CStrings::strToIntDef( $intVacancyLossGlAccountId, NULL, false ) );
	}

	public function getVacancyLossGlAccountId() {
		return $this->m_intVacancyLossGlAccountId;
	}

	public function sqlVacancyLossGlAccountId() {
		return ( true == isset( $this->m_intVacancyLossGlAccountId ) ) ? ( string ) $this->m_intVacancyLossGlAccountId : 'NULL';
	}

	public function setDelinquentRentGlAccountId( $intDelinquentRentGlAccountId ) {
		$this->set( 'm_intDelinquentRentGlAccountId', CStrings::strToIntDef( $intDelinquentRentGlAccountId, NULL, false ) );
	}

	public function getDelinquentRentGlAccountId() {
		return $this->m_intDelinquentRentGlAccountId;
	}

	public function sqlDelinquentRentGlAccountId() {
		return ( true == isset( $this->m_intDelinquentRentGlAccountId ) ) ? ( string ) $this->m_intDelinquentRentGlAccountId : 'NULL';
	}

	public function setPrepaidRentGlAccountId( $intPrepaidRentGlAccountId ) {
		$this->set( 'm_intPrepaidRentGlAccountId', CStrings::strToIntDef( $intPrepaidRentGlAccountId, NULL, false ) );
	}

	public function getPrepaidRentGlAccountId() {
		return $this->m_intPrepaidRentGlAccountId;
	}

	public function sqlPrepaidRentGlAccountId() {
		return ( true == isset( $this->m_intPrepaidRentGlAccountId ) ) ? ( string ) $this->m_intPrepaidRentGlAccountId : 'NULL';
	}

	public function setRetainedEarningsGlAccountId( $intRetainedEarningsGlAccountId ) {
		$this->set( 'm_intRetainedEarningsGlAccountId', CStrings::strToIntDef( $intRetainedEarningsGlAccountId, NULL, false ) );
	}

	public function getRetainedEarningsGlAccountId() {
		return $this->m_intRetainedEarningsGlAccountId;
	}

	public function sqlRetainedEarningsGlAccountId() {
		return ( true == isset( $this->m_intRetainedEarningsGlAccountId ) ) ? ( string ) $this->m_intRetainedEarningsGlAccountId : 'NULL';
	}

	public function setInterCoArGlAccountId( $intInterCoArGlAccountId ) {
		$this->set( 'm_intInterCoArGlAccountId', CStrings::strToIntDef( $intInterCoArGlAccountId, NULL, false ) );
	}

	public function getInterCoArGlAccountId() {
		return $this->m_intInterCoArGlAccountId;
	}

	public function sqlInterCoArGlAccountId() {
		return ( true == isset( $this->m_intInterCoArGlAccountId ) ) ? ( string ) $this->m_intInterCoArGlAccountId : 'NULL';
	}

	public function setInterCoApGlAccountId( $intInterCoApGlAccountId ) {
		$this->set( 'm_intInterCoApGlAccountId', CStrings::strToIntDef( $intInterCoApGlAccountId, NULL, false ) );
	}

	public function getInterCoApGlAccountId() {
		return $this->m_intInterCoApGlAccountId;
	}

	public function sqlInterCoApGlAccountId() {
		return ( true == isset( $this->m_intInterCoApGlAccountId ) ) ? ( string ) $this->m_intInterCoApGlAccountId : 'NULL';
	}

	public function setPoBalancesApGlAccountId( $intPoBalancesApGlAccountId ) {
		$this->set( 'm_intPoBalancesApGlAccountId', CStrings::strToIntDef( $intPoBalancesApGlAccountId, NULL, false ) );
	}

	public function getPoBalancesApGlAccountId() {
		return $this->m_intPoBalancesApGlAccountId;
	}

	public function sqlPoBalancesApGlAccountId() {
		return ( true == isset( $this->m_intPoBalancesApGlAccountId ) ) ? ( string ) $this->m_intPoBalancesApGlAccountId : 'NULL';
	}

	public function setPurchasesClearingGlAccountId( $intPurchasesClearingGlAccountId ) {
		$this->set( 'm_intPurchasesClearingGlAccountId', CStrings::strToIntDef( $intPurchasesClearingGlAccountId, NULL, false ) );
	}

	public function getPurchasesClearingGlAccountId() {
		return $this->m_intPurchasesClearingGlAccountId;
	}

	public function sqlPurchasesClearingGlAccountId() {
		return ( true == isset( $this->m_intPurchasesClearingGlAccountId ) ) ? ( string ) $this->m_intPurchasesClearingGlAccountId : 'NULL';
	}

	public function setPendingReimbursementsGlAccountId( $intPendingReimbursementsGlAccountId ) {
		$this->set( 'm_intPendingReimbursementsGlAccountId', CStrings::strToIntDef( $intPendingReimbursementsGlAccountId, NULL, false ) );
	}

	public function getPendingReimbursementsGlAccountId() {
		return $this->m_intPendingReimbursementsGlAccountId;
	}

	public function sqlPendingReimbursementsGlAccountId() {
		return ( true == isset( $this->m_intPendingReimbursementsGlAccountId ) ) ? ( string ) $this->m_intPendingReimbursementsGlAccountId : 'NULL';
	}

	public function setUnclaimedPropertyGlAccountId( $intUnclaimedPropertyGlAccountId ) {
		$this->set( 'm_intUnclaimedPropertyGlAccountId', CStrings::strToIntDef( $intUnclaimedPropertyGlAccountId, NULL, false ) );
	}

	public function getUnclaimedPropertyGlAccountId() {
		return $this->m_intUnclaimedPropertyGlAccountId;
	}

	public function sqlUnclaimedPropertyGlAccountId() {
		return ( true == isset( $this->m_intUnclaimedPropertyGlAccountId ) ) ? ( string ) $this->m_intUnclaimedPropertyGlAccountId : 'NULL';
	}

	public function setAccelRentArCodeId( $intAccelRentArCodeId ) {
		$this->set( 'm_intAccelRentArCodeId', CStrings::strToIntDef( $intAccelRentArCodeId, NULL, false ) );
	}

	public function getAccelRentArCodeId() {
		return $this->m_intAccelRentArCodeId;
	}

	public function sqlAccelRentArCodeId() {
		return ( true == isset( $this->m_intAccelRentArCodeId ) ) ? ( string ) $this->m_intAccelRentArCodeId : 'NULL';
	}

	public function setSkipAccelRentArCodeId( $intSkipAccelRentArCodeId ) {
		$this->set( 'm_intSkipAccelRentArCodeId', CStrings::strToIntDef( $intSkipAccelRentArCodeId, NULL, false ) );
	}

	public function getSkipAccelRentArCodeId() {
		return $this->m_intSkipAccelRentArCodeId;
	}

	public function sqlSkipAccelRentArCodeId() {
		return ( true == isset( $this->m_intSkipAccelRentArCodeId ) ) ? ( string ) $this->m_intSkipAccelRentArCodeId : 'NULL';
	}

	public function setRentArCodeId( $intRentArCodeId ) {
		$this->set( 'm_intRentArCodeId', CStrings::strToIntDef( $intRentArCodeId, NULL, false ) );
	}

	public function getRentArCodeId() {
		return $this->m_intRentArCodeId;
	}

	public function sqlRentArCodeId() {
		return ( true == isset( $this->m_intRentArCodeId ) ) ? ( string ) $this->m_intRentArCodeId : 'NULL';
	}

	public function setDepositArCodeId( $intDepositArCodeId ) {
		$this->set( 'm_intDepositArCodeId', CStrings::strToIntDef( $intDepositArCodeId, NULL, false ) );
	}

	public function getDepositArCodeId() {
		return $this->m_intDepositArCodeId;
	}

	public function sqlDepositArCodeId() {
		return ( true == isset( $this->m_intDepositArCodeId ) ) ? ( string ) $this->m_intDepositArCodeId : 'NULL';
	}

	public function setPercentOfSalesArCodeId( $intPercentOfSalesArCodeId ) {
		$this->set( 'm_intPercentOfSalesArCodeId', CStrings::strToIntDef( $intPercentOfSalesArCodeId, NULL, false ) );
	}

	public function getPercentOfSalesArCodeId() {
		return $this->m_intPercentOfSalesArCodeId;
	}

	public function sqlPercentOfSalesArCodeId() {
		return ( true == isset( $this->m_intPercentOfSalesArCodeId ) ) ? ( string ) $this->m_intPercentOfSalesArCodeId : 'NULL';
	}

	public function setMarketRentTypeId( $intMarketRentTypeId ) {
		$this->set( 'm_intMarketRentTypeId', CStrings::strToIntDef( $intMarketRentTypeId, NULL, false ) );
	}

	public function getMarketRentTypeId() {
		return $this->m_intMarketRentTypeId;
	}

	public function sqlMarketRentTypeId() {
		return ( true == isset( $this->m_intMarketRentTypeId ) ) ? ( string ) $this->m_intMarketRentTypeId : '1';
	}

	public function setLeaseExpirationStructureId( $intLeaseExpirationStructureId ) {
		$this->set( 'm_intLeaseExpirationStructureId', CStrings::strToIntDef( $intLeaseExpirationStructureId, NULL, false ) );
	}

	public function getLeaseExpirationStructureId() {
		return $this->m_intLeaseExpirationStructureId;
	}

	public function sqlLeaseExpirationStructureId() {
		return ( true == isset( $this->m_intLeaseExpirationStructureId ) ) ? ( string ) $this->m_intLeaseExpirationStructureId : 'NULL';
	}

	public function setDrawRequestApHeaderTypeId( $intDrawRequestApHeaderTypeId ) {
		$this->set( 'm_intDrawRequestApHeaderTypeId', CStrings::strToIntDef( $intDrawRequestApHeaderTypeId, NULL, false ) );
	}

	public function getDrawRequestApHeaderTypeId() {
		return $this->m_intDrawRequestApHeaderTypeId;
	}

	public function sqlDrawRequestApHeaderTypeId() {
		return ( true == isset( $this->m_intDrawRequestApHeaderTypeId ) ) ? ( string ) $this->m_intDrawRequestApHeaderTypeId : '1';
	}

	public function setUnclaimedPropertyApPayeeId( $intUnclaimedPropertyApPayeeId ) {
		$this->set( 'm_intUnclaimedPropertyApPayeeId', CStrings::strToIntDef( $intUnclaimedPropertyApPayeeId, NULL, false ) );
	}

	public function getUnclaimedPropertyApPayeeId() {
		return $this->m_intUnclaimedPropertyApPayeeId;
	}

	public function sqlUnclaimedPropertyApPayeeId() {
		return ( true == isset( $this->m_intUnclaimedPropertyApPayeeId ) ) ? ( string ) $this->m_intUnclaimedPropertyApPayeeId : 'NULL';
	}

	public function setApReceivingTypeId( $intApReceivingTypeId ) {
		$this->set( 'm_intApReceivingTypeId', CStrings::strToIntDef( $intApReceivingTypeId, NULL, false ) );
	}

	public function getApReceivingTypeId() {
		return $this->m_intApReceivingTypeId;
	}

	public function sqlApReceivingTypeId() {
		return ( true == isset( $this->m_intApReceivingTypeId ) ) ? ( string ) $this->m_intApReceivingTypeId : '1';
	}

	public function setIsCashBasis( $intIsCashBasis ) {
		$this->set( 'm_intIsCashBasis', CStrings::strToIntDef( $intIsCashBasis, NULL, false ) );
	}

	public function getIsCashBasis() {
		return $this->m_intIsCashBasis;
	}

	public function sqlIsCashBasis() {
		return ( true == isset( $this->m_intIsCashBasis ) ) ? ( string ) $this->m_intIsCashBasis : '0';
	}

	public function setFiscalYearEndMonth( $intFiscalYearEndMonth ) {
		$this->set( 'm_intFiscalYearEndMonth', CStrings::strToIntDef( $intFiscalYearEndMonth, NULL, false ) );
	}

	public function getFiscalYearEndMonth() {
		return $this->m_intFiscalYearEndMonth;
	}

	public function sqlFiscalYearEndMonth() {
		return ( true == isset( $this->m_intFiscalYearEndMonth ) ) ? ( string ) $this->m_intFiscalYearEndMonth : '12';
	}

	public function setArAdvanceDay( $intArAdvanceDay ) {
		$this->set( 'm_intArAdvanceDay', CStrings::strToIntDef( $intArAdvanceDay, NULL, false ) );
	}

	public function getArAdvanceDay() {
		return $this->m_intArAdvanceDay;
	}

	public function sqlArAdvanceDay() {
		return ( true == isset( $this->m_intArAdvanceDay ) ) ? ( string ) $this->m_intArAdvanceDay : '25';
	}

	public function setApAdvanceDay( $intApAdvanceDay ) {
		$this->set( 'm_intApAdvanceDay', CStrings::strToIntDef( $intApAdvanceDay, NULL, false ) );
	}

	public function getApAdvanceDay() {
		return $this->m_intApAdvanceDay;
	}

	public function sqlApAdvanceDay() {
		return ( true == isset( $this->m_intApAdvanceDay ) ) ? ( string ) $this->m_intApAdvanceDay : '25';
	}

	public function setGlAdvanceDay( $intGlAdvanceDay ) {
		$this->set( 'm_intGlAdvanceDay', CStrings::strToIntDef( $intGlAdvanceDay, NULL, false ) );
	}

	public function getGlAdvanceDay() {
		return $this->m_intGlAdvanceDay;
	}

	public function sqlGlAdvanceDay() {
		return ( true == isset( $this->m_intGlAdvanceDay ) ) ? ( string ) $this->m_intGlAdvanceDay : '25';
	}

	public function setAutoLockArOffsetDays( $intAutoLockArOffsetDays ) {
		$this->set( 'm_intAutoLockArOffsetDays', CStrings::strToIntDef( $intAutoLockArOffsetDays, NULL, false ) );
	}

	public function getAutoLockArOffsetDays() {
		return $this->m_intAutoLockArOffsetDays;
	}

	public function sqlAutoLockArOffsetDays() {
		return ( true == isset( $this->m_intAutoLockArOffsetDays ) ) ? ( string ) $this->m_intAutoLockArOffsetDays : '30';
	}

	public function setAutoLockApOffsetDays( $intAutoLockApOffsetDays ) {
		$this->set( 'm_intAutoLockApOffsetDays', CStrings::strToIntDef( $intAutoLockApOffsetDays, NULL, false ) );
	}

	public function getAutoLockApOffsetDays() {
		return $this->m_intAutoLockApOffsetDays;
	}

	public function sqlAutoLockApOffsetDays() {
		return ( true == isset( $this->m_intAutoLockApOffsetDays ) ) ? ( string ) $this->m_intAutoLockApOffsetDays : '30';
	}

	public function setAutoLockGlOffsetDays( $intAutoLockGlOffsetDays ) {
		$this->set( 'm_intAutoLockGlOffsetDays', CStrings::strToIntDef( $intAutoLockGlOffsetDays, NULL, false ) );
	}

	public function getAutoLockGlOffsetDays() {
		return $this->m_intAutoLockGlOffsetDays;
	}

	public function sqlAutoLockGlOffsetDays() {
		return ( true == isset( $this->m_intAutoLockGlOffsetDays ) ) ? ( string ) $this->m_intAutoLockGlOffsetDays : '30';
	}

	public function setArPostMonth( $strArPostMonth ) {
		$this->set( 'm_strArPostMonth', CStrings::strTrimDef( $strArPostMonth, -1, NULL, true ) );
	}

	public function getArPostMonth() {
		return $this->m_strArPostMonth;
	}

	public function sqlArPostMonth() {
		return ( true == isset( $this->m_strArPostMonth ) ) ? '\'' . $this->m_strArPostMonth . '\'' : 'NOW()';
	}

	public function setApPostMonth( $strApPostMonth ) {
		$this->set( 'm_strApPostMonth', CStrings::strTrimDef( $strApPostMonth, -1, NULL, true ) );
	}

	public function getApPostMonth() {
		return $this->m_strApPostMonth;
	}

	public function sqlApPostMonth() {
		return ( true == isset( $this->m_strApPostMonth ) ) ? '\'' . $this->m_strApPostMonth . '\'' : 'NOW()';
	}

	public function setGlPostMonth( $strGlPostMonth ) {
		$this->set( 'm_strGlPostMonth', CStrings::strTrimDef( $strGlPostMonth, -1, NULL, true ) );
	}

	public function getGlPostMonth() {
		return $this->m_strGlPostMonth;
	}

	public function sqlGlPostMonth() {
		return ( true == isset( $this->m_strGlPostMonth ) ) ? '\'' . $this->m_strGlPostMonth . '\'' : 'NOW()';
	}

	public function setArLockMonth( $strArLockMonth ) {
		$this->set( 'm_strArLockMonth', CStrings::strTrimDef( $strArLockMonth, -1, NULL, true ) );
	}

	public function getArLockMonth() {
		return $this->m_strArLockMonth;
	}

	public function sqlArLockMonth() {
		return ( true == isset( $this->m_strArLockMonth ) ) ? '\'' . $this->m_strArLockMonth . '\'' : 'NOW()';
	}

	public function setApLockMonth( $strApLockMonth ) {
		$this->set( 'm_strApLockMonth', CStrings::strTrimDef( $strApLockMonth, -1, NULL, true ) );
	}

	public function getApLockMonth() {
		return $this->m_strApLockMonth;
	}

	public function sqlApLockMonth() {
		return ( true == isset( $this->m_strApLockMonth ) ) ? '\'' . $this->m_strApLockMonth . '\'' : 'NOW()';
	}

	public function setGlLockMonth( $strGlLockMonth ) {
		$this->set( 'm_strGlLockMonth', CStrings::strTrimDef( $strGlLockMonth, -1, NULL, true ) );
	}

	public function getGlLockMonth() {
		return $this->m_strGlLockMonth;
	}

	public function sqlGlLockMonth() {
		return ( true == isset( $this->m_strGlLockMonth ) ) ? '\'' . $this->m_strGlLockMonth . '\'' : 'NOW()';
	}

	public function setArSoftLockMonth( $strArSoftLockMonth ) {
		$this->set( 'm_strArSoftLockMonth', CStrings::strTrimDef( $strArSoftLockMonth, -1, NULL, true ) );
	}

	public function getArSoftLockMonth() {
		return $this->m_strArSoftLockMonth;
	}

	public function sqlArSoftLockMonth() {
		return ( true == isset( $this->m_strArSoftLockMonth ) ) ? '\'' . $this->m_strArSoftLockMonth . '\'' : 'NOW()';
	}

	public function setApSoftLockMonth( $strApSoftLockMonth ) {
		$this->set( 'm_strApSoftLockMonth', CStrings::strTrimDef( $strApSoftLockMonth, -1, NULL, true ) );
	}

	public function getApSoftLockMonth() {
		return $this->m_strApSoftLockMonth;
	}

	public function sqlApSoftLockMonth() {
		return ( true == isset( $this->m_strApSoftLockMonth ) ) ? '\'' . $this->m_strApSoftLockMonth . '\'' : 'NOW()';
	}

	public function setGlSoftLockMonth( $strGlSoftLockMonth ) {
		$this->set( 'm_strGlSoftLockMonth', CStrings::strTrimDef( $strGlSoftLockMonth, -1, NULL, true ) );
	}

	public function getGlSoftLockMonth() {
		return $this->m_strGlSoftLockMonth;
	}

	public function sqlGlSoftLockMonth() {
		return ( true == isset( $this->m_strGlSoftLockMonth ) ) ? '\'' . $this->m_strGlSoftLockMonth . '\'' : 'NOW()';
	}

	public function setOpeningPeriod( $strOpeningPeriod ) {
		$this->set( 'm_strOpeningPeriod', CStrings::strTrimDef( $strOpeningPeriod, -1, NULL, true ) );
	}

	public function getOpeningPeriod() {
		return $this->m_strOpeningPeriod;
	}

	public function sqlOpeningPeriod() {
		return ( true == isset( $this->m_strOpeningPeriod ) ) ? '\'' . $this->m_strOpeningPeriod . '\'' : 'NULL';
	}

	public function setActivationDate( $strActivationDate ) {
		$this->set( 'm_strActivationDate', CStrings::strTrimDef( $strActivationDate, -1, NULL, true ) );
	}

	public function getActivationDate() {
		return $this->m_strActivationDate;
	}

	public function sqlActivationDate() {
		return ( true == isset( $this->m_strActivationDate ) ) ? '\'' . $this->m_strActivationDate . '\'' : 'NULL';
	}

	public function setTerminationDate( $strTerminationDate ) {
		$this->set( 'm_strTerminationDate', CStrings::strTrimDef( $strTerminationDate, -1, NULL, true ) );
	}

	public function getTerminationDate() {
		return $this->m_strTerminationDate;
	}

	public function sqlTerminationDate() {
		return ( true == isset( $this->m_strTerminationDate ) ) ? '\'' . $this->m_strTerminationDate . '\'' : 'NULL';
	}

	public function setArAutoAdvancePostMonth( $intArAutoAdvancePostMonth ) {
		$this->set( 'm_intArAutoAdvancePostMonth', CStrings::strToIntDef( $intArAutoAdvancePostMonth, NULL, false ) );
	}

	public function getArAutoAdvancePostMonth() {
		return $this->m_intArAutoAdvancePostMonth;
	}

	public function sqlArAutoAdvancePostMonth() {
		return ( true == isset( $this->m_intArAutoAdvancePostMonth ) ) ? ( string ) $this->m_intArAutoAdvancePostMonth : '0';
	}

	public function setApAutoAdvancePostMonth( $intApAutoAdvancePostMonth ) {
		$this->set( 'm_intApAutoAdvancePostMonth', CStrings::strToIntDef( $intApAutoAdvancePostMonth, NULL, false ) );
	}

	public function getApAutoAdvancePostMonth() {
		return $this->m_intApAutoAdvancePostMonth;
	}

	public function sqlApAutoAdvancePostMonth() {
		return ( true == isset( $this->m_intApAutoAdvancePostMonth ) ) ? ( string ) $this->m_intApAutoAdvancePostMonth : '0';
	}

	public function setGlAutoAdvancePostMonth( $intGlAutoAdvancePostMonth ) {
		$this->set( 'm_intGlAutoAdvancePostMonth', CStrings::strToIntDef( $intGlAutoAdvancePostMonth, NULL, false ) );
	}

	public function getGlAutoAdvancePostMonth() {
		return $this->m_intGlAutoAdvancePostMonth;
	}

	public function sqlGlAutoAdvancePostMonth() {
		return ( true == isset( $this->m_intGlAutoAdvancePostMonth ) ) ? ( string ) $this->m_intGlAutoAdvancePostMonth : '0';
	}

	public function setIsAutoLockAr( $boolIsAutoLockAr ) {
		$this->set( 'm_boolIsAutoLockAr', CStrings::strToBool( $boolIsAutoLockAr ) );
	}

	public function getIsAutoLockAr() {
		return $this->m_boolIsAutoLockAr;
	}

	public function sqlIsAutoLockAr() {
		return ( true == isset( $this->m_boolIsAutoLockAr ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAutoLockAr ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsAutoLockAp( $boolIsAutoLockAp ) {
		$this->set( 'm_boolIsAutoLockAp', CStrings::strToBool( $boolIsAutoLockAp ) );
	}

	public function getIsAutoLockAp() {
		return $this->m_boolIsAutoLockAp;
	}

	public function sqlIsAutoLockAp() {
		return ( true == isset( $this->m_boolIsAutoLockAp ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAutoLockAp ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsAutoLockGl( $boolIsAutoLockGl ) {
		$this->set( 'm_boolIsAutoLockGl', CStrings::strToBool( $boolIsAutoLockGl ) );
	}

	public function getIsAutoLockGl() {
		return $this->m_boolIsAutoLockGl;
	}

	public function sqlIsAutoLockGl() {
		return ( true == isset( $this->m_boolIsAutoLockGl ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAutoLockGl ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIgnoreHolidaysAndWeekends( $intIgnoreHolidaysAndWeekends ) {
		$this->set( 'm_intIgnoreHolidaysAndWeekends', CStrings::strToIntDef( $intIgnoreHolidaysAndWeekends, NULL, false ) );
	}

	public function getIgnoreHolidaysAndWeekends() {
		return $this->m_intIgnoreHolidaysAndWeekends;
	}

	public function sqlIgnoreHolidaysAndWeekends() {
		return ( true == isset( $this->m_intIgnoreHolidaysAndWeekends ) ) ? ( string ) $this->m_intIgnoreHolidaysAndWeekends : '0';
	}

	public function setAutoPostGprAdjustment( $intAutoPostGprAdjustment ) {
		$this->set( 'm_intAutoPostGprAdjustment', CStrings::strToIntDef( $intAutoPostGprAdjustment, NULL, false ) );
	}

	public function getAutoPostGprAdjustment() {
		return $this->m_intAutoPostGprAdjustment;
	}

	public function sqlAutoPostGprAdjustment() {
		return ( true == isset( $this->m_intAutoPostGprAdjustment ) ) ? ( string ) $this->m_intAutoPostGprAdjustment : '1';
	}

	public function setGprBookBasis( $strGprBookBasis ) {
		$this->set( 'm_strGprBookBasis', CStrings::strTrimDef( $strGprBookBasis, 7, NULL, true ) );
	}

	public function getGprBookBasis() {
		return $this->m_strGprBookBasis;
	}

	public function sqlGprBookBasis() {
		return ( true == isset( $this->m_strGprBookBasis ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strGprBookBasis ) : '\'' . addslashes( $this->m_strGprBookBasis ) . '\'' ) : '\'accrual\'';
	}

	public function setIsModifiedAccrualGpr( $intIsModifiedAccrualGpr ) {
		$this->set( 'm_intIsModifiedAccrualGpr', CStrings::strToIntDef( $intIsModifiedAccrualGpr, NULL, false ) );
	}

	public function getIsModifiedAccrualGpr() {
		return $this->m_intIsModifiedAccrualGpr;
	}

	public function sqlIsModifiedAccrualGpr() {
		return ( true == isset( $this->m_intIsModifiedAccrualGpr ) ) ? ( string ) $this->m_intIsModifiedAccrualGpr : '0';
	}

	public function setUseMarketRentInGpr( $intUseMarketRentInGpr ) {
		$this->set( 'm_intUseMarketRentInGpr', CStrings::strToIntDef( $intUseMarketRentInGpr, NULL, false ) );
	}

	public function getUseMarketRentInGpr() {
		return $this->m_intUseMarketRentInGpr;
	}

	public function sqlUseMarketRentInGpr() {
		return ( true == isset( $this->m_intUseMarketRentInGpr ) ) ? ( string ) $this->m_intUseMarketRentInGpr : '0';
	}

	public function setUseBeginOfMonthMarketRent( $intUseBeginOfMonthMarketRent ) {
		$this->set( 'm_intUseBeginOfMonthMarketRent', CStrings::strToIntDef( $intUseBeginOfMonthMarketRent, NULL, false ) );
	}

	public function getUseBeginOfMonthMarketRent() {
		return $this->m_intUseBeginOfMonthMarketRent;
	}

	public function sqlUseBeginOfMonthMarketRent() {
		return ( true == isset( $this->m_intUseBeginOfMonthMarketRent ) ) ? ( string ) $this->m_intUseBeginOfMonthMarketRent : '1';
	}

	public function setGroupExcludedUnitsInGpr( $intGroupExcludedUnitsInGpr ) {
		$this->set( 'm_intGroupExcludedUnitsInGpr', CStrings::strToIntDef( $intGroupExcludedUnitsInGpr, NULL, false ) );
	}

	public function getGroupExcludedUnitsInGpr() {
		return $this->m_intGroupExcludedUnitsInGpr;
	}

	public function sqlGroupExcludedUnitsInGpr() {
		return ( true == isset( $this->m_intGroupExcludedUnitsInGpr ) ) ? ( string ) $this->m_intGroupExcludedUnitsInGpr : '1';
	}

	public function setCalculateVacancyOnArClose( $intCalculateVacancyOnArClose ) {
		$this->set( 'm_intCalculateVacancyOnArClose', CStrings::strToIntDef( $intCalculateVacancyOnArClose, NULL, false ) );
	}

	public function getCalculateVacancyOnArClose() {
		return $this->m_intCalculateVacancyOnArClose;
	}

	public function sqlCalculateVacancyOnArClose() {
		return ( true == isset( $this->m_intCalculateVacancyOnArClose ) ) ? ( string ) $this->m_intCalculateVacancyOnArClose : '1';
	}

	public function setProrateGprVacancyForTransfers( $boolProrateGprVacancyForTransfers ) {
		$this->set( 'm_boolProrateGprVacancyForTransfers', CStrings::strToBool( $boolProrateGprVacancyForTransfers ) );
	}

	public function getProrateGprVacancyForTransfers() {
		return $this->m_boolProrateGprVacancyForTransfers;
	}

	public function sqlProrateGprVacancyForTransfers() {
		return ( true == isset( $this->m_boolProrateGprVacancyForTransfers ) ) ? '\'' . ( true == ( bool ) $this->m_boolProrateGprVacancyForTransfers ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDepositPaymentsInTransit( $intDepositPaymentsInTransit ) {
		$this->set( 'm_intDepositPaymentsInTransit', CStrings::strToIntDef( $intDepositPaymentsInTransit, NULL, false ) );
	}

	public function getDepositPaymentsInTransit() {
		return $this->m_intDepositPaymentsInTransit;
	}

	public function sqlDepositPaymentsInTransit() {
		return ( true == isset( $this->m_intDepositPaymentsInTransit ) ) ? ( string ) $this->m_intDepositPaymentsInTransit : '0';
	}

	public function setPostPosToGlDuringMonthEnd( $boolPostPosToGlDuringMonthEnd ) {
		$this->set( 'm_boolPostPosToGlDuringMonthEnd', CStrings::strToBool( $boolPostPosToGlDuringMonthEnd ) );
	}

	public function getPostPosToGlDuringMonthEnd() {
		return $this->m_boolPostPosToGlDuringMonthEnd;
	}

	public function sqlPostPosToGlDuringMonthEnd() {
		return ( true == isset( $this->m_boolPostPosToGlDuringMonthEnd ) ) ? '\'' . ( true == ( bool ) $this->m_boolPostPosToGlDuringMonthEnd ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAutoPostRefundInvoice( $intAutoPostRefundInvoice ) {
		$this->set( 'm_intAutoPostRefundInvoice', CStrings::strToIntDef( $intAutoPostRefundInvoice, NULL, false ) );
	}

	public function getAutoPostRefundInvoice() {
		return $this->m_intAutoPostRefundInvoice;
	}

	public function sqlAutoPostRefundInvoice() {
		return ( true == isset( $this->m_intAutoPostRefundInvoice ) ) ? ( string ) $this->m_intAutoPostRefundInvoice : '0';
	}

	public function setAllocateRentFirst( $intAllocateRentFirst ) {
		$this->set( 'm_intAllocateRentFirst', CStrings::strToIntDef( $intAllocateRentFirst, NULL, false ) );
	}

	public function getAllocateRentFirst() {
		return $this->m_intAllocateRentFirst;
	}

	public function sqlAllocateRentFirst() {
		return ( true == isset( $this->m_intAllocateRentFirst ) ) ? ( string ) $this->m_intAllocateRentFirst : '0';
	}

	public function setPostingAndCachingDisabled( $boolPostingAndCachingDisabled ) {
		$this->set( 'm_boolPostingAndCachingDisabled', CStrings::strToBool( $boolPostingAndCachingDisabled ) );
	}

	public function getPostingAndCachingDisabled() {
		return $this->m_boolPostingAndCachingDisabled;
	}

	public function sqlPostingAndCachingDisabled() {
		return ( true == isset( $this->m_boolPostingAndCachingDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolPostingAndCachingDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUseCustomArCodes( $intUseCustomArCodes ) {
		$this->set( 'm_intUseCustomArCodes', CStrings::strToIntDef( $intUseCustomArCodes, NULL, false ) );
	}

	public function getUseCustomArCodes() {
		return $this->m_intUseCustomArCodes;
	}

	public function sqlUseCustomArCodes() {
		return ( true == isset( $this->m_intUseCustomArCodes ) ) ? ( string ) $this->m_intUseCustomArCodes : '0';
	}

	public function setRequirePosForReceiving( $boolRequirePosForReceiving ) {
		$this->set( 'm_boolRequirePosForReceiving', CStrings::strToBool( $boolRequirePosForReceiving ) );
	}

	public function getRequirePosForReceiving() {
		return $this->m_boolRequirePosForReceiving;
	}

	public function sqlRequirePosForReceiving() {
		return ( true == isset( $this->m_boolRequirePosForReceiving ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequirePosForReceiving ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequirePosForJobs( $boolRequirePosForJobs ) {
		$this->set( 'm_boolRequirePosForJobs', CStrings::strToBool( $boolRequirePosForJobs ) );
	}

	public function getRequirePosForJobs() {
		return $this->m_boolRequirePosForJobs;
	}

	public function sqlRequirePosForJobs() {
		return ( true == isset( $this->m_boolRequirePosForJobs ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequirePosForJobs ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequirePosForManualInvoices( $boolRequirePosForManualInvoices ) {
		$this->set( 'm_boolRequirePosForManualInvoices', CStrings::strToBool( $boolRequirePosForManualInvoices ) );
	}

	public function getRequirePosForManualInvoices() {
		return $this->m_boolRequirePosForManualInvoices;
	}

	public function sqlRequirePosForManualInvoices() {
		return ( true == isset( $this->m_boolRequirePosForManualInvoices ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequirePosForManualInvoices ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequireVendorCompliance( $boolRequireVendorCompliance ) {
		$this->set( 'm_boolRequireVendorCompliance', CStrings::strToBool( $boolRequireVendorCompliance ) );
	}

	public function getRequireVendorCompliance() {
		return $this->m_boolRequireVendorCompliance;
	}

	public function sqlRequireVendorCompliance() {
		return ( true == isset( $this->m_boolRequireVendorCompliance ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequireVendorCompliance ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setActivateStandardPosting( $boolActivateStandardPosting ) {
		$this->set( 'm_boolActivateStandardPosting', CStrings::strToBool( $boolActivateStandardPosting ) );
	}

	public function getActivateStandardPosting() {
		return $this->m_boolActivateStandardPosting;
	}

	public function sqlActivateStandardPosting() {
		return ( true == isset( $this->m_boolActivateStandardPosting ) ) ? '\'' . ( true == ( bool ) $this->m_boolActivateStandardPosting ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setActivatePoPosting( $boolActivatePoPosting ) {
		$this->set( 'm_boolActivatePoPosting', CStrings::strToBool( $boolActivatePoPosting ) );
	}

	public function getActivatePoPosting() {
		return $this->m_boolActivatePoPosting;
	}

	public function sqlActivatePoPosting() {
		return ( true == isset( $this->m_boolActivatePoPosting ) ) ? '\'' . ( true == ( bool ) $this->m_boolActivatePoPosting ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setActivateAssetPosting( $boolActivateAssetPosting ) {
		$this->set( 'm_boolActivateAssetPosting', CStrings::strToBool( $boolActivateAssetPosting ) );
	}

	public function getActivateAssetPosting() {
		return $this->m_boolActivateAssetPosting;
	}

	public function sqlActivateAssetPosting() {
		return ( true == isset( $this->m_boolActivateAssetPosting ) ) ? '\'' . ( true == ( bool ) $this->m_boolActivateAssetPosting ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsArMigrationMode( $boolIsArMigrationMode ) {
		$this->set( 'm_boolIsArMigrationMode', CStrings::strToBool( $boolIsArMigrationMode ) );
	}

	public function getIsArMigrationMode() {
		return $this->m_boolIsArMigrationMode;
	}

	public function sqlIsArMigrationMode() {
		return ( true == isset( $this->m_boolIsArMigrationMode ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsArMigrationMode ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsApMigrationMode( $boolIsApMigrationMode ) {
		$this->set( 'm_boolIsApMigrationMode', CStrings::strToBool( $boolIsApMigrationMode ) );
	}

	public function getIsApMigrationMode() {
		return $this->m_boolIsApMigrationMode;
	}

	public function sqlIsApMigrationMode() {
		return ( true == isset( $this->m_boolIsApMigrationMode ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsApMigrationMode ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsAssetMigrationMode( $boolIsAssetMigrationMode ) {
		$this->set( 'm_boolIsAssetMigrationMode', CStrings::strToBool( $boolIsAssetMigrationMode ) );
	}

	public function getIsAssetMigrationMode() {
		return $this->m_boolIsAssetMigrationMode;
	}

	public function sqlIsAssetMigrationMode() {
		return ( true == isset( $this->m_boolIsAssetMigrationMode ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAssetMigrationMode ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAutoUpdateBudgetedRent( $boolAutoUpdateBudgetedRent ) {
		$this->set( 'm_boolAutoUpdateBudgetedRent', CStrings::strToBool( $boolAutoUpdateBudgetedRent ) );
	}

	public function getAutoUpdateBudgetedRent() {
		return $this->m_boolAutoUpdateBudgetedRent;
	}

	public function sqlAutoUpdateBudgetedRent() {
		return ( true == isset( $this->m_boolAutoUpdateBudgetedRent ) ) ? '\'' . ( true == ( bool ) $this->m_boolAutoUpdateBudgetedRent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUserValidationMode( $intUserValidationMode ) {
		$this->set( 'm_intUserValidationMode', CStrings::strToIntDef( $intUserValidationMode, NULL, false ) );
	}

	public function getUserValidationMode() {
		return $this->m_intUserValidationMode;
	}

	public function sqlUserValidationMode() {
		return ( true == isset( $this->m_intUserValidationMode ) ) ? ( string ) $this->m_intUserValidationMode : '0';
	}

	public function setScriptValidationMode( $intScriptValidationMode ) {
		$this->set( 'm_intScriptValidationMode', CStrings::strToIntDef( $intScriptValidationMode, NULL, false ) );
	}

	public function getScriptValidationMode() {
		return $this->m_intScriptValidationMode;
	}

	public function sqlScriptValidationMode() {
		return ( true == isset( $this->m_intScriptValidationMode ) ) ? ( string ) $this->m_intScriptValidationMode : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setMilitaryMacBankAccountId( $intMilitaryMacBankAccountId ) {
		$this->set( 'm_intMilitaryMacBankAccountId', CStrings::strToIntDef( $intMilitaryMacBankAccountId, NULL, false ) );
	}

	public function getMilitaryMacBankAccountId() {
		return $this->m_intMilitaryMacBankAccountId;
	}

	public function sqlMilitaryMacBankAccountId() {
		return ( true == isset( $this->m_intMilitaryMacBankAccountId ) ) ? ( string ) $this->m_intMilitaryMacBankAccountId : 'NULL';
	}

	public function setJobCostingApBankAccountId( $intJobCostingApBankAccountId ) {
		$this->set( 'm_intJobCostingApBankAccountId', CStrings::strToIntDef( $intJobCostingApBankAccountId, NULL, false ) );
	}

	public function getJobCostingApBankAccountId() {
		return $this->m_intJobCostingApBankAccountId;
	}

	public function sqlJobCostingApBankAccountId() {
		return ( true == isset( $this->m_intJobCostingApBankAccountId ) ) ? ( string ) $this->m_intJobCostingApBankAccountId : 'NULL';
	}

	public function setCcApBankAccountId( $intCcApBankAccountId ) {
		$this->set( 'm_intCcApBankAccountId', CStrings::strToIntDef( $intCcApBankAccountId, NULL, false ) );
	}

	public function getCcApBankAccountId() {
		return $this->m_intCcApBankAccountId;
	}

	public function sqlCcApBankAccountId() {
		return ( true == isset( $this->m_intCcApBankAccountId ) ) ? ( string ) $this->m_intCcApBankAccountId : 'NULL';
	}

	public function setStraightLineAdjustmentArCodeId( $intStraightLineAdjustmentArCodeId ) {
		$this->set( 'm_intStraightLineAdjustmentArCodeId', CStrings::strToIntDef( $intStraightLineAdjustmentArCodeId, NULL, false ) );
	}

	public function getStraightLineAdjustmentArCodeId() {
		return $this->m_intStraightLineAdjustmentArCodeId;
	}

	public function sqlStraightLineAdjustmentArCodeId() {
		return ( true == isset( $this->m_intStraightLineAdjustmentArCodeId ) ) ? ( string ) $this->m_intStraightLineAdjustmentArCodeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, gl_tree_id, rent_bank_account_id, deposit_bank_account_id, refund_bank_account_id, ap_bank_account_id, ap_gl_account_id, inter_co_reimbursement_bank_account_id, market_rent_gl_account_id, gain_to_lease_gl_account_id, loss_to_lease_gl_account_id, vacancy_loss_gl_account_id, delinquent_rent_gl_account_id, prepaid_rent_gl_account_id, retained_earnings_gl_account_id, inter_co_ar_gl_account_id, inter_co_ap_gl_account_id, po_balances_ap_gl_account_id, purchases_clearing_gl_account_id, pending_reimbursements_gl_account_id, unclaimed_property_gl_account_id, accel_rent_ar_code_id, skip_accel_rent_ar_code_id, rent_ar_code_id, deposit_ar_code_id, percent_of_sales_ar_code_id, market_rent_type_id, lease_expiration_structure_id, draw_request_ap_header_type_id, unclaimed_property_ap_payee_id, ap_receiving_type_id, is_cash_basis, fiscal_year_end_month, ar_advance_day, ap_advance_day, gl_advance_day, auto_lock_ar_offset_days, auto_lock_ap_offset_days, auto_lock_gl_offset_days, ar_post_month, ap_post_month, gl_post_month, ar_lock_month, ap_lock_month, gl_lock_month, ar_soft_lock_month, ap_soft_lock_month, gl_soft_lock_month, opening_period, activation_date, termination_date, ar_auto_advance_post_month, ap_auto_advance_post_month, gl_auto_advance_post_month, is_auto_lock_ar, is_auto_lock_ap, is_auto_lock_gl, ignore_holidays_and_weekends, auto_post_gpr_adjustment, gpr_book_basis, is_modified_accrual_gpr, use_market_rent_in_gpr, use_begin_of_month_market_rent, group_excluded_units_in_gpr, calculate_vacancy_on_ar_close, prorate_gpr_vacancy_for_transfers, deposit_payments_in_transit, post_pos_to_gl_during_month_end, auto_post_refund_invoice, allocate_rent_first, posting_and_caching_disabled, use_custom_ar_codes, require_pos_for_receiving, require_pos_for_jobs, require_pos_for_manual_invoices, require_vendor_compliance, activate_standard_posting, activate_po_posting, activate_asset_posting, is_ar_migration_mode, is_ap_migration_mode, is_asset_migration_mode, auto_update_budgeted_rent, user_validation_mode, script_validation_mode, updated_by, updated_on, created_by, created_on, military_mac_bank_account_id, job_costing_ap_bank_account_id, cc_ap_bank_account_id, straight_line_adjustment_ar_code_id )
					VALUES ( ' .
			$strId . ', ' .
			$this->sqlCid() . ', ' .
			$this->sqlPropertyId() . ', ' .
			$this->sqlGlTreeId() . ', ' .
			$this->sqlRentBankAccountId() . ', ' .
			$this->sqlDepositBankAccountId() . ', ' .
			$this->sqlRefundBankAccountId() . ', ' .
			$this->sqlApBankAccountId() . ', ' .
			$this->sqlApGlAccountId() . ', ' .
			$this->sqlInterCoReimbursementBankAccountId() . ', ' .
			$this->sqlMarketRentGlAccountId() . ', ' .
			$this->sqlGainToLeaseGlAccountId() . ', ' .
			$this->sqlLossToLeaseGlAccountId() . ', ' .
			$this->sqlVacancyLossGlAccountId() . ', ' .
			$this->sqlDelinquentRentGlAccountId() . ', ' .
			$this->sqlPrepaidRentGlAccountId() . ', ' .
			$this->sqlRetainedEarningsGlAccountId() . ', ' .
			$this->sqlInterCoArGlAccountId() . ', ' .
			$this->sqlInterCoApGlAccountId() . ', ' .
			$this->sqlPoBalancesApGlAccountId() . ', ' .
			$this->sqlPurchasesClearingGlAccountId() . ', ' .
			$this->sqlPendingReimbursementsGlAccountId() . ', ' .
			$this->sqlUnclaimedPropertyGlAccountId() . ', ' .
			$this->sqlAccelRentArCodeId() . ', ' .
			$this->sqlSkipAccelRentArCodeId() . ', ' .
			$this->sqlRentArCodeId() . ', ' .
			$this->sqlDepositArCodeId() . ', ' .
			$this->sqlPercentOfSalesArCodeId() . ', ' .
			$this->sqlMarketRentTypeId() . ', ' .
			$this->sqlLeaseExpirationStructureId() . ', ' .
			$this->sqlDrawRequestApHeaderTypeId() . ', ' .
			$this->sqlUnclaimedPropertyApPayeeId() . ', ' .
			$this->sqlApReceivingTypeId() . ', ' .
			$this->sqlIsCashBasis() . ', ' .
			$this->sqlFiscalYearEndMonth() . ', ' .
			$this->sqlArAdvanceDay() . ', ' .
			$this->sqlApAdvanceDay() . ', ' .
			$this->sqlGlAdvanceDay() . ', ' .
			$this->sqlAutoLockArOffsetDays() . ', ' .
			$this->sqlAutoLockApOffsetDays() . ', ' .
			$this->sqlAutoLockGlOffsetDays() . ', ' .
			$this->sqlArPostMonth() . ', ' .
			$this->sqlApPostMonth() . ', ' .
			$this->sqlGlPostMonth() . ', ' .
			$this->sqlArLockMonth() . ', ' .
			$this->sqlApLockMonth() . ', ' .
			$this->sqlGlLockMonth() . ', ' .
			$this->sqlArSoftLockMonth() . ', ' .
			$this->sqlApSoftLockMonth() . ', ' .
			$this->sqlGlSoftLockMonth() . ', ' .
			$this->sqlOpeningPeriod() . ', ' .
			$this->sqlActivationDate() . ', ' .
			$this->sqlTerminationDate() . ', ' .
			$this->sqlArAutoAdvancePostMonth() . ', ' .
			$this->sqlApAutoAdvancePostMonth() . ', ' .
			$this->sqlGlAutoAdvancePostMonth() . ', ' .
			$this->sqlIsAutoLockAr() . ', ' .
			$this->sqlIsAutoLockAp() . ', ' .
			$this->sqlIsAutoLockGl() . ', ' .
			$this->sqlIgnoreHolidaysAndWeekends() . ', ' .
			$this->sqlAutoPostGprAdjustment() . ', ' .
			$this->sqlGprBookBasis() . ', ' .
			$this->sqlIsModifiedAccrualGpr() . ', ' .
			$this->sqlUseMarketRentInGpr() . ', ' .
			$this->sqlUseBeginOfMonthMarketRent() . ', ' .
			$this->sqlGroupExcludedUnitsInGpr() . ', ' .
			$this->sqlCalculateVacancyOnArClose() . ', ' .
			$this->sqlProrateGprVacancyForTransfers() . ', ' .
			$this->sqlDepositPaymentsInTransit() . ', ' .
			$this->sqlPostPosToGlDuringMonthEnd() . ', ' .
			$this->sqlAutoPostRefundInvoice() . ', ' .
			$this->sqlAllocateRentFirst() . ', ' .
			$this->sqlPostingAndCachingDisabled() . ', ' .
			$this->sqlUseCustomArCodes() . ', ' .
			$this->sqlRequirePosForReceiving() . ', ' .
			$this->sqlRequirePosForJobs() . ', ' .
			$this->sqlRequirePosForManualInvoices() . ', ' .
			$this->sqlRequireVendorCompliance() . ', ' .
			$this->sqlActivateStandardPosting() . ', ' .
			$this->sqlActivatePoPosting() . ', ' .
			$this->sqlActivateAssetPosting() . ', ' .
			$this->sqlIsArMigrationMode() . ', ' .
			$this->sqlIsApMigrationMode() . ', ' .
			$this->sqlIsAssetMigrationMode() . ', ' .
			$this->sqlAutoUpdateBudgetedRent() . ', ' .
			$this->sqlUserValidationMode() . ', ' .
			$this->sqlScriptValidationMode() . ', ' .
			( int ) $intCurrentUserId . ', ' .
			$this->sqlUpdatedOn() . ', ' .
			( int ) $intCurrentUserId . ', ' .
			$this->sqlCreatedOn() . ', ' .
			$this->sqlMilitaryMacBankAccountId() . ', ' .
			$this->sqlJobCostingApBankAccountId() . ', ' .
			$this->sqlCcApBankAccountId() . ', ' .
			$this->sqlStraightLineAdjustmentArCodeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_tree_id = ' . $this->sqlGlTreeId(). ',' ; } elseif( true == array_key_exists( 'GlTreeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_tree_id = ' . $this->sqlGlTreeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rent_bank_account_id = ' . $this->sqlRentBankAccountId(). ',' ; } elseif( true == array_key_exists( 'RentBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' rent_bank_account_id = ' . $this->sqlRentBankAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit_bank_account_id = ' . $this->sqlDepositBankAccountId(). ',' ; } elseif( true == array_key_exists( 'DepositBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' deposit_bank_account_id = ' . $this->sqlDepositBankAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' refund_bank_account_id = ' . $this->sqlRefundBankAccountId(). ',' ; } elseif( true == array_key_exists( 'RefundBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' refund_bank_account_id = ' . $this->sqlRefundBankAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_bank_account_id = ' . $this->sqlApBankAccountId(). ',' ; } elseif( true == array_key_exists( 'ApBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' ap_bank_account_id = ' . $this->sqlApBankAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_gl_account_id = ' . $this->sqlApGlAccountId(). ',' ; } elseif( true == array_key_exists( 'ApGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' ap_gl_account_id = ' . $this->sqlApGlAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inter_co_reimbursement_bank_account_id = ' . $this->sqlInterCoReimbursementBankAccountId(). ',' ; } elseif( true == array_key_exists( 'InterCoReimbursementBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' inter_co_reimbursement_bank_account_id = ' . $this->sqlInterCoReimbursementBankAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' market_rent_gl_account_id = ' . $this->sqlMarketRentGlAccountId(). ',' ; } elseif( true == array_key_exists( 'MarketRentGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' market_rent_gl_account_id = ' . $this->sqlMarketRentGlAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gain_to_lease_gl_account_id = ' . $this->sqlGainToLeaseGlAccountId(). ',' ; } elseif( true == array_key_exists( 'GainToLeaseGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' gain_to_lease_gl_account_id = ' . $this->sqlGainToLeaseGlAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' loss_to_lease_gl_account_id = ' . $this->sqlLossToLeaseGlAccountId(). ',' ; } elseif( true == array_key_exists( 'LossToLeaseGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' loss_to_lease_gl_account_id = ' . $this->sqlLossToLeaseGlAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vacancy_loss_gl_account_id = ' . $this->sqlVacancyLossGlAccountId(). ',' ; } elseif( true == array_key_exists( 'VacancyLossGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' vacancy_loss_gl_account_id = ' . $this->sqlVacancyLossGlAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquent_rent_gl_account_id = ' . $this->sqlDelinquentRentGlAccountId(). ',' ; } elseif( true == array_key_exists( 'DelinquentRentGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' delinquent_rent_gl_account_id = ' . $this->sqlDelinquentRentGlAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prepaid_rent_gl_account_id = ' . $this->sqlPrepaidRentGlAccountId(). ',' ; } elseif( true == array_key_exists( 'PrepaidRentGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' prepaid_rent_gl_account_id = ' . $this->sqlPrepaidRentGlAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' retained_earnings_gl_account_id = ' . $this->sqlRetainedEarningsGlAccountId(). ',' ; } elseif( true == array_key_exists( 'RetainedEarningsGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' retained_earnings_gl_account_id = ' . $this->sqlRetainedEarningsGlAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inter_co_ar_gl_account_id = ' . $this->sqlInterCoArGlAccountId(). ',' ; } elseif( true == array_key_exists( 'InterCoArGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' inter_co_ar_gl_account_id = ' . $this->sqlInterCoArGlAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inter_co_ap_gl_account_id = ' . $this->sqlInterCoApGlAccountId(). ',' ; } elseif( true == array_key_exists( 'InterCoApGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' inter_co_ap_gl_account_id = ' . $this->sqlInterCoApGlAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' po_balances_ap_gl_account_id = ' . $this->sqlPoBalancesApGlAccountId(). ',' ; } elseif( true == array_key_exists( 'PoBalancesApGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' po_balances_ap_gl_account_id = ' . $this->sqlPoBalancesApGlAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchases_clearing_gl_account_id = ' . $this->sqlPurchasesClearingGlAccountId(). ',' ; } elseif( true == array_key_exists( 'PurchasesClearingGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' purchases_clearing_gl_account_id = ' . $this->sqlPurchasesClearingGlAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pending_reimbursements_gl_account_id = ' . $this->sqlPendingReimbursementsGlAccountId(). ',' ; } elseif( true == array_key_exists( 'PendingReimbursementsGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' pending_reimbursements_gl_account_id = ' . $this->sqlPendingReimbursementsGlAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unclaimed_property_gl_account_id = ' . $this->sqlUnclaimedPropertyGlAccountId(). ',' ; } elseif( true == array_key_exists( 'UnclaimedPropertyGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' unclaimed_property_gl_account_id = ' . $this->sqlUnclaimedPropertyGlAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accel_rent_ar_code_id = ' . $this->sqlAccelRentArCodeId(). ',' ; } elseif( true == array_key_exists( 'AccelRentArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' accel_rent_ar_code_id = ' . $this->sqlAccelRentArCodeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' skip_accel_rent_ar_code_id = ' . $this->sqlSkipAccelRentArCodeId(). ',' ; } elseif( true == array_key_exists( 'SkipAccelRentArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' skip_accel_rent_ar_code_id = ' . $this->sqlSkipAccelRentArCodeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rent_ar_code_id = ' . $this->sqlRentArCodeId(). ',' ; } elseif( true == array_key_exists( 'RentArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' rent_ar_code_id = ' . $this->sqlRentArCodeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit_ar_code_id = ' . $this->sqlDepositArCodeId(). ',' ; } elseif( true == array_key_exists( 'DepositArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' deposit_ar_code_id = ' . $this->sqlDepositArCodeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' percent_of_sales_ar_code_id = ' . $this->sqlPercentOfSalesArCodeId(). ',' ; } elseif( true == array_key_exists( 'PercentOfSalesArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' percent_of_sales_ar_code_id = ' . $this->sqlPercentOfSalesArCodeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' market_rent_type_id = ' . $this->sqlMarketRentTypeId(). ',' ; } elseif( true == array_key_exists( 'MarketRentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' market_rent_type_id = ' . $this->sqlMarketRentTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_expiration_structure_id = ' . $this->sqlLeaseExpirationStructureId(). ',' ; } elseif( true == array_key_exists( 'LeaseExpirationStructureId', $this->getChangedColumns() ) ) { $strSql .= ' lease_expiration_structure_id = ' . $this->sqlLeaseExpirationStructureId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' draw_request_ap_header_type_id = ' . $this->sqlDrawRequestApHeaderTypeId(). ',' ; } elseif( true == array_key_exists( 'DrawRequestApHeaderTypeId', $this->getChangedColumns() ) ) { $strSql .= ' draw_request_ap_header_type_id = ' . $this->sqlDrawRequestApHeaderTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unclaimed_property_ap_payee_id = ' . $this->sqlUnclaimedPropertyApPayeeId(). ',' ; } elseif( true == array_key_exists( 'UnclaimedPropertyApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' unclaimed_property_ap_payee_id = ' . $this->sqlUnclaimedPropertyApPayeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_receiving_type_id = ' . $this->sqlApReceivingTypeId(). ',' ; } elseif( true == array_key_exists( 'ApReceivingTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_receiving_type_id = ' . $this->sqlApReceivingTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_cash_basis = ' . $this->sqlIsCashBasis(). ',' ; } elseif( true == array_key_exists( 'IsCashBasis', $this->getChangedColumns() ) ) { $strSql .= ' is_cash_basis = ' . $this->sqlIsCashBasis() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fiscal_year_end_month = ' . $this->sqlFiscalYearEndMonth(). ',' ; } elseif( true == array_key_exists( 'FiscalYearEndMonth', $this->getChangedColumns() ) ) { $strSql .= ' fiscal_year_end_month = ' . $this->sqlFiscalYearEndMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_advance_day = ' . $this->sqlArAdvanceDay(). ',' ; } elseif( true == array_key_exists( 'ArAdvanceDay', $this->getChangedColumns() ) ) { $strSql .= ' ar_advance_day = ' . $this->sqlArAdvanceDay() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_advance_day = ' . $this->sqlApAdvanceDay(). ',' ; } elseif( true == array_key_exists( 'ApAdvanceDay', $this->getChangedColumns() ) ) { $strSql .= ' ap_advance_day = ' . $this->sqlApAdvanceDay() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_advance_day = ' . $this->sqlGlAdvanceDay(). ',' ; } elseif( true == array_key_exists( 'GlAdvanceDay', $this->getChangedColumns() ) ) { $strSql .= ' gl_advance_day = ' . $this->sqlGlAdvanceDay() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_lock_ar_offset_days = ' . $this->sqlAutoLockArOffsetDays(). ',' ; } elseif( true == array_key_exists( 'AutoLockArOffsetDays', $this->getChangedColumns() ) ) { $strSql .= ' auto_lock_ar_offset_days = ' . $this->sqlAutoLockArOffsetDays() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_lock_ap_offset_days = ' . $this->sqlAutoLockApOffsetDays(). ',' ; } elseif( true == array_key_exists( 'AutoLockApOffsetDays', $this->getChangedColumns() ) ) { $strSql .= ' auto_lock_ap_offset_days = ' . $this->sqlAutoLockApOffsetDays() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_lock_gl_offset_days = ' . $this->sqlAutoLockGlOffsetDays(). ',' ; } elseif( true == array_key_exists( 'AutoLockGlOffsetDays', $this->getChangedColumns() ) ) { $strSql .= ' auto_lock_gl_offset_days = ' . $this->sqlAutoLockGlOffsetDays() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_post_month = ' . $this->sqlArPostMonth(). ',' ; } elseif( true == array_key_exists( 'ArPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' ar_post_month = ' . $this->sqlArPostMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_post_month = ' . $this->sqlApPostMonth(). ',' ; } elseif( true == array_key_exists( 'ApPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' ap_post_month = ' . $this->sqlApPostMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_post_month = ' . $this->sqlGlPostMonth(). ',' ; } elseif( true == array_key_exists( 'GlPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' gl_post_month = ' . $this->sqlGlPostMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_lock_month = ' . $this->sqlArLockMonth(). ',' ; } elseif( true == array_key_exists( 'ArLockMonth', $this->getChangedColumns() ) ) { $strSql .= ' ar_lock_month = ' . $this->sqlArLockMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_lock_month = ' . $this->sqlApLockMonth(). ',' ; } elseif( true == array_key_exists( 'ApLockMonth', $this->getChangedColumns() ) ) { $strSql .= ' ap_lock_month = ' . $this->sqlApLockMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_lock_month = ' . $this->sqlGlLockMonth(). ',' ; } elseif( true == array_key_exists( 'GlLockMonth', $this->getChangedColumns() ) ) { $strSql .= ' gl_lock_month = ' . $this->sqlGlLockMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_soft_lock_month = ' . $this->sqlArSoftLockMonth(). ',' ; } elseif( true == array_key_exists( 'ArSoftLockMonth', $this->getChangedColumns() ) ) { $strSql .= ' ar_soft_lock_month = ' . $this->sqlArSoftLockMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_soft_lock_month = ' . $this->sqlApSoftLockMonth(). ',' ; } elseif( true == array_key_exists( 'ApSoftLockMonth', $this->getChangedColumns() ) ) { $strSql .= ' ap_soft_lock_month = ' . $this->sqlApSoftLockMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_soft_lock_month = ' . $this->sqlGlSoftLockMonth(). ',' ; } elseif( true == array_key_exists( 'GlSoftLockMonth', $this->getChangedColumns() ) ) { $strSql .= ' gl_soft_lock_month = ' . $this->sqlGlSoftLockMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' opening_period = ' . $this->sqlOpeningPeriod(). ',' ; } elseif( true == array_key_exists( 'OpeningPeriod', $this->getChangedColumns() ) ) { $strSql .= ' opening_period = ' . $this->sqlOpeningPeriod() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' activation_date = ' . $this->sqlActivationDate(). ',' ; } elseif( true == array_key_exists( 'ActivationDate', $this->getChangedColumns() ) ) { $strSql .= ' activation_date = ' . $this->sqlActivationDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_date = ' . $this->sqlTerminationDate(). ',' ; } elseif( true == array_key_exists( 'TerminationDate', $this->getChangedColumns() ) ) { $strSql .= ' termination_date = ' . $this->sqlTerminationDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_auto_advance_post_month = ' . $this->sqlArAutoAdvancePostMonth(). ',' ; } elseif( true == array_key_exists( 'ArAutoAdvancePostMonth', $this->getChangedColumns() ) ) { $strSql .= ' ar_auto_advance_post_month = ' . $this->sqlArAutoAdvancePostMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_auto_advance_post_month = ' . $this->sqlApAutoAdvancePostMonth(). ',' ; } elseif( true == array_key_exists( 'ApAutoAdvancePostMonth', $this->getChangedColumns() ) ) { $strSql .= ' ap_auto_advance_post_month = ' . $this->sqlApAutoAdvancePostMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_auto_advance_post_month = ' . $this->sqlGlAutoAdvancePostMonth(). ',' ; } elseif( true == array_key_exists( 'GlAutoAdvancePostMonth', $this->getChangedColumns() ) ) { $strSql .= ' gl_auto_advance_post_month = ' . $this->sqlGlAutoAdvancePostMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_lock_ar = ' . $this->sqlIsAutoLockAr(). ',' ; } elseif( true == array_key_exists( 'IsAutoLockAr', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_lock_ar = ' . $this->sqlIsAutoLockAr() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_lock_ap = ' . $this->sqlIsAutoLockAp(). ',' ; } elseif( true == array_key_exists( 'IsAutoLockAp', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_lock_ap = ' . $this->sqlIsAutoLockAp() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_lock_gl = ' . $this->sqlIsAutoLockGl(). ',' ; } elseif( true == array_key_exists( 'IsAutoLockGl', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_lock_gl = ' . $this->sqlIsAutoLockGl() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ignore_holidays_and_weekends = ' . $this->sqlIgnoreHolidaysAndWeekends(). ',' ; } elseif( true == array_key_exists( 'IgnoreHolidaysAndWeekends', $this->getChangedColumns() ) ) { $strSql .= ' ignore_holidays_and_weekends = ' . $this->sqlIgnoreHolidaysAndWeekends() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_post_gpr_adjustment = ' . $this->sqlAutoPostGprAdjustment(). ',' ; } elseif( true == array_key_exists( 'AutoPostGprAdjustment', $this->getChangedColumns() ) ) { $strSql .= ' auto_post_gpr_adjustment = ' . $this->sqlAutoPostGprAdjustment() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gpr_book_basis = ' . $this->sqlGprBookBasis(). ',' ; } elseif( true == array_key_exists( 'GprBookBasis', $this->getChangedColumns() ) ) { $strSql .= ' gpr_book_basis = ' . $this->sqlGprBookBasis() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_modified_accrual_gpr = ' . $this->sqlIsModifiedAccrualGpr(). ',' ; } elseif( true == array_key_exists( 'IsModifiedAccrualGpr', $this->getChangedColumns() ) ) { $strSql .= ' is_modified_accrual_gpr = ' . $this->sqlIsModifiedAccrualGpr() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_market_rent_in_gpr = ' . $this->sqlUseMarketRentInGpr(). ',' ; } elseif( true == array_key_exists( 'UseMarketRentInGpr', $this->getChangedColumns() ) ) { $strSql .= ' use_market_rent_in_gpr = ' . $this->sqlUseMarketRentInGpr() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_begin_of_month_market_rent = ' . $this->sqlUseBeginOfMonthMarketRent(). ',' ; } elseif( true == array_key_exists( 'UseBeginOfMonthMarketRent', $this->getChangedColumns() ) ) { $strSql .= ' use_begin_of_month_market_rent = ' . $this->sqlUseBeginOfMonthMarketRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' group_excluded_units_in_gpr = ' . $this->sqlGroupExcludedUnitsInGpr(). ',' ; } elseif( true == array_key_exists( 'GroupExcludedUnitsInGpr', $this->getChangedColumns() ) ) { $strSql .= ' group_excluded_units_in_gpr = ' . $this->sqlGroupExcludedUnitsInGpr() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' calculate_vacancy_on_ar_close = ' . $this->sqlCalculateVacancyOnArClose(). ',' ; } elseif( true == array_key_exists( 'CalculateVacancyOnArClose', $this->getChangedColumns() ) ) { $strSql .= ' calculate_vacancy_on_ar_close = ' . $this->sqlCalculateVacancyOnArClose() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prorate_gpr_vacancy_for_transfers = ' . $this->sqlProrateGprVacancyForTransfers(). ',' ; } elseif( true == array_key_exists( 'ProrateGprVacancyForTransfers', $this->getChangedColumns() ) ) { $strSql .= ' prorate_gpr_vacancy_for_transfers = ' . $this->sqlProrateGprVacancyForTransfers() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit_payments_in_transit = ' . $this->sqlDepositPaymentsInTransit(). ',' ; } elseif( true == array_key_exists( 'DepositPaymentsInTransit', $this->getChangedColumns() ) ) { $strSql .= ' deposit_payments_in_transit = ' . $this->sqlDepositPaymentsInTransit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_pos_to_gl_during_month_end = ' . $this->sqlPostPosToGlDuringMonthEnd(). ',' ; } elseif( true == array_key_exists( 'PostPosToGlDuringMonthEnd', $this->getChangedColumns() ) ) { $strSql .= ' post_pos_to_gl_during_month_end = ' . $this->sqlPostPosToGlDuringMonthEnd() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_post_refund_invoice = ' . $this->sqlAutoPostRefundInvoice(). ',' ; } elseif( true == array_key_exists( 'AutoPostRefundInvoice', $this->getChangedColumns() ) ) { $strSql .= ' auto_post_refund_invoice = ' . $this->sqlAutoPostRefundInvoice() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allocate_rent_first = ' . $this->sqlAllocateRentFirst(). ',' ; } elseif( true == array_key_exists( 'AllocateRentFirst', $this->getChangedColumns() ) ) { $strSql .= ' allocate_rent_first = ' . $this->sqlAllocateRentFirst() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' posting_and_caching_disabled = ' . $this->sqlPostingAndCachingDisabled(). ',' ; } elseif( true == array_key_exists( 'PostingAndCachingDisabled', $this->getChangedColumns() ) ) { $strSql .= ' posting_and_caching_disabled = ' . $this->sqlPostingAndCachingDisabled() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_custom_ar_codes = ' . $this->sqlUseCustomArCodes(). ',' ; } elseif( true == array_key_exists( 'UseCustomArCodes', $this->getChangedColumns() ) ) { $strSql .= ' use_custom_ar_codes = ' . $this->sqlUseCustomArCodes() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_pos_for_receiving = ' . $this->sqlRequirePosForReceiving(). ',' ; } elseif( true == array_key_exists( 'RequirePosForReceiving', $this->getChangedColumns() ) ) { $strSql .= ' require_pos_for_receiving = ' . $this->sqlRequirePosForReceiving() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_pos_for_jobs = ' . $this->sqlRequirePosForJobs(). ',' ; } elseif( true == array_key_exists( 'RequirePosForJobs', $this->getChangedColumns() ) ) { $strSql .= ' require_pos_for_jobs = ' . $this->sqlRequirePosForJobs() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_pos_for_manual_invoices = ' . $this->sqlRequirePosForManualInvoices(). ',' ; } elseif( true == array_key_exists( 'RequirePosForManualInvoices', $this->getChangedColumns() ) ) { $strSql .= ' require_pos_for_manual_invoices = ' . $this->sqlRequirePosForManualInvoices() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_vendor_compliance = ' . $this->sqlRequireVendorCompliance(). ',' ; } elseif( true == array_key_exists( 'RequireVendorCompliance', $this->getChangedColumns() ) ) { $strSql .= ' require_vendor_compliance = ' . $this->sqlRequireVendorCompliance() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' activate_standard_posting = ' . $this->sqlActivateStandardPosting(). ',' ; } elseif( true == array_key_exists( 'ActivateStandardPosting', $this->getChangedColumns() ) ) { $strSql .= ' activate_standard_posting = ' . $this->sqlActivateStandardPosting() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' activate_po_posting = ' . $this->sqlActivatePoPosting(). ',' ; } elseif( true == array_key_exists( 'ActivatePoPosting', $this->getChangedColumns() ) ) { $strSql .= ' activate_po_posting = ' . $this->sqlActivatePoPosting() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' activate_asset_posting = ' . $this->sqlActivateAssetPosting(). ',' ; } elseif( true == array_key_exists( 'ActivateAssetPosting', $this->getChangedColumns() ) ) { $strSql .= ' activate_asset_posting = ' . $this->sqlActivateAssetPosting() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ar_migration_mode = ' . $this->sqlIsArMigrationMode(). ',' ; } elseif( true == array_key_exists( 'IsArMigrationMode', $this->getChangedColumns() ) ) { $strSql .= ' is_ar_migration_mode = ' . $this->sqlIsArMigrationMode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ap_migration_mode = ' . $this->sqlIsApMigrationMode(). ',' ; } elseif( true == array_key_exists( 'IsApMigrationMode', $this->getChangedColumns() ) ) { $strSql .= ' is_ap_migration_mode = ' . $this->sqlIsApMigrationMode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_asset_migration_mode = ' . $this->sqlIsAssetMigrationMode(). ',' ; } elseif( true == array_key_exists( 'IsAssetMigrationMode', $this->getChangedColumns() ) ) { $strSql .= ' is_asset_migration_mode = ' . $this->sqlIsAssetMigrationMode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_update_budgeted_rent = ' . $this->sqlAutoUpdateBudgetedRent(). ',' ; } elseif( true == array_key_exists( 'AutoUpdateBudgetedRent', $this->getChangedColumns() ) ) { $strSql .= ' auto_update_budgeted_rent = ' . $this->sqlAutoUpdateBudgetedRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_validation_mode = ' . $this->sqlUserValidationMode(). ',' ; } elseif( true == array_key_exists( 'UserValidationMode', $this->getChangedColumns() ) ) { $strSql .= ' user_validation_mode = ' . $this->sqlUserValidationMode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' script_validation_mode = ' . $this->sqlScriptValidationMode(). ',' ; } elseif( true == array_key_exists( 'ScriptValidationMode', $this->getChangedColumns() ) ) { $strSql .= ' script_validation_mode = ' . $this->sqlScriptValidationMode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' military_mac_bank_account_id = ' . $this->sqlMilitaryMacBankAccountId(). ',' ; } elseif( true == array_key_exists( 'MilitaryMacBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' military_mac_bank_account_id = ' . $this->sqlMilitaryMacBankAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_costing_ap_bank_account_id = ' . $this->sqlJobCostingApBankAccountId(). ',' ; } elseif( true == array_key_exists( 'JobCostingApBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' job_costing_ap_bank_account_id = ' . $this->sqlJobCostingApBankAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_ap_bank_account_id = ' . $this->sqlCcApBankAccountId(). ',' ; } elseif( true == array_key_exists( 'CcApBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' cc_ap_bank_account_id = ' . $this->sqlCcApBankAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' straight_line_adjustment_ar_code_id = ' . $this->sqlStraightLineAdjustmentArCodeId(). ',' ; } elseif( true == array_key_exists( 'StraightLineAdjustmentArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' straight_line_adjustment_ar_code_id = ' . $this->sqlStraightLineAdjustmentArCodeId() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'gl_tree_id' => $this->getGlTreeId(),
			'rent_bank_account_id' => $this->getRentBankAccountId(),
			'deposit_bank_account_id' => $this->getDepositBankAccountId(),
			'refund_bank_account_id' => $this->getRefundBankAccountId(),
			'ap_bank_account_id' => $this->getApBankAccountId(),
			'ap_gl_account_id' => $this->getApGlAccountId(),
			'inter_co_reimbursement_bank_account_id' => $this->getInterCoReimbursementBankAccountId(),
			'market_rent_gl_account_id' => $this->getMarketRentGlAccountId(),
			'gain_to_lease_gl_account_id' => $this->getGainToLeaseGlAccountId(),
			'loss_to_lease_gl_account_id' => $this->getLossToLeaseGlAccountId(),
			'vacancy_loss_gl_account_id' => $this->getVacancyLossGlAccountId(),
			'delinquent_rent_gl_account_id' => $this->getDelinquentRentGlAccountId(),
			'prepaid_rent_gl_account_id' => $this->getPrepaidRentGlAccountId(),
			'retained_earnings_gl_account_id' => $this->getRetainedEarningsGlAccountId(),
			'inter_co_ar_gl_account_id' => $this->getInterCoArGlAccountId(),
			'inter_co_ap_gl_account_id' => $this->getInterCoApGlAccountId(),
			'po_balances_ap_gl_account_id' => $this->getPoBalancesApGlAccountId(),
			'purchases_clearing_gl_account_id' => $this->getPurchasesClearingGlAccountId(),
			'pending_reimbursements_gl_account_id' => $this->getPendingReimbursementsGlAccountId(),
			'unclaimed_property_gl_account_id' => $this->getUnclaimedPropertyGlAccountId(),
			'accel_rent_ar_code_id' => $this->getAccelRentArCodeId(),
			'skip_accel_rent_ar_code_id' => $this->getSkipAccelRentArCodeId(),
			'rent_ar_code_id' => $this->getRentArCodeId(),
			'deposit_ar_code_id' => $this->getDepositArCodeId(),
			'percent_of_sales_ar_code_id' => $this->getPercentOfSalesArCodeId(),
			'market_rent_type_id' => $this->getMarketRentTypeId(),
			'lease_expiration_structure_id' => $this->getLeaseExpirationStructureId(),
			'draw_request_ap_header_type_id' => $this->getDrawRequestApHeaderTypeId(),
			'unclaimed_property_ap_payee_id' => $this->getUnclaimedPropertyApPayeeId(),
			'ap_receiving_type_id' => $this->getApReceivingTypeId(),
			'is_cash_basis' => $this->getIsCashBasis(),
			'fiscal_year_end_month' => $this->getFiscalYearEndMonth(),
			'ar_advance_day' => $this->getArAdvanceDay(),
			'ap_advance_day' => $this->getApAdvanceDay(),
			'gl_advance_day' => $this->getGlAdvanceDay(),
			'auto_lock_ar_offset_days' => $this->getAutoLockArOffsetDays(),
			'auto_lock_ap_offset_days' => $this->getAutoLockApOffsetDays(),
			'auto_lock_gl_offset_days' => $this->getAutoLockGlOffsetDays(),
			'ar_post_month' => $this->getArPostMonth(),
			'ap_post_month' => $this->getApPostMonth(),
			'gl_post_month' => $this->getGlPostMonth(),
			'ar_lock_month' => $this->getArLockMonth(),
			'ap_lock_month' => $this->getApLockMonth(),
			'gl_lock_month' => $this->getGlLockMonth(),
			'ar_soft_lock_month' => $this->getArSoftLockMonth(),
			'ap_soft_lock_month' => $this->getApSoftLockMonth(),
			'gl_soft_lock_month' => $this->getGlSoftLockMonth(),
			'opening_period' => $this->getOpeningPeriod(),
			'activation_date' => $this->getActivationDate(),
			'termination_date' => $this->getTerminationDate(),
			'ar_auto_advance_post_month' => $this->getArAutoAdvancePostMonth(),
			'ap_auto_advance_post_month' => $this->getApAutoAdvancePostMonth(),
			'gl_auto_advance_post_month' => $this->getGlAutoAdvancePostMonth(),
			'is_auto_lock_ar' => $this->getIsAutoLockAr(),
			'is_auto_lock_ap' => $this->getIsAutoLockAp(),
			'is_auto_lock_gl' => $this->getIsAutoLockGl(),
			'ignore_holidays_and_weekends' => $this->getIgnoreHolidaysAndWeekends(),
			'auto_post_gpr_adjustment' => $this->getAutoPostGprAdjustment(),
			'gpr_book_basis' => $this->getGprBookBasis(),
			'is_modified_accrual_gpr' => $this->getIsModifiedAccrualGpr(),
			'use_market_rent_in_gpr' => $this->getUseMarketRentInGpr(),
			'use_begin_of_month_market_rent' => $this->getUseBeginOfMonthMarketRent(),
			'group_excluded_units_in_gpr' => $this->getGroupExcludedUnitsInGpr(),
			'calculate_vacancy_on_ar_close' => $this->getCalculateVacancyOnArClose(),
			'prorate_gpr_vacancy_for_transfers' => $this->getProrateGprVacancyForTransfers(),
			'deposit_payments_in_transit' => $this->getDepositPaymentsInTransit(),
			'post_pos_to_gl_during_month_end' => $this->getPostPosToGlDuringMonthEnd(),
			'auto_post_refund_invoice' => $this->getAutoPostRefundInvoice(),
			'allocate_rent_first' => $this->getAllocateRentFirst(),
			'posting_and_caching_disabled' => $this->getPostingAndCachingDisabled(),
			'use_custom_ar_codes' => $this->getUseCustomArCodes(),
			'require_pos_for_receiving' => $this->getRequirePosForReceiving(),
			'require_pos_for_jobs' => $this->getRequirePosForJobs(),
			'require_pos_for_manual_invoices' => $this->getRequirePosForManualInvoices(),
			'require_vendor_compliance' => $this->getRequireVendorCompliance(),
			'activate_standard_posting' => $this->getActivateStandardPosting(),
			'activate_po_posting' => $this->getActivatePoPosting(),
			'activate_asset_posting' => $this->getActivateAssetPosting(),
			'is_ar_migration_mode' => $this->getIsArMigrationMode(),
			'is_ap_migration_mode' => $this->getIsApMigrationMode(),
			'is_asset_migration_mode' => $this->getIsAssetMigrationMode(),
			'auto_update_budgeted_rent' => $this->getAutoUpdateBudgetedRent(),
			'user_validation_mode' => $this->getUserValidationMode(),
			'script_validation_mode' => $this->getScriptValidationMode(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'military_mac_bank_account_id' => $this->getMilitaryMacBankAccountId(),
			'job_costing_ap_bank_account_id' => $this->getJobCostingApBankAccountId(),
			'cc_ap_bank_account_id' => $this->getCcApBankAccountId(),
			'straight_line_adjustment_ar_code_id' => $this->getStraightLineAdjustmentArCodeId()
		);
	}

}
?>