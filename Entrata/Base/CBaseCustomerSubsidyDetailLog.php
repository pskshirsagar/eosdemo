<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerSubsidyDetailLog extends CEosSingularBase {

	const TABLE_NAME = 'public.customer_subsidy_detail_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerSubsidyDetailId;
	protected $m_intCustomerId;
	protected $m_intSubsidyCitizenshipTypeId;
	protected $m_intSubsidyEthnicityTypeId;
	protected $m_intSubsidyEthnicitySubTypeId;
	protected $m_intSubsidySsnExceptionTypeId;
	protected $m_intSubsidyDependentTypeId;
	protected $m_intPriorCustomerSubsidyDetailLogId;
	protected $m_strPostDate;
	protected $m_strApplyThroughPostDate;
	protected $m_strAlienRegistrationNumber;
	protected $m_strTracsIdEncrypted;
	protected $m_fltWelfareRentAmount;
	protected $m_fltMaxWelfareRentAmount;
	protected $m_strLogDatetime;
	protected $m_intIsPostDateIgnored;
	protected $m_intIsOpeningLog;
	protected $m_boolHasDeclinedToSpecifyGender;
	protected $m_boolIsPartTimeStudent;
	protected $m_boolIsPoliceOrSecurityOfficer;
	protected $m_boolIsQualifiedSurvivingMember;
	protected $m_strDateOfDeath;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strApplyThroughPostDate = '12/31/2099';
		$this->m_intIsPostDateIgnored = '0';
		$this->m_intIsOpeningLog = '0';
		$this->m_boolHasDeclinedToSpecifyGender = true;
		$this->m_boolIsPartTimeStudent = false;
		$this->m_boolIsPoliceOrSecurityOfficer = false;
		$this->m_boolIsQualifiedSurvivingMember = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_subsidy_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerSubsidyDetailId', trim( $arrValues['customer_subsidy_detail_id'] ) ); elseif( isset( $arrValues['customer_subsidy_detail_id'] ) ) $this->setCustomerSubsidyDetailId( $arrValues['customer_subsidy_detail_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['subsidy_citizenship_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyCitizenshipTypeId', trim( $arrValues['subsidy_citizenship_type_id'] ) ); elseif( isset( $arrValues['subsidy_citizenship_type_id'] ) ) $this->setSubsidyCitizenshipTypeId( $arrValues['subsidy_citizenship_type_id'] );
		if( isset( $arrValues['subsidy_ethnicity_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyEthnicityTypeId', trim( $arrValues['subsidy_ethnicity_type_id'] ) ); elseif( isset( $arrValues['subsidy_ethnicity_type_id'] ) ) $this->setSubsidyEthnicityTypeId( $arrValues['subsidy_ethnicity_type_id'] );
		if( isset( $arrValues['subsidy_ethnicity_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyEthnicitySubTypeId', trim( $arrValues['subsidy_ethnicity_sub_type_id'] ) ); elseif( isset( $arrValues['subsidy_ethnicity_sub_type_id'] ) ) $this->setSubsidyEthnicitySubTypeId( $arrValues['subsidy_ethnicity_sub_type_id'] );
		if( isset( $arrValues['subsidy_ssn_exception_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidySsnExceptionTypeId', trim( $arrValues['subsidy_ssn_exception_type_id'] ) ); elseif( isset( $arrValues['subsidy_ssn_exception_type_id'] ) ) $this->setSubsidySsnExceptionTypeId( $arrValues['subsidy_ssn_exception_type_id'] );
		if( isset( $arrValues['subsidy_dependent_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyDependentTypeId', trim( $arrValues['subsidy_dependent_type_id'] ) ); elseif( isset( $arrValues['subsidy_dependent_type_id'] ) ) $this->setSubsidyDependentTypeId( $arrValues['subsidy_dependent_type_id'] );
		if( isset( $arrValues['prior_customer_subsidy_detail_log_id'] ) && $boolDirectSet ) $this->set( 'm_intPriorCustomerSubsidyDetailLogId', trim( $arrValues['prior_customer_subsidy_detail_log_id'] ) ); elseif( isset( $arrValues['prior_customer_subsidy_detail_log_id'] ) ) $this->setPriorCustomerSubsidyDetailLogId( $arrValues['prior_customer_subsidy_detail_log_id'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['apply_through_post_date'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostDate', trim( $arrValues['apply_through_post_date'] ) ); elseif( isset( $arrValues['apply_through_post_date'] ) ) $this->setApplyThroughPostDate( $arrValues['apply_through_post_date'] );
		if( isset( $arrValues['alien_registration_number'] ) && $boolDirectSet ) $this->set( 'm_strAlienRegistrationNumber', trim( stripcslashes( $arrValues['alien_registration_number'] ) ) ); elseif( isset( $arrValues['alien_registration_number'] ) ) $this->setAlienRegistrationNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['alien_registration_number'] ) : $arrValues['alien_registration_number'] );
		if( isset( $arrValues['tracs_id_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTracsIdEncrypted', trim( stripcslashes( $arrValues['tracs_id_encrypted'] ) ) ); elseif( isset( $arrValues['tracs_id_encrypted'] ) ) $this->setTracsIdEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tracs_id_encrypted'] ) : $arrValues['tracs_id_encrypted'] );
		if( isset( $arrValues['welfare_rent_amount'] ) && $boolDirectSet ) $this->set( 'm_fltWelfareRentAmount', trim( $arrValues['welfare_rent_amount'] ) ); elseif( isset( $arrValues['welfare_rent_amount'] ) ) $this->setWelfareRentAmount( $arrValues['welfare_rent_amount'] );
		if( isset( $arrValues['max_welfare_rent_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMaxWelfareRentAmount', trim( $arrValues['max_welfare_rent_amount'] ) ); elseif( isset( $arrValues['max_welfare_rent_amount'] ) ) $this->setMaxWelfareRentAmount( $arrValues['max_welfare_rent_amount'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['is_post_date_ignored'] ) && $boolDirectSet ) $this->set( 'm_intIsPostDateIgnored', trim( $arrValues['is_post_date_ignored'] ) ); elseif( isset( $arrValues['is_post_date_ignored'] ) ) $this->setIsPostDateIgnored( $arrValues['is_post_date_ignored'] );
		if( isset( $arrValues['is_opening_log'] ) && $boolDirectSet ) $this->set( 'm_intIsOpeningLog', trim( $arrValues['is_opening_log'] ) ); elseif( isset( $arrValues['is_opening_log'] ) ) $this->setIsOpeningLog( $arrValues['is_opening_log'] );
		if( isset( $arrValues['has_declined_to_specify_gender'] ) && $boolDirectSet ) $this->set( 'm_boolHasDeclinedToSpecifyGender', trim( stripcslashes( $arrValues['has_declined_to_specify_gender'] ) ) ); elseif( isset( $arrValues['has_declined_to_specify_gender'] ) ) $this->setHasDeclinedToSpecifyGender( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_declined_to_specify_gender'] ) : $arrValues['has_declined_to_specify_gender'] );
		if( isset( $arrValues['is_part_time_student'] ) && $boolDirectSet ) $this->set( 'm_boolIsPartTimeStudent', trim( stripcslashes( $arrValues['is_part_time_student'] ) ) ); elseif( isset( $arrValues['is_part_time_student'] ) ) $this->setIsPartTimeStudent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_part_time_student'] ) : $arrValues['is_part_time_student'] );
		if( isset( $arrValues['is_police_or_security_officer'] ) && $boolDirectSet ) $this->set( 'm_boolIsPoliceOrSecurityOfficer', trim( stripcslashes( $arrValues['is_police_or_security_officer'] ) ) ); elseif( isset( $arrValues['is_police_or_security_officer'] ) ) $this->setIsPoliceOrSecurityOfficer( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_police_or_security_officer'] ) : $arrValues['is_police_or_security_officer'] );
		if( isset( $arrValues['is_qualified_surviving_member'] ) && $boolDirectSet ) $this->set( 'm_boolIsQualifiedSurvivingMember', trim( stripcslashes( $arrValues['is_qualified_surviving_member'] ) ) ); elseif( isset( $arrValues['is_qualified_surviving_member'] ) ) $this->setIsQualifiedSurvivingMember( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_qualified_surviving_member'] ) : $arrValues['is_qualified_surviving_member'] );
		if( isset( $arrValues['date_of_death'] ) && $boolDirectSet ) $this->set( 'm_strDateOfDeath', trim( $arrValues['date_of_death'] ) ); elseif( isset( $arrValues['date_of_death'] ) ) $this->setDateOfDeath( $arrValues['date_of_death'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerSubsidyDetailId( $intCustomerSubsidyDetailId ) {
		$this->set( 'm_intCustomerSubsidyDetailId', CStrings::strToIntDef( $intCustomerSubsidyDetailId, NULL, false ) );
	}

	public function getCustomerSubsidyDetailId() {
		return $this->m_intCustomerSubsidyDetailId;
	}

	public function sqlCustomerSubsidyDetailId() {
		return ( true == isset( $this->m_intCustomerSubsidyDetailId ) ) ? ( string ) $this->m_intCustomerSubsidyDetailId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setSubsidyCitizenshipTypeId( $intSubsidyCitizenshipTypeId ) {
		$this->set( 'm_intSubsidyCitizenshipTypeId', CStrings::strToIntDef( $intSubsidyCitizenshipTypeId, NULL, false ) );
	}

	public function getSubsidyCitizenshipTypeId() {
		return $this->m_intSubsidyCitizenshipTypeId;
	}

	public function sqlSubsidyCitizenshipTypeId() {
		return ( true == isset( $this->m_intSubsidyCitizenshipTypeId ) ) ? ( string ) $this->m_intSubsidyCitizenshipTypeId : 'NULL';
	}

	public function setSubsidyEthnicityTypeId( $intSubsidyEthnicityTypeId ) {
		$this->set( 'm_intSubsidyEthnicityTypeId', CStrings::strToIntDef( $intSubsidyEthnicityTypeId, NULL, false ) );
	}

	public function getSubsidyEthnicityTypeId() {
		return $this->m_intSubsidyEthnicityTypeId;
	}

	public function sqlSubsidyEthnicityTypeId() {
		return ( true == isset( $this->m_intSubsidyEthnicityTypeId ) ) ? ( string ) $this->m_intSubsidyEthnicityTypeId : 'NULL';
	}

	public function setSubsidyEthnicitySubTypeId( $intSubsidyEthnicitySubTypeId ) {
		$this->set( 'm_intSubsidyEthnicitySubTypeId', CStrings::strToIntDef( $intSubsidyEthnicitySubTypeId, NULL, false ) );
	}

	public function getSubsidyEthnicitySubTypeId() {
		return $this->m_intSubsidyEthnicitySubTypeId;
	}

	public function sqlSubsidyEthnicitySubTypeId() {
		return ( true == isset( $this->m_intSubsidyEthnicitySubTypeId ) ) ? ( string ) $this->m_intSubsidyEthnicitySubTypeId : 'NULL';
	}

	public function setSubsidySsnExceptionTypeId( $intSubsidySsnExceptionTypeId ) {
		$this->set( 'm_intSubsidySsnExceptionTypeId', CStrings::strToIntDef( $intSubsidySsnExceptionTypeId, NULL, false ) );
	}

	public function getSubsidySsnExceptionTypeId() {
		return $this->m_intSubsidySsnExceptionTypeId;
	}

	public function sqlSubsidySsnExceptionTypeId() {
		return ( true == isset( $this->m_intSubsidySsnExceptionTypeId ) ) ? ( string ) $this->m_intSubsidySsnExceptionTypeId : 'NULL';
	}

	public function setSubsidyDependentTypeId( $intSubsidyDependentTypeId ) {
		$this->set( 'm_intSubsidyDependentTypeId', CStrings::strToIntDef( $intSubsidyDependentTypeId, NULL, false ) );
	}

	public function getSubsidyDependentTypeId() {
		return $this->m_intSubsidyDependentTypeId;
	}

	public function sqlSubsidyDependentTypeId() {
		return ( true == isset( $this->m_intSubsidyDependentTypeId ) ) ? ( string ) $this->m_intSubsidyDependentTypeId : 'NULL';
	}

	public function setPriorCustomerSubsidyDetailLogId( $intPriorCustomerSubsidyDetailLogId ) {
		$this->set( 'm_intPriorCustomerSubsidyDetailLogId', CStrings::strToIntDef( $intPriorCustomerSubsidyDetailLogId, NULL, false ) );
	}

	public function getPriorCustomerSubsidyDetailLogId() {
		return $this->m_intPriorCustomerSubsidyDetailLogId;
	}

	public function sqlPriorCustomerSubsidyDetailLogId() {
		return ( true == isset( $this->m_intPriorCustomerSubsidyDetailLogId ) ) ? ( string ) $this->m_intPriorCustomerSubsidyDetailLogId : 'NULL';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setApplyThroughPostDate( $strApplyThroughPostDate ) {
		$this->set( 'm_strApplyThroughPostDate', CStrings::strTrimDef( $strApplyThroughPostDate, -1, NULL, true ) );
	}

	public function getApplyThroughPostDate() {
		return $this->m_strApplyThroughPostDate;
	}

	public function sqlApplyThroughPostDate() {
		return ( true == isset( $this->m_strApplyThroughPostDate ) ) ? '\'' . $this->m_strApplyThroughPostDate . '\'' : 'NOW()';
	}

	public function setAlienRegistrationNumber( $strAlienRegistrationNumber ) {
		$this->set( 'm_strAlienRegistrationNumber', CStrings::strTrimDef( $strAlienRegistrationNumber, 10, NULL, true ) );
	}

	public function getAlienRegistrationNumber() {
		return $this->m_strAlienRegistrationNumber;
	}

	public function sqlAlienRegistrationNumber() {
		return ( true == isset( $this->m_strAlienRegistrationNumber ) ) ? '\'' . addslashes( $this->m_strAlienRegistrationNumber ) . '\'' : 'NULL';
	}

	public function setTracsIdEncrypted( $strTracsIdEncrypted ) {
		$this->set( 'm_strTracsIdEncrypted', CStrings::strTrimDef( $strTracsIdEncrypted, 240, NULL, true ) );
	}

	public function getTracsIdEncrypted() {
		return $this->m_strTracsIdEncrypted;
	}

	public function sqlTracsIdEncrypted() {
		return ( true == isset( $this->m_strTracsIdEncrypted ) ) ? '\'' . addslashes( $this->m_strTracsIdEncrypted ) . '\'' : 'NULL';
	}

	public function setWelfareRentAmount( $fltWelfareRentAmount ) {
		$this->set( 'm_fltWelfareRentAmount', CStrings::strToFloatDef( $fltWelfareRentAmount, NULL, false, 2 ) );
	}

	public function getWelfareRentAmount() {
		return $this->m_fltWelfareRentAmount;
	}

	public function sqlWelfareRentAmount() {
		return ( true == isset( $this->m_fltWelfareRentAmount ) ) ? ( string ) $this->m_fltWelfareRentAmount : 'NULL';
	}

	public function setMaxWelfareRentAmount( $fltMaxWelfareRentAmount ) {
		$this->set( 'm_fltMaxWelfareRentAmount', CStrings::strToFloatDef( $fltMaxWelfareRentAmount, NULL, false, 2 ) );
	}

	public function getMaxWelfareRentAmount() {
		return $this->m_fltMaxWelfareRentAmount;
	}

	public function sqlMaxWelfareRentAmount() {
		return ( true == isset( $this->m_fltMaxWelfareRentAmount ) ) ? ( string ) $this->m_fltMaxWelfareRentAmount : 'NULL';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setIsPostDateIgnored( $intIsPostDateIgnored ) {
		$this->set( 'm_intIsPostDateIgnored', CStrings::strToIntDef( $intIsPostDateIgnored, NULL, false ) );
	}

	public function getIsPostDateIgnored() {
		return $this->m_intIsPostDateIgnored;
	}

	public function sqlIsPostDateIgnored() {
		return ( true == isset( $this->m_intIsPostDateIgnored ) ) ? ( string ) $this->m_intIsPostDateIgnored : '0';
	}

	public function setIsOpeningLog( $intIsOpeningLog ) {
		$this->set( 'm_intIsOpeningLog', CStrings::strToIntDef( $intIsOpeningLog, NULL, false ) );
	}

	public function getIsOpeningLog() {
		return $this->m_intIsOpeningLog;
	}

	public function sqlIsOpeningLog() {
		return ( true == isset( $this->m_intIsOpeningLog ) ) ? ( string ) $this->m_intIsOpeningLog : '0';
	}

	public function setHasDeclinedToSpecifyGender( $boolHasDeclinedToSpecifyGender ) {
		$this->set( 'm_boolHasDeclinedToSpecifyGender', CStrings::strToBool( $boolHasDeclinedToSpecifyGender ) );
	}

	public function getHasDeclinedToSpecifyGender() {
		return $this->m_boolHasDeclinedToSpecifyGender;
	}

	public function sqlHasDeclinedToSpecifyGender() {
		return ( true == isset( $this->m_boolHasDeclinedToSpecifyGender ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasDeclinedToSpecifyGender ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPartTimeStudent( $boolIsPartTimeStudent ) {
		$this->set( 'm_boolIsPartTimeStudent', CStrings::strToBool( $boolIsPartTimeStudent ) );
	}

	public function getIsPartTimeStudent() {
		return $this->m_boolIsPartTimeStudent;
	}

	public function sqlIsPartTimeStudent() {
		return ( true == isset( $this->m_boolIsPartTimeStudent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPartTimeStudent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPoliceOrSecurityOfficer( $boolIsPoliceOrSecurityOfficer ) {
		$this->set( 'm_boolIsPoliceOrSecurityOfficer', CStrings::strToBool( $boolIsPoliceOrSecurityOfficer ) );
	}

	public function getIsPoliceOrSecurityOfficer() {
		return $this->m_boolIsPoliceOrSecurityOfficer;
	}

	public function sqlIsPoliceOrSecurityOfficer() {
		return ( true == isset( $this->m_boolIsPoliceOrSecurityOfficer ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPoliceOrSecurityOfficer ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsQualifiedSurvivingMember( $boolIsQualifiedSurvivingMember ) {
		$this->set( 'm_boolIsQualifiedSurvivingMember', CStrings::strToBool( $boolIsQualifiedSurvivingMember ) );
	}

	public function getIsQualifiedSurvivingMember() {
		return $this->m_boolIsQualifiedSurvivingMember;
	}

	public function sqlIsQualifiedSurvivingMember() {
		return ( true == isset( $this->m_boolIsQualifiedSurvivingMember ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsQualifiedSurvivingMember ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDateOfDeath( $strDateOfDeath ) {
		$this->set( 'm_strDateOfDeath', CStrings::strTrimDef( $strDateOfDeath, -1, NULL, true ) );
	}

	public function getDateOfDeath() {
		return $this->m_strDateOfDeath;
	}

	public function sqlDateOfDeath() {
		return ( true == isset( $this->m_strDateOfDeath ) ) ? '\'' . $this->m_strDateOfDeath . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_subsidy_detail_id, customer_id, subsidy_citizenship_type_id, subsidy_ethnicity_type_id, subsidy_ethnicity_sub_type_id, subsidy_ssn_exception_type_id, subsidy_dependent_type_id, prior_customer_subsidy_detail_log_id, post_date, apply_through_post_date, alien_registration_number, tracs_id_encrypted, welfare_rent_amount, max_welfare_rent_amount, log_datetime, is_post_date_ignored, is_opening_log, has_declined_to_specify_gender, is_part_time_student, is_police_or_security_officer, is_qualified_surviving_member, date_of_death, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCustomerSubsidyDetailId() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlSubsidyCitizenshipTypeId() . ', ' .
 						$this->sqlSubsidyEthnicityTypeId() . ', ' .
 						$this->sqlSubsidyEthnicitySubTypeId() . ', ' .
 						$this->sqlSubsidySsnExceptionTypeId() . ', ' .
 						$this->sqlSubsidyDependentTypeId() . ', ' .
 						$this->sqlPriorCustomerSubsidyDetailLogId() . ', ' .
 						$this->sqlPostDate() . ', ' .
 						$this->sqlApplyThroughPostDate() . ', ' .
 						$this->sqlAlienRegistrationNumber() . ', ' .
 						$this->sqlTracsIdEncrypted() . ', ' .
 						$this->sqlWelfareRentAmount() . ', ' .
 						$this->sqlMaxWelfareRentAmount() . ', ' .
 						$this->sqlLogDatetime() . ', ' .
 						$this->sqlIsPostDateIgnored() . ', ' .
 						$this->sqlIsOpeningLog() . ', ' .
 						$this->sqlHasDeclinedToSpecifyGender() . ', ' .
 						$this->sqlIsPartTimeStudent() . ', ' .
 						$this->sqlIsPoliceOrSecurityOfficer() . ', ' .
 						$this->sqlIsQualifiedSurvivingMember() . ', ' .
 						$this->sqlDateOfDeath() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_subsidy_detail_id = ' . $this->sqlCustomerSubsidyDetailId() . ','; } elseif( true == array_key_exists( 'CustomerSubsidyDetailId', $this->getChangedColumns() ) ) { $strSql .= ' customer_subsidy_detail_id = ' . $this->sqlCustomerSubsidyDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_citizenship_type_id = ' . $this->sqlSubsidyCitizenshipTypeId() . ','; } elseif( true == array_key_exists( 'SubsidyCitizenshipTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_citizenship_type_id = ' . $this->sqlSubsidyCitizenshipTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_ethnicity_type_id = ' . $this->sqlSubsidyEthnicityTypeId() . ','; } elseif( true == array_key_exists( 'SubsidyEthnicityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_ethnicity_type_id = ' . $this->sqlSubsidyEthnicityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_ethnicity_sub_type_id = ' . $this->sqlSubsidyEthnicitySubTypeId() . ','; } elseif( true == array_key_exists( 'SubsidyEthnicitySubTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_ethnicity_sub_type_id = ' . $this->sqlSubsidyEthnicitySubTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_ssn_exception_type_id = ' . $this->sqlSubsidySsnExceptionTypeId() . ','; } elseif( true == array_key_exists( 'SubsidySsnExceptionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_ssn_exception_type_id = ' . $this->sqlSubsidySsnExceptionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_dependent_type_id = ' . $this->sqlSubsidyDependentTypeId() . ','; } elseif( true == array_key_exists( 'SubsidyDependentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_dependent_type_id = ' . $this->sqlSubsidyDependentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prior_customer_subsidy_detail_log_id = ' . $this->sqlPriorCustomerSubsidyDetailLogId() . ','; } elseif( true == array_key_exists( 'PriorCustomerSubsidyDetailLogId', $this->getChangedColumns() ) ) { $strSql .= ' prior_customer_subsidy_detail_log_id = ' . $this->sqlPriorCustomerSubsidyDetailLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate() . ','; } elseif( true == array_key_exists( 'ApplyThroughPostDate', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alien_registration_number = ' . $this->sqlAlienRegistrationNumber() . ','; } elseif( true == array_key_exists( 'AlienRegistrationNumber', $this->getChangedColumns() ) ) { $strSql .= ' alien_registration_number = ' . $this->sqlAlienRegistrationNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tracs_id_encrypted = ' . $this->sqlTracsIdEncrypted() . ','; } elseif( true == array_key_exists( 'TracsIdEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' tracs_id_encrypted = ' . $this->sqlTracsIdEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' welfare_rent_amount = ' . $this->sqlWelfareRentAmount() . ','; } elseif( true == array_key_exists( 'WelfareRentAmount', $this->getChangedColumns() ) ) { $strSql .= ' welfare_rent_amount = ' . $this->sqlWelfareRentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_welfare_rent_amount = ' . $this->sqlMaxWelfareRentAmount() . ','; } elseif( true == array_key_exists( 'MaxWelfareRentAmount', $this->getChangedColumns() ) ) { $strSql .= ' max_welfare_rent_amount = ' . $this->sqlMaxWelfareRentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored() . ','; } elseif( true == array_key_exists( 'IsPostDateIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog() . ','; } elseif( true == array_key_exists( 'IsOpeningLog', $this->getChangedColumns() ) ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_declined_to_specify_gender = ' . $this->sqlHasDeclinedToSpecifyGender() . ','; } elseif( true == array_key_exists( 'HasDeclinedToSpecifyGender', $this->getChangedColumns() ) ) { $strSql .= ' has_declined_to_specify_gender = ' . $this->sqlHasDeclinedToSpecifyGender() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_part_time_student = ' . $this->sqlIsPartTimeStudent() . ','; } elseif( true == array_key_exists( 'IsPartTimeStudent', $this->getChangedColumns() ) ) { $strSql .= ' is_part_time_student = ' . $this->sqlIsPartTimeStudent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_police_or_security_officer = ' . $this->sqlIsPoliceOrSecurityOfficer() . ','; } elseif( true == array_key_exists( 'IsPoliceOrSecurityOfficer', $this->getChangedColumns() ) ) { $strSql .= ' is_police_or_security_officer = ' . $this->sqlIsPoliceOrSecurityOfficer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_qualified_surviving_member = ' . $this->sqlIsQualifiedSurvivingMember() . ','; } elseif( true == array_key_exists( 'IsQualifiedSurvivingMember', $this->getChangedColumns() ) ) { $strSql .= ' is_qualified_surviving_member = ' . $this->sqlIsQualifiedSurvivingMember() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_of_death = ' . $this->sqlDateOfDeath() . ','; } elseif( true == array_key_exists( 'DateOfDeath', $this->getChangedColumns() ) ) { $strSql .= ' date_of_death = ' . $this->sqlDateOfDeath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_subsidy_detail_id' => $this->getCustomerSubsidyDetailId(),
			'customer_id' => $this->getCustomerId(),
			'subsidy_citizenship_type_id' => $this->getSubsidyCitizenshipTypeId(),
			'subsidy_ethnicity_type_id' => $this->getSubsidyEthnicityTypeId(),
			'subsidy_ethnicity_sub_type_id' => $this->getSubsidyEthnicitySubTypeId(),
			'subsidy_ssn_exception_type_id' => $this->getSubsidySsnExceptionTypeId(),
			'subsidy_dependent_type_id' => $this->getSubsidyDependentTypeId(),
			'prior_customer_subsidy_detail_log_id' => $this->getPriorCustomerSubsidyDetailLogId(),
			'post_date' => $this->getPostDate(),
			'apply_through_post_date' => $this->getApplyThroughPostDate(),
			'alien_registration_number' => $this->getAlienRegistrationNumber(),
			'tracs_id_encrypted' => $this->getTracsIdEncrypted(),
			'welfare_rent_amount' => $this->getWelfareRentAmount(),
			'max_welfare_rent_amount' => $this->getMaxWelfareRentAmount(),
			'log_datetime' => $this->getLogDatetime(),
			'is_post_date_ignored' => $this->getIsPostDateIgnored(),
			'is_opening_log' => $this->getIsOpeningLog(),
			'has_declined_to_specify_gender' => $this->getHasDeclinedToSpecifyGender(),
			'is_part_time_student' => $this->getIsPartTimeStudent(),
			'is_police_or_security_officer' => $this->getIsPoliceOrSecurityOfficer(),
			'is_qualified_surviving_member' => $this->getIsQualifiedSurvivingMember(),
			'date_of_death' => $this->getDateOfDeath(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>