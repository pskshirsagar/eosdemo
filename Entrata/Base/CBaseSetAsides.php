<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSetAsides
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSetAsides extends CEosPluralBase {

	/**
	 * @return CSetAside[]
	 */
	public static function fetchSetAsides( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CSetAside', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSetAside
	 */
	public static function fetchSetAside( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSetAside', $objDatabase );
	}

	public static function fetchSetAsideCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'set_asides', $objDatabase );
	}

	public static function fetchSetAsideByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSetAside( sprintf( 'SELECT * FROM set_asides WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSetAsidesByCid( $intCid, $objDatabase ) {
		return self::fetchSetAsides( sprintf( 'SELECT * FROM set_asides WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSetAsidesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSetAsides( sprintf( 'SELECT * FROM set_asides WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSetAsidesByPropertyBuildingIdByCid( $intPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchSetAsides( sprintf( 'SELECT * FROM set_asides WHERE property_building_id = %d AND cid = %d', ( int ) $intPropertyBuildingId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSetAsidesBySubsidySetAsideTypeIdByCid( $intSubsidySetAsideTypeId, $intCid, $objDatabase ) {
		return self::fetchSetAsides( sprintf( 'SELECT * FROM set_asides WHERE subsidy_set_aside_type_id = %d AND cid = %d', ( int ) $intSubsidySetAsideTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSetAsidesBySubsidyIncomeLimitAreaIdByCid( $intSubsidyIncomeLimitAreaId, $intCid, $objDatabase ) {
		return self::fetchSetAsides( sprintf( 'SELECT * FROM set_asides WHERE subsidy_income_limit_area_id = %d AND cid = %d', ( int ) $intSubsidyIncomeLimitAreaId, ( int ) $intCid ), $objDatabase );
	}

}
?>