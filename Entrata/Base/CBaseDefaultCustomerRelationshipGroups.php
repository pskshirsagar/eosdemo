<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultCustomerRelationshipGroups
 * Do not add any new functions to this class.
 */

class CBaseDefaultCustomerRelationshipGroups extends CEosPluralBase {

	/**
	 * @return CDefaultCustomerRelationshipGroup[]
	 */
	public static function fetchDefaultCustomerRelationshipGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultCustomerRelationshipGroup::class, $objDatabase );
	}

	/**
	 * @return CDefaultCustomerRelationshipGroup
	 */
	public static function fetchDefaultCustomerRelationshipGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultCustomerRelationshipGroup::class, $objDatabase );
	}

	public static function fetchDefaultCustomerRelationshipGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_customer_relationship_groups', $objDatabase );
	}

	public static function fetchDefaultCustomerRelationshipGroupById( $intId, $objDatabase ) {
		return self::fetchDefaultCustomerRelationshipGroup( sprintf( 'SELECT * FROM default_customer_relationship_groups WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultCustomerRelationshipGroupsByOccupancyTypeId( $intOccupancyTypeId, $objDatabase ) {
		return self::fetchDefaultCustomerRelationshipGroups( sprintf( 'SELECT * FROM default_customer_relationship_groups WHERE occupancy_type_id = %d', $intOccupancyTypeId ), $objDatabase );
	}

}
?>