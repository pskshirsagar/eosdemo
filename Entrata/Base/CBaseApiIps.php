<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApiIps
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApiIps extends CEosPluralBase {

	/**
	 * @return CApiIp[]
	 */
	public static function fetchApiIps( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApiIp', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApiIp
	 */
	public static function fetchApiIp( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApiIp', $objDatabase );
	}

	public static function fetchApiIpCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'api_ips', $objDatabase );
	}

	public static function fetchApiIpByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApiIp( sprintf( 'SELECT * FROM api_ips WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApiIpsByCid( $intCid, $objDatabase ) {
		return self::fetchApiIps( sprintf( 'SELECT * FROM api_ips WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>