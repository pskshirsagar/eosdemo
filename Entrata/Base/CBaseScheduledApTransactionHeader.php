<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledApTransactionHeader extends CEosSingularBase {

	const TABLE_NAME = 'public.scheduled_ap_transaction_headers';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApPayeeId;
	protected $m_intApPayeeLocationId;
	protected $m_intScheduledPoTypeId;
	protected $m_intBankAccountId;
	protected $m_intFrequencyId;
	protected $m_intApPayeeTermId;
	protected $m_intTemplatePropertyId;
	protected $m_intTemplateCompanyDepartmentId;
	protected $m_intTemplateGlDimensionId;
	protected $m_intTemplateJobPhaseId;
	protected $m_intTemplateContractPhaseId;
	protected $m_intTemplateInterCompanyPostTypeId;
	protected $m_strRemotePrimaryKey;
	protected $m_intFrequencyInterval;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_strLastPostedDate;
	protected $m_strNextPostDate;
	protected $m_strHeaderNote;
	protected $m_intHeaderNumber;
	protected $m_intAutoApproveInvoice;
	protected $m_intAutoApprovePo;
	protected $m_intAutoCreateInvoice;
	protected $m_intAutoCreatePo;
	protected $m_intAutoPostInvoice;
	protected $m_boolTemplateIsConfidential;
	protected $m_intIsOnHold;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['ap_payee_location_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeLocationId', trim( $arrValues['ap_payee_location_id'] ) ); elseif( isset( $arrValues['ap_payee_location_id'] ) ) $this->setApPayeeLocationId( $arrValues['ap_payee_location_id'] );
		if( isset( $arrValues['scheduled_po_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledPoTypeId', trim( $arrValues['scheduled_po_type_id'] ) ); elseif( isset( $arrValues['scheduled_po_type_id'] ) ) $this->setScheduledPoTypeId( $arrValues['scheduled_po_type_id'] );
		if( isset( $arrValues['bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBankAccountId', trim( $arrValues['bank_account_id'] ) ); elseif( isset( $arrValues['bank_account_id'] ) ) $this->setBankAccountId( $arrValues['bank_account_id'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['ap_payee_term_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeTermId', trim( $arrValues['ap_payee_term_id'] ) ); elseif( isset( $arrValues['ap_payee_term_id'] ) ) $this->setApPayeeTermId( $arrValues['ap_payee_term_id'] );
		if( isset( $arrValues['template_property_id'] ) && $boolDirectSet ) $this->set( 'm_intTemplatePropertyId', trim( $arrValues['template_property_id'] ) ); elseif( isset( $arrValues['template_property_id'] ) ) $this->setTemplatePropertyId( $arrValues['template_property_id'] );
		if( isset( $arrValues['template_company_department_id'] ) && $boolDirectSet ) $this->set( 'm_intTemplateCompanyDepartmentId', trim( $arrValues['template_company_department_id'] ) ); elseif( isset( $arrValues['template_company_department_id'] ) ) $this->setTemplateCompanyDepartmentId( $arrValues['template_company_department_id'] );
		if( isset( $arrValues['template_gl_dimension_id'] ) && $boolDirectSet ) $this->set( 'm_intTemplateGlDimensionId', trim( $arrValues['template_gl_dimension_id'] ) ); elseif( isset( $arrValues['template_gl_dimension_id'] ) ) $this->setTemplateGlDimensionId( $arrValues['template_gl_dimension_id'] );
		if( isset( $arrValues['template_job_phase_id'] ) && $boolDirectSet ) $this->set( 'm_intTemplateJobPhaseId', trim( $arrValues['template_job_phase_id'] ) ); elseif( isset( $arrValues['template_job_phase_id'] ) ) $this->setTemplateJobPhaseId( $arrValues['template_job_phase_id'] );
		if( isset( $arrValues['template_contract_phase_id'] ) && $boolDirectSet ) $this->set( 'm_intTemplateContractPhaseId', trim( $arrValues['template_contract_phase_id'] ) ); elseif( isset( $arrValues['template_contract_phase_id'] ) ) $this->setTemplateContractPhaseId( $arrValues['template_contract_phase_id'] );
		if( isset( $arrValues['template_inter_company_post_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTemplateInterCompanyPostTypeId', trim( $arrValues['template_inter_company_post_type_id'] ) ); elseif( isset( $arrValues['template_inter_company_post_type_id'] ) ) $this->setTemplateInterCompanyPostTypeId( $arrValues['template_inter_company_post_type_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['frequency_interval'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyInterval', trim( $arrValues['frequency_interval'] ) ); elseif( isset( $arrValues['frequency_interval'] ) ) $this->setFrequencyInterval( $arrValues['frequency_interval'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['last_posted_date'] ) && $boolDirectSet ) $this->set( 'm_strLastPostedDate', trim( $arrValues['last_posted_date'] ) ); elseif( isset( $arrValues['last_posted_date'] ) ) $this->setLastPostedDate( $arrValues['last_posted_date'] );
		if( isset( $arrValues['next_post_date'] ) && $boolDirectSet ) $this->set( 'm_strNextPostDate', trim( $arrValues['next_post_date'] ) ); elseif( isset( $arrValues['next_post_date'] ) ) $this->setNextPostDate( $arrValues['next_post_date'] );
		if( isset( $arrValues['header_note'] ) && $boolDirectSet ) $this->set( 'm_strHeaderNote', trim( stripcslashes( $arrValues['header_note'] ) ) ); elseif( isset( $arrValues['header_note'] ) ) $this->setHeaderNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['header_note'] ) : $arrValues['header_note'] );
		if( isset( $arrValues['header_number'] ) && $boolDirectSet ) $this->set( 'm_intHeaderNumber', trim( $arrValues['header_number'] ) ); elseif( isset( $arrValues['header_number'] ) ) $this->setHeaderNumber( $arrValues['header_number'] );
		if( isset( $arrValues['auto_approve_invoice'] ) && $boolDirectSet ) $this->set( 'm_intAutoApproveInvoice', trim( $arrValues['auto_approve_invoice'] ) ); elseif( isset( $arrValues['auto_approve_invoice'] ) ) $this->setAutoApproveInvoice( $arrValues['auto_approve_invoice'] );
		if( isset( $arrValues['auto_approve_po'] ) && $boolDirectSet ) $this->set( 'm_intAutoApprovePo', trim( $arrValues['auto_approve_po'] ) ); elseif( isset( $arrValues['auto_approve_po'] ) ) $this->setAutoApprovePo( $arrValues['auto_approve_po'] );
		if( isset( $arrValues['auto_create_invoice'] ) && $boolDirectSet ) $this->set( 'm_intAutoCreateInvoice', trim( $arrValues['auto_create_invoice'] ) ); elseif( isset( $arrValues['auto_create_invoice'] ) ) $this->setAutoCreateInvoice( $arrValues['auto_create_invoice'] );
		if( isset( $arrValues['auto_create_po'] ) && $boolDirectSet ) $this->set( 'm_intAutoCreatePo', trim( $arrValues['auto_create_po'] ) ); elseif( isset( $arrValues['auto_create_po'] ) ) $this->setAutoCreatePo( $arrValues['auto_create_po'] );
		if( isset( $arrValues['auto_post_invoice'] ) && $boolDirectSet ) $this->set( 'm_intAutoPostInvoice', trim( $arrValues['auto_post_invoice'] ) ); elseif( isset( $arrValues['auto_post_invoice'] ) ) $this->setAutoPostInvoice( $arrValues['auto_post_invoice'] );
		if( isset( $arrValues['template_is_confidential'] ) && $boolDirectSet ) $this->set( 'm_boolTemplateIsConfidential', trim( stripcslashes( $arrValues['template_is_confidential'] ) ) ); elseif( isset( $arrValues['template_is_confidential'] ) ) $this->setTemplateIsConfidential( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['template_is_confidential'] ) : $arrValues['template_is_confidential'] );
		if( isset( $arrValues['is_on_hold'] ) && $boolDirectSet ) $this->set( 'm_intIsOnHold', trim( $arrValues['is_on_hold'] ) ); elseif( isset( $arrValues['is_on_hold'] ) ) $this->setIsOnHold( $arrValues['is_on_hold'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setApPayeeLocationId( $intApPayeeLocationId ) {
		$this->set( 'm_intApPayeeLocationId', CStrings::strToIntDef( $intApPayeeLocationId, NULL, false ) );
	}

	public function getApPayeeLocationId() {
		return $this->m_intApPayeeLocationId;
	}

	public function sqlApPayeeLocationId() {
		return ( true == isset( $this->m_intApPayeeLocationId ) ) ? ( string ) $this->m_intApPayeeLocationId : 'NULL';
	}

	public function setScheduledPoTypeId( $intScheduledPoTypeId ) {
		$this->set( 'm_intScheduledPoTypeId', CStrings::strToIntDef( $intScheduledPoTypeId, NULL, false ) );
	}

	public function getScheduledPoTypeId() {
		return $this->m_intScheduledPoTypeId;
	}

	public function sqlScheduledPoTypeId() {
		return ( true == isset( $this->m_intScheduledPoTypeId ) ) ? ( string ) $this->m_intScheduledPoTypeId : 'NULL';
	}

	public function setBankAccountId( $intBankAccountId ) {
		$this->set( 'm_intBankAccountId', CStrings::strToIntDef( $intBankAccountId, NULL, false ) );
	}

	public function getBankAccountId() {
		return $this->m_intBankAccountId;
	}

	public function sqlBankAccountId() {
		return ( true == isset( $this->m_intBankAccountId ) ) ? ( string ) $this->m_intBankAccountId : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : 'NULL';
	}

	public function setApPayeeTermId( $intApPayeeTermId ) {
		$this->set( 'm_intApPayeeTermId', CStrings::strToIntDef( $intApPayeeTermId, NULL, false ) );
	}

	public function getApPayeeTermId() {
		return $this->m_intApPayeeTermId;
	}

	public function sqlApPayeeTermId() {
		return ( true == isset( $this->m_intApPayeeTermId ) ) ? ( string ) $this->m_intApPayeeTermId : 'NULL';
	}

	public function setTemplatePropertyId( $intTemplatePropertyId ) {
		$this->set( 'm_intTemplatePropertyId', CStrings::strToIntDef( $intTemplatePropertyId, NULL, false ) );
	}

	public function getTemplatePropertyId() {
		return $this->m_intTemplatePropertyId;
	}

	public function sqlTemplatePropertyId() {
		return ( true == isset( $this->m_intTemplatePropertyId ) ) ? ( string ) $this->m_intTemplatePropertyId : 'NULL';
	}

	public function setTemplateCompanyDepartmentId( $intTemplateCompanyDepartmentId ) {
		$this->set( 'm_intTemplateCompanyDepartmentId', CStrings::strToIntDef( $intTemplateCompanyDepartmentId, NULL, false ) );
	}

	public function getTemplateCompanyDepartmentId() {
		return $this->m_intTemplateCompanyDepartmentId;
	}

	public function sqlTemplateCompanyDepartmentId() {
		return ( true == isset( $this->m_intTemplateCompanyDepartmentId ) ) ? ( string ) $this->m_intTemplateCompanyDepartmentId : 'NULL';
	}

	public function setTemplateGlDimensionId( $intTemplateGlDimensionId ) {
		$this->set( 'm_intTemplateGlDimensionId', CStrings::strToIntDef( $intTemplateGlDimensionId, NULL, false ) );
	}

	public function getTemplateGlDimensionId() {
		return $this->m_intTemplateGlDimensionId;
	}

	public function sqlTemplateGlDimensionId() {
		return ( true == isset( $this->m_intTemplateGlDimensionId ) ) ? ( string ) $this->m_intTemplateGlDimensionId : 'NULL';
	}

	public function setTemplateJobPhaseId( $intTemplateJobPhaseId ) {
		$this->set( 'm_intTemplateJobPhaseId', CStrings::strToIntDef( $intTemplateJobPhaseId, NULL, false ) );
	}

	public function getTemplateJobPhaseId() {
		return $this->m_intTemplateJobPhaseId;
	}

	public function sqlTemplateJobPhaseId() {
		return ( true == isset( $this->m_intTemplateJobPhaseId ) ) ? ( string ) $this->m_intTemplateJobPhaseId : 'NULL';
	}

	public function setTemplateContractPhaseId( $intTemplateContractPhaseId ) {
		$this->set( 'm_intTemplateContractPhaseId', CStrings::strToIntDef( $intTemplateContractPhaseId, NULL, false ) );
	}

	public function getTemplateContractPhaseId() {
		return $this->m_intTemplateContractPhaseId;
	}

	public function sqlTemplateContractPhaseId() {
		return ( true == isset( $this->m_intTemplateContractPhaseId ) ) ? ( string ) $this->m_intTemplateContractPhaseId : 'NULL';
	}

	public function setTemplateInterCompanyPostTypeId( $intTemplateInterCompanyPostTypeId ) {
		$this->set( 'm_intTemplateInterCompanyPostTypeId', CStrings::strToIntDef( $intTemplateInterCompanyPostTypeId, NULL, false ) );
	}

	public function getTemplateInterCompanyPostTypeId() {
		return $this->m_intTemplateInterCompanyPostTypeId;
	}

	public function sqlTemplateInterCompanyPostTypeId() {
		return ( true == isset( $this->m_intTemplateInterCompanyPostTypeId ) ) ? ( string ) $this->m_intTemplateInterCompanyPostTypeId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setFrequencyInterval( $intFrequencyInterval ) {
		$this->set( 'm_intFrequencyInterval', CStrings::strToIntDef( $intFrequencyInterval, NULL, false ) );
	}

	public function getFrequencyInterval() {
		return $this->m_intFrequencyInterval;
	}

	public function sqlFrequencyInterval() {
		return ( true == isset( $this->m_intFrequencyInterval ) ) ? ( string ) $this->m_intFrequencyInterval : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NOW()';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setLastPostedDate( $strLastPostedDate ) {
		$this->set( 'm_strLastPostedDate', CStrings::strTrimDef( $strLastPostedDate, -1, NULL, true ) );
	}

	public function getLastPostedDate() {
		return $this->m_strLastPostedDate;
	}

	public function sqlLastPostedDate() {
		return ( true == isset( $this->m_strLastPostedDate ) ) ? '\'' . $this->m_strLastPostedDate . '\'' : 'NULL';
	}

	public function setNextPostDate( $strNextPostDate ) {
		$this->set( 'm_strNextPostDate', CStrings::strTrimDef( $strNextPostDate, -1, NULL, true ) );
	}

	public function getNextPostDate() {
		return $this->m_strNextPostDate;
	}

	public function sqlNextPostDate() {
		return ( true == isset( $this->m_strNextPostDate ) ) ? '\'' . $this->m_strNextPostDate . '\'' : 'NULL';
	}

	public function setHeaderNote( $strHeaderNote ) {
		$this->set( 'm_strHeaderNote', CStrings::strTrimDef( $strHeaderNote, 2000, NULL, true ) );
	}

	public function getHeaderNote() {
		return $this->m_strHeaderNote;
	}

	public function sqlHeaderNote() {
		return ( true == isset( $this->m_strHeaderNote ) ) ? '\'' . addslashes( $this->m_strHeaderNote ) . '\'' : 'NULL';
	}

	public function setHeaderNumber( $intHeaderNumber ) {
		$this->set( 'm_intHeaderNumber', CStrings::strToIntDef( $intHeaderNumber, NULL, false ) );
	}

	public function getHeaderNumber() {
		return $this->m_intHeaderNumber;
	}

	public function sqlHeaderNumber() {
		return ( true == isset( $this->m_intHeaderNumber ) ) ? ( string ) $this->m_intHeaderNumber : 'NULL';
	}

	public function setAutoApproveInvoice( $intAutoApproveInvoice ) {
		$this->set( 'm_intAutoApproveInvoice', CStrings::strToIntDef( $intAutoApproveInvoice, NULL, false ) );
	}

	public function getAutoApproveInvoice() {
		return $this->m_intAutoApproveInvoice;
	}

	public function sqlAutoApproveInvoice() {
		return ( true == isset( $this->m_intAutoApproveInvoice ) ) ? ( string ) $this->m_intAutoApproveInvoice : 'NULL';
	}

	public function setAutoApprovePo( $intAutoApprovePo ) {
		$this->set( 'm_intAutoApprovePo', CStrings::strToIntDef( $intAutoApprovePo, NULL, false ) );
	}

	public function getAutoApprovePo() {
		return $this->m_intAutoApprovePo;
	}

	public function sqlAutoApprovePo() {
		return ( true == isset( $this->m_intAutoApprovePo ) ) ? ( string ) $this->m_intAutoApprovePo : 'NULL';
	}

	public function setAutoCreateInvoice( $intAutoCreateInvoice ) {
		$this->set( 'm_intAutoCreateInvoice', CStrings::strToIntDef( $intAutoCreateInvoice, NULL, false ) );
	}

	public function getAutoCreateInvoice() {
		return $this->m_intAutoCreateInvoice;
	}

	public function sqlAutoCreateInvoice() {
		return ( true == isset( $this->m_intAutoCreateInvoice ) ) ? ( string ) $this->m_intAutoCreateInvoice : 'NULL';
	}

	public function setAutoCreatePo( $intAutoCreatePo ) {
		$this->set( 'm_intAutoCreatePo', CStrings::strToIntDef( $intAutoCreatePo, NULL, false ) );
	}

	public function getAutoCreatePo() {
		return $this->m_intAutoCreatePo;
	}

	public function sqlAutoCreatePo() {
		return ( true == isset( $this->m_intAutoCreatePo ) ) ? ( string ) $this->m_intAutoCreatePo : 'NULL';
	}

	public function setAutoPostInvoice( $intAutoPostInvoice ) {
		$this->set( 'm_intAutoPostInvoice', CStrings::strToIntDef( $intAutoPostInvoice, NULL, false ) );
	}

	public function getAutoPostInvoice() {
		return $this->m_intAutoPostInvoice;
	}

	public function sqlAutoPostInvoice() {
		return ( true == isset( $this->m_intAutoPostInvoice ) ) ? ( string ) $this->m_intAutoPostInvoice : 'NULL';
	}

	public function setTemplateIsConfidential( $boolTemplateIsConfidential ) {
		$this->set( 'm_boolTemplateIsConfidential', CStrings::strToBool( $boolTemplateIsConfidential ) );
	}

	public function getTemplateIsConfidential() {
		return $this->m_boolTemplateIsConfidential;
	}

	public function sqlTemplateIsConfidential() {
		return ( true == isset( $this->m_boolTemplateIsConfidential ) ) ? '\'' . ( true == ( bool ) $this->m_boolTemplateIsConfidential ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsOnHold( $intIsOnHold ) {
		$this->set( 'm_intIsOnHold', CStrings::strToIntDef( $intIsOnHold, NULL, false ) );
	}

	public function getIsOnHold() {
		return $this->m_intIsOnHold;
	}

	public function sqlIsOnHold() {
		return ( true == isset( $this->m_intIsOnHold ) ) ? ( string ) $this->m_intIsOnHold : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ap_payee_id, ap_payee_location_id, scheduled_po_type_id, bank_account_id, frequency_id, ap_payee_term_id, template_property_id, template_company_department_id, template_gl_dimension_id, template_job_phase_id, template_contract_phase_id, template_inter_company_post_type_id, remote_primary_key, frequency_interval, start_date, end_date, last_posted_date, next_post_date, header_note, header_number, auto_approve_invoice, auto_approve_po, auto_create_invoice, auto_create_po, auto_post_invoice, template_is_confidential, is_on_hold, approved_by, approved_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlApPayeeId() . ', ' .
 						$this->sqlApPayeeLocationId() . ', ' .
 						$this->sqlScheduledPoTypeId() . ', ' .
 						$this->sqlBankAccountId() . ', ' .
 						$this->sqlFrequencyId() . ', ' .
 						$this->sqlApPayeeTermId() . ', ' .
 						$this->sqlTemplatePropertyId() . ', ' .
 						$this->sqlTemplateCompanyDepartmentId() . ', ' .
 						$this->sqlTemplateGlDimensionId() . ', ' .
 						$this->sqlTemplateJobPhaseId() . ', ' .
 						$this->sqlTemplateContractPhaseId() . ', ' .
 						$this->sqlTemplateInterCompanyPostTypeId() . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlFrequencyInterval() . ', ' .
 						$this->sqlStartDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlLastPostedDate() . ', ' .
 						$this->sqlNextPostDate() . ', ' .
 						$this->sqlHeaderNote() . ', ' .
 						$this->sqlHeaderNumber() . ', ' .
 						$this->sqlAutoApproveInvoice() . ', ' .
 						$this->sqlAutoApprovePo() . ', ' .
 						$this->sqlAutoCreateInvoice() . ', ' .
 						$this->sqlAutoCreatePo() . ', ' .
 						$this->sqlAutoPostInvoice() . ', ' .
 						$this->sqlTemplateIsConfidential() . ', ' .
 						$this->sqlIsOnHold() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId() . ','; } elseif( true == array_key_exists( 'ApPayeeLocationId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_po_type_id = ' . $this->sqlScheduledPoTypeId() . ','; } elseif( true == array_key_exists( 'ScheduledPoTypeId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_po_type_id = ' . $this->sqlScheduledPoTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_account_id = ' . $this->sqlBankAccountId() . ','; } elseif( true == array_key_exists( 'BankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' bank_account_id = ' . $this->sqlBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_term_id = ' . $this->sqlApPayeeTermId() . ','; } elseif( true == array_key_exists( 'ApPayeeTermId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_term_id = ' . $this->sqlApPayeeTermId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_property_id = ' . $this->sqlTemplatePropertyId() . ','; } elseif( true == array_key_exists( 'TemplatePropertyId', $this->getChangedColumns() ) ) { $strSql .= ' template_property_id = ' . $this->sqlTemplatePropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_company_department_id = ' . $this->sqlTemplateCompanyDepartmentId() . ','; } elseif( true == array_key_exists( 'TemplateCompanyDepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' template_company_department_id = ' . $this->sqlTemplateCompanyDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_gl_dimension_id = ' . $this->sqlTemplateGlDimensionId() . ','; } elseif( true == array_key_exists( 'TemplateGlDimensionId', $this->getChangedColumns() ) ) { $strSql .= ' template_gl_dimension_id = ' . $this->sqlTemplateGlDimensionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_job_phase_id = ' . $this->sqlTemplateJobPhaseId() . ','; } elseif( true == array_key_exists( 'TemplateJobPhaseId', $this->getChangedColumns() ) ) { $strSql .= ' template_job_phase_id = ' . $this->sqlTemplateJobPhaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_contract_phase_id = ' . $this->sqlTemplateContractPhaseId() . ','; } elseif( true == array_key_exists( 'TemplateContractPhaseId', $this->getChangedColumns() ) ) { $strSql .= ' template_contract_phase_id = ' . $this->sqlTemplateContractPhaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_inter_company_post_type_id = ' . $this->sqlTemplateInterCompanyPostTypeId() . ','; } elseif( true == array_key_exists( 'TemplateInterCompanyPostTypeId', $this->getChangedColumns() ) ) { $strSql .= ' template_inter_company_post_type_id = ' . $this->sqlTemplateInterCompanyPostTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_interval = ' . $this->sqlFrequencyInterval() . ','; } elseif( true == array_key_exists( 'FrequencyInterval', $this->getChangedColumns() ) ) { $strSql .= ' frequency_interval = ' . $this->sqlFrequencyInterval() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_posted_date = ' . $this->sqlLastPostedDate() . ','; } elseif( true == array_key_exists( 'LastPostedDate', $this->getChangedColumns() ) ) { $strSql .= ' last_posted_date = ' . $this->sqlLastPostedDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' next_post_date = ' . $this->sqlNextPostDate() . ','; } elseif( true == array_key_exists( 'NextPostDate', $this->getChangedColumns() ) ) { $strSql .= ' next_post_date = ' . $this->sqlNextPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_note = ' . $this->sqlHeaderNote() . ','; } elseif( true == array_key_exists( 'HeaderNote', $this->getChangedColumns() ) ) { $strSql .= ' header_note = ' . $this->sqlHeaderNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_number = ' . $this->sqlHeaderNumber() . ','; } elseif( true == array_key_exists( 'HeaderNumber', $this->getChangedColumns() ) ) { $strSql .= ' header_number = ' . $this->sqlHeaderNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_approve_invoice = ' . $this->sqlAutoApproveInvoice() . ','; } elseif( true == array_key_exists( 'AutoApproveInvoice', $this->getChangedColumns() ) ) { $strSql .= ' auto_approve_invoice = ' . $this->sqlAutoApproveInvoice() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_approve_po = ' . $this->sqlAutoApprovePo() . ','; } elseif( true == array_key_exists( 'AutoApprovePo', $this->getChangedColumns() ) ) { $strSql .= ' auto_approve_po = ' . $this->sqlAutoApprovePo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_create_invoice = ' . $this->sqlAutoCreateInvoice() . ','; } elseif( true == array_key_exists( 'AutoCreateInvoice', $this->getChangedColumns() ) ) { $strSql .= ' auto_create_invoice = ' . $this->sqlAutoCreateInvoice() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_create_po = ' . $this->sqlAutoCreatePo() . ','; } elseif( true == array_key_exists( 'AutoCreatePo', $this->getChangedColumns() ) ) { $strSql .= ' auto_create_po = ' . $this->sqlAutoCreatePo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_post_invoice = ' . $this->sqlAutoPostInvoice() . ','; } elseif( true == array_key_exists( 'AutoPostInvoice', $this->getChangedColumns() ) ) { $strSql .= ' auto_post_invoice = ' . $this->sqlAutoPostInvoice() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_is_confidential = ' . $this->sqlTemplateIsConfidential() . ','; } elseif( true == array_key_exists( 'TemplateIsConfidential', $this->getChangedColumns() ) ) { $strSql .= ' template_is_confidential = ' . $this->sqlTemplateIsConfidential() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_on_hold = ' . $this->sqlIsOnHold() . ','; } elseif( true == array_key_exists( 'IsOnHold', $this->getChangedColumns() ) ) { $strSql .= ' is_on_hold = ' . $this->sqlIsOnHold() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ap_payee_id' => $this->getApPayeeId(),
			'ap_payee_location_id' => $this->getApPayeeLocationId(),
			'scheduled_po_type_id' => $this->getScheduledPoTypeId(),
			'bank_account_id' => $this->getBankAccountId(),
			'frequency_id' => $this->getFrequencyId(),
			'ap_payee_term_id' => $this->getApPayeeTermId(),
			'template_property_id' => $this->getTemplatePropertyId(),
			'template_company_department_id' => $this->getTemplateCompanyDepartmentId(),
			'template_gl_dimension_id' => $this->getTemplateGlDimensionId(),
			'template_job_phase_id' => $this->getTemplateJobPhaseId(),
			'template_contract_phase_id' => $this->getTemplateContractPhaseId(),
			'template_inter_company_post_type_id' => $this->getTemplateInterCompanyPostTypeId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'frequency_interval' => $this->getFrequencyInterval(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'last_posted_date' => $this->getLastPostedDate(),
			'next_post_date' => $this->getNextPostDate(),
			'header_note' => $this->getHeaderNote(),
			'header_number' => $this->getHeaderNumber(),
			'auto_approve_invoice' => $this->getAutoApproveInvoice(),
			'auto_approve_po' => $this->getAutoApprovePo(),
			'auto_create_invoice' => $this->getAutoCreateInvoice(),
			'auto_create_po' => $this->getAutoCreatePo(),
			'auto_post_invoice' => $this->getAutoPostInvoice(),
			'template_is_confidential' => $this->getTemplateIsConfidential(),
			'is_on_hold' => $this->getIsOnHold(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>