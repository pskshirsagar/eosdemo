<?php

class CBaseDefaultMaintenanceTemplate extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_maintenance_templates';

	protected $m_intId;
	protected $m_intDefaultMaintenanceTemplateId;
	protected $m_intDefaultMaintenancePriorityId;
	protected $m_intDefaultMaintenanceStatusId;
	protected $m_intDefaultMaintenanceLocationId;
	protected $m_intDefaultMaintenanceProblemId;
	protected $m_intMaintenanceRequestTypeId;
	protected $m_strSystemCode;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strProblemDescription;
	protected $m_strLocationSpecifics;
	protected $m_strAdditionalInfo;
	protected $m_intEstimatedHours;
	protected $m_fltCostPerHour;
	protected $m_strPartsDescription;
	protected $m_fltPartsCost;
	protected $m_strEquipmentDescription;
	protected $m_fltEquipmentCost;
	protected $m_fltAdditionalCost;
	protected $m_strIsBillable;
	protected $m_strIsSystem;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_strIsSystem = NULL;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['default_maintenance_template_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultMaintenanceTemplateId', trim( $arrValues['default_maintenance_template_id'] ) ); elseif( isset( $arrValues['default_maintenance_template_id'] ) ) $this->setDefaultMaintenanceTemplateId( $arrValues['default_maintenance_template_id'] );
		if( isset( $arrValues['default_maintenance_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultMaintenancePriorityId', trim( $arrValues['default_maintenance_priority_id'] ) ); elseif( isset( $arrValues['default_maintenance_priority_id'] ) ) $this->setDefaultMaintenancePriorityId( $arrValues['default_maintenance_priority_id'] );
		if( isset( $arrValues['default_maintenance_status_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultMaintenanceStatusId', trim( $arrValues['default_maintenance_status_id'] ) ); elseif( isset( $arrValues['default_maintenance_status_id'] ) ) $this->setDefaultMaintenanceStatusId( $arrValues['default_maintenance_status_id'] );
		if( isset( $arrValues['default_maintenance_location_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultMaintenanceLocationId', trim( $arrValues['default_maintenance_location_id'] ) ); elseif( isset( $arrValues['default_maintenance_location_id'] ) ) $this->setDefaultMaintenanceLocationId( $arrValues['default_maintenance_location_id'] );
		if( isset( $arrValues['default_maintenance_problem_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultMaintenanceProblemId', trim( $arrValues['default_maintenance_problem_id'] ) ); elseif( isset( $arrValues['default_maintenance_problem_id'] ) ) $this->setDefaultMaintenanceProblemId( $arrValues['default_maintenance_problem_id'] );
		if( isset( $arrValues['maintenance_request_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceRequestTypeId', trim( $arrValues['maintenance_request_type_id'] ) ); elseif( isset( $arrValues['maintenance_request_type_id'] ) ) $this->setMaintenanceRequestTypeId( $arrValues['maintenance_request_type_id'] );
		if( isset( $arrValues['system_code'] ) && $boolDirectSet ) $this->set( 'm_strSystemCode', trim( stripcslashes( $arrValues['system_code'] ) ) ); elseif( isset( $arrValues['system_code'] ) ) $this->setSystemCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['system_code'] ) : $arrValues['system_code'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['problem_description'] ) && $boolDirectSet ) $this->set( 'm_strProblemDescription', trim( stripcslashes( $arrValues['problem_description'] ) ) ); elseif( isset( $arrValues['problem_description'] ) ) $this->setProblemDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['problem_description'] ) : $arrValues['problem_description'] );
		if( isset( $arrValues['location_specifics'] ) && $boolDirectSet ) $this->set( 'm_strLocationSpecifics', trim( stripcslashes( $arrValues['location_specifics'] ) ) ); elseif( isset( $arrValues['location_specifics'] ) ) $this->setLocationSpecifics( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['location_specifics'] ) : $arrValues['location_specifics'] );
		if( isset( $arrValues['additional_info'] ) && $boolDirectSet ) $this->set( 'm_strAdditionalInfo', trim( stripcslashes( $arrValues['additional_info'] ) ) ); elseif( isset( $arrValues['additional_info'] ) ) $this->setAdditionalInfo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['additional_info'] ) : $arrValues['additional_info'] );
		if( isset( $arrValues['estimated_hours'] ) && $boolDirectSet ) $this->set( 'm_intEstimatedHours', trim( $arrValues['estimated_hours'] ) ); elseif( isset( $arrValues['estimated_hours'] ) ) $this->setEstimatedHours( $arrValues['estimated_hours'] );
		if( isset( $arrValues['cost_per_hour'] ) && $boolDirectSet ) $this->set( 'm_fltCostPerHour', trim( $arrValues['cost_per_hour'] ) ); elseif( isset( $arrValues['cost_per_hour'] ) ) $this->setCostPerHour( $arrValues['cost_per_hour'] );
		if( isset( $arrValues['parts_description'] ) && $boolDirectSet ) $this->set( 'm_strPartsDescription', trim( stripcslashes( $arrValues['parts_description'] ) ) ); elseif( isset( $arrValues['parts_description'] ) ) $this->setPartsDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['parts_description'] ) : $arrValues['parts_description'] );
		if( isset( $arrValues['parts_cost'] ) && $boolDirectSet ) $this->set( 'm_fltPartsCost', trim( $arrValues['parts_cost'] ) ); elseif( isset( $arrValues['parts_cost'] ) ) $this->setPartsCost( $arrValues['parts_cost'] );
		if( isset( $arrValues['equipment_description'] ) && $boolDirectSet ) $this->set( 'm_strEquipmentDescription', trim( stripcslashes( $arrValues['equipment_description'] ) ) ); elseif( isset( $arrValues['equipment_description'] ) ) $this->setEquipmentDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['equipment_description'] ) : $arrValues['equipment_description'] );
		if( isset( $arrValues['equipment_cost'] ) && $boolDirectSet ) $this->set( 'm_fltEquipmentCost', trim( $arrValues['equipment_cost'] ) ); elseif( isset( $arrValues['equipment_cost'] ) ) $this->setEquipmentCost( $arrValues['equipment_cost'] );
		if( isset( $arrValues['additional_cost'] ) && $boolDirectSet ) $this->set( 'm_fltAdditionalCost', trim( $arrValues['additional_cost'] ) ); elseif( isset( $arrValues['additional_cost'] ) ) $this->setAdditionalCost( $arrValues['additional_cost'] );
		if( isset( $arrValues['is_billable'] ) && $boolDirectSet ) $this->set( 'm_strIsBillable', trim( $arrValues['is_billable'] ) ); elseif( isset( $arrValues['is_billable'] ) ) $this->setIsBillable( $arrValues['is_billable'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_strIsSystem', trim( stripcslashes( $arrValues['is_system'] ) ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_system'] ) : $arrValues['is_system'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDefaultMaintenanceTemplateId( $intDefaultMaintenanceTemplateId ) {
		$this->set( 'm_intDefaultMaintenanceTemplateId', CStrings::strToIntDef( $intDefaultMaintenanceTemplateId, NULL, false ) );
	}

	public function getDefaultMaintenanceTemplateId() {
		return $this->m_intDefaultMaintenanceTemplateId;
	}

	public function sqlDefaultMaintenanceTemplateId() {
		return ( true == isset( $this->m_intDefaultMaintenanceTemplateId ) ) ? ( string ) $this->m_intDefaultMaintenanceTemplateId : 'NULL';
	}

	public function setDefaultMaintenancePriorityId( $intDefaultMaintenancePriorityId ) {
		$this->set( 'm_intDefaultMaintenancePriorityId', CStrings::strToIntDef( $intDefaultMaintenancePriorityId, NULL, false ) );
	}

	public function getDefaultMaintenancePriorityId() {
		return $this->m_intDefaultMaintenancePriorityId;
	}

	public function sqlDefaultMaintenancePriorityId() {
		return ( true == isset( $this->m_intDefaultMaintenancePriorityId ) ) ? ( string ) $this->m_intDefaultMaintenancePriorityId : 'NULL';
	}

	public function setDefaultMaintenanceStatusId( $intDefaultMaintenanceStatusId ) {
		$this->set( 'm_intDefaultMaintenanceStatusId', CStrings::strToIntDef( $intDefaultMaintenanceStatusId, NULL, false ) );
	}

	public function getDefaultMaintenanceStatusId() {
		return $this->m_intDefaultMaintenanceStatusId;
	}

	public function sqlDefaultMaintenanceStatusId() {
		return ( true == isset( $this->m_intDefaultMaintenanceStatusId ) ) ? ( string ) $this->m_intDefaultMaintenanceStatusId : 'NULL';
	}

	public function setDefaultMaintenanceLocationId( $intDefaultMaintenanceLocationId ) {
		$this->set( 'm_intDefaultMaintenanceLocationId', CStrings::strToIntDef( $intDefaultMaintenanceLocationId, NULL, false ) );
	}

	public function getDefaultMaintenanceLocationId() {
		return $this->m_intDefaultMaintenanceLocationId;
	}

	public function sqlDefaultMaintenanceLocationId() {
		return ( true == isset( $this->m_intDefaultMaintenanceLocationId ) ) ? ( string ) $this->m_intDefaultMaintenanceLocationId : 'NULL';
	}

	public function setDefaultMaintenanceProblemId( $intDefaultMaintenanceProblemId ) {
		$this->set( 'm_intDefaultMaintenanceProblemId', CStrings::strToIntDef( $intDefaultMaintenanceProblemId, NULL, false ) );
	}

	public function getDefaultMaintenanceProblemId() {
		return $this->m_intDefaultMaintenanceProblemId;
	}

	public function sqlDefaultMaintenanceProblemId() {
		return ( true == isset( $this->m_intDefaultMaintenanceProblemId ) ) ? ( string ) $this->m_intDefaultMaintenanceProblemId : 'NULL';
	}

	public function setMaintenanceRequestTypeId( $intMaintenanceRequestTypeId ) {
		$this->set( 'm_intMaintenanceRequestTypeId', CStrings::strToIntDef( $intMaintenanceRequestTypeId, NULL, false ) );
	}

	public function getMaintenanceRequestTypeId() {
		return $this->m_intMaintenanceRequestTypeId;
	}

	public function sqlMaintenanceRequestTypeId() {
		return ( true == isset( $this->m_intMaintenanceRequestTypeId ) ) ? ( string ) $this->m_intMaintenanceRequestTypeId : 'NULL';
	}

	public function setSystemCode( $strSystemCode ) {
		$this->set( 'm_strSystemCode', CStrings::strTrimDef( $strSystemCode, 10, NULL, true ) );
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	public function sqlSystemCode() {
		return ( true == isset( $this->m_strSystemCode ) ) ? '\'' . addslashes( $this->m_strSystemCode ) . '\'' : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setProblemDescription( $strProblemDescription ) {
		$this->set( 'm_strProblemDescription', CStrings::strTrimDef( $strProblemDescription, -1, NULL, true ) );
	}

	public function getProblemDescription() {
		return $this->m_strProblemDescription;
	}

	public function sqlProblemDescription() {
		return ( true == isset( $this->m_strProblemDescription ) ) ? '\'' . addslashes( $this->m_strProblemDescription ) . '\'' : 'NULL';
	}

	public function setLocationSpecifics( $strLocationSpecifics ) {
		$this->set( 'm_strLocationSpecifics', CStrings::strTrimDef( $strLocationSpecifics, -1, NULL, true ) );
	}

	public function getLocationSpecifics() {
		return $this->m_strLocationSpecifics;
	}

	public function sqlLocationSpecifics() {
		return ( true == isset( $this->m_strLocationSpecifics ) ) ? '\'' . addslashes( $this->m_strLocationSpecifics ) . '\'' : 'NULL';
	}

	public function setAdditionalInfo( $strAdditionalInfo ) {
		$this->set( 'm_strAdditionalInfo', CStrings::strTrimDef( $strAdditionalInfo, -1, NULL, true ) );
	}

	public function getAdditionalInfo() {
		return $this->m_strAdditionalInfo;
	}

	public function sqlAdditionalInfo() {
		return ( true == isset( $this->m_strAdditionalInfo ) ) ? '\'' . addslashes( $this->m_strAdditionalInfo ) . '\'' : 'NULL';
	}

	public function setEstimatedHours( $intEstimatedHours ) {
		$this->set( 'm_intEstimatedHours', CStrings::strToIntDef( $intEstimatedHours, NULL, false ) );
	}

	public function getEstimatedHours() {
		return $this->m_intEstimatedHours;
	}

	public function sqlEstimatedHours() {
		return ( true == isset( $this->m_intEstimatedHours ) ) ? ( string ) $this->m_intEstimatedHours : 'NULL';
	}

	public function setCostPerHour( $fltCostPerHour ) {
		$this->set( 'm_fltCostPerHour', CStrings::strToFloatDef( $fltCostPerHour, NULL, false, 4 ) );
	}

	public function getCostPerHour() {
		return $this->m_fltCostPerHour;
	}

	public function sqlCostPerHour() {
		return ( true == isset( $this->m_fltCostPerHour ) ) ? ( string ) $this->m_fltCostPerHour : 'NULL';
	}

	public function setPartsDescription( $strPartsDescription ) {
		$this->set( 'm_strPartsDescription', CStrings::strTrimDef( $strPartsDescription, 240, NULL, true ) );
	}

	public function getPartsDescription() {
		return $this->m_strPartsDescription;
	}

	public function sqlPartsDescription() {
		return ( true == isset( $this->m_strPartsDescription ) ) ? '\'' . addslashes( $this->m_strPartsDescription ) . '\'' : 'NULL';
	}

	public function setPartsCost( $fltPartsCost ) {
		$this->set( 'm_fltPartsCost', CStrings::strToFloatDef( $fltPartsCost, NULL, false, 4 ) );
	}

	public function getPartsCost() {
		return $this->m_fltPartsCost;
	}

	public function sqlPartsCost() {
		return ( true == isset( $this->m_fltPartsCost ) ) ? ( string ) $this->m_fltPartsCost : 'NULL';
	}

	public function setEquipmentDescription( $strEquipmentDescription ) {
		$this->set( 'm_strEquipmentDescription', CStrings::strTrimDef( $strEquipmentDescription, 240, NULL, true ) );
	}

	public function getEquipmentDescription() {
		return $this->m_strEquipmentDescription;
	}

	public function sqlEquipmentDescription() {
		return ( true == isset( $this->m_strEquipmentDescription ) ) ? '\'' . addslashes( $this->m_strEquipmentDescription ) . '\'' : 'NULL';
	}

	public function setEquipmentCost( $fltEquipmentCost ) {
		$this->set( 'm_fltEquipmentCost', CStrings::strToFloatDef( $fltEquipmentCost, NULL, false, 4 ) );
	}

	public function getEquipmentCost() {
		return $this->m_fltEquipmentCost;
	}

	public function sqlEquipmentCost() {
		return ( true == isset( $this->m_fltEquipmentCost ) ) ? ( string ) $this->m_fltEquipmentCost : 'NULL';
	}

	public function setAdditionalCost( $fltAdditionalCost ) {
		$this->set( 'm_fltAdditionalCost', CStrings::strToFloatDef( $fltAdditionalCost, NULL, false, 4 ) );
	}

	public function getAdditionalCost() {
		return $this->m_fltAdditionalCost;
	}

	public function sqlAdditionalCost() {
		return ( true == isset( $this->m_fltAdditionalCost ) ) ? ( string ) $this->m_fltAdditionalCost : 'NULL';
	}

	public function setIsBillable( $strIsBillable ) {
		$this->set( 'm_strIsBillable', CStrings::strTrimDef( $strIsBillable, -1, NULL, true ) );
	}

	public function getIsBillable() {
		return $this->m_strIsBillable;
	}

	public function sqlIsBillable() {
		return ( true == isset( $this->m_strIsBillable ) ) ? '\'' . $this->m_strIsBillable . '\'' : 'NULL';
	}

	public function setIsSystem( $strIsSystem ) {
		$this->set( 'm_strIsSystem', CStrings::strTrimDef( $strIsSystem, 10, NULL, true ) );
	}

	public function getIsSystem() {
		return $this->m_strIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_strIsSystem ) ) ? '\'' . addslashes( $this->m_strIsSystem ) . '\'' : '\'NULL\'';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'default_maintenance_template_id' => $this->getDefaultMaintenanceTemplateId(),
			'default_maintenance_priority_id' => $this->getDefaultMaintenancePriorityId(),
			'default_maintenance_status_id' => $this->getDefaultMaintenanceStatusId(),
			'default_maintenance_location_id' => $this->getDefaultMaintenanceLocationId(),
			'default_maintenance_problem_id' => $this->getDefaultMaintenanceProblemId(),
			'maintenance_request_type_id' => $this->getMaintenanceRequestTypeId(),
			'system_code' => $this->getSystemCode(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'problem_description' => $this->getProblemDescription(),
			'location_specifics' => $this->getLocationSpecifics(),
			'additional_info' => $this->getAdditionalInfo(),
			'estimated_hours' => $this->getEstimatedHours(),
			'cost_per_hour' => $this->getCostPerHour(),
			'parts_description' => $this->getPartsDescription(),
			'parts_cost' => $this->getPartsCost(),
			'equipment_description' => $this->getEquipmentDescription(),
			'equipment_cost' => $this->getEquipmentCost(),
			'additional_cost' => $this->getAdditionalCost(),
			'is_billable' => $this->getIsBillable(),
			'is_system' => $this->getIsSystem(),
			'details' => $this->getDetails()
		);
	}

}
?>