<?php

class CBaseConfidentialityTypes extends CEosPluralBase {

    public static function fetchConfidentialityTypes( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CConfidentialityType', $objDatabase );
    }

    public static function fetchConfidentialityType( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CConfidentialityType', $objDatabase );
    }

    public static function fetchConfidentialityTypeCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'confidentiality_types', $objDatabase );
    }

    public static function fetchConfidentialityTypeById( $intId, $objDatabase ) {
        return self::fetchConfidentialityType( sprintf( 'SELECT * FROM confidentiality_types WHERE id = %d', (int) $intId ), $objDatabase );
    }

}
?>