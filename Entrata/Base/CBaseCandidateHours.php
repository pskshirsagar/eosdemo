<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCandidateHours
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCandidateHours extends CEosPluralBase {

	/**
	 * @return CCandidateHour[]
	 */
	public static function fetchCandidateHours( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCandidateHour', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCandidateHour
	 */
	public static function fetchCandidateHour( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCandidateHour', $objDatabase );
	}

	public static function fetchCandidateHourCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'candidate_hours', $objDatabase );
	}

	public static function fetchCandidateHourByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCandidateHour( sprintf( 'SELECT * FROM candidate_hours WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCandidateHoursByCid( $intCid, $objDatabase ) {
		return self::fetchCandidateHours( sprintf( 'SELECT * FROM candidate_hours WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCandidateHoursByCandidateIdByCid( $intCandidateId, $intCid, $objDatabase ) {
		return self::fetchCandidateHours( sprintf( 'SELECT * FROM candidate_hours WHERE candidate_id = %d AND cid = %d', ( int ) $intCandidateId, ( int ) $intCid ), $objDatabase );
	}

}
?>