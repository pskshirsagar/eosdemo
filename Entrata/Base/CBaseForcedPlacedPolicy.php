<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseForcedPlacedPolicy extends CEosSingularBase {

	const TABLE_NAME = 'public.forced_placed_policies';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCustomerId;
	protected $m_intLeaseId;
	protected $m_intResidentInsurancePolicyId;
	protected $m_intArTransactionId;
	protected $m_strArTransactionRemotePrimaryKey;
	protected $m_strArTransactionExportedOn;
	protected $m_strUninsuredOn;
	protected $m_strReinsuredOn;
	protected $m_strPolicyEffectiveDate;
	protected $m_strPolicyEndDate;
	protected $m_strFirstNoticeSentOn;
	protected $m_strExportedOn;
	protected $m_strEnrolledOn;
	protected $m_strPostponedOn;
	protected $m_intPostponedBy;
	protected $m_boolIsDisabled;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strChargePostDate;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsDisabled = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['resident_insurance_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intResidentInsurancePolicyId', trim( $arrValues['resident_insurance_policy_id'] ) ); elseif( isset( $arrValues['resident_insurance_policy_id'] ) ) $this->setResidentInsurancePolicyId( $arrValues['resident_insurance_policy_id'] );
		if( isset( $arrValues['ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intArTransactionId', trim( $arrValues['ar_transaction_id'] ) ); elseif( isset( $arrValues['ar_transaction_id'] ) ) $this->setArTransactionId( $arrValues['ar_transaction_id'] );
		if( isset( $arrValues['ar_transaction_remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strArTransactionRemotePrimaryKey', trim( $arrValues['ar_transaction_remote_primary_key'] ) ); elseif( isset( $arrValues['ar_transaction_remote_primary_key'] ) ) $this->setArTransactionRemotePrimaryKey( $arrValues['ar_transaction_remote_primary_key'] );
		if( isset( $arrValues['ar_transaction_exported_on'] ) && $boolDirectSet ) $this->set( 'm_strArTransactionExportedOn', trim( $arrValues['ar_transaction_exported_on'] ) ); elseif( isset( $arrValues['ar_transaction_exported_on'] ) ) $this->setArTransactionExportedOn( $arrValues['ar_transaction_exported_on'] );
		if( isset( $arrValues['uninsured_on'] ) && $boolDirectSet ) $this->set( 'm_strUninsuredOn', trim( $arrValues['uninsured_on'] ) ); elseif( isset( $arrValues['uninsured_on'] ) ) $this->setUninsuredOn( $arrValues['uninsured_on'] );
		if( isset( $arrValues['reinsured_on'] ) && $boolDirectSet ) $this->set( 'm_strReinsuredOn', trim( $arrValues['reinsured_on'] ) ); elseif( isset( $arrValues['reinsured_on'] ) ) $this->setReinsuredOn( $arrValues['reinsured_on'] );
		if( isset( $arrValues['policy_effective_date'] ) && $boolDirectSet ) $this->set( 'm_strPolicyEffectiveDate', trim( $arrValues['policy_effective_date'] ) ); elseif( isset( $arrValues['policy_effective_date'] ) ) $this->setPolicyEffectiveDate( $arrValues['policy_effective_date'] );
		if( isset( $arrValues['policy_end_date'] ) && $boolDirectSet ) $this->set( 'm_strPolicyEndDate', trim( $arrValues['policy_end_date'] ) ); elseif( isset( $arrValues['policy_end_date'] ) ) $this->setPolicyEndDate( $arrValues['policy_end_date'] );
		if( isset( $arrValues['first_notice_sent_on'] ) && $boolDirectSet ) $this->set( 'm_strFirstNoticeSentOn', trim( $arrValues['first_notice_sent_on'] ) ); elseif( isset( $arrValues['first_notice_sent_on'] ) ) $this->setFirstNoticeSentOn( $arrValues['first_notice_sent_on'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['enrolled_on'] ) && $boolDirectSet ) $this->set( 'm_strEnrolledOn', trim( $arrValues['enrolled_on'] ) ); elseif( isset( $arrValues['enrolled_on'] ) ) $this->setEnrolledOn( $arrValues['enrolled_on'] );
		if( isset( $arrValues['postponed_on'] ) && $boolDirectSet ) $this->set( 'm_strPostponedOn', trim( $arrValues['postponed_on'] ) ); elseif( isset( $arrValues['postponed_on'] ) ) $this->setPostponedOn( $arrValues['postponed_on'] );
		if( isset( $arrValues['postponed_by'] ) && $boolDirectSet ) $this->set( 'm_intPostponedBy', trim( $arrValues['postponed_by'] ) ); elseif( isset( $arrValues['postponed_by'] ) ) $this->setPostponedBy( $arrValues['postponed_by'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['charge_post_date'] ) && $boolDirectSet ) $this->set( 'm_strChargePostDate', trim( $arrValues['charge_post_date'] ) ); elseif( isset( $arrValues['charge_post_date'] ) ) $this->setChargePostDate( $arrValues['charge_post_date'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setResidentInsurancePolicyId( $intResidentInsurancePolicyId ) {
		$this->set( 'm_intResidentInsurancePolicyId', CStrings::strToIntDef( $intResidentInsurancePolicyId, NULL, false ) );
	}

	public function getResidentInsurancePolicyId() {
		return $this->m_intResidentInsurancePolicyId;
	}

	public function sqlResidentInsurancePolicyId() {
		return ( true == isset( $this->m_intResidentInsurancePolicyId ) ) ? ( string ) $this->m_intResidentInsurancePolicyId : 'NULL';
	}

	public function setArTransactionId( $intArTransactionId ) {
		$this->set( 'm_intArTransactionId', CStrings::strToIntDef( $intArTransactionId, NULL, false ) );
	}

	public function getArTransactionId() {
		return $this->m_intArTransactionId;
	}

	public function sqlArTransactionId() {
		return ( true == isset( $this->m_intArTransactionId ) ) ? ( string ) $this->m_intArTransactionId : 'NULL';
	}

	public function setArTransactionRemotePrimaryKey( $strArTransactionRemotePrimaryKey ) {
		$this->set( 'm_strArTransactionRemotePrimaryKey', CStrings::strTrimDef( $strArTransactionRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getArTransactionRemotePrimaryKey() {
		return $this->m_strArTransactionRemotePrimaryKey;
	}

	public function sqlArTransactionRemotePrimaryKey() {
		return ( true == isset( $this->m_strArTransactionRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strArTransactionRemotePrimaryKey ) : '\'' . addslashes( $this->m_strArTransactionRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setArTransactionExportedOn( $strArTransactionExportedOn ) {
		$this->set( 'm_strArTransactionExportedOn', CStrings::strTrimDef( $strArTransactionExportedOn, -1, NULL, true ) );
	}

	public function getArTransactionExportedOn() {
		return $this->m_strArTransactionExportedOn;
	}

	public function sqlArTransactionExportedOn() {
		return ( true == isset( $this->m_strArTransactionExportedOn ) ) ? '\'' . $this->m_strArTransactionExportedOn . '\'' : 'NULL';
	}

	public function setUninsuredOn( $strUninsuredOn ) {
		$this->set( 'm_strUninsuredOn', CStrings::strTrimDef( $strUninsuredOn, -1, NULL, true ) );
	}

	public function getUninsuredOn() {
		return $this->m_strUninsuredOn;
	}

	public function sqlUninsuredOn() {
		return ( true == isset( $this->m_strUninsuredOn ) ) ? '\'' . $this->m_strUninsuredOn . '\'' : 'NULL';
	}

	public function setReinsuredOn( $strReinsuredOn ) {
		$this->set( 'm_strReinsuredOn', CStrings::strTrimDef( $strReinsuredOn, -1, NULL, true ) );
	}

	public function getReinsuredOn() {
		return $this->m_strReinsuredOn;
	}

	public function sqlReinsuredOn() {
		return ( true == isset( $this->m_strReinsuredOn ) ) ? '\'' . $this->m_strReinsuredOn . '\'' : 'NULL';
	}

	public function setPolicyEffectiveDate( $strPolicyEffectiveDate ) {
		$this->set( 'm_strPolicyEffectiveDate', CStrings::strTrimDef( $strPolicyEffectiveDate, -1, NULL, true ) );
	}

	public function getPolicyEffectiveDate() {
		return $this->m_strPolicyEffectiveDate;
	}

	public function sqlPolicyEffectiveDate() {
		return ( true == isset( $this->m_strPolicyEffectiveDate ) ) ? '\'' . $this->m_strPolicyEffectiveDate . '\'' : 'NOW()';
	}

	public function setPolicyEndDate( $strPolicyEndDate ) {
		$this->set( 'm_strPolicyEndDate', CStrings::strTrimDef( $strPolicyEndDate, -1, NULL, true ) );
	}

	public function getPolicyEndDate() {
		return $this->m_strPolicyEndDate;
	}

	public function sqlPolicyEndDate() {
		return ( true == isset( $this->m_strPolicyEndDate ) ) ? '\'' . $this->m_strPolicyEndDate . '\'' : 'NULL';
	}

	public function setFirstNoticeSentOn( $strFirstNoticeSentOn ) {
		$this->set( 'm_strFirstNoticeSentOn', CStrings::strTrimDef( $strFirstNoticeSentOn, -1, NULL, true ) );
	}

	public function getFirstNoticeSentOn() {
		return $this->m_strFirstNoticeSentOn;
	}

	public function sqlFirstNoticeSentOn() {
		return ( true == isset( $this->m_strFirstNoticeSentOn ) ) ? '\'' . $this->m_strFirstNoticeSentOn . '\'' : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setEnrolledOn( $strEnrolledOn ) {
		$this->set( 'm_strEnrolledOn', CStrings::strTrimDef( $strEnrolledOn, -1, NULL, true ) );
	}

	public function getEnrolledOn() {
		return $this->m_strEnrolledOn;
	}

	public function sqlEnrolledOn() {
		return ( true == isset( $this->m_strEnrolledOn ) ) ? '\'' . $this->m_strEnrolledOn . '\'' : 'NULL';
	}

	public function setPostponedOn( $strPostponedOn ) {
		$this->set( 'm_strPostponedOn', CStrings::strTrimDef( $strPostponedOn, -1, NULL, true ) );
	}

	public function getPostponedOn() {
		return $this->m_strPostponedOn;
	}

	public function sqlPostponedOn() {
		return ( true == isset( $this->m_strPostponedOn ) ) ? '\'' . $this->m_strPostponedOn . '\'' : 'NULL';
	}

	public function setPostponedBy( $intPostponedBy ) {
		$this->set( 'm_intPostponedBy', CStrings::strToIntDef( $intPostponedBy, NULL, false ) );
	}

	public function getPostponedBy() {
		return $this->m_intPostponedBy;
	}

	public function sqlPostponedBy() {
		return ( true == isset( $this->m_intPostponedBy ) ) ? ( string ) $this->m_intPostponedBy : 'NULL';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setChargePostDate( $strChargePostDate ) {
		$this->set( 'm_strChargePostDate', CStrings::strTrimDef( $strChargePostDate, -1, NULL, true ) );
	}

	public function getChargePostDate() {
		return $this->m_strChargePostDate;
	}

	public function sqlChargePostDate() {
		return ( true == isset( $this->m_strChargePostDate ) ) ? '\'' . $this->m_strChargePostDate . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, customer_id, lease_id, resident_insurance_policy_id, ar_transaction_id, ar_transaction_remote_primary_key, ar_transaction_exported_on, uninsured_on, reinsured_on, policy_effective_date, policy_end_date, first_notice_sent_on, exported_on, enrolled_on, postponed_on, postponed_by, is_disabled, updated_by, updated_on, created_by, created_on, charge_post_date )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlResidentInsurancePolicyId() . ', ' .
						$this->sqlArTransactionId() . ', ' .
						$this->sqlArTransactionRemotePrimaryKey() . ', ' .
						$this->sqlArTransactionExportedOn() . ', ' .
						$this->sqlUninsuredOn() . ', ' .
						$this->sqlReinsuredOn() . ', ' .
						$this->sqlPolicyEffectiveDate() . ', ' .
						$this->sqlPolicyEndDate() . ', ' .
						$this->sqlFirstNoticeSentOn() . ', ' .
						$this->sqlExportedOn() . ', ' .
						$this->sqlEnrolledOn() . ', ' .
						$this->sqlPostponedOn() . ', ' .
						$this->sqlPostponedBy() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlChargePostDate() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_insurance_policy_id = ' . $this->sqlResidentInsurancePolicyId(). ',' ; } elseif( true == array_key_exists( 'ResidentInsurancePolicyId', $this->getChangedColumns() ) ) { $strSql .= ' resident_insurance_policy_id = ' . $this->sqlResidentInsurancePolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId(). ',' ; } elseif( true == array_key_exists( 'ArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_transaction_remote_primary_key = ' . $this->sqlArTransactionRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'ArTransactionRemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' ar_transaction_remote_primary_key = ' . $this->sqlArTransactionRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_transaction_exported_on = ' . $this->sqlArTransactionExportedOn(). ',' ; } elseif( true == array_key_exists( 'ArTransactionExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' ar_transaction_exported_on = ' . $this->sqlArTransactionExportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' uninsured_on = ' . $this->sqlUninsuredOn(). ',' ; } elseif( true == array_key_exists( 'UninsuredOn', $this->getChangedColumns() ) ) { $strSql .= ' uninsured_on = ' . $this->sqlUninsuredOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reinsured_on = ' . $this->sqlReinsuredOn(). ',' ; } elseif( true == array_key_exists( 'ReinsuredOn', $this->getChangedColumns() ) ) { $strSql .= ' reinsured_on = ' . $this->sqlReinsuredOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' policy_effective_date = ' . $this->sqlPolicyEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'PolicyEffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' policy_effective_date = ' . $this->sqlPolicyEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' policy_end_date = ' . $this->sqlPolicyEndDate(). ',' ; } elseif( true == array_key_exists( 'PolicyEndDate', $this->getChangedColumns() ) ) { $strSql .= ' policy_end_date = ' . $this->sqlPolicyEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_notice_sent_on = ' . $this->sqlFirstNoticeSentOn(). ',' ; } elseif( true == array_key_exists( 'FirstNoticeSentOn', $this->getChangedColumns() ) ) { $strSql .= ' first_notice_sent_on = ' . $this->sqlFirstNoticeSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn(). ',' ; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' enrolled_on = ' . $this->sqlEnrolledOn(). ',' ; } elseif( true == array_key_exists( 'EnrolledOn', $this->getChangedColumns() ) ) { $strSql .= ' enrolled_on = ' . $this->sqlEnrolledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postponed_on = ' . $this->sqlPostponedOn(). ',' ; } elseif( true == array_key_exists( 'PostponedOn', $this->getChangedColumns() ) ) { $strSql .= ' postponed_on = ' . $this->sqlPostponedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postponed_by = ' . $this->sqlPostponedBy(). ',' ; } elseif( true == array_key_exists( 'PostponedBy', $this->getChangedColumns() ) ) { $strSql .= ' postponed_by = ' . $this->sqlPostponedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_post_date = ' . $this->sqlChargePostDate(). ',' ; } elseif( true == array_key_exists( 'ChargePostDate', $this->getChangedColumns() ) ) { $strSql .= ' charge_post_date = ' . $this->sqlChargePostDate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'customer_id' => $this->getCustomerId(),
			'lease_id' => $this->getLeaseId(),
			'resident_insurance_policy_id' => $this->getResidentInsurancePolicyId(),
			'ar_transaction_id' => $this->getArTransactionId(),
			'ar_transaction_remote_primary_key' => $this->getArTransactionRemotePrimaryKey(),
			'ar_transaction_exported_on' => $this->getArTransactionExportedOn(),
			'uninsured_on' => $this->getUninsuredOn(),
			'reinsured_on' => $this->getReinsuredOn(),
			'policy_effective_date' => $this->getPolicyEffectiveDate(),
			'policy_end_date' => $this->getPolicyEndDate(),
			'first_notice_sent_on' => $this->getFirstNoticeSentOn(),
			'exported_on' => $this->getExportedOn(),
			'enrolled_on' => $this->getEnrolledOn(),
			'postponed_on' => $this->getPostponedOn(),
			'postponed_by' => $this->getPostponedBy(),
			'is_disabled' => $this->getIsDisabled(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'charge_post_date' => $this->getChargePostDate()
		);
	}

}
?>