<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyAmenityAvailabilityFileAssociations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyAmenityAvailabilityFileAssociations extends CEosPluralBase {

	/**
	 * @return CPropertyAmenityAvailabilityFileAssociation[]
	 */
	public static function fetchPropertyAmenityAvailabilityFileAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyAmenityAvailabilityFileAssociation', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyAmenityAvailabilityFileAssociation
	 */
	public static function fetchPropertyAmenityAvailabilityFileAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyAmenityAvailabilityFileAssociation', $objDatabase );
	}

	public static function fetchPropertyAmenityAvailabilityFileAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_amenity_availability_file_associations', $objDatabase );
	}

	public static function fetchPropertyAmenityAvailabilityFileAssociationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyAmenityAvailabilityFileAssociation( sprintf( 'SELECT * FROM property_amenity_availability_file_associations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyAmenityAvailabilityFileAssociationsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyAmenityAvailabilityFileAssociations( sprintf( 'SELECT * FROM property_amenity_availability_file_associations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyAmenityAvailabilityFileAssociationsByPropertyAmenityAvailabilityIdByCid( $intPropertyAmenityAvailabilityId, $intCid, $objDatabase ) {
		return self::fetchPropertyAmenityAvailabilityFileAssociations( sprintf( 'SELECT * FROM property_amenity_availability_file_associations WHERE property_amenity_availability_id = %d AND cid = %d', ( int ) $intPropertyAmenityAvailabilityId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyAmenityAvailabilityFileAssociationsByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		return self::fetchPropertyAmenityAvailabilityFileAssociations( sprintf( 'SELECT * FROM property_amenity_availability_file_associations WHERE file_id = %d AND cid = %d', ( int ) $intFileId, ( int ) $intCid ), $objDatabase );
	}

}
?>