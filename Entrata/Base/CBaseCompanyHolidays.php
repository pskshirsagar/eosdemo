<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyHolidays
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyHolidays extends CEosPluralBase {

	/**
	 * @return CCompanyHoliday[]
	 */
	public static function fetchCompanyHolidays( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCompanyHoliday::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyHoliday
	 */
	public static function fetchCompanyHoliday( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyHoliday::class, $objDatabase );
	}

	public static function fetchCompanyHolidayCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_holidays', $objDatabase );
	}

	public static function fetchCompanyHolidayByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyHoliday( sprintf( 'SELECT * FROM company_holidays WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyHolidaysByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyHolidays( sprintf( 'SELECT * FROM company_holidays WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCompanyHolidaysByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCompanyHolidays( sprintf( 'SELECT * FROM company_holidays WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

}
?>