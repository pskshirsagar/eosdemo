<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlReconciliationBankTransaction extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.gl_reconciliation_bank_transactions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intGlReconciliationId;
	protected $m_intBankCsvColumnMappingId;
	protected $m_strTransactionDate;
	protected $m_strTransactionType;
	protected $m_strDescription;
	protected $m_strTransactionNumber;
	protected $m_fltAmount;
	protected $m_boolIsCleared;
	protected $m_intGlHeaderId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsCleared = false;
		$this->m_strUpdatedOn = 'now()';
		$this->m_strDeletedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['gl_reconciliation_id'] ) && $boolDirectSet ) $this->set( 'm_intGlReconciliationId', trim( $arrValues['gl_reconciliation_id'] ) ); elseif( isset( $arrValues['gl_reconciliation_id'] ) ) $this->setGlReconciliationId( $arrValues['gl_reconciliation_id'] );
		if( isset( $arrValues['bank_csv_column_mapping_id'] ) && $boolDirectSet ) $this->set( 'm_intBankCsvColumnMappingId', trim( $arrValues['bank_csv_column_mapping_id'] ) ); elseif( isset( $arrValues['bank_csv_column_mapping_id'] ) ) $this->setBankCsvColumnMappingId( $arrValues['bank_csv_column_mapping_id'] );
		if( isset( $arrValues['transaction_date'] ) && $boolDirectSet ) $this->set( 'm_strTransactionDate', trim( $arrValues['transaction_date'] ) ); elseif( isset( $arrValues['transaction_date'] ) ) $this->setTransactionDate( $arrValues['transaction_date'] );
		if( isset( $arrValues['transaction_type'] ) && $boolDirectSet ) $this->set( 'm_strTransactionType', trim( $arrValues['transaction_type'] ) ); elseif( isset( $arrValues['transaction_type'] ) ) $this->setTransactionType( $arrValues['transaction_type'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['transaction_number'] ) && $boolDirectSet ) $this->set( 'm_strTransactionNumber', trim( $arrValues['transaction_number'] ) ); elseif( isset( $arrValues['transaction_number'] ) ) $this->setTransactionNumber( $arrValues['transaction_number'] );
		if( isset( $arrValues['amount'] ) && $boolDirectSet ) $this->set( 'm_fltAmount', trim( $arrValues['amount'] ) ); elseif( isset( $arrValues['amount'] ) ) $this->setAmount( $arrValues['amount'] );
		if( isset( $arrValues['is_cleared'] ) && $boolDirectSet ) $this->set( 'm_boolIsCleared', trim( stripcslashes( $arrValues['is_cleared'] ) ) ); elseif( isset( $arrValues['is_cleared'] ) ) $this->setIsCleared( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_cleared'] ) : $arrValues['is_cleared'] );
		if( isset( $arrValues['gl_header_id'] ) && $boolDirectSet ) $this->set( 'm_intGlHeaderId', trim( $arrValues['gl_header_id'] ) ); elseif( isset( $arrValues['gl_header_id'] ) ) $this->setGlHeaderId( $arrValues['gl_header_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setGlReconciliationId( $intGlReconciliationId ) {
		$this->set( 'm_intGlReconciliationId', CStrings::strToIntDef( $intGlReconciliationId, NULL, false ) );
	}

	public function getGlReconciliationId() {
		return $this->m_intGlReconciliationId;
	}

	public function sqlGlReconciliationId() {
		return ( true == isset( $this->m_intGlReconciliationId ) ) ? ( string ) $this->m_intGlReconciliationId : 'NULL';
	}

	public function setBankCsvColumnMappingId( $intBankCsvColumnMappingId ) {
		$this->set( 'm_intBankCsvColumnMappingId', CStrings::strToIntDef( $intBankCsvColumnMappingId, NULL, false ) );
	}

	public function getBankCsvColumnMappingId() {
		return $this->m_intBankCsvColumnMappingId;
	}

	public function sqlBankCsvColumnMappingId() {
		return ( true == isset( $this->m_intBankCsvColumnMappingId ) ) ? ( string ) $this->m_intBankCsvColumnMappingId : 'NULL';
	}

	public function setTransactionDate( $strTransactionDate ) {
		$this->set( 'm_strTransactionDate', CStrings::strTrimDef( $strTransactionDate, -1, NULL, true ) );
	}

	public function getTransactionDate() {
		return $this->m_strTransactionDate;
	}

	public function sqlTransactionDate() {
		return ( true == isset( $this->m_strTransactionDate ) ) ? '\'' . $this->m_strTransactionDate . '\'' : 'NULL';
	}

	public function setTransactionType( $strTransactionType ) {
		$this->set( 'm_strTransactionType', CStrings::strTrimDef( $strTransactionType, 40, NULL, true ) );
	}

	public function getTransactionType() {
		return $this->m_strTransactionType;
	}

	public function sqlTransactionType() {
		return ( true == isset( $this->m_strTransactionType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTransactionType ) : '\'' . addslashes( $this->m_strTransactionType ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setTransactionNumber( $strTransactionNumber ) {
		$this->set( 'm_strTransactionNumber', CStrings::strTrimDef( $strTransactionNumber, 40, NULL, true ) );
	}

	public function getTransactionNumber() {
		return $this->m_strTransactionNumber;
	}

	public function sqlTransactionNumber() {
		return ( true == isset( $this->m_strTransactionNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTransactionNumber ) : '\'' . addslashes( $this->m_strTransactionNumber ) . '\'' ) : 'NULL';
	}

	public function setAmount( $fltAmount ) {
		$this->set( 'm_fltAmount', CStrings::strToFloatDef( $fltAmount, NULL, false, 2 ) );
	}

	public function getAmount() {
		return $this->m_fltAmount;
	}

	public function sqlAmount() {
		return ( true == isset( $this->m_fltAmount ) ) ? ( string ) $this->m_fltAmount : 'NULL';
	}

	public function setIsCleared( $boolIsCleared ) {
		$this->set( 'm_boolIsCleared', CStrings::strToBool( $boolIsCleared ) );
	}

	public function getIsCleared() {
		return $this->m_boolIsCleared;
	}

	public function sqlIsCleared() {
		return ( true == isset( $this->m_boolIsCleared ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCleared ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setGlHeaderId( $intGlHeaderId ) {
		$this->set( 'm_intGlHeaderId', CStrings::strToIntDef( $intGlHeaderId, NULL, false ) );
	}

	public function getGlHeaderId() {
		return $this->m_intGlHeaderId;
	}

	public function sqlGlHeaderId() {
		return ( true == isset( $this->m_intGlHeaderId ) ) ? ( string ) $this->m_intGlHeaderId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, gl_reconciliation_id, bank_csv_column_mapping_id, transaction_date, transaction_type, description, transaction_number, amount, is_cleared, gl_header_id, updated_by, updated_on, deleted_by, deleted_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlGlReconciliationId() . ', ' .
						$this->sqlBankCsvColumnMappingId() . ', ' .
						$this->sqlTransactionDate() . ', ' .
						$this->sqlTransactionType() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlTransactionNumber() . ', ' .
						$this->sqlAmount() . ', ' .
						$this->sqlIsCleared() . ', ' .
						$this->sqlGlHeaderId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_reconciliation_id = ' . $this->sqlGlReconciliationId(). ',' ; } elseif( true == array_key_exists( 'GlReconciliationId', $this->getChangedColumns() ) ) { $strSql .= ' gl_reconciliation_id = ' . $this->sqlGlReconciliationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_csv_column_mapping_id = ' . $this->sqlBankCsvColumnMappingId(). ',' ; } elseif( true == array_key_exists( 'BankCsvColumnMappingId', $this->getChangedColumns() ) ) { $strSql .= ' bank_csv_column_mapping_id = ' . $this->sqlBankCsvColumnMappingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_date = ' . $this->sqlTransactionDate(). ',' ; } elseif( true == array_key_exists( 'TransactionDate', $this->getChangedColumns() ) ) { $strSql .= ' transaction_date = ' . $this->sqlTransactionDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_type = ' . $this->sqlTransactionType(). ',' ; } elseif( true == array_key_exists( 'TransactionType', $this->getChangedColumns() ) ) { $strSql .= ' transaction_type = ' . $this->sqlTransactionType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_number = ' . $this->sqlTransactionNumber(). ',' ; } elseif( true == array_key_exists( 'TransactionNumber', $this->getChangedColumns() ) ) { $strSql .= ' transaction_number = ' . $this->sqlTransactionNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount = ' . $this->sqlAmount(). ',' ; } elseif( true == array_key_exists( 'Amount', $this->getChangedColumns() ) ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_cleared = ' . $this->sqlIsCleared(). ',' ; } elseif( true == array_key_exists( 'IsCleared', $this->getChangedColumns() ) ) { $strSql .= ' is_cleared = ' . $this->sqlIsCleared() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_header_id = ' . $this->sqlGlHeaderId(). ',' ; } elseif( true == array_key_exists( 'GlHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' gl_header_id = ' . $this->sqlGlHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'gl_reconciliation_id' => $this->getGlReconciliationId(),
			'bank_csv_column_mapping_id' => $this->getBankCsvColumnMappingId(),
			'transaction_date' => $this->getTransactionDate(),
			'transaction_type' => $this->getTransactionType(),
			'description' => $this->getDescription(),
			'transaction_number' => $this->getTransactionNumber(),
			'amount' => $this->getAmount(),
			'is_cleared' => $this->getIsCleared(),
			'gl_header_id' => $this->getGlHeaderId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>