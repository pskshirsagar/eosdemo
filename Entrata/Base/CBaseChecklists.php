<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChecklists
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseChecklists extends CEosPluralBase {

	/**
	 * @return CChecklist[]
	 */
	public static function fetchChecklists( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CChecklist::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CChecklist
	 */
	public static function fetchChecklist( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CChecklist::class, $objDatabase );
	}

	public static function fetchChecklistCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'checklists', $objDatabase );
	}

	public static function fetchChecklistByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchChecklist( sprintf( 'SELECT * FROM checklists WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchChecklistsByCid( $intCid, $objDatabase ) {
		return self::fetchChecklists( sprintf( 'SELECT * FROM checklists WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchChecklistsByChecklistTypeIdByCid( $intChecklistTypeId, $intCid, $objDatabase ) {
		return self::fetchChecklists( sprintf( 'SELECT * FROM checklists WHERE checklist_type_id = %d AND cid = %d', $intChecklistTypeId, $intCid ), $objDatabase );
	}

	public static function fetchChecklistsByChecklistTriggerIdByCid( $intChecklistTriggerId, $intCid, $objDatabase ) {
		return self::fetchChecklists( sprintf( 'SELECT * FROM checklists WHERE checklist_trigger_id = %d AND cid = %d', $intChecklistTriggerId, $intCid ), $objDatabase );
	}

	public static function fetchChecklistsByDefaultChecklistIdByCid( $intDefaultChecklistId, $intCid, $objDatabase ) {
		return self::fetchChecklists( sprintf( 'SELECT * FROM checklists WHERE default_checklist_id = %d AND cid = %d', $intDefaultChecklistId, $intCid ), $objDatabase );
	}

}
?>