<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicantApplication extends CEosSingularBase {

	const TABLE_NAME = 'public.applicant_applications';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApplicantId;
	protected $m_intApplicationId;
	protected $m_intLeaseStatusTypeId;
	protected $m_intCustomerTypeId;
	protected $m_intApplicationStepId;
	protected $m_intApplicationDocumentId;
	protected $m_intApplicationFormId;
	protected $m_intApplicantMoveInReasonId;
	protected $m_intLeaseDocumentId;
	protected $m_intCustomerRelationshipId;
	protected $m_strGuestRemotePrimaryKey;
	protected $m_strAppRemotePrimaryKey;
	protected $m_strRenewalRemotePrimaryKey;
	protected $m_strAdditionalInfo;
	protected $m_strApplicationSignedOn;
	protected $m_strApplicationSignature;
	protected $m_strApplicationIpAddress;
	protected $m_intRequireScreening;
	protected $m_strLeaseGeneratedOn;
	protected $m_boolIsLeasePartiallyGenerated;
	protected $m_strLeaseSkippedOn;
	protected $m_strLeaseSignedOn;
	protected $m_strLeaseSignature;
	protected $m_strLeaseIpAddress;
	protected $m_strBlueMoonApplicationSentOn;
	protected $m_intHasVisited;
	protected $m_intPrimaryIsResponsible;
	protected $m_intLeaseInvitationSent;
	protected $m_intIsLead;
	protected $m_intDesiredVehicles;
	protected $m_boolSendConsumerReport;
	protected $m_strStartedOn;
	protected $m_strCompletedOn;
	protected $m_strApprovedOn;
	protected $m_intApprovedBy;
	protected $m_strExportedOn;
	protected $m_strOfferSentOn;
	protected $m_intLockedBy;
	protected $m_strLockedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCustomerTypeId = '1';
		$this->m_intRequireScreening = '0';
		$this->m_boolIsLeasePartiallyGenerated = false;
		$this->m_intHasVisited = '0';
		$this->m_intPrimaryIsResponsible = '0';
		$this->m_intLeaseInvitationSent = '0';
		$this->m_intIsLead = '1';
		$this->m_boolSendConsumerReport = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantId', trim( $arrValues['applicant_id'] ) ); elseif( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['lease_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStatusTypeId', trim( $arrValues['lease_status_type_id'] ) ); elseif( isset( $arrValues['lease_status_type_id'] ) ) $this->setLeaseStatusTypeId( $arrValues['lease_status_type_id'] );
		if( isset( $arrValues['customer_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerTypeId', trim( $arrValues['customer_type_id'] ) ); elseif( isset( $arrValues['customer_type_id'] ) ) $this->setCustomerTypeId( $arrValues['customer_type_id'] );
		if( isset( $arrValues['application_step_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationStepId', trim( $arrValues['application_step_id'] ) ); elseif( isset( $arrValues['application_step_id'] ) ) $this->setApplicationStepId( $arrValues['application_step_id'] );
		if( isset( $arrValues['application_document_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationDocumentId', trim( $arrValues['application_document_id'] ) ); elseif( isset( $arrValues['application_document_id'] ) ) $this->setApplicationDocumentId( $arrValues['application_document_id'] );
		if( isset( $arrValues['application_form_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationFormId', trim( $arrValues['application_form_id'] ) ); elseif( isset( $arrValues['application_form_id'] ) ) $this->setApplicationFormId( $arrValues['application_form_id'] );
		if( isset( $arrValues['applicant_move_in_reason_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantMoveInReasonId', trim( $arrValues['applicant_move_in_reason_id'] ) ); elseif( isset( $arrValues['applicant_move_in_reason_id'] ) ) $this->setApplicantMoveInReasonId( $arrValues['applicant_move_in_reason_id'] );
		if( isset( $arrValues['lease_document_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseDocumentId', trim( $arrValues['lease_document_id'] ) ); elseif( isset( $arrValues['lease_document_id'] ) ) $this->setLeaseDocumentId( $arrValues['lease_document_id'] );
		if( isset( $arrValues['customer_relationship_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerRelationshipId', trim( $arrValues['customer_relationship_id'] ) ); elseif( isset( $arrValues['customer_relationship_id'] ) ) $this->setCustomerRelationshipId( $arrValues['customer_relationship_id'] );
		if( isset( $arrValues['guest_remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strGuestRemotePrimaryKey', trim( stripcslashes( $arrValues['guest_remote_primary_key'] ) ) ); elseif( isset( $arrValues['guest_remote_primary_key'] ) ) $this->setGuestRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['guest_remote_primary_key'] ) : $arrValues['guest_remote_primary_key'] );
		if( isset( $arrValues['app_remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strAppRemotePrimaryKey', trim( stripcslashes( $arrValues['app_remote_primary_key'] ) ) ); elseif( isset( $arrValues['app_remote_primary_key'] ) ) $this->setAppRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['app_remote_primary_key'] ) : $arrValues['app_remote_primary_key'] );
		if( isset( $arrValues['renewal_remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRenewalRemotePrimaryKey', trim( stripcslashes( $arrValues['renewal_remote_primary_key'] ) ) ); elseif( isset( $arrValues['renewal_remote_primary_key'] ) ) $this->setRenewalRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['renewal_remote_primary_key'] ) : $arrValues['renewal_remote_primary_key'] );
		if( isset( $arrValues['additional_info'] ) && $boolDirectSet ) $this->set( 'm_strAdditionalInfo', trim( stripcslashes( $arrValues['additional_info'] ) ) ); elseif( isset( $arrValues['additional_info'] ) ) $this->setAdditionalInfo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['additional_info'] ) : $arrValues['additional_info'] );
		if( isset( $arrValues['application_signed_on'] ) && $boolDirectSet ) $this->set( 'm_strApplicationSignedOn', trim( $arrValues['application_signed_on'] ) ); elseif( isset( $arrValues['application_signed_on'] ) ) $this->setApplicationSignedOn( $arrValues['application_signed_on'] );
		if( isset( $arrValues['application_signature'] ) && $boolDirectSet ) $this->set( 'm_strApplicationSignature', trim( stripcslashes( $arrValues['application_signature'] ) ) ); elseif( isset( $arrValues['application_signature'] ) ) $this->setApplicationSignature( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['application_signature'] ) : $arrValues['application_signature'] );
		if( isset( $arrValues['application_ip_address'] ) && $boolDirectSet ) $this->set( 'm_strApplicationIpAddress', trim( stripcslashes( $arrValues['application_ip_address'] ) ) ); elseif( isset( $arrValues['application_ip_address'] ) ) $this->setApplicationIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['application_ip_address'] ) : $arrValues['application_ip_address'] );
		if( isset( $arrValues['require_screening'] ) && $boolDirectSet ) $this->set( 'm_intRequireScreening', trim( $arrValues['require_screening'] ) ); elseif( isset( $arrValues['require_screening'] ) ) $this->setRequireScreening( $arrValues['require_screening'] );
		if( isset( $arrValues['lease_generated_on'] ) && $boolDirectSet ) $this->set( 'm_strLeaseGeneratedOn', trim( $arrValues['lease_generated_on'] ) ); elseif( isset( $arrValues['lease_generated_on'] ) ) $this->setLeaseGeneratedOn( $arrValues['lease_generated_on'] );
		if( isset( $arrValues['is_lease_partially_generated'] ) && $boolDirectSet ) $this->set( 'm_boolIsLeasePartiallyGenerated', trim( stripcslashes( $arrValues['is_lease_partially_generated'] ) ) ); elseif( isset( $arrValues['is_lease_partially_generated'] ) ) $this->setIsLeasePartiallyGenerated( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_lease_partially_generated'] ) : $arrValues['is_lease_partially_generated'] );
		if( isset( $arrValues['lease_skipped_on'] ) && $boolDirectSet ) $this->set( 'm_strLeaseSkippedOn', trim( $arrValues['lease_skipped_on'] ) ); elseif( isset( $arrValues['lease_skipped_on'] ) ) $this->setLeaseSkippedOn( $arrValues['lease_skipped_on'] );
		if( isset( $arrValues['lease_signed_on'] ) && $boolDirectSet ) $this->set( 'm_strLeaseSignedOn', trim( $arrValues['lease_signed_on'] ) ); elseif( isset( $arrValues['lease_signed_on'] ) ) $this->setLeaseSignedOn( $arrValues['lease_signed_on'] );
		if( isset( $arrValues['lease_signature'] ) && $boolDirectSet ) $this->set( 'm_strLeaseSignature', trim( stripcslashes( $arrValues['lease_signature'] ) ) ); elseif( isset( $arrValues['lease_signature'] ) ) $this->setLeaseSignature( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['lease_signature'] ) : $arrValues['lease_signature'] );
		if( isset( $arrValues['lease_ip_address'] ) && $boolDirectSet ) $this->set( 'm_strLeaseIpAddress', trim( stripcslashes( $arrValues['lease_ip_address'] ) ) ); elseif( isset( $arrValues['lease_ip_address'] ) ) $this->setLeaseIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['lease_ip_address'] ) : $arrValues['lease_ip_address'] );
		if( isset( $arrValues['blue_moon_application_sent_on'] ) && $boolDirectSet ) $this->set( 'm_strBlueMoonApplicationSentOn', trim( $arrValues['blue_moon_application_sent_on'] ) ); elseif( isset( $arrValues['blue_moon_application_sent_on'] ) ) $this->setBlueMoonApplicationSentOn( $arrValues['blue_moon_application_sent_on'] );
		if( isset( $arrValues['has_visited'] ) && $boolDirectSet ) $this->set( 'm_intHasVisited', trim( $arrValues['has_visited'] ) ); elseif( isset( $arrValues['has_visited'] ) ) $this->setHasVisited( $arrValues['has_visited'] );
		if( isset( $arrValues['primary_is_responsible'] ) && $boolDirectSet ) $this->set( 'm_intPrimaryIsResponsible', trim( $arrValues['primary_is_responsible'] ) ); elseif( isset( $arrValues['primary_is_responsible'] ) ) $this->setPrimaryIsResponsible( $arrValues['primary_is_responsible'] );
		if( isset( $arrValues['lease_invitation_sent'] ) && $boolDirectSet ) $this->set( 'm_intLeaseInvitationSent', trim( $arrValues['lease_invitation_sent'] ) ); elseif( isset( $arrValues['lease_invitation_sent'] ) ) $this->setLeaseInvitationSent( $arrValues['lease_invitation_sent'] );
		if( isset( $arrValues['is_lead'] ) && $boolDirectSet ) $this->set( 'm_intIsLead', trim( $arrValues['is_lead'] ) ); elseif( isset( $arrValues['is_lead'] ) ) $this->setIsLead( $arrValues['is_lead'] );
		if( isset( $arrValues['desired_vehicles'] ) && $boolDirectSet ) $this->set( 'm_intDesiredVehicles', trim( $arrValues['desired_vehicles'] ) ); elseif( isset( $arrValues['desired_vehicles'] ) ) $this->setDesiredVehicles( $arrValues['desired_vehicles'] );
		if( isset( $arrValues['send_consumer_report'] ) && $boolDirectSet ) $this->set( 'm_boolSendConsumerReport', trim( stripcslashes( $arrValues['send_consumer_report'] ) ) ); elseif( isset( $arrValues['send_consumer_report'] ) ) $this->setSendConsumerReport( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['send_consumer_report'] ) : $arrValues['send_consumer_report'] );
		if( isset( $arrValues['started_on'] ) && $boolDirectSet ) $this->set( 'm_strStartedOn', trim( $arrValues['started_on'] ) ); elseif( isset( $arrValues['started_on'] ) ) $this->setStartedOn( $arrValues['started_on'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['offer_sent_on'] ) && $boolDirectSet ) $this->set( 'm_strOfferSentOn', trim( $arrValues['offer_sent_on'] ) ); elseif( isset( $arrValues['offer_sent_on'] ) ) $this->setOfferSentOn( $arrValues['offer_sent_on'] );
		if( isset( $arrValues['locked_by'] ) && $boolDirectSet ) $this->set( 'm_intLockedBy', trim( $arrValues['locked_by'] ) ); elseif( isset( $arrValues['locked_by'] ) ) $this->setLockedBy( $arrValues['locked_by'] );
		if( isset( $arrValues['locked_on'] ) && $boolDirectSet ) $this->set( 'm_strLockedOn', trim( $arrValues['locked_on'] ) ); elseif( isset( $arrValues['locked_on'] ) ) $this->setLockedOn( $arrValues['locked_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApplicantId( $intApplicantId ) {
		$this->set( 'm_intApplicantId', CStrings::strToIntDef( $intApplicantId, NULL, false ) );
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function sqlApplicantId() {
		return ( true == isset( $this->m_intApplicantId ) ) ? ( string ) $this->m_intApplicantId : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$this->set( 'm_intLeaseStatusTypeId', CStrings::strToIntDef( $intLeaseStatusTypeId, NULL, false ) );
	}

	public function getLeaseStatusTypeId() {
		return $this->m_intLeaseStatusTypeId;
	}

	public function sqlLeaseStatusTypeId() {
		return ( true == isset( $this->m_intLeaseStatusTypeId ) ) ? ( string ) $this->m_intLeaseStatusTypeId : 'NULL';
	}

	public function setCustomerTypeId( $intCustomerTypeId ) {
		$this->set( 'm_intCustomerTypeId', CStrings::strToIntDef( $intCustomerTypeId, NULL, false ) );
	}

	public function getCustomerTypeId() {
		return $this->m_intCustomerTypeId;
	}

	public function sqlCustomerTypeId() {
		return ( true == isset( $this->m_intCustomerTypeId ) ) ? ( string ) $this->m_intCustomerTypeId : '1';
	}

	public function setApplicationStepId( $intApplicationStepId ) {
		$this->set( 'm_intApplicationStepId', CStrings::strToIntDef( $intApplicationStepId, NULL, false ) );
	}

	public function getApplicationStepId() {
		return $this->m_intApplicationStepId;
	}

	public function sqlApplicationStepId() {
		return ( true == isset( $this->m_intApplicationStepId ) ) ? ( string ) $this->m_intApplicationStepId : 'NULL';
	}

	public function setApplicationDocumentId( $intApplicationDocumentId ) {
		$this->set( 'm_intApplicationDocumentId', CStrings::strToIntDef( $intApplicationDocumentId, NULL, false ) );
	}

	public function getApplicationDocumentId() {
		return $this->m_intApplicationDocumentId;
	}

	public function sqlApplicationDocumentId() {
		return ( true == isset( $this->m_intApplicationDocumentId ) ) ? ( string ) $this->m_intApplicationDocumentId : 'NULL';
	}

	public function setApplicationFormId( $intApplicationFormId ) {
		$this->set( 'm_intApplicationFormId', CStrings::strToIntDef( $intApplicationFormId, NULL, false ) );
	}

	public function getApplicationFormId() {
		return $this->m_intApplicationFormId;
	}

	public function sqlApplicationFormId() {
		return ( true == isset( $this->m_intApplicationFormId ) ) ? ( string ) $this->m_intApplicationFormId : 'NULL';
	}

	public function setApplicantMoveInReasonId( $intApplicantMoveInReasonId ) {
		$this->set( 'm_intApplicantMoveInReasonId', CStrings::strToIntDef( $intApplicantMoveInReasonId, NULL, false ) );
	}

	public function getApplicantMoveInReasonId() {
		return $this->m_intApplicantMoveInReasonId;
	}

	public function sqlApplicantMoveInReasonId() {
		return ( true == isset( $this->m_intApplicantMoveInReasonId ) ) ? ( string ) $this->m_intApplicantMoveInReasonId : 'NULL';
	}

	public function setLeaseDocumentId( $intLeaseDocumentId ) {
		$this->set( 'm_intLeaseDocumentId', CStrings::strToIntDef( $intLeaseDocumentId, NULL, false ) );
	}

	public function getLeaseDocumentId() {
		return $this->m_intLeaseDocumentId;
	}

	public function sqlLeaseDocumentId() {
		return ( true == isset( $this->m_intLeaseDocumentId ) ) ? ( string ) $this->m_intLeaseDocumentId : 'NULL';
	}

	public function setCustomerRelationshipId( $intCustomerRelationshipId ) {
		$this->set( 'm_intCustomerRelationshipId', CStrings::strToIntDef( $intCustomerRelationshipId, NULL, false ) );
	}

	public function getCustomerRelationshipId() {
		return $this->m_intCustomerRelationshipId;
	}

	public function sqlCustomerRelationshipId() {
		return ( true == isset( $this->m_intCustomerRelationshipId ) ) ? ( string ) $this->m_intCustomerRelationshipId : 'NULL';
	}

	public function setGuestRemotePrimaryKey( $strGuestRemotePrimaryKey ) {
		$this->set( 'm_strGuestRemotePrimaryKey', CStrings::strTrimDef( $strGuestRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getGuestRemotePrimaryKey() {
		return $this->m_strGuestRemotePrimaryKey;
	}

	public function sqlGuestRemotePrimaryKey() {
		return ( true == isset( $this->m_strGuestRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strGuestRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setAppRemotePrimaryKey( $strAppRemotePrimaryKey ) {
		$this->set( 'm_strAppRemotePrimaryKey', CStrings::strTrimDef( $strAppRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getAppRemotePrimaryKey() {
		return $this->m_strAppRemotePrimaryKey;
	}

	public function sqlAppRemotePrimaryKey() {
		return ( true == isset( $this->m_strAppRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strAppRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setRenewalRemotePrimaryKey( $strRenewalRemotePrimaryKey ) {
		$this->set( 'm_strRenewalRemotePrimaryKey', CStrings::strTrimDef( $strRenewalRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRenewalRemotePrimaryKey() {
		return $this->m_strRenewalRemotePrimaryKey;
	}

	public function sqlRenewalRemotePrimaryKey() {
		return ( true == isset( $this->m_strRenewalRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRenewalRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setAdditionalInfo( $strAdditionalInfo ) {
		$this->set( 'm_strAdditionalInfo', CStrings::strTrimDef( $strAdditionalInfo, -1, NULL, true ) );
	}

	public function getAdditionalInfo() {
		return $this->m_strAdditionalInfo;
	}

	public function sqlAdditionalInfo() {
		return ( true == isset( $this->m_strAdditionalInfo ) ) ? '\'' . addslashes( $this->m_strAdditionalInfo ) . '\'' : 'NULL';
	}

	public function setApplicationSignedOn( $strApplicationSignedOn ) {
		$this->set( 'm_strApplicationSignedOn', CStrings::strTrimDef( $strApplicationSignedOn, -1, NULL, true ) );
	}

	public function getApplicationSignedOn() {
		return $this->m_strApplicationSignedOn;
	}

	public function sqlApplicationSignedOn() {
		return ( true == isset( $this->m_strApplicationSignedOn ) ) ? '\'' . $this->m_strApplicationSignedOn . '\'' : 'NULL';
	}

	public function setApplicationSignature( $strApplicationSignature ) {
		$this->set( 'm_strApplicationSignature', CStrings::strTrimDef( $strApplicationSignature, 240, NULL, true ) );
	}

	public function getApplicationSignature() {
		return $this->m_strApplicationSignature;
	}

	public function sqlApplicationSignature() {
		return ( true == isset( $this->m_strApplicationSignature ) ) ? '\'' . addslashes( $this->m_strApplicationSignature ) . '\'' : 'NULL';
	}

	public function setApplicationIpAddress( $strApplicationIpAddress ) {
		$this->set( 'm_strApplicationIpAddress', CStrings::strTrimDef( $strApplicationIpAddress, 23, NULL, true ) );
	}

	public function getApplicationIpAddress() {
		return $this->m_strApplicationIpAddress;
	}

	public function sqlApplicationIpAddress() {
		return ( true == isset( $this->m_strApplicationIpAddress ) ) ? '\'' . addslashes( $this->m_strApplicationIpAddress ) . '\'' : 'NULL';
	}

	public function setRequireScreening( $intRequireScreening ) {
		$this->set( 'm_intRequireScreening', CStrings::strToIntDef( $intRequireScreening, NULL, false ) );
	}

	public function getRequireScreening() {
		return $this->m_intRequireScreening;
	}

	public function sqlRequireScreening() {
		return ( true == isset( $this->m_intRequireScreening ) ) ? ( string ) $this->m_intRequireScreening : '0';
	}

	public function setLeaseGeneratedOn( $strLeaseGeneratedOn ) {
		$this->set( 'm_strLeaseGeneratedOn', CStrings::strTrimDef( $strLeaseGeneratedOn, -1, NULL, true ) );
	}

	public function getLeaseGeneratedOn() {
		return $this->m_strLeaseGeneratedOn;
	}

	public function sqlLeaseGeneratedOn() {
		return ( true == isset( $this->m_strLeaseGeneratedOn ) ) ? '\'' . $this->m_strLeaseGeneratedOn . '\'' : 'NULL';
	}

	public function setIsLeasePartiallyGenerated( $boolIsLeasePartiallyGenerated ) {
		$this->set( 'm_boolIsLeasePartiallyGenerated', CStrings::strToBool( $boolIsLeasePartiallyGenerated ) );
	}

	public function getIsLeasePartiallyGenerated() {
		return $this->m_boolIsLeasePartiallyGenerated;
	}

	public function sqlIsLeasePartiallyGenerated() {
		return ( true == isset( $this->m_boolIsLeasePartiallyGenerated ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLeasePartiallyGenerated ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setLeaseSkippedOn( $strLeaseSkippedOn ) {
		$this->set( 'm_strLeaseSkippedOn', CStrings::strTrimDef( $strLeaseSkippedOn, -1, NULL, true ) );
	}

	public function getLeaseSkippedOn() {
		return $this->m_strLeaseSkippedOn;
	}

	public function sqlLeaseSkippedOn() {
		return ( true == isset( $this->m_strLeaseSkippedOn ) ) ? '\'' . $this->m_strLeaseSkippedOn . '\'' : 'NULL';
	}

	public function setLeaseSignedOn( $strLeaseSignedOn ) {
		$this->set( 'm_strLeaseSignedOn', CStrings::strTrimDef( $strLeaseSignedOn, -1, NULL, true ) );
	}

	public function getLeaseSignedOn() {
		return $this->m_strLeaseSignedOn;
	}

	public function sqlLeaseSignedOn() {
		return ( true == isset( $this->m_strLeaseSignedOn ) ) ? '\'' . $this->m_strLeaseSignedOn . '\'' : 'NULL';
	}

	public function setLeaseSignature( $strLeaseSignature ) {
		$this->set( 'm_strLeaseSignature', CStrings::strTrimDef( $strLeaseSignature, 240, NULL, true ) );
	}

	public function getLeaseSignature() {
		return $this->m_strLeaseSignature;
	}

	public function sqlLeaseSignature() {
		return ( true == isset( $this->m_strLeaseSignature ) ) ? '\'' . addslashes( $this->m_strLeaseSignature ) . '\'' : 'NULL';
	}

	public function setLeaseIpAddress( $strLeaseIpAddress ) {
		$this->set( 'm_strLeaseIpAddress', CStrings::strTrimDef( $strLeaseIpAddress, 23, NULL, true ) );
	}

	public function getLeaseIpAddress() {
		return $this->m_strLeaseIpAddress;
	}

	public function sqlLeaseIpAddress() {
		return ( true == isset( $this->m_strLeaseIpAddress ) ) ? '\'' . addslashes( $this->m_strLeaseIpAddress ) . '\'' : 'NULL';
	}

	public function setBlueMoonApplicationSentOn( $strBlueMoonApplicationSentOn ) {
		$this->set( 'm_strBlueMoonApplicationSentOn', CStrings::strTrimDef( $strBlueMoonApplicationSentOn, -1, NULL, true ) );
	}

	public function getBlueMoonApplicationSentOn() {
		return $this->m_strBlueMoonApplicationSentOn;
	}

	public function sqlBlueMoonApplicationSentOn() {
		return ( true == isset( $this->m_strBlueMoonApplicationSentOn ) ) ? '\'' . $this->m_strBlueMoonApplicationSentOn . '\'' : 'NULL';
	}

	public function setHasVisited( $intHasVisited ) {
		$this->set( 'm_intHasVisited', CStrings::strToIntDef( $intHasVisited, NULL, false ) );
	}

	public function getHasVisited() {
		return $this->m_intHasVisited;
	}

	public function sqlHasVisited() {
		return ( true == isset( $this->m_intHasVisited ) ) ? ( string ) $this->m_intHasVisited : '0';
	}

	public function setPrimaryIsResponsible( $intPrimaryIsResponsible ) {
		$this->set( 'm_intPrimaryIsResponsible', CStrings::strToIntDef( $intPrimaryIsResponsible, NULL, false ) );
	}

	public function getPrimaryIsResponsible() {
		return $this->m_intPrimaryIsResponsible;
	}

	public function sqlPrimaryIsResponsible() {
		return ( true == isset( $this->m_intPrimaryIsResponsible ) ) ? ( string ) $this->m_intPrimaryIsResponsible : '0';
	}

	public function setLeaseInvitationSent( $intLeaseInvitationSent ) {
		$this->set( 'm_intLeaseInvitationSent', CStrings::strToIntDef( $intLeaseInvitationSent, NULL, false ) );
	}

	public function getLeaseInvitationSent() {
		return $this->m_intLeaseInvitationSent;
	}

	public function sqlLeaseInvitationSent() {
		return ( true == isset( $this->m_intLeaseInvitationSent ) ) ? ( string ) $this->m_intLeaseInvitationSent : '0';
	}

	public function setIsLead( $intIsLead ) {
		$this->set( 'm_intIsLead', CStrings::strToIntDef( $intIsLead, NULL, false ) );
	}

	public function getIsLead() {
		return $this->m_intIsLead;
	}

	public function sqlIsLead() {
		return ( true == isset( $this->m_intIsLead ) ) ? ( string ) $this->m_intIsLead : '1';
	}

	public function setDesiredVehicles( $intDesiredVehicles ) {
		$this->set( 'm_intDesiredVehicles', CStrings::strToIntDef( $intDesiredVehicles, NULL, false ) );
	}

	public function getDesiredVehicles() {
		return $this->m_intDesiredVehicles;
	}

	public function sqlDesiredVehicles() {
		return ( true == isset( $this->m_intDesiredVehicles ) ) ? ( string ) $this->m_intDesiredVehicles : 'NULL';
	}

	public function setSendConsumerReport( $boolSendConsumerReport ) {
		$this->set( 'm_boolSendConsumerReport', CStrings::strToBool( $boolSendConsumerReport ) );
	}

	public function getSendConsumerReport() {
		return $this->m_boolSendConsumerReport;
	}

	public function sqlSendConsumerReport() {
		return ( true == isset( $this->m_boolSendConsumerReport ) ) ? '\'' . ( true == ( bool ) $this->m_boolSendConsumerReport ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setStartedOn( $strStartedOn ) {
		$this->set( 'm_strStartedOn', CStrings::strTrimDef( $strStartedOn, -1, NULL, true ) );
	}

	public function getStartedOn() {
		return $this->m_strStartedOn;
	}

	public function sqlStartedOn() {
		return ( true == isset( $this->m_strStartedOn ) ) ? '\'' . $this->m_strStartedOn . '\'' : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setOfferSentOn( $strOfferSentOn ) {
		$this->set( 'm_strOfferSentOn', CStrings::strTrimDef( $strOfferSentOn, -1, NULL, true ) );
	}

	public function getOfferSentOn() {
		return $this->m_strOfferSentOn;
	}

	public function sqlOfferSentOn() {
		return ( true == isset( $this->m_strOfferSentOn ) ) ? '\'' . $this->m_strOfferSentOn . '\'' : 'NULL';
	}

	public function setLockedBy( $intLockedBy ) {
		$this->set( 'm_intLockedBy', CStrings::strToIntDef( $intLockedBy, NULL, false ) );
	}

	public function getLockedBy() {
		return $this->m_intLockedBy;
	}

	public function sqlLockedBy() {
		return ( true == isset( $this->m_intLockedBy ) ) ? ( string ) $this->m_intLockedBy : 'NULL';
	}

	public function setLockedOn( $strLockedOn ) {
		$this->set( 'm_strLockedOn', CStrings::strTrimDef( $strLockedOn, -1, NULL, true ) );
	}

	public function getLockedOn() {
		return $this->m_strLockedOn;
	}

	public function sqlLockedOn() {
		return ( true == isset( $this->m_strLockedOn ) ) ? '\'' . $this->m_strLockedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, applicant_id, application_id, lease_status_type_id, customer_type_id, application_step_id, application_document_id, application_form_id, applicant_move_in_reason_id, lease_document_id, customer_relationship_id, guest_remote_primary_key, app_remote_primary_key, renewal_remote_primary_key, additional_info, application_signed_on, application_signature, application_ip_address, require_screening, lease_generated_on, is_lease_partially_generated, lease_skipped_on, lease_signed_on, lease_signature, lease_ip_address, blue_moon_application_sent_on, has_visited, primary_is_responsible, lease_invitation_sent, is_lead, desired_vehicles, send_consumer_report, started_on, completed_on, approved_on, approved_by, exported_on, offer_sent_on, locked_by, locked_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlApplicantId() . ', ' .
						$this->sqlApplicationId() . ', ' .
						$this->sqlLeaseStatusTypeId() . ', ' .
						$this->sqlCustomerTypeId() . ', ' .
						$this->sqlApplicationStepId() . ', ' .
						$this->sqlApplicationDocumentId() . ', ' .
						$this->sqlApplicationFormId() . ', ' .
						$this->sqlApplicantMoveInReasonId() . ', ' .
						$this->sqlLeaseDocumentId() . ', ' .
						$this->sqlCustomerRelationshipId() . ', ' .
						$this->sqlGuestRemotePrimaryKey() . ', ' .
						$this->sqlAppRemotePrimaryKey() . ', ' .
						$this->sqlRenewalRemotePrimaryKey() . ', ' .
						$this->sqlAdditionalInfo() . ', ' .
						$this->sqlApplicationSignedOn() . ', ' .
						$this->sqlApplicationSignature() . ', ' .
						$this->sqlApplicationIpAddress() . ', ' .
						$this->sqlRequireScreening() . ', ' .
						$this->sqlLeaseGeneratedOn() . ', ' .
						$this->sqlIsLeasePartiallyGenerated() . ', ' .
						$this->sqlLeaseSkippedOn() . ', ' .
						$this->sqlLeaseSignedOn() . ', ' .
						$this->sqlLeaseSignature() . ', ' .
						$this->sqlLeaseIpAddress() . ', ' .
						$this->sqlBlueMoonApplicationSentOn() . ', ' .
						$this->sqlHasVisited() . ', ' .
						$this->sqlPrimaryIsResponsible() . ', ' .
						$this->sqlLeaseInvitationSent() . ', ' .
						$this->sqlIsLead() . ', ' .
						$this->sqlDesiredVehicles() . ', ' .
						$this->sqlSendConsumerReport() . ', ' .
						$this->sqlStartedOn() . ', ' .
						$this->sqlCompletedOn() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlExportedOn() . ', ' .
						$this->sqlOfferSentOn() . ', ' .
						$this->sqlLockedBy() . ', ' .
						$this->sqlLockedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; } elseif( true == array_key_exists( 'ApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_status_type_id = ' . $this->sqlLeaseStatusTypeId() . ','; } elseif( true == array_key_exists( 'LeaseStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_status_type_id = ' . $this->sqlLeaseStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_type_id = ' . $this->sqlCustomerTypeId() . ','; } elseif( true == array_key_exists( 'CustomerTypeId', $this->getChangedColumns() ) ) { $strSql .= ' customer_type_id = ' . $this->sqlCustomerTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_step_id = ' . $this->sqlApplicationStepId() . ','; } elseif( true == array_key_exists( 'ApplicationStepId', $this->getChangedColumns() ) ) { $strSql .= ' application_step_id = ' . $this->sqlApplicationStepId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_document_id = ' . $this->sqlApplicationDocumentId() . ','; } elseif( true == array_key_exists( 'ApplicationDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' application_document_id = ' . $this->sqlApplicationDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_form_id = ' . $this->sqlApplicationFormId() . ','; } elseif( true == array_key_exists( 'ApplicationFormId', $this->getChangedColumns() ) ) { $strSql .= ' application_form_id = ' . $this->sqlApplicationFormId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_move_in_reason_id = ' . $this->sqlApplicantMoveInReasonId() . ','; } elseif( true == array_key_exists( 'ApplicantMoveInReasonId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_move_in_reason_id = ' . $this->sqlApplicantMoveInReasonId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_document_id = ' . $this->sqlLeaseDocumentId() . ','; } elseif( true == array_key_exists( 'LeaseDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' lease_document_id = ' . $this->sqlLeaseDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_relationship_id = ' . $this->sqlCustomerRelationshipId() . ','; } elseif( true == array_key_exists( 'CustomerRelationshipId', $this->getChangedColumns() ) ) { $strSql .= ' customer_relationship_id = ' . $this->sqlCustomerRelationshipId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' guest_remote_primary_key = ' . $this->sqlGuestRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'GuestRemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' guest_remote_primary_key = ' . $this->sqlGuestRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' app_remote_primary_key = ' . $this->sqlAppRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'AppRemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' app_remote_primary_key = ' . $this->sqlAppRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' renewal_remote_primary_key = ' . $this->sqlRenewalRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RenewalRemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' renewal_remote_primary_key = ' . $this->sqlRenewalRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' additional_info = ' . $this->sqlAdditionalInfo() . ','; } elseif( true == array_key_exists( 'AdditionalInfo', $this->getChangedColumns() ) ) { $strSql .= ' additional_info = ' . $this->sqlAdditionalInfo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_signed_on = ' . $this->sqlApplicationSignedOn() . ','; } elseif( true == array_key_exists( 'ApplicationSignedOn', $this->getChangedColumns() ) ) { $strSql .= ' application_signed_on = ' . $this->sqlApplicationSignedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_signature = ' . $this->sqlApplicationSignature() . ','; } elseif( true == array_key_exists( 'ApplicationSignature', $this->getChangedColumns() ) ) { $strSql .= ' application_signature = ' . $this->sqlApplicationSignature() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_ip_address = ' . $this->sqlApplicationIpAddress() . ','; } elseif( true == array_key_exists( 'ApplicationIpAddress', $this->getChangedColumns() ) ) { $strSql .= ' application_ip_address = ' . $this->sqlApplicationIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_screening = ' . $this->sqlRequireScreening() . ','; } elseif( true == array_key_exists( 'RequireScreening', $this->getChangedColumns() ) ) { $strSql .= ' require_screening = ' . $this->sqlRequireScreening() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_generated_on = ' . $this->sqlLeaseGeneratedOn() . ','; } elseif( true == array_key_exists( 'LeaseGeneratedOn', $this->getChangedColumns() ) ) { $strSql .= ' lease_generated_on = ' . $this->sqlLeaseGeneratedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_lease_partially_generated = ' . $this->sqlIsLeasePartiallyGenerated() . ','; } elseif( true == array_key_exists( 'IsLeasePartiallyGenerated', $this->getChangedColumns() ) ) { $strSql .= ' is_lease_partially_generated = ' . $this->sqlIsLeasePartiallyGenerated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_skipped_on = ' . $this->sqlLeaseSkippedOn() . ','; } elseif( true == array_key_exists( 'LeaseSkippedOn', $this->getChangedColumns() ) ) { $strSql .= ' lease_skipped_on = ' . $this->sqlLeaseSkippedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_signed_on = ' . $this->sqlLeaseSignedOn() . ','; } elseif( true == array_key_exists( 'LeaseSignedOn', $this->getChangedColumns() ) ) { $strSql .= ' lease_signed_on = ' . $this->sqlLeaseSignedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_signature = ' . $this->sqlLeaseSignature() . ','; } elseif( true == array_key_exists( 'LeaseSignature', $this->getChangedColumns() ) ) { $strSql .= ' lease_signature = ' . $this->sqlLeaseSignature() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_ip_address = ' . $this->sqlLeaseIpAddress() . ','; } elseif( true == array_key_exists( 'LeaseIpAddress', $this->getChangedColumns() ) ) { $strSql .= ' lease_ip_address = ' . $this->sqlLeaseIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' blue_moon_application_sent_on = ' . $this->sqlBlueMoonApplicationSentOn() . ','; } elseif( true == array_key_exists( 'BlueMoonApplicationSentOn', $this->getChangedColumns() ) ) { $strSql .= ' blue_moon_application_sent_on = ' . $this->sqlBlueMoonApplicationSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_visited = ' . $this->sqlHasVisited() . ','; } elseif( true == array_key_exists( 'HasVisited', $this->getChangedColumns() ) ) { $strSql .= ' has_visited = ' . $this->sqlHasVisited() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_is_responsible = ' . $this->sqlPrimaryIsResponsible() . ','; } elseif( true == array_key_exists( 'PrimaryIsResponsible', $this->getChangedColumns() ) ) { $strSql .= ' primary_is_responsible = ' . $this->sqlPrimaryIsResponsible() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_invitation_sent = ' . $this->sqlLeaseInvitationSent() . ','; } elseif( true == array_key_exists( 'LeaseInvitationSent', $this->getChangedColumns() ) ) { $strSql .= ' lease_invitation_sent = ' . $this->sqlLeaseInvitationSent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_lead = ' . $this->sqlIsLead() . ','; } elseif( true == array_key_exists( 'IsLead', $this->getChangedColumns() ) ) { $strSql .= ' is_lead = ' . $this->sqlIsLead() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desired_vehicles = ' . $this->sqlDesiredVehicles() . ','; } elseif( true == array_key_exists( 'DesiredVehicles', $this->getChangedColumns() ) ) { $strSql .= ' desired_vehicles = ' . $this->sqlDesiredVehicles() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' send_consumer_report = ' . $this->sqlSendConsumerReport() . ','; } elseif( true == array_key_exists( 'SendConsumerReport', $this->getChangedColumns() ) ) { $strSql .= ' send_consumer_report = ' . $this->sqlSendConsumerReport() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' started_on = ' . $this->sqlStartedOn() . ','; } elseif( true == array_key_exists( 'StartedOn', $this->getChangedColumns() ) ) { $strSql .= ' started_on = ' . $this->sqlStartedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' offer_sent_on = ' . $this->sqlOfferSentOn() . ','; } elseif( true == array_key_exists( 'OfferSentOn', $this->getChangedColumns() ) ) { $strSql .= ' offer_sent_on = ' . $this->sqlOfferSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locked_by = ' . $this->sqlLockedBy() . ','; } elseif( true == array_key_exists( 'LockedBy', $this->getChangedColumns() ) ) { $strSql .= ' locked_by = ' . $this->sqlLockedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn() . ','; } elseif( true == array_key_exists( 'LockedOn', $this->getChangedColumns() ) ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'applicant_id' => $this->getApplicantId(),
			'application_id' => $this->getApplicationId(),
			'lease_status_type_id' => $this->getLeaseStatusTypeId(),
			'customer_type_id' => $this->getCustomerTypeId(),
			'application_step_id' => $this->getApplicationStepId(),
			'application_document_id' => $this->getApplicationDocumentId(),
			'application_form_id' => $this->getApplicationFormId(),
			'applicant_move_in_reason_id' => $this->getApplicantMoveInReasonId(),
			'lease_document_id' => $this->getLeaseDocumentId(),
			'customer_relationship_id' => $this->getCustomerRelationshipId(),
			'guest_remote_primary_key' => $this->getGuestRemotePrimaryKey(),
			'app_remote_primary_key' => $this->getAppRemotePrimaryKey(),
			'renewal_remote_primary_key' => $this->getRenewalRemotePrimaryKey(),
			'additional_info' => $this->getAdditionalInfo(),
			'application_signed_on' => $this->getApplicationSignedOn(),
			'application_signature' => $this->getApplicationSignature(),
			'application_ip_address' => $this->getApplicationIpAddress(),
			'require_screening' => $this->getRequireScreening(),
			'lease_generated_on' => $this->getLeaseGeneratedOn(),
			'is_lease_partially_generated' => $this->getIsLeasePartiallyGenerated(),
			'lease_skipped_on' => $this->getLeaseSkippedOn(),
			'lease_signed_on' => $this->getLeaseSignedOn(),
			'lease_signature' => $this->getLeaseSignature(),
			'lease_ip_address' => $this->getLeaseIpAddress(),
			'blue_moon_application_sent_on' => $this->getBlueMoonApplicationSentOn(),
			'has_visited' => $this->getHasVisited(),
			'primary_is_responsible' => $this->getPrimaryIsResponsible(),
			'lease_invitation_sent' => $this->getLeaseInvitationSent(),
			'is_lead' => $this->getIsLead(),
			'desired_vehicles' => $this->getDesiredVehicles(),
			'send_consumer_report' => $this->getSendConsumerReport(),
			'started_on' => $this->getStartedOn(),
			'completed_on' => $this->getCompletedOn(),
			'approved_on' => $this->getApprovedOn(),
			'approved_by' => $this->getApprovedBy(),
			'exported_on' => $this->getExportedOn(),
			'offer_sent_on' => $this->getOfferSentOn(),
			'locked_by' => $this->getLockedBy(),
			'locked_on' => $this->getLockedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>