<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultGroupPermissions
 * Do not add any new functions to this class.
 */

class CBaseDefaultGroupPermissions extends CEosPluralBase {

	/**
	 * @return CDefaultGroupPermission[]
	 */
	public static function fetchDefaultGroupPermissions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDefaultGroupPermission', $objDatabase );
	}

	/**
	 * @return CDefaultGroupPermission
	 */
	public static function fetchDefaultGroupPermission( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDefaultGroupPermission', $objDatabase );
	}

	public static function fetchDefaultGroupPermissionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_group_permissions', $objDatabase );
	}

	public static function fetchDefaultGroupPermissionById( $intId, $objDatabase ) {
		return self::fetchDefaultGroupPermission( sprintf( 'SELECT * FROM default_group_permissions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDefaultGroupPermissionsByModuleId( $intModuleId, $objDatabase ) {
		return self::fetchDefaultGroupPermissions( sprintf( 'SELECT * FROM default_group_permissions WHERE module_id = %d', ( int ) $intModuleId ), $objDatabase );
	}

}
?>