<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseTransmission extends CEosSingularBase {

	const TABLE_NAME = 'public.transmissions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intTransmissionTypeId;
	protected $m_intTransmissionVendorId;
	protected $m_intCompanyTransmissionVendorId;
	protected $m_intPropertyId;
	protected $m_intApplicationId;
	protected $m_intLeaseId;
	protected $m_intCustomerId;
	protected $m_intApplicantApplicationId;
	protected $m_intTransmissionResponseTypeId;
	protected $m_intScreeningConfigurationId;
	protected $m_strTransmissionDatetime;
	protected $m_strTransmissionRemotePrimaryKey;
	protected $m_strRemotePrimaryKey;
	protected $m_intLeaseScore;
	protected $m_strResponse;
	protected $m_strMessage;
	protected $m_intHasCriminalFlag;
	protected $m_intHasCreditAlert;
	protected $m_intHasDataError;
	protected $m_intIsFailed;
	protected $m_strRequestFileName;
	protected $m_strRequestFilePath;
	protected $m_strResponseFileName;
	protected $m_strResponseFilePath;
	protected $m_boolUseAmazonFileStorage;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intHasCriminalFlag = '0';
		$this->m_intHasCreditAlert = '0';
		$this->m_intHasDataError = '0';
		$this->m_intIsFailed = '0';
		$this->m_boolUseAmazonFileStorage = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['transmission_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionTypeId', trim( $arrValues['transmission_type_id'] ) ); elseif( isset( $arrValues['transmission_type_id'] ) ) $this->setTransmissionTypeId( $arrValues['transmission_type_id'] );
		if( isset( $arrValues['transmission_vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionVendorId', trim( $arrValues['transmission_vendor_id'] ) ); elseif( isset( $arrValues['transmission_vendor_id'] ) ) $this->setTransmissionVendorId( $arrValues['transmission_vendor_id'] );
		if( isset( $arrValues['company_transmission_vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyTransmissionVendorId', trim( $arrValues['company_transmission_vendor_id'] ) ); elseif( isset( $arrValues['company_transmission_vendor_id'] ) ) $this->setCompanyTransmissionVendorId( $arrValues['company_transmission_vendor_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['applicant_application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantApplicationId', trim( $arrValues['applicant_application_id'] ) ); elseif( isset( $arrValues['applicant_application_id'] ) ) $this->setApplicantApplicationId( $arrValues['applicant_application_id'] );
		if( isset( $arrValues['transmission_response_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionResponseTypeId', trim( $arrValues['transmission_response_type_id'] ) ); elseif( isset( $arrValues['transmission_response_type_id'] ) ) $this->setTransmissionResponseTypeId( $arrValues['transmission_response_type_id'] );
		if( isset( $arrValues['screening_configuration_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningConfigurationId', trim( $arrValues['screening_configuration_id'] ) ); elseif( isset( $arrValues['screening_configuration_id'] ) ) $this->setScreeningConfigurationId( $arrValues['screening_configuration_id'] );
		if( isset( $arrValues['transmission_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTransmissionDatetime', trim( $arrValues['transmission_datetime'] ) ); elseif( isset( $arrValues['transmission_datetime'] ) ) $this->setTransmissionDatetime( $arrValues['transmission_datetime'] );
		if( isset( $arrValues['transmission_remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strTransmissionRemotePrimaryKey', trim( stripcslashes( $arrValues['transmission_remote_primary_key'] ) ) ); elseif( isset( $arrValues['transmission_remote_primary_key'] ) ) $this->setTransmissionRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['transmission_remote_primary_key'] ) : $arrValues['transmission_remote_primary_key'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['lease_score'] ) && $boolDirectSet ) $this->set( 'm_intLeaseScore', trim( $arrValues['lease_score'] ) ); elseif( isset( $arrValues['lease_score'] ) ) $this->setLeaseScore( $arrValues['lease_score'] );
		if( isset( $arrValues['response'] ) && $boolDirectSet ) $this->set( 'm_strResponse', trim( stripcslashes( $arrValues['response'] ) ) ); elseif( isset( $arrValues['response'] ) ) $this->setResponse( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['response'] ) : $arrValues['response'] );
		if( isset( $arrValues['message'] ) && $boolDirectSet ) $this->set( 'm_strMessage', trim( stripcslashes( $arrValues['message'] ) ) ); elseif( isset( $arrValues['message'] ) ) $this->setMessage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['message'] ) : $arrValues['message'] );
		if( isset( $arrValues['has_criminal_flag'] ) && $boolDirectSet ) $this->set( 'm_intHasCriminalFlag', trim( $arrValues['has_criminal_flag'] ) ); elseif( isset( $arrValues['has_criminal_flag'] ) ) $this->setHasCriminalFlag( $arrValues['has_criminal_flag'] );
		if( isset( $arrValues['has_credit_alert'] ) && $boolDirectSet ) $this->set( 'm_intHasCreditAlert', trim( $arrValues['has_credit_alert'] ) ); elseif( isset( $arrValues['has_credit_alert'] ) ) $this->setHasCreditAlert( $arrValues['has_credit_alert'] );
		if( isset( $arrValues['has_data_error'] ) && $boolDirectSet ) $this->set( 'm_intHasDataError', trim( $arrValues['has_data_error'] ) ); elseif( isset( $arrValues['has_data_error'] ) ) $this->setHasDataError( $arrValues['has_data_error'] );
		if( isset( $arrValues['is_failed'] ) && $boolDirectSet ) $this->set( 'm_intIsFailed', trim( $arrValues['is_failed'] ) ); elseif( isset( $arrValues['is_failed'] ) ) $this->setIsFailed( $arrValues['is_failed'] );
		if( isset( $arrValues['request_file_name'] ) && $boolDirectSet ) $this->set( 'm_strRequestFileName', trim( stripcslashes( $arrValues['request_file_name'] ) ) ); elseif( isset( $arrValues['request_file_name'] ) ) $this->setRequestFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['request_file_name'] ) : $arrValues['request_file_name'] );
		if( isset( $arrValues['request_file_path'] ) && $boolDirectSet ) $this->set( 'm_strRequestFilePath', trim( stripcslashes( $arrValues['request_file_path'] ) ) ); elseif( isset( $arrValues['request_file_path'] ) ) $this->setRequestFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['request_file_path'] ) : $arrValues['request_file_path'] );
		if( isset( $arrValues['response_file_name'] ) && $boolDirectSet ) $this->set( 'm_strResponseFileName', trim( stripcslashes( $arrValues['response_file_name'] ) ) ); elseif( isset( $arrValues['response_file_name'] ) ) $this->setResponseFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['response_file_name'] ) : $arrValues['response_file_name'] );
		if( isset( $arrValues['response_file_path'] ) && $boolDirectSet ) $this->set( 'm_strResponseFilePath', trim( stripcslashes( $arrValues['response_file_path'] ) ) ); elseif( isset( $arrValues['response_file_path'] ) ) $this->setResponseFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['response_file_path'] ) : $arrValues['response_file_path'] );
		if( isset( $arrValues['use_amazon_file_storage'] ) && $boolDirectSet ) $this->set( 'm_boolUseAmazonFileStorage', trim( stripcslashes( $arrValues['use_amazon_file_storage'] ) ) ); elseif( isset( $arrValues['use_amazon_file_storage'] ) ) $this->setUseAmazonFileStorage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_amazon_file_storage'] ) : $arrValues['use_amazon_file_storage'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setTransmissionTypeId( $intTransmissionTypeId ) {
		$this->set( 'm_intTransmissionTypeId', CStrings::strToIntDef( $intTransmissionTypeId, NULL, false ) );
	}

	public function getTransmissionTypeId() {
		return $this->m_intTransmissionTypeId;
	}

	public function sqlTransmissionTypeId() {
		return ( true == isset( $this->m_intTransmissionTypeId ) ) ? ( string ) $this->m_intTransmissionTypeId : 'NULL';
	}

	public function setTransmissionVendorId( $intTransmissionVendorId ) {
		$this->set( 'm_intTransmissionVendorId', CStrings::strToIntDef( $intTransmissionVendorId, NULL, false ) );
	}

	public function getTransmissionVendorId() {
		return $this->m_intTransmissionVendorId;
	}

	public function sqlTransmissionVendorId() {
		return ( true == isset( $this->m_intTransmissionVendorId ) ) ? ( string ) $this->m_intTransmissionVendorId : 'NULL';
	}

	public function setCompanyTransmissionVendorId( $intCompanyTransmissionVendorId ) {
		$this->set( 'm_intCompanyTransmissionVendorId', CStrings::strToIntDef( $intCompanyTransmissionVendorId, NULL, false ) );
	}

	public function getCompanyTransmissionVendorId() {
		return $this->m_intCompanyTransmissionVendorId;
	}

	public function sqlCompanyTransmissionVendorId() {
		return ( true == isset( $this->m_intCompanyTransmissionVendorId ) ) ? ( string ) $this->m_intCompanyTransmissionVendorId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setApplicantApplicationId( $intApplicantApplicationId ) {
		$this->set( 'm_intApplicantApplicationId', CStrings::strToIntDef( $intApplicantApplicationId, NULL, false ) );
	}

	public function getApplicantApplicationId() {
		return $this->m_intApplicantApplicationId;
	}

	public function sqlApplicantApplicationId() {
		return ( true == isset( $this->m_intApplicantApplicationId ) ) ? ( string ) $this->m_intApplicantApplicationId : 'NULL';
	}

	public function setTransmissionResponseTypeId( $intTransmissionResponseTypeId ) {
		$this->set( 'm_intTransmissionResponseTypeId', CStrings::strToIntDef( $intTransmissionResponseTypeId, NULL, false ) );
	}

	public function getTransmissionResponseTypeId() {
		return $this->m_intTransmissionResponseTypeId;
	}

	public function sqlTransmissionResponseTypeId() {
		return ( true == isset( $this->m_intTransmissionResponseTypeId ) ) ? ( string ) $this->m_intTransmissionResponseTypeId : 'NULL';
	}

	public function setScreeningConfigurationId( $intScreeningConfigurationId ) {
		$this->set( 'm_intScreeningConfigurationId', CStrings::strToIntDef( $intScreeningConfigurationId, NULL, false ) );
	}

	public function getScreeningConfigurationId() {
		return $this->m_intScreeningConfigurationId;
	}

	public function sqlScreeningConfigurationId() {
		return ( true == isset( $this->m_intScreeningConfigurationId ) ) ? ( string ) $this->m_intScreeningConfigurationId : 'NULL';
	}

	public function setTransmissionDatetime( $strTransmissionDatetime ) {
		$this->set( 'm_strTransmissionDatetime', CStrings::strTrimDef( $strTransmissionDatetime, -1, NULL, true ) );
	}

	public function getTransmissionDatetime() {
		return $this->m_strTransmissionDatetime;
	}

	public function sqlTransmissionDatetime() {
		return ( true == isset( $this->m_strTransmissionDatetime ) ) ? '\'' . $this->m_strTransmissionDatetime . '\'' : 'NOW()';
	}

	public function setTransmissionRemotePrimaryKey( $strTransmissionRemotePrimaryKey ) {
		$this->set( 'm_strTransmissionRemotePrimaryKey', CStrings::strTrimDef( $strTransmissionRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getTransmissionRemotePrimaryKey() {
		return $this->m_strTransmissionRemotePrimaryKey;
	}

	public function sqlTransmissionRemotePrimaryKey() {
		return ( true == isset( $this->m_strTransmissionRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strTransmissionRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setLeaseScore( $intLeaseScore ) {
		$this->set( 'm_intLeaseScore', CStrings::strToIntDef( $intLeaseScore, NULL, false ) );
	}

	public function getLeaseScore() {
		return $this->m_intLeaseScore;
	}

	public function sqlLeaseScore() {
		return ( true == isset( $this->m_intLeaseScore ) ) ? ( string ) $this->m_intLeaseScore : 'NULL';
	}

	public function setResponse( $strResponse ) {
		$this->set( 'm_strResponse', CStrings::strTrimDef( $strResponse, -1, NULL, true ) );
	}

	public function getResponse() {
		return $this->m_strResponse;
	}

	public function sqlResponse() {
		return ( true == isset( $this->m_strResponse ) ) ? '\'' . addslashes( $this->m_strResponse ) . '\'' : 'NULL';
	}

	public function setMessage( $strMessage ) {
		$this->set( 'm_strMessage', CStrings::strTrimDef( $strMessage, -1, NULL, true ) );
	}

	public function getMessage() {
		return $this->m_strMessage;
	}

	public function sqlMessage() {
		return ( true == isset( $this->m_strMessage ) ) ? '\'' . addslashes( $this->m_strMessage ) . '\'' : 'NULL';
	}

	public function setHasCriminalFlag( $intHasCriminalFlag ) {
		$this->set( 'm_intHasCriminalFlag', CStrings::strToIntDef( $intHasCriminalFlag, NULL, false ) );
	}

	public function getHasCriminalFlag() {
		return $this->m_intHasCriminalFlag;
	}

	public function sqlHasCriminalFlag() {
		return ( true == isset( $this->m_intHasCriminalFlag ) ) ? ( string ) $this->m_intHasCriminalFlag : '0';
	}

	public function setHasCreditAlert( $intHasCreditAlert ) {
		$this->set( 'm_intHasCreditAlert', CStrings::strToIntDef( $intHasCreditAlert, NULL, false ) );
	}

	public function getHasCreditAlert() {
		return $this->m_intHasCreditAlert;
	}

	public function sqlHasCreditAlert() {
		return ( true == isset( $this->m_intHasCreditAlert ) ) ? ( string ) $this->m_intHasCreditAlert : '0';
	}

	public function setHasDataError( $intHasDataError ) {
		$this->set( 'm_intHasDataError', CStrings::strToIntDef( $intHasDataError, NULL, false ) );
	}

	public function getHasDataError() {
		return $this->m_intHasDataError;
	}

	public function sqlHasDataError() {
		return ( true == isset( $this->m_intHasDataError ) ) ? ( string ) $this->m_intHasDataError : '0';
	}

	public function setIsFailed( $intIsFailed ) {
		$this->set( 'm_intIsFailed', CStrings::strToIntDef( $intIsFailed, NULL, false ) );
	}

	public function getIsFailed() {
		return $this->m_intIsFailed;
	}

	public function sqlIsFailed() {
		return ( true == isset( $this->m_intIsFailed ) ) ? ( string ) $this->m_intIsFailed : '0';
	}

	public function setRequestFileName( $strRequestFileName ) {
		$this->set( 'm_strRequestFileName', CStrings::strTrimDef( $strRequestFileName, 240, NULL, true ) );
	}

	public function getRequestFileName() {
		return $this->m_strRequestFileName;
	}

	public function sqlRequestFileName() {
		return ( true == isset( $this->m_strRequestFileName ) ) ? '\'' . addslashes( $this->m_strRequestFileName ) . '\'' : 'NULL';
	}

	public function setRequestFilePath( $strRequestFilePath ) {
		$this->set( 'm_strRequestFilePath', CStrings::strTrimDef( $strRequestFilePath, 4096, NULL, true ) );
	}

	public function getRequestFilePath() {
		return $this->m_strRequestFilePath;
	}

	public function sqlRequestFilePath() {
		return ( true == isset( $this->m_strRequestFilePath ) ) ? '\'' . addslashes( $this->m_strRequestFilePath ) . '\'' : 'NULL';
	}

	public function setResponseFileName( $strResponseFileName ) {
		$this->set( 'm_strResponseFileName', CStrings::strTrimDef( $strResponseFileName, 240, NULL, true ) );
	}

	public function getResponseFileName() {
		return $this->m_strResponseFileName;
	}

	public function sqlResponseFileName() {
		return ( true == isset( $this->m_strResponseFileName ) ) ? '\'' . addslashes( $this->m_strResponseFileName ) . '\'' : 'NULL';
	}

	public function setResponseFilePath( $strResponseFilePath ) {
		$this->set( 'm_strResponseFilePath', CStrings::strTrimDef( $strResponseFilePath, 4096, NULL, true ) );
	}

	public function getResponseFilePath() {
		return $this->m_strResponseFilePath;
	}

	public function sqlResponseFilePath() {
		return ( true == isset( $this->m_strResponseFilePath ) ) ? '\'' . addslashes( $this->m_strResponseFilePath ) . '\'' : 'NULL';
	}

	public function setUseAmazonFileStorage( $boolUseAmazonFileStorage ) {
		$this->set( 'm_boolUseAmazonFileStorage', CStrings::strToBool( $boolUseAmazonFileStorage ) );
	}

	public function getUseAmazonFileStorage() {
		return $this->m_boolUseAmazonFileStorage;
	}

	public function sqlUseAmazonFileStorage() {
		return ( true == isset( $this->m_boolUseAmazonFileStorage ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseAmazonFileStorage ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, transmission_type_id, transmission_vendor_id, company_transmission_vendor_id, property_id, application_id, lease_id, customer_id, applicant_application_id, transmission_response_type_id, screening_configuration_id, transmission_datetime, transmission_remote_primary_key, remote_primary_key, lease_score, response, message, has_criminal_flag, has_credit_alert, has_data_error, is_failed, request_file_name, request_file_path, response_file_name, response_file_path, use_amazon_file_storage, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlTransmissionTypeId() . ', ' .
 						$this->sqlTransmissionVendorId() . ', ' .
 						$this->sqlCompanyTransmissionVendorId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlApplicationId() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlApplicantApplicationId() . ', ' .
 						$this->sqlTransmissionResponseTypeId() . ', ' .
 						$this->sqlScreeningConfigurationId() . ', ' .
 						$this->sqlTransmissionDatetime() . ', ' .
 						$this->sqlTransmissionRemotePrimaryKey() . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlLeaseScore() . ', ' .
 						$this->sqlResponse() . ', ' .
 						$this->sqlMessage() . ', ' .
 						$this->sqlHasCriminalFlag() . ', ' .
 						$this->sqlHasCreditAlert() . ', ' .
 						$this->sqlHasDataError() . ', ' .
 						$this->sqlIsFailed() . ', ' .
 						$this->sqlRequestFileName() . ', ' .
 						$this->sqlRequestFilePath() . ', ' .
 						$this->sqlResponseFileName() . ', ' .
 						$this->sqlResponseFilePath() . ', ' .
 						$this->sqlUseAmazonFileStorage() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_type_id = ' . $this->sqlTransmissionTypeId() . ','; } elseif( true == array_key_exists( 'TransmissionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' transmission_type_id = ' . $this->sqlTransmissionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId() . ','; } elseif( true == array_key_exists( 'TransmissionVendorId', $this->getChangedColumns() ) ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_transmission_vendor_id = ' . $this->sqlCompanyTransmissionVendorId() . ','; } elseif( true == array_key_exists( 'CompanyTransmissionVendorId', $this->getChangedColumns() ) ) { $strSql .= ' company_transmission_vendor_id = ' . $this->sqlCompanyTransmissionVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_application_id = ' . $this->sqlApplicantApplicationId() . ','; } elseif( true == array_key_exists( 'ApplicantApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_application_id = ' . $this->sqlApplicantApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_response_type_id = ' . $this->sqlTransmissionResponseTypeId() . ','; } elseif( true == array_key_exists( 'TransmissionResponseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' transmission_response_type_id = ' . $this->sqlTransmissionResponseTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_configuration_id = ' . $this->sqlScreeningConfigurationId() . ','; } elseif( true == array_key_exists( 'ScreeningConfigurationId', $this->getChangedColumns() ) ) { $strSql .= ' screening_configuration_id = ' . $this->sqlScreeningConfigurationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_datetime = ' . $this->sqlTransmissionDatetime() . ','; } elseif( true == array_key_exists( 'TransmissionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' transmission_datetime = ' . $this->sqlTransmissionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_remote_primary_key = ' . $this->sqlTransmissionRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'TransmissionRemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' transmission_remote_primary_key = ' . $this->sqlTransmissionRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_score = ' . $this->sqlLeaseScore() . ','; } elseif( true == array_key_exists( 'LeaseScore', $this->getChangedColumns() ) ) { $strSql .= ' lease_score = ' . $this->sqlLeaseScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response = ' . $this->sqlResponse() . ','; } elseif( true == array_key_exists( 'Response', $this->getChangedColumns() ) ) { $strSql .= ' response = ' . $this->sqlResponse() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message = ' . $this->sqlMessage() . ','; } elseif( true == array_key_exists( 'Message', $this->getChangedColumns() ) ) { $strSql .= ' message = ' . $this->sqlMessage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_criminal_flag = ' . $this->sqlHasCriminalFlag() . ','; } elseif( true == array_key_exists( 'HasCriminalFlag', $this->getChangedColumns() ) ) { $strSql .= ' has_criminal_flag = ' . $this->sqlHasCriminalFlag() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_credit_alert = ' . $this->sqlHasCreditAlert() . ','; } elseif( true == array_key_exists( 'HasCreditAlert', $this->getChangedColumns() ) ) { $strSql .= ' has_credit_alert = ' . $this->sqlHasCreditAlert() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_data_error = ' . $this->sqlHasDataError() . ','; } elseif( true == array_key_exists( 'HasDataError', $this->getChangedColumns() ) ) { $strSql .= ' has_data_error = ' . $this->sqlHasDataError() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_failed = ' . $this->sqlIsFailed() . ','; } elseif( true == array_key_exists( 'IsFailed', $this->getChangedColumns() ) ) { $strSql .= ' is_failed = ' . $this->sqlIsFailed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_file_name = ' . $this->sqlRequestFileName() . ','; } elseif( true == array_key_exists( 'RequestFileName', $this->getChangedColumns() ) ) { $strSql .= ' request_file_name = ' . $this->sqlRequestFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_file_path = ' . $this->sqlRequestFilePath() . ','; } elseif( true == array_key_exists( 'RequestFilePath', $this->getChangedColumns() ) ) { $strSql .= ' request_file_path = ' . $this->sqlRequestFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_file_name = ' . $this->sqlResponseFileName() . ','; } elseif( true == array_key_exists( 'ResponseFileName', $this->getChangedColumns() ) ) { $strSql .= ' response_file_name = ' . $this->sqlResponseFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_file_path = ' . $this->sqlResponseFilePath() . ','; } elseif( true == array_key_exists( 'ResponseFilePath', $this->getChangedColumns() ) ) { $strSql .= ' response_file_path = ' . $this->sqlResponseFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_amazon_file_storage = ' . $this->sqlUseAmazonFileStorage() . ','; } elseif( true == array_key_exists( 'UseAmazonFileStorage', $this->getChangedColumns() ) ) { $strSql .= ' use_amazon_file_storage = ' . $this->sqlUseAmazonFileStorage() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'transmission_type_id' => $this->getTransmissionTypeId(),
			'transmission_vendor_id' => $this->getTransmissionVendorId(),
			'company_transmission_vendor_id' => $this->getCompanyTransmissionVendorId(),
			'property_id' => $this->getPropertyId(),
			'application_id' => $this->getApplicationId(),
			'lease_id' => $this->getLeaseId(),
			'customer_id' => $this->getCustomerId(),
			'applicant_application_id' => $this->getApplicantApplicationId(),
			'transmission_response_type_id' => $this->getTransmissionResponseTypeId(),
			'screening_configuration_id' => $this->getScreeningConfigurationId(),
			'transmission_datetime' => $this->getTransmissionDatetime(),
			'transmission_remote_primary_key' => $this->getTransmissionRemotePrimaryKey(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'lease_score' => $this->getLeaseScore(),
			'response' => $this->getResponse(),
			'message' => $this->getMessage(),
			'has_criminal_flag' => $this->getHasCriminalFlag(),
			'has_credit_alert' => $this->getHasCreditAlert(),
			'has_data_error' => $this->getHasDataError(),
			'is_failed' => $this->getIsFailed(),
			'request_file_name' => $this->getRequestFileName(),
			'request_file_path' => $this->getRequestFilePath(),
			'response_file_name' => $this->getResponseFileName(),
			'response_file_path' => $this->getResponseFilePath(),
			'use_amazon_file_storage' => $this->getUseAmazonFileStorage(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>