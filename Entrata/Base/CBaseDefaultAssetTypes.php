<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultAssetTypes
 * Do not add any new functions to this class.
 */

class CBaseDefaultAssetTypes extends CEosPluralBase {

	/**
	 * @return CDefaultAssetType[]
	 */
	public static function fetchDefaultAssetTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultAssetType::class, $objDatabase );
	}

	/**
	 * @return CDefaultAssetType
	 */
	public static function fetchDefaultAssetType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultAssetType::class, $objDatabase );
	}

	public static function fetchDefaultAssetTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_asset_types', $objDatabase );
	}

	public static function fetchDefaultAssetTypeById( $intId, $objDatabase ) {
		return self::fetchDefaultAssetType( sprintf( 'SELECT * FROM default_asset_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>