<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COfferItemLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOfferItemLogs extends CEosPluralBase {

	/**
	 * @return COfferItemLog[]
	 */
	public static function fetchOfferItemLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, COfferItemLog::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return COfferItemLog
	 */
	public static function fetchOfferItemLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, COfferItemLog::class, $objDatabase );
	}

	public static function fetchOfferItemLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'offer_item_logs', $objDatabase );
	}

	public static function fetchOfferItemLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchOfferItemLog( sprintf( 'SELECT * FROM offer_item_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferItemLogsByCid( $intCid, $objDatabase ) {
		return self::fetchOfferItemLogs( sprintf( 'SELECT * FROM offer_item_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferItemLogsByOfferItemIdByCid( $intOfferItemId, $intCid, $objDatabase ) {
		return self::fetchOfferItemLogs( sprintf( 'SELECT * FROM offer_item_logs WHERE offer_item_id = %d AND cid = %d', ( int ) $intOfferItemId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferItemLogsByOfferIdByCid( $intOfferId, $intCid, $objDatabase ) {
		return self::fetchOfferItemLogs( sprintf( 'SELECT * FROM offer_item_logs WHERE offer_id = %d AND cid = %d', ( int ) $intOfferId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferItemLogsByArOriginIdByCid( $intArOriginId, $intCid, $objDatabase ) {
		return self::fetchOfferItemLogs( sprintf( 'SELECT * FROM offer_item_logs WHERE ar_origin_id = %d AND cid = %d', ( int ) $intArOriginId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferItemLogsByArOriginReferenceIdByCid( $intArOriginReferenceId, $intCid, $objDatabase ) {
		return self::fetchOfferItemLogs( sprintf( 'SELECT * FROM offer_item_logs WHERE ar_origin_reference_id = %d AND cid = %d', ( int ) $intArOriginReferenceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferItemLogsByPeriodIdByCid( $intPeriodId, $intCid, $objDatabase ) {
		return self::fetchOfferItemLogs( sprintf( 'SELECT * FROM offer_item_logs WHERE period_id = %d AND cid = %d', ( int ) $intPeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferItemLogsByReportingPeriodIdByCid( $intReportingPeriodId, $intCid, $objDatabase ) {
		return self::fetchOfferItemLogs( sprintf( 'SELECT * FROM offer_item_logs WHERE reporting_period_id = %d AND cid = %d', ( int ) $intReportingPeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferItemLogsByEffectivePeriodIdByCid( $intEffectivePeriodId, $intCid, $objDatabase ) {
		return self::fetchOfferItemLogs( sprintf( 'SELECT * FROM offer_item_logs WHERE effective_period_id = %d AND cid = %d', ( int ) $intEffectivePeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferItemLogsByOriginalPeriodIdByCid( $intOriginalPeriodId, $intCid, $objDatabase ) {
		return self::fetchOfferItemLogs( sprintf( 'SELECT * FROM offer_item_logs WHERE original_period_id = %d AND cid = %d', ( int ) $intOriginalPeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferItemLogsByPriorOfferItemLogIdByCid( $intPriorOfferItemLogId, $intCid, $objDatabase ) {
		return self::fetchOfferItemLogs( sprintf( 'SELECT * FROM offer_item_logs WHERE prior_offer_item_log_id = %d AND cid = %d', ( int ) $intPriorOfferItemLogId, ( int ) $intCid ), $objDatabase );
	}

}
?>