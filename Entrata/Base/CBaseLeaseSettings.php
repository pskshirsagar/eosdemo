<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseSettings
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseSettings extends CEosPluralBase {

	/**
	 * @return CLeaseSetting[]
	 */
	public static function fetchLeaseSettings( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CLeaseSetting', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CLeaseSetting
	 */
	public static function fetchLeaseSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CLeaseSetting', $objDatabase );
	}

	public static function fetchLeaseSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'lease_settings', $objDatabase );
	}

	public static function fetchLeaseSettingByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchLeaseSetting( sprintf( 'SELECT * FROM lease_settings WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseSettingsByCid( $intCid, $objDatabase ) {
		return self::fetchLeaseSettings( sprintf( 'SELECT * FROM lease_settings WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseSettingsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchLeaseSettings( sprintf( 'SELECT * FROM lease_settings WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

}
?>