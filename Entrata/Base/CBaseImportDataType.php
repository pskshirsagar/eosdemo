<?php

class CBaseImportDataType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.import_data_types';

	protected $m_intId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_arrintImportTypeIds;
	protected $m_intParentImportDataTypeId;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['import_type_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintImportTypeIds', trim( $arrValues['import_type_ids'] ) ); elseif( isset( $arrValues['import_type_ids'] ) ) $this->setImportTypeIds( $arrValues['import_type_ids'] );
		if( isset( $arrValues['parent_import_data_type_id'] ) && $boolDirectSet ) $this->set( 'm_intParentImportDataTypeId', trim( $arrValues['parent_import_data_type_id'] ) ); elseif( isset( $arrValues['parent_import_data_type_id'] ) ) $this->setParentImportDataTypeId( $arrValues['parent_import_data_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setImportTypeIds( $arrintImportTypeIds ) {
		$this->set( 'm_arrintImportTypeIds', CStrings::strToArrIntDef( $arrintImportTypeIds, NULL ) );
	}

	public function getImportTypeIds() {
		return $this->m_arrintImportTypeIds;
	}

	public function sqlImportTypeIds() {
		return ( true == isset( $this->m_arrintImportTypeIds ) && true == valArr( $this->m_arrintImportTypeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintImportTypeIds, NULL ) . '\'' : 'NULL';
	}

	public function setParentImportDataTypeId( $intParentImportDataTypeId ) {
		$this->set( 'm_intParentImportDataTypeId', CStrings::strToIntDef( $intParentImportDataTypeId, NULL, false ) );
	}

	public function getParentImportDataTypeId() {
		return $this->m_intParentImportDataTypeId;
	}

	public function sqlParentImportDataTypeId() {
		return ( true == isset( $this->m_intParentImportDataTypeId ) ) ? ( string ) $this->m_intParentImportDataTypeId : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails(),
			'import_type_ids' => $this->getImportTypeIds(),
			'parent_import_data_type_id' => $this->getParentImportDataTypeId()
		);
	}

}
?>