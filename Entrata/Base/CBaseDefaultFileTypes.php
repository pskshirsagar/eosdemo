<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultFileTypes
 * Do not add any new functions to this class.
 */

class CBaseDefaultFileTypes extends CEosPluralBase {

	/**
	 * @return CDefaultFileType[]
	 */
	public static function fetchDefaultFileTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultFileType::class, $objDatabase );
	}

	/**
	 * @return CDefaultFileType
	 */
	public static function fetchDefaultFileType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultFileType::class, $objDatabase );
	}

	public static function fetchDefaultFileTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_file_types', $objDatabase );
	}

	public static function fetchDefaultFileTypeById( $intId, $objDatabase ) {
		return self::fetchDefaultFileType( sprintf( 'SELECT * FROM default_file_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultFileTypesByFileTypeGroupId( $intFileTypeGroupId, $objDatabase ) {
		return self::fetchDefaultFileTypes( sprintf( 'SELECT * FROM default_file_types WHERE file_type_group_id = %d', $intFileTypeGroupId ), $objDatabase );
	}

}
?>