<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFieldOptions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFieldOptions extends CEosPluralBase {

	/**
	 * @return CFieldOption[]
	 */
	public static function fetchFieldOptions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CFieldOption', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFieldOption
	 */
	public static function fetchFieldOption( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CFieldOption', $objDatabase );
	}

	public static function fetchFieldOptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'field_options', $objDatabase );
	}

	public static function fetchFieldOptionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFieldOption( sprintf( 'SELECT * FROM field_options WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFieldOptionsByCid( $intCid, $objDatabase ) {
		return self::fetchFieldOptions( sprintf( 'SELECT * FROM field_options WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFieldOptionsByFieldIdByCid( $intFieldId, $intCid, $objDatabase ) {
		return self::fetchFieldOptions( sprintf( 'SELECT * FROM field_options WHERE field_id = %d AND cid = %d', ( int ) $intFieldId, ( int ) $intCid ), $objDatabase );
	}

}
?>