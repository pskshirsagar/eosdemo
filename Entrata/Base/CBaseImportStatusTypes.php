<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CImportStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseImportStatusTypes extends CEosPluralBase {

	/**
	 * @return CImportStatusType[]
	 */
	public static function fetchImportStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CImportStatusType::class, $objDatabase );
	}

	/**
	 * @return CImportStatusType
	 */
	public static function fetchImportStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CImportStatusType::class, $objDatabase );
	}

	public static function fetchImportStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'import_status_types', $objDatabase );
	}

	public static function fetchImportStatusTypeById( $intId, $objDatabase ) {
		return self::fetchImportStatusType( sprintf( 'SELECT * FROM import_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>