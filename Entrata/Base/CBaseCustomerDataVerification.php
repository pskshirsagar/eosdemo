<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerDataVerification extends CEosSingularBase {

	const TABLE_NAME = 'public.customer_data_verifications';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerId;
	protected $m_intApplicationId;
	protected $m_intVerificationTypeId;
	protected $m_intCustomerDataTypeId;
	protected $m_intCustomerDataReferenceId;
	protected $m_intCustomerDataReferenceLogId;
	protected $m_strNotes;
	protected $m_intVerifiedBy;
	protected $m_strVerifiedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['verification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intVerificationTypeId', trim( $arrValues['verification_type_id'] ) ); elseif( isset( $arrValues['verification_type_id'] ) ) $this->setVerificationTypeId( $arrValues['verification_type_id'] );
		if( isset( $arrValues['customer_data_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerDataTypeId', trim( $arrValues['customer_data_type_id'] ) ); elseif( isset( $arrValues['customer_data_type_id'] ) ) $this->setCustomerDataTypeId( $arrValues['customer_data_type_id'] );
		if( isset( $arrValues['customer_data_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerDataReferenceId', trim( $arrValues['customer_data_reference_id'] ) ); elseif( isset( $arrValues['customer_data_reference_id'] ) ) $this->setCustomerDataReferenceId( $arrValues['customer_data_reference_id'] );
		if( isset( $arrValues['customer_data_reference_log_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerDataReferenceLogId', trim( $arrValues['customer_data_reference_log_id'] ) ); elseif( isset( $arrValues['customer_data_reference_log_id'] ) ) $this->setCustomerDataReferenceLogId( $arrValues['customer_data_reference_log_id'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['verified_by'] ) && $boolDirectSet ) $this->set( 'm_intVerifiedBy', trim( $arrValues['verified_by'] ) ); elseif( isset( $arrValues['verified_by'] ) ) $this->setVerifiedBy( $arrValues['verified_by'] );
		if( isset( $arrValues['verified_on'] ) && $boolDirectSet ) $this->set( 'm_strVerifiedOn', trim( $arrValues['verified_on'] ) ); elseif( isset( $arrValues['verified_on'] ) ) $this->setVerifiedOn( $arrValues['verified_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setVerificationTypeId( $intVerificationTypeId ) {
		$this->set( 'm_intVerificationTypeId', CStrings::strToIntDef( $intVerificationTypeId, NULL, false ) );
	}

	public function getVerificationTypeId() {
		return $this->m_intVerificationTypeId;
	}

	public function sqlVerificationTypeId() {
		return ( true == isset( $this->m_intVerificationTypeId ) ) ? ( string ) $this->m_intVerificationTypeId : 'NULL';
	}

	public function setCustomerDataTypeId( $intCustomerDataTypeId ) {
		$this->set( 'm_intCustomerDataTypeId', CStrings::strToIntDef( $intCustomerDataTypeId, NULL, false ) );
	}

	public function getCustomerDataTypeId() {
		return $this->m_intCustomerDataTypeId;
	}

	public function sqlCustomerDataTypeId() {
		return ( true == isset( $this->m_intCustomerDataTypeId ) ) ? ( string ) $this->m_intCustomerDataTypeId : 'NULL';
	}

	public function setCustomerDataReferenceId( $intCustomerDataReferenceId ) {
		$this->set( 'm_intCustomerDataReferenceId', CStrings::strToIntDef( $intCustomerDataReferenceId, NULL, false ) );
	}

	public function getCustomerDataReferenceId() {
		return $this->m_intCustomerDataReferenceId;
	}

	public function sqlCustomerDataReferenceId() {
		return ( true == isset( $this->m_intCustomerDataReferenceId ) ) ? ( string ) $this->m_intCustomerDataReferenceId : 'NULL';
	}

	public function setCustomerDataReferenceLogId( $intCustomerDataReferenceLogId ) {
		$this->set( 'm_intCustomerDataReferenceLogId', CStrings::strToIntDef( $intCustomerDataReferenceLogId, NULL, false ) );
	}

	public function getCustomerDataReferenceLogId() {
		return $this->m_intCustomerDataReferenceLogId;
	}

	public function sqlCustomerDataReferenceLogId() {
		return ( true == isset( $this->m_intCustomerDataReferenceLogId ) ) ? ( string ) $this->m_intCustomerDataReferenceLogId : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setVerifiedBy( $intVerifiedBy ) {
		$this->set( 'm_intVerifiedBy', CStrings::strToIntDef( $intVerifiedBy, NULL, false ) );
	}

	public function getVerifiedBy() {
		return $this->m_intVerifiedBy;
	}

	public function sqlVerifiedBy() {
		return ( true == isset( $this->m_intVerifiedBy ) ) ? ( string ) $this->m_intVerifiedBy : 'NULL';
	}

	public function setVerifiedOn( $strVerifiedOn ) {
		$this->set( 'm_strVerifiedOn', CStrings::strTrimDef( $strVerifiedOn, -1, NULL, true ) );
	}

	public function getVerifiedOn() {
		return $this->m_strVerifiedOn;
	}

	public function sqlVerifiedOn() {
		return ( true == isset( $this->m_strVerifiedOn ) ) ? '\'' . $this->m_strVerifiedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_id, application_id, verification_type_id, customer_data_type_id, customer_data_reference_id, customer_data_reference_log_id, notes, verified_by, verified_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlApplicationId() . ', ' .
 						$this->sqlVerificationTypeId() . ', ' .
 						$this->sqlCustomerDataTypeId() . ', ' .
 						$this->sqlCustomerDataReferenceId() . ', ' .
 						$this->sqlCustomerDataReferenceLogId() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlVerifiedBy() . ', ' .
 						$this->sqlVerifiedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' verification_type_id = ' . $this->sqlVerificationTypeId() . ','; } elseif( true == array_key_exists( 'VerificationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' verification_type_id = ' . $this->sqlVerificationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_data_type_id = ' . $this->sqlCustomerDataTypeId() . ','; } elseif( true == array_key_exists( 'CustomerDataTypeId', $this->getChangedColumns() ) ) { $strSql .= ' customer_data_type_id = ' . $this->sqlCustomerDataTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_data_reference_id = ' . $this->sqlCustomerDataReferenceId() . ','; } elseif( true == array_key_exists( 'CustomerDataReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' customer_data_reference_id = ' . $this->sqlCustomerDataReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_data_reference_log_id = ' . $this->sqlCustomerDataReferenceLogId() . ','; } elseif( true == array_key_exists( 'CustomerDataReferenceLogId', $this->getChangedColumns() ) ) { $strSql .= ' customer_data_reference_log_id = ' . $this->sqlCustomerDataReferenceLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' verified_by = ' . $this->sqlVerifiedBy() . ','; } elseif( true == array_key_exists( 'VerifiedBy', $this->getChangedColumns() ) ) { $strSql .= ' verified_by = ' . $this->sqlVerifiedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' verified_on = ' . $this->sqlVerifiedOn() . ','; } elseif( true == array_key_exists( 'VerifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' verified_on = ' . $this->sqlVerifiedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_id' => $this->getCustomerId(),
			'application_id' => $this->getApplicationId(),
			'verification_type_id' => $this->getVerificationTypeId(),
			'customer_data_type_id' => $this->getCustomerDataTypeId(),
			'customer_data_reference_id' => $this->getCustomerDataReferenceId(),
			'customer_data_reference_log_id' => $this->getCustomerDataReferenceLogId(),
			'notes' => $this->getNotes(),
			'verified_by' => $this->getVerifiedBy(),
			'verified_on' => $this->getVerifiedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>