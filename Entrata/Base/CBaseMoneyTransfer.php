<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMoneyTransfer extends CEosSingularBase {

	const TABLE_NAME = 'public.money_transfers';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApPaymentId;
	protected $m_intApCodeTypeId;
	protected $m_intApCodeId;
	protected $m_intFromBankAccountId;
	protected $m_intToApRemittanceId;
	protected $m_intInvoiceApHeaderId;
	protected $m_intPaymentApHeaderId;
	protected $m_intReimbursementInvoiceApHeaderId;
	protected $m_intReimbursementPaymentApHeaderId;
	protected $m_intChargeArTransactionId;
	protected $m_intPaymentArTransactionId;
	protected $m_intReimbursementChargeArTransactionId;
	protected $m_intReimbursementPaymentArTransactionId;
	protected $m_fltTransferAmount;
	protected $m_strPostMonth;
	protected $m_strPostDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltTransferAmount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ap_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intApPaymentId', trim( $arrValues['ap_payment_id'] ) ); elseif( isset( $arrValues['ap_payment_id'] ) ) $this->setApPaymentId( $arrValues['ap_payment_id'] );
		if( isset( $arrValues['ap_code_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApCodeTypeId', trim( $arrValues['ap_code_type_id'] ) ); elseif( isset( $arrValues['ap_code_type_id'] ) ) $this->setApCodeTypeId( $arrValues['ap_code_type_id'] );
		if( isset( $arrValues['ap_code_id'] ) && $boolDirectSet ) $this->set( 'm_intApCodeId', trim( $arrValues['ap_code_id'] ) ); elseif( isset( $arrValues['ap_code_id'] ) ) $this->setApCodeId( $arrValues['ap_code_id'] );
		if( isset( $arrValues['from_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intFromBankAccountId', trim( $arrValues['from_bank_account_id'] ) ); elseif( isset( $arrValues['from_bank_account_id'] ) ) $this->setFromBankAccountId( $arrValues['from_bank_account_id'] );
		if( isset( $arrValues['to_ap_remittance_id'] ) && $boolDirectSet ) $this->set( 'm_intToApRemittanceId', trim( $arrValues['to_ap_remittance_id'] ) ); elseif( isset( $arrValues['to_ap_remittance_id'] ) ) $this->setToApRemittanceId( $arrValues['to_ap_remittance_id'] );
		if( isset( $arrValues['invoice_ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceApHeaderId', trim( $arrValues['invoice_ap_header_id'] ) ); elseif( isset( $arrValues['invoice_ap_header_id'] ) ) $this->setInvoiceApHeaderId( $arrValues['invoice_ap_header_id'] );
		if( isset( $arrValues['payment_ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentApHeaderId', trim( $arrValues['payment_ap_header_id'] ) ); elseif( isset( $arrValues['payment_ap_header_id'] ) ) $this->setPaymentApHeaderId( $arrValues['payment_ap_header_id'] );
		if( isset( $arrValues['reimbursement_invoice_ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intReimbursementInvoiceApHeaderId', trim( $arrValues['reimbursement_invoice_ap_header_id'] ) ); elseif( isset( $arrValues['reimbursement_invoice_ap_header_id'] ) ) $this->setReimbursementInvoiceApHeaderId( $arrValues['reimbursement_invoice_ap_header_id'] );
		if( isset( $arrValues['reimbursement_payment_ap_header_id'] ) && $boolDirectSet ) $this->set( 'm_intReimbursementPaymentApHeaderId', trim( $arrValues['reimbursement_payment_ap_header_id'] ) ); elseif( isset( $arrValues['reimbursement_payment_ap_header_id'] ) ) $this->setReimbursementPaymentApHeaderId( $arrValues['reimbursement_payment_ap_header_id'] );
		if( isset( $arrValues['charge_ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intChargeArTransactionId', trim( $arrValues['charge_ar_transaction_id'] ) ); elseif( isset( $arrValues['charge_ar_transaction_id'] ) ) $this->setChargeArTransactionId( $arrValues['charge_ar_transaction_id'] );
		if( isset( $arrValues['payment_ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentArTransactionId', trim( $arrValues['payment_ar_transaction_id'] ) ); elseif( isset( $arrValues['payment_ar_transaction_id'] ) ) $this->setPaymentArTransactionId( $arrValues['payment_ar_transaction_id'] );
		if( isset( $arrValues['reimbursement_charge_ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intReimbursementChargeArTransactionId', trim( $arrValues['reimbursement_charge_ar_transaction_id'] ) ); elseif( isset( $arrValues['reimbursement_charge_ar_transaction_id'] ) ) $this->setReimbursementChargeArTransactionId( $arrValues['reimbursement_charge_ar_transaction_id'] );
		if( isset( $arrValues['reimbursement_payment_ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intReimbursementPaymentArTransactionId', trim( $arrValues['reimbursement_payment_ar_transaction_id'] ) ); elseif( isset( $arrValues['reimbursement_payment_ar_transaction_id'] ) ) $this->setReimbursementPaymentArTransactionId( $arrValues['reimbursement_payment_ar_transaction_id'] );
		if( isset( $arrValues['transfer_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTransferAmount', trim( $arrValues['transfer_amount'] ) ); elseif( isset( $arrValues['transfer_amount'] ) ) $this->setTransferAmount( $arrValues['transfer_amount'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApPaymentId( $intApPaymentId ) {
		$this->set( 'm_intApPaymentId', CStrings::strToIntDef( $intApPaymentId, NULL, false ) );
	}

	public function getApPaymentId() {
		return $this->m_intApPaymentId;
	}

	public function sqlApPaymentId() {
		return ( true == isset( $this->m_intApPaymentId ) ) ? ( string ) $this->m_intApPaymentId : 'NULL';
	}

	public function setApCodeTypeId( $intApCodeTypeId ) {
		$this->set( 'm_intApCodeTypeId', CStrings::strToIntDef( $intApCodeTypeId, NULL, false ) );
	}

	public function getApCodeTypeId() {
		return $this->m_intApCodeTypeId;
	}

	public function sqlApCodeTypeId() {
		return ( true == isset( $this->m_intApCodeTypeId ) ) ? ( string ) $this->m_intApCodeTypeId : 'NULL';
	}

	public function setApCodeId( $intApCodeId ) {
		$this->set( 'm_intApCodeId', CStrings::strToIntDef( $intApCodeId, NULL, false ) );
	}

	public function getApCodeId() {
		return $this->m_intApCodeId;
	}

	public function sqlApCodeId() {
		return ( true == isset( $this->m_intApCodeId ) ) ? ( string ) $this->m_intApCodeId : 'NULL';
	}

	public function setFromBankAccountId( $intFromBankAccountId ) {
		$this->set( 'm_intFromBankAccountId', CStrings::strToIntDef( $intFromBankAccountId, NULL, false ) );
	}

	public function getFromBankAccountId() {
		return $this->m_intFromBankAccountId;
	}

	public function sqlFromBankAccountId() {
		return ( true == isset( $this->m_intFromBankAccountId ) ) ? ( string ) $this->m_intFromBankAccountId : 'NULL';
	}

	public function setToApRemittanceId( $intToApRemittanceId ) {
		$this->set( 'm_intToApRemittanceId', CStrings::strToIntDef( $intToApRemittanceId, NULL, false ) );
	}

	public function getToApRemittanceId() {
		return $this->m_intToApRemittanceId;
	}

	public function sqlToApRemittanceId() {
		return ( true == isset( $this->m_intToApRemittanceId ) ) ? ( string ) $this->m_intToApRemittanceId : 'NULL';
	}

	public function setInvoiceApHeaderId( $intInvoiceApHeaderId ) {
		$this->set( 'm_intInvoiceApHeaderId', CStrings::strToIntDef( $intInvoiceApHeaderId, NULL, false ) );
	}

	public function getInvoiceApHeaderId() {
		return $this->m_intInvoiceApHeaderId;
	}

	public function sqlInvoiceApHeaderId() {
		return ( true == isset( $this->m_intInvoiceApHeaderId ) ) ? ( string ) $this->m_intInvoiceApHeaderId : 'NULL';
	}

	public function setPaymentApHeaderId( $intPaymentApHeaderId ) {
		$this->set( 'm_intPaymentApHeaderId', CStrings::strToIntDef( $intPaymentApHeaderId, NULL, false ) );
	}

	public function getPaymentApHeaderId() {
		return $this->m_intPaymentApHeaderId;
	}

	public function sqlPaymentApHeaderId() {
		return ( true == isset( $this->m_intPaymentApHeaderId ) ) ? ( string ) $this->m_intPaymentApHeaderId : 'NULL';
	}

	public function setReimbursementInvoiceApHeaderId( $intReimbursementInvoiceApHeaderId ) {
		$this->set( 'm_intReimbursementInvoiceApHeaderId', CStrings::strToIntDef( $intReimbursementInvoiceApHeaderId, NULL, false ) );
	}

	public function getReimbursementInvoiceApHeaderId() {
		return $this->m_intReimbursementInvoiceApHeaderId;
	}

	public function sqlReimbursementInvoiceApHeaderId() {
		return ( true == isset( $this->m_intReimbursementInvoiceApHeaderId ) ) ? ( string ) $this->m_intReimbursementInvoiceApHeaderId : 'NULL';
	}

	public function setReimbursementPaymentApHeaderId( $intReimbursementPaymentApHeaderId ) {
		$this->set( 'm_intReimbursementPaymentApHeaderId', CStrings::strToIntDef( $intReimbursementPaymentApHeaderId, NULL, false ) );
	}

	public function getReimbursementPaymentApHeaderId() {
		return $this->m_intReimbursementPaymentApHeaderId;
	}

	public function sqlReimbursementPaymentApHeaderId() {
		return ( true == isset( $this->m_intReimbursementPaymentApHeaderId ) ) ? ( string ) $this->m_intReimbursementPaymentApHeaderId : 'NULL';
	}

	public function setChargeArTransactionId( $intChargeArTransactionId ) {
		$this->set( 'm_intChargeArTransactionId', CStrings::strToIntDef( $intChargeArTransactionId, NULL, false ) );
	}

	public function getChargeArTransactionId() {
		return $this->m_intChargeArTransactionId;
	}

	public function sqlChargeArTransactionId() {
		return ( true == isset( $this->m_intChargeArTransactionId ) ) ? ( string ) $this->m_intChargeArTransactionId : 'NULL';
	}

	public function setPaymentArTransactionId( $intPaymentArTransactionId ) {
		$this->set( 'm_intPaymentArTransactionId', CStrings::strToIntDef( $intPaymentArTransactionId, NULL, false ) );
	}

	public function getPaymentArTransactionId() {
		return $this->m_intPaymentArTransactionId;
	}

	public function sqlPaymentArTransactionId() {
		return ( true == isset( $this->m_intPaymentArTransactionId ) ) ? ( string ) $this->m_intPaymentArTransactionId : 'NULL';
	}

	public function setReimbursementChargeArTransactionId( $intReimbursementChargeArTransactionId ) {
		$this->set( 'm_intReimbursementChargeArTransactionId', CStrings::strToIntDef( $intReimbursementChargeArTransactionId, NULL, false ) );
	}

	public function getReimbursementChargeArTransactionId() {
		return $this->m_intReimbursementChargeArTransactionId;
	}

	public function sqlReimbursementChargeArTransactionId() {
		return ( true == isset( $this->m_intReimbursementChargeArTransactionId ) ) ? ( string ) $this->m_intReimbursementChargeArTransactionId : 'NULL';
	}

	public function setReimbursementPaymentArTransactionId( $intReimbursementPaymentArTransactionId ) {
		$this->set( 'm_intReimbursementPaymentArTransactionId', CStrings::strToIntDef( $intReimbursementPaymentArTransactionId, NULL, false ) );
	}

	public function getReimbursementPaymentArTransactionId() {
		return $this->m_intReimbursementPaymentArTransactionId;
	}

	public function sqlReimbursementPaymentArTransactionId() {
		return ( true == isset( $this->m_intReimbursementPaymentArTransactionId ) ) ? ( string ) $this->m_intReimbursementPaymentArTransactionId : 'NULL';
	}

	public function setTransferAmount( $fltTransferAmount ) {
		$this->set( 'm_fltTransferAmount', CStrings::strToFloatDef( $fltTransferAmount, NULL, false, 2 ) );
	}

	public function getTransferAmount() {
		return $this->m_fltTransferAmount;
	}

	public function sqlTransferAmount() {
		return ( true == isset( $this->m_fltTransferAmount ) ) ? ( string ) $this->m_fltTransferAmount : '0';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ap_payment_id, ap_code_type_id, ap_code_id, from_bank_account_id, to_ap_remittance_id, invoice_ap_header_id, payment_ap_header_id, reimbursement_invoice_ap_header_id, reimbursement_payment_ap_header_id, charge_ar_transaction_id, payment_ar_transaction_id, reimbursement_charge_ar_transaction_id, reimbursement_payment_ar_transaction_id, transfer_amount, post_month, post_date, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlApPaymentId() . ', ' .
 						$this->sqlApCodeTypeId() . ', ' .
 						$this->sqlApCodeId() . ', ' .
 						$this->sqlFromBankAccountId() . ', ' .
 						$this->sqlToApRemittanceId() . ', ' .
 						$this->sqlInvoiceApHeaderId() . ', ' .
 						$this->sqlPaymentApHeaderId() . ', ' .
 						$this->sqlReimbursementInvoiceApHeaderId() . ', ' .
 						$this->sqlReimbursementPaymentApHeaderId() . ', ' .
 						$this->sqlChargeArTransactionId() . ', ' .
 						$this->sqlPaymentArTransactionId() . ', ' .
 						$this->sqlReimbursementChargeArTransactionId() . ', ' .
 						$this->sqlReimbursementPaymentArTransactionId() . ', ' .
 						$this->sqlTransferAmount() . ', ' .
 						$this->sqlPostMonth() . ', ' .
 						$this->sqlPostDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payment_id = ' . $this->sqlApPaymentId() . ','; } elseif( true == array_key_exists( 'ApPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payment_id = ' . $this->sqlApPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_code_type_id = ' . $this->sqlApCodeTypeId() . ','; } elseif( true == array_key_exists( 'ApCodeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_code_type_id = ' . $this->sqlApCodeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_code_id = ' . $this->sqlApCodeId() . ','; } elseif( true == array_key_exists( 'ApCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_code_id = ' . $this->sqlApCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' from_bank_account_id = ' . $this->sqlFromBankAccountId() . ','; } elseif( true == array_key_exists( 'FromBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' from_bank_account_id = ' . $this->sqlFromBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' to_ap_remittance_id = ' . $this->sqlToApRemittanceId() . ','; } elseif( true == array_key_exists( 'ToApRemittanceId', $this->getChangedColumns() ) ) { $strSql .= ' to_ap_remittance_id = ' . $this->sqlToApRemittanceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_ap_header_id = ' . $this->sqlInvoiceApHeaderId() . ','; } elseif( true == array_key_exists( 'InvoiceApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' invoice_ap_header_id = ' . $this->sqlInvoiceApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_ap_header_id = ' . $this->sqlPaymentApHeaderId() . ','; } elseif( true == array_key_exists( 'PaymentApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' payment_ap_header_id = ' . $this->sqlPaymentApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reimbursement_invoice_ap_header_id = ' . $this->sqlReimbursementInvoiceApHeaderId() . ','; } elseif( true == array_key_exists( 'ReimbursementInvoiceApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' reimbursement_invoice_ap_header_id = ' . $this->sqlReimbursementInvoiceApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reimbursement_payment_ap_header_id = ' . $this->sqlReimbursementPaymentApHeaderId() . ','; } elseif( true == array_key_exists( 'ReimbursementPaymentApHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' reimbursement_payment_ap_header_id = ' . $this->sqlReimbursementPaymentApHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_ar_transaction_id = ' . $this->sqlChargeArTransactionId() . ','; } elseif( true == array_key_exists( 'ChargeArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' charge_ar_transaction_id = ' . $this->sqlChargeArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_ar_transaction_id = ' . $this->sqlPaymentArTransactionId() . ','; } elseif( true == array_key_exists( 'PaymentArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' payment_ar_transaction_id = ' . $this->sqlPaymentArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reimbursement_charge_ar_transaction_id = ' . $this->sqlReimbursementChargeArTransactionId() . ','; } elseif( true == array_key_exists( 'ReimbursementChargeArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' reimbursement_charge_ar_transaction_id = ' . $this->sqlReimbursementChargeArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reimbursement_payment_ar_transaction_id = ' . $this->sqlReimbursementPaymentArTransactionId() . ','; } elseif( true == array_key_exists( 'ReimbursementPaymentArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' reimbursement_payment_ar_transaction_id = ' . $this->sqlReimbursementPaymentArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transfer_amount = ' . $this->sqlTransferAmount() . ','; } elseif( true == array_key_exists( 'TransferAmount', $this->getChangedColumns() ) ) { $strSql .= ' transfer_amount = ' . $this->sqlTransferAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ap_payment_id' => $this->getApPaymentId(),
			'ap_code_type_id' => $this->getApCodeTypeId(),
			'ap_code_id' => $this->getApCodeId(),
			'from_bank_account_id' => $this->getFromBankAccountId(),
			'to_ap_remittance_id' => $this->getToApRemittanceId(),
			'invoice_ap_header_id' => $this->getInvoiceApHeaderId(),
			'payment_ap_header_id' => $this->getPaymentApHeaderId(),
			'reimbursement_invoice_ap_header_id' => $this->getReimbursementInvoiceApHeaderId(),
			'reimbursement_payment_ap_header_id' => $this->getReimbursementPaymentApHeaderId(),
			'charge_ar_transaction_id' => $this->getChargeArTransactionId(),
			'payment_ar_transaction_id' => $this->getPaymentArTransactionId(),
			'reimbursement_charge_ar_transaction_id' => $this->getReimbursementChargeArTransactionId(),
			'reimbursement_payment_ar_transaction_id' => $this->getReimbursementPaymentArTransactionId(),
			'transfer_amount' => $this->getTransferAmount(),
			'post_month' => $this->getPostMonth(),
			'post_date' => $this->getPostDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>