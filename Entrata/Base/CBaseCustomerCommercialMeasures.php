<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerCommercialMeasures
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerCommercialMeasures extends CEosPluralBase {

	/**
	 * @return CCustomerCommercialMeasure[]
	 */
	public static function fetchCustomerCommercialMeasures( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerCommercialMeasure', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerCommercialMeasure
	 */
	public static function fetchCustomerCommercialMeasure( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerCommercialMeasure', $objDatabase );
	}

	public static function fetchCustomerCommercialMeasureCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_commercial_measures', $objDatabase );
	}

	public static function fetchCustomerCommercialMeasureByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerCommercialMeasure( sprintf( 'SELECT * FROM customer_commercial_measures WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerCommercialMeasuresByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerCommercialMeasures( sprintf( 'SELECT * FROM customer_commercial_measures WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerCommercialMeasuresByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerCommercialMeasures( sprintf( 'SELECT * FROM customer_commercial_measures WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerCommercialMeasuresByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchCustomerCommercialMeasures( sprintf( 'SELECT * FROM customer_commercial_measures WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

}
?>