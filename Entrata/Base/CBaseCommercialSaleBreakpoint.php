<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCommercialSaleBreakpoint extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.commercial_sale_breakpoints';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intLeaseId;
	protected $m_intThresholdArTriggerId;
	protected $m_fltPercentAmount;
	protected $m_fltThresholdAmount;
	protected $m_strBreakpointStartMonth;
	protected $m_strBreakpointEndMonth;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
    protected $m_strDetails;
    protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_fltPercentAmount = '0';
		$this->m_fltThresholdAmount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['threshold_ar_trigger_id'] ) && $boolDirectSet ) $this->set( 'm_intThresholdArTriggerId', trim( $arrValues['threshold_ar_trigger_id'] ) ); elseif( isset( $arrValues['threshold_ar_trigger_id'] ) ) $this->setThresholdArTriggerId( $arrValues['threshold_ar_trigger_id'] );
		if( isset( $arrValues['percent_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPercentAmount', trim( $arrValues['percent_amount'] ) ); elseif( isset( $arrValues['percent_amount'] ) ) $this->setPercentAmount( $arrValues['percent_amount'] );
		if( isset( $arrValues['threshold_amount'] ) && $boolDirectSet ) $this->set( 'm_fltThresholdAmount', trim( $arrValues['threshold_amount'] ) ); elseif( isset( $arrValues['threshold_amount'] ) ) $this->setThresholdAmount( $arrValues['threshold_amount'] );
		if( isset( $arrValues['breakpoint_start_month'] ) && $boolDirectSet ) $this->set( 'm_strBreakpointStartMonth', trim( $arrValues['breakpoint_start_month'] ) ); elseif( isset( $arrValues['breakpoint_start_month'] ) ) $this->setBreakpointStartMonth( $arrValues['breakpoint_start_month'] );
		if( isset( $arrValues['breakpoint_end_month'] ) && $boolDirectSet ) $this->set( 'm_strBreakpointEndMonth', trim( $arrValues['breakpoint_end_month'] ) ); elseif( isset( $arrValues['breakpoint_end_month'] ) ) $this->setBreakpointEndMonth( $arrValues['breakpoint_end_month'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
        if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setThresholdArTriggerId( $intThresholdArTriggerId ) {
		$this->set( 'm_intThresholdArTriggerId', CStrings::strToIntDef( $intThresholdArTriggerId, NULL, false ) );
	}

	public function getThresholdArTriggerId() {
		return $this->m_intThresholdArTriggerId;
	}

	public function sqlThresholdArTriggerId() {
		return ( true == isset( $this->m_intThresholdArTriggerId ) ) ? ( string ) $this->m_intThresholdArTriggerId : 'NULL';
	}

	public function setPercentAmount( $fltPercentAmount ) {
		$this->set( 'm_fltPercentAmount', CStrings::strToFloatDef( $fltPercentAmount, NULL, false, 4 ) );
	}

	public function getPercentAmount() {
		return $this->m_fltPercentAmount;
	}

	public function sqlPercentAmount() {
		return ( true == isset( $this->m_fltPercentAmount ) ) ? ( string ) $this->m_fltPercentAmount : '0';
	}

	public function setThresholdAmount( $fltThresholdAmount ) {
		$this->set( 'm_fltThresholdAmount', CStrings::strToFloatDef( $fltThresholdAmount, NULL, false, 2 ) );
	}

	public function getThresholdAmount() {
		return $this->m_fltThresholdAmount;
	}

	public function sqlThresholdAmount() {
		return ( true == isset( $this->m_fltThresholdAmount ) ) ? ( string ) $this->m_fltThresholdAmount : '0';
	}

	public function setBreakpointStartMonth( $strBreakpointStartMonth ) {
		$this->set( 'm_strBreakpointStartMonth', CStrings::strTrimDef( $strBreakpointStartMonth, -1, NULL, true ) );
	}

	public function getBreakpointStartMonth() {
		return $this->m_strBreakpointStartMonth;
	}

	public function sqlBreakpointStartMonth() {
		return ( true == isset( $this->m_strBreakpointStartMonth ) ) ? '\'' . $this->m_strBreakpointStartMonth . '\'' : 'NOW()';
	}

	public function setBreakpointEndMonth( $strBreakpointEndMonth ) {
		$this->set( 'm_strBreakpointEndMonth', CStrings::strTrimDef( $strBreakpointEndMonth, -1, NULL, true ) );
	}

	public function getBreakpointEndMonth() {
		return $this->m_strBreakpointEndMonth;
	}

	public function sqlBreakpointEndMonth() {
		return ( true == isset( $this->m_strBreakpointEndMonth ) ) ? '\'' . $this->m_strBreakpointEndMonth . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, lease_id, threshold_ar_trigger_id, percent_amount, threshold_amount, breakpoint_start_month, breakpoint_end_month, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlThresholdArTriggerId() . ', ' .
 						$this->sqlPercentAmount() . ', ' .
 						$this->sqlThresholdAmount() . ', ' .
 						$this->sqlBreakpointStartMonth() . ', ' .
 						$this->sqlBreakpointEndMonth() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' threshold_ar_trigger_id = ' . $this->sqlThresholdArTriggerId() . ','; } elseif( true == array_key_exists( 'ThresholdArTriggerId', $this->getChangedColumns() ) ) { $strSql .= ' threshold_ar_trigger_id = ' . $this->sqlThresholdArTriggerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' percent_amount = ' . $this->sqlPercentAmount() . ','; } elseif( true == array_key_exists( 'PercentAmount', $this->getChangedColumns() ) ) { $strSql .= ' percent_amount = ' . $this->sqlPercentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' threshold_amount = ' . $this->sqlThresholdAmount() . ','; } elseif( true == array_key_exists( 'ThresholdAmount', $this->getChangedColumns() ) ) { $strSql .= ' threshold_amount = ' . $this->sqlThresholdAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' breakpoint_start_month = ' . $this->sqlBreakpointStartMonth() . ','; } elseif( true == array_key_exists( 'BreakpointStartMonth', $this->getChangedColumns() ) ) { $strSql .= ' breakpoint_start_month = ' . $this->sqlBreakpointStartMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' breakpoint_end_month = ' . $this->sqlBreakpointEndMonth() . ','; } elseif( true == array_key_exists( 'BreakpointEndMonth', $this->getChangedColumns() ) ) { $strSql .= ' breakpoint_end_month = ' . $this->sqlBreakpointEndMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'lease_id' => $this->getLeaseId(),
			'threshold_ar_trigger_id' => $this->getThresholdArTriggerId(),
			'percent_amount' => $this->getPercentAmount(),
			'threshold_amount' => $this->getThresholdAmount(),
			'breakpoint_start_month' => $this->getBreakpointStartMonth(),
			'breakpoint_end_month' => $this->getBreakpointEndMonth(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>