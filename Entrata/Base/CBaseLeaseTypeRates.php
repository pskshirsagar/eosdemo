<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseTypeRates extends CEosPluralBase {

    const TABLE_LEASE_TYPE_RATES = 'public.lease_type_rates';

    public static function fetchLeaseTypeRates( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CLeaseTypeRate', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchLeaseTypeRate( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CLeaseTypeRate', $objDatabase );
    }

    public static function fetchLeaseTypeRateCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'lease_type_rates', $objDatabase );
    }

    public static function fetchLeaseTypeRateByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchLeaseTypeRate( sprintf( 'SELECT * FROM lease_type_rates WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchLeaseTypeRatesByCid( $intCid, $objDatabase ) {
        return self::fetchLeaseTypeRates( sprintf( 'SELECT * FROM lease_type_rates WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchLeaseTypeRatesByLeaseTypeIdByCid( $intLeaseTypeId, $intCid, $objDatabase ) {
        return self::fetchLeaseTypeRates( sprintf( 'SELECT * FROM lease_type_rates WHERE lease_type_id = %d AND cid = %d', (int) $intLeaseTypeId, (int) $intCid ), $objDatabase );
    }

    public static function fetchLeaseTypeRatesByRateIdByCid( $intRateId, $intCid, $objDatabase ) {
        return self::fetchLeaseTypeRates( sprintf( 'SELECT * FROM lease_type_rates WHERE rate_id = %d AND cid = %d', ( double ) $intRateId, (int) $intCid ), $objDatabase );
    }

}
?>