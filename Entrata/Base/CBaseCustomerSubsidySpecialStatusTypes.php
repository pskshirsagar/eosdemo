<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerSubsidySpecialStatusTypes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerSubsidySpecialStatusTypes extends CEosPluralBase {

	/**
	 * @return CCustomerSubsidySpecialStatusType[]
	 */
	public static function fetchCustomerSubsidySpecialStatusTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerSubsidySpecialStatusType', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerSubsidySpecialStatusType
	 */
	public static function fetchCustomerSubsidySpecialStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCustomerSubsidySpecialStatusType', $objDatabase );
	}

	public static function fetchCustomerSubsidySpecialStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_subsidy_special_status_types', $objDatabase );
	}

	public static function fetchCustomerSubsidySpecialStatusTypeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidySpecialStatusType( sprintf( 'SELECT * FROM customer_subsidy_special_status_types WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidySpecialStatusTypesByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidySpecialStatusTypes( sprintf( 'SELECT * FROM customer_subsidy_special_status_types WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidySpecialStatusTypesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidySpecialStatusTypes( sprintf( 'SELECT * FROM customer_subsidy_special_status_types WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerSubsidySpecialStatusTypesBySubsidySpecialStatusTypeIdByCid( $intSubsidySpecialStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerSubsidySpecialStatusTypes( sprintf( 'SELECT * FROM customer_subsidy_special_status_types WHERE subsidy_special_status_type_id = %d AND cid = %d', ( int ) $intSubsidySpecialStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>