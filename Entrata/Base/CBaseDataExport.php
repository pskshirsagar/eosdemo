<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDataExport extends CEosSingularBase {

	const TABLE_NAME = 'public.data_exports';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDataExportScheduleId;
	protected $m_intDataExportStatusTypeId;
	protected $m_arrintPropertyIds;
	protected $m_strStartDatetime;
	protected $m_strEndDatetime;
	protected $m_strFileName;
	protected $m_strFileSize;
	protected $m_strDownloadedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['data_export_schedule_id'] ) && $boolDirectSet ) $this->set( 'm_intDataExportScheduleId', trim( $arrValues['data_export_schedule_id'] ) ); elseif( isset( $arrValues['data_export_schedule_id'] ) ) $this->setDataExportScheduleId( $arrValues['data_export_schedule_id'] );
		if( isset( $arrValues['data_export_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDataExportStatusTypeId', trim( $arrValues['data_export_status_type_id'] ) ); elseif( isset( $arrValues['data_export_status_type_id'] ) ) $this->setDataExportStatusTypeId( $arrValues['data_export_status_type_id'] );
		if( isset( $arrValues['property_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintPropertyIds', trim( $arrValues['property_ids'] ) ); elseif( isset( $arrValues['property_ids'] ) ) $this->setPropertyIds( $arrValues['property_ids'] );
		if( isset( $arrValues['start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStartDatetime', trim( $arrValues['start_datetime'] ) ); elseif( isset( $arrValues['start_datetime'] ) ) $this->setStartDatetime( $arrValues['start_datetime'] );
		if( isset( $arrValues['end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEndDatetime', trim( $arrValues['end_datetime'] ) ); elseif( isset( $arrValues['end_datetime'] ) ) $this->setEndDatetime( $arrValues['end_datetime'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['file_size'] ) && $boolDirectSet ) $this->set( 'm_strFileSize', trim( stripcslashes( $arrValues['file_size'] ) ) ); elseif( isset( $arrValues['file_size'] ) ) $this->setFileSize( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_size'] ) : $arrValues['file_size'] );
		if( isset( $arrValues['downloaded_on'] ) && $boolDirectSet ) $this->set( 'm_strDownloadedOn', trim( $arrValues['downloaded_on'] ) ); elseif( isset( $arrValues['downloaded_on'] ) ) $this->setDownloadedOn( $arrValues['downloaded_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDataExportScheduleId( $intDataExportScheduleId ) {
		$this->set( 'm_intDataExportScheduleId', CStrings::strToIntDef( $intDataExportScheduleId, NULL, false ) );
	}

	public function getDataExportScheduleId() {
		return $this->m_intDataExportScheduleId;
	}

	public function sqlDataExportScheduleId() {
		return ( true == isset( $this->m_intDataExportScheduleId ) ) ? ( string ) $this->m_intDataExportScheduleId : 'NULL';
	}

	public function setDataExportStatusTypeId( $intDataExportStatusTypeId ) {
		$this->set( 'm_intDataExportStatusTypeId', CStrings::strToIntDef( $intDataExportStatusTypeId, NULL, false ) );
	}

	public function getDataExportStatusTypeId() {
		return $this->m_intDataExportStatusTypeId;
	}

	public function sqlDataExportStatusTypeId() {
		return ( true == isset( $this->m_intDataExportStatusTypeId ) ) ? ( string ) $this->m_intDataExportStatusTypeId : 'NULL';
	}

	public function setPropertyIds( $arrintPropertyIds ) {
		$this->set( 'm_arrintPropertyIds', CStrings::strToArrIntDef( $arrintPropertyIds, NULL ) );
	}

	public function getPropertyIds() {
		return $this->m_arrintPropertyIds;
	}

	public function sqlPropertyIds() {
		return ( true == isset( $this->m_arrintPropertyIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintPropertyIds, NULL ) . '\'' : 'NULL';
	}

	public function setStartDatetime( $strStartDatetime ) {
		$this->set( 'm_strStartDatetime', CStrings::strTrimDef( $strStartDatetime, -1, NULL, true ) );
	}

	public function getStartDatetime() {
		return $this->m_strStartDatetime;
	}

	public function sqlStartDatetime() {
		return ( true == isset( $this->m_strStartDatetime ) ) ? '\'' . $this->m_strStartDatetime . '\'' : 'NULL';
	}

	public function setEndDatetime( $strEndDatetime ) {
		$this->set( 'm_strEndDatetime', CStrings::strTrimDef( $strEndDatetime, -1, NULL, true ) );
	}

	public function getEndDatetime() {
		return $this->m_strEndDatetime;
	}

	public function sqlEndDatetime() {
		return ( true == isset( $this->m_strEndDatetime ) ) ? '\'' . $this->m_strEndDatetime . '\'' : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setFileSize( $strFileSize ) {
		$this->set( 'm_strFileSize', CStrings::strTrimDef( $strFileSize, 4096, NULL, true ) );
	}

	public function getFileSize() {
		return $this->m_strFileSize;
	}

	public function sqlFileSize() {
		return ( true == isset( $this->m_strFileSize ) ) ? '\'' . addslashes( $this->m_strFileSize ) . '\'' : 'NULL';
	}

	public function setDownloadedOn( $strDownloadedOn ) {
		$this->set( 'm_strDownloadedOn', CStrings::strTrimDef( $strDownloadedOn, -1, NULL, true ) );
	}

	public function getDownloadedOn() {
		return $this->m_strDownloadedOn;
	}

	public function sqlDownloadedOn() {
		return ( true == isset( $this->m_strDownloadedOn ) ) ? '\'' . $this->m_strDownloadedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, data_export_schedule_id, data_export_status_type_id, property_ids, start_datetime, end_datetime, file_name, file_size, downloaded_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlDataExportScheduleId() . ', ' .
 						$this->sqlDataExportStatusTypeId() . ', ' .
 						$this->sqlPropertyIds() . ', ' .
 						$this->sqlStartDatetime() . ', ' .
 						$this->sqlEndDatetime() . ', ' .
 						$this->sqlFileName() . ', ' .
 						$this->sqlFileSize() . ', ' .
 						$this->sqlDownloadedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_export_schedule_id = ' . $this->sqlDataExportScheduleId() . ','; } elseif( true == array_key_exists( 'DataExportScheduleId', $this->getChangedColumns() ) ) { $strSql .= ' data_export_schedule_id = ' . $this->sqlDataExportScheduleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_export_status_type_id = ' . $this->sqlDataExportStatusTypeId() . ','; } elseif( true == array_key_exists( 'DataExportStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' data_export_status_type_id = ' . $this->sqlDataExportStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_ids = ' . $this->sqlPropertyIds() . ','; } elseif( true == array_key_exists( 'PropertyIds', $this->getChangedColumns() ) ) { $strSql .= ' property_ids = ' . $this->sqlPropertyIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_datetime = ' . $this->sqlStartDatetime() . ','; } elseif( true == array_key_exists( 'StartDatetime', $this->getChangedColumns() ) ) { $strSql .= ' start_datetime = ' . $this->sqlStartDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; } elseif( true == array_key_exists( 'EndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_size = ' . $this->sqlFileSize() . ','; } elseif( true == array_key_exists( 'FileSize', $this->getChangedColumns() ) ) { $strSql .= ' file_size = ' . $this->sqlFileSize() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' downloaded_on = ' . $this->sqlDownloadedOn() . ','; } elseif( true == array_key_exists( 'DownloadedOn', $this->getChangedColumns() ) ) { $strSql .= ' downloaded_on = ' . $this->sqlDownloadedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'data_export_schedule_id' => $this->getDataExportScheduleId(),
			'data_export_status_type_id' => $this->getDataExportStatusTypeId(),
			'property_ids' => $this->getPropertyIds(),
			'start_datetime' => $this->getStartDatetime(),
			'end_datetime' => $this->getEndDatetime(),
			'file_name' => $this->getFileName(),
			'file_size' => $this->getFileSize(),
			'downloaded_on' => $this->getDownloadedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>