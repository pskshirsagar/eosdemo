<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerExpenseLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerExpenseLogs extends CEosPluralBase {

	/**
	 * @return CCustomerExpenseLog[]
	 */
	public static function fetchCustomerExpenseLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCustomerExpenseLog::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerExpenseLog
	 */
	public static function fetchCustomerExpenseLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerExpenseLog::class, $objDatabase );
	}

	public static function fetchCustomerExpenseLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_expense_logs', $objDatabase );
	}

	public static function fetchCustomerExpenseLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerExpenseLog( sprintf( 'SELECT * FROM customer_expense_logs WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerExpenseLogsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerExpenseLogs( sprintf( 'SELECT * FROM customer_expense_logs WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCustomerExpenseLogsByCustomerExpenseIdByCid( $intCustomerExpenseId, $intCid, $objDatabase ) {
		return self::fetchCustomerExpenseLogs( sprintf( 'SELECT * FROM customer_expense_logs WHERE customer_expense_id = %d AND cid = %d', $intCustomerExpenseId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerExpenseLogsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerExpenseLogs( sprintf( 'SELECT * FROM customer_expense_logs WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerExpenseLogsByExpenseTypeIdByCid( $intExpenseTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerExpenseLogs( sprintf( 'SELECT * FROM customer_expense_logs WHERE expense_type_id = %d AND cid = %d', $intExpenseTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerExpenseLogsByFrequencyIdByCid( $intFrequencyId, $intCid, $objDatabase ) {
		return self::fetchCustomerExpenseLogs( sprintf( 'SELECT * FROM customer_expense_logs WHERE frequency_id = %d AND cid = %d', $intFrequencyId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerExpenseLogsByPriorCustomerExpenseLogIdByCid( $intPriorCustomerExpenseLogId, $intCid, $objDatabase ) {
		return self::fetchCustomerExpenseLogs( sprintf( 'SELECT * FROM customer_expense_logs WHERE prior_customer_expense_log_id = %d AND cid = %d', $intPriorCustomerExpenseLogId, $intCid ), $objDatabase );
	}

}
?>