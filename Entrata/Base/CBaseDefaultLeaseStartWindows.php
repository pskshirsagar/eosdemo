<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultLeaseStartWindows
 * Do not add any new functions to this class.
 */

class CBaseDefaultLeaseStartWindows extends CEosPluralBase {

	/**
	 * @return CDefaultLeaseStartWindow[]
	 */
	public static function fetchDefaultLeaseStartWindows( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultLeaseStartWindow::class, $objDatabase );
	}

	/**
	 * @return CDefaultLeaseStartWindow
	 */
	public static function fetchDefaultLeaseStartWindow( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultLeaseStartWindow::class, $objDatabase );
	}

	public static function fetchDefaultLeaseStartWindowCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_lease_start_windows', $objDatabase );
	}

	public static function fetchDefaultLeaseStartWindowById( $intId, $objDatabase ) {
		return self::fetchDefaultLeaseStartWindow( sprintf( 'SELECT * FROM default_lease_start_windows WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultLeaseStartWindowsByDefaultLeaseStartStructureId( $intDefaultLeaseStartStructureId, $objDatabase ) {
		return self::fetchDefaultLeaseStartWindows( sprintf( 'SELECT * FROM default_lease_start_windows WHERE default_lease_start_structure_id = %d', $intDefaultLeaseStartStructureId ), $objDatabase );
	}

}
?>