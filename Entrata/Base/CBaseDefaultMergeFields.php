<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultMergeFields
 * Do not add any new functions to this class.
 */

class CBaseDefaultMergeFields extends CEosPluralBase {

	/**
	 * @return CDefaultMergeField[]
	 */
	public static function fetchDefaultMergeFields( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultMergeField::class, $objDatabase );
	}

	/**
	 * @return CDefaultMergeField
	 */
	public static function fetchDefaultMergeField( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultMergeField::class, $objDatabase );
	}

	public static function fetchDefaultMergeFieldCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_merge_fields', $objDatabase );
	}

	public static function fetchDefaultMergeFieldById( $intId, $objDatabase ) {
		return self::fetchDefaultMergeField( sprintf( 'SELECT * FROM default_merge_fields WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultMergeFieldsByMergeFieldTypeId( $intMergeFieldTypeId, $objDatabase ) {
		return self::fetchDefaultMergeFields( sprintf( 'SELECT * FROM default_merge_fields WHERE merge_field_type_id = %d', $intMergeFieldTypeId ), $objDatabase );
	}

	public static function fetchDefaultMergeFieldsByBlockDefaultMergeFieldId( $intBlockDefaultMergeFieldId, $objDatabase ) {
		return self::fetchDefaultMergeFields( sprintf( 'SELECT * FROM default_merge_fields WHERE block_default_merge_field_id = %d', $intBlockDefaultMergeFieldId ), $objDatabase );
	}

	public static function fetchDefaultMergeFieldsByMergeFieldGroupId( $intMergeFieldGroupId, $objDatabase ) {
		return self::fetchDefaultMergeFields( sprintf( 'SELECT * FROM default_merge_fields WHERE merge_field_group_id = %d', $intMergeFieldGroupId ), $objDatabase );
	}

	public static function fetchDefaultMergeFieldsByMergeFieldSubGroupId( $intMergeFieldSubGroupId, $objDatabase ) {
		return self::fetchDefaultMergeFields( sprintf( 'SELECT * FROM default_merge_fields WHERE merge_field_sub_group_id = %d', $intMergeFieldSubGroupId ), $objDatabase );
	}

	public static function fetchDefaultMergeFieldsByExternalMergeFieldGroupId( $intExternalMergeFieldGroupId, $objDatabase ) {
		return self::fetchDefaultMergeFields( sprintf( 'SELECT * FROM default_merge_fields WHERE external_merge_field_group_id = %d', $intExternalMergeFieldGroupId ), $objDatabase );
	}

}
?>