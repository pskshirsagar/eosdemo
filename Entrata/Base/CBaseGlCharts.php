<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlCharts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlCharts extends CEosPluralBase {

	/**
	 * @return CGlChart[]
	 */
	public static function fetchGlCharts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CGlChart', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CGlChart
	 */
	public static function fetchGlChart( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGlChart', $objDatabase );
	}

	public static function fetchGlChartCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_charts', $objDatabase );
	}

	public static function fetchGlChartByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchGlChart( sprintf( 'SELECT * FROM gl_charts WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlChartsByCid( $intCid, $objDatabase ) {
		return self::fetchGlCharts( sprintf( 'SELECT * FROM gl_charts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>