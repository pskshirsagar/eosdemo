<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledPaymentTransTypes
 * Do not add any new functions to this class.
 */

class CBaseScheduledPaymentTransTypes extends CEosPluralBase {

	/**
	 * @return CScheduledPaymentTransType[]
	 */
	public static function fetchScheduledPaymentTransTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CScheduledPaymentTransType::class, $objDatabase );
	}

	/**
	 * @return CScheduledPaymentTransType
	 */
	public static function fetchScheduledPaymentTransType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScheduledPaymentTransType::class, $objDatabase );
	}

	public static function fetchScheduledPaymentTransTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_payment_trans_types', $objDatabase );
	}

	public static function fetchScheduledPaymentTransTypeById( $intId, $objDatabase ) {
		return self::fetchScheduledPaymentTransType( sprintf( 'SELECT * FROM scheduled_payment_trans_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>