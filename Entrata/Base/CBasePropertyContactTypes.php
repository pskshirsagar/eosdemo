<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyContactTypes
 * Do not add any new functions to this class.
 */

class CBasePropertyContactTypes extends CEosPluralBase {

	/**
	 * @return CPropertyContactType[]
	 */
	public static function fetchPropertyContactTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPropertyContactType::class, $objDatabase );
	}

	/**
	 * @return CPropertyContactType
	 */
	public static function fetchPropertyContactType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyContactType::class, $objDatabase );
	}

	public static function fetchPropertyContactTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_contact_types', $objDatabase );
	}

	public static function fetchPropertyContactTypeById( $intId, $objDatabase ) {
		return self::fetchPropertyContactType( sprintf( 'SELECT * FROM property_contact_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>