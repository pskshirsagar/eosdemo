<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApiQueue extends CEosSingularBase {

	const TABLE_NAME = 'public.api_queues';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intIntegrationResultId;
	protected $m_intServiceId;
	protected $m_strContentType;
	protected $m_intMinorApiVersionId;
	protected $m_intMajorApiVersionId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strQueueMessageCorrelationId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['integration_result_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationResultId', trim( $arrValues['integration_result_id'] ) ); elseif( isset( $arrValues['integration_result_id'] ) ) $this->setIntegrationResultId( $arrValues['integration_result_id'] );
		if( isset( $arrValues['service_id'] ) && $boolDirectSet ) $this->set( 'm_intServiceId', trim( $arrValues['service_id'] ) ); elseif( isset( $arrValues['service_id'] ) ) $this->setServiceId( $arrValues['service_id'] );
		if( isset( $arrValues['content_type'] ) && $boolDirectSet ) $this->set( 'm_strContentType', trim( $arrValues['content_type'] ) ); elseif( isset( $arrValues['content_type'] ) ) $this->setContentType( $arrValues['content_type'] );
		if( isset( $arrValues['minor_api_version_id'] ) && $boolDirectSet ) $this->set( 'm_intMinorApiVersionId', trim( $arrValues['minor_api_version_id'] ) ); elseif( isset( $arrValues['minor_api_version_id'] ) ) $this->setMinorApiVersionId( $arrValues['minor_api_version_id'] );
		if( isset( $arrValues['major_api_version_id'] ) && $boolDirectSet ) $this->set( 'm_intMajorApiVersionId', trim( $arrValues['major_api_version_id'] ) ); elseif( isset( $arrValues['major_api_version_id'] ) ) $this->setMajorApiVersionId( $arrValues['major_api_version_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['queue_message_correlation_id'] ) && $boolDirectSet ) $this->set( 'm_strQueueMessageCorrelationId', trim( $arrValues['queue_message_correlation_id'] ) ); elseif( isset( $arrValues['queue_message_correlation_id'] ) ) $this->setQueueMessageCorrelationId( $arrValues['queue_message_correlation_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setIntegrationResultId( $intIntegrationResultId ) {
		$this->set( 'm_intIntegrationResultId', CStrings::strToIntDef( $intIntegrationResultId, NULL, false ) );
	}

	public function getIntegrationResultId() {
		return $this->m_intIntegrationResultId;
	}

	public function sqlIntegrationResultId() {
		return ( true == isset( $this->m_intIntegrationResultId ) ) ? ( string ) $this->m_intIntegrationResultId : 'NULL';
	}

	public function setServiceId( $intServiceId ) {
		$this->set( 'm_intServiceId', CStrings::strToIntDef( $intServiceId, NULL, false ) );
	}

	public function getServiceId() {
		return $this->m_intServiceId;
	}

	public function sqlServiceId() {
		return ( true == isset( $this->m_intServiceId ) ) ? ( string ) $this->m_intServiceId : 'NULL';
	}

	public function setContentType( $strContentType ) {
		$this->set( 'm_strContentType', CStrings::strTrimDef( $strContentType, -1, NULL, true ) );
	}

	public function getContentType() {
		return $this->m_strContentType;
	}

	public function sqlContentType() {
		return ( true == isset( $this->m_strContentType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strContentType ) : '\'' . addslashes( $this->m_strContentType ) . '\'' ) : 'NULL';
	}

	public function setMinorApiVersionId( $intMinorApiVersionId ) {
		$this->set( 'm_intMinorApiVersionId', CStrings::strToIntDef( $intMinorApiVersionId, NULL, false ) );
	}

	public function getMinorApiVersionId() {
		return $this->m_intMinorApiVersionId;
	}

	public function sqlMinorApiVersionId() {
		return ( true == isset( $this->m_intMinorApiVersionId ) ) ? ( string ) $this->m_intMinorApiVersionId : 'NULL';
	}

	public function setMajorApiVersionId( $intMajorApiVersionId ) {
		$this->set( 'm_intMajorApiVersionId', CStrings::strToIntDef( $intMajorApiVersionId, NULL, false ) );
	}

	public function getMajorApiVersionId() {
		return $this->m_intMajorApiVersionId;
	}

	public function sqlMajorApiVersionId() {
		return ( true == isset( $this->m_intMajorApiVersionId ) ) ? ( string ) $this->m_intMajorApiVersionId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setQueueMessageCorrelationId( $strQueueMessageCorrelationId ) {
		$this->set( 'm_strQueueMessageCorrelationId', CStrings::strTrimDef( $strQueueMessageCorrelationId, -1, NULL, true ) );
	}

	public function getQueueMessageCorrelationId() {
		return $this->m_strQueueMessageCorrelationId;
	}

	public function sqlQueueMessageCorrelationId() {
		return ( true == isset( $this->m_strQueueMessageCorrelationId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strQueueMessageCorrelationId ) : '\'' . addslashes( $this->m_strQueueMessageCorrelationId ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, integration_result_id, service_id, content_type, minor_api_version_id, major_api_version_id, updated_by, updated_on, created_by, created_on, queue_message_correlation_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlIntegrationResultId() . ', ' .
						$this->sqlServiceId() . ', ' .
						$this->sqlContentType() . ', ' .
						$this->sqlMinorApiVersionId() . ', ' .
						$this->sqlMajorApiVersionId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlQueueMessageCorrelationId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_result_id = ' . $this->sqlIntegrationResultId(). ',' ; } elseif( true == array_key_exists( 'IntegrationResultId', $this->getChangedColumns() ) ) { $strSql .= ' integration_result_id = ' . $this->sqlIntegrationResultId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_id = ' . $this->sqlServiceId(). ',' ; } elseif( true == array_key_exists( 'ServiceId', $this->getChangedColumns() ) ) { $strSql .= ' service_id = ' . $this->sqlServiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' content_type = ' . $this->sqlContentType(). ',' ; } elseif( true == array_key_exists( 'ContentType', $this->getChangedColumns() ) ) { $strSql .= ' content_type = ' . $this->sqlContentType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minor_api_version_id = ' . $this->sqlMinorApiVersionId(). ',' ; } elseif( true == array_key_exists( 'MinorApiVersionId', $this->getChangedColumns() ) ) { $strSql .= ' minor_api_version_id = ' . $this->sqlMinorApiVersionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' major_api_version_id = ' . $this->sqlMajorApiVersionId(). ',' ; } elseif( true == array_key_exists( 'MajorApiVersionId', $this->getChangedColumns() ) ) { $strSql .= ' major_api_version_id = ' . $this->sqlMajorApiVersionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' queue_message_correlation_id = ' . $this->sqlQueueMessageCorrelationId(). ',' ; } elseif( true == array_key_exists( 'QueueMessageCorrelationId', $this->getChangedColumns() ) ) { $strSql .= ' queue_message_correlation_id = ' . $this->sqlQueueMessageCorrelationId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'integration_result_id' => $this->getIntegrationResultId(),
			'service_id' => $this->getServiceId(),
			'content_type' => $this->getContentType(),
			'minor_api_version_id' => $this->getMinorApiVersionId(),
			'major_api_version_id' => $this->getMajorApiVersionId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'queue_message_correlation_id' => $this->getQueueMessageCorrelationId()
		);
	}

}
?>