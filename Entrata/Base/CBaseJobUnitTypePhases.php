<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobUnitTypePhases
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseJobUnitTypePhases extends CEosPluralBase {

	/**
	 * @return CJobUnitTypePhase[]
	 */
	public static function fetchJobUnitTypePhases( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CJobUnitTypePhase::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CJobUnitTypePhase
	 */
	public static function fetchJobUnitTypePhase( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CJobUnitTypePhase::class, $objDatabase );
	}

	public static function fetchJobUnitTypePhaseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_unit_type_phases', $objDatabase );
	}

	public static function fetchJobUnitTypePhaseByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchJobUnitTypePhase( sprintf( 'SELECT * FROM job_unit_type_phases WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobUnitTypePhasesByCid( $intCid, $objDatabase ) {
		return self::fetchJobUnitTypePhases( sprintf( 'SELECT * FROM job_unit_type_phases WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobUnitTypePhasesByJobIdByCid( $intJobId, $intCid, $objDatabase ) {
		return self::fetchJobUnitTypePhases( sprintf( 'SELECT * FROM job_unit_type_phases WHERE job_id = %d AND cid = %d', ( int ) $intJobId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobUnitTypePhasesByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchJobUnitTypePhases( sprintf( 'SELECT * FROM job_unit_type_phases WHERE unit_type_id = %d AND cid = %d', ( int ) $intUnitTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobUnitTypePhasesByJobPhaseIdByCid( $intJobPhaseId, $intCid, $objDatabase ) {
		return self::fetchJobUnitTypePhases( sprintf( 'SELECT * FROM job_unit_type_phases WHERE job_phase_id = %d AND cid = %d', ( int ) $intJobPhaseId, ( int ) $intCid ), $objDatabase );
	}

}
?>