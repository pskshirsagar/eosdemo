<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUtilityFormulas
 * Do not add any new functions to this class.
 */

class CBaseUtilityFormulas extends CEosPluralBase {

	/**
	 * @return CUtilityFormula[]
	 */
	public static function fetchUtilityFormulas( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CUtilityFormula::class, $objDatabase );
	}

	/**
	 * @return CUtilityFormula
	 */
	public static function fetchUtilityFormula( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CUtilityFormula::class, $objDatabase );
	}

	public static function fetchUtilityFormulaCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'utility_formulas', $objDatabase );
	}

	public static function fetchUtilityFormulaById( $intId, $objDatabase ) {
		return self::fetchUtilityFormula( sprintf( 'SELECT * FROM utility_formulas WHERE id = %d', $intId ), $objDatabase );
	}

}
?>