<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyAnnouncements
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyAnnouncements extends CEosPluralBase {

	/**
	 * @return CPropertyAnnouncement[]
	 */
	public static function fetchPropertyAnnouncements( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyAnnouncement', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyAnnouncement
	 */
	public static function fetchPropertyAnnouncement( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyAnnouncement', $objDatabase );
	}

	public static function fetchPropertyAnnouncementCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_announcements', $objDatabase );
	}

	public static function fetchPropertyAnnouncementByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyAnnouncement( sprintf( 'SELECT * FROM property_announcements WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyAnnouncementsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyAnnouncements( sprintf( 'SELECT * FROM property_announcements WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyAnnouncementsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyAnnouncements( sprintf( 'SELECT * FROM property_announcements WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyAnnouncementsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchPropertyAnnouncements( sprintf( 'SELECT * FROM property_announcements WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyAnnouncementsByAnnouncementIdByCid( $intAnnouncementId, $intCid, $objDatabase ) {
		return self::fetchPropertyAnnouncements( sprintf( 'SELECT * FROM property_announcements WHERE announcement_id = %d AND cid = %d', ( int ) $intAnnouncementId, ( int ) $intCid ), $objDatabase );
	}

}
?>