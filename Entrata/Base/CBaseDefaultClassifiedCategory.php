<?php

class CBaseDefaultClassifiedCategory extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_classified_categories';

	protected $m_intId;
	protected $m_intDefaultClassifiedCategoryId;
	protected $m_strCode;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intShowStartDate;
	protected $m_intShowEndDate;
	protected $m_intShowAmount;
	protected $m_intIsSystem;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intShowStartDate = '0';
		$this->m_intShowEndDate = '0';
		$this->m_intShowAmount = '0';
		$this->m_intIsSystem = '0';
		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['default_classified_category_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultClassifiedCategoryId', trim( $arrValues['default_classified_category_id'] ) ); elseif( isset( $arrValues['default_classified_category_id'] ) ) $this->setDefaultClassifiedCategoryId( $arrValues['default_classified_category_id'] );
		if( isset( $arrValues['code'] ) && $boolDirectSet ) $this->set( 'm_strCode', trim( stripcslashes( $arrValues['code'] ) ) ); elseif( isset( $arrValues['code'] ) ) $this->setCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['code'] ) : $arrValues['code'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['show_start_date'] ) && $boolDirectSet ) $this->set( 'm_intShowStartDate', trim( $arrValues['show_start_date'] ) ); elseif( isset( $arrValues['show_start_date'] ) ) $this->setShowStartDate( $arrValues['show_start_date'] );
		if( isset( $arrValues['show_end_date'] ) && $boolDirectSet ) $this->set( 'm_intShowEndDate', trim( $arrValues['show_end_date'] ) ); elseif( isset( $arrValues['show_end_date'] ) ) $this->setShowEndDate( $arrValues['show_end_date'] );
		if( isset( $arrValues['show_amount'] ) && $boolDirectSet ) $this->set( 'm_intShowAmount', trim( $arrValues['show_amount'] ) ); elseif( isset( $arrValues['show_amount'] ) ) $this->setShowAmount( $arrValues['show_amount'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDefaultClassifiedCategoryId( $intDefaultClassifiedCategoryId ) {
		$this->set( 'm_intDefaultClassifiedCategoryId', CStrings::strToIntDef( $intDefaultClassifiedCategoryId, NULL, false ) );
	}

	public function getDefaultClassifiedCategoryId() {
		return $this->m_intDefaultClassifiedCategoryId;
	}

	public function sqlDefaultClassifiedCategoryId() {
		return ( true == isset( $this->m_intDefaultClassifiedCategoryId ) ) ? ( string ) $this->m_intDefaultClassifiedCategoryId : 'NULL';
	}

	public function setCode( $strCode ) {
		$this->set( 'm_strCode', CStrings::strTrimDef( $strCode, 10, NULL, true ) );
	}

	public function getCode() {
		return $this->m_strCode;
	}

	public function sqlCode() {
		return ( true == isset( $this->m_strCode ) ) ? '\'' . addslashes( $this->m_strCode ) . '\'' : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setShowStartDate( $intShowStartDate ) {
		$this->set( 'm_intShowStartDate', CStrings::strToIntDef( $intShowStartDate, NULL, false ) );
	}

	public function getShowStartDate() {
		return $this->m_intShowStartDate;
	}

	public function sqlShowStartDate() {
		return ( true == isset( $this->m_intShowStartDate ) ) ? ( string ) $this->m_intShowStartDate : '0';
	}

	public function setShowEndDate( $intShowEndDate ) {
		$this->set( 'm_intShowEndDate', CStrings::strToIntDef( $intShowEndDate, NULL, false ) );
	}

	public function getShowEndDate() {
		return $this->m_intShowEndDate;
	}

	public function sqlShowEndDate() {
		return ( true == isset( $this->m_intShowEndDate ) ) ? ( string ) $this->m_intShowEndDate : '0';
	}

	public function setShowAmount( $intShowAmount ) {
		$this->set( 'm_intShowAmount', CStrings::strToIntDef( $intShowAmount, NULL, false ) );
	}

	public function getShowAmount() {
		return $this->m_intShowAmount;
	}

	public function sqlShowAmount() {
		return ( true == isset( $this->m_intShowAmount ) ) ? ( string ) $this->m_intShowAmount : '0';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'default_classified_category_id' => $this->getDefaultClassifiedCategoryId(),
			'code' => $this->getCode(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'show_start_date' => $this->getShowStartDate(),
			'show_end_date' => $this->getShowEndDate(),
			'show_amount' => $this->getShowAmount(),
			'is_system' => $this->getIsSystem(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>