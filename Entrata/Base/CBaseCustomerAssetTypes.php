<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerAssetTypes
 * Do not add any new functions to this class.
 */

class CBaseCustomerAssetTypes extends CEosPluralBase {

	/**
	 * @return CCustomerAssetType[]
	 */
	public static function fetchCustomerAssetTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCustomerAssetType::class, $objDatabase );
	}

	/**
	 * @return CCustomerAssetType
	 */
	public static function fetchCustomerAssetType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerAssetType::class, $objDatabase );
	}

	public static function fetchCustomerAssetTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_asset_types', $objDatabase );
	}

	public static function fetchCustomerAssetTypeById( $intId, $objDatabase ) {
		return self::fetchCustomerAssetType( sprintf( 'SELECT * FROM customer_asset_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>