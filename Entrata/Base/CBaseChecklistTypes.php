<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChecklistTypes
 * Do not add any new functions to this class.
 */

class CBaseChecklistTypes extends CEosPluralBase {

	/**
	 * @return CChecklistType[]
	 */
	public static function fetchChecklistTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CChecklistType::class, $objDatabase );
	}

	/**
	 * @return CChecklistType
	 */
	public static function fetchChecklistType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CChecklistType::class, $objDatabase );
	}

	public static function fetchChecklistTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'checklist_types', $objDatabase );
	}

	public static function fetchChecklistTypeById( $intId, $objDatabase ) {
		return self::fetchChecklistType( sprintf( 'SELECT * FROM checklist_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>