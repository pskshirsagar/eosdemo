<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseChoreType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.chore_types';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDefaultChoreTypeId;
	protected $m_intChoreCategoryId;
	protected $m_strName;
	protected $m_intSendEmail;
	protected $m_intIsRequiredToLockMonth;
	protected $m_intIsRequiredToAdvanceMonth;
	protected $m_intIsPublished;
	protected $m_intIsSystem;
	protected $m_intOrderNum;
	protected $m_intDisabledBy;
	protected $m_strDisabledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intSendEmail = '1';
		$this->m_intIsRequiredToLockMonth = '0';
		$this->m_intIsRequiredToAdvanceMonth = '0';
		$this->m_intIsPublished = '1';
		$this->m_intIsSystem = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['default_chore_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultChoreTypeId', trim( $arrValues['default_chore_type_id'] ) ); elseif( isset( $arrValues['default_chore_type_id'] ) ) $this->setDefaultChoreTypeId( $arrValues['default_chore_type_id'] );
		if( isset( $arrValues['chore_category_id'] ) && $boolDirectSet ) $this->set( 'm_intChoreCategoryId', trim( $arrValues['chore_category_id'] ) ); elseif( isset( $arrValues['chore_category_id'] ) ) $this->setChoreCategoryId( $arrValues['chore_category_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['send_email'] ) && $boolDirectSet ) $this->set( 'm_intSendEmail', trim( $arrValues['send_email'] ) ); elseif( isset( $arrValues['send_email'] ) ) $this->setSendEmail( $arrValues['send_email'] );
		if( isset( $arrValues['is_required_to_lock_month'] ) && $boolDirectSet ) $this->set( 'm_intIsRequiredToLockMonth', trim( $arrValues['is_required_to_lock_month'] ) ); elseif( isset( $arrValues['is_required_to_lock_month'] ) ) $this->setIsRequiredToLockMonth( $arrValues['is_required_to_lock_month'] );
		if( isset( $arrValues['is_required_to_advance_month'] ) && $boolDirectSet ) $this->set( 'm_intIsRequiredToAdvanceMonth', trim( $arrValues['is_required_to_advance_month'] ) ); elseif( isset( $arrValues['is_required_to_advance_month'] ) ) $this->setIsRequiredToAdvanceMonth( $arrValues['is_required_to_advance_month'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['disabled_by'] ) && $boolDirectSet ) $this->set( 'm_intDisabledBy', trim( $arrValues['disabled_by'] ) ); elseif( isset( $arrValues['disabled_by'] ) ) $this->setDisabledBy( $arrValues['disabled_by'] );
		if( isset( $arrValues['disabled_on'] ) && $boolDirectSet ) $this->set( 'm_strDisabledOn', trim( $arrValues['disabled_on'] ) ); elseif( isset( $arrValues['disabled_on'] ) ) $this->setDisabledOn( $arrValues['disabled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDefaultChoreTypeId( $intDefaultChoreTypeId ) {
		$this->set( 'm_intDefaultChoreTypeId', CStrings::strToIntDef( $intDefaultChoreTypeId, NULL, false ) );
	}

	public function getDefaultChoreTypeId() {
		return $this->m_intDefaultChoreTypeId;
	}

	public function sqlDefaultChoreTypeId() {
		return ( true == isset( $this->m_intDefaultChoreTypeId ) ) ? ( string ) $this->m_intDefaultChoreTypeId : 'NULL';
	}

	public function setChoreCategoryId( $intChoreCategoryId ) {
		$this->set( 'm_intChoreCategoryId', CStrings::strToIntDef( $intChoreCategoryId, NULL, false ) );
	}

	public function getChoreCategoryId() {
		return $this->m_intChoreCategoryId;
	}

	public function sqlChoreCategoryId() {
		return ( true == isset( $this->m_intChoreCategoryId ) ) ? ( string ) $this->m_intChoreCategoryId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setSendEmail( $intSendEmail ) {
		$this->set( 'm_intSendEmail', CStrings::strToIntDef( $intSendEmail, NULL, false ) );
	}

	public function getSendEmail() {
		return $this->m_intSendEmail;
	}

	public function sqlSendEmail() {
		return ( true == isset( $this->m_intSendEmail ) ) ? ( string ) $this->m_intSendEmail : '1';
	}

	public function setIsRequiredToLockMonth( $intIsRequiredToLockMonth ) {
		$this->set( 'm_intIsRequiredToLockMonth', CStrings::strToIntDef( $intIsRequiredToLockMonth, NULL, false ) );
	}

	public function getIsRequiredToLockMonth() {
		return $this->m_intIsRequiredToLockMonth;
	}

	public function sqlIsRequiredToLockMonth() {
		return ( true == isset( $this->m_intIsRequiredToLockMonth ) ) ? ( string ) $this->m_intIsRequiredToLockMonth : '0';
	}

	public function setIsRequiredToAdvanceMonth( $intIsRequiredToAdvanceMonth ) {
		$this->set( 'm_intIsRequiredToAdvanceMonth', CStrings::strToIntDef( $intIsRequiredToAdvanceMonth, NULL, false ) );
	}

	public function getIsRequiredToAdvanceMonth() {
		return $this->m_intIsRequiredToAdvanceMonth;
	}

	public function sqlIsRequiredToAdvanceMonth() {
		return ( true == isset( $this->m_intIsRequiredToAdvanceMonth ) ) ? ( string ) $this->m_intIsRequiredToAdvanceMonth : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDisabledBy( $intDisabledBy ) {
		$this->set( 'm_intDisabledBy', CStrings::strToIntDef( $intDisabledBy, NULL, false ) );
	}

	public function getDisabledBy() {
		return $this->m_intDisabledBy;
	}

	public function sqlDisabledBy() {
		return ( true == isset( $this->m_intDisabledBy ) ) ? ( string ) $this->m_intDisabledBy : 'NULL';
	}

	public function setDisabledOn( $strDisabledOn ) {
		$this->set( 'm_strDisabledOn', CStrings::strTrimDef( $strDisabledOn, -1, NULL, true ) );
	}

	public function getDisabledOn() {
		return $this->m_strDisabledOn;
	}

	public function sqlDisabledOn() {
		return ( true == isset( $this->m_strDisabledOn ) ) ? '\'' . $this->m_strDisabledOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, default_chore_type_id, chore_category_id, name, send_email, is_required_to_lock_month, is_required_to_advance_month, is_published, is_system, order_num, disabled_by, disabled_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlDefaultChoreTypeId() . ', ' .
						$this->sqlChoreCategoryId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlSendEmail() . ', ' .
						$this->sqlIsRequiredToLockMonth() . ', ' .
						$this->sqlIsRequiredToAdvanceMonth() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsSystem() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDisabledBy() . ', ' .
						$this->sqlDisabledOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_chore_type_id = ' . $this->sqlDefaultChoreTypeId(). ',' ; } elseif( true == array_key_exists( 'DefaultChoreTypeId', $this->getChangedColumns() ) ) { $strSql .= ' default_chore_type_id = ' . $this->sqlDefaultChoreTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chore_category_id = ' . $this->sqlChoreCategoryId(). ',' ; } elseif( true == array_key_exists( 'ChoreCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' chore_category_id = ' . $this->sqlChoreCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' send_email = ' . $this->sqlSendEmail(). ',' ; } elseif( true == array_key_exists( 'SendEmail', $this->getChangedColumns() ) ) { $strSql .= ' send_email = ' . $this->sqlSendEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_required_to_lock_month = ' . $this->sqlIsRequiredToLockMonth(). ',' ; } elseif( true == array_key_exists( 'IsRequiredToLockMonth', $this->getChangedColumns() ) ) { $strSql .= ' is_required_to_lock_month = ' . $this->sqlIsRequiredToLockMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_required_to_advance_month = ' . $this->sqlIsRequiredToAdvanceMonth(). ',' ; } elseif( true == array_key_exists( 'IsRequiredToAdvanceMonth', $this->getChangedColumns() ) ) { $strSql .= ' is_required_to_advance_month = ' . $this->sqlIsRequiredToAdvanceMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem(). ',' ; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy(). ',' ; } elseif( true == array_key_exists( 'DisabledBy', $this->getChangedColumns() ) ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn(). ',' ; } elseif( true == array_key_exists( 'DisabledOn', $this->getChangedColumns() ) ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'default_chore_type_id' => $this->getDefaultChoreTypeId(),
			'chore_category_id' => $this->getChoreCategoryId(),
			'name' => $this->getName(),
			'send_email' => $this->getSendEmail(),
			'is_required_to_lock_month' => $this->getIsRequiredToLockMonth(),
			'is_required_to_advance_month' => $this->getIsRequiredToAdvanceMonth(),
			'is_published' => $this->getIsPublished(),
			'is_system' => $this->getIsSystem(),
			'order_num' => $this->getOrderNum(),
			'disabled_by' => $this->getDisabledBy(),
			'disabled_on' => $this->getDisabledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>