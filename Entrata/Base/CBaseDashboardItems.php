<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDashboardItems
 * Do not add any new functions to this class.
 */

class CBaseDashboardItems extends CEosPluralBase {

	/**
	 * @return CDashboardItem[]
	 */
	public static function fetchDashboardItems( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDashboardItem::class, $objDatabase );
	}

	/**
	 * @return CDashboardItem
	 */
	public static function fetchDashboardItem( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDashboardItem::class, $objDatabase );
	}

	public static function fetchDashboardItemCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'dashboard_items', $objDatabase );
	}

	public static function fetchDashboardItemById( $intId, $objDatabase ) {
		return self::fetchDashboardItem( sprintf( 'SELECT * FROM dashboard_items WHERE id = %d', $intId ), $objDatabase );
	}

}
?>