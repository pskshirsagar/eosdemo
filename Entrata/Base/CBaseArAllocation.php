<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseArAllocation extends CEosSingularBase {

	const TABLE_NAME = 'public.ar_allocations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intLeaseId;
	protected $m_intPeriodId;
	protected $m_intGlTransactionTypeId;
	protected $m_intChargeArTransactionId;
	protected $m_intCreditArTransactionId;
	protected $m_intAccrualDebitGlAccountId;
	protected $m_intAccrualCreditGlAccountId;
	protected $m_intCashDebitGlAccountId;
	protected $m_intCashCreditGlAccountId;
	protected $m_intOriginArAllocationId;
	protected $m_intArProcessId;
	protected $m_intApAllocationId;
	protected $m_strAllocationDatetime;
	protected $m_fltAllocationAmount;
	protected $m_strReportingPostDate;
	protected $m_strPostDate;
	protected $m_strPostMonth;
	protected $m_boolChargePostToCash;
	protected $m_boolCreditPostToCash;
	protected $m_boolIsDeleted;
	protected $m_boolIsPosted;
	protected $m_boolIsInitialImport;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strAllocationDatetime = 'now()';
		$this->m_boolChargePostToCash = false;
		$this->m_boolCreditPostToCash = false;
		$this->m_boolIsDeleted = false;
		$this->m_boolIsPosted = false;
		$this->m_boolIsInitialImport = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['period_id'] ) && $boolDirectSet ) $this->set( 'm_intPeriodId', trim( $arrValues['period_id'] ) ); elseif( isset( $arrValues['period_id'] ) ) $this->setPeriodId( $arrValues['period_id'] );
		if( isset( $arrValues['gl_transaction_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlTransactionTypeId', trim( $arrValues['gl_transaction_type_id'] ) ); elseif( isset( $arrValues['gl_transaction_type_id'] ) ) $this->setGlTransactionTypeId( $arrValues['gl_transaction_type_id'] );
		if( isset( $arrValues['charge_ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intChargeArTransactionId', trim( $arrValues['charge_ar_transaction_id'] ) ); elseif( isset( $arrValues['charge_ar_transaction_id'] ) ) $this->setChargeArTransactionId( $arrValues['charge_ar_transaction_id'] );
		if( isset( $arrValues['credit_ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intCreditArTransactionId', trim( $arrValues['credit_ar_transaction_id'] ) ); elseif( isset( $arrValues['credit_ar_transaction_id'] ) ) $this->setCreditArTransactionId( $arrValues['credit_ar_transaction_id'] );
		if( isset( $arrValues['accrual_debit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccrualDebitGlAccountId', trim( $arrValues['accrual_debit_gl_account_id'] ) ); elseif( isset( $arrValues['accrual_debit_gl_account_id'] ) ) $this->setAccrualDebitGlAccountId( $arrValues['accrual_debit_gl_account_id'] );
		if( isset( $arrValues['accrual_credit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccrualCreditGlAccountId', trim( $arrValues['accrual_credit_gl_account_id'] ) ); elseif( isset( $arrValues['accrual_credit_gl_account_id'] ) ) $this->setAccrualCreditGlAccountId( $arrValues['accrual_credit_gl_account_id'] );
		if( isset( $arrValues['cash_debit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCashDebitGlAccountId', trim( $arrValues['cash_debit_gl_account_id'] ) ); elseif( isset( $arrValues['cash_debit_gl_account_id'] ) ) $this->setCashDebitGlAccountId( $arrValues['cash_debit_gl_account_id'] );
		if( isset( $arrValues['cash_credit_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCashCreditGlAccountId', trim( $arrValues['cash_credit_gl_account_id'] ) ); elseif( isset( $arrValues['cash_credit_gl_account_id'] ) ) $this->setCashCreditGlAccountId( $arrValues['cash_credit_gl_account_id'] );
		if( isset( $arrValues['origin_ar_allocation_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginArAllocationId', trim( $arrValues['origin_ar_allocation_id'] ) ); elseif( isset( $arrValues['origin_ar_allocation_id'] ) ) $this->setOriginArAllocationId( $arrValues['origin_ar_allocation_id'] );
		if( isset( $arrValues['ar_process_id'] ) && $boolDirectSet ) $this->set( 'm_intArProcessId', trim( $arrValues['ar_process_id'] ) ); elseif( isset( $arrValues['ar_process_id'] ) ) $this->setArProcessId( $arrValues['ar_process_id'] );
		if( isset( $arrValues['ap_allocation_id'] ) && $boolDirectSet ) $this->set( 'm_intApAllocationId', trim( $arrValues['ap_allocation_id'] ) ); elseif( isset( $arrValues['ap_allocation_id'] ) ) $this->setApAllocationId( $arrValues['ap_allocation_id'] );
		if( isset( $arrValues['allocation_datetime'] ) && $boolDirectSet ) $this->set( 'm_strAllocationDatetime', trim( $arrValues['allocation_datetime'] ) ); elseif( isset( $arrValues['allocation_datetime'] ) ) $this->setAllocationDatetime( $arrValues['allocation_datetime'] );
		if( isset( $arrValues['allocation_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAllocationAmount', trim( $arrValues['allocation_amount'] ) ); elseif( isset( $arrValues['allocation_amount'] ) ) $this->setAllocationAmount( $arrValues['allocation_amount'] );
		if( isset( $arrValues['reporting_post_date'] ) && $boolDirectSet ) $this->set( 'm_strReportingPostDate', trim( $arrValues['reporting_post_date'] ) ); elseif( isset( $arrValues['reporting_post_date'] ) ) $this->setReportingPostDate( $arrValues['reporting_post_date'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['charge_post_to_cash'] ) && $boolDirectSet ) $this->set( 'm_boolChargePostToCash', trim( stripcslashes( $arrValues['charge_post_to_cash'] ) ) ); elseif( isset( $arrValues['charge_post_to_cash'] ) ) $this->setChargePostToCash( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['charge_post_to_cash'] ) : $arrValues['charge_post_to_cash'] );
		if( isset( $arrValues['credit_post_to_cash'] ) && $boolDirectSet ) $this->set( 'm_boolCreditPostToCash', trim( stripcslashes( $arrValues['credit_post_to_cash'] ) ) ); elseif( isset( $arrValues['credit_post_to_cash'] ) ) $this->setCreditPostToCash( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['credit_post_to_cash'] ) : $arrValues['credit_post_to_cash'] );
		if( isset( $arrValues['is_deleted'] ) && $boolDirectSet ) $this->set( 'm_boolIsDeleted', trim( stripcslashes( $arrValues['is_deleted'] ) ) ); elseif( isset( $arrValues['is_deleted'] ) ) $this->setIsDeleted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_deleted'] ) : $arrValues['is_deleted'] );
		if( isset( $arrValues['is_posted'] ) && $boolDirectSet ) $this->set( 'm_boolIsPosted', trim( stripcslashes( $arrValues['is_posted'] ) ) ); elseif( isset( $arrValues['is_posted'] ) ) $this->setIsPosted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_posted'] ) : $arrValues['is_posted'] );
		if( isset( $arrValues['is_initial_import'] ) && $boolDirectSet ) $this->set( 'm_boolIsInitialImport', trim( stripcslashes( $arrValues['is_initial_import'] ) ) ); elseif( isset( $arrValues['is_initial_import'] ) ) $this->setIsInitialImport( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_initial_import'] ) : $arrValues['is_initial_import'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setPeriodId( $intPeriodId ) {
		$this->set( 'm_intPeriodId', CStrings::strToIntDef( $intPeriodId, NULL, false ) );
	}

	public function getPeriodId() {
		return $this->m_intPeriodId;
	}

	public function sqlPeriodId() {
		return ( true == isset( $this->m_intPeriodId ) ) ? ( string ) $this->m_intPeriodId : 'NULL';
	}

	public function setGlTransactionTypeId( $intGlTransactionTypeId ) {
		$this->set( 'm_intGlTransactionTypeId', CStrings::strToIntDef( $intGlTransactionTypeId, NULL, false ) );
	}

	public function getGlTransactionTypeId() {
		return $this->m_intGlTransactionTypeId;
	}

	public function sqlGlTransactionTypeId() {
		return ( true == isset( $this->m_intGlTransactionTypeId ) ) ? ( string ) $this->m_intGlTransactionTypeId : 'NULL';
	}

	public function setChargeArTransactionId( $intChargeArTransactionId ) {
		$this->set( 'm_intChargeArTransactionId', CStrings::strToIntDef( $intChargeArTransactionId, NULL, false ) );
	}

	public function getChargeArTransactionId() {
		return $this->m_intChargeArTransactionId;
	}

	public function sqlChargeArTransactionId() {
		return ( true == isset( $this->m_intChargeArTransactionId ) ) ? ( string ) $this->m_intChargeArTransactionId : 'NULL';
	}

	public function setCreditArTransactionId( $intCreditArTransactionId ) {
		$this->set( 'm_intCreditArTransactionId', CStrings::strToIntDef( $intCreditArTransactionId, NULL, false ) );
	}

	public function getCreditArTransactionId() {
		return $this->m_intCreditArTransactionId;
	}

	public function sqlCreditArTransactionId() {
		return ( true == isset( $this->m_intCreditArTransactionId ) ) ? ( string ) $this->m_intCreditArTransactionId : 'NULL';
	}

	public function setAccrualDebitGlAccountId( $intAccrualDebitGlAccountId ) {
		$this->set( 'm_intAccrualDebitGlAccountId', CStrings::strToIntDef( $intAccrualDebitGlAccountId, NULL, false ) );
	}

	public function getAccrualDebitGlAccountId() {
		return $this->m_intAccrualDebitGlAccountId;
	}

	public function sqlAccrualDebitGlAccountId() {
		return ( true == isset( $this->m_intAccrualDebitGlAccountId ) ) ? ( string ) $this->m_intAccrualDebitGlAccountId : 'NULL';
	}

	public function setAccrualCreditGlAccountId( $intAccrualCreditGlAccountId ) {
		$this->set( 'm_intAccrualCreditGlAccountId', CStrings::strToIntDef( $intAccrualCreditGlAccountId, NULL, false ) );
	}

	public function getAccrualCreditGlAccountId() {
		return $this->m_intAccrualCreditGlAccountId;
	}

	public function sqlAccrualCreditGlAccountId() {
		return ( true == isset( $this->m_intAccrualCreditGlAccountId ) ) ? ( string ) $this->m_intAccrualCreditGlAccountId : 'NULL';
	}

	public function setCashDebitGlAccountId( $intCashDebitGlAccountId ) {
		$this->set( 'm_intCashDebitGlAccountId', CStrings::strToIntDef( $intCashDebitGlAccountId, NULL, false ) );
	}

	public function getCashDebitGlAccountId() {
		return $this->m_intCashDebitGlAccountId;
	}

	public function sqlCashDebitGlAccountId() {
		return ( true == isset( $this->m_intCashDebitGlAccountId ) ) ? ( string ) $this->m_intCashDebitGlAccountId : 'NULL';
	}

	public function setCashCreditGlAccountId( $intCashCreditGlAccountId ) {
		$this->set( 'm_intCashCreditGlAccountId', CStrings::strToIntDef( $intCashCreditGlAccountId, NULL, false ) );
	}

	public function getCashCreditGlAccountId() {
		return $this->m_intCashCreditGlAccountId;
	}

	public function sqlCashCreditGlAccountId() {
		return ( true == isset( $this->m_intCashCreditGlAccountId ) ) ? ( string ) $this->m_intCashCreditGlAccountId : 'NULL';
	}

	public function setOriginArAllocationId( $intOriginArAllocationId ) {
		$this->set( 'm_intOriginArAllocationId', CStrings::strToIntDef( $intOriginArAllocationId, NULL, false ) );
	}

	public function getOriginArAllocationId() {
		return $this->m_intOriginArAllocationId;
	}

	public function sqlOriginArAllocationId() {
		return ( true == isset( $this->m_intOriginArAllocationId ) ) ? ( string ) $this->m_intOriginArAllocationId : 'NULL';
	}

	public function setArProcessId( $intArProcessId ) {
		$this->set( 'm_intArProcessId', CStrings::strToIntDef( $intArProcessId, NULL, false ) );
	}

	public function getArProcessId() {
		return $this->m_intArProcessId;
	}

	public function sqlArProcessId() {
		return ( true == isset( $this->m_intArProcessId ) ) ? ( string ) $this->m_intArProcessId : 'NULL';
	}

	public function setApAllocationId( $intApAllocationId ) {
		$this->set( 'm_intApAllocationId', CStrings::strToIntDef( $intApAllocationId, NULL, false ) );
	}

	public function getApAllocationId() {
		return $this->m_intApAllocationId;
	}

	public function sqlApAllocationId() {
		return ( true == isset( $this->m_intApAllocationId ) ) ? ( string ) $this->m_intApAllocationId : 'NULL';
	}

	public function setAllocationDatetime( $strAllocationDatetime ) {
		$this->set( 'm_strAllocationDatetime', CStrings::strTrimDef( $strAllocationDatetime, -1, NULL, true ) );
	}

	public function getAllocationDatetime() {
		return $this->m_strAllocationDatetime;
	}

	public function sqlAllocationDatetime() {
		return ( true == isset( $this->m_strAllocationDatetime ) ) ? '\'' . $this->m_strAllocationDatetime . '\'' : 'NOW()';
	}

	public function setAllocationAmount( $fltAllocationAmount ) {
		$this->set( 'm_fltAllocationAmount', CStrings::strToFloatDef( $fltAllocationAmount, NULL, false, 2 ) );
	}

	public function getAllocationAmount() {
		return $this->m_fltAllocationAmount;
	}

	public function sqlAllocationAmount() {
		return ( true == isset( $this->m_fltAllocationAmount ) ) ? ( string ) $this->m_fltAllocationAmount : 'NULL';
	}

	public function setReportingPostDate( $strReportingPostDate ) {
		$this->set( 'm_strReportingPostDate', CStrings::strTrimDef( $strReportingPostDate, -1, NULL, true ) );
	}

	public function getReportingPostDate() {
		return $this->m_strReportingPostDate;
	}

	public function sqlReportingPostDate() {
		return ( true == isset( $this->m_strReportingPostDate ) ) ? '\'' . $this->m_strReportingPostDate . '\'' : 'NOW()';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setChargePostToCash( $boolChargePostToCash ) {
		$this->set( 'm_boolChargePostToCash', CStrings::strToBool( $boolChargePostToCash ) );
	}

	public function getChargePostToCash() {
		return $this->m_boolChargePostToCash;
	}

	public function sqlChargePostToCash() {
		return ( true == isset( $this->m_boolChargePostToCash ) ) ? '\'' . ( true == ( bool ) $this->m_boolChargePostToCash ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreditPostToCash( $boolCreditPostToCash ) {
		$this->set( 'm_boolCreditPostToCash', CStrings::strToBool( $boolCreditPostToCash ) );
	}

	public function getCreditPostToCash() {
		return $this->m_boolCreditPostToCash;
	}

	public function sqlCreditPostToCash() {
		return ( true == isset( $this->m_boolCreditPostToCash ) ) ? '\'' . ( true == ( bool ) $this->m_boolCreditPostToCash ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDeleted( $boolIsDeleted ) {
		$this->set( 'm_boolIsDeleted', CStrings::strToBool( $boolIsDeleted ) );
	}

	public function getIsDeleted() {
		return $this->m_boolIsDeleted;
	}

	public function sqlIsDeleted() {
		return ( true == isset( $this->m_boolIsDeleted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDeleted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPosted( $boolIsPosted ) {
		$this->set( 'm_boolIsPosted', CStrings::strToBool( $boolIsPosted ) );
	}

	public function getIsPosted() {
		return $this->m_boolIsPosted;
	}

	public function sqlIsPosted() {
		return ( true == isset( $this->m_boolIsPosted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPosted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsInitialImport( $boolIsInitialImport ) {
		$this->set( 'm_boolIsInitialImport', CStrings::strToBool( $boolIsInitialImport ) );
	}

	public function getIsInitialImport() {
		return $this->m_boolIsInitialImport;
	}

	public function sqlIsInitialImport() {
		return ( true == isset( $this->m_boolIsInitialImport ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInitialImport ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, lease_id, period_id, gl_transaction_type_id, charge_ar_transaction_id, credit_ar_transaction_id, accrual_debit_gl_account_id, accrual_credit_gl_account_id, cash_debit_gl_account_id, cash_credit_gl_account_id, origin_ar_allocation_id, ar_process_id, ap_allocation_id, allocation_datetime, allocation_amount, reporting_post_date, post_date, post_month, charge_post_to_cash, credit_post_to_cash, is_deleted, is_posted, is_initial_import, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlPeriodId() . ', ' .
 						$this->sqlGlTransactionTypeId() . ', ' .
 						$this->sqlChargeArTransactionId() . ', ' .
 						$this->sqlCreditArTransactionId() . ', ' .
 						$this->sqlAccrualDebitGlAccountId() . ', ' .
 						$this->sqlAccrualCreditGlAccountId() . ', ' .
 						$this->sqlCashDebitGlAccountId() . ', ' .
 						$this->sqlCashCreditGlAccountId() . ', ' .
 						$this->sqlOriginArAllocationId() . ', ' .
 						$this->sqlArProcessId() . ', ' .
 						$this->sqlApAllocationId() . ', ' .
 						$this->sqlAllocationDatetime() . ', ' .
 						$this->sqlAllocationAmount() . ', ' .
 						$this->sqlReportingPostDate() . ', ' .
 						$this->sqlPostDate() . ', ' .
 						$this->sqlPostMonth() . ', ' .
 						$this->sqlChargePostToCash() . ', ' .
 						$this->sqlCreditPostToCash() . ', ' .
 						$this->sqlIsDeleted() . ', ' .
 						$this->sqlIsPosted() . ', ' .
 						$this->sqlIsInitialImport() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; } elseif( true == array_key_exists( 'PeriodId', $this->getChangedColumns() ) ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_transaction_type_id = ' . $this->sqlGlTransactionTypeId() . ','; } elseif( true == array_key_exists( 'GlTransactionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_transaction_type_id = ' . $this->sqlGlTransactionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_ar_transaction_id = ' . $this->sqlChargeArTransactionId() . ','; } elseif( true == array_key_exists( 'ChargeArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' charge_ar_transaction_id = ' . $this->sqlChargeArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_ar_transaction_id = ' . $this->sqlCreditArTransactionId() . ','; } elseif( true == array_key_exists( 'CreditArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' credit_ar_transaction_id = ' . $this->sqlCreditArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accrual_debit_gl_account_id = ' . $this->sqlAccrualDebitGlAccountId() . ','; } elseif( true == array_key_exists( 'AccrualDebitGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' accrual_debit_gl_account_id = ' . $this->sqlAccrualDebitGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accrual_credit_gl_account_id = ' . $this->sqlAccrualCreditGlAccountId() . ','; } elseif( true == array_key_exists( 'AccrualCreditGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' accrual_credit_gl_account_id = ' . $this->sqlAccrualCreditGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cash_debit_gl_account_id = ' . $this->sqlCashDebitGlAccountId() . ','; } elseif( true == array_key_exists( 'CashDebitGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' cash_debit_gl_account_id = ' . $this->sqlCashDebitGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cash_credit_gl_account_id = ' . $this->sqlCashCreditGlAccountId() . ','; } elseif( true == array_key_exists( 'CashCreditGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' cash_credit_gl_account_id = ' . $this->sqlCashCreditGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' origin_ar_allocation_id = ' . $this->sqlOriginArAllocationId() . ','; } elseif( true == array_key_exists( 'OriginArAllocationId', $this->getChangedColumns() ) ) { $strSql .= ' origin_ar_allocation_id = ' . $this->sqlOriginArAllocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_process_id = ' . $this->sqlArProcessId() . ','; } elseif( true == array_key_exists( 'ArProcessId', $this->getChangedColumns() ) ) { $strSql .= ' ar_process_id = ' . $this->sqlArProcessId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_allocation_id = ' . $this->sqlApAllocationId() . ','; } elseif( true == array_key_exists( 'ApAllocationId', $this->getChangedColumns() ) ) { $strSql .= ' ap_allocation_id = ' . $this->sqlApAllocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allocation_datetime = ' . $this->sqlAllocationDatetime() . ','; } elseif( true == array_key_exists( 'AllocationDatetime', $this->getChangedColumns() ) ) { $strSql .= ' allocation_datetime = ' . $this->sqlAllocationDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allocation_amount = ' . $this->sqlAllocationAmount() . ','; } elseif( true == array_key_exists( 'AllocationAmount', $this->getChangedColumns() ) ) { $strSql .= ' allocation_amount = ' . $this->sqlAllocationAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate() . ','; } elseif( true == array_key_exists( 'ReportingPostDate', $this->getChangedColumns() ) ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_post_to_cash = ' . $this->sqlChargePostToCash() . ','; } elseif( true == array_key_exists( 'ChargePostToCash', $this->getChangedColumns() ) ) { $strSql .= ' charge_post_to_cash = ' . $this->sqlChargePostToCash() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_post_to_cash = ' . $this->sqlCreditPostToCash() . ','; } elseif( true == array_key_exists( 'CreditPostToCash', $this->getChangedColumns() ) ) { $strSql .= ' credit_post_to_cash = ' . $this->sqlCreditPostToCash() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted() . ','; } elseif( true == array_key_exists( 'IsDeleted', $this->getChangedColumns() ) ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_posted = ' . $this->sqlIsPosted() . ','; } elseif( true == array_key_exists( 'IsPosted', $this->getChangedColumns() ) ) { $strSql .= ' is_posted = ' . $this->sqlIsPosted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_initial_import = ' . $this->sqlIsInitialImport() . ','; } elseif( true == array_key_exists( 'IsInitialImport', $this->getChangedColumns() ) ) { $strSql .= ' is_initial_import = ' . $this->sqlIsInitialImport() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'lease_id' => $this->getLeaseId(),
			'period_id' => $this->getPeriodId(),
			'gl_transaction_type_id' => $this->getGlTransactionTypeId(),
			'charge_ar_transaction_id' => $this->getChargeArTransactionId(),
			'credit_ar_transaction_id' => $this->getCreditArTransactionId(),
			'accrual_debit_gl_account_id' => $this->getAccrualDebitGlAccountId(),
			'accrual_credit_gl_account_id' => $this->getAccrualCreditGlAccountId(),
			'cash_debit_gl_account_id' => $this->getCashDebitGlAccountId(),
			'cash_credit_gl_account_id' => $this->getCashCreditGlAccountId(),
			'origin_ar_allocation_id' => $this->getOriginArAllocationId(),
			'ar_process_id' => $this->getArProcessId(),
			'ap_allocation_id' => $this->getApAllocationId(),
			'allocation_datetime' => $this->getAllocationDatetime(),
			'allocation_amount' => $this->getAllocationAmount(),
			'reporting_post_date' => $this->getReportingPostDate(),
			'post_date' => $this->getPostDate(),
			'post_month' => $this->getPostMonth(),
			'charge_post_to_cash' => $this->getChargePostToCash(),
			'credit_post_to_cash' => $this->getCreditPostToCash(),
			'is_deleted' => $this->getIsDeleted(),
			'is_posted' => $this->getIsPosted(),
			'is_initial_import' => $this->getIsInitialImport(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>