<?php

class CBaseArRuleTypeConditionAssociation extends CEosSingularBase {

	const TABLE_NAME = 'public.ar_rule_type_condition_associations';

	protected $m_intId;
	protected $m_intArRuleTypeId;
	protected $m_intArRuleConditionTypeId;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ar_rule_type_id'] ) && $boolDirectSet ) $this->set( 'm_intArRuleTypeId', trim( $arrValues['ar_rule_type_id'] ) ); elseif( isset( $arrValues['ar_rule_type_id'] ) ) $this->setArRuleTypeId( $arrValues['ar_rule_type_id'] );
		if( isset( $arrValues['ar_rule_condition_type_id'] ) && $boolDirectSet ) $this->set( 'm_intArRuleConditionTypeId', trim( $arrValues['ar_rule_condition_type_id'] ) ); elseif( isset( $arrValues['ar_rule_condition_type_id'] ) ) $this->setArRuleConditionTypeId( $arrValues['ar_rule_condition_type_id'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setArRuleTypeId( $intArRuleTypeId ) {
		$this->set( 'm_intArRuleTypeId', CStrings::strToIntDef( $intArRuleTypeId, NULL, false ) );
	}

	public function getArRuleTypeId() {
		return $this->m_intArRuleTypeId;
	}

	public function sqlArRuleTypeId() {
		return ( true == isset( $this->m_intArRuleTypeId ) ) ? ( string ) $this->m_intArRuleTypeId : 'NULL';
	}

	public function setArRuleConditionTypeId( $intArRuleConditionTypeId ) {
		$this->set( 'm_intArRuleConditionTypeId', CStrings::strToIntDef( $intArRuleConditionTypeId, NULL, false ) );
	}

	public function getArRuleConditionTypeId() {
		return $this->m_intArRuleConditionTypeId;
	}

	public function sqlArRuleConditionTypeId() {
		return ( true == isset( $this->m_intArRuleConditionTypeId ) ) ? ( string ) $this->m_intArRuleConditionTypeId : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ar_rule_type_id' => $this->getArRuleTypeId(),
			'ar_rule_condition_type_id' => $this->getArRuleConditionTypeId(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>