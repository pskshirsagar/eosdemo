<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLoggedActions
 * Do not add any new functions to this class.
 */

class CBaseLoggedActions extends CEosPluralBase {

	/**
	 * @return CLoggedAction[]
	 */
	public static function fetchLoggedActions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CLoggedAction', $objDatabase );
	}

	/**
	 * @return CLoggedAction
	 */
	public static function fetchLoggedAction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CLoggedAction', $objDatabase );
	}

	public static function fetchLoggedActionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'logged_actions', $objDatabase );
	}

	public static function fetchLoggedActionById( $intId, $objDatabase ) {
		return self::fetchLoggedAction( sprintf( 'SELECT * FROM logged_actions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>