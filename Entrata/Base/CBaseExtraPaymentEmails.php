<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CExtraPaymentEmails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseExtraPaymentEmails extends CEosPluralBase {

	/**
	 * @return CExtraPaymentEmail[]
	 */
	public static function fetchExtraPaymentEmails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CExtraPaymentEmail', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CExtraPaymentEmail
	 */
	public static function fetchExtraPaymentEmail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CExtraPaymentEmail', $objDatabase );
	}

	public static function fetchExtraPaymentEmailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'extra_payment_emails', $objDatabase );
	}

	public static function fetchExtraPaymentEmailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchExtraPaymentEmail( sprintf( 'SELECT * FROM extra_payment_emails WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchExtraPaymentEmailsByCid( $intCid, $objDatabase ) {
		return self::fetchExtraPaymentEmails( sprintf( 'SELECT * FROM extra_payment_emails WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchExtraPaymentEmailsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchExtraPaymentEmails( sprintf( 'SELECT * FROM extra_payment_emails WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchExtraPaymentEmailsByWebsiteIdByCid( $intWebsiteId, $intCid, $objDatabase ) {
		return self::fetchExtraPaymentEmails( sprintf( 'SELECT * FROM extra_payment_emails WHERE website_id = %d AND cid = %d', ( int ) $intWebsiteId, ( int ) $intCid ), $objDatabase );
	}

}
?>