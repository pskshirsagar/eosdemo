<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetWorkbookPropertyGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetWorkbookPropertyGroups extends CEosPluralBase {

	/**
	 * @return CBudgetWorkbookPropertyGroup[]
	 */
	public static function fetchBudgetWorkbookPropertyGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CBudgetWorkbookPropertyGroup::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBudgetWorkbookPropertyGroup
	 */
	public static function fetchBudgetWorkbookPropertyGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetWorkbookPropertyGroup::class, $objDatabase );
	}

	public static function fetchBudgetWorkbookPropertyGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_workbook_property_groups', $objDatabase );
	}

	public static function fetchBudgetWorkbookPropertyGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorkbookPropertyGroup( sprintf( 'SELECT * FROM budget_workbook_property_groups WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorkbookPropertyGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchBudgetWorkbookPropertyGroups( sprintf( 'SELECT * FROM budget_workbook_property_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorkbookPropertyGroupsByBudgetWorkbookIdByCid( $intBudgetWorkbookId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorkbookPropertyGroups( sprintf( 'SELECT * FROM budget_workbook_property_groups WHERE budget_workbook_id = %d AND cid = %d', ( int ) $intBudgetWorkbookId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorkbookPropertyGroupsByPropertyGroupIdByCid( $intPropertyGroupId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorkbookPropertyGroups( sprintf( 'SELECT * FROM budget_workbook_property_groups WHERE property_group_id = %d AND cid = %d', ( int ) $intPropertyGroupId, ( int ) $intCid ), $objDatabase );
	}

}
?>