<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerGuest extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.customer_guests';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerId;
	protected $m_intLeaseId;
	protected $m_intGuestTypeId;
	protected $m_intGuestFrequencyTypeId;
	protected $m_strGuestName;
	protected $m_strPhoneNumber;
	protected $m_strWeekDays;
	protected $m_strStartTime;
	protected $m_strEndTime;
	protected $m_intFrequency;
	protected $m_strMonthDays;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_strCustomGuestTypeName;
	protected $m_intIsKeyPermitted;
	protected $m_intIsNoExpiration;
	protected $m_boolIsParcelPickupPermitted;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strPreferredName;
	protected $m_boolIsPropertyInterest;
	protected $m_strGender;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strEmailAddress;
	protected $m_intCompanyMediaFileId;

	public function __construct() {
		parent::__construct();

		$this->m_intIsKeyPermitted = '0';
		$this->m_intIsNoExpiration = '0';
		$this->m_boolIsParcelPickupPermitted = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['guest_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGuestTypeId', trim( $arrValues['guest_type_id'] ) ); elseif( isset( $arrValues['guest_type_id'] ) ) $this->setGuestTypeId( $arrValues['guest_type_id'] );
		if( isset( $arrValues['guest_frequency_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGuestFrequencyTypeId', trim( $arrValues['guest_frequency_type_id'] ) ); elseif( isset( $arrValues['guest_frequency_type_id'] ) ) $this->setGuestFrequencyTypeId( $arrValues['guest_frequency_type_id'] );
		if( isset( $arrValues['guest_name'] ) && $boolDirectSet ) $this->set( 'm_strGuestName', trim( $arrValues['guest_name'] ) ); elseif( isset( $arrValues['guest_name'] ) ) $this->setGuestName( $arrValues['guest_name'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( $arrValues['phone_number'] ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( $arrValues['phone_number'] );
		if( isset( $arrValues['week_days'] ) && $boolDirectSet ) $this->set( 'm_strWeekDays', trim( $arrValues['week_days'] ) ); elseif( isset( $arrValues['week_days'] ) ) $this->setWeekDays( $arrValues['week_days'] );
		if( isset( $arrValues['start_time'] ) && $boolDirectSet ) $this->set( 'm_strStartTime', trim( $arrValues['start_time'] ) ); elseif( isset( $arrValues['start_time'] ) ) $this->setStartTime( $arrValues['start_time'] );
		if( isset( $arrValues['end_time'] ) && $boolDirectSet ) $this->set( 'm_strEndTime', trim( $arrValues['end_time'] ) ); elseif( isset( $arrValues['end_time'] ) ) $this->setEndTime( $arrValues['end_time'] );
		if( isset( $arrValues['frequency'] ) && $boolDirectSet ) $this->set( 'm_intFrequency', trim( $arrValues['frequency'] ) ); elseif( isset( $arrValues['frequency'] ) ) $this->setFrequency( $arrValues['frequency'] );
		if( isset( $arrValues['month_days'] ) && $boolDirectSet ) $this->set( 'm_strMonthDays', trim( $arrValues['month_days'] ) ); elseif( isset( $arrValues['month_days'] ) ) $this->setMonthDays( $arrValues['month_days'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['custom_guest_type_name'] ) && $boolDirectSet ) $this->set( 'm_strCustomGuestTypeName', trim( $arrValues['custom_guest_type_name'] ) ); elseif( isset( $arrValues['custom_guest_type_name'] ) ) $this->setCustomGuestTypeName( $arrValues['custom_guest_type_name'] );
		if( isset( $arrValues['is_key_permitted'] ) && $boolDirectSet ) $this->set( 'm_intIsKeyPermitted', trim( $arrValues['is_key_permitted'] ) ); elseif( isset( $arrValues['is_key_permitted'] ) ) $this->setIsKeyPermitted( $arrValues['is_key_permitted'] );
		if( isset( $arrValues['is_no_expiration'] ) && $boolDirectSet ) $this->set( 'm_intIsNoExpiration', trim( $arrValues['is_no_expiration'] ) ); elseif( isset( $arrValues['is_no_expiration'] ) ) $this->setIsNoExpiration( $arrValues['is_no_expiration'] );
		if( isset( $arrValues['is_parcel_pickup_permitted'] ) && $boolDirectSet ) $this->set( 'm_boolIsParcelPickupPermitted', trim( stripcslashes( $arrValues['is_parcel_pickup_permitted'] ) ) ); elseif( isset( $arrValues['is_parcel_pickup_permitted'] ) ) $this->setIsParcelPickupPermitted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_parcel_pickup_permitted'] ) : $arrValues['is_parcel_pickup_permitted'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['preferred_name'] ) && $boolDirectSet ) $this->set( 'm_strPreferredName', trim( $arrValues['preferred_name'] ) ); elseif( isset( $arrValues['preferred_name'] ) ) $this->setPreferredName( $arrValues['preferred_name'] );
		if( isset( $arrValues['is_property_interest'] ) && $boolDirectSet ) $this->set( 'm_boolIsPropertyInterest', trim( stripcslashes( $arrValues['is_property_interest'] ) ) ); elseif( isset( $arrValues['is_property_interest'] ) ) $this->setIsPropertyInterest( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_property_interest'] ) : $arrValues['is_property_interest'] );
		if( isset( $arrValues['gender'] ) && $boolDirectSet ) $this->set( 'm_strGender', trim( $arrValues['gender'] ) ); elseif( isset( $arrValues['gender'] ) ) $this->setGender( $arrValues['gender'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['company_media_file_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMediaFileId', trim( $arrValues['company_media_file_id'] ) ); elseif( isset( $arrValues['company_media_file_id'] ) ) $this->setCompanyMediaFileId( $arrValues['company_media_file_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setGuestTypeId( $intGuestTypeId ) {
		$this->set( 'm_intGuestTypeId', CStrings::strToIntDef( $intGuestTypeId, NULL, false ) );
	}

	public function getGuestTypeId() {
		return $this->m_intGuestTypeId;
	}

	public function sqlGuestTypeId() {
		return ( true == isset( $this->m_intGuestTypeId ) ) ? ( string ) $this->m_intGuestTypeId : 'NULL';
	}

	public function setGuestFrequencyTypeId( $intGuestFrequencyTypeId ) {
		$this->set( 'm_intGuestFrequencyTypeId', CStrings::strToIntDef( $intGuestFrequencyTypeId, NULL, false ) );
	}

	public function getGuestFrequencyTypeId() {
		return $this->m_intGuestFrequencyTypeId;
	}

	public function sqlGuestFrequencyTypeId() {
		return ( true == isset( $this->m_intGuestFrequencyTypeId ) ) ? ( string ) $this->m_intGuestFrequencyTypeId : 'NULL';
	}

	public function setGuestName( $strGuestName ) {
		$this->set( 'm_strGuestName', CStrings::strTrimDef( $strGuestName, 70, NULL, true ) );
	}

	public function getGuestName() {
		return $this->m_strGuestName;
	}

	public function sqlGuestName() {
		return ( true == isset( $this->m_strGuestName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strGuestName ) : '\'' . addslashes( $this->m_strGuestName ) . '\'' ) : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNumber ) : '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setWeekDays( $strWeekDays ) {
		$this->set( 'm_strWeekDays', CStrings::strTrimDef( $strWeekDays, 15, NULL, true ) );
	}

	public function getWeekDays() {
		return $this->m_strWeekDays;
	}

	public function sqlWeekDays() {
		return ( true == isset( $this->m_strWeekDays ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWeekDays ) : '\'' . addslashes( $this->m_strWeekDays ) . '\'' ) : 'NULL';
	}

	public function setStartTime( $strStartTime ) {
		$this->set( 'm_strStartTime', CStrings::strTrimDef( $strStartTime, NULL, NULL, true ) );
	}

	public function getStartTime() {
		return $this->m_strStartTime;
	}

	public function sqlStartTime() {
		return ( true == isset( $this->m_strStartTime ) ) ? '\'' . $this->m_strStartTime . '\'' : 'NULL';
	}

	public function setEndTime( $strEndTime ) {
		$this->set( 'm_strEndTime', CStrings::strTrimDef( $strEndTime, NULL, NULL, true ) );
	}

	public function getEndTime() {
		return $this->m_strEndTime;
	}

	public function sqlEndTime() {
		return ( true == isset( $this->m_strEndTime ) ) ? '\'' . $this->m_strEndTime . '\'' : 'NULL';
	}

	public function setFrequency( $intFrequency ) {
		$this->set( 'm_intFrequency', CStrings::strToIntDef( $intFrequency, NULL, false ) );
	}

	public function getFrequency() {
		return $this->m_intFrequency;
	}

	public function sqlFrequency() {
		return ( true == isset( $this->m_intFrequency ) ) ? ( string ) $this->m_intFrequency : 'NULL';
	}

	public function setMonthDays( $strMonthDays ) {
		$this->set( 'm_strMonthDays', CStrings::strTrimDef( $strMonthDays, 95, NULL, true ) );
	}

	public function getMonthDays() {
		return $this->m_strMonthDays;
	}

	public function sqlMonthDays() {
		return ( true == isset( $this->m_strMonthDays ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMonthDays ) : '\'' . addslashes( $this->m_strMonthDays ) . '\'' ) : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NOW()';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setCustomGuestTypeName( $strCustomGuestTypeName ) {
		$this->set( 'm_strCustomGuestTypeName', CStrings::strTrimDef( $strCustomGuestTypeName, 50, NULL, true ) );
	}

	public function getCustomGuestTypeName() {
		return $this->m_strCustomGuestTypeName;
	}

	public function sqlCustomGuestTypeName() {
		return ( true == isset( $this->m_strCustomGuestTypeName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCustomGuestTypeName ) : '\'' . addslashes( $this->m_strCustomGuestTypeName ) . '\'' ) : 'NULL';
	}

	public function setIsKeyPermitted( $intIsKeyPermitted ) {
		$this->set( 'm_intIsKeyPermitted', CStrings::strToIntDef( $intIsKeyPermitted, NULL, false ) );
	}

	public function getIsKeyPermitted() {
		return $this->m_intIsKeyPermitted;
	}

	public function sqlIsKeyPermitted() {
		return ( true == isset( $this->m_intIsKeyPermitted ) ) ? ( string ) $this->m_intIsKeyPermitted : '0';
	}

	public function setIsNoExpiration( $intIsNoExpiration ) {
		$this->set( 'm_intIsNoExpiration', CStrings::strToIntDef( $intIsNoExpiration, NULL, false ) );
	}

	public function getIsNoExpiration() {
		return $this->m_intIsNoExpiration;
	}

	public function sqlIsNoExpiration() {
		return ( true == isset( $this->m_intIsNoExpiration ) ) ? ( string ) $this->m_intIsNoExpiration : '0';
	}

	public function setIsParcelPickupPermitted( $boolIsParcelPickupPermitted ) {
		$this->set( 'm_boolIsParcelPickupPermitted', CStrings::strToBool( $boolIsParcelPickupPermitted ) );
	}

	public function getIsParcelPickupPermitted() {
		return $this->m_boolIsParcelPickupPermitted;
	}

	public function sqlIsParcelPickupPermitted() {
		return ( true == isset( $this->m_boolIsParcelPickupPermitted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsParcelPickupPermitted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setPreferredName( $strPreferredName ) {
		$this->set( 'm_strPreferredName', CStrings::strTrimDef( $strPreferredName, 200, NULL, true ) );
	}

	public function getPreferredName() {
		return $this->m_strPreferredName;
	}

	public function sqlPreferredName() {
		return ( true == isset( $this->m_strPreferredName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreferredName ) : '\'' . addslashes( $this->m_strPreferredName ) . '\'' ) : 'NULL';
	}

	public function setIsPropertyInterest( $boolIsPropertyInterest ) {
		$this->set( 'm_boolIsPropertyInterest', CStrings::strToBool( $boolIsPropertyInterest ) );
	}

	public function getIsPropertyInterest() {
		return $this->m_boolIsPropertyInterest;
	}

	public function sqlIsPropertyInterest() {
		return ( true == isset( $this->m_boolIsPropertyInterest ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPropertyInterest ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setGender( $strGender ) {
		$this->set( 'm_strGender', CStrings::strTrimDef( $strGender, 20, NULL, true ) );
	}

	public function getGender() {
		return $this->m_strGender;
	}

	public function sqlGender() {
		return ( true == isset( $this->m_strGender ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strGender ) : '\'' . addslashes( $this->m_strGender ) . '\'' ) : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
		$this->set( 'm_intCompanyMediaFileId', CStrings::strToIntDef( $intCompanyMediaFileId, NULL, false ) );
	}

	public function getCompanyMediaFileId() {
		return $this->m_intCompanyMediaFileId;
	}

	public function sqlCompanyMediaFileId() {
		return ( true == isset( $this->m_intCompanyMediaFileId ) ) ? ( string ) $this->m_intCompanyMediaFileId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_id, lease_id, guest_type_id, guest_frequency_type_id, guest_name, phone_number, week_days, start_time, end_time, frequency, month_days, start_date, end_date, custom_guest_type_name, is_key_permitted, is_no_expiration, is_parcel_pickup_permitted, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, preferred_name, is_property_interest, gender, details, email_address, company_media_file_id )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlCustomerId() . ', ' .
		          $this->sqlLeaseId() . ', ' .
		          $this->sqlGuestTypeId() . ', ' .
		          $this->sqlGuestFrequencyTypeId() . ', ' .
		          $this->sqlGuestName() . ', ' .
		          $this->sqlPhoneNumber() . ', ' .
		          $this->sqlWeekDays() . ', ' .
		          $this->sqlStartTime() . ', ' .
		          $this->sqlEndTime() . ', ' .
		          $this->sqlFrequency() . ', ' .
		          $this->sqlMonthDays() . ', ' .
		          $this->sqlStartDate() . ', ' .
		          $this->sqlEndDate() . ', ' .
		          $this->sqlCustomGuestTypeName() . ', ' .
		          $this->sqlIsKeyPermitted() . ', ' .
		          $this->sqlIsNoExpiration() . ', ' .
		          $this->sqlIsParcelPickupPermitted() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlPreferredName() . ', ' .
		          $this->sqlIsPropertyInterest() . ', ' .
		          $this->sqlGender() . ', ' .
		          $this->sqlDetails() . ', ' .
		          $this->sqlEmailAddress() . ', ' .
		          $this->sqlCompanyMediaFileId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' guest_type_id = ' . $this->sqlGuestTypeId(). ',' ; } elseif( true == array_key_exists( 'GuestTypeId', $this->getChangedColumns() ) ) { $strSql .= ' guest_type_id = ' . $this->sqlGuestTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' guest_frequency_type_id = ' . $this->sqlGuestFrequencyTypeId(). ',' ; } elseif( true == array_key_exists( 'GuestFrequencyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' guest_frequency_type_id = ' . $this->sqlGuestFrequencyTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' guest_name = ' . $this->sqlGuestName(). ',' ; } elseif( true == array_key_exists( 'GuestName', $this->getChangedColumns() ) ) { $strSql .= ' guest_name = ' . $this->sqlGuestName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' week_days = ' . $this->sqlWeekDays(). ',' ; } elseif( true == array_key_exists( 'WeekDays', $this->getChangedColumns() ) ) { $strSql .= ' week_days = ' . $this->sqlWeekDays() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_time = ' . $this->sqlStartTime(). ',' ; } elseif( true == array_key_exists( 'StartTime', $this->getChangedColumns() ) ) { $strSql .= ' start_time = ' . $this->sqlStartTime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_time = ' . $this->sqlEndTime(). ',' ; } elseif( true == array_key_exists( 'EndTime', $this->getChangedColumns() ) ) { $strSql .= ' end_time = ' . $this->sqlEndTime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency = ' . $this->sqlFrequency(). ',' ; } elseif( true == array_key_exists( 'Frequency', $this->getChangedColumns() ) ) { $strSql .= ' frequency = ' . $this->sqlFrequency() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month_days = ' . $this->sqlMonthDays(). ',' ; } elseif( true == array_key_exists( 'MonthDays', $this->getChangedColumns() ) ) { $strSql .= ' month_days = ' . $this->sqlMonthDays() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate(). ',' ; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate(). ',' ; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' custom_guest_type_name = ' . $this->sqlCustomGuestTypeName(). ',' ; } elseif( true == array_key_exists( 'CustomGuestTypeName', $this->getChangedColumns() ) ) { $strSql .= ' custom_guest_type_name = ' . $this->sqlCustomGuestTypeName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_key_permitted = ' . $this->sqlIsKeyPermitted(). ',' ; } elseif( true == array_key_exists( 'IsKeyPermitted', $this->getChangedColumns() ) ) { $strSql .= ' is_key_permitted = ' . $this->sqlIsKeyPermitted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_no_expiration = ' . $this->sqlIsNoExpiration(). ',' ; } elseif( true == array_key_exists( 'IsNoExpiration', $this->getChangedColumns() ) ) { $strSql .= ' is_no_expiration = ' . $this->sqlIsNoExpiration() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_parcel_pickup_permitted = ' . $this->sqlIsParcelPickupPermitted(). ',' ; } elseif( true == array_key_exists( 'IsParcelPickupPermitted', $this->getChangedColumns() ) ) { $strSql .= ' is_parcel_pickup_permitted = ' . $this->sqlIsParcelPickupPermitted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' preferred_name = ' . $this->sqlPreferredName(). ',' ; } elseif( true == array_key_exists( 'PreferredName', $this->getChangedColumns() ) ) { $strSql .= ' preferred_name = ' . $this->sqlPreferredName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_property_interest = ' . $this->sqlIsPropertyInterest(). ',' ; } elseif( true == array_key_exists( 'IsPropertyInterest', $this->getChangedColumns() ) ) { $strSql .= ' is_property_interest = ' . $this->sqlIsPropertyInterest() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gender = ' . $this->sqlGender(). ',' ; } elseif( true == array_key_exists( 'Gender', $this->getChangedColumns() ) ) { $strSql .= ' gender = ' . $this->sqlGender() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId(). ',' ; } elseif( true == array_key_exists( 'CompanyMediaFileId', $this->getChangedColumns() ) ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_id' => $this->getCustomerId(),
			'lease_id' => $this->getLeaseId(),
			'guest_type_id' => $this->getGuestTypeId(),
			'guest_frequency_type_id' => $this->getGuestFrequencyTypeId(),
			'guest_name' => $this->getGuestName(),
			'phone_number' => $this->getPhoneNumber(),
			'week_days' => $this->getWeekDays(),
			'start_time' => $this->getStartTime(),
			'end_time' => $this->getEndTime(),
			'frequency' => $this->getFrequency(),
			'month_days' => $this->getMonthDays(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'custom_guest_type_name' => $this->getCustomGuestTypeName(),
			'is_key_permitted' => $this->getIsKeyPermitted(),
			'is_no_expiration' => $this->getIsNoExpiration(),
			'is_parcel_pickup_permitted' => $this->getIsParcelPickupPermitted(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'preferred_name' => $this->getPreferredName(),
			'is_property_interest' => $this->getIsPropertyInterest(),
			'gender' => $this->getGender(),
			'details' => $this->getDetails(),
			'email_address' => $this->getEmailAddress(),
			'company_media_file_id' => $this->getCompanyMediaFileId()
		);
	}

}
?>