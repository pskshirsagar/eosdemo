<?php

class CBaseDefaultMaintenanceBoard extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_maintenance_boards';

	protected $m_intId;
	protected $m_strSystemCode;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intGroupByProblems;
	protected $m_boolShowInspectionColumn;
	protected $m_boolShowFloorplanOnUnitHover;
	protected $m_boolDontShowEndDatesAndDaysRemaining;
	protected $m_boolIsDisabled;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolShowInspectionColumn = false;
		$this->m_boolShowFloorplanOnUnitHover = false;
		$this->m_boolDontShowEndDatesAndDaysRemaining = false;
		$this->m_boolIsDisabled = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['system_code'] ) && $boolDirectSet ) $this->set( 'm_strSystemCode', trim( stripcslashes( $arrValues['system_code'] ) ) ); elseif( isset( $arrValues['system_code'] ) ) $this->setSystemCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['system_code'] ) : $arrValues['system_code'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['group_by_problems'] ) && $boolDirectSet ) $this->set( 'm_intGroupByProblems', trim( $arrValues['group_by_problems'] ) ); elseif( isset( $arrValues['group_by_problems'] ) ) $this->setGroupByProblems( $arrValues['group_by_problems'] );
		if( isset( $arrValues['show_inspection_column'] ) && $boolDirectSet ) $this->set( 'm_boolShowInspectionColumn', trim( stripcslashes( $arrValues['show_inspection_column'] ) ) ); elseif( isset( $arrValues['show_inspection_column'] ) ) $this->setShowInspectionColumn( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_inspection_column'] ) : $arrValues['show_inspection_column'] );
		if( isset( $arrValues['show_floorplan_on_unit_hover'] ) && $boolDirectSet ) $this->set( 'm_boolShowFloorplanOnUnitHover', trim( stripcslashes( $arrValues['show_floorplan_on_unit_hover'] ) ) ); elseif( isset( $arrValues['show_floorplan_on_unit_hover'] ) ) $this->setShowFloorplanOnUnitHover( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_floorplan_on_unit_hover'] ) : $arrValues['show_floorplan_on_unit_hover'] );
		if( isset( $arrValues['dont_show_end_dates_and_days_remaining'] ) && $boolDirectSet ) $this->set( 'm_boolDontShowEndDatesAndDaysRemaining', trim( stripcslashes( $arrValues['dont_show_end_dates_and_days_remaining'] ) ) ); elseif( isset( $arrValues['dont_show_end_dates_and_days_remaining'] ) ) $this->setDontShowEndDatesAndDaysRemaining( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dont_show_end_dates_and_days_remaining'] ) : $arrValues['dont_show_end_dates_and_days_remaining'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSystemCode( $strSystemCode ) {
		$this->set( 'm_strSystemCode', CStrings::strTrimDef( $strSystemCode, 10, NULL, true ) );
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	public function sqlSystemCode() {
		return ( true == isset( $this->m_strSystemCode ) ) ? '\'' . addslashes( $this->m_strSystemCode ) . '\'' : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setGroupByProblems( $intGroupByProblems ) {
		$this->set( 'm_intGroupByProblems', CStrings::strToIntDef( $intGroupByProblems, NULL, false ) );
	}

	public function getGroupByProblems() {
		return $this->m_intGroupByProblems;
	}

	public function sqlGroupByProblems() {
		return ( true == isset( $this->m_intGroupByProblems ) ) ? ( string ) $this->m_intGroupByProblems : 'NULL';
	}

	public function setShowInspectionColumn( $boolShowInspectionColumn ) {
		$this->set( 'm_boolShowInspectionColumn', CStrings::strToBool( $boolShowInspectionColumn ) );
	}

	public function getShowInspectionColumn() {
		return $this->m_boolShowInspectionColumn;
	}

	public function sqlShowInspectionColumn() {
		return ( true == isset( $this->m_boolShowInspectionColumn ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowInspectionColumn ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowFloorplanOnUnitHover( $boolShowFloorplanOnUnitHover ) {
		$this->set( 'm_boolShowFloorplanOnUnitHover', CStrings::strToBool( $boolShowFloorplanOnUnitHover ) );
	}

	public function getShowFloorplanOnUnitHover() {
		return $this->m_boolShowFloorplanOnUnitHover;
	}

	public function sqlShowFloorplanOnUnitHover() {
		return ( true == isset( $this->m_boolShowFloorplanOnUnitHover ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowFloorplanOnUnitHover ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDontShowEndDatesAndDaysRemaining( $boolDontShowEndDatesAndDaysRemaining ) {
		$this->set( 'm_boolDontShowEndDatesAndDaysRemaining', CStrings::strToBool( $boolDontShowEndDatesAndDaysRemaining ) );
	}

	public function getDontShowEndDatesAndDaysRemaining() {
		return $this->m_boolDontShowEndDatesAndDaysRemaining;
	}

	public function sqlDontShowEndDatesAndDaysRemaining() {
		return ( true == isset( $this->m_boolDontShowEndDatesAndDaysRemaining ) ) ? '\'' . ( true == ( bool ) $this->m_boolDontShowEndDatesAndDaysRemaining ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'system_code' => $this->getSystemCode(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'group_by_problems' => $this->getGroupByProblems(),
			'show_inspection_column' => $this->getShowInspectionColumn(),
			'show_floorplan_on_unit_hover' => $this->getShowFloorplanOnUnitHover(),
			'dont_show_end_dates_and_days_remaining' => $this->getDontShowEndDatesAndDaysRemaining(),
			'is_disabled' => $this->getIsDisabled(),
			'details' => $this->getDetails()
		);
	}

}
?>