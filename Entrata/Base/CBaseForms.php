<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CForms
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseForms extends CEosPluralBase {

	/**
	 * @return CForm[]
	 */
	public static function fetchForms( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CForm', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CForm
	 */
	public static function fetchForm( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CForm', $objDatabase );
	}

	public static function fetchFormCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'forms', $objDatabase );
	}

	public static function fetchFormByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchForm( sprintf( 'SELECT * FROM forms WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFormsByCid( $intCid, $objDatabase ) {
		return self::fetchForms( sprintf( 'SELECT * FROM forms WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFormsByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		return self::fetchForms( sprintf( 'SELECT * FROM forms WHERE document_id = %d AND cid = %d', ( int ) $intDocumentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFormsByFormStatusTypeIdByCid( $intFormStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchForms( sprintf( 'SELECT * FROM forms WHERE form_status_type_id = %d AND cid = %d', ( int ) $intFormStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFormsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchForms( sprintf( 'SELECT * FROM forms WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFormsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchForms( sprintf( 'SELECT * FROM forms WHERE ap_payee_id = %d AND cid = %d', ( int ) $intApPayeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFormsByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchForms( sprintf( 'SELECT * FROM forms WHERE company_employee_id = %d AND cid = %d', ( int ) $intCompanyEmployeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFormsByCompanyOwnerIdByCid( $intCompanyOwnerId, $intCid, $objDatabase ) {
		return self::fetchForms( sprintf( 'SELECT * FROM forms WHERE company_owner_id = %d AND cid = %d', ( int ) $intCompanyOwnerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFormsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchForms( sprintf( 'SELECT * FROM forms WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFormsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchForms( sprintf( 'SELECT * FROM forms WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFormsByMaintenanceRequestIdByCid( $intMaintenanceRequestId, $intCid, $objDatabase ) {
		return self::fetchForms( sprintf( 'SELECT * FROM forms WHERE maintenance_request_id = %d AND cid = %d', ( int ) $intMaintenanceRequestId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFormsByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchForms( sprintf( 'SELECT * FROM forms WHERE applicant_id = %d AND cid = %d', ( int ) $intApplicantId, ( int ) $intCid ), $objDatabase );
	}

}
?>