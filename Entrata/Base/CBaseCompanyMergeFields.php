<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyMergeFields
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyMergeFields extends CEosPluralBase {

	/**
	 * @return CCompanyMergeField[]
	 */
	public static function fetchCompanyMergeFields( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCompanyMergeField::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyMergeField
	 */
	public static function fetchCompanyMergeField( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyMergeField::class, $objDatabase );
	}

	public static function fetchCompanyMergeFieldCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_merge_fields', $objDatabase );
	}

	public static function fetchCompanyMergeFieldByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyMergeField( sprintf( 'SELECT * FROM company_merge_fields WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyMergeFieldsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyMergeFields( sprintf( 'SELECT * FROM company_merge_fields WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCompanyMergeFieldsByMergeFieldTypeIdByCid( $intMergeFieldTypeId, $intCid, $objDatabase ) {
		return self::fetchCompanyMergeFields( sprintf( 'SELECT * FROM company_merge_fields WHERE merge_field_type_id = %d AND cid = %d', $intMergeFieldTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyMergeFieldsByOriginalCompanyMergeFieldIdByCid( $intOriginalCompanyMergeFieldId, $intCid, $objDatabase ) {
		return self::fetchCompanyMergeFields( sprintf( 'SELECT * FROM company_merge_fields WHERE original_company_merge_field_id = %d AND cid = %d', $intOriginalCompanyMergeFieldId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyMergeFieldsByMergeFieldGroupIdByCid( $intMergeFieldGroupId, $intCid, $objDatabase ) {
		return self::fetchCompanyMergeFields( sprintf( 'SELECT * FROM company_merge_fields WHERE merge_field_group_id = %d AND cid = %d', $intMergeFieldGroupId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyMergeFieldsByDefaultMergeFieldIdByCid( $intDefaultMergeFieldId, $intCid, $objDatabase ) {
		return self::fetchCompanyMergeFields( sprintf( 'SELECT * FROM company_merge_fields WHERE default_merge_field_id = %d AND cid = %d', $intDefaultMergeFieldId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyMergeFieldsByBlockCompanyMergeFieldIdByCid( $intBlockCompanyMergeFieldId, $intCid, $objDatabase ) {
		return self::fetchCompanyMergeFields( sprintf( 'SELECT * FROM company_merge_fields WHERE block_company_merge_field_id = %d AND cid = %d', $intBlockCompanyMergeFieldId, $intCid ), $objDatabase );
	}

}
?>