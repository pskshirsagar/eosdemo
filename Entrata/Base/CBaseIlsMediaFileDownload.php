<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseIlsMediaFileDownload extends CEosSingularBase {

	const TABLE_NAME = 'public.ils_media_file_downloads';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyFloorplanId;
	protected $m_strImageRemotePrimaryKey;
	protected $m_strImageUrlToDownload;
	protected $m_strCaption;
	protected $m_intIsDownloaded;
	protected $m_intSortOrder;
	protected $m_strImageUrlAddedOn;
	protected $m_strImageDownloadedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsDownloaded = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorplanId', trim( $arrValues['property_floorplan_id'] ) ); elseif( isset( $arrValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrValues['property_floorplan_id'] );
		if( isset( $arrValues['image_remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strImageRemotePrimaryKey', trim( stripcslashes( $arrValues['image_remote_primary_key'] ) ) ); elseif( isset( $arrValues['image_remote_primary_key'] ) ) $this->setImageRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_remote_primary_key'] ) : $arrValues['image_remote_primary_key'] );
		if( isset( $arrValues['image_url_to_download'] ) && $boolDirectSet ) $this->set( 'm_strImageUrlToDownload', trim( stripcslashes( $arrValues['image_url_to_download'] ) ) ); elseif( isset( $arrValues['image_url_to_download'] ) ) $this->setImageUrlToDownload( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_url_to_download'] ) : $arrValues['image_url_to_download'] );
		if( isset( $arrValues['caption'] ) && $boolDirectSet ) $this->set( 'm_strCaption', trim( stripcslashes( $arrValues['caption'] ) ) ); elseif( isset( $arrValues['caption'] ) ) $this->setCaption( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['caption'] ) : $arrValues['caption'] );
		if( isset( $arrValues['is_downloaded'] ) && $boolDirectSet ) $this->set( 'm_intIsDownloaded', trim( $arrValues['is_downloaded'] ) ); elseif( isset( $arrValues['is_downloaded'] ) ) $this->setIsDownloaded( $arrValues['is_downloaded'] );
		if( isset( $arrValues['sort_order'] ) && $boolDirectSet ) $this->set( 'm_intSortOrder', trim( $arrValues['sort_order'] ) ); elseif( isset( $arrValues['sort_order'] ) ) $this->setSortOrder( $arrValues['sort_order'] );
		if( isset( $arrValues['image_url_added_on'] ) && $boolDirectSet ) $this->set( 'm_strImageUrlAddedOn', trim( $arrValues['image_url_added_on'] ) ); elseif( isset( $arrValues['image_url_added_on'] ) ) $this->setImageUrlAddedOn( $arrValues['image_url_added_on'] );
		if( isset( $arrValues['image_downloaded_on'] ) && $boolDirectSet ) $this->set( 'm_strImageDownloadedOn', trim( $arrValues['image_downloaded_on'] ) ); elseif( isset( $arrValues['image_downloaded_on'] ) ) $this->setImageDownloadedOn( $arrValues['image_downloaded_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->set( 'm_intPropertyFloorplanId', CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false ) );
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function sqlPropertyFloorplanId() {
		return ( true == isset( $this->m_intPropertyFloorplanId ) ) ? ( string ) $this->m_intPropertyFloorplanId : 'NULL';
	}

	public function setImageRemotePrimaryKey( $strImageRemotePrimaryKey ) {
		$this->set( 'm_strImageRemotePrimaryKey', CStrings::strTrimDef( $strImageRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getImageRemotePrimaryKey() {
		return $this->m_strImageRemotePrimaryKey;
	}

	public function sqlImageRemotePrimaryKey() {
		return ( true == isset( $this->m_strImageRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strImageRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setImageUrlToDownload( $strImageUrlToDownload ) {
		$this->set( 'm_strImageUrlToDownload', CStrings::strTrimDef( $strImageUrlToDownload, -1, NULL, true ) );
	}

	public function getImageUrlToDownload() {
		return $this->m_strImageUrlToDownload;
	}

	public function sqlImageUrlToDownload() {
		return ( true == isset( $this->m_strImageUrlToDownload ) ) ? '\'' . addslashes( $this->m_strImageUrlToDownload ) . '\'' : 'NULL';
	}

	public function setCaption( $strCaption ) {
		$this->set( 'm_strCaption', CStrings::strTrimDef( $strCaption, 200, NULL, true ) );
	}

	public function getCaption() {
		return $this->m_strCaption;
	}

	public function sqlCaption() {
		return ( true == isset( $this->m_strCaption ) ) ? '\'' . addslashes( $this->m_strCaption ) . '\'' : 'NULL';
	}

	public function setIsDownloaded( $intIsDownloaded ) {
		$this->set( 'm_intIsDownloaded', CStrings::strToIntDef( $intIsDownloaded, NULL, false ) );
	}

	public function getIsDownloaded() {
		return $this->m_intIsDownloaded;
	}

	public function sqlIsDownloaded() {
		return ( true == isset( $this->m_intIsDownloaded ) ) ? ( string ) $this->m_intIsDownloaded : '0';
	}

	public function setSortOrder( $intSortOrder ) {
		$this->set( 'm_intSortOrder', CStrings::strToIntDef( $intSortOrder, NULL, false ) );
	}

	public function getSortOrder() {
		return $this->m_intSortOrder;
	}

	public function sqlSortOrder() {
		return ( true == isset( $this->m_intSortOrder ) ) ? ( string ) $this->m_intSortOrder : 'NULL';
	}

	public function setImageUrlAddedOn( $strImageUrlAddedOn ) {
		$this->set( 'm_strImageUrlAddedOn', CStrings::strTrimDef( $strImageUrlAddedOn, -1, NULL, true ) );
	}

	public function getImageUrlAddedOn() {
		return $this->m_strImageUrlAddedOn;
	}

	public function sqlImageUrlAddedOn() {
		return ( true == isset( $this->m_strImageUrlAddedOn ) ) ? '\'' . $this->m_strImageUrlAddedOn . '\'' : 'NULL';
	}

	public function setImageDownloadedOn( $strImageDownloadedOn ) {
		$this->set( 'm_strImageDownloadedOn', CStrings::strTrimDef( $strImageDownloadedOn, -1, NULL, true ) );
	}

	public function getImageDownloadedOn() {
		return $this->m_strImageDownloadedOn;
	}

	public function sqlImageDownloadedOn() {
		return ( true == isset( $this->m_strImageDownloadedOn ) ) ? '\'' . $this->m_strImageDownloadedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_floorplan_id, image_remote_primary_key, image_url_to_download, caption, is_downloaded, sort_order, image_url_added_on, image_downloaded_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyFloorplanId() . ', ' .
 						$this->sqlImageRemotePrimaryKey() . ', ' .
 						$this->sqlImageUrlToDownload() . ', ' .
 						$this->sqlCaption() . ', ' .
 						$this->sqlIsDownloaded() . ', ' .
 						$this->sqlSortOrder() . ', ' .
 						$this->sqlImageUrlAddedOn() . ', ' .
 						$this->sqlImageDownloadedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; } elseif( true == array_key_exists( 'PropertyFloorplanId', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_remote_primary_key = ' . $this->sqlImageRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'ImageRemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' image_remote_primary_key = ' . $this->sqlImageRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_url_to_download = ' . $this->sqlImageUrlToDownload() . ','; } elseif( true == array_key_exists( 'ImageUrlToDownload', $this->getChangedColumns() ) ) { $strSql .= ' image_url_to_download = ' . $this->sqlImageUrlToDownload() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' caption = ' . $this->sqlCaption() . ','; } elseif( true == array_key_exists( 'Caption', $this->getChangedColumns() ) ) { $strSql .= ' caption = ' . $this->sqlCaption() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_downloaded = ' . $this->sqlIsDownloaded() . ','; } elseif( true == array_key_exists( 'IsDownloaded', $this->getChangedColumns() ) ) { $strSql .= ' is_downloaded = ' . $this->sqlIsDownloaded() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sort_order = ' . $this->sqlSortOrder() . ','; } elseif( true == array_key_exists( 'SortOrder', $this->getChangedColumns() ) ) { $strSql .= ' sort_order = ' . $this->sqlSortOrder() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_url_added_on = ' . $this->sqlImageUrlAddedOn() . ','; } elseif( true == array_key_exists( 'ImageUrlAddedOn', $this->getChangedColumns() ) ) { $strSql .= ' image_url_added_on = ' . $this->sqlImageUrlAddedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_downloaded_on = ' . $this->sqlImageDownloadedOn() . ','; } elseif( true == array_key_exists( 'ImageDownloadedOn', $this->getChangedColumns() ) ) { $strSql .= ' image_downloaded_on = ' . $this->sqlImageDownloadedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_floorplan_id' => $this->getPropertyFloorplanId(),
			'image_remote_primary_key' => $this->getImageRemotePrimaryKey(),
			'image_url_to_download' => $this->getImageUrlToDownload(),
			'caption' => $this->getCaption(),
			'is_downloaded' => $this->getIsDownloaded(),
			'sort_order' => $this->getSortOrder(),
			'image_url_added_on' => $this->getImageUrlAddedOn(),
			'image_downloaded_on' => $this->getImageDownloadedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>