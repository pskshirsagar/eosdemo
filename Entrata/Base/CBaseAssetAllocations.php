<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAssetAllocations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAssetAllocations extends CEosPluralBase {

	/**
	 * @return CAssetAllocation[]
	 */
	public static function fetchAssetAllocations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CAssetAllocation', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CAssetAllocation
	 */
	public static function fetchAssetAllocation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAssetAllocation', $objDatabase );
	}

	public static function fetchAssetAllocationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'asset_allocations', $objDatabase );
	}

	public static function fetchAssetAllocationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchAssetAllocation( sprintf( 'SELECT * FROM asset_allocations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetAllocationsByCid( $intCid, $objDatabase ) {
		return self::fetchAssetAllocations( sprintf( 'SELECT * FROM asset_allocations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetAllocationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchAssetAllocations( sprintf( 'SELECT * FROM asset_allocations WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetAllocationsByPeriodIdByCid( $intPeriodId, $intCid, $objDatabase ) {
		return self::fetchAssetAllocations( sprintf( 'SELECT * FROM asset_allocations WHERE period_id = %d AND cid = %d', ( int ) $intPeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetAllocationsByGlTransactionTypeIdByCid( $intGlTransactionTypeId, $intCid, $objDatabase ) {
		return self::fetchAssetAllocations( sprintf( 'SELECT * FROM asset_allocations WHERE gl_transaction_type_id = %d AND cid = %d', ( int ) $intGlTransactionTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetAllocationsByChargeAssetTransactionIdByCid( $intChargeAssetTransactionId, $intCid, $objDatabase ) {
		return self::fetchAssetAllocations( sprintf( 'SELECT * FROM asset_allocations WHERE charge_asset_transaction_id = %d AND cid = %d', ( int ) $intChargeAssetTransactionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetAllocationsByCreditAssetTransactionIdByCid( $intCreditAssetTransactionId, $intCid, $objDatabase ) {
		return self::fetchAssetAllocations( sprintf( 'SELECT * FROM asset_allocations WHERE credit_asset_transaction_id = %d AND cid = %d', ( int ) $intCreditAssetTransactionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetAllocationsByDebitGlAccountIdByCid( $intDebitGlAccountId, $intCid, $objDatabase ) {
		return self::fetchAssetAllocations( sprintf( 'SELECT * FROM asset_allocations WHERE debit_gl_account_id = %d AND cid = %d', ( int ) $intDebitGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetAllocationsByCreditGlAccountIdByCid( $intCreditGlAccountId, $intCid, $objDatabase ) {
		return self::fetchAssetAllocations( sprintf( 'SELECT * FROM asset_allocations WHERE credit_gl_account_id = %d AND cid = %d', ( int ) $intCreditGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetAllocationsByAssetAllocationIdByCid( $intAssetAllocationId, $intCid, $objDatabase ) {
		return self::fetchAssetAllocations( sprintf( 'SELECT * FROM asset_allocations WHERE asset_allocation_id = %d AND cid = %d', ( int ) $intAssetAllocationId, ( int ) $intCid ), $objDatabase );
	}

}
?>