<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CVerificationTypes
 * Do not add any new functions to this class.
 */

class CBaseVerificationTypes extends CEosPluralBase {

	/**
	 * @return CVerificationType[]
	 */
	public static function fetchVerificationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CVerificationType::class, $objDatabase );
	}

	/**
	 * @return CVerificationType
	 */
	public static function fetchVerificationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CVerificationType::class, $objDatabase );
	}

	public static function fetchVerificationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'verification_types', $objDatabase );
	}

	public static function fetchVerificationTypeById( $intId, $objDatabase ) {
		return self::fetchVerificationType( sprintf( 'SELECT * FROM verification_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>