<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileMdrImports
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFileMdrImports extends CEosPluralBase {

	/**
	 * @return CFileMdrImport[]
	 */
	public static function fetchFileMdrImports( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CFileMdrImport', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFileMdrImport
	 */
	public static function fetchFileMdrImport( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CFileMdrImport', $objDatabase );
	}

	public static function fetchFileMdrImportCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'file_mdr_imports', $objDatabase );
	}

	public static function fetchFileMdrImportByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFileMdrImport( sprintf( 'SELECT * FROM file_mdr_imports WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileMdrImportsByCid( $intCid, $objDatabase ) {
		return self::fetchFileMdrImports( sprintf( 'SELECT * FROM file_mdr_imports WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileMdrImportsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchFileMdrImports( sprintf( 'SELECT * FROM file_mdr_imports WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileMdrImportsByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		return self::fetchFileMdrImports( sprintf( 'SELECT * FROM file_mdr_imports WHERE file_id = %d AND cid = %d', ( int ) $intFileId, ( int ) $intCid ), $objDatabase );
	}

}
?>