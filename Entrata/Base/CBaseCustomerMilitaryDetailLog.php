<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerMilitaryDetailLog extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.customer_military_detail_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCustomerMilitaryDetailId;
	protected $m_intCustomerId;
	protected $m_intMilitaryStatusId;
	protected $m_intMilitaryComponentId;
	protected $m_intMilitaryPayGradeId;
	protected $m_intMilitaryRankId;
	protected $m_intMilitaryAssistanceTypeId;
	protected $m_intMilitaryNonMilitaryTypeId;
	protected $m_intFutureMilitaryInstallationId;
	protected $m_intPriorCustomerMilitaryDetailLogId;
	protected $m_strPostDate;
	protected $m_strApplyThroughPostDate;
	protected $m_strUnitIdCode;
	protected $m_strCommandingOfficer;
	protected $m_strLastAssignment;
	protected $m_strDutyAddressStreet1;
	protected $m_strDutyAddressStreet2;
	protected $m_strDutyAddressCity;
	protected $m_strDutyAddressStateCode;
	protected $m_strDutyAddressPostalCode;
	protected $m_strDutyPhoneNumber;
	protected $m_strDutyEmailAddress;
	protected $m_strDeployedLocation;
	protected $m_strFutureInstallationOther;
	protected $m_strFutureReportDate;
	protected $m_strEnlistmentDate;
	protected $m_strRankEffectiveDate;
	protected $m_strReportDate;
	protected $m_strChangeInDutyDate;
	protected $m_strEndOfServiceDate;
	protected $m_strDeploymentStartDate;
	protected $m_strDeploymentEndDate;
	protected $m_strLogDatetime;
	protected $m_boolIsPromotable;
	protected $m_boolIsWarrantCandidate;
	protected $m_boolIsActiveReservist;
	protected $m_boolIsDeployed;
	protected $m_boolIsPostDateIgnored;
	protected $m_boolIsOpeningLog;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_strEligibilityDate;
	protected $m_fltRateProtectedAmount;
	protected $m_intMilitaryMaritalStatusTypeId;
	protected $m_intMilitaryDependentStatusTypeId;
	protected $m_intMilitaryDependentRelationshipId;
	protected $m_strExternalResidentId;
	protected $m_strArrivalDate;
	protected $m_strMilitaryPayGradeEffectiveDate;
	protected $m_boolIsFullBah;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_strApplyThroughPostDate = '12/31/2099';
		$this->m_strLogDatetime = 'now()';
		$this->m_boolIsPromotable = false;
		$this->m_boolIsWarrantCandidate = false;
		$this->m_boolIsDeployed = false;
		$this->m_boolIsPostDateIgnored = false;
		$this->m_boolIsOpeningLog = false;
		$this->m_fltRateProtectedAmount = '0';
		$this->m_intMilitaryMaritalStatusTypeId = '1';
		$this->m_intMilitaryDependentStatusTypeId = '2';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['customer_military_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerMilitaryDetailId', trim( $arrValues['customer_military_detail_id'] ) ); elseif( isset( $arrValues['customer_military_detail_id'] ) ) $this->setCustomerMilitaryDetailId( $arrValues['customer_military_detail_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['military_status_id'] ) && $boolDirectSet ) $this->set( 'm_intMilitaryStatusId', trim( $arrValues['military_status_id'] ) ); elseif( isset( $arrValues['military_status_id'] ) ) $this->setMilitaryStatusId( $arrValues['military_status_id'] );
		if( isset( $arrValues['military_component_id'] ) && $boolDirectSet ) $this->set( 'm_intMilitaryComponentId', trim( $arrValues['military_component_id'] ) ); elseif( isset( $arrValues['military_component_id'] ) ) $this->setMilitaryComponentId( $arrValues['military_component_id'] );
		if( isset( $arrValues['military_pay_grade_id'] ) && $boolDirectSet ) $this->set( 'm_intMilitaryPayGradeId', trim( $arrValues['military_pay_grade_id'] ) ); elseif( isset( $arrValues['military_pay_grade_id'] ) ) $this->setMilitaryPayGradeId( $arrValues['military_pay_grade_id'] );
		if( isset( $arrValues['military_rank_id'] ) && $boolDirectSet ) $this->set( 'm_intMilitaryRankId', trim( $arrValues['military_rank_id'] ) ); elseif( isset( $arrValues['military_rank_id'] ) ) $this->setMilitaryRankId( $arrValues['military_rank_id'] );
		if( isset( $arrValues['military_assistance_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMilitaryAssistanceTypeId', trim( $arrValues['military_assistance_type_id'] ) ); elseif( isset( $arrValues['military_assistance_type_id'] ) ) $this->setMilitaryAssistanceTypeId( $arrValues['military_assistance_type_id'] );
		if( isset( $arrValues['military_non_military_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMilitaryNonMilitaryTypeId', trim( $arrValues['military_non_military_type_id'] ) ); elseif( isset( $arrValues['military_non_military_type_id'] ) ) $this->setMilitaryNonMilitaryTypeId( $arrValues['military_non_military_type_id'] );
		if( isset( $arrValues['future_military_installation_id'] ) && $boolDirectSet ) $this->set( 'm_intFutureMilitaryInstallationId', trim( $arrValues['future_military_installation_id'] ) ); elseif( isset( $arrValues['future_military_installation_id'] ) ) $this->setFutureMilitaryInstallationId( $arrValues['future_military_installation_id'] );
		if( isset( $arrValues['prior_customer_military_detail_log_id'] ) && $boolDirectSet ) $this->set( 'm_intPriorCustomerMilitaryDetailLogId', trim( $arrValues['prior_customer_military_detail_log_id'] ) ); elseif( isset( $arrValues['prior_customer_military_detail_log_id'] ) ) $this->setPriorCustomerMilitaryDetailLogId( $arrValues['prior_customer_military_detail_log_id'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['apply_through_post_date'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostDate', trim( $arrValues['apply_through_post_date'] ) ); elseif( isset( $arrValues['apply_through_post_date'] ) ) $this->setApplyThroughPostDate( $arrValues['apply_through_post_date'] );
		if( isset( $arrValues['unit_id_code'] ) && $boolDirectSet ) $this->set( 'm_strUnitIdCode', trim( $arrValues['unit_id_code'] ) ); elseif( isset( $arrValues['unit_id_code'] ) ) $this->setUnitIdCode( $arrValues['unit_id_code'] );
		if( isset( $arrValues['commanding_officer'] ) && $boolDirectSet ) $this->set( 'm_strCommandingOfficer', trim( $arrValues['commanding_officer'] ) ); elseif( isset( $arrValues['commanding_officer'] ) ) $this->setCommandingOfficer( $arrValues['commanding_officer'] );
		if( isset( $arrValues['last_assignment'] ) && $boolDirectSet ) $this->set( 'm_strLastAssignment', trim( $arrValues['last_assignment'] ) ); elseif( isset( $arrValues['last_assignment'] ) ) $this->setLastAssignment( $arrValues['last_assignment'] );
		if( isset( $arrValues['duty_address_street1'] ) && $boolDirectSet ) $this->set( 'm_strDutyAddressStreet1', trim( $arrValues['duty_address_street1'] ) ); elseif( isset( $arrValues['duty_address_street1'] ) ) $this->setDutyAddressStreet1( $arrValues['duty_address_street1'] );
		if( isset( $arrValues['duty_address_street2'] ) && $boolDirectSet ) $this->set( 'm_strDutyAddressStreet2', trim( $arrValues['duty_address_street2'] ) ); elseif( isset( $arrValues['duty_address_street2'] ) ) $this->setDutyAddressStreet2( $arrValues['duty_address_street2'] );
		if( isset( $arrValues['duty_address_city'] ) && $boolDirectSet ) $this->set( 'm_strDutyAddressCity', trim( $arrValues['duty_address_city'] ) ); elseif( isset( $arrValues['duty_address_city'] ) ) $this->setDutyAddressCity( $arrValues['duty_address_city'] );
		if( isset( $arrValues['duty_address_state_code'] ) && $boolDirectSet ) $this->set( 'm_strDutyAddressStateCode', trim( $arrValues['duty_address_state_code'] ) ); elseif( isset( $arrValues['duty_address_state_code'] ) ) $this->setDutyAddressStateCode( $arrValues['duty_address_state_code'] );
		if( isset( $arrValues['duty_address_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strDutyAddressPostalCode', trim( $arrValues['duty_address_postal_code'] ) ); elseif( isset( $arrValues['duty_address_postal_code'] ) ) $this->setDutyAddressPostalCode( $arrValues['duty_address_postal_code'] );
		if( isset( $arrValues['duty_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strDutyPhoneNumber', trim( $arrValues['duty_phone_number'] ) ); elseif( isset( $arrValues['duty_phone_number'] ) ) $this->setDutyPhoneNumber( $arrValues['duty_phone_number'] );
		if( isset( $arrValues['duty_email_address'] ) && $boolDirectSet ) $this->set( 'm_strDutyEmailAddress', trim( $arrValues['duty_email_address'] ) ); elseif( isset( $arrValues['duty_email_address'] ) ) $this->setDutyEmailAddress( $arrValues['duty_email_address'] );
		if( isset( $arrValues['deployed_location'] ) && $boolDirectSet ) $this->set( 'm_strDeployedLocation', trim( $arrValues['deployed_location'] ) ); elseif( isset( $arrValues['deployed_location'] ) ) $this->setDeployedLocation( $arrValues['deployed_location'] );
		if( isset( $arrValues['future_installation_other'] ) && $boolDirectSet ) $this->set( 'm_strFutureInstallationOther', trim( $arrValues['future_installation_other'] ) ); elseif( isset( $arrValues['future_installation_other'] ) ) $this->setFutureInstallationOther( $arrValues['future_installation_other'] );
		if( isset( $arrValues['future_report_date'] ) && $boolDirectSet ) $this->set( 'm_strFutureReportDate', trim( $arrValues['future_report_date'] ) ); elseif( isset( $arrValues['future_report_date'] ) ) $this->setFutureReportDate( $arrValues['future_report_date'] );
		if( isset( $arrValues['enlistment_date'] ) && $boolDirectSet ) $this->set( 'm_strEnlistmentDate', trim( $arrValues['enlistment_date'] ) ); elseif( isset( $arrValues['enlistment_date'] ) ) $this->setEnlistmentDate( $arrValues['enlistment_date'] );
		if( isset( $arrValues['rank_effective_date'] ) && $boolDirectSet ) $this->set( 'm_strRankEffectiveDate', trim( $arrValues['rank_effective_date'] ) ); elseif( isset( $arrValues['rank_effective_date'] ) ) $this->setRankEffectiveDate( $arrValues['rank_effective_date'] );
		if( isset( $arrValues['report_date'] ) && $boolDirectSet ) $this->set( 'm_strReportDate', trim( $arrValues['report_date'] ) ); elseif( isset( $arrValues['report_date'] ) ) $this->setReportDate( $arrValues['report_date'] );
		if( isset( $arrValues['change_in_duty_date'] ) && $boolDirectSet ) $this->set( 'm_strChangeInDutyDate', trim( $arrValues['change_in_duty_date'] ) ); elseif( isset( $arrValues['change_in_duty_date'] ) ) $this->setChangeInDutyDate( $arrValues['change_in_duty_date'] );
		if( isset( $arrValues['end_of_service_date'] ) && $boolDirectSet ) $this->set( 'm_strEndOfServiceDate', trim( $arrValues['end_of_service_date'] ) ); elseif( isset( $arrValues['end_of_service_date'] ) ) $this->setEndOfServiceDate( $arrValues['end_of_service_date'] );
		if( isset( $arrValues['deployment_start_date'] ) && $boolDirectSet ) $this->set( 'm_strDeploymentStartDate', trim( $arrValues['deployment_start_date'] ) ); elseif( isset( $arrValues['deployment_start_date'] ) ) $this->setDeploymentStartDate( $arrValues['deployment_start_date'] );
		if( isset( $arrValues['deployment_end_date'] ) && $boolDirectSet ) $this->set( 'm_strDeploymentEndDate', trim( $arrValues['deployment_end_date'] ) ); elseif( isset( $arrValues['deployment_end_date'] ) ) $this->setDeploymentEndDate( $arrValues['deployment_end_date'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['is_promotable'] ) && $boolDirectSet ) $this->set( 'm_boolIsPromotable', trim( stripcslashes( $arrValues['is_promotable'] ) ) ); elseif( isset( $arrValues['is_promotable'] ) ) $this->setIsPromotable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_promotable'] ) : $arrValues['is_promotable'] );
		if( isset( $arrValues['is_warrant_candidate'] ) && $boolDirectSet ) $this->set( 'm_boolIsWarrantCandidate', trim( stripcslashes( $arrValues['is_warrant_candidate'] ) ) ); elseif( isset( $arrValues['is_warrant_candidate'] ) ) $this->setIsWarrantCandidate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_warrant_candidate'] ) : $arrValues['is_warrant_candidate'] );
		if( isset( $arrValues['is_active_reservist'] ) && $boolDirectSet ) $this->set( 'm_boolIsActiveReservist', trim( stripcslashes( $arrValues['is_active_reservist'] ) ) ); elseif( isset( $arrValues['is_active_reservist'] ) ) $this->setIsActiveReservist( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active_reservist'] ) : $arrValues['is_active_reservist'] );
		if( isset( $arrValues['is_deployed'] ) && $boolDirectSet ) $this->set( 'm_boolIsDeployed', trim( stripcslashes( $arrValues['is_deployed'] ) ) ); elseif( isset( $arrValues['is_deployed'] ) ) $this->setIsDeployed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_deployed'] ) : $arrValues['is_deployed'] );
		if( isset( $arrValues['is_post_date_ignored'] ) && $boolDirectSet ) $this->set( 'm_boolIsPostDateIgnored', trim( stripcslashes( $arrValues['is_post_date_ignored'] ) ) ); elseif( isset( $arrValues['is_post_date_ignored'] ) ) $this->setIsPostDateIgnored( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_post_date_ignored'] ) : $arrValues['is_post_date_ignored'] );
		if( isset( $arrValues['is_opening_log'] ) && $boolDirectSet ) $this->set( 'm_boolIsOpeningLog', trim( stripcslashes( $arrValues['is_opening_log'] ) ) ); elseif( isset( $arrValues['is_opening_log'] ) ) $this->setIsOpeningLog( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_opening_log'] ) : $arrValues['is_opening_log'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['eligibility_date'] ) && $boolDirectSet ) $this->set( 'm_strEligibilityDate', trim( $arrValues['eligibility_date'] ) ); elseif( isset( $arrValues['eligibility_date'] ) ) $this->setEligibilityDate( $arrValues['eligibility_date'] );
		if( isset( $arrValues['rate_protected_amount'] ) && $boolDirectSet ) $this->set( 'm_fltRateProtectedAmount', trim( $arrValues['rate_protected_amount'] ) ); elseif( isset( $arrValues['rate_protected_amount'] ) ) $this->setRateProtectedAmount( $arrValues['rate_protected_amount'] );
		if( isset( $arrValues['military_marital_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMilitaryMaritalStatusTypeId', trim( $arrValues['military_marital_status_type_id'] ) ); elseif( isset( $arrValues['military_marital_status_type_id'] ) ) $this->setMilitaryMaritalStatusTypeId( $arrValues['military_marital_status_type_id'] );
		if( isset( $arrValues['military_dependent_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMilitaryDependentStatusTypeId', trim( $arrValues['military_dependent_status_type_id'] ) ); elseif( isset( $arrValues['military_dependent_status_type_id'] ) ) $this->setMilitaryDependentStatusTypeId( $arrValues['military_dependent_status_type_id'] );
		if( isset( $arrValues['military_dependent_relationship_id'] ) && $boolDirectSet ) $this->set( 'm_intMilitaryDependentRelationshipId', trim( $arrValues['military_dependent_relationship_id'] ) ); elseif( isset( $arrValues['military_dependent_relationship_id'] ) ) $this->setMilitaryDependentRelationshipId( $arrValues['military_dependent_relationship_id'] );
		if( isset( $arrValues['external_resident_id'] ) && $boolDirectSet ) $this->set( 'm_strExternalResidentId', trim( $arrValues['external_resident_id'] ) ); elseif( isset( $arrValues['external_resident_id'] ) ) $this->setExternalResidentId( $arrValues['external_resident_id'] );
		if( isset( $arrValues['arrival_date'] ) && $boolDirectSet ) $this->set( 'm_strArrivalDate', trim( $arrValues['arrival_date'] ) ); elseif( isset( $arrValues['arrival_date'] ) ) $this->setArrivalDate( $arrValues['arrival_date'] );
		if( isset( $arrValues['military_pay_grade_effective_date'] ) && $boolDirectSet ) $this->set( 'm_strMilitaryPayGradeEffectiveDate', trim( $arrValues['military_pay_grade_effective_date'] ) ); elseif( isset( $arrValues['military_pay_grade_effective_date'] ) ) $this->setMilitaryPayGradeEffectiveDate( $arrValues['military_pay_grade_effective_date'] );
		if( isset( $arrValues['is_full_bah'] ) && $boolDirectSet ) $this->set( 'm_boolIsFullBah', trim( stripcslashes( $arrValues['is_full_bah'] ) ) ); elseif( isset( $arrValues['is_full_bah'] ) ) $this->setIsFullBah( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_full_bah'] ) : $arrValues['is_full_bah'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCustomerMilitaryDetailId( $intCustomerMilitaryDetailId ) {
		$this->set( 'm_intCustomerMilitaryDetailId', CStrings::strToIntDef( $intCustomerMilitaryDetailId, NULL, false ) );
	}

	public function getCustomerMilitaryDetailId() {
		return $this->m_intCustomerMilitaryDetailId;
	}

	public function sqlCustomerMilitaryDetailId() {
		return ( true == isset( $this->m_intCustomerMilitaryDetailId ) ) ? ( string ) $this->m_intCustomerMilitaryDetailId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setMilitaryStatusId( $intMilitaryStatusId ) {
		$this->set( 'm_intMilitaryStatusId', CStrings::strToIntDef( $intMilitaryStatusId, NULL, false ) );
	}

	public function getMilitaryStatusId() {
		return $this->m_intMilitaryStatusId;
	}

	public function sqlMilitaryStatusId() {
		return ( true == isset( $this->m_intMilitaryStatusId ) ) ? ( string ) $this->m_intMilitaryStatusId : 'NULL';
	}

	public function setMilitaryComponentId( $intMilitaryComponentId ) {
		$this->set( 'm_intMilitaryComponentId', CStrings::strToIntDef( $intMilitaryComponentId, NULL, false ) );
	}

	public function getMilitaryComponentId() {
		return $this->m_intMilitaryComponentId;
	}

	public function sqlMilitaryComponentId() {
		return ( true == isset( $this->m_intMilitaryComponentId ) ) ? ( string ) $this->m_intMilitaryComponentId : 'NULL';
	}

	public function setMilitaryPayGradeId( $intMilitaryPayGradeId ) {
		$this->set( 'm_intMilitaryPayGradeId', CStrings::strToIntDef( $intMilitaryPayGradeId, NULL, false ) );
	}

	public function getMilitaryPayGradeId() {
		return $this->m_intMilitaryPayGradeId;
	}

	public function sqlMilitaryPayGradeId() {
		return ( true == isset( $this->m_intMilitaryPayGradeId ) ) ? ( string ) $this->m_intMilitaryPayGradeId : 'NULL';
	}

	public function setMilitaryRankId( $intMilitaryRankId ) {
		$this->set( 'm_intMilitaryRankId', CStrings::strToIntDef( $intMilitaryRankId, NULL, false ) );
	}

	public function getMilitaryRankId() {
		return $this->m_intMilitaryRankId;
	}

	public function sqlMilitaryRankId() {
		return ( true == isset( $this->m_intMilitaryRankId ) ) ? ( string ) $this->m_intMilitaryRankId : 'NULL';
	}

	public function setMilitaryAssistanceTypeId( $intMilitaryAssistanceTypeId ) {
		$this->set( 'm_intMilitaryAssistanceTypeId', CStrings::strToIntDef( $intMilitaryAssistanceTypeId, NULL, false ) );
	}

	public function getMilitaryAssistanceTypeId() {
		return $this->m_intMilitaryAssistanceTypeId;
	}

	public function sqlMilitaryAssistanceTypeId() {
		return ( true == isset( $this->m_intMilitaryAssistanceTypeId ) ) ? ( string ) $this->m_intMilitaryAssistanceTypeId : 'NULL';
	}

	public function setMilitaryNonMilitaryTypeId( $intMilitaryNonMilitaryTypeId ) {
		$this->set( 'm_intMilitaryNonMilitaryTypeId', CStrings::strToIntDef( $intMilitaryNonMilitaryTypeId, NULL, false ) );
	}

	public function getMilitaryNonMilitaryTypeId() {
		return $this->m_intMilitaryNonMilitaryTypeId;
	}

	public function sqlMilitaryNonMilitaryTypeId() {
		return ( true == isset( $this->m_intMilitaryNonMilitaryTypeId ) ) ? ( string ) $this->m_intMilitaryNonMilitaryTypeId : 'NULL';
	}

	public function setFutureMilitaryInstallationId( $intFutureMilitaryInstallationId ) {
		$this->set( 'm_intFutureMilitaryInstallationId', CStrings::strToIntDef( $intFutureMilitaryInstallationId, NULL, false ) );
	}

	public function getFutureMilitaryInstallationId() {
		return $this->m_intFutureMilitaryInstallationId;
	}

	public function sqlFutureMilitaryInstallationId() {
		return ( true == isset( $this->m_intFutureMilitaryInstallationId ) ) ? ( string ) $this->m_intFutureMilitaryInstallationId : 'NULL';
	}

	public function setPriorCustomerMilitaryDetailLogId( $intPriorCustomerMilitaryDetailLogId ) {
		$this->set( 'm_intPriorCustomerMilitaryDetailLogId', CStrings::strToIntDef( $intPriorCustomerMilitaryDetailLogId, NULL, false ) );
	}

	public function getPriorCustomerMilitaryDetailLogId() {
		return $this->m_intPriorCustomerMilitaryDetailLogId;
	}

	public function sqlPriorCustomerMilitaryDetailLogId() {
		return ( true == isset( $this->m_intPriorCustomerMilitaryDetailLogId ) ) ? ( string ) $this->m_intPriorCustomerMilitaryDetailLogId : 'NULL';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setApplyThroughPostDate( $strApplyThroughPostDate ) {
		$this->set( 'm_strApplyThroughPostDate', CStrings::strTrimDef( $strApplyThroughPostDate, -1, NULL, true ) );
	}

	public function getApplyThroughPostDate() {
		return $this->m_strApplyThroughPostDate;
	}

	public function sqlApplyThroughPostDate() {
		return ( true == isset( $this->m_strApplyThroughPostDate ) ) ? '\'' . $this->m_strApplyThroughPostDate . '\'' : 'NOW()';
	}

	public function setUnitIdCode( $strUnitIdCode ) {
		$this->set( 'm_strUnitIdCode', CStrings::strTrimDef( $strUnitIdCode, 6, NULL, true ) );
	}

	public function getUnitIdCode() {
		return $this->m_strUnitIdCode;
	}

	public function sqlUnitIdCode() {
		return ( true == isset( $this->m_strUnitIdCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUnitIdCode ) : '\'' . addslashes( $this->m_strUnitIdCode ) . '\'' ) : 'NULL';
	}

	public function setCommandingOfficer( $strCommandingOfficer ) {
		$this->set( 'm_strCommandingOfficer', CStrings::strTrimDef( $strCommandingOfficer, 240, NULL, true ) );
	}

	public function getCommandingOfficer() {
		return $this->m_strCommandingOfficer;
	}

	public function sqlCommandingOfficer() {
		return ( true == isset( $this->m_strCommandingOfficer ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCommandingOfficer ) : '\'' . addslashes( $this->m_strCommandingOfficer ) . '\'' ) : 'NULL';
	}

	public function setLastAssignment( $strLastAssignment ) {
		$this->set( 'm_strLastAssignment', CStrings::strTrimDef( $strLastAssignment, 240, NULL, true ) );
	}

	public function getLastAssignment() {
		return $this->m_strLastAssignment;
	}

	public function sqlLastAssignment() {
		return ( true == isset( $this->m_strLastAssignment ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLastAssignment ) : '\'' . addslashes( $this->m_strLastAssignment ) . '\'' ) : 'NULL';
	}

	public function setDutyAddressStreet1( $strDutyAddressStreet1 ) {
		$this->set( 'm_strDutyAddressStreet1', CStrings::strTrimDef( $strDutyAddressStreet1, 100, NULL, true ) );
	}

	public function getDutyAddressStreet1() {
		return $this->m_strDutyAddressStreet1;
	}

	public function sqlDutyAddressStreet1() {
		return ( true == isset( $this->m_strDutyAddressStreet1 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDutyAddressStreet1 ) : '\'' . addslashes( $this->m_strDutyAddressStreet1 ) . '\'' ) : 'NULL';
	}

	public function setDutyAddressStreet2( $strDutyAddressStreet2 ) {
		$this->set( 'm_strDutyAddressStreet2', CStrings::strTrimDef( $strDutyAddressStreet2, 100, NULL, true ) );
	}

	public function getDutyAddressStreet2() {
		return $this->m_strDutyAddressStreet2;
	}

	public function sqlDutyAddressStreet2() {
		return ( true == isset( $this->m_strDutyAddressStreet2 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDutyAddressStreet2 ) : '\'' . addslashes( $this->m_strDutyAddressStreet2 ) . '\'' ) : 'NULL';
	}

	public function setDutyAddressCity( $strDutyAddressCity ) {
		$this->set( 'm_strDutyAddressCity', CStrings::strTrimDef( $strDutyAddressCity, 50, NULL, true ) );
	}

	public function getDutyAddressCity() {
		return $this->m_strDutyAddressCity;
	}

	public function sqlDutyAddressCity() {
		return ( true == isset( $this->m_strDutyAddressCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDutyAddressCity ) : '\'' . addslashes( $this->m_strDutyAddressCity ) . '\'' ) : 'NULL';
	}

	public function setDutyAddressStateCode( $strDutyAddressStateCode ) {
		$this->set( 'm_strDutyAddressStateCode', CStrings::strTrimDef( $strDutyAddressStateCode, 2, NULL, true ) );
	}

	public function getDutyAddressStateCode() {
		return $this->m_strDutyAddressStateCode;
	}

	public function sqlDutyAddressStateCode() {
		return ( true == isset( $this->m_strDutyAddressStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDutyAddressStateCode ) : '\'' . addslashes( $this->m_strDutyAddressStateCode ) . '\'' ) : 'NULL';
	}

	public function setDutyAddressPostalCode( $strDutyAddressPostalCode ) {
		$this->set( 'm_strDutyAddressPostalCode', CStrings::strTrimDef( $strDutyAddressPostalCode, 20, NULL, true ) );
	}

	public function getDutyAddressPostalCode() {
		return $this->m_strDutyAddressPostalCode;
	}

	public function sqlDutyAddressPostalCode() {
		return ( true == isset( $this->m_strDutyAddressPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDutyAddressPostalCode ) : '\'' . addslashes( $this->m_strDutyAddressPostalCode ) . '\'' ) : 'NULL';
	}

	public function setDutyPhoneNumber( $strDutyPhoneNumber ) {
		$this->set( 'm_strDutyPhoneNumber', CStrings::strTrimDef( $strDutyPhoneNumber, 30, NULL, true ) );
	}

	public function getDutyPhoneNumber() {
		return $this->m_strDutyPhoneNumber;
	}

	public function sqlDutyPhoneNumber() {
		return ( true == isset( $this->m_strDutyPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDutyPhoneNumber ) : '\'' . addslashes( $this->m_strDutyPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setDutyEmailAddress( $strDutyEmailAddress ) {
		$this->set( 'm_strDutyEmailAddress', CStrings::strTrimDef( $strDutyEmailAddress, 240, NULL, true ) );
	}

	public function getDutyEmailAddress() {
		return $this->m_strDutyEmailAddress;
	}

	public function sqlDutyEmailAddress() {
		return ( true == isset( $this->m_strDutyEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDutyEmailAddress ) : '\'' . addslashes( $this->m_strDutyEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setDeployedLocation( $strDeployedLocation ) {
		$this->set( 'm_strDeployedLocation', CStrings::strTrimDef( $strDeployedLocation, 240, NULL, true ) );
	}

	public function getDeployedLocation() {
		return $this->m_strDeployedLocation;
	}

	public function sqlDeployedLocation() {
		return ( true == isset( $this->m_strDeployedLocation ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDeployedLocation ) : '\'' . addslashes( $this->m_strDeployedLocation ) . '\'' ) : 'NULL';
	}

	public function setFutureInstallationOther( $strFutureInstallationOther ) {
		$this->set( 'm_strFutureInstallationOther', CStrings::strTrimDef( $strFutureInstallationOther, 240, NULL, true ) );
	}

	public function getFutureInstallationOther() {
		return $this->m_strFutureInstallationOther;
	}

	public function sqlFutureInstallationOther() {
		return ( true == isset( $this->m_strFutureInstallationOther ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFutureInstallationOther ) : '\'' . addslashes( $this->m_strFutureInstallationOther ) . '\'' ) : 'NULL';
	}

	public function setFutureReportDate( $strFutureReportDate ) {
		$this->set( 'm_strFutureReportDate', CStrings::strTrimDef( $strFutureReportDate, -1, NULL, true ) );
	}

	public function getFutureReportDate() {
		return $this->m_strFutureReportDate;
	}

	public function sqlFutureReportDate() {
		return ( true == isset( $this->m_strFutureReportDate ) ) ? '\'' . $this->m_strFutureReportDate . '\'' : 'NULL';
	}

	public function setEnlistmentDate( $strEnlistmentDate ) {
		$this->set( 'm_strEnlistmentDate', CStrings::strTrimDef( $strEnlistmentDate, -1, NULL, true ) );
	}

	public function getEnlistmentDate() {
		return $this->m_strEnlistmentDate;
	}

	public function sqlEnlistmentDate() {
		return ( true == isset( $this->m_strEnlistmentDate ) ) ? '\'' . $this->m_strEnlistmentDate . '\'' : 'NULL';
	}

	public function setRankEffectiveDate( $strRankEffectiveDate ) {
		$this->set( 'm_strRankEffectiveDate', CStrings::strTrimDef( $strRankEffectiveDate, -1, NULL, true ) );
	}

	public function getRankEffectiveDate() {
		return $this->m_strRankEffectiveDate;
	}

	public function sqlRankEffectiveDate() {
		return ( true == isset( $this->m_strRankEffectiveDate ) ) ? '\'' . $this->m_strRankEffectiveDate . '\'' : 'NULL';
	}

	public function setReportDate( $strReportDate ) {
		$this->set( 'm_strReportDate', CStrings::strTrimDef( $strReportDate, -1, NULL, true ) );
	}

	public function getReportDate() {
		return $this->m_strReportDate;
	}

	public function sqlReportDate() {
		return ( true == isset( $this->m_strReportDate ) ) ? '\'' . $this->m_strReportDate . '\'' : 'NULL';
	}

	public function setChangeInDutyDate( $strChangeInDutyDate ) {
		$this->set( 'm_strChangeInDutyDate', CStrings::strTrimDef( $strChangeInDutyDate, -1, NULL, true ) );
	}

	public function getChangeInDutyDate() {
		return $this->m_strChangeInDutyDate;
	}

	public function sqlChangeInDutyDate() {
		return ( true == isset( $this->m_strChangeInDutyDate ) ) ? '\'' . $this->m_strChangeInDutyDate . '\'' : 'NULL';
	}

	public function setEndOfServiceDate( $strEndOfServiceDate ) {
		$this->set( 'm_strEndOfServiceDate', CStrings::strTrimDef( $strEndOfServiceDate, -1, NULL, true ) );
	}

	public function getEndOfServiceDate() {
		return $this->m_strEndOfServiceDate;
	}

	public function sqlEndOfServiceDate() {
		return ( true == isset( $this->m_strEndOfServiceDate ) ) ? '\'' . $this->m_strEndOfServiceDate . '\'' : 'NULL';
	}

	public function setDeploymentStartDate( $strDeploymentStartDate ) {
		$this->set( 'm_strDeploymentStartDate', CStrings::strTrimDef( $strDeploymentStartDate, -1, NULL, true ) );
	}

	public function getDeploymentStartDate() {
		return $this->m_strDeploymentStartDate;
	}

	public function sqlDeploymentStartDate() {
		return ( true == isset( $this->m_strDeploymentStartDate ) ) ? '\'' . $this->m_strDeploymentStartDate . '\'' : 'NULL';
	}

	public function setDeploymentEndDate( $strDeploymentEndDate ) {
		$this->set( 'm_strDeploymentEndDate', CStrings::strTrimDef( $strDeploymentEndDate, -1, NULL, true ) );
	}

	public function getDeploymentEndDate() {
		return $this->m_strDeploymentEndDate;
	}

	public function sqlDeploymentEndDate() {
		return ( true == isset( $this->m_strDeploymentEndDate ) ) ? '\'' . $this->m_strDeploymentEndDate . '\'' : 'NULL';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NULL';
	}

	public function setIsPromotable( $boolIsPromotable ) {
		$this->set( 'm_boolIsPromotable', CStrings::strToBool( $boolIsPromotable ) );
	}

	public function getIsPromotable() {
		return $this->m_boolIsPromotable;
	}

	public function sqlIsPromotable() {
		return ( true == isset( $this->m_boolIsPromotable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPromotable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsWarrantCandidate( $boolIsWarrantCandidate ) {
		$this->set( 'm_boolIsWarrantCandidate', CStrings::strToBool( $boolIsWarrantCandidate ) );
	}

	public function getIsWarrantCandidate() {
		return $this->m_boolIsWarrantCandidate;
	}

	public function sqlIsWarrantCandidate() {
		return ( true == isset( $this->m_boolIsWarrantCandidate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsWarrantCandidate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsActiveReservist( $boolIsActiveReservist ) {
		$this->set( 'm_boolIsActiveReservist', CStrings::strToBool( $boolIsActiveReservist ) );
	}

	public function getIsActiveReservist() {
		return $this->m_boolIsActiveReservist;
	}

	public function sqlIsActiveReservist() {
		return ( true == isset( $this->m_boolIsActiveReservist ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActiveReservist ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDeployed( $boolIsDeployed ) {
		$this->set( 'm_boolIsDeployed', CStrings::strToBool( $boolIsDeployed ) );
	}

	public function getIsDeployed() {
		return $this->m_boolIsDeployed;
	}

	public function sqlIsDeployed() {
		return ( true == isset( $this->m_boolIsDeployed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDeployed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPostDateIgnored( $boolIsPostDateIgnored ) {
		$this->set( 'm_boolIsPostDateIgnored', CStrings::strToBool( $boolIsPostDateIgnored ) );
	}

	public function getIsPostDateIgnored() {
		return $this->m_boolIsPostDateIgnored;
	}

	public function sqlIsPostDateIgnored() {
		return ( true == isset( $this->m_boolIsPostDateIgnored ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPostDateIgnored ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsOpeningLog( $boolIsOpeningLog ) {
		$this->set( 'm_boolIsOpeningLog', CStrings::strToBool( $boolIsOpeningLog ) );
	}

	public function getIsOpeningLog() {
		return $this->m_boolIsOpeningLog;
	}

	public function sqlIsOpeningLog() {
		return ( true == isset( $this->m_boolIsOpeningLog ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOpeningLog ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setEligibilityDate( $strEligibilityDate ) {
		$this->set( 'm_strEligibilityDate', CStrings::strTrimDef( $strEligibilityDate, -1, NULL, true ) );
	}

	public function getEligibilityDate() {
		return $this->m_strEligibilityDate;
	}

	public function sqlEligibilityDate() {
		return ( true == isset( $this->m_strEligibilityDate ) ) ? '\'' . $this->m_strEligibilityDate . '\'' : 'NULL';
	}

	public function setRateProtectedAmount( $fltRateProtectedAmount ) {
		$this->set( 'm_fltRateProtectedAmount', CStrings::strToFloatDef( $fltRateProtectedAmount, NULL, false, 2 ) );
	}

	public function getRateProtectedAmount() {
		return $this->m_fltRateProtectedAmount;
	}

	public function sqlRateProtectedAmount() {
		return ( true == isset( $this->m_fltRateProtectedAmount ) ) ? ( string ) $this->m_fltRateProtectedAmount : '0';
	}

	public function setMilitaryMaritalStatusTypeId( $intMilitaryMaritalStatusTypeId ) {
		$this->set( 'm_intMilitaryMaritalStatusTypeId', CStrings::strToIntDef( $intMilitaryMaritalStatusTypeId, NULL, false ) );
	}

	public function getMilitaryMaritalStatusTypeId() {
		return $this->m_intMilitaryMaritalStatusTypeId;
	}

	public function sqlMilitaryMaritalStatusTypeId() {
		return ( true == isset( $this->m_intMilitaryMaritalStatusTypeId ) ) ? ( string ) $this->m_intMilitaryMaritalStatusTypeId : '1';
	}

	public function setMilitaryDependentStatusTypeId( $intMilitaryDependentStatusTypeId ) {
		$this->set( 'm_intMilitaryDependentStatusTypeId', CStrings::strToIntDef( $intMilitaryDependentStatusTypeId, NULL, false ) );
	}

	public function getMilitaryDependentStatusTypeId() {
		return $this->m_intMilitaryDependentStatusTypeId;
	}

	public function sqlMilitaryDependentStatusTypeId() {
		return ( true == isset( $this->m_intMilitaryDependentStatusTypeId ) ) ? ( string ) $this->m_intMilitaryDependentStatusTypeId : '2';
	}

	public function setMilitaryDependentRelationshipId( $intMilitaryDependentRelationshipId ) {
		$this->set( 'm_intMilitaryDependentRelationshipId', CStrings::strToIntDef( $intMilitaryDependentRelationshipId, NULL, false ) );
	}

	public function getMilitaryDependentRelationshipId() {
		return $this->m_intMilitaryDependentRelationshipId;
	}

	public function sqlMilitaryDependentRelationshipId() {
		return ( true == isset( $this->m_intMilitaryDependentRelationshipId ) ) ? ( string ) $this->m_intMilitaryDependentRelationshipId : 'NULL';
	}

	public function setExternalResidentId( $strExternalResidentId ) {
		$this->set( 'm_strExternalResidentId', CStrings::strTrimDef( $strExternalResidentId, 50, NULL, true ) );
	}

	public function getExternalResidentId() {
		return $this->m_strExternalResidentId;
	}

	public function sqlExternalResidentId() {
		return ( true == isset( $this->m_strExternalResidentId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strExternalResidentId ) : '\'' . addslashes( $this->m_strExternalResidentId ) . '\'' ) : 'NULL';
	}

	public function setArrivalDate( $strArrivalDate ) {
		$this->set( 'm_strArrivalDate', CStrings::strTrimDef( $strArrivalDate, -1, NULL, true ) );
	}

	public function getArrivalDate() {
		return $this->m_strArrivalDate;
	}

	public function sqlArrivalDate() {
		return ( true == isset( $this->m_strArrivalDate ) ) ? '\'' . $this->m_strArrivalDate . '\'' : 'NULL';
	}

	public function setMilitaryPayGradeEffectiveDate( $strMilitaryPayGradeEffectiveDate ) {
		$this->set( 'm_strMilitaryPayGradeEffectiveDate', CStrings::strTrimDef( $strMilitaryPayGradeEffectiveDate, -1, NULL, true ) );
	}

	public function getMilitaryPayGradeEffectiveDate() {
		return $this->m_strMilitaryPayGradeEffectiveDate;
	}

	public function sqlMilitaryPayGradeEffectiveDate() {
		return ( true == isset( $this->m_strMilitaryPayGradeEffectiveDate ) ) ? '\'' . $this->m_strMilitaryPayGradeEffectiveDate . '\'' : 'NULL';
	}

	public function setIsFullBah( $boolIsFullBah ) {
		$this->set( 'm_boolIsFullBah', CStrings::strToBool( $boolIsFullBah ) );
	}

	public function getIsFullBah() {
		return $this->m_boolIsFullBah;
	}

	public function sqlIsFullBah() {
		return ( true == isset( $this->m_boolIsFullBah ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFullBah ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, customer_military_detail_id, customer_id, military_status_id, military_component_id, military_pay_grade_id, military_rank_id, military_assistance_type_id, military_non_military_type_id, future_military_installation_id, prior_customer_military_detail_log_id, post_date, apply_through_post_date, unit_id_code, commanding_officer, last_assignment, duty_address_street1, duty_address_street2, duty_address_city, duty_address_state_code, duty_address_postal_code, duty_phone_number, duty_email_address, deployed_location, future_installation_other, future_report_date, enlistment_date, rank_effective_date, report_date, change_in_duty_date, end_of_service_date, deployment_start_date, deployment_end_date, log_datetime, is_promotable, is_warrant_candidate, is_active_reservist, is_deployed, is_post_date_ignored, is_opening_log, created_by, created_on, updated_by, updated_on, eligibility_date, rate_protected_amount, military_marital_status_type_id, military_dependent_status_type_id, military_dependent_relationship_id, external_resident_id, arrival_date, military_pay_grade_effective_date, is_full_bah, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCustomerMilitaryDetailId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlMilitaryStatusId() . ', ' .
						$this->sqlMilitaryComponentId() . ', ' .
						$this->sqlMilitaryPayGradeId() . ', ' .
						$this->sqlMilitaryRankId() . ', ' .
						$this->sqlMilitaryAssistanceTypeId() . ', ' .
						$this->sqlMilitaryNonMilitaryTypeId() . ', ' .
						$this->sqlFutureMilitaryInstallationId() . ', ' .
						$this->sqlPriorCustomerMilitaryDetailLogId() . ', ' .
						$this->sqlPostDate() . ', ' .
						$this->sqlApplyThroughPostDate() . ', ' .
						$this->sqlUnitIdCode() . ', ' .
						$this->sqlCommandingOfficer() . ', ' .
						$this->sqlLastAssignment() . ', ' .
						$this->sqlDutyAddressStreet1() . ', ' .
						$this->sqlDutyAddressStreet2() . ', ' .
						$this->sqlDutyAddressCity() . ', ' .
						$this->sqlDutyAddressStateCode() . ', ' .
						$this->sqlDutyAddressPostalCode() . ', ' .
						$this->sqlDutyPhoneNumber() . ', ' .
						$this->sqlDutyEmailAddress() . ', ' .
						$this->sqlDeployedLocation() . ', ' .
						$this->sqlFutureInstallationOther() . ', ' .
						$this->sqlFutureReportDate() . ', ' .
						$this->sqlEnlistmentDate() . ', ' .
						$this->sqlRankEffectiveDate() . ', ' .
						$this->sqlReportDate() . ', ' .
						$this->sqlChangeInDutyDate() . ', ' .
						$this->sqlEndOfServiceDate() . ', ' .
						$this->sqlDeploymentStartDate() . ', ' .
						$this->sqlDeploymentEndDate() . ', ' .
						$this->sqlLogDatetime() . ', ' .
						$this->sqlIsPromotable() . ', ' .
						$this->sqlIsWarrantCandidate() . ', ' .
						$this->sqlIsActiveReservist() . ', ' .
						$this->sqlIsDeployed() . ', ' .
						$this->sqlIsPostDateIgnored() . ', ' .
						$this->sqlIsOpeningLog() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$this->sqlEligibilityDate() . ', ' .
						$this->sqlRateProtectedAmount() . ', ' .
						$this->sqlMilitaryMaritalStatusTypeId() . ', ' .
						$this->sqlMilitaryDependentStatusTypeId() . ', ' .
						$this->sqlMilitaryDependentRelationshipId() . ', ' .
						$this->sqlExternalResidentId() . ', ' .
						$this->sqlArrivalDate() . ', ' .
						$this->sqlMilitaryPayGradeEffectiveDate() . ', ' .
						$this->sqlIsFullBah() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_military_detail_id = ' . $this->sqlCustomerMilitaryDetailId(). ',' ; } elseif( true == array_key_exists( 'CustomerMilitaryDetailId', $this->getChangedColumns() ) ) { $strSql .= ' customer_military_detail_id = ' . $this->sqlCustomerMilitaryDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' military_status_id = ' . $this->sqlMilitaryStatusId(). ',' ; } elseif( true == array_key_exists( 'MilitaryStatusId', $this->getChangedColumns() ) ) { $strSql .= ' military_status_id = ' . $this->sqlMilitaryStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' military_component_id = ' . $this->sqlMilitaryComponentId(). ',' ; } elseif( true == array_key_exists( 'MilitaryComponentId', $this->getChangedColumns() ) ) { $strSql .= ' military_component_id = ' . $this->sqlMilitaryComponentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' military_pay_grade_id = ' . $this->sqlMilitaryPayGradeId(). ',' ; } elseif( true == array_key_exists( 'MilitaryPayGradeId', $this->getChangedColumns() ) ) { $strSql .= ' military_pay_grade_id = ' . $this->sqlMilitaryPayGradeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' military_rank_id = ' . $this->sqlMilitaryRankId(). ',' ; } elseif( true == array_key_exists( 'MilitaryRankId', $this->getChangedColumns() ) ) { $strSql .= ' military_rank_id = ' . $this->sqlMilitaryRankId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' military_assistance_type_id = ' . $this->sqlMilitaryAssistanceTypeId(). ',' ; } elseif( true == array_key_exists( 'MilitaryAssistanceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' military_assistance_type_id = ' . $this->sqlMilitaryAssistanceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' military_non_military_type_id = ' . $this->sqlMilitaryNonMilitaryTypeId(). ',' ; } elseif( true == array_key_exists( 'MilitaryNonMilitaryTypeId', $this->getChangedColumns() ) ) { $strSql .= ' military_non_military_type_id = ' . $this->sqlMilitaryNonMilitaryTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' future_military_installation_id = ' . $this->sqlFutureMilitaryInstallationId(). ',' ; } elseif( true == array_key_exists( 'FutureMilitaryInstallationId', $this->getChangedColumns() ) ) { $strSql .= ' future_military_installation_id = ' . $this->sqlFutureMilitaryInstallationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prior_customer_military_detail_log_id = ' . $this->sqlPriorCustomerMilitaryDetailLogId(). ',' ; } elseif( true == array_key_exists( 'PriorCustomerMilitaryDetailLogId', $this->getChangedColumns() ) ) { $strSql .= ' prior_customer_military_detail_log_id = ' . $this->sqlPriorCustomerMilitaryDetailLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate(). ',' ; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate(). ',' ; } elseif( true == array_key_exists( 'ApplyThroughPostDate', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_id_code = ' . $this->sqlUnitIdCode(). ',' ; } elseif( true == array_key_exists( 'UnitIdCode', $this->getChangedColumns() ) ) { $strSql .= ' unit_id_code = ' . $this->sqlUnitIdCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commanding_officer = ' . $this->sqlCommandingOfficer(). ',' ; } elseif( true == array_key_exists( 'CommandingOfficer', $this->getChangedColumns() ) ) { $strSql .= ' commanding_officer = ' . $this->sqlCommandingOfficer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_assignment = ' . $this->sqlLastAssignment(). ',' ; } elseif( true == array_key_exists( 'LastAssignment', $this->getChangedColumns() ) ) { $strSql .= ' last_assignment = ' . $this->sqlLastAssignment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' duty_address_street1 = ' . $this->sqlDutyAddressStreet1(). ',' ; } elseif( true == array_key_exists( 'DutyAddressStreet1', $this->getChangedColumns() ) ) { $strSql .= ' duty_address_street1 = ' . $this->sqlDutyAddressStreet1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' duty_address_street2 = ' . $this->sqlDutyAddressStreet2(). ',' ; } elseif( true == array_key_exists( 'DutyAddressStreet2', $this->getChangedColumns() ) ) { $strSql .= ' duty_address_street2 = ' . $this->sqlDutyAddressStreet2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' duty_address_city = ' . $this->sqlDutyAddressCity(). ',' ; } elseif( true == array_key_exists( 'DutyAddressCity', $this->getChangedColumns() ) ) { $strSql .= ' duty_address_city = ' . $this->sqlDutyAddressCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' duty_address_state_code = ' . $this->sqlDutyAddressStateCode(). ',' ; } elseif( true == array_key_exists( 'DutyAddressStateCode', $this->getChangedColumns() ) ) { $strSql .= ' duty_address_state_code = ' . $this->sqlDutyAddressStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' duty_address_postal_code = ' . $this->sqlDutyAddressPostalCode(). ',' ; } elseif( true == array_key_exists( 'DutyAddressPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' duty_address_postal_code = ' . $this->sqlDutyAddressPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' duty_phone_number = ' . $this->sqlDutyPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'DutyPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' duty_phone_number = ' . $this->sqlDutyPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' duty_email_address = ' . $this->sqlDutyEmailAddress(). ',' ; } elseif( true == array_key_exists( 'DutyEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' duty_email_address = ' . $this->sqlDutyEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deployed_location = ' . $this->sqlDeployedLocation(). ',' ; } elseif( true == array_key_exists( 'DeployedLocation', $this->getChangedColumns() ) ) { $strSql .= ' deployed_location = ' . $this->sqlDeployedLocation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' future_installation_other = ' . $this->sqlFutureInstallationOther(). ',' ; } elseif( true == array_key_exists( 'FutureInstallationOther', $this->getChangedColumns() ) ) { $strSql .= ' future_installation_other = ' . $this->sqlFutureInstallationOther() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' future_report_date = ' . $this->sqlFutureReportDate(). ',' ; } elseif( true == array_key_exists( 'FutureReportDate', $this->getChangedColumns() ) ) { $strSql .= ' future_report_date = ' . $this->sqlFutureReportDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' enlistment_date = ' . $this->sqlEnlistmentDate(). ',' ; } elseif( true == array_key_exists( 'EnlistmentDate', $this->getChangedColumns() ) ) { $strSql .= ' enlistment_date = ' . $this->sqlEnlistmentDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rank_effective_date = ' . $this->sqlRankEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'RankEffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' rank_effective_date = ' . $this->sqlRankEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_date = ' . $this->sqlReportDate(). ',' ; } elseif( true == array_key_exists( 'ReportDate', $this->getChangedColumns() ) ) { $strSql .= ' report_date = ' . $this->sqlReportDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' change_in_duty_date = ' . $this->sqlChangeInDutyDate(). ',' ; } elseif( true == array_key_exists( 'ChangeInDutyDate', $this->getChangedColumns() ) ) { $strSql .= ' change_in_duty_date = ' . $this->sqlChangeInDutyDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_of_service_date = ' . $this->sqlEndOfServiceDate(). ',' ; } elseif( true == array_key_exists( 'EndOfServiceDate', $this->getChangedColumns() ) ) { $strSql .= ' end_of_service_date = ' . $this->sqlEndOfServiceDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deployment_start_date = ' . $this->sqlDeploymentStartDate(). ',' ; } elseif( true == array_key_exists( 'DeploymentStartDate', $this->getChangedColumns() ) ) { $strSql .= ' deployment_start_date = ' . $this->sqlDeploymentStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deployment_end_date = ' . $this->sqlDeploymentEndDate(). ',' ; } elseif( true == array_key_exists( 'DeploymentEndDate', $this->getChangedColumns() ) ) { $strSql .= ' deployment_end_date = ' . $this->sqlDeploymentEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime(). ',' ; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_promotable = ' . $this->sqlIsPromotable(). ',' ; } elseif( true == array_key_exists( 'IsPromotable', $this->getChangedColumns() ) ) { $strSql .= ' is_promotable = ' . $this->sqlIsPromotable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_warrant_candidate = ' . $this->sqlIsWarrantCandidate(). ',' ; } elseif( true == array_key_exists( 'IsWarrantCandidate', $this->getChangedColumns() ) ) { $strSql .= ' is_warrant_candidate = ' . $this->sqlIsWarrantCandidate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active_reservist = ' . $this->sqlIsActiveReservist(). ',' ; } elseif( true == array_key_exists( 'IsActiveReservist', $this->getChangedColumns() ) ) { $strSql .= ' is_active_reservist = ' . $this->sqlIsActiveReservist() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_deployed = ' . $this->sqlIsDeployed(). ',' ; } elseif( true == array_key_exists( 'IsDeployed', $this->getChangedColumns() ) ) { $strSql .= ' is_deployed = ' . $this->sqlIsDeployed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored(). ',' ; } elseif( true == array_key_exists( 'IsPostDateIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog(). ',' ; } elseif( true == array_key_exists( 'IsOpeningLog', $this->getChangedColumns() ) ) { $strSql .= ' is_opening_log = ' . $this->sqlIsOpeningLog() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eligibility_date = ' . $this->sqlEligibilityDate(). ',' ; } elseif( true == array_key_exists( 'EligibilityDate', $this->getChangedColumns() ) ) { $strSql .= ' eligibility_date = ' . $this->sqlEligibilityDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate_protected_amount = ' . $this->sqlRateProtectedAmount(). ',' ; } elseif( true == array_key_exists( 'RateProtectedAmount', $this->getChangedColumns() ) ) { $strSql .= ' rate_protected_amount = ' . $this->sqlRateProtectedAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' military_marital_status_type_id = ' . $this->sqlMilitaryMaritalStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'MilitaryMaritalStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' military_marital_status_type_id = ' . $this->sqlMilitaryMaritalStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' military_dependent_status_type_id = ' . $this->sqlMilitaryDependentStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'MilitaryDependentStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' military_dependent_status_type_id = ' . $this->sqlMilitaryDependentStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' military_dependent_relationship_id = ' . $this->sqlMilitaryDependentRelationshipId(). ',' ; } elseif( true == array_key_exists( 'MilitaryDependentRelationshipId', $this->getChangedColumns() ) ) { $strSql .= ' military_dependent_relationship_id = ' . $this->sqlMilitaryDependentRelationshipId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_resident_id = ' . $this->sqlExternalResidentId(). ',' ; } elseif( true == array_key_exists( 'ExternalResidentId', $this->getChangedColumns() ) ) { $strSql .= ' external_resident_id = ' . $this->sqlExternalResidentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' arrival_date = ' . $this->sqlArrivalDate(). ',' ; } elseif( true == array_key_exists( 'ArrivalDate', $this->getChangedColumns() ) ) { $strSql .= ' arrival_date = ' . $this->sqlArrivalDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' military_pay_grade_effective_date = ' . $this->sqlMilitaryPayGradeEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'MilitaryPayGradeEffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' military_pay_grade_effective_date = ' . $this->sqlMilitaryPayGradeEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_full_bah = ' . $this->sqlIsFullBah(). ',' ; } elseif( true == array_key_exists( 'IsFullBah', $this->getChangedColumns() ) ) { $strSql .= ' is_full_bah = ' . $this->sqlIsFullBah() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'customer_military_detail_id' => $this->getCustomerMilitaryDetailId(),
			'customer_id' => $this->getCustomerId(),
			'military_status_id' => $this->getMilitaryStatusId(),
			'military_component_id' => $this->getMilitaryComponentId(),
			'military_pay_grade_id' => $this->getMilitaryPayGradeId(),
			'military_rank_id' => $this->getMilitaryRankId(),
			'military_assistance_type_id' => $this->getMilitaryAssistanceTypeId(),
			'military_non_military_type_id' => $this->getMilitaryNonMilitaryTypeId(),
			'future_military_installation_id' => $this->getFutureMilitaryInstallationId(),
			'prior_customer_military_detail_log_id' => $this->getPriorCustomerMilitaryDetailLogId(),
			'post_date' => $this->getPostDate(),
			'apply_through_post_date' => $this->getApplyThroughPostDate(),
			'unit_id_code' => $this->getUnitIdCode(),
			'commanding_officer' => $this->getCommandingOfficer(),
			'last_assignment' => $this->getLastAssignment(),
			'duty_address_street1' => $this->getDutyAddressStreet1(),
			'duty_address_street2' => $this->getDutyAddressStreet2(),
			'duty_address_city' => $this->getDutyAddressCity(),
			'duty_address_state_code' => $this->getDutyAddressStateCode(),
			'duty_address_postal_code' => $this->getDutyAddressPostalCode(),
			'duty_phone_number' => $this->getDutyPhoneNumber(),
			'duty_email_address' => $this->getDutyEmailAddress(),
			'deployed_location' => $this->getDeployedLocation(),
			'future_installation_other' => $this->getFutureInstallationOther(),
			'future_report_date' => $this->getFutureReportDate(),
			'enlistment_date' => $this->getEnlistmentDate(),
			'rank_effective_date' => $this->getRankEffectiveDate(),
			'report_date' => $this->getReportDate(),
			'change_in_duty_date' => $this->getChangeInDutyDate(),
			'end_of_service_date' => $this->getEndOfServiceDate(),
			'deployment_start_date' => $this->getDeploymentStartDate(),
			'deployment_end_date' => $this->getDeploymentEndDate(),
			'log_datetime' => $this->getLogDatetime(),
			'is_promotable' => $this->getIsPromotable(),
			'is_warrant_candidate' => $this->getIsWarrantCandidate(),
			'is_active_reservist' => $this->getIsActiveReservist(),
			'is_deployed' => $this->getIsDeployed(),
			'is_post_date_ignored' => $this->getIsPostDateIgnored(),
			'is_opening_log' => $this->getIsOpeningLog(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'eligibility_date' => $this->getEligibilityDate(),
			'rate_protected_amount' => $this->getRateProtectedAmount(),
			'military_marital_status_type_id' => $this->getMilitaryMaritalStatusTypeId(),
			'military_dependent_status_type_id' => $this->getMilitaryDependentStatusTypeId(),
			'military_dependent_relationship_id' => $this->getMilitaryDependentRelationshipId(),
			'external_resident_id' => $this->getExternalResidentId(),
			'arrival_date' => $this->getArrivalDate(),
			'military_pay_grade_effective_date' => $this->getMilitaryPayGradeEffectiveDate(),
			'is_full_bah' => $this->getIsFullBah(),
			'details' => $this->getDetails()
		);
	}

}
?>