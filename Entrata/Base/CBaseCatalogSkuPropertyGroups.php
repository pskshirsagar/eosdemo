<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCatalogSkuPropertyGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCatalogSkuPropertyGroups extends CEosPluralBase {

	/**
	 * @return CCatalogSkuPropertyGroup[]
	 */
	public static function fetchCatalogSkuPropertyGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCatalogSkuPropertyGroup::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCatalogSkuPropertyGroup
	 */
	public static function fetchCatalogSkuPropertyGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCatalogSkuPropertyGroup::class, $objDatabase );
	}

	public static function fetchCatalogSkuPropertyGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'catalog_sku_property_groups', $objDatabase );
	}

	public static function fetchCatalogSkuPropertyGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCatalogSkuPropertyGroup( sprintf( 'SELECT * FROM catalog_sku_property_groups WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCatalogSkuPropertyGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchCatalogSkuPropertyGroups( sprintf( 'SELECT * FROM catalog_sku_property_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCatalogSkuPropertyGroupsByApCatalogItemIdByCid( $intApCatalogItemId, $intCid, $objDatabase ) {
		return self::fetchCatalogSkuPropertyGroups( sprintf( 'SELECT * FROM catalog_sku_property_groups WHERE ap_catalog_item_id = %d AND cid = %d', ( int ) $intApCatalogItemId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCatalogSkuPropertyGroupsByPropertyGroupIdByCid( $intPropertyGroupId, $intCid, $objDatabase ) {
		return self::fetchCatalogSkuPropertyGroups( sprintf( 'SELECT * FROM catalog_sku_property_groups WHERE property_group_id = %d AND cid = %d', ( int ) $intPropertyGroupId, ( int ) $intCid ), $objDatabase );
	}

}
?>