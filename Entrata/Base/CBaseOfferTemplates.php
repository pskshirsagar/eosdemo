<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COfferTemplates
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOfferTemplates extends CEosPluralBase {

	/**
	 * @return COfferTemplate[]
	 */
	public static function fetchOfferTemplates( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, COfferTemplate::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return COfferTemplate
	 */
	public static function fetchOfferTemplate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, COfferTemplate::class, $objDatabase );
	}

	public static function fetchOfferTemplateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'offer_templates', $objDatabase );
	}

	public static function fetchOfferTemplateByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchOfferTemplate( sprintf( 'SELECT * FROM offer_templates WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferTemplatesByCid( $intCid, $objDatabase ) {
		return self::fetchOfferTemplates( sprintf( 'SELECT * FROM offer_templates WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferTemplatesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchOfferTemplates( sprintf( 'SELECT * FROM offer_templates WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferTemplatesByApplicationStageStatusIdByCid( $intApplicationStageStatusId, $intCid, $objDatabase ) {
		return self::fetchOfferTemplates( sprintf( 'SELECT * FROM offer_templates WHERE application_stage_status_id = %d AND cid = %d', ( int ) $intApplicationStageStatusId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferTemplatesByOfferTemplateTypeIdByCid( $intOfferTemplateTypeId, $intCid, $objDatabase ) {
		return self::fetchOfferTemplates( sprintf( 'SELECT * FROM offer_templates WHERE offer_template_type_id = %d AND cid = %d', ( int ) $intOfferTemplateTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferTemplatesByOccupancyTypeIdByCid( $intOccupancyTypeId, $intCid, $objDatabase ) {
		return self::fetchOfferTemplates( sprintf( 'SELECT * FROM offer_templates WHERE occupancy_type_id = %d AND cid = %d', ( int ) $intOccupancyTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferTemplatesByLeaseIntervalTypeIdByCid( $intLeaseIntervalTypeId, $intCid, $objDatabase ) {
		return self::fetchOfferTemplates( sprintf( 'SELECT * FROM offer_templates WHERE lease_interval_type_id = %d AND cid = %d', ( int ) $intLeaseIntervalTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOfferTemplatesByImportOfferTemplateIdByCid( $intImportOfferTemplateId, $intCid, $objDatabase ) {
		return self::fetchOfferTemplates( sprintf( 'SELECT * FROM offer_templates WHERE import_offer_template_id = %d AND cid = %d', ( int ) $intImportOfferTemplateId, ( int ) $intCid ), $objDatabase );
	}

}
?>