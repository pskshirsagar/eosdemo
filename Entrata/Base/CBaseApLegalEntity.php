<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApLegalEntity extends CEosSingularBase {

	const TABLE_NAME = 'public.ap_legal_entities';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApPayeeId;
	protected $m_intForm1099TypeId;
	protected $m_intForm1099BoxTypeId;
	protected $m_intOwnerTypeId;
	protected $m_intVendorEntityId;
	protected $m_intApRemittanceId;
	protected $m_strEntityName;
	protected $m_boolReceives1099;
	protected $m_boolIsShowCompanyName;
	protected $m_strTaxNumberEncrypted;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_strLastInvitedToSync;
	protected $m_strTaxNumberHash;

	public function __construct() {
		parent::__construct();

		$this->m_intForm1099TypeId = '1';
		$this->m_intForm1099BoxTypeId = '7';
		$this->m_boolReceives1099 = true;
		$this->m_boolIsShowCompanyName = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['form_1099_type_id'] ) && $boolDirectSet ) $this->set( 'm_intForm1099TypeId', trim( $arrValues['form_1099_type_id'] ) ); elseif( isset( $arrValues['form_1099_type_id'] ) ) $this->setForm1099TypeId( $arrValues['form_1099_type_id'] );
		if( isset( $arrValues['form_1099_box_type_id'] ) && $boolDirectSet ) $this->set( 'm_intForm1099BoxTypeId', trim( $arrValues['form_1099_box_type_id'] ) ); elseif( isset( $arrValues['form_1099_box_type_id'] ) ) $this->setForm1099BoxTypeId( $arrValues['form_1099_box_type_id'] );
		if( isset( $arrValues['owner_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOwnerTypeId', trim( $arrValues['owner_type_id'] ) ); elseif( isset( $arrValues['owner_type_id'] ) ) $this->setOwnerTypeId( $arrValues['owner_type_id'] );
		if( isset( $arrValues['vendor_entity_id'] ) && $boolDirectSet ) $this->set( 'm_intVendorEntityId', trim( $arrValues['vendor_entity_id'] ) ); elseif( isset( $arrValues['vendor_entity_id'] ) ) $this->setVendorEntityId( $arrValues['vendor_entity_id'] );
		if( isset( $arrValues['ap_remittance_id'] ) && $boolDirectSet ) $this->set( 'm_intApRemittanceId', trim( $arrValues['ap_remittance_id'] ) ); elseif( isset( $arrValues['ap_remittance_id'] ) ) $this->setApRemittanceId( $arrValues['ap_remittance_id'] );
		if( isset( $arrValues['entity_name'] ) && $boolDirectSet ) $this->set( 'm_strEntityName', trim( stripcslashes( $arrValues['entity_name'] ) ) ); elseif( isset( $arrValues['entity_name'] ) ) $this->setEntityName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['entity_name'] ) : $arrValues['entity_name'] );
		if( isset( $arrValues['receives_1099'] ) && $boolDirectSet ) $this->set( 'm_boolReceives1099', trim( stripcslashes( $arrValues['receives_1099'] ) ) ); elseif( isset( $arrValues['receives_1099'] ) ) $this->setReceives1099( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['receives_1099'] ) : $arrValues['receives_1099'] );
		if( isset( $arrValues['is_show_company_name'] ) && $boolDirectSet ) $this->set( 'm_boolIsShowCompanyName', trim( stripcslashes( $arrValues['is_show_company_name'] ) ) ); elseif( isset( $arrValues['is_show_company_name'] ) ) $this->setIsShowCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_show_company_name'] ) : $arrValues['is_show_company_name'] );
		if( isset( $arrValues['tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberEncrypted', trim( stripcslashes( $arrValues['tax_number_encrypted'] ) ) ); elseif( isset( $arrValues['tax_number_encrypted'] ) ) $this->setTaxNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_number_encrypted'] ) : $arrValues['tax_number_encrypted'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['last_invited_to_sync'] ) && $boolDirectSet ) $this->set( 'm_strLastInvitedToSync', trim( $arrValues['last_invited_to_sync'] ) ); elseif( isset( $arrValues['last_invited_to_sync'] ) ) $this->setLastInvitedToSync( $arrValues['last_invited_to_sync'] );
		if( isset( $arrValues['tax_number_hash'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberHash', trim( stripcslashes( $arrValues['tax_number_hash'] ) ) ); elseif( isset( $arrValues['tax_number_hash'] ) ) $this->setTaxNumberHash( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_number_hash'] ) : $arrValues['tax_number_hash'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setForm1099TypeId( $intForm1099TypeId ) {
		$this->set( 'm_intForm1099TypeId', CStrings::strToIntDef( $intForm1099TypeId, NULL, false ) );
	}

	public function getForm1099TypeId() {
		return $this->m_intForm1099TypeId;
	}

	public function sqlForm1099TypeId() {
		return ( true == isset( $this->m_intForm1099TypeId ) ) ? ( string ) $this->m_intForm1099TypeId : '1';
	}

	public function setForm1099BoxTypeId( $intForm1099BoxTypeId ) {
		$this->set( 'm_intForm1099BoxTypeId', CStrings::strToIntDef( $intForm1099BoxTypeId, NULL, false ) );
	}

	public function getForm1099BoxTypeId() {
		return $this->m_intForm1099BoxTypeId;
	}

	public function sqlForm1099BoxTypeId() {
		return ( true == isset( $this->m_intForm1099BoxTypeId ) ) ? ( string ) $this->m_intForm1099BoxTypeId : '7';
	}

	public function setOwnerTypeId( $intOwnerTypeId ) {
		$this->set( 'm_intOwnerTypeId', CStrings::strToIntDef( $intOwnerTypeId, NULL, false ) );
	}

	public function getOwnerTypeId() {
		return $this->m_intOwnerTypeId;
	}

	public function sqlOwnerTypeId() {
		return ( true == isset( $this->m_intOwnerTypeId ) ) ? ( string ) $this->m_intOwnerTypeId : 'NULL';
	}

	public function setVendorEntityId( $intVendorEntityId ) {
		$this->set( 'm_intVendorEntityId', CStrings::strToIntDef( $intVendorEntityId, NULL, false ) );
	}

	public function getVendorEntityId() {
		return $this->m_intVendorEntityId;
	}

	public function sqlVendorEntityId() {
		return ( true == isset( $this->m_intVendorEntityId ) ) ? ( string ) $this->m_intVendorEntityId : 'NULL';
	}

	public function setApRemittanceId( $intApRemittanceId ) {
		$this->set( 'm_intApRemittanceId', CStrings::strToIntDef( $intApRemittanceId, NULL, false ) );
	}

	public function getApRemittanceId() {
		return $this->m_intApRemittanceId;
	}

	public function sqlApRemittanceId() {
		return ( true == isset( $this->m_intApRemittanceId ) ) ? ( string ) $this->m_intApRemittanceId : 'NULL';
	}

	public function setEntityName( $strEntityName ) {
		$this->set( 'm_strEntityName', CStrings::strTrimDef( $strEntityName, 200, NULL, true ) );
	}

	public function getEntityName() {
		return $this->m_strEntityName;
	}

	public function sqlEntityName() {
		return ( true == isset( $this->m_strEntityName ) ) ? '\'' . addslashes( $this->m_strEntityName ) . '\'' : 'NULL';
	}

	public function setReceives1099( $boolReceives1099 ) {
		$this->set( 'm_boolReceives1099', CStrings::strToBool( $boolReceives1099 ) );
	}

	public function getReceives1099() {
		return $this->m_boolReceives1099;
	}

	public function sqlReceives1099() {
		return ( true == isset( $this->m_boolReceives1099 ) ) ? '\'' . ( true == ( bool ) $this->m_boolReceives1099 ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsShowCompanyName( $boolIsShowCompanyName ) {
		$this->set( 'm_boolIsShowCompanyName', CStrings::strToBool( $boolIsShowCompanyName ) );
	}

	public function getIsShowCompanyName() {
		return $this->m_boolIsShowCompanyName;
	}

	public function sqlIsShowCompanyName() {
		return ( true == isset( $this->m_boolIsShowCompanyName ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsShowCompanyName ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted ) {
		$this->set( 'm_strTaxNumberEncrypted', CStrings::strTrimDef( $strTaxNumberEncrypted, 240, NULL, true ) );
	}

	public function getTaxNumberEncrypted() {
		return $this->m_strTaxNumberEncrypted;
	}

	public function sqlTaxNumberEncrypted() {
		return ( true == isset( $this->m_strTaxNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strTaxNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setLastInvitedToSync( $strLastInvitedToSync ) {
		$this->set( 'm_strLastInvitedToSync', CStrings::strTrimDef( $strLastInvitedToSync, -1, NULL, true ) );
	}

	public function getLastInvitedToSync() {
		return $this->m_strLastInvitedToSync;
	}

	public function sqlLastInvitedToSync() {
		return ( true == isset( $this->m_strLastInvitedToSync ) ) ? '\'' . $this->m_strLastInvitedToSync . '\'' : 'NULL';
	}

	public function setTaxNumberHash( $strTaxNumberHash ) {
		$this->set( 'm_strTaxNumberHash', CStrings::strTrimDef( $strTaxNumberHash, 240, NULL, true ) );
	}

	public function getTaxNumberHash() {
		return $this->m_strTaxNumberHash;
	}

	public function sqlTaxNumberHash() {
		return ( true == isset( $this->m_strTaxNumberHash ) ) ? '\'' . addslashes( $this->m_strTaxNumberHash ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ap_payee_id, form_1099_type_id, form_1099_box_type_id, owner_type_id, vendor_entity_id, ap_remittance_id, entity_name, receives_1099, is_show_company_name, tax_number_encrypted, updated_by, updated_on, created_by, created_on, deleted_by, deleted_on, last_invited_to_sync, tax_number_hash )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlApPayeeId() . ', ' .
						$this->sqlForm1099TypeId() . ', ' .
						$this->sqlForm1099BoxTypeId() . ', ' .
						$this->sqlOwnerTypeId() . ', ' .
						$this->sqlVendorEntityId() . ', ' .
						$this->sqlApRemittanceId() . ', ' .
						$this->sqlEntityName() . ', ' .
						$this->sqlReceives1099() . ', ' .
						$this->sqlIsShowCompanyName() . ', ' .
						$this->sqlTaxNumberEncrypted() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						$this->sqlLastInvitedToSync() . ', ' .
						$this->sqlTaxNumberHash() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' form_1099_type_id = ' . $this->sqlForm1099TypeId(). ',' ; } elseif( true == array_key_exists( 'Form1099TypeId', $this->getChangedColumns() ) ) { $strSql .= ' form_1099_type_id = ' . $this->sqlForm1099TypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' form_1099_box_type_id = ' . $this->sqlForm1099BoxTypeId(). ',' ; } elseif( true == array_key_exists( 'Form1099BoxTypeId', $this->getChangedColumns() ) ) { $strSql .= ' form_1099_box_type_id = ' . $this->sqlForm1099BoxTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_type_id = ' . $this->sqlOwnerTypeId(). ',' ; } elseif( true == array_key_exists( 'OwnerTypeId', $this->getChangedColumns() ) ) { $strSql .= ' owner_type_id = ' . $this->sqlOwnerTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_entity_id = ' . $this->sqlVendorEntityId(). ',' ; } elseif( true == array_key_exists( 'VendorEntityId', $this->getChangedColumns() ) ) { $strSql .= ' vendor_entity_id = ' . $this->sqlVendorEntityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_remittance_id = ' . $this->sqlApRemittanceId(). ',' ; } elseif( true == array_key_exists( 'ApRemittanceId', $this->getChangedColumns() ) ) { $strSql .= ' ap_remittance_id = ' . $this->sqlApRemittanceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entity_name = ' . $this->sqlEntityName(). ',' ; } elseif( true == array_key_exists( 'EntityName', $this->getChangedColumns() ) ) { $strSql .= ' entity_name = ' . $this->sqlEntityName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' receives_1099 = ' . $this->sqlReceives1099(). ',' ; } elseif( true == array_key_exists( 'Receives1099', $this->getChangedColumns() ) ) { $strSql .= ' receives_1099 = ' . $this->sqlReceives1099() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_show_company_name = ' . $this->sqlIsShowCompanyName(). ',' ; } elseif( true == array_key_exists( 'IsShowCompanyName', $this->getChangedColumns() ) ) { $strSql .= ' is_show_company_name = ' . $this->sqlIsShowCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'TaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_invited_to_sync = ' . $this->sqlLastInvitedToSync(). ',' ; } elseif( true == array_key_exists( 'LastInvitedToSync', $this->getChangedColumns() ) ) { $strSql .= ' last_invited_to_sync = ' . $this->sqlLastInvitedToSync() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_hash = ' . $this->sqlTaxNumberHash(). ',' ; } elseif( true == array_key_exists( 'TaxNumberHash', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_hash = ' . $this->sqlTaxNumberHash() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ap_payee_id' => $this->getApPayeeId(),
			'form_1099_type_id' => $this->getForm1099TypeId(),
			'form_1099_box_type_id' => $this->getForm1099BoxTypeId(),
			'owner_type_id' => $this->getOwnerTypeId(),
			'vendor_entity_id' => $this->getVendorEntityId(),
			'ap_remittance_id' => $this->getApRemittanceId(),
			'entity_name' => $this->getEntityName(),
			'receives_1099' => $this->getReceives1099(),
			'is_show_company_name' => $this->getIsShowCompanyName(),
			'tax_number_encrypted' => $this->getTaxNumberEncrypted(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'last_invited_to_sync' => $this->getLastInvitedToSync(),
			'tax_number_hash' => $this->getTaxNumberHash()
		);
	}

}
?>