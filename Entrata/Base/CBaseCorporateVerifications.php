<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCorporateVerifications
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCorporateVerifications extends CEosPluralBase {

	/**
	 * @return CCorporateVerification[]
	 */
	public static function fetchCorporateVerifications( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCorporateVerification', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCorporateVerification
	 */
	public static function fetchCorporateVerification( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCorporateVerification', $objDatabase );
	}

	public static function fetchCorporateVerificationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'corporate_verifications', $objDatabase );
	}

	public static function fetchCorporateVerificationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCorporateVerification( sprintf( 'SELECT * FROM corporate_verifications WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporateVerificationsByCid( $intCid, $objDatabase ) {
		return self::fetchCorporateVerifications( sprintf( 'SELECT * FROM corporate_verifications WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporateVerificationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCorporateVerifications( sprintf( 'SELECT * FROM corporate_verifications WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporateVerificationsByCorporateIdByCid( $intCorporateId, $intCid, $objDatabase ) {
		return self::fetchCorporateVerifications( sprintf( 'SELECT * FROM corporate_verifications WHERE corporate_id = %d AND cid = %d', ( int ) $intCorporateId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporateVerificationsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchCorporateVerifications( sprintf( 'SELECT * FROM corporate_verifications WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCorporateVerificationsByStatusTypeIdByCid( $intStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchCorporateVerifications( sprintf( 'SELECT * FROM corporate_verifications WHERE status_type_id = %d AND cid = %d', ( int ) $intStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>