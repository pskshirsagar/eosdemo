<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFileBatchEmail extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.file_batch_emails';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intFileBatchId;
	protected $m_strToEmailAddress;
	protected $m_strFromEmailDisplay;
	protected $m_strReplyToEmailAddress;
	protected $m_strEmailMessage;
	protected $m_strEmailSubject;
	protected $m_intFileExportStatusId;
	protected $m_strExpiresOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['file_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intFileBatchId', trim( $arrValues['file_batch_id'] ) ); elseif( isset( $arrValues['file_batch_id'] ) ) $this->setFileBatchId( $arrValues['file_batch_id'] );
		if( isset( $arrValues['to_email_address'] ) && $boolDirectSet ) $this->set( 'm_strToEmailAddress', trim( stripcslashes( $arrValues['to_email_address'] ) ) ); elseif( isset( $arrValues['to_email_address'] ) ) $this->setToEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['to_email_address'] ) : $arrValues['to_email_address'] );
		if( isset( $arrValues['from_email_display'] ) && $boolDirectSet ) $this->set( 'm_strFromEmailDisplay', trim( stripcslashes( $arrValues['from_email_display'] ) ) ); elseif( isset( $arrValues['from_email_display'] ) ) $this->setFromEmailDisplay( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['from_email_display'] ) : $arrValues['from_email_display'] );
		if( isset( $arrValues['reply_to_email_address'] ) && $boolDirectSet ) $this->set( 'm_strReplyToEmailAddress', trim( stripcslashes( $arrValues['reply_to_email_address'] ) ) ); elseif( isset( $arrValues['reply_to_email_address'] ) ) $this->setReplyToEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reply_to_email_address'] ) : $arrValues['reply_to_email_address'] );
		if( isset( $arrValues['email_message'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strEmailMessage', trim( stripcslashes( $arrValues['email_message'] ) ) ); elseif( isset( $arrValues['email_message'] ) ) $this->setEmailMessage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_message'] ) : $arrValues['email_message'] );
		if( isset( $arrValues['email_subject'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strEmailSubject', trim( stripcslashes( $arrValues['email_subject'] ) ) ); elseif( isset( $arrValues['email_subject'] ) ) $this->setEmailSubject( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_subject'] ) : $arrValues['email_subject'] );
		if( isset( $arrValues['file_export_status_id'] ) && $boolDirectSet ) $this->set( 'm_intFileExportStatusId', trim( $arrValues['file_export_status_id'] ) ); elseif( isset( $arrValues['file_export_status_id'] ) ) $this->setFileExportStatusId( $arrValues['file_export_status_id'] );
		if( isset( $arrValues['expires_on'] ) && $boolDirectSet ) $this->set( 'm_strExpiresOn', trim( $arrValues['expires_on'] ) ); elseif( isset( $arrValues['expires_on'] ) ) $this->setExpiresOn( $arrValues['expires_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setFileBatchId( $intFileBatchId ) {
		$this->set( 'm_intFileBatchId', CStrings::strToIntDef( $intFileBatchId, NULL, false ) );
	}

	public function getFileBatchId() {
		return $this->m_intFileBatchId;
	}

	public function sqlFileBatchId() {
		return ( true == isset( $this->m_intFileBatchId ) ) ? ( string ) $this->m_intFileBatchId : 'NULL';
	}

	public function setToEmailAddress( $strToEmailAddress ) {
		$this->set( 'm_strToEmailAddress', CStrings::strTrimDef( $strToEmailAddress, -1, NULL, true ) );
	}

	public function getToEmailAddress() {
		return $this->m_strToEmailAddress;
	}

	public function sqlToEmailAddress() {
		return ( true == isset( $this->m_strToEmailAddress ) ) ? '\'' . addslashes( $this->m_strToEmailAddress ) . '\'' : 'NULL';
	}

	public function setFromEmailDisplay( $strFromEmailDisplay ) {
		$this->set( 'm_strFromEmailDisplay', CStrings::strTrimDef( $strFromEmailDisplay, 150, NULL, true ) );
	}

	public function getFromEmailDisplay() {
		return $this->m_strFromEmailDisplay;
	}

	public function sqlFromEmailDisplay() {
		return ( true == isset( $this->m_strFromEmailDisplay ) ) ? '\'' . addslashes( $this->m_strFromEmailDisplay ) . '\'' : 'NULL';
	}

	public function setReplyToEmailAddress( $strReplyToEmailAddress ) {
		$this->set( 'm_strReplyToEmailAddress', CStrings::strTrimDef( $strReplyToEmailAddress, 150, NULL, true ) );
	}

	public function getReplyToEmailAddress() {
		return $this->m_strReplyToEmailAddress;
	}

	public function sqlReplyToEmailAddress() {
		return ( true == isset( $this->m_strReplyToEmailAddress ) ) ? '\'' . addslashes( $this->m_strReplyToEmailAddress ) . '\'' : 'NULL';
	}

	public function setEmailMessage( $strEmailMessage, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strEmailMessage', CStrings::strTrimDef( $strEmailMessage, -1, NULL, true ), $strLocaleCode );
	}

	public function getEmailMessage( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strEmailMessage', $strLocaleCode );
	}

	public function sqlEmailMessage() {
		return ( true == isset( $this->m_strEmailMessage ) ) ? '\'' . addslashes( $this->m_strEmailMessage ) . '\'' : 'NULL';
	}

	public function setEmailSubject( $strEmailSubject, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strEmailSubject', CStrings::strTrimDef( $strEmailSubject, -1, NULL, true ), $strLocaleCode );
	}

	public function getEmailSubject( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strEmailSubject', $strLocaleCode );
	}

	public function sqlEmailSubject() {
		return ( true == isset( $this->m_strEmailSubject ) ) ? '\'' . addslashes( $this->m_strEmailSubject ) . '\'' : 'NULL';
	}

	public function setFileExportStatusId( $intFileExportStatusId ) {
		$this->set( 'm_intFileExportStatusId', CStrings::strToIntDef( $intFileExportStatusId, NULL, false ) );
	}

	public function getFileExportStatusId() {
		return $this->m_intFileExportStatusId;
	}

	public function sqlFileExportStatusId() {
		return ( true == isset( $this->m_intFileExportStatusId ) ) ? ( string ) $this->m_intFileExportStatusId : 'NULL';
	}

	public function setExpiresOn( $strExpiresOn ) {
		$this->set( 'm_strExpiresOn', CStrings::strTrimDef( $strExpiresOn, -1, NULL, true ) );
	}

	public function getExpiresOn() {
		return $this->m_strExpiresOn;
	}

	public function sqlExpiresOn() {
		return ( true == isset( $this->m_strExpiresOn ) ) ? '\'' . $this->m_strExpiresOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, file_batch_id, to_email_address, from_email_display, reply_to_email_address, email_message, email_subject, file_export_status_id, expires_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlFileBatchId() . ', ' .
						$this->sqlToEmailAddress() . ', ' .
						$this->sqlFromEmailDisplay() . ', ' .
						$this->sqlReplyToEmailAddress() . ', ' .
						$this->sqlEmailMessage() . ', ' .
						$this->sqlEmailSubject() . ', ' .
						$this->sqlFileExportStatusId() . ', ' .
						$this->sqlExpiresOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_batch_id = ' . $this->sqlFileBatchId(). ',' ; } elseif( true == array_key_exists( 'FileBatchId', $this->getChangedColumns() ) ) { $strSql .= ' file_batch_id = ' . $this->sqlFileBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' to_email_address = ' . $this->sqlToEmailAddress(). ',' ; } elseif( true == array_key_exists( 'ToEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' to_email_address = ' . $this->sqlToEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' from_email_display = ' . $this->sqlFromEmailDisplay(). ',' ; } elseif( true == array_key_exists( 'FromEmailDisplay', $this->getChangedColumns() ) ) { $strSql .= ' from_email_display = ' . $this->sqlFromEmailDisplay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reply_to_email_address = ' . $this->sqlReplyToEmailAddress(). ',' ; } elseif( true == array_key_exists( 'ReplyToEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' reply_to_email_address = ' . $this->sqlReplyToEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_message = ' . $this->sqlEmailMessage(). ',' ; } elseif( true == array_key_exists( 'EmailMessage', $this->getChangedColumns() ) ) { $strSql .= ' email_message = ' . $this->sqlEmailMessage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_subject = ' . $this->sqlEmailSubject(). ',' ; } elseif( true == array_key_exists( 'EmailSubject', $this->getChangedColumns() ) ) { $strSql .= ' email_subject = ' . $this->sqlEmailSubject() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_export_status_id = ' . $this->sqlFileExportStatusId(). ',' ; } elseif( true == array_key_exists( 'FileExportStatusId', $this->getChangedColumns() ) ) { $strSql .= ' file_export_status_id = ' . $this->sqlFileExportStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expires_on = ' . $this->sqlExpiresOn(). ',' ; } elseif( true == array_key_exists( 'ExpiresOn', $this->getChangedColumns() ) ) { $strSql .= ' expires_on = ' . $this->sqlExpiresOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'file_batch_id' => $this->getFileBatchId(),
			'to_email_address' => $this->getToEmailAddress(),
			'from_email_display' => $this->getFromEmailDisplay(),
			'reply_to_email_address' => $this->getReplyToEmailAddress(),
			'email_message' => $this->getEmailMessage(),
			'email_subject' => $this->getEmailSubject(),
			'file_export_status_id' => $this->getFileExportStatusId(),
			'expires_on' => $this->getExpiresOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>