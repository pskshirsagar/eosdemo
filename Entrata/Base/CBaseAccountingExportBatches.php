<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAccountingExportBatches
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAccountingExportBatches extends CEosPluralBase {

	/**
	 * @return CAccountingExportBatch[]
	 */
	public static function fetchAccountingExportBatches( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CAccountingExportBatch::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CAccountingExportBatch
	 */
	public static function fetchAccountingExportBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CAccountingExportBatch::class, $objDatabase );
	}

	public static function fetchAccountingExportBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'accounting_export_batches', $objDatabase );
	}

	public static function fetchAccountingExportBatchByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportBatch( sprintf( 'SELECT * FROM accounting_export_batches WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportBatchesByCid( $intCid, $objDatabase ) {
		return self::fetchAccountingExportBatches( sprintf( 'SELECT * FROM accounting_export_batches WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportBatchesByTransmissionVendorIdByCid( $intTransmissionVendorId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportBatches( sprintf( 'SELECT * FROM accounting_export_batches WHERE transmission_vendor_id = %d AND cid = %d', $intTransmissionVendorId, $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportBatchesByAccountingExportSchedulerIdByCid( $intAccountingExportSchedulerId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportBatches( sprintf( 'SELECT * FROM accounting_export_batches WHERE accounting_export_scheduler_id = %d AND cid = %d', $intAccountingExportSchedulerId, $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportBatchesByAccountingExportFileFormatTypeIdByCid( $intAccountingExportFileFormatTypeId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportBatches( sprintf( 'SELECT * FROM accounting_export_batches WHERE accounting_export_file_format_type_id = %d AND cid = %d', $intAccountingExportFileFormatTypeId, $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportBatchesByGlChartIdByCid( $intGlChartId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportBatches( sprintf( 'SELECT * FROM accounting_export_batches WHERE gl_chart_id = %d AND cid = %d', $intGlChartId, $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportBatchesByGlTreeIdByCid( $intGlTreeId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportBatches( sprintf( 'SELECT * FROM accounting_export_batches WHERE gl_tree_id = %d AND cid = %d', $intGlTreeId, $intCid ), $objDatabase );
	}

	public static function fetchAccountingExportBatchesByTaxableEntityIdByCid( $intTaxableEntityId, $intCid, $objDatabase ) {
		return self::fetchAccountingExportBatches( sprintf( 'SELECT * FROM accounting_export_batches WHERE taxable_entity_id = %d AND cid = %d', $intTaxableEntityId, $intCid ), $objDatabase );
	}

}
?>