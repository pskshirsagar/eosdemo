<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningDocumentExportRequest extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_document_export_requests';

	protected $m_intId;
	protected $m_intCid;
	protected $m_arrintPropertyIds;
	protected $m_intFileTypeId;
	protected $m_intScreeningDocumentExportRequestStatusTypeId;
	protected $m_strFromDate;
	protected $m_strToDate;
	protected $m_boolIsActive;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intScreeningDocumentExportRequestStatusTypeId = '1';
		$this->m_boolIsActive = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintPropertyIds', trim( $arrValues['property_ids'] ) ); elseif( isset( $arrValues['property_ids'] ) ) $this->setPropertyIds( $arrValues['property_ids'] );
		if( isset( $arrValues['file_type_id'] ) && $boolDirectSet ) $this->set( 'm_intFileTypeId', trim( $arrValues['file_type_id'] ) ); elseif( isset( $arrValues['file_type_id'] ) ) $this->setFileTypeId( $arrValues['file_type_id'] );
		if( isset( $arrValues['screening_document_export_request_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningDocumentExportRequestStatusTypeId', trim( $arrValues['screening_document_export_request_status_type_id'] ) ); elseif( isset( $arrValues['screening_document_export_request_status_type_id'] ) ) $this->setScreeningDocumentExportRequestStatusTypeId( $arrValues['screening_document_export_request_status_type_id'] );
		if( isset( $arrValues['from_date'] ) && $boolDirectSet ) $this->set( 'm_strFromDate', trim( $arrValues['from_date'] ) ); elseif( isset( $arrValues['from_date'] ) ) $this->setFromDate( $arrValues['from_date'] );
		if( isset( $arrValues['to_date'] ) && $boolDirectSet ) $this->set( 'm_strToDate', trim( $arrValues['to_date'] ) ); elseif( isset( $arrValues['to_date'] ) ) $this->setToDate( $arrValues['to_date'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyIds( $arrintPropertyIds ) {
		$this->set( 'm_arrintPropertyIds', CStrings::strToArrIntDef( $arrintPropertyIds, NULL ) );
	}

	public function getPropertyIds() {
		return $this->m_arrintPropertyIds;
	}

	public function sqlPropertyIds() {
		return ( true == isset( $this->m_arrintPropertyIds ) && true == valArr( $this->m_arrintPropertyIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintPropertyIds, NULL ) . '\'' : 'NULL';
	}

	public function setFileTypeId( $intFileTypeId ) {
		$this->set( 'm_intFileTypeId', CStrings::strToIntDef( $intFileTypeId, NULL, false ) );
	}

	public function getFileTypeId() {
		return $this->m_intFileTypeId;
	}

	public function sqlFileTypeId() {
		return ( true == isset( $this->m_intFileTypeId ) ) ? ( string ) $this->m_intFileTypeId : 'NULL';
	}

	public function setScreeningDocumentExportRequestStatusTypeId( $intScreeningDocumentExportRequestStatusTypeId ) {
		$this->set( 'm_intScreeningDocumentExportRequestStatusTypeId', CStrings::strToIntDef( $intScreeningDocumentExportRequestStatusTypeId, NULL, false ) );
	}

	public function getScreeningDocumentExportRequestStatusTypeId() {
		return $this->m_intScreeningDocumentExportRequestStatusTypeId;
	}

	public function sqlScreeningDocumentExportRequestStatusTypeId() {
		return ( true == isset( $this->m_intScreeningDocumentExportRequestStatusTypeId ) ) ? ( string ) $this->m_intScreeningDocumentExportRequestStatusTypeId : '1';
	}

	public function setFromDate( $strFromDate ) {
		$this->set( 'm_strFromDate', CStrings::strTrimDef( $strFromDate, -1, NULL, true ) );
	}

	public function getFromDate() {
		return $this->m_strFromDate;
	}

	public function sqlFromDate() {
		return ( true == isset( $this->m_strFromDate ) ) ? '\'' . $this->m_strFromDate . '\'' : 'NOW()';
	}

	public function setToDate( $strToDate ) {
		$this->set( 'm_strToDate', CStrings::strTrimDef( $strToDate, -1, NULL, true ) );
	}

	public function getToDate() {
		return $this->m_strToDate;
	}

	public function sqlToDate() {
		return ( true == isset( $this->m_strToDate ) ) ? '\'' . $this->m_strToDate . '\'' : 'NOW()';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_ids, file_type_id, screening_document_export_request_status_type_id, from_date, to_date, is_active, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyIds() . ', ' .
						$this->sqlFileTypeId() . ', ' .
						$this->sqlScreeningDocumentExportRequestStatusTypeId() . ', ' .
						$this->sqlFromDate() . ', ' .
						$this->sqlToDate() . ', ' .
						$this->sqlIsActive() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_ids = ' . $this->sqlPropertyIds() . ','; } elseif( true == array_key_exists( 'PropertyIds', $this->getChangedColumns() ) ) { $strSql .= ' property_ids = ' . $this->sqlPropertyIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_type_id = ' . $this->sqlFileTypeId() . ','; } elseif( true == array_key_exists( 'FileTypeId', $this->getChangedColumns() ) ) { $strSql .= ' file_type_id = ' . $this->sqlFileTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_document_export_request_status_type_id = ' . $this->sqlScreeningDocumentExportRequestStatusTypeId() . ','; } elseif( true == array_key_exists( 'ScreeningDocumentExportRequestStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_document_export_request_status_type_id = ' . $this->sqlScreeningDocumentExportRequestStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' from_date = ' . $this->sqlFromDate() . ','; } elseif( true == array_key_exists( 'FromDate', $this->getChangedColumns() ) ) { $strSql .= ' from_date = ' . $this->sqlFromDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' to_date = ' . $this->sqlToDate() . ','; } elseif( true == array_key_exists( 'ToDate', $this->getChangedColumns() ) ) { $strSql .= ' to_date = ' . $this->sqlToDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_ids' => $this->getPropertyIds(),
			'file_type_id' => $this->getFileTypeId(),
			'screening_document_export_request_status_type_id' => $this->getScreeningDocumentExportRequestStatusTypeId(),
			'from_date' => $this->getFromDate(),
			'to_date' => $this->getToDate(),
			'is_active' => $this->getIsActive(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>