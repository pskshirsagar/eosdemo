<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFormStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseFormStatusTypes extends CEosPluralBase {

	/**
	 * @return CFormStatusType[]
	 */
	public static function fetchFormStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CFormStatusType::class, $objDatabase );
	}

	/**
	 * @return CFormStatusType
	 */
	public static function fetchFormStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFormStatusType::class, $objDatabase );
	}

	public static function fetchFormStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'form_status_types', $objDatabase );
	}

	public static function fetchFormStatusTypeById( $intId, $objDatabase ) {
		return self::fetchFormStatusType( sprintf( 'SELECT * FROM form_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>