<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerGuests
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerGuests extends CEosPluralBase {

	/**
	 * @return CCustomerGuest[]
	 */
	public static function fetchCustomerGuests( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCustomerGuest::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerGuest
	 */
	public static function fetchCustomerGuest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerGuest::class, $objDatabase );
	}

	public static function fetchCustomerGuestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_guests', $objDatabase );
	}

	public static function fetchCustomerGuestByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerGuest( sprintf( 'SELECT * FROM customer_guests WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerGuestsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerGuests( sprintf( 'SELECT * FROM customer_guests WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCustomerGuestsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerGuests( sprintf( 'SELECT * FROM customer_guests WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerGuestsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchCustomerGuests( sprintf( 'SELECT * FROM customer_guests WHERE lease_id = %d AND cid = %d', $intLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerGuestsByGuestTypeIdByCid( $intGuestTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerGuests( sprintf( 'SELECT * FROM customer_guests WHERE guest_type_id = %d AND cid = %d', $intGuestTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerGuestsByGuestFrequencyTypeIdByCid( $intGuestFrequencyTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerGuests( sprintf( 'SELECT * FROM customer_guests WHERE guest_frequency_type_id = %d AND cid = %d', $intGuestFrequencyTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerGuestsByCompanyMediaFileIdByCid( $intCompanyMediaFileId, $intCid, $objDatabase ) {
		return self::fetchCustomerGuests( sprintf( 'SELECT * FROM customer_guests WHERE company_media_file_id = %d AND cid = %d', $intCompanyMediaFileId, $intCid ), $objDatabase );
	}

}
?>