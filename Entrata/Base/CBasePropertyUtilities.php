<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyUtilities
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyUtilities extends CEosPluralBase {

	/**
	 * @return CPropertyUtility[]
	 */
	public static function fetchPropertyUtilities( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPropertyUtility::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyUtility
	 */
	public static function fetchPropertyUtility( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyUtility::class, $objDatabase );
	}

	public static function fetchPropertyUtilityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_utilities', $objDatabase );
	}

	public static function fetchPropertyUtilityByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyUtility( sprintf( 'SELECT * FROM property_utilities WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyUtilitiesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyUtilities( sprintf( 'SELECT * FROM property_utilities WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchPropertyUtilitiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyUtilities( sprintf( 'SELECT * FROM property_utilities WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyUtilitiesByArCascadeIdByCid( $intArCascadeId, $intCid, $objDatabase ) {
		return self::fetchPropertyUtilities( sprintf( 'SELECT * FROM property_utilities WHERE ar_cascade_id = %d AND cid = %d', $intArCascadeId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyUtilitiesByArCascadeReferenceIdByCid( $intArCascadeReferenceId, $intCid, $objDatabase ) {
		return self::fetchPropertyUtilities( sprintf( 'SELECT * FROM property_utilities WHERE ar_cascade_reference_id = %d AND cid = %d', $intArCascadeReferenceId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyUtilitiesByUtilityIdByCid( $intUtilityId, $intCid, $objDatabase ) {
		return self::fetchPropertyUtilities( sprintf( 'SELECT * FROM property_utilities WHERE utility_id = %d AND cid = %d', $intUtilityId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyUtilitiesByUtilityFormulaIdByCid( $intUtilityFormulaId, $intCid, $objDatabase ) {
		return self::fetchPropertyUtilities( sprintf( 'SELECT * FROM property_utilities WHERE utility_formula_id = %d AND cid = %d', $intUtilityFormulaId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyUtilitiesByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objDatabase ) {
		return self::fetchPropertyUtilities( sprintf( 'SELECT * FROM property_utilities WHERE ap_payee_location_id = %d AND cid = %d', $intApPayeeLocationId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyUtilitiesByAmenityIdByCid( $intAmenityId, $intCid, $objDatabase ) {
		return self::fetchPropertyUtilities( sprintf( 'SELECT * FROM property_utilities WHERE amenity_id = %d AND cid = %d', $intAmenityId, $intCid ), $objDatabase );
	}

}
?>