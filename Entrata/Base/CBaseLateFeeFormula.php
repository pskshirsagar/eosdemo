<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLateFeeFormula extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.late_fee_formulas';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDefaultLateFeeFormulaId;
	protected $m_intParentLateFeeFormulaId;
	protected $m_intFirstDayArCodeId;
	protected $m_intDailyArCodeId;
	protected $m_intLegalFeeChargeCodeId;
	protected $m_intOccupancyTypeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_fltFixedAmount;
	protected $m_fltAmountPerDay;
	protected $m_fltFixedPercent;
	protected $m_fltPercentPerDay;
	protected $m_intFirstLateDay;
	protected $m_intDailyLateFeeStartDay;
	protected $m_fltMinimumBalanceDue;
	protected $m_fltMaxMonthlyLateFeePercent;
	protected $m_fltMaxMonthlyLateFeeAmount;
	protected $m_fltLegalFeeAmount;
	protected $m_boolScheduledChargesOnly;
	protected $m_boolBaseChargeOnAmountDue;
	protected $m_boolAutoPostNsfLateFees;
	protected $m_boolPostFeesToNextPeriod;
	protected $m_boolIncludePastPeriodTransactions;
	protected $m_boolAllowToApplyLegalFee;
	protected $m_boolIsSystem;
	protected $m_intDailyLateFeeEndDay;
	protected $m_boolIncludeFutureLeases;
	protected $m_boolChargeOnPaymentPostDate;
	protected $m_boolChargeLateFeesOnHolidays;
	protected $m_boolChargeLateFeesOnSaturdays;
	protected $m_boolChargeLateFeesOnSundays;
	protected $m_boolIsDisabled;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intGraceDays;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intFirstDayLateFeeCalculationMethodId;
	protected $m_intDailyLateFeeCalculationMethodId;
	protected $m_fltFixedPercentThreshold;
	protected $m_fltDailyPercentThreshold;
	protected $m_intFirstDayLateFeeCalculationTypeReferenceId;
	protected $m_intDailyLateFeeCalculationTypeReferenceId;
	protected $m_intMonthlyMaxCalculationTypeId;
	protected $m_intMonthlyMaxCalculationTypeReferenceId;
	protected $m_intBalanceThresholdCalculationTypeId;
	protected $m_intBalanceThresholdCalculationTypeReferenceId;
	protected $m_intSubsidyTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_intOccupancyTypeId = '1';
		$this->m_fltFixedAmount = '0';
		$this->m_fltAmountPerDay = '0';
		$this->m_fltFixedPercent = '0';
		$this->m_fltPercentPerDay = '0';
		$this->m_intFirstLateDay = '5';
		$this->m_fltMinimumBalanceDue = '0';
		$this->m_boolScheduledChargesOnly = false;
		$this->m_boolBaseChargeOnAmountDue = true;
		$this->m_boolAutoPostNsfLateFees = true;
		$this->m_boolPostFeesToNextPeriod = false;
		$this->m_boolIncludePastPeriodTransactions = false;
		$this->m_boolAllowToApplyLegalFee = false;
		$this->m_boolIsSystem = false;
		$this->m_intDailyLateFeeEndDay = '31';
		$this->m_boolIncludeFutureLeases = false;
		$this->m_boolChargeOnPaymentPostDate = true;
		$this->m_boolChargeLateFeesOnHolidays = true;
		$this->m_boolChargeLateFeesOnSaturdays = true;
		$this->m_boolChargeLateFeesOnSundays = true;
		$this->m_boolIsDisabled = false;
		$this->m_intGraceDays = '1';
		$this->m_intFirstDayLateFeeCalculationMethodId = '1';
		$this->m_intDailyLateFeeCalculationMethodId = '1';
		$this->m_fltFixedPercentThreshold = '0';
		$this->m_fltDailyPercentThreshold = '0';
		$this->m_intBalanceThresholdCalculationTypeId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['default_late_fee_formula_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultLateFeeFormulaId', trim( $arrValues['default_late_fee_formula_id'] ) ); elseif( isset( $arrValues['default_late_fee_formula_id'] ) ) $this->setDefaultLateFeeFormulaId( $arrValues['default_late_fee_formula_id'] );
		if( isset( $arrValues['parent_late_fee_formula_id'] ) && $boolDirectSet ) $this->set( 'm_intParentLateFeeFormulaId', trim( $arrValues['parent_late_fee_formula_id'] ) ); elseif( isset( $arrValues['parent_late_fee_formula_id'] ) ) $this->setParentLateFeeFormulaId( $arrValues['parent_late_fee_formula_id'] );
		if( isset( $arrValues['first_day_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intFirstDayArCodeId', trim( $arrValues['first_day_ar_code_id'] ) ); elseif( isset( $arrValues['first_day_ar_code_id'] ) ) $this->setFirstDayArCodeId( $arrValues['first_day_ar_code_id'] );
		if( isset( $arrValues['daily_ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intDailyArCodeId', trim( $arrValues['daily_ar_code_id'] ) ); elseif( isset( $arrValues['daily_ar_code_id'] ) ) $this->setDailyArCodeId( $arrValues['daily_ar_code_id'] );
		if( isset( $arrValues['legal_fee_charge_code_id'] ) && $boolDirectSet ) $this->set( 'm_intLegalFeeChargeCodeId', trim( $arrValues['legal_fee_charge_code_id'] ) ); elseif( isset( $arrValues['legal_fee_charge_code_id'] ) ) $this->setLegalFeeChargeCodeId( $arrValues['legal_fee_charge_code_id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['fixed_amount'] ) && $boolDirectSet ) $this->set( 'm_fltFixedAmount', trim( $arrValues['fixed_amount'] ) ); elseif( isset( $arrValues['fixed_amount'] ) ) $this->setFixedAmount( $arrValues['fixed_amount'] );
		if( isset( $arrValues['amount_per_day'] ) && $boolDirectSet ) $this->set( 'm_fltAmountPerDay', trim( $arrValues['amount_per_day'] ) ); elseif( isset( $arrValues['amount_per_day'] ) ) $this->setAmountPerDay( $arrValues['amount_per_day'] );
		if( isset( $arrValues['fixed_percent'] ) && $boolDirectSet ) $this->set( 'm_fltFixedPercent', trim( $arrValues['fixed_percent'] ) ); elseif( isset( $arrValues['fixed_percent'] ) ) $this->setFixedPercent( $arrValues['fixed_percent'] );
		if( isset( $arrValues['percent_per_day'] ) && $boolDirectSet ) $this->set( 'm_fltPercentPerDay', trim( $arrValues['percent_per_day'] ) ); elseif( isset( $arrValues['percent_per_day'] ) ) $this->setPercentPerDay( $arrValues['percent_per_day'] );
		if( isset( $arrValues['first_late_day'] ) && $boolDirectSet ) $this->set( 'm_intFirstLateDay', trim( $arrValues['first_late_day'] ) ); elseif( isset( $arrValues['first_late_day'] ) ) $this->setFirstLateDay( $arrValues['first_late_day'] );
		if( isset( $arrValues['daily_late_fee_start_day'] ) && $boolDirectSet ) $this->set( 'm_intDailyLateFeeStartDay', trim( $arrValues['daily_late_fee_start_day'] ) ); elseif( isset( $arrValues['daily_late_fee_start_day'] ) ) $this->setDailyLateFeeStartDay( $arrValues['daily_late_fee_start_day'] );
		if( isset( $arrValues['minimum_balance_due'] ) && $boolDirectSet ) $this->set( 'm_fltMinimumBalanceDue', trim( $arrValues['minimum_balance_due'] ) ); elseif( isset( $arrValues['minimum_balance_due'] ) ) $this->setMinimumBalanceDue( $arrValues['minimum_balance_due'] );
		if( isset( $arrValues['max_monthly_late_fee_percent'] ) && $boolDirectSet ) $this->set( 'm_fltMaxMonthlyLateFeePercent', trim( $arrValues['max_monthly_late_fee_percent'] ) ); elseif( isset( $arrValues['max_monthly_late_fee_percent'] ) ) $this->setMaxMonthlyLateFeePercent( $arrValues['max_monthly_late_fee_percent'] );
		if( isset( $arrValues['max_monthly_late_fee_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMaxMonthlyLateFeeAmount', trim( $arrValues['max_monthly_late_fee_amount'] ) ); elseif( isset( $arrValues['max_monthly_late_fee_amount'] ) ) $this->setMaxMonthlyLateFeeAmount( $arrValues['max_monthly_late_fee_amount'] );
		if( isset( $arrValues['legal_fee_amount'] ) && $boolDirectSet ) $this->set( 'm_fltLegalFeeAmount', trim( $arrValues['legal_fee_amount'] ) ); elseif( isset( $arrValues['legal_fee_amount'] ) ) $this->setLegalFeeAmount( $arrValues['legal_fee_amount'] );
		if( isset( $arrValues['scheduled_charges_only'] ) && $boolDirectSet ) $this->set( 'm_boolScheduledChargesOnly', trim( stripcslashes( $arrValues['scheduled_charges_only'] ) ) ); elseif( isset( $arrValues['scheduled_charges_only'] ) ) $this->setScheduledChargesOnly( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['scheduled_charges_only'] ) : $arrValues['scheduled_charges_only'] );
		if( isset( $arrValues['base_charge_on_amount_due'] ) && $boolDirectSet ) $this->set( 'm_boolBaseChargeOnAmountDue', trim( stripcslashes( $arrValues['base_charge_on_amount_due'] ) ) ); elseif( isset( $arrValues['base_charge_on_amount_due'] ) ) $this->setBaseChargeOnAmountDue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['base_charge_on_amount_due'] ) : $arrValues['base_charge_on_amount_due'] );
		if( isset( $arrValues['auto_post_nsf_late_fees'] ) && $boolDirectSet ) $this->set( 'm_boolAutoPostNsfLateFees', trim( stripcslashes( $arrValues['auto_post_nsf_late_fees'] ) ) ); elseif( isset( $arrValues['auto_post_nsf_late_fees'] ) ) $this->setAutoPostNsfLateFees( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['auto_post_nsf_late_fees'] ) : $arrValues['auto_post_nsf_late_fees'] );
		if( isset( $arrValues['post_fees_to_next_period'] ) && $boolDirectSet ) $this->set( 'm_boolPostFeesToNextPeriod', trim( stripcslashes( $arrValues['post_fees_to_next_period'] ) ) ); elseif( isset( $arrValues['post_fees_to_next_period'] ) ) $this->setPostFeesToNextPeriod( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['post_fees_to_next_period'] ) : $arrValues['post_fees_to_next_period'] );
		if( isset( $arrValues['include_past_period_transactions'] ) && $boolDirectSet ) $this->set( 'm_boolIncludePastPeriodTransactions', trim( stripcslashes( $arrValues['include_past_period_transactions'] ) ) ); elseif( isset( $arrValues['include_past_period_transactions'] ) ) $this->setIncludePastPeriodTransactions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_past_period_transactions'] ) : $arrValues['include_past_period_transactions'] );
		if( isset( $arrValues['allow_to_apply_legal_fee'] ) && $boolDirectSet ) $this->set( 'm_boolAllowToApplyLegalFee', trim( stripcslashes( $arrValues['allow_to_apply_legal_fee'] ) ) ); elseif( isset( $arrValues['allow_to_apply_legal_fee'] ) ) $this->setAllowToApplyLegalFee( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_to_apply_legal_fee'] ) : $arrValues['allow_to_apply_legal_fee'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_boolIsSystem', trim( stripcslashes( $arrValues['is_system'] ) ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_system'] ) : $arrValues['is_system'] );
		if( isset( $arrValues['daily_late_fee_end_day'] ) && $boolDirectSet ) $this->set( 'm_intDailyLateFeeEndDay', trim( $arrValues['daily_late_fee_end_day'] ) ); elseif( isset( $arrValues['daily_late_fee_end_day'] ) ) $this->setDailyLateFeeEndDay( $arrValues['daily_late_fee_end_day'] );
		if( isset( $arrValues['include_future_leases'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeFutureLeases', trim( stripcslashes( $arrValues['include_future_leases'] ) ) ); elseif( isset( $arrValues['include_future_leases'] ) ) $this->setIncludeFutureLeases( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_future_leases'] ) : $arrValues['include_future_leases'] );
		if( isset( $arrValues['charge_on_payment_post_date'] ) && $boolDirectSet ) $this->set( 'm_boolChargeOnPaymentPostDate', trim( stripcslashes( $arrValues['charge_on_payment_post_date'] ) ) ); elseif( isset( $arrValues['charge_on_payment_post_date'] ) ) $this->setChargeOnPaymentPostDate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['charge_on_payment_post_date'] ) : $arrValues['charge_on_payment_post_date'] );
		if( isset( $arrValues['charge_late_fees_on_holidays'] ) && $boolDirectSet ) $this->set( 'm_boolChargeLateFeesOnHolidays', trim( stripcslashes( $arrValues['charge_late_fees_on_holidays'] ) ) ); elseif( isset( $arrValues['charge_late_fees_on_holidays'] ) ) $this->setChargeLateFeesOnHolidays( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['charge_late_fees_on_holidays'] ) : $arrValues['charge_late_fees_on_holidays'] );
		if( isset( $arrValues['charge_late_fees_on_saturdays'] ) && $boolDirectSet ) $this->set( 'm_boolChargeLateFeesOnSaturdays', trim( stripcslashes( $arrValues['charge_late_fees_on_saturdays'] ) ) ); elseif( isset( $arrValues['charge_late_fees_on_saturdays'] ) ) $this->setChargeLateFeesOnSaturdays( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['charge_late_fees_on_saturdays'] ) : $arrValues['charge_late_fees_on_saturdays'] );
		if( isset( $arrValues['charge_late_fees_on_sundays'] ) && $boolDirectSet ) $this->set( 'm_boolChargeLateFeesOnSundays', trim( stripcslashes( $arrValues['charge_late_fees_on_sundays'] ) ) ); elseif( isset( $arrValues['charge_late_fees_on_sundays'] ) ) $this->setChargeLateFeesOnSundays( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['charge_late_fees_on_sundays'] ) : $arrValues['charge_late_fees_on_sundays'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['grace_days'] ) && $boolDirectSet ) $this->set( 'm_intGraceDays', trim( $arrValues['grace_days'] ) ); elseif( isset( $arrValues['grace_days'] ) ) $this->setGraceDays( $arrValues['grace_days'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['first_day_late_fee_calculation_method_id'] ) && $boolDirectSet ) $this->set( 'm_intFirstDayLateFeeCalculationMethodId', trim( $arrValues['first_day_late_fee_calculation_method_id'] ) ); elseif( isset( $arrValues['first_day_late_fee_calculation_method_id'] ) ) $this->setFirstDayLateFeeCalculationMethodId( $arrValues['first_day_late_fee_calculation_method_id'] );
		if( isset( $arrValues['daily_late_fee_calculation_method_id'] ) && $boolDirectSet ) $this->set( 'm_intDailyLateFeeCalculationMethodId', trim( $arrValues['daily_late_fee_calculation_method_id'] ) ); elseif( isset( $arrValues['daily_late_fee_calculation_method_id'] ) ) $this->setDailyLateFeeCalculationMethodId( $arrValues['daily_late_fee_calculation_method_id'] );
		if( isset( $arrValues['fixed_percent_threshold'] ) && $boolDirectSet ) $this->set( 'm_fltFixedPercentThreshold', trim( $arrValues['fixed_percent_threshold'] ) ); elseif( isset( $arrValues['fixed_percent_threshold'] ) ) $this->setFixedPercentThreshold( $arrValues['fixed_percent_threshold'] );
		if( isset( $arrValues['daily_percent_threshold'] ) && $boolDirectSet ) $this->set( 'm_fltDailyPercentThreshold', trim( $arrValues['daily_percent_threshold'] ) ); elseif( isset( $arrValues['daily_percent_threshold'] ) ) $this->setDailyPercentThreshold( $arrValues['daily_percent_threshold'] );
		if( isset( $arrValues['first_day_late_fee_calculation_type_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intFirstDayLateFeeCalculationTypeReferenceId', trim( $arrValues['first_day_late_fee_calculation_type_reference_id'] ) ); elseif( isset( $arrValues['first_day_late_fee_calculation_type_reference_id'] ) ) $this->setFirstDayLateFeeCalculationTypeReferenceId( $arrValues['first_day_late_fee_calculation_type_reference_id'] );
		if( isset( $arrValues['daily_late_fee_calculation_type_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intDailyLateFeeCalculationTypeReferenceId', trim( $arrValues['daily_late_fee_calculation_type_reference_id'] ) ); elseif( isset( $arrValues['daily_late_fee_calculation_type_reference_id'] ) ) $this->setDailyLateFeeCalculationTypeReferenceId( $arrValues['daily_late_fee_calculation_type_reference_id'] );
		if( isset( $arrValues['monthly_max_calculation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMonthlyMaxCalculationTypeId', trim( $arrValues['monthly_max_calculation_type_id'] ) ); elseif( isset( $arrValues['monthly_max_calculation_type_id'] ) ) $this->setMonthlyMaxCalculationTypeId( $arrValues['monthly_max_calculation_type_id'] );
		if( isset( $arrValues['monthly_max_calculation_type_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intMonthlyMaxCalculationTypeReferenceId', trim( $arrValues['monthly_max_calculation_type_reference_id'] ) ); elseif( isset( $arrValues['monthly_max_calculation_type_reference_id'] ) ) $this->setMonthlyMaxCalculationTypeReferenceId( $arrValues['monthly_max_calculation_type_reference_id'] );
		if( isset( $arrValues['balance_threshold_calculation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBalanceThresholdCalculationTypeId', trim( $arrValues['balance_threshold_calculation_type_id'] ) ); elseif( isset( $arrValues['balance_threshold_calculation_type_id'] ) ) $this->setBalanceThresholdCalculationTypeId( $arrValues['balance_threshold_calculation_type_id'] );
		if( isset( $arrValues['balance_threshold_calculation_type_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intBalanceThresholdCalculationTypeReferenceId', trim( $arrValues['balance_threshold_calculation_type_reference_id'] ) ); elseif( isset( $arrValues['balance_threshold_calculation_type_reference_id'] ) ) $this->setBalanceThresholdCalculationTypeReferenceId( $arrValues['balance_threshold_calculation_type_reference_id'] );
		if( isset( $arrValues['subsidy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubsidyTypeId', trim( $arrValues['subsidy_type_id'] ) ); elseif( isset( $arrValues['subsidy_type_id'] ) ) $this->setSubsidyTypeId( $arrValues['subsidy_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDefaultLateFeeFormulaId( $intDefaultLateFeeFormulaId ) {
		$this->set( 'm_intDefaultLateFeeFormulaId', CStrings::strToIntDef( $intDefaultLateFeeFormulaId, NULL, false ) );
	}

	public function getDefaultLateFeeFormulaId() {
		return $this->m_intDefaultLateFeeFormulaId;
	}

	public function sqlDefaultLateFeeFormulaId() {
		return ( true == isset( $this->m_intDefaultLateFeeFormulaId ) ) ? ( string ) $this->m_intDefaultLateFeeFormulaId : 'NULL';
	}

	public function setParentLateFeeFormulaId( $intParentLateFeeFormulaId ) {
		$this->set( 'm_intParentLateFeeFormulaId', CStrings::strToIntDef( $intParentLateFeeFormulaId, NULL, false ) );
	}

	public function getParentLateFeeFormulaId() {
		return $this->m_intParentLateFeeFormulaId;
	}

	public function sqlParentLateFeeFormulaId() {
		return ( true == isset( $this->m_intParentLateFeeFormulaId ) ) ? ( string ) $this->m_intParentLateFeeFormulaId : 'NULL';
	}

	public function setFirstDayArCodeId( $intFirstDayArCodeId ) {
		$this->set( 'm_intFirstDayArCodeId', CStrings::strToIntDef( $intFirstDayArCodeId, NULL, false ) );
	}

	public function getFirstDayArCodeId() {
		return $this->m_intFirstDayArCodeId;
	}

	public function sqlFirstDayArCodeId() {
		return ( true == isset( $this->m_intFirstDayArCodeId ) ) ? ( string ) $this->m_intFirstDayArCodeId : 'NULL';
	}

	public function setDailyArCodeId( $intDailyArCodeId ) {
		$this->set( 'm_intDailyArCodeId', CStrings::strToIntDef( $intDailyArCodeId, NULL, false ) );
	}

	public function getDailyArCodeId() {
		return $this->m_intDailyArCodeId;
	}

	public function sqlDailyArCodeId() {
		return ( true == isset( $this->m_intDailyArCodeId ) ) ? ( string ) $this->m_intDailyArCodeId : 'NULL';
	}

	public function setLegalFeeChargeCodeId( $intLegalFeeChargeCodeId ) {
		$this->set( 'm_intLegalFeeChargeCodeId', CStrings::strToIntDef( $intLegalFeeChargeCodeId, NULL, false ) );
	}

	public function getLegalFeeChargeCodeId() {
		return $this->m_intLegalFeeChargeCodeId;
	}

	public function sqlLegalFeeChargeCodeId() {
		return ( true == isset( $this->m_intLegalFeeChargeCodeId ) ) ? ( string ) $this->m_intLegalFeeChargeCodeId : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : '1';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setFixedAmount( $fltFixedAmount ) {
		$this->set( 'm_fltFixedAmount', CStrings::strToFloatDef( $fltFixedAmount, NULL, false, 2 ) );
	}

	public function getFixedAmount() {
		return $this->m_fltFixedAmount;
	}

	public function sqlFixedAmount() {
		return ( true == isset( $this->m_fltFixedAmount ) ) ? ( string ) $this->m_fltFixedAmount : '0';
	}

	public function setAmountPerDay( $fltAmountPerDay ) {
		$this->set( 'm_fltAmountPerDay', CStrings::strToFloatDef( $fltAmountPerDay, NULL, false, 2 ) );
	}

	public function getAmountPerDay() {
		return $this->m_fltAmountPerDay;
	}

	public function sqlAmountPerDay() {
		return ( true == isset( $this->m_fltAmountPerDay ) ) ? ( string ) $this->m_fltAmountPerDay : '0';
	}

	public function setFixedPercent( $fltFixedPercent ) {
		$this->set( 'm_fltFixedPercent', CStrings::strToFloatDef( $fltFixedPercent, NULL, false, 6 ) );
	}

	public function getFixedPercent() {
		return $this->m_fltFixedPercent;
	}

	public function sqlFixedPercent() {
		return ( true == isset( $this->m_fltFixedPercent ) ) ? ( string ) $this->m_fltFixedPercent : '0';
	}

	public function setPercentPerDay( $fltPercentPerDay ) {
		$this->set( 'm_fltPercentPerDay', CStrings::strToFloatDef( $fltPercentPerDay, NULL, false, 6 ) );
	}

	public function getPercentPerDay() {
		return $this->m_fltPercentPerDay;
	}

	public function sqlPercentPerDay() {
		return ( true == isset( $this->m_fltPercentPerDay ) ) ? ( string ) $this->m_fltPercentPerDay : '0';
	}

	public function setFirstLateDay( $intFirstLateDay ) {
		$this->set( 'm_intFirstLateDay', CStrings::strToIntDef( $intFirstLateDay, NULL, false ) );
	}

	public function getFirstLateDay() {
		return $this->m_intFirstLateDay;
	}

	public function sqlFirstLateDay() {
		return ( true == isset( $this->m_intFirstLateDay ) ) ? ( string ) $this->m_intFirstLateDay : '5';
	}

	public function setDailyLateFeeStartDay( $intDailyLateFeeStartDay ) {
		$this->set( 'm_intDailyLateFeeStartDay', CStrings::strToIntDef( $intDailyLateFeeStartDay, NULL, false ) );
	}

	public function getDailyLateFeeStartDay() {
		return $this->m_intDailyLateFeeStartDay;
	}

	public function sqlDailyLateFeeStartDay() {
		return ( true == isset( $this->m_intDailyLateFeeStartDay ) ) ? ( string ) $this->m_intDailyLateFeeStartDay : 'NULL';
	}

	public function setMinimumBalanceDue( $fltMinimumBalanceDue ) {
		$this->set( 'm_fltMinimumBalanceDue', CStrings::strToFloatDef( $fltMinimumBalanceDue, NULL, false, 2 ) );
	}

	public function getMinimumBalanceDue() {
		return $this->m_fltMinimumBalanceDue;
	}

	public function sqlMinimumBalanceDue() {
		return ( true == isset( $this->m_fltMinimumBalanceDue ) ) ? ( string ) $this->m_fltMinimumBalanceDue : '0';
	}

	public function setMaxMonthlyLateFeePercent( $fltMaxMonthlyLateFeePercent ) {
		$this->set( 'm_fltMaxMonthlyLateFeePercent', CStrings::strToFloatDef( $fltMaxMonthlyLateFeePercent, NULL, false, 2 ) );
	}

	public function getMaxMonthlyLateFeePercent() {
		return $this->m_fltMaxMonthlyLateFeePercent;
	}

	public function sqlMaxMonthlyLateFeePercent() {
		return ( true == isset( $this->m_fltMaxMonthlyLateFeePercent ) ) ? ( string ) $this->m_fltMaxMonthlyLateFeePercent : 'NULL';
	}

	public function setMaxMonthlyLateFeeAmount( $fltMaxMonthlyLateFeeAmount ) {
		$this->set( 'm_fltMaxMonthlyLateFeeAmount', CStrings::strToFloatDef( $fltMaxMonthlyLateFeeAmount, NULL, false, 2 ) );
	}

	public function getMaxMonthlyLateFeeAmount() {
		return $this->m_fltMaxMonthlyLateFeeAmount;
	}

	public function sqlMaxMonthlyLateFeeAmount() {
		return ( true == isset( $this->m_fltMaxMonthlyLateFeeAmount ) ) ? ( string ) $this->m_fltMaxMonthlyLateFeeAmount : 'NULL';
	}

	public function setLegalFeeAmount( $fltLegalFeeAmount ) {
		$this->set( 'm_fltLegalFeeAmount', CStrings::strToFloatDef( $fltLegalFeeAmount, NULL, false, 2 ) );
	}

	public function getLegalFeeAmount() {
		return $this->m_fltLegalFeeAmount;
	}

	public function sqlLegalFeeAmount() {
		return ( true == isset( $this->m_fltLegalFeeAmount ) ) ? ( string ) $this->m_fltLegalFeeAmount : 'NULL';
	}

	public function setScheduledChargesOnly( $boolScheduledChargesOnly ) {
		$this->set( 'm_boolScheduledChargesOnly', CStrings::strToBool( $boolScheduledChargesOnly ) );
	}

	public function getScheduledChargesOnly() {
		return $this->m_boolScheduledChargesOnly;
	}

	public function sqlScheduledChargesOnly() {
		return ( true == isset( $this->m_boolScheduledChargesOnly ) ) ? '\'' . ( true == ( bool ) $this->m_boolScheduledChargesOnly ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setBaseChargeOnAmountDue( $boolBaseChargeOnAmountDue ) {
		$this->set( 'm_boolBaseChargeOnAmountDue', CStrings::strToBool( $boolBaseChargeOnAmountDue ) );
	}

	public function getBaseChargeOnAmountDue() {
		return $this->m_boolBaseChargeOnAmountDue;
	}

	public function sqlBaseChargeOnAmountDue() {
		return ( true == isset( $this->m_boolBaseChargeOnAmountDue ) ) ? '\'' . ( true == ( bool ) $this->m_boolBaseChargeOnAmountDue ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAutoPostNsfLateFees( $boolAutoPostNsfLateFees ) {
		$this->set( 'm_boolAutoPostNsfLateFees', CStrings::strToBool( $boolAutoPostNsfLateFees ) );
	}

	public function getAutoPostNsfLateFees() {
		return $this->m_boolAutoPostNsfLateFees;
	}

	public function sqlAutoPostNsfLateFees() {
		return ( true == isset( $this->m_boolAutoPostNsfLateFees ) ) ? '\'' . ( true == ( bool ) $this->m_boolAutoPostNsfLateFees ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPostFeesToNextPeriod( $boolPostFeesToNextPeriod ) {
		$this->set( 'm_boolPostFeesToNextPeriod', CStrings::strToBool( $boolPostFeesToNextPeriod ) );
	}

	public function getPostFeesToNextPeriod() {
		return $this->m_boolPostFeesToNextPeriod;
	}

	public function sqlPostFeesToNextPeriod() {
		return ( true == isset( $this->m_boolPostFeesToNextPeriod ) ) ? '\'' . ( true == ( bool ) $this->m_boolPostFeesToNextPeriod ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIncludePastPeriodTransactions( $boolIncludePastPeriodTransactions ) {
		$this->set( 'm_boolIncludePastPeriodTransactions', CStrings::strToBool( $boolIncludePastPeriodTransactions ) );
	}

	public function getIncludePastPeriodTransactions() {
		return $this->m_boolIncludePastPeriodTransactions;
	}

	public function sqlIncludePastPeriodTransactions() {
		return ( true == isset( $this->m_boolIncludePastPeriodTransactions ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludePastPeriodTransactions ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowToApplyLegalFee( $boolAllowToApplyLegalFee ) {
		$this->set( 'm_boolAllowToApplyLegalFee', CStrings::strToBool( $boolAllowToApplyLegalFee ) );
	}

	public function getAllowToApplyLegalFee() {
		return $this->m_boolAllowToApplyLegalFee;
	}

	public function sqlAllowToApplyLegalFee() {
		return ( true == isset( $this->m_boolAllowToApplyLegalFee ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowToApplyLegalFee ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSystem( $boolIsSystem ) {
		$this->set( 'm_boolIsSystem', CStrings::strToBool( $boolIsSystem ) );
	}

	public function getIsSystem() {
		return $this->m_boolIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_boolIsSystem ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSystem ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDailyLateFeeEndDay( $intDailyLateFeeEndDay ) {
		$this->set( 'm_intDailyLateFeeEndDay', CStrings::strToIntDef( $intDailyLateFeeEndDay, NULL, false ) );
	}

	public function getDailyLateFeeEndDay() {
		return $this->m_intDailyLateFeeEndDay;
	}

	public function sqlDailyLateFeeEndDay() {
		return ( true == isset( $this->m_intDailyLateFeeEndDay ) ) ? ( string ) $this->m_intDailyLateFeeEndDay : '31';
	}

	public function setIncludeFutureLeases( $boolIncludeFutureLeases ) {
		$this->set( 'm_boolIncludeFutureLeases', CStrings::strToBool( $boolIncludeFutureLeases ) );
	}

	public function getIncludeFutureLeases() {
		return $this->m_boolIncludeFutureLeases;
	}

	public function sqlIncludeFutureLeases() {
		return ( true == isset( $this->m_boolIncludeFutureLeases ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeFutureLeases ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setChargeOnPaymentPostDate( $boolChargeOnPaymentPostDate ) {
		$this->set( 'm_boolChargeOnPaymentPostDate', CStrings::strToBool( $boolChargeOnPaymentPostDate ) );
	}

	public function getChargeOnPaymentPostDate() {
		return $this->m_boolChargeOnPaymentPostDate;
	}

	public function sqlChargeOnPaymentPostDate() {
		return ( true == isset( $this->m_boolChargeOnPaymentPostDate ) ) ? '\'' . ( true == ( bool ) $this->m_boolChargeOnPaymentPostDate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setChargeLateFeesOnHolidays( $boolChargeLateFeesOnHolidays ) {
		$this->set( 'm_boolChargeLateFeesOnHolidays', CStrings::strToBool( $boolChargeLateFeesOnHolidays ) );
	}

	public function getChargeLateFeesOnHolidays() {
		return $this->m_boolChargeLateFeesOnHolidays;
	}

	public function sqlChargeLateFeesOnHolidays() {
		return ( true == isset( $this->m_boolChargeLateFeesOnHolidays ) ) ? '\'' . ( true == ( bool ) $this->m_boolChargeLateFeesOnHolidays ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setChargeLateFeesOnSaturdays( $boolChargeLateFeesOnSaturdays ) {
		$this->set( 'm_boolChargeLateFeesOnSaturdays', CStrings::strToBool( $boolChargeLateFeesOnSaturdays ) );
	}

	public function getChargeLateFeesOnSaturdays() {
		return $this->m_boolChargeLateFeesOnSaturdays;
	}

	public function sqlChargeLateFeesOnSaturdays() {
		return ( true == isset( $this->m_boolChargeLateFeesOnSaturdays ) ) ? '\'' . ( true == ( bool ) $this->m_boolChargeLateFeesOnSaturdays ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setChargeLateFeesOnSundays( $boolChargeLateFeesOnSundays ) {
		$this->set( 'm_boolChargeLateFeesOnSundays', CStrings::strToBool( $boolChargeLateFeesOnSundays ) );
	}

	public function getChargeLateFeesOnSundays() {
		return $this->m_boolChargeLateFeesOnSundays;
	}

	public function sqlChargeLateFeesOnSundays() {
		return ( true == isset( $this->m_boolChargeLateFeesOnSundays ) ) ? '\'' . ( true == ( bool ) $this->m_boolChargeLateFeesOnSundays ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setGraceDays( $intGraceDays ) {
		$this->set( 'm_intGraceDays', CStrings::strToIntDef( $intGraceDays, NULL, false ) );
	}

	public function getGraceDays() {
		return $this->m_intGraceDays;
	}

	public function sqlGraceDays() {
		return ( true == isset( $this->m_intGraceDays ) ) ? ( string ) $this->m_intGraceDays : '1';
	}

	public function setFirstDayLateFeeCalculationMethodId( $intFirstDayLateFeeCalculationMethodId ) {
		$this->set( 'm_intFirstDayLateFeeCalculationMethodId', CStrings::strToIntDef( $intFirstDayLateFeeCalculationMethodId, NULL, false ) );
	}

	public function getFirstDayLateFeeCalculationMethodId() {
		return $this->m_intFirstDayLateFeeCalculationMethodId;
	}

	public function sqlFirstDayLateFeeCalculationMethodId() {
		return ( true == isset( $this->m_intFirstDayLateFeeCalculationMethodId ) ) ? ( string ) $this->m_intFirstDayLateFeeCalculationMethodId : '1';
	}

	public function setDailyLateFeeCalculationMethodId( $intDailyLateFeeCalculationMethodId ) {
		$this->set( 'm_intDailyLateFeeCalculationMethodId', CStrings::strToIntDef( $intDailyLateFeeCalculationMethodId, NULL, false ) );
	}

	public function getDailyLateFeeCalculationMethodId() {
		return $this->m_intDailyLateFeeCalculationMethodId;
	}

	public function sqlDailyLateFeeCalculationMethodId() {
		return ( true == isset( $this->m_intDailyLateFeeCalculationMethodId ) ) ? ( string ) $this->m_intDailyLateFeeCalculationMethodId : '1';
	}

	public function setFixedPercentThreshold( $fltFixedPercentThreshold ) {
		$this->set( 'm_fltFixedPercentThreshold', CStrings::strToFloatDef( $fltFixedPercentThreshold, NULL, false, 2 ) );
	}

	public function getFixedPercentThreshold() {
		return $this->m_fltFixedPercentThreshold;
	}

	public function sqlFixedPercentThreshold() {
		return ( true == isset( $this->m_fltFixedPercentThreshold ) ) ? ( string ) $this->m_fltFixedPercentThreshold : '0';
	}

	public function setDailyPercentThreshold( $fltDailyPercentThreshold ) {
		$this->set( 'm_fltDailyPercentThreshold', CStrings::strToFloatDef( $fltDailyPercentThreshold, NULL, false, 2 ) );
	}

	public function getDailyPercentThreshold() {
		return $this->m_fltDailyPercentThreshold;
	}

	public function sqlDailyPercentThreshold() {
		return ( true == isset( $this->m_fltDailyPercentThreshold ) ) ? ( string ) $this->m_fltDailyPercentThreshold : '0';
	}

	public function setFirstDayLateFeeCalculationTypeReferenceId( $intFirstDayLateFeeCalculationTypeReferenceId ) {
		$this->set( 'm_intFirstDayLateFeeCalculationTypeReferenceId', CStrings::strToIntDef( $intFirstDayLateFeeCalculationTypeReferenceId, NULL, false ) );
	}

	public function getFirstDayLateFeeCalculationTypeReferenceId() {
		return $this->m_intFirstDayLateFeeCalculationTypeReferenceId;
	}

	public function sqlFirstDayLateFeeCalculationTypeReferenceId() {
		return ( true == isset( $this->m_intFirstDayLateFeeCalculationTypeReferenceId ) ) ? ( string ) $this->m_intFirstDayLateFeeCalculationTypeReferenceId : 'NULL';
	}

	public function setDailyLateFeeCalculationTypeReferenceId( $intDailyLateFeeCalculationTypeReferenceId ) {
		$this->set( 'm_intDailyLateFeeCalculationTypeReferenceId', CStrings::strToIntDef( $intDailyLateFeeCalculationTypeReferenceId, NULL, false ) );
	}

	public function getDailyLateFeeCalculationTypeReferenceId() {
		return $this->m_intDailyLateFeeCalculationTypeReferenceId;
	}

	public function sqlDailyLateFeeCalculationTypeReferenceId() {
		return ( true == isset( $this->m_intDailyLateFeeCalculationTypeReferenceId ) ) ? ( string ) $this->m_intDailyLateFeeCalculationTypeReferenceId : 'NULL';
	}

	public function setMonthlyMaxCalculationTypeId( $intMonthlyMaxCalculationTypeId ) {
		$this->set( 'm_intMonthlyMaxCalculationTypeId', CStrings::strToIntDef( $intMonthlyMaxCalculationTypeId, NULL, false ) );
	}

	public function getMonthlyMaxCalculationTypeId() {
		return $this->m_intMonthlyMaxCalculationTypeId;
	}

	public function sqlMonthlyMaxCalculationTypeId() {
		return ( true == isset( $this->m_intMonthlyMaxCalculationTypeId ) ) ? ( string ) $this->m_intMonthlyMaxCalculationTypeId : 'NULL';
	}

	public function setMonthlyMaxCalculationTypeReferenceId( $intMonthlyMaxCalculationTypeReferenceId ) {
		$this->set( 'm_intMonthlyMaxCalculationTypeReferenceId', CStrings::strToIntDef( $intMonthlyMaxCalculationTypeReferenceId, NULL, false ) );
	}

	public function getMonthlyMaxCalculationTypeReferenceId() {
		return $this->m_intMonthlyMaxCalculationTypeReferenceId;
	}

	public function sqlMonthlyMaxCalculationTypeReferenceId() {
		return ( true == isset( $this->m_intMonthlyMaxCalculationTypeReferenceId ) ) ? ( string ) $this->m_intMonthlyMaxCalculationTypeReferenceId : 'NULL';
	}

	public function setBalanceThresholdCalculationTypeId( $intBalanceThresholdCalculationTypeId ) {
		$this->set( 'm_intBalanceThresholdCalculationTypeId', CStrings::strToIntDef( $intBalanceThresholdCalculationTypeId, NULL, false ) );
	}

	public function getBalanceThresholdCalculationTypeId() {
		return $this->m_intBalanceThresholdCalculationTypeId;
	}

	public function sqlBalanceThresholdCalculationTypeId() {
		return ( true == isset( $this->m_intBalanceThresholdCalculationTypeId ) ) ? ( string ) $this->m_intBalanceThresholdCalculationTypeId : '1';
	}

	public function setBalanceThresholdCalculationTypeReferenceId( $intBalanceThresholdCalculationTypeReferenceId ) {
		$this->set( 'm_intBalanceThresholdCalculationTypeReferenceId', CStrings::strToIntDef( $intBalanceThresholdCalculationTypeReferenceId, NULL, false ) );
	}

	public function getBalanceThresholdCalculationTypeReferenceId() {
		return $this->m_intBalanceThresholdCalculationTypeReferenceId;
	}

	public function sqlBalanceThresholdCalculationTypeReferenceId() {
		return ( true == isset( $this->m_intBalanceThresholdCalculationTypeReferenceId ) ) ? ( string ) $this->m_intBalanceThresholdCalculationTypeReferenceId : 'NULL';
	}

	public function setSubsidyTypeId( $intSubsidyTypeId ) {
		$this->set( 'm_intSubsidyTypeId', CStrings::strToIntDef( $intSubsidyTypeId, NULL, false ) );
	}

	public function getSubsidyTypeId() {
		return $this->m_intSubsidyTypeId;
	}

	public function sqlSubsidyTypeId() {
		return ( true == isset( $this->m_intSubsidyTypeId ) ) ? ( string ) $this->m_intSubsidyTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, default_late_fee_formula_id, parent_late_fee_formula_id, first_day_ar_code_id, daily_ar_code_id, legal_fee_charge_code_id, occupancy_type_id, name, description, fixed_amount, amount_per_day, fixed_percent, percent_per_day, first_late_day, daily_late_fee_start_day, minimum_balance_due, max_monthly_late_fee_percent, max_monthly_late_fee_amount, legal_fee_amount, scheduled_charges_only, base_charge_on_amount_due, auto_post_nsf_late_fees, post_fees_to_next_period, include_past_period_transactions, allow_to_apply_legal_fee, is_system, daily_late_fee_end_day, include_future_leases, charge_on_payment_post_date, charge_late_fees_on_holidays, charge_late_fees_on_saturdays, charge_late_fees_on_sundays, is_disabled, updated_by, updated_on, created_by, created_on, grace_days, details, first_day_late_fee_calculation_method_id, daily_late_fee_calculation_method_id, fixed_percent_threshold, daily_percent_threshold, first_day_late_fee_calculation_type_reference_id, daily_late_fee_calculation_type_reference_id, monthly_max_calculation_type_id, monthly_max_calculation_type_reference_id, balance_threshold_calculation_type_id, balance_threshold_calculation_type_reference_id, subsidy_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlDefaultLateFeeFormulaId() . ', ' .
						$this->sqlParentLateFeeFormulaId() . ', ' .
						$this->sqlFirstDayArCodeId() . ', ' .
						$this->sqlDailyArCodeId() . ', ' .
						$this->sqlLegalFeeChargeCodeId() . ', ' .
						$this->sqlOccupancyTypeId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlFixedAmount() . ', ' .
						$this->sqlAmountPerDay() . ', ' .
						$this->sqlFixedPercent() . ', ' .
						$this->sqlPercentPerDay() . ', ' .
						$this->sqlFirstLateDay() . ', ' .
						$this->sqlDailyLateFeeStartDay() . ', ' .
						$this->sqlMinimumBalanceDue() . ', ' .
						$this->sqlMaxMonthlyLateFeePercent() . ', ' .
						$this->sqlMaxMonthlyLateFeeAmount() . ', ' .
						$this->sqlLegalFeeAmount() . ', ' .
						$this->sqlScheduledChargesOnly() . ', ' .
						$this->sqlBaseChargeOnAmountDue() . ', ' .
						$this->sqlAutoPostNsfLateFees() . ', ' .
						$this->sqlPostFeesToNextPeriod() . ', ' .
						$this->sqlIncludePastPeriodTransactions() . ', ' .
						$this->sqlAllowToApplyLegalFee() . ', ' .
						$this->sqlIsSystem() . ', ' .
						$this->sqlDailyLateFeeEndDay() . ', ' .
						$this->sqlIncludeFutureLeases() . ', ' .
						$this->sqlChargeOnPaymentPostDate() . ', ' .
						$this->sqlChargeLateFeesOnHolidays() . ', ' .
						$this->sqlChargeLateFeesOnSaturdays() . ', ' .
						$this->sqlChargeLateFeesOnSundays() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlGraceDays() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlFirstDayLateFeeCalculationMethodId() . ', ' .
						$this->sqlDailyLateFeeCalculationMethodId() . ', ' .
						$this->sqlFixedPercentThreshold() . ', ' .
						$this->sqlDailyPercentThreshold() . ', ' .
						$this->sqlFirstDayLateFeeCalculationTypeReferenceId() . ', ' .
						$this->sqlDailyLateFeeCalculationTypeReferenceId() . ', ' .
						$this->sqlMonthlyMaxCalculationTypeId() . ', ' .
						$this->sqlMonthlyMaxCalculationTypeReferenceId() . ', ' .
						$this->sqlBalanceThresholdCalculationTypeId() . ', ' .
						$this->sqlBalanceThresholdCalculationTypeReferenceId() . ', ' .
						$this->sqlSubsidyTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_late_fee_formula_id = ' . $this->sqlDefaultLateFeeFormulaId(). ',' ; } elseif( true == array_key_exists( 'DefaultLateFeeFormulaId', $this->getChangedColumns() ) ) { $strSql .= ' default_late_fee_formula_id = ' . $this->sqlDefaultLateFeeFormulaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_late_fee_formula_id = ' . $this->sqlParentLateFeeFormulaId(). ',' ; } elseif( true == array_key_exists( 'ParentLateFeeFormulaId', $this->getChangedColumns() ) ) { $strSql .= ' parent_late_fee_formula_id = ' . $this->sqlParentLateFeeFormulaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_day_ar_code_id = ' . $this->sqlFirstDayArCodeId(). ',' ; } elseif( true == array_key_exists( 'FirstDayArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' first_day_ar_code_id = ' . $this->sqlFirstDayArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' daily_ar_code_id = ' . $this->sqlDailyArCodeId(). ',' ; } elseif( true == array_key_exists( 'DailyArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' daily_ar_code_id = ' . $this->sqlDailyArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' legal_fee_charge_code_id = ' . $this->sqlLegalFeeChargeCodeId(). ',' ; } elseif( true == array_key_exists( 'LegalFeeChargeCodeId', $this->getChangedColumns() ) ) { $strSql .= ' legal_fee_charge_code_id = ' . $this->sqlLegalFeeChargeCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fixed_amount = ' . $this->sqlFixedAmount(). ',' ; } elseif( true == array_key_exists( 'FixedAmount', $this->getChangedColumns() ) ) { $strSql .= ' fixed_amount = ' . $this->sqlFixedAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount_per_day = ' . $this->sqlAmountPerDay(). ',' ; } elseif( true == array_key_exists( 'AmountPerDay', $this->getChangedColumns() ) ) { $strSql .= ' amount_per_day = ' . $this->sqlAmountPerDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fixed_percent = ' . $this->sqlFixedPercent(). ',' ; } elseif( true == array_key_exists( 'FixedPercent', $this->getChangedColumns() ) ) { $strSql .= ' fixed_percent = ' . $this->sqlFixedPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' percent_per_day = ' . $this->sqlPercentPerDay(). ',' ; } elseif( true == array_key_exists( 'PercentPerDay', $this->getChangedColumns() ) ) { $strSql .= ' percent_per_day = ' . $this->sqlPercentPerDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_late_day = ' . $this->sqlFirstLateDay(). ',' ; } elseif( true == array_key_exists( 'FirstLateDay', $this->getChangedColumns() ) ) { $strSql .= ' first_late_day = ' . $this->sqlFirstLateDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' daily_late_fee_start_day = ' . $this->sqlDailyLateFeeStartDay(). ',' ; } elseif( true == array_key_exists( 'DailyLateFeeStartDay', $this->getChangedColumns() ) ) { $strSql .= ' daily_late_fee_start_day = ' . $this->sqlDailyLateFeeStartDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minimum_balance_due = ' . $this->sqlMinimumBalanceDue(). ',' ; } elseif( true == array_key_exists( 'MinimumBalanceDue', $this->getChangedColumns() ) ) { $strSql .= ' minimum_balance_due = ' . $this->sqlMinimumBalanceDue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_monthly_late_fee_percent = ' . $this->sqlMaxMonthlyLateFeePercent(). ',' ; } elseif( true == array_key_exists( 'MaxMonthlyLateFeePercent', $this->getChangedColumns() ) ) { $strSql .= ' max_monthly_late_fee_percent = ' . $this->sqlMaxMonthlyLateFeePercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_monthly_late_fee_amount = ' . $this->sqlMaxMonthlyLateFeeAmount(). ',' ; } elseif( true == array_key_exists( 'MaxMonthlyLateFeeAmount', $this->getChangedColumns() ) ) { $strSql .= ' max_monthly_late_fee_amount = ' . $this->sqlMaxMonthlyLateFeeAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' legal_fee_amount = ' . $this->sqlLegalFeeAmount(). ',' ; } elseif( true == array_key_exists( 'LegalFeeAmount', $this->getChangedColumns() ) ) { $strSql .= ' legal_fee_amount = ' . $this->sqlLegalFeeAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_charges_only = ' . $this->sqlScheduledChargesOnly(). ',' ; } elseif( true == array_key_exists( 'ScheduledChargesOnly', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_charges_only = ' . $this->sqlScheduledChargesOnly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_charge_on_amount_due = ' . $this->sqlBaseChargeOnAmountDue(). ',' ; } elseif( true == array_key_exists( 'BaseChargeOnAmountDue', $this->getChangedColumns() ) ) { $strSql .= ' base_charge_on_amount_due = ' . $this->sqlBaseChargeOnAmountDue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_post_nsf_late_fees = ' . $this->sqlAutoPostNsfLateFees(). ',' ; } elseif( true == array_key_exists( 'AutoPostNsfLateFees', $this->getChangedColumns() ) ) { $strSql .= ' auto_post_nsf_late_fees = ' . $this->sqlAutoPostNsfLateFees() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_fees_to_next_period = ' . $this->sqlPostFeesToNextPeriod(). ',' ; } elseif( true == array_key_exists( 'PostFeesToNextPeriod', $this->getChangedColumns() ) ) { $strSql .= ' post_fees_to_next_period = ' . $this->sqlPostFeesToNextPeriod() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_past_period_transactions = ' . $this->sqlIncludePastPeriodTransactions(). ',' ; } elseif( true == array_key_exists( 'IncludePastPeriodTransactions', $this->getChangedColumns() ) ) { $strSql .= ' include_past_period_transactions = ' . $this->sqlIncludePastPeriodTransactions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_to_apply_legal_fee = ' . $this->sqlAllowToApplyLegalFee(). ',' ; } elseif( true == array_key_exists( 'AllowToApplyLegalFee', $this->getChangedColumns() ) ) { $strSql .= ' allow_to_apply_legal_fee = ' . $this->sqlAllowToApplyLegalFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem(). ',' ; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' daily_late_fee_end_day = ' . $this->sqlDailyLateFeeEndDay(). ',' ; } elseif( true == array_key_exists( 'DailyLateFeeEndDay', $this->getChangedColumns() ) ) { $strSql .= ' daily_late_fee_end_day = ' . $this->sqlDailyLateFeeEndDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_future_leases = ' . $this->sqlIncludeFutureLeases(). ',' ; } elseif( true == array_key_exists( 'IncludeFutureLeases', $this->getChangedColumns() ) ) { $strSql .= ' include_future_leases = ' . $this->sqlIncludeFutureLeases() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_on_payment_post_date = ' . $this->sqlChargeOnPaymentPostDate(). ',' ; } elseif( true == array_key_exists( 'ChargeOnPaymentPostDate', $this->getChangedColumns() ) ) { $strSql .= ' charge_on_payment_post_date = ' . $this->sqlChargeOnPaymentPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_late_fees_on_holidays = ' . $this->sqlChargeLateFeesOnHolidays(). ',' ; } elseif( true == array_key_exists( 'ChargeLateFeesOnHolidays', $this->getChangedColumns() ) ) { $strSql .= ' charge_late_fees_on_holidays = ' . $this->sqlChargeLateFeesOnHolidays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_late_fees_on_saturdays = ' . $this->sqlChargeLateFeesOnSaturdays(). ',' ; } elseif( true == array_key_exists( 'ChargeLateFeesOnSaturdays', $this->getChangedColumns() ) ) { $strSql .= ' charge_late_fees_on_saturdays = ' . $this->sqlChargeLateFeesOnSaturdays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_late_fees_on_sundays = ' . $this->sqlChargeLateFeesOnSundays(). ',' ; } elseif( true == array_key_exists( 'ChargeLateFeesOnSundays', $this->getChangedColumns() ) ) { $strSql .= ' charge_late_fees_on_sundays = ' . $this->sqlChargeLateFeesOnSundays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' grace_days = ' . $this->sqlGraceDays(). ',' ; } elseif( true == array_key_exists( 'GraceDays', $this->getChangedColumns() ) ) { $strSql .= ' grace_days = ' . $this->sqlGraceDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_day_late_fee_calculation_method_id = ' . $this->sqlFirstDayLateFeeCalculationMethodId(). ',' ; } elseif( true == array_key_exists( 'FirstDayLateFeeCalculationMethodId', $this->getChangedColumns() ) ) { $strSql .= ' first_day_late_fee_calculation_method_id = ' . $this->sqlFirstDayLateFeeCalculationMethodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' daily_late_fee_calculation_method_id = ' . $this->sqlDailyLateFeeCalculationMethodId(). ',' ; } elseif( true == array_key_exists( 'DailyLateFeeCalculationMethodId', $this->getChangedColumns() ) ) { $strSql .= ' daily_late_fee_calculation_method_id = ' . $this->sqlDailyLateFeeCalculationMethodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fixed_percent_threshold = ' . $this->sqlFixedPercentThreshold(). ',' ; } elseif( true == array_key_exists( 'FixedPercentThreshold', $this->getChangedColumns() ) ) { $strSql .= ' fixed_percent_threshold = ' . $this->sqlFixedPercentThreshold() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' daily_percent_threshold = ' . $this->sqlDailyPercentThreshold(). ',' ; } elseif( true == array_key_exists( 'DailyPercentThreshold', $this->getChangedColumns() ) ) { $strSql .= ' daily_percent_threshold = ' . $this->sqlDailyPercentThreshold() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_day_late_fee_calculation_type_reference_id = ' . $this->sqlFirstDayLateFeeCalculationTypeReferenceId(). ',' ; } elseif( true == array_key_exists( 'FirstDayLateFeeCalculationTypeReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' first_day_late_fee_calculation_type_reference_id = ' . $this->sqlFirstDayLateFeeCalculationTypeReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' daily_late_fee_calculation_type_reference_id = ' . $this->sqlDailyLateFeeCalculationTypeReferenceId(). ',' ; } elseif( true == array_key_exists( 'DailyLateFeeCalculationTypeReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' daily_late_fee_calculation_type_reference_id = ' . $this->sqlDailyLateFeeCalculationTypeReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' monthly_max_calculation_type_id = ' . $this->sqlMonthlyMaxCalculationTypeId(). ',' ; } elseif( true == array_key_exists( 'MonthlyMaxCalculationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' monthly_max_calculation_type_id = ' . $this->sqlMonthlyMaxCalculationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' monthly_max_calculation_type_reference_id = ' . $this->sqlMonthlyMaxCalculationTypeReferenceId(). ',' ; } elseif( true == array_key_exists( 'MonthlyMaxCalculationTypeReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' monthly_max_calculation_type_reference_id = ' . $this->sqlMonthlyMaxCalculationTypeReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' balance_threshold_calculation_type_id = ' . $this->sqlBalanceThresholdCalculationTypeId(). ',' ; } elseif( true == array_key_exists( 'BalanceThresholdCalculationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' balance_threshold_calculation_type_id = ' . $this->sqlBalanceThresholdCalculationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' balance_threshold_calculation_type_reference_id = ' . $this->sqlBalanceThresholdCalculationTypeReferenceId(). ',' ; } elseif( true == array_key_exists( 'BalanceThresholdCalculationTypeReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' balance_threshold_calculation_type_reference_id = ' . $this->sqlBalanceThresholdCalculationTypeReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subsidy_type_id = ' . $this->sqlSubsidyTypeId(). ',' ; } elseif( true == array_key_exists( 'SubsidyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' subsidy_type_id = ' . $this->sqlSubsidyTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'default_late_fee_formula_id' => $this->getDefaultLateFeeFormulaId(),
			'parent_late_fee_formula_id' => $this->getParentLateFeeFormulaId(),
			'first_day_ar_code_id' => $this->getFirstDayArCodeId(),
			'daily_ar_code_id' => $this->getDailyArCodeId(),
			'legal_fee_charge_code_id' => $this->getLegalFeeChargeCodeId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'fixed_amount' => $this->getFixedAmount(),
			'amount_per_day' => $this->getAmountPerDay(),
			'fixed_percent' => $this->getFixedPercent(),
			'percent_per_day' => $this->getPercentPerDay(),
			'first_late_day' => $this->getFirstLateDay(),
			'daily_late_fee_start_day' => $this->getDailyLateFeeStartDay(),
			'minimum_balance_due' => $this->getMinimumBalanceDue(),
			'max_monthly_late_fee_percent' => $this->getMaxMonthlyLateFeePercent(),
			'max_monthly_late_fee_amount' => $this->getMaxMonthlyLateFeeAmount(),
			'legal_fee_amount' => $this->getLegalFeeAmount(),
			'scheduled_charges_only' => $this->getScheduledChargesOnly(),
			'base_charge_on_amount_due' => $this->getBaseChargeOnAmountDue(),
			'auto_post_nsf_late_fees' => $this->getAutoPostNsfLateFees(),
			'post_fees_to_next_period' => $this->getPostFeesToNextPeriod(),
			'include_past_period_transactions' => $this->getIncludePastPeriodTransactions(),
			'allow_to_apply_legal_fee' => $this->getAllowToApplyLegalFee(),
			'is_system' => $this->getIsSystem(),
			'daily_late_fee_end_day' => $this->getDailyLateFeeEndDay(),
			'include_future_leases' => $this->getIncludeFutureLeases(),
			'charge_on_payment_post_date' => $this->getChargeOnPaymentPostDate(),
			'charge_late_fees_on_holidays' => $this->getChargeLateFeesOnHolidays(),
			'charge_late_fees_on_saturdays' => $this->getChargeLateFeesOnSaturdays(),
			'charge_late_fees_on_sundays' => $this->getChargeLateFeesOnSundays(),
			'is_disabled' => $this->getIsDisabled(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'grace_days' => $this->getGraceDays(),
			'details' => $this->getDetails(),
			'first_day_late_fee_calculation_method_id' => $this->getFirstDayLateFeeCalculationMethodId(),
			'daily_late_fee_calculation_method_id' => $this->getDailyLateFeeCalculationMethodId(),
			'fixed_percent_threshold' => $this->getFixedPercentThreshold(),
			'daily_percent_threshold' => $this->getDailyPercentThreshold(),
			'first_day_late_fee_calculation_type_reference_id' => $this->getFirstDayLateFeeCalculationTypeReferenceId(),
			'daily_late_fee_calculation_type_reference_id' => $this->getDailyLateFeeCalculationTypeReferenceId(),
			'monthly_max_calculation_type_id' => $this->getMonthlyMaxCalculationTypeId(),
			'monthly_max_calculation_type_reference_id' => $this->getMonthlyMaxCalculationTypeReferenceId(),
			'balance_threshold_calculation_type_id' => $this->getBalanceThresholdCalculationTypeId(),
			'balance_threshold_calculation_type_reference_id' => $this->getBalanceThresholdCalculationTypeReferenceId(),
			'subsidy_type_id' => $this->getSubsidyTypeId()
		);
	}

}
?>