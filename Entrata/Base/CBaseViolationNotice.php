<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseViolationNotice extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.violation_notices';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intViolationId;
	protected $m_intFileId;
	protected $m_strDescription;
	protected $m_strFollowUpDate;
	protected $m_strViolationNoticeDatetime;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intArCodeId;
	protected $m_fltAmount;
	protected $m_intReportedBy;
	protected $m_intDocumentId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intViolationTemplateNoticeId;

	public function __construct() {
		parent::__construct();

		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['violation_id'] ) && $boolDirectSet ) $this->set( 'm_intViolationId', trim( $arrValues['violation_id'] ) ); elseif( isset( $arrValues['violation_id'] ) ) $this->setViolationId( $arrValues['violation_id'] );
		if( isset( $arrValues['file_id'] ) && $boolDirectSet ) $this->set( 'm_intFileId', trim( $arrValues['file_id'] ) ); elseif( isset( $arrValues['file_id'] ) ) $this->setFileId( $arrValues['file_id'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['follow_up_date'] ) && $boolDirectSet ) $this->set( 'm_strFollowUpDate', trim( $arrValues['follow_up_date'] ) ); elseif( isset( $arrValues['follow_up_date'] ) ) $this->setFollowUpDate( $arrValues['follow_up_date'] );
		if( isset( $arrValues['violation_notice_datetime'] ) && $boolDirectSet ) $this->set( 'm_strViolationNoticeDatetime', trim( $arrValues['violation_notice_datetime'] ) ); elseif( isset( $arrValues['violation_notice_datetime'] ) ) $this->setViolationNoticeDatetime( $arrValues['violation_notice_datetime'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeId', trim( $arrValues['ar_code_id'] ) ); elseif( isset( $arrValues['ar_code_id'] ) ) $this->setArCodeId( $arrValues['ar_code_id'] );
		if( isset( $arrValues['amount'] ) && $boolDirectSet ) $this->set( 'm_fltAmount', trim( $arrValues['amount'] ) ); elseif( isset( $arrValues['amount'] ) ) $this->setAmount( $arrValues['amount'] );
		if( isset( $arrValues['reported_by'] ) && $boolDirectSet ) $this->set( 'm_intReportedBy', trim( $arrValues['reported_by'] ) ); elseif( isset( $arrValues['reported_by'] ) ) $this->setReportedBy( $arrValues['reported_by'] );
		if( isset( $arrValues['document_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentId', trim( $arrValues['document_id'] ) ); elseif( isset( $arrValues['document_id'] ) ) $this->setDocumentId( $arrValues['document_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['violation_template_notice_id'] ) && $boolDirectSet ) $this->set( 'm_intViolationTemplateNoticeId', trim( $arrValues['violation_template_notice_id'] ) ); elseif( isset( $arrValues['violation_template_notice_id'] ) ) $this->setViolationTemplateNoticeId( $arrValues['violation_template_notice_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setViolationId( $intViolationId ) {
		$this->set( 'm_intViolationId', CStrings::strToIntDef( $intViolationId, NULL, false ) );
	}

	public function getViolationId() {
		return $this->m_intViolationId;
	}

	public function sqlViolationId() {
		return ( true == isset( $this->m_intViolationId ) ) ? ( string ) $this->m_intViolationId : 'NULL';
	}

	public function setFileId( $intFileId ) {
		$this->set( 'm_intFileId', CStrings::strToIntDef( $intFileId, NULL, false ) );
	}

	public function getFileId() {
		return $this->m_intFileId;
	}

	public function sqlFileId() {
		return ( true == isset( $this->m_intFileId ) ) ? ( string ) $this->m_intFileId : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setFollowUpDate( $strFollowUpDate ) {
		$this->set( 'm_strFollowUpDate', CStrings::strTrimDef( $strFollowUpDate, -1, NULL, true ) );
	}

	public function getFollowUpDate() {
		return $this->m_strFollowUpDate;
	}

	public function sqlFollowUpDate() {
		return ( true == isset( $this->m_strFollowUpDate ) ) ? '\'' . $this->m_strFollowUpDate . '\'' : 'NULL';
	}

	public function setViolationNoticeDatetime( $strViolationNoticeDatetime ) {
		$this->set( 'm_strViolationNoticeDatetime', CStrings::strTrimDef( $strViolationNoticeDatetime, -1, NULL, true ) );
	}

	public function getViolationNoticeDatetime() {
		return $this->m_strViolationNoticeDatetime;
	}

	public function sqlViolationNoticeDatetime() {
		return ( true == isset( $this->m_strViolationNoticeDatetime ) ) ? '\'' . $this->m_strViolationNoticeDatetime . '\'' : 'NOW()';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setArCodeId( $intArCodeId ) {
		$this->set( 'm_intArCodeId', CStrings::strToIntDef( $intArCodeId, NULL, false ) );
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function sqlArCodeId() {
		return ( true == isset( $this->m_intArCodeId ) ) ? ( string ) $this->m_intArCodeId : 'NULL';
	}

	public function setAmount( $fltAmount ) {
		$this->set( 'm_fltAmount', CStrings::strToFloatDef( $fltAmount, NULL, false, 2 ) );
	}

	public function getAmount() {
		return $this->m_fltAmount;
	}

	public function sqlAmount() {
		return ( true == isset( $this->m_fltAmount ) ) ? ( string ) $this->m_fltAmount : 'NULL';
	}

	public function setReportedBy( $intReportedBy ) {
		$this->set( 'm_intReportedBy', CStrings::strToIntDef( $intReportedBy, NULL, false ) );
	}

	public function getReportedBy() {
		return $this->m_intReportedBy;
	}

	public function sqlReportedBy() {
		return ( true == isset( $this->m_intReportedBy ) ) ? ( string ) $this->m_intReportedBy : 'NULL';
	}

	public function setDocumentId( $intDocumentId ) {
		$this->set( 'm_intDocumentId', CStrings::strToIntDef( $intDocumentId, NULL, false ) );
	}

	public function getDocumentId() {
		return $this->m_intDocumentId;
	}

	public function sqlDocumentId() {
		return ( true == isset( $this->m_intDocumentId ) ) ? ( string ) $this->m_intDocumentId : 'NULL';
	}

	public function setViolationTemplateNoticeId( $intViolationTemplateNoticeId ) {
		$this->set( 'm_intViolationTemplateNoticeId', CStrings::strToIntDef( $intViolationTemplateNoticeId, NULL, false ) );
	}

	public function getViolationTemplateNoticeId() {
		return $this->m_intViolationTemplateNoticeId;
	}

	public function sqlViolationTemplateNoticeId() {
		return ( true == isset( $this->m_intViolationTemplateNoticeId ) ) ? ( string ) $this->m_intViolationTemplateNoticeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, violation_id, file_id, description, follow_up_date, violation_notice_datetime, order_num, updated_by, updated_on, created_by, created_on, ar_code_id, amount, reported_by, document_id, details, violation_template_notice_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlViolationId() . ', ' .
						$this->sqlFileId() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlFollowUpDate() . ', ' .
						$this->sqlViolationNoticeDatetime() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlArCodeId() . ', ' .
						$this->sqlAmount() . ', ' .
						$this->sqlReportedBy() . ', ' .
						$this->sqlDocumentId() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlViolationTemplateNoticeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' violation_id = ' . $this->sqlViolationId(). ',' ; } elseif( true == array_key_exists( 'ViolationId', $this->getChangedColumns() ) ) { $strSql .= ' violation_id = ' . $this->sqlViolationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_id = ' . $this->sqlFileId(). ',' ; } elseif( true == array_key_exists( 'FileId', $this->getChangedColumns() ) ) { $strSql .= ' file_id = ' . $this->sqlFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' follow_up_date = ' . $this->sqlFollowUpDate(). ',' ; } elseif( true == array_key_exists( 'FollowUpDate', $this->getChangedColumns() ) ) { $strSql .= ' follow_up_date = ' . $this->sqlFollowUpDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' violation_notice_datetime = ' . $this->sqlViolationNoticeDatetime(). ',' ; } elseif( true == array_key_exists( 'ViolationNoticeDatetime', $this->getChangedColumns() ) ) { $strSql .= ' violation_notice_datetime = ' . $this->sqlViolationNoticeDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId(). ',' ; } elseif( true == array_key_exists( 'ArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount = ' . $this->sqlAmount(). ',' ; } elseif( true == array_key_exists( 'Amount', $this->getChangedColumns() ) ) { $strSql .= ' amount = ' . $this->sqlAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reported_by = ' . $this->sqlReportedBy(). ',' ; } elseif( true == array_key_exists( 'ReportedBy', $this->getChangedColumns() ) ) { $strSql .= ' reported_by = ' . $this->sqlReportedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_id = ' . $this->sqlDocumentId(). ',' ; } elseif( true == array_key_exists( 'DocumentId', $this->getChangedColumns() ) ) { $strSql .= ' document_id = ' . $this->sqlDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' violation_template_notice_id = ' . $this->sqlViolationTemplateNoticeId(). ',' ; } elseif( true == array_key_exists( 'ViolationTemplateNoticeId', $this->getChangedColumns() ) ) { $strSql .= ' violation_template_notice_id = ' . $this->sqlViolationTemplateNoticeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'violation_id' => $this->getViolationId(),
			'file_id' => $this->getFileId(),
			'description' => $this->getDescription(),
			'follow_up_date' => $this->getFollowUpDate(),
			'violation_notice_datetime' => $this->getViolationNoticeDatetime(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'ar_code_id' => $this->getArCodeId(),
			'amount' => $this->getAmount(),
			'reported_by' => $this->getReportedBy(),
			'document_id' => $this->getDocumentId(),
			'details' => $this->getDetails(),
			'violation_template_notice_id' => $this->getViolationTemplateNoticeId()
		);
	}

}
?>