<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePaymentPlans extends CEosPluralBase {

    const TABLE_PAYMENT_PLANS = 'public.payment_plans';

    public static function fetchPaymentPlans( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CPaymentPlan', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchPaymentPlan( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CPaymentPlan', $objDatabase );
    }

    public static function fetchPaymentPlanCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'payment_plans', $objDatabase );
    }

    public static function fetchPaymentPlanByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchPaymentPlan( sprintf( 'SELECT * FROM payment_plans WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchPaymentPlansByCid( $intCid, $objDatabase ) {
        return self::fetchPaymentPlans( sprintf( 'SELECT * FROM payment_plans WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchPaymentPlansByPaymentArCodeIdByCid( $intPaymentArCodeId, $intCid, $objDatabase ) {
        return self::fetchPaymentPlans( sprintf( 'SELECT * FROM payment_plans WHERE payment_ar_code_id = %d AND cid = %d', (int) $intPaymentArCodeId, (int) $intCid ), $objDatabase );
    }

    public static function fetchPaymentPlansBySpecialIdByCid( $intSpecialId, $intCid, $objDatabase ) {
        return self::fetchPaymentPlans( sprintf( 'SELECT * FROM payment_plans WHERE special_id = %d AND cid = %d', (int) $intSpecialId, (int) $intCid ), $objDatabase );
    }

}
?>