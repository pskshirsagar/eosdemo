<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMarketingMedia extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.marketing_medias';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCompanyMediaFolderId;
	protected $m_intMimeTypeId;
	protected $m_intMarketingStorageTypeId;
	protected $m_strMarketingStorageTypeReference;
	protected $m_boolLegacyIsHidden;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_media_folder_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMediaFolderId', trim( $arrValues['company_media_folder_id'] ) ); elseif( isset( $arrValues['company_media_folder_id'] ) ) $this->setCompanyMediaFolderId( $arrValues['company_media_folder_id'] );
		if( isset( $arrValues['mime_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMimeTypeId', trim( $arrValues['mime_type_id'] ) ); elseif( isset( $arrValues['mime_type_id'] ) ) $this->setMimeTypeId( $arrValues['mime_type_id'] );
		if( isset( $arrValues['marketing_storage_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMarketingStorageTypeId', trim( $arrValues['marketing_storage_type_id'] ) ); elseif( isset( $arrValues['marketing_storage_type_id'] ) ) $this->setMarketingStorageTypeId( $arrValues['marketing_storage_type_id'] );
		if( isset( $arrValues['marketing_storage_type_reference'] ) && $boolDirectSet ) $this->set( 'm_strMarketingStorageTypeReference', trim( stripcslashes( $arrValues['marketing_storage_type_reference'] ) ) ); elseif( isset( $arrValues['marketing_storage_type_reference'] ) ) $this->setMarketingStorageTypeReference( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['marketing_storage_type_reference'] ) : $arrValues['marketing_storage_type_reference'] );
		if( isset( $arrValues['legacy_is_hidden'] ) && $boolDirectSet ) $this->set( 'm_boolLegacyIsHidden', trim( stripcslashes( $arrValues['legacy_is_hidden'] ) ) ); elseif( isset( $arrValues['legacy_is_hidden'] ) ) $this->setLegacyIsHidden( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['legacy_is_hidden'] ) : $arrValues['legacy_is_hidden'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : '1';
	}

	public function setCompanyMediaFolderId( $intCompanyMediaFolderId ) {
		$this->set( 'm_intCompanyMediaFolderId', CStrings::strToIntDef( $intCompanyMediaFolderId, NULL, false ) );
	}

	public function getCompanyMediaFolderId() {
		return $this->m_intCompanyMediaFolderId;
	}

	public function sqlCompanyMediaFolderId() {
		return ( true == isset( $this->m_intCompanyMediaFolderId ) ) ? ( string ) $this->m_intCompanyMediaFolderId : 'NULL';
	}

	public function setMimeTypeId( $intMimeTypeId ) {
		$this->set( 'm_intMimeTypeId', CStrings::strToIntDef( $intMimeTypeId, NULL, false ) );
	}

	public function getMimeTypeId() {
		return $this->m_intMimeTypeId;
	}

	public function sqlMimeTypeId() {
		return ( true == isset( $this->m_intMimeTypeId ) ) ? ( string ) $this->m_intMimeTypeId : 'NULL';
	}

	public function setMarketingStorageTypeId( $intMarketingStorageTypeId ) {
		$this->set( 'm_intMarketingStorageTypeId', CStrings::strToIntDef( $intMarketingStorageTypeId, NULL, false ) );
	}

	public function getMarketingStorageTypeId() {
		return $this->m_intMarketingStorageTypeId;
	}

	public function sqlMarketingStorageTypeId() {
		return ( true == isset( $this->m_intMarketingStorageTypeId ) ) ? ( string ) $this->m_intMarketingStorageTypeId : 'NULL';
	}

	public function setMarketingStorageTypeReference( $strMarketingStorageTypeReference ) {
		$this->set( 'm_strMarketingStorageTypeReference', CStrings::strTrimDef( $strMarketingStorageTypeReference, 4096, NULL, true ) );
	}

	public function getMarketingStorageTypeReference() {
		return $this->m_strMarketingStorageTypeReference;
	}

	public function sqlMarketingStorageTypeReference() {
		return ( true == isset( $this->m_strMarketingStorageTypeReference ) ) ? '\'' . addslashes( $this->m_strMarketingStorageTypeReference ) . '\'' : 'NULL';
	}

	public function setLegacyIsHidden( $boolLegacyIsHidden ) {
		$this->set( 'm_boolLegacyIsHidden', CStrings::strToBool( $boolLegacyIsHidden ) );
	}

	public function getLegacyIsHidden() {
		return $this->m_boolLegacyIsHidden;
	}

	public function sqlLegacyIsHidden() {
		return ( true == isset( $this->m_boolLegacyIsHidden ) ) ? '\'' . ( true == ( bool ) $this->m_boolLegacyIsHidden ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, company_media_folder_id, mime_type_id, marketing_storage_type_id, marketing_storage_type_reference, legacy_is_hidden, details, created_by, created_on, updated_by, updated_on, deleted_by, deleted_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCompanyMediaFolderId() . ', ' .
						$this->sqlMimeTypeId() . ', ' .
						$this->sqlMarketingStorageTypeId() . ', ' .
						$this->sqlMarketingStorageTypeReference() . ', ' .
						$this->sqlLegacyIsHidden() . ', ' .
						$this->sqlDetails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_folder_id = ' . $this->sqlCompanyMediaFolderId(). ',' ; } elseif( true == array_key_exists( 'CompanyMediaFolderId', $this->getChangedColumns() ) ) { $strSql .= ' company_media_folder_id = ' . $this->sqlCompanyMediaFolderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mime_type_id = ' . $this->sqlMimeTypeId(). ',' ; } elseif( true == array_key_exists( 'MimeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' mime_type_id = ' . $this->sqlMimeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_storage_type_id = ' . $this->sqlMarketingStorageTypeId(). ',' ; } elseif( true == array_key_exists( 'MarketingStorageTypeId', $this->getChangedColumns() ) ) { $strSql .= ' marketing_storage_type_id = ' . $this->sqlMarketingStorageTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_storage_type_reference = ' . $this->sqlMarketingStorageTypeReference(). ',' ; } elseif( true == array_key_exists( 'MarketingStorageTypeReference', $this->getChangedColumns() ) ) { $strSql .= ' marketing_storage_type_reference = ' . $this->sqlMarketingStorageTypeReference() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' legacy_is_hidden = ' . $this->sqlLegacyIsHidden(). ',' ; } elseif( true == array_key_exists( 'LegacyIsHidden', $this->getChangedColumns() ) ) { $strSql .= ' legacy_is_hidden = ' . $this->sqlLegacyIsHidden() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'company_media_folder_id' => $this->getCompanyMediaFolderId(),
			'mime_type_id' => $this->getMimeTypeId(),
			'marketing_storage_type_id' => $this->getMarketingStorageTypeId(),
			'marketing_storage_type_reference' => $this->getMarketingStorageTypeReference(),
			'legacy_is_hidden' => $this->getLegacyIsHidden(),
			'details' => $this->getDetails(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn()
		);
	}

}
?>