<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFieldValues
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFieldValues extends CEosPluralBase {

	/**
	 * @return CFieldValue[]
	 */
	public static function fetchFieldValues( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CFieldValue', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFieldValue
	 */
	public static function fetchFieldValue( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CFieldValue', $objDatabase );
	}

	public static function fetchFieldValueCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'field_values', $objDatabase );
	}

	public static function fetchFieldValueByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFieldValue( sprintf( 'SELECT * FROM field_values WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFieldValuesByCid( $intCid, $objDatabase ) {
		return self::fetchFieldValues( sprintf( 'SELECT * FROM field_values WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFieldValuesByFieldTypeIdByCid( $intFieldTypeId, $intCid, $objDatabase ) {
		return self::fetchFieldValues( sprintf( 'SELECT * FROM field_values WHERE field_type_id = %d AND cid = %d', ( int ) $intFieldTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFieldValuesByFieldIdByCid( $intFieldId, $intCid, $objDatabase ) {
		return self::fetchFieldValues( sprintf( 'SELECT * FROM field_values WHERE field_id = %d AND cid = %d', ( int ) $intFieldId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFieldValuesByFieldOptionIdByCid( $intFieldOptionId, $intCid, $objDatabase ) {
		return self::fetchFieldValues( sprintf( 'SELECT * FROM field_values WHERE field_option_id = %d AND cid = %d', ( int ) $intFieldOptionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFieldValuesByReferenceIdByCid( $intReferenceId, $intCid, $objDatabase ) {
		return self::fetchFieldValues( sprintf( 'SELECT * FROM field_values WHERE reference_id = %d AND cid = %d', ( int ) $intReferenceId, ( int ) $intCid ), $objDatabase );
	}

}
?>