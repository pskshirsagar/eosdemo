<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyPreferenceTypes
 * Do not add any new functions to this class.
 */

class CBasePropertyPreferenceTypes extends CEosPluralBase {

	/**
	 * @return CPropertyPreferenceType[]
	 */
	public static function fetchPropertyPreferenceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPropertyPreferenceType::class, $objDatabase );
	}

	/**
	 * @return CPropertyPreferenceType
	 */
	public static function fetchPropertyPreferenceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyPreferenceType::class, $objDatabase );
	}

	public static function fetchPropertyPreferenceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_preference_types', $objDatabase );
	}

	public static function fetchPropertyPreferenceTypeById( $intId, $objDatabase ) {
		return self::fetchPropertyPreferenceType( sprintf( 'SELECT * FROM property_preference_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>