<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCalendarEventTypes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCalendarEventTypes extends CEosPluralBase {

	/**
	 * @return CCalendarEventType[]
	 */
	public static function fetchCalendarEventTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCalendarEventType::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCalendarEventType
	 */
	public static function fetchCalendarEventType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCalendarEventType::class, $objDatabase );
	}

	public static function fetchCalendarEventTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'calendar_event_types', $objDatabase );
	}

	public static function fetchCalendarEventTypeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCalendarEventType( sprintf( 'SELECT * FROM calendar_event_types WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventTypesByCid( $intCid, $objDatabase ) {
		return self::fetchCalendarEventTypes( sprintf( 'SELECT * FROM calendar_event_types WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventTypesByDefaultCalendarEventTypeIdByCid( $intDefaultCalendarEventTypeId, $intCid, $objDatabase ) {
		return self::fetchCalendarEventTypes( sprintf( 'SELECT * FROM calendar_event_types WHERE default_calendar_event_type_id = %d AND cid = %d', $intDefaultCalendarEventTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventTypesByCalendarEventCategoryIdByCid( $intCalendarEventCategoryId, $intCid, $objDatabase ) {
		return self::fetchCalendarEventTypes( sprintf( 'SELECT * FROM calendar_event_types WHERE calendar_event_category_id = %d AND cid = %d', $intCalendarEventCategoryId, $intCid ), $objDatabase );
	}

	public static function fetchCalendarEventTypesByEventTypeIdByCid( $intEventTypeId, $intCid, $objDatabase ) {
		return self::fetchCalendarEventTypes( sprintf( 'SELECT * FROM calendar_event_types WHERE event_type_id = %d AND cid = %d', $intEventTypeId, $intCid ), $objDatabase );
	}

}
?>