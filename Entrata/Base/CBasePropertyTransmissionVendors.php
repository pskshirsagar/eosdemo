<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyTransmissionVendors
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyTransmissionVendors extends CEosPluralBase {

	/**
	 * @return CPropertyTransmissionVendor[]
	 */
	public static function fetchPropertyTransmissionVendors( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPropertyTransmissionVendor::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyTransmissionVendor
	 */
	public static function fetchPropertyTransmissionVendor( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyTransmissionVendor::class, $objDatabase );
	}

	public static function fetchPropertyTransmissionVendorCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_transmission_vendors', $objDatabase );
	}

	public static function fetchPropertyTransmissionVendorByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyTransmissionVendor( sprintf( 'SELECT * FROM property_transmission_vendors WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyTransmissionVendorsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyTransmissionVendors( sprintf( 'SELECT * FROM property_transmission_vendors WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchPropertyTransmissionVendorsByCompanyTransmissionVendorIdByCid( $intCompanyTransmissionVendorId, $intCid, $objDatabase ) {
		return self::fetchPropertyTransmissionVendors( sprintf( 'SELECT * FROM property_transmission_vendors WHERE company_transmission_vendor_id = %d AND cid = %d', $intCompanyTransmissionVendorId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyTransmissionVendorsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyTransmissionVendors( sprintf( 'SELECT * FROM property_transmission_vendors WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyTransmissionVendorsByTaxableEntityIdByCid( $intTaxableEntityId, $intCid, $objDatabase ) {
		return self::fetchPropertyTransmissionVendors( sprintf( 'SELECT * FROM property_transmission_vendors WHERE taxable_entity_id = %d AND cid = %d', $intTaxableEntityId, $intCid ), $objDatabase );
	}

}
?>