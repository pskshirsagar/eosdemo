<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CImportBatches
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseImportBatches extends CEosPluralBase {

	/**
	 * @return CImportBatch[]
	 */
	public static function fetchImportBatches( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CImportBatch', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CImportBatch
	 */
	public static function fetchImportBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CImportBatch', $objDatabase );
	}

	public static function fetchImportBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'import_batches', $objDatabase );
	}

	public static function fetchImportBatchByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchImportBatch( sprintf( 'SELECT * FROM import_batches WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchImportBatchesByCid( $intCid, $objDatabase ) {
		return self::fetchImportBatches( sprintf( 'SELECT * FROM import_batches WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchImportBatchesByImportSourceIdByCid( $intImportSourceId, $intCid, $objDatabase ) {
		return self::fetchImportBatches( sprintf( 'SELECT * FROM import_batches WHERE import_source_id = %d AND cid = %d', ( int ) $intImportSourceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchImportBatchesByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchImportBatches( sprintf( 'SELECT * FROM import_batches WHERE company_employee_id = %d AND cid = %d', ( int ) $intCompanyEmployeeId, ( int ) $intCid ), $objDatabase );
	}

}
?>