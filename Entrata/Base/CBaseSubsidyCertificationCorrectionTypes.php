<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyCertificationCorrectionTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyCertificationCorrectionTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyCertificationCorrectionType[]
	 */
	public static function fetchSubsidyCertificationCorrectionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyCertificationCorrectionType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyCertificationCorrectionType
	 */
	public static function fetchSubsidyCertificationCorrectionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyCertificationCorrectionType::class, $objDatabase );
	}

	public static function fetchSubsidyCertificationCorrectionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_certification_correction_types', $objDatabase );
	}

	public static function fetchSubsidyCertificationCorrectionTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyCertificationCorrectionType( sprintf( 'SELECT * FROM subsidy_certification_correction_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>