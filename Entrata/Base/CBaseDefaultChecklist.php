<?php

class CBaseDefaultChecklist extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_checklists';

	protected $m_intId;
	protected $m_intChecklistTypeId;
	protected $m_intChecklistTriggerId;
	protected $m_arrintOccupancyTypeIds;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_boolIsEnabled;
	protected $m_boolIsSystem;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsEnabled = true;
		$this->m_boolIsSystem = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['checklist_type_id'] ) && $boolDirectSet ) $this->set( 'm_intChecklistTypeId', trim( $arrValues['checklist_type_id'] ) ); elseif( isset( $arrValues['checklist_type_id'] ) ) $this->setChecklistTypeId( $arrValues['checklist_type_id'] );
		if( isset( $arrValues['checklist_trigger_id'] ) && $boolDirectSet ) $this->set( 'm_intChecklistTriggerId', trim( $arrValues['checklist_trigger_id'] ) ); elseif( isset( $arrValues['checklist_trigger_id'] ) ) $this->setChecklistTriggerId( $arrValues['checklist_trigger_id'] );
		if( isset( $arrValues['occupancy_type_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintOccupancyTypeIds', trim( $arrValues['occupancy_type_ids'] ) ); elseif( isset( $arrValues['occupancy_type_ids'] ) ) $this->setOccupancyTypeIds( $arrValues['occupancy_type_ids'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_enabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsEnabled', trim( stripcslashes( $arrValues['is_enabled'] ) ) ); elseif( isset( $arrValues['is_enabled'] ) ) $this->setIsEnabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_enabled'] ) : $arrValues['is_enabled'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_boolIsSystem', trim( stripcslashes( $arrValues['is_system'] ) ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_system'] ) : $arrValues['is_system'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setChecklistTypeId( $intChecklistTypeId ) {
		$this->set( 'm_intChecklistTypeId', CStrings::strToIntDef( $intChecklistTypeId, NULL, false ) );
	}

	public function getChecklistTypeId() {
		return $this->m_intChecklistTypeId;
	}

	public function sqlChecklistTypeId() {
		return ( true == isset( $this->m_intChecklistTypeId ) ) ? ( string ) $this->m_intChecklistTypeId : 'NULL';
	}

	public function setChecklistTriggerId( $intChecklistTriggerId ) {
		$this->set( 'm_intChecklistTriggerId', CStrings::strToIntDef( $intChecklistTriggerId, NULL, false ) );
	}

	public function getChecklistTriggerId() {
		return $this->m_intChecklistTriggerId;
	}

	public function sqlChecklistTriggerId() {
		return ( true == isset( $this->m_intChecklistTriggerId ) ) ? ( string ) $this->m_intChecklistTriggerId : 'NULL';
	}

	public function setOccupancyTypeIds( $arrintOccupancyTypeIds ) {
		$this->set( 'm_arrintOccupancyTypeIds', CStrings::strToArrIntDef( $arrintOccupancyTypeIds, NULL ) );
	}

	public function getOccupancyTypeIds() {
		return $this->m_arrintOccupancyTypeIds;
	}

	public function sqlOccupancyTypeIds() {
		return ( true == isset( $this->m_arrintOccupancyTypeIds ) && true == valArr( $this->m_arrintOccupancyTypeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintOccupancyTypeIds, NULL ) . '\'' : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsEnabled( $boolIsEnabled ) {
		$this->set( 'm_boolIsEnabled', CStrings::strToBool( $boolIsEnabled ) );
	}

	public function getIsEnabled() {
		return $this->m_boolIsEnabled;
	}

	public function sqlIsEnabled() {
		return ( true == isset( $this->m_boolIsEnabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsEnabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSystem( $boolIsSystem ) {
		$this->set( 'm_boolIsSystem', CStrings::strToBool( $boolIsSystem ) );
	}

	public function getIsSystem() {
		return $this->m_boolIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_boolIsSystem ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSystem ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'checklist_type_id' => $this->getChecklistTypeId(),
			'checklist_trigger_id' => $this->getChecklistTriggerId(),
			'occupancy_type_ids' => $this->getOccupancyTypeIds(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'is_enabled' => $this->getIsEnabled(),
			'is_system' => $this->getIsSystem(),
			'details' => $this->getDetails()
		);
	}

}
?>