<?php

class CBaseDefaultLeaseTerm extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_lease_terms';

	protected $m_intId;
	protected $m_intDefaultLeaseTermStructureId;
	protected $m_strName;
	protected $m_intTermMonth;
	protected $m_boolAllowProspectEdit;
	protected $m_boolAllowRenewalEdit;
	protected $m_boolIsProspect;
	protected $m_boolIsRenewal;
	protected $m_boolIsSystem;
	protected $m_boolIsDisabled;
	protected $m_boolIsDefault;
	protected $m_boolIsUnset;
	protected $m_boolShowOnWebsite;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intOccupancyTypeId;
	protected $m_intLeaseTermTypeId;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolAllowProspectEdit = false;
		$this->m_boolAllowRenewalEdit = false;
		$this->m_boolIsProspect = true;
		$this->m_boolIsRenewal = true;
		$this->m_boolIsSystem = false;
		$this->m_boolIsDisabled = false;
		$this->m_boolIsDefault = false;
		$this->m_boolIsUnset = false;
		$this->m_boolShowOnWebsite = true;
		$this->m_intOccupancyTypeId = '1';
		$this->m_intLeaseTermTypeId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['default_lease_term_structure_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultLeaseTermStructureId', trim( $arrValues['default_lease_term_structure_id'] ) ); elseif( isset( $arrValues['default_lease_term_structure_id'] ) ) $this->setDefaultLeaseTermStructureId( $arrValues['default_lease_term_structure_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['term_month'] ) && $boolDirectSet ) $this->set( 'm_intTermMonth', trim( $arrValues['term_month'] ) ); elseif( isset( $arrValues['term_month'] ) ) $this->setTermMonth( $arrValues['term_month'] );
		if( isset( $arrValues['allow_prospect_edit'] ) && $boolDirectSet ) $this->set( 'm_boolAllowProspectEdit', trim( stripcslashes( $arrValues['allow_prospect_edit'] ) ) ); elseif( isset( $arrValues['allow_prospect_edit'] ) ) $this->setAllowProspectEdit( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_prospect_edit'] ) : $arrValues['allow_prospect_edit'] );
		if( isset( $arrValues['allow_renewal_edit'] ) && $boolDirectSet ) $this->set( 'm_boolAllowRenewalEdit', trim( stripcslashes( $arrValues['allow_renewal_edit'] ) ) ); elseif( isset( $arrValues['allow_renewal_edit'] ) ) $this->setAllowRenewalEdit( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_renewal_edit'] ) : $arrValues['allow_renewal_edit'] );
		if( isset( $arrValues['is_prospect'] ) && $boolDirectSet ) $this->set( 'm_boolIsProspect', trim( stripcslashes( $arrValues['is_prospect'] ) ) ); elseif( isset( $arrValues['is_prospect'] ) ) $this->setIsProspect( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_prospect'] ) : $arrValues['is_prospect'] );
		if( isset( $arrValues['is_renewal'] ) && $boolDirectSet ) $this->set( 'm_boolIsRenewal', trim( stripcslashes( $arrValues['is_renewal'] ) ) ); elseif( isset( $arrValues['is_renewal'] ) ) $this->setIsRenewal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_renewal'] ) : $arrValues['is_renewal'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_boolIsSystem', trim( stripcslashes( $arrValues['is_system'] ) ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_system'] ) : $arrValues['is_system'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_boolIsDefault', trim( stripcslashes( $arrValues['is_default'] ) ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_default'] ) : $arrValues['is_default'] );
		if( isset( $arrValues['is_unset'] ) && $boolDirectSet ) $this->set( 'm_boolIsUnset', trim( stripcslashes( $arrValues['is_unset'] ) ) ); elseif( isset( $arrValues['is_unset'] ) ) $this->setIsUnset( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_unset'] ) : $arrValues['is_unset'] );
		if( isset( $arrValues['show_on_website'] ) && $boolDirectSet ) $this->set( 'm_boolShowOnWebsite', trim( stripcslashes( $arrValues['show_on_website'] ) ) ); elseif( isset( $arrValues['show_on_website'] ) ) $this->setShowOnWebsite( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_on_website'] ) : $arrValues['show_on_website'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['lease_term_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTermTypeId', trim( $arrValues['lease_term_type_id'] ) ); elseif( isset( $arrValues['lease_term_type_id'] ) ) $this->setLeaseTermTypeId( $arrValues['lease_term_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDefaultLeaseTermStructureId( $intDefaultLeaseTermStructureId ) {
		$this->set( 'm_intDefaultLeaseTermStructureId', CStrings::strToIntDef( $intDefaultLeaseTermStructureId, NULL, false ) );
	}

	public function getDefaultLeaseTermStructureId() {
		return $this->m_intDefaultLeaseTermStructureId;
	}

	public function sqlDefaultLeaseTermStructureId() {
		return ( true == isset( $this->m_intDefaultLeaseTermStructureId ) ) ? ( string ) $this->m_intDefaultLeaseTermStructureId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, -1, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setTermMonth( $intTermMonth ) {
		$this->set( 'm_intTermMonth', CStrings::strToIntDef( $intTermMonth, NULL, false ) );
	}

	public function getTermMonth() {
		return $this->m_intTermMonth;
	}

	public function sqlTermMonth() {
		return ( true == isset( $this->m_intTermMonth ) ) ? ( string ) $this->m_intTermMonth : 'NULL';
	}

	public function setAllowProspectEdit( $boolAllowProspectEdit ) {
		$this->set( 'm_boolAllowProspectEdit', CStrings::strToBool( $boolAllowProspectEdit ) );
	}

	public function getAllowProspectEdit() {
		return $this->m_boolAllowProspectEdit;
	}

	public function sqlAllowProspectEdit() {
		return ( true == isset( $this->m_boolAllowProspectEdit ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowProspectEdit ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowRenewalEdit( $boolAllowRenewalEdit ) {
		$this->set( 'm_boolAllowRenewalEdit', CStrings::strToBool( $boolAllowRenewalEdit ) );
	}

	public function getAllowRenewalEdit() {
		return $this->m_boolAllowRenewalEdit;
	}

	public function sqlAllowRenewalEdit() {
		return ( true == isset( $this->m_boolAllowRenewalEdit ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowRenewalEdit ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsProspect( $boolIsProspect ) {
		$this->set( 'm_boolIsProspect', CStrings::strToBool( $boolIsProspect ) );
	}

	public function getIsProspect() {
		return $this->m_boolIsProspect;
	}

	public function sqlIsProspect() {
		return ( true == isset( $this->m_boolIsProspect ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsProspect ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsRenewal( $boolIsRenewal ) {
		$this->set( 'm_boolIsRenewal', CStrings::strToBool( $boolIsRenewal ) );
	}

	public function getIsRenewal() {
		return $this->m_boolIsRenewal;
	}

	public function sqlIsRenewal() {
		return ( true == isset( $this->m_boolIsRenewal ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRenewal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSystem( $boolIsSystem ) {
		$this->set( 'm_boolIsSystem', CStrings::strToBool( $boolIsSystem ) );
	}

	public function getIsSystem() {
		return $this->m_boolIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_boolIsSystem ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSystem ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDefault( $boolIsDefault ) {
		$this->set( 'm_boolIsDefault', CStrings::strToBool( $boolIsDefault ) );
	}

	public function getIsDefault() {
		return $this->m_boolIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_boolIsDefault ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDefault ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsUnset( $boolIsUnset ) {
		$this->set( 'm_boolIsUnset', CStrings::strToBool( $boolIsUnset ) );
	}

	public function getIsUnset() {
		return $this->m_boolIsUnset;
	}

	public function sqlIsUnset() {
		return ( true == isset( $this->m_boolIsUnset ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsUnset ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowOnWebsite( $boolShowOnWebsite ) {
		$this->set( 'm_boolShowOnWebsite', CStrings::strToBool( $boolShowOnWebsite ) );
	}

	public function getShowOnWebsite() {
		return $this->m_boolShowOnWebsite;
	}

	public function sqlShowOnWebsite() {
		return ( true == isset( $this->m_boolShowOnWebsite ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowOnWebsite ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : '1';
	}

	public function setLeaseTermTypeId( $intLeaseTermTypeId ) {
		$this->set( 'm_intLeaseTermTypeId', CStrings::strToIntDef( $intLeaseTermTypeId, NULL, false ) );
	}

	public function getLeaseTermTypeId() {
		return $this->m_intLeaseTermTypeId;
	}

	public function sqlLeaseTermTypeId() {
		return ( true == isset( $this->m_intLeaseTermTypeId ) ) ? ( string ) $this->m_intLeaseTermTypeId : '1';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, default_lease_term_structure_id, name, term_month, allow_prospect_edit, allow_renewal_edit, is_prospect, is_renewal, is_system, is_disabled, is_default, is_unset, show_on_website, deleted_by, deleted_on, details, occupancy_type_id, lease_term_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlDefaultLeaseTermStructureId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlTermMonth() . ', ' .
						$this->sqlAllowProspectEdit() . ', ' .
						$this->sqlAllowRenewalEdit() . ', ' .
						$this->sqlIsProspect() . ', ' .
						$this->sqlIsRenewal() . ', ' .
						$this->sqlIsSystem() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						$this->sqlIsDefault() . ', ' .
						$this->sqlIsUnset() . ', ' .
						$this->sqlShowOnWebsite() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlOccupancyTypeId() . ', ' .
						$this->sqlLeaseTermTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_lease_term_structure_id = ' . $this->sqlDefaultLeaseTermStructureId(). ',' ; } elseif( true == array_key_exists( 'DefaultLeaseTermStructureId', $this->getChangedColumns() ) ) { $strSql .= ' default_lease_term_structure_id = ' . $this->sqlDefaultLeaseTermStructureId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' term_month = ' . $this->sqlTermMonth(). ',' ; } elseif( true == array_key_exists( 'TermMonth', $this->getChangedColumns() ) ) { $strSql .= ' term_month = ' . $this->sqlTermMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_prospect_edit = ' . $this->sqlAllowProspectEdit(). ',' ; } elseif( true == array_key_exists( 'AllowProspectEdit', $this->getChangedColumns() ) ) { $strSql .= ' allow_prospect_edit = ' . $this->sqlAllowProspectEdit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_renewal_edit = ' . $this->sqlAllowRenewalEdit(). ',' ; } elseif( true == array_key_exists( 'AllowRenewalEdit', $this->getChangedColumns() ) ) { $strSql .= ' allow_renewal_edit = ' . $this->sqlAllowRenewalEdit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_prospect = ' . $this->sqlIsProspect(). ',' ; } elseif( true == array_key_exists( 'IsProspect', $this->getChangedColumns() ) ) { $strSql .= ' is_prospect = ' . $this->sqlIsProspect() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_renewal = ' . $this->sqlIsRenewal(). ',' ; } elseif( true == array_key_exists( 'IsRenewal', $this->getChangedColumns() ) ) { $strSql .= ' is_renewal = ' . $this->sqlIsRenewal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem(). ',' ; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault(). ',' ; } elseif( true == array_key_exists( 'IsDefault', $this->getChangedColumns() ) ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_unset = ' . $this->sqlIsUnset(). ',' ; } elseif( true == array_key_exists( 'IsUnset', $this->getChangedColumns() ) ) { $strSql .= ' is_unset = ' . $this->sqlIsUnset() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite(). ',' ; } elseif( true == array_key_exists( 'ShowOnWebsite', $this->getChangedColumns() ) ) { $strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_term_type_id = ' . $this->sqlLeaseTermTypeId() ; } elseif( true == array_key_exists( 'LeaseTermTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lease_term_type_id = ' . $this->sqlLeaseTermTypeId() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'default_lease_term_structure_id' => $this->getDefaultLeaseTermStructureId(),
			'name' => $this->getName(),
			'term_month' => $this->getTermMonth(),
			'allow_prospect_edit' => $this->getAllowProspectEdit(),
			'allow_renewal_edit' => $this->getAllowRenewalEdit(),
			'is_prospect' => $this->getIsProspect(),
			'is_renewal' => $this->getIsRenewal(),
			'is_system' => $this->getIsSystem(),
			'is_disabled' => $this->getIsDisabled(),
			'is_default' => $this->getIsDefault(),
			'is_unset' => $this->getIsUnset(),
			'show_on_website' => $this->getShowOnWebsite(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'details' => $this->getDetails(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'lease_term_type_id' => $this->getLeaseTermTypeId()
		);
	}

}
?>