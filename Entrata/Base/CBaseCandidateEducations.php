<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCandidateEducations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCandidateEducations extends CEosPluralBase {

	/**
	 * @return CCandidateEducation[]
	 */
	public static function fetchCandidateEducations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCandidateEducation', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCandidateEducation
	 */
	public static function fetchCandidateEducation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCandidateEducation', $objDatabase );
	}

	public static function fetchCandidateEducationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'candidate_educations', $objDatabase );
	}

	public static function fetchCandidateEducationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCandidateEducation( sprintf( 'SELECT * FROM candidate_educations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCandidateEducationsByCid( $intCid, $objDatabase ) {
		return self::fetchCandidateEducations( sprintf( 'SELECT * FROM candidate_educations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCandidateEducationsByCandidateIdByCid( $intCandidateId, $intCid, $objDatabase ) {
		return self::fetchCandidateEducations( sprintf( 'SELECT * FROM candidate_educations WHERE candidate_id = %d AND cid = %d', ( int ) $intCandidateId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCandidateEducationsByCandidateEducationTypeIdByCid( $intCandidateEducationTypeId, $intCid, $objDatabase ) {
		return self::fetchCandidateEducations( sprintf( 'SELECT * FROM candidate_educations WHERE candidate_education_type_id = %d AND cid = %d', ( int ) $intCandidateEducationTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>