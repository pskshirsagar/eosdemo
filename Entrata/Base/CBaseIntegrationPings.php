<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationPings
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseIntegrationPings extends CEosPluralBase {

	/**
	 * @return CIntegrationPing[]
	 */
	public static function fetchIntegrationPings( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CIntegrationPing', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CIntegrationPing
	 */
	public static function fetchIntegrationPing( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CIntegrationPing', $objDatabase );
	}

	public static function fetchIntegrationPingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'integration_pings', $objDatabase );
	}

	public static function fetchIntegrationPingByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchIntegrationPing( sprintf( 'SELECT * FROM integration_pings WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationPingsByCid( $intCid, $objDatabase ) {
		return self::fetchIntegrationPings( sprintf( 'SELECT * FROM integration_pings WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationPingsByIntegrationClientIdByCid( $intIntegrationClientId, $intCid, $objDatabase ) {
		return self::fetchIntegrationPings( sprintf( 'SELECT * FROM integration_pings WHERE integration_client_id = %d AND cid = %d', ( int ) $intIntegrationClientId, ( int ) $intCid ), $objDatabase );
	}

}
?>