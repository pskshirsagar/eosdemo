<?php

class CBaseDefaultReportInstance extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_report_instances';

	protected $m_intId;
	protected $m_intDefaultReportGroupId;
	protected $m_intDefaultReportId;
	protected $m_intDefaultReportVersionId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['default_report_group_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultReportGroupId', trim( $arrValues['default_report_group_id'] ) ); elseif( isset( $arrValues['default_report_group_id'] ) ) $this->setDefaultReportGroupId( $arrValues['default_report_group_id'] );
		if( isset( $arrValues['default_report_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultReportId', trim( $arrValues['default_report_id'] ) ); elseif( isset( $arrValues['default_report_id'] ) ) $this->setDefaultReportId( $arrValues['default_report_id'] );
		if( isset( $arrValues['default_report_version_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultReportVersionId', trim( $arrValues['default_report_version_id'] ) ); elseif( isset( $arrValues['default_report_version_id'] ) ) $this->setDefaultReportVersionId( $arrValues['default_report_version_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDefaultReportGroupId( $intDefaultReportGroupId ) {
		$this->set( 'm_intDefaultReportGroupId', CStrings::strToIntDef( $intDefaultReportGroupId, NULL, false ) );
	}

	public function getDefaultReportGroupId() {
		return $this->m_intDefaultReportGroupId;
	}

	public function sqlDefaultReportGroupId() {
		return ( true == isset( $this->m_intDefaultReportGroupId ) ) ? ( string ) $this->m_intDefaultReportGroupId : 'NULL';
	}

	public function setDefaultReportId( $intDefaultReportId ) {
		$this->set( 'm_intDefaultReportId', CStrings::strToIntDef( $intDefaultReportId, NULL, false ) );
	}

	public function getDefaultReportId() {
		return $this->m_intDefaultReportId;
	}

	public function sqlDefaultReportId() {
		return ( true == isset( $this->m_intDefaultReportId ) ) ? ( string ) $this->m_intDefaultReportId : 'NULL';
	}

	public function setDefaultReportVersionId( $intDefaultReportVersionId ) {
		$this->set( 'm_intDefaultReportVersionId', CStrings::strToIntDef( $intDefaultReportVersionId, NULL, false ) );
	}

	public function getDefaultReportVersionId() {
		return $this->m_intDefaultReportVersionId;
	}

	public function sqlDefaultReportVersionId() {
		return ( true == isset( $this->m_intDefaultReportVersionId ) ) ? ( string ) $this->m_intDefaultReportVersionId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'default_report_group_id' => $this->getDefaultReportGroupId(),
			'default_report_id' => $this->getDefaultReportId(),
			'default_report_version_id' => $this->getDefaultReportVersionId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'details' => $this->getDetails()
		);
	}

}
?>