<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPaymentPrintingOrderTypes
 * Do not add any new functions to this class.
 */

class CBaseApPaymentPrintingOrderTypes extends CEosPluralBase {

	/**
	 * @return CApPaymentPrintingOrderType[]
	 */
	public static function fetchApPaymentPrintingOrderTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CApPaymentPrintingOrderType::class, $objDatabase );
	}

	/**
	 * @return CApPaymentPrintingOrderType
	 */
	public static function fetchApPaymentPrintingOrderType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApPaymentPrintingOrderType::class, $objDatabase );
	}

	public static function fetchApPaymentPrintingOrderTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_payment_printing_order_types', $objDatabase );
	}

	public static function fetchApPaymentPrintingOrderTypeById( $intId, $objDatabase ) {
		return self::fetchApPaymentPrintingOrderType( sprintf( 'SELECT * FROM ap_payment_printing_order_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>