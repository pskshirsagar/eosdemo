<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAssetLedgers extends CEosPluralBase {

	public static function fetchAssetLedgers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CAssetLedger', $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchAssetLedger( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAssetLedger', $objDatabase );
	}

	public static function fetchAssetLedgerCount( $strWhere = NULL, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'asset_ledgers', $objDatabase );
	}

	public static function fetchAssetLedgerByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchAssetLedger( sprintf( 'SELECT * FROM asset_ledgers WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
	}

	public static function fetchAssetLedgersByCid( $intCid, $objDatabase ) {
		return self::fetchAssetLedgers( sprintf( 'SELECT * FROM asset_ledgers WHERE cid = %d', (int) $intCid ), $objDatabase );
	}

	public static function fetchAssetLedgersByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchAssetLedgers( sprintf( 'SELECT * FROM asset_ledgers WHERE property_id = %d AND cid = %d', (int) $intPropertyId, (int) $intCid ), $objDatabase );
	}

	public static function fetchAssetLedgersByAssetLedgerTypeIdByCid( $intAssetLedgerTypeId, $intCid, $objDatabase ) {
		return self::fetchAssetLedgers( sprintf( 'SELECT * FROM asset_ledgers WHERE asset_ledger_type_id = %d AND cid = %d', (int) $intAssetLedgerTypeId, (int) $intCid ), $objDatabase );
	}

	public static function fetchAssetLedgersByApCodeIdByCid( $intApCodeId, $intCid, $objDatabase ) {
		return self::fetchAssetLedgers( sprintf( 'SELECT * FROM asset_ledgers WHERE ap_code_id = %d AND cid = %d', (int) $intApCodeId, (int) $intCid ), $objDatabase );
	}

	public static function fetchAssetLedgersByAssetIdByCid( $intAssetId, $intCid, $objDatabase ) {
		return self::fetchAssetLedgers( sprintf( 'SELECT * FROM asset_ledgers WHERE asset_id = %d AND cid = %d', (int) $intAssetId, (int) $intCid ), $objDatabase );
	}

}
?>