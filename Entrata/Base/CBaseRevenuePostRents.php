<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenuePostRents
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRevenuePostRents extends CEosPluralBase {

	/**
	 * @return CRevenuePostRent[]
	 */
	public static function fetchRevenuePostRents( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CRevenuePostRent::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRevenuePostRent
	 */
	public static function fetchRevenuePostRent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRevenuePostRent::class, $objDatabase );
	}

	public static function fetchRevenuePostRentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_post_rents', $objDatabase );
	}

	public static function fetchRevenuePostRentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRevenuePostRent( sprintf( 'SELECT * FROM revenue_post_rents WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchRevenuePostRentsByCid( $intCid, $objDatabase ) {
		return self::fetchRevenuePostRents( sprintf( 'SELECT * FROM revenue_post_rents WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchRevenuePostRentsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchRevenuePostRents( sprintf( 'SELECT * FROM revenue_post_rents WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchRevenuePostRentsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchRevenuePostRents( sprintf( 'SELECT * FROM revenue_post_rents WHERE unit_type_id = %d AND cid = %d', $intUnitTypeId, $intCid ), $objDatabase );
	}

	public static function fetchRevenuePostRentsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchRevenuePostRents( sprintf( 'SELECT * FROM revenue_post_rents WHERE unit_space_id = %d AND cid = %d', $intUnitSpaceId, $intCid ), $objDatabase );
	}

	public static function fetchRevenuePostRentsByBatchIdByCid( $intBatchId, $intCid, $objDatabase ) {
		return self::fetchRevenuePostRents( sprintf( 'SELECT * FROM revenue_post_rents WHERE batch_id = %d AND cid = %d', $intBatchId, $intCid ), $objDatabase );
	}

}
?>