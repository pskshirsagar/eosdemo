<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyMaintenanceTemplate extends CEosSingularBase {

	const TABLE_NAME = 'public.property_maintenance_templates';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intMaintenanceTemplateId;
	protected $m_intPropertyId;
	protected $m_intPropertyMaintenanceTemplateId;
	protected $m_intMaintenanceGroupTypeId;
	protected $m_intGroupCount;
	protected $m_intIsDefault;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intGroupCount = '1';
		$this->m_intIsDefault = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['maintenance_template_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceTemplateId', trim( $arrValues['maintenance_template_id'] ) ); elseif( isset( $arrValues['maintenance_template_id'] ) ) $this->setMaintenanceTemplateId( $arrValues['maintenance_template_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_maintenance_template_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyMaintenanceTemplateId', trim( $arrValues['property_maintenance_template_id'] ) ); elseif( isset( $arrValues['property_maintenance_template_id'] ) ) $this->setPropertyMaintenanceTemplateId( $arrValues['property_maintenance_template_id'] );
		if( isset( $arrValues['maintenance_group_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceGroupTypeId', trim( $arrValues['maintenance_group_type_id'] ) ); elseif( isset( $arrValues['maintenance_group_type_id'] ) ) $this->setMaintenanceGroupTypeId( $arrValues['maintenance_group_type_id'] );
		if( isset( $arrValues['group_count'] ) && $boolDirectSet ) $this->set( 'm_intGroupCount', trim( $arrValues['group_count'] ) ); elseif( isset( $arrValues['group_count'] ) ) $this->setGroupCount( $arrValues['group_count'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_intIsDefault', trim( $arrValues['is_default'] ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( $arrValues['is_default'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setMaintenanceTemplateId( $intMaintenanceTemplateId ) {
		$this->set( 'm_intMaintenanceTemplateId', CStrings::strToIntDef( $intMaintenanceTemplateId, NULL, false ) );
	}

	public function getMaintenanceTemplateId() {
		return $this->m_intMaintenanceTemplateId;
	}

	public function sqlMaintenanceTemplateId() {
		return ( true == isset( $this->m_intMaintenanceTemplateId ) ) ? ( string ) $this->m_intMaintenanceTemplateId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyMaintenanceTemplateId( $intPropertyMaintenanceTemplateId ) {
		$this->set( 'm_intPropertyMaintenanceTemplateId', CStrings::strToIntDef( $intPropertyMaintenanceTemplateId, NULL, false ) );
	}

	public function getPropertyMaintenanceTemplateId() {
		return $this->m_intPropertyMaintenanceTemplateId;
	}

	public function sqlPropertyMaintenanceTemplateId() {
		return ( true == isset( $this->m_intPropertyMaintenanceTemplateId ) ) ? ( string ) $this->m_intPropertyMaintenanceTemplateId : 'NULL';
	}

	public function setMaintenanceGroupTypeId( $intMaintenanceGroupTypeId ) {
		$this->set( 'm_intMaintenanceGroupTypeId', CStrings::strToIntDef( $intMaintenanceGroupTypeId, NULL, false ) );
	}

	public function getMaintenanceGroupTypeId() {
		return $this->m_intMaintenanceGroupTypeId;
	}

	public function sqlMaintenanceGroupTypeId() {
		return ( true == isset( $this->m_intMaintenanceGroupTypeId ) ) ? ( string ) $this->m_intMaintenanceGroupTypeId : 'NULL';
	}

	public function setGroupCount( $intGroupCount ) {
		$this->set( 'm_intGroupCount', CStrings::strToIntDef( $intGroupCount, NULL, false ) );
	}

	public function getGroupCount() {
		return $this->m_intGroupCount;
	}

	public function sqlGroupCount() {
		return ( true == isset( $this->m_intGroupCount ) ) ? ( string ) $this->m_intGroupCount : '1';
	}

	public function setIsDefault( $intIsDefault ) {
		$this->set( 'm_intIsDefault', CStrings::strToIntDef( $intIsDefault, NULL, false ) );
	}

	public function getIsDefault() {
		return $this->m_intIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_intIsDefault ) ) ? ( string ) $this->m_intIsDefault : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, maintenance_template_id, property_id, property_maintenance_template_id, maintenance_group_type_id, group_count, is_default, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlMaintenanceTemplateId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyMaintenanceTemplateId() . ', ' .
 						$this->sqlMaintenanceGroupTypeId() . ', ' .
 						$this->sqlGroupCount() . ', ' .
 						$this->sqlIsDefault() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_template_id = ' . $this->sqlMaintenanceTemplateId() . ','; } elseif( true == array_key_exists( 'MaintenanceTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_template_id = ' . $this->sqlMaintenanceTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_maintenance_template_id = ' . $this->sqlPropertyMaintenanceTemplateId() . ','; } elseif( true == array_key_exists( 'PropertyMaintenanceTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' property_maintenance_template_id = ' . $this->sqlPropertyMaintenanceTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_group_type_id = ' . $this->sqlMaintenanceGroupTypeId() . ','; } elseif( true == array_key_exists( 'MaintenanceGroupTypeId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_group_type_id = ' . $this->sqlMaintenanceGroupTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' group_count = ' . $this->sqlGroupCount() . ','; } elseif( true == array_key_exists( 'GroupCount', $this->getChangedColumns() ) ) { $strSql .= ' group_count = ' . $this->sqlGroupCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; } elseif( true == array_key_exists( 'IsDefault', $this->getChangedColumns() ) ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'maintenance_template_id' => $this->getMaintenanceTemplateId(),
			'property_id' => $this->getPropertyId(),
			'property_maintenance_template_id' => $this->getPropertyMaintenanceTemplateId(),
			'maintenance_group_type_id' => $this->getMaintenanceGroupTypeId(),
			'group_count' => $this->getGroupCount(),
			'is_default' => $this->getIsDefault(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>