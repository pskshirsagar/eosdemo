<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMenuIcons
 * Do not add any new functions to this class.
 */

class CBaseMenuIcons extends CEosPluralBase {

	/**
	 * @return CMenuIcon[]
	 */
	public static function fetchMenuIcons( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMenuIcon::class, $objDatabase );
	}

	/**
	 * @return CMenuIcon
	 */
	public static function fetchMenuIcon( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMenuIcon::class, $objDatabase );
	}

	public static function fetchMenuIconCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'menu_icons', $objDatabase );
	}

	public static function fetchMenuIconById( $intId, $objDatabase ) {
		return self::fetchMenuIcon( sprintf( 'SELECT * FROM menu_icons WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchMenuIconsByAdministrativeMenuTypeId( $intAdministrativeMenuTypeId, $objDatabase ) {
		return self::fetchMenuIcons( sprintf( 'SELECT * FROM menu_icons WHERE administrative_menu_type_id = %d', $intAdministrativeMenuTypeId ), $objDatabase );
	}

}
?>