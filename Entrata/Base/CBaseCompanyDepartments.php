<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyDepartments
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyDepartments extends CEosPluralBase {

	/**
	 * @return CCompanyDepartment[]
	 */
	public static function fetchCompanyDepartments( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCompanyDepartment::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyDepartment
	 */
	public static function fetchCompanyDepartment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyDepartment::class, $objDatabase );
	}

	public static function fetchCompanyDepartmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_departments', $objDatabase );
	}

	public static function fetchCompanyDepartmentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyDepartment( sprintf( 'SELECT * FROM company_departments WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCompanyDepartmentsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyDepartments( sprintf( 'SELECT * FROM company_departments WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCompanyDepartmentsByDefaultCompanyDepartmentIdByCid( $intDefaultCompanyDepartmentId, $intCid, $objDatabase ) {
		return self::fetchCompanyDepartments( sprintf( 'SELECT * FROM company_departments WHERE default_company_department_id = %d AND cid = %d', $intDefaultCompanyDepartmentId, $intCid ), $objDatabase );
	}

}
?>