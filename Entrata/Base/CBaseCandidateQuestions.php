<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCandidateQuestions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCandidateQuestions extends CEosPluralBase {

	/**
	 * @return CCandidateQuestion[]
	 */
	public static function fetchCandidateQuestions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCandidateQuestion', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCandidateQuestion
	 */
	public static function fetchCandidateQuestion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCandidateQuestion', $objDatabase );
	}

	public static function fetchCandidateQuestionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'candidate_questions', $objDatabase );
	}

	public static function fetchCandidateQuestionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCandidateQuestion( sprintf( 'SELECT * FROM candidate_questions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCandidateQuestionsByCid( $intCid, $objDatabase ) {
		return self::fetchCandidateQuestions( sprintf( 'SELECT * FROM candidate_questions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCandidateQuestionsByCandidateIdByCid( $intCandidateId, $intCid, $objDatabase ) {
		return self::fetchCandidateQuestions( sprintf( 'SELECT * FROM candidate_questions WHERE candidate_id = %d AND cid = %d', ( int ) $intCandidateId, ( int ) $intCid ), $objDatabase );
	}

}
?>