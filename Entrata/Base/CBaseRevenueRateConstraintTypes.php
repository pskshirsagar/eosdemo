<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueRateConstraintTypes
 * Do not add any new functions to this class.
 */

class CBaseRevenueRateConstraintTypes extends CEosPluralBase {

	/**
	 * @return CRevenueRateConstraintType[]
	 */
	public static function fetchRevenueRateConstraintTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRevenueRateConstraintType::class, $objDatabase );
	}

	/**
	 * @return CRevenueRateConstraintType
	 */
	public static function fetchRevenueRateConstraintType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRevenueRateConstraintType::class, $objDatabase );
	}

	public static function fetchRevenueRateConstraintTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_rate_constraint_types', $objDatabase );
	}

	public static function fetchRevenueRateConstraintTypeById( $intId, $objDatabase ) {
		return self::fetchRevenueRateConstraintType( sprintf( 'SELECT * FROM revenue_rate_constraint_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>