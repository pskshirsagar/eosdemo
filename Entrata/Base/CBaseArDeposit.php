<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseArDeposit extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.ar_deposits';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intBankAccountId;
	protected $m_intGlTransactionTypeId;
	protected $m_intArDepositTypeId;
	protected $m_intSettlementDistributionId;
	protected $m_intOriginArDepositId;
	protected $m_strRemotePrimaryKey;
	protected $m_intDepositNumber;
	protected $m_strTransactionDatetime;
	protected $m_fltDepositAmount;
	protected $m_strPostDate;
	protected $m_strPostMonth;
	protected $m_strDepositMemo;
	protected $m_intDetailsInserted;
	protected $m_intIsPosted;
	protected $m_intIsInitialImport;
	protected $m_intIsDeleted;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_fltDepositAmount = '0';
		$this->m_intDetailsInserted = '0';
		$this->m_intIsPosted = '0';
		$this->m_intIsInitialImport = '0';
		$this->m_intIsDeleted = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBankAccountId', trim( $arrValues['bank_account_id'] ) ); elseif( isset( $arrValues['bank_account_id'] ) ) $this->setBankAccountId( $arrValues['bank_account_id'] );
		if( isset( $arrValues['gl_transaction_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlTransactionTypeId', trim( $arrValues['gl_transaction_type_id'] ) ); elseif( isset( $arrValues['gl_transaction_type_id'] ) ) $this->setGlTransactionTypeId( $arrValues['gl_transaction_type_id'] );
		if( isset( $arrValues['ar_deposit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intArDepositTypeId', trim( $arrValues['ar_deposit_type_id'] ) ); elseif( isset( $arrValues['ar_deposit_type_id'] ) ) $this->setArDepositTypeId( $arrValues['ar_deposit_type_id'] );
		if( isset( $arrValues['settlement_distribution_id'] ) && $boolDirectSet ) $this->set( 'm_intSettlementDistributionId', trim( $arrValues['settlement_distribution_id'] ) ); elseif( isset( $arrValues['settlement_distribution_id'] ) ) $this->setSettlementDistributionId( $arrValues['settlement_distribution_id'] );
		if( isset( $arrValues['origin_ar_deposit_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginArDepositId', trim( $arrValues['origin_ar_deposit_id'] ) ); elseif( isset( $arrValues['origin_ar_deposit_id'] ) ) $this->setOriginArDepositId( $arrValues['origin_ar_deposit_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['deposit_number'] ) && $boolDirectSet ) $this->set( 'm_intDepositNumber', trim( $arrValues['deposit_number'] ) ); elseif( isset( $arrValues['deposit_number'] ) ) $this->setDepositNumber( $arrValues['deposit_number'] );
		if( isset( $arrValues['transaction_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTransactionDatetime', trim( $arrValues['transaction_datetime'] ) ); elseif( isset( $arrValues['transaction_datetime'] ) ) $this->setTransactionDatetime( $arrValues['transaction_datetime'] );
		if( isset( $arrValues['deposit_amount'] ) && $boolDirectSet ) $this->set( 'm_fltDepositAmount', trim( $arrValues['deposit_amount'] ) ); elseif( isset( $arrValues['deposit_amount'] ) ) $this->setDepositAmount( $arrValues['deposit_amount'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['deposit_memo'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDepositMemo', trim( stripcslashes( $arrValues['deposit_memo'] ) ) ); elseif( isset( $arrValues['deposit_memo'] ) ) $this->setDepositMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['deposit_memo'] ) : $arrValues['deposit_memo'] );
		if( isset( $arrValues['details_inserted'] ) && $boolDirectSet ) $this->set( 'm_intDetailsInserted', trim( $arrValues['details_inserted'] ) ); elseif( isset( $arrValues['details_inserted'] ) ) $this->setDetailsInserted( $arrValues['details_inserted'] );
		if( isset( $arrValues['is_posted'] ) && $boolDirectSet ) $this->set( 'm_intIsPosted', trim( $arrValues['is_posted'] ) ); elseif( isset( $arrValues['is_posted'] ) ) $this->setIsPosted( $arrValues['is_posted'] );
		if( isset( $arrValues['is_initial_import'] ) && $boolDirectSet ) $this->set( 'm_intIsInitialImport', trim( $arrValues['is_initial_import'] ) ); elseif( isset( $arrValues['is_initial_import'] ) ) $this->setIsInitialImport( $arrValues['is_initial_import'] );
		if( isset( $arrValues['is_deleted'] ) && $boolDirectSet ) $this->set( 'm_intIsDeleted', trim( $arrValues['is_deleted'] ) ); elseif( isset( $arrValues['is_deleted'] ) ) $this->setIsDeleted( $arrValues['is_deleted'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setBankAccountId( $intBankAccountId ) {
		$this->set( 'm_intBankAccountId', CStrings::strToIntDef( $intBankAccountId, NULL, false ) );
	}

	public function getBankAccountId() {
		return $this->m_intBankAccountId;
	}

	public function sqlBankAccountId() {
		return ( true == isset( $this->m_intBankAccountId ) ) ? ( string ) $this->m_intBankAccountId : 'NULL';
	}

	public function setGlTransactionTypeId( $intGlTransactionTypeId ) {
		$this->set( 'm_intGlTransactionTypeId', CStrings::strToIntDef( $intGlTransactionTypeId, NULL, false ) );
	}

	public function getGlTransactionTypeId() {
		return $this->m_intGlTransactionTypeId;
	}

	public function sqlGlTransactionTypeId() {
		return ( true == isset( $this->m_intGlTransactionTypeId ) ) ? ( string ) $this->m_intGlTransactionTypeId : 'NULL';
	}

	public function setArDepositTypeId( $intArDepositTypeId ) {
		$this->set( 'm_intArDepositTypeId', CStrings::strToIntDef( $intArDepositTypeId, NULL, false ) );
	}

	public function getArDepositTypeId() {
		return $this->m_intArDepositTypeId;
	}

	public function sqlArDepositTypeId() {
		return ( true == isset( $this->m_intArDepositTypeId ) ) ? ( string ) $this->m_intArDepositTypeId : 'NULL';
	}

	public function setSettlementDistributionId( $intSettlementDistributionId ) {
		$this->set( 'm_intSettlementDistributionId', CStrings::strToIntDef( $intSettlementDistributionId, NULL, false ) );
	}

	public function getSettlementDistributionId() {
		return $this->m_intSettlementDistributionId;
	}

	public function sqlSettlementDistributionId() {
		return ( true == isset( $this->m_intSettlementDistributionId ) ) ? ( string ) $this->m_intSettlementDistributionId : 'NULL';
	}

	public function setOriginArDepositId( $intOriginArDepositId ) {
		$this->set( 'm_intOriginArDepositId', CStrings::strToIntDef( $intOriginArDepositId, NULL, false ) );
	}

	public function getOriginArDepositId() {
		return $this->m_intOriginArDepositId;
	}

	public function sqlOriginArDepositId() {
		return ( true == isset( $this->m_intOriginArDepositId ) ) ? ( string ) $this->m_intOriginArDepositId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setDepositNumber( $intDepositNumber ) {
		$this->set( 'm_intDepositNumber', CStrings::strToIntDef( $intDepositNumber, NULL, false ) );
	}

	public function getDepositNumber() {
		return $this->m_intDepositNumber;
	}

	public function sqlDepositNumber() {
		return ( true == isset( $this->m_intDepositNumber ) ) ? ( string ) $this->m_intDepositNumber : 'NULL';
	}

	public function setTransactionDatetime( $strTransactionDatetime ) {
		$this->set( 'm_strTransactionDatetime', CStrings::strTrimDef( $strTransactionDatetime, -1, NULL, true ) );
	}

	public function getTransactionDatetime() {
		return $this->m_strTransactionDatetime;
	}

	public function sqlTransactionDatetime() {
		return ( true == isset( $this->m_strTransactionDatetime ) ) ? '\'' . $this->m_strTransactionDatetime . '\'' : 'NOW()';
	}

	public function setDepositAmount( $fltDepositAmount ) {
		$this->set( 'm_fltDepositAmount', CStrings::strToFloatDef( $fltDepositAmount, NULL, false, 2 ) );
	}

	public function getDepositAmount() {
		return $this->m_fltDepositAmount;
	}

	public function sqlDepositAmount() {
		return ( true == isset( $this->m_fltDepositAmount ) ) ? ( string ) $this->m_fltDepositAmount : '0';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setDepositMemo( $strDepositMemo, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDepositMemo', CStrings::strTrimDef( $strDepositMemo, 2000, NULL, true ), $strLocaleCode );
	}

	public function getDepositMemo( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDepositMemo', $strLocaleCode );
	}

	public function sqlDepositMemo() {
		return ( true == isset( $this->m_strDepositMemo ) ) ? '\'' . addslashes( $this->m_strDepositMemo ) . '\'' : 'NULL';
	}

	public function setDetailsInserted( $intDetailsInserted ) {
		$this->set( 'm_intDetailsInserted', CStrings::strToIntDef( $intDetailsInserted, NULL, false ) );
	}

	public function getDetailsInserted() {
		return $this->m_intDetailsInserted;
	}

	public function sqlDetailsInserted() {
		return ( true == isset( $this->m_intDetailsInserted ) ) ? ( string ) $this->m_intDetailsInserted : '0';
	}

	public function setIsPosted( $intIsPosted ) {
		$this->set( 'm_intIsPosted', CStrings::strToIntDef( $intIsPosted, NULL, false ) );
	}

	public function getIsPosted() {
		return $this->m_intIsPosted;
	}

	public function sqlIsPosted() {
		return ( true == isset( $this->m_intIsPosted ) ) ? ( string ) $this->m_intIsPosted : '0';
	}

	public function setIsInitialImport( $intIsInitialImport ) {
		$this->set( 'm_intIsInitialImport', CStrings::strToIntDef( $intIsInitialImport, NULL, false ) );
	}

	public function getIsInitialImport() {
		return $this->m_intIsInitialImport;
	}

	public function sqlIsInitialImport() {
		return ( true == isset( $this->m_intIsInitialImport ) ) ? ( string ) $this->m_intIsInitialImport : '0';
	}

	public function setIsDeleted( $intIsDeleted ) {
		$this->set( 'm_intIsDeleted', CStrings::strToIntDef( $intIsDeleted, NULL, false ) );
	}

	public function getIsDeleted() {
		return $this->m_intIsDeleted;
	}

	public function sqlIsDeleted() {
		return ( true == isset( $this->m_intIsDeleted ) ) ? ( string ) $this->m_intIsDeleted : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, bank_account_id, gl_transaction_type_id, ar_deposit_type_id, settlement_distribution_id, origin_ar_deposit_id, remote_primary_key, deposit_number, transaction_datetime, deposit_amount, post_date, post_month, deposit_memo, details_inserted, is_posted, is_initial_import, is_deleted, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlBankAccountId() . ', ' .
						$this->sqlGlTransactionTypeId() . ', ' .
						$this->sqlArDepositTypeId() . ', ' .
						$this->sqlSettlementDistributionId() . ', ' .
						$this->sqlOriginArDepositId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlDepositNumber() . ', ' .
						$this->sqlTransactionDatetime() . ', ' .
						$this->sqlDepositAmount() . ', ' .
						$this->sqlPostDate() . ', ' .
						$this->sqlPostMonth() . ', ' .
						$this->sqlDepositMemo() . ', ' .
						$this->sqlDetailsInserted() . ', ' .
						$this->sqlIsPosted() . ', ' .
						$this->sqlIsInitialImport() . ', ' .
						$this->sqlIsDeleted() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_account_id = ' . $this->sqlBankAccountId(). ',' ; } elseif( true == array_key_exists( 'BankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' bank_account_id = ' . $this->sqlBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_transaction_type_id = ' . $this->sqlGlTransactionTypeId(). ',' ; } elseif( true == array_key_exists( 'GlTransactionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_transaction_type_id = ' . $this->sqlGlTransactionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_deposit_type_id = ' . $this->sqlArDepositTypeId(). ',' ; } elseif( true == array_key_exists( 'ArDepositTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_deposit_type_id = ' . $this->sqlArDepositTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' settlement_distribution_id = ' . $this->sqlSettlementDistributionId(). ',' ; } elseif( true == array_key_exists( 'SettlementDistributionId', $this->getChangedColumns() ) ) { $strSql .= ' settlement_distribution_id = ' . $this->sqlSettlementDistributionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' origin_ar_deposit_id = ' . $this->sqlOriginArDepositId(). ',' ; } elseif( true == array_key_exists( 'OriginArDepositId', $this->getChangedColumns() ) ) { $strSql .= ' origin_ar_deposit_id = ' . $this->sqlOriginArDepositId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit_number = ' . $this->sqlDepositNumber(). ',' ; } elseif( true == array_key_exists( 'DepositNumber', $this->getChangedColumns() ) ) { $strSql .= ' deposit_number = ' . $this->sqlDepositNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime(). ',' ; } elseif( true == array_key_exists( 'TransactionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit_amount = ' . $this->sqlDepositAmount(). ',' ; } elseif( true == array_key_exists( 'DepositAmount', $this->getChangedColumns() ) ) { $strSql .= ' deposit_amount = ' . $this->sqlDepositAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate(). ',' ; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth(). ',' ; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit_memo = ' . $this->sqlDepositMemo(). ',' ; } elseif( true == array_key_exists( 'DepositMemo', $this->getChangedColumns() ) ) { $strSql .= ' deposit_memo = ' . $this->sqlDepositMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details_inserted = ' . $this->sqlDetailsInserted(). ',' ; } elseif( true == array_key_exists( 'DetailsInserted', $this->getChangedColumns() ) ) { $strSql .= ' details_inserted = ' . $this->sqlDetailsInserted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_posted = ' . $this->sqlIsPosted(). ',' ; } elseif( true == array_key_exists( 'IsPosted', $this->getChangedColumns() ) ) { $strSql .= ' is_posted = ' . $this->sqlIsPosted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_initial_import = ' . $this->sqlIsInitialImport(). ',' ; } elseif( true == array_key_exists( 'IsInitialImport', $this->getChangedColumns() ) ) { $strSql .= ' is_initial_import = ' . $this->sqlIsInitialImport() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted(). ',' ; } elseif( true == array_key_exists( 'IsDeleted', $this->getChangedColumns() ) ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'bank_account_id' => $this->getBankAccountId(),
			'gl_transaction_type_id' => $this->getGlTransactionTypeId(),
			'ar_deposit_type_id' => $this->getArDepositTypeId(),
			'settlement_distribution_id' => $this->getSettlementDistributionId(),
			'origin_ar_deposit_id' => $this->getOriginArDepositId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'deposit_number' => $this->getDepositNumber(),
			'transaction_datetime' => $this->getTransactionDatetime(),
			'deposit_amount' => $this->getDepositAmount(),
			'post_date' => $this->getPostDate(),
			'post_month' => $this->getPostMonth(),
			'deposit_memo' => $this->getDepositMemo(),
			'details_inserted' => $this->getDetailsInserted(),
			'is_posted' => $this->getIsPosted(),
			'is_initial_import' => $this->getIsInitialImport(),
			'is_deleted' => $this->getIsDeleted(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>