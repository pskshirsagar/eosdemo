<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSiteTabletStats
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSiteTabletStats extends CEosPluralBase {

	/**
	 * @return CSiteTabletStat[]
	 */
	public static function fetchSiteTabletStats( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CSiteTabletStat', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSiteTabletStat
	 */
	public static function fetchSiteTabletStat( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSiteTabletStat', $objDatabase );
	}

	public static function fetchSiteTabletStatCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'site_tablet_stats', $objDatabase );
	}

	public static function fetchSiteTabletStatByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSiteTabletStat( sprintf( 'SELECT * FROM site_tablet_stats WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSiteTabletStatsByCid( $intCid, $objDatabase ) {
		return self::fetchSiteTabletStats( sprintf( 'SELECT * FROM site_tablet_stats WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSiteTabletStatsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSiteTabletStats( sprintf( 'SELECT * FROM site_tablet_stats WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>