<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudget extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.budgets';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intBudgetStatusTypeId;
	protected $m_intPropertyId;
	protected $m_strName;
	protected $m_intFiscalYear;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_intIsDefault;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intBudgetWorkbookId;
	protected $m_boolIsAdvancedBudget;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strQueueStatus;
	protected $m_strQueueErrors;
	protected $m_strQueuedDatetime;
	protected $m_strStartedDatetime;
	protected $m_strCompletedDatetime;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_strBusinessPlan;

	public function __construct() {
		parent::__construct();

		$this->m_intIsDefault = '0';
		$this->m_boolIsAdvancedBudget = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['budget_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetStatusTypeId', trim( $arrValues['budget_status_type_id'] ) ); elseif( isset( $arrValues['budget_status_type_id'] ) ) $this->setBudgetStatusTypeId( $arrValues['budget_status_type_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['fiscal_year'] ) && $boolDirectSet ) $this->set( 'm_intFiscalYear', trim( $arrValues['fiscal_year'] ) ); elseif( isset( $arrValues['fiscal_year'] ) ) $this->setFiscalYear( $arrValues['fiscal_year'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_intIsDefault', trim( $arrValues['is_default'] ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( $arrValues['is_default'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['budget_workbook_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetWorkbookId', trim( $arrValues['budget_workbook_id'] ) ); elseif( isset( $arrValues['budget_workbook_id'] ) ) $this->setBudgetWorkbookId( $arrValues['budget_workbook_id'] );
		if( isset( $arrValues['is_advanced_budget'] ) && $boolDirectSet ) $this->set( 'm_boolIsAdvancedBudget', trim( stripcslashes( $arrValues['is_advanced_budget'] ) ) ); elseif( isset( $arrValues['is_advanced_budget'] ) ) $this->setIsAdvancedBudget( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_advanced_budget'] ) : $arrValues['is_advanced_budget'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['queue_status'] ) && $boolDirectSet ) $this->set( 'm_strQueueStatus', trim( stripcslashes( $arrValues['queue_status'] ) ) ); elseif( isset( $arrValues['queue_status'] ) ) $this->setQueueStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['queue_status'] ) : $arrValues['queue_status'] );
		if( isset( $arrValues['queue_errors'] ) && $boolDirectSet ) $this->set( 'm_strQueueErrors', trim( stripcslashes( $arrValues['queue_errors'] ) ) ); elseif( isset( $arrValues['queue_errors'] ) ) $this->setQueueErrors( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['queue_errors'] ) : $arrValues['queue_errors'] );
		if( isset( $arrValues['queued_datetime'] ) && $boolDirectSet ) $this->set( 'm_strQueuedDatetime', trim( $arrValues['queued_datetime'] ) ); elseif( isset( $arrValues['queued_datetime'] ) ) $this->setQueuedDatetime( $arrValues['queued_datetime'] );
		if( isset( $arrValues['started_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStartedDatetime', trim( $arrValues['started_datetime'] ) ); elseif( isset( $arrValues['started_datetime'] ) ) $this->setStartedDatetime( $arrValues['started_datetime'] );
		if( isset( $arrValues['completed_datetime'] ) && $boolDirectSet ) $this->set( 'm_strCompletedDatetime', trim( $arrValues['completed_datetime'] ) ); elseif( isset( $arrValues['completed_datetime'] ) ) $this->setCompletedDatetime( $arrValues['completed_datetime'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['business_plan'] ) && $boolDirectSet ) $this->set( 'm_strBusinessPlan', trim( stripcslashes( $arrValues['business_plan'] ) ) ); elseif( isset( $arrValues['business_plan'] ) ) $this->setBusinessPlan( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['business_plan'] ) : $arrValues['business_plan'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setBudgetStatusTypeId( $intBudgetStatusTypeId ) {
		$this->set( 'm_intBudgetStatusTypeId', CStrings::strToIntDef( $intBudgetStatusTypeId, NULL, false ) );
	}

	public function getBudgetStatusTypeId() {
		return $this->m_intBudgetStatusTypeId;
	}

	public function sqlBudgetStatusTypeId() {
		return ( true == isset( $this->m_intBudgetStatusTypeId ) ) ? ( string ) $this->m_intBudgetStatusTypeId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setFiscalYear( $intFiscalYear ) {
		$this->set( 'm_intFiscalYear', CStrings::strToIntDef( $intFiscalYear, NULL, false ) );
	}

	public function getFiscalYear() {
		return $this->m_intFiscalYear;
	}

	public function sqlFiscalYear() {
		return ( true == isset( $this->m_intFiscalYear ) ) ? ( string ) $this->m_intFiscalYear : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setIsDefault( $intIsDefault ) {
		$this->set( 'm_intIsDefault', CStrings::strToIntDef( $intIsDefault, NULL, false ) );
	}

	public function getIsDefault() {
		return $this->m_intIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_intIsDefault ) ) ? ( string ) $this->m_intIsDefault : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setBudgetWorkbookId( $intBudgetWorkbookId ) {
		$this->set( 'm_intBudgetWorkbookId', CStrings::strToIntDef( $intBudgetWorkbookId, NULL, false ) );
	}

	public function getBudgetWorkbookId() {
		return $this->m_intBudgetWorkbookId;
	}

	public function sqlBudgetWorkbookId() {
		return ( true == isset( $this->m_intBudgetWorkbookId ) ) ? ( string ) $this->m_intBudgetWorkbookId : 'NULL';
	}

	public function setIsAdvancedBudget( $boolIsAdvancedBudget ) {
		$this->set( 'm_boolIsAdvancedBudget', CStrings::strToBool( $boolIsAdvancedBudget ) );
	}

	public function getIsAdvancedBudget() {
		return $this->m_boolIsAdvancedBudget;
	}

	public function sqlIsAdvancedBudget() {
		return ( true == isset( $this->m_boolIsAdvancedBudget ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAdvancedBudget ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setQueueStatus( $strQueueStatus ) {
		$this->set( 'm_strQueueStatus', CStrings::strTrimDef( $strQueueStatus, 240, NULL, true ) );
	}

	public function getQueueStatus() {
		return $this->m_strQueueStatus;
	}

	public function sqlQueueStatus() {
		return ( true == isset( $this->m_strQueueStatus ) ) ? '\'' . addslashes( $this->m_strQueueStatus ) . '\'' : 'NULL';
	}

	public function setQueueErrors( $strQueueErrors ) {
		$this->set( 'm_strQueueErrors', CStrings::strTrimDef( $strQueueErrors, -1, NULL, true ) );
	}

	public function getQueueErrors() {
		return $this->m_strQueueErrors;
	}

	public function sqlQueueErrors() {
		return ( true == isset( $this->m_strQueueErrors ) ) ? '\'' . addslashes( $this->m_strQueueErrors ) . '\'' : 'NULL';
	}

	public function setQueuedDatetime( $strQueuedDatetime ) {
		$this->set( 'm_strQueuedDatetime', CStrings::strTrimDef( $strQueuedDatetime, -1, NULL, true ) );
	}

	public function getQueuedDatetime() {
		return $this->m_strQueuedDatetime;
	}

	public function sqlQueuedDatetime() {
		return ( true == isset( $this->m_strQueuedDatetime ) ) ? '\'' . $this->m_strQueuedDatetime . '\'' : 'NULL';
	}

	public function setStartedDatetime( $strStartedDatetime ) {
		$this->set( 'm_strStartedDatetime', CStrings::strTrimDef( $strStartedDatetime, -1, NULL, true ) );
	}

	public function getStartedDatetime() {
		return $this->m_strStartedDatetime;
	}

	public function sqlStartedDatetime() {
		return ( true == isset( $this->m_strStartedDatetime ) ) ? '\'' . $this->m_strStartedDatetime . '\'' : 'NULL';
	}

	public function setCompletedDatetime( $strCompletedDatetime ) {
		$this->set( 'm_strCompletedDatetime', CStrings::strTrimDef( $strCompletedDatetime, -1, NULL, true ) );
	}

	public function getCompletedDatetime() {
		return $this->m_strCompletedDatetime;
	}

	public function sqlCompletedDatetime() {
		return ( true == isset( $this->m_strCompletedDatetime ) ) ? '\'' . $this->m_strCompletedDatetime . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setBusinessPlan( $strBusinessPlan ) {
		$this->set( 'm_strBusinessPlan', CStrings::strTrimDef( $strBusinessPlan, -1, NULL, true ) );
	}

	public function getBusinessPlan() {
		return $this->m_strBusinessPlan;
	}

	public function sqlBusinessPlan() {
		return ( true == isset( $this->m_strBusinessPlan ) ) ? '\'' . addslashes( $this->m_strBusinessPlan ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, budget_status_type_id, property_id, name, fiscal_year, start_date, end_date, is_default, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, budget_workbook_id, is_advanced_budget, details, queue_status, queue_errors, queued_datetime, started_datetime, completed_datetime, approved_by, approved_on, business_plan )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlBudgetStatusTypeId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlFiscalYear() . ', ' .
						$this->sqlStartDate() . ', ' .
						$this->sqlEndDate() . ', ' .
						$this->sqlIsDefault() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlBudgetWorkbookId() . ', ' .
						$this->sqlIsAdvancedBudget() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlQueueStatus() . ', ' .
						$this->sqlQueueErrors() . ', ' .
						$this->sqlQueuedDatetime() . ', ' .
						$this->sqlStartedDatetime() . ', ' .
						$this->sqlCompletedDatetime() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						$this->sqlBusinessPlan() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_status_type_id = ' . $this->sqlBudgetStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'BudgetStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' budget_status_type_id = ' . $this->sqlBudgetStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fiscal_year = ' . $this->sqlFiscalYear(). ',' ; } elseif( true == array_key_exists( 'FiscalYear', $this->getChangedColumns() ) ) { $strSql .= ' fiscal_year = ' . $this->sqlFiscalYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate(). ',' ; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate(). ',' ; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault(). ',' ; } elseif( true == array_key_exists( 'IsDefault', $this->getChangedColumns() ) ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_workbook_id = ' . $this->sqlBudgetWorkbookId(). ',' ; } elseif( true == array_key_exists( 'BudgetWorkbookId', $this->getChangedColumns() ) ) { $strSql .= ' budget_workbook_id = ' . $this->sqlBudgetWorkbookId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_advanced_budget = ' . $this->sqlIsAdvancedBudget(). ',' ; } elseif( true == array_key_exists( 'IsAdvancedBudget', $this->getChangedColumns() ) ) { $strSql .= ' is_advanced_budget = ' . $this->sqlIsAdvancedBudget() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' queue_status = ' . $this->sqlQueueStatus(). ',' ; } elseif( true == array_key_exists( 'QueueStatus', $this->getChangedColumns() ) ) { $strSql .= ' queue_status = ' . $this->sqlQueueStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' queue_errors = ' . $this->sqlQueueErrors(). ',' ; } elseif( true == array_key_exists( 'QueueErrors', $this->getChangedColumns() ) ) { $strSql .= ' queue_errors = ' . $this->sqlQueueErrors() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' queued_datetime = ' . $this->sqlQueuedDatetime(). ',' ; } elseif( true == array_key_exists( 'QueuedDatetime', $this->getChangedColumns() ) ) { $strSql .= ' queued_datetime = ' . $this->sqlQueuedDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' started_datetime = ' . $this->sqlStartedDatetime(). ',' ; } elseif( true == array_key_exists( 'StartedDatetime', $this->getChangedColumns() ) ) { $strSql .= ' started_datetime = ' . $this->sqlStartedDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_datetime = ' . $this->sqlCompletedDatetime(). ',' ; } elseif( true == array_key_exists( 'CompletedDatetime', $this->getChangedColumns() ) ) { $strSql .= ' completed_datetime = ' . $this->sqlCompletedDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' business_plan = ' . $this->sqlBusinessPlan(). ',' ; } elseif( true == array_key_exists( 'BusinessPlan', $this->getChangedColumns() ) ) { $strSql .= ' business_plan = ' . $this->sqlBusinessPlan() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'budget_status_type_id' => $this->getBudgetStatusTypeId(),
			'property_id' => $this->getPropertyId(),
			'name' => $this->getName(),
			'fiscal_year' => $this->getFiscalYear(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'is_default' => $this->getIsDefault(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'budget_workbook_id' => $this->getBudgetWorkbookId(),
			'is_advanced_budget' => $this->getIsAdvancedBudget(),
			'details' => $this->getDetails(),
			'queue_status' => $this->getQueueStatus(),
			'queue_errors' => $this->getQueueErrors(),
			'queued_datetime' => $this->getQueuedDatetime(),
			'started_datetime' => $this->getStartedDatetime(),
			'completed_datetime' => $this->getCompletedDatetime(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'business_plan' => $this->getBusinessPlan()
		);
	}

}
?>