<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CImplementationChecklists
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseImplementationChecklists extends CEosPluralBase {

	/**
	 * @return CImplementationChecklist[]
	 */
	public static function fetchImplementationChecklists( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CImplementationChecklist', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CImplementationChecklist
	 */
	public static function fetchImplementationChecklist( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CImplementationChecklist', $objDatabase );
	}

	public static function fetchImplementationChecklistCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'implementation_checklists', $objDatabase );
	}

	public static function fetchImplementationChecklistByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchImplementationChecklist( sprintf( 'SELECT * FROM implementation_checklists WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchImplementationChecklistsByCid( $intCid, $objDatabase ) {
		return self::fetchImplementationChecklists( sprintf( 'SELECT * FROM implementation_checklists WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchImplementationChecklistsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchImplementationChecklists( sprintf( 'SELECT * FROM implementation_checklists WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchImplementationChecklistsByModuleIdByCid( $intModuleId, $intCid, $objDatabase ) {
		return self::fetchImplementationChecklists( sprintf( 'SELECT * FROM implementation_checklists WHERE module_id = %d AND cid = %d', ( int ) $intModuleId, ( int ) $intCid ), $objDatabase );
	}

}
?>