<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COnSiteContactAddresses
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseOnSiteContactAddresses extends CEosPluralBase {

	/**
	 * @return COnSiteContactAddress[]
	 */
	public static function fetchOnSiteContactAddresses( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'COnSiteContactAddress', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return COnSiteContactAddress
	 */
	public static function fetchOnSiteContactAddress( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'COnSiteContactAddress', $objDatabase );
	}

	public static function fetchOnSiteContactAddressCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'on_site_contact_addresses', $objDatabase );
	}

	public static function fetchOnSiteContactAddressByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchOnSiteContactAddress( sprintf( 'SELECT * FROM on_site_contact_addresses WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOnSiteContactAddressesByCid( $intCid, $objDatabase ) {
		return self::fetchOnSiteContactAddresses( sprintf( 'SELECT * FROM on_site_contact_addresses WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOnSiteContactAddressesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchOnSiteContactAddresses( sprintf( 'SELECT * FROM on_site_contact_addresses WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOnSiteContactAddressesByPropertyOnSiteContactIdByCid( $intPropertyOnSiteContactId, $intCid, $objDatabase ) {
		return self::fetchOnSiteContactAddresses( sprintf( 'SELECT * FROM on_site_contact_addresses WHERE property_on_site_contact_id = %d AND cid = %d', ( int ) $intPropertyOnSiteContactId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOnSiteContactAddressesByAddressTypeIdByCid( $intAddressTypeId, $intCid, $objDatabase ) {
		return self::fetchOnSiteContactAddresses( sprintf( 'SELECT * FROM on_site_contact_addresses WHERE address_type_id = %d AND cid = %d', ( int ) $intAddressTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOnSiteContactAddressesByTimeZoneIdByCid( $intTimeZoneId, $intCid, $objDatabase ) {
		return self::fetchOnSiteContactAddresses( sprintf( 'SELECT * FROM on_site_contact_addresses WHERE time_zone_id = %d AND cid = %d', ( int ) $intTimeZoneId, ( int ) $intCid ), $objDatabase );
	}

}
?>