<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentMergeFields
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDocumentMergeFields extends CEosPluralBase {

	/**
	 * @return CDocumentMergeField[]
	 */
	public static function fetchDocumentMergeFields( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CDocumentMergeField::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CDocumentMergeField
	 */
	public static function fetchDocumentMergeField( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDocumentMergeField::class, $objDatabase );
	}

	public static function fetchDocumentMergeFieldCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_merge_fields', $objDatabase );
	}

	public static function fetchDocumentMergeFieldByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchDocumentMergeField( sprintf( 'SELECT * FROM document_merge_fields WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentMergeFieldsByCid( $intCid, $objDatabase ) {
		return self::fetchDocumentMergeFields( sprintf( 'SELECT * FROM document_merge_fields WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchDocumentMergeFieldsByCompanyMergeFieldIdByCid( $intCompanyMergeFieldId, $intCid, $objDatabase ) {
		return self::fetchDocumentMergeFields( sprintf( 'SELECT * FROM document_merge_fields WHERE company_merge_field_id = %d AND cid = %d', $intCompanyMergeFieldId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentMergeFieldsByDefaultMergeFieldIdByCid( $intDefaultMergeFieldId, $intCid, $objDatabase ) {
		return self::fetchDocumentMergeFields( sprintf( 'SELECT * FROM document_merge_fields WHERE default_merge_field_id = %d AND cid = %d', $intDefaultMergeFieldId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentMergeFieldsByBlockDocumentMergeFieldIdByCid( $intBlockDocumentMergeFieldId, $intCid, $objDatabase ) {
		return self::fetchDocumentMergeFields( sprintf( 'SELECT * FROM document_merge_fields WHERE block_document_merge_field_id = %d AND cid = %d', $intBlockDocumentMergeFieldId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentMergeFieldsByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		return self::fetchDocumentMergeFields( sprintf( 'SELECT * FROM document_merge_fields WHERE document_id = %d AND cid = %d', $intDocumentId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentMergeFieldsByDocumentAddendaIdByCid( $intDocumentAddendaId, $intCid, $objDatabase ) {
		return self::fetchDocumentMergeFields( sprintf( 'SELECT * FROM document_merge_fields WHERE document_addenda_id = %d AND cid = %d', $intDocumentAddendaId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentMergeFieldsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchDocumentMergeFields( sprintf( 'SELECT * FROM document_merge_fields WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentMergeFieldsByMergeFieldGroupIdByCid( $intMergeFieldGroupId, $intCid, $objDatabase ) {
		return self::fetchDocumentMergeFields( sprintf( 'SELECT * FROM document_merge_fields WHERE merge_field_group_id = %d AND cid = %d', $intMergeFieldGroupId, $intCid ), $objDatabase );
	}

	public static function fetchDocumentMergeFieldsByExternalMergeFieldGroupIdByCid( $intExternalMergeFieldGroupId, $intCid, $objDatabase ) {
		return self::fetchDocumentMergeFields( sprintf( 'SELECT * FROM document_merge_fields WHERE external_merge_field_group_id = %d AND cid = %d', $intExternalMergeFieldGroupId, $intCid ), $objDatabase );
	}

}
?>