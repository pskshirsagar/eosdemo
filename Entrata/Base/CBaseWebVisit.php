<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseWebVisit extends CEosSingularBase {

	const TABLE_NAME = 'public.web_visits';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intWebsiteId;
	protected $m_intPropertyId;
	protected $m_intInterentListingServiceId;
	protected $m_intCraigslistPostId;
	protected $m_intClAdId;
	protected $m_intWebVisitTypeId;
	protected $m_intTrafficCookieId;
	protected $m_strReferrer;
	protected $m_strVisitDatetime;
	protected $m_intViewCount;
	protected $m_intIfFirstVisit;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intViewCount = '0';
		$this->m_intIfFirstVisit = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['website_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteId', trim( $arrValues['website_id'] ) ); elseif( isset( $arrValues['website_id'] ) ) $this->setWebsiteId( $arrValues['website_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['interent_listing_service_id'] ) && $boolDirectSet ) $this->set( 'm_intInterentListingServiceId', trim( $arrValues['interent_listing_service_id'] ) ); elseif( isset( $arrValues['interent_listing_service_id'] ) ) $this->setInterentListingServiceId( $arrValues['interent_listing_service_id'] );
		if( isset( $arrValues['craigslist_post_id'] ) && $boolDirectSet ) $this->set( 'm_intCraigslistPostId', trim( $arrValues['craigslist_post_id'] ) ); elseif( isset( $arrValues['craigslist_post_id'] ) ) $this->setCraigslistPostId( $arrValues['craigslist_post_id'] );
		if( isset( $arrValues['cl_ad_id'] ) && $boolDirectSet ) $this->set( 'm_intClAdId', trim( $arrValues['cl_ad_id'] ) ); elseif( isset( $arrValues['cl_ad_id'] ) ) $this->setClAdId( $arrValues['cl_ad_id'] );
		if( isset( $arrValues['web_visit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intWebVisitTypeId', trim( $arrValues['web_visit_type_id'] ) ); elseif( isset( $arrValues['web_visit_type_id'] ) ) $this->setWebVisitTypeId( $arrValues['web_visit_type_id'] );
		if( isset( $arrValues['traffic_cookie_id'] ) && $boolDirectSet ) $this->set( 'm_intTrafficCookieId', trim( $arrValues['traffic_cookie_id'] ) ); elseif( isset( $arrValues['traffic_cookie_id'] ) ) $this->setTrafficCookieId( $arrValues['traffic_cookie_id'] );
		if( isset( $arrValues['referrer'] ) && $boolDirectSet ) $this->set( 'm_strReferrer', trim( stripcslashes( $arrValues['referrer'] ) ) ); elseif( isset( $arrValues['referrer'] ) ) $this->setReferrer( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['referrer'] ) : $arrValues['referrer'] );
		if( isset( $arrValues['visit_datetime'] ) && $boolDirectSet ) $this->set( 'm_strVisitDatetime', trim( $arrValues['visit_datetime'] ) ); elseif( isset( $arrValues['visit_datetime'] ) ) $this->setVisitDatetime( $arrValues['visit_datetime'] );
		if( isset( $arrValues['view_count'] ) && $boolDirectSet ) $this->set( 'm_intViewCount', trim( $arrValues['view_count'] ) ); elseif( isset( $arrValues['view_count'] ) ) $this->setViewCount( $arrValues['view_count'] );
		if( isset( $arrValues['if_first_visit'] ) && $boolDirectSet ) $this->set( 'm_intIfFirstVisit', trim( $arrValues['if_first_visit'] ) ); elseif( isset( $arrValues['if_first_visit'] ) ) $this->setIfFirstVisit( $arrValues['if_first_visit'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->set( 'm_intWebsiteId', CStrings::strToIntDef( $intWebsiteId, NULL, false ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function sqlWebsiteId() {
		return ( true == isset( $this->m_intWebsiteId ) ) ? ( string ) $this->m_intWebsiteId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setInterentListingServiceId( $intInterentListingServiceId ) {
		$this->set( 'm_intInterentListingServiceId', CStrings::strToIntDef( $intInterentListingServiceId, NULL, false ) );
	}

	public function getInterentListingServiceId() {
		return $this->m_intInterentListingServiceId;
	}

	public function sqlInterentListingServiceId() {
		return ( true == isset( $this->m_intInterentListingServiceId ) ) ? ( string ) $this->m_intInterentListingServiceId : 'NULL';
	}

	public function setCraigslistPostId( $intCraigslistPostId ) {
		$this->set( 'm_intCraigslistPostId', CStrings::strToIntDef( $intCraigslistPostId, NULL, false ) );
	}

	public function getCraigslistPostId() {
		return $this->m_intCraigslistPostId;
	}

	public function sqlCraigslistPostId() {
		return ( true == isset( $this->m_intCraigslistPostId ) ) ? ( string ) $this->m_intCraigslistPostId : 'NULL';
	}

	public function setClAdId( $intClAdId ) {
		$this->set( 'm_intClAdId', CStrings::strToIntDef( $intClAdId, NULL, false ) );
	}

	public function getClAdId() {
		return $this->m_intClAdId;
	}

	public function sqlClAdId() {
		return ( true == isset( $this->m_intClAdId ) ) ? ( string ) $this->m_intClAdId : 'NULL';
	}

	public function setWebVisitTypeId( $intWebVisitTypeId ) {
		$this->set( 'm_intWebVisitTypeId', CStrings::strToIntDef( $intWebVisitTypeId, NULL, false ) );
	}

	public function getWebVisitTypeId() {
		return $this->m_intWebVisitTypeId;
	}

	public function sqlWebVisitTypeId() {
		return ( true == isset( $this->m_intWebVisitTypeId ) ) ? ( string ) $this->m_intWebVisitTypeId : 'NULL';
	}

	public function setTrafficCookieId( $intTrafficCookieId ) {
		$this->set( 'm_intTrafficCookieId', CStrings::strToIntDef( $intTrafficCookieId, NULL, false ) );
	}

	public function getTrafficCookieId() {
		return $this->m_intTrafficCookieId;
	}

	public function sqlTrafficCookieId() {
		return ( true == isset( $this->m_intTrafficCookieId ) ) ? ( string ) $this->m_intTrafficCookieId : 'NULL';
	}

	public function setReferrer( $strReferrer ) {
		$this->set( 'm_strReferrer', CStrings::strTrimDef( $strReferrer, 240, NULL, true ) );
	}

	public function getReferrer() {
		return $this->m_strReferrer;
	}

	public function sqlReferrer() {
		return ( true == isset( $this->m_strReferrer ) ) ? '\'' . addslashes( $this->m_strReferrer ) . '\'' : 'NULL';
	}

	public function setVisitDatetime( $strVisitDatetime ) {
		$this->set( 'm_strVisitDatetime', CStrings::strTrimDef( $strVisitDatetime, -1, NULL, true ) );
	}

	public function getVisitDatetime() {
		return $this->m_strVisitDatetime;
	}

	public function sqlVisitDatetime() {
		return ( true == isset( $this->m_strVisitDatetime ) ) ? '\'' . $this->m_strVisitDatetime . '\'' : 'NOW()';
	}

	public function setViewCount( $intViewCount ) {
		$this->set( 'm_intViewCount', CStrings::strToIntDef( $intViewCount, NULL, false ) );
	}

	public function getViewCount() {
		return $this->m_intViewCount;
	}

	public function sqlViewCount() {
		return ( true == isset( $this->m_intViewCount ) ) ? ( string ) $this->m_intViewCount : '0';
	}

	public function setIfFirstVisit( $intIfFirstVisit ) {
		$this->set( 'm_intIfFirstVisit', CStrings::strToIntDef( $intIfFirstVisit, NULL, false ) );
	}

	public function getIfFirstVisit() {
		return $this->m_intIfFirstVisit;
	}

	public function sqlIfFirstVisit() {
		return ( true == isset( $this->m_intIfFirstVisit ) ) ? ( string ) $this->m_intIfFirstVisit : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, website_id, property_id, interent_listing_service_id, craigslist_post_id, cl_ad_id, web_visit_type_id, traffic_cookie_id, referrer, visit_datetime, view_count, if_first_visit, created_by, created_on, updated_by, updated_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlWebsiteId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlInterentListingServiceId() . ', ' .
 						$this->sqlCraigslistPostId() . ', ' .
 						$this->sqlClAdId() . ', ' .
 						$this->sqlWebVisitTypeId() . ', ' .
 						$this->sqlTrafficCookieId() . ', ' .
 						$this->sqlReferrer() . ', ' .
 						$this->sqlVisitDatetime() . ', ' .
 						$this->sqlViewCount() . ', ' .
 						$this->sqlIfFirstVisit() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; } elseif( true == array_key_exists( 'WebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interent_listing_service_id = ' . $this->sqlInterentListingServiceId() . ','; } elseif( true == array_key_exists( 'InterentListingServiceId', $this->getChangedColumns() ) ) { $strSql .= ' interent_listing_service_id = ' . $this->sqlInterentListingServiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' craigslist_post_id = ' . $this->sqlCraigslistPostId() . ','; } elseif( true == array_key_exists( 'CraigslistPostId', $this->getChangedColumns() ) ) { $strSql .= ' craigslist_post_id = ' . $this->sqlCraigslistPostId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cl_ad_id = ' . $this->sqlClAdId() . ','; } elseif( true == array_key_exists( 'ClAdId', $this->getChangedColumns() ) ) { $strSql .= ' cl_ad_id = ' . $this->sqlClAdId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' web_visit_type_id = ' . $this->sqlWebVisitTypeId() . ','; } elseif( true == array_key_exists( 'WebVisitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' web_visit_type_id = ' . $this->sqlWebVisitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' traffic_cookie_id = ' . $this->sqlTrafficCookieId() . ','; } elseif( true == array_key_exists( 'TrafficCookieId', $this->getChangedColumns() ) ) { $strSql .= ' traffic_cookie_id = ' . $this->sqlTrafficCookieId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' referrer = ' . $this->sqlReferrer() . ','; } elseif( true == array_key_exists( 'Referrer', $this->getChangedColumns() ) ) { $strSql .= ' referrer = ' . $this->sqlReferrer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' visit_datetime = ' . $this->sqlVisitDatetime() . ','; } elseif( true == array_key_exists( 'VisitDatetime', $this->getChangedColumns() ) ) { $strSql .= ' visit_datetime = ' . $this->sqlVisitDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' view_count = ' . $this->sqlViewCount() . ','; } elseif( true == array_key_exists( 'ViewCount', $this->getChangedColumns() ) ) { $strSql .= ' view_count = ' . $this->sqlViewCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' if_first_visit = ' . $this->sqlIfFirstVisit() . ','; } elseif( true == array_key_exists( 'IfFirstVisit', $this->getChangedColumns() ) ) { $strSql .= ' if_first_visit = ' . $this->sqlIfFirstVisit() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'website_id' => $this->getWebsiteId(),
			'property_id' => $this->getPropertyId(),
			'interent_listing_service_id' => $this->getInterentListingServiceId(),
			'craigslist_post_id' => $this->getCraigslistPostId(),
			'cl_ad_id' => $this->getClAdId(),
			'web_visit_type_id' => $this->getWebVisitTypeId(),
			'traffic_cookie_id' => $this->getTrafficCookieId(),
			'referrer' => $this->getReferrer(),
			'visit_datetime' => $this->getVisitDatetime(),
			'view_count' => $this->getViewCount(),
			'if_first_visit' => $this->getIfFirstVisit(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn()
		);
	}

}
?>