<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CInstallments
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseInstallments extends CEosPluralBase {

	/**
	 * @return CInstallment[]
	 */
	public static function fetchInstallments( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CInstallment::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CInstallment
	 */
	public static function fetchInstallment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInstallment::class, $objDatabase );
	}

	public static function fetchInstallmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'installments', $objDatabase );
	}

	public static function fetchInstallmentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchInstallment( sprintf( 'SELECT * FROM installments WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchInstallmentsByCid( $intCid, $objDatabase ) {
		return self::fetchInstallments( sprintf( 'SELECT * FROM installments WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchInstallmentsByInstallmentPlanIdByCid( $intInstallmentPlanId, $intCid, $objDatabase ) {
		return self::fetchInstallments( sprintf( 'SELECT * FROM installments WHERE installment_plan_id = %d AND cid = %d', $intInstallmentPlanId, $intCid ), $objDatabase );
	}

}
?>