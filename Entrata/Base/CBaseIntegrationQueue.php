<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseIntegrationQueue extends CEosSingularBase {

	const TABLE_NAME = 'public.integration_queues';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intIntegrationClientId;
	protected $m_intIntegrationServiceId;
	protected $m_intIntegrationQueueStatusTypeId;
	protected $m_intIntegrationResultId;
	protected $m_intLockSequenceNumber;
	protected $m_intReferenceNumber;
	protected $m_strLastFailedOn;
	protected $m_strLockedOn;
	protected $m_intCompletedBy;
	protected $m_strCompletedOn;
	protected $m_strDueDatetime;
	protected $m_intOrderNum;
	protected $m_intReattemptCount;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['integration_client_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationClientId', trim( $arrValues['integration_client_id'] ) ); elseif( isset( $arrValues['integration_client_id'] ) ) $this->setIntegrationClientId( $arrValues['integration_client_id'] );
		if( isset( $arrValues['integration_service_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationServiceId', trim( $arrValues['integration_service_id'] ) ); elseif( isset( $arrValues['integration_service_id'] ) ) $this->setIntegrationServiceId( $arrValues['integration_service_id'] );
		if( isset( $arrValues['integration_queue_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationQueueStatusTypeId', trim( $arrValues['integration_queue_status_type_id'] ) ); elseif( isset( $arrValues['integration_queue_status_type_id'] ) ) $this->setIntegrationQueueStatusTypeId( $arrValues['integration_queue_status_type_id'] );
		if( isset( $arrValues['integration_result_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationResultId', trim( $arrValues['integration_result_id'] ) ); elseif( isset( $arrValues['integration_result_id'] ) ) $this->setIntegrationResultId( $arrValues['integration_result_id'] );
		if( isset( $arrValues['lock_sequence_number'] ) && $boolDirectSet ) $this->set( 'm_intLockSequenceNumber', trim( $arrValues['lock_sequence_number'] ) ); elseif( isset( $arrValues['lock_sequence_number'] ) ) $this->setLockSequenceNumber( $arrValues['lock_sequence_number'] );
		if( isset( $arrValues['reference_number'] ) && $boolDirectSet ) $this->set( 'm_intReferenceNumber', trim( $arrValues['reference_number'] ) ); elseif( isset( $arrValues['reference_number'] ) ) $this->setReferenceNumber( $arrValues['reference_number'] );
		if( isset( $arrValues['last_failed_on'] ) && $boolDirectSet ) $this->set( 'm_strLastFailedOn', trim( $arrValues['last_failed_on'] ) ); elseif( isset( $arrValues['last_failed_on'] ) ) $this->setLastFailedOn( $arrValues['last_failed_on'] );
		if( isset( $arrValues['locked_on'] ) && $boolDirectSet ) $this->set( 'm_strLockedOn', trim( $arrValues['locked_on'] ) ); elseif( isset( $arrValues['locked_on'] ) ) $this->setLockedOn( $arrValues['locked_on'] );
		if( isset( $arrValues['completed_by'] ) && $boolDirectSet ) $this->set( 'm_intCompletedBy', trim( $arrValues['completed_by'] ) ); elseif( isset( $arrValues['completed_by'] ) ) $this->setCompletedBy( $arrValues['completed_by'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['due_datetime'] ) && $boolDirectSet ) $this->set( 'm_strDueDatetime', trim( $arrValues['due_datetime'] ) ); elseif( isset( $arrValues['due_datetime'] ) ) $this->setDueDatetime( $arrValues['due_datetime'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['reattempt_count'] ) && $boolDirectSet ) $this->set( 'm_intReattemptCount', trim( $arrValues['reattempt_count'] ) ); elseif( isset( $arrValues['reattempt_count'] ) ) $this->setReattemptCount( $arrValues['reattempt_count'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setIntegrationClientId( $intIntegrationClientId ) {
		$this->set( 'm_intIntegrationClientId', CStrings::strToIntDef( $intIntegrationClientId, NULL, false ) );
	}

	public function getIntegrationClientId() {
		return $this->m_intIntegrationClientId;
	}

	public function sqlIntegrationClientId() {
		return ( true == isset( $this->m_intIntegrationClientId ) ) ? ( string ) $this->m_intIntegrationClientId : 'NULL';
	}

	public function setIntegrationServiceId( $intIntegrationServiceId ) {
		$this->set( 'm_intIntegrationServiceId', CStrings::strToIntDef( $intIntegrationServiceId, NULL, false ) );
	}

	public function getIntegrationServiceId() {
		return $this->m_intIntegrationServiceId;
	}

	public function sqlIntegrationServiceId() {
		return ( true == isset( $this->m_intIntegrationServiceId ) ) ? ( string ) $this->m_intIntegrationServiceId : 'NULL';
	}

	public function setIntegrationQueueStatusTypeId( $intIntegrationQueueStatusTypeId ) {
		$this->set( 'm_intIntegrationQueueStatusTypeId', CStrings::strToIntDef( $intIntegrationQueueStatusTypeId, NULL, false ) );
	}

	public function getIntegrationQueueStatusTypeId() {
		return $this->m_intIntegrationQueueStatusTypeId;
	}

	public function sqlIntegrationQueueStatusTypeId() {
		return ( true == isset( $this->m_intIntegrationQueueStatusTypeId ) ) ? ( string ) $this->m_intIntegrationQueueStatusTypeId : 'NULL';
	}

	public function setIntegrationResultId( $intIntegrationResultId ) {
		$this->set( 'm_intIntegrationResultId', CStrings::strToIntDef( $intIntegrationResultId, NULL, false ) );
	}

	public function getIntegrationResultId() {
		return $this->m_intIntegrationResultId;
	}

	public function sqlIntegrationResultId() {
		return ( true == isset( $this->m_intIntegrationResultId ) ) ? ( string ) $this->m_intIntegrationResultId : 'NULL';
	}

	public function setLockSequenceNumber( $intLockSequenceNumber ) {
		$this->set( 'm_intLockSequenceNumber', CStrings::strToIntDef( $intLockSequenceNumber, NULL, false ) );
	}

	public function getLockSequenceNumber() {
		return $this->m_intLockSequenceNumber;
	}

	public function sqlLockSequenceNumber() {
		return ( true == isset( $this->m_intLockSequenceNumber ) ) ? ( string ) $this->m_intLockSequenceNumber : 'NULL';
	}

	public function setReferenceNumber( $intReferenceNumber ) {
		$this->set( 'm_intReferenceNumber', CStrings::strToIntDef( $intReferenceNumber, NULL, false ) );
	}

	public function getReferenceNumber() {
		return $this->m_intReferenceNumber;
	}

	public function sqlReferenceNumber() {
		return ( true == isset( $this->m_intReferenceNumber ) ) ? ( string ) $this->m_intReferenceNumber : 'NULL';
	}

	public function setLastFailedOn( $strLastFailedOn ) {
		$this->set( 'm_strLastFailedOn', CStrings::strTrimDef( $strLastFailedOn, -1, NULL, true ) );
	}

	public function getLastFailedOn() {
		return $this->m_strLastFailedOn;
	}

	public function sqlLastFailedOn() {
		return ( true == isset( $this->m_strLastFailedOn ) ) ? '\'' . $this->m_strLastFailedOn . '\'' : 'NULL';
	}

	public function setLockedOn( $strLockedOn ) {
		$this->set( 'm_strLockedOn', CStrings::strTrimDef( $strLockedOn, -1, NULL, true ) );
	}

	public function getLockedOn() {
		return $this->m_strLockedOn;
	}

	public function sqlLockedOn() {
		return ( true == isset( $this->m_strLockedOn ) ) ? '\'' . $this->m_strLockedOn . '\'' : 'NULL';
	}

	public function setCompletedBy( $intCompletedBy ) {
		$this->set( 'm_intCompletedBy', CStrings::strToIntDef( $intCompletedBy, NULL, false ) );
	}

	public function getCompletedBy() {
		return $this->m_intCompletedBy;
	}

	public function sqlCompletedBy() {
		return ( true == isset( $this->m_intCompletedBy ) ) ? ( string ) $this->m_intCompletedBy : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setDueDatetime( $strDueDatetime ) {
		$this->set( 'm_strDueDatetime', CStrings::strTrimDef( $strDueDatetime, -1, NULL, true ) );
	}

	public function getDueDatetime() {
		return $this->m_strDueDatetime;
	}

	public function sqlDueDatetime() {
		return ( true == isset( $this->m_strDueDatetime ) ) ? '\'' . $this->m_strDueDatetime . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setReattemptCount( $intReattemptCount ) {
		$this->set( 'm_intReattemptCount', CStrings::strToIntDef( $intReattemptCount, NULL, false ) );
	}

	public function getReattemptCount() {
		return $this->m_intReattemptCount;
	}

	public function sqlReattemptCount() {
		return ( true == isset( $this->m_intReattemptCount ) ) ? ( string ) $this->m_intReattemptCount : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, integration_client_id, integration_service_id, integration_queue_status_type_id, integration_result_id, lock_sequence_number, reference_number, last_failed_on, locked_on, completed_by, completed_on, due_datetime, order_num, reattempt_count, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlIntegrationClientId() . ', ' .
 						$this->sqlIntegrationServiceId() . ', ' .
 						$this->sqlIntegrationQueueStatusTypeId() . ', ' .
 						$this->sqlIntegrationResultId() . ', ' .
 						$this->sqlLockSequenceNumber() . ', ' .
 						$this->sqlReferenceNumber() . ', ' .
 						$this->sqlLastFailedOn() . ', ' .
 						$this->sqlLockedOn() . ', ' .
 						$this->sqlCompletedBy() . ', ' .
 						$this->sqlCompletedOn() . ', ' .
 						$this->sqlDueDatetime() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlReattemptCount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_client_id = ' . $this->sqlIntegrationClientId() . ','; } elseif( true == array_key_exists( 'IntegrationClientId', $this->getChangedColumns() ) ) { $strSql .= ' integration_client_id = ' . $this->sqlIntegrationClientId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_service_id = ' . $this->sqlIntegrationServiceId() . ','; } elseif( true == array_key_exists( 'IntegrationServiceId', $this->getChangedColumns() ) ) { $strSql .= ' integration_service_id = ' . $this->sqlIntegrationServiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_queue_status_type_id = ' . $this->sqlIntegrationQueueStatusTypeId() . ','; } elseif( true == array_key_exists( 'IntegrationQueueStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' integration_queue_status_type_id = ' . $this->sqlIntegrationQueueStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_result_id = ' . $this->sqlIntegrationResultId() . ','; } elseif( true == array_key_exists( 'IntegrationResultId', $this->getChangedColumns() ) ) { $strSql .= ' integration_result_id = ' . $this->sqlIntegrationResultId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lock_sequence_number = ' . $this->sqlLockSequenceNumber() . ','; } elseif( true == array_key_exists( 'LockSequenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' lock_sequence_number = ' . $this->sqlLockSequenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber() . ','; } elseif( true == array_key_exists( 'ReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_failed_on = ' . $this->sqlLastFailedOn() . ','; } elseif( true == array_key_exists( 'LastFailedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_failed_on = ' . $this->sqlLastFailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn() . ','; } elseif( true == array_key_exists( 'LockedOn', $this->getChangedColumns() ) ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy() . ','; } elseif( true == array_key_exists( 'CompletedBy', $this->getChangedColumns() ) ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_datetime = ' . $this->sqlDueDatetime() . ','; } elseif( true == array_key_exists( 'DueDatetime', $this->getChangedColumns() ) ) { $strSql .= ' due_datetime = ' . $this->sqlDueDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reattempt_count = ' . $this->sqlReattemptCount() . ','; } elseif( true == array_key_exists( 'ReattemptCount', $this->getChangedColumns() ) ) { $strSql .= ' reattempt_count = ' . $this->sqlReattemptCount() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'integration_client_id' => $this->getIntegrationClientId(),
			'integration_service_id' => $this->getIntegrationServiceId(),
			'integration_queue_status_type_id' => $this->getIntegrationQueueStatusTypeId(),
			'integration_result_id' => $this->getIntegrationResultId(),
			'lock_sequence_number' => $this->getLockSequenceNumber(),
			'reference_number' => $this->getReferenceNumber(),
			'last_failed_on' => $this->getLastFailedOn(),
			'locked_on' => $this->getLockedOn(),
			'completed_by' => $this->getCompletedBy(),
			'completed_on' => $this->getCompletedOn(),
			'due_datetime' => $this->getDueDatetime(),
			'order_num' => $this->getOrderNum(),
			'reattempt_count' => $this->getReattemptCount(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>