<?php

class CBaseApplicantAssetTypes extends CEosPluralBase {

	public static function fetchApplicantAssetTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CApplicantAssetType', $objDatabase );
	}

	public static function fetchApplicantAssetType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApplicantAssetType', $objDatabase );
	}

	public static function fetchApplicantAssetTypeCount( $strWhere = NULL, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'applicant_asset_types', $objDatabase );
	}

	public static function fetchApplicantAssetTypeById( $intId, $objDatabase ) {
		return self::fetchApplicantAssetType( sprintf( 'SELECT * FROM applicant_asset_types WHERE id = %d', (int) $intId ), $objDatabase );
	}

}
?>