<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFeeTemplateRules
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFeeTemplateRules extends CEosPluralBase {

	/**
	 * @return CFeeTemplateRule[]
	 */
	public static function fetchFeeTemplateRules( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CFeeTemplateRule::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFeeTemplateRule
	 */
	public static function fetchFeeTemplateRule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFeeTemplateRule::class, $objDatabase );
	}

	public static function fetchFeeTemplateRuleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'fee_template_rules', $objDatabase );
	}

	public static function fetchFeeTemplateRuleByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFeeTemplateRule( sprintf( 'SELECT * FROM fee_template_rules WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchFeeTemplateRulesByCid( $intCid, $objDatabase ) {
		return self::fetchFeeTemplateRules( sprintf( 'SELECT * FROM fee_template_rules WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchFeeTemplateRulesByFeeTemplateIdByCid( $intFeeTemplateId, $intCid, $objDatabase ) {
		return self::fetchFeeTemplateRules( sprintf( 'SELECT * FROM fee_template_rules WHERE fee_template_id = %d AND cid = %d', $intFeeTemplateId, $intCid ), $objDatabase );
	}

	public static function fetchFeeTemplateRulesByFeeRuleTypeIdByCid( $intFeeRuleTypeId, $intCid, $objDatabase ) {
		return self::fetchFeeTemplateRules( sprintf( 'SELECT * FROM fee_template_rules WHERE fee_rule_type_id = %d AND cid = %d', $intFeeRuleTypeId, $intCid ), $objDatabase );
	}

	public static function fetchFeeTemplateRulesByGlGroupIdByCid( $intGlGroupId, $intCid, $objDatabase ) {
		return self::fetchFeeTemplateRules( sprintf( 'SELECT * FROM fee_template_rules WHERE gl_group_id = %d AND cid = %d', $intGlGroupId, $intCid ), $objDatabase );
	}

	public static function fetchFeeTemplateRulesByGlAccountIdByCid( $intGlAccountId, $intCid, $objDatabase ) {
		return self::fetchFeeTemplateRules( sprintf( 'SELECT * FROM fee_template_rules WHERE gl_account_id = %d AND cid = %d', $intGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchFeeTemplateRulesByArCodeIdByCid( $intArCodeId, $intCid, $objDatabase ) {
		return self::fetchFeeTemplateRules( sprintf( 'SELECT * FROM fee_template_rules WHERE ar_code_id = %d AND cid = %d', $intArCodeId, $intCid ), $objDatabase );
	}

}
?>