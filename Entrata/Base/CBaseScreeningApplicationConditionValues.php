<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningApplicationConditionValues
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningApplicationConditionValues extends CEosPluralBase {

	/**
	 * @return CScreeningApplicationConditionValue[]
	 */
	public static function fetchScreeningApplicationConditionValues( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CScreeningApplicationConditionValue', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScreeningApplicationConditionValue
	 */
	public static function fetchScreeningApplicationConditionValue( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningApplicationConditionValue', $objDatabase );
	}

	public static function fetchScreeningApplicationConditionValueCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_application_condition_values', $objDatabase );
	}

	public static function fetchScreeningApplicationConditionValueByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionValue( sprintf( 'SELECT * FROM screening_application_condition_values WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationConditionValuesByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionValues( sprintf( 'SELECT * FROM screening_application_condition_values WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationConditionValuesByScreeningApplicationConditionSetIdByCid( $intScreeningApplicationConditionSetId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionValues( sprintf( 'SELECT * FROM screening_application_condition_values WHERE screening_application_condition_set_id = %d AND cid = %d', ( int ) $intScreeningApplicationConditionSetId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationConditionValuesByConditionSubsetIdByCid( $intConditionSubsetId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionValues( sprintf( 'SELECT * FROM screening_application_condition_values WHERE condition_subset_id = %d AND cid = %d', ( int ) $intConditionSubsetId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationConditionValuesByConditionSubsetValueIdByCid( $intConditionSubsetValueId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionValues( sprintf( 'SELECT * FROM screening_application_condition_values WHERE condition_subset_value_id = %d AND cid = %d', ( int ) $intConditionSubsetValueId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationConditionValuesByConditionTypeIdByCid( $intConditionTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionValues( sprintf( 'SELECT * FROM screening_application_condition_values WHERE condition_type_id = %d AND cid = %d', ( int ) $intConditionTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationConditionValuesByConditionValueTypeIdByCid( $intConditionValueTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionValues( sprintf( 'SELECT * FROM screening_application_condition_values WHERE condition_value_type_id = %d AND cid = %d', ( int ) $intConditionValueTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationConditionValuesByConditionAmountTypeIdByCid( $intConditionAmountTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionValues( sprintf( 'SELECT * FROM screening_application_condition_values WHERE condition_amount_type_id = %d AND cid = %d', ( int ) $intConditionAmountTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationConditionValuesByArTransactionIdByCid( $intArTransactionId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionValues( sprintf( 'SELECT * FROM screening_application_condition_values WHERE ar_transaction_id = %d AND cid = %d', ( int ) $intArTransactionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationConditionValuesByApplicationRateIdByCid( $intApplicationRateId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionValues( sprintf( 'SELECT * FROM screening_application_condition_values WHERE application_rate_id = %d AND cid = %d', ( int ) $intApplicationRateId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationConditionValuesByScheduledChargeIdByCid( $intScheduledChargeId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionValues( sprintf( 'SELECT * FROM screening_application_condition_values WHERE scheduled_charge_id = %d AND cid = %d', ( int ) $intScheduledChargeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationConditionValuesByBaseChargeCodeIdByCid( $intBaseChargeCodeId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionValues( sprintf( 'SELECT * FROM screening_application_condition_values WHERE base_charge_code_id = %d AND cid = %d', ( int ) $intBaseChargeCodeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationConditionValuesByApplyToChargeCodeIdByCid( $intApplyToChargeCodeId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionValues( sprintf( 'SELECT * FROM screening_application_condition_values WHERE apply_to_charge_code_id = %d AND cid = %d', ( int ) $intApplyToChargeCodeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicationConditionValuesByArTriggerIdByCid( $intArTriggerId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicationConditionValues( sprintf( 'SELECT * FROM screening_application_condition_values WHERE ar_trigger_id = %d AND cid = %d', ( int ) $intArTriggerId, ( int ) $intCid ), $objDatabase );
	}

}
?>