<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningPackageCustomerTypesMappings
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningPackageCustomerTypesMappings extends CEosPluralBase {

	/**
	 * @return CScreeningPackageCustomerTypesMapping[]
	 */
	public static function fetchScreeningPackageCustomerTypesMappings( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CScreeningPackageCustomerTypesMapping', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScreeningPackageCustomerTypesMapping
	 */
	public static function fetchScreeningPackageCustomerTypesMapping( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScreeningPackageCustomerTypesMapping', $objDatabase );
	}

	public static function fetchScreeningPackageCustomerTypesMappingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_package_customer_types_mappings', $objDatabase );
	}

	public static function fetchScreeningPackageCustomerTypesMappingByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageCustomerTypesMapping( sprintf( 'SELECT * FROM screening_package_customer_types_mappings WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageCustomerTypesMappingsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningPackageCustomerTypesMappings( sprintf( 'SELECT * FROM screening_package_customer_types_mappings WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageCustomerTypesMappingsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageCustomerTypesMappings( sprintf( 'SELECT * FROM screening_package_customer_types_mappings WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageCustomerTypesMappingsByLeaseTypeIdByCid( $intLeaseTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageCustomerTypesMappings( sprintf( 'SELECT * FROM screening_package_customer_types_mappings WHERE lease_type_id = %d AND cid = %d', ( int ) $intLeaseTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageCustomerTypesMappingsByCustomerTypeIdByCid( $intCustomerTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageCustomerTypesMappings( sprintf( 'SELECT * FROM screening_package_customer_types_mappings WHERE customer_type_id = %d AND cid = %d', ( int ) $intCustomerTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageCustomerTypesMappingsByCustomerRelationshipIdByCid( $intCustomerRelationshipId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageCustomerTypesMappings( sprintf( 'SELECT * FROM screening_package_customer_types_mappings WHERE customer_relationship_id = %d AND cid = %d', ( int ) $intCustomerRelationshipId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageCustomerTypesMappingsByDefaultScreeningPackageIdByCid( $intDefaultScreeningPackageId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageCustomerTypesMappings( sprintf( 'SELECT * FROM screening_package_customer_types_mappings WHERE default_screening_package_id = %d AND cid = %d', ( int ) $intDefaultScreeningPackageId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScreeningPackageCustomerTypesMappingsByScreeningApplicantRequirementTypeIdByCid( $intScreeningApplicantRequirementTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningPackageCustomerTypesMappings( sprintf( 'SELECT * FROM screening_package_customer_types_mappings WHERE screening_applicant_requirement_type_id = %d AND cid = %d', ( int ) $intScreeningApplicantRequirementTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>