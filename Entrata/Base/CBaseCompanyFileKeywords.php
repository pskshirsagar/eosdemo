<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyFileKeywords extends CEosPluralBase {

    const TABLE_COMPANY_FILE_KEYWORDS = 'public.company_file_keywords';

    public static function fetchCompanyFileKeywords( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CCompanyFileKeyword', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchCompanyFileKeyword( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CCompanyFileKeyword', $objDatabase );
    }

    public static function fetchCompanyFileKeywordCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'company_file_keywords', $objDatabase );
    }

    public static function fetchCompanyFileKeywordByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchCompanyFileKeyword( sprintf( 'SELECT * FROM company_file_keywords WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchCompanyFileKeywordsByCid( $intCid, $objDatabase ) {
        return self::fetchCompanyFileKeywords( sprintf( 'SELECT * FROM company_file_keywords WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

}
?>