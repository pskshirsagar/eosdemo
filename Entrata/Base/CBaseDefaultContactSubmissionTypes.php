<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultContactSubmissionTypes
 * Do not add any new functions to this class.
 */

class CBaseDefaultContactSubmissionTypes extends CEosPluralBase {

	/**
	 * @return CDefaultContactSubmissionType[]
	 */
	public static function fetchDefaultContactSubmissionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultContactSubmissionType::class, $objDatabase );
	}

	/**
	 * @return CDefaultContactSubmissionType
	 */
	public static function fetchDefaultContactSubmissionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultContactSubmissionType::class, $objDatabase );
	}

	public static function fetchDefaultContactSubmissionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_contact_submission_types', $objDatabase );
	}

	public static function fetchDefaultContactSubmissionTypeById( $intId, $objDatabase ) {
		return self::fetchDefaultContactSubmissionType( sprintf( 'SELECT * FROM default_contact_submission_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>