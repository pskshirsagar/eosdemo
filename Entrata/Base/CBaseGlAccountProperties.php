<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlAccountProperties
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlAccountProperties extends CEosPluralBase {

	/**
	 * @return CGlAccountProperty[]
	 */
	public static function fetchGlAccountProperties( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CGlAccountProperty', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CGlAccountProperty
	 */
	public static function fetchGlAccountProperty( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGlAccountProperty', $objDatabase );
	}

	public static function fetchGlAccountPropertyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_account_properties', $objDatabase );
	}

	public static function fetchGlAccountPropertyByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchGlAccountProperty( sprintf( 'SELECT * FROM gl_account_properties WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlAccountPropertiesByCid( $intCid, $objDatabase ) {
		return self::fetchGlAccountProperties( sprintf( 'SELECT * FROM gl_account_properties WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlAccountPropertiesByGlAccountIdByCid( $intGlAccountId, $intCid, $objDatabase ) {
		return self::fetchGlAccountProperties( sprintf( 'SELECT * FROM gl_account_properties WHERE gl_account_id = %d AND cid = %d', ( int ) $intGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlAccountPropertiesByPropertyGroupIdByCid( $intPropertyGroupId, $intCid, $objDatabase ) {
		return self::fetchGlAccountProperties( sprintf( 'SELECT * FROM gl_account_properties WHERE property_group_id = %d AND cid = %d', ( int ) $intPropertyGroupId, ( int ) $intCid ), $objDatabase );
	}

}
?>