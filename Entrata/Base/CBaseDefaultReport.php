<?php

class CBaseDefaultReport extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_reports';

	protected $m_intId;
	protected $m_intReportTypeId;
	protected $m_intDefaultReportGroupId;
	protected $m_strName;
	protected $m_strTitle;
	protected $m_strDescription;
	protected $m_boolIsPublished;
	protected $m_boolIsAllowedForApi;
	protected $m_boolIsNewDocumentation;
	protected $m_boolIsSchedulable;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = false;
		$this->m_boolIsAllowedForApi = false;
		$this->m_boolIsNewDocumentation = false;
		$this->m_boolIsSchedulable = false;
		$this->m_intUpdatedBy = '1';
		$this->m_intCreatedBy = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['report_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReportTypeId', trim( $arrValues['report_type_id'] ) ); elseif( isset( $arrValues['report_type_id'] ) ) $this->setReportTypeId( $arrValues['report_type_id'] );
		if( isset( $arrValues['default_report_group_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultReportGroupId', trim( $arrValues['default_report_group_id'] ) ); elseif( isset( $arrValues['default_report_group_id'] ) ) $this->setDefaultReportGroupId( $arrValues['default_report_group_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['is_allowed_for_api'] ) && $boolDirectSet ) $this->set( 'm_boolIsAllowedForApi', trim( stripcslashes( $arrValues['is_allowed_for_api'] ) ) ); elseif( isset( $arrValues['is_allowed_for_api'] ) ) $this->setIsAllowedForApi( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_allowed_for_api'] ) : $arrValues['is_allowed_for_api'] );
		if( isset( $arrValues['is_new_documentation'] ) && $boolDirectSet ) $this->set( 'm_boolIsNewDocumentation', trim( stripcslashes( $arrValues['is_new_documentation'] ) ) ); elseif( isset( $arrValues['is_new_documentation'] ) ) $this->setIsNewDocumentation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_new_documentation'] ) : $arrValues['is_new_documentation'] );
		if( isset( $arrValues['is_schedulable'] ) && $boolDirectSet ) $this->set( 'm_boolIsSchedulable', trim( stripcslashes( $arrValues['is_schedulable'] ) ) ); elseif( isset( $arrValues['is_schedulable'] ) ) $this->setIsSchedulable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_schedulable'] ) : $arrValues['is_schedulable'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setReportTypeId( $intReportTypeId ) {
		$this->set( 'm_intReportTypeId', CStrings::strToIntDef( $intReportTypeId, NULL, false ) );
	}

	public function getReportTypeId() {
		return $this->m_intReportTypeId;
	}

	public function sqlReportTypeId() {
		return ( true == isset( $this->m_intReportTypeId ) ) ? ( string ) $this->m_intReportTypeId : 'NULL';
	}

	public function setDefaultReportGroupId( $intDefaultReportGroupId ) {
		$this->set( 'm_intDefaultReportGroupId', CStrings::strToIntDef( $intDefaultReportGroupId, NULL, false ) );
	}

	public function getDefaultReportGroupId() {
		return $this->m_intDefaultReportGroupId;
	}

	public function sqlDefaultReportGroupId() {
		return ( true == isset( $this->m_intDefaultReportGroupId ) ) ? ( string ) $this->m_intDefaultReportGroupId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 240, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsAllowedForApi( $boolIsAllowedForApi ) {
		$this->set( 'm_boolIsAllowedForApi', CStrings::strToBool( $boolIsAllowedForApi ) );
	}

	public function getIsAllowedForApi() {
		return $this->m_boolIsAllowedForApi;
	}

	public function sqlIsAllowedForApi() {
		return ( true == isset( $this->m_boolIsAllowedForApi ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAllowedForApi ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsNewDocumentation( $boolIsNewDocumentation ) {
		$this->set( 'm_boolIsNewDocumentation', CStrings::strToBool( $boolIsNewDocumentation ) );
	}

	public function getIsNewDocumentation() {
		return $this->m_boolIsNewDocumentation;
	}

	public function sqlIsNewDocumentation() {
		return ( true == isset( $this->m_boolIsNewDocumentation ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsNewDocumentation ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSchedulable( $boolIsSchedulable ) {
		$this->set( 'm_boolIsSchedulable', CStrings::strToBool( $boolIsSchedulable ) );
	}

	public function getIsSchedulable() {
		return $this->m_boolIsSchedulable;
	}

	public function sqlIsSchedulable() {
		return ( true == isset( $this->m_boolIsSchedulable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSchedulable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : '1';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, report_type_id, default_report_group_id, name, title, description, is_published, is_allowed_for_api, is_new_documentation, is_schedulable, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlReportTypeId() . ', ' .
						$this->sqlDefaultReportGroupId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsAllowedForApi() . ', ' .
						$this->sqlIsNewDocumentation() . ', ' .
						$this->sqlIsSchedulable() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_type_id = ' . $this->sqlReportTypeId(). ',' ; } elseif( true == array_key_exists( 'ReportTypeId', $this->getChangedColumns() ) ) { $strSql .= ' report_type_id = ' . $this->sqlReportTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_report_group_id = ' . $this->sqlDefaultReportGroupId(). ',' ; } elseif( true == array_key_exists( 'DefaultReportGroupId', $this->getChangedColumns() ) ) { $strSql .= ' default_report_group_id = ' . $this->sqlDefaultReportGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_allowed_for_api = ' . $this->sqlIsAllowedForApi(). ',' ; } elseif( true == array_key_exists( 'IsAllowedForApi', $this->getChangedColumns() ) ) { $strSql .= ' is_allowed_for_api = ' . $this->sqlIsAllowedForApi() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_new_documentation = ' . $this->sqlIsNewDocumentation(). ',' ; } elseif( true == array_key_exists( 'IsNewDocumentation', $this->getChangedColumns() ) ) { $strSql .= ' is_new_documentation = ' . $this->sqlIsNewDocumentation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_schedulable = ' . $this->sqlIsSchedulable(). ',' ; } elseif( true == array_key_exists( 'IsSchedulable', $this->getChangedColumns() ) ) { $strSql .= ' is_schedulable = ' . $this->sqlIsSchedulable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'report_type_id' => $this->getReportTypeId(),
			'default_report_group_id' => $this->getDefaultReportGroupId(),
			'name' => $this->getName(),
			'title' => $this->getTitle(),
			'description' => $this->getDescription(),
			'is_published' => $this->getIsPublished(),
			'is_allowed_for_api' => $this->getIsAllowedForApi(),
			'is_new_documentation' => $this->getIsNewDocumentation(),
			'is_schedulable' => $this->getIsSchedulable(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>