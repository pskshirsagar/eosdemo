<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAssetNotes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAssetNotes extends CEosPluralBase {

	/**
	 * @return CAssetNote[]
	 */
	public static function fetchAssetNotes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CAssetNote', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CAssetNote
	 */
	public static function fetchAssetNote( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAssetNote', $objDatabase );
	}

	public static function fetchAssetNoteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'asset_notes', $objDatabase );
	}

	public static function fetchAssetNoteByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchAssetNote( sprintf( 'SELECT * FROM asset_notes WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetNotesByCid( $intCid, $objDatabase ) {
		return self::fetchAssetNotes( sprintf( 'SELECT * FROM asset_notes WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetNotesByAssetIdByCid( $intAssetId, $intCid, $objDatabase ) {
		return self::fetchAssetNotes( sprintf( 'SELECT * FROM asset_notes WHERE asset_id = %d AND cid = %d', ( int ) $intAssetId, ( int ) $intCid ), $objDatabase );
	}

}
?>