<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicantApplicationChange extends CEosSingularBase {

	const TABLE_NAME = 'public.applicant_application_changes';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intEventId;
	protected $m_intApplicantId;
	protected $m_intApplicationId;
	protected $m_intApplicantApplicationChangeTypeId;
	protected $m_strOldValue;
	protected $m_strNewValue;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['event_id'] ) && $boolDirectSet ) $this->set( 'm_intEventId', trim( $arrValues['event_id'] ) ); elseif( isset( $arrValues['event_id'] ) ) $this->setEventId( $arrValues['event_id'] );
		if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantId', trim( $arrValues['applicant_id'] ) ); elseif( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['applicant_application_change_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantApplicationChangeTypeId', trim( $arrValues['applicant_application_change_type_id'] ) ); elseif( isset( $arrValues['applicant_application_change_type_id'] ) ) $this->setApplicantApplicationChangeTypeId( $arrValues['applicant_application_change_type_id'] );
		if( isset( $arrValues['old_value'] ) && $boolDirectSet ) $this->set( 'm_strOldValue', trim( stripcslashes( $arrValues['old_value'] ) ) ); elseif( isset( $arrValues['old_value'] ) ) $this->setOldValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['old_value'] ) : $arrValues['old_value'] );
		if( isset( $arrValues['new_value'] ) && $boolDirectSet ) $this->set( 'm_strNewValue', trim( stripcslashes( $arrValues['new_value'] ) ) ); elseif( isset( $arrValues['new_value'] ) ) $this->setNewValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['new_value'] ) : $arrValues['new_value'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setEventId( $intEventId ) {
		$this->set( 'm_intEventId', CStrings::strToIntDef( $intEventId, NULL, false ) );
	}

	public function getEventId() {
		return $this->m_intEventId;
	}

	public function sqlEventId() {
		return ( true == isset( $this->m_intEventId ) ) ? ( string ) $this->m_intEventId : 'NULL';
	}

	public function setApplicantId( $intApplicantId ) {
		$this->set( 'm_intApplicantId', CStrings::strToIntDef( $intApplicantId, NULL, false ) );
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function sqlApplicantId() {
		return ( true == isset( $this->m_intApplicantId ) ) ? ( string ) $this->m_intApplicantId : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setApplicantApplicationChangeTypeId( $intApplicantApplicationChangeTypeId ) {
		$this->set( 'm_intApplicantApplicationChangeTypeId', CStrings::strToIntDef( $intApplicantApplicationChangeTypeId, NULL, false ) );
	}

	public function getApplicantApplicationChangeTypeId() {
		return $this->m_intApplicantApplicationChangeTypeId;
	}

	public function sqlApplicantApplicationChangeTypeId() {
		return ( true == isset( $this->m_intApplicantApplicationChangeTypeId ) ) ? ( string ) $this->m_intApplicantApplicationChangeTypeId : 'NULL';
	}

	public function setOldValue( $strOldValue ) {
		$this->set( 'm_strOldValue', CStrings::strTrimDef( $strOldValue, 240, NULL, true ) );
	}

	public function getOldValue() {
		return $this->m_strOldValue;
	}

	public function sqlOldValue() {
		return ( true == isset( $this->m_strOldValue ) ) ? '\'' . addslashes( $this->m_strOldValue ) . '\'' : 'NULL';
	}

	public function setNewValue( $strNewValue ) {
		$this->set( 'm_strNewValue', CStrings::strTrimDef( $strNewValue, 240, NULL, true ) );
	}

	public function getNewValue() {
		return $this->m_strNewValue;
	}

	public function sqlNewValue() {
		return ( true == isset( $this->m_strNewValue ) ) ? '\'' . addslashes( $this->m_strNewValue ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, event_id, applicant_id, application_id, applicant_application_change_type_id, old_value, new_value, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlEventId() . ', ' .
 						$this->sqlApplicantId() . ', ' .
 						$this->sqlApplicationId() . ', ' .
 						$this->sqlApplicantApplicationChangeTypeId() . ', ' .
 						$this->sqlOldValue() . ', ' .
 						$this->sqlNewValue() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_id = ' . $this->sqlEventId() . ','; } elseif( true == array_key_exists( 'EventId', $this->getChangedColumns() ) ) { $strSql .= ' event_id = ' . $this->sqlEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; } elseif( true == array_key_exists( 'ApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_application_change_type_id = ' . $this->sqlApplicantApplicationChangeTypeId() . ','; } elseif( true == array_key_exists( 'ApplicantApplicationChangeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_application_change_type_id = ' . $this->sqlApplicantApplicationChangeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_value = ' . $this->sqlOldValue() . ','; } elseif( true == array_key_exists( 'OldValue', $this->getChangedColumns() ) ) { $strSql .= ' old_value = ' . $this->sqlOldValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_value = ' . $this->sqlNewValue() . ','; } elseif( true == array_key_exists( 'NewValue', $this->getChangedColumns() ) ) { $strSql .= ' new_value = ' . $this->sqlNewValue() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'event_id' => $this->getEventId(),
			'applicant_id' => $this->getApplicantId(),
			'application_id' => $this->getApplicationId(),
			'applicant_application_change_type_id' => $this->getApplicantApplicationChangeTypeId(),
			'old_value' => $this->getOldValue(),
			'new_value' => $this->getNewValue(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>