<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlAccountUsageTypes
 * Do not add any new functions to this class.
 */

class CBaseGlAccountUsageTypes extends CEosPluralBase {

	/**
	 * @return CGlAccountUsageType[]
	 */
	public static function fetchGlAccountUsageTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CGlAccountUsageType::class, $objDatabase );
	}

	/**
	 * @return CGlAccountUsageType
	 */
	public static function fetchGlAccountUsageType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGlAccountUsageType::class, $objDatabase );
	}

	public static function fetchGlAccountUsageTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_account_usage_types', $objDatabase );
	}

	public static function fetchGlAccountUsageTypeById( $intId, $objDatabase ) {
		return self::fetchGlAccountUsageType( sprintf( 'SELECT * FROM gl_account_usage_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchGlAccountUsageTypesByGlAccountTypeId( $intGlAccountTypeId, $objDatabase ) {
		return self::fetchGlAccountUsageTypes( sprintf( 'SELECT * FROM gl_account_usage_types WHERE gl_account_type_id = %d', $intGlAccountTypeId ), $objDatabase );
	}

}
?>