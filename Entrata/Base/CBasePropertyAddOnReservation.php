<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyAddOnReservation extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.property_add_on_reservations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intRateAssociationId;
	protected $m_intCustomerId;
	protected $m_strName;
	protected $m_intArPaymentId;
	protected $m_strReservationStart;
	protected $m_strReservationEnd;
	protected $m_intAmenityReservationStatusTypeId;
	protected $m_strRejectionComment;
	protected $m_strReservationPurpose;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolReservedByDay;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['rate_association_id'] ) && $boolDirectSet ) $this->set( 'm_intRateAssociationId', trim( $arrValues['rate_association_id'] ) ); elseif( isset( $arrValues['rate_association_id'] ) ) $this->setRateAssociationId( $arrValues['rate_association_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['ar_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentId', trim( $arrValues['ar_payment_id'] ) ); elseif( isset( $arrValues['ar_payment_id'] ) ) $this->setArPaymentId( $arrValues['ar_payment_id'] );
		if( isset( $arrValues['reservation_start'] ) && $boolDirectSet ) $this->set( 'm_strReservationStart', trim( $arrValues['reservation_start'] ) ); elseif( isset( $arrValues['reservation_start'] ) ) $this->setReservationStart( $arrValues['reservation_start'] );
		if( isset( $arrValues['reservation_end'] ) && $boolDirectSet ) $this->set( 'm_strReservationEnd', trim( $arrValues['reservation_end'] ) ); elseif( isset( $arrValues['reservation_end'] ) ) $this->setReservationEnd( $arrValues['reservation_end'] );
		if( isset( $arrValues['amenity_reservation_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAmenityReservationStatusTypeId', trim( $arrValues['amenity_reservation_status_type_id'] ) ); elseif( isset( $arrValues['amenity_reservation_status_type_id'] ) ) $this->setAmenityReservationStatusTypeId( $arrValues['amenity_reservation_status_type_id'] );
		if( isset( $arrValues['rejection_comment'] ) && $boolDirectSet ) $this->set( 'm_strRejectionComment', trim( stripcslashes( $arrValues['rejection_comment'] ) ) ); elseif( isset( $arrValues['rejection_comment'] ) ) $this->setRejectionComment( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['rejection_comment'] ) : $arrValues['rejection_comment'] );
		if( isset( $arrValues['reservation_purpose'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strReservationPurpose', trim( stripcslashes( $arrValues['reservation_purpose'] ) ) ); elseif( isset( $arrValues['reservation_purpose'] ) ) $this->setReservationPurpose( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reservation_purpose'] ) : $arrValues['reservation_purpose'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['reserved_by_day'] ) && $boolDirectSet ) $this->set( 'm_boolReservedByDay', trim( stripcslashes( $arrValues['reserved_by_day'] ) ) ); elseif( isset( $arrValues['reserved_by_day'] ) ) $this->setReservedByDay( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reserved_by_day'] ) : $arrValues['reserved_by_day'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setRateAssociationId( $intRateAssociationId ) {
		$this->set( 'm_intRateAssociationId', CStrings::strToIntDef( $intRateAssociationId, NULL, false ) );
	}

	public function getRateAssociationId() {
		return $this->m_intRateAssociationId;
	}

	public function sqlRateAssociationId() {
		return ( true == isset( $this->m_intRateAssociationId ) ) ? ( string ) $this->m_intRateAssociationId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setArPaymentId( $intArPaymentId ) {
		$this->set( 'm_intArPaymentId', CStrings::strToIntDef( $intArPaymentId, NULL, false ) );
	}

	public function getArPaymentId() {
		return $this->m_intArPaymentId;
	}

	public function sqlArPaymentId() {
		return ( true == isset( $this->m_intArPaymentId ) ) ? ( string ) $this->m_intArPaymentId : 'NULL';
	}

	public function setReservationStart( $strReservationStart ) {
		$this->set( 'm_strReservationStart', CStrings::strTrimDef( $strReservationStart, -1, NULL, true ) );
	}

	public function getReservationStart() {
		return $this->m_strReservationStart;
	}

	public function sqlReservationStart() {
		return ( true == isset( $this->m_strReservationStart ) ) ? '\'' . $this->m_strReservationStart . '\'' : 'NOW()';
	}

	public function setReservationEnd( $strReservationEnd ) {
		$this->set( 'm_strReservationEnd', CStrings::strTrimDef( $strReservationEnd, -1, NULL, true ) );
	}

	public function getReservationEnd() {
		return $this->m_strReservationEnd;
	}

	public function sqlReservationEnd() {
		return ( true == isset( $this->m_strReservationEnd ) ) ? '\'' . $this->m_strReservationEnd . '\'' : 'NOW()';
	}

	public function setAmenityReservationStatusTypeId( $intAmenityReservationStatusTypeId ) {
		$this->set( 'm_intAmenityReservationStatusTypeId', CStrings::strToIntDef( $intAmenityReservationStatusTypeId, NULL, false ) );
	}

	public function getAmenityReservationStatusTypeId() {
		return $this->m_intAmenityReservationStatusTypeId;
	}

	public function sqlAmenityReservationStatusTypeId() {
		return ( true == isset( $this->m_intAmenityReservationStatusTypeId ) ) ? ( string ) $this->m_intAmenityReservationStatusTypeId : 'NULL';
	}

	public function setRejectionComment( $strRejectionComment ) {
		$this->set( 'm_strRejectionComment', CStrings::strTrimDef( $strRejectionComment, 500, NULL, true ) );
	}

	public function getRejectionComment() {
		return $this->m_strRejectionComment;
	}

	public function sqlRejectionComment() {
		return ( true == isset( $this->m_strRejectionComment ) ) ? '\'' . addslashes( $this->m_strRejectionComment ) . '\'' : 'NULL';
	}

	public function setReservationPurpose( $strReservationPurpose, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strReservationPurpose', CStrings::strTrimDef( $strReservationPurpose, -1, NULL, true ), $strLocaleCode );
	}

	public function getReservationPurpose( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strReservationPurpose', $strLocaleCode );
	}

	public function sqlReservationPurpose() {
		return ( true == isset( $this->m_strReservationPurpose ) ) ? '\'' . addslashes( $this->m_strReservationPurpose ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setReservedByDay( $boolReservedByDay ) {
		$this->set( 'm_boolReservedByDay', CStrings::strToBool( $boolReservedByDay ) );
	}

	public function getReservedByDay() {
		return $this->m_boolReservedByDay;
	}

	public function sqlReservedByDay() {
		return ( true == isset( $this->m_boolReservedByDay ) ) ? '\'' . ( true == ( bool ) $this->m_boolReservedByDay ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, rate_association_id, customer_id, name, ar_payment_id, reservation_start, reservation_end, amenity_reservation_status_type_id, rejection_comment, reservation_purpose, updated_by, updated_on, created_by, created_on, reserved_by_day, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlRateAssociationId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlArPaymentId() . ', ' .
						$this->sqlReservationStart() . ', ' .
						$this->sqlReservationEnd() . ', ' .
						$this->sqlAmenityReservationStatusTypeId() . ', ' .
						$this->sqlRejectionComment() . ', ' .
						$this->sqlReservationPurpose() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlReservedByDay() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rate_association_id = ' . $this->sqlRateAssociationId(). ',' ; } elseif( true == array_key_exists( 'RateAssociationId', $this->getChangedColumns() ) ) { $strSql .= ' rate_association_id = ' . $this->sqlRateAssociationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId(). ',' ; } elseif( true == array_key_exists( 'ArPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reservation_start = ' . $this->sqlReservationStart(). ',' ; } elseif( true == array_key_exists( 'ReservationStart', $this->getChangedColumns() ) ) { $strSql .= ' reservation_start = ' . $this->sqlReservationStart() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reservation_end = ' . $this->sqlReservationEnd(). ',' ; } elseif( true == array_key_exists( 'ReservationEnd', $this->getChangedColumns() ) ) { $strSql .= ' reservation_end = ' . $this->sqlReservationEnd() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_reservation_status_type_id = ' . $this->sqlAmenityReservationStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'AmenityReservationStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' amenity_reservation_status_type_id = ' . $this->sqlAmenityReservationStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejection_comment = ' . $this->sqlRejectionComment(). ',' ; } elseif( true == array_key_exists( 'RejectionComment', $this->getChangedColumns() ) ) { $strSql .= ' rejection_comment = ' . $this->sqlRejectionComment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reservation_purpose = ' . $this->sqlReservationPurpose(). ',' ; } elseif( true == array_key_exists( 'ReservationPurpose', $this->getChangedColumns() ) ) { $strSql .= ' reservation_purpose = ' . $this->sqlReservationPurpose() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reserved_by_day = ' . $this->sqlReservedByDay(). ',' ; } elseif( true == array_key_exists( 'ReservedByDay', $this->getChangedColumns() ) ) { $strSql .= ' reserved_by_day = ' . $this->sqlReservedByDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'rate_association_id' => $this->getRateAssociationId(),
			'customer_id' => $this->getCustomerId(),
			'name' => $this->getName(),
			'ar_payment_id' => $this->getArPaymentId(),
			'reservation_start' => $this->getReservationStart(),
			'reservation_end' => $this->getReservationEnd(),
			'amenity_reservation_status_type_id' => $this->getAmenityReservationStatusTypeId(),
			'rejection_comment' => $this->getRejectionComment(),
			'reservation_purpose' => $this->getReservationPurpose(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'reserved_by_day' => $this->getReservedByDay(),
			'details' => $this->getDetails()
		);
	}

}
?>