<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyMaintenancePriority extends CEosSingularBase {

	const TABLE_NAME = 'public.property_maintenance_priorities';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intMaintenancePriorityId;
	protected $m_strRemotePrimaryKey;
	protected $m_intSendEmail;
	protected $m_intSendSms;
	protected $m_intIsPublished;
	protected $m_intIsDefault;
	protected $m_intOrderNum;
	protected $m_intShowOnResidentPortal;
	protected $m_fltResponseInterval;
	protected $m_intResponseIntervalTypeId;
	protected $m_fltDueInterval;
	protected $m_intDueIntervalTypeId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolUseCalendarHours;
	protected $m_intMaintenanceHourTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_intSendEmail = '0';
		$this->m_intSendSms = '0';
		$this->m_intIsPublished = '1';
		$this->m_intIsDefault = '0';
		$this->m_intOrderNum = '0';
		$this->m_intShowOnResidentPortal = '1';
		$this->m_intResponseIntervalTypeId = '2';
		$this->m_intDueIntervalTypeId = '3';
		$this->m_boolUseCalendarHours = true;
		$this->m_intMaintenanceHourTypeId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['maintenance_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenancePriorityId', trim( $arrValues['maintenance_priority_id'] ) ); elseif( isset( $arrValues['maintenance_priority_id'] ) ) $this->setMaintenancePriorityId( $arrValues['maintenance_priority_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['send_email'] ) && $boolDirectSet ) $this->set( 'm_intSendEmail', trim( $arrValues['send_email'] ) ); elseif( isset( $arrValues['send_email'] ) ) $this->setSendEmail( $arrValues['send_email'] );
		if( isset( $arrValues['send_sms'] ) && $boolDirectSet ) $this->set( 'm_intSendSms', trim( $arrValues['send_sms'] ) ); elseif( isset( $arrValues['send_sms'] ) ) $this->setSendSms( $arrValues['send_sms'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_intIsDefault', trim( $arrValues['is_default'] ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( $arrValues['is_default'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['show_on_resident_portal'] ) && $boolDirectSet ) $this->set( 'm_intShowOnResidentPortal', trim( $arrValues['show_on_resident_portal'] ) ); elseif( isset( $arrValues['show_on_resident_portal'] ) ) $this->setShowOnResidentPortal( $arrValues['show_on_resident_portal'] );
		if( isset( $arrValues['response_interval'] ) && $boolDirectSet ) $this->set( 'm_fltResponseInterval', trim( $arrValues['response_interval'] ) ); elseif( isset( $arrValues['response_interval'] ) ) $this->setResponseInterval( $arrValues['response_interval'] );
		if( isset( $arrValues['response_interval_type_id'] ) && $boolDirectSet ) $this->set( 'm_intResponseIntervalTypeId', trim( $arrValues['response_interval_type_id'] ) ); elseif( isset( $arrValues['response_interval_type_id'] ) ) $this->setResponseIntervalTypeId( $arrValues['response_interval_type_id'] );
		if( isset( $arrValues['due_interval'] ) && $boolDirectSet ) $this->set( 'm_fltDueInterval', trim( $arrValues['due_interval'] ) ); elseif( isset( $arrValues['due_interval'] ) ) $this->setDueInterval( $arrValues['due_interval'] );
		if( isset( $arrValues['due_interval_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDueIntervalTypeId', trim( $arrValues['due_interval_type_id'] ) ); elseif( isset( $arrValues['due_interval_type_id'] ) ) $this->setDueIntervalTypeId( $arrValues['due_interval_type_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['use_calendar_hours'] ) && $boolDirectSet ) $this->set( 'm_boolUseCalendarHours', trim( stripcslashes( $arrValues['use_calendar_hours'] ) ) ); elseif( isset( $arrValues['use_calendar_hours'] ) ) $this->setUseCalendarHours( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_calendar_hours'] ) : $arrValues['use_calendar_hours'] );
		if( isset( $arrValues['maintenance_hour_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceHourTypeId', trim( $arrValues['maintenance_hour_type_id'] ) ); elseif( isset( $arrValues['maintenance_hour_type_id'] ) ) $this->setMaintenanceHourTypeId( $arrValues['maintenance_hour_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setMaintenancePriorityId( $intMaintenancePriorityId ) {
		$this->set( 'm_intMaintenancePriorityId', CStrings::strToIntDef( $intMaintenancePriorityId, NULL, false ) );
	}

	public function getMaintenancePriorityId() {
		return $this->m_intMaintenancePriorityId;
	}

	public function sqlMaintenancePriorityId() {
		return ( true == isset( $this->m_intMaintenancePriorityId ) ) ? ( string ) $this->m_intMaintenancePriorityId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setSendEmail( $intSendEmail ) {
		$this->set( 'm_intSendEmail', CStrings::strToIntDef( $intSendEmail, NULL, false ) );
	}

	public function getSendEmail() {
		return $this->m_intSendEmail;
	}

	public function sqlSendEmail() {
		return ( true == isset( $this->m_intSendEmail ) ) ? ( string ) $this->m_intSendEmail : '0';
	}

	public function setSendSms( $intSendSms ) {
		$this->set( 'm_intSendSms', CStrings::strToIntDef( $intSendSms, NULL, false ) );
	}

	public function getSendSms() {
		return $this->m_intSendSms;
	}

	public function sqlSendSms() {
		return ( true == isset( $this->m_intSendSms ) ) ? ( string ) $this->m_intSendSms : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsDefault( $intIsDefault ) {
		$this->set( 'm_intIsDefault', CStrings::strToIntDef( $intIsDefault, NULL, false ) );
	}

	public function getIsDefault() {
		return $this->m_intIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_intIsDefault ) ) ? ( string ) $this->m_intIsDefault : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setShowOnResidentPortal( $intShowOnResidentPortal ) {
		$this->set( 'm_intShowOnResidentPortal', CStrings::strToIntDef( $intShowOnResidentPortal, NULL, false ) );
	}

	public function getShowOnResidentPortal() {
		return $this->m_intShowOnResidentPortal;
	}

	public function sqlShowOnResidentPortal() {
		return ( true == isset( $this->m_intShowOnResidentPortal ) ) ? ( string ) $this->m_intShowOnResidentPortal : '1';
	}

	public function setResponseInterval( $fltResponseInterval ) {
		$this->set( 'm_fltResponseInterval', CStrings::strToFloatDef( $fltResponseInterval, NULL, false, 0 ) );
	}

	public function getResponseInterval() {
		return $this->m_fltResponseInterval;
	}

	public function sqlResponseInterval() {
		return ( true == isset( $this->m_fltResponseInterval ) ) ? ( string ) $this->m_fltResponseInterval : 'NULL';
	}

	public function setResponseIntervalTypeId( $intResponseIntervalTypeId ) {
		$this->set( 'm_intResponseIntervalTypeId', CStrings::strToIntDef( $intResponseIntervalTypeId, NULL, false ) );
	}

	public function getResponseIntervalTypeId() {
		return $this->m_intResponseIntervalTypeId;
	}

	public function sqlResponseIntervalTypeId() {
		return ( true == isset( $this->m_intResponseIntervalTypeId ) ) ? ( string ) $this->m_intResponseIntervalTypeId : '2';
	}

	public function setDueInterval( $fltDueInterval ) {
		$this->set( 'm_fltDueInterval', CStrings::strToFloatDef( $fltDueInterval, NULL, false, 0 ) );
	}

	public function getDueInterval() {
		return $this->m_fltDueInterval;
	}

	public function sqlDueInterval() {
		return ( true == isset( $this->m_fltDueInterval ) ) ? ( string ) $this->m_fltDueInterval : 'NULL';
	}

	public function setDueIntervalTypeId( $intDueIntervalTypeId ) {
		$this->set( 'm_intDueIntervalTypeId', CStrings::strToIntDef( $intDueIntervalTypeId, NULL, false ) );
	}

	public function getDueIntervalTypeId() {
		return $this->m_intDueIntervalTypeId;
	}

	public function sqlDueIntervalTypeId() {
		return ( true == isset( $this->m_intDueIntervalTypeId ) ) ? ( string ) $this->m_intDueIntervalTypeId : '3';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUseCalendarHours( $boolUseCalendarHours ) {
		$this->set( 'm_boolUseCalendarHours', CStrings::strToBool( $boolUseCalendarHours ) );
	}

	public function getUseCalendarHours() {
		return $this->m_boolUseCalendarHours;
	}

	public function sqlUseCalendarHours() {
		return ( true == isset( $this->m_boolUseCalendarHours ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseCalendarHours ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setMaintenanceHourTypeId( $intMaintenanceHourTypeId ) {
		$this->set( 'm_intMaintenanceHourTypeId', CStrings::strToIntDef( $intMaintenanceHourTypeId, NULL, false ) );
	}

	public function getMaintenanceHourTypeId() {
		return $this->m_intMaintenanceHourTypeId;
	}

	public function sqlMaintenanceHourTypeId() {
		return ( true == isset( $this->m_intMaintenanceHourTypeId ) ) ? ( string ) $this->m_intMaintenanceHourTypeId : '1';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, maintenance_priority_id, remote_primary_key, send_email, send_sms, is_published, is_default, order_num, show_on_resident_portal, response_interval, response_interval_type_id, due_interval, due_interval_type_id, updated_by, updated_on, created_by, created_on, use_calendar_hours, maintenance_hour_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlMaintenancePriorityId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlSendEmail() . ', ' .
						$this->sqlSendSms() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsDefault() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlShowOnResidentPortal() . ', ' .
						$this->sqlResponseInterval() . ', ' .
						$this->sqlResponseIntervalTypeId() . ', ' .
						$this->sqlDueInterval() . ', ' .
						$this->sqlDueIntervalTypeId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlUseCalendarHours() . ', ' .
						$this->sqlMaintenanceHourTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_priority_id = ' . $this->sqlMaintenancePriorityId(). ',' ; } elseif( true == array_key_exists( 'MaintenancePriorityId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_priority_id = ' . $this->sqlMaintenancePriorityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' send_email = ' . $this->sqlSendEmail(). ',' ; } elseif( true == array_key_exists( 'SendEmail', $this->getChangedColumns() ) ) { $strSql .= ' send_email = ' . $this->sqlSendEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' send_sms = ' . $this->sqlSendSms(). ',' ; } elseif( true == array_key_exists( 'SendSms', $this->getChangedColumns() ) ) { $strSql .= ' send_sms = ' . $this->sqlSendSms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault(). ',' ; } elseif( true == array_key_exists( 'IsDefault', $this->getChangedColumns() ) ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_on_resident_portal = ' . $this->sqlShowOnResidentPortal(). ',' ; } elseif( true == array_key_exists( 'ShowOnResidentPortal', $this->getChangedColumns() ) ) { $strSql .= ' show_on_resident_portal = ' . $this->sqlShowOnResidentPortal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_interval = ' . $this->sqlResponseInterval(). ',' ; } elseif( true == array_key_exists( 'ResponseInterval', $this->getChangedColumns() ) ) { $strSql .= ' response_interval = ' . $this->sqlResponseInterval() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_interval_type_id = ' . $this->sqlResponseIntervalTypeId(). ',' ; } elseif( true == array_key_exists( 'ResponseIntervalTypeId', $this->getChangedColumns() ) ) { $strSql .= ' response_interval_type_id = ' . $this->sqlResponseIntervalTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_interval = ' . $this->sqlDueInterval(). ',' ; } elseif( true == array_key_exists( 'DueInterval', $this->getChangedColumns() ) ) { $strSql .= ' due_interval = ' . $this->sqlDueInterval() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_interval_type_id = ' . $this->sqlDueIntervalTypeId(). ',' ; } elseif( true == array_key_exists( 'DueIntervalTypeId', $this->getChangedColumns() ) ) { $strSql .= ' due_interval_type_id = ' . $this->sqlDueIntervalTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_calendar_hours = ' . $this->sqlUseCalendarHours(). ',' ; } elseif( true == array_key_exists( 'UseCalendarHours', $this->getChangedColumns() ) ) { $strSql .= ' use_calendar_hours = ' . $this->sqlUseCalendarHours() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_hour_type_id = ' . $this->sqlMaintenanceHourTypeId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceHourTypeId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_hour_type_id = ' . $this->sqlMaintenanceHourTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'maintenance_priority_id' => $this->getMaintenancePriorityId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'send_email' => $this->getSendEmail(),
			'send_sms' => $this->getSendSms(),
			'is_published' => $this->getIsPublished(),
			'is_default' => $this->getIsDefault(),
			'order_num' => $this->getOrderNum(),
			'show_on_resident_portal' => $this->getShowOnResidentPortal(),
			'response_interval' => $this->getResponseInterval(),
			'response_interval_type_id' => $this->getResponseIntervalTypeId(),
			'due_interval' => $this->getDueInterval(),
			'due_interval_type_id' => $this->getDueIntervalTypeId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'use_calendar_hours' => $this->getUseCalendarHours(),
			'maintenance_hour_type_id' => $this->getMaintenanceHourTypeId()
		);
	}

}
?>