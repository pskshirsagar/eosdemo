<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlAccountTrees
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlAccountTrees extends CEosPluralBase {

	/**
	 * @return CGlAccountTree[]
	 */
	public static function fetchGlAccountTrees( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CGlAccountTree::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CGlAccountTree
	 */
	public static function fetchGlAccountTree( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGlAccountTree::class, $objDatabase );
	}

	public static function fetchGlAccountTreeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_account_trees', $objDatabase );
	}

	public static function fetchGlAccountTreeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchGlAccountTree( sprintf( 'SELECT * FROM gl_account_trees WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchGlAccountTreesByCid( $intCid, $objDatabase ) {
		return self::fetchGlAccountTrees( sprintf( 'SELECT * FROM gl_account_trees WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchGlAccountTreesByGlTreeIdByCid( $intGlTreeId, $intCid, $objDatabase ) {
		return self::fetchGlAccountTrees( sprintf( 'SELECT * FROM gl_account_trees WHERE gl_tree_id = %d AND cid = %d', $intGlTreeId, $intCid ), $objDatabase );
	}

	public static function fetchGlAccountTreesByGlGroupIdByCid( $intGlGroupId, $intCid, $objDatabase ) {
		return self::fetchGlAccountTrees( sprintf( 'SELECT * FROM gl_account_trees WHERE gl_group_id = %d AND cid = %d', $intGlGroupId, $intCid ), $objDatabase );
	}

	public static function fetchGlAccountTreesByGlAccountIdByCid( $intGlAccountId, $intCid, $objDatabase ) {
		return self::fetchGlAccountTrees( sprintf( 'SELECT * FROM gl_account_trees WHERE gl_account_id = %d AND cid = %d', $intGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchGlAccountTreesByGlAccountTreeIdByCid( $intGlAccountTreeId, $intCid, $objDatabase ) {
		return self::fetchGlAccountTrees( sprintf( 'SELECT * FROM gl_account_trees WHERE gl_account_tree_id = %d AND cid = %d', $intGlAccountTreeId, $intCid ), $objDatabase );
	}

	public static function fetchGlAccountTreesByGroupingGlAccountIdByCid( $intGroupingGlAccountId, $intCid, $objDatabase ) {
		return self::fetchGlAccountTrees( sprintf( 'SELECT * FROM gl_account_trees WHERE grouping_gl_account_id = %d AND cid = %d', $intGroupingGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchGlAccountTreesByGlAccountTypeIdByCid( $intGlAccountTypeId, $intCid, $objDatabase ) {
		return self::fetchGlAccountTrees( sprintf( 'SELECT * FROM gl_account_trees WHERE gl_account_type_id = %d AND cid = %d', $intGlAccountTypeId, $intCid ), $objDatabase );
	}

}
?>