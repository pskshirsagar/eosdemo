<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeadCommunicationSchedules
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeadCommunicationSchedules extends CEosPluralBase {

	/**
	 * @return CLeadCommunicationSchedule[]
	 */
	public static function fetchLeadCommunicationSchedules( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CLeadCommunicationSchedule', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CLeadCommunicationSchedule
	 */
	public static function fetchLeadCommunicationSchedule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CLeadCommunicationSchedule', $objDatabase );
	}

	public static function fetchLeadCommunicationScheduleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'lead_communication_schedules', $objDatabase );
	}

	public static function fetchLeadCommunicationScheduleByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchLeadCommunicationSchedule( sprintf( 'SELECT * FROM lead_communication_schedules WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeadCommunicationSchedulesByCid( $intCid, $objDatabase ) {
		return self::fetchLeadCommunicationSchedules( sprintf( 'SELECT * FROM lead_communication_schedules WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>