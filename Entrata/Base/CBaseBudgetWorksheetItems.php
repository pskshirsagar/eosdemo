<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetWorksheetItems
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetWorksheetItems extends CEosPluralBase {

	/**
	 * @return CBudgetWorksheetItem[]
	 */
	public static function fetchBudgetWorksheetItems( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CBudgetWorksheetItem::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBudgetWorksheetItem
	 */
	public static function fetchBudgetWorksheetItem( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetWorksheetItem::class, $objDatabase );
	}

	public static function fetchBudgetWorksheetItemCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_worksheet_items', $objDatabase );
	}

	public static function fetchBudgetWorksheetItemByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorksheetItem( sprintf( 'SELECT * FROM budget_worksheet_items WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorksheetItemsByCid( $intCid, $objDatabase ) {
		return self::fetchBudgetWorksheetItems( sprintf( 'SELECT * FROM budget_worksheet_items WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorksheetItemsByBudgetWorksheetIdByCid( $intBudgetWorksheetId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorksheetItems( sprintf( 'SELECT * FROM budget_worksheet_items WHERE budget_worksheet_id = %d AND cid = %d', $intBudgetWorksheetId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorksheetItemsByGlAccountIdByCid( $intGlAccountId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorksheetItems( sprintf( 'SELECT * FROM budget_worksheet_items WHERE gl_account_id = %d AND cid = %d', $intGlAccountId, $intCid ), $objDatabase );
	}

	public static function fetchBudgetWorksheetItemsByBudgetDataSourceTypeIdByCid( $intBudgetDataSourceTypeId, $intCid, $objDatabase ) {
		return self::fetchBudgetWorksheetItems( sprintf( 'SELECT * FROM budget_worksheet_items WHERE budget_data_source_type_id = %d AND cid = %d', $intBudgetDataSourceTypeId, $intCid ), $objDatabase );
	}

}
?>