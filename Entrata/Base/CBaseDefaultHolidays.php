<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultHolidays
 * Do not add any new functions to this class.
 */

class CBaseDefaultHolidays extends CEosPluralBase {

	/**
	 * @return CDefaultHoliday[]
	 */
	public static function fetchDefaultHolidays( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultHoliday::class, $objDatabase );
	}

	/**
	 * @return CDefaultHoliday
	 */
	public static function fetchDefaultHoliday( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultHoliday::class, $objDatabase );
	}

	public static function fetchDefaultHolidayCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_holidays', $objDatabase );
	}

	public static function fetchDefaultHolidayById( $intId, $objDatabase ) {
		return self::fetchDefaultHoliday( sprintf( 'SELECT * FROM default_holidays WHERE id = %d', $intId ), $objDatabase );
	}

}
?>