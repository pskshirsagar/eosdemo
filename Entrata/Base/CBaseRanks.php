<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRanks
 * Do not add any new functions to this class.
 */

class CBaseRanks extends CEosPluralBase {

	/**
	 * @return CRank[]
	 */
	public static function fetchRanks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRank::class, $objDatabase );
	}

	/**
	 * @return CRank
	 */
	public static function fetchRank( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRank::class, $objDatabase );
	}

	public static function fetchRankCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ranks', $objDatabase );
	}

	public static function fetchRankById( $intId, $objDatabase ) {
		return self::fetchRank( sprintf( 'SELECT * FROM ranks WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchRanksByRankGroupId( $intRankGroupId, $objDatabase ) {
		return self::fetchRanks( sprintf( 'SELECT * FROM ranks WHERE rank_group_id = %d', $intRankGroupId ), $objDatabase );
	}

}
?>