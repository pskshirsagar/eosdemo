<?php

class CBaseLeaseStatusType extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.lease_status_types';

	protected $m_intId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intLoginAllowed;
	protected $m_intIsActive;
	protected $m_intIsApproved;
	protected $m_intIsPending;
	protected $m_intIsChargedRent;
	protected $m_intIsEvict;
	protected $m_intIsExpired;
	protected $m_intIsArchiveable;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intLoginAllowed = '1';
		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['login_allowed'] ) && $boolDirectSet ) $this->set( 'm_intLoginAllowed', trim( $arrValues['login_allowed'] ) ); elseif( isset( $arrValues['login_allowed'] ) ) $this->setLoginAllowed( $arrValues['login_allowed'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_intIsActive', trim( $arrValues['is_active'] ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( $arrValues['is_active'] );
		if( isset( $arrValues['is_approved'] ) && $boolDirectSet ) $this->set( 'm_intIsApproved', trim( $arrValues['is_approved'] ) ); elseif( isset( $arrValues['is_approved'] ) ) $this->setIsApproved( $arrValues['is_approved'] );
		if( isset( $arrValues['is_pending'] ) && $boolDirectSet ) $this->set( 'm_intIsPending', trim( $arrValues['is_pending'] ) ); elseif( isset( $arrValues['is_pending'] ) ) $this->setIsPending( $arrValues['is_pending'] );
		if( isset( $arrValues['is_charged_rent'] ) && $boolDirectSet ) $this->set( 'm_intIsChargedRent', trim( $arrValues['is_charged_rent'] ) ); elseif( isset( $arrValues['is_charged_rent'] ) ) $this->setIsChargedRent( $arrValues['is_charged_rent'] );
		if( isset( $arrValues['is_evict'] ) && $boolDirectSet ) $this->set( 'm_intIsEvict', trim( $arrValues['is_evict'] ) ); elseif( isset( $arrValues['is_evict'] ) ) $this->setIsEvict( $arrValues['is_evict'] );
		if( isset( $arrValues['is_expired'] ) && $boolDirectSet ) $this->set( 'm_intIsExpired', trim( $arrValues['is_expired'] ) ); elseif( isset( $arrValues['is_expired'] ) ) $this->setIsExpired( $arrValues['is_expired'] );
		if( isset( $arrValues['is_archiveable'] ) && $boolDirectSet ) $this->set( 'm_intIsArchiveable', trim( $arrValues['is_archiveable'] ) ); elseif( isset( $arrValues['is_archiveable'] ) ) $this->setIsArchiveable( $arrValues['is_archiveable'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setLoginAllowed( $intLoginAllowed ) {
		$this->set( 'm_intLoginAllowed', CStrings::strToIntDef( $intLoginAllowed, NULL, false ) );
	}

	public function getLoginAllowed() {
		return $this->m_intLoginAllowed;
	}

	public function sqlLoginAllowed() {
		return ( true == isset( $this->m_intLoginAllowed ) ) ? ( string ) $this->m_intLoginAllowed : '1';
	}

	public function setIsActive( $intIsActive ) {
		$this->set( 'm_intIsActive', CStrings::strToIntDef( $intIsActive, NULL, false ) );
	}

	public function getIsActive() {
		return $this->m_intIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_intIsActive ) ) ? ( string ) $this->m_intIsActive : 'NULL';
	}

	public function setIsApproved( $intIsApproved ) {
		$this->set( 'm_intIsApproved', CStrings::strToIntDef( $intIsApproved, NULL, false ) );
	}

	public function getIsApproved() {
		return $this->m_intIsApproved;
	}

	public function sqlIsApproved() {
		return ( true == isset( $this->m_intIsApproved ) ) ? ( string ) $this->m_intIsApproved : 'NULL';
	}

	public function setIsPending( $intIsPending ) {
		$this->set( 'm_intIsPending', CStrings::strToIntDef( $intIsPending, NULL, false ) );
	}

	public function getIsPending() {
		return $this->m_intIsPending;
	}

	public function sqlIsPending() {
		return ( true == isset( $this->m_intIsPending ) ) ? ( string ) $this->m_intIsPending : 'NULL';
	}

	public function setIsChargedRent( $intIsChargedRent ) {
		$this->set( 'm_intIsChargedRent', CStrings::strToIntDef( $intIsChargedRent, NULL, false ) );
	}

	public function getIsChargedRent() {
		return $this->m_intIsChargedRent;
	}

	public function sqlIsChargedRent() {
		return ( true == isset( $this->m_intIsChargedRent ) ) ? ( string ) $this->m_intIsChargedRent : 'NULL';
	}

	public function setIsEvict( $intIsEvict ) {
		$this->set( 'm_intIsEvict', CStrings::strToIntDef( $intIsEvict, NULL, false ) );
	}

	public function getIsEvict() {
		return $this->m_intIsEvict;
	}

	public function sqlIsEvict() {
		return ( true == isset( $this->m_intIsEvict ) ) ? ( string ) $this->m_intIsEvict : 'NULL';
	}

	public function setIsExpired( $intIsExpired ) {
		$this->set( 'm_intIsExpired', CStrings::strToIntDef( $intIsExpired, NULL, false ) );
	}

	public function getIsExpired() {
		return $this->m_intIsExpired;
	}

	public function sqlIsExpired() {
		return ( true == isset( $this->m_intIsExpired ) ) ? ( string ) $this->m_intIsExpired : 'NULL';
	}

	public function setIsArchiveable( $intIsArchiveable ) {
		$this->set( 'm_intIsArchiveable', CStrings::strToIntDef( $intIsArchiveable, NULL, false ) );
	}

	public function getIsArchiveable() {
		return $this->m_intIsArchiveable;
	}

	public function sqlIsArchiveable() {
		return ( true == isset( $this->m_intIsArchiveable ) ) ? ( string ) $this->m_intIsArchiveable : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'login_allowed' => $this->getLoginAllowed(),
			'is_active' => $this->getIsActive(),
			'is_approved' => $this->getIsApproved(),
			'is_pending' => $this->getIsPending(),
			'is_charged_rent' => $this->getIsChargedRent(),
			'is_evict' => $this->getIsEvict(),
			'is_expired' => $this->getIsExpired(),
			'is_archiveable' => $this->getIsArchiveable(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>