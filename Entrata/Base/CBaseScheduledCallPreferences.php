<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledCallPreferences
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledCallPreferences extends CEosPluralBase {

	/**
	 * @return CScheduledCallPreference[]
	 */
	public static function fetchScheduledCallPreferences( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CScheduledCallPreference', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScheduledCallPreference
	 */
	public static function fetchScheduledCallPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScheduledCallPreference', $objDatabase );
	}

	public static function fetchScheduledCallPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_call_preferences', $objDatabase );
	}

	public static function fetchScheduledCallPreferenceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScheduledCallPreference( sprintf( 'SELECT * FROM scheduled_call_preferences WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledCallPreferencesByCid( $intCid, $objDatabase ) {
		return self::fetchScheduledCallPreferences( sprintf( 'SELECT * FROM scheduled_call_preferences WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledCallPreferencesByScheduledCallIdByCid( $intScheduledCallId, $intCid, $objDatabase ) {
		return self::fetchScheduledCallPreferences( sprintf( 'SELECT * FROM scheduled_call_preferences WHERE scheduled_call_id = %d AND cid = %d', ( int ) $intScheduledCallId, ( int ) $intCid ), $objDatabase );
	}

}
?>