<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAnnouncement extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.announcements';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intAnnouncementTypeId;
	protected $m_intFrequencyId;
	protected $m_strTitle;
	protected $m_strAnnouncement;
	protected $m_strDateStart;
	protected $m_strDateEnd;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intCompanyMediaFileId;
	protected $m_boolIsPinned;
	protected $m_strPublishedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';
		$this->m_boolIsPinned = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['announcement_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAnnouncementTypeId', trim( $arrValues['announcement_type_id'] ) ); elseif( isset( $arrValues['announcement_type_id'] ) ) $this->setAnnouncementTypeId( $arrValues['announcement_type_id'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( $arrValues['title'] ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( $arrValues['title'] );
		if( isset( $arrValues['announcement'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strAnnouncement', trim( $arrValues['announcement'] ) ); elseif( isset( $arrValues['announcement'] ) ) $this->setAnnouncement( $arrValues['announcement'] );
		if( isset( $arrValues['date_start'] ) && $boolDirectSet ) $this->set( 'm_strDateStart', trim( $arrValues['date_start'] ) ); elseif( isset( $arrValues['date_start'] ) ) $this->setDateStart( $arrValues['date_start'] );
		if( isset( $arrValues['date_end'] ) && $boolDirectSet ) $this->set( 'm_strDateEnd', trim( $arrValues['date_end'] ) ); elseif( isset( $arrValues['date_end'] ) ) $this->setDateEnd( $arrValues['date_end'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['company_media_file_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMediaFileId', trim( $arrValues['company_media_file_id'] ) ); elseif( isset( $arrValues['company_media_file_id'] ) ) $this->setCompanyMediaFileId( $arrValues['company_media_file_id'] );
		if( isset( $arrValues['is_pinned'] ) && $boolDirectSet ) $this->set( 'm_boolIsPinned', trim( stripcslashes( $arrValues['is_pinned'] ) ) ); elseif( isset( $arrValues['is_pinned'] ) ) $this->setIsPinned( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_pinned'] ) : $arrValues['is_pinned'] );
		if( isset( $arrValues['published_on'] ) && $boolDirectSet ) $this->set( 'm_strPublishedOn', trim( $arrValues['published_on'] ) ); elseif( isset( $arrValues['published_on'] ) ) $this->setPublishedOn( $arrValues['published_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setAnnouncementTypeId( $intAnnouncementTypeId ) {
		$this->set( 'm_intAnnouncementTypeId', CStrings::strToIntDef( $intAnnouncementTypeId, NULL, false ) );
	}

	public function getAnnouncementTypeId() {
		return $this->m_intAnnouncementTypeId;
	}

	public function sqlAnnouncementTypeId() {
		return ( true == isset( $this->m_intAnnouncementTypeId ) ) ? ( string ) $this->m_intAnnouncementTypeId : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : 'NULL';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 50, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTitle ) : '\'' . addslashes( $this->m_strTitle ) . '\'' ) : 'NULL';
	}

	public function setAnnouncement( $strAnnouncement, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strAnnouncement', CStrings::strTrimDef( $strAnnouncement, -1, NULL, true ), $strLocaleCode );
	}

	public function getAnnouncement( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strAnnouncement', $strLocaleCode );
	}

	public function sqlAnnouncement() {
		return ( true == isset( $this->m_strAnnouncement ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAnnouncement ) : '\'' . addslashes( $this->m_strAnnouncement ) . '\'' ) : 'NULL';
	}

	public function setDateStart( $strDateStart ) {
		$this->set( 'm_strDateStart', CStrings::strTrimDef( $strDateStart, -1, NULL, true ) );
	}

	public function getDateStart() {
		return $this->m_strDateStart;
	}

	public function sqlDateStart() {
		return ( true == isset( $this->m_strDateStart ) ) ? '\'' . $this->m_strDateStart . '\'' : 'NULL';
	}

	public function setDateEnd( $strDateEnd ) {
		$this->set( 'm_strDateEnd', CStrings::strTrimDef( $strDateEnd, -1, NULL, true ) );
	}

	public function getDateEnd() {
		return $this->m_strDateEnd;
	}

	public function sqlDateEnd() {
		return ( true == isset( $this->m_strDateEnd ) ) ? '\'' . $this->m_strDateEnd . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
		$this->set( 'm_intCompanyMediaFileId', CStrings::strToIntDef( $intCompanyMediaFileId, NULL, false ) );
	}

	public function getCompanyMediaFileId() {
		return $this->m_intCompanyMediaFileId;
	}

	public function sqlCompanyMediaFileId() {
		return ( true == isset( $this->m_intCompanyMediaFileId ) ) ? ( string ) $this->m_intCompanyMediaFileId : 'NULL';
	}

	public function setIsPinned( $boolIsPinned ) {
		$this->set( 'm_boolIsPinned', CStrings::strToBool( $boolIsPinned ) );
	}

	public function getIsPinned() {
		return $this->m_boolIsPinned;
	}

	public function sqlIsPinned() {
		return ( true == isset( $this->m_boolIsPinned ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPinned ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPublishedOn( $strPublishedOn ) {
		$this->set( 'm_strPublishedOn', CStrings::strTrimDef( $strPublishedOn, -1, NULL, true ) );
	}

	public function getPublishedOn() {
		return $this->m_strPublishedOn;
	}

	public function sqlPublishedOn() {
		return ( true == isset( $this->m_strPublishedOn ) ) ? '\'' . $this->m_strPublishedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, announcement_type_id, frequency_id, title, announcement, date_start, date_end, is_published, order_num, updated_by, updated_on, created_by, created_on, details, company_media_file_id, is_pinned, published_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlAnnouncementTypeId() . ', ' .
						$this->sqlFrequencyId() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlAnnouncement() . ', ' .
						$this->sqlDateStart() . ', ' .
						$this->sqlDateEnd() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlCompanyMediaFileId() . ', ' .
						$this->sqlIsPinned() . ', ' .
						$this->sqlPublishedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' announcement_type_id = ' . $this->sqlAnnouncementTypeId(). ',' ; } elseif( true == array_key_exists( 'AnnouncementTypeId', $this->getChangedColumns() ) ) { $strSql .= ' announcement_type_id = ' . $this->sqlAnnouncementTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId(). ',' ; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' announcement = ' . $this->sqlAnnouncement(). ',' ; } elseif( true == array_key_exists( 'Announcement', $this->getChangedColumns() ) ) { $strSql .= ' announcement = ' . $this->sqlAnnouncement() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_start = ' . $this->sqlDateStart(). ',' ; } elseif( true == array_key_exists( 'DateStart', $this->getChangedColumns() ) ) { $strSql .= ' date_start = ' . $this->sqlDateStart() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_end = ' . $this->sqlDateEnd(). ',' ; } elseif( true == array_key_exists( 'DateEnd', $this->getChangedColumns() ) ) { $strSql .= ' date_end = ' . $this->sqlDateEnd() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId(). ',' ; } elseif( true == array_key_exists( 'CompanyMediaFileId', $this->getChangedColumns() ) ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_pinned = ' . $this->sqlIsPinned(). ',' ; } elseif( true == array_key_exists( 'IsPinned', $this->getChangedColumns() ) ) { $strSql .= ' is_pinned = ' . $this->sqlIsPinned() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' published_on = ' . $this->sqlPublishedOn(). ',' ; } elseif( true == array_key_exists( 'PublishedOn', $this->getChangedColumns() ) ) { $strSql .= ' published_on = ' . $this->sqlPublishedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'announcement_type_id' => $this->getAnnouncementTypeId(),
			'frequency_id' => $this->getFrequencyId(),
			'title' => $this->getTitle(),
			'announcement' => $this->getAnnouncement(),
			'date_start' => $this->getDateStart(),
			'date_end' => $this->getDateEnd(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'company_media_file_id' => $this->getCompanyMediaFileId(),
			'is_pinned' => $this->getIsPinned(),
			'published_on' => $this->getPublishedOn()
		);
	}

}
?>