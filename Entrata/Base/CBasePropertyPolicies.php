<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyPolicies
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyPolicies extends CEosPluralBase {

	/**
	 * @return CPropertyPolicy[]
	 */
	public static function fetchPropertyPolicies( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyPolicy', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyPolicy
	 */
	public static function fetchPropertyPolicy( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyPolicy', $objDatabase );
	}

	public static function fetchPropertyPolicyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_policies', $objDatabase );
	}

	public static function fetchPropertyPolicyByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyPolicy( sprintf( 'SELECT * FROM property_policies WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyPoliciesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyPolicies( sprintf( 'SELECT * FROM property_policies WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyPoliciesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyPolicies( sprintf( 'SELECT * FROM property_policies WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

}
?>