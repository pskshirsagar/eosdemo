<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CViolationTypes
 * Do not add any new functions to this class.
 */

class CBaseViolationTypes extends CEosPluralBase {

	/**
	 * @return CViolationType[]
	 */
	public static function fetchViolationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CViolationType::class, $objDatabase );
	}

	/**
	 * @return CViolationType
	 */
	public static function fetchViolationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CViolationType::class, $objDatabase );
	}

	public static function fetchViolationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'violation_types', $objDatabase );
	}

	public static function fetchViolationTypeById( $intId, $objDatabase ) {
		return self::fetchViolationType( sprintf( 'SELECT * FROM violation_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>