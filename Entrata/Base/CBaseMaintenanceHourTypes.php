<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceHourTypes
 * Do not add any new functions to this class.
 */

class CBaseMaintenanceHourTypes extends CEosPluralBase {

	/**
	 * @return CMaintenanceHourType[]
	 */
	public static function fetchMaintenanceHourTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMaintenanceHourType::class, $objDatabase );
	}

	/**
	 * @return CMaintenanceHourType
	 */
	public static function fetchMaintenanceHourType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceHourType::class, $objDatabase );
	}

	public static function fetchMaintenanceHourTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_hour_types', $objDatabase );
	}

	public static function fetchMaintenanceHourTypeById( $intId, $objDatabase ) {
		return self::fetchMaintenanceHourType( sprintf( 'SELECT * FROM maintenance_hour_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>