<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningApplicantResultLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningApplicantResultLogs extends CEosPluralBase {

	/**
	 * @return CScreeningApplicantResultLog[]
	 */
	public static function fetchScreeningApplicantResultLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CScreeningApplicantResultLog::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScreeningApplicantResultLog
	 */
	public static function fetchScreeningApplicantResultLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CScreeningApplicantResultLog::class, $objDatabase );
	}

	public static function fetchScreeningApplicantResultLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'screening_applicant_result_logs', $objDatabase );
	}

	public static function fetchScreeningApplicantResultLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicantResultLog( sprintf( 'SELECT * FROM screening_applicant_result_logs WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantResultLogsByCid( $intCid, $objDatabase ) {
		return self::fetchScreeningApplicantResultLogs( sprintf( 'SELECT * FROM screening_applicant_result_logs WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantResultLogsByScreeningApplicantResultIdByCid( $intScreeningApplicantResultId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicantResultLogs( sprintf( 'SELECT * FROM screening_applicant_result_logs WHERE screening_applicant_result_id = %d AND cid = %d', $intScreeningApplicantResultId, $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantResultLogsByScreeningPackageIdByCid( $intScreeningPackageId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicantResultLogs( sprintf( 'SELECT * FROM screening_applicant_result_logs WHERE screening_package_id = %d AND cid = %d', $intScreeningPackageId, $intCid ), $objDatabase );
	}

	public static function fetchScreeningApplicantResultLogsByScreeningRecommendationTypeIdByCid( $intScreeningRecommendationTypeId, $intCid, $objDatabase ) {
		return self::fetchScreeningApplicantResultLogs( sprintf( 'SELECT * FROM screening_applicant_result_logs WHERE screening_recommendation_type_id = %d AND cid = %d', $intScreeningRecommendationTypeId, $intCid ), $objDatabase );
	}

}
?>