<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyInspectionForms
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyInspectionForms extends CEosPluralBase {

	/**
	 * @return CPropertyInspectionForm[]
	 */
	public static function fetchPropertyInspectionForms( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyInspectionForm', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyInspectionForm
	 */
	public static function fetchPropertyInspectionForm( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyInspectionForm', $objDatabase );
	}

	public static function fetchPropertyInspectionFormCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_inspection_forms', $objDatabase );
	}

	public static function fetchPropertyInspectionFormByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyInspectionForm( sprintf( 'SELECT * FROM property_inspection_forms WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyInspectionFormsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyInspectionForms( sprintf( 'SELECT * FROM property_inspection_forms WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyInspectionFormsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyInspectionForms( sprintf( 'SELECT * FROM property_inspection_forms WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyInspectionFormsByInspectionFormIdByCid( $intInspectionFormId, $intCid, $objDatabase ) {
		return self::fetchPropertyInspectionForms( sprintf( 'SELECT * FROM property_inspection_forms WHERE inspection_form_id = %d AND cid = %d', ( int ) $intInspectionFormId, ( int ) $intCid ), $objDatabase );
	}

}
?>