<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CImportEntityReports
 * Do not add any new functions to this class.
 */

class CBaseImportEntityReports extends CEosPluralBase {

	/**
	 * @return CImportEntityReport[]
	 */
	public static function fetchImportEntityReports( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CImportEntityReport', $objDatabase );
	}

	/**
	 * @return CImportEntityReport
	 */
	public static function fetchImportEntityReport( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CImportEntityReport', $objDatabase );
	}

	public static function fetchImportEntityReportCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'import_entity_reports', $objDatabase );
	}

	public static function fetchImportEntityReportById( $intId, $objDatabase ) {
		return self::fetchImportEntityReport( sprintf( 'SELECT * FROM import_entity_reports WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchImportEntityReportsByImportEntityId( $intImportEntityId, $objDatabase ) {
		return self::fetchImportEntityReports( sprintf( 'SELECT * FROM import_entity_reports WHERE import_entity_id = %d', ( int ) $intImportEntityId ), $objDatabase );
	}

	public static function fetchImportEntityReportsByDefaultReportId( $intDefaultReportId, $objDatabase ) {
		return self::fetchImportEntityReports( sprintf( 'SELECT * FROM import_entity_reports WHERE default_report_id = %d', ( int ) $intDefaultReportId ), $objDatabase );
	}

}
?>