<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTabletButtons
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseTabletButtons extends CEosPluralBase {

	/**
	 * @return CTabletButton[]
	 */
	public static function fetchTabletButtons( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CTabletButton', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CTabletButton
	 */
	public static function fetchTabletButton( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTabletButton', $objDatabase );
	}

	public static function fetchTabletButtonCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'tablet_buttons', $objDatabase );
	}

	public static function fetchTabletButtonByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchTabletButton( sprintf( 'SELECT * FROM tablet_buttons WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTabletButtonsByCid( $intCid, $objDatabase ) {
		return self::fetchTabletButtons( sprintf( 'SELECT * FROM tablet_buttons WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTabletButtonsByWebsiteIdByCid( $intWebsiteId, $intCid, $objDatabase ) {
		return self::fetchTabletButtons( sprintf( 'SELECT * FROM tablet_buttons WHERE website_id = %d AND cid = %d', ( int ) $intWebsiteId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTabletButtonsByTabletButtonTypeIdByCid( $intTabletButtonTypeId, $intCid, $objDatabase ) {
		return self::fetchTabletButtons( sprintf( 'SELECT * FROM tablet_buttons WHERE tablet_button_type_id = %d AND cid = %d', ( int ) $intTabletButtonTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>