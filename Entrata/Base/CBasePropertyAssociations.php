<?php

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyAssociations extends CEosPluralBase {

    const TABLE_PROPERTY_ASSOCIATIONS = 'public.property_associations';

    public static function fetchPropertyAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return parent::fetchObjects( $strSql, 'CPropertyAssociation', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchPropertyAssociation( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CPropertyAssociation', $objDatabase );
    }

    public static function fetchPropertyAssociationCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'property_associations', $objDatabase );
    }

    public static function fetchPropertyAssociationByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchPropertyAssociation( sprintf( 'SELECT * FROM property_associations WHERE id = %d AND cid = %d', (int) $intId, (int) $intCid ), $objDatabase );
    }

    public static function fetchPropertyAssociationsByCid( $intCid, $objDatabase ) {
        return self::fetchPropertyAssociations( sprintf( 'SELECT * FROM property_associations WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchPropertyAssociationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
        return self::fetchPropertyAssociations( sprintf( 'SELECT * FROM property_associations WHERE property_id = %d AND cid = %d', (int) $intPropertyId, (int) $intCid ), $objDatabase );
    }

    public static function fetchPropertyAssociationsByArCascadeIdByCid( $intArCascadeId, $intCid, $objDatabase ) {
        return self::fetchPropertyAssociations( sprintf( 'SELECT * FROM property_associations WHERE ar_cascade_id = %d AND cid = %d', (int) $intArCascadeId, (int) $intCid ), $objDatabase );
    }

    public static function fetchPropertyAssociationsByArCascadeReferenceIdByCid( $intArCascadeReferenceId, $intCid, $objDatabase ) {
        return self::fetchPropertyAssociations( sprintf( 'SELECT * FROM property_associations WHERE ar_cascade_reference_id = %d AND cid = %d', (int) $intArCascadeReferenceId, (int) $intCid ), $objDatabase );
    }

    public static function fetchPropertyAssociationsByArOriginIdByCid( $intArOriginId, $intCid, $objDatabase ) {
        return self::fetchPropertyAssociations( sprintf( 'SELECT * FROM property_associations WHERE ar_origin_id = %d AND cid = %d', (int) $intArOriginId, (int) $intCid ), $objDatabase );
    }

    public static function fetchPropertyAssociationsByArOriginCategoryIdByCid( $intArOriginCategoryId, $intCid, $objDatabase ) {
        return self::fetchPropertyAssociations( sprintf( 'SELECT * FROM property_associations WHERE ar_origin_category_id = %d AND cid = %d', (int) $intArOriginCategoryId, (int) $intCid ), $objDatabase );
    }

    public static function fetchPropertyAssociationsByArOriginGroupIdByCid( $intArOriginGroupId, $intCid, $objDatabase ) {
        return self::fetchPropertyAssociations( sprintf( 'SELECT * FROM property_associations WHERE ar_origin_group_id = %d AND cid = %d', (int) $intArOriginGroupId, (int) $intCid ), $objDatabase );
    }

    public static function fetchPropertyAssociationsByArOriginReferenceIdByCid( $intArOriginReferenceId, $intCid, $objDatabase ) {
        return self::fetchPropertyAssociations( sprintf( 'SELECT * FROM property_associations WHERE ar_origin_reference_id = %d AND cid = %d', (int) $intArOriginReferenceId, (int) $intCid ), $objDatabase );
    }

    public static function fetchPropertyAssociationsByCompanyMediaFileIdByCid( $intCompanyMediaFileId, $intCid, $objDatabase ) {
        return self::fetchPropertyAssociations( sprintf( 'SELECT * FROM property_associations WHERE company_media_file_id = %d AND cid = %d', (int) $intCompanyMediaFileId, (int) $intCid ), $objDatabase );
    }

    public static function fetchPropertyAssociationsByMappingIdByCid( $intMappingId, $intCid, $objDatabase ) {
        return self::fetchPropertyAssociations( sprintf( 'SELECT * FROM property_associations WHERE mapping_id = %d AND cid = %d', (int) $intMappingId, (int) $intCid ), $objDatabase );
    }

}
?>