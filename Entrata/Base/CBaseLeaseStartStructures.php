<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseStartStructures
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseStartStructures extends CEosPluralBase {

	/**
	 * @return CLeaseStartStructure[]
	 */
	public static function fetchLeaseStartStructures( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CLeaseStartStructure::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CLeaseStartStructure
	 */
	public static function fetchLeaseStartStructure( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CLeaseStartStructure::class, $objDatabase );
	}

	public static function fetchLeaseStartStructureCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'lease_start_structures', $objDatabase );
	}

	public static function fetchLeaseStartStructureByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchLeaseStartStructure( sprintf( 'SELECT * FROM lease_start_structures WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchLeaseStartStructuresByCid( $intCid, $objDatabase ) {
		return self::fetchLeaseStartStructures( sprintf( 'SELECT * FROM lease_start_structures WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchLeaseStartStructuresByDefaultLeaseStartStructureIdByCid( $intDefaultLeaseStartStructureId, $intCid, $objDatabase ) {
		return self::fetchLeaseStartStructures( sprintf( 'SELECT * FROM lease_start_structures WHERE default_lease_start_structure_id = %d AND cid = %d', $intDefaultLeaseStartStructureId, $intCid ), $objDatabase );
	}

}
?>