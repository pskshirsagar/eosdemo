<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMessageBatchTypes
 * Do not add any new functions to this class.
 */

class CBaseMessageBatchTypes extends CEosPluralBase {

	/**
	 * @return CMessageBatchType[]
	 */
	public static function fetchMessageBatchTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMessageBatchType::class, $objDatabase );
	}

	/**
	 * @return CMessageBatchType
	 */
	public static function fetchMessageBatchType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMessageBatchType::class, $objDatabase );
	}

	public static function fetchMessageBatchTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'message_batch_types', $objDatabase );
	}

	public static function fetchMessageBatchTypeById( $intId, $objDatabase ) {
		return self::fetchMessageBatchType( sprintf( 'SELECT * FROM message_batch_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>