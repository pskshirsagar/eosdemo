<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyCertifications
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyCertifications extends CEosPluralBase {

	/**
	 * @return CSubsidyCertification[]
	 */
	public static function fetchSubsidyCertifications( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CSubsidyCertification::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSubsidyCertification
	 */
	public static function fetchSubsidyCertification( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyCertification::class, $objDatabase );
	}

	public static function fetchSubsidyCertificationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_certifications', $objDatabase );
	}

	public static function fetchSubsidyCertificationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertification( sprintf( 'SELECT * FROM subsidy_certifications WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationsByCid( $intCid, $objDatabase ) {
		return self::fetchSubsidyCertifications( sprintf( 'SELECT * FROM subsidy_certifications WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertifications( sprintf( 'SELECT * FROM subsidy_certifications WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertifications( sprintf( 'SELECT * FROM subsidy_certifications WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertifications( sprintf( 'SELECT * FROM subsidy_certifications WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationsBySubsidyContractIdByCid( $intSubsidyContractId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertifications( sprintf( 'SELECT * FROM subsidy_certifications WHERE subsidy_contract_id = %d AND cid = %d', ( int ) $intSubsidyContractId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationsBySetAsideIdByCid( $intSetAsideId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertifications( sprintf( 'SELECT * FROM subsidy_certifications WHERE set_aside_id = %d AND cid = %d', ( int ) $intSetAsideId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationsByPrimaryCustomerIdByCid( $intPrimaryCustomerId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertifications( sprintf( 'SELECT * FROM subsidy_certifications WHERE primary_customer_id = %d AND cid = %d', ( int ) $intPrimaryCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationsBySubsidyCertificationTypeIdByCid( $intSubsidyCertificationTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertifications( sprintf( 'SELECT * FROM subsidy_certifications WHERE subsidy_certification_type_id = %d AND cid = %d', ( int ) $intSubsidyCertificationTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationsBySubsidySubCertificationTypeIdByCid( $intSubsidySubCertificationTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertifications( sprintf( 'SELECT * FROM subsidy_certifications WHERE subsidy_sub_certification_type_id = %d AND cid = %d', ( int ) $intSubsidySubCertificationTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationsBySubsidyCertificationStatusTypeIdByCid( $intSubsidyCertificationStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertifications( sprintf( 'SELECT * FROM subsidy_certifications WHERE subsidy_certification_status_type_id = %d AND cid = %d', ( int ) $intSubsidyCertificationStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationsBySubsidyCertificationReasonTypeIdByCid( $intSubsidyCertificationReasonTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertifications( sprintf( 'SELECT * FROM subsidy_certifications WHERE subsidy_certification_reason_type_id = %d AND cid = %d', ( int ) $intSubsidyCertificationReasonTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationsBySubsidyIncomeLimitVersionIdByCid( $intSubsidyIncomeLimitVersionId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertifications( sprintf( 'SELECT * FROM subsidy_certifications WHERE subsidy_income_limit_version_id = %d AND cid = %d', ( int ) $intSubsidyIncomeLimitVersionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationsBySubsidyCertificationCorrectionTypeIdByCid( $intSubsidyCertificationCorrectionTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertifications( sprintf( 'SELECT * FROM subsidy_certifications WHERE subsidy_certification_correction_type_id = %d AND cid = %d', ( int ) $intSubsidyCertificationCorrectionTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationsBySubsidySignatureExceptionTypeIdByCid( $intSubsidySignatureExceptionTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertifications( sprintf( 'SELECT * FROM subsidy_certifications WHERE subsidy_signature_exception_type_id = %d AND cid = %d', ( int ) $intSubsidySignatureExceptionTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationsByCorrectedSubsidyCertificationIdByCid( $intCorrectedSubsidyCertificationId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertifications( sprintf( 'SELECT * FROM subsidy_certifications WHERE corrected_subsidy_certification_id = %d AND cid = %d', ( int ) $intCorrectedSubsidyCertificationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationsByResultOfSubsidyCertificationIdByCid( $intResultOfSubsidyCertificationId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertifications( sprintf( 'SELECT * FROM subsidy_certifications WHERE result_of_subsidy_certification_id = %d AND cid = %d', ( int ) $intResultOfSubsidyCertificationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationsBySubsidyGrossRentChangeIdByCid( $intSubsidyGrossRentChangeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertifications( sprintf( 'SELECT * FROM subsidy_certifications WHERE subsidy_gross_rent_change_id = %d AND cid = %d', ( int ) $intSubsidyGrossRentChangeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationsByRepaymentIdByCid( $intRepaymentId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertifications( sprintf( 'SELECT * FROM subsidy_certifications WHERE repayment_id = %d AND cid = %d', ( int ) $intRepaymentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationsBySubsidyTracsVersionIdByCid( $intSubsidyTracsVersionId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertifications( sprintf( 'SELECT * FROM subsidy_certifications WHERE subsidy_tracs_version_id = %d AND cid = %d', ( int ) $intSubsidyTracsVersionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSubsidyCertificationsBySubsidyRecertificationTypeIdByCid( $intSubsidyRecertificationTypeId, $intCid, $objDatabase ) {
		return self::fetchSubsidyCertifications( sprintf( 'SELECT * FROM subsidy_certifications WHERE subsidy_recertification_type_id = %d AND cid = %d', $intSubsidyRecertificationTypeId, $intCid ), $objDatabase );
	}
}
?>