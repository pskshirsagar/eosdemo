<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobUnitTypes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseJobUnitTypes extends CEosPluralBase {

	/**
	 * @return CJobUnitType[]
	 */
	public static function fetchJobUnitTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CJobUnitType::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CJobUnitType
	 */
	public static function fetchJobUnitType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CJobUnitType::class, $objDatabase );
	}

	public static function fetchJobUnitTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_unit_types', $objDatabase );
	}

	public static function fetchJobUnitTypeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchJobUnitType( sprintf( 'SELECT * FROM job_unit_types WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobUnitTypesByCid( $intCid, $objDatabase ) {
		return self::fetchJobUnitTypes( sprintf( 'SELECT * FROM job_unit_types WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobUnitTypesByJobIdByCid( $intJobId, $intCid, $objDatabase ) {
		return self::fetchJobUnitTypes( sprintf( 'SELECT * FROM job_unit_types WHERE job_id = %d AND cid = %d', ( int ) $intJobId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchJobUnitTypesByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchJobUnitTypes( sprintf( 'SELECT * FROM job_unit_types WHERE unit_type_id = %d AND cid = %d', ( int ) $intUnitTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>