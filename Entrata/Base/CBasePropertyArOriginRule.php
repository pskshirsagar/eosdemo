<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyArOriginRule extends CEosSingularBase {

	const TABLE_NAME = 'public.property_ar_origin_rules';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intArOriginId;
	protected $m_boolAllowPerItem;
	protected $m_boolAllowVolumeLookups;
	protected $m_boolAllowTieredLookups;
	protected $m_boolAllowPercentCode;
	protected $m_boolAllowPercentCodeGroup;
	protected $m_boolAllowPercentCodeType;
	protected $m_boolUseLeaseTermsForRent;
	protected $m_boolUseLeaseTermsForDeposits;
	protected $m_boolUseLeaseTermsForOther;
	protected $m_boolUseLeaseStartWindowsForRent;
	protected $m_boolUseLeaseStartWindowsForDeposits;
	protected $m_boolUseLeaseStartWindowsForOther;
	protected $m_boolUseSpaceConfigurationsForRent;
	protected $m_boolUseSpaceConfigurationsForDeposits;
	protected $m_boolUseSpaceConfigurationsForOther;
	protected $m_boolAllowRateSchedulingForRent;
	protected $m_boolAllowRateSchedulingForDeposits;
	protected $m_boolAllowRateSchedulingForOther;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolAllowFullBah;
	protected $m_boolAllowFixedBah;
	protected $m_boolAllowFixedBahAdjustment;

	public function __construct() {
		parent::__construct();

		$this->m_boolAllowPerItem = false;
		$this->m_boolAllowVolumeLookups = false;
		$this->m_boolAllowTieredLookups = false;
		$this->m_boolAllowPercentCode = false;
		$this->m_boolAllowPercentCodeGroup = false;
		$this->m_boolAllowPercentCodeType = false;
		$this->m_boolUseLeaseTermsForRent = false;
		$this->m_boolUseLeaseTermsForDeposits = false;
		$this->m_boolUseLeaseTermsForOther = false;
		$this->m_boolUseLeaseStartWindowsForRent = false;
		$this->m_boolUseLeaseStartWindowsForDeposits = false;
		$this->m_boolUseLeaseStartWindowsForOther = false;
		$this->m_boolUseSpaceConfigurationsForRent = false;
		$this->m_boolUseSpaceConfigurationsForDeposits = false;
		$this->m_boolUseSpaceConfigurationsForOther = false;
		$this->m_boolAllowRateSchedulingForRent = false;
		$this->m_boolAllowRateSchedulingForDeposits = false;
		$this->m_boolAllowRateSchedulingForOther = false;
		$this->m_boolAllowFullBah = false;
		$this->m_boolAllowFixedBah = false;
		$this->m_boolAllowFixedBahAdjustment = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['ar_origin_id'] ) && $boolDirectSet ) $this->set( 'm_intArOriginId', trim( $arrValues['ar_origin_id'] ) ); elseif( isset( $arrValues['ar_origin_id'] ) ) $this->setArOriginId( $arrValues['ar_origin_id'] );
		if( isset( $arrValues['allow_per_item'] ) && $boolDirectSet ) $this->set( 'm_boolAllowPerItem', trim( stripcslashes( $arrValues['allow_per_item'] ) ) ); elseif( isset( $arrValues['allow_per_item'] ) ) $this->setAllowPerItem( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_per_item'] ) : $arrValues['allow_per_item'] );
		if( isset( $arrValues['allow_volume_lookups'] ) && $boolDirectSet ) $this->set( 'm_boolAllowVolumeLookups', trim( stripcslashes( $arrValues['allow_volume_lookups'] ) ) ); elseif( isset( $arrValues['allow_volume_lookups'] ) ) $this->setAllowVolumeLookups( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_volume_lookups'] ) : $arrValues['allow_volume_lookups'] );
		if( isset( $arrValues['allow_tiered_lookups'] ) && $boolDirectSet ) $this->set( 'm_boolAllowTieredLookups', trim( stripcslashes( $arrValues['allow_tiered_lookups'] ) ) ); elseif( isset( $arrValues['allow_tiered_lookups'] ) ) $this->setAllowTieredLookups( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_tiered_lookups'] ) : $arrValues['allow_tiered_lookups'] );
		if( isset( $arrValues['allow_percent_code'] ) && $boolDirectSet ) $this->set( 'm_boolAllowPercentCode', trim( stripcslashes( $arrValues['allow_percent_code'] ) ) ); elseif( isset( $arrValues['allow_percent_code'] ) ) $this->setAllowPercentCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_percent_code'] ) : $arrValues['allow_percent_code'] );
		if( isset( $arrValues['allow_percent_code_group'] ) && $boolDirectSet ) $this->set( 'm_boolAllowPercentCodeGroup', trim( stripcslashes( $arrValues['allow_percent_code_group'] ) ) ); elseif( isset( $arrValues['allow_percent_code_group'] ) ) $this->setAllowPercentCodeGroup( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_percent_code_group'] ) : $arrValues['allow_percent_code_group'] );
		if( isset( $arrValues['allow_percent_code_type'] ) && $boolDirectSet ) $this->set( 'm_boolAllowPercentCodeType', trim( stripcslashes( $arrValues['allow_percent_code_type'] ) ) ); elseif( isset( $arrValues['allow_percent_code_type'] ) ) $this->setAllowPercentCodeType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_percent_code_type'] ) : $arrValues['allow_percent_code_type'] );
		if( isset( $arrValues['use_lease_terms_for_rent'] ) && $boolDirectSet ) $this->set( 'm_boolUseLeaseTermsForRent', trim( stripcslashes( $arrValues['use_lease_terms_for_rent'] ) ) ); elseif( isset( $arrValues['use_lease_terms_for_rent'] ) ) $this->setUseLeaseTermsForRent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_lease_terms_for_rent'] ) : $arrValues['use_lease_terms_for_rent'] );
		if( isset( $arrValues['use_lease_terms_for_deposits'] ) && $boolDirectSet ) $this->set( 'm_boolUseLeaseTermsForDeposits', trim( stripcslashes( $arrValues['use_lease_terms_for_deposits'] ) ) ); elseif( isset( $arrValues['use_lease_terms_for_deposits'] ) ) $this->setUseLeaseTermsForDeposits( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_lease_terms_for_deposits'] ) : $arrValues['use_lease_terms_for_deposits'] );
		if( isset( $arrValues['use_lease_terms_for_other'] ) && $boolDirectSet ) $this->set( 'm_boolUseLeaseTermsForOther', trim( stripcslashes( $arrValues['use_lease_terms_for_other'] ) ) ); elseif( isset( $arrValues['use_lease_terms_for_other'] ) ) $this->setUseLeaseTermsForOther( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_lease_terms_for_other'] ) : $arrValues['use_lease_terms_for_other'] );
		if( isset( $arrValues['use_lease_start_windows_for_rent'] ) && $boolDirectSet ) $this->set( 'm_boolUseLeaseStartWindowsForRent', trim( stripcslashes( $arrValues['use_lease_start_windows_for_rent'] ) ) ); elseif( isset( $arrValues['use_lease_start_windows_for_rent'] ) ) $this->setUseLeaseStartWindowsForRent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_lease_start_windows_for_rent'] ) : $arrValues['use_lease_start_windows_for_rent'] );
		if( isset( $arrValues['use_lease_start_windows_for_deposits'] ) && $boolDirectSet ) $this->set( 'm_boolUseLeaseStartWindowsForDeposits', trim( stripcslashes( $arrValues['use_lease_start_windows_for_deposits'] ) ) ); elseif( isset( $arrValues['use_lease_start_windows_for_deposits'] ) ) $this->setUseLeaseStartWindowsForDeposits( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_lease_start_windows_for_deposits'] ) : $arrValues['use_lease_start_windows_for_deposits'] );
		if( isset( $arrValues['use_lease_start_windows_for_other'] ) && $boolDirectSet ) $this->set( 'm_boolUseLeaseStartWindowsForOther', trim( stripcslashes( $arrValues['use_lease_start_windows_for_other'] ) ) ); elseif( isset( $arrValues['use_lease_start_windows_for_other'] ) ) $this->setUseLeaseStartWindowsForOther( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_lease_start_windows_for_other'] ) : $arrValues['use_lease_start_windows_for_other'] );
		if( isset( $arrValues['use_space_configurations_for_rent'] ) && $boolDirectSet ) $this->set( 'm_boolUseSpaceConfigurationsForRent', trim( stripcslashes( $arrValues['use_space_configurations_for_rent'] ) ) ); elseif( isset( $arrValues['use_space_configurations_for_rent'] ) ) $this->setUseSpaceConfigurationsForRent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_space_configurations_for_rent'] ) : $arrValues['use_space_configurations_for_rent'] );
		if( isset( $arrValues['use_space_configurations_for_deposits'] ) && $boolDirectSet ) $this->set( 'm_boolUseSpaceConfigurationsForDeposits', trim( stripcslashes( $arrValues['use_space_configurations_for_deposits'] ) ) ); elseif( isset( $arrValues['use_space_configurations_for_deposits'] ) ) $this->setUseSpaceConfigurationsForDeposits( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_space_configurations_for_deposits'] ) : $arrValues['use_space_configurations_for_deposits'] );
		if( isset( $arrValues['use_space_configurations_for_other'] ) && $boolDirectSet ) $this->set( 'm_boolUseSpaceConfigurationsForOther', trim( stripcslashes( $arrValues['use_space_configurations_for_other'] ) ) ); elseif( isset( $arrValues['use_space_configurations_for_other'] ) ) $this->setUseSpaceConfigurationsForOther( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['use_space_configurations_for_other'] ) : $arrValues['use_space_configurations_for_other'] );
		if( isset( $arrValues['allow_rate_scheduling_for_rent'] ) && $boolDirectSet ) $this->set( 'm_boolAllowRateSchedulingForRent', trim( stripcslashes( $arrValues['allow_rate_scheduling_for_rent'] ) ) ); elseif( isset( $arrValues['allow_rate_scheduling_for_rent'] ) ) $this->setAllowRateSchedulingForRent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_rate_scheduling_for_rent'] ) : $arrValues['allow_rate_scheduling_for_rent'] );
		if( isset( $arrValues['allow_rate_scheduling_for_deposits'] ) && $boolDirectSet ) $this->set( 'm_boolAllowRateSchedulingForDeposits', trim( stripcslashes( $arrValues['allow_rate_scheduling_for_deposits'] ) ) ); elseif( isset( $arrValues['allow_rate_scheduling_for_deposits'] ) ) $this->setAllowRateSchedulingForDeposits( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_rate_scheduling_for_deposits'] ) : $arrValues['allow_rate_scheduling_for_deposits'] );
		if( isset( $arrValues['allow_rate_scheduling_for_other'] ) && $boolDirectSet ) $this->set( 'm_boolAllowRateSchedulingForOther', trim( stripcslashes( $arrValues['allow_rate_scheduling_for_other'] ) ) ); elseif( isset( $arrValues['allow_rate_scheduling_for_other'] ) ) $this->setAllowRateSchedulingForOther( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_rate_scheduling_for_other'] ) : $arrValues['allow_rate_scheduling_for_other'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['allow_full_bah'] ) && $boolDirectSet ) $this->set( 'm_boolAllowFullBah', trim( stripcslashes( $arrValues['allow_full_bah'] ) ) ); elseif( isset( $arrValues['allow_full_bah'] ) ) $this->setAllowFullBah( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_full_bah'] ) : $arrValues['allow_full_bah'] );
		if( isset( $arrValues['allow_fixed_bah'] ) && $boolDirectSet ) $this->set( 'm_boolAllowFixedBah', trim( stripcslashes( $arrValues['allow_fixed_bah'] ) ) ); elseif( isset( $arrValues['allow_fixed_bah'] ) ) $this->setAllowFixedBah( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_fixed_bah'] ) : $arrValues['allow_fixed_bah'] );
		if( isset( $arrValues['allow_fixed_bah_adjustment'] ) && $boolDirectSet ) $this->set( 'm_boolAllowFixedBahAdjustment', trim( stripcslashes( $arrValues['allow_fixed_bah_adjustment'] ) ) ); elseif( isset( $arrValues['allow_fixed_bah_adjustment'] ) ) $this->setAllowFixedBahAdjustment( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_fixed_bah_adjustment'] ) : $arrValues['allow_fixed_bah_adjustment'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setArOriginId( $intArOriginId ) {
		$this->set( 'm_intArOriginId', CStrings::strToIntDef( $intArOriginId, NULL, false ) );
	}

	public function getArOriginId() {
		return $this->m_intArOriginId;
	}

	public function sqlArOriginId() {
		return ( true == isset( $this->m_intArOriginId ) ) ? ( string ) $this->m_intArOriginId : 'NULL';
	}

	public function setAllowPerItem( $boolAllowPerItem ) {
		$this->set( 'm_boolAllowPerItem', CStrings::strToBool( $boolAllowPerItem ) );
	}

	public function getAllowPerItem() {
		return $this->m_boolAllowPerItem;
	}

	public function sqlAllowPerItem() {
		return ( true == isset( $this->m_boolAllowPerItem ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowPerItem ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowVolumeLookups( $boolAllowVolumeLookups ) {
		$this->set( 'm_boolAllowVolumeLookups', CStrings::strToBool( $boolAllowVolumeLookups ) );
	}

	public function getAllowVolumeLookups() {
		return $this->m_boolAllowVolumeLookups;
	}

	public function sqlAllowVolumeLookups() {
		return ( true == isset( $this->m_boolAllowVolumeLookups ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowVolumeLookups ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowTieredLookups( $boolAllowTieredLookups ) {
		$this->set( 'm_boolAllowTieredLookups', CStrings::strToBool( $boolAllowTieredLookups ) );
	}

	public function getAllowTieredLookups() {
		return $this->m_boolAllowTieredLookups;
	}

	public function sqlAllowTieredLookups() {
		return ( true == isset( $this->m_boolAllowTieredLookups ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowTieredLookups ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowPercentCode( $boolAllowPercentCode ) {
		$this->set( 'm_boolAllowPercentCode', CStrings::strToBool( $boolAllowPercentCode ) );
	}

	public function getAllowPercentCode() {
		return $this->m_boolAllowPercentCode;
	}

	public function sqlAllowPercentCode() {
		return ( true == isset( $this->m_boolAllowPercentCode ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowPercentCode ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowPercentCodeGroup( $boolAllowPercentCodeGroup ) {
		$this->set( 'm_boolAllowPercentCodeGroup', CStrings::strToBool( $boolAllowPercentCodeGroup ) );
	}

	public function getAllowPercentCodeGroup() {
		return $this->m_boolAllowPercentCodeGroup;
	}

	public function sqlAllowPercentCodeGroup() {
		return ( true == isset( $this->m_boolAllowPercentCodeGroup ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowPercentCodeGroup ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowPercentCodeType( $boolAllowPercentCodeType ) {
		$this->set( 'm_boolAllowPercentCodeType', CStrings::strToBool( $boolAllowPercentCodeType ) );
	}

	public function getAllowPercentCodeType() {
		return $this->m_boolAllowPercentCodeType;
	}

	public function sqlAllowPercentCodeType() {
		return ( true == isset( $this->m_boolAllowPercentCodeType ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowPercentCodeType ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUseLeaseTermsForRent( $boolUseLeaseTermsForRent ) {
		$this->set( 'm_boolUseLeaseTermsForRent', CStrings::strToBool( $boolUseLeaseTermsForRent ) );
	}

	public function getUseLeaseTermsForRent() {
		return $this->m_boolUseLeaseTermsForRent;
	}

	public function sqlUseLeaseTermsForRent() {
		return ( true == isset( $this->m_boolUseLeaseTermsForRent ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseLeaseTermsForRent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUseLeaseTermsForDeposits( $boolUseLeaseTermsForDeposits ) {
		$this->set( 'm_boolUseLeaseTermsForDeposits', CStrings::strToBool( $boolUseLeaseTermsForDeposits ) );
	}

	public function getUseLeaseTermsForDeposits() {
		return $this->m_boolUseLeaseTermsForDeposits;
	}

	public function sqlUseLeaseTermsForDeposits() {
		return ( true == isset( $this->m_boolUseLeaseTermsForDeposits ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseLeaseTermsForDeposits ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUseLeaseTermsForOther( $boolUseLeaseTermsForOther ) {
		$this->set( 'm_boolUseLeaseTermsForOther', CStrings::strToBool( $boolUseLeaseTermsForOther ) );
	}

	public function getUseLeaseTermsForOther() {
		return $this->m_boolUseLeaseTermsForOther;
	}

	public function sqlUseLeaseTermsForOther() {
		return ( true == isset( $this->m_boolUseLeaseTermsForOther ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseLeaseTermsForOther ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUseLeaseStartWindowsForRent( $boolUseLeaseStartWindowsForRent ) {
		$this->set( 'm_boolUseLeaseStartWindowsForRent', CStrings::strToBool( $boolUseLeaseStartWindowsForRent ) );
	}

	public function getUseLeaseStartWindowsForRent() {
		return $this->m_boolUseLeaseStartWindowsForRent;
	}

	public function sqlUseLeaseStartWindowsForRent() {
		return ( true == isset( $this->m_boolUseLeaseStartWindowsForRent ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseLeaseStartWindowsForRent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUseLeaseStartWindowsForDeposits( $boolUseLeaseStartWindowsForDeposits ) {
		$this->set( 'm_boolUseLeaseStartWindowsForDeposits', CStrings::strToBool( $boolUseLeaseStartWindowsForDeposits ) );
	}

	public function getUseLeaseStartWindowsForDeposits() {
		return $this->m_boolUseLeaseStartWindowsForDeposits;
	}

	public function sqlUseLeaseStartWindowsForDeposits() {
		return ( true == isset( $this->m_boolUseLeaseStartWindowsForDeposits ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseLeaseStartWindowsForDeposits ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUseLeaseStartWindowsForOther( $boolUseLeaseStartWindowsForOther ) {
		$this->set( 'm_boolUseLeaseStartWindowsForOther', CStrings::strToBool( $boolUseLeaseStartWindowsForOther ) );
	}

	public function getUseLeaseStartWindowsForOther() {
		return $this->m_boolUseLeaseStartWindowsForOther;
	}

	public function sqlUseLeaseStartWindowsForOther() {
		return ( true == isset( $this->m_boolUseLeaseStartWindowsForOther ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseLeaseStartWindowsForOther ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUseSpaceConfigurationsForRent( $boolUseSpaceConfigurationsForRent ) {
		$this->set( 'm_boolUseSpaceConfigurationsForRent', CStrings::strToBool( $boolUseSpaceConfigurationsForRent ) );
	}

	public function getUseSpaceConfigurationsForRent() {
		return $this->m_boolUseSpaceConfigurationsForRent;
	}

	public function sqlUseSpaceConfigurationsForRent() {
		return ( true == isset( $this->m_boolUseSpaceConfigurationsForRent ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseSpaceConfigurationsForRent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUseSpaceConfigurationsForDeposits( $boolUseSpaceConfigurationsForDeposits ) {
		$this->set( 'm_boolUseSpaceConfigurationsForDeposits', CStrings::strToBool( $boolUseSpaceConfigurationsForDeposits ) );
	}

	public function getUseSpaceConfigurationsForDeposits() {
		return $this->m_boolUseSpaceConfigurationsForDeposits;
	}

	public function sqlUseSpaceConfigurationsForDeposits() {
		return ( true == isset( $this->m_boolUseSpaceConfigurationsForDeposits ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseSpaceConfigurationsForDeposits ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUseSpaceConfigurationsForOther( $boolUseSpaceConfigurationsForOther ) {
		$this->set( 'm_boolUseSpaceConfigurationsForOther', CStrings::strToBool( $boolUseSpaceConfigurationsForOther ) );
	}

	public function getUseSpaceConfigurationsForOther() {
		return $this->m_boolUseSpaceConfigurationsForOther;
	}

	public function sqlUseSpaceConfigurationsForOther() {
		return ( true == isset( $this->m_boolUseSpaceConfigurationsForOther ) ) ? '\'' . ( true == ( bool ) $this->m_boolUseSpaceConfigurationsForOther ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowRateSchedulingForRent( $boolAllowRateSchedulingForRent ) {
		$this->set( 'm_boolAllowRateSchedulingForRent', CStrings::strToBool( $boolAllowRateSchedulingForRent ) );
	}

	public function getAllowRateSchedulingForRent() {
		return $this->m_boolAllowRateSchedulingForRent;
	}

	public function sqlAllowRateSchedulingForRent() {
		return ( true == isset( $this->m_boolAllowRateSchedulingForRent ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowRateSchedulingForRent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowRateSchedulingForDeposits( $boolAllowRateSchedulingForDeposits ) {
		$this->set( 'm_boolAllowRateSchedulingForDeposits', CStrings::strToBool( $boolAllowRateSchedulingForDeposits ) );
	}

	public function getAllowRateSchedulingForDeposits() {
		return $this->m_boolAllowRateSchedulingForDeposits;
	}

	public function sqlAllowRateSchedulingForDeposits() {
		return ( true == isset( $this->m_boolAllowRateSchedulingForDeposits ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowRateSchedulingForDeposits ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowRateSchedulingForOther( $boolAllowRateSchedulingForOther ) {
		$this->set( 'm_boolAllowRateSchedulingForOther', CStrings::strToBool( $boolAllowRateSchedulingForOther ) );
	}

	public function getAllowRateSchedulingForOther() {
		return $this->m_boolAllowRateSchedulingForOther;
	}

	public function sqlAllowRateSchedulingForOther() {
		return ( true == isset( $this->m_boolAllowRateSchedulingForOther ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowRateSchedulingForOther ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setAllowFullBah( $boolAllowFullBah ) {
		$this->set( 'm_boolAllowFullBah', CStrings::strToBool( $boolAllowFullBah ) );
	}

	public function getAllowFullBah() {
		return $this->m_boolAllowFullBah;
	}

	public function sqlAllowFullBah() {
		return ( true == isset( $this->m_boolAllowFullBah ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowFullBah ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowFixedBah( $boolAllowFixedBah ) {
		$this->set( 'm_boolAllowFixedBah', CStrings::strToBool( $boolAllowFixedBah ) );
	}

	public function getAllowFixedBah() {
		return $this->m_boolAllowFixedBah;
	}

	public function sqlAllowFixedBah() {
		return ( true == isset( $this->m_boolAllowFixedBah ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowFixedBah ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowFixedBahAdjustment( $boolAllowFixedBahAdjustment ) {
		$this->set( 'm_boolAllowFixedBahAdjustment', CStrings::strToBool( $boolAllowFixedBahAdjustment ) );
	}

	public function getAllowFixedBahAdjustment() {
		return $this->m_boolAllowFixedBahAdjustment;
	}

	public function sqlAllowFixedBahAdjustment() {
		return ( true == isset( $this->m_boolAllowFixedBahAdjustment ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowFixedBahAdjustment ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, ar_origin_id, allow_per_item, allow_volume_lookups, allow_tiered_lookups, allow_percent_code, allow_percent_code_group, allow_percent_code_type, use_lease_terms_for_rent, use_lease_terms_for_deposits, use_lease_terms_for_other, use_lease_start_windows_for_rent, use_lease_start_windows_for_deposits, use_lease_start_windows_for_other, use_space_configurations_for_rent, use_space_configurations_for_deposits, use_space_configurations_for_other, allow_rate_scheduling_for_rent, allow_rate_scheduling_for_deposits, allow_rate_scheduling_for_other, updated_by, updated_on, created_by, created_on, allow_full_bah, allow_fixed_bah, allow_fixed_bah_adjustment )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlArOriginId() . ', ' .
 						$this->sqlAllowPerItem() . ', ' .
 						$this->sqlAllowVolumeLookups() . ', ' .
 						$this->sqlAllowTieredLookups() . ', ' .
 						$this->sqlAllowPercentCode() . ', ' .
 						$this->sqlAllowPercentCodeGroup() . ', ' .
 						$this->sqlAllowPercentCodeType() . ', ' .
 						$this->sqlUseLeaseTermsForRent() . ', ' .
 						$this->sqlUseLeaseTermsForDeposits() . ', ' .
 						$this->sqlUseLeaseTermsForOther() . ', ' .
 						$this->sqlUseLeaseStartWindowsForRent() . ', ' .
 						$this->sqlUseLeaseStartWindowsForDeposits() . ', ' .
 						$this->sqlUseLeaseStartWindowsForOther() . ', ' .
 						$this->sqlUseSpaceConfigurationsForRent() . ', ' .
 						$this->sqlUseSpaceConfigurationsForDeposits() . ', ' .
 						$this->sqlUseSpaceConfigurationsForOther() . ', ' .
 						$this->sqlAllowRateSchedulingForRent() . ', ' .
 						$this->sqlAllowRateSchedulingForDeposits() . ', ' .
 						$this->sqlAllowRateSchedulingForOther() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlAllowFullBah() . ', ' .
 						$this->sqlAllowFixedBah() . ', ' .
 						$this->sqlAllowFixedBahAdjustment() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_origin_id = ' . $this->sqlArOriginId() . ','; } elseif( true == array_key_exists( 'ArOriginId', $this->getChangedColumns() ) ) { $strSql .= ' ar_origin_id = ' . $this->sqlArOriginId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_per_item = ' . $this->sqlAllowPerItem() . ','; } elseif( true == array_key_exists( 'AllowPerItem', $this->getChangedColumns() ) ) { $strSql .= ' allow_per_item = ' . $this->sqlAllowPerItem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_volume_lookups = ' . $this->sqlAllowVolumeLookups() . ','; } elseif( true == array_key_exists( 'AllowVolumeLookups', $this->getChangedColumns() ) ) { $strSql .= ' allow_volume_lookups = ' . $this->sqlAllowVolumeLookups() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_tiered_lookups = ' . $this->sqlAllowTieredLookups() . ','; } elseif( true == array_key_exists( 'AllowTieredLookups', $this->getChangedColumns() ) ) { $strSql .= ' allow_tiered_lookups = ' . $this->sqlAllowTieredLookups() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_percent_code = ' . $this->sqlAllowPercentCode() . ','; } elseif( true == array_key_exists( 'AllowPercentCode', $this->getChangedColumns() ) ) { $strSql .= ' allow_percent_code = ' . $this->sqlAllowPercentCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_percent_code_group = ' . $this->sqlAllowPercentCodeGroup() . ','; } elseif( true == array_key_exists( 'AllowPercentCodeGroup', $this->getChangedColumns() ) ) { $strSql .= ' allow_percent_code_group = ' . $this->sqlAllowPercentCodeGroup() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_percent_code_type = ' . $this->sqlAllowPercentCodeType() . ','; } elseif( true == array_key_exists( 'AllowPercentCodeType', $this->getChangedColumns() ) ) { $strSql .= ' allow_percent_code_type = ' . $this->sqlAllowPercentCodeType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_lease_terms_for_rent = ' . $this->sqlUseLeaseTermsForRent() . ','; } elseif( true == array_key_exists( 'UseLeaseTermsForRent', $this->getChangedColumns() ) ) { $strSql .= ' use_lease_terms_for_rent = ' . $this->sqlUseLeaseTermsForRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_lease_terms_for_deposits = ' . $this->sqlUseLeaseTermsForDeposits() . ','; } elseif( true == array_key_exists( 'UseLeaseTermsForDeposits', $this->getChangedColumns() ) ) { $strSql .= ' use_lease_terms_for_deposits = ' . $this->sqlUseLeaseTermsForDeposits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_lease_terms_for_other = ' . $this->sqlUseLeaseTermsForOther() . ','; } elseif( true == array_key_exists( 'UseLeaseTermsForOther', $this->getChangedColumns() ) ) { $strSql .= ' use_lease_terms_for_other = ' . $this->sqlUseLeaseTermsForOther() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_lease_start_windows_for_rent = ' . $this->sqlUseLeaseStartWindowsForRent() . ','; } elseif( true == array_key_exists( 'UseLeaseStartWindowsForRent', $this->getChangedColumns() ) ) { $strSql .= ' use_lease_start_windows_for_rent = ' . $this->sqlUseLeaseStartWindowsForRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_lease_start_windows_for_deposits = ' . $this->sqlUseLeaseStartWindowsForDeposits() . ','; } elseif( true == array_key_exists( 'UseLeaseStartWindowsForDeposits', $this->getChangedColumns() ) ) { $strSql .= ' use_lease_start_windows_for_deposits = ' . $this->sqlUseLeaseStartWindowsForDeposits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_lease_start_windows_for_other = ' . $this->sqlUseLeaseStartWindowsForOther() . ','; } elseif( true == array_key_exists( 'UseLeaseStartWindowsForOther', $this->getChangedColumns() ) ) { $strSql .= ' use_lease_start_windows_for_other = ' . $this->sqlUseLeaseStartWindowsForOther() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_space_configurations_for_rent = ' . $this->sqlUseSpaceConfigurationsForRent() . ','; } elseif( true == array_key_exists( 'UseSpaceConfigurationsForRent', $this->getChangedColumns() ) ) { $strSql .= ' use_space_configurations_for_rent = ' . $this->sqlUseSpaceConfigurationsForRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_space_configurations_for_deposits = ' . $this->sqlUseSpaceConfigurationsForDeposits() . ','; } elseif( true == array_key_exists( 'UseSpaceConfigurationsForDeposits', $this->getChangedColumns() ) ) { $strSql .= ' use_space_configurations_for_deposits = ' . $this->sqlUseSpaceConfigurationsForDeposits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_space_configurations_for_other = ' . $this->sqlUseSpaceConfigurationsForOther() . ','; } elseif( true == array_key_exists( 'UseSpaceConfigurationsForOther', $this->getChangedColumns() ) ) { $strSql .= ' use_space_configurations_for_other = ' . $this->sqlUseSpaceConfigurationsForOther() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_rate_scheduling_for_rent = ' . $this->sqlAllowRateSchedulingForRent() . ','; } elseif( true == array_key_exists( 'AllowRateSchedulingForRent', $this->getChangedColumns() ) ) { $strSql .= ' allow_rate_scheduling_for_rent = ' . $this->sqlAllowRateSchedulingForRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_rate_scheduling_for_deposits = ' . $this->sqlAllowRateSchedulingForDeposits() . ','; } elseif( true == array_key_exists( 'AllowRateSchedulingForDeposits', $this->getChangedColumns() ) ) { $strSql .= ' allow_rate_scheduling_for_deposits = ' . $this->sqlAllowRateSchedulingForDeposits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_rate_scheduling_for_other = ' . $this->sqlAllowRateSchedulingForOther() . ','; } elseif( true == array_key_exists( 'AllowRateSchedulingForOther', $this->getChangedColumns() ) ) { $strSql .= ' allow_rate_scheduling_for_other = ' . $this->sqlAllowRateSchedulingForOther() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_full_bah = ' . $this->sqlAllowFullBah() . ','; } elseif( true == array_key_exists( 'AllowFullBah', $this->getChangedColumns() ) ) { $strSql .= ' allow_full_bah = ' . $this->sqlAllowFullBah() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_fixed_bah = ' . $this->sqlAllowFixedBah() . ','; } elseif( true == array_key_exists( 'AllowFixedBah', $this->getChangedColumns() ) ) { $strSql .= ' allow_fixed_bah = ' . $this->sqlAllowFixedBah() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_fixed_bah_adjustment = ' . $this->sqlAllowFixedBahAdjustment() . ','; } elseif( true == array_key_exists( 'AllowFixedBahAdjustment', $this->getChangedColumns() ) ) { $strSql .= ' allow_fixed_bah_adjustment = ' . $this->sqlAllowFixedBahAdjustment() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'ar_origin_id' => $this->getArOriginId(),
			'allow_per_item' => $this->getAllowPerItem(),
			'allow_volume_lookups' => $this->getAllowVolumeLookups(),
			'allow_tiered_lookups' => $this->getAllowTieredLookups(),
			'allow_percent_code' => $this->getAllowPercentCode(),
			'allow_percent_code_group' => $this->getAllowPercentCodeGroup(),
			'allow_percent_code_type' => $this->getAllowPercentCodeType(),
			'use_lease_terms_for_rent' => $this->getUseLeaseTermsForRent(),
			'use_lease_terms_for_deposits' => $this->getUseLeaseTermsForDeposits(),
			'use_lease_terms_for_other' => $this->getUseLeaseTermsForOther(),
			'use_lease_start_windows_for_rent' => $this->getUseLeaseStartWindowsForRent(),
			'use_lease_start_windows_for_deposits' => $this->getUseLeaseStartWindowsForDeposits(),
			'use_lease_start_windows_for_other' => $this->getUseLeaseStartWindowsForOther(),
			'use_space_configurations_for_rent' => $this->getUseSpaceConfigurationsForRent(),
			'use_space_configurations_for_deposits' => $this->getUseSpaceConfigurationsForDeposits(),
			'use_space_configurations_for_other' => $this->getUseSpaceConfigurationsForOther(),
			'allow_rate_scheduling_for_rent' => $this->getAllowRateSchedulingForRent(),
			'allow_rate_scheduling_for_deposits' => $this->getAllowRateSchedulingForDeposits(),
			'allow_rate_scheduling_for_other' => $this->getAllowRateSchedulingForOther(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'allow_full_bah' => $this->getAllowFullBah(),
			'allow_fixed_bah' => $this->getAllowFixedBah(),
			'allow_fixed_bah_adjustment' => $this->getAllowFixedBahAdjustment()
		);
	}

}
?>