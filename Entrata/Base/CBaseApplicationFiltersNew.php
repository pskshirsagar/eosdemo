<?php

class CBaseApplicationFiltersNew extends CEosPluralBase {

    const TABLE_APPLICATION_FILTERS = 'public.application_filters';

    public static function fetchApplicationFilters( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CApplicationFilterNew', $objDatabase );
    }

    public static function fetchApplicationFilter( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CApplicationFilterNew', $objDatabase );
    }

    public static function fetchApplicationFilterCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'application_filters', $objDatabase );
    }

    public static function fetchApplicationFilterById( $intId, $objDatabase ) {
        return self::fetchApplicationFilter( sprintf('SELECT * FROM application_filters WHERE id = %d', (int) $intId ), $objDatabase );
    }

    public static function fetchApplicationFiltersByCid( $intCid, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchApplicationFiltersByCompanyUserId( $intCompanyUserId, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE company_user_id = %d', (int) $intCompanyUserId ), $objDatabase );
    }

    public static function fetchApplicationFiltersByProperties( $strProperties, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE properties = \'%s\'', $strProperties ), $objDatabase );
    }

    public static function fetchApplicationFiltersByPropertyFloorplans( $strPropertyFloorplans, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE property_floorplans = \'%s\'', $strPropertyFloorplans ), $objDatabase );
    }

     public static function fetchApplicationFiltersByScreeningResponseTypes( $strScreeningResponseTypes, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE screening_response_types = \'%s\'', $strScreeningResponseTypes ), $objDatabase );
    }

    public static function fetchApplicationFiltersByLeadTypes( $strLeadTypes, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE lead_types = \'%s\'', $strLeadTypes ), $objDatabase );
    }

    public static function fetchApplicationFiltersByLeadSources( $strLeadSources, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE lead_sources = \'%s\'', $strLeadSources ), $objDatabase );
    }

    public static function fetchApplicationFiltersByPsProducts( $strPsProducts, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE ps_products = \'%s\'', $strPsProducts ), $objDatabase );
    }

    public static function fetchApplicationFiltersByCompanyEmployees( $strCompanyEmployees, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE company_employees = \'%s\'', $strCompanyEmployees ), $objDatabase );
    }

    public static function fetchApplicationFiltersByApplications( $strApplications, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE applications = \'%s\'', $strApplications ), $objDatabase );
    }

    public static function fetchApplicationFiltersByFilterName( $strFilterName, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE filter_name = \'%s\'', $strFilterName ), $objDatabase );
    }

    public static function fetchApplicationFiltersByQuickSearch( $strQuickSearch, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE quick_search = \'%s\'', $strQuickSearch ), $objDatabase );
    }

    public static function fetchApplicationFiltersByLastContactMinDays( $intLastContactMinDays, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE last_contact_min_days = %d', (int) $intLastContactMinDays ), $objDatabase );
    }

    public static function fetchApplicationFiltersByLastContactMaxDays( $intLastContactMaxDays, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE last_contact_max_days = %d', (int) $intLastContactMaxDays ), $objDatabase );
    }

    public static function fetchApplicationFiltersByAdDelayDays( $intAdDelayDays, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE ad_delay_days = %d', (int) $intAdDelayDays ), $objDatabase );
    }

    public static function fetchApplicationFiltersByDesiredBedrooms( $strDesiredBedrooms, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE desired_bedrooms = \'%s\'', $strDesiredBedrooms ), $objDatabase );
    }

    public static function fetchApplicationFiltersByDesiredBathrooms( $strDesiredBathrooms, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE desired_bathrooms = \'%s\'', $strDesiredBathrooms ), $objDatabase );
    }

    public static function fetchApplicationFiltersByDesiredPets( $strDesiredPets, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE desired_pets = \'%s\'', $strDesiredPets ), $objDatabase );
    }

    public static function fetchApplicationFiltersByDesiredRentMin( $fltDesiredRentMin, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE desired_rent_min = \'%s\'', $fltDesiredRentMin ), $objDatabase );
    }

    public static function fetchApplicationFiltersByDesiredRentMax( $fltDesiredRentMax, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE desired_rent_max = \'%s\'', $fltDesiredRentMax ), $objDatabase );
    }

    public static function fetchApplicationFiltersByDesiredOccupants( $strDesiredOccupants, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE desired_occupants = \'%s\'', $strDesiredOccupants ), $objDatabase );
    }

    public static function fetchApplicationFiltersByDesiredFloors( $strDesiredFloors, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE desired_floors = \'%s\'', $strDesiredFloors ), $objDatabase );
    }

    public static function fetchApplicationFiltersByApplicationStartDate( $strApplicationStartDate, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE application_start_date = \'%s\'', $strApplicationStartDate ), $objDatabase );
    }

    public static function fetchApplicationFiltersByApplicationEndDate( $strApplicationEndDate, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE application_end_date = \'%s\'', $strApplicationEndDate ), $objDatabase );
    }

    public static function fetchApplicationFiltersByMoveInDateMin( $strMoveInDateMin, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE move_in_date_min = \'%s\'', $strMoveInDateMin ), $objDatabase );
    }

    public static function fetchApplicationFiltersByMoveInDateMax( $strMoveInDateMax, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE move_in_date_max = \'%s\'', $strMoveInDateMax ), $objDatabase );
    }

    public static function fetchApplicationFiltersBySortBy( $strSortBy, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE sort_by = \'%s\'', $strSortBy ), $objDatabase );
    }

    public static function fetchApplicationFiltersBySortDirection( $strSortDirection, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE sort_direction = \'%s\'', $strSortDirection ), $objDatabase );
    }

    public static function fetchApplicationFiltersByIsIntegrated( $intIsIntegrated, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE is_integrated = %d', (int) $intIsIntegrated ), $objDatabase );
    }

    public static function fetchApplicationFiltersByDeletedBy( $intDeletedBy, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE deleted_by = %d', (int) $intDeletedBy ), $objDatabase );
    }

    public static function fetchApplicationFiltersByDeletedOn( $strDeletedOn, $objDatabase ) {
        return self::fetchApplicationFilters( sprintf('SELECT * FROM application_filters WHERE deleted_on = \'%s\'', $strDeletedOn ), $objDatabase );
    }
}
?>