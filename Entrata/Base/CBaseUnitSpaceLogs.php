<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitSpaceLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitSpaceLogs extends CEosPluralBase {

	/**
	 * @return CUnitSpaceLog[]
	 */
	public static function fetchUnitSpaceLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CUnitSpaceLog::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CUnitSpaceLog
	 */
	public static function fetchUnitSpaceLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CUnitSpaceLog::class, $objDatabase );
	}

	public static function fetchUnitSpaceLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'unit_space_logs', $objDatabase );
	}

	public static function fetchUnitSpaceLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLog( sprintf( 'SELECT * FROM unit_space_logs WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByCid( $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByUnitKindIdByCid( $intUnitKindId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE unit_kind_id = %d AND cid = %d', $intUnitKindId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE unit_type_id = %d AND cid = %d', $intUnitTypeId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByPropertyBuildingIdByCid( $intPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE property_building_id = %d AND cid = %d', $intPropertyBuildingId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByPropertyFloorplanIdByCid( $intPropertyFloorplanId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE property_floorplan_id = %d AND cid = %d', $intPropertyFloorplanId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByPropertyFloorIdByCid( $intPropertyFloorId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE property_floor_id = %d AND cid = %d', $intPropertyFloorId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE property_unit_id = %d AND cid = %d', $intPropertyUnitId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE unit_space_id = %d AND cid = %d', $intUnitSpaceId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByUnitSpaceLogTypeIdByCid( $intUnitSpaceLogTypeId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE unit_space_log_type_id = %d AND cid = %d', $intUnitSpaceLogTypeId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByUnitSpaceStatusTypeIdByCid( $intUnitSpaceStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE unit_space_status_type_id = %d AND cid = %d', $intUnitSpaceStatusTypeId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByUnitExclusionReasonTypeIdByCid( $intUnitExclusionReasonTypeId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE unit_exclusion_reason_type_id = %d AND cid = %d', $intUnitExclusionReasonTypeId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByOccupancyTypeIdByCid( $intOccupancyTypeId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE occupancy_type_id = %d AND cid = %d', $intOccupancyTypeId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByApplicationLeaseIdByCid( $intApplicationLeaseId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE application_lease_id = %d AND cid = %d', $intApplicationLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByHoldLeaseIdByCid( $intHoldLeaseId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE hold_lease_id = %d AND cid = %d', $intHoldLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByFutureLeaseIdByCid( $intFutureLeaseId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE future_lease_id = %d AND cid = %d', $intFutureLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByCurrentLeaseIdByCid( $intCurrentLeaseId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE current_lease_id = %d AND cid = %d', $intCurrentLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByPastLeaseIdByCid( $intPastLeaseId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE past_lease_id = %d AND cid = %d', $intPastLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByPriorUnitSpaceLogIdByCid( $intPriorUnitSpaceLogId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE prior_unit_space_log_id = %d AND cid = %d', $intPriorUnitSpaceLogId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByReferenceIdByCid( $intReferenceId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE reference_id = %d AND cid = %d', $intReferenceId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByReferenceLogIdByCid( $intReferenceLogId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE reference_log_id = %d AND cid = %d', $intReferenceLogId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByPeriodIdByCid( $intPeriodId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE period_id = %d AND cid = %d', $intPeriodId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByReportingPeriodIdByCid( $intReportingPeriodId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE reporting_period_id = %d AND cid = %d', $intReportingPeriodId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByEffectivePeriodIdByCid( $intEffectivePeriodId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE effective_period_id = %d AND cid = %d', $intEffectivePeriodId, $intCid ), $objDatabase );
	}

	public static function fetchUnitSpaceLogsByOriginalPeriodIdByCid( $intOriginalPeriodId, $intCid, $objDatabase ) {
		return self::fetchUnitSpaceLogs( sprintf( 'SELECT * FROM unit_space_logs WHERE original_period_id = %d AND cid = %d', $intOriginalPeriodId, $intCid ), $objDatabase );
	}

}
?>