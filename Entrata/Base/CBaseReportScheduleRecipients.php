<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportScheduleRecipients
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReportScheduleRecipients extends CEosPluralBase {

	/**
	 * @return CReportScheduleRecipient[]
	 */
	public static function fetchReportScheduleRecipients( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CReportScheduleRecipient::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CReportScheduleRecipient
	 */
	public static function fetchReportScheduleRecipient( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportScheduleRecipient::class, $objDatabase );
	}

	public static function fetchReportScheduleRecipientCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_schedule_recipients', $objDatabase );
	}

	public static function fetchReportScheduleRecipientByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchReportScheduleRecipient( sprintf( 'SELECT * FROM report_schedule_recipients WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchReportScheduleRecipientsByCid( $intCid, $objDatabase ) {
		return self::fetchReportScheduleRecipients( sprintf( 'SELECT * FROM report_schedule_recipients WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchReportScheduleRecipientsByReportScheduleIdByCid( $intReportScheduleId, $intCid, $objDatabase ) {
		return self::fetchReportScheduleRecipients( sprintf( 'SELECT * FROM report_schedule_recipients WHERE report_schedule_id = %d AND cid = %d', $intReportScheduleId, $intCid ), $objDatabase );
	}

	public static function fetchReportScheduleRecipientsByReportScheduleRecipientTypeIdByCid( $intReportScheduleRecipientTypeId, $intCid, $objDatabase ) {
		return self::fetchReportScheduleRecipients( sprintf( 'SELECT * FROM report_schedule_recipients WHERE report_schedule_recipient_type_id = %d AND cid = %d', $intReportScheduleRecipientTypeId, $intCid ), $objDatabase );
	}

	public static function fetchReportScheduleRecipientsByReferenceIdByCid( $intReferenceId, $intCid, $objDatabase ) {
		return self::fetchReportScheduleRecipients( sprintf( 'SELECT * FROM report_schedule_recipients WHERE reference_id = %d AND cid = %d', $intReferenceId, $intCid ), $objDatabase );
	}

}
?>