<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledEmail extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.scheduled_emails';

	protected $m_intId;
	protected $m_intCid;
	protected $m_arrintScheduledEmailFilterIds;
	protected $m_intScheduledEmailEventTypeId;
	protected $m_intScheduledEmailTypeId;
	protected $m_intScheduledEmailDeliveryTypeId;
	protected $m_intSystemMessageTemplateId;
	protected $m_intFrequencyId;
	protected $m_intHeaderFooterOptionTypeId;
	protected $m_strSubject;
	protected $m_strFromEmailAddress;
	protected $m_strTestEmailAddress;
	protected $m_strStartDatetime;
	protected $m_strEndDatetime;
	protected $m_strLastSentOn;
	protected $m_intSendOnLastDay;
	protected $m_intDaysFromEvent;
	protected $m_intHeaderFooterOptionId;
	protected $m_intIsAfterEvent;
	protected $m_intIsShared;
	protected $m_intIsDisabled;
	protected $m_strConfirmedOn;
	protected $m_strLockedOn;
	protected $m_intAttemptCount;
	protected $m_intEmailOwnerId;
	protected $m_intOrderNum;
	protected $m_intSendOptionUpdatedBy;
	protected $m_strSendOptionUpdatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strLocaleCode;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intScheduledEmailDeliveryTypeId = '1';
		$this->m_intSendOnLastDay = '0';
		$this->m_intDaysFromEvent = '0';
		$this->m_intHeaderFooterOptionId = '1';
		$this->m_intIsAfterEvent = '0';
		$this->m_intIsShared = '0';
		$this->m_intIsDisabled = '0';
		$this->m_intAttemptCount = '0';
		$this->m_intOrderNum = '0';
		$this->m_strLocaleCode = 'en_US';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['scheduled_email_filter_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintScheduledEmailFilterIds', trim( $arrValues['scheduled_email_filter_ids'] ) ); elseif( isset( $arrValues['scheduled_email_filter_ids'] ) ) $this->setScheduledEmailFilterIds( $arrValues['scheduled_email_filter_ids'] );
		if( isset( $arrValues['scheduled_email_event_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledEmailEventTypeId', trim( $arrValues['scheduled_email_event_type_id'] ) ); elseif( isset( $arrValues['scheduled_email_event_type_id'] ) ) $this->setScheduledEmailEventTypeId( $arrValues['scheduled_email_event_type_id'] );
		if( isset( $arrValues['scheduled_email_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledEmailTypeId', trim( $arrValues['scheduled_email_type_id'] ) ); elseif( isset( $arrValues['scheduled_email_type_id'] ) ) $this->setScheduledEmailTypeId( $arrValues['scheduled_email_type_id'] );
		if( isset( $arrValues['scheduled_email_delivery_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledEmailDeliveryTypeId', trim( $arrValues['scheduled_email_delivery_type_id'] ) ); elseif( isset( $arrValues['scheduled_email_delivery_type_id'] ) ) $this->setScheduledEmailDeliveryTypeId( $arrValues['scheduled_email_delivery_type_id'] );
		if( isset( $arrValues['system_message_template_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemMessageTemplateId', trim( $arrValues['system_message_template_id'] ) ); elseif( isset( $arrValues['system_message_template_id'] ) ) $this->setSystemMessageTemplateId( $arrValues['system_message_template_id'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['header_footer_option_type_id'] ) && $boolDirectSet ) $this->set( 'm_intHeaderFooterOptionTypeId', trim( $arrValues['header_footer_option_type_id'] ) ); elseif( isset( $arrValues['header_footer_option_type_id'] ) ) $this->setHeaderFooterOptionTypeId( $arrValues['header_footer_option_type_id'] );
		if( isset( $arrValues['subject'] ) && $boolDirectSet ) $this->set( 'm_strSubject', trim( stripcslashes( $arrValues['subject'] ) ) ); elseif( isset( $arrValues['subject'] ) ) $this->setSubject( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['subject'] ) : $arrValues['subject'] );
		if( isset( $arrValues['from_email_address'] ) && $boolDirectSet ) $this->set( 'm_strFromEmailAddress', trim( stripcslashes( $arrValues['from_email_address'] ) ) ); elseif( isset( $arrValues['from_email_address'] ) ) $this->setFromEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['from_email_address'] ) : $arrValues['from_email_address'] );
		if( isset( $arrValues['test_email_address'] ) && $boolDirectSet ) $this->set( 'm_strTestEmailAddress', trim( stripcslashes( $arrValues['test_email_address'] ) ) ); elseif( isset( $arrValues['test_email_address'] ) ) $this->setTestEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['test_email_address'] ) : $arrValues['test_email_address'] );
		if( isset( $arrValues['start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStartDatetime', trim( $arrValues['start_datetime'] ) ); elseif( isset( $arrValues['start_datetime'] ) ) $this->setStartDatetime( $arrValues['start_datetime'] );
		if( isset( $arrValues['end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEndDatetime', trim( $arrValues['end_datetime'] ) ); elseif( isset( $arrValues['end_datetime'] ) ) $this->setEndDatetime( $arrValues['end_datetime'] );
		if( isset( $arrValues['last_sent_on'] ) && $boolDirectSet ) $this->set( 'm_strLastSentOn', trim( $arrValues['last_sent_on'] ) ); elseif( isset( $arrValues['last_sent_on'] ) ) $this->setLastSentOn( $arrValues['last_sent_on'] );
		if( isset( $arrValues['send_on_last_day'] ) && $boolDirectSet ) $this->set( 'm_intSendOnLastDay', trim( $arrValues['send_on_last_day'] ) ); elseif( isset( $arrValues['send_on_last_day'] ) ) $this->setSendOnLastDay( $arrValues['send_on_last_day'] );
		if( isset( $arrValues['days_from_event'] ) && $boolDirectSet ) $this->set( 'm_intDaysFromEvent', trim( $arrValues['days_from_event'] ) ); elseif( isset( $arrValues['days_from_event'] ) ) $this->setDaysFromEvent( $arrValues['days_from_event'] );
		if( isset( $arrValues['header_footer_option_id'] ) && $boolDirectSet ) $this->set( 'm_intHeaderFooterOptionId', trim( $arrValues['header_footer_option_id'] ) ); elseif( isset( $arrValues['header_footer_option_id'] ) ) $this->setHeaderFooterOptionId( $arrValues['header_footer_option_id'] );
		if( isset( $arrValues['is_after_event'] ) && $boolDirectSet ) $this->set( 'm_intIsAfterEvent', trim( $arrValues['is_after_event'] ) ); elseif( isset( $arrValues['is_after_event'] ) ) $this->setIsAfterEvent( $arrValues['is_after_event'] );
		if( isset( $arrValues['is_shared'] ) && $boolDirectSet ) $this->set( 'm_intIsShared', trim( $arrValues['is_shared'] ) ); elseif( isset( $arrValues['is_shared'] ) ) $this->setIsShared( $arrValues['is_shared'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['confirmed_on'] ) && $boolDirectSet ) $this->set( 'm_strConfirmedOn', trim( $arrValues['confirmed_on'] ) ); elseif( isset( $arrValues['confirmed_on'] ) ) $this->setConfirmedOn( $arrValues['confirmed_on'] );
		if( isset( $arrValues['locked_on'] ) && $boolDirectSet ) $this->set( 'm_strLockedOn', trim( $arrValues['locked_on'] ) ); elseif( isset( $arrValues['locked_on'] ) ) $this->setLockedOn( $arrValues['locked_on'] );
		if( isset( $arrValues['attempt_count'] ) && $boolDirectSet ) $this->set( 'm_intAttemptCount', trim( $arrValues['attempt_count'] ) ); elseif( isset( $arrValues['attempt_count'] ) ) $this->setAttemptCount( $arrValues['attempt_count'] );
		if( isset( $arrValues['email_owner_id'] ) && $boolDirectSet ) $this->set( 'm_intEmailOwnerId', trim( $arrValues['email_owner_id'] ) ); elseif( isset( $arrValues['email_owner_id'] ) ) $this->setEmailOwnerId( $arrValues['email_owner_id'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['send_option_updated_by'] ) && $boolDirectSet ) $this->set( 'm_intSendOptionUpdatedBy', trim( $arrValues['send_option_updated_by'] ) ); elseif( isset( $arrValues['send_option_updated_by'] ) ) $this->setSendOptionUpdatedBy( $arrValues['send_option_updated_by'] );
		if( isset( $arrValues['send_option_updated_on'] ) && $boolDirectSet ) $this->set( 'm_strSendOptionUpdatedOn', trim( $arrValues['send_option_updated_on'] ) ); elseif( isset( $arrValues['send_option_updated_on'] ) ) $this->setSendOptionUpdatedOn( $arrValues['send_option_updated_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['locale_code'] ) && $boolDirectSet ) $this->set( 'm_strLocaleCode', trim( stripcslashes( $arrValues['locale_code'] ) ) ); elseif( isset( $arrValues['locale_code'] ) ) $this->setLocaleCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['locale_code'] ) : $arrValues['locale_code'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScheduledEmailFilterIds( $arrintScheduledEmailFilterIds ) {
		$this->set( 'm_arrintScheduledEmailFilterIds', CStrings::strToArrIntDef( $arrintScheduledEmailFilterIds, NULL ) );
	}

	public function getScheduledEmailFilterIds() {
		return $this->m_arrintScheduledEmailFilterIds;
	}

	public function sqlScheduledEmailFilterIds() {
		return ( true == isset( $this->m_arrintScheduledEmailFilterIds ) && true == valArr( $this->m_arrintScheduledEmailFilterIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintScheduledEmailFilterIds, NULL ) . '\'' : 'NULL';
	}

	public function setScheduledEmailEventTypeId( $intScheduledEmailEventTypeId ) {
		$this->set( 'm_intScheduledEmailEventTypeId', CStrings::strToIntDef( $intScheduledEmailEventTypeId, NULL, false ) );
	}

	public function getScheduledEmailEventTypeId() {
		return $this->m_intScheduledEmailEventTypeId;
	}

	public function sqlScheduledEmailEventTypeId() {
		return ( true == isset( $this->m_intScheduledEmailEventTypeId ) ) ? ( string ) $this->m_intScheduledEmailEventTypeId : 'NULL';
	}

	public function setScheduledEmailTypeId( $intScheduledEmailTypeId ) {
		$this->set( 'm_intScheduledEmailTypeId', CStrings::strToIntDef( $intScheduledEmailTypeId, NULL, false ) );
	}

	public function getScheduledEmailTypeId() {
		return $this->m_intScheduledEmailTypeId;
	}

	public function sqlScheduledEmailTypeId() {
		return ( true == isset( $this->m_intScheduledEmailTypeId ) ) ? ( string ) $this->m_intScheduledEmailTypeId : 'NULL';
	}

	public function setScheduledEmailDeliveryTypeId( $intScheduledEmailDeliveryTypeId ) {
		$this->set( 'm_intScheduledEmailDeliveryTypeId', CStrings::strToIntDef( $intScheduledEmailDeliveryTypeId, NULL, false ) );
	}

	public function getScheduledEmailDeliveryTypeId() {
		return $this->m_intScheduledEmailDeliveryTypeId;
	}

	public function sqlScheduledEmailDeliveryTypeId() {
		return ( true == isset( $this->m_intScheduledEmailDeliveryTypeId ) ) ? ( string ) $this->m_intScheduledEmailDeliveryTypeId : '1';
	}

	public function setSystemMessageTemplateId( $intSystemMessageTemplateId ) {
		$this->set( 'm_intSystemMessageTemplateId', CStrings::strToIntDef( $intSystemMessageTemplateId, NULL, false ) );
	}

	public function getSystemMessageTemplateId() {
		return $this->m_intSystemMessageTemplateId;
	}

	public function sqlSystemMessageTemplateId() {
		return ( true == isset( $this->m_intSystemMessageTemplateId ) ) ? ( string ) $this->m_intSystemMessageTemplateId : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : 'NULL';
	}

	public function setHeaderFooterOptionTypeId( $intHeaderFooterOptionTypeId ) {
		$this->set( 'm_intHeaderFooterOptionTypeId', CStrings::strToIntDef( $intHeaderFooterOptionTypeId, NULL, false ) );
	}

	public function getHeaderFooterOptionTypeId() {
		return $this->m_intHeaderFooterOptionTypeId;
	}

	public function sqlHeaderFooterOptionTypeId() {
		return ( true == isset( $this->m_intHeaderFooterOptionTypeId ) ) ? ( string ) $this->m_intHeaderFooterOptionTypeId : 'NULL';
	}

	public function setSubject( $strSubject ) {
		$this->set( 'm_strSubject', CStrings::strTrimDef( $strSubject, 240, NULL, true ) );
	}

	public function getSubject() {
		return $this->m_strSubject;
	}

	public function sqlSubject() {
		return ( true == isset( $this->m_strSubject ) ) ? '\'' . addslashes( $this->m_strSubject ) . '\'' : 'NULL';
	}

	public function setFromEmailAddress( $strFromEmailAddress ) {
		$this->set( 'm_strFromEmailAddress', CStrings::strTrimDef( $strFromEmailAddress, 240, NULL, true ) );
	}

	public function getFromEmailAddress() {
		return $this->m_strFromEmailAddress;
	}

	public function sqlFromEmailAddress() {
		return ( true == isset( $this->m_strFromEmailAddress ) ) ? '\'' . addslashes( $this->m_strFromEmailAddress ) . '\'' : 'NULL';
	}

	public function setTestEmailAddress( $strTestEmailAddress ) {
		$this->set( 'm_strTestEmailAddress', CStrings::strTrimDef( $strTestEmailAddress, 240, NULL, true ) );
	}

	public function getTestEmailAddress() {
		return $this->m_strTestEmailAddress;
	}

	public function sqlTestEmailAddress() {
		return ( true == isset( $this->m_strTestEmailAddress ) ) ? '\'' . addslashes( $this->m_strTestEmailAddress ) . '\'' : 'NULL';
	}

	public function setStartDatetime( $strStartDatetime ) {
		$this->set( 'm_strStartDatetime', CStrings::strTrimDef( $strStartDatetime, -1, NULL, true ) );
	}

	public function getStartDatetime() {
		return $this->m_strStartDatetime;
	}

	public function sqlStartDatetime() {
		return ( true == isset( $this->m_strStartDatetime ) ) ? '\'' . $this->m_strStartDatetime . '\'' : 'NULL';
	}

	public function setEndDatetime( $strEndDatetime ) {
		$this->set( 'm_strEndDatetime', CStrings::strTrimDef( $strEndDatetime, -1, NULL, true ) );
	}

	public function getEndDatetime() {
		return $this->m_strEndDatetime;
	}

	public function sqlEndDatetime() {
		return ( true == isset( $this->m_strEndDatetime ) ) ? '\'' . $this->m_strEndDatetime . '\'' : 'NULL';
	}

	public function setLastSentOn( $strLastSentOn ) {
		$this->set( 'm_strLastSentOn', CStrings::strTrimDef( $strLastSentOn, -1, NULL, true ) );
	}

	public function getLastSentOn() {
		return $this->m_strLastSentOn;
	}

	public function sqlLastSentOn() {
		return ( true == isset( $this->m_strLastSentOn ) ) ? '\'' . $this->m_strLastSentOn . '\'' : 'NULL';
	}

	public function setSendOnLastDay( $intSendOnLastDay ) {
		$this->set( 'm_intSendOnLastDay', CStrings::strToIntDef( $intSendOnLastDay, NULL, false ) );
	}

	public function getSendOnLastDay() {
		return $this->m_intSendOnLastDay;
	}

	public function sqlSendOnLastDay() {
		return ( true == isset( $this->m_intSendOnLastDay ) ) ? ( string ) $this->m_intSendOnLastDay : '0';
	}

	public function setDaysFromEvent( $intDaysFromEvent ) {
		$this->set( 'm_intDaysFromEvent', CStrings::strToIntDef( $intDaysFromEvent, NULL, false ) );
	}

	public function getDaysFromEvent() {
		return $this->m_intDaysFromEvent;
	}

	public function sqlDaysFromEvent() {
		return ( true == isset( $this->m_intDaysFromEvent ) ) ? ( string ) $this->m_intDaysFromEvent : '0';
	}

	public function setHeaderFooterOptionId( $intHeaderFooterOptionId ) {
		$this->set( 'm_intHeaderFooterOptionId', CStrings::strToIntDef( $intHeaderFooterOptionId, NULL, false ) );
	}

	public function getHeaderFooterOptionId() {
		return $this->m_intHeaderFooterOptionId;
	}

	public function sqlHeaderFooterOptionId() {
		return ( true == isset( $this->m_intHeaderFooterOptionId ) ) ? ( string ) $this->m_intHeaderFooterOptionId : '1';
	}

	public function setIsAfterEvent( $intIsAfterEvent ) {
		$this->set( 'm_intIsAfterEvent', CStrings::strToIntDef( $intIsAfterEvent, NULL, false ) );
	}

	public function getIsAfterEvent() {
		return $this->m_intIsAfterEvent;
	}

	public function sqlIsAfterEvent() {
		return ( true == isset( $this->m_intIsAfterEvent ) ) ? ( string ) $this->m_intIsAfterEvent : '0';
	}

	public function setIsShared( $intIsShared ) {
		$this->set( 'm_intIsShared', CStrings::strToIntDef( $intIsShared, NULL, false ) );
	}

	public function getIsShared() {
		return $this->m_intIsShared;
	}

	public function sqlIsShared() {
		return ( true == isset( $this->m_intIsShared ) ) ? ( string ) $this->m_intIsShared : '0';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function setConfirmedOn( $strConfirmedOn ) {
		$this->set( 'm_strConfirmedOn', CStrings::strTrimDef( $strConfirmedOn, -1, NULL, true ) );
	}

	public function getConfirmedOn() {
		return $this->m_strConfirmedOn;
	}

	public function sqlConfirmedOn() {
		return ( true == isset( $this->m_strConfirmedOn ) ) ? '\'' . $this->m_strConfirmedOn . '\'' : 'NULL';
	}

	public function setLockedOn( $strLockedOn ) {
		$this->set( 'm_strLockedOn', CStrings::strTrimDef( $strLockedOn, -1, NULL, true ) );
	}

	public function getLockedOn() {
		return $this->m_strLockedOn;
	}

	public function sqlLockedOn() {
		return ( true == isset( $this->m_strLockedOn ) ) ? '\'' . $this->m_strLockedOn . '\'' : 'NULL';
	}

	public function setAttemptCount( $intAttemptCount ) {
		$this->set( 'm_intAttemptCount', CStrings::strToIntDef( $intAttemptCount, NULL, false ) );
	}

	public function getAttemptCount() {
		return $this->m_intAttemptCount;
	}

	public function sqlAttemptCount() {
		return ( true == isset( $this->m_intAttemptCount ) ) ? ( string ) $this->m_intAttemptCount : '0';
	}

	public function setEmailOwnerId( $intEmailOwnerId ) {
		$this->set( 'm_intEmailOwnerId', CStrings::strToIntDef( $intEmailOwnerId, NULL, false ) );
	}

	public function getEmailOwnerId() {
		return $this->m_intEmailOwnerId;
	}

	public function sqlEmailOwnerId() {
		return ( true == isset( $this->m_intEmailOwnerId ) ) ? ( string ) $this->m_intEmailOwnerId : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setSendOptionUpdatedBy( $intSendOptionUpdatedBy ) {
		$this->set( 'm_intSendOptionUpdatedBy', CStrings::strToIntDef( $intSendOptionUpdatedBy, NULL, false ) );
	}

	public function getSendOptionUpdatedBy() {
		return $this->m_intSendOptionUpdatedBy;
	}

	public function sqlSendOptionUpdatedBy() {
		return ( true == isset( $this->m_intSendOptionUpdatedBy ) ) ? ( string ) $this->m_intSendOptionUpdatedBy : 'NULL';
	}

	public function setSendOptionUpdatedOn( $strSendOptionUpdatedOn ) {
		$this->set( 'm_strSendOptionUpdatedOn', CStrings::strTrimDef( $strSendOptionUpdatedOn, -1, NULL, true ) );
	}

	public function getSendOptionUpdatedOn() {
		return $this->m_strSendOptionUpdatedOn;
	}

	public function sqlSendOptionUpdatedOn() {
		return ( true == isset( $this->m_strSendOptionUpdatedOn ) ) ? '\'' . $this->m_strSendOptionUpdatedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setLocaleCode( $strLocaleCode ) {
		$this->set( 'm_strLocaleCode', CStrings::strTrimDef( $strLocaleCode, 100, NULL, true ) );
	}

	public function getLocaleCode() {
		return $this->m_strLocaleCode;
	}

	public function sqlLocaleCode() {
		return ( true == isset( $this->m_strLocaleCode ) ) ? '\'' . addslashes( $this->m_strLocaleCode ) . '\'' : '\'en_US\'';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, scheduled_email_filter_ids, scheduled_email_event_type_id, scheduled_email_type_id, scheduled_email_delivery_type_id, system_message_template_id, frequency_id, header_footer_option_type_id, subject, from_email_address, test_email_address, start_datetime, end_datetime, last_sent_on, send_on_last_day, days_from_event, header_footer_option_id, is_after_event, is_shared, is_disabled, confirmed_on, locked_on, attempt_count, email_owner_id, order_num, send_option_updated_by, send_option_updated_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, locale_code, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlScheduledEmailFilterIds() . ', ' .
						$this->sqlScheduledEmailEventTypeId() . ', ' .
						$this->sqlScheduledEmailTypeId() . ', ' .
						$this->sqlScheduledEmailDeliveryTypeId() . ', ' .
						$this->sqlSystemMessageTemplateId() . ', ' .
						$this->sqlFrequencyId() . ', ' .
						$this->sqlHeaderFooterOptionTypeId() . ', ' .
						$this->sqlSubject() . ', ' .
						$this->sqlFromEmailAddress() . ', ' .
						$this->sqlTestEmailAddress() . ', ' .
						$this->sqlStartDatetime() . ', ' .
						$this->sqlEndDatetime() . ', ' .
						$this->sqlLastSentOn() . ', ' .
						$this->sqlSendOnLastDay() . ', ' .
						$this->sqlDaysFromEvent() . ', ' .
						$this->sqlHeaderFooterOptionId() . ', ' .
						$this->sqlIsAfterEvent() . ', ' .
						$this->sqlIsShared() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						$this->sqlConfirmedOn() . ', ' .
						$this->sqlLockedOn() . ', ' .
						$this->sqlAttemptCount() . ', ' .
						$this->sqlEmailOwnerId() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlSendOptionUpdatedBy() . ', ' .
						$this->sqlSendOptionUpdatedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlLocaleCode() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_email_filter_ids = ' . $this->sqlScheduledEmailFilterIds(). ',' ; } elseif( true == array_key_exists( 'ScheduledEmailFilterIds', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_email_filter_ids = ' . $this->sqlScheduledEmailFilterIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_email_event_type_id = ' . $this->sqlScheduledEmailEventTypeId(). ',' ; } elseif( true == array_key_exists( 'ScheduledEmailEventTypeId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_email_event_type_id = ' . $this->sqlScheduledEmailEventTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_email_type_id = ' . $this->sqlScheduledEmailTypeId(). ',' ; } elseif( true == array_key_exists( 'ScheduledEmailTypeId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_email_type_id = ' . $this->sqlScheduledEmailTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_email_delivery_type_id = ' . $this->sqlScheduledEmailDeliveryTypeId(). ',' ; } elseif( true == array_key_exists( 'ScheduledEmailDeliveryTypeId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_email_delivery_type_id = ' . $this->sqlScheduledEmailDeliveryTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_message_template_id = ' . $this->sqlSystemMessageTemplateId(). ',' ; } elseif( true == array_key_exists( 'SystemMessageTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' system_message_template_id = ' . $this->sqlSystemMessageTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId(). ',' ; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_footer_option_type_id = ' . $this->sqlHeaderFooterOptionTypeId(). ',' ; } elseif( true == array_key_exists( 'HeaderFooterOptionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' header_footer_option_type_id = ' . $this->sqlHeaderFooterOptionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subject = ' . $this->sqlSubject(). ',' ; } elseif( true == array_key_exists( 'Subject', $this->getChangedColumns() ) ) { $strSql .= ' subject = ' . $this->sqlSubject() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' from_email_address = ' . $this->sqlFromEmailAddress(). ',' ; } elseif( true == array_key_exists( 'FromEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' from_email_address = ' . $this->sqlFromEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_email_address = ' . $this->sqlTestEmailAddress(). ',' ; } elseif( true == array_key_exists( 'TestEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' test_email_address = ' . $this->sqlTestEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_datetime = ' . $this->sqlStartDatetime(). ',' ; } elseif( true == array_key_exists( 'StartDatetime', $this->getChangedColumns() ) ) { $strSql .= ' start_datetime = ' . $this->sqlStartDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime(). ',' ; } elseif( true == array_key_exists( 'EndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_sent_on = ' . $this->sqlLastSentOn(). ',' ; } elseif( true == array_key_exists( 'LastSentOn', $this->getChangedColumns() ) ) { $strSql .= ' last_sent_on = ' . $this->sqlLastSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' send_on_last_day = ' . $this->sqlSendOnLastDay(). ',' ; } elseif( true == array_key_exists( 'SendOnLastDay', $this->getChangedColumns() ) ) { $strSql .= ' send_on_last_day = ' . $this->sqlSendOnLastDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' days_from_event = ' . $this->sqlDaysFromEvent(). ',' ; } elseif( true == array_key_exists( 'DaysFromEvent', $this->getChangedColumns() ) ) { $strSql .= ' days_from_event = ' . $this->sqlDaysFromEvent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_footer_option_id = ' . $this->sqlHeaderFooterOptionId(). ',' ; } elseif( true == array_key_exists( 'HeaderFooterOptionId', $this->getChangedColumns() ) ) { $strSql .= ' header_footer_option_id = ' . $this->sqlHeaderFooterOptionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_after_event = ' . $this->sqlIsAfterEvent(). ',' ; } elseif( true == array_key_exists( 'IsAfterEvent', $this->getChangedColumns() ) ) { $strSql .= ' is_after_event = ' . $this->sqlIsAfterEvent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_shared = ' . $this->sqlIsShared(). ',' ; } elseif( true == array_key_exists( 'IsShared', $this->getChangedColumns() ) ) { $strSql .= ' is_shared = ' . $this->sqlIsShared() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn(). ',' ; } elseif( true == array_key_exists( 'ConfirmedOn', $this->getChangedColumns() ) ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn(). ',' ; } elseif( true == array_key_exists( 'LockedOn', $this->getChangedColumns() ) ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' attempt_count = ' . $this->sqlAttemptCount(). ',' ; } elseif( true == array_key_exists( 'AttemptCount', $this->getChangedColumns() ) ) { $strSql .= ' attempt_count = ' . $this->sqlAttemptCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_owner_id = ' . $this->sqlEmailOwnerId(). ',' ; } elseif( true == array_key_exists( 'EmailOwnerId', $this->getChangedColumns() ) ) { $strSql .= ' email_owner_id = ' . $this->sqlEmailOwnerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' send_option_updated_by = ' . $this->sqlSendOptionUpdatedBy(). ',' ; } elseif( true == array_key_exists( 'SendOptionUpdatedBy', $this->getChangedColumns() ) ) { $strSql .= ' send_option_updated_by = ' . $this->sqlSendOptionUpdatedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' send_option_updated_on = ' . $this->sqlSendOptionUpdatedOn(). ',' ; } elseif( true == array_key_exists( 'SendOptionUpdatedOn', $this->getChangedColumns() ) ) { $strSql .= ' send_option_updated_on = ' . $this->sqlSendOptionUpdatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locale_code = ' . $this->sqlLocaleCode(). ',' ; } elseif( true == array_key_exists( 'LocaleCode', $this->getChangedColumns() ) ) { $strSql .= ' locale_code = ' . $this->sqlLocaleCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'scheduled_email_filter_ids' => $this->getScheduledEmailFilterIds(),
			'scheduled_email_event_type_id' => $this->getScheduledEmailEventTypeId(),
			'scheduled_email_type_id' => $this->getScheduledEmailTypeId(),
			'scheduled_email_delivery_type_id' => $this->getScheduledEmailDeliveryTypeId(),
			'system_message_template_id' => $this->getSystemMessageTemplateId(),
			'frequency_id' => $this->getFrequencyId(),
			'header_footer_option_type_id' => $this->getHeaderFooterOptionTypeId(),
			'subject' => $this->getSubject(),
			'from_email_address' => $this->getFromEmailAddress(),
			'test_email_address' => $this->getTestEmailAddress(),
			'start_datetime' => $this->getStartDatetime(),
			'end_datetime' => $this->getEndDatetime(),
			'last_sent_on' => $this->getLastSentOn(),
			'send_on_last_day' => $this->getSendOnLastDay(),
			'days_from_event' => $this->getDaysFromEvent(),
			'header_footer_option_id' => $this->getHeaderFooterOptionId(),
			'is_after_event' => $this->getIsAfterEvent(),
			'is_shared' => $this->getIsShared(),
			'is_disabled' => $this->getIsDisabled(),
			'confirmed_on' => $this->getConfirmedOn(),
			'locked_on' => $this->getLockedOn(),
			'attempt_count' => $this->getAttemptCount(),
			'email_owner_id' => $this->getEmailOwnerId(),
			'order_num' => $this->getOrderNum(),
			'send_option_updated_by' => $this->getSendOptionUpdatedBy(),
			'send_option_updated_on' => $this->getSendOptionUpdatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'locale_code' => $this->getLocaleCode(),
			'details' => $this->getDetails()
		);
	}

}
?>