<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningApplicantResultLog extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_applicant_result_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScreeningApplicantResultId;
	protected $m_intScreeningPackageId;
	protected $m_strScreeningPackageName;
	protected $m_strScreeningApplicantScreenTypeResult;
	protected $m_jsonScreeningApplicantScreenTypeResult;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intScreeningRecommendationTypeId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['screening_applicant_result_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningApplicantResultId', trim( $arrValues['screening_applicant_result_id'] ) ); elseif( isset( $arrValues['screening_applicant_result_id'] ) ) $this->setScreeningApplicantResultId( $arrValues['screening_applicant_result_id'] );
		if( isset( $arrValues['screening_package_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningPackageId', trim( $arrValues['screening_package_id'] ) ); elseif( isset( $arrValues['screening_package_id'] ) ) $this->setScreeningPackageId( $arrValues['screening_package_id'] );
		if( isset( $arrValues['screening_package_name'] ) && $boolDirectSet ) $this->set( 'm_strScreeningPackageName', trim( stripcslashes( $arrValues['screening_package_name'] ) ) ); elseif( isset( $arrValues['screening_package_name'] ) ) $this->setScreeningPackageName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['screening_package_name'] ) : $arrValues['screening_package_name'] );
		if( isset( $arrValues['screening_applicant_screen_type_result'] ) ) $this->set( 'm_strScreeningApplicantScreenTypeResult', trim( $arrValues['screening_applicant_screen_type_result'] ) );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['screening_recommendation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningRecommendationTypeId', trim( $arrValues['screening_recommendation_type_id'] ) ); elseif( isset( $arrValues['screening_recommendation_type_id'] ) ) $this->setScreeningRecommendationTypeId( $arrValues['screening_recommendation_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScreeningApplicantResultId( $intScreeningApplicantResultId ) {
		$this->set( 'm_intScreeningApplicantResultId', CStrings::strToIntDef( $intScreeningApplicantResultId, NULL, false ) );
	}

	public function getScreeningApplicantResultId() {
		return $this->m_intScreeningApplicantResultId;
	}

	public function sqlScreeningApplicantResultId() {
		return ( true == isset( $this->m_intScreeningApplicantResultId ) ) ? ( string ) $this->m_intScreeningApplicantResultId : 'NULL';
	}

	public function setScreeningPackageId( $intScreeningPackageId ) {
		$this->set( 'm_intScreeningPackageId', CStrings::strToIntDef( $intScreeningPackageId, NULL, false ) );
	}

	public function getScreeningPackageId() {
		return $this->m_intScreeningPackageId;
	}

	public function sqlScreeningPackageId() {
		return ( true == isset( $this->m_intScreeningPackageId ) ) ? ( string ) $this->m_intScreeningPackageId : 'NULL';
	}

	public function setScreeningPackageName( $strScreeningPackageName ) {
		$this->set( 'm_strScreeningPackageName', CStrings::strTrimDef( $strScreeningPackageName, 64, NULL, true ) );
	}

	public function getScreeningPackageName() {
		return $this->m_strScreeningPackageName;
	}

	public function sqlScreeningPackageName() {
		return ( true == isset( $this->m_strScreeningPackageName ) ) ? '\'' . addslashes( $this->m_strScreeningPackageName ) . '\'' : 'NULL';
	}

	public function setScreeningApplicantScreenTypeResult( $jsonScreeningApplicantScreenTypeResult ) {
		if( true == valObj( $jsonScreeningApplicantScreenTypeResult, 'stdClass' ) ) {
			$this->set( 'm_jsonScreeningApplicantScreenTypeResult', $jsonScreeningApplicantScreenTypeResult );
		} elseif( true == valJsonString( $jsonScreeningApplicantScreenTypeResult ) ) {
			$this->set( 'm_jsonScreeningApplicantScreenTypeResult', CStrings::strToJson( $jsonScreeningApplicantScreenTypeResult ) );
		} else {
			$this->set( 'm_jsonScreeningApplicantScreenTypeResult', NULL ); 
		}
		unset( $this->m_strScreeningApplicantScreenTypeResult );
	}

	public function getScreeningApplicantScreenTypeResult() {
		if( true == isset( $this->m_strScreeningApplicantScreenTypeResult ) ) {
			$this->m_jsonScreeningApplicantScreenTypeResult = CStrings::strToJson( $this->m_strScreeningApplicantScreenTypeResult );
			unset( $this->m_strScreeningApplicantScreenTypeResult );
		}
		return $this->m_jsonScreeningApplicantScreenTypeResult;
	}

	public function sqlScreeningApplicantScreenTypeResult() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getScreeningApplicantScreenTypeResult() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getScreeningApplicantScreenTypeResult() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setScreeningRecommendationTypeId( $intScreeningRecommendationTypeId ) {
		$this->set( 'm_intScreeningRecommendationTypeId', CStrings::strToIntDef( $intScreeningRecommendationTypeId, NULL, false ) );
	}

	public function getScreeningRecommendationTypeId() {
		return $this->m_intScreeningRecommendationTypeId;
	}

	public function sqlScreeningRecommendationTypeId() {
		return ( true == isset( $this->m_intScreeningRecommendationTypeId ) ) ? ( string ) $this->m_intScreeningRecommendationTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, screening_applicant_result_id, screening_package_id, screening_package_name, screening_applicant_screen_type_result, updated_by, updated_on, created_by, created_on, screening_recommendation_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlScreeningApplicantResultId() . ', ' .
						$this->sqlScreeningPackageId() . ', ' .
						$this->sqlScreeningPackageName() . ', ' .
						$this->sqlScreeningApplicantScreenTypeResult() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlScreeningRecommendationTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_applicant_result_id = ' . $this->sqlScreeningApplicantResultId(). ',' ; } elseif( true == array_key_exists( 'ScreeningApplicantResultId', $this->getChangedColumns() ) ) { $strSql .= ' screening_applicant_result_id = ' . $this->sqlScreeningApplicantResultId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_package_id = ' . $this->sqlScreeningPackageId(). ',' ; } elseif( true == array_key_exists( 'ScreeningPackageId', $this->getChangedColumns() ) ) { $strSql .= ' screening_package_id = ' . $this->sqlScreeningPackageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_package_name = ' . $this->sqlScreeningPackageName(). ',' ; } elseif( true == array_key_exists( 'ScreeningPackageName', $this->getChangedColumns() ) ) { $strSql .= ' screening_package_name = ' . $this->sqlScreeningPackageName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_applicant_screen_type_result = ' . $this->sqlScreeningApplicantScreenTypeResult(). ',' ; } elseif( true == array_key_exists( 'ScreeningApplicantScreenTypeResult', $this->getChangedColumns() ) ) { $strSql .= ' screening_applicant_screen_type_result = ' . $this->sqlScreeningApplicantScreenTypeResult() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_recommendation_type_id = ' . $this->sqlScreeningRecommendationTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningRecommendationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_recommendation_type_id = ' . $this->sqlScreeningRecommendationTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'screening_applicant_result_id' => $this->getScreeningApplicantResultId(),
			'screening_package_id' => $this->getScreeningPackageId(),
			'screening_package_name' => $this->getScreeningPackageName(),
			'screening_applicant_screen_type_result' => $this->getScreeningApplicantScreenTypeResult(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'screening_recommendation_type_id' => $this->getScreeningRecommendationTypeId()
		);
	}

}
?>