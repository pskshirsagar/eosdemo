<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportColumns
 * Do not add any new functions to this class.
 */

class CBaseReportColumns extends CEosPluralBase {

	/**
	 * @return CReportColumn[]
	 */
	public static function fetchReportColumns( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportColumn::class, $objDatabase );
	}

	/**
	 * @return CReportColumn
	 */
	public static function fetchReportColumn( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportColumn::class, $objDatabase );
	}

	public static function fetchReportColumnCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_columns', $objDatabase );
	}

	public static function fetchReportColumnById( $intId, $objDatabase ) {
		return self::fetchReportColumn( sprintf( 'SELECT * FROM report_columns WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReportColumnsByDefaultCid( $intDefaultCid, $objDatabase ) {
		return self::fetchReportColumns( sprintf( 'SELECT * FROM report_columns WHERE default_cid = %d', ( int ) $intDefaultCid ), $objDatabase );
	}

	public static function fetchReportColumnsByReportDatasetId( $intReportDatasetId, $objDatabase ) {
		return self::fetchReportColumns( sprintf( 'SELECT * FROM report_columns WHERE report_dataset_id = %d', ( int ) $intReportDatasetId ), $objDatabase );
	}

}
?>