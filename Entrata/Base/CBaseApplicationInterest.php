<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicationInterest extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.application_interests';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intApplicationId;
	protected $m_intPropertyFloorplanId;
	protected $m_intUnitSpaceId;
	protected $m_intAddOnId;
	protected $m_intEventTypeId;
	protected $m_intIsEmailOffer;
	protected $m_intIsSmsOffer;
	protected $m_intIsPhoneOffer;
	protected $m_intIsMailOffer;
	protected $m_strOfferSubmittedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorplanId', trim( $arrValues['property_floorplan_id'] ) ); elseif( isset( $arrValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrValues['property_floorplan_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['add_on_id'] ) && $boolDirectSet ) $this->set( 'm_intAddOnId', trim( $arrValues['add_on_id'] ) ); elseif( isset( $arrValues['add_on_id'] ) ) $this->setAddOnId( $arrValues['add_on_id'] );
		if( isset( $arrValues['event_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEventTypeId', trim( $arrValues['event_type_id'] ) ); elseif( isset( $arrValues['event_type_id'] ) ) $this->setEventTypeId( $arrValues['event_type_id'] );
		if( isset( $arrValues['is_email_offer'] ) && $boolDirectSet ) $this->set( 'm_intIsEmailOffer', trim( $arrValues['is_email_offer'] ) ); elseif( isset( $arrValues['is_email_offer'] ) ) $this->setIsEmailOffer( $arrValues['is_email_offer'] );
		if( isset( $arrValues['is_sms_offer'] ) && $boolDirectSet ) $this->set( 'm_intIsSmsOffer', trim( $arrValues['is_sms_offer'] ) ); elseif( isset( $arrValues['is_sms_offer'] ) ) $this->setIsSmsOffer( $arrValues['is_sms_offer'] );
		if( isset( $arrValues['is_phone_offer'] ) && $boolDirectSet ) $this->set( 'm_intIsPhoneOffer', trim( $arrValues['is_phone_offer'] ) ); elseif( isset( $arrValues['is_phone_offer'] ) ) $this->setIsPhoneOffer( $arrValues['is_phone_offer'] );
		if( isset( $arrValues['is_mail_offer'] ) && $boolDirectSet ) $this->set( 'm_intIsMailOffer', trim( $arrValues['is_mail_offer'] ) ); elseif( isset( $arrValues['is_mail_offer'] ) ) $this->setIsMailOffer( $arrValues['is_mail_offer'] );
		if( isset( $arrValues['offer_submitted_on'] ) && $boolDirectSet ) $this->set( 'm_strOfferSubmittedOn', trim( $arrValues['offer_submitted_on'] ) ); elseif( isset( $arrValues['offer_submitted_on'] ) ) $this->setOfferSubmittedOn( $arrValues['offer_submitted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->set( 'm_intPropertyFloorplanId', CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false ) );
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function sqlPropertyFloorplanId() {
		return ( true == isset( $this->m_intPropertyFloorplanId ) ) ? ( string ) $this->m_intPropertyFloorplanId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setAddOnId( $intAddOnId ) {
		$this->set( 'm_intAddOnId', CStrings::strToIntDef( $intAddOnId, NULL, false ) );
	}

	public function getAddOnId() {
		return $this->m_intAddOnId;
	}

	public function sqlAddOnId() {
		return ( true == isset( $this->m_intAddOnId ) ) ? ( string ) $this->m_intAddOnId : 'NULL';
	}

	public function setEventTypeId( $intEventTypeId ) {
		$this->set( 'm_intEventTypeId', CStrings::strToIntDef( $intEventTypeId, NULL, false ) );
	}

	public function getEventTypeId() {
		return $this->m_intEventTypeId;
	}

	public function sqlEventTypeId() {
		return ( true == isset( $this->m_intEventTypeId ) ) ? ( string ) $this->m_intEventTypeId : 'NULL';
	}

	public function setIsEmailOffer( $intIsEmailOffer ) {
		$this->set( 'm_intIsEmailOffer', CStrings::strToIntDef( $intIsEmailOffer, NULL, false ) );
	}

	public function getIsEmailOffer() {
		return $this->m_intIsEmailOffer;
	}

	public function sqlIsEmailOffer() {
		return ( true == isset( $this->m_intIsEmailOffer ) ) ? ( string ) $this->m_intIsEmailOffer : 'NULL';
	}

	public function setIsSmsOffer( $intIsSmsOffer ) {
		$this->set( 'm_intIsSmsOffer', CStrings::strToIntDef( $intIsSmsOffer, NULL, false ) );
	}

	public function getIsSmsOffer() {
		return $this->m_intIsSmsOffer;
	}

	public function sqlIsSmsOffer() {
		return ( true == isset( $this->m_intIsSmsOffer ) ) ? ( string ) $this->m_intIsSmsOffer : 'NULL';
	}

	public function setIsPhoneOffer( $intIsPhoneOffer ) {
		$this->set( 'm_intIsPhoneOffer', CStrings::strToIntDef( $intIsPhoneOffer, NULL, false ) );
	}

	public function getIsPhoneOffer() {
		return $this->m_intIsPhoneOffer;
	}

	public function sqlIsPhoneOffer() {
		return ( true == isset( $this->m_intIsPhoneOffer ) ) ? ( string ) $this->m_intIsPhoneOffer : 'NULL';
	}

	public function setIsMailOffer( $intIsMailOffer ) {
		$this->set( 'm_intIsMailOffer', CStrings::strToIntDef( $intIsMailOffer, NULL, false ) );
	}

	public function getIsMailOffer() {
		return $this->m_intIsMailOffer;
	}

	public function sqlIsMailOffer() {
		return ( true == isset( $this->m_intIsMailOffer ) ) ? ( string ) $this->m_intIsMailOffer : 'NULL';
	}

	public function setOfferSubmittedOn( $strOfferSubmittedOn ) {
		$this->set( 'm_strOfferSubmittedOn', CStrings::strTrimDef( $strOfferSubmittedOn, -1, NULL, true ) );
	}

	public function getOfferSubmittedOn() {
		return $this->m_strOfferSubmittedOn;
	}

	public function sqlOfferSubmittedOn() {
		return ( true == isset( $this->m_strOfferSubmittedOn ) ) ? '\'' . $this->m_strOfferSubmittedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, application_id, property_floorplan_id, unit_space_id, add_on_id, event_type_id, is_email_offer, is_sms_offer, is_phone_offer, is_mail_offer, offer_submitted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlApplicationId() . ', ' .
		          $this->sqlPropertyFloorplanId() . ', ' .
		          $this->sqlUnitSpaceId() . ', ' .
		          $this->sqlAddOnId() . ', ' .
		          $this->sqlEventTypeId() . ', ' .
		          $this->sqlIsEmailOffer() . ', ' .
		          $this->sqlIsSmsOffer() . ', ' .
		          $this->sqlIsPhoneOffer() . ', ' .
		          $this->sqlIsMailOffer() . ', ' .
		          $this->sqlOfferSubmittedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlDetails() . ' ) ' . ' RETURNING id;';
		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId(). ',' ; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId(). ',' ; } elseif( true == array_key_exists( 'PropertyFloorplanId', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_on_id = ' . $this->sqlAddOnId(). ',' ; } elseif( true == array_key_exists( 'AddOnId', $this->getChangedColumns() ) ) { $strSql .= ' add_on_id = ' . $this->sqlAddOnId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_type_id = ' . $this->sqlEventTypeId(). ',' ; } elseif( true == array_key_exists( 'EventTypeId', $this->getChangedColumns() ) ) { $strSql .= ' event_type_id = ' . $this->sqlEventTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_email_offer = ' . $this->sqlIsEmailOffer(). ',' ; } elseif( true == array_key_exists( 'IsEmailOffer', $this->getChangedColumns() ) ) { $strSql .= ' is_email_offer = ' . $this->sqlIsEmailOffer() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_sms_offer = ' . $this->sqlIsSmsOffer(). ',' ; } elseif( true == array_key_exists( 'IsSmsOffer', $this->getChangedColumns() ) ) { $strSql .= ' is_sms_offer = ' . $this->sqlIsSmsOffer() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_phone_offer = ' . $this->sqlIsPhoneOffer(). ',' ; } elseif( true == array_key_exists( 'IsPhoneOffer', $this->getChangedColumns() ) ) { $strSql .= ' is_phone_offer = ' . $this->sqlIsPhoneOffer() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_mail_offer = ' . $this->sqlIsMailOffer(). ',' ; } elseif( true == array_key_exists( 'IsMailOffer', $this->getChangedColumns() ) ) { $strSql .= ' is_mail_offer = ' . $this->sqlIsMailOffer() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' offer_submitted_on = ' . $this->sqlOfferSubmittedOn(). ',' ; } elseif( true == array_key_exists( 'OfferSubmittedOn', $this->getChangedColumns() ) ) { $strSql .= ' offer_submitted_on = ' . $this->sqlOfferSubmittedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';
		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'application_id' => $this->getApplicationId(),
			'property_floorplan_id' => $this->getPropertyFloorplanId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'add_on_id' => $this->getAddOnId(),
			'event_type_id' => $this->getEventTypeId(),
			'is_email_offer' => $this->getIsEmailOffer(),
			'is_sms_offer' => $this->getIsSmsOffer(),
			'is_phone_offer' => $this->getIsPhoneOffer(),
			'is_mail_offer' => $this->getIsMailOffer(),
			'offer_submitted_on' => $this->getOfferSubmittedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>