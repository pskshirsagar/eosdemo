<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRevenueOptimalRent extends CEosSingularBase {

	const TABLE_NAME = 'public.revenue_optimal_rents';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUnitTypeId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_fltMinExecutedRent;
	protected $m_fltMaxExecutedRent;
	protected $m_fltAvgExecutedRent;
	protected $m_fltLastExecutedRent;
	protected $m_fltMinAdvertisedRent;
	protected $m_fltMaxAdvertisedRent;
	protected $m_fltAvgAdvertisedRent;
	protected $m_fltStdAdvertisedRent;
	protected $m_fltMinOptimalRent;
	protected $m_fltMaxOptimalRent;
	protected $m_fltAvgOptimalRent;
	protected $m_fltStdOptimalRent;
	protected $m_fltMinCompetitiveRent;
	protected $m_fltMaxCompetitiveRent;
	protected $m_fltAvgCompetitiveRent;
	protected $m_fltBaseRent;
	protected $m_fltOptimalBaseRent;
	protected $m_fltNoticePeriodAdjustment;
	protected $m_fltStalenessPeriodAdjustment;
	protected $m_fltMinimumLeasedAdjustment;
	protected $m_fltSustainableOccupancyAdjustment;
	protected $m_fltOptimalOccupancyRent;
	protected $m_fltOptimalOccupancyRentConstrained;
	protected $m_fltOptimalAdvertisedRent;
	protected $m_intAvailableUnitCount;
	protected $m_intOccupiedUnitCount;
	protected $m_intRentableUnitCount;
	protected $m_fltExposed30UnitCount;
	protected $m_fltExposed60UnitCount;
	protected $m_fltExposed90UnitCount;
	protected $m_fltDemandThirtyUnitCount;
	protected $m_fltDemandSixtyUnitCount;
	protected $m_fltDemandNinetyUnitCount;
	protected $m_fltWeeklyGrowthRate;
	protected $m_fltMonthlyGrowthRate;
	protected $m_fltQuarterlyGrowthRate;
	protected $m_fltYearlyGrowthRate;
	protected $m_strFirstMoveInDate;
	protected $m_strMatrix;
	protected $m_jsonMatrix;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_fltStdVacancyLossAdjustment;
	protected $m_fltStdTurnCostAdjustment;
	protected $m_fltStdExpirationAdjustment;
	protected $m_fltOptimalAdvertisedRentUnconstrained;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['min_executed_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMinExecutedRent', trim( $arrValues['min_executed_rent'] ) ); elseif( isset( $arrValues['min_executed_rent'] ) ) $this->setMinExecutedRent( $arrValues['min_executed_rent'] );
		if( isset( $arrValues['max_executed_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMaxExecutedRent', trim( $arrValues['max_executed_rent'] ) ); elseif( isset( $arrValues['max_executed_rent'] ) ) $this->setMaxExecutedRent( $arrValues['max_executed_rent'] );
		if( isset( $arrValues['avg_executed_rent'] ) && $boolDirectSet ) $this->set( 'm_fltAvgExecutedRent', trim( $arrValues['avg_executed_rent'] ) ); elseif( isset( $arrValues['avg_executed_rent'] ) ) $this->setAvgExecutedRent( $arrValues['avg_executed_rent'] );
		if( isset( $arrValues['last_executed_rent'] ) && $boolDirectSet ) $this->set( 'm_fltLastExecutedRent', trim( $arrValues['last_executed_rent'] ) ); elseif( isset( $arrValues['last_executed_rent'] ) ) $this->setLastExecutedRent( $arrValues['last_executed_rent'] );
		if( isset( $arrValues['min_advertised_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMinAdvertisedRent', trim( $arrValues['min_advertised_rent'] ) ); elseif( isset( $arrValues['min_advertised_rent'] ) ) $this->setMinAdvertisedRent( $arrValues['min_advertised_rent'] );
		if( isset( $arrValues['max_advertised_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMaxAdvertisedRent', trim( $arrValues['max_advertised_rent'] ) ); elseif( isset( $arrValues['max_advertised_rent'] ) ) $this->setMaxAdvertisedRent( $arrValues['max_advertised_rent'] );
		if( isset( $arrValues['avg_advertised_rent'] ) && $boolDirectSet ) $this->set( 'm_fltAvgAdvertisedRent', trim( $arrValues['avg_advertised_rent'] ) ); elseif( isset( $arrValues['avg_advertised_rent'] ) ) $this->setAvgAdvertisedRent( $arrValues['avg_advertised_rent'] );
		if( isset( $arrValues['std_advertised_rent'] ) && $boolDirectSet ) $this->set( 'm_fltStdAdvertisedRent', trim( $arrValues['std_advertised_rent'] ) ); elseif( isset( $arrValues['std_advertised_rent'] ) ) $this->setStdAdvertisedRent( $arrValues['std_advertised_rent'] );
		if( isset( $arrValues['min_optimal_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMinOptimalRent', trim( $arrValues['min_optimal_rent'] ) ); elseif( isset( $arrValues['min_optimal_rent'] ) ) $this->setMinOptimalRent( $arrValues['min_optimal_rent'] );
		if( isset( $arrValues['max_optimal_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMaxOptimalRent', trim( $arrValues['max_optimal_rent'] ) ); elseif( isset( $arrValues['max_optimal_rent'] ) ) $this->setMaxOptimalRent( $arrValues['max_optimal_rent'] );
		if( isset( $arrValues['avg_optimal_rent'] ) && $boolDirectSet ) $this->set( 'm_fltAvgOptimalRent', trim( $arrValues['avg_optimal_rent'] ) ); elseif( isset( $arrValues['avg_optimal_rent'] ) ) $this->setAvgOptimalRent( $arrValues['avg_optimal_rent'] );
		if( isset( $arrValues['std_optimal_rent'] ) && $boolDirectSet ) $this->set( 'm_fltStdOptimalRent', trim( $arrValues['std_optimal_rent'] ) ); elseif( isset( $arrValues['std_optimal_rent'] ) ) $this->setStdOptimalRent( $arrValues['std_optimal_rent'] );
		if( isset( $arrValues['min_competitive_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMinCompetitiveRent', trim( $arrValues['min_competitive_rent'] ) ); elseif( isset( $arrValues['min_competitive_rent'] ) ) $this->setMinCompetitiveRent( $arrValues['min_competitive_rent'] );
		if( isset( $arrValues['max_competitive_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMaxCompetitiveRent', trim( $arrValues['max_competitive_rent'] ) ); elseif( isset( $arrValues['max_competitive_rent'] ) ) $this->setMaxCompetitiveRent( $arrValues['max_competitive_rent'] );
		if( isset( $arrValues['avg_competitive_rent'] ) && $boolDirectSet ) $this->set( 'm_fltAvgCompetitiveRent', trim( $arrValues['avg_competitive_rent'] ) ); elseif( isset( $arrValues['avg_competitive_rent'] ) ) $this->setAvgCompetitiveRent( $arrValues['avg_competitive_rent'] );
		if( isset( $arrValues['base_rent'] ) && $boolDirectSet ) $this->set( 'm_fltBaseRent', trim( $arrValues['base_rent'] ) ); elseif( isset( $arrValues['base_rent'] ) ) $this->setBaseRent( $arrValues['base_rent'] );
		if( isset( $arrValues['optimal_base_rent'] ) && $boolDirectSet ) $this->set( 'm_fltOptimalBaseRent', trim( $arrValues['optimal_base_rent'] ) ); elseif( isset( $arrValues['optimal_base_rent'] ) ) $this->setOptimalBaseRent( $arrValues['optimal_base_rent'] );
		if( isset( $arrValues['notice_period_adjustment'] ) && $boolDirectSet ) $this->set( 'm_fltNoticePeriodAdjustment', trim( $arrValues['notice_period_adjustment'] ) ); elseif( isset( $arrValues['notice_period_adjustment'] ) ) $this->setNoticePeriodAdjustment( $arrValues['notice_period_adjustment'] );
		if( isset( $arrValues['staleness_period_adjustment'] ) && $boolDirectSet ) $this->set( 'm_fltStalenessPeriodAdjustment', trim( $arrValues['staleness_period_adjustment'] ) ); elseif( isset( $arrValues['staleness_period_adjustment'] ) ) $this->setStalenessPeriodAdjustment( $arrValues['staleness_period_adjustment'] );
		if( isset( $arrValues['minimum_leased_adjustment'] ) && $boolDirectSet ) $this->set( 'm_fltMinimumLeasedAdjustment', trim( $arrValues['minimum_leased_adjustment'] ) ); elseif( isset( $arrValues['minimum_leased_adjustment'] ) ) $this->setMinimumLeasedAdjustment( $arrValues['minimum_leased_adjustment'] );
		if( isset( $arrValues['sustainable_occupancy_adjustment'] ) && $boolDirectSet ) $this->set( 'm_fltSustainableOccupancyAdjustment', trim( $arrValues['sustainable_occupancy_adjustment'] ) ); elseif( isset( $arrValues['sustainable_occupancy_adjustment'] ) ) $this->setSustainableOccupancyAdjustment( $arrValues['sustainable_occupancy_adjustment'] );
		if( isset( $arrValues['optimal_occupancy_rent'] ) && $boolDirectSet ) $this->set( 'm_fltOptimalOccupancyRent', trim( $arrValues['optimal_occupancy_rent'] ) ); elseif( isset( $arrValues['optimal_occupancy_rent'] ) ) $this->setOptimalOccupancyRent( $arrValues['optimal_occupancy_rent'] );
		if( isset( $arrValues['optimal_occupancy_rent_constrained'] ) && $boolDirectSet ) $this->set( 'm_fltOptimalOccupancyRentConstrained', trim( $arrValues['optimal_occupancy_rent_constrained'] ) ); elseif( isset( $arrValues['optimal_occupancy_rent_constrained'] ) ) $this->setOptimalOccupancyRentConstrained( $arrValues['optimal_occupancy_rent_constrained'] );
		if( isset( $arrValues['optimal_advertised_rent'] ) && $boolDirectSet ) $this->set( 'm_fltOptimalAdvertisedRent', trim( $arrValues['optimal_advertised_rent'] ) ); elseif( isset( $arrValues['optimal_advertised_rent'] ) ) $this->setOptimalAdvertisedRent( $arrValues['optimal_advertised_rent'] );
		if( isset( $arrValues['available_unit_count'] ) && $boolDirectSet ) $this->set( 'm_intAvailableUnitCount', trim( $arrValues['available_unit_count'] ) ); elseif( isset( $arrValues['available_unit_count'] ) ) $this->setAvailableUnitCount( $arrValues['available_unit_count'] );
		if( isset( $arrValues['occupied_unit_count'] ) && $boolDirectSet ) $this->set( 'm_intOccupiedUnitCount', trim( $arrValues['occupied_unit_count'] ) ); elseif( isset( $arrValues['occupied_unit_count'] ) ) $this->setOccupiedUnitCount( $arrValues['occupied_unit_count'] );
		if( isset( $arrValues['rentable_unit_count'] ) && $boolDirectSet ) $this->set( 'm_intRentableUnitCount', trim( $arrValues['rentable_unit_count'] ) ); elseif( isset( $arrValues['rentable_unit_count'] ) ) $this->setRentableUnitCount( $arrValues['rentable_unit_count'] );
		if( isset( $arrValues['exposed_30_unit_count'] ) && $boolDirectSet ) $this->set( 'm_fltExposed30UnitCount', trim( $arrValues['exposed_30_unit_count'] ) ); elseif( isset( $arrValues['exposed_30_unit_count'] ) ) $this->setExposed30UnitCount( $arrValues['exposed_30_unit_count'] );
		if( isset( $arrValues['exposed_60_unit_count'] ) && $boolDirectSet ) $this->set( 'm_fltExposed60UnitCount', trim( $arrValues['exposed_60_unit_count'] ) ); elseif( isset( $arrValues['exposed_60_unit_count'] ) ) $this->setExposed60UnitCount( $arrValues['exposed_60_unit_count'] );
		if( isset( $arrValues['exposed_90_unit_count'] ) && $boolDirectSet ) $this->set( 'm_fltExposed90UnitCount', trim( $arrValues['exposed_90_unit_count'] ) ); elseif( isset( $arrValues['exposed_90_unit_count'] ) ) $this->setExposed90UnitCount( $arrValues['exposed_90_unit_count'] );
		if( isset( $arrValues['demand_thirty_unit_count'] ) && $boolDirectSet ) $this->set( 'm_fltDemandThirtyUnitCount', trim( $arrValues['demand_thirty_unit_count'] ) ); elseif( isset( $arrValues['demand_thirty_unit_count'] ) ) $this->setDemandThirtyUnitCount( $arrValues['demand_thirty_unit_count'] );
		if( isset( $arrValues['demand_sixty_unit_count'] ) && $boolDirectSet ) $this->set( 'm_fltDemandSixtyUnitCount', trim( $arrValues['demand_sixty_unit_count'] ) ); elseif( isset( $arrValues['demand_sixty_unit_count'] ) ) $this->setDemandSixtyUnitCount( $arrValues['demand_sixty_unit_count'] );
		if( isset( $arrValues['demand_ninety_unit_count'] ) && $boolDirectSet ) $this->set( 'm_fltDemandNinetyUnitCount', trim( $arrValues['demand_ninety_unit_count'] ) ); elseif( isset( $arrValues['demand_ninety_unit_count'] ) ) $this->setDemandNinetyUnitCount( $arrValues['demand_ninety_unit_count'] );
		if( isset( $arrValues['weekly_growth_rate'] ) && $boolDirectSet ) $this->set( 'm_fltWeeklyGrowthRate', trim( $arrValues['weekly_growth_rate'] ) ); elseif( isset( $arrValues['weekly_growth_rate'] ) ) $this->setWeeklyGrowthRate( $arrValues['weekly_growth_rate'] );
		if( isset( $arrValues['monthly_growth_rate'] ) && $boolDirectSet ) $this->set( 'm_fltMonthlyGrowthRate', trim( $arrValues['monthly_growth_rate'] ) ); elseif( isset( $arrValues['monthly_growth_rate'] ) ) $this->setMonthlyGrowthRate( $arrValues['monthly_growth_rate'] );
		if( isset( $arrValues['quarterly_growth_rate'] ) && $boolDirectSet ) $this->set( 'm_fltQuarterlyGrowthRate', trim( $arrValues['quarterly_growth_rate'] ) ); elseif( isset( $arrValues['quarterly_growth_rate'] ) ) $this->setQuarterlyGrowthRate( $arrValues['quarterly_growth_rate'] );
		if( isset( $arrValues['yearly_growth_rate'] ) && $boolDirectSet ) $this->set( 'm_fltYearlyGrowthRate', trim( $arrValues['yearly_growth_rate'] ) ); elseif( isset( $arrValues['yearly_growth_rate'] ) ) $this->setYearlyGrowthRate( $arrValues['yearly_growth_rate'] );
		if( isset( $arrValues['first_move_in_date'] ) && $boolDirectSet ) $this->set( 'm_strFirstMoveInDate', trim( $arrValues['first_move_in_date'] ) ); elseif( isset( $arrValues['first_move_in_date'] ) ) $this->setFirstMoveInDate( $arrValues['first_move_in_date'] );
		if( isset( $arrValues['matrix'] ) ) $this->set( 'm_strMatrix', trim( $arrValues['matrix'] ) );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['std_vacancy_loss_adjustment'] ) && $boolDirectSet ) $this->set( 'm_fltStdVacancyLossAdjustment', trim( $arrValues['std_vacancy_loss_adjustment'] ) ); elseif( isset( $arrValues['std_vacancy_loss_adjustment'] ) ) $this->setStdVacancyLossAdjustment( $arrValues['std_vacancy_loss_adjustment'] );
		if( isset( $arrValues['std_turn_cost_adjustment'] ) && $boolDirectSet ) $this->set( 'm_fltStdTurnCostAdjustment', trim( $arrValues['std_turn_cost_adjustment'] ) ); elseif( isset( $arrValues['std_turn_cost_adjustment'] ) ) $this->setStdTurnCostAdjustment( $arrValues['std_turn_cost_adjustment'] );
		if( isset( $arrValues['std_expiration_adjustment'] ) && $boolDirectSet ) $this->set( 'm_fltStdExpirationAdjustment', trim( $arrValues['std_expiration_adjustment'] ) ); elseif( isset( $arrValues['std_expiration_adjustment'] ) ) $this->setStdExpirationAdjustment( $arrValues['std_expiration_adjustment'] );
		if( isset( $arrValues['optimal_advertised_rent_unconstrained'] ) && $boolDirectSet ) $this->set( 'm_fltOptimalAdvertisedRentUnconstrained', trim( $arrValues['optimal_advertised_rent_unconstrained'] ) ); elseif( isset( $arrValues['optimal_advertised_rent_unconstrained'] ) ) $this->setOptimalAdvertisedRentUnconstrained( $arrValues['optimal_advertised_rent_unconstrained'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setMinExecutedRent( $fltMinExecutedRent ) {
		$this->set( 'm_fltMinExecutedRent', CStrings::strToFloatDef( $fltMinExecutedRent, NULL, false, 2 ) );
	}

	public function getMinExecutedRent() {
		return $this->m_fltMinExecutedRent;
	}

	public function sqlMinExecutedRent() {
		return ( true == isset( $this->m_fltMinExecutedRent ) ) ? ( string ) $this->m_fltMinExecutedRent : 'NULL';
	}

	public function setMaxExecutedRent( $fltMaxExecutedRent ) {
		$this->set( 'm_fltMaxExecutedRent', CStrings::strToFloatDef( $fltMaxExecutedRent, NULL, false, 2 ) );
	}

	public function getMaxExecutedRent() {
		return $this->m_fltMaxExecutedRent;
	}

	public function sqlMaxExecutedRent() {
		return ( true == isset( $this->m_fltMaxExecutedRent ) ) ? ( string ) $this->m_fltMaxExecutedRent : 'NULL';
	}

	public function setAvgExecutedRent( $fltAvgExecutedRent ) {
		$this->set( 'm_fltAvgExecutedRent', CStrings::strToFloatDef( $fltAvgExecutedRent, NULL, false, 2 ) );
	}

	public function getAvgExecutedRent() {
		return $this->m_fltAvgExecutedRent;
	}

	public function sqlAvgExecutedRent() {
		return ( true == isset( $this->m_fltAvgExecutedRent ) ) ? ( string ) $this->m_fltAvgExecutedRent : 'NULL';
	}

	public function setLastExecutedRent( $fltLastExecutedRent ) {
		$this->set( 'm_fltLastExecutedRent', CStrings::strToFloatDef( $fltLastExecutedRent, NULL, false, 2 ) );
	}

	public function getLastExecutedRent() {
		return $this->m_fltLastExecutedRent;
	}

	public function sqlLastExecutedRent() {
		return ( true == isset( $this->m_fltLastExecutedRent ) ) ? ( string ) $this->m_fltLastExecutedRent : 'NULL';
	}

	public function setMinAdvertisedRent( $fltMinAdvertisedRent ) {
		$this->set( 'm_fltMinAdvertisedRent', CStrings::strToFloatDef( $fltMinAdvertisedRent, NULL, false, 2 ) );
	}

	public function getMinAdvertisedRent() {
		return $this->m_fltMinAdvertisedRent;
	}

	public function sqlMinAdvertisedRent() {
		return ( true == isset( $this->m_fltMinAdvertisedRent ) ) ? ( string ) $this->m_fltMinAdvertisedRent : 'NULL';
	}

	public function setMaxAdvertisedRent( $fltMaxAdvertisedRent ) {
		$this->set( 'm_fltMaxAdvertisedRent', CStrings::strToFloatDef( $fltMaxAdvertisedRent, NULL, false, 2 ) );
	}

	public function getMaxAdvertisedRent() {
		return $this->m_fltMaxAdvertisedRent;
	}

	public function sqlMaxAdvertisedRent() {
		return ( true == isset( $this->m_fltMaxAdvertisedRent ) ) ? ( string ) $this->m_fltMaxAdvertisedRent : 'NULL';
	}

	public function setAvgAdvertisedRent( $fltAvgAdvertisedRent ) {
		$this->set( 'm_fltAvgAdvertisedRent', CStrings::strToFloatDef( $fltAvgAdvertisedRent, NULL, false, 2 ) );
	}

	public function getAvgAdvertisedRent() {
		return $this->m_fltAvgAdvertisedRent;
	}

	public function sqlAvgAdvertisedRent() {
		return ( true == isset( $this->m_fltAvgAdvertisedRent ) ) ? ( string ) $this->m_fltAvgAdvertisedRent : 'NULL';
	}

	public function setStdAdvertisedRent( $fltStdAdvertisedRent ) {
		$this->set( 'm_fltStdAdvertisedRent', CStrings::strToFloatDef( $fltStdAdvertisedRent, NULL, false, 2 ) );
	}

	public function getStdAdvertisedRent() {
		return $this->m_fltStdAdvertisedRent;
	}

	public function sqlStdAdvertisedRent() {
		return ( true == isset( $this->m_fltStdAdvertisedRent ) ) ? ( string ) $this->m_fltStdAdvertisedRent : 'NULL';
	}

	public function setMinOptimalRent( $fltMinOptimalRent ) {
		$this->set( 'm_fltMinOptimalRent', CStrings::strToFloatDef( $fltMinOptimalRent, NULL, false, 2 ) );
	}

	public function getMinOptimalRent() {
		return $this->m_fltMinOptimalRent;
	}

	public function sqlMinOptimalRent() {
		return ( true == isset( $this->m_fltMinOptimalRent ) ) ? ( string ) $this->m_fltMinOptimalRent : 'NULL';
	}

	public function setMaxOptimalRent( $fltMaxOptimalRent ) {
		$this->set( 'm_fltMaxOptimalRent', CStrings::strToFloatDef( $fltMaxOptimalRent, NULL, false, 2 ) );
	}

	public function getMaxOptimalRent() {
		return $this->m_fltMaxOptimalRent;
	}

	public function sqlMaxOptimalRent() {
		return ( true == isset( $this->m_fltMaxOptimalRent ) ) ? ( string ) $this->m_fltMaxOptimalRent : 'NULL';
	}

	public function setAvgOptimalRent( $fltAvgOptimalRent ) {
		$this->set( 'm_fltAvgOptimalRent', CStrings::strToFloatDef( $fltAvgOptimalRent, NULL, false, 2 ) );
	}

	public function getAvgOptimalRent() {
		return $this->m_fltAvgOptimalRent;
	}

	public function sqlAvgOptimalRent() {
		return ( true == isset( $this->m_fltAvgOptimalRent ) ) ? ( string ) $this->m_fltAvgOptimalRent : 'NULL';
	}

	public function setStdOptimalRent( $fltStdOptimalRent ) {
		$this->set( 'm_fltStdOptimalRent', CStrings::strToFloatDef( $fltStdOptimalRent, NULL, false, 2 ) );
	}

	public function getStdOptimalRent() {
		return $this->m_fltStdOptimalRent;
	}

	public function sqlStdOptimalRent() {
		return ( true == isset( $this->m_fltStdOptimalRent ) ) ? ( string ) $this->m_fltStdOptimalRent : 'NULL';
	}

	public function setMinCompetitiveRent( $fltMinCompetitiveRent ) {
		$this->set( 'm_fltMinCompetitiveRent', CStrings::strToFloatDef( $fltMinCompetitiveRent, NULL, false, 2 ) );
	}

	public function getMinCompetitiveRent() {
		return $this->m_fltMinCompetitiveRent;
	}

	public function sqlMinCompetitiveRent() {
		return ( true == isset( $this->m_fltMinCompetitiveRent ) ) ? ( string ) $this->m_fltMinCompetitiveRent : 'NULL';
	}

	public function setMaxCompetitiveRent( $fltMaxCompetitiveRent ) {
		$this->set( 'm_fltMaxCompetitiveRent', CStrings::strToFloatDef( $fltMaxCompetitiveRent, NULL, false, 2 ) );
	}

	public function getMaxCompetitiveRent() {
		return $this->m_fltMaxCompetitiveRent;
	}

	public function sqlMaxCompetitiveRent() {
		return ( true == isset( $this->m_fltMaxCompetitiveRent ) ) ? ( string ) $this->m_fltMaxCompetitiveRent : 'NULL';
	}

	public function setAvgCompetitiveRent( $fltAvgCompetitiveRent ) {
		$this->set( 'm_fltAvgCompetitiveRent', CStrings::strToFloatDef( $fltAvgCompetitiveRent, NULL, false, 2 ) );
	}

	public function getAvgCompetitiveRent() {
		return $this->m_fltAvgCompetitiveRent;
	}

	public function sqlAvgCompetitiveRent() {
		return ( true == isset( $this->m_fltAvgCompetitiveRent ) ) ? ( string ) $this->m_fltAvgCompetitiveRent : 'NULL';
	}

	public function setBaseRent( $fltBaseRent ) {
		$this->set( 'm_fltBaseRent', CStrings::strToFloatDef( $fltBaseRent, NULL, false, 2 ) );
	}

	public function getBaseRent() {
		return $this->m_fltBaseRent;
	}

	public function sqlBaseRent() {
		return ( true == isset( $this->m_fltBaseRent ) ) ? ( string ) $this->m_fltBaseRent : 'NULL';
	}

	public function setOptimalBaseRent( $fltOptimalBaseRent ) {
		$this->set( 'm_fltOptimalBaseRent', CStrings::strToFloatDef( $fltOptimalBaseRent, NULL, false, 2 ) );
	}

	public function getOptimalBaseRent() {
		return $this->m_fltOptimalBaseRent;
	}

	public function sqlOptimalBaseRent() {
		return ( true == isset( $this->m_fltOptimalBaseRent ) ) ? ( string ) $this->m_fltOptimalBaseRent : 'NULL';
	}

	public function setNoticePeriodAdjustment( $fltNoticePeriodAdjustment ) {
		$this->set( 'm_fltNoticePeriodAdjustment', CStrings::strToFloatDef( $fltNoticePeriodAdjustment, NULL, false, 2 ) );
	}

	public function getNoticePeriodAdjustment() {
		return $this->m_fltNoticePeriodAdjustment;
	}

	public function sqlNoticePeriodAdjustment() {
		return ( true == isset( $this->m_fltNoticePeriodAdjustment ) ) ? ( string ) $this->m_fltNoticePeriodAdjustment : 'NULL';
	}

	public function setStalenessPeriodAdjustment( $fltStalenessPeriodAdjustment ) {
		$this->set( 'm_fltStalenessPeriodAdjustment', CStrings::strToFloatDef( $fltStalenessPeriodAdjustment, NULL, false, 2 ) );
	}

	public function getStalenessPeriodAdjustment() {
		return $this->m_fltStalenessPeriodAdjustment;
	}

	public function sqlStalenessPeriodAdjustment() {
		return ( true == isset( $this->m_fltStalenessPeriodAdjustment ) ) ? ( string ) $this->m_fltStalenessPeriodAdjustment : 'NULL';
	}

	public function setMinimumLeasedAdjustment( $fltMinimumLeasedAdjustment ) {
		$this->set( 'm_fltMinimumLeasedAdjustment', CStrings::strToFloatDef( $fltMinimumLeasedAdjustment, NULL, false, 2 ) );
	}

	public function getMinimumLeasedAdjustment() {
		return $this->m_fltMinimumLeasedAdjustment;
	}

	public function sqlMinimumLeasedAdjustment() {
		return ( true == isset( $this->m_fltMinimumLeasedAdjustment ) ) ? ( string ) $this->m_fltMinimumLeasedAdjustment : 'NULL';
	}

	public function setSustainableOccupancyAdjustment( $fltSustainableOccupancyAdjustment ) {
		$this->set( 'm_fltSustainableOccupancyAdjustment', CStrings::strToFloatDef( $fltSustainableOccupancyAdjustment, NULL, false, 2 ) );
	}

	public function getSustainableOccupancyAdjustment() {
		return $this->m_fltSustainableOccupancyAdjustment;
	}

	public function sqlSustainableOccupancyAdjustment() {
		return ( true == isset( $this->m_fltSustainableOccupancyAdjustment ) ) ? ( string ) $this->m_fltSustainableOccupancyAdjustment : 'NULL';
	}

	public function setOptimalOccupancyRent( $fltOptimalOccupancyRent ) {
		$this->set( 'm_fltOptimalOccupancyRent', CStrings::strToFloatDef( $fltOptimalOccupancyRent, NULL, false, 2 ) );
	}

	public function getOptimalOccupancyRent() {
		return $this->m_fltOptimalOccupancyRent;
	}

	public function sqlOptimalOccupancyRent() {
		return ( true == isset( $this->m_fltOptimalOccupancyRent ) ) ? ( string ) $this->m_fltOptimalOccupancyRent : 'NULL';
	}

	public function setOptimalOccupancyRentConstrained( $fltOptimalOccupancyRentConstrained ) {
		$this->set( 'm_fltOptimalOccupancyRentConstrained', CStrings::strToFloatDef( $fltOptimalOccupancyRentConstrained, NULL, false, 2 ) );
	}

	public function getOptimalOccupancyRentConstrained() {
		return $this->m_fltOptimalOccupancyRentConstrained;
	}

	public function sqlOptimalOccupancyRentConstrained() {
		return ( true == isset( $this->m_fltOptimalOccupancyRentConstrained ) ) ? ( string ) $this->m_fltOptimalOccupancyRentConstrained : 'NULL';
	}

	public function setOptimalAdvertisedRent( $fltOptimalAdvertisedRent ) {
		$this->set( 'm_fltOptimalAdvertisedRent', CStrings::strToFloatDef( $fltOptimalAdvertisedRent, NULL, false, 2 ) );
	}

	public function getOptimalAdvertisedRent() {
		return $this->m_fltOptimalAdvertisedRent;
	}

	public function sqlOptimalAdvertisedRent() {
		return ( true == isset( $this->m_fltOptimalAdvertisedRent ) ) ? ( string ) $this->m_fltOptimalAdvertisedRent : 'NULL';
	}

	public function setAvailableUnitCount( $intAvailableUnitCount ) {
		$this->set( 'm_intAvailableUnitCount', CStrings::strToIntDef( $intAvailableUnitCount, NULL, false ) );
	}

	public function getAvailableUnitCount() {
		return $this->m_intAvailableUnitCount;
	}

	public function sqlAvailableUnitCount() {
		return ( true == isset( $this->m_intAvailableUnitCount ) ) ? ( string ) $this->m_intAvailableUnitCount : 'NULL';
	}

	public function setOccupiedUnitCount( $intOccupiedUnitCount ) {
		$this->set( 'm_intOccupiedUnitCount', CStrings::strToIntDef( $intOccupiedUnitCount, NULL, false ) );
	}

	public function getOccupiedUnitCount() {
		return $this->m_intOccupiedUnitCount;
	}

	public function sqlOccupiedUnitCount() {
		return ( true == isset( $this->m_intOccupiedUnitCount ) ) ? ( string ) $this->m_intOccupiedUnitCount : 'NULL';
	}

	public function setRentableUnitCount( $intRentableUnitCount ) {
		$this->set( 'm_intRentableUnitCount', CStrings::strToIntDef( $intRentableUnitCount, NULL, false ) );
	}

	public function getRentableUnitCount() {
		return $this->m_intRentableUnitCount;
	}

	public function sqlRentableUnitCount() {
		return ( true == isset( $this->m_intRentableUnitCount ) ) ? ( string ) $this->m_intRentableUnitCount : 'NULL';
	}

	public function setExposed30UnitCount( $fltExposed30UnitCount ) {
		$this->set( 'm_fltExposed30UnitCount', CStrings::strToFloatDef( $fltExposed30UnitCount, NULL, false, 4 ) );
	}

	public function getExposed30UnitCount() {
		return $this->m_fltExposed30UnitCount;
	}

	public function sqlExposed30UnitCount() {
		return ( true == isset( $this->m_fltExposed30UnitCount ) ) ? ( string ) $this->m_fltExposed30UnitCount : 'NULL';
	}

	public function setExposed60UnitCount( $fltExposed60UnitCount ) {
		$this->set( 'm_fltExposed60UnitCount', CStrings::strToFloatDef( $fltExposed60UnitCount, NULL, false, 4 ) );
	}

	public function getExposed60UnitCount() {
		return $this->m_fltExposed60UnitCount;
	}

	public function sqlExposed60UnitCount() {
		return ( true == isset( $this->m_fltExposed60UnitCount ) ) ? ( string ) $this->m_fltExposed60UnitCount : 'NULL';
	}

	public function setExposed90UnitCount( $fltExposed90UnitCount ) {
		$this->set( 'm_fltExposed90UnitCount', CStrings::strToFloatDef( $fltExposed90UnitCount, NULL, false, 4 ) );
	}

	public function getExposed90UnitCount() {
		return $this->m_fltExposed90UnitCount;
	}

	public function sqlExposed90UnitCount() {
		return ( true == isset( $this->m_fltExposed90UnitCount ) ) ? ( string ) $this->m_fltExposed90UnitCount : 'NULL';
	}

	public function setDemandThirtyUnitCount( $fltDemandThirtyUnitCount ) {
		$this->set( 'm_fltDemandThirtyUnitCount', CStrings::strToFloatDef( $fltDemandThirtyUnitCount, NULL, false, 4 ) );
	}

	public function getDemandThirtyUnitCount() {
		return $this->m_fltDemandThirtyUnitCount;
	}

	public function sqlDemandThirtyUnitCount() {
		return ( true == isset( $this->m_fltDemandThirtyUnitCount ) ) ? ( string ) $this->m_fltDemandThirtyUnitCount : 'NULL';
	}

	public function setDemandSixtyUnitCount( $fltDemandSixtyUnitCount ) {
		$this->set( 'm_fltDemandSixtyUnitCount', CStrings::strToFloatDef( $fltDemandSixtyUnitCount, NULL, false, 4 ) );
	}

	public function getDemandSixtyUnitCount() {
		return $this->m_fltDemandSixtyUnitCount;
	}

	public function sqlDemandSixtyUnitCount() {
		return ( true == isset( $this->m_fltDemandSixtyUnitCount ) ) ? ( string ) $this->m_fltDemandSixtyUnitCount : 'NULL';
	}

	public function setDemandNinetyUnitCount( $fltDemandNinetyUnitCount ) {
		$this->set( 'm_fltDemandNinetyUnitCount', CStrings::strToFloatDef( $fltDemandNinetyUnitCount, NULL, false, 4 ) );
	}

	public function getDemandNinetyUnitCount() {
		return $this->m_fltDemandNinetyUnitCount;
	}

	public function sqlDemandNinetyUnitCount() {
		return ( true == isset( $this->m_fltDemandNinetyUnitCount ) ) ? ( string ) $this->m_fltDemandNinetyUnitCount : 'NULL';
	}

	public function setWeeklyGrowthRate( $fltWeeklyGrowthRate ) {
		$this->set( 'm_fltWeeklyGrowthRate', CStrings::strToFloatDef( $fltWeeklyGrowthRate, NULL, false, 2 ) );
	}

	public function getWeeklyGrowthRate() {
		return $this->m_fltWeeklyGrowthRate;
	}

	public function sqlWeeklyGrowthRate() {
		return ( true == isset( $this->m_fltWeeklyGrowthRate ) ) ? ( string ) $this->m_fltWeeklyGrowthRate : 'NULL';
	}

	public function setMonthlyGrowthRate( $fltMonthlyGrowthRate ) {
		$this->set( 'm_fltMonthlyGrowthRate', CStrings::strToFloatDef( $fltMonthlyGrowthRate, NULL, false, 2 ) );
	}

	public function getMonthlyGrowthRate() {
		return $this->m_fltMonthlyGrowthRate;
	}

	public function sqlMonthlyGrowthRate() {
		return ( true == isset( $this->m_fltMonthlyGrowthRate ) ) ? ( string ) $this->m_fltMonthlyGrowthRate : 'NULL';
	}

	public function setQuarterlyGrowthRate( $fltQuarterlyGrowthRate ) {
		$this->set( 'm_fltQuarterlyGrowthRate', CStrings::strToFloatDef( $fltQuarterlyGrowthRate, NULL, false, 2 ) );
	}

	public function getQuarterlyGrowthRate() {
		return $this->m_fltQuarterlyGrowthRate;
	}

	public function sqlQuarterlyGrowthRate() {
		return ( true == isset( $this->m_fltQuarterlyGrowthRate ) ) ? ( string ) $this->m_fltQuarterlyGrowthRate : 'NULL';
	}

	public function setYearlyGrowthRate( $fltYearlyGrowthRate ) {
		$this->set( 'm_fltYearlyGrowthRate', CStrings::strToFloatDef( $fltYearlyGrowthRate, NULL, false, 2 ) );
	}

	public function getYearlyGrowthRate() {
		return $this->m_fltYearlyGrowthRate;
	}

	public function sqlYearlyGrowthRate() {
		return ( true == isset( $this->m_fltYearlyGrowthRate ) ) ? ( string ) $this->m_fltYearlyGrowthRate : 'NULL';
	}

	public function setFirstMoveInDate( $strFirstMoveInDate ) {
		$this->set( 'm_strFirstMoveInDate', CStrings::strTrimDef( $strFirstMoveInDate, -1, NULL, true ) );
	}

	public function getFirstMoveInDate() {
		return $this->m_strFirstMoveInDate;
	}

	public function sqlFirstMoveInDate() {
		return ( true == isset( $this->m_strFirstMoveInDate ) ) ? '\'' . $this->m_strFirstMoveInDate . '\'' : 'NULL';
	}

	public function setMatrix( $jsonMatrix ) {
		if( true == valObj( $jsonMatrix, 'stdClass' ) ) {
			$this->set( 'm_jsonMatrix', $jsonMatrix );
		} elseif( true == valJsonString( $jsonMatrix ) ) {
			$this->set( 'm_jsonMatrix', CStrings::strToJson( $jsonMatrix ) );
		} else {
			$this->set( 'm_jsonMatrix', NULL ); 
		}
		unset( $this->m_strMatrix );
	}

	public function getMatrix() {
		if( true == isset( $this->m_strMatrix ) ) {
			$this->m_jsonMatrix = CStrings::strToJson( $this->m_strMatrix );
			unset( $this->m_strMatrix );
		}
		return $this->m_jsonMatrix;
	}

	public function sqlMatrix() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getMatrix() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getMatrix() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setStdVacancyLossAdjustment( $fltStdVacancyLossAdjustment ) {
		$this->set( 'm_fltStdVacancyLossAdjustment', CStrings::strToFloatDef( $fltStdVacancyLossAdjustment, NULL, false, 2 ) );
	}

	public function getStdVacancyLossAdjustment() {
		return $this->m_fltStdVacancyLossAdjustment;
	}

	public function sqlStdVacancyLossAdjustment() {
		return ( true == isset( $this->m_fltStdVacancyLossAdjustment ) ) ? ( string ) $this->m_fltStdVacancyLossAdjustment : 'NULL';
	}

	public function setStdTurnCostAdjustment( $fltStdTurnCostAdjustment ) {
		$this->set( 'm_fltStdTurnCostAdjustment', CStrings::strToFloatDef( $fltStdTurnCostAdjustment, NULL, false, 2 ) );
	}

	public function getStdTurnCostAdjustment() {
		return $this->m_fltStdTurnCostAdjustment;
	}

	public function sqlStdTurnCostAdjustment() {
		return ( true == isset( $this->m_fltStdTurnCostAdjustment ) ) ? ( string ) $this->m_fltStdTurnCostAdjustment : 'NULL';
	}

	public function setStdExpirationAdjustment( $fltStdExpirationAdjustment ) {
		$this->set( 'm_fltStdExpirationAdjustment', CStrings::strToFloatDef( $fltStdExpirationAdjustment, NULL, false, 2 ) );
	}

	public function getStdExpirationAdjustment() {
		return $this->m_fltStdExpirationAdjustment;
	}

	public function sqlStdExpirationAdjustment() {
		return ( true == isset( $this->m_fltStdExpirationAdjustment ) ) ? ( string ) $this->m_fltStdExpirationAdjustment : 'NULL';
	}

	public function setOptimalAdvertisedRentUnconstrained( $fltOptimalAdvertisedRentUnconstrained ) {
		$this->set( 'm_fltOptimalAdvertisedRentUnconstrained', CStrings::strToFloatDef( $fltOptimalAdvertisedRentUnconstrained, NULL, false, 2 ) );
	}

	public function getOptimalAdvertisedRentUnconstrained() {
		return $this->m_fltOptimalAdvertisedRentUnconstrained;
	}

	public function sqlOptimalAdvertisedRentUnconstrained() {
		return ( true == isset( $this->m_fltOptimalAdvertisedRentUnconstrained ) ) ? ( string ) $this->m_fltOptimalAdvertisedRentUnconstrained : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, unit_type_id, property_unit_id, unit_space_id, min_executed_rent, max_executed_rent, avg_executed_rent, last_executed_rent, min_advertised_rent, max_advertised_rent, avg_advertised_rent, std_advertised_rent, min_optimal_rent, max_optimal_rent, avg_optimal_rent, std_optimal_rent, min_competitive_rent, max_competitive_rent, avg_competitive_rent, base_rent, optimal_base_rent, notice_period_adjustment, staleness_period_adjustment, minimum_leased_adjustment, sustainable_occupancy_adjustment, optimal_occupancy_rent, optimal_occupancy_rent_constrained, optimal_advertised_rent, available_unit_count, occupied_unit_count, rentable_unit_count, exposed_30_unit_count, exposed_60_unit_count, exposed_90_unit_count, demand_thirty_unit_count, demand_sixty_unit_count, demand_ninety_unit_count, weekly_growth_rate, monthly_growth_rate, quarterly_growth_rate, yearly_growth_rate, first_move_in_date, matrix, updated_by, updated_on, created_by, created_on, std_vacancy_loss_adjustment, std_turn_cost_adjustment, std_expiration_adjustment, optimal_advertised_rent_unconstrained )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlUnitTypeId() . ', ' .
 						$this->sqlPropertyUnitId() . ', ' .
 						$this->sqlUnitSpaceId() . ', ' .
 						$this->sqlMinExecutedRent() . ', ' .
 						$this->sqlMaxExecutedRent() . ', ' .
 						$this->sqlAvgExecutedRent() . ', ' .
 						$this->sqlLastExecutedRent() . ', ' .
 						$this->sqlMinAdvertisedRent() . ', ' .
 						$this->sqlMaxAdvertisedRent() . ', ' .
 						$this->sqlAvgAdvertisedRent() . ', ' .
 						$this->sqlStdAdvertisedRent() . ', ' .
 						$this->sqlMinOptimalRent() . ', ' .
 						$this->sqlMaxOptimalRent() . ', ' .
 						$this->sqlAvgOptimalRent() . ', ' .
 						$this->sqlStdOptimalRent() . ', ' .
 						$this->sqlMinCompetitiveRent() . ', ' .
 						$this->sqlMaxCompetitiveRent() . ', ' .
 						$this->sqlAvgCompetitiveRent() . ', ' .
 						$this->sqlBaseRent() . ', ' .
 						$this->sqlOptimalBaseRent() . ', ' .
 						$this->sqlNoticePeriodAdjustment() . ', ' .
 						$this->sqlStalenessPeriodAdjustment() . ', ' .
 						$this->sqlMinimumLeasedAdjustment() . ', ' .
 						$this->sqlSustainableOccupancyAdjustment() . ', ' .
 						$this->sqlOptimalOccupancyRent() . ', ' .
 						$this->sqlOptimalOccupancyRentConstrained() . ', ' .
 						$this->sqlOptimalAdvertisedRent() . ', ' .
 						$this->sqlAvailableUnitCount() . ', ' .
 						$this->sqlOccupiedUnitCount() . ', ' .
 						$this->sqlRentableUnitCount() . ', ' .
 						$this->sqlExposed30UnitCount() . ', ' .
 						$this->sqlExposed60UnitCount() . ', ' .
 						$this->sqlExposed90UnitCount() . ', ' .
 						$this->sqlDemandThirtyUnitCount() . ', ' .
 						$this->sqlDemandSixtyUnitCount() . ', ' .
 						$this->sqlDemandNinetyUnitCount() . ', ' .
 						$this->sqlWeeklyGrowthRate() . ', ' .
 						$this->sqlMonthlyGrowthRate() . ', ' .
 						$this->sqlQuarterlyGrowthRate() . ', ' .
 						$this->sqlYearlyGrowthRate() . ', ' .
 						$this->sqlFirstMoveInDate() . ', ' .
 						$this->sqlMatrix() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlStdVacancyLossAdjustment() . ', ' .
 						$this->sqlStdTurnCostAdjustment() . ', ' .
 						$this->sqlStdExpirationAdjustment() . ', ' .
 						$this->sqlOptimalAdvertisedRentUnconstrained() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_executed_rent = ' . $this->sqlMinExecutedRent() . ','; } elseif( true == array_key_exists( 'MinExecutedRent', $this->getChangedColumns() ) ) { $strSql .= ' min_executed_rent = ' . $this->sqlMinExecutedRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_executed_rent = ' . $this->sqlMaxExecutedRent() . ','; } elseif( true == array_key_exists( 'MaxExecutedRent', $this->getChangedColumns() ) ) { $strSql .= ' max_executed_rent = ' . $this->sqlMaxExecutedRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_executed_rent = ' . $this->sqlAvgExecutedRent() . ','; } elseif( true == array_key_exists( 'AvgExecutedRent', $this->getChangedColumns() ) ) { $strSql .= ' avg_executed_rent = ' . $this->sqlAvgExecutedRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_executed_rent = ' . $this->sqlLastExecutedRent() . ','; } elseif( true == array_key_exists( 'LastExecutedRent', $this->getChangedColumns() ) ) { $strSql .= ' last_executed_rent = ' . $this->sqlLastExecutedRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_advertised_rent = ' . $this->sqlMinAdvertisedRent() . ','; } elseif( true == array_key_exists( 'MinAdvertisedRent', $this->getChangedColumns() ) ) { $strSql .= ' min_advertised_rent = ' . $this->sqlMinAdvertisedRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_advertised_rent = ' . $this->sqlMaxAdvertisedRent() . ','; } elseif( true == array_key_exists( 'MaxAdvertisedRent', $this->getChangedColumns() ) ) { $strSql .= ' max_advertised_rent = ' . $this->sqlMaxAdvertisedRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_advertised_rent = ' . $this->sqlAvgAdvertisedRent() . ','; } elseif( true == array_key_exists( 'AvgAdvertisedRent', $this->getChangedColumns() ) ) { $strSql .= ' avg_advertised_rent = ' . $this->sqlAvgAdvertisedRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' std_advertised_rent = ' . $this->sqlStdAdvertisedRent() . ','; } elseif( true == array_key_exists( 'StdAdvertisedRent', $this->getChangedColumns() ) ) { $strSql .= ' std_advertised_rent = ' . $this->sqlStdAdvertisedRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_optimal_rent = ' . $this->sqlMinOptimalRent() . ','; } elseif( true == array_key_exists( 'MinOptimalRent', $this->getChangedColumns() ) ) { $strSql .= ' min_optimal_rent = ' . $this->sqlMinOptimalRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_optimal_rent = ' . $this->sqlMaxOptimalRent() . ','; } elseif( true == array_key_exists( 'MaxOptimalRent', $this->getChangedColumns() ) ) { $strSql .= ' max_optimal_rent = ' . $this->sqlMaxOptimalRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_optimal_rent = ' . $this->sqlAvgOptimalRent() . ','; } elseif( true == array_key_exists( 'AvgOptimalRent', $this->getChangedColumns() ) ) { $strSql .= ' avg_optimal_rent = ' . $this->sqlAvgOptimalRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' std_optimal_rent = ' . $this->sqlStdOptimalRent() . ','; } elseif( true == array_key_exists( 'StdOptimalRent', $this->getChangedColumns() ) ) { $strSql .= ' std_optimal_rent = ' . $this->sqlStdOptimalRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_competitive_rent = ' . $this->sqlMinCompetitiveRent() . ','; } elseif( true == array_key_exists( 'MinCompetitiveRent', $this->getChangedColumns() ) ) { $strSql .= ' min_competitive_rent = ' . $this->sqlMinCompetitiveRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_competitive_rent = ' . $this->sqlMaxCompetitiveRent() . ','; } elseif( true == array_key_exists( 'MaxCompetitiveRent', $this->getChangedColumns() ) ) { $strSql .= ' max_competitive_rent = ' . $this->sqlMaxCompetitiveRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_competitive_rent = ' . $this->sqlAvgCompetitiveRent() . ','; } elseif( true == array_key_exists( 'AvgCompetitiveRent', $this->getChangedColumns() ) ) { $strSql .= ' avg_competitive_rent = ' . $this->sqlAvgCompetitiveRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent() . ','; } elseif( true == array_key_exists( 'BaseRent', $this->getChangedColumns() ) ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' optimal_base_rent = ' . $this->sqlOptimalBaseRent() . ','; } elseif( true == array_key_exists( 'OptimalBaseRent', $this->getChangedColumns() ) ) { $strSql .= ' optimal_base_rent = ' . $this->sqlOptimalBaseRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notice_period_adjustment = ' . $this->sqlNoticePeriodAdjustment() . ','; } elseif( true == array_key_exists( 'NoticePeriodAdjustment', $this->getChangedColumns() ) ) { $strSql .= ' notice_period_adjustment = ' . $this->sqlNoticePeriodAdjustment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' staleness_period_adjustment = ' . $this->sqlStalenessPeriodAdjustment() . ','; } elseif( true == array_key_exists( 'StalenessPeriodAdjustment', $this->getChangedColumns() ) ) { $strSql .= ' staleness_period_adjustment = ' . $this->sqlStalenessPeriodAdjustment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minimum_leased_adjustment = ' . $this->sqlMinimumLeasedAdjustment() . ','; } elseif( true == array_key_exists( 'MinimumLeasedAdjustment', $this->getChangedColumns() ) ) { $strSql .= ' minimum_leased_adjustment = ' . $this->sqlMinimumLeasedAdjustment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sustainable_occupancy_adjustment = ' . $this->sqlSustainableOccupancyAdjustment() . ','; } elseif( true == array_key_exists( 'SustainableOccupancyAdjustment', $this->getChangedColumns() ) ) { $strSql .= ' sustainable_occupancy_adjustment = ' . $this->sqlSustainableOccupancyAdjustment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' optimal_occupancy_rent = ' . $this->sqlOptimalOccupancyRent() . ','; } elseif( true == array_key_exists( 'OptimalOccupancyRent', $this->getChangedColumns() ) ) { $strSql .= ' optimal_occupancy_rent = ' . $this->sqlOptimalOccupancyRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' optimal_occupancy_rent_constrained = ' . $this->sqlOptimalOccupancyRentConstrained() . ','; } elseif( true == array_key_exists( 'OptimalOccupancyRentConstrained', $this->getChangedColumns() ) ) { $strSql .= ' optimal_occupancy_rent_constrained = ' . $this->sqlOptimalOccupancyRentConstrained() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' optimal_advertised_rent = ' . $this->sqlOptimalAdvertisedRent() . ','; } elseif( true == array_key_exists( 'OptimalAdvertisedRent', $this->getChangedColumns() ) ) { $strSql .= ' optimal_advertised_rent = ' . $this->sqlOptimalAdvertisedRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' available_unit_count = ' . $this->sqlAvailableUnitCount() . ','; } elseif( true == array_key_exists( 'AvailableUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' available_unit_count = ' . $this->sqlAvailableUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupied_unit_count = ' . $this->sqlOccupiedUnitCount() . ','; } elseif( true == array_key_exists( 'OccupiedUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' occupied_unit_count = ' . $this->sqlOccupiedUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rentable_unit_count = ' . $this->sqlRentableUnitCount() . ','; } elseif( true == array_key_exists( 'RentableUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' rentable_unit_count = ' . $this->sqlRentableUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exposed_30_unit_count = ' . $this->sqlExposed30UnitCount() . ','; } elseif( true == array_key_exists( 'Exposed30UnitCount', $this->getChangedColumns() ) ) { $strSql .= ' exposed_30_unit_count = ' . $this->sqlExposed30UnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exposed_60_unit_count = ' . $this->sqlExposed60UnitCount() . ','; } elseif( true == array_key_exists( 'Exposed60UnitCount', $this->getChangedColumns() ) ) { $strSql .= ' exposed_60_unit_count = ' . $this->sqlExposed60UnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exposed_90_unit_count = ' . $this->sqlExposed90UnitCount() . ','; } elseif( true == array_key_exists( 'Exposed90UnitCount', $this->getChangedColumns() ) ) { $strSql .= ' exposed_90_unit_count = ' . $this->sqlExposed90UnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' demand_thirty_unit_count = ' . $this->sqlDemandThirtyUnitCount() . ','; } elseif( true == array_key_exists( 'DemandThirtyUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' demand_thirty_unit_count = ' . $this->sqlDemandThirtyUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' demand_sixty_unit_count = ' . $this->sqlDemandSixtyUnitCount() . ','; } elseif( true == array_key_exists( 'DemandSixtyUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' demand_sixty_unit_count = ' . $this->sqlDemandSixtyUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' demand_ninety_unit_count = ' . $this->sqlDemandNinetyUnitCount() . ','; } elseif( true == array_key_exists( 'DemandNinetyUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' demand_ninety_unit_count = ' . $this->sqlDemandNinetyUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' weekly_growth_rate = ' . $this->sqlWeeklyGrowthRate() . ','; } elseif( true == array_key_exists( 'WeeklyGrowthRate', $this->getChangedColumns() ) ) { $strSql .= ' weekly_growth_rate = ' . $this->sqlWeeklyGrowthRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' monthly_growth_rate = ' . $this->sqlMonthlyGrowthRate() . ','; } elseif( true == array_key_exists( 'MonthlyGrowthRate', $this->getChangedColumns() ) ) { $strSql .= ' monthly_growth_rate = ' . $this->sqlMonthlyGrowthRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quarterly_growth_rate = ' . $this->sqlQuarterlyGrowthRate() . ','; } elseif( true == array_key_exists( 'QuarterlyGrowthRate', $this->getChangedColumns() ) ) { $strSql .= ' quarterly_growth_rate = ' . $this->sqlQuarterlyGrowthRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' yearly_growth_rate = ' . $this->sqlYearlyGrowthRate() . ','; } elseif( true == array_key_exists( 'YearlyGrowthRate', $this->getChangedColumns() ) ) { $strSql .= ' yearly_growth_rate = ' . $this->sqlYearlyGrowthRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_move_in_date = ' . $this->sqlFirstMoveInDate() . ','; } elseif( true == array_key_exists( 'FirstMoveInDate', $this->getChangedColumns() ) ) { $strSql .= ' first_move_in_date = ' . $this->sqlFirstMoveInDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' matrix = ' . $this->sqlMatrix() . ','; } elseif( true == array_key_exists( 'Matrix', $this->getChangedColumns() ) ) { $strSql .= ' matrix = ' . $this->sqlMatrix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' std_vacancy_loss_adjustment = ' . $this->sqlStdVacancyLossAdjustment() . ','; } elseif( true == array_key_exists( 'StdVacancyLossAdjustment', $this->getChangedColumns() ) ) { $strSql .= ' std_vacancy_loss_adjustment = ' . $this->sqlStdVacancyLossAdjustment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' std_turn_cost_adjustment = ' . $this->sqlStdTurnCostAdjustment() . ','; } elseif( true == array_key_exists( 'StdTurnCostAdjustment', $this->getChangedColumns() ) ) { $strSql .= ' std_turn_cost_adjustment = ' . $this->sqlStdTurnCostAdjustment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' std_expiration_adjustment = ' . $this->sqlStdExpirationAdjustment() . ','; } elseif( true == array_key_exists( 'StdExpirationAdjustment', $this->getChangedColumns() ) ) { $strSql .= ' std_expiration_adjustment = ' . $this->sqlStdExpirationAdjustment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' optimal_advertised_rent_unconstrained = ' . $this->sqlOptimalAdvertisedRentUnconstrained() . ','; } elseif( true == array_key_exists( 'OptimalAdvertisedRentUnconstrained', $this->getChangedColumns() ) ) { $strSql .= ' optimal_advertised_rent_unconstrained = ' . $this->sqlOptimalAdvertisedRentUnconstrained() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'min_executed_rent' => $this->getMinExecutedRent(),
			'max_executed_rent' => $this->getMaxExecutedRent(),
			'avg_executed_rent' => $this->getAvgExecutedRent(),
			'last_executed_rent' => $this->getLastExecutedRent(),
			'min_advertised_rent' => $this->getMinAdvertisedRent(),
			'max_advertised_rent' => $this->getMaxAdvertisedRent(),
			'avg_advertised_rent' => $this->getAvgAdvertisedRent(),
			'std_advertised_rent' => $this->getStdAdvertisedRent(),
			'min_optimal_rent' => $this->getMinOptimalRent(),
			'max_optimal_rent' => $this->getMaxOptimalRent(),
			'avg_optimal_rent' => $this->getAvgOptimalRent(),
			'std_optimal_rent' => $this->getStdOptimalRent(),
			'min_competitive_rent' => $this->getMinCompetitiveRent(),
			'max_competitive_rent' => $this->getMaxCompetitiveRent(),
			'avg_competitive_rent' => $this->getAvgCompetitiveRent(),
			'base_rent' => $this->getBaseRent(),
			'optimal_base_rent' => $this->getOptimalBaseRent(),
			'notice_period_adjustment' => $this->getNoticePeriodAdjustment(),
			'staleness_period_adjustment' => $this->getStalenessPeriodAdjustment(),
			'minimum_leased_adjustment' => $this->getMinimumLeasedAdjustment(),
			'sustainable_occupancy_adjustment' => $this->getSustainableOccupancyAdjustment(),
			'optimal_occupancy_rent' => $this->getOptimalOccupancyRent(),
			'optimal_occupancy_rent_constrained' => $this->getOptimalOccupancyRentConstrained(),
			'optimal_advertised_rent' => $this->getOptimalAdvertisedRent(),
			'available_unit_count' => $this->getAvailableUnitCount(),
			'occupied_unit_count' => $this->getOccupiedUnitCount(),
			'rentable_unit_count' => $this->getRentableUnitCount(),
			'exposed_30_unit_count' => $this->getExposed30UnitCount(),
			'exposed_60_unit_count' => $this->getExposed60UnitCount(),
			'exposed_90_unit_count' => $this->getExposed90UnitCount(),
			'demand_thirty_unit_count' => $this->getDemandThirtyUnitCount(),
			'demand_sixty_unit_count' => $this->getDemandSixtyUnitCount(),
			'demand_ninety_unit_count' => $this->getDemandNinetyUnitCount(),
			'weekly_growth_rate' => $this->getWeeklyGrowthRate(),
			'monthly_growth_rate' => $this->getMonthlyGrowthRate(),
			'quarterly_growth_rate' => $this->getQuarterlyGrowthRate(),
			'yearly_growth_rate' => $this->getYearlyGrowthRate(),
			'first_move_in_date' => $this->getFirstMoveInDate(),
			'matrix' => $this->getMatrix(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'std_vacancy_loss_adjustment' => $this->getStdVacancyLossAdjustment(),
			'std_turn_cost_adjustment' => $this->getStdTurnCostAdjustment(),
			'std_expiration_adjustment' => $this->getStdExpirationAdjustment(),
			'optimal_advertised_rent_unconstrained' => $this->getOptimalAdvertisedRentUnconstrained()
		);
	}

}
?>