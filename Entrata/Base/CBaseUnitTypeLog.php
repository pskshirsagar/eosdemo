<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitTypeLog extends CEosSingularBase {

	const TABLE_NAME = 'public.unit_type_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyFloorplanId;
	protected $m_intOptimizationStructureId;
	protected $m_intUnitExclusionReasonTypeId;
	protected $m_intUnitTypeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_fltBaseRent;
	protected $m_fltCompetitiveGroupRentAmount;
	protected $m_fltCompetitiveMarketRentAmount;
	protected $m_fltCompetitiveRegionalRentAmount;
	protected $m_fltExecutedRentAmount;
	protected $m_fltAdvertisedRentAmount;
	protected $m_fltAdvertisedOtherRentAmount;
	protected $m_fltAdvertisedDepositAmount;
	protected $m_fltAdvertisedSpecialsAmount;
	protected $m_fltAdvertisedRentableItemsAmount;
	protected $m_fltAdvertisedAmenitiesAmount;
	protected $m_fltAdvertisedLiabilityAmount;
	protected $m_fltAdvertisedTaxAmount;
	protected $m_fltAdvertisedOtherAmount;
	protected $m_fltMinSquareFeet;
	protected $m_fltMaxSquareFeet;
	protected $m_intNumberOfUnits;
	protected $m_intNumberOfFloors;
	protected $m_intNumberOfRooms;
	protected $m_intNumberOfBedrooms;
	protected $m_intNumberOfBathrooms;
	protected $m_strBedroomDescription;
	protected $m_strBathroomDescription;
	protected $m_strAppliancesDescription;
	protected $m_intMaximumOccupants;
	protected $m_intDefaultLeaseTermMonths;
	protected $m_intBlockIntegration;
	protected $m_intBlockPricingIntegration;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_strImportedOn;
	protected $m_strExportedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltBaseRent = '0';
		$this->m_fltCompetitiveGroupRentAmount = '0';
		$this->m_fltCompetitiveMarketRentAmount = '0';
		$this->m_fltCompetitiveRegionalRentAmount = '0';
		$this->m_fltExecutedRentAmount = '0';
		$this->m_fltAdvertisedRentAmount = '0';
		$this->m_fltAdvertisedOtherRentAmount = '0';
		$this->m_fltAdvertisedDepositAmount = '0';
		$this->m_fltAdvertisedSpecialsAmount = '0';
		$this->m_fltAdvertisedRentableItemsAmount = '0';
		$this->m_fltAdvertisedAmenitiesAmount = '0';
		$this->m_fltAdvertisedLiabilityAmount = '0';
		$this->m_fltAdvertisedTaxAmount = '0';
		$this->m_fltAdvertisedOtherAmount = '0';
		$this->m_intNumberOfUnits = '0';
		$this->m_intNumberOfFloors = '0';
		$this->m_intBlockIntegration = '0';
		$this->m_intBlockPricingIntegration = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorplanId', trim( $arrValues['property_floorplan_id'] ) ); elseif( isset( $arrValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrValues['property_floorplan_id'] );
		if( isset( $arrValues['optimization_structure_id'] ) && $boolDirectSet ) $this->set( 'm_intOptimizationStructureId', trim( $arrValues['optimization_structure_id'] ) ); elseif( isset( $arrValues['optimization_structure_id'] ) ) $this->setOptimizationStructureId( $arrValues['optimization_structure_id'] );
		if( isset( $arrValues['unit_exclusion_reason_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitExclusionReasonTypeId', trim( $arrValues['unit_exclusion_reason_type_id'] ) ); elseif( isset( $arrValues['unit_exclusion_reason_type_id'] ) ) $this->setUnitExclusionReasonTypeId( $arrValues['unit_exclusion_reason_type_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['base_rent'] ) && $boolDirectSet ) $this->set( 'm_fltBaseRent', trim( $arrValues['base_rent'] ) ); elseif( isset( $arrValues['base_rent'] ) ) $this->setBaseRent( $arrValues['base_rent'] );
		if( isset( $arrValues['competitive_group_rent_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCompetitiveGroupRentAmount', trim( $arrValues['competitive_group_rent_amount'] ) ); elseif( isset( $arrValues['competitive_group_rent_amount'] ) ) $this->setCompetitiveGroupRentAmount( $arrValues['competitive_group_rent_amount'] );
		if( isset( $arrValues['competitive_market_rent_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCompetitiveMarketRentAmount', trim( $arrValues['competitive_market_rent_amount'] ) ); elseif( isset( $arrValues['competitive_market_rent_amount'] ) ) $this->setCompetitiveMarketRentAmount( $arrValues['competitive_market_rent_amount'] );
		if( isset( $arrValues['competitive_regional_rent_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCompetitiveRegionalRentAmount', trim( $arrValues['competitive_regional_rent_amount'] ) ); elseif( isset( $arrValues['competitive_regional_rent_amount'] ) ) $this->setCompetitiveRegionalRentAmount( $arrValues['competitive_regional_rent_amount'] );
		if( isset( $arrValues['executed_rent_amount'] ) && $boolDirectSet ) $this->set( 'm_fltExecutedRentAmount', trim( $arrValues['executed_rent_amount'] ) ); elseif( isset( $arrValues['executed_rent_amount'] ) ) $this->setExecutedRentAmount( $arrValues['executed_rent_amount'] );
		if( isset( $arrValues['advertised_rent_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedRentAmount', trim( $arrValues['advertised_rent_amount'] ) ); elseif( isset( $arrValues['advertised_rent_amount'] ) ) $this->setAdvertisedRentAmount( $arrValues['advertised_rent_amount'] );
		if( isset( $arrValues['advertised_other_rent_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedOtherRentAmount', trim( $arrValues['advertised_other_rent_amount'] ) ); elseif( isset( $arrValues['advertised_other_rent_amount'] ) ) $this->setAdvertisedOtherRentAmount( $arrValues['advertised_other_rent_amount'] );
		if( isset( $arrValues['advertised_deposit_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedDepositAmount', trim( $arrValues['advertised_deposit_amount'] ) ); elseif( isset( $arrValues['advertised_deposit_amount'] ) ) $this->setAdvertisedDepositAmount( $arrValues['advertised_deposit_amount'] );
		if( isset( $arrValues['advertised_specials_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedSpecialsAmount', trim( $arrValues['advertised_specials_amount'] ) ); elseif( isset( $arrValues['advertised_specials_amount'] ) ) $this->setAdvertisedSpecialsAmount( $arrValues['advertised_specials_amount'] );
		if( isset( $arrValues['advertised_rentable_items_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedRentableItemsAmount', trim( $arrValues['advertised_rentable_items_amount'] ) ); elseif( isset( $arrValues['advertised_rentable_items_amount'] ) ) $this->setAdvertisedRentableItemsAmount( $arrValues['advertised_rentable_items_amount'] );
		if( isset( $arrValues['advertised_amenities_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedAmenitiesAmount', trim( $arrValues['advertised_amenities_amount'] ) ); elseif( isset( $arrValues['advertised_amenities_amount'] ) ) $this->setAdvertisedAmenitiesAmount( $arrValues['advertised_amenities_amount'] );
		if( isset( $arrValues['advertised_liability_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedLiabilityAmount', trim( $arrValues['advertised_liability_amount'] ) ); elseif( isset( $arrValues['advertised_liability_amount'] ) ) $this->setAdvertisedLiabilityAmount( $arrValues['advertised_liability_amount'] );
		if( isset( $arrValues['advertised_tax_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedTaxAmount', trim( $arrValues['advertised_tax_amount'] ) ); elseif( isset( $arrValues['advertised_tax_amount'] ) ) $this->setAdvertisedTaxAmount( $arrValues['advertised_tax_amount'] );
		if( isset( $arrValues['advertised_other_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedOtherAmount', trim( $arrValues['advertised_other_amount'] ) ); elseif( isset( $arrValues['advertised_other_amount'] ) ) $this->setAdvertisedOtherAmount( $arrValues['advertised_other_amount'] );
		if( isset( $arrValues['min_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltMinSquareFeet', trim( $arrValues['min_square_feet'] ) ); elseif( isset( $arrValues['min_square_feet'] ) ) $this->setMinSquareFeet( $arrValues['min_square_feet'] );
		if( isset( $arrValues['max_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltMaxSquareFeet', trim( $arrValues['max_square_feet'] ) ); elseif( isset( $arrValues['max_square_feet'] ) ) $this->setMaxSquareFeet( $arrValues['max_square_feet'] );
		if( isset( $arrValues['number_of_units'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfUnits', trim( $arrValues['number_of_units'] ) ); elseif( isset( $arrValues['number_of_units'] ) ) $this->setNumberOfUnits( $arrValues['number_of_units'] );
		if( isset( $arrValues['number_of_floors'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfFloors', trim( $arrValues['number_of_floors'] ) ); elseif( isset( $arrValues['number_of_floors'] ) ) $this->setNumberOfFloors( $arrValues['number_of_floors'] );
		if( isset( $arrValues['number_of_rooms'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfRooms', trim( $arrValues['number_of_rooms'] ) ); elseif( isset( $arrValues['number_of_rooms'] ) ) $this->setNumberOfRooms( $arrValues['number_of_rooms'] );
		if( isset( $arrValues['number_of_bedrooms'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfBedrooms', trim( $arrValues['number_of_bedrooms'] ) ); elseif( isset( $arrValues['number_of_bedrooms'] ) ) $this->setNumberOfBedrooms( $arrValues['number_of_bedrooms'] );
		if( isset( $arrValues['number_of_bathrooms'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfBathrooms', trim( $arrValues['number_of_bathrooms'] ) ); elseif( isset( $arrValues['number_of_bathrooms'] ) ) $this->setNumberOfBathrooms( $arrValues['number_of_bathrooms'] );
		if( isset( $arrValues['bedroom_description'] ) && $boolDirectSet ) $this->set( 'm_strBedroomDescription', trim( stripcslashes( $arrValues['bedroom_description'] ) ) ); elseif( isset( $arrValues['bedroom_description'] ) ) $this->setBedroomDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bedroom_description'] ) : $arrValues['bedroom_description'] );
		if( isset( $arrValues['bathroom_description'] ) && $boolDirectSet ) $this->set( 'm_strBathroomDescription', trim( stripcslashes( $arrValues['bathroom_description'] ) ) ); elseif( isset( $arrValues['bathroom_description'] ) ) $this->setBathroomDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bathroom_description'] ) : $arrValues['bathroom_description'] );
		if( isset( $arrValues['appliances_description'] ) && $boolDirectSet ) $this->set( 'm_strAppliancesDescription', trim( stripcslashes( $arrValues['appliances_description'] ) ) ); elseif( isset( $arrValues['appliances_description'] ) ) $this->setAppliancesDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['appliances_description'] ) : $arrValues['appliances_description'] );
		if( isset( $arrValues['maximum_occupants'] ) && $boolDirectSet ) $this->set( 'm_intMaximumOccupants', trim( $arrValues['maximum_occupants'] ) ); elseif( isset( $arrValues['maximum_occupants'] ) ) $this->setMaximumOccupants( $arrValues['maximum_occupants'] );
		if( isset( $arrValues['default_lease_term_months'] ) && $boolDirectSet ) $this->set( 'm_intDefaultLeaseTermMonths', trim( $arrValues['default_lease_term_months'] ) ); elseif( isset( $arrValues['default_lease_term_months'] ) ) $this->setDefaultLeaseTermMonths( $arrValues['default_lease_term_months'] );
		if( isset( $arrValues['block_integration'] ) && $boolDirectSet ) $this->set( 'm_intBlockIntegration', trim( $arrValues['block_integration'] ) ); elseif( isset( $arrValues['block_integration'] ) ) $this->setBlockIntegration( $arrValues['block_integration'] );
		if( isset( $arrValues['block_pricing_integration'] ) && $boolDirectSet ) $this->set( 'm_intBlockPricingIntegration', trim( $arrValues['block_pricing_integration'] ) ); elseif( isset( $arrValues['block_pricing_integration'] ) ) $this->setBlockPricingIntegration( $arrValues['block_pricing_integration'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['imported_on'] ) && $boolDirectSet ) $this->set( 'm_strImportedOn', trim( $arrValues['imported_on'] ) ); elseif( isset( $arrValues['imported_on'] ) ) $this->setImportedOn( $arrValues['imported_on'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->set( 'm_intPropertyFloorplanId', CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false ) );
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function sqlPropertyFloorplanId() {
		return ( true == isset( $this->m_intPropertyFloorplanId ) ) ? ( string ) $this->m_intPropertyFloorplanId : 'NULL';
	}

	public function setOptimizationStructureId( $intOptimizationStructureId ) {
		$this->set( 'm_intOptimizationStructureId', CStrings::strToIntDef( $intOptimizationStructureId, NULL, false ) );
	}

	public function getOptimizationStructureId() {
		return $this->m_intOptimizationStructureId;
	}

	public function sqlOptimizationStructureId() {
		return ( true == isset( $this->m_intOptimizationStructureId ) ) ? ( string ) $this->m_intOptimizationStructureId : 'NULL';
	}

	public function setUnitExclusionReasonTypeId( $intUnitExclusionReasonTypeId ) {
		$this->set( 'm_intUnitExclusionReasonTypeId', CStrings::strToIntDef( $intUnitExclusionReasonTypeId, NULL, false ) );
	}

	public function getUnitExclusionReasonTypeId() {
		return $this->m_intUnitExclusionReasonTypeId;
	}

	public function sqlUnitExclusionReasonTypeId() {
		return ( true == isset( $this->m_intUnitExclusionReasonTypeId ) ) ? ( string ) $this->m_intUnitExclusionReasonTypeId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setBaseRent( $fltBaseRent ) {
		$this->set( 'm_fltBaseRent', CStrings::strToFloatDef( $fltBaseRent, NULL, false, 2 ) );
	}

	public function getBaseRent() {
		return $this->m_fltBaseRent;
	}

	public function sqlBaseRent() {
		return ( true == isset( $this->m_fltBaseRent ) ) ? ( string ) $this->m_fltBaseRent : '0';
	}

	public function setCompetitiveGroupRentAmount( $fltCompetitiveGroupRentAmount ) {
		$this->set( 'm_fltCompetitiveGroupRentAmount', CStrings::strToFloatDef( $fltCompetitiveGroupRentAmount, NULL, false, 2 ) );
	}

	public function getCompetitiveGroupRentAmount() {
		return $this->m_fltCompetitiveGroupRentAmount;
	}

	public function sqlCompetitiveGroupRentAmount() {
		return ( true == isset( $this->m_fltCompetitiveGroupRentAmount ) ) ? ( string ) $this->m_fltCompetitiveGroupRentAmount : '0';
	}

	public function setCompetitiveMarketRentAmount( $fltCompetitiveMarketRentAmount ) {
		$this->set( 'm_fltCompetitiveMarketRentAmount', CStrings::strToFloatDef( $fltCompetitiveMarketRentAmount, NULL, false, 2 ) );
	}

	public function getCompetitiveMarketRentAmount() {
		return $this->m_fltCompetitiveMarketRentAmount;
	}

	public function sqlCompetitiveMarketRentAmount() {
		return ( true == isset( $this->m_fltCompetitiveMarketRentAmount ) ) ? ( string ) $this->m_fltCompetitiveMarketRentAmount : '0';
	}

	public function setCompetitiveRegionalRentAmount( $fltCompetitiveRegionalRentAmount ) {
		$this->set( 'm_fltCompetitiveRegionalRentAmount', CStrings::strToFloatDef( $fltCompetitiveRegionalRentAmount, NULL, false, 2 ) );
	}

	public function getCompetitiveRegionalRentAmount() {
		return $this->m_fltCompetitiveRegionalRentAmount;
	}

	public function sqlCompetitiveRegionalRentAmount() {
		return ( true == isset( $this->m_fltCompetitiveRegionalRentAmount ) ) ? ( string ) $this->m_fltCompetitiveRegionalRentAmount : '0';
	}

	public function setExecutedRentAmount( $fltExecutedRentAmount ) {
		$this->set( 'm_fltExecutedRentAmount', CStrings::strToFloatDef( $fltExecutedRentAmount, NULL, false, 2 ) );
	}

	public function getExecutedRentAmount() {
		return $this->m_fltExecutedRentAmount;
	}

	public function sqlExecutedRentAmount() {
		return ( true == isset( $this->m_fltExecutedRentAmount ) ) ? ( string ) $this->m_fltExecutedRentAmount : '0';
	}

	public function setAdvertisedRentAmount( $fltAdvertisedRentAmount ) {
		$this->set( 'm_fltAdvertisedRentAmount', CStrings::strToFloatDef( $fltAdvertisedRentAmount, NULL, false, 2 ) );
	}

	public function getAdvertisedRentAmount() {
		return $this->m_fltAdvertisedRentAmount;
	}

	public function sqlAdvertisedRentAmount() {
		return ( true == isset( $this->m_fltAdvertisedRentAmount ) ) ? ( string ) $this->m_fltAdvertisedRentAmount : '0';
	}

	public function setAdvertisedOtherRentAmount( $fltAdvertisedOtherRentAmount ) {
		$this->set( 'm_fltAdvertisedOtherRentAmount', CStrings::strToFloatDef( $fltAdvertisedOtherRentAmount, NULL, false, 2 ) );
	}

	public function getAdvertisedOtherRentAmount() {
		return $this->m_fltAdvertisedOtherRentAmount;
	}

	public function sqlAdvertisedOtherRentAmount() {
		return ( true == isset( $this->m_fltAdvertisedOtherRentAmount ) ) ? ( string ) $this->m_fltAdvertisedOtherRentAmount : '0';
	}

	public function setAdvertisedDepositAmount( $fltAdvertisedDepositAmount ) {
		$this->set( 'm_fltAdvertisedDepositAmount', CStrings::strToFloatDef( $fltAdvertisedDepositAmount, NULL, false, 2 ) );
	}

	public function getAdvertisedDepositAmount() {
		return $this->m_fltAdvertisedDepositAmount;
	}

	public function sqlAdvertisedDepositAmount() {
		return ( true == isset( $this->m_fltAdvertisedDepositAmount ) ) ? ( string ) $this->m_fltAdvertisedDepositAmount : '0';
	}

	public function setAdvertisedSpecialsAmount( $fltAdvertisedSpecialsAmount ) {
		$this->set( 'm_fltAdvertisedSpecialsAmount', CStrings::strToFloatDef( $fltAdvertisedSpecialsAmount, NULL, false, 2 ) );
	}

	public function getAdvertisedSpecialsAmount() {
		return $this->m_fltAdvertisedSpecialsAmount;
	}

	public function sqlAdvertisedSpecialsAmount() {
		return ( true == isset( $this->m_fltAdvertisedSpecialsAmount ) ) ? ( string ) $this->m_fltAdvertisedSpecialsAmount : '0';
	}

	public function setAdvertisedRentableItemsAmount( $fltAdvertisedRentableItemsAmount ) {
		$this->set( 'm_fltAdvertisedRentableItemsAmount', CStrings::strToFloatDef( $fltAdvertisedRentableItemsAmount, NULL, false, 2 ) );
	}

	public function getAdvertisedRentableItemsAmount() {
		return $this->m_fltAdvertisedRentableItemsAmount;
	}

	public function sqlAdvertisedRentableItemsAmount() {
		return ( true == isset( $this->m_fltAdvertisedRentableItemsAmount ) ) ? ( string ) $this->m_fltAdvertisedRentableItemsAmount : '0';
	}

	public function setAdvertisedAmenitiesAmount( $fltAdvertisedAmenitiesAmount ) {
		$this->set( 'm_fltAdvertisedAmenitiesAmount', CStrings::strToFloatDef( $fltAdvertisedAmenitiesAmount, NULL, false, 2 ) );
	}

	public function getAdvertisedAmenitiesAmount() {
		return $this->m_fltAdvertisedAmenitiesAmount;
	}

	public function sqlAdvertisedAmenitiesAmount() {
		return ( true == isset( $this->m_fltAdvertisedAmenitiesAmount ) ) ? ( string ) $this->m_fltAdvertisedAmenitiesAmount : '0';
	}

	public function setAdvertisedLiabilityAmount( $fltAdvertisedLiabilityAmount ) {
		$this->set( 'm_fltAdvertisedLiabilityAmount', CStrings::strToFloatDef( $fltAdvertisedLiabilityAmount, NULL, false, 2 ) );
	}

	public function getAdvertisedLiabilityAmount() {
		return $this->m_fltAdvertisedLiabilityAmount;
	}

	public function sqlAdvertisedLiabilityAmount() {
		return ( true == isset( $this->m_fltAdvertisedLiabilityAmount ) ) ? ( string ) $this->m_fltAdvertisedLiabilityAmount : '0';
	}

	public function setAdvertisedTaxAmount( $fltAdvertisedTaxAmount ) {
		$this->set( 'm_fltAdvertisedTaxAmount', CStrings::strToFloatDef( $fltAdvertisedTaxAmount, NULL, false, 2 ) );
	}

	public function getAdvertisedTaxAmount() {
		return $this->m_fltAdvertisedTaxAmount;
	}

	public function sqlAdvertisedTaxAmount() {
		return ( true == isset( $this->m_fltAdvertisedTaxAmount ) ) ? ( string ) $this->m_fltAdvertisedTaxAmount : '0';
	}

	public function setAdvertisedOtherAmount( $fltAdvertisedOtherAmount ) {
		$this->set( 'm_fltAdvertisedOtherAmount', CStrings::strToFloatDef( $fltAdvertisedOtherAmount, NULL, false, 2 ) );
	}

	public function getAdvertisedOtherAmount() {
		return $this->m_fltAdvertisedOtherAmount;
	}

	public function sqlAdvertisedOtherAmount() {
		return ( true == isset( $this->m_fltAdvertisedOtherAmount ) ) ? ( string ) $this->m_fltAdvertisedOtherAmount : '0';
	}

	public function setMinSquareFeet( $fltMinSquareFeet ) {
		$this->set( 'm_fltMinSquareFeet', CStrings::strToFloatDef( $fltMinSquareFeet, NULL, false, 4 ) );
	}

	public function getMinSquareFeet() {
		return $this->m_fltMinSquareFeet;
	}

	public function sqlMinSquareFeet() {
		return ( true == isset( $this->m_fltMinSquareFeet ) ) ? ( string ) $this->m_fltMinSquareFeet : 'NULL';
	}

	public function setMaxSquareFeet( $fltMaxSquareFeet ) {
		$this->set( 'm_fltMaxSquareFeet', CStrings::strToFloatDef( $fltMaxSquareFeet, NULL, false, 4 ) );
	}

	public function getMaxSquareFeet() {
		return $this->m_fltMaxSquareFeet;
	}

	public function sqlMaxSquareFeet() {
		return ( true == isset( $this->m_fltMaxSquareFeet ) ) ? ( string ) $this->m_fltMaxSquareFeet : 'NULL';
	}

	public function setNumberOfUnits( $intNumberOfUnits ) {
		$this->set( 'm_intNumberOfUnits', CStrings::strToIntDef( $intNumberOfUnits, NULL, false ) );
	}

	public function getNumberOfUnits() {
		return $this->m_intNumberOfUnits;
	}

	public function sqlNumberOfUnits() {
		return ( true == isset( $this->m_intNumberOfUnits ) ) ? ( string ) $this->m_intNumberOfUnits : '0';
	}

	public function setNumberOfFloors( $intNumberOfFloors ) {
		$this->set( 'm_intNumberOfFloors', CStrings::strToIntDef( $intNumberOfFloors, NULL, false ) );
	}

	public function getNumberOfFloors() {
		return $this->m_intNumberOfFloors;
	}

	public function sqlNumberOfFloors() {
		return ( true == isset( $this->m_intNumberOfFloors ) ) ? ( string ) $this->m_intNumberOfFloors : '0';
	}

	public function setNumberOfRooms( $intNumberOfRooms ) {
		$this->set( 'm_intNumberOfRooms', CStrings::strToIntDef( $intNumberOfRooms, NULL, false ) );
	}

	public function getNumberOfRooms() {
		return $this->m_intNumberOfRooms;
	}

	public function sqlNumberOfRooms() {
		return ( true == isset( $this->m_intNumberOfRooms ) ) ? ( string ) $this->m_intNumberOfRooms : 'NULL';
	}

	public function setNumberOfBedrooms( $intNumberOfBedrooms ) {
		$this->set( 'm_intNumberOfBedrooms', CStrings::strToIntDef( $intNumberOfBedrooms, NULL, false ) );
	}

	public function getNumberOfBedrooms() {
		return $this->m_intNumberOfBedrooms;
	}

	public function sqlNumberOfBedrooms() {
		return ( true == isset( $this->m_intNumberOfBedrooms ) ) ? ( string ) $this->m_intNumberOfBedrooms : 'NULL';
	}

	public function setNumberOfBathrooms( $intNumberOfBathrooms ) {
		$this->set( 'm_intNumberOfBathrooms', CStrings::strToIntDef( $intNumberOfBathrooms, NULL, false ) );
	}

	public function getNumberOfBathrooms() {
		return $this->m_intNumberOfBathrooms;
	}

	public function sqlNumberOfBathrooms() {
		return ( true == isset( $this->m_intNumberOfBathrooms ) ) ? ( string ) $this->m_intNumberOfBathrooms : 'NULL';
	}

	public function setBedroomDescription( $strBedroomDescription ) {
		$this->set( 'm_strBedroomDescription', CStrings::strTrimDef( $strBedroomDescription, 240, NULL, true ) );
	}

	public function getBedroomDescription() {
		return $this->m_strBedroomDescription;
	}

	public function sqlBedroomDescription() {
		return ( true == isset( $this->m_strBedroomDescription ) ) ? '\'' . addslashes( $this->m_strBedroomDescription ) . '\'' : 'NULL';
	}

	public function setBathroomDescription( $strBathroomDescription ) {
		$this->set( 'm_strBathroomDescription', CStrings::strTrimDef( $strBathroomDescription, 240, NULL, true ) );
	}

	public function getBathroomDescription() {
		return $this->m_strBathroomDescription;
	}

	public function sqlBathroomDescription() {
		return ( true == isset( $this->m_strBathroomDescription ) ) ? '\'' . addslashes( $this->m_strBathroomDescription ) . '\'' : 'NULL';
	}

	public function setAppliancesDescription( $strAppliancesDescription ) {
		$this->set( 'm_strAppliancesDescription', CStrings::strTrimDef( $strAppliancesDescription, 240, NULL, true ) );
	}

	public function getAppliancesDescription() {
		return $this->m_strAppliancesDescription;
	}

	public function sqlAppliancesDescription() {
		return ( true == isset( $this->m_strAppliancesDescription ) ) ? '\'' . addslashes( $this->m_strAppliancesDescription ) . '\'' : 'NULL';
	}

	public function setMaximumOccupants( $intMaximumOccupants ) {
		$this->set( 'm_intMaximumOccupants', CStrings::strToIntDef( $intMaximumOccupants, NULL, false ) );
	}

	public function getMaximumOccupants() {
		return $this->m_intMaximumOccupants;
	}

	public function sqlMaximumOccupants() {
		return ( true == isset( $this->m_intMaximumOccupants ) ) ? ( string ) $this->m_intMaximumOccupants : 'NULL';
	}

	public function setDefaultLeaseTermMonths( $intDefaultLeaseTermMonths ) {
		$this->set( 'm_intDefaultLeaseTermMonths', CStrings::strToIntDef( $intDefaultLeaseTermMonths, NULL, false ) );
	}

	public function getDefaultLeaseTermMonths() {
		return $this->m_intDefaultLeaseTermMonths;
	}

	public function sqlDefaultLeaseTermMonths() {
		return ( true == isset( $this->m_intDefaultLeaseTermMonths ) ) ? ( string ) $this->m_intDefaultLeaseTermMonths : 'NULL';
	}

	public function setBlockIntegration( $intBlockIntegration ) {
		$this->set( 'm_intBlockIntegration', CStrings::strToIntDef( $intBlockIntegration, NULL, false ) );
	}

	public function getBlockIntegration() {
		return $this->m_intBlockIntegration;
	}

	public function sqlBlockIntegration() {
		return ( true == isset( $this->m_intBlockIntegration ) ) ? ( string ) $this->m_intBlockIntegration : '0';
	}

	public function setBlockPricingIntegration( $intBlockPricingIntegration ) {
		$this->set( 'm_intBlockPricingIntegration', CStrings::strToIntDef( $intBlockPricingIntegration, NULL, false ) );
	}

	public function getBlockPricingIntegration() {
		return $this->m_intBlockPricingIntegration;
	}

	public function sqlBlockPricingIntegration() {
		return ( true == isset( $this->m_intBlockPricingIntegration ) ) ? ( string ) $this->m_intBlockPricingIntegration : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setImportedOn( $strImportedOn ) {
		$this->set( 'm_strImportedOn', CStrings::strTrimDef( $strImportedOn, -1, NULL, true ) );
	}

	public function getImportedOn() {
		return $this->m_strImportedOn;
	}

	public function sqlImportedOn() {
		return ( true == isset( $this->m_strImportedOn ) ) ? '\'' . $this->m_strImportedOn . '\'' : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_floorplan_id, optimization_structure_id, unit_exclusion_reason_type_id, unit_type_id, name, description, base_rent, competitive_group_rent_amount, competitive_market_rent_amount, competitive_regional_rent_amount, executed_rent_amount, advertised_rent_amount, advertised_other_rent_amount, advertised_deposit_amount, advertised_specials_amount, advertised_rentable_items_amount, advertised_amenities_amount, advertised_liability_amount, advertised_tax_amount, advertised_other_amount, min_square_feet, max_square_feet, number_of_units, number_of_floors, number_of_rooms, number_of_bedrooms, number_of_bathrooms, bedroom_description, bathroom_description, appliances_description, maximum_occupants, default_lease_term_months, block_integration, block_pricing_integration, is_published, order_num, imported_on, exported_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyFloorplanId() . ', ' .
 						$this->sqlOptimizationStructureId() . ', ' .
 						$this->sqlUnitExclusionReasonTypeId() . ', ' .
 						$this->sqlUnitTypeId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlBaseRent() . ', ' .
 						$this->sqlCompetitiveGroupRentAmount() . ', ' .
 						$this->sqlCompetitiveMarketRentAmount() . ', ' .
 						$this->sqlCompetitiveRegionalRentAmount() . ', ' .
 						$this->sqlExecutedRentAmount() . ', ' .
 						$this->sqlAdvertisedRentAmount() . ', ' .
 						$this->sqlAdvertisedOtherRentAmount() . ', ' .
 						$this->sqlAdvertisedDepositAmount() . ', ' .
 						$this->sqlAdvertisedSpecialsAmount() . ', ' .
 						$this->sqlAdvertisedRentableItemsAmount() . ', ' .
 						$this->sqlAdvertisedAmenitiesAmount() . ', ' .
 						$this->sqlAdvertisedLiabilityAmount() . ', ' .
 						$this->sqlAdvertisedTaxAmount() . ', ' .
 						$this->sqlAdvertisedOtherAmount() . ', ' .
 						$this->sqlMinSquareFeet() . ', ' .
 						$this->sqlMaxSquareFeet() . ', ' .
 						$this->sqlNumberOfUnits() . ', ' .
 						$this->sqlNumberOfFloors() . ', ' .
 						$this->sqlNumberOfRooms() . ', ' .
 						$this->sqlNumberOfBedrooms() . ', ' .
 						$this->sqlNumberOfBathrooms() . ', ' .
 						$this->sqlBedroomDescription() . ', ' .
 						$this->sqlBathroomDescription() . ', ' .
 						$this->sqlAppliancesDescription() . ', ' .
 						$this->sqlMaximumOccupants() . ', ' .
 						$this->sqlDefaultLeaseTermMonths() . ', ' .
 						$this->sqlBlockIntegration() . ', ' .
 						$this->sqlBlockPricingIntegration() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlImportedOn() . ', ' .
 						$this->sqlExportedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; } elseif( true == array_key_exists( 'PropertyFloorplanId', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' optimization_structure_id = ' . $this->sqlOptimizationStructureId() . ','; } elseif( true == array_key_exists( 'OptimizationStructureId', $this->getChangedColumns() ) ) { $strSql .= ' optimization_structure_id = ' . $this->sqlOptimizationStructureId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_exclusion_reason_type_id = ' . $this->sqlUnitExclusionReasonTypeId() . ','; } elseif( true == array_key_exists( 'UnitExclusionReasonTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_exclusion_reason_type_id = ' . $this->sqlUnitExclusionReasonTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent() . ','; } elseif( true == array_key_exists( 'BaseRent', $this->getChangedColumns() ) ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' competitive_group_rent_amount = ' . $this->sqlCompetitiveGroupRentAmount() . ','; } elseif( true == array_key_exists( 'CompetitiveGroupRentAmount', $this->getChangedColumns() ) ) { $strSql .= ' competitive_group_rent_amount = ' . $this->sqlCompetitiveGroupRentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' competitive_market_rent_amount = ' . $this->sqlCompetitiveMarketRentAmount() . ','; } elseif( true == array_key_exists( 'CompetitiveMarketRentAmount', $this->getChangedColumns() ) ) { $strSql .= ' competitive_market_rent_amount = ' . $this->sqlCompetitiveMarketRentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' competitive_regional_rent_amount = ' . $this->sqlCompetitiveRegionalRentAmount() . ','; } elseif( true == array_key_exists( 'CompetitiveRegionalRentAmount', $this->getChangedColumns() ) ) { $strSql .= ' competitive_regional_rent_amount = ' . $this->sqlCompetitiveRegionalRentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_rent_amount = ' . $this->sqlExecutedRentAmount() . ','; } elseif( true == array_key_exists( 'ExecutedRentAmount', $this->getChangedColumns() ) ) { $strSql .= ' executed_rent_amount = ' . $this->sqlExecutedRentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_rent_amount = ' . $this->sqlAdvertisedRentAmount() . ','; } elseif( true == array_key_exists( 'AdvertisedRentAmount', $this->getChangedColumns() ) ) { $strSql .= ' advertised_rent_amount = ' . $this->sqlAdvertisedRentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_other_rent_amount = ' . $this->sqlAdvertisedOtherRentAmount() . ','; } elseif( true == array_key_exists( 'AdvertisedOtherRentAmount', $this->getChangedColumns() ) ) { $strSql .= ' advertised_other_rent_amount = ' . $this->sqlAdvertisedOtherRentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_deposit_amount = ' . $this->sqlAdvertisedDepositAmount() . ','; } elseif( true == array_key_exists( 'AdvertisedDepositAmount', $this->getChangedColumns() ) ) { $strSql .= ' advertised_deposit_amount = ' . $this->sqlAdvertisedDepositAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_specials_amount = ' . $this->sqlAdvertisedSpecialsAmount() . ','; } elseif( true == array_key_exists( 'AdvertisedSpecialsAmount', $this->getChangedColumns() ) ) { $strSql .= ' advertised_specials_amount = ' . $this->sqlAdvertisedSpecialsAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_rentable_items_amount = ' . $this->sqlAdvertisedRentableItemsAmount() . ','; } elseif( true == array_key_exists( 'AdvertisedRentableItemsAmount', $this->getChangedColumns() ) ) { $strSql .= ' advertised_rentable_items_amount = ' . $this->sqlAdvertisedRentableItemsAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_amenities_amount = ' . $this->sqlAdvertisedAmenitiesAmount() . ','; } elseif( true == array_key_exists( 'AdvertisedAmenitiesAmount', $this->getChangedColumns() ) ) { $strSql .= ' advertised_amenities_amount = ' . $this->sqlAdvertisedAmenitiesAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_liability_amount = ' . $this->sqlAdvertisedLiabilityAmount() . ','; } elseif( true == array_key_exists( 'AdvertisedLiabilityAmount', $this->getChangedColumns() ) ) { $strSql .= ' advertised_liability_amount = ' . $this->sqlAdvertisedLiabilityAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_tax_amount = ' . $this->sqlAdvertisedTaxAmount() . ','; } elseif( true == array_key_exists( 'AdvertisedTaxAmount', $this->getChangedColumns() ) ) { $strSql .= ' advertised_tax_amount = ' . $this->sqlAdvertisedTaxAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_other_amount = ' . $this->sqlAdvertisedOtherAmount() . ','; } elseif( true == array_key_exists( 'AdvertisedOtherAmount', $this->getChangedColumns() ) ) { $strSql .= ' advertised_other_amount = ' . $this->sqlAdvertisedOtherAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_square_feet = ' . $this->sqlMinSquareFeet() . ','; } elseif( true == array_key_exists( 'MinSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' min_square_feet = ' . $this->sqlMinSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_square_feet = ' . $this->sqlMaxSquareFeet() . ','; } elseif( true == array_key_exists( 'MaxSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' max_square_feet = ' . $this->sqlMaxSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_units = ' . $this->sqlNumberOfUnits() . ','; } elseif( true == array_key_exists( 'NumberOfUnits', $this->getChangedColumns() ) ) { $strSql .= ' number_of_units = ' . $this->sqlNumberOfUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_floors = ' . $this->sqlNumberOfFloors() . ','; } elseif( true == array_key_exists( 'NumberOfFloors', $this->getChangedColumns() ) ) { $strSql .= ' number_of_floors = ' . $this->sqlNumberOfFloors() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_rooms = ' . $this->sqlNumberOfRooms() . ','; } elseif( true == array_key_exists( 'NumberOfRooms', $this->getChangedColumns() ) ) { $strSql .= ' number_of_rooms = ' . $this->sqlNumberOfRooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_bedrooms = ' . $this->sqlNumberOfBedrooms() . ','; } elseif( true == array_key_exists( 'NumberOfBedrooms', $this->getChangedColumns() ) ) { $strSql .= ' number_of_bedrooms = ' . $this->sqlNumberOfBedrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_bathrooms = ' . $this->sqlNumberOfBathrooms() . ','; } elseif( true == array_key_exists( 'NumberOfBathrooms', $this->getChangedColumns() ) ) { $strSql .= ' number_of_bathrooms = ' . $this->sqlNumberOfBathrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bedroom_description = ' . $this->sqlBedroomDescription() . ','; } elseif( true == array_key_exists( 'BedroomDescription', $this->getChangedColumns() ) ) { $strSql .= ' bedroom_description = ' . $this->sqlBedroomDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bathroom_description = ' . $this->sqlBathroomDescription() . ','; } elseif( true == array_key_exists( 'BathroomDescription', $this->getChangedColumns() ) ) { $strSql .= ' bathroom_description = ' . $this->sqlBathroomDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' appliances_description = ' . $this->sqlAppliancesDescription() . ','; } elseif( true == array_key_exists( 'AppliancesDescription', $this->getChangedColumns() ) ) { $strSql .= ' appliances_description = ' . $this->sqlAppliancesDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maximum_occupants = ' . $this->sqlMaximumOccupants() . ','; } elseif( true == array_key_exists( 'MaximumOccupants', $this->getChangedColumns() ) ) { $strSql .= ' maximum_occupants = ' . $this->sqlMaximumOccupants() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_lease_term_months = ' . $this->sqlDefaultLeaseTermMonths() . ','; } elseif( true == array_key_exists( 'DefaultLeaseTermMonths', $this->getChangedColumns() ) ) { $strSql .= ' default_lease_term_months = ' . $this->sqlDefaultLeaseTermMonths() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block_integration = ' . $this->sqlBlockIntegration() . ','; } elseif( true == array_key_exists( 'BlockIntegration', $this->getChangedColumns() ) ) { $strSql .= ' block_integration = ' . $this->sqlBlockIntegration() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block_pricing_integration = ' . $this->sqlBlockPricingIntegration() . ','; } elseif( true == array_key_exists( 'BlockPricingIntegration', $this->getChangedColumns() ) ) { $strSql .= ' block_pricing_integration = ' . $this->sqlBlockPricingIntegration() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn() . ','; } elseif( true == array_key_exists( 'ImportedOn', $this->getChangedColumns() ) ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_floorplan_id' => $this->getPropertyFloorplanId(),
			'optimization_structure_id' => $this->getOptimizationStructureId(),
			'unit_exclusion_reason_type_id' => $this->getUnitExclusionReasonTypeId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'base_rent' => $this->getBaseRent(),
			'competitive_group_rent_amount' => $this->getCompetitiveGroupRentAmount(),
			'competitive_market_rent_amount' => $this->getCompetitiveMarketRentAmount(),
			'competitive_regional_rent_amount' => $this->getCompetitiveRegionalRentAmount(),
			'executed_rent_amount' => $this->getExecutedRentAmount(),
			'advertised_rent_amount' => $this->getAdvertisedRentAmount(),
			'advertised_other_rent_amount' => $this->getAdvertisedOtherRentAmount(),
			'advertised_deposit_amount' => $this->getAdvertisedDepositAmount(),
			'advertised_specials_amount' => $this->getAdvertisedSpecialsAmount(),
			'advertised_rentable_items_amount' => $this->getAdvertisedRentableItemsAmount(),
			'advertised_amenities_amount' => $this->getAdvertisedAmenitiesAmount(),
			'advertised_liability_amount' => $this->getAdvertisedLiabilityAmount(),
			'advertised_tax_amount' => $this->getAdvertisedTaxAmount(),
			'advertised_other_amount' => $this->getAdvertisedOtherAmount(),
			'min_square_feet' => $this->getMinSquareFeet(),
			'max_square_feet' => $this->getMaxSquareFeet(),
			'number_of_units' => $this->getNumberOfUnits(),
			'number_of_floors' => $this->getNumberOfFloors(),
			'number_of_rooms' => $this->getNumberOfRooms(),
			'number_of_bedrooms' => $this->getNumberOfBedrooms(),
			'number_of_bathrooms' => $this->getNumberOfBathrooms(),
			'bedroom_description' => $this->getBedroomDescription(),
			'bathroom_description' => $this->getBathroomDescription(),
			'appliances_description' => $this->getAppliancesDescription(),
			'maximum_occupants' => $this->getMaximumOccupants(),
			'default_lease_term_months' => $this->getDefaultLeaseTermMonths(),
			'block_integration' => $this->getBlockIntegration(),
			'block_pricing_integration' => $this->getBlockPricingIntegration(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'imported_on' => $this->getImportedOn(),
			'exported_on' => $this->getExportedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>