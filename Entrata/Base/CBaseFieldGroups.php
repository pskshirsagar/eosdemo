<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFieldGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFieldGroups extends CEosPluralBase {

	/**
	 * @return CFieldGroup[]
	 */
	public static function fetchFieldGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CFieldGroup', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFieldGroup
	 */
	public static function fetchFieldGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CFieldGroup', $objDatabase );
	}

	public static function fetchFieldGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'field_groups', $objDatabase );
	}

	public static function fetchFieldGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFieldGroup( sprintf( 'SELECT * FROM field_groups WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFieldGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchFieldGroups( sprintf( 'SELECT * FROM field_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFieldGroupsByFieldTypeIdByCid( $intFieldTypeId, $intCid, $objDatabase ) {
		return self::fetchFieldGroups( sprintf( 'SELECT * FROM field_groups WHERE field_type_id = %d AND cid = %d', ( int ) $intFieldTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>