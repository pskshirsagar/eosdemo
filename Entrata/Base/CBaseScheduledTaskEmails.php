<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledTaskEmails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScheduledTaskEmails extends CEosPluralBase {

	/**
	 * @return CScheduledTaskEmail[]
	 */
	public static function fetchScheduledTaskEmails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CScheduledTaskEmail', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CScheduledTaskEmail
	 */
	public static function fetchScheduledTaskEmail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScheduledTaskEmail', $objDatabase );
	}

	public static function fetchScheduledTaskEmailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_task_emails', $objDatabase );
	}

	public static function fetchScheduledTaskEmailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchScheduledTaskEmail( sprintf( 'SELECT * FROM scheduled_task_emails WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledTaskEmailsByCid( $intCid, $objDatabase ) {
		return self::fetchScheduledTaskEmails( sprintf( 'SELECT * FROM scheduled_task_emails WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledTaskEmailsByEmailTemplateIdByCid( $intSystemMessageTemplateId, $intCid, $objDatabase ) {
		return self::fetchScheduledTaskEmails( sprintf( 'SELECT * FROM scheduled_task_emails WHERE system_message_template_id = %d AND cid = %d', ( int ) $intSystemMessageTemplateId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchScheduledTaskEmailsByHeaderFooterOptionTypeIdByCid( $intHeaderFooterOptionTypeId, $intCid, $objDatabase ) {
		return self::fetchScheduledTaskEmails( sprintf( 'SELECT * FROM scheduled_task_emails WHERE header_footer_option_type_id = %d AND cid = %d', ( int ) $intHeaderFooterOptionTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>