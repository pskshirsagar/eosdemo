<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyPeriods
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyPeriods extends CEosPluralBase {

	/**
	 * @return CPropertyPeriod[]
	 */
	public static function fetchPropertyPeriods( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyPeriod', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyPeriod
	 */
	public static function fetchPropertyPeriod( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyPeriod', $objDatabase );
	}

	public static function fetchPropertyPeriodCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_periods', $objDatabase );
	}

	public static function fetchPropertyPeriodByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyPeriod( sprintf( 'SELECT * FROM property_periods WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyPeriodsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyPeriods( sprintf( 'SELECT * FROM property_periods WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyPeriodsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyPeriods( sprintf( 'SELECT * FROM property_periods WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyPeriodsByPeriodIdByCid( $intPeriodId, $intCid, $objDatabase ) {
		return self::fetchPropertyPeriods( sprintf( 'SELECT * FROM property_periods WHERE period_id = %d AND cid = %d', ( int ) $intPeriodId, ( int ) $intCid ), $objDatabase );
	}

}
?>