<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationQueues
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseIntegrationQueues extends CEosPluralBase {

	/**
	 * @return CIntegrationQueue[]
	 */
	public static function fetchIntegrationQueues( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CIntegrationQueue', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CIntegrationQueue
	 */
	public static function fetchIntegrationQueue( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CIntegrationQueue', $objDatabase );
	}

	public static function fetchIntegrationQueueCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'integration_queues', $objDatabase );
	}

	public static function fetchIntegrationQueueByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchIntegrationQueue( sprintf( 'SELECT * FROM integration_queues WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationQueuesByCid( $intCid, $objDatabase ) {
		return self::fetchIntegrationQueues( sprintf( 'SELECT * FROM integration_queues WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationQueuesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchIntegrationQueues( sprintf( 'SELECT * FROM integration_queues WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationQueuesByIntegrationClientIdByCid( $intIntegrationClientId, $intCid, $objDatabase ) {
		return self::fetchIntegrationQueues( sprintf( 'SELECT * FROM integration_queues WHERE integration_client_id = %d AND cid = %d', ( int ) $intIntegrationClientId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationQueuesByIntegrationServiceIdByCid( $intIntegrationServiceId, $intCid, $objDatabase ) {
		return self::fetchIntegrationQueues( sprintf( 'SELECT * FROM integration_queues WHERE integration_service_id = %d AND cid = %d', ( int ) $intIntegrationServiceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationQueuesByIntegrationQueueStatusTypeIdByCid( $intIntegrationQueueStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchIntegrationQueues( sprintf( 'SELECT * FROM integration_queues WHERE integration_queue_status_type_id = %d AND cid = %d', ( int ) $intIntegrationQueueStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIntegrationQueuesByIntegrationResultIdByCid( $intIntegrationResultId, $intCid, $objDatabase ) {
		return self::fetchIntegrationQueues( sprintf( 'SELECT * FROM integration_queues WHERE integration_result_id = %d AND cid = %d', ( int ) $intIntegrationResultId, ( int ) $intCid ), $objDatabase );
	}

}
?>