<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyTransferEntities
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyTransferEntities extends CEosPluralBase {

	/**
	 * @return CPropertyTransferEntity[]
	 */
	public static function fetchPropertyTransferEntities( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyTransferEntity', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyTransferEntity
	 */
	public static function fetchPropertyTransferEntity( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyTransferEntity', $objDatabase );
	}

	public static function fetchPropertyTransferEntityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_transfer_entities', $objDatabase );
	}

	public static function fetchPropertyTransferEntityByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyTransferEntity( sprintf( 'SELECT * FROM property_transfer_entities WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyTransferEntitiesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyTransferEntities( sprintf( 'SELECT * FROM property_transfer_entities WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyTransferEntitiesByPropertyTransferIdByCid( $intPropertyTransferId, $intCid, $objDatabase ) {
		return self::fetchPropertyTransferEntities( sprintf( 'SELECT * FROM property_transfer_entities WHERE property_transfer_id = %d AND cid = %d', ( int ) $intPropertyTransferId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyTransferEntitiesByImportDataTypeIdByCid( $intImportDataTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyTransferEntities( sprintf( 'SELECT * FROM property_transfer_entities WHERE import_data_type_id = %d AND cid = %d', ( int ) $intImportDataTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>