<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseStateMetaSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.state_meta_settings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_strStateCode;
	protected $m_intCompanyAreaId;
	protected $m_strMetaTitle;
	protected $m_strPageTitle;
	protected $m_strMetaKeywords;
	protected $m_strMetaDescription;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['company_area_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyAreaId', trim( $arrValues['company_area_id'] ) ); elseif( isset( $arrValues['company_area_id'] ) ) $this->setCompanyAreaId( $arrValues['company_area_id'] );
		if( isset( $arrValues['meta_title'] ) && $boolDirectSet ) $this->set( 'm_strMetaTitle', trim( stripcslashes( $arrValues['meta_title'] ) ) ); elseif( isset( $arrValues['meta_title'] ) ) $this->setMetaTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['meta_title'] ) : $arrValues['meta_title'] );
		if( isset( $arrValues['page_title'] ) && $boolDirectSet ) $this->set( 'm_strPageTitle', trim( stripcslashes( $arrValues['page_title'] ) ) ); elseif( isset( $arrValues['page_title'] ) ) $this->setPageTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['page_title'] ) : $arrValues['page_title'] );
		if( isset( $arrValues['meta_keywords'] ) && $boolDirectSet ) $this->set( 'm_strMetaKeywords', trim( stripcslashes( $arrValues['meta_keywords'] ) ) ); elseif( isset( $arrValues['meta_keywords'] ) ) $this->setMetaKeywords( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['meta_keywords'] ) : $arrValues['meta_keywords'] );
		if( isset( $arrValues['meta_description'] ) && $boolDirectSet ) $this->set( 'm_strMetaDescription', trim( stripcslashes( $arrValues['meta_description'] ) ) ); elseif( isset( $arrValues['meta_description'] ) ) $this->setMetaDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['meta_description'] ) : $arrValues['meta_description'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setCompanyAreaId( $intCompanyAreaId ) {
		$this->set( 'm_intCompanyAreaId', CStrings::strToIntDef( $intCompanyAreaId, NULL, false ) );
	}

	public function getCompanyAreaId() {
		return $this->m_intCompanyAreaId;
	}

	public function sqlCompanyAreaId() {
		return ( true == isset( $this->m_intCompanyAreaId ) ) ? ( string ) $this->m_intCompanyAreaId : 'NULL';
	}

	public function setMetaTitle( $strMetaTitle ) {
		$this->set( 'm_strMetaTitle', CStrings::strTrimDef( $strMetaTitle, 240, NULL, true ) );
	}

	public function getMetaTitle() {
		return $this->m_strMetaTitle;
	}

	public function sqlMetaTitle() {
		return ( true == isset( $this->m_strMetaTitle ) ) ? '\'' . addslashes( $this->m_strMetaTitle ) . '\'' : 'NULL';
	}

	public function setPageTitle( $strPageTitle ) {
		$this->set( 'm_strPageTitle', CStrings::strTrimDef( $strPageTitle, 240, NULL, true ) );
	}

	public function getPageTitle() {
		return $this->m_strPageTitle;
	}

	public function sqlPageTitle() {
		return ( true == isset( $this->m_strPageTitle ) ) ? '\'' . addslashes( $this->m_strPageTitle ) . '\'' : 'NULL';
	}

	public function setMetaKeywords( $strMetaKeywords ) {
		$this->set( 'm_strMetaKeywords', CStrings::strTrimDef( $strMetaKeywords, 4096, NULL, true ) );
	}

	public function getMetaKeywords() {
		return $this->m_strMetaKeywords;
	}

	public function sqlMetaKeywords() {
		return ( true == isset( $this->m_strMetaKeywords ) ) ? '\'' . addslashes( $this->m_strMetaKeywords ) . '\'' : 'NULL';
	}

	public function setMetaDescription( $strMetaDescription ) {
		$this->set( 'm_strMetaDescription', CStrings::strTrimDef( $strMetaDescription, 240, NULL, true ) );
	}

	public function getMetaDescription() {
		return $this->m_strMetaDescription;
	}

	public function sqlMetaDescription() {
		return ( true == isset( $this->m_strMetaDescription ) ) ? '\'' . addslashes( $this->m_strMetaDescription ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, state_code, company_area_id, meta_title, page_title, meta_keywords, meta_description, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlStateCode() . ', ' .
 						$this->sqlCompanyAreaId() . ', ' .
 						$this->sqlMetaTitle() . ', ' .
 						$this->sqlPageTitle() . ', ' .
 						$this->sqlMetaKeywords() . ', ' .
 						$this->sqlMetaDescription() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_area_id = ' . $this->sqlCompanyAreaId() . ','; } elseif( true == array_key_exists( 'CompanyAreaId', $this->getChangedColumns() ) ) { $strSql .= ' company_area_id = ' . $this->sqlCompanyAreaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meta_title = ' . $this->sqlMetaTitle() . ','; } elseif( true == array_key_exists( 'MetaTitle', $this->getChangedColumns() ) ) { $strSql .= ' meta_title = ' . $this->sqlMetaTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' page_title = ' . $this->sqlPageTitle() . ','; } elseif( true == array_key_exists( 'PageTitle', $this->getChangedColumns() ) ) { $strSql .= ' page_title = ' . $this->sqlPageTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meta_keywords = ' . $this->sqlMetaKeywords() . ','; } elseif( true == array_key_exists( 'MetaKeywords', $this->getChangedColumns() ) ) { $strSql .= ' meta_keywords = ' . $this->sqlMetaKeywords() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meta_description = ' . $this->sqlMetaDescription() . ','; } elseif( true == array_key_exists( 'MetaDescription', $this->getChangedColumns() ) ) { $strSql .= ' meta_description = ' . $this->sqlMetaDescription() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'state_code' => $this->getStateCode(),
			'company_area_id' => $this->getCompanyAreaId(),
			'meta_title' => $this->getMetaTitle(),
			'page_title' => $this->getPageTitle(),
			'meta_keywords' => $this->getMetaKeywords(),
			'meta_description' => $this->getMetaDescription(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>