<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationPayments
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicationPayments extends CEosPluralBase {

	/**
	 * @return CApplicationPayment[]
	 */
	public static function fetchApplicationPayments( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApplicationPayment', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApplicationPayment
	 */
	public static function fetchApplicationPayment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApplicationPayment', $objDatabase );
	}

	public static function fetchApplicationPaymentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'application_payments', $objDatabase );
	}

	public static function fetchApplicationPaymentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApplicationPayment( sprintf( 'SELECT * FROM application_payments WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationPaymentsByCid( $intCid, $objDatabase ) {
		return self::fetchApplicationPayments( sprintf( 'SELECT * FROM application_payments WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationPaymentsByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchApplicationPayments( sprintf( 'SELECT * FROM application_payments WHERE applicant_id = %d AND cid = %d', ( int ) $intApplicantId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationPaymentsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchApplicationPayments( sprintf( 'SELECT * FROM application_payments WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationPaymentsByArPaymentIdByCid( $intArPaymentId, $intCid, $objDatabase ) {
		return self::fetchApplicationPayments( sprintf( 'SELECT * FROM application_payments WHERE ar_payment_id = %d AND cid = %d', ( int ) $intArPaymentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationPaymentsByApplicationStageIdByCid( $intApplicationStageId, $intCid, $objDatabase ) {
		return self::fetchApplicationPayments( sprintf( 'SELECT * FROM application_payments WHERE application_stage_id = %d AND cid = %d', ( int ) $intApplicationStageId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicationPaymentsByApplicationStatusIdByCid( $intApplicationStatusId, $intCid, $objDatabase ) {
		return self::fetchApplicationPayments( sprintf( 'SELECT * FROM application_payments WHERE application_status_id = %d AND cid = %d', ( int ) $intApplicationStatusId, ( int ) $intCid ), $objDatabase );
	}

}
?>