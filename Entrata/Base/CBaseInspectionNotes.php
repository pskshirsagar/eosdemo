<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CInspectionNotes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseInspectionNotes extends CEosPluralBase {

	/**
	 * @return CInspectionNote[]
	 */
	public static function fetchInspectionNotes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CInspectionNote::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CInspectionNote
	 */
	public static function fetchInspectionNote( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInspectionNote::class, $objDatabase );
	}

	public static function fetchInspectionNoteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'inspection_notes', $objDatabase );
	}

	public static function fetchInspectionNoteByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchInspectionNote( sprintf( 'SELECT * FROM inspection_notes WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionNotesByCid( $intCid, $objDatabase ) {
		return self::fetchInspectionNotes( sprintf( 'SELECT * FROM inspection_notes WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchInspectionNotesByInspectionIdByCid( $intInspectionId, $intCid, $objDatabase ) {
		return self::fetchInspectionNotes( sprintf( 'SELECT * FROM inspection_notes WHERE inspection_id = %d AND cid = %d', $intInspectionId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionNotesByInspectionFormLocationProblemIdByCid( $intInspectionFormLocationProblemId, $intCid, $objDatabase ) {
		return self::fetchInspectionNotes( sprintf( 'SELECT * FROM inspection_notes WHERE inspection_form_location_problem_id = %d AND cid = %d', $intInspectionFormLocationProblemId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionNotesByInspectionResponseIdByCid( $intInspectionResponseId, $intCid, $objDatabase ) {
		return self::fetchInspectionNotes( sprintf( 'SELECT * FROM inspection_notes WHERE inspection_response_id = %d AND cid = %d', $intInspectionResponseId, $intCid ), $objDatabase );
	}

	public static function fetchInspectionNotesByPropertyUnitMaintenanceLocationIdByCid( $intPropertyUnitMaintenanceLocationId, $intCid, $objDatabase ) {
		return self::fetchInspectionNotes( sprintf( 'SELECT * FROM inspection_notes WHERE property_unit_maintenance_location_id = %d AND cid = %d', $intPropertyUnitMaintenanceLocationId, $intCid ), $objDatabase );
	}

}
?>