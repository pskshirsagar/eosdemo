<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerMessageTypes
 * Do not add any new functions to this class.
 */

class CBaseCustomerMessageTypes extends CEosPluralBase {

	/**
	 * @return CCustomerMessageType[]
	 */
	public static function fetchCustomerMessageTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCustomerMessageType::class, $objDatabase );
	}

	/**
	 * @return CCustomerMessageType
	 */
	public static function fetchCustomerMessageType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerMessageType::class, $objDatabase );
	}

	public static function fetchCustomerMessageTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_message_types', $objDatabase );
	}

	public static function fetchCustomerMessageTypeById( $intId, $objDatabase ) {
		return self::fetchCustomerMessageType( sprintf( 'SELECT * FROM customer_message_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>