<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPayeeAccount extends CEosSingularBase {

	const TABLE_NAME = 'public.ap_payee_accounts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApPayeeId;
	protected $m_intApPayeeLocationId;
	protected $m_intDefaultGlAccountId;
	protected $m_intDefaultPropertyId;
	protected $m_intUtilityBillAccountId;
	protected $m_intReplacementApPayeeAccountId;
	protected $m_intAuditFrequencyId;
	protected $m_intBuyerAccountId;
	protected $m_intUtilityBillReceiptTypeId;
	protected $m_strAccountNumber;
	protected $m_strAccountDescription;
	protected $m_intAuditDay;
	protected $m_strLastInvoiceDate;
	protected $m_strAuditStartDate;
	protected $m_boolAuditInvoice;
	protected $m_intAuditInvoiceCount;
	protected $m_boolIsDisabled;
	protected $m_strEntrataAddressOn;
	protected $m_boolIsCaptureInvoiceTotalOnly;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsPunchout;
	protected $m_intCatalogRequestStatusTypeId;
	protected $m_intDefaultApRemittanceId;

	public function __construct() {
		parent::__construct();

		$this->m_intAuditFrequencyId = '1';
		$this->m_boolAuditInvoice = false;
		$this->m_intAuditInvoiceCount = '1';
		$this->m_boolIsDisabled = false;
		$this->m_boolIsCaptureInvoiceTotalOnly = false;
		$this->m_boolIsPunchout = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['ap_payee_location_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeLocationId', trim( $arrValues['ap_payee_location_id'] ) ); elseif( isset( $arrValues['ap_payee_location_id'] ) ) $this->setApPayeeLocationId( $arrValues['ap_payee_location_id'] );
		if( isset( $arrValues['default_gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultGlAccountId', trim( $arrValues['default_gl_account_id'] ) ); elseif( isset( $arrValues['default_gl_account_id'] ) ) $this->setDefaultGlAccountId( $arrValues['default_gl_account_id'] );
		if( isset( $arrValues['default_property_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultPropertyId', trim( $arrValues['default_property_id'] ) ); elseif( isset( $arrValues['default_property_id'] ) ) $this->setDefaultPropertyId( $arrValues['default_property_id'] );
		if( isset( $arrValues['utility_bill_account_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillAccountId', trim( $arrValues['utility_bill_account_id'] ) ); elseif( isset( $arrValues['utility_bill_account_id'] ) ) $this->setUtilityBillAccountId( $arrValues['utility_bill_account_id'] );
		if( isset( $arrValues['replacement_ap_payee_account_id'] ) && $boolDirectSet ) $this->set( 'm_intReplacementApPayeeAccountId', trim( $arrValues['replacement_ap_payee_account_id'] ) ); elseif( isset( $arrValues['replacement_ap_payee_account_id'] ) ) $this->setReplacementApPayeeAccountId( $arrValues['replacement_ap_payee_account_id'] );
		if( isset( $arrValues['audit_frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intAuditFrequencyId', trim( $arrValues['audit_frequency_id'] ) ); elseif( isset( $arrValues['audit_frequency_id'] ) ) $this->setAuditFrequencyId( $arrValues['audit_frequency_id'] );
		if( isset( $arrValues['buyer_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBuyerAccountId', trim( $arrValues['buyer_account_id'] ) ); elseif( isset( $arrValues['buyer_account_id'] ) ) $this->setBuyerAccountId( $arrValues['buyer_account_id'] );
		if( isset( $arrValues['utility_bill_receipt_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillReceiptTypeId', trim( $arrValues['utility_bill_receipt_type_id'] ) ); elseif( isset( $arrValues['utility_bill_receipt_type_id'] ) ) $this->setUtilityBillReceiptTypeId( $arrValues['utility_bill_receipt_type_id'] );
		if( isset( $arrValues['account_number'] ) && $boolDirectSet ) $this->set( 'm_strAccountNumber', trim( $arrValues['account_number'] ) ); elseif( isset( $arrValues['account_number'] ) ) $this->setAccountNumber( $arrValues['account_number'] );
		if( isset( $arrValues['account_description'] ) && $boolDirectSet ) $this->set( 'm_strAccountDescription', trim( $arrValues['account_description'] ) ); elseif( isset( $arrValues['account_description'] ) ) $this->setAccountDescription( $arrValues['account_description'] );
		if( isset( $arrValues['audit_day'] ) && $boolDirectSet ) $this->set( 'm_intAuditDay', trim( $arrValues['audit_day'] ) ); elseif( isset( $arrValues['audit_day'] ) ) $this->setAuditDay( $arrValues['audit_day'] );
		if( isset( $arrValues['last_invoice_date'] ) && $boolDirectSet ) $this->set( 'm_strLastInvoiceDate', trim( $arrValues['last_invoice_date'] ) ); elseif( isset( $arrValues['last_invoice_date'] ) ) $this->setLastInvoiceDate( $arrValues['last_invoice_date'] );
		if( isset( $arrValues['audit_start_date'] ) && $boolDirectSet ) $this->set( 'm_strAuditStartDate', trim( $arrValues['audit_start_date'] ) ); elseif( isset( $arrValues['audit_start_date'] ) ) $this->setAuditStartDate( $arrValues['audit_start_date'] );
		if( isset( $arrValues['audit_invoice'] ) && $boolDirectSet ) $this->set( 'm_boolAuditInvoice', trim( stripcslashes( $arrValues['audit_invoice'] ) ) ); elseif( isset( $arrValues['audit_invoice'] ) ) $this->setAuditInvoice( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['audit_invoice'] ) : $arrValues['audit_invoice'] );
		if( isset( $arrValues['audit_invoice_count'] ) && $boolDirectSet ) $this->set( 'm_intAuditInvoiceCount', trim( $arrValues['audit_invoice_count'] ) ); elseif( isset( $arrValues['audit_invoice_count'] ) ) $this->setAuditInvoiceCount( $arrValues['audit_invoice_count'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['entrata_address_on'] ) && $boolDirectSet ) $this->set( 'm_strEntrataAddressOn', trim( $arrValues['entrata_address_on'] ) ); elseif( isset( $arrValues['entrata_address_on'] ) ) $this->setEntrataAddressOn( $arrValues['entrata_address_on'] );
		if( isset( $arrValues['is_capture_invoice_total_only'] ) && $boolDirectSet ) $this->set( 'm_boolIsCaptureInvoiceTotalOnly', trim( stripcslashes( $arrValues['is_capture_invoice_total_only'] ) ) ); elseif( isset( $arrValues['is_capture_invoice_total_only'] ) ) $this->setIsCaptureInvoiceTotalOnly( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_capture_invoice_total_only'] ) : $arrValues['is_capture_invoice_total_only'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_punchout'] ) && $boolDirectSet ) $this->set( 'm_boolIsPunchout', trim( stripcslashes( $arrValues['is_punchout'] ) ) ); elseif( isset( $arrValues['is_punchout'] ) ) $this->setIsPunchout( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_punchout'] ) : $arrValues['is_punchout'] );
		if( isset( $arrValues['catalog_request_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCatalogRequestStatusTypeId', trim( $arrValues['catalog_request_status_type_id'] ) ); elseif( isset( $arrValues['catalog_request_status_type_id'] ) ) $this->setCatalogRequestStatusTypeId( $arrValues['catalog_request_status_type_id'] );
		if( isset( $arrValues['default_ap_remittance_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultApRemittanceId', trim( $arrValues['default_ap_remittance_id'] ) ); elseif( isset( $arrValues['default_ap_remittance_id'] ) ) $this->setDefaultApRemittanceId( $arrValues['default_ap_remittance_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setApPayeeLocationId( $intApPayeeLocationId ) {
		$this->set( 'm_intApPayeeLocationId', CStrings::strToIntDef( $intApPayeeLocationId, NULL, false ) );
	}

	public function getApPayeeLocationId() {
		return $this->m_intApPayeeLocationId;
	}

	public function sqlApPayeeLocationId() {
		return ( true == isset( $this->m_intApPayeeLocationId ) ) ? ( string ) $this->m_intApPayeeLocationId : 'NULL';
	}

	public function setDefaultGlAccountId( $intDefaultGlAccountId ) {
		$this->set( 'm_intDefaultGlAccountId', CStrings::strToIntDef( $intDefaultGlAccountId, NULL, false ) );
	}

	public function getDefaultGlAccountId() {
		return $this->m_intDefaultGlAccountId;
	}

	public function sqlDefaultGlAccountId() {
		return ( true == isset( $this->m_intDefaultGlAccountId ) ) ? ( string ) $this->m_intDefaultGlAccountId : 'NULL';
	}

	public function setDefaultPropertyId( $intDefaultPropertyId ) {
		$this->set( 'm_intDefaultPropertyId', CStrings::strToIntDef( $intDefaultPropertyId, NULL, false ) );
	}

	public function getDefaultPropertyId() {
		return $this->m_intDefaultPropertyId;
	}

	public function sqlDefaultPropertyId() {
		return ( true == isset( $this->m_intDefaultPropertyId ) ) ? ( string ) $this->m_intDefaultPropertyId : 'NULL';
	}

	public function setUtilityBillAccountId( $intUtilityBillAccountId ) {
		$this->set( 'm_intUtilityBillAccountId', CStrings::strToIntDef( $intUtilityBillAccountId, NULL, false ) );
	}

	public function getUtilityBillAccountId() {
		return $this->m_intUtilityBillAccountId;
	}

	public function sqlUtilityBillAccountId() {
		return ( true == isset( $this->m_intUtilityBillAccountId ) ) ? ( string ) $this->m_intUtilityBillAccountId : 'NULL';
	}

	public function setReplacementApPayeeAccountId( $intReplacementApPayeeAccountId ) {
		$this->set( 'm_intReplacementApPayeeAccountId', CStrings::strToIntDef( $intReplacementApPayeeAccountId, NULL, false ) );
	}

	public function getReplacementApPayeeAccountId() {
		return $this->m_intReplacementApPayeeAccountId;
	}

	public function sqlReplacementApPayeeAccountId() {
		return ( true == isset( $this->m_intReplacementApPayeeAccountId ) ) ? ( string ) $this->m_intReplacementApPayeeAccountId : 'NULL';
	}

	public function setAuditFrequencyId( $intAuditFrequencyId ) {
		$this->set( 'm_intAuditFrequencyId', CStrings::strToIntDef( $intAuditFrequencyId, NULL, false ) );
	}

	public function getAuditFrequencyId() {
		return $this->m_intAuditFrequencyId;
	}

	public function sqlAuditFrequencyId() {
		return ( true == isset( $this->m_intAuditFrequencyId ) ) ? ( string ) $this->m_intAuditFrequencyId : '1';
	}

	public function setBuyerAccountId( $intBuyerAccountId ) {
		$this->set( 'm_intBuyerAccountId', CStrings::strToIntDef( $intBuyerAccountId, NULL, false ) );
	}

	public function getBuyerAccountId() {
		return $this->m_intBuyerAccountId;
	}

	public function sqlBuyerAccountId() {
		return ( true == isset( $this->m_intBuyerAccountId ) ) ? ( string ) $this->m_intBuyerAccountId : 'NULL';
	}

	public function setUtilityBillReceiptTypeId( $intUtilityBillReceiptTypeId ) {
		$this->set( 'm_intUtilityBillReceiptTypeId', CStrings::strToIntDef( $intUtilityBillReceiptTypeId, NULL, false ) );
	}

	public function getUtilityBillReceiptTypeId() {
		return $this->m_intUtilityBillReceiptTypeId;
	}

	public function sqlUtilityBillReceiptTypeId() {
		return ( true == isset( $this->m_intUtilityBillReceiptTypeId ) ) ? ( string ) $this->m_intUtilityBillReceiptTypeId : 'NULL';
	}

	public function setAccountNumber( $strAccountNumber ) {
		$this->set( 'm_strAccountNumber', CStrings::strTrimDef( $strAccountNumber, 50, NULL, true ) );
	}

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function sqlAccountNumber() {
		return ( true == isset( $this->m_strAccountNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAccountNumber ) : '\'' . addslashes( $this->m_strAccountNumber ) . '\'' ) : 'NULL';
	}

	public function setAccountDescription( $strAccountDescription ) {
		$this->set( 'm_strAccountDescription', CStrings::strTrimDef( $strAccountDescription, 50, NULL, true ) );
	}

	public function getAccountDescription() {
		return $this->m_strAccountDescription;
	}

	public function sqlAccountDescription() {
		return ( true == isset( $this->m_strAccountDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAccountDescription ) : '\'' . addslashes( $this->m_strAccountDescription ) . '\'' ) : 'NULL';
	}

	public function setAuditDay( $intAuditDay ) {
		$this->set( 'm_intAuditDay', CStrings::strToIntDef( $intAuditDay, NULL, false ) );
	}

	public function getAuditDay() {
		return $this->m_intAuditDay;
	}

	public function sqlAuditDay() {
		return ( true == isset( $this->m_intAuditDay ) ) ? ( string ) $this->m_intAuditDay : 'NULL';
	}

	public function setLastInvoiceDate( $strLastInvoiceDate ) {
		$this->set( 'm_strLastInvoiceDate', CStrings::strTrimDef( $strLastInvoiceDate, -1, NULL, true ) );
	}

	public function getLastInvoiceDate() {
		return $this->m_strLastInvoiceDate;
	}

	public function sqlLastInvoiceDate() {
		return ( true == isset( $this->m_strLastInvoiceDate ) ) ? '\'' . $this->m_strLastInvoiceDate . '\'' : 'NULL';
	}

	public function setAuditStartDate( $strAuditStartDate ) {
		$this->set( 'm_strAuditStartDate', CStrings::strTrimDef( $strAuditStartDate, -1, NULL, true ) );
	}

	public function getAuditStartDate() {
		return $this->m_strAuditStartDate;
	}

	public function sqlAuditStartDate() {
		return ( true == isset( $this->m_strAuditStartDate ) ) ? '\'' . $this->m_strAuditStartDate . '\'' : 'NULL';
	}

	public function setAuditInvoice( $boolAuditInvoice ) {
		$this->set( 'm_boolAuditInvoice', CStrings::strToBool( $boolAuditInvoice ) );
	}

	public function getAuditInvoice() {
		return $this->m_boolAuditInvoice;
	}

	public function sqlAuditInvoice() {
		return ( true == isset( $this->m_boolAuditInvoice ) ) ? '\'' . ( true == ( bool ) $this->m_boolAuditInvoice ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAuditInvoiceCount( $intAuditInvoiceCount ) {
		$this->set( 'm_intAuditInvoiceCount', CStrings::strToIntDef( $intAuditInvoiceCount, NULL, false ) );
	}

	public function getAuditInvoiceCount() {
		return $this->m_intAuditInvoiceCount;
	}

	public function sqlAuditInvoiceCount() {
		return ( true == isset( $this->m_intAuditInvoiceCount ) ) ? ( string ) $this->m_intAuditInvoiceCount : '1';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setEntrataAddressOn( $strEntrataAddressOn ) {
		$this->set( 'm_strEntrataAddressOn', CStrings::strTrimDef( $strEntrataAddressOn, -1, NULL, true ) );
	}

	public function getEntrataAddressOn() {
		return $this->m_strEntrataAddressOn;
	}

	public function sqlEntrataAddressOn() {
		return ( true == isset( $this->m_strEntrataAddressOn ) ) ? '\'' . $this->m_strEntrataAddressOn . '\'' : 'NULL';
	}

	public function setIsCaptureInvoiceTotalOnly( $boolIsCaptureInvoiceTotalOnly ) {
		$this->set( 'm_boolIsCaptureInvoiceTotalOnly', CStrings::strToBool( $boolIsCaptureInvoiceTotalOnly ) );
	}

	public function getIsCaptureInvoiceTotalOnly() {
		return $this->m_boolIsCaptureInvoiceTotalOnly;
	}

	public function sqlIsCaptureInvoiceTotalOnly() {
		return ( true == isset( $this->m_boolIsCaptureInvoiceTotalOnly ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCaptureInvoiceTotalOnly ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsPunchout( $boolIsPunchout ) {
		$this->set( 'm_boolIsPunchout', CStrings::strToBool( $boolIsPunchout ) );
	}

	public function getIsPunchout() {
		return $this->m_boolIsPunchout;
	}

	public function sqlIsPunchout() {
		return ( true == isset( $this->m_boolIsPunchout ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPunchout ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCatalogRequestStatusTypeId( $intCatalogRequestStatusTypeId ) {
		$this->set( 'm_intCatalogRequestStatusTypeId', CStrings::strToIntDef( $intCatalogRequestStatusTypeId, NULL, false ) );
	}

	public function getCatalogRequestStatusTypeId() {
		return $this->m_intCatalogRequestStatusTypeId;
	}

	public function sqlCatalogRequestStatusTypeId() {
		return ( true == isset( $this->m_intCatalogRequestStatusTypeId ) ) ? ( string ) $this->m_intCatalogRequestStatusTypeId : 'NULL';
	}

	public function setDefaultApRemittanceId( $intDefaultApRemittanceId ) {
		$this->set( 'm_intDefaultApRemittanceId', CStrings::strToIntDef( $intDefaultApRemittanceId, NULL, false ) );
	}

	public function getDefaultApRemittanceId() {
		return $this->m_intDefaultApRemittanceId;
	}

	public function sqlDefaultApRemittanceId() {
		return ( true == isset( $this->m_intDefaultApRemittanceId ) ) ? ( string ) $this->m_intDefaultApRemittanceId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ap_payee_id, ap_payee_location_id, default_gl_account_id, default_property_id, utility_bill_account_id, replacement_ap_payee_account_id, audit_frequency_id, buyer_account_id, utility_bill_receipt_type_id, account_number, account_description, audit_day, last_invoice_date, audit_start_date, audit_invoice, audit_invoice_count, is_disabled, entrata_address_on, is_capture_invoice_total_only, updated_by, updated_on, created_by, created_on, is_punchout, catalog_request_status_type_id, default_ap_remittance_id )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlApPayeeId() . ', ' .
		          $this->sqlApPayeeLocationId() . ', ' .
		          $this->sqlDefaultGlAccountId() . ', ' .
		          $this->sqlDefaultPropertyId() . ', ' .
		          $this->sqlUtilityBillAccountId() . ', ' .
		          $this->sqlReplacementApPayeeAccountId() . ', ' .
		          $this->sqlAuditFrequencyId() . ', ' .
		          $this->sqlBuyerAccountId() . ', ' .
		          $this->sqlUtilityBillReceiptTypeId() . ', ' .
		          $this->sqlAccountNumber() . ', ' .
		          $this->sqlAccountDescription() . ', ' .
		          $this->sqlAuditDay() . ', ' .
		          $this->sqlLastInvoiceDate() . ', ' .
		          $this->sqlAuditStartDate() . ', ' .
		          $this->sqlAuditInvoice() . ', ' .
		          $this->sqlAuditInvoiceCount() . ', ' .
		          $this->sqlIsDisabled() . ', ' .
		          $this->sqlEntrataAddressOn() . ', ' .
		          $this->sqlIsCaptureInvoiceTotalOnly() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlIsPunchout() . ', ' .
		          $this->sqlCatalogRequestStatusTypeId() . ', ' .
		          $this->sqlDefaultApRemittanceId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId(). ',' ; } elseif( true == array_key_exists( 'ApPayeeLocationId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_location_id = ' . $this->sqlApPayeeLocationId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_gl_account_id = ' . $this->sqlDefaultGlAccountId(). ',' ; } elseif( true == array_key_exists( 'DefaultGlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' default_gl_account_id = ' . $this->sqlDefaultGlAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_property_id = ' . $this->sqlDefaultPropertyId(). ',' ; } elseif( true == array_key_exists( 'DefaultPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' default_property_id = ' . $this->sqlDefaultPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillAccountId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_account_id = ' . $this->sqlUtilityBillAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' replacement_ap_payee_account_id = ' . $this->sqlReplacementApPayeeAccountId(). ',' ; } elseif( true == array_key_exists( 'ReplacementApPayeeAccountId', $this->getChangedColumns() ) ) { $strSql .= ' replacement_ap_payee_account_id = ' . $this->sqlReplacementApPayeeAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audit_frequency_id = ' . $this->sqlAuditFrequencyId(). ',' ; } elseif( true == array_key_exists( 'AuditFrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' audit_frequency_id = ' . $this->sqlAuditFrequencyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' buyer_account_id = ' . $this->sqlBuyerAccountId(). ',' ; } elseif( true == array_key_exists( 'BuyerAccountId', $this->getChangedColumns() ) ) { $strSql .= ' buyer_account_id = ' . $this->sqlBuyerAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_bill_receipt_type_id = ' . $this->sqlUtilityBillReceiptTypeId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillReceiptTypeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_bill_receipt_type_id = ' . $this->sqlUtilityBillReceiptTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_number = ' . $this->sqlAccountNumber(). ',' ; } elseif( true == array_key_exists( 'AccountNumber', $this->getChangedColumns() ) ) { $strSql .= ' account_number = ' . $this->sqlAccountNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_description = ' . $this->sqlAccountDescription(). ',' ; } elseif( true == array_key_exists( 'AccountDescription', $this->getChangedColumns() ) ) { $strSql .= ' account_description = ' . $this->sqlAccountDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audit_day = ' . $this->sqlAuditDay(). ',' ; } elseif( true == array_key_exists( 'AuditDay', $this->getChangedColumns() ) ) { $strSql .= ' audit_day = ' . $this->sqlAuditDay() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_invoice_date = ' . $this->sqlLastInvoiceDate(). ',' ; } elseif( true == array_key_exists( 'LastInvoiceDate', $this->getChangedColumns() ) ) { $strSql .= ' last_invoice_date = ' . $this->sqlLastInvoiceDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audit_start_date = ' . $this->sqlAuditStartDate(). ',' ; } elseif( true == array_key_exists( 'AuditStartDate', $this->getChangedColumns() ) ) { $strSql .= ' audit_start_date = ' . $this->sqlAuditStartDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audit_invoice = ' . $this->sqlAuditInvoice(). ',' ; } elseif( true == array_key_exists( 'AuditInvoice', $this->getChangedColumns() ) ) { $strSql .= ' audit_invoice = ' . $this->sqlAuditInvoice() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audit_invoice_count = ' . $this->sqlAuditInvoiceCount(). ',' ; } elseif( true == array_key_exists( 'AuditInvoiceCount', $this->getChangedColumns() ) ) { $strSql .= ' audit_invoice_count = ' . $this->sqlAuditInvoiceCount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_address_on = ' . $this->sqlEntrataAddressOn(). ',' ; } elseif( true == array_key_exists( 'EntrataAddressOn', $this->getChangedColumns() ) ) { $strSql .= ' entrata_address_on = ' . $this->sqlEntrataAddressOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_capture_invoice_total_only = ' . $this->sqlIsCaptureInvoiceTotalOnly(). ',' ; } elseif( true == array_key_exists( 'IsCaptureInvoiceTotalOnly', $this->getChangedColumns() ) ) { $strSql .= ' is_capture_invoice_total_only = ' . $this->sqlIsCaptureInvoiceTotalOnly() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_punchout = ' . $this->sqlIsPunchout(). ',' ; } elseif( true == array_key_exists( 'IsPunchout', $this->getChangedColumns() ) ) { $strSql .= ' is_punchout = ' . $this->sqlIsPunchout() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' catalog_request_status_type_id = ' . $this->sqlCatalogRequestStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'CatalogRequestStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' catalog_request_status_type_id = ' . $this->sqlCatalogRequestStatusTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_ap_remittance_id = ' . $this->sqlDefaultApRemittanceId(). ',' ; } elseif( true == array_key_exists( 'DefaultApRemittanceId', $this->getChangedColumns() ) ) { $strSql .= ' default_ap_remittance_id = ' . $this->sqlDefaultApRemittanceId() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ap_payee_id' => $this->getApPayeeId(),
			'ap_payee_location_id' => $this->getApPayeeLocationId(),
			'default_gl_account_id' => $this->getDefaultGlAccountId(),
			'default_property_id' => $this->getDefaultPropertyId(),
			'utility_bill_account_id' => $this->getUtilityBillAccountId(),
			'replacement_ap_payee_account_id' => $this->getReplacementApPayeeAccountId(),
			'audit_frequency_id' => $this->getAuditFrequencyId(),
			'buyer_account_id' => $this->getBuyerAccountId(),
			'utility_bill_receipt_type_id' => $this->getUtilityBillReceiptTypeId(),
			'account_number' => $this->getAccountNumber(),
			'account_description' => $this->getAccountDescription(),
			'audit_day' => $this->getAuditDay(),
			'last_invoice_date' => $this->getLastInvoiceDate(),
			'audit_start_date' => $this->getAuditStartDate(),
			'audit_invoice' => $this->getAuditInvoice(),
			'audit_invoice_count' => $this->getAuditInvoiceCount(),
			'is_disabled' => $this->getIsDisabled(),
			'entrata_address_on' => $this->getEntrataAddressOn(),
			'is_capture_invoice_total_only' => $this->getIsCaptureInvoiceTotalOnly(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_punchout' => $this->getIsPunchout(),
			'catalog_request_status_type_id' => $this->getCatalogRequestStatusTypeId(),
			'default_ap_remittance_id' => $this->getDefaultApRemittanceId()
		);
	}

}
?>