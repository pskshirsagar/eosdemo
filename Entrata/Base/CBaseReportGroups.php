<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReportGroups extends CEosPluralBase {

	/**
	 * @return CReportGroup[]
	 */
	public static function fetchReportGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CReportGroup::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CReportGroup
	 */
	public static function fetchReportGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportGroup::class, $objDatabase );
	}

	public static function fetchReportGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_groups', $objDatabase );
	}

	public static function fetchReportGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchReportGroup( sprintf( 'SELECT * FROM report_groups WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchReportGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchReportGroups( sprintf( 'SELECT * FROM report_groups WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchReportGroupsByReportGroupTypeIdByCid( $intReportGroupTypeId, $intCid, $objDatabase ) {
		return self::fetchReportGroups( sprintf( 'SELECT * FROM report_groups WHERE report_group_type_id = %d AND cid = %d', $intReportGroupTypeId, $intCid ), $objDatabase );
	}

	public static function fetchReportGroupByDefaultReportGroupIdByCid( $intDefaultReportGroupId, $intCid, $objDatabase ) {
		return self::fetchReportGroup( sprintf( 'SELECT * FROM report_groups WHERE default_report_group_id = %d AND cid = %d', $intDefaultReportGroupId, $intCid ), $objDatabase );
	}

	public static function fetchReportGroupsByDefaultReportGroupIdByCid( $intDefaultReportGroupId, $intCid, $objDatabase ) {
		return self::fetchReportGroups( sprintf( 'SELECT * FROM report_groups WHERE default_report_group_id = %d AND cid = %d', $intDefaultReportGroupId, $intCid ), $objDatabase );
	}

	public static function fetchReportGroupByModuleIdByCid( $intModuleId, $intCid, $objDatabase ) {
		return self::fetchReportGroup( sprintf( 'SELECT * FROM report_groups WHERE module_id = %d AND cid = %d', $intModuleId, $intCid ), $objDatabase );
	}

	public static function fetchReportGroupsByModuleIdByCid( $intModuleId, $intCid, $objDatabase ) {
		return self::fetchReportGroups( sprintf( 'SELECT * FROM report_groups WHERE module_id = %d AND cid = %d', $intModuleId, $intCid ), $objDatabase );
	}

	public static function fetchReportGroupsByParentModuleIdByCid( $intParentModuleId, $intCid, $objDatabase ) {
		return self::fetchReportGroups( sprintf( 'SELECT * FROM report_groups WHERE parent_module_id = %d AND cid = %d', $intParentModuleId, $intCid ), $objDatabase );
	}

	public static function fetchReportGroupsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchReportGroups( sprintf( 'SELECT * FROM report_groups WHERE company_user_id = %d AND cid = %d', $intCompanyUserId, $intCid ), $objDatabase );
	}

}
?>