<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CImportTemplates
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseImportTemplates extends CEosPluralBase {

	/**
	 * @return CImportTemplate[]
	 */
	public static function fetchImportTemplates( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CImportTemplate::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CImportTemplate
	 */
	public static function fetchImportTemplate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CImportTemplate::class, $objDatabase );
	}

	public static function fetchImportTemplateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'import_templates', $objDatabase );
	}

	public static function fetchImportTemplateByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchImportTemplate( sprintf( 'SELECT * FROM import_templates WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchImportTemplatesByCid( $intCid, $objDatabase ) {
		return self::fetchImportTemplates( sprintf( 'SELECT * FROM import_templates WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchImportTemplatesBySourceCidByCid( $intSourceCid, $intCid, $objDatabase ) {
		return self::fetchImportTemplates( sprintf( 'SELECT * FROM import_templates WHERE source_cid = %d AND cid = %d', $intSourceCid, $intCid ), $objDatabase );
	}

	public static function fetchImportTemplatesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchImportTemplates( sprintf( 'SELECT * FROM import_templates WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchImportTemplatesBySourcePropertyIdByCid( $intSourcePropertyId, $intCid, $objDatabase ) {
		return self::fetchImportTemplates( sprintf( 'SELECT * FROM import_templates WHERE source_property_id = %d AND cid = %d', $intSourcePropertyId, $intCid ), $objDatabase );
	}

	public static function fetchImportTemplatesByImportTypeIdByCid( $intImportTypeId, $intCid, $objDatabase ) {
		return self::fetchImportTemplates( sprintf( 'SELECT * FROM import_templates WHERE import_type_id = %d AND cid = %d', $intImportTypeId, $intCid ), $objDatabase );
	}

	public static function fetchImportTemplatesByImportStatusTypeIdByCid( $intImportStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchImportTemplates( sprintf( 'SELECT * FROM import_templates WHERE import_status_type_id = %d AND cid = %d', $intImportStatusTypeId, $intCid ), $objDatabase );
	}

	public static function fetchImportTemplatesByCompetitorIdByCid( $intCompetitorId, $intCid, $objDatabase ) {
		return self::fetchImportTemplates( sprintf( 'SELECT * FROM import_templates WHERE competitor_id = %d AND cid = %d', $intCompetitorId, $intCid ), $objDatabase );
	}

	public static function fetchImportTemplatesByImportDataTypeIdByCid( $intImportDataTypeId, $intCid, $objDatabase ) {
		return self::fetchImportTemplates( sprintf( 'SELECT * FROM import_templates WHERE import_data_type_id = %d AND cid = %d', $intImportDataTypeId, $intCid ), $objDatabase );
	}

}
?>