<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyApplicationPreferences
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyApplicationPreferences extends CEosPluralBase {

	/**
	 * @return CPropertyApplicationPreference[]
	 */
	public static function fetchPropertyApplicationPreferences( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPropertyApplicationPreference::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyApplicationPreference
	 */
	public static function fetchPropertyApplicationPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyApplicationPreference::class, $objDatabase );
	}

	public static function fetchPropertyApplicationPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_application_preferences', $objDatabase );
	}

	public static function fetchPropertyApplicationPreferenceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyApplicationPreference( sprintf( 'SELECT * FROM property_application_preferences WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyApplicationPreferencesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyApplicationPreferences( sprintf( 'SELECT * FROM property_application_preferences WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchPropertyApplicationPreferencesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyApplicationPreferences( sprintf( 'SELECT * FROM property_application_preferences WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyApplicationPreferencesByCompanyApplicationIdByCid( $intCompanyApplicationId, $intCid, $objDatabase ) {
		return self::fetchPropertyApplicationPreferences( sprintf( 'SELECT * FROM property_application_preferences WHERE company_application_id = %d AND cid = %d', $intCompanyApplicationId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyApplicationPreferencesByApplicationPreferenceTypeIdByCid( $intApplicationPreferenceTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyApplicationPreferences( sprintf( 'SELECT * FROM property_application_preferences WHERE application_preference_type_id = %d AND cid = %d', $intApplicationPreferenceTypeId, $intCid ), $objDatabase );
	}

}
?>