<?php

class CBaseApplicationSettingKey extends CEosSingularBase {

	const TABLE_NAME = 'public.application_setting_keys';

	protected $m_intId;
	protected $m_intApplicationSettingGroupId;
	protected $m_intOccupancyTypeId;
	protected $m_intToolTipId;
	protected $m_strKey;
	protected $m_strOldLabel;
	protected $m_strLabel;
	protected $m_strTableName;
	protected $m_strColumnName;
	protected $m_boolIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;
		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['application_setting_group_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationSettingGroupId', trim( $arrValues['application_setting_group_id'] ) ); elseif( isset( $arrValues['application_setting_group_id'] ) ) $this->setApplicationSettingGroupId( $arrValues['application_setting_group_id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['tool_tip_id'] ) && $boolDirectSet ) $this->set( 'm_intToolTipId', trim( $arrValues['tool_tip_id'] ) ); elseif( isset( $arrValues['tool_tip_id'] ) ) $this->setToolTipId( $arrValues['tool_tip_id'] );
		if( isset( $arrValues['key'] ) && $boolDirectSet ) $this->set( 'm_strKey', trim( stripcslashes( $arrValues['key'] ) ) ); elseif( isset( $arrValues['key'] ) ) $this->setKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['key'] ) : $arrValues['key'] );
		if( isset( $arrValues['old_label'] ) && $boolDirectSet ) $this->set( 'm_strOldLabel', trim( stripcslashes( $arrValues['old_label'] ) ) ); elseif( isset( $arrValues['old_label'] ) ) $this->setOldLabel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['old_label'] ) : $arrValues['old_label'] );
		if( isset( $arrValues['label'] ) && $boolDirectSet ) $this->set( 'm_strLabel', trim( stripcslashes( $arrValues['label'] ) ) ); elseif( isset( $arrValues['label'] ) ) $this->setLabel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['label'] ) : $arrValues['label'] );
		if( isset( $arrValues['table_name'] ) && $boolDirectSet ) $this->set( 'm_strTableName', trim( stripcslashes( $arrValues['table_name'] ) ) ); elseif( isset( $arrValues['table_name'] ) ) $this->setTableName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['table_name'] ) : $arrValues['table_name'] );
		if( isset( $arrValues['column_name'] ) && $boolDirectSet ) $this->set( 'm_strColumnName', trim( stripcslashes( $arrValues['column_name'] ) ) ); elseif( isset( $arrValues['column_name'] ) ) $this->setColumnName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['column_name'] ) : $arrValues['column_name'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setApplicationSettingGroupId( $intApplicationSettingGroupId ) {
		$this->set( 'm_intApplicationSettingGroupId', CStrings::strToIntDef( $intApplicationSettingGroupId, NULL, false ) );
	}

	public function getApplicationSettingGroupId() {
		return $this->m_intApplicationSettingGroupId;
	}

	public function sqlApplicationSettingGroupId() {
		return ( true == isset( $this->m_intApplicationSettingGroupId ) ) ? ( string ) $this->m_intApplicationSettingGroupId : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : 'NULL';
	}

	public function setToolTipId( $intToolTipId ) {
		$this->set( 'm_intToolTipId', CStrings::strToIntDef( $intToolTipId, NULL, false ) );
	}

	public function getToolTipId() {
		return $this->m_intToolTipId;
	}

	public function sqlToolTipId() {
		return ( true == isset( $this->m_intToolTipId ) ) ? ( string ) $this->m_intToolTipId : 'NULL';
	}

	public function setKey( $strKey ) {
		$this->set( 'm_strKey', CStrings::strTrimDef( $strKey, 100, NULL, true ) );
	}

	public function getKey() {
		return $this->m_strKey;
	}

	public function sqlKey() {
		return ( true == isset( $this->m_strKey ) ) ? '\'' . addslashes( $this->m_strKey ) . '\'' : 'NULL';
	}

	public function setOldLabel( $strOldLabel ) {
		$this->set( 'm_strOldLabel', CStrings::strTrimDef( $strOldLabel, 240, NULL, true ) );
	}

	public function getOldLabel() {
		return $this->m_strOldLabel;
	}

	public function sqlOldLabel() {
		return ( true == isset( $this->m_strOldLabel ) ) ? '\'' . addslashes( $this->m_strOldLabel ) . '\'' : 'NULL';
	}

	public function setLabel( $strLabel ) {
		$this->set( 'm_strLabel', CStrings::strTrimDef( $strLabel, 240, NULL, true ) );
	}

	public function getLabel() {
		return $this->m_strLabel;
	}

	public function sqlLabel() {
		return ( true == isset( $this->m_strLabel ) ) ? '\'' . addslashes( $this->m_strLabel ) . '\'' : 'NULL';
	}

	public function setTableName( $strTableName ) {
		$this->set( 'm_strTableName', CStrings::strTrimDef( $strTableName, 255, NULL, true ) );
	}

	public function getTableName() {
		return $this->m_strTableName;
	}

	public function sqlTableName() {
		return ( true == isset( $this->m_strTableName ) ) ? '\'' . addslashes( $this->m_strTableName ) . '\'' : 'NULL';
	}

	public function setColumnName( $strColumnName ) {
		$this->set( 'm_strColumnName', CStrings::strTrimDef( $strColumnName, 255, NULL, true ) );
	}

	public function getColumnName() {
		return $this->m_strColumnName;
	}

	public function sqlColumnName() {
		return ( true == isset( $this->m_strColumnName ) ) ? '\'' . addslashes( $this->m_strColumnName ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, application_setting_group_id, occupancy_type_id, tool_tip_id, key, old_label, label, table_name, column_name, is_published, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlApplicationSettingGroupId() . ', ' .
 						$this->sqlOccupancyTypeId() . ', ' .
 						$this->sqlToolTipId() . ', ' .
 						$this->sqlKey() . ', ' .
 						$this->sqlOldLabel() . ', ' .
 						$this->sqlLabel() . ', ' .
 						$this->sqlTableName() . ', ' .
 						$this->sqlColumnName() . ', ' .
 						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_setting_group_id = ' . $this->sqlApplicationSettingGroupId() . ','; } elseif( true == array_key_exists( 'ApplicationSettingGroupId', $this->getChangedColumns() ) ) { $strSql .= ' application_setting_group_id = ' . $this->sqlApplicationSettingGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tool_tip_id = ' . $this->sqlToolTipId() . ','; } elseif( true == array_key_exists( 'ToolTipId', $this->getChangedColumns() ) ) { $strSql .= ' tool_tip_id = ' . $this->sqlToolTipId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' key = ' . $this->sqlKey() . ','; } elseif( true == array_key_exists( 'Key', $this->getChangedColumns() ) ) { $strSql .= ' key = ' . $this->sqlKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_label = ' . $this->sqlOldLabel() . ','; } elseif( true == array_key_exists( 'OldLabel', $this->getChangedColumns() ) ) { $strSql .= ' old_label = ' . $this->sqlOldLabel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' label = ' . $this->sqlLabel() . ','; } elseif( true == array_key_exists( 'Label', $this->getChangedColumns() ) ) { $strSql .= ' label = ' . $this->sqlLabel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' table_name = ' . $this->sqlTableName() . ','; } elseif( true == array_key_exists( 'TableName', $this->getChangedColumns() ) ) { $strSql .= ' table_name = ' . $this->sqlTableName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' column_name = ' . $this->sqlColumnName() . ','; } elseif( true == array_key_exists( 'ColumnName', $this->getChangedColumns() ) ) { $strSql .= ' column_name = ' . $this->sqlColumnName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'application_setting_group_id' => $this->getApplicationSettingGroupId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'tool_tip_id' => $this->getToolTipId(),
			'key' => $this->getKey(),
			'old_label' => $this->getOldLabel(),
			'label' => $this->getLabel(),
			'table_name' => $this->getTableName(),
			'column_name' => $this->getColumnName(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>