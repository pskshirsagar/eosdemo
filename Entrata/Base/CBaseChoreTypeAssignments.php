<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChoreTypeAssignments
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseChoreTypeAssignments extends CEosPluralBase {

	/**
	 * @return CChoreTypeAssignment[]
	 */
	public static function fetchChoreTypeAssignments( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CChoreTypeAssignment', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CChoreTypeAssignment
	 */
	public static function fetchChoreTypeAssignment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CChoreTypeAssignment', $objDatabase );
	}

	public static function fetchChoreTypeAssignmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'chore_type_assignments', $objDatabase );
	}

	public static function fetchChoreTypeAssignmentByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchChoreTypeAssignment( sprintf( 'SELECT * FROM chore_type_assignments WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoreTypeAssignmentsByCid( $intCid, $objDatabase ) {
		return self::fetchChoreTypeAssignments( sprintf( 'SELECT * FROM chore_type_assignments WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoreTypeAssignmentsByCompanyGroupIdByCid( $intCompanyGroupId, $intCid, $objDatabase ) {
		return self::fetchChoreTypeAssignments( sprintf( 'SELECT * FROM chore_type_assignments WHERE company_group_id = %d AND cid = %d', ( int ) $intCompanyGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoreTypeAssignmentsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchChoreTypeAssignments( sprintf( 'SELECT * FROM chore_type_assignments WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChoreTypeAssignmentsByChoreTypeIdByCid( $intChoreTypeId, $intCid, $objDatabase ) {
		return self::fetchChoreTypeAssignments( sprintf( 'SELECT * FROM chore_type_assignments WHERE chore_type_id = %d AND cid = %d', ( int ) $intChoreTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>