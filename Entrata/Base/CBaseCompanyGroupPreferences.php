<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyGroupPreferences
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyGroupPreferences extends CEosPluralBase {

	/**
	 * @return CCompanyGroupPreference[]
	 */
	public static function fetchCompanyGroupPreferences( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyGroupPreference', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyGroupPreference
	 */
	public static function fetchCompanyGroupPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyGroupPreference', $objDatabase );
	}

	public static function fetchCompanyGroupPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_group_preferences', $objDatabase );
	}

	public static function fetchCompanyGroupPreferenceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyGroupPreference( sprintf( 'SELECT * FROM company_group_preferences WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyGroupPreferencesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyGroupPreferences( sprintf( 'SELECT * FROM company_group_preferences WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyGroupPreferencesByCompanyGroupIdByCid( $intCompanyGroupId, $intCid, $objDatabase ) {
		return self::fetchCompanyGroupPreferences( sprintf( 'SELECT * FROM company_group_preferences WHERE company_group_id = %d AND cid = %d', ( int ) $intCompanyGroupId, ( int ) $intCid ), $objDatabase );
	}

}
?>