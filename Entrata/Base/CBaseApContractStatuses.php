<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApContractStatuses
 * Do not add any new functions to this class.
 */

class CBaseApContractStatuses extends CEosPluralBase {

	/**
	 * @return CApContractStatus[]
	 */
	public static function fetchApContractStatuses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CApContractStatus::class, $objDatabase );
	}

	/**
	 * @return CApContractStatus
	 */
	public static function fetchApContractStatus( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApContractStatus::class, $objDatabase );
	}

	public static function fetchApContractStatusCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_contract_statuses', $objDatabase );
	}

	public static function fetchApContractStatusById( $intId, $objDatabase ) {
		return self::fetchApContractStatus( sprintf( 'SELECT * FROM ap_contract_statuses WHERE id = %d', $intId ), $objDatabase );
	}

}
?>