<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitSpaceLogTypes
 * Do not add any new functions to this class.
 */

class CBaseUnitSpaceLogTypes extends CEosPluralBase {

	/**
	 * @return CUnitSpaceLogType[]
	 */
	public static function fetchUnitSpaceLogTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CUnitSpaceLogType::class, $objDatabase );
	}

	/**
	 * @return CUnitSpaceLogType
	 */
	public static function fetchUnitSpaceLogType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CUnitSpaceLogType::class, $objDatabase );
	}

	public static function fetchUnitSpaceLogTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'unit_space_log_types', $objDatabase );
	}

	public static function fetchUnitSpaceLogTypeById( $intId, $objDatabase ) {
		return self::fetchUnitSpaceLogType( sprintf( 'SELECT * FROM unit_space_log_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>