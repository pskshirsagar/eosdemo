<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPaymentLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPaymentLogs extends CEosPluralBase {

	/**
	 * @return CApPaymentLog[]
	 */
	public static function fetchApPaymentLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApPaymentLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApPaymentLog
	 */
	public static function fetchApPaymentLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApPaymentLog', $objDatabase );
	}

	public static function fetchApPaymentLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_payment_logs', $objDatabase );
	}

	public static function fetchApPaymentLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApPaymentLog( sprintf( 'SELECT * FROM ap_payment_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentLogsByCid( $intCid, $objDatabase ) {
		return self::fetchApPaymentLogs( sprintf( 'SELECT * FROM ap_payment_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentLogsByApPaymentIdByCid( $intApPaymentId, $intCid, $objDatabase ) {
		return self::fetchApPaymentLogs( sprintf( 'SELECT * FROM ap_payment_logs WHERE ap_payment_id = %d AND cid = %d', ( int ) $intApPaymentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentLogsByApHeaderIdByCid( $intApHeaderId, $intCid, $objDatabase ) {
		return self::fetchApPaymentLogs( sprintf( 'SELECT * FROM ap_payment_logs WHERE ap_header_id = %d AND cid = %d', ( int ) $intApHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentLogsByMerchantAccountIdByCid( $intMerchantAccountId, $intCid, $objDatabase ) {
		return self::fetchApPaymentLogs( sprintf( 'SELECT * FROM ap_payment_logs WHERE merchant_account_id = %d AND cid = %d', ( int ) $intMerchantAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentLogsByBankAccountIdByCid( $intBankAccountId, $intCid, $objDatabase ) {
		return self::fetchApPaymentLogs( sprintf( 'SELECT * FROM ap_payment_logs WHERE bank_account_id = %d AND cid = %d', ( int ) $intBankAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentLogsByMerchantGatewayIdByCid( $intMerchantGatewayId, $intCid, $objDatabase ) {
		return self::fetchApPaymentLogs( sprintf( 'SELECT * FROM ap_payment_logs WHERE merchant_gateway_id = %d AND cid = %d', ( int ) $intMerchantGatewayId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentLogsByPaymentMediumIdByCid( $intPaymentMediumId, $intCid, $objDatabase ) {
		return self::fetchApPaymentLogs( sprintf( 'SELECT * FROM ap_payment_logs WHERE payment_medium_id = %d AND cid = %d', ( int ) $intPaymentMediumId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentLogsByApPaymentTypeIdByCid( $intApPaymentTypeId, $intCid, $objDatabase ) {
		return self::fetchApPaymentLogs( sprintf( 'SELECT * FROM ap_payment_logs WHERE ap_payment_type_id = %d AND cid = %d', ( int ) $intApPaymentTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentLogsByPaymentStatusTypeIdByCid( $intPaymentStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchApPaymentLogs( sprintf( 'SELECT * FROM ap_payment_logs WHERE payment_status_type_id = %d AND cid = %d', ( int ) $intPaymentStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentLogsByReturnTypeIdByCid( $intReturnTypeId, $intCid, $objDatabase ) {
		return self::fetchApPaymentLogs( sprintf( 'SELECT * FROM ap_payment_logs WHERE return_type_id = %d AND cid = %d', ( int ) $intReturnTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPaymentLogsByCheckAccountTypeIdByCid( $intCheckAccountTypeId, $intCid, $objDatabase ) {
		return self::fetchApPaymentLogs( sprintf( 'SELECT * FROM ap_payment_logs WHERE check_account_type_id = %d AND cid = %d', ( int ) $intCheckAccountTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>