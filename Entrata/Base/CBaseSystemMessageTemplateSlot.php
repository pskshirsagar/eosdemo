<?php

class CBaseSystemMessageTemplateSlot extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.system_message_template_slots';

	protected $m_intId;
	protected $m_intSystemMessageTemplateId;
	protected $m_strKey;
	protected $m_strTitle;
	protected $m_strDefaultImageName;
	protected $m_strBgcolor;
	protected $m_intMediaWidth;
	protected $m_intMediaHeight;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['system_message_template_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemMessageTemplateId', trim( $arrValues['system_message_template_id'] ) ); elseif( isset( $arrValues['system_message_template_id'] ) ) $this->setSystemMessageTemplateId( $arrValues['system_message_template_id'] );
		if( isset( $arrValues['key'] ) && $boolDirectSet ) $this->set( 'm_strKey', trim( stripcslashes( $arrValues['key'] ) ) ); elseif( isset( $arrValues['key'] ) ) $this->setKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['key'] ) : $arrValues['key'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['default_image_name'] ) && $boolDirectSet ) $this->set( 'm_strDefaultImageName', trim( stripcslashes( $arrValues['default_image_name'] ) ) ); elseif( isset( $arrValues['default_image_name'] ) ) $this->setDefaultImageName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['default_image_name'] ) : $arrValues['default_image_name'] );
		if( isset( $arrValues['bgcolor'] ) && $boolDirectSet ) $this->set( 'm_strBgcolor', trim( stripcslashes( $arrValues['bgcolor'] ) ) ); elseif( isset( $arrValues['bgcolor'] ) ) $this->setBgcolor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bgcolor'] ) : $arrValues['bgcolor'] );
		if( isset( $arrValues['media_width'] ) && $boolDirectSet ) $this->set( 'm_intMediaWidth', trim( $arrValues['media_width'] ) ); elseif( isset( $arrValues['media_width'] ) ) $this->setMediaWidth( $arrValues['media_width'] );
		if( isset( $arrValues['media_height'] ) && $boolDirectSet ) $this->set( 'm_intMediaHeight', trim( $arrValues['media_height'] ) ); elseif( isset( $arrValues['media_height'] ) ) $this->setMediaHeight( $arrValues['media_height'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSystemMessageTemplateId( $intSystemMessageTemplateId ) {
		$this->set( 'm_intSystemMessageTemplateId', CStrings::strToIntDef( $intSystemMessageTemplateId, NULL, false ) );
	}

	public function getSystemMessageTemplateId() {
		return $this->m_intSystemMessageTemplateId;
	}

	public function sqlSystemMessageTemplateId() {
		return ( true == isset( $this->m_intSystemMessageTemplateId ) ) ? ( string ) $this->m_intSystemMessageTemplateId : 'NULL';
	}

	public function setKey( $strKey ) {
		$this->set( 'm_strKey', CStrings::strTrimDef( $strKey, 64, NULL, true ) );
	}

	public function getKey() {
		return $this->m_strKey;
	}

	public function sqlKey() {
		return ( true == isset( $this->m_strKey ) ) ? '\'' . addslashes( $this->m_strKey ) . '\'' : 'NULL';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 50, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setDefaultImageName( $strDefaultImageName ) {
		$this->set( 'm_strDefaultImageName', CStrings::strTrimDef( $strDefaultImageName, 255, NULL, true ) );
	}

	public function getDefaultImageName() {
		return $this->m_strDefaultImageName;
	}

	public function sqlDefaultImageName() {
		return ( true == isset( $this->m_strDefaultImageName ) ) ? '\'' . addslashes( $this->m_strDefaultImageName ) . '\'' : 'NULL';
	}

	public function setBgcolor( $strBgcolor ) {
		$this->set( 'm_strBgcolor', CStrings::strTrimDef( $strBgcolor, 20, NULL, true ) );
	}

	public function getBgcolor() {
		return $this->m_strBgcolor;
	}

	public function sqlBgcolor() {
		return ( true == isset( $this->m_strBgcolor ) ) ? '\'' . addslashes( $this->m_strBgcolor ) . '\'' : 'NULL';
	}

	public function setMediaWidth( $intMediaWidth ) {
		$this->set( 'm_intMediaWidth', CStrings::strToIntDef( $intMediaWidth, NULL, false ) );
	}

	public function getMediaWidth() {
		return $this->m_intMediaWidth;
	}

	public function sqlMediaWidth() {
		return ( true == isset( $this->m_intMediaWidth ) ) ? ( string ) $this->m_intMediaWidth : 'NULL';
	}

	public function setMediaHeight( $intMediaHeight ) {
		$this->set( 'm_intMediaHeight', CStrings::strToIntDef( $intMediaHeight, NULL, false ) );
	}

	public function getMediaHeight() {
		return $this->m_intMediaHeight;
	}

	public function sqlMediaHeight() {
		return ( true == isset( $this->m_intMediaHeight ) ) ? ( string ) $this->m_intMediaHeight : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'system_message_template_id' => $this->getSystemMessageTemplateId(),
			'key' => $this->getKey(),
			'title' => $this->getTitle(),
			'default_image_name' => $this->getDefaultImageName(),
			'bgcolor' => $this->getBgcolor(),
			'media_width' => $this->getMediaWidth(),
			'media_height' => $this->getMediaHeight(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>