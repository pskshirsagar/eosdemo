<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFeeTypes
 * Do not add any new functions to this class.
 */

class CBaseFeeTypes extends CEosPluralBase {

	/**
	 * @return CFeeType[]
	 */
	public static function fetchFeeTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CFeeType::class, $objDatabase );
	}

	/**
	 * @return CFeeType
	 */
	public static function fetchFeeType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFeeType::class, $objDatabase );
	}

	public static function fetchFeeTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'fee_types', $objDatabase );
	}

	public static function fetchFeeTypeById( $intId, $objDatabase ) {
		return self::fetchFeeType( sprintf( 'SELECT * FROM fee_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>