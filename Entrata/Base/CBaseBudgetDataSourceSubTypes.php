<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetDataSourceSubTypes
 * Do not add any new functions to this class.
 */

class CBaseBudgetDataSourceSubTypes extends CEosPluralBase {

	/**
	 * @return CBudgetDataSourceSubType[]
	 */
	public static function fetchBudgetDataSourceSubTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CBudgetDataSourceSubType::class, $objDatabase );
	}

	/**
	 * @return CBudgetDataSourceSubType
	 */
	public static function fetchBudgetDataSourceSubType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBudgetDataSourceSubType::class, $objDatabase );
	}

	public static function fetchBudgetDataSourceSubTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'budget_data_source_sub_types', $objDatabase );
	}

	public static function fetchBudgetDataSourceSubTypeById( $intId, $objDatabase ) {
		return self::fetchBudgetDataSourceSubType( sprintf( 'SELECT * FROM budget_data_source_sub_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>