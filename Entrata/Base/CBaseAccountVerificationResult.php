<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAccountVerificationResult extends CEosSingularBase {

	const TABLE_NAME = 'public.account_verification_results';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCompanyMerchantAccountId;
	protected $m_intLeaseId;
	protected $m_intCustomerId;
	protected $m_intAccountVerificationType;
	protected $m_strVerificationResult;
	protected $m_strRejectionReason;
	protected $m_strVerificationDatetime;
	protected $m_intPsProductId;
	protected $m_strCreatedOn;
	protected $m_intCreatedBy;
	protected $m_intGiactRequestLogId;

	public function __construct() {
		parent::__construct();

		$this->m_strVerificationResult = NULL;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['company_merchant_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMerchantAccountId', trim( $arrValues['company_merchant_account_id'] ) ); elseif( isset( $arrValues['company_merchant_account_id'] ) ) $this->setCompanyMerchantAccountId( $arrValues['company_merchant_account_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['account_verification_type'] ) && $boolDirectSet ) $this->set( 'm_intAccountVerificationType', trim( $arrValues['account_verification_type'] ) ); elseif( isset( $arrValues['account_verification_type'] ) ) $this->setAccountVerificationType( $arrValues['account_verification_type'] );
		if( isset( $arrValues['verification_result'] ) && $boolDirectSet ) $this->set( 'm_strVerificationResult', trim( stripcslashes( $arrValues['verification_result'] ) ) ); elseif( isset( $arrValues['verification_result'] ) ) $this->setVerificationResult( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['verification_result'] ) : $arrValues['verification_result'] );
		if( isset( $arrValues['rejection_reason'] ) && $boolDirectSet ) $this->set( 'm_strRejectionReason', trim( stripcslashes( $arrValues['rejection_reason'] ) ) ); elseif( isset( $arrValues['rejection_reason'] ) ) $this->setRejectionReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['rejection_reason'] ) : $arrValues['rejection_reason'] );
		if( isset( $arrValues['verification_datetime'] ) && $boolDirectSet ) $this->set( 'm_strVerificationDatetime', trim( $arrValues['verification_datetime'] ) ); elseif( isset( $arrValues['verification_datetime'] ) ) $this->setVerificationDatetime( $arrValues['verification_datetime'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['giact_request_log_id'] ) && $boolDirectSet ) $this->set( 'm_intGiactRequestLogId', trim( $arrValues['giact_request_log_id'] ) ); elseif( isset( $arrValues['giact_request_log_id'] ) ) $this->setGiactRequestLogId( $arrValues['giact_request_log_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCompanyMerchantAccountId( $intCompanyMerchantAccountId ) {
		$this->set( 'm_intCompanyMerchantAccountId', CStrings::strToIntDef( $intCompanyMerchantAccountId, NULL, false ) );
	}

	public function getCompanyMerchantAccountId() {
		return $this->m_intCompanyMerchantAccountId;
	}

	public function sqlCompanyMerchantAccountId() {
		return ( true == isset( $this->m_intCompanyMerchantAccountId ) ) ? ( string ) $this->m_intCompanyMerchantAccountId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setAccountVerificationType( $intAccountVerificationType ) {
		$this->set( 'm_intAccountVerificationType', CStrings::strToIntDef( $intAccountVerificationType, NULL, false ) );
	}

	public function getAccountVerificationType() {
		return $this->m_intAccountVerificationType;
	}

	public function sqlAccountVerificationType() {
		return ( true == isset( $this->m_intAccountVerificationType ) ) ? ( string ) $this->m_intAccountVerificationType : 'NULL';
	}

	public function setVerificationResult( $strVerificationResult ) {
		$this->set( 'm_strVerificationResult', CStrings::strTrimDef( $strVerificationResult, 25, NULL, true ) );
	}

	public function getVerificationResult() {
		return $this->m_strVerificationResult;
	}

	public function sqlVerificationResult() {
		return ( true == isset( $this->m_strVerificationResult ) ) ? '\'' . addslashes( $this->m_strVerificationResult ) . '\'' : '\'NULL\'';
	}

	public function setRejectionReason( $strRejectionReason ) {
		$this->set( 'm_strRejectionReason', CStrings::strTrimDef( $strRejectionReason, -1, NULL, true ) );
	}

	public function getRejectionReason() {
		return $this->m_strRejectionReason;
	}

	public function sqlRejectionReason() {
		return ( true == isset( $this->m_strRejectionReason ) ) ? '\'' . addslashes( $this->m_strRejectionReason ) . '\'' : 'NULL';
	}

	public function setVerificationDatetime( $strVerificationDatetime ) {
		$this->set( 'm_strVerificationDatetime', CStrings::strTrimDef( $strVerificationDatetime, -1, NULL, true ) );
	}

	public function getVerificationDatetime() {
		return $this->m_strVerificationDatetime;
	}

	public function sqlVerificationDatetime() {
		return ( true == isset( $this->m_strVerificationDatetime ) ) ? '\'' . $this->m_strVerificationDatetime . '\'' : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setGiactRequestLogId( $intGiactRequestLogId ) {
		$this->set( 'm_intGiactRequestLogId', CStrings::strToIntDef( $intGiactRequestLogId, NULL, false ) );
	}

	public function getGiactRequestLogId() {
		return $this->m_intGiactRequestLogId;
	}

	public function sqlGiactRequestLogId() {
		return ( true == isset( $this->m_intGiactRequestLogId ) ) ? ( string ) $this->m_intGiactRequestLogId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, company_merchant_account_id, lease_id, customer_id, account_verification_type, verification_result, rejection_reason, verification_datetime, ps_product_id, created_on, created_by, giact_request_log_id )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlCompanyMerchantAccountId() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlAccountVerificationType() . ', ' .
 						$this->sqlVerificationResult() . ', ' .
 						$this->sqlRejectionReason() . ', ' .
 						$this->sqlVerificationDatetime() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlGiactRequestLogId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_merchant_account_id = ' . $this->sqlCompanyMerchantAccountId() . ','; } elseif( true == array_key_exists( 'CompanyMerchantAccountId', $this->getChangedColumns() ) ) { $strSql .= ' company_merchant_account_id = ' . $this->sqlCompanyMerchantAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_verification_type = ' . $this->sqlAccountVerificationType() . ','; } elseif( true == array_key_exists( 'AccountVerificationType', $this->getChangedColumns() ) ) { $strSql .= ' account_verification_type = ' . $this->sqlAccountVerificationType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' verification_result = ' . $this->sqlVerificationResult() . ','; } elseif( true == array_key_exists( 'VerificationResult', $this->getChangedColumns() ) ) { $strSql .= ' verification_result = ' . $this->sqlVerificationResult() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejection_reason = ' . $this->sqlRejectionReason() . ','; } elseif( true == array_key_exists( 'RejectionReason', $this->getChangedColumns() ) ) { $strSql .= ' rejection_reason = ' . $this->sqlRejectionReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' verification_datetime = ' . $this->sqlVerificationDatetime() . ','; } elseif( true == array_key_exists( 'VerificationDatetime', $this->getChangedColumns() ) ) { $strSql .= ' verification_datetime = ' . $this->sqlVerificationDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' giact_request_log_id = ' . $this->sqlGiactRequestLogId() . ','; } elseif( true == array_key_exists( 'GiactRequestLogId', $this->getChangedColumns() ) ) { $strSql .= ' giact_request_log_id = ' . $this->sqlGiactRequestLogId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'company_merchant_account_id' => $this->getCompanyMerchantAccountId(),
			'lease_id' => $this->getLeaseId(),
			'customer_id' => $this->getCustomerId(),
			'account_verification_type' => $this->getAccountVerificationType(),
			'verification_result' => $this->getVerificationResult(),
			'rejection_reason' => $this->getRejectionReason(),
			'verification_datetime' => $this->getVerificationDatetime(),
			'ps_product_id' => $this->getPsProductId(),
			'created_on' => $this->getCreatedOn(),
			'created_by' => $this->getCreatedBy(),
			'giact_request_log_id' => $this->getGiactRequestLogId()
		);
	}

}
?>