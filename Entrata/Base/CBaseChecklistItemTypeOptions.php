<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChecklistItemTypeOptions
 * Do not add any new functions to this class.
 */

class CBaseChecklistItemTypeOptions extends CEosPluralBase {

	/**
	 * @return CChecklistItemTypeOption[]
	 */
	public static function fetchChecklistItemTypeOptions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CChecklistItemTypeOption::class, $objDatabase );
	}

	/**
	 * @return CChecklistItemTypeOption
	 */
	public static function fetchChecklistItemTypeOption( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CChecklistItemTypeOption::class, $objDatabase );
	}

	public static function fetchChecklistItemTypeOptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'checklist_item_type_options', $objDatabase );
	}

	public static function fetchChecklistItemTypeOptionById( $intId, $objDatabase ) {
		return self::fetchChecklistItemTypeOption( sprintf( 'SELECT * FROM checklist_item_type_options WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchChecklistItemTypeOptionsByChecklistItemTypeId( $intChecklistItemTypeId, $objDatabase ) {
		return self::fetchChecklistItemTypeOptions( sprintf( 'SELECT * FROM checklist_item_type_options WHERE checklist_item_type_id = %d', $intChecklistItemTypeId ), $objDatabase );
	}

}
?>