<?php

class CBaseDefaultApPayee extends CEosSingularBase {

	const TABLE_NAME = 'public.default_ap_payees';

	protected $m_intId;
	protected $m_intApPayeeTypeId;
	protected $m_intDefaultApPayeeTermId;
	protected $m_strGlAccountSystemCode;
	protected $m_strSystemCode;
	protected $m_strCompanyName;
	protected $m_intReceives1099;
	protected $m_intIsConsolidated;
	protected $m_intIsSystem;

	public function __construct() {
		parent::__construct();

		$this->m_intReceives1099 = '0';
		$this->m_intIsConsolidated = '1';
		$this->m_intIsSystem = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ap_payee_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeTypeId', trim( $arrValues['ap_payee_type_id'] ) ); elseif( isset( $arrValues['ap_payee_type_id'] ) ) $this->setApPayeeTypeId( $arrValues['ap_payee_type_id'] );
		if( isset( $arrValues['default_ap_payee_term_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultApPayeeTermId', trim( $arrValues['default_ap_payee_term_id'] ) ); elseif( isset( $arrValues['default_ap_payee_term_id'] ) ) $this->setDefaultApPayeeTermId( $arrValues['default_ap_payee_term_id'] );
		if( isset( $arrValues['gl_account_system_code'] ) && $boolDirectSet ) $this->set( 'm_strGlAccountSystemCode', trim( stripcslashes( $arrValues['gl_account_system_code'] ) ) ); elseif( isset( $arrValues['gl_account_system_code'] ) ) $this->setGlAccountSystemCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gl_account_system_code'] ) : $arrValues['gl_account_system_code'] );
		if( isset( $arrValues['system_code'] ) && $boolDirectSet ) $this->set( 'm_strSystemCode', trim( stripcslashes( $arrValues['system_code'] ) ) ); elseif( isset( $arrValues['system_code'] ) ) $this->setSystemCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['system_code'] ) : $arrValues['system_code'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( stripcslashes( $arrValues['company_name'] ) ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_name'] ) : $arrValues['company_name'] );
		if( isset( $arrValues['receives_1099'] ) && $boolDirectSet ) $this->set( 'm_intReceives1099', trim( $arrValues['receives_1099'] ) ); elseif( isset( $arrValues['receives_1099'] ) ) $this->setReceives1099( $arrValues['receives_1099'] );
		if( isset( $arrValues['is_consolidated'] ) && $boolDirectSet ) $this->set( 'm_intIsConsolidated', trim( $arrValues['is_consolidated'] ) ); elseif( isset( $arrValues['is_consolidated'] ) ) $this->setIsConsolidated( $arrValues['is_consolidated'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setApPayeeTypeId( $intApPayeeTypeId ) {
		$this->set( 'm_intApPayeeTypeId', CStrings::strToIntDef( $intApPayeeTypeId, NULL, false ) );
	}

	public function getApPayeeTypeId() {
		return $this->m_intApPayeeTypeId;
	}

	public function sqlApPayeeTypeId() {
		return ( true == isset( $this->m_intApPayeeTypeId ) ) ? ( string ) $this->m_intApPayeeTypeId : 'NULL';
	}

	public function setDefaultApPayeeTermId( $intDefaultApPayeeTermId ) {
		$this->set( 'm_intDefaultApPayeeTermId', CStrings::strToIntDef( $intDefaultApPayeeTermId, NULL, false ) );
	}

	public function getDefaultApPayeeTermId() {
		return $this->m_intDefaultApPayeeTermId;
	}

	public function sqlDefaultApPayeeTermId() {
		return ( true == isset( $this->m_intDefaultApPayeeTermId ) ) ? ( string ) $this->m_intDefaultApPayeeTermId : 'NULL';
	}

	public function setGlAccountSystemCode( $strGlAccountSystemCode ) {
		$this->set( 'm_strGlAccountSystemCode', CStrings::strTrimDef( $strGlAccountSystemCode, 10, NULL, true ) );
	}

	public function getGlAccountSystemCode() {
		return $this->m_strGlAccountSystemCode;
	}

	public function sqlGlAccountSystemCode() {
		return ( true == isset( $this->m_strGlAccountSystemCode ) ) ? '\'' . addslashes( $this->m_strGlAccountSystemCode ) . '\'' : 'NULL';
	}

	public function setSystemCode( $strSystemCode ) {
		$this->set( 'm_strSystemCode', CStrings::strTrimDef( $strSystemCode, 10, NULL, true ) );
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	public function sqlSystemCode() {
		return ( true == isset( $this->m_strSystemCode ) ) ? '\'' . addslashes( $this->m_strSystemCode ) . '\'' : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 100, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? '\'' . addslashes( $this->m_strCompanyName ) . '\'' : 'NULL';
	}

	public function setReceives1099( $intReceives1099 ) {
		$this->set( 'm_intReceives1099', CStrings::strToIntDef( $intReceives1099, NULL, false ) );
	}

	public function getReceives1099() {
		return $this->m_intReceives1099;
	}

	public function sqlReceives1099() {
		return ( true == isset( $this->m_intReceives1099 ) ) ? ( string ) $this->m_intReceives1099 : '0';
	}

	public function setIsConsolidated( $intIsConsolidated ) {
		$this->set( 'm_intIsConsolidated', CStrings::strToIntDef( $intIsConsolidated, NULL, false ) );
	}

	public function getIsConsolidated() {
		return $this->m_intIsConsolidated;
	}

	public function sqlIsConsolidated() {
		return ( true == isset( $this->m_intIsConsolidated ) ) ? ( string ) $this->m_intIsConsolidated : '1';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ap_payee_type_id' => $this->getApPayeeTypeId(),
			'default_ap_payee_term_id' => $this->getDefaultApPayeeTermId(),
			'gl_account_system_code' => $this->getGlAccountSystemCode(),
			'system_code' => $this->getSystemCode(),
			'company_name' => $this->getCompanyName(),
			'receives_1099' => $this->getReceives1099(),
			'is_consolidated' => $this->getIsConsolidated(),
			'is_system' => $this->getIsSystem()
		);
	}

}
?>