<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlAccountTypes
 * Do not add any new functions to this class.
 */

class CBaseGlAccountTypes extends CEosPluralBase {

	/**
	 * @return CGlAccountType[]
	 */
	public static function fetchGlAccountTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CGlAccountType::class, $objDatabase );
	}

	/**
	 * @return CGlAccountType
	 */
	public static function fetchGlAccountType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGlAccountType::class, $objDatabase );
	}

	public static function fetchGlAccountTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_account_types', $objDatabase );
	}

	public static function fetchGlAccountTypeById( $intId, $objDatabase ) {
		return self::fetchGlAccountType( sprintf( 'SELECT * FROM gl_account_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>