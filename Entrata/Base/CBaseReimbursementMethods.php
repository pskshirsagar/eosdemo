<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReimbursementMethods
 * Do not add any new functions to this class.
 */

class CBaseReimbursementMethods extends CEosPluralBase {

	/**
	 * @return CReimbursementMethod[]
	 */
	public static function fetchReimbursementMethods( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReimbursementMethod::class, $objDatabase );
	}

	/**
	 * @return CReimbursementMethod
	 */
	public static function fetchReimbursementMethod( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReimbursementMethod::class, $objDatabase );
	}

	public static function fetchReimbursementMethodCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'reimbursement_methods', $objDatabase );
	}

	public static function fetchReimbursementMethodById( $intId, $objDatabase ) {
		return self::fetchReimbursementMethod( sprintf( 'SELECT * FROM reimbursement_methods WHERE id = %d', $intId ), $objDatabase );
	}

}
?>