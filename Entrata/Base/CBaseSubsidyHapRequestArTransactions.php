<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyHapRequestArTransactions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSubsidyHapRequestArTransactions extends CEosPluralBase {

	/**
	 * @return CSubsidyHapRequestArTransaction[]
	 */
	public static function fetchSubsidyHapRequestArTransactions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CSubsidyHapRequestArTransaction::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CSubsidyHapRequestArTransaction
	 */
	public static function fetchSubsidyHapRequestArTransaction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyHapRequestArTransaction::class, $objDatabase );
	}

	public static function fetchSubsidyHapRequestArTransactionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_hap_request_ar_transactions', $objDatabase );
	}

	public static function fetchSubsidyHapRequestArTransactionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchSubsidyHapRequestArTransaction( sprintf( 'SELECT * FROM subsidy_hap_request_ar_transactions WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchSubsidyHapRequestArTransactionsByCid( $intCid, $objDatabase ) {
		return self::fetchSubsidyHapRequestArTransactions( sprintf( 'SELECT * FROM subsidy_hap_request_ar_transactions WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchSubsidyHapRequestArTransactionsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchSubsidyHapRequestArTransactions( sprintf( 'SELECT * FROM subsidy_hap_request_ar_transactions WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchSubsidyHapRequestArTransactionsBySubsidyHapRequestIdByCid( $intSubsidyHapRequestId, $intCid, $objDatabase ) {
		return self::fetchSubsidyHapRequestArTransactions( sprintf( 'SELECT * FROM subsidy_hap_request_ar_transactions WHERE subsidy_hap_request_id = %d AND cid = %d', $intSubsidyHapRequestId, $intCid ), $objDatabase );
	}

	public static function fetchSubsidyHapRequestArTransactionsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchSubsidyHapRequestArTransactions( sprintf( 'SELECT * FROM subsidy_hap_request_ar_transactions WHERE lease_id = %d AND cid = %d', $intLeaseId, $intCid ), $objDatabase );
	}

	public static function fetchSubsidyHapRequestArTransactionsByArTransactionIdByCid( $intArTransactionId, $intCid, $objDatabase ) {
		return self::fetchSubsidyHapRequestArTransactions( sprintf( 'SELECT * FROM subsidy_hap_request_ar_transactions WHERE ar_transaction_id = %d AND cid = %d', $intArTransactionId, $intCid ), $objDatabase );
	}

	public static function fetchSubsidyHapRequestArTransactionsBySubsidyCertificationIdByCid( $intSubsidyCertificationId, $intCid, $objDatabase ) {
		return self::fetchSubsidyHapRequestArTransactions( sprintf( 'SELECT * FROM subsidy_hap_request_ar_transactions WHERE subsidy_certification_id = %d AND cid = %d', $intSubsidyCertificationId, $intCid ), $objDatabase );
	}

}
?>
