<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CWebsiteTypes
 * Do not add any new functions to this class.
 */

class CBaseWebsiteTypes extends CEosPluralBase {

	/**
	 * @return CWebsiteType[]
	 */
	public static function fetchWebsiteTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CWebsiteType::class, $objDatabase );
	}

	/**
	 * @return CWebsiteType
	 */
	public static function fetchWebsiteType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CWebsiteType::class, $objDatabase );
	}

	public static function fetchWebsiteTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'website_types', $objDatabase );
	}

	public static function fetchWebsiteTypeById( $intId, $objDatabase ) {
		return self::fetchWebsiteType( sprintf( 'SELECT * FROM website_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>