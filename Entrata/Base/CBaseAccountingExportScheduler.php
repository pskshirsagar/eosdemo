<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAccountingExportScheduler extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.accounting_export_schedulers';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intTransmissionVendorId;
	protected $m_intAccountingExportFileFormatTypeId;
	protected $m_intCompanyTransmissionVendorId;
	protected $m_arrintBankAccountIds;
	protected $m_intFrequencyId;
	protected $m_intFrequencyInterval;
	protected $m_intNumberOfOccurrences;
	protected $m_strWeekDays;
	protected $m_strMonthDays;
	protected $m_strFileNamePrefix;
	protected $m_strFileNameSuffix;
	protected $m_strExportName;
	protected $m_strEmailAddresses;
	protected $m_strLastPostedDate;
	protected $m_strNextPostDate;
	protected $m_strScheduledStartOn;
	protected $m_strScheduledEndOn;
	protected $m_strStartingPostMonth;
	protected $m_boolIncludeAllUnexportedTransactions;
	protected $m_boolIncludeFutureTransactions;
	protected $m_boolIsZipFiles;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIncludeAllUnexportedTransactions = false;
		$this->m_boolIncludeFutureTransactions = false;
		$this->m_boolIsZipFiles = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['transmission_vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionVendorId', trim( $arrValues['transmission_vendor_id'] ) ); elseif( isset( $arrValues['transmission_vendor_id'] ) ) $this->setTransmissionVendorId( $arrValues['transmission_vendor_id'] );
		if( isset( $arrValues['accounting_export_file_format_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountingExportFileFormatTypeId', trim( $arrValues['accounting_export_file_format_type_id'] ) ); elseif( isset( $arrValues['accounting_export_file_format_type_id'] ) ) $this->setAccountingExportFileFormatTypeId( $arrValues['accounting_export_file_format_type_id'] );
		if( isset( $arrValues['company_transmission_vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyTransmissionVendorId', trim( $arrValues['company_transmission_vendor_id'] ) ); elseif( isset( $arrValues['company_transmission_vendor_id'] ) ) $this->setCompanyTransmissionVendorId( $arrValues['company_transmission_vendor_id'] );
		if( isset( $arrValues['bank_account_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintBankAccountIds', trim( $arrValues['bank_account_ids'] ) ); elseif( isset( $arrValues['bank_account_ids'] ) ) $this->setBankAccountIds( $arrValues['bank_account_ids'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['frequency_interval'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyInterval', trim( $arrValues['frequency_interval'] ) ); elseif( isset( $arrValues['frequency_interval'] ) ) $this->setFrequencyInterval( $arrValues['frequency_interval'] );
		if( isset( $arrValues['number_of_occurrences'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfOccurrences', trim( $arrValues['number_of_occurrences'] ) ); elseif( isset( $arrValues['number_of_occurrences'] ) ) $this->setNumberOfOccurrences( $arrValues['number_of_occurrences'] );
		if( isset( $arrValues['week_days'] ) && $boolDirectSet ) $this->set( 'm_strWeekDays', trim( stripcslashes( $arrValues['week_days'] ) ) ); elseif( isset( $arrValues['week_days'] ) ) $this->setWeekDays( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['week_days'] ) : $arrValues['week_days'] );
		if( isset( $arrValues['month_days'] ) && $boolDirectSet ) $this->set( 'm_strMonthDays', trim( stripcslashes( $arrValues['month_days'] ) ) ); elseif( isset( $arrValues['month_days'] ) ) $this->setMonthDays( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['month_days'] ) : $arrValues['month_days'] );
		if( isset( $arrValues['file_name_prefix'] ) && $boolDirectSet ) $this->set( 'm_strFileNamePrefix', trim( stripcslashes( $arrValues['file_name_prefix'] ) ) ); elseif( isset( $arrValues['file_name_prefix'] ) ) $this->setFileNamePrefix( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name_prefix'] ) : $arrValues['file_name_prefix'] );
		if( isset( $arrValues['file_name_suffix'] ) && $boolDirectSet ) $this->set( 'm_strFileNameSuffix', trim( stripcslashes( $arrValues['file_name_suffix'] ) ) ); elseif( isset( $arrValues['file_name_suffix'] ) ) $this->setFileNameSuffix( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name_suffix'] ) : $arrValues['file_name_suffix'] );
		if( isset( $arrValues['export_name'] ) && $boolDirectSet ) $this->set( 'm_strExportName', trim( stripcslashes( $arrValues['export_name'] ) ) ); elseif( isset( $arrValues['export_name'] ) ) $this->setExportName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['export_name'] ) : $arrValues['export_name'] );
		if( isset( $arrValues['email_addresses'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddresses', trim( stripcslashes( $arrValues['email_addresses'] ) ) ); elseif( isset( $arrValues['email_addresses'] ) ) $this->setEmailAddresses( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_addresses'] ) : $arrValues['email_addresses'] );
		if( isset( $arrValues['last_posted_date'] ) && $boolDirectSet ) $this->set( 'm_strLastPostedDate', trim( $arrValues['last_posted_date'] ) ); elseif( isset( $arrValues['last_posted_date'] ) ) $this->setLastPostedDate( $arrValues['last_posted_date'] );
		if( isset( $arrValues['next_post_date'] ) && $boolDirectSet ) $this->set( 'm_strNextPostDate', trim( $arrValues['next_post_date'] ) ); elseif( isset( $arrValues['next_post_date'] ) ) $this->setNextPostDate( $arrValues['next_post_date'] );
		if( isset( $arrValues['scheduled_start_on'] ) && $boolDirectSet ) $this->set( 'm_strScheduledStartOn', trim( $arrValues['scheduled_start_on'] ) ); elseif( isset( $arrValues['scheduled_start_on'] ) ) $this->setScheduledStartOn( $arrValues['scheduled_start_on'] );
		if( isset( $arrValues['scheduled_end_on'] ) && $boolDirectSet ) $this->set( 'm_strScheduledEndOn', trim( $arrValues['scheduled_end_on'] ) ); elseif( isset( $arrValues['scheduled_end_on'] ) ) $this->setScheduledEndOn( $arrValues['scheduled_end_on'] );
		if( isset( $arrValues['starting_post_month'] ) && $boolDirectSet ) $this->set( 'm_strStartingPostMonth', trim( $arrValues['starting_post_month'] ) ); elseif( isset( $arrValues['starting_post_month'] ) ) $this->setStartingPostMonth( $arrValues['starting_post_month'] );
		if( isset( $arrValues['include_all_unexported_transactions'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeAllUnexportedTransactions', trim( stripcslashes( $arrValues['include_all_unexported_transactions'] ) ) ); elseif( isset( $arrValues['include_all_unexported_transactions'] ) ) $this->setIncludeAllUnexportedTransactions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_all_unexported_transactions'] ) : $arrValues['include_all_unexported_transactions'] );
		if( isset( $arrValues['include_future_transactions'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeFutureTransactions', trim( stripcslashes( $arrValues['include_future_transactions'] ) ) ); elseif( isset( $arrValues['include_future_transactions'] ) ) $this->setIncludeFutureTransactions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_future_transactions'] ) : $arrValues['include_future_transactions'] );
		if( isset( $arrValues['is_zip_files'] ) && $boolDirectSet ) $this->set( 'm_boolIsZipFiles', trim( stripcslashes( $arrValues['is_zip_files'] ) ) ); elseif( isset( $arrValues['is_zip_files'] ) ) $this->setIsZipFiles( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_zip_files'] ) : $arrValues['is_zip_files'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setTransmissionVendorId( $intTransmissionVendorId ) {
		$this->set( 'm_intTransmissionVendorId', CStrings::strToIntDef( $intTransmissionVendorId, NULL, false ) );
	}

	public function getTransmissionVendorId() {
		return $this->m_intTransmissionVendorId;
	}

	public function sqlTransmissionVendorId() {
		return ( true == isset( $this->m_intTransmissionVendorId ) ) ? ( string ) $this->m_intTransmissionVendorId : 'NULL';
	}

	public function setAccountingExportFileFormatTypeId( $intAccountingExportFileFormatTypeId ) {
		$this->set( 'm_intAccountingExportFileFormatTypeId', CStrings::strToIntDef( $intAccountingExportFileFormatTypeId, NULL, false ) );
	}

	public function getAccountingExportFileFormatTypeId() {
		return $this->m_intAccountingExportFileFormatTypeId;
	}

	public function sqlAccountingExportFileFormatTypeId() {
		return ( true == isset( $this->m_intAccountingExportFileFormatTypeId ) ) ? ( string ) $this->m_intAccountingExportFileFormatTypeId : 'NULL';
	}

	public function setCompanyTransmissionVendorId( $intCompanyTransmissionVendorId ) {
		$this->set( 'm_intCompanyTransmissionVendorId', CStrings::strToIntDef( $intCompanyTransmissionVendorId, NULL, false ) );
	}

	public function getCompanyTransmissionVendorId() {
		return $this->m_intCompanyTransmissionVendorId;
	}

	public function sqlCompanyTransmissionVendorId() {
		return ( true == isset( $this->m_intCompanyTransmissionVendorId ) ) ? ( string ) $this->m_intCompanyTransmissionVendorId : 'NULL';
	}

	public function setBankAccountIds( $arrintBankAccountIds ) {
		$this->set( 'm_arrintBankAccountIds', CStrings::strToArrIntDef( $arrintBankAccountIds, NULL ) );
	}

	public function getBankAccountIds() {
		return $this->m_arrintBankAccountIds;
	}

	public function sqlBankAccountIds() {
		return ( true == isset( $this->m_arrintBankAccountIds ) && true == valArr( $this->m_arrintBankAccountIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintBankAccountIds, NULL ) . '\'' : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : 'NULL';
	}

	public function setFrequencyInterval( $intFrequencyInterval ) {
		$this->set( 'm_intFrequencyInterval', CStrings::strToIntDef( $intFrequencyInterval, NULL, false ) );
	}

	public function getFrequencyInterval() {
		return $this->m_intFrequencyInterval;
	}

	public function sqlFrequencyInterval() {
		return ( true == isset( $this->m_intFrequencyInterval ) ) ? ( string ) $this->m_intFrequencyInterval : 'NULL';
	}

	public function setNumberOfOccurrences( $intNumberOfOccurrences ) {
		$this->set( 'm_intNumberOfOccurrences', CStrings::strToIntDef( $intNumberOfOccurrences, NULL, false ) );
	}

	public function getNumberOfOccurrences() {
		return $this->m_intNumberOfOccurrences;
	}

	public function sqlNumberOfOccurrences() {
		return ( true == isset( $this->m_intNumberOfOccurrences ) ) ? ( string ) $this->m_intNumberOfOccurrences : 'NULL';
	}

	public function setWeekDays( $strWeekDays ) {
		$this->set( 'm_strWeekDays', CStrings::strTrimDef( $strWeekDays, 240, NULL, true ) );
	}

	public function getWeekDays() {
		return $this->m_strWeekDays;
	}

	public function sqlWeekDays() {
		return ( true == isset( $this->m_strWeekDays ) ) ? '\'' . addslashes( $this->m_strWeekDays ) . '\'' : 'NULL';
	}

	public function setMonthDays( $strMonthDays ) {
		$this->set( 'm_strMonthDays', CStrings::strTrimDef( $strMonthDays, 240, NULL, true ) );
	}

	public function getMonthDays() {
		return $this->m_strMonthDays;
	}

	public function sqlMonthDays() {
		return ( true == isset( $this->m_strMonthDays ) ) ? '\'' . addslashes( $this->m_strMonthDays ) . '\'' : 'NULL';
	}

	public function setFileNamePrefix( $strFileNamePrefix ) {
		$this->set( 'm_strFileNamePrefix', CStrings::strTrimDef( $strFileNamePrefix, 100, NULL, true ) );
	}

	public function getFileNamePrefix() {
		return $this->m_strFileNamePrefix;
	}

	public function sqlFileNamePrefix() {
		return ( true == isset( $this->m_strFileNamePrefix ) ) ? '\'' . addslashes( $this->m_strFileNamePrefix ) . '\'' : 'NULL';
	}

	public function setFileNameSuffix( $strFileNameSuffix ) {
		$this->set( 'm_strFileNameSuffix', CStrings::strTrimDef( $strFileNameSuffix, 100, NULL, true ) );
	}

	public function getFileNameSuffix() {
		return $this->m_strFileNameSuffix;
	}

	public function sqlFileNameSuffix() {
		return ( true == isset( $this->m_strFileNameSuffix ) ) ? '\'' . addslashes( $this->m_strFileNameSuffix ) . '\'' : 'NULL';
	}

	public function setExportName( $strExportName ) {
		$this->set( 'm_strExportName', CStrings::strTrimDef( $strExportName, 100, NULL, true ) );
	}

	public function getExportName() {
		return $this->m_strExportName;
	}

	public function sqlExportName() {
		return ( true == isset( $this->m_strExportName ) ) ? '\'' . addslashes( $this->m_strExportName ) . '\'' : 'NULL';
	}

	public function setEmailAddresses( $strEmailAddresses ) {
		$this->set( 'm_strEmailAddresses', CStrings::strTrimDef( $strEmailAddresses, -1, NULL, true ) );
	}

	public function getEmailAddresses() {
		return $this->m_strEmailAddresses;
	}

	public function sqlEmailAddresses() {
		return ( true == isset( $this->m_strEmailAddresses ) ) ? '\'' . addslashes( $this->m_strEmailAddresses ) . '\'' : 'NULL';
	}

	public function setLastPostedDate( $strLastPostedDate ) {
		$this->set( 'm_strLastPostedDate', CStrings::strTrimDef( $strLastPostedDate, -1, NULL, true ) );
	}

	public function getLastPostedDate() {
		return $this->m_strLastPostedDate;
	}

	public function sqlLastPostedDate() {
		return ( true == isset( $this->m_strLastPostedDate ) ) ? '\'' . $this->m_strLastPostedDate . '\'' : 'NULL';
	}

	public function setNextPostDate( $strNextPostDate ) {
		$this->set( 'm_strNextPostDate', CStrings::strTrimDef( $strNextPostDate, -1, NULL, true ) );
	}

	public function getNextPostDate() {
		return $this->m_strNextPostDate;
	}

	public function sqlNextPostDate() {
		return ( true == isset( $this->m_strNextPostDate ) ) ? '\'' . $this->m_strNextPostDate . '\'' : 'NULL';
	}

	public function setScheduledStartOn( $strScheduledStartOn ) {
		$this->set( 'm_strScheduledStartOn', CStrings::strTrimDef( $strScheduledStartOn, -1, NULL, true ) );
	}

	public function getScheduledStartOn() {
		return $this->m_strScheduledStartOn;
	}

	public function sqlScheduledStartOn() {
		return ( true == isset( $this->m_strScheduledStartOn ) ) ? '\'' . $this->m_strScheduledStartOn . '\'' : 'NOW()';
	}

	public function setScheduledEndOn( $strScheduledEndOn ) {
		$this->set( 'm_strScheduledEndOn', CStrings::strTrimDef( $strScheduledEndOn, -1, NULL, true ) );
	}

	public function getScheduledEndOn() {
		return $this->m_strScheduledEndOn;
	}

	public function sqlScheduledEndOn() {
		return ( true == isset( $this->m_strScheduledEndOn ) ) ? '\'' . $this->m_strScheduledEndOn . '\'' : 'NULL';
	}

	public function setStartingPostMonth( $strStartingPostMonth ) {
		$this->set( 'm_strStartingPostMonth', CStrings::strTrimDef( $strStartingPostMonth, -1, NULL, true ) );
	}

	public function getStartingPostMonth() {
		return $this->m_strStartingPostMonth;
	}

	public function sqlStartingPostMonth() {
		return ( true == isset( $this->m_strStartingPostMonth ) ) ? '\'' . $this->m_strStartingPostMonth . '\'' : 'NULL';
	}

	public function setIncludeAllUnexportedTransactions( $boolIncludeAllUnexportedTransactions ) {
		$this->set( 'm_boolIncludeAllUnexportedTransactions', CStrings::strToBool( $boolIncludeAllUnexportedTransactions ) );
	}

	public function getIncludeAllUnexportedTransactions() {
		return $this->m_boolIncludeAllUnexportedTransactions;
	}

	public function sqlIncludeAllUnexportedTransactions() {
		return ( true == isset( $this->m_boolIncludeAllUnexportedTransactions ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeAllUnexportedTransactions ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIncludeFutureTransactions( $boolIncludeFutureTransactions ) {
		$this->set( 'm_boolIncludeFutureTransactions', CStrings::strToBool( $boolIncludeFutureTransactions ) );
	}

	public function getIncludeFutureTransactions() {
		return $this->m_boolIncludeFutureTransactions;
	}

	public function sqlIncludeFutureTransactions() {
		return ( true == isset( $this->m_boolIncludeFutureTransactions ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeFutureTransactions ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsZipFiles( $boolIsZipFiles ) {
		$this->set( 'm_boolIsZipFiles', CStrings::strToBool( $boolIsZipFiles ) );
	}

	public function getIsZipFiles() {
		return $this->m_boolIsZipFiles;
	}

	public function sqlIsZipFiles() {
		return ( true == isset( $this->m_boolIsZipFiles ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsZipFiles ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, transmission_vendor_id, accounting_export_file_format_type_id, company_transmission_vendor_id, bank_account_ids, frequency_id, frequency_interval, number_of_occurrences, week_days, month_days, file_name_prefix, file_name_suffix, export_name, email_addresses, last_posted_date, next_post_date, scheduled_start_on, scheduled_end_on, starting_post_month, include_all_unexported_transactions, include_future_transactions, is_zip_files, details, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlTransmissionVendorId() . ', ' .
						$this->sqlAccountingExportFileFormatTypeId() . ', ' .
						$this->sqlCompanyTransmissionVendorId() . ', ' .
						$this->sqlBankAccountIds() . ', ' .
						$this->sqlFrequencyId() . ', ' .
						$this->sqlFrequencyInterval() . ', ' .
						$this->sqlNumberOfOccurrences() . ', ' .
						$this->sqlWeekDays() . ', ' .
						$this->sqlMonthDays() . ', ' .
						$this->sqlFileNamePrefix() . ', ' .
						$this->sqlFileNameSuffix() . ', ' .
						$this->sqlExportName() . ', ' .
						$this->sqlEmailAddresses() . ', ' .
						$this->sqlLastPostedDate() . ', ' .
						$this->sqlNextPostDate() . ', ' .
						$this->sqlScheduledStartOn() . ', ' .
						$this->sqlScheduledEndOn() . ', ' .
						$this->sqlStartingPostMonth() . ', ' .
						$this->sqlIncludeAllUnexportedTransactions() . ', ' .
						$this->sqlIncludeFutureTransactions() . ', ' .
						$this->sqlIsZipFiles() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId(). ',' ; } elseif( true == array_key_exists( 'TransmissionVendorId', $this->getChangedColumns() ) ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accounting_export_file_format_type_id = ' . $this->sqlAccountingExportFileFormatTypeId(). ',' ; } elseif( true == array_key_exists( 'AccountingExportFileFormatTypeId', $this->getChangedColumns() ) ) { $strSql .= ' accounting_export_file_format_type_id = ' . $this->sqlAccountingExportFileFormatTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_transmission_vendor_id = ' . $this->sqlCompanyTransmissionVendorId(). ',' ; } elseif( true == array_key_exists( 'CompanyTransmissionVendorId', $this->getChangedColumns() ) ) { $strSql .= ' company_transmission_vendor_id = ' . $this->sqlCompanyTransmissionVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_account_ids = ' . $this->sqlBankAccountIds(). ',' ; } elseif( true == array_key_exists( 'BankAccountIds', $this->getChangedColumns() ) ) { $strSql .= ' bank_account_ids = ' . $this->sqlBankAccountIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId(). ',' ; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_interval = ' . $this->sqlFrequencyInterval(). ',' ; } elseif( true == array_key_exists( 'FrequencyInterval', $this->getChangedColumns() ) ) { $strSql .= ' frequency_interval = ' . $this->sqlFrequencyInterval() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_occurrences = ' . $this->sqlNumberOfOccurrences(). ',' ; } elseif( true == array_key_exists( 'NumberOfOccurrences', $this->getChangedColumns() ) ) { $strSql .= ' number_of_occurrences = ' . $this->sqlNumberOfOccurrences() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' week_days = ' . $this->sqlWeekDays(). ',' ; } elseif( true == array_key_exists( 'WeekDays', $this->getChangedColumns() ) ) { $strSql .= ' week_days = ' . $this->sqlWeekDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month_days = ' . $this->sqlMonthDays(). ',' ; } elseif( true == array_key_exists( 'MonthDays', $this->getChangedColumns() ) ) { $strSql .= ' month_days = ' . $this->sqlMonthDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name_prefix = ' . $this->sqlFileNamePrefix(). ',' ; } elseif( true == array_key_exists( 'FileNamePrefix', $this->getChangedColumns() ) ) { $strSql .= ' file_name_prefix = ' . $this->sqlFileNamePrefix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name_suffix = ' . $this->sqlFileNameSuffix(). ',' ; } elseif( true == array_key_exists( 'FileNameSuffix', $this->getChangedColumns() ) ) { $strSql .= ' file_name_suffix = ' . $this->sqlFileNameSuffix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_name = ' . $this->sqlExportName(). ',' ; } elseif( true == array_key_exists( 'ExportName', $this->getChangedColumns() ) ) { $strSql .= ' export_name = ' . $this->sqlExportName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_addresses = ' . $this->sqlEmailAddresses(). ',' ; } elseif( true == array_key_exists( 'EmailAddresses', $this->getChangedColumns() ) ) { $strSql .= ' email_addresses = ' . $this->sqlEmailAddresses() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_posted_date = ' . $this->sqlLastPostedDate(). ',' ; } elseif( true == array_key_exists( 'LastPostedDate', $this->getChangedColumns() ) ) { $strSql .= ' last_posted_date = ' . $this->sqlLastPostedDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' next_post_date = ' . $this->sqlNextPostDate(). ',' ; } elseif( true == array_key_exists( 'NextPostDate', $this->getChangedColumns() ) ) { $strSql .= ' next_post_date = ' . $this->sqlNextPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_start_on = ' . $this->sqlScheduledStartOn(). ',' ; } elseif( true == array_key_exists( 'ScheduledStartOn', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_start_on = ' . $this->sqlScheduledStartOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_end_on = ' . $this->sqlScheduledEndOn(). ',' ; } elseif( true == array_key_exists( 'ScheduledEndOn', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_end_on = ' . $this->sqlScheduledEndOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' starting_post_month = ' . $this->sqlStartingPostMonth(). ',' ; } elseif( true == array_key_exists( 'StartingPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' starting_post_month = ' . $this->sqlStartingPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_all_unexported_transactions = ' . $this->sqlIncludeAllUnexportedTransactions(). ',' ; } elseif( true == array_key_exists( 'IncludeAllUnexportedTransactions', $this->getChangedColumns() ) ) { $strSql .= ' include_all_unexported_transactions = ' . $this->sqlIncludeAllUnexportedTransactions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_future_transactions = ' . $this->sqlIncludeFutureTransactions(). ',' ; } elseif( true == array_key_exists( 'IncludeFutureTransactions', $this->getChangedColumns() ) ) { $strSql .= ' include_future_transactions = ' . $this->sqlIncludeFutureTransactions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_zip_files = ' . $this->sqlIsZipFiles(). ',' ; } elseif( true == array_key_exists( 'IsZipFiles', $this->getChangedColumns() ) ) { $strSql .= ' is_zip_files = ' . $this->sqlIsZipFiles() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'transmission_vendor_id' => $this->getTransmissionVendorId(),
			'accounting_export_file_format_type_id' => $this->getAccountingExportFileFormatTypeId(),
			'company_transmission_vendor_id' => $this->getCompanyTransmissionVendorId(),
			'bank_account_ids' => $this->getBankAccountIds(),
			'frequency_id' => $this->getFrequencyId(),
			'frequency_interval' => $this->getFrequencyInterval(),
			'number_of_occurrences' => $this->getNumberOfOccurrences(),
			'week_days' => $this->getWeekDays(),
			'month_days' => $this->getMonthDays(),
			'file_name_prefix' => $this->getFileNamePrefix(),
			'file_name_suffix' => $this->getFileNameSuffix(),
			'export_name' => $this->getExportName(),
			'email_addresses' => $this->getEmailAddresses(),
			'last_posted_date' => $this->getLastPostedDate(),
			'next_post_date' => $this->getNextPostDate(),
			'scheduled_start_on' => $this->getScheduledStartOn(),
			'scheduled_end_on' => $this->getScheduledEndOn(),
			'starting_post_month' => $this->getStartingPostMonth(),
			'include_all_unexported_transactions' => $this->getIncludeAllUnexportedTransactions(),
			'include_future_transactions' => $this->getIncludeFutureTransactions(),
			'is_zip_files' => $this->getIsZipFiles(),
			'details' => $this->getDetails(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>