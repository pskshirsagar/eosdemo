<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyLeadSource extends CEosSingularBase {

	const TABLE_NAME = 'public.property_lead_sources';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intLeadSourceId;
	protected $m_intCallPhoneNumberId;
	protected $m_intSmsKeywordId;
	protected $m_strRemotePrimaryKey;
	protected $m_strPhoneNumber;
	protected $m_strEmailAddress;
	protected $m_intCallTrackingExtension;
	protected $m_strShortDescription;
	protected $m_intUseForOrganicLeads;
	protected $m_fltMonthlyFixedCost;
	protected $m_fltPerLeadCost;
	protected $m_fltPerLeaseCost;
	protected $m_fltOneTimeCost;
	protected $m_intIsPublished;
	protected $m_boolHideCampaignEmailAddress;
	protected $m_intOrderNum;
	protected $m_strImportedOn;
	protected $m_strExportedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intApPayeeId;

	public function __construct() {
		parent::__construct();

		$this->m_intUseForOrganicLeads = '0';
		$this->m_intIsPublished = '1';
		$this->m_boolHideCampaignEmailAddress = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lead_source_id'] ) && $boolDirectSet ) $this->set( 'm_intLeadSourceId', trim( $arrValues['lead_source_id'] ) ); elseif( isset( $arrValues['lead_source_id'] ) ) $this->setLeadSourceId( $arrValues['lead_source_id'] );
		if( isset( $arrValues['call_phone_number_id'] ) && $boolDirectSet ) $this->set( 'm_intCallPhoneNumberId', trim( $arrValues['call_phone_number_id'] ) ); elseif( isset( $arrValues['call_phone_number_id'] ) ) $this->setCallPhoneNumberId( $arrValues['call_phone_number_id'] );
		if( isset( $arrValues['sms_keyword_id'] ) && $boolDirectSet ) $this->set( 'm_intSmsKeywordId', trim( $arrValues['sms_keyword_id'] ) ); elseif( isset( $arrValues['sms_keyword_id'] ) ) $this->setSmsKeywordId( $arrValues['sms_keyword_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( $arrValues['phone_number'] ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( $arrValues['phone_number'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['call_tracking_extension'] ) && $boolDirectSet ) $this->set( 'm_intCallTrackingExtension', trim( $arrValues['call_tracking_extension'] ) ); elseif( isset( $arrValues['call_tracking_extension'] ) ) $this->setCallTrackingExtension( $arrValues['call_tracking_extension'] );
		if( isset( $arrValues['short_description'] ) && $boolDirectSet ) $this->set( 'm_strShortDescription', trim( $arrValues['short_description'] ) ); elseif( isset( $arrValues['short_description'] ) ) $this->setShortDescription( $arrValues['short_description'] );
		if( isset( $arrValues['use_for_organic_leads'] ) && $boolDirectSet ) $this->set( 'm_intUseForOrganicLeads', trim( $arrValues['use_for_organic_leads'] ) ); elseif( isset( $arrValues['use_for_organic_leads'] ) ) $this->setUseForOrganicLeads( $arrValues['use_for_organic_leads'] );
		if( isset( $arrValues['monthly_fixed_cost'] ) && $boolDirectSet ) $this->set( 'm_fltMonthlyFixedCost', trim( $arrValues['monthly_fixed_cost'] ) ); elseif( isset( $arrValues['monthly_fixed_cost'] ) ) $this->setMonthlyFixedCost( $arrValues['monthly_fixed_cost'] );
		if( isset( $arrValues['per_lead_cost'] ) && $boolDirectSet ) $this->set( 'm_fltPerLeadCost', trim( $arrValues['per_lead_cost'] ) ); elseif( isset( $arrValues['per_lead_cost'] ) ) $this->setPerLeadCost( $arrValues['per_lead_cost'] );
		if( isset( $arrValues['per_lease_cost'] ) && $boolDirectSet ) $this->set( 'm_fltPerLeaseCost', trim( $arrValues['per_lease_cost'] ) ); elseif( isset( $arrValues['per_lease_cost'] ) ) $this->setPerLeaseCost( $arrValues['per_lease_cost'] );
		if( isset( $arrValues['one_time_cost'] ) && $boolDirectSet ) $this->set( 'm_fltOneTimeCost', trim( $arrValues['one_time_cost'] ) ); elseif( isset( $arrValues['one_time_cost'] ) ) $this->setOneTimeCost( $arrValues['one_time_cost'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['hide_campaign_email_address'] ) && $boolDirectSet ) $this->set( 'm_boolHideCampaignEmailAddress', trim( stripcslashes( $arrValues['hide_campaign_email_address'] ) ) ); elseif( isset( $arrValues['hide_campaign_email_address'] ) ) $this->setHideCampaignEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hide_campaign_email_address'] ) : $arrValues['hide_campaign_email_address'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['imported_on'] ) && $boolDirectSet ) $this->set( 'm_strImportedOn', trim( $arrValues['imported_on'] ) ); elseif( isset( $arrValues['imported_on'] ) ) $this->setImportedOn( $arrValues['imported_on'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeadSourceId( $intLeadSourceId ) {
		$this->set( 'm_intLeadSourceId', CStrings::strToIntDef( $intLeadSourceId, NULL, false ) );
	}

	public function getLeadSourceId() {
		return $this->m_intLeadSourceId;
	}

	public function sqlLeadSourceId() {
		return ( true == isset( $this->m_intLeadSourceId ) ) ? ( string ) $this->m_intLeadSourceId : 'NULL';
	}

	public function setCallPhoneNumberId( $intCallPhoneNumberId ) {
		$this->set( 'm_intCallPhoneNumberId', CStrings::strToIntDef( $intCallPhoneNumberId, NULL, false ) );
	}

	public function getCallPhoneNumberId() {
		return $this->m_intCallPhoneNumberId;
	}

	public function sqlCallPhoneNumberId() {
		return ( true == isset( $this->m_intCallPhoneNumberId ) ) ? ( string ) $this->m_intCallPhoneNumberId : 'NULL';
	}

	public function setSmsKeywordId( $intSmsKeywordId ) {
		$this->set( 'm_intSmsKeywordId', CStrings::strToIntDef( $intSmsKeywordId, NULL, false ) );
	}

	public function getSmsKeywordId() {
		return $this->m_intSmsKeywordId;
	}

	public function sqlSmsKeywordId() {
		return ( true == isset( $this->m_intSmsKeywordId ) ) ? ( string ) $this->m_intSmsKeywordId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNumber ) : '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setCallTrackingExtension( $intCallTrackingExtension ) {
		$this->set( 'm_intCallTrackingExtension', CStrings::strToIntDef( $intCallTrackingExtension, NULL, false ) );
	}

	public function getCallTrackingExtension() {
		return $this->m_intCallTrackingExtension;
	}

	public function sqlCallTrackingExtension() {
		return ( true == isset( $this->m_intCallTrackingExtension ) ) ? ( string ) $this->m_intCallTrackingExtension : 'NULL';
	}

	public function setShortDescription( $strShortDescription ) {
		$this->set( 'm_strShortDescription', CStrings::strTrimDef( $strShortDescription, 240, NULL, true ) );
	}

	public function getShortDescription() {
		return $this->m_strShortDescription;
	}

	public function sqlShortDescription() {
		return ( true == isset( $this->m_strShortDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strShortDescription ) : '\'' . addslashes( $this->m_strShortDescription ) . '\'' ) : 'NULL';
	}

	public function setUseForOrganicLeads( $intUseForOrganicLeads ) {
		$this->set( 'm_intUseForOrganicLeads', CStrings::strToIntDef( $intUseForOrganicLeads, NULL, false ) );
	}

	public function getUseForOrganicLeads() {
		return $this->m_intUseForOrganicLeads;
	}

	public function sqlUseForOrganicLeads() {
		return ( true == isset( $this->m_intUseForOrganicLeads ) ) ? ( string ) $this->m_intUseForOrganicLeads : '0';
	}

	public function setMonthlyFixedCost( $fltMonthlyFixedCost ) {
		$this->set( 'm_fltMonthlyFixedCost', CStrings::strToFloatDef( $fltMonthlyFixedCost, NULL, false, 2 ) );
	}

	public function getMonthlyFixedCost() {
		return $this->m_fltMonthlyFixedCost;
	}

	public function sqlMonthlyFixedCost() {
		return ( true == isset( $this->m_fltMonthlyFixedCost ) ) ? ( string ) $this->m_fltMonthlyFixedCost : 'NULL';
	}

	public function setPerLeadCost( $fltPerLeadCost ) {
		$this->set( 'm_fltPerLeadCost', CStrings::strToFloatDef( $fltPerLeadCost, NULL, false, 2 ) );
	}

	public function getPerLeadCost() {
		return $this->m_fltPerLeadCost;
	}

	public function sqlPerLeadCost() {
		return ( true == isset( $this->m_fltPerLeadCost ) ) ? ( string ) $this->m_fltPerLeadCost : 'NULL';
	}

	public function setPerLeaseCost( $fltPerLeaseCost ) {
		$this->set( 'm_fltPerLeaseCost', CStrings::strToFloatDef( $fltPerLeaseCost, NULL, false, 2 ) );
	}

	public function getPerLeaseCost() {
		return $this->m_fltPerLeaseCost;
	}

	public function sqlPerLeaseCost() {
		return ( true == isset( $this->m_fltPerLeaseCost ) ) ? ( string ) $this->m_fltPerLeaseCost : 'NULL';
	}

	public function setOneTimeCost( $fltOneTimeCost ) {
		$this->set( 'm_fltOneTimeCost', CStrings::strToFloatDef( $fltOneTimeCost, NULL, false, 2 ) );
	}

	public function getOneTimeCost() {
		return $this->m_fltOneTimeCost;
	}

	public function sqlOneTimeCost() {
		return ( true == isset( $this->m_fltOneTimeCost ) ) ? ( string ) $this->m_fltOneTimeCost : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setHideCampaignEmailAddress( $boolHideCampaignEmailAddress ) {
		$this->set( 'm_boolHideCampaignEmailAddress', CStrings::strToBool( $boolHideCampaignEmailAddress ) );
	}

	public function getHideCampaignEmailAddress() {
		return $this->m_boolHideCampaignEmailAddress;
	}

	public function sqlHideCampaignEmailAddress() {
		return ( true == isset( $this->m_boolHideCampaignEmailAddress ) ) ? '\'' . ( true == ( bool ) $this->m_boolHideCampaignEmailAddress ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'nextval( \'property_lead_sources_order_num_seq\'::regclass )';
	}

	public function setImportedOn( $strImportedOn ) {
		$this->set( 'm_strImportedOn', CStrings::strTrimDef( $strImportedOn, -1, NULL, true ) );
	}

	public function getImportedOn() {
		return $this->m_strImportedOn;
	}

	public function sqlImportedOn() {
		return ( true == isset( $this->m_strImportedOn ) ) ? '\'' . $this->m_strImportedOn . '\'' : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, lead_source_id, call_phone_number_id, sms_keyword_id, remote_primary_key, phone_number, email_address, call_tracking_extension, short_description, use_for_organic_leads, monthly_fixed_cost, per_lead_cost, per_lease_cost, one_time_cost, is_published, hide_campaign_email_address, order_num, imported_on, exported_on, deleted_by, deleted_on, created_by, created_on, ap_payee_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlLeadSourceId() . ', ' .
						$this->sqlCallPhoneNumberId() . ', ' .
						$this->sqlSmsKeywordId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlPhoneNumber() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlCallTrackingExtension() . ', ' .
						$this->sqlShortDescription() . ', ' .
						$this->sqlUseForOrganicLeads() . ', ' .
						$this->sqlMonthlyFixedCost() . ', ' .
						$this->sqlPerLeadCost() . ', ' .
						$this->sqlPerLeaseCost() . ', ' .
						$this->sqlOneTimeCost() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlHideCampaignEmailAddress() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlImportedOn() . ', ' .
						$this->sqlExportedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlApPayeeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_source_id = ' . $this->sqlLeadSourceId(). ',' ; } elseif( true == array_key_exists( 'LeadSourceId', $this->getChangedColumns() ) ) { $strSql .= ' lead_source_id = ' . $this->sqlLeadSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_phone_number_id = ' . $this->sqlCallPhoneNumberId(). ',' ; } elseif( true == array_key_exists( 'CallPhoneNumberId', $this->getChangedColumns() ) ) { $strSql .= ' call_phone_number_id = ' . $this->sqlCallPhoneNumberId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sms_keyword_id = ' . $this->sqlSmsKeywordId(). ',' ; } elseif( true == array_key_exists( 'SmsKeywordId', $this->getChangedColumns() ) ) { $strSql .= ' sms_keyword_id = ' . $this->sqlSmsKeywordId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_tracking_extension = ' . $this->sqlCallTrackingExtension(). ',' ; } elseif( true == array_key_exists( 'CallTrackingExtension', $this->getChangedColumns() ) ) { $strSql .= ' call_tracking_extension = ' . $this->sqlCallTrackingExtension() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' short_description = ' . $this->sqlShortDescription(). ',' ; } elseif( true == array_key_exists( 'ShortDescription', $this->getChangedColumns() ) ) { $strSql .= ' short_description = ' . $this->sqlShortDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_for_organic_leads = ' . $this->sqlUseForOrganicLeads(). ',' ; } elseif( true == array_key_exists( 'UseForOrganicLeads', $this->getChangedColumns() ) ) { $strSql .= ' use_for_organic_leads = ' . $this->sqlUseForOrganicLeads() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' monthly_fixed_cost = ' . $this->sqlMonthlyFixedCost(). ',' ; } elseif( true == array_key_exists( 'MonthlyFixedCost', $this->getChangedColumns() ) ) { $strSql .= ' monthly_fixed_cost = ' . $this->sqlMonthlyFixedCost() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' per_lead_cost = ' . $this->sqlPerLeadCost(). ',' ; } elseif( true == array_key_exists( 'PerLeadCost', $this->getChangedColumns() ) ) { $strSql .= ' per_lead_cost = ' . $this->sqlPerLeadCost() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' per_lease_cost = ' . $this->sqlPerLeaseCost(). ',' ; } elseif( true == array_key_exists( 'PerLeaseCost', $this->getChangedColumns() ) ) { $strSql .= ' per_lease_cost = ' . $this->sqlPerLeaseCost() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' one_time_cost = ' . $this->sqlOneTimeCost(). ',' ; } elseif( true == array_key_exists( 'OneTimeCost', $this->getChangedColumns() ) ) { $strSql .= ' one_time_cost = ' . $this->sqlOneTimeCost() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_campaign_email_address = ' . $this->sqlHideCampaignEmailAddress(). ',' ; } elseif( true == array_key_exists( 'HideCampaignEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' hide_campaign_email_address = ' . $this->sqlHideCampaignEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn(). ',' ; } elseif( true == array_key_exists( 'ImportedOn', $this->getChangedColumns() ) ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn(). ',' ; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() ; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'lead_source_id' => $this->getLeadSourceId(),
			'call_phone_number_id' => $this->getCallPhoneNumberId(),
			'sms_keyword_id' => $this->getSmsKeywordId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'phone_number' => $this->getPhoneNumber(),
			'email_address' => $this->getEmailAddress(),
			'call_tracking_extension' => $this->getCallTrackingExtension(),
			'short_description' => $this->getShortDescription(),
			'use_for_organic_leads' => $this->getUseForOrganicLeads(),
			'monthly_fixed_cost' => $this->getMonthlyFixedCost(),
			'per_lead_cost' => $this->getPerLeadCost(),
			'per_lease_cost' => $this->getPerLeaseCost(),
			'one_time_cost' => $this->getOneTimeCost(),
			'is_published' => $this->getIsPublished(),
			'hide_campaign_email_address' => $this->getHideCampaignEmailAddress(),
			'order_num' => $this->getOrderNum(),
			'imported_on' => $this->getImportedOn(),
			'exported_on' => $this->getExportedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'ap_payee_id' => $this->getApPayeeId()
		);
	}

}
?>