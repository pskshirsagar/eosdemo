<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseConstraintCondition extends CEosSingularBase {

	const TABLE_NAME = 'public.constraint_conditions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intUnitTypeId;
	protected $m_intRevenueConstraintId;
	protected $m_strEvent;
	protected $m_strBaseFormula;
	protected $m_strEventFormula;
	protected $m_strComparator;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['revenue_constraint_id'] ) && $boolDirectSet ) $this->set( 'm_intRevenueConstraintId', trim( $arrValues['revenue_constraint_id'] ) ); elseif( isset( $arrValues['revenue_constraint_id'] ) ) $this->setRevenueConstraintId( $arrValues['revenue_constraint_id'] );
		if( isset( $arrValues['event'] ) && $boolDirectSet ) $this->set( 'm_strEvent', trim( stripcslashes( $arrValues['event'] ) ) ); elseif( isset( $arrValues['event'] ) ) $this->setEvent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['event'] ) : $arrValues['event'] );
		if( isset( $arrValues['base_formula'] ) && $boolDirectSet ) $this->set( 'm_strBaseFormula', trim( stripcslashes( $arrValues['base_formula'] ) ) ); elseif( isset( $arrValues['base_formula'] ) ) $this->setBaseFormula( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['base_formula'] ) : $arrValues['base_formula'] );
		if( isset( $arrValues['event_formula'] ) && $boolDirectSet ) $this->set( 'm_strEventFormula', trim( stripcslashes( $arrValues['event_formula'] ) ) ); elseif( isset( $arrValues['event_formula'] ) ) $this->setEventFormula( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['event_formula'] ) : $arrValues['event_formula'] );
		if( isset( $arrValues['comparator'] ) && $boolDirectSet ) $this->set( 'm_strComparator', trim( stripcslashes( $arrValues['comparator'] ) ) ); elseif( isset( $arrValues['comparator'] ) ) $this->setComparator( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['comparator'] ) : $arrValues['comparator'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setRevenueConstraintId( $intRevenueConstraintId ) {
		$this->set( 'm_intRevenueConstraintId', CStrings::strToIntDef( $intRevenueConstraintId, NULL, false ) );
	}

	public function getRevenueConstraintId() {
		return $this->m_intRevenueConstraintId;
	}

	public function sqlRevenueConstraintId() {
		return ( true == isset( $this->m_intRevenueConstraintId ) ) ? ( string ) $this->m_intRevenueConstraintId : 'NULL';
	}

	public function setEvent( $strEvent ) {
		$this->set( 'm_strEvent', CStrings::strTrimDef( $strEvent, 50, NULL, true ) );
	}

	public function getEvent() {
		return $this->m_strEvent;
	}

	public function sqlEvent() {
		return ( true == isset( $this->m_strEvent ) ) ? '\'' . addslashes( $this->m_strEvent ) . '\'' : 'NULL';
	}

	public function setBaseFormula( $strBaseFormula ) {
		$this->set( 'm_strBaseFormula', CStrings::strTrimDef( $strBaseFormula, 50, NULL, true ) );
	}

	public function getBaseFormula() {
		return $this->m_strBaseFormula;
	}

	public function sqlBaseFormula() {
		return ( true == isset( $this->m_strBaseFormula ) ) ? '\'' . addslashes( $this->m_strBaseFormula ) . '\'' : 'NULL';
	}

	public function setEventFormula( $strEventFormula ) {
		$this->set( 'm_strEventFormula', CStrings::strTrimDef( $strEventFormula, 50, NULL, true ) );
	}

	public function getEventFormula() {
		return $this->m_strEventFormula;
	}

	public function sqlEventFormula() {
		return ( true == isset( $this->m_strEventFormula ) ) ? '\'' . addslashes( $this->m_strEventFormula ) . '\'' : 'NULL';
	}

	public function setComparator( $strComparator ) {
		$this->set( 'm_strComparator', CStrings::strTrimDef( $strComparator, 50, NULL, true ) );
	}

	public function getComparator() {
		return $this->m_strComparator;
	}

	public function sqlComparator() {
		return ( true == isset( $this->m_strComparator ) ) ? '\'' . addslashes( $this->m_strComparator ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, unit_type_id, revenue_constraint_id, event, base_formula, event_formula, comparator, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlUnitTypeId() . ', ' .
 						$this->sqlRevenueConstraintId() . ', ' .
 						$this->sqlEvent() . ', ' .
 						$this->sqlBaseFormula() . ', ' .
 						$this->sqlEventFormula() . ', ' .
 						$this->sqlComparator() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' revenue_constraint_id = ' . $this->sqlRevenueConstraintId() . ','; } elseif( true == array_key_exists( 'RevenueConstraintId', $this->getChangedColumns() ) ) { $strSql .= ' revenue_constraint_id = ' . $this->sqlRevenueConstraintId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event = ' . $this->sqlEvent() . ','; } elseif( true == array_key_exists( 'Event', $this->getChangedColumns() ) ) { $strSql .= ' event = ' . $this->sqlEvent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_formula = ' . $this->sqlBaseFormula() . ','; } elseif( true == array_key_exists( 'BaseFormula', $this->getChangedColumns() ) ) { $strSql .= ' base_formula = ' . $this->sqlBaseFormula() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_formula = ' . $this->sqlEventFormula() . ','; } elseif( true == array_key_exists( 'EventFormula', $this->getChangedColumns() ) ) { $strSql .= ' event_formula = ' . $this->sqlEventFormula() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' comparator = ' . $this->sqlComparator() . ','; } elseif( true == array_key_exists( 'Comparator', $this->getChangedColumns() ) ) { $strSql .= ' comparator = ' . $this->sqlComparator() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'unit_type_id' => $this->getUnitTypeId(),
			'revenue_constraint_id' => $this->getRevenueConstraintId(),
			'event' => $this->getEvent(),
			'base_formula' => $this->getBaseFormula(),
			'event_formula' => $this->getEventFormula(),
			'comparator' => $this->getComparator(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>