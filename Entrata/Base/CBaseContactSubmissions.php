<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CContactSubmissions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseContactSubmissions extends CEosPluralBase {

	/**
	 * @return CContactSubmission[]
	 */
	public static function fetchContactSubmissions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CContactSubmission', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CContactSubmission
	 */
	public static function fetchContactSubmission( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CContactSubmission', $objDatabase );
	}

	public static function fetchContactSubmissionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'contact_submissions', $objDatabase );
	}

	public static function fetchContactSubmissionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchContactSubmission( sprintf( 'SELECT * FROM contact_submissions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchContactSubmissionsByCid( $intCid, $objDatabase ) {
		return self::fetchContactSubmissions( sprintf( 'SELECT * FROM contact_submissions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchContactSubmissionsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchContactSubmissions( sprintf( 'SELECT * FROM contact_submissions WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchContactSubmissionsByPsProductIdByCid( $intPsProductId, $intCid, $objDatabase ) {
		return self::fetchContactSubmissions( sprintf( 'SELECT * FROM contact_submissions WHERE ps_product_id = %d AND cid = %d', ( int ) $intPsProductId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchContactSubmissionsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchContactSubmissions( sprintf( 'SELECT * FROM contact_submissions WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchContactSubmissionsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchContactSubmissions( sprintf( 'SELECT * FROM contact_submissions WHERE ap_payee_id = %d AND cid = %d', ( int ) $intApPayeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchContactSubmissionsByCompanyOwnerIdByCid( $intCompanyOwnerId, $intCid, $objDatabase ) {
		return self::fetchContactSubmissions( sprintf( 'SELECT * FROM contact_submissions WHERE company_owner_id = %d AND cid = %d', ( int ) $intCompanyOwnerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchContactSubmissionsByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		return self::fetchContactSubmissions( sprintf( 'SELECT * FROM contact_submissions WHERE company_employee_id = %d AND cid = %d', ( int ) $intCompanyEmployeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchContactSubmissionsByResponseRequestTypeIdByCid( $intResponseRequestTypeId, $intCid, $objDatabase ) {
		return self::fetchContactSubmissions( sprintf( 'SELECT * FROM contact_submissions WHERE response_request_type_id = %d AND cid = %d', ( int ) $intResponseRequestTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchContactSubmissionsByCustomerMessageIdByCid( $intCustomerMessageId, $intCid, $objDatabase ) {
		return self::fetchContactSubmissions( sprintf( 'SELECT * FROM contact_submissions WHERE customer_message_id = %d AND cid = %d', ( int ) $intCustomerMessageId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchContactSubmissionsByContactSubmissionTypeIdByCid( $intContactSubmissionTypeId, $intCid, $objDatabase ) {
		return self::fetchContactSubmissions( sprintf( 'SELECT * FROM contact_submissions WHERE contact_submission_type_id = %d AND cid = %d', ( int ) $intContactSubmissionTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchContactSubmissionsByListItemsIdByCid( $intListItemsId, $intCid, $objDatabase ) {
		return self::fetchContactSubmissions( sprintf( 'SELECT * FROM contact_submissions WHERE list_items_id = %d AND cid = %d', ( int ) $intListItemsId, ( int ) $intCid ), $objDatabase );
	}

}
?>