<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMilitaryOfficerStructures
 * Do not add any new functions to this class.
 */

class CBaseMilitaryOfficerStructures extends CEosPluralBase {

	/**
	 * @return CMilitaryOfficerStructure[]
	 */
	public static function fetchMilitaryOfficerStructures( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMilitaryOfficerStructure::class, $objDatabase );
	}

	/**
	 * @return CMilitaryOfficerStructure
	 */
	public static function fetchMilitaryOfficerStructure( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMilitaryOfficerStructure::class, $objDatabase );
	}

	public static function fetchMilitaryOfficerStructureCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'military_officer_structures', $objDatabase );
	}

	public static function fetchMilitaryOfficerStructureById( $intId, $objDatabase ) {
		return self::fetchMilitaryOfficerStructure( sprintf( 'SELECT * FROM military_officer_structures WHERE id = %d', $intId ), $objDatabase );
	}

}
?>