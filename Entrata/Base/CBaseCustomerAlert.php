<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerAlert extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.customer_alerts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCustomerId;
	protected $m_intCustomerAlertTypeId;
	protected $m_intReferenceId;
	protected $m_strActionText;
	protected $m_intPriority;
	protected $m_intDismissedBy;
	protected $m_strDismissedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strAlertTitle;
	protected $m_strAlertDescription;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['customer_alert_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerAlertTypeId', trim( $arrValues['customer_alert_type_id'] ) ); elseif( isset( $arrValues['customer_alert_type_id'] ) ) $this->setCustomerAlertTypeId( $arrValues['customer_alert_type_id'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		if( isset( $arrValues['action_text'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strActionText', trim( $arrValues['action_text'] ) ); elseif( isset( $arrValues['action_text'] ) ) $this->setActionText( $arrValues['action_text'] );
		if( isset( $arrValues['priority'] ) && $boolDirectSet ) $this->set( 'm_intPriority', trim( $arrValues['priority'] ) ); elseif( isset( $arrValues['priority'] ) ) $this->setPriority( $arrValues['priority'] );
		if( isset( $arrValues['dismissed_by'] ) && $boolDirectSet ) $this->set( 'm_intDismissedBy', trim( $arrValues['dismissed_by'] ) ); elseif( isset( $arrValues['dismissed_by'] ) ) $this->setDismissedBy( $arrValues['dismissed_by'] );
		if( isset( $arrValues['dismissed_on'] ) && $boolDirectSet ) $this->set( 'm_strDismissedOn', trim( $arrValues['dismissed_on'] ) ); elseif( isset( $arrValues['dismissed_on'] ) ) $this->setDismissedOn( $arrValues['dismissed_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['alert_title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strAlertTitle', trim( $arrValues['alert_title'] ) ); elseif( isset( $arrValues['alert_title'] ) ) $this->setAlertTitle( $arrValues['alert_title'] );
		if( isset( $arrValues['alert_description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strAlertDescription', trim( $arrValues['alert_description'] ) ); elseif( isset( $arrValues['alert_description'] ) ) $this->setAlertDescription( $arrValues['alert_description'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setCustomerAlertTypeId( $intCustomerAlertTypeId ) {
		$this->set( 'm_intCustomerAlertTypeId', CStrings::strToIntDef( $intCustomerAlertTypeId, NULL, false ) );
	}

	public function getCustomerAlertTypeId() {
		return $this->m_intCustomerAlertTypeId;
	}

	public function sqlCustomerAlertTypeId() {
		return ( true == isset( $this->m_intCustomerAlertTypeId ) ) ? ( string ) $this->m_intCustomerAlertTypeId : 'NULL';
	}

	public function setReferenceId( $intReferenceId ) {
		$this->set( 'm_intReferenceId', CStrings::strToIntDef( $intReferenceId, NULL, false ) );
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_intReferenceId ) ) ? ( string ) $this->m_intReferenceId : 'NULL';
	}

	public function setActionText( $strActionText, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strActionText', CStrings::strTrimDef( $strActionText, -1, NULL, true ), $strLocaleCode );
	}

	public function getActionText( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strActionText', $strLocaleCode );
	}

	public function sqlActionText() {
		return ( true == isset( $this->m_strActionText ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strActionText ) : '\'' . addslashes( $this->m_strActionText ) . '\'' ) : 'NULL';
	}

	public function setPriority( $intPriority ) {
		$this->set( 'm_intPriority', CStrings::strToIntDef( $intPriority, NULL, false ) );
	}

	public function getPriority() {
		return $this->m_intPriority;
	}

	public function sqlPriority() {
		return ( true == isset( $this->m_intPriority ) ) ? ( string ) $this->m_intPriority : 'NULL';
	}

	public function setDismissedBy( $intDismissedBy ) {
		$this->set( 'm_intDismissedBy', CStrings::strToIntDef( $intDismissedBy, NULL, false ) );
	}

	public function getDismissedBy() {
		return $this->m_intDismissedBy;
	}

	public function sqlDismissedBy() {
		return ( true == isset( $this->m_intDismissedBy ) ) ? ( string ) $this->m_intDismissedBy : 'NULL';
	}

	public function setDismissedOn( $strDismissedOn ) {
		$this->set( 'm_strDismissedOn', CStrings::strTrimDef( $strDismissedOn, -1, NULL, true ) );
	}

	public function getDismissedOn() {
		return $this->m_strDismissedOn;
	}

	public function sqlDismissedOn() {
		return ( true == isset( $this->m_strDismissedOn ) ) ? '\'' . $this->m_strDismissedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setAlertTitle( $strAlertTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strAlertTitle', CStrings::strTrimDef( $strAlertTitle, -1, NULL, true ), $strLocaleCode );
	}

	public function getAlertTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strAlertTitle', $strLocaleCode );
	}

	public function sqlAlertTitle() {
		return ( true == isset( $this->m_strAlertTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAlertTitle ) : '\'' . addslashes( $this->m_strAlertTitle ) . '\'' ) : 'NULL';
	}

	public function setAlertDescription( $strAlertDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strAlertDescription', CStrings::strTrimDef( $strAlertDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getAlertDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strAlertDescription', $strLocaleCode );
	}

	public function sqlAlertDescription() {
		return ( true == isset( $this->m_strAlertDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAlertDescription ) : '\'' . addslashes( $this->m_strAlertDescription ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, customer_id, customer_alert_type_id, reference_id, action_text, priority, dismissed_by, dismissed_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, alert_title, alert_description, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlCustomerAlertTypeId() . ', ' .
						$this->sqlReferenceId() . ', ' .
						$this->sqlActionText() . ', ' .
						$this->sqlPriority() . ', ' .
						$this->sqlDismissedBy() . ', ' .
						$this->sqlDismissedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlAlertTitle() . ', ' .
						$this->sqlAlertDescription() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_alert_type_id = ' . $this->sqlCustomerAlertTypeId(). ',' ; } elseif( true == array_key_exists( 'CustomerAlertTypeId', $this->getChangedColumns() ) ) { $strSql .= ' customer_alert_type_id = ' . $this->sqlCustomerAlertTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId(). ',' ; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_text = ' . $this->sqlActionText(). ',' ; } elseif( true == array_key_exists( 'ActionText', $this->getChangedColumns() ) ) { $strSql .= ' action_text = ' . $this->sqlActionText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' priority = ' . $this->sqlPriority(). ',' ; } elseif( true == array_key_exists( 'Priority', $this->getChangedColumns() ) ) { $strSql .= ' priority = ' . $this->sqlPriority() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dismissed_by = ' . $this->sqlDismissedBy(). ',' ; } elseif( true == array_key_exists( 'DismissedBy', $this->getChangedColumns() ) ) { $strSql .= ' dismissed_by = ' . $this->sqlDismissedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dismissed_on = ' . $this->sqlDismissedOn(). ',' ; } elseif( true == array_key_exists( 'DismissedOn', $this->getChangedColumns() ) ) { $strSql .= ' dismissed_on = ' . $this->sqlDismissedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alert_title = ' . $this->sqlAlertTitle(). ',' ; } elseif( true == array_key_exists( 'AlertTitle', $this->getChangedColumns() ) ) { $strSql .= ' alert_title = ' . $this->sqlAlertTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alert_description = ' . $this->sqlAlertDescription(). ',' ; } elseif( true == array_key_exists( 'AlertDescription', $this->getChangedColumns() ) ) { $strSql .= ' alert_description = ' . $this->sqlAlertDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'customer_id' => $this->getCustomerId(),
			'customer_alert_type_id' => $this->getCustomerAlertTypeId(),
			'reference_id' => $this->getReferenceId(),
			'action_text' => $this->getActionText(),
			'priority' => $this->getPriority(),
			'dismissed_by' => $this->getDismissedBy(),
			'dismissed_on' => $this->getDismissedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'alert_title' => $this->getAlertTitle(),
			'alert_description' => $this->getAlertDescription(),
			'details' => $this->getDetails()
		);
	}

}
?>