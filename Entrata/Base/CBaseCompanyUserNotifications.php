<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserNotifications
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyUserNotifications extends CEosPluralBase {

	/**
	 * @return CCompanyUserNotification[]
	 */
	public static function fetchCompanyUserNotifications( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyUserNotification', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyUserNotification
	 */
	public static function fetchCompanyUserNotification( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyUserNotification', $objDatabase );
	}

	public static function fetchCompanyUserNotificationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_user_notifications', $objDatabase );
	}

	public static function fetchCompanyUserNotificationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserNotification( sprintf( 'SELECT * FROM company_user_notifications WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserNotificationsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyUserNotifications( sprintf( 'SELECT * FROM company_user_notifications WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyUserNotificationsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchCompanyUserNotifications( sprintf( 'SELECT * FROM company_user_notifications WHERE company_user_id = %d AND cid = %d', ( int ) $intCompanyUserId, ( int ) $intCid ), $objDatabase );
	}

}
?>