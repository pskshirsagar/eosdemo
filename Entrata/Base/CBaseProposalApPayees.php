<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CProposalApPayees
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseProposalApPayees extends CEosPluralBase {

	/**
	 * @return CProposalApPayee[]
	 */
	public static function fetchProposalApPayees( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CProposalApPayee', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CProposalApPayee
	 */
	public static function fetchProposalApPayee( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CProposalApPayee', $objDatabase );
	}

	public static function fetchProposalApPayeeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'proposal_ap_payees', $objDatabase );
	}

	public static function fetchProposalApPayeeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchProposalApPayee( sprintf( 'SELECT * FROM proposal_ap_payees WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchProposalApPayeesByCid( $intCid, $objDatabase ) {
		return self::fetchProposalApPayees( sprintf( 'SELECT * FROM proposal_ap_payees WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchProposalApPayeesByProposalIdByCid( $intProposalId, $intCid, $objDatabase ) {
		return self::fetchProposalApPayees( sprintf( 'SELECT * FROM proposal_ap_payees WHERE proposal_id = %d AND cid = %d', ( int ) $intProposalId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchProposalApPayeesByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {
		return self::fetchProposalApPayees( sprintf( 'SELECT * FROM proposal_ap_payees WHERE ap_payee_id = %d AND cid = %d', ( int ) $intApPayeeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchProposalApPayeesByApPayeeContactIdByCid( $intApPayeeContactId, $intCid, $objDatabase ) {
		return self::fetchProposalApPayees( sprintf( 'SELECT * FROM proposal_ap_payees WHERE ap_payee_contact_id = %d AND cid = %d', ( int ) $intApPayeeContactId, ( int ) $intCid ), $objDatabase );
	}

}
?>