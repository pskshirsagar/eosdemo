<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyCitizenshipTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyCitizenshipTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyCitizenshipType[]
	 */
	public static function fetchSubsidyCitizenshipTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyCitizenshipType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyCitizenshipType
	 */
	public static function fetchSubsidyCitizenshipType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyCitizenshipType::class, $objDatabase );
	}

	public static function fetchSubsidyCitizenshipTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_citizenship_types', $objDatabase );
	}

	public static function fetchSubsidyCitizenshipTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyCitizenshipType( sprintf( 'SELECT * FROM subsidy_citizenship_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>