<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseSocialPost extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.social_posts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSocialPostTypeId;
	protected $m_intSocialMediaAccountDetailId;
	protected $m_intSocialPostCategoryId;
	protected $m_intSocialPostStatusAssociationId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strEventDetails;
	protected $m_jsonEventDetails;
	protected $m_strPostedMessage;
	protected $m_strLinkCaption;
	protected $m_strLinkUrl;
	protected $m_strRemotePrimaryKey;
	protected $m_strExternalResponse;
	protected $m_strRemoteUserKey;
	protected $m_strRemoteUsername;
	protected $m_strRemoteFeelingKey;
	protected $m_boolIsPostedBySmPoster;
	protected $m_boolIsShared;
	protected $m_strPostUrl;
	protected $m_strScheduledOn;
	protected $m_strPostedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intSocialMediaCommunicationTypeId;
	protected $m_intSocialPostId;
	protected $m_intSocialPostKindReferenceId;
	protected $m_intSocialPostKindId;
	protected $m_boolIsActive;
	protected $m_intUpdateErrorCount;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPostedBySmPoster = true;
		$this->m_boolIsShared = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['social_post_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSocialPostTypeId', trim( $arrValues['social_post_type_id'] ) ); elseif( isset( $arrValues['social_post_type_id'] ) ) $this->setSocialPostTypeId( $arrValues['social_post_type_id'] );
		if( isset( $arrValues['social_media_account_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intSocialMediaAccountDetailId', trim( $arrValues['social_media_account_detail_id'] ) ); elseif( isset( $arrValues['social_media_account_detail_id'] ) ) $this->setSocialMediaAccountDetailId( $arrValues['social_media_account_detail_id'] );
		if( isset( $arrValues['social_post_category_id'] ) && $boolDirectSet ) $this->set( 'm_intSocialPostCategoryId', trim( $arrValues['social_post_category_id'] ) ); elseif( isset( $arrValues['social_post_category_id'] ) ) $this->setSocialPostCategoryId( $arrValues['social_post_category_id'] );
		if( isset( $arrValues['social_post_status_association_id'] ) && $boolDirectSet ) $this->set( 'm_intSocialPostStatusAssociationId', trim( $arrValues['social_post_status_association_id'] ) ); elseif( isset( $arrValues['social_post_status_association_id'] ) ) $this->setSocialPostStatusAssociationId( $arrValues['social_post_status_association_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['event_details'] ) ) $this->set( 'm_strEventDetails', trim( $arrValues['event_details'] ) );
		if( isset( $arrValues['posted_message'] ) && $boolDirectSet ) $this->set( 'm_strPostedMessage', trim( $arrValues['posted_message'] ) ); elseif( isset( $arrValues['posted_message'] ) ) $this->setPostedMessage( $arrValues['posted_message'] );
		if( isset( $arrValues['link_caption'] ) && $boolDirectSet ) $this->set( 'm_strLinkCaption', trim( $arrValues['link_caption'] ) ); elseif( isset( $arrValues['link_caption'] ) ) $this->setLinkCaption( $arrValues['link_caption'] );
		if( isset( $arrValues['link_url'] ) && $boolDirectSet ) $this->set( 'm_strLinkUrl', trim( $arrValues['link_url'] ) ); elseif( isset( $arrValues['link_url'] ) ) $this->setLinkUrl( $arrValues['link_url'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['external_response'] ) && $boolDirectSet ) $this->set( 'm_strExternalResponse', trim( $arrValues['external_response'] ) ); elseif( isset( $arrValues['external_response'] ) ) $this->setExternalResponse( $arrValues['external_response'] );
		if( isset( $arrValues['remote_user_key'] ) && $boolDirectSet ) $this->set( 'm_strRemoteUserKey', trim( $arrValues['remote_user_key'] ) ); elseif( isset( $arrValues['remote_user_key'] ) ) $this->setRemoteUserKey( $arrValues['remote_user_key'] );
		if( isset( $arrValues['remote_username'] ) && $boolDirectSet ) $this->set( 'm_strRemoteUsername', trim( $arrValues['remote_username'] ) ); elseif( isset( $arrValues['remote_username'] ) ) $this->setRemoteUsername( $arrValues['remote_username'] );
		if( isset( $arrValues['remote_feeling_key'] ) && $boolDirectSet ) $this->set( 'm_strRemoteFeelingKey', trim( $arrValues['remote_feeling_key'] ) ); elseif( isset( $arrValues['remote_feeling_key'] ) ) $this->setRemoteFeelingKey( $arrValues['remote_feeling_key'] );
		if( isset( $arrValues['is_posted_by_sm_poster'] ) && $boolDirectSet ) $this->set( 'm_boolIsPostedBySmPoster', trim( stripcslashes( $arrValues['is_posted_by_sm_poster'] ) ) ); elseif( isset( $arrValues['is_posted_by_sm_poster'] ) ) $this->setIsPostedBySmPoster( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_posted_by_sm_poster'] ) : $arrValues['is_posted_by_sm_poster'] );
		if( isset( $arrValues['is_shared'] ) && $boolDirectSet ) $this->set( 'm_boolIsShared', trim( stripcslashes( $arrValues['is_shared'] ) ) ); elseif( isset( $arrValues['is_shared'] ) ) $this->setIsShared( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_shared'] ) : $arrValues['is_shared'] );
		if( isset( $arrValues['post_url'] ) && $boolDirectSet ) $this->set( 'm_strPostUrl', trim( $arrValues['post_url'] ) ); elseif( isset( $arrValues['post_url'] ) ) $this->setPostUrl( $arrValues['post_url'] );
		if( isset( $arrValues['scheduled_on'] ) && $boolDirectSet ) $this->set( 'm_strScheduledOn', trim( $arrValues['scheduled_on'] ) ); elseif( isset( $arrValues['scheduled_on'] ) ) $this->setScheduledOn( $arrValues['scheduled_on'] );
		if( isset( $arrValues['posted_on'] ) && $boolDirectSet ) $this->set( 'm_strPostedOn', trim( $arrValues['posted_on'] ) ); elseif( isset( $arrValues['posted_on'] ) ) $this->setPostedOn( $arrValues['posted_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['social_media_communication_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSocialMediaCommunicationTypeId', trim( $arrValues['social_media_communication_type_id'] ) ); elseif( isset( $arrValues['social_media_communication_type_id'] ) ) $this->setSocialMediaCommunicationTypeId( $arrValues['social_media_communication_type_id'] );
		if( isset( $arrValues['social_post_id'] ) && $boolDirectSet ) $this->set( 'm_intSocialPostId', trim( $arrValues['social_post_id'] ) ); elseif( isset( $arrValues['social_post_id'] ) ) $this->setSocialPostId( $arrValues['social_post_id'] );
		if( isset( $arrValues['social_post_kind_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intSocialPostKindReferenceId', trim( $arrValues['social_post_kind_reference_id'] ) ); elseif( isset( $arrValues['social_post_kind_reference_id'] ) ) $this->setSocialPostKindReferenceId( $arrValues['social_post_kind_reference_id'] );
		if( isset( $arrValues['social_post_kind_id'] ) && $boolDirectSet ) $this->set( 'm_intSocialPostKindId', trim( $arrValues['social_post_kind_id'] ) ); elseif( isset( $arrValues['social_post_kind_id'] ) ) $this->setSocialPostKindId( $arrValues['social_post_kind_id'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['update_error_count'] ) && $boolDirectSet ) $this->set( 'm_intUpdateErrorCount', trim( $arrValues['update_error_count'] ) ); elseif( isset( $arrValues['update_error_count'] ) ) $this->setUpdateErrorCount( $arrValues['update_error_count'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSocialPostTypeId( $intSocialPostTypeId ) {
		$this->set( 'm_intSocialPostTypeId', CStrings::strToIntDef( $intSocialPostTypeId, NULL, false ) );
	}

	public function getSocialPostTypeId() {
		return $this->m_intSocialPostTypeId;
	}

	public function sqlSocialPostTypeId() {
		return ( true == isset( $this->m_intSocialPostTypeId ) ) ? ( string ) $this->m_intSocialPostTypeId : 'NULL';
	}

	public function setSocialMediaAccountDetailId( $intSocialMediaAccountDetailId ) {
		$this->set( 'm_intSocialMediaAccountDetailId', CStrings::strToIntDef( $intSocialMediaAccountDetailId, NULL, false ) );
	}

	public function getSocialMediaAccountDetailId() {
		return $this->m_intSocialMediaAccountDetailId;
	}

	public function sqlSocialMediaAccountDetailId() {
		return ( true == isset( $this->m_intSocialMediaAccountDetailId ) ) ? ( string ) $this->m_intSocialMediaAccountDetailId : 'NULL';
	}

	public function setSocialPostCategoryId( $intSocialPostCategoryId ) {
		$this->set( 'm_intSocialPostCategoryId', CStrings::strToIntDef( $intSocialPostCategoryId, NULL, false ) );
	}

	public function getSocialPostCategoryId() {
		return $this->m_intSocialPostCategoryId;
	}

	public function sqlSocialPostCategoryId() {
		return ( true == isset( $this->m_intSocialPostCategoryId ) ) ? ( string ) $this->m_intSocialPostCategoryId : 'NULL';
	}

	public function setSocialPostStatusAssociationId( $intSocialPostStatusAssociationId ) {
		$this->set( 'm_intSocialPostStatusAssociationId', CStrings::strToIntDef( $intSocialPostStatusAssociationId, NULL, false ) );
	}

	public function getSocialPostStatusAssociationId() {
		return $this->m_intSocialPostStatusAssociationId;
	}

	public function sqlSocialPostStatusAssociationId() {
		return ( true == isset( $this->m_intSocialPostStatusAssociationId ) ) ? ( string ) $this->m_intSocialPostStatusAssociationId : 'NULL';
	}

	public function setEventDetails( $jsonEventDetails ) {
		if( true == valObj( $jsonEventDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonEventDetails', $jsonEventDetails );
		} elseif( true == valJsonString( $jsonEventDetails ) ) {
			$this->set( 'm_jsonEventDetails', CStrings::strToJson( $jsonEventDetails ) );
		} else {
			$this->set( 'm_jsonEventDetails', NULL );
		}
		unset( $this->m_strEventDetails );
	}

	public function getEventDetails() {
		if( true == isset( $this->m_strEventDetails ) ) {
			$this->m_jsonEventDetails = CStrings::strToJson( $this->m_strEventDetails );
			unset( $this->m_strEventDetails );
		}
		return $this->m_jsonEventDetails;
	}

	public function sqlEventDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getEventDetails() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getEventDetails() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getEventDetails() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setPostedMessage( $strPostedMessage ) {
		$this->set( 'm_strPostedMessage', CStrings::strTrimDef( $strPostedMessage, -1, NULL, true ) );
	}

	public function getPostedMessage() {
		return $this->m_strPostedMessage;
	}

	public function sqlPostedMessage() {
		return ( true == isset( $this->m_strPostedMessage ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPostedMessage ) : '\'' . addslashes( $this->m_strPostedMessage ) . '\'' ) : 'NULL';
	}

	public function setLinkCaption( $strLinkCaption ) {
		$this->set( 'm_strLinkCaption', CStrings::strTrimDef( $strLinkCaption, 120, NULL, true ) );
	}

	public function getLinkCaption() {
		return $this->m_strLinkCaption;
	}

	public function sqlLinkCaption() {
		return ( true == isset( $this->m_strLinkCaption ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLinkCaption ) : '\'' . addslashes( $this->m_strLinkCaption ) . '\'' ) : 'NULL';
	}

	public function setLinkUrl( $strLinkUrl ) {
		$this->set( 'm_strLinkUrl', CStrings::strTrimDef( $strLinkUrl, 240, NULL, true ) );
	}

	public function getLinkUrl() {
		return $this->m_strLinkUrl;
	}

	public function sqlLinkUrl() {
		return ( true == isset( $this->m_strLinkUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLinkUrl ) : '\'' . addslashes( $this->m_strLinkUrl ) . '\'' ) : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, -1, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setExternalResponse( $strExternalResponse ) {
		$this->set( 'm_strExternalResponse', CStrings::strTrimDef( $strExternalResponse, 500, NULL, true ) );
	}

	public function getExternalResponse() {
		return $this->m_strExternalResponse;
	}

	public function sqlExternalResponse() {
		return ( true == isset( $this->m_strExternalResponse ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strExternalResponse ) : '\'' . addslashes( $this->m_strExternalResponse ) . '\'' ) : 'NULL';
	}

	public function setRemoteUserKey( $strRemoteUserKey ) {
		$this->set( 'm_strRemoteUserKey', CStrings::strTrimDef( $strRemoteUserKey, 250, NULL, true ) );
	}

	public function getRemoteUserKey() {
		return $this->m_strRemoteUserKey;
	}

	public function sqlRemoteUserKey() {
		return ( true == isset( $this->m_strRemoteUserKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemoteUserKey ) : '\'' . addslashes( $this->m_strRemoteUserKey ) . '\'' ) : 'NULL';
	}

	public function setRemoteUsername( $strRemoteUsername ) {
		$this->set( 'm_strRemoteUsername', CStrings::strTrimDef( $strRemoteUsername, 250, NULL, true ) );
	}

	public function getRemoteUsername() {
		return $this->m_strRemoteUsername;
	}

	public function sqlRemoteUsername() {
		return ( true == isset( $this->m_strRemoteUsername ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemoteUsername ) : '\'' . addslashes( $this->m_strRemoteUsername ) . '\'' ) : 'NULL';
	}

	public function setRemoteFeelingKey( $strRemoteFeelingKey ) {
		$this->set( 'm_strRemoteFeelingKey', CStrings::strTrimDef( $strRemoteFeelingKey, 250, NULL, true ) );
	}

	public function getRemoteFeelingKey() {
		return $this->m_strRemoteFeelingKey;
	}

	public function sqlRemoteFeelingKey() {
		return ( true == isset( $this->m_strRemoteFeelingKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemoteFeelingKey ) : '\'' . addslashes( $this->m_strRemoteFeelingKey ) . '\'' ) : 'NULL';
	}

	public function setIsPostedBySmPoster( $boolIsPostedBySmPoster ) {
		$this->set( 'm_boolIsPostedBySmPoster', CStrings::strToBool( $boolIsPostedBySmPoster ) );
	}

	public function getIsPostedBySmPoster() {
		return $this->m_boolIsPostedBySmPoster;
	}

	public function sqlIsPostedBySmPoster() {
		return ( true == isset( $this->m_boolIsPostedBySmPoster ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPostedBySmPoster ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsShared( $boolIsShared ) {
		$this->set( 'm_boolIsShared', CStrings::strToBool( $boolIsShared ) );
	}

	public function getIsShared() {
		return $this->m_boolIsShared;
	}

	public function sqlIsShared() {
		return ( true == isset( $this->m_boolIsShared ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsShared ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPostUrl( $strPostUrl ) {
		$this->set( 'm_strPostUrl', CStrings::strTrimDef( $strPostUrl, -1, NULL, true ) );
	}

	public function getPostUrl() {
		return $this->m_strPostUrl;
	}

	public function sqlPostUrl() {
		return ( true == isset( $this->m_strPostUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPostUrl ) : '\'' . addslashes( $this->m_strPostUrl ) . '\'' ) : 'NULL';
	}

	public function setScheduledOn( $strScheduledOn ) {
		$this->set( 'm_strScheduledOn', CStrings::strTrimDef( $strScheduledOn, -1, NULL, true ) );
	}

	public function getScheduledOn() {
		return $this->m_strScheduledOn;
	}

	public function sqlScheduledOn() {
		return ( true == isset( $this->m_strScheduledOn ) ) ? '\'' . $this->m_strScheduledOn . '\'' : 'NULL';
	}

	public function setPostedOn( $strPostedOn ) {
		$this->set( 'm_strPostedOn', CStrings::strTrimDef( $strPostedOn, -1, NULL, true ) );
	}

	public function getPostedOn() {
		return $this->m_strPostedOn;
	}

	public function sqlPostedOn() {
		return ( true == isset( $this->m_strPostedOn ) ) ? '\'' . $this->m_strPostedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setSocialMediaCommunicationTypeId( $intSocialMediaCommunicationTypeId ) {
		$this->set( 'm_intSocialMediaCommunicationTypeId', CStrings::strToIntDef( $intSocialMediaCommunicationTypeId, NULL, false ) );
	}

	public function getSocialMediaCommunicationTypeId() {
		return $this->m_intSocialMediaCommunicationTypeId;
	}

	public function sqlSocialMediaCommunicationTypeId() {
		return ( true == isset( $this->m_intSocialMediaCommunicationTypeId ) ) ? ( string ) $this->m_intSocialMediaCommunicationTypeId : 'NULL';
	}

	public function setSocialPostId( $intSocialPostId ) {
		$this->set( 'm_intSocialPostId', CStrings::strToIntDef( $intSocialPostId, NULL, false ) );
	}

	public function getSocialPostId() {
		return $this->m_intSocialPostId;
	}

	public function sqlSocialPostId() {
		return ( true == isset( $this->m_intSocialPostId ) ) ? ( string ) $this->m_intSocialPostId : 'NULL';
	}

	public function setSocialPostKindReferenceId( $intSocialPostKindReferenceId ) {
		$this->set( 'm_intSocialPostKindReferenceId', CStrings::strToIntDef( $intSocialPostKindReferenceId, NULL, false ) );
	}

	public function getSocialPostKindReferenceId() {
		return $this->m_intSocialPostKindReferenceId;
	}

	public function sqlSocialPostKindReferenceId() {
		return ( true == isset( $this->m_intSocialPostKindReferenceId ) ) ? ( string ) $this->m_intSocialPostKindReferenceId : 'NULL';
	}

	public function setSocialPostKindId( $intSocialPostKindId ) {
		$this->set( 'm_intSocialPostKindId', CStrings::strToIntDef( $intSocialPostKindId, NULL, false ) );
	}

	public function getSocialPostKindId() {
		return $this->m_intSocialPostKindId;
	}

	public function sqlSocialPostKindId() {
		return ( true == isset( $this->m_intSocialPostKindId ) ) ? ( string ) $this->m_intSocialPostKindId : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdateErrorCount( $intUpdateErrorCount ) {
		$this->set( 'm_intUpdateErrorCount', CStrings::strToIntDef( $intUpdateErrorCount, NULL, false ) );
	}

	public function getUpdateErrorCount() {
		return $this->m_intUpdateErrorCount;
	}

	public function sqlUpdateErrorCount() {
		return ( true == isset( $this->m_intUpdateErrorCount ) ) ? ( string ) $this->m_intUpdateErrorCount : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, social_post_type_id, social_media_account_detail_id, social_post_category_id, social_post_status_association_id, details, event_details, posted_message, link_caption, link_url, remote_primary_key, external_response, remote_user_key, remote_username, remote_feeling_key, is_posted_by_sm_poster, is_shared, post_url, scheduled_on, posted_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, social_media_communication_type_id, social_post_id, social_post_kind_reference_id, social_post_kind_id, is_active, update_error_count )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlSocialPostTypeId() . ', ' .
		          $this->sqlSocialMediaAccountDetailId() . ', ' .
		          $this->sqlSocialPostCategoryId() . ', ' .
		          $this->sqlSocialPostStatusAssociationId() . ', ' .
		          $this->sqlDetails() . ', ' .
		          $this->sqlEventDetails() . ', ' .
		          $this->sqlPostedMessage() . ', ' .
		          $this->sqlLinkCaption() . ', ' .
		          $this->sqlLinkUrl() . ', ' .
		          $this->sqlRemotePrimaryKey() . ', ' .
		          $this->sqlExternalResponse() . ', ' .
		          $this->sqlRemoteUserKey() . ', ' .
		          $this->sqlRemoteUsername() . ', ' .
		          $this->sqlRemoteFeelingKey() . ', ' .
		          $this->sqlIsPostedBySmPoster() . ', ' .
		          $this->sqlIsShared() . ', ' .
		          $this->sqlPostUrl() . ', ' .
		          $this->sqlScheduledOn() . ', ' .
		          $this->sqlPostedOn() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlSocialMediaCommunicationTypeId() . ', ' .
		          $this->sqlSocialPostId() . ', ' .
		          $this->sqlSocialPostKindReferenceId() . ', ' .
		          $this->sqlSocialPostKindId() . ', ' .
		          $this->sqlIsActive() . ', ' .
		          $this->sqlUpdateErrorCount() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' social_post_type_id = ' . $this->sqlSocialPostTypeId(). ',' ; } elseif( true == array_key_exists( 'SocialPostTypeId', $this->getChangedColumns() ) ) { $strSql .= ' social_post_type_id = ' . $this->sqlSocialPostTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' social_media_account_detail_id = ' . $this->sqlSocialMediaAccountDetailId(). ',' ; } elseif( true == array_key_exists( 'SocialMediaAccountDetailId', $this->getChangedColumns() ) ) { $strSql .= ' social_media_account_detail_id = ' . $this->sqlSocialMediaAccountDetailId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' social_post_category_id = ' . $this->sqlSocialPostCategoryId(). ',' ; } elseif( true == array_key_exists( 'SocialPostCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' social_post_category_id = ' . $this->sqlSocialPostCategoryId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' social_post_status_association_id = ' . $this->sqlSocialPostStatusAssociationId(). ',' ; } elseif( true == array_key_exists( 'SocialPostStatusAssociationId', $this->getChangedColumns() ) ) { $strSql .= ' social_post_status_association_id = ' . $this->sqlSocialPostStatusAssociationId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_details = ' . $this->sqlEventDetails(). ',' ; } elseif( true == array_key_exists( 'EventDetails', $this->getChangedColumns() ) ) { $strSql .= ' event_details = ' . $this->sqlEventDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' posted_message = ' . $this->sqlPostedMessage(). ',' ; } elseif( true == array_key_exists( 'PostedMessage', $this->getChangedColumns() ) ) { $strSql .= ' posted_message = ' . $this->sqlPostedMessage() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' link_caption = ' . $this->sqlLinkCaption(). ',' ; } elseif( true == array_key_exists( 'LinkCaption', $this->getChangedColumns() ) ) { $strSql .= ' link_caption = ' . $this->sqlLinkCaption() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' link_url = ' . $this->sqlLinkUrl(). ',' ; } elseif( true == array_key_exists( 'LinkUrl', $this->getChangedColumns() ) ) { $strSql .= ' link_url = ' . $this->sqlLinkUrl() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_response = ' . $this->sqlExternalResponse(). ',' ; } elseif( true == array_key_exists( 'ExternalResponse', $this->getChangedColumns() ) ) { $strSql .= ' external_response = ' . $this->sqlExternalResponse() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_user_key = ' . $this->sqlRemoteUserKey(). ',' ; } elseif( true == array_key_exists( 'RemoteUserKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_user_key = ' . $this->sqlRemoteUserKey() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_username = ' . $this->sqlRemoteUsername(). ',' ; } elseif( true == array_key_exists( 'RemoteUsername', $this->getChangedColumns() ) ) { $strSql .= ' remote_username = ' . $this->sqlRemoteUsername() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_feeling_key = ' . $this->sqlRemoteFeelingKey(). ',' ; } elseif( true == array_key_exists( 'RemoteFeelingKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_feeling_key = ' . $this->sqlRemoteFeelingKey() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_posted_by_sm_poster = ' . $this->sqlIsPostedBySmPoster(). ',' ; } elseif( true == array_key_exists( 'IsPostedBySmPoster', $this->getChangedColumns() ) ) { $strSql .= ' is_posted_by_sm_poster = ' . $this->sqlIsPostedBySmPoster() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_shared = ' . $this->sqlIsShared(). ',' ; } elseif( true == array_key_exists( 'IsShared', $this->getChangedColumns() ) ) { $strSql .= ' is_shared = ' . $this->sqlIsShared() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_url = ' . $this->sqlPostUrl(). ',' ; } elseif( true == array_key_exists( 'PostUrl', $this->getChangedColumns() ) ) { $strSql .= ' post_url = ' . $this->sqlPostUrl() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_on = ' . $this->sqlScheduledOn(). ',' ; } elseif( true == array_key_exists( 'ScheduledOn', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_on = ' . $this->sqlScheduledOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' posted_on = ' . $this->sqlPostedOn(). ',' ; } elseif( true == array_key_exists( 'PostedOn', $this->getChangedColumns() ) ) { $strSql .= ' posted_on = ' . $this->sqlPostedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' social_media_communication_type_id = ' . $this->sqlSocialMediaCommunicationTypeId(). ',' ; } elseif( true == array_key_exists( 'SocialMediaCommunicationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' social_media_communication_type_id = ' . $this->sqlSocialMediaCommunicationTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' social_post_id = ' . $this->sqlSocialPostId(). ',' ; } elseif( true == array_key_exists( 'SocialPostId', $this->getChangedColumns() ) ) { $strSql .= ' social_post_id = ' . $this->sqlSocialPostId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' social_post_kind_reference_id = ' . $this->sqlSocialPostKindReferenceId(). ',' ; } elseif( true == array_key_exists( 'SocialPostKindReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' social_post_kind_reference_id = ' . $this->sqlSocialPostKindReferenceId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' social_post_kind_id = ' . $this->sqlSocialPostKindId(). ',' ; } elseif( true == array_key_exists( 'SocialPostKindId', $this->getChangedColumns() ) ) { $strSql .= ' social_post_kind_id = ' . $this->sqlSocialPostKindId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive(). ',' ; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' update_error_count = ' . $this->sqlUpdateErrorCount(). ',' ; } elseif( true == array_key_exists( 'UpdateErrorCount', $this->getChangedColumns() ) ) { $strSql .= ' update_error_count = ' . $this->sqlUpdateErrorCount() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'social_post_type_id' => $this->getSocialPostTypeId(),
			'social_media_account_detail_id' => $this->getSocialMediaAccountDetailId(),
			'social_post_category_id' => $this->getSocialPostCategoryId(),
			'social_post_status_association_id' => $this->getSocialPostStatusAssociationId(),
			'details' => $this->getDetails(),
			'event_details' => $this->getEventDetails(),
			'posted_message' => $this->getPostedMessage(),
			'link_caption' => $this->getLinkCaption(),
			'link_url' => $this->getLinkUrl(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'external_response' => $this->getExternalResponse(),
			'remote_user_key' => $this->getRemoteUserKey(),
			'remote_username' => $this->getRemoteUsername(),
			'remote_feeling_key' => $this->getRemoteFeelingKey(),
			'is_posted_by_sm_poster' => $this->getIsPostedBySmPoster(),
			'is_shared' => $this->getIsShared(),
			'post_url' => $this->getPostUrl(),
			'scheduled_on' => $this->getScheduledOn(),
			'posted_on' => $this->getPostedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'social_media_communication_type_id' => $this->getSocialMediaCommunicationTypeId(),
			'social_post_id' => $this->getSocialPostId(),
			'social_post_kind_reference_id' => $this->getSocialPostKindReferenceId(),
			'social_post_kind_id' => $this->getSocialPostKindId(),
			'is_active' => $this->getIsActive(),
			'update_error_count' => $this->getUpdateErrorCount()
		);
	}

}
?>