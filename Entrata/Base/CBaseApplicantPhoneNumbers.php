<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicantPhoneNumbers
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApplicantPhoneNumbers extends CEosPluralBase {

	/**
	 * @return CApplicantPhoneNumber[]
	 */
	public static function fetchApplicantPhoneNumbers( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CApplicantPhoneNumber', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApplicantPhoneNumber
	 */
	public static function fetchApplicantPhoneNumber( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CApplicantPhoneNumber', $objDatabase );
	}

	public static function fetchApplicantPhoneNumberCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'applicant_phone_numbers', $objDatabase );
	}

	public static function fetchApplicantPhoneNumberByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApplicantPhoneNumber( sprintf( 'SELECT * FROM applicant_phone_numbers WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantPhoneNumbersByCid( $intCid, $objDatabase ) {
		return self::fetchApplicantPhoneNumbers( sprintf( 'SELECT * FROM applicant_phone_numbers WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantPhoneNumbersByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchApplicantPhoneNumbers( sprintf( 'SELECT * FROM applicant_phone_numbers WHERE applicant_id = %d AND cid = %d', ( int ) $intApplicantId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApplicantPhoneNumbersByPhoneNumberTypeIdByCid( $intPhoneNumberTypeId, $intCid, $objDatabase ) {
		return self::fetchApplicantPhoneNumbers( sprintf( 'SELECT * FROM applicant_phone_numbers WHERE phone_number_type_id = %d AND cid = %d', ( int ) $intPhoneNumberTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>