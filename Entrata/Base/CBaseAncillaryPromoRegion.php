<?php

class CBaseAncillaryPromoRegion extends CEosSingularBase {

	const TABLE_NAME = 'public.ancillary_promo_regions';

	protected $m_intId;
	protected $m_intAncillaryPlanPromoDetailsId;
	protected $m_strCountryCode;
	protected $m_strStateCode;
	protected $m_strPostalCodes;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ancillary_plan_promo_details_id'] ) && $boolDirectSet ) $this->set( 'm_intAncillaryPlanPromoDetailsId', trim( $arrValues['ancillary_plan_promo_details_id'] ) ); elseif( isset( $arrValues['ancillary_plan_promo_details_id'] ) ) $this->setAncillaryPlanPromoDetailsId( $arrValues['ancillary_plan_promo_details_id'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['postal_codes'] ) && $boolDirectSet ) $this->set( 'm_strPostalCodes', trim( stripcslashes( $arrValues['postal_codes'] ) ) ); elseif( isset( $arrValues['postal_codes'] ) ) $this->setPostalCodes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_codes'] ) : $arrValues['postal_codes'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setAncillaryPlanPromoDetailsId( $intAncillaryPlanPromoDetailsId ) {
		$this->set( 'm_intAncillaryPlanPromoDetailsId', CStrings::strToIntDef( $intAncillaryPlanPromoDetailsId, NULL, false ) );
	}

	public function getAncillaryPlanPromoDetailsId() {
		return $this->m_intAncillaryPlanPromoDetailsId;
	}

	public function sqlAncillaryPlanPromoDetailsId() {
		return ( true == isset( $this->m_intAncillaryPlanPromoDetailsId ) ) ? ( string ) $this->m_intAncillaryPlanPromoDetailsId : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setPostalCodes( $strPostalCodes ) {
		$this->set( 'm_strPostalCodes', CStrings::strTrimDef( $strPostalCodes, -1, NULL, true ) );
	}

	public function getPostalCodes() {
		return $this->m_strPostalCodes;
	}

	public function sqlPostalCodes() {
		return ( true == isset( $this->m_strPostalCodes ) ) ? '\'' . addslashes( $this->m_strPostalCodes ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ancillary_plan_promo_details_id' => $this->getAncillaryPlanPromoDetailsId(),
			'country_code' => $this->getCountryCode(),
			'state_code' => $this->getStateCode(),
			'postal_codes' => $this->getPostalCodes()
		);
	}

}
?>