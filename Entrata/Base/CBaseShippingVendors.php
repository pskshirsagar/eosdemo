<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CShippingVendors
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseShippingVendors extends CEosPluralBase {

	/**
	 * @return CShippingVendor[]
	 */
	public static function fetchShippingVendors( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CShippingVendor', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CShippingVendor
	 */
	public static function fetchShippingVendor( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CShippingVendor', $objDatabase );
	}

	public static function fetchShippingVendorCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'shipping_vendors', $objDatabase );
	}

	public static function fetchShippingVendorByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchShippingVendor( sprintf( 'SELECT * FROM shipping_vendors WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchShippingVendorsByCid( $intCid, $objDatabase ) {
		return self::fetchShippingVendors( sprintf( 'SELECT * FROM shipping_vendors WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchShippingVendorsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchShippingVendors( sprintf( 'SELECT * FROM shipping_vendors WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchShippingVendorsByCompanyMediaFileIdByCid( $intCompanyMediaFileId, $intCid, $objDatabase ) {
		return self::fetchShippingVendors( sprintf( 'SELECT * FROM shipping_vendors WHERE company_media_file_id = %d AND cid = %d', ( int ) $intCompanyMediaFileId, ( int ) $intCid ), $objDatabase );
	}

}
?>