<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMerchantMethod extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.merchant_methods';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intMerchantAccountId;
	protected $m_intProcessingBankAccountId;
	protected $m_intMerchantGatewayId;
	protected $m_intPaymentTypeId;
	protected $m_intPaymentMediumId;
	protected $m_strPin;
	protected $m_strGatewayUsernameEncrypted;
	protected $m_strGatewayPasswordEncrypted;
	protected $m_strLoginUsernameEncrypted;
	protected $m_strLoginPasswordEncrypted;
	protected $m_fltMaxPaymentAmount;
	protected $m_fltUndelayedSettlementCeiling;
	protected $m_fltMaxMonthlyProcessAmount;
	protected $m_fltMinimumConvenienceFee;
	protected $m_fltGatewayTransactionFee;
	protected $m_fltResidentTransactionFee;
	protected $m_fltResidentDebitTransactionFee;
	protected $m_fltTransactionFee;
	protected $m_fltReverseTransactionFee;
	protected $m_fltWaivedTransactionFee;
	protected $m_fltWaivedDebitTransactionFee;
	protected $m_fltCompanyReturnFee;
	protected $m_fltCompanyAdjustmentFee;
	protected $m_fltAuthSuccessFee;
	protected $m_fltAuthFailureFee;
	protected $m_fltDowngradeTransactionFee;
	protected $m_fltInternationalTransactionFee;
	protected $m_fltResidentReturnFee;
	protected $m_fltUnauthorizedReturnFee;
	protected $m_fltCompanyDebitTransactionFee;
	protected $m_fltRebateTransactionFee;
	protected $m_fltProviderDiscountRate;
	protected $m_fltResidentDiscountRate;
	protected $m_fltResidentDebitDiscountRate;
	protected $m_fltCompanyDiscountRate;
	protected $m_fltWaivedDiscountRate;
	protected $m_fltWaivedDebitDiscountRate;
	protected $m_fltDowngradeDiscountRate;
	protected $m_fltInternationalDiscountRate;
	protected $m_fltCompanyDebitDiscountRate;
	protected $m_fltRebateDiscountRate;
	protected $m_fltTierOneCompanyIncentiveFee;
	protected $m_fltTierOneCompanyIncentivePercentage;
	protected $m_fltTierTwoCompanyIncentiveFee;
	protected $m_fltTierTwoCompanyIncentivePercentage;
	protected $m_fltTierThreeCompanyIncentiveFee;
	protected $m_fltEveryPaymentFee;
	protected $m_fltStoredBillingFee;
	protected $m_fltTierThreeCompanyIncentivePercentage;
	protected $m_intDistributionDelayDays;
	protected $m_intReversalDelayDays;
	protected $m_strCutOffTime;
	protected $m_intCalcFeesOnMaxPayment;
	protected $m_intPhoneAuthRequired;
	protected $m_intIsControlledDistribution;
	protected $m_intIsEnabled;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_fltResidentCommercialTransactionFee;
	protected $m_fltResidentCommercialDiscountRate;
	protected $m_fltResidentInternationalTransactionFee;
	protected $m_fltResidentInternationalDiscountRate;

	public function __construct() {
		parent::__construct();

		$this->m_fltEveryPaymentFee = '0';
		$this->m_fltStoredBillingFee = '0';
		$this->m_intDistributionDelayDays = '0';
		$this->m_intReversalDelayDays = '0';
		$this->m_strCutOffTime = '15:30:00-07';
		$this->m_intCalcFeesOnMaxPayment = '0';
		$this->m_intPhoneAuthRequired = '0';
		$this->m_intIsControlledDistribution = '0';
		$this->m_intIsEnabled = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['merchant_account_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantAccountId', trim( $arrValues['merchant_account_id'] ) ); elseif( isset( $arrValues['merchant_account_id'] ) ) $this->setMerchantAccountId( $arrValues['merchant_account_id'] );
		if( isset( $arrValues['processing_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankAccountId', trim( $arrValues['processing_bank_account_id'] ) ); elseif( isset( $arrValues['processing_bank_account_id'] ) ) $this->setProcessingBankAccountId( $arrValues['processing_bank_account_id'] );
		if( isset( $arrValues['merchant_gateway_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantGatewayId', trim( $arrValues['merchant_gateway_id'] ) ); elseif( isset( $arrValues['merchant_gateway_id'] ) ) $this->setMerchantGatewayId( $arrValues['merchant_gateway_id'] );
		if( isset( $arrValues['payment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentTypeId', trim( $arrValues['payment_type_id'] ) ); elseif( isset( $arrValues['payment_type_id'] ) ) $this->setPaymentTypeId( $arrValues['payment_type_id'] );
		if( isset( $arrValues['payment_medium_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentMediumId', trim( $arrValues['payment_medium_id'] ) ); elseif( isset( $arrValues['payment_medium_id'] ) ) $this->setPaymentMediumId( $arrValues['payment_medium_id'] );
		if( isset( $arrValues['pin'] ) && $boolDirectSet ) $this->set( 'm_strPin', trim( stripcslashes( $arrValues['pin'] ) ) ); elseif( isset( $arrValues['pin'] ) ) $this->setPin( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['pin'] ) : $arrValues['pin'] );
		if( isset( $arrValues['gateway_username_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strGatewayUsernameEncrypted', trim( stripcslashes( $arrValues['gateway_username_encrypted'] ) ) ); elseif( isset( $arrValues['gateway_username_encrypted'] ) ) $this->setGatewayUsernameEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gateway_username_encrypted'] ) : $arrValues['gateway_username_encrypted'] );
		if( isset( $arrValues['gateway_password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strGatewayPasswordEncrypted', trim( stripcslashes( $arrValues['gateway_password_encrypted'] ) ) ); elseif( isset( $arrValues['gateway_password_encrypted'] ) ) $this->setGatewayPasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gateway_password_encrypted'] ) : $arrValues['gateway_password_encrypted'] );
		if( isset( $arrValues['login_username_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strLoginUsernameEncrypted', trim( stripcslashes( $arrValues['login_username_encrypted'] ) ) ); elseif( isset( $arrValues['login_username_encrypted'] ) ) $this->setLoginUsernameEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['login_username_encrypted'] ) : $arrValues['login_username_encrypted'] );
		if( isset( $arrValues['login_password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strLoginPasswordEncrypted', trim( stripcslashes( $arrValues['login_password_encrypted'] ) ) ); elseif( isset( $arrValues['login_password_encrypted'] ) ) $this->setLoginPasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['login_password_encrypted'] ) : $arrValues['login_password_encrypted'] );
		if( isset( $arrValues['max_payment_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMaxPaymentAmount', trim( $arrValues['max_payment_amount'] ) ); elseif( isset( $arrValues['max_payment_amount'] ) ) $this->setMaxPaymentAmount( $arrValues['max_payment_amount'] );
		if( isset( $arrValues['undelayed_settlement_ceiling'] ) && $boolDirectSet ) $this->set( 'm_fltUndelayedSettlementCeiling', trim( $arrValues['undelayed_settlement_ceiling'] ) ); elseif( isset( $arrValues['undelayed_settlement_ceiling'] ) ) $this->setUndelayedSettlementCeiling( $arrValues['undelayed_settlement_ceiling'] );
		if( isset( $arrValues['max_monthly_process_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMaxMonthlyProcessAmount', trim( $arrValues['max_monthly_process_amount'] ) ); elseif( isset( $arrValues['max_monthly_process_amount'] ) ) $this->setMaxMonthlyProcessAmount( $arrValues['max_monthly_process_amount'] );
		if( isset( $arrValues['minimum_convenience_fee'] ) && $boolDirectSet ) $this->set( 'm_fltMinimumConvenienceFee', trim( $arrValues['minimum_convenience_fee'] ) ); elseif( isset( $arrValues['minimum_convenience_fee'] ) ) $this->setMinimumConvenienceFee( $arrValues['minimum_convenience_fee'] );
		if( isset( $arrValues['gateway_transaction_fee'] ) && $boolDirectSet ) $this->set( 'm_fltGatewayTransactionFee', trim( $arrValues['gateway_transaction_fee'] ) ); elseif( isset( $arrValues['gateway_transaction_fee'] ) ) $this->setGatewayTransactionFee( $arrValues['gateway_transaction_fee'] );
		if( isset( $arrValues['resident_transaction_fee'] ) && $boolDirectSet ) $this->set( 'm_fltResidentTransactionFee', trim( $arrValues['resident_transaction_fee'] ) ); elseif( isset( $arrValues['resident_transaction_fee'] ) ) $this->setResidentTransactionFee( $arrValues['resident_transaction_fee'] );
		if( isset( $arrValues['resident_debit_transaction_fee'] ) && $boolDirectSet ) $this->set( 'm_fltResidentDebitTransactionFee', trim( $arrValues['resident_debit_transaction_fee'] ) ); elseif( isset( $arrValues['resident_debit_transaction_fee'] ) ) $this->setResidentDebitTransactionFee( $arrValues['resident_debit_transaction_fee'] );
		if( isset( $arrValues['transaction_fee'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionFee', trim( $arrValues['transaction_fee'] ) ); elseif( isset( $arrValues['transaction_fee'] ) ) $this->setTransactionFee( $arrValues['transaction_fee'] );
		if( isset( $arrValues['reverse_transaction_fee'] ) && $boolDirectSet ) $this->set( 'm_fltReverseTransactionFee', trim( $arrValues['reverse_transaction_fee'] ) ); elseif( isset( $arrValues['reverse_transaction_fee'] ) ) $this->setReverseTransactionFee( $arrValues['reverse_transaction_fee'] );
		if( isset( $arrValues['waived_transaction_fee'] ) && $boolDirectSet ) $this->set( 'm_fltWaivedTransactionFee', trim( $arrValues['waived_transaction_fee'] ) ); elseif( isset( $arrValues['waived_transaction_fee'] ) ) $this->setWaivedTransactionFee( $arrValues['waived_transaction_fee'] );
		if( isset( $arrValues['waived_debit_transaction_fee'] ) && $boolDirectSet ) $this->set( 'm_fltWaivedDebitTransactionFee', trim( $arrValues['waived_debit_transaction_fee'] ) ); elseif( isset( $arrValues['waived_debit_transaction_fee'] ) ) $this->setWaivedDebitTransactionFee( $arrValues['waived_debit_transaction_fee'] );
		if( isset( $arrValues['company_return_fee'] ) && $boolDirectSet ) $this->set( 'm_fltCompanyReturnFee', trim( $arrValues['company_return_fee'] ) ); elseif( isset( $arrValues['company_return_fee'] ) ) $this->setCompanyReturnFee( $arrValues['company_return_fee'] );
		if( isset( $arrValues['company_adjustment_fee'] ) && $boolDirectSet ) $this->set( 'm_fltCompanyAdjustmentFee', trim( $arrValues['company_adjustment_fee'] ) ); elseif( isset( $arrValues['company_adjustment_fee'] ) ) $this->setCompanyAdjustmentFee( $arrValues['company_adjustment_fee'] );
		if( isset( $arrValues['auth_success_fee'] ) && $boolDirectSet ) $this->set( 'm_fltAuthSuccessFee', trim( $arrValues['auth_success_fee'] ) ); elseif( isset( $arrValues['auth_success_fee'] ) ) $this->setAuthSuccessFee( $arrValues['auth_success_fee'] );
		if( isset( $arrValues['auth_failure_fee'] ) && $boolDirectSet ) $this->set( 'm_fltAuthFailureFee', trim( $arrValues['auth_failure_fee'] ) ); elseif( isset( $arrValues['auth_failure_fee'] ) ) $this->setAuthFailureFee( $arrValues['auth_failure_fee'] );
		if( isset( $arrValues['downgrade_transaction_fee'] ) && $boolDirectSet ) $this->set( 'm_fltDowngradeTransactionFee', trim( $arrValues['downgrade_transaction_fee'] ) ); elseif( isset( $arrValues['downgrade_transaction_fee'] ) ) $this->setDowngradeTransactionFee( $arrValues['downgrade_transaction_fee'] );
		if( isset( $arrValues['international_transaction_fee'] ) && $boolDirectSet ) $this->set( 'm_fltInternationalTransactionFee', trim( $arrValues['international_transaction_fee'] ) ); elseif( isset( $arrValues['international_transaction_fee'] ) ) $this->setInternationalTransactionFee( $arrValues['international_transaction_fee'] );
		if( isset( $arrValues['resident_return_fee'] ) && $boolDirectSet ) $this->set( 'm_fltResidentReturnFee', trim( $arrValues['resident_return_fee'] ) ); elseif( isset( $arrValues['resident_return_fee'] ) ) $this->setResidentReturnFee( $arrValues['resident_return_fee'] );
		if( isset( $arrValues['unauthorized_return_fee'] ) && $boolDirectSet ) $this->set( 'm_fltUnauthorizedReturnFee', trim( $arrValues['unauthorized_return_fee'] ) ); elseif( isset( $arrValues['unauthorized_return_fee'] ) ) $this->setUnauthorizedReturnFee( $arrValues['unauthorized_return_fee'] );
		if( isset( $arrValues['company_debit_transaction_fee'] ) && $boolDirectSet ) $this->set( 'm_fltCompanyDebitTransactionFee', trim( $arrValues['company_debit_transaction_fee'] ) ); elseif( isset( $arrValues['company_debit_transaction_fee'] ) ) $this->setCompanyDebitTransactionFee( $arrValues['company_debit_transaction_fee'] );
		if( isset( $arrValues['rebate_transaction_fee'] ) && $boolDirectSet ) $this->set( 'm_fltRebateTransactionFee', trim( $arrValues['rebate_transaction_fee'] ) ); elseif( isset( $arrValues['rebate_transaction_fee'] ) ) $this->setRebateTransactionFee( $arrValues['rebate_transaction_fee'] );
		if( isset( $arrValues['provider_discount_rate'] ) && $boolDirectSet ) $this->set( 'm_fltProviderDiscountRate', trim( $arrValues['provider_discount_rate'] ) ); elseif( isset( $arrValues['provider_discount_rate'] ) ) $this->setProviderDiscountRate( $arrValues['provider_discount_rate'] );
		if( isset( $arrValues['resident_discount_rate'] ) && $boolDirectSet ) $this->set( 'm_fltResidentDiscountRate', trim( $arrValues['resident_discount_rate'] ) ); elseif( isset( $arrValues['resident_discount_rate'] ) ) $this->setResidentDiscountRate( $arrValues['resident_discount_rate'] );
		if( isset( $arrValues['resident_debit_discount_rate'] ) && $boolDirectSet ) $this->set( 'm_fltResidentDebitDiscountRate', trim( $arrValues['resident_debit_discount_rate'] ) ); elseif( isset( $arrValues['resident_debit_discount_rate'] ) ) $this->setResidentDebitDiscountRate( $arrValues['resident_debit_discount_rate'] );
		if( isset( $arrValues['company_discount_rate'] ) && $boolDirectSet ) $this->set( 'm_fltCompanyDiscountRate', trim( $arrValues['company_discount_rate'] ) ); elseif( isset( $arrValues['company_discount_rate'] ) ) $this->setCompanyDiscountRate( $arrValues['company_discount_rate'] );
		if( isset( $arrValues['waived_discount_rate'] ) && $boolDirectSet ) $this->set( 'm_fltWaivedDiscountRate', trim( $arrValues['waived_discount_rate'] ) ); elseif( isset( $arrValues['waived_discount_rate'] ) ) $this->setWaivedDiscountRate( $arrValues['waived_discount_rate'] );
		if( isset( $arrValues['waived_debit_discount_rate'] ) && $boolDirectSet ) $this->set( 'm_fltWaivedDebitDiscountRate', trim( $arrValues['waived_debit_discount_rate'] ) ); elseif( isset( $arrValues['waived_debit_discount_rate'] ) ) $this->setWaivedDebitDiscountRate( $arrValues['waived_debit_discount_rate'] );
		if( isset( $arrValues['downgrade_discount_rate'] ) && $boolDirectSet ) $this->set( 'm_fltDowngradeDiscountRate', trim( $arrValues['downgrade_discount_rate'] ) ); elseif( isset( $arrValues['downgrade_discount_rate'] ) ) $this->setDowngradeDiscountRate( $arrValues['downgrade_discount_rate'] );
		if( isset( $arrValues['international_discount_rate'] ) && $boolDirectSet ) $this->set( 'm_fltInternationalDiscountRate', trim( $arrValues['international_discount_rate'] ) ); elseif( isset( $arrValues['international_discount_rate'] ) ) $this->setInternationalDiscountRate( $arrValues['international_discount_rate'] );
		if( isset( $arrValues['company_debit_discount_rate'] ) && $boolDirectSet ) $this->set( 'm_fltCompanyDebitDiscountRate', trim( $arrValues['company_debit_discount_rate'] ) ); elseif( isset( $arrValues['company_debit_discount_rate'] ) ) $this->setCompanyDebitDiscountRate( $arrValues['company_debit_discount_rate'] );
		if( isset( $arrValues['rebate_discount_rate'] ) && $boolDirectSet ) $this->set( 'm_fltRebateDiscountRate', trim( $arrValues['rebate_discount_rate'] ) ); elseif( isset( $arrValues['rebate_discount_rate'] ) ) $this->setRebateDiscountRate( $arrValues['rebate_discount_rate'] );
		if( isset( $arrValues['tier_one_company_incentive_fee'] ) && $boolDirectSet ) $this->set( 'm_fltTierOneCompanyIncentiveFee', trim( $arrValues['tier_one_company_incentive_fee'] ) ); elseif( isset( $arrValues['tier_one_company_incentive_fee'] ) ) $this->setTierOneCompanyIncentiveFee( $arrValues['tier_one_company_incentive_fee'] );
		if( isset( $arrValues['tier_one_company_incentive_percentage'] ) && $boolDirectSet ) $this->set( 'm_fltTierOneCompanyIncentivePercentage', trim( $arrValues['tier_one_company_incentive_percentage'] ) ); elseif( isset( $arrValues['tier_one_company_incentive_percentage'] ) ) $this->setTierOneCompanyIncentivePercentage( $arrValues['tier_one_company_incentive_percentage'] );
		if( isset( $arrValues['tier_two_company_incentive_fee'] ) && $boolDirectSet ) $this->set( 'm_fltTierTwoCompanyIncentiveFee', trim( $arrValues['tier_two_company_incentive_fee'] ) ); elseif( isset( $arrValues['tier_two_company_incentive_fee'] ) ) $this->setTierTwoCompanyIncentiveFee( $arrValues['tier_two_company_incentive_fee'] );
		if( isset( $arrValues['tier_two_company_incentive_percentage'] ) && $boolDirectSet ) $this->set( 'm_fltTierTwoCompanyIncentivePercentage', trim( $arrValues['tier_two_company_incentive_percentage'] ) ); elseif( isset( $arrValues['tier_two_company_incentive_percentage'] ) ) $this->setTierTwoCompanyIncentivePercentage( $arrValues['tier_two_company_incentive_percentage'] );
		if( isset( $arrValues['tier_three_company_incentive_fee'] ) && $boolDirectSet ) $this->set( 'm_fltTierThreeCompanyIncentiveFee', trim( $arrValues['tier_three_company_incentive_fee'] ) ); elseif( isset( $arrValues['tier_three_company_incentive_fee'] ) ) $this->setTierThreeCompanyIncentiveFee( $arrValues['tier_three_company_incentive_fee'] );
		if( isset( $arrValues['every_payment_fee'] ) && $boolDirectSet ) $this->set( 'm_fltEveryPaymentFee', trim( $arrValues['every_payment_fee'] ) ); elseif( isset( $arrValues['every_payment_fee'] ) ) $this->setEveryPaymentFee( $arrValues['every_payment_fee'] );
		if( isset( $arrValues['stored_billing_fee'] ) && $boolDirectSet ) $this->set( 'm_fltStoredBillingFee', trim( $arrValues['stored_billing_fee'] ) ); elseif( isset( $arrValues['stored_billing_fee'] ) ) $this->setStoredBillingFee( $arrValues['stored_billing_fee'] );
		if( isset( $arrValues['tier_three_company_incentive_percentage'] ) && $boolDirectSet ) $this->set( 'm_fltTierThreeCompanyIncentivePercentage', trim( $arrValues['tier_three_company_incentive_percentage'] ) ); elseif( isset( $arrValues['tier_three_company_incentive_percentage'] ) ) $this->setTierThreeCompanyIncentivePercentage( $arrValues['tier_three_company_incentive_percentage'] );
		if( isset( $arrValues['distribution_delay_days'] ) && $boolDirectSet ) $this->set( 'm_intDistributionDelayDays', trim( $arrValues['distribution_delay_days'] ) ); elseif( isset( $arrValues['distribution_delay_days'] ) ) $this->setDistributionDelayDays( $arrValues['distribution_delay_days'] );
		if( isset( $arrValues['reversal_delay_days'] ) && $boolDirectSet ) $this->set( 'm_intReversalDelayDays', trim( $arrValues['reversal_delay_days'] ) ); elseif( isset( $arrValues['reversal_delay_days'] ) ) $this->setReversalDelayDays( $arrValues['reversal_delay_days'] );
		if( isset( $arrValues['cut_off_time'] ) && $boolDirectSet ) $this->set( 'm_strCutOffTime', trim( $arrValues['cut_off_time'] ) ); elseif( isset( $arrValues['cut_off_time'] ) ) $this->setCutOffTime( $arrValues['cut_off_time'] );
		if( isset( $arrValues['calc_fees_on_max_payment'] ) && $boolDirectSet ) $this->set( 'm_intCalcFeesOnMaxPayment', trim( $arrValues['calc_fees_on_max_payment'] ) ); elseif( isset( $arrValues['calc_fees_on_max_payment'] ) ) $this->setCalcFeesOnMaxPayment( $arrValues['calc_fees_on_max_payment'] );
		if( isset( $arrValues['phone_auth_required'] ) && $boolDirectSet ) $this->set( 'm_intPhoneAuthRequired', trim( $arrValues['phone_auth_required'] ) ); elseif( isset( $arrValues['phone_auth_required'] ) ) $this->setPhoneAuthRequired( $arrValues['phone_auth_required'] );
		if( isset( $arrValues['is_controlled_distribution'] ) && $boolDirectSet ) $this->set( 'm_intIsControlledDistribution', trim( $arrValues['is_controlled_distribution'] ) ); elseif( isset( $arrValues['is_controlled_distribution'] ) ) $this->setIsControlledDistribution( $arrValues['is_controlled_distribution'] );
		if( isset( $arrValues['is_enabled'] ) && $boolDirectSet ) $this->set( 'm_intIsEnabled', trim( $arrValues['is_enabled'] ) ); elseif( isset( $arrValues['is_enabled'] ) ) $this->setIsEnabled( $arrValues['is_enabled'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['resident_commercial_transaction_fee'] ) && $boolDirectSet ) $this->set( 'm_fltResidentCommercialTransactionFee', trim( $arrValues['resident_commercial_transaction_fee'] ) ); elseif( isset( $arrValues['resident_commercial_transaction_fee'] ) ) $this->setResidentCommercialTransactionFee( $arrValues['resident_commercial_transaction_fee'] );
		if( isset( $arrValues['resident_commercial_discount_rate'] ) && $boolDirectSet ) $this->set( 'm_fltResidentCommercialDiscountRate', trim( $arrValues['resident_commercial_discount_rate'] ) ); elseif( isset( $arrValues['resident_commercial_discount_rate'] ) ) $this->setResidentCommercialDiscountRate( $arrValues['resident_commercial_discount_rate'] );
		if( isset( $arrValues['resident_international_transaction_fee'] ) && $boolDirectSet ) $this->set( 'm_fltResidentInternationalTransactionFee', trim( $arrValues['resident_international_transaction_fee'] ) ); elseif( isset( $arrValues['resident_international_transaction_fee'] ) ) $this->setResidentInternationalTransactionFee( $arrValues['resident_international_transaction_fee'] );
		if( isset( $arrValues['resident_international_discount_rate'] ) && $boolDirectSet ) $this->set( 'm_fltResidentInternationalDiscountRate', trim( $arrValues['resident_international_discount_rate'] ) ); elseif( isset( $arrValues['resident_international_discount_rate'] ) ) $this->setResidentInternationalDiscountRate( $arrValues['resident_international_discount_rate'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setMerchantAccountId( $intMerchantAccountId ) {
		$this->set( 'm_intMerchantAccountId', CStrings::strToIntDef( $intMerchantAccountId, NULL, false ) );
	}

	public function getMerchantAccountId() {
		return $this->m_intMerchantAccountId;
	}

	public function sqlMerchantAccountId() {
		return ( true == isset( $this->m_intMerchantAccountId ) ) ? ( string ) $this->m_intMerchantAccountId : 'NULL';
	}

	public function setProcessingBankAccountId( $intProcessingBankAccountId ) {
		$this->set( 'm_intProcessingBankAccountId', CStrings::strToIntDef( $intProcessingBankAccountId, NULL, false ) );
	}

	public function getProcessingBankAccountId() {
		return $this->m_intProcessingBankAccountId;
	}

	public function sqlProcessingBankAccountId() {
		return ( true == isset( $this->m_intProcessingBankAccountId ) ) ? ( string ) $this->m_intProcessingBankAccountId : 'NULL';
	}

	public function setMerchantGatewayId( $intMerchantGatewayId ) {
		$this->set( 'm_intMerchantGatewayId', CStrings::strToIntDef( $intMerchantGatewayId, NULL, false ) );
	}

	public function getMerchantGatewayId() {
		return $this->m_intMerchantGatewayId;
	}

	public function sqlMerchantGatewayId() {
		return ( true == isset( $this->m_intMerchantGatewayId ) ) ? ( string ) $this->m_intMerchantGatewayId : 'NULL';
	}

	public function setPaymentTypeId( $intPaymentTypeId ) {
		$this->set( 'm_intPaymentTypeId', CStrings::strToIntDef( $intPaymentTypeId, NULL, false ) );
	}

	public function getPaymentTypeId() {
		return $this->m_intPaymentTypeId;
	}

	public function sqlPaymentTypeId() {
		return ( true == isset( $this->m_intPaymentTypeId ) ) ? ( string ) $this->m_intPaymentTypeId : 'NULL';
	}

	public function setPaymentMediumId( $intPaymentMediumId ) {
		$this->set( 'm_intPaymentMediumId', CStrings::strToIntDef( $intPaymentMediumId, NULL, false ) );
	}

	public function getPaymentMediumId() {
		return $this->m_intPaymentMediumId;
	}

	public function sqlPaymentMediumId() {
		return ( true == isset( $this->m_intPaymentMediumId ) ) ? ( string ) $this->m_intPaymentMediumId : 'NULL';
	}

	public function setPin( $strPin ) {
		$this->set( 'm_strPin', CStrings::strTrimDef( $strPin, 50, NULL, true ) );
	}

	public function getPin() {
		return $this->m_strPin;
	}

	public function sqlPin() {
		return ( true == isset( $this->m_strPin ) ) ? '\'' . addslashes( $this->m_strPin ) . '\'' : 'NULL';
	}

	public function setGatewayUsernameEncrypted( $strGatewayUsernameEncrypted ) {
		$this->set( 'm_strGatewayUsernameEncrypted', CStrings::strTrimDef( $strGatewayUsernameEncrypted, 240, NULL, true ) );
	}

	public function getGatewayUsernameEncrypted() {
		return $this->m_strGatewayUsernameEncrypted;
	}

	public function sqlGatewayUsernameEncrypted() {
		return ( true == isset( $this->m_strGatewayUsernameEncrypted ) ) ? '\'' . addslashes( $this->m_strGatewayUsernameEncrypted ) . '\'' : 'NULL';
	}

	public function setGatewayPasswordEncrypted( $strGatewayPasswordEncrypted ) {
		$this->set( 'm_strGatewayPasswordEncrypted', CStrings::strTrimDef( $strGatewayPasswordEncrypted, 240, NULL, true ) );
	}

	public function getGatewayPasswordEncrypted() {
		return $this->m_strGatewayPasswordEncrypted;
	}

	public function sqlGatewayPasswordEncrypted() {
		return ( true == isset( $this->m_strGatewayPasswordEncrypted ) ) ? '\'' . addslashes( $this->m_strGatewayPasswordEncrypted ) . '\'' : 'NULL';
	}

	public function setLoginUsernameEncrypted( $strLoginUsernameEncrypted ) {
		$this->set( 'm_strLoginUsernameEncrypted', CStrings::strTrimDef( $strLoginUsernameEncrypted, 240, NULL, true ) );
	}

	public function getLoginUsernameEncrypted() {
		return $this->m_strLoginUsernameEncrypted;
	}

	public function sqlLoginUsernameEncrypted() {
		return ( true == isset( $this->m_strLoginUsernameEncrypted ) ) ? '\'' . addslashes( $this->m_strLoginUsernameEncrypted ) . '\'' : 'NULL';
	}

	public function setLoginPasswordEncrypted( $strLoginPasswordEncrypted ) {
		$this->set( 'm_strLoginPasswordEncrypted', CStrings::strTrimDef( $strLoginPasswordEncrypted, 240, NULL, true ) );
	}

	public function getLoginPasswordEncrypted() {
		return $this->m_strLoginPasswordEncrypted;
	}

	public function sqlLoginPasswordEncrypted() {
		return ( true == isset( $this->m_strLoginPasswordEncrypted ) ) ? '\'' . addslashes( $this->m_strLoginPasswordEncrypted ) . '\'' : 'NULL';
	}

	public function setMaxPaymentAmount( $fltMaxPaymentAmount ) {
		$this->set( 'm_fltMaxPaymentAmount', CStrings::strToFloatDef( $fltMaxPaymentAmount, NULL, false, 4 ) );
	}

	public function getMaxPaymentAmount() {
		return $this->m_fltMaxPaymentAmount;
	}

	public function sqlMaxPaymentAmount() {
		return ( true == isset( $this->m_fltMaxPaymentAmount ) ) ? ( string ) $this->m_fltMaxPaymentAmount : 'NULL';
	}

	public function setUndelayedSettlementCeiling( $fltUndelayedSettlementCeiling ) {
		$this->set( 'm_fltUndelayedSettlementCeiling', CStrings::strToFloatDef( $fltUndelayedSettlementCeiling, NULL, false, 4 ) );
	}

	public function getUndelayedSettlementCeiling() {
		return $this->m_fltUndelayedSettlementCeiling;
	}

	public function sqlUndelayedSettlementCeiling() {
		return ( true == isset( $this->m_fltUndelayedSettlementCeiling ) ) ? ( string ) $this->m_fltUndelayedSettlementCeiling : 'NULL';
	}

	public function setMaxMonthlyProcessAmount( $fltMaxMonthlyProcessAmount ) {
		$this->set( 'm_fltMaxMonthlyProcessAmount', CStrings::strToFloatDef( $fltMaxMonthlyProcessAmount, NULL, false, 4 ) );
	}

	public function getMaxMonthlyProcessAmount() {
		return $this->m_fltMaxMonthlyProcessAmount;
	}

	public function sqlMaxMonthlyProcessAmount() {
		return ( true == isset( $this->m_fltMaxMonthlyProcessAmount ) ) ? ( string ) $this->m_fltMaxMonthlyProcessAmount : 'NULL';
	}

	public function setMinimumConvenienceFee( $fltMinimumConvenienceFee ) {
		$this->set( 'm_fltMinimumConvenienceFee', CStrings::strToFloatDef( $fltMinimumConvenienceFee, NULL, false, 4 ) );
	}

	public function getMinimumConvenienceFee() {
		return $this->m_fltMinimumConvenienceFee;
	}

	public function sqlMinimumConvenienceFee() {
		return ( true == isset( $this->m_fltMinimumConvenienceFee ) ) ? ( string ) $this->m_fltMinimumConvenienceFee : 'NULL';
	}

	public function setGatewayTransactionFee( $fltGatewayTransactionFee ) {
		$this->set( 'm_fltGatewayTransactionFee', CStrings::strToFloatDef( $fltGatewayTransactionFee, NULL, false, 4 ) );
	}

	public function getGatewayTransactionFee() {
		return $this->m_fltGatewayTransactionFee;
	}

	public function sqlGatewayTransactionFee() {
		return ( true == isset( $this->m_fltGatewayTransactionFee ) ) ? ( string ) $this->m_fltGatewayTransactionFee : 'NULL';
	}

	public function setResidentTransactionFee( $fltResidentTransactionFee ) {
		$this->set( 'm_fltResidentTransactionFee', CStrings::strToFloatDef( $fltResidentTransactionFee, NULL, false, 4 ) );
	}

	public function getResidentTransactionFee() {
		return $this->m_fltResidentTransactionFee;
	}

	public function sqlResidentTransactionFee() {
		return ( true == isset( $this->m_fltResidentTransactionFee ) ) ? ( string ) $this->m_fltResidentTransactionFee : 'NULL';
	}

	public function setResidentDebitTransactionFee( $fltResidentDebitTransactionFee ) {
		$this->set( 'm_fltResidentDebitTransactionFee', CStrings::strToFloatDef( $fltResidentDebitTransactionFee, NULL, false, 4 ) );
	}

	public function getResidentDebitTransactionFee() {
		return $this->m_fltResidentDebitTransactionFee;
	}

	public function sqlResidentDebitTransactionFee() {
		return ( true == isset( $this->m_fltResidentDebitTransactionFee ) ) ? ( string ) $this->m_fltResidentDebitTransactionFee : 'NULL';
	}

	public function setTransactionFee( $fltTransactionFee ) {
		$this->set( 'm_fltTransactionFee', CStrings::strToFloatDef( $fltTransactionFee, NULL, false, 4 ) );
	}

	public function getTransactionFee() {
		return $this->m_fltTransactionFee;
	}

	public function sqlTransactionFee() {
		return ( true == isset( $this->m_fltTransactionFee ) ) ? ( string ) $this->m_fltTransactionFee : 'NULL';
	}

	public function setReverseTransactionFee( $fltReverseTransactionFee ) {
		$this->set( 'm_fltReverseTransactionFee', CStrings::strToFloatDef( $fltReverseTransactionFee, NULL, false, 4 ) );
	}

	public function getReverseTransactionFee() {
		return $this->m_fltReverseTransactionFee;
	}

	public function sqlReverseTransactionFee() {
		return ( true == isset( $this->m_fltReverseTransactionFee ) ) ? ( string ) $this->m_fltReverseTransactionFee : 'NULL';
	}

	public function setWaivedTransactionFee( $fltWaivedTransactionFee ) {
		$this->set( 'm_fltWaivedTransactionFee', CStrings::strToFloatDef( $fltWaivedTransactionFee, NULL, false, 4 ) );
	}

	public function getWaivedTransactionFee() {
		return $this->m_fltWaivedTransactionFee;
	}

	public function sqlWaivedTransactionFee() {
		return ( true == isset( $this->m_fltWaivedTransactionFee ) ) ? ( string ) $this->m_fltWaivedTransactionFee : 'NULL';
	}

	public function setWaivedDebitTransactionFee( $fltWaivedDebitTransactionFee ) {
		$this->set( 'm_fltWaivedDebitTransactionFee', CStrings::strToFloatDef( $fltWaivedDebitTransactionFee, NULL, false, 4 ) );
	}

	public function getWaivedDebitTransactionFee() {
		return $this->m_fltWaivedDebitTransactionFee;
	}

	public function sqlWaivedDebitTransactionFee() {
		return ( true == isset( $this->m_fltWaivedDebitTransactionFee ) ) ? ( string ) $this->m_fltWaivedDebitTransactionFee : 'NULL';
	}

	public function setCompanyReturnFee( $fltCompanyReturnFee ) {
		$this->set( 'm_fltCompanyReturnFee', CStrings::strToFloatDef( $fltCompanyReturnFee, NULL, false, 4 ) );
	}

	public function getCompanyReturnFee() {
		return $this->m_fltCompanyReturnFee;
	}

	public function sqlCompanyReturnFee() {
		return ( true == isset( $this->m_fltCompanyReturnFee ) ) ? ( string ) $this->m_fltCompanyReturnFee : 'NULL';
	}

	public function setCompanyAdjustmentFee( $fltCompanyAdjustmentFee ) {
		$this->set( 'm_fltCompanyAdjustmentFee', CStrings::strToFloatDef( $fltCompanyAdjustmentFee, NULL, false, 4 ) );
	}

	public function getCompanyAdjustmentFee() {
		return $this->m_fltCompanyAdjustmentFee;
	}

	public function sqlCompanyAdjustmentFee() {
		return ( true == isset( $this->m_fltCompanyAdjustmentFee ) ) ? ( string ) $this->m_fltCompanyAdjustmentFee : 'NULL';
	}

	public function setAuthSuccessFee( $fltAuthSuccessFee ) {
		$this->set( 'm_fltAuthSuccessFee', CStrings::strToFloatDef( $fltAuthSuccessFee, NULL, false, 4 ) );
	}

	public function getAuthSuccessFee() {
		return $this->m_fltAuthSuccessFee;
	}

	public function sqlAuthSuccessFee() {
		return ( true == isset( $this->m_fltAuthSuccessFee ) ) ? ( string ) $this->m_fltAuthSuccessFee : 'NULL';
	}

	public function setAuthFailureFee( $fltAuthFailureFee ) {
		$this->set( 'm_fltAuthFailureFee', CStrings::strToFloatDef( $fltAuthFailureFee, NULL, false, 4 ) );
	}

	public function getAuthFailureFee() {
		return $this->m_fltAuthFailureFee;
	}

	public function sqlAuthFailureFee() {
		return ( true == isset( $this->m_fltAuthFailureFee ) ) ? ( string ) $this->m_fltAuthFailureFee : 'NULL';
	}

	public function setDowngradeTransactionFee( $fltDowngradeTransactionFee ) {
		$this->set( 'm_fltDowngradeTransactionFee', CStrings::strToFloatDef( $fltDowngradeTransactionFee, NULL, false, 4 ) );
	}

	public function getDowngradeTransactionFee() {
		return $this->m_fltDowngradeTransactionFee;
	}

	public function sqlDowngradeTransactionFee() {
		return ( true == isset( $this->m_fltDowngradeTransactionFee ) ) ? ( string ) $this->m_fltDowngradeTransactionFee : 'NULL';
	}

	public function setInternationalTransactionFee( $fltInternationalTransactionFee ) {
		$this->set( 'm_fltInternationalTransactionFee', CStrings::strToFloatDef( $fltInternationalTransactionFee, NULL, false, 4 ) );
	}

	public function getInternationalTransactionFee() {
		return $this->m_fltInternationalTransactionFee;
	}

	public function sqlInternationalTransactionFee() {
		return ( true == isset( $this->m_fltInternationalTransactionFee ) ) ? ( string ) $this->m_fltInternationalTransactionFee : 'NULL';
	}

	public function setResidentReturnFee( $fltResidentReturnFee ) {
		$this->set( 'm_fltResidentReturnFee', CStrings::strToFloatDef( $fltResidentReturnFee, NULL, false, 4 ) );
	}

	public function getResidentReturnFee() {
		return $this->m_fltResidentReturnFee;
	}

	public function sqlResidentReturnFee() {
		return ( true == isset( $this->m_fltResidentReturnFee ) ) ? ( string ) $this->m_fltResidentReturnFee : 'NULL';
	}

	public function setUnauthorizedReturnFee( $fltUnauthorizedReturnFee ) {
		$this->set( 'm_fltUnauthorizedReturnFee', CStrings::strToFloatDef( $fltUnauthorizedReturnFee, NULL, false, 4 ) );
	}

	public function getUnauthorizedReturnFee() {
		return $this->m_fltUnauthorizedReturnFee;
	}

	public function sqlUnauthorizedReturnFee() {
		return ( true == isset( $this->m_fltUnauthorizedReturnFee ) ) ? ( string ) $this->m_fltUnauthorizedReturnFee : 'NULL';
	}

	public function setCompanyDebitTransactionFee( $fltCompanyDebitTransactionFee ) {
		$this->set( 'm_fltCompanyDebitTransactionFee', CStrings::strToFloatDef( $fltCompanyDebitTransactionFee, NULL, false, 4 ) );
	}

	public function getCompanyDebitTransactionFee() {
		return $this->m_fltCompanyDebitTransactionFee;
	}

	public function sqlCompanyDebitTransactionFee() {
		return ( true == isset( $this->m_fltCompanyDebitTransactionFee ) ) ? ( string ) $this->m_fltCompanyDebitTransactionFee : 'NULL';
	}

	public function setRebateTransactionFee( $fltRebateTransactionFee ) {
		$this->set( 'm_fltRebateTransactionFee', CStrings::strToFloatDef( $fltRebateTransactionFee, NULL, false, 4 ) );
	}

	public function getRebateTransactionFee() {
		return $this->m_fltRebateTransactionFee;
	}

	public function sqlRebateTransactionFee() {
		return ( true == isset( $this->m_fltRebateTransactionFee ) ) ? ( string ) $this->m_fltRebateTransactionFee : 'NULL';
	}

	public function setProviderDiscountRate( $fltProviderDiscountRate ) {
		$this->set( 'm_fltProviderDiscountRate', CStrings::strToFloatDef( $fltProviderDiscountRate, NULL, false, 6 ) );
	}

	public function getProviderDiscountRate() {
		return $this->m_fltProviderDiscountRate;
	}

	public function sqlProviderDiscountRate() {
		return ( true == isset( $this->m_fltProviderDiscountRate ) ) ? ( string ) $this->m_fltProviderDiscountRate : 'NULL';
	}

	public function setResidentDiscountRate( $fltResidentDiscountRate ) {
		$this->set( 'm_fltResidentDiscountRate', CStrings::strToFloatDef( $fltResidentDiscountRate, NULL, false, 6 ) );
	}

	public function getResidentDiscountRate() {
		return $this->m_fltResidentDiscountRate;
	}

	public function sqlResidentDiscountRate() {
		return ( true == isset( $this->m_fltResidentDiscountRate ) ) ? ( string ) $this->m_fltResidentDiscountRate : 'NULL';
	}

	public function setResidentDebitDiscountRate( $fltResidentDebitDiscountRate ) {
		$this->set( 'm_fltResidentDebitDiscountRate', CStrings::strToFloatDef( $fltResidentDebitDiscountRate, NULL, false, 6 ) );
	}

	public function getResidentDebitDiscountRate() {
		return $this->m_fltResidentDebitDiscountRate;
	}

	public function sqlResidentDebitDiscountRate() {
		return ( true == isset( $this->m_fltResidentDebitDiscountRate ) ) ? ( string ) $this->m_fltResidentDebitDiscountRate : 'NULL';
	}

	public function setCompanyDiscountRate( $fltCompanyDiscountRate ) {
		$this->set( 'm_fltCompanyDiscountRate', CStrings::strToFloatDef( $fltCompanyDiscountRate, NULL, false, 6 ) );
	}

	public function getCompanyDiscountRate() {
		return $this->m_fltCompanyDiscountRate;
	}

	public function sqlCompanyDiscountRate() {
		return ( true == isset( $this->m_fltCompanyDiscountRate ) ) ? ( string ) $this->m_fltCompanyDiscountRate : 'NULL';
	}

	public function setWaivedDiscountRate( $fltWaivedDiscountRate ) {
		$this->set( 'm_fltWaivedDiscountRate', CStrings::strToFloatDef( $fltWaivedDiscountRate, NULL, false, 6 ) );
	}

	public function getWaivedDiscountRate() {
		return $this->m_fltWaivedDiscountRate;
	}

	public function sqlWaivedDiscountRate() {
		return ( true == isset( $this->m_fltWaivedDiscountRate ) ) ? ( string ) $this->m_fltWaivedDiscountRate : 'NULL';
	}

	public function setWaivedDebitDiscountRate( $fltWaivedDebitDiscountRate ) {
		$this->set( 'm_fltWaivedDebitDiscountRate', CStrings::strToFloatDef( $fltWaivedDebitDiscountRate, NULL, false, 6 ) );
	}

	public function getWaivedDebitDiscountRate() {
		return $this->m_fltWaivedDebitDiscountRate;
	}

	public function sqlWaivedDebitDiscountRate() {
		return ( true == isset( $this->m_fltWaivedDebitDiscountRate ) ) ? ( string ) $this->m_fltWaivedDebitDiscountRate : 'NULL';
	}

	public function setDowngradeDiscountRate( $fltDowngradeDiscountRate ) {
		$this->set( 'm_fltDowngradeDiscountRate', CStrings::strToFloatDef( $fltDowngradeDiscountRate, NULL, false, 6 ) );
	}

	public function getDowngradeDiscountRate() {
		return $this->m_fltDowngradeDiscountRate;
	}

	public function sqlDowngradeDiscountRate() {
		return ( true == isset( $this->m_fltDowngradeDiscountRate ) ) ? ( string ) $this->m_fltDowngradeDiscountRate : 'NULL';
	}

	public function setInternationalDiscountRate( $fltInternationalDiscountRate ) {
		$this->set( 'm_fltInternationalDiscountRate', CStrings::strToFloatDef( $fltInternationalDiscountRate, NULL, false, 6 ) );
	}

	public function getInternationalDiscountRate() {
		return $this->m_fltInternationalDiscountRate;
	}

	public function sqlInternationalDiscountRate() {
		return ( true == isset( $this->m_fltInternationalDiscountRate ) ) ? ( string ) $this->m_fltInternationalDiscountRate : 'NULL';
	}

	public function setCompanyDebitDiscountRate( $fltCompanyDebitDiscountRate ) {
		$this->set( 'm_fltCompanyDebitDiscountRate', CStrings::strToFloatDef( $fltCompanyDebitDiscountRate, NULL, false, 6 ) );
	}

	public function getCompanyDebitDiscountRate() {
		return $this->m_fltCompanyDebitDiscountRate;
	}

	public function sqlCompanyDebitDiscountRate() {
		return ( true == isset( $this->m_fltCompanyDebitDiscountRate ) ) ? ( string ) $this->m_fltCompanyDebitDiscountRate : 'NULL';
	}

	public function setRebateDiscountRate( $fltRebateDiscountRate ) {
		$this->set( 'm_fltRebateDiscountRate', CStrings::strToFloatDef( $fltRebateDiscountRate, NULL, false, 6 ) );
	}

	public function getRebateDiscountRate() {
		return $this->m_fltRebateDiscountRate;
	}

	public function sqlRebateDiscountRate() {
		return ( true == isset( $this->m_fltRebateDiscountRate ) ) ? ( string ) $this->m_fltRebateDiscountRate : 'NULL';
	}

	public function setTierOneCompanyIncentiveFee( $fltTierOneCompanyIncentiveFee ) {
		$this->set( 'm_fltTierOneCompanyIncentiveFee', CStrings::strToFloatDef( $fltTierOneCompanyIncentiveFee, NULL, false, 2 ) );
	}

	public function getTierOneCompanyIncentiveFee() {
		return $this->m_fltTierOneCompanyIncentiveFee;
	}

	public function sqlTierOneCompanyIncentiveFee() {
		return ( true == isset( $this->m_fltTierOneCompanyIncentiveFee ) ) ? ( string ) $this->m_fltTierOneCompanyIncentiveFee : 'NULL';
	}

	public function setTierOneCompanyIncentivePercentage( $fltTierOneCompanyIncentivePercentage ) {
		$this->set( 'm_fltTierOneCompanyIncentivePercentage', CStrings::strToFloatDef( $fltTierOneCompanyIncentivePercentage, NULL, false, 2 ) );
	}

	public function getTierOneCompanyIncentivePercentage() {
		return $this->m_fltTierOneCompanyIncentivePercentage;
	}

	public function sqlTierOneCompanyIncentivePercentage() {
		return ( true == isset( $this->m_fltTierOneCompanyIncentivePercentage ) ) ? ( string ) $this->m_fltTierOneCompanyIncentivePercentage : 'NULL';
	}

	public function setTierTwoCompanyIncentiveFee( $fltTierTwoCompanyIncentiveFee ) {
		$this->set( 'm_fltTierTwoCompanyIncentiveFee', CStrings::strToFloatDef( $fltTierTwoCompanyIncentiveFee, NULL, false, 2 ) );
	}

	public function getTierTwoCompanyIncentiveFee() {
		return $this->m_fltTierTwoCompanyIncentiveFee;
	}

	public function sqlTierTwoCompanyIncentiveFee() {
		return ( true == isset( $this->m_fltTierTwoCompanyIncentiveFee ) ) ? ( string ) $this->m_fltTierTwoCompanyIncentiveFee : 'NULL';
	}

	public function setTierTwoCompanyIncentivePercentage( $fltTierTwoCompanyIncentivePercentage ) {
		$this->set( 'm_fltTierTwoCompanyIncentivePercentage', CStrings::strToFloatDef( $fltTierTwoCompanyIncentivePercentage, NULL, false, 2 ) );
	}

	public function getTierTwoCompanyIncentivePercentage() {
		return $this->m_fltTierTwoCompanyIncentivePercentage;
	}

	public function sqlTierTwoCompanyIncentivePercentage() {
		return ( true == isset( $this->m_fltTierTwoCompanyIncentivePercentage ) ) ? ( string ) $this->m_fltTierTwoCompanyIncentivePercentage : 'NULL';
	}

	public function setTierThreeCompanyIncentiveFee( $fltTierThreeCompanyIncentiveFee ) {
		$this->set( 'm_fltTierThreeCompanyIncentiveFee', CStrings::strToFloatDef( $fltTierThreeCompanyIncentiveFee, NULL, false, 2 ) );
	}

	public function getTierThreeCompanyIncentiveFee() {
		return $this->m_fltTierThreeCompanyIncentiveFee;
	}

	public function sqlTierThreeCompanyIncentiveFee() {
		return ( true == isset( $this->m_fltTierThreeCompanyIncentiveFee ) ) ? ( string ) $this->m_fltTierThreeCompanyIncentiveFee : 'NULL';
	}

	public function setEveryPaymentFee( $fltEveryPaymentFee ) {
		$this->set( 'm_fltEveryPaymentFee', CStrings::strToFloatDef( $fltEveryPaymentFee, NULL, false, 2 ) );
	}

	public function getEveryPaymentFee() {
		return $this->m_fltEveryPaymentFee;
	}

	public function sqlEveryPaymentFee() {
		return ( true == isset( $this->m_fltEveryPaymentFee ) ) ? ( string ) $this->m_fltEveryPaymentFee : '0';
	}

	public function setStoredBillingFee( $fltStoredBillingFee ) {
		$this->set( 'm_fltStoredBillingFee', CStrings::strToFloatDef( $fltStoredBillingFee, NULL, false, 2 ) );
	}

	public function getStoredBillingFee() {
		return $this->m_fltStoredBillingFee;
	}

	public function sqlStoredBillingFee() {
		return ( true == isset( $this->m_fltStoredBillingFee ) ) ? ( string ) $this->m_fltStoredBillingFee : '0';
	}

	public function setTierThreeCompanyIncentivePercentage( $fltTierThreeCompanyIncentivePercentage ) {
		$this->set( 'm_fltTierThreeCompanyIncentivePercentage', CStrings::strToFloatDef( $fltTierThreeCompanyIncentivePercentage, NULL, false, 2 ) );
	}

	public function getTierThreeCompanyIncentivePercentage() {
		return $this->m_fltTierThreeCompanyIncentivePercentage;
	}

	public function sqlTierThreeCompanyIncentivePercentage() {
		return ( true == isset( $this->m_fltTierThreeCompanyIncentivePercentage ) ) ? ( string ) $this->m_fltTierThreeCompanyIncentivePercentage : 'NULL';
	}

	public function setDistributionDelayDays( $intDistributionDelayDays ) {
		$this->set( 'm_intDistributionDelayDays', CStrings::strToIntDef( $intDistributionDelayDays, NULL, false ) );
	}

	public function getDistributionDelayDays() {
		return $this->m_intDistributionDelayDays;
	}

	public function sqlDistributionDelayDays() {
		return ( true == isset( $this->m_intDistributionDelayDays ) ) ? ( string ) $this->m_intDistributionDelayDays : '0';
	}

	public function setReversalDelayDays( $intReversalDelayDays ) {
		$this->set( 'm_intReversalDelayDays', CStrings::strToIntDef( $intReversalDelayDays, NULL, false ) );
	}

	public function getReversalDelayDays() {
		return $this->m_intReversalDelayDays;
	}

	public function sqlReversalDelayDays() {
		return ( true == isset( $this->m_intReversalDelayDays ) ) ? ( string ) $this->m_intReversalDelayDays : '0';
	}

	public function setCutOffTime( $strCutOffTime ) {
		$this->set( 'm_strCutOffTime', CStrings::strTrimDef( $strCutOffTime, NULL, NULL, true ) );
	}

	public function getCutOffTime() {
		return $this->m_strCutOffTime;
	}

	public function sqlCutOffTime() {
		return ( true == isset( $this->m_strCutOffTime ) ) ? '\'' . $this->m_strCutOffTime . '\'' : '\'15:30:00-07\'::time with time zone';
	}

	public function setCalcFeesOnMaxPayment( $intCalcFeesOnMaxPayment ) {
		$this->set( 'm_intCalcFeesOnMaxPayment', CStrings::strToIntDef( $intCalcFeesOnMaxPayment, NULL, false ) );
	}

	public function getCalcFeesOnMaxPayment() {
		return $this->m_intCalcFeesOnMaxPayment;
	}

	public function sqlCalcFeesOnMaxPayment() {
		return ( true == isset( $this->m_intCalcFeesOnMaxPayment ) ) ? ( string ) $this->m_intCalcFeesOnMaxPayment : '0';
	}

	public function setPhoneAuthRequired( $intPhoneAuthRequired ) {
		$this->set( 'm_intPhoneAuthRequired', CStrings::strToIntDef( $intPhoneAuthRequired, NULL, false ) );
	}

	public function getPhoneAuthRequired() {
		return $this->m_intPhoneAuthRequired;
	}

	public function sqlPhoneAuthRequired() {
		return ( true == isset( $this->m_intPhoneAuthRequired ) ) ? ( string ) $this->m_intPhoneAuthRequired : '0';
	}

	public function setIsControlledDistribution( $intIsControlledDistribution ) {
		$this->set( 'm_intIsControlledDistribution', CStrings::strToIntDef( $intIsControlledDistribution, NULL, false ) );
	}

	public function getIsControlledDistribution() {
		return $this->m_intIsControlledDistribution;
	}

	public function sqlIsControlledDistribution() {
		return ( true == isset( $this->m_intIsControlledDistribution ) ) ? ( string ) $this->m_intIsControlledDistribution : '0';
	}

	public function setIsEnabled( $intIsEnabled ) {
		$this->set( 'm_intIsEnabled', CStrings::strToIntDef( $intIsEnabled, NULL, false ) );
	}

	public function getIsEnabled() {
		return $this->m_intIsEnabled;
	}

	public function sqlIsEnabled() {
		return ( true == isset( $this->m_intIsEnabled ) ) ? ( string ) $this->m_intIsEnabled : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setResidentCommercialTransactionFee( $fltResidentCommercialTransactionFee ) {
		$this->set( 'm_fltResidentCommercialTransactionFee', CStrings::strToFloatDef( $fltResidentCommercialTransactionFee, NULL, false, 4 ) );
	}

	public function getResidentCommercialTransactionFee() {
		return $this->m_fltResidentCommercialTransactionFee;
	}

	public function sqlResidentCommercialTransactionFee() {
		return ( true == isset( $this->m_fltResidentCommercialTransactionFee ) ) ? ( string ) $this->m_fltResidentCommercialTransactionFee : 'NULL';
	}

	public function setResidentCommercialDiscountRate( $fltResidentCommercialDiscountRate ) {
		$this->set( 'm_fltResidentCommercialDiscountRate', CStrings::strToFloatDef( $fltResidentCommercialDiscountRate, NULL, false, 6 ) );
	}

	public function getResidentCommercialDiscountRate() {
		return $this->m_fltResidentCommercialDiscountRate;
	}

	public function sqlResidentCommercialDiscountRate() {
		return ( true == isset( $this->m_fltResidentCommercialDiscountRate ) ) ? ( string ) $this->m_fltResidentCommercialDiscountRate : 'NULL';
	}

	public function setResidentInternationalTransactionFee( $fltResidentInternationalTransactionFee ) {
		$this->set( 'm_fltResidentInternationalTransactionFee', CStrings::strToFloatDef( $fltResidentInternationalTransactionFee, NULL, false, 4 ) );
	}

	public function getResidentInternationalTransactionFee() {
		return $this->m_fltResidentInternationalTransactionFee;
	}

	public function sqlResidentInternationalTransactionFee() {
		return ( true == isset( $this->m_fltResidentInternationalTransactionFee ) ) ? ( string ) $this->m_fltResidentInternationalTransactionFee : 'NULL';
	}

	public function setResidentInternationalDiscountRate( $fltResidentInternationalDiscountRate ) {
		$this->set( 'm_fltResidentInternationalDiscountRate', CStrings::strToFloatDef( $fltResidentInternationalDiscountRate, NULL, false, 6 ) );
	}

	public function getResidentInternationalDiscountRate() {
		return $this->m_fltResidentInternationalDiscountRate;
	}

	public function sqlResidentInternationalDiscountRate() {
		return ( true == isset( $this->m_fltResidentInternationalDiscountRate ) ) ? ( string ) $this->m_fltResidentInternationalDiscountRate : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, merchant_account_id, processing_bank_account_id, merchant_gateway_id, payment_type_id, payment_medium_id, pin, gateway_username_encrypted, gateway_password_encrypted, login_username_encrypted, login_password_encrypted, max_payment_amount, undelayed_settlement_ceiling, max_monthly_process_amount, minimum_convenience_fee, gateway_transaction_fee, resident_transaction_fee, resident_debit_transaction_fee, transaction_fee, reverse_transaction_fee, waived_transaction_fee, waived_debit_transaction_fee, company_return_fee, company_adjustment_fee, auth_success_fee, auth_failure_fee, downgrade_transaction_fee, international_transaction_fee, resident_return_fee, unauthorized_return_fee, company_debit_transaction_fee, rebate_transaction_fee, provider_discount_rate, resident_discount_rate, resident_debit_discount_rate, company_discount_rate, waived_discount_rate, waived_debit_discount_rate, downgrade_discount_rate, international_discount_rate, company_debit_discount_rate, rebate_discount_rate, tier_one_company_incentive_fee, tier_one_company_incentive_percentage, tier_two_company_incentive_fee, tier_two_company_incentive_percentage, tier_three_company_incentive_fee, every_payment_fee, stored_billing_fee, tier_three_company_incentive_percentage, distribution_delay_days, reversal_delay_days, cut_off_time, calc_fees_on_max_payment, phone_auth_required, is_controlled_distribution, is_enabled, updated_by, updated_on, created_by, created_on, deleted_by, deleted_on, details, resident_commercial_transaction_fee, resident_commercial_discount_rate, resident_international_transaction_fee, resident_international_discount_rate )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlMerchantAccountId() . ', ' .
						$this->sqlProcessingBankAccountId() . ', ' .
						$this->sqlMerchantGatewayId() . ', ' .
						$this->sqlPaymentTypeId() . ', ' .
						$this->sqlPaymentMediumId() . ', ' .
						$this->sqlPin() . ', ' .
						$this->sqlGatewayUsernameEncrypted() . ', ' .
						$this->sqlGatewayPasswordEncrypted() . ', ' .
						$this->sqlLoginUsernameEncrypted() . ', ' .
						$this->sqlLoginPasswordEncrypted() . ', ' .
						$this->sqlMaxPaymentAmount() . ', ' .
						$this->sqlUndelayedSettlementCeiling() . ', ' .
						$this->sqlMaxMonthlyProcessAmount() . ', ' .
						$this->sqlMinimumConvenienceFee() . ', ' .
						$this->sqlGatewayTransactionFee() . ', ' .
						$this->sqlResidentTransactionFee() . ', ' .
						$this->sqlResidentDebitTransactionFee() . ', ' .
						$this->sqlTransactionFee() . ', ' .
						$this->sqlReverseTransactionFee() . ', ' .
						$this->sqlWaivedTransactionFee() . ', ' .
						$this->sqlWaivedDebitTransactionFee() . ', ' .
						$this->sqlCompanyReturnFee() . ', ' .
						$this->sqlCompanyAdjustmentFee() . ', ' .
						$this->sqlAuthSuccessFee() . ', ' .
						$this->sqlAuthFailureFee() . ', ' .
						$this->sqlDowngradeTransactionFee() . ', ' .
						$this->sqlInternationalTransactionFee() . ', ' .
						$this->sqlResidentReturnFee() . ', ' .
						$this->sqlUnauthorizedReturnFee() . ', ' .
						$this->sqlCompanyDebitTransactionFee() . ', ' .
						$this->sqlRebateTransactionFee() . ', ' .
						$this->sqlProviderDiscountRate() . ', ' .
						$this->sqlResidentDiscountRate() . ', ' .
						$this->sqlResidentDebitDiscountRate() . ', ' .
						$this->sqlCompanyDiscountRate() . ', ' .
						$this->sqlWaivedDiscountRate() . ', ' .
						$this->sqlWaivedDebitDiscountRate() . ', ' .
						$this->sqlDowngradeDiscountRate() . ', ' .
						$this->sqlInternationalDiscountRate() . ', ' .
						$this->sqlCompanyDebitDiscountRate() . ', ' .
						$this->sqlRebateDiscountRate() . ', ' .
						$this->sqlTierOneCompanyIncentiveFee() . ', ' .
						$this->sqlTierOneCompanyIncentivePercentage() . ', ' .
						$this->sqlTierTwoCompanyIncentiveFee() . ', ' .
						$this->sqlTierTwoCompanyIncentivePercentage() . ', ' .
						$this->sqlTierThreeCompanyIncentiveFee() . ', ' .
						$this->sqlEveryPaymentFee() . ', ' .
						$this->sqlStoredBillingFee() . ', ' .
						$this->sqlTierThreeCompanyIncentivePercentage() . ', ' .
						$this->sqlDistributionDelayDays() . ', ' .
						$this->sqlReversalDelayDays() . ', ' .
						$this->sqlCutOffTime() . ', ' .
						$this->sqlCalcFeesOnMaxPayment() . ', ' .
						$this->sqlPhoneAuthRequired() . ', ' .
						$this->sqlIsControlledDistribution() . ', ' .
						$this->sqlIsEnabled() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlResidentCommercialTransactionFee() . ', ' .
						$this->sqlResidentCommercialDiscountRate() . ', ' .
						$this->sqlResidentInternationalTransactionFee() . ', ' .
						$this->sqlResidentInternationalDiscountRate() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_account_id = ' . $this->sqlMerchantAccountId(). ',' ; } elseif( true == array_key_exists( 'MerchantAccountId', $this->getChangedColumns() ) ) { $strSql .= ' merchant_account_id = ' . $this->sqlMerchantAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_account_id = ' . $this->sqlProcessingBankAccountId(). ',' ; } elseif( true == array_key_exists( 'ProcessingBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_account_id = ' . $this->sqlProcessingBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_gateway_id = ' . $this->sqlMerchantGatewayId(). ',' ; } elseif( true == array_key_exists( 'MerchantGatewayId', $this->getChangedColumns() ) ) { $strSql .= ' merchant_gateway_id = ' . $this->sqlMerchantGatewayId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_type_id = ' . $this->sqlPaymentTypeId(). ',' ; } elseif( true == array_key_exists( 'PaymentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_type_id = ' . $this->sqlPaymentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_medium_id = ' . $this->sqlPaymentMediumId(). ',' ; } elseif( true == array_key_exists( 'PaymentMediumId', $this->getChangedColumns() ) ) { $strSql .= ' payment_medium_id = ' . $this->sqlPaymentMediumId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pin = ' . $this->sqlPin(). ',' ; } elseif( true == array_key_exists( 'Pin', $this->getChangedColumns() ) ) { $strSql .= ' pin = ' . $this->sqlPin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gateway_username_encrypted = ' . $this->sqlGatewayUsernameEncrypted(). ',' ; } elseif( true == array_key_exists( 'GatewayUsernameEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' gateway_username_encrypted = ' . $this->sqlGatewayUsernameEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gateway_password_encrypted = ' . $this->sqlGatewayPasswordEncrypted(). ',' ; } elseif( true == array_key_exists( 'GatewayPasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' gateway_password_encrypted = ' . $this->sqlGatewayPasswordEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' login_username_encrypted = ' . $this->sqlLoginUsernameEncrypted(). ',' ; } elseif( true == array_key_exists( 'LoginUsernameEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' login_username_encrypted = ' . $this->sqlLoginUsernameEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' login_password_encrypted = ' . $this->sqlLoginPasswordEncrypted(). ',' ; } elseif( true == array_key_exists( 'LoginPasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' login_password_encrypted = ' . $this->sqlLoginPasswordEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_payment_amount = ' . $this->sqlMaxPaymentAmount(). ',' ; } elseif( true == array_key_exists( 'MaxPaymentAmount', $this->getChangedColumns() ) ) { $strSql .= ' max_payment_amount = ' . $this->sqlMaxPaymentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' undelayed_settlement_ceiling = ' . $this->sqlUndelayedSettlementCeiling(). ',' ; } elseif( true == array_key_exists( 'UndelayedSettlementCeiling', $this->getChangedColumns() ) ) { $strSql .= ' undelayed_settlement_ceiling = ' . $this->sqlUndelayedSettlementCeiling() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_monthly_process_amount = ' . $this->sqlMaxMonthlyProcessAmount(). ',' ; } elseif( true == array_key_exists( 'MaxMonthlyProcessAmount', $this->getChangedColumns() ) ) { $strSql .= ' max_monthly_process_amount = ' . $this->sqlMaxMonthlyProcessAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minimum_convenience_fee = ' . $this->sqlMinimumConvenienceFee(). ',' ; } elseif( true == array_key_exists( 'MinimumConvenienceFee', $this->getChangedColumns() ) ) { $strSql .= ' minimum_convenience_fee = ' . $this->sqlMinimumConvenienceFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gateway_transaction_fee = ' . $this->sqlGatewayTransactionFee(). ',' ; } elseif( true == array_key_exists( 'GatewayTransactionFee', $this->getChangedColumns() ) ) { $strSql .= ' gateway_transaction_fee = ' . $this->sqlGatewayTransactionFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_transaction_fee = ' . $this->sqlResidentTransactionFee(). ',' ; } elseif( true == array_key_exists( 'ResidentTransactionFee', $this->getChangedColumns() ) ) { $strSql .= ' resident_transaction_fee = ' . $this->sqlResidentTransactionFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_debit_transaction_fee = ' . $this->sqlResidentDebitTransactionFee(). ',' ; } elseif( true == array_key_exists( 'ResidentDebitTransactionFee', $this->getChangedColumns() ) ) { $strSql .= ' resident_debit_transaction_fee = ' . $this->sqlResidentDebitTransactionFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_fee = ' . $this->sqlTransactionFee(). ',' ; } elseif( true == array_key_exists( 'TransactionFee', $this->getChangedColumns() ) ) { $strSql .= ' transaction_fee = ' . $this->sqlTransactionFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reverse_transaction_fee = ' . $this->sqlReverseTransactionFee(). ',' ; } elseif( true == array_key_exists( 'ReverseTransactionFee', $this->getChangedColumns() ) ) { $strSql .= ' reverse_transaction_fee = ' . $this->sqlReverseTransactionFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' waived_transaction_fee = ' . $this->sqlWaivedTransactionFee(). ',' ; } elseif( true == array_key_exists( 'WaivedTransactionFee', $this->getChangedColumns() ) ) { $strSql .= ' waived_transaction_fee = ' . $this->sqlWaivedTransactionFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' waived_debit_transaction_fee = ' . $this->sqlWaivedDebitTransactionFee(). ',' ; } elseif( true == array_key_exists( 'WaivedDebitTransactionFee', $this->getChangedColumns() ) ) { $strSql .= ' waived_debit_transaction_fee = ' . $this->sqlWaivedDebitTransactionFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_return_fee = ' . $this->sqlCompanyReturnFee(). ',' ; } elseif( true == array_key_exists( 'CompanyReturnFee', $this->getChangedColumns() ) ) { $strSql .= ' company_return_fee = ' . $this->sqlCompanyReturnFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_adjustment_fee = ' . $this->sqlCompanyAdjustmentFee(). ',' ; } elseif( true == array_key_exists( 'CompanyAdjustmentFee', $this->getChangedColumns() ) ) { $strSql .= ' company_adjustment_fee = ' . $this->sqlCompanyAdjustmentFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auth_success_fee = ' . $this->sqlAuthSuccessFee(). ',' ; } elseif( true == array_key_exists( 'AuthSuccessFee', $this->getChangedColumns() ) ) { $strSql .= ' auth_success_fee = ' . $this->sqlAuthSuccessFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auth_failure_fee = ' . $this->sqlAuthFailureFee(). ',' ; } elseif( true == array_key_exists( 'AuthFailureFee', $this->getChangedColumns() ) ) { $strSql .= ' auth_failure_fee = ' . $this->sqlAuthFailureFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' downgrade_transaction_fee = ' . $this->sqlDowngradeTransactionFee(). ',' ; } elseif( true == array_key_exists( 'DowngradeTransactionFee', $this->getChangedColumns() ) ) { $strSql .= ' downgrade_transaction_fee = ' . $this->sqlDowngradeTransactionFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' international_transaction_fee = ' . $this->sqlInternationalTransactionFee(). ',' ; } elseif( true == array_key_exists( 'InternationalTransactionFee', $this->getChangedColumns() ) ) { $strSql .= ' international_transaction_fee = ' . $this->sqlInternationalTransactionFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_return_fee = ' . $this->sqlResidentReturnFee(). ',' ; } elseif( true == array_key_exists( 'ResidentReturnFee', $this->getChangedColumns() ) ) { $strSql .= ' resident_return_fee = ' . $this->sqlResidentReturnFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unauthorized_return_fee = ' . $this->sqlUnauthorizedReturnFee(). ',' ; } elseif( true == array_key_exists( 'UnauthorizedReturnFee', $this->getChangedColumns() ) ) { $strSql .= ' unauthorized_return_fee = ' . $this->sqlUnauthorizedReturnFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_debit_transaction_fee = ' . $this->sqlCompanyDebitTransactionFee(). ',' ; } elseif( true == array_key_exists( 'CompanyDebitTransactionFee', $this->getChangedColumns() ) ) { $strSql .= ' company_debit_transaction_fee = ' . $this->sqlCompanyDebitTransactionFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rebate_transaction_fee = ' . $this->sqlRebateTransactionFee(). ',' ; } elseif( true == array_key_exists( 'RebateTransactionFee', $this->getChangedColumns() ) ) { $strSql .= ' rebate_transaction_fee = ' . $this->sqlRebateTransactionFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' provider_discount_rate = ' . $this->sqlProviderDiscountRate(). ',' ; } elseif( true == array_key_exists( 'ProviderDiscountRate', $this->getChangedColumns() ) ) { $strSql .= ' provider_discount_rate = ' . $this->sqlProviderDiscountRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_discount_rate = ' . $this->sqlResidentDiscountRate(). ',' ; } elseif( true == array_key_exists( 'ResidentDiscountRate', $this->getChangedColumns() ) ) { $strSql .= ' resident_discount_rate = ' . $this->sqlResidentDiscountRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_debit_discount_rate = ' . $this->sqlResidentDebitDiscountRate(). ',' ; } elseif( true == array_key_exists( 'ResidentDebitDiscountRate', $this->getChangedColumns() ) ) { $strSql .= ' resident_debit_discount_rate = ' . $this->sqlResidentDebitDiscountRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_discount_rate = ' . $this->sqlCompanyDiscountRate(). ',' ; } elseif( true == array_key_exists( 'CompanyDiscountRate', $this->getChangedColumns() ) ) { $strSql .= ' company_discount_rate = ' . $this->sqlCompanyDiscountRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' waived_discount_rate = ' . $this->sqlWaivedDiscountRate(). ',' ; } elseif( true == array_key_exists( 'WaivedDiscountRate', $this->getChangedColumns() ) ) { $strSql .= ' waived_discount_rate = ' . $this->sqlWaivedDiscountRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' waived_debit_discount_rate = ' . $this->sqlWaivedDebitDiscountRate(). ',' ; } elseif( true == array_key_exists( 'WaivedDebitDiscountRate', $this->getChangedColumns() ) ) { $strSql .= ' waived_debit_discount_rate = ' . $this->sqlWaivedDebitDiscountRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' downgrade_discount_rate = ' . $this->sqlDowngradeDiscountRate(). ',' ; } elseif( true == array_key_exists( 'DowngradeDiscountRate', $this->getChangedColumns() ) ) { $strSql .= ' downgrade_discount_rate = ' . $this->sqlDowngradeDiscountRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' international_discount_rate = ' . $this->sqlInternationalDiscountRate(). ',' ; } elseif( true == array_key_exists( 'InternationalDiscountRate', $this->getChangedColumns() ) ) { $strSql .= ' international_discount_rate = ' . $this->sqlInternationalDiscountRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_debit_discount_rate = ' . $this->sqlCompanyDebitDiscountRate(). ',' ; } elseif( true == array_key_exists( 'CompanyDebitDiscountRate', $this->getChangedColumns() ) ) { $strSql .= ' company_debit_discount_rate = ' . $this->sqlCompanyDebitDiscountRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rebate_discount_rate = ' . $this->sqlRebateDiscountRate(). ',' ; } elseif( true == array_key_exists( 'RebateDiscountRate', $this->getChangedColumns() ) ) { $strSql .= ' rebate_discount_rate = ' . $this->sqlRebateDiscountRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tier_one_company_incentive_fee = ' . $this->sqlTierOneCompanyIncentiveFee(). ',' ; } elseif( true == array_key_exists( 'TierOneCompanyIncentiveFee', $this->getChangedColumns() ) ) { $strSql .= ' tier_one_company_incentive_fee = ' . $this->sqlTierOneCompanyIncentiveFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tier_one_company_incentive_percentage = ' . $this->sqlTierOneCompanyIncentivePercentage(). ',' ; } elseif( true == array_key_exists( 'TierOneCompanyIncentivePercentage', $this->getChangedColumns() ) ) { $strSql .= ' tier_one_company_incentive_percentage = ' . $this->sqlTierOneCompanyIncentivePercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tier_two_company_incentive_fee = ' . $this->sqlTierTwoCompanyIncentiveFee(). ',' ; } elseif( true == array_key_exists( 'TierTwoCompanyIncentiveFee', $this->getChangedColumns() ) ) { $strSql .= ' tier_two_company_incentive_fee = ' . $this->sqlTierTwoCompanyIncentiveFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tier_two_company_incentive_percentage = ' . $this->sqlTierTwoCompanyIncentivePercentage(). ',' ; } elseif( true == array_key_exists( 'TierTwoCompanyIncentivePercentage', $this->getChangedColumns() ) ) { $strSql .= ' tier_two_company_incentive_percentage = ' . $this->sqlTierTwoCompanyIncentivePercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tier_three_company_incentive_fee = ' . $this->sqlTierThreeCompanyIncentiveFee(). ',' ; } elseif( true == array_key_exists( 'TierThreeCompanyIncentiveFee', $this->getChangedColumns() ) ) { $strSql .= ' tier_three_company_incentive_fee = ' . $this->sqlTierThreeCompanyIncentiveFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' every_payment_fee = ' . $this->sqlEveryPaymentFee(). ',' ; } elseif( true == array_key_exists( 'EveryPaymentFee', $this->getChangedColumns() ) ) { $strSql .= ' every_payment_fee = ' . $this->sqlEveryPaymentFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' stored_billing_fee = ' . $this->sqlStoredBillingFee(). ',' ; } elseif( true == array_key_exists( 'StoredBillingFee', $this->getChangedColumns() ) ) { $strSql .= ' stored_billing_fee = ' . $this->sqlStoredBillingFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tier_three_company_incentive_percentage = ' . $this->sqlTierThreeCompanyIncentivePercentage(). ',' ; } elseif( true == array_key_exists( 'TierThreeCompanyIncentivePercentage', $this->getChangedColumns() ) ) { $strSql .= ' tier_three_company_incentive_percentage = ' . $this->sqlTierThreeCompanyIncentivePercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' distribution_delay_days = ' . $this->sqlDistributionDelayDays(). ',' ; } elseif( true == array_key_exists( 'DistributionDelayDays', $this->getChangedColumns() ) ) { $strSql .= ' distribution_delay_days = ' . $this->sqlDistributionDelayDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reversal_delay_days = ' . $this->sqlReversalDelayDays(). ',' ; } elseif( true == array_key_exists( 'ReversalDelayDays', $this->getChangedColumns() ) ) { $strSql .= ' reversal_delay_days = ' . $this->sqlReversalDelayDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cut_off_time = ' . $this->sqlCutOffTime(). ',' ; } elseif( true == array_key_exists( 'CutOffTime', $this->getChangedColumns() ) ) { $strSql .= ' cut_off_time = ' . $this->sqlCutOffTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' calc_fees_on_max_payment = ' . $this->sqlCalcFeesOnMaxPayment(). ',' ; } elseif( true == array_key_exists( 'CalcFeesOnMaxPayment', $this->getChangedColumns() ) ) { $strSql .= ' calc_fees_on_max_payment = ' . $this->sqlCalcFeesOnMaxPayment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_auth_required = ' . $this->sqlPhoneAuthRequired(). ',' ; } elseif( true == array_key_exists( 'PhoneAuthRequired', $this->getChangedColumns() ) ) { $strSql .= ' phone_auth_required = ' . $this->sqlPhoneAuthRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_controlled_distribution = ' . $this->sqlIsControlledDistribution(). ',' ; } elseif( true == array_key_exists( 'IsControlledDistribution', $this->getChangedColumns() ) ) { $strSql .= ' is_controlled_distribution = ' . $this->sqlIsControlledDistribution() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_enabled = ' . $this->sqlIsEnabled(). ',' ; } elseif( true == array_key_exists( 'IsEnabled', $this->getChangedColumns() ) ) { $strSql .= ' is_enabled = ' . $this->sqlIsEnabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_commercial_transaction_fee = ' . $this->sqlResidentCommercialTransactionFee(). ',' ; } elseif( true == array_key_exists( 'ResidentCommercialTransactionFee', $this->getChangedColumns() ) ) { $strSql .= ' resident_commercial_transaction_fee = ' . $this->sqlResidentCommercialTransactionFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_commercial_discount_rate = ' . $this->sqlResidentCommercialDiscountRate(). ',' ; } elseif( true == array_key_exists( 'ResidentCommercialDiscountRate', $this->getChangedColumns() ) ) { $strSql .= ' resident_commercial_discount_rate = ' . $this->sqlResidentCommercialDiscountRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_international_transaction_fee = ' . $this->sqlResidentInternationalTransactionFee(). ',' ; } elseif( true == array_key_exists( 'ResidentInternationalTransactionFee', $this->getChangedColumns() ) ) { $strSql .= ' resident_international_transaction_fee = ' . $this->sqlResidentInternationalTransactionFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_international_discount_rate = ' . $this->sqlResidentInternationalDiscountRate(). ',' ; } elseif( true == array_key_exists( 'ResidentInternationalDiscountRate', $this->getChangedColumns() ) ) { $strSql .= ' resident_international_discount_rate = ' . $this->sqlResidentInternationalDiscountRate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'merchant_account_id' => $this->getMerchantAccountId(),
			'processing_bank_account_id' => $this->getProcessingBankAccountId(),
			'merchant_gateway_id' => $this->getMerchantGatewayId(),
			'payment_type_id' => $this->getPaymentTypeId(),
			'payment_medium_id' => $this->getPaymentMediumId(),
			'pin' => $this->getPin(),
			'gateway_username_encrypted' => $this->getGatewayUsernameEncrypted(),
			'gateway_password_encrypted' => $this->getGatewayPasswordEncrypted(),
			'login_username_encrypted' => $this->getLoginUsernameEncrypted(),
			'login_password_encrypted' => $this->getLoginPasswordEncrypted(),
			'max_payment_amount' => $this->getMaxPaymentAmount(),
			'undelayed_settlement_ceiling' => $this->getUndelayedSettlementCeiling(),
			'max_monthly_process_amount' => $this->getMaxMonthlyProcessAmount(),
			'minimum_convenience_fee' => $this->getMinimumConvenienceFee(),
			'gateway_transaction_fee' => $this->getGatewayTransactionFee(),
			'resident_transaction_fee' => $this->getResidentTransactionFee(),
			'resident_debit_transaction_fee' => $this->getResidentDebitTransactionFee(),
			'transaction_fee' => $this->getTransactionFee(),
			'reverse_transaction_fee' => $this->getReverseTransactionFee(),
			'waived_transaction_fee' => $this->getWaivedTransactionFee(),
			'waived_debit_transaction_fee' => $this->getWaivedDebitTransactionFee(),
			'company_return_fee' => $this->getCompanyReturnFee(),
			'company_adjustment_fee' => $this->getCompanyAdjustmentFee(),
			'auth_success_fee' => $this->getAuthSuccessFee(),
			'auth_failure_fee' => $this->getAuthFailureFee(),
			'downgrade_transaction_fee' => $this->getDowngradeTransactionFee(),
			'international_transaction_fee' => $this->getInternationalTransactionFee(),
			'resident_return_fee' => $this->getResidentReturnFee(),
			'unauthorized_return_fee' => $this->getUnauthorizedReturnFee(),
			'company_debit_transaction_fee' => $this->getCompanyDebitTransactionFee(),
			'rebate_transaction_fee' => $this->getRebateTransactionFee(),
			'provider_discount_rate' => $this->getProviderDiscountRate(),
			'resident_discount_rate' => $this->getResidentDiscountRate(),
			'resident_debit_discount_rate' => $this->getResidentDebitDiscountRate(),
			'company_discount_rate' => $this->getCompanyDiscountRate(),
			'waived_discount_rate' => $this->getWaivedDiscountRate(),
			'waived_debit_discount_rate' => $this->getWaivedDebitDiscountRate(),
			'downgrade_discount_rate' => $this->getDowngradeDiscountRate(),
			'international_discount_rate' => $this->getInternationalDiscountRate(),
			'company_debit_discount_rate' => $this->getCompanyDebitDiscountRate(),
			'rebate_discount_rate' => $this->getRebateDiscountRate(),
			'tier_one_company_incentive_fee' => $this->getTierOneCompanyIncentiveFee(),
			'tier_one_company_incentive_percentage' => $this->getTierOneCompanyIncentivePercentage(),
			'tier_two_company_incentive_fee' => $this->getTierTwoCompanyIncentiveFee(),
			'tier_two_company_incentive_percentage' => $this->getTierTwoCompanyIncentivePercentage(),
			'tier_three_company_incentive_fee' => $this->getTierThreeCompanyIncentiveFee(),
			'every_payment_fee' => $this->getEveryPaymentFee(),
			'stored_billing_fee' => $this->getStoredBillingFee(),
			'tier_three_company_incentive_percentage' => $this->getTierThreeCompanyIncentivePercentage(),
			'distribution_delay_days' => $this->getDistributionDelayDays(),
			'reversal_delay_days' => $this->getReversalDelayDays(),
			'cut_off_time' => $this->getCutOffTime(),
			'calc_fees_on_max_payment' => $this->getCalcFeesOnMaxPayment(),
			'phone_auth_required' => $this->getPhoneAuthRequired(),
			'is_controlled_distribution' => $this->getIsControlledDistribution(),
			'is_enabled' => $this->getIsEnabled(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'details' => $this->getDetails(),
			'resident_commercial_transaction_fee' => $this->getResidentCommercialTransactionFee(),
			'resident_commercial_discount_rate' => $this->getResidentCommercialDiscountRate(),
			'resident_international_transaction_fee' => $this->getResidentInternationalTransactionFee(),
			'resident_international_discount_rate' => $this->getResidentInternationalDiscountRate()
		);
	}

}
?>