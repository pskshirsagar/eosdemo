<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyPreferences
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyPreferences extends CEosPluralBase {

	/**
	 * @return CPropertyPreference[]
	 */
	public static function fetchPropertyPreferences( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CPropertyPreference::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyPreference
	 */
	public static function fetchPropertyPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyPreference::class, $objDatabase );
	}

	public static function fetchPropertyPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_preferences', $objDatabase );
	}

	public static function fetchPropertyPreferenceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyPreference( sprintf( 'SELECT * FROM property_preferences WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyPreferencesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyPreferences( sprintf( 'SELECT * FROM property_preferences WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchPropertyPreferencesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyPreferences( sprintf( 'SELECT * FROM property_preferences WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyPreferencesByPropertySettingKeyIdByCid( $intPropertySettingKeyId, $intCid, $objDatabase ) {
		return self::fetchPropertyPreferences( sprintf( 'SELECT * FROM property_preferences WHERE property_setting_key_id = %d AND cid = %d', $intPropertySettingKeyId, $intCid ), $objDatabase );
	}

	public static function fetchPropertyPreferencesByPropertyPreferenceTypeIdByCid( $intPropertyPreferenceTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertyPreferences( sprintf( 'SELECT * FROM property_preferences WHERE property_preference_type_id = %d AND cid = %d', $intPropertyPreferenceTypeId, $intCid ), $objDatabase );
	}

}
?>