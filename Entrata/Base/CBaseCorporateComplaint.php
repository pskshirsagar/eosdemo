<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCorporateComplaint extends CEosSingularBase {

	const TABLE_NAME = 'public.corporate_complaints';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_arrintCorporateComplaintTypeIds;
	protected $m_intContactSubmissionId;
	protected $m_intCorporateComplaintId;
	protected $m_intCompanyEmployeeId;
	protected $m_intMaintenanceRequestId;
	protected $m_strBusinessName;
	protected $m_strServiceName;
	protected $m_intCallId;
	protected $m_strNotes;
	protected $m_intResolvedBy;
	protected $m_strResolvedOn;
	protected $m_intAssignedBy;
	protected $m_strAssignedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strBusinessName = NULL;
		$this->m_strServiceName = NULL;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['corporate_complaint_type_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintCorporateComplaintTypeIds', trim( $arrValues['corporate_complaint_type_ids'] ) ); elseif( isset( $arrValues['corporate_complaint_type_ids'] ) ) $this->setCorporateComplaintTypeIds( $arrValues['corporate_complaint_type_ids'] );
		if( isset( $arrValues['contact_submission_id'] ) && $boolDirectSet ) $this->set( 'm_intContactSubmissionId', trim( $arrValues['contact_submission_id'] ) ); elseif( isset( $arrValues['contact_submission_id'] ) ) $this->setContactSubmissionId( $arrValues['contact_submission_id'] );
		if( isset( $arrValues['corporate_complaint_id'] ) && $boolDirectSet ) $this->set( 'm_intCorporateComplaintId', trim( $arrValues['corporate_complaint_id'] ) ); elseif( isset( $arrValues['corporate_complaint_id'] ) ) $this->setCorporateComplaintId( $arrValues['corporate_complaint_id'] );
		if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
		if( isset( $arrValues['maintenance_request_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceRequestId', trim( $arrValues['maintenance_request_id'] ) ); elseif( isset( $arrValues['maintenance_request_id'] ) ) $this->setMaintenanceRequestId( $arrValues['maintenance_request_id'] );
		if( isset( $arrValues['business_name'] ) && $boolDirectSet ) $this->set( 'm_strBusinessName', trim( stripcslashes( $arrValues['business_name'] ) ) ); elseif( isset( $arrValues['business_name'] ) ) $this->setBusinessName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['business_name'] ) : $arrValues['business_name'] );
		if( isset( $arrValues['service_name'] ) && $boolDirectSet ) $this->set( 'm_strServiceName', trim( stripcslashes( $arrValues['service_name'] ) ) ); elseif( isset( $arrValues['service_name'] ) ) $this->setServiceName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['service_name'] ) : $arrValues['service_name'] );
		if( isset( $arrValues['call_id'] ) && $boolDirectSet ) $this->set( 'm_intCallId', trim( $arrValues['call_id'] ) ); elseif( isset( $arrValues['call_id'] ) ) $this->setCallId( $arrValues['call_id'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['resolved_by'] ) && $boolDirectSet ) $this->set( 'm_intResolvedBy', trim( $arrValues['resolved_by'] ) ); elseif( isset( $arrValues['resolved_by'] ) ) $this->setResolvedBy( $arrValues['resolved_by'] );
		if( isset( $arrValues['resolved_on'] ) && $boolDirectSet ) $this->set( 'm_strResolvedOn', trim( $arrValues['resolved_on'] ) ); elseif( isset( $arrValues['resolved_on'] ) ) $this->setResolvedOn( $arrValues['resolved_on'] );
		if( isset( $arrValues['assigned_by'] ) && $boolDirectSet ) $this->set( 'm_intAssignedBy', trim( $arrValues['assigned_by'] ) ); elseif( isset( $arrValues['assigned_by'] ) ) $this->setAssignedBy( $arrValues['assigned_by'] );
		if( isset( $arrValues['assigned_on'] ) && $boolDirectSet ) $this->set( 'm_strAssignedOn', trim( $arrValues['assigned_on'] ) ); elseif( isset( $arrValues['assigned_on'] ) ) $this->setAssignedOn( $arrValues['assigned_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCorporateComplaintTypeIds( $arrintCorporateComplaintTypeIds ) {
		$this->set( 'm_arrintCorporateComplaintTypeIds', CStrings::strToArrIntDef( $arrintCorporateComplaintTypeIds, NULL ) );
	}

	public function getCorporateComplaintTypeIds() {
		return $this->m_arrintCorporateComplaintTypeIds;
	}

	public function sqlCorporateComplaintTypeIds() {
		return ( true == isset( $this->m_arrintCorporateComplaintTypeIds ) && true == valArr( $this->m_arrintCorporateComplaintTypeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintCorporateComplaintTypeIds, NULL ) . '\'' : 'NULL';
	}

	public function setContactSubmissionId( $intContactSubmissionId ) {
		$this->set( 'm_intContactSubmissionId', CStrings::strToIntDef( $intContactSubmissionId, NULL, false ) );
	}

	public function getContactSubmissionId() {
		return $this->m_intContactSubmissionId;
	}

	public function sqlContactSubmissionId() {
		return ( true == isset( $this->m_intContactSubmissionId ) ) ? ( string ) $this->m_intContactSubmissionId : 'NULL';
	}

	public function setCorporateComplaintId( $intCorporateComplaintId ) {
		$this->set( 'm_intCorporateComplaintId', CStrings::strToIntDef( $intCorporateComplaintId, NULL, false ) );
	}

	public function getCorporateComplaintId() {
		return $this->m_intCorporateComplaintId;
	}

	public function sqlCorporateComplaintId() {
		return ( true == isset( $this->m_intCorporateComplaintId ) ) ? ( string ) $this->m_intCorporateComplaintId : 'NULL';
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function sqlCompanyEmployeeId() {
		return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
	}

	public function setMaintenanceRequestId( $intMaintenanceRequestId ) {
		$this->set( 'm_intMaintenanceRequestId', CStrings::strToIntDef( $intMaintenanceRequestId, NULL, false ) );
	}

	public function getMaintenanceRequestId() {
		return $this->m_intMaintenanceRequestId;
	}

	public function sqlMaintenanceRequestId() {
		return ( true == isset( $this->m_intMaintenanceRequestId ) ) ? ( string ) $this->m_intMaintenanceRequestId : 'NULL';
	}

	public function setBusinessName( $strBusinessName ) {
		$this->set( 'm_strBusinessName', CStrings::strTrimDef( $strBusinessName, 250, NULL, true ) );
	}

	public function getBusinessName() {
		return $this->m_strBusinessName;
	}

	public function sqlBusinessName() {
		return ( true == isset( $this->m_strBusinessName ) ) ? '\'' . addslashes( $this->m_strBusinessName ) . '\'' : '\'NULL\'';
	}

	public function setServiceName( $strServiceName ) {
		$this->set( 'm_strServiceName', CStrings::strTrimDef( $strServiceName, 250, NULL, true ) );
	}

	public function getServiceName() {
		return $this->m_strServiceName;
	}

	public function sqlServiceName() {
		return ( true == isset( $this->m_strServiceName ) ) ? '\'' . addslashes( $this->m_strServiceName ) . '\'' : '\'NULL\'';
	}

	public function setCallId( $intCallId ) {
		$this->set( 'm_intCallId', CStrings::strToIntDef( $intCallId, NULL, false ) );
	}

	public function getCallId() {
		return $this->m_intCallId;
	}

	public function sqlCallId() {
		return ( true == isset( $this->m_intCallId ) ) ? ( string ) $this->m_intCallId : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setResolvedBy( $intResolvedBy ) {
		$this->set( 'm_intResolvedBy', CStrings::strToIntDef( $intResolvedBy, NULL, false ) );
	}

	public function getResolvedBy() {
		return $this->m_intResolvedBy;
	}

	public function sqlResolvedBy() {
		return ( true == isset( $this->m_intResolvedBy ) ) ? ( string ) $this->m_intResolvedBy : 'NULL';
	}

	public function setResolvedOn( $strResolvedOn ) {
		$this->set( 'm_strResolvedOn', CStrings::strTrimDef( $strResolvedOn, -1, NULL, true ) );
	}

	public function getResolvedOn() {
		return $this->m_strResolvedOn;
	}

	public function sqlResolvedOn() {
		return ( true == isset( $this->m_strResolvedOn ) ) ? '\'' . $this->m_strResolvedOn . '\'' : 'NULL';
	}

	public function setAssignedBy( $intAssignedBy ) {
		$this->set( 'm_intAssignedBy', CStrings::strToIntDef( $intAssignedBy, NULL, false ) );
	}

	public function getAssignedBy() {
		return $this->m_intAssignedBy;
	}

	public function sqlAssignedBy() {
		return ( true == isset( $this->m_intAssignedBy ) ) ? ( string ) $this->m_intAssignedBy : 'NULL';
	}

	public function setAssignedOn( $strAssignedOn ) {
		$this->set( 'm_strAssignedOn', CStrings::strTrimDef( $strAssignedOn, -1, NULL, true ) );
	}

	public function getAssignedOn() {
		return $this->m_strAssignedOn;
	}

	public function sqlAssignedOn() {
		return ( true == isset( $this->m_strAssignedOn ) ) ? '\'' . $this->m_strAssignedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, corporate_complaint_type_ids, contact_submission_id, corporate_complaint_id, company_employee_id, maintenance_request_id, business_name, service_name, call_id, notes, resolved_by, resolved_on, assigned_by, assigned_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlCorporateComplaintTypeIds() . ', ' .
 						$this->sqlContactSubmissionId() . ', ' .
 						$this->sqlCorporateComplaintId() . ', ' .
 						$this->sqlCompanyEmployeeId() . ', ' .
 						$this->sqlMaintenanceRequestId() . ', ' .
 						$this->sqlBusinessName() . ', ' .
 						$this->sqlServiceName() . ', ' .
 						$this->sqlCallId() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlResolvedBy() . ', ' .
 						$this->sqlResolvedOn() . ', ' .
 						$this->sqlAssignedBy() . ', ' .
 						$this->sqlAssignedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' corporate_complaint_type_ids = ' . $this->sqlCorporateComplaintTypeIds() . ','; } elseif( true == array_key_exists( 'CorporateComplaintTypeIds', $this->getChangedColumns() ) ) { $strSql .= ' corporate_complaint_type_ids = ' . $this->sqlCorporateComplaintTypeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_submission_id = ' . $this->sqlContactSubmissionId() . ','; } elseif( true == array_key_exists( 'ContactSubmissionId', $this->getChangedColumns() ) ) { $strSql .= ' contact_submission_id = ' . $this->sqlContactSubmissionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' corporate_complaint_id = ' . $this->sqlCorporateComplaintId() . ','; } elseif( true == array_key_exists( 'CorporateComplaintId', $this->getChangedColumns() ) ) { $strSql .= ' corporate_complaint_id = ' . $this->sqlCorporateComplaintId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; } elseif( true == array_key_exists( 'CompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId() . ','; } elseif( true == array_key_exists( 'MaintenanceRequestId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' business_name = ' . $this->sqlBusinessName() . ','; } elseif( true == array_key_exists( 'BusinessName', $this->getChangedColumns() ) ) { $strSql .= ' business_name = ' . $this->sqlBusinessName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_name = ' . $this->sqlServiceName() . ','; } elseif( true == array_key_exists( 'ServiceName', $this->getChangedColumns() ) ) { $strSql .= ' service_name = ' . $this->sqlServiceName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; } elseif( true == array_key_exists( 'CallId', $this->getChangedColumns() ) ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolved_by = ' . $this->sqlResolvedBy() . ','; } elseif( true == array_key_exists( 'ResolvedBy', $this->getChangedColumns() ) ) { $strSql .= ' resolved_by = ' . $this->sqlResolvedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolved_on = ' . $this->sqlResolvedOn() . ','; } elseif( true == array_key_exists( 'ResolvedOn', $this->getChangedColumns() ) ) { $strSql .= ' resolved_on = ' . $this->sqlResolvedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' assigned_by = ' . $this->sqlAssignedBy() . ','; } elseif( true == array_key_exists( 'AssignedBy', $this->getChangedColumns() ) ) { $strSql .= ' assigned_by = ' . $this->sqlAssignedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' assigned_on = ' . $this->sqlAssignedOn() . ','; } elseif( true == array_key_exists( 'AssignedOn', $this->getChangedColumns() ) ) { $strSql .= ' assigned_on = ' . $this->sqlAssignedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'corporate_complaint_type_ids' => $this->getCorporateComplaintTypeIds(),
			'contact_submission_id' => $this->getContactSubmissionId(),
			'corporate_complaint_id' => $this->getCorporateComplaintId(),
			'company_employee_id' => $this->getCompanyEmployeeId(),
			'maintenance_request_id' => $this->getMaintenanceRequestId(),
			'business_name' => $this->getBusinessName(),
			'service_name' => $this->getServiceName(),
			'call_id' => $this->getCallId(),
			'notes' => $this->getNotes(),
			'resolved_by' => $this->getResolvedBy(),
			'resolved_on' => $this->getResolvedOn(),
			'assigned_by' => $this->getAssignedBy(),
			'assigned_on' => $this->getAssignedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>