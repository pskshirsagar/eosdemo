<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseUnitSpaceExclusion extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.unit_space_exclusions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUnitExclusionReasonTypeId;
	protected $m_intGlAccountId;
	protected $m_intIsMarketed;
	protected $m_boolIsReportable;
	protected $m_strCustomName;
	protected $m_boolIsConsideredOccupied;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intUnitExclusionReasonTypeId = '8';
		$this->m_intIsMarketed = '0';
		$this->m_boolIsReportable = false;
		$this->m_strCustomName = 'Construction Unit';
		$this->m_boolIsConsideredOccupied = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['unit_exclusion_reason_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitExclusionReasonTypeId', trim( $arrValues['unit_exclusion_reason_type_id'] ) ); elseif( isset( $arrValues['unit_exclusion_reason_type_id'] ) ) $this->setUnitExclusionReasonTypeId( $arrValues['unit_exclusion_reason_type_id'] );
		if( isset( $arrValues['gl_account_id'] ) && $boolDirectSet ) $this->set( 'm_intGlAccountId', trim( $arrValues['gl_account_id'] ) ); elseif( isset( $arrValues['gl_account_id'] ) ) $this->setGlAccountId( $arrValues['gl_account_id'] );
		if( isset( $arrValues['is_marketed'] ) && $boolDirectSet ) $this->set( 'm_intIsMarketed', trim( $arrValues['is_marketed'] ) ); elseif( isset( $arrValues['is_marketed'] ) ) $this->setIsMarketed( $arrValues['is_marketed'] );
		if( isset( $arrValues['is_reportable'] ) && $boolDirectSet ) $this->set( 'm_boolIsReportable', trim( stripcslashes( $arrValues['is_reportable'] ) ) ); elseif( isset( $arrValues['is_reportable'] ) ) $this->setIsReportable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_reportable'] ) : $arrValues['is_reportable'] );
		if( isset( $arrValues['custom_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strCustomName', trim( stripcslashes( $arrValues['custom_name'] ) ) ); elseif( isset( $arrValues['custom_name'] ) ) $this->setCustomName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['custom_name'] ) : $arrValues['custom_name'] );
		if( isset( $arrValues['is_considered_occupied'] ) && $boolDirectSet ) $this->set( 'm_boolIsConsideredOccupied', trim( stripcslashes( $arrValues['is_considered_occupied'] ) ) ); elseif( isset( $arrValues['is_considered_occupied'] ) ) $this->setIsConsideredOccupied( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_considered_occupied'] ) : $arrValues['is_considered_occupied'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUnitExclusionReasonTypeId( $intUnitExclusionReasonTypeId ) {
		$this->set( 'm_intUnitExclusionReasonTypeId', CStrings::strToIntDef( $intUnitExclusionReasonTypeId, NULL, false ) );
	}

	public function getUnitExclusionReasonTypeId() {
		return $this->m_intUnitExclusionReasonTypeId;
	}

	public function sqlUnitExclusionReasonTypeId() {
		return ( true == isset( $this->m_intUnitExclusionReasonTypeId ) ) ? ( string ) $this->m_intUnitExclusionReasonTypeId : '8';
	}

	public function setGlAccountId( $intGlAccountId ) {
		$this->set( 'm_intGlAccountId', CStrings::strToIntDef( $intGlAccountId, NULL, false ) );
	}

	public function getGlAccountId() {
		return $this->m_intGlAccountId;
	}

	public function sqlGlAccountId() {
		return ( true == isset( $this->m_intGlAccountId ) ) ? ( string ) $this->m_intGlAccountId : 'NULL';
	}

	public function setIsMarketed( $intIsMarketed ) {
		$this->set( 'm_intIsMarketed', CStrings::strToIntDef( $intIsMarketed, NULL, false ) );
	}

	public function getIsMarketed() {
		return $this->m_intIsMarketed;
	}

	public function sqlIsMarketed() {
		return ( true == isset( $this->m_intIsMarketed ) ) ? ( string ) $this->m_intIsMarketed : '0';
	}

	public function setIsReportable( $boolIsReportable ) {
		$this->set( 'm_boolIsReportable', CStrings::strToBool( $boolIsReportable ) );
	}

	public function getIsReportable() {
		return $this->m_boolIsReportable;
	}

	public function sqlIsReportable() {
		return ( true == isset( $this->m_boolIsReportable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReportable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCustomName( $strCustomName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strCustomName', CStrings::strTrimDef( $strCustomName, 2000, NULL, true ), $strLocaleCode );
	}

	public function getCustomName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strCustomName', $strLocaleCode );
	}

	public function sqlCustomName() {
		return ( true == isset( $this->m_strCustomName ) ) ? '\'' . addslashes( $this->m_strCustomName ) . '\'' : '\'Construction Unit\'';
	}

	public function setIsConsideredOccupied( $boolIsConsideredOccupied ) {
		$this->set( 'm_boolIsConsideredOccupied', CStrings::strToBool( $boolIsConsideredOccupied ) );
	}

	public function getIsConsideredOccupied() {
		return $this->m_boolIsConsideredOccupied;
	}

	public function sqlIsConsideredOccupied() {
		return ( true == isset( $this->m_boolIsConsideredOccupied ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsConsideredOccupied ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, unit_exclusion_reason_type_id, gl_account_id, is_marketed, is_reportable, custom_name, is_considered_occupied, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUnitExclusionReasonTypeId() . ', ' .
						$this->sqlGlAccountId() . ', ' .
						$this->sqlIsMarketed() . ', ' .
						$this->sqlIsReportable() . ', ' .
						$this->sqlCustomName() . ', ' .
						$this->sqlIsConsideredOccupied() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_exclusion_reason_type_id = ' . $this->sqlUnitExclusionReasonTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitExclusionReasonTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_exclusion_reason_type_id = ' . $this->sqlUnitExclusionReasonTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_account_id = ' . $this->sqlGlAccountId(). ',' ; } elseif( true == array_key_exists( 'GlAccountId', $this->getChangedColumns() ) ) { $strSql .= ' gl_account_id = ' . $this->sqlGlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_marketed = ' . $this->sqlIsMarketed(). ',' ; } elseif( true == array_key_exists( 'IsMarketed', $this->getChangedColumns() ) ) { $strSql .= ' is_marketed = ' . $this->sqlIsMarketed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reportable = ' . $this->sqlIsReportable(). ',' ; } elseif( true == array_key_exists( 'IsReportable', $this->getChangedColumns() ) ) { $strSql .= ' is_reportable = ' . $this->sqlIsReportable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' custom_name = ' . $this->sqlCustomName(). ',' ; } elseif( true == array_key_exists( 'CustomName', $this->getChangedColumns() ) ) { $strSql .= ' custom_name = ' . $this->sqlCustomName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_considered_occupied = ' . $this->sqlIsConsideredOccupied(). ',' ; } elseif( true == array_key_exists( 'IsConsideredOccupied', $this->getChangedColumns() ) ) { $strSql .= ' is_considered_occupied = ' . $this->sqlIsConsideredOccupied() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'unit_exclusion_reason_type_id' => $this->getUnitExclusionReasonTypeId(),
			'gl_account_id' => $this->getGlAccountId(),
			'is_marketed' => $this->getIsMarketed(),
			'is_reportable' => $this->getIsReportable(),
			'custom_name' => $this->getCustomName(),
			'is_considered_occupied' => $this->getIsConsideredOccupied(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>