<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyApplicationAddOnGroups
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyApplicationAddOnGroups extends CEosPluralBase {

	/**
	 * @return CCompanyApplicationAddOnGroup[]
	 */
	public static function fetchCompanyApplicationAddOnGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyApplicationAddOnGroup', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyApplicationAddOnGroup
	 */
	public static function fetchCompanyApplicationAddOnGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyApplicationAddOnGroup', $objDatabase );
	}

	public static function fetchCompanyApplicationAddOnGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_application_add_on_groups', $objDatabase );
	}

	public static function fetchCompanyApplicationAddOnGroupByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyApplicationAddOnGroup( sprintf( 'SELECT * FROM company_application_add_on_groups WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyApplicationAddOnGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyApplicationAddOnGroups( sprintf( 'SELECT * FROM company_application_add_on_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyApplicationAddOnGroupsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCompanyApplicationAddOnGroups( sprintf( 'SELECT * FROM company_application_add_on_groups WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyApplicationAddOnGroupsByCompanyApplicationIdByCid( $intCompanyApplicationId, $intCid, $objDatabase ) {
		return self::fetchCompanyApplicationAddOnGroups( sprintf( 'SELECT * FROM company_application_add_on_groups WHERE company_application_id = %d AND cid = %d', ( int ) $intCompanyApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyApplicationAddOnGroupsByAddOnGroupIdByCid( $intAddOnGroupId, $intCid, $objDatabase ) {
		return self::fetchCompanyApplicationAddOnGroups( sprintf( 'SELECT * FROM company_application_add_on_groups WHERE add_on_group_id = %d AND cid = %d', ( int ) $intAddOnGroupId, ( int ) $intCid ), $objDatabase );
	}

}
?>