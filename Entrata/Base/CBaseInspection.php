<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseInspection extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.inspections';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intInspectionFormId;
	protected $m_strInspectionDetails;
	protected $m_jsonInspectionDetails;
	protected $m_intPropertyId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_intCustomerId;
	protected $m_intLeaseId;
	protected $m_intMaintenanceRequestId;
	protected $m_intSignatureFileId;
	protected $m_intInspectionFileId;
	protected $m_intPsProductId;
	protected $m_strRemotePrimaryKey;
	protected $m_intScheduledTaskId;
	protected $m_intMaintenanceGroupId;
	protected $m_strScheduledDate;
	protected $m_strInspectionDueDate;
	protected $m_strNotes;
	protected $m_intIsResidentVisible;
	protected $m_intIsStaffInspection;
	protected $m_strSignatureDatetime;
	protected $m_intInspectedBy;
	protected $m_strInspectedOn;
	protected $m_intReviewedBy;
	protected $m_strReviewedOn;
	protected $m_intInspectionTypeId;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intIsResidentVisible = '0';
		$this->m_intIsStaffInspection = '1';
		$this->m_intInspectionTypeId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['inspection_form_id'] ) && $boolDirectSet ) $this->set( 'm_intInspectionFormId', trim( $arrValues['inspection_form_id'] ) ); elseif( isset( $arrValues['inspection_form_id'] ) ) $this->setInspectionFormId( $arrValues['inspection_form_id'] );
		if( isset( $arrValues['inspection_details'] ) ) $this->set( 'm_strInspectionDetails', trim( $arrValues['inspection_details'] ) );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['maintenance_request_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceRequestId', trim( $arrValues['maintenance_request_id'] ) ); elseif( isset( $arrValues['maintenance_request_id'] ) ) $this->setMaintenanceRequestId( $arrValues['maintenance_request_id'] );
		if( isset( $arrValues['signature_file_id'] ) && $boolDirectSet ) $this->set( 'm_intSignatureFileId', trim( $arrValues['signature_file_id'] ) ); elseif( isset( $arrValues['signature_file_id'] ) ) $this->setSignatureFileId( $arrValues['signature_file_id'] );
		if( isset( $arrValues['inspection_file_id'] ) && $boolDirectSet ) $this->set( 'm_intInspectionFileId', trim( $arrValues['inspection_file_id'] ) ); elseif( isset( $arrValues['inspection_file_id'] ) ) $this->setInspectionFileId( $arrValues['inspection_file_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['scheduled_task_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledTaskId', trim( $arrValues['scheduled_task_id'] ) ); elseif( isset( $arrValues['scheduled_task_id'] ) ) $this->setScheduledTaskId( $arrValues['scheduled_task_id'] );
		if( isset( $arrValues['maintenance_group_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceGroupId', trim( $arrValues['maintenance_group_id'] ) ); elseif( isset( $arrValues['maintenance_group_id'] ) ) $this->setMaintenanceGroupId( $arrValues['maintenance_group_id'] );
		if( isset( $arrValues['scheduled_date'] ) && $boolDirectSet ) $this->set( 'm_strScheduledDate', trim( $arrValues['scheduled_date'] ) ); elseif( isset( $arrValues['scheduled_date'] ) ) $this->setScheduledDate( $arrValues['scheduled_date'] );
		if( isset( $arrValues['inspection_due_date'] ) && $boolDirectSet ) $this->set( 'm_strInspectionDueDate', trim( $arrValues['inspection_due_date'] ) ); elseif( isset( $arrValues['inspection_due_date'] ) ) $this->setInspectionDueDate( $arrValues['inspection_due_date'] );
		if( isset( $arrValues['notes'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['is_resident_visible'] ) && $boolDirectSet ) $this->set( 'm_intIsResidentVisible', trim( $arrValues['is_resident_visible'] ) ); elseif( isset( $arrValues['is_resident_visible'] ) ) $this->setIsResidentVisible( $arrValues['is_resident_visible'] );
		if( isset( $arrValues['is_staff_inspection'] ) && $boolDirectSet ) $this->set( 'm_intIsStaffInspection', trim( $arrValues['is_staff_inspection'] ) ); elseif( isset( $arrValues['is_staff_inspection'] ) ) $this->setIsStaffInspection( $arrValues['is_staff_inspection'] );
		if( isset( $arrValues['signature_datetime'] ) && $boolDirectSet ) $this->set( 'm_strSignatureDatetime', trim( $arrValues['signature_datetime'] ) ); elseif( isset( $arrValues['signature_datetime'] ) ) $this->setSignatureDatetime( $arrValues['signature_datetime'] );
		if( isset( $arrValues['inspected_by'] ) && $boolDirectSet ) $this->set( 'm_intInspectedBy', trim( $arrValues['inspected_by'] ) ); elseif( isset( $arrValues['inspected_by'] ) ) $this->setInspectedBy( $arrValues['inspected_by'] );
		if( isset( $arrValues['inspected_on'] ) && $boolDirectSet ) $this->set( 'm_strInspectedOn', trim( $arrValues['inspected_on'] ) ); elseif( isset( $arrValues['inspected_on'] ) ) $this->setInspectedOn( $arrValues['inspected_on'] );
		if( isset( $arrValues['reviewed_by'] ) && $boolDirectSet ) $this->set( 'm_intReviewedBy', trim( $arrValues['reviewed_by'] ) ); elseif( isset( $arrValues['reviewed_by'] ) ) $this->setReviewedBy( $arrValues['reviewed_by'] );
		if( isset( $arrValues['reviewed_on'] ) && $boolDirectSet ) $this->set( 'm_strReviewedOn', trim( $arrValues['reviewed_on'] ) ); elseif( isset( $arrValues['reviewed_on'] ) ) $this->setReviewedOn( $arrValues['reviewed_on'] );
		if( isset( $arrValues['inspection_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInspectionTypeId', trim( $arrValues['inspection_type_id'] ) ); elseif( isset( $arrValues['inspection_type_id'] ) ) $this->setInspectionTypeId( $arrValues['inspection_type_id'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setInspectionFormId( $intInspectionFormId ) {
		$this->set( 'm_intInspectionFormId', CStrings::strToIntDef( $intInspectionFormId, NULL, false ) );
	}

	public function getInspectionFormId() {
		return $this->m_intInspectionFormId;
	}

	public function sqlInspectionFormId() {
		return ( true == isset( $this->m_intInspectionFormId ) ) ? ( string ) $this->m_intInspectionFormId : 'NULL';
	}

	public function setInspectionDetails( $jsonInspectionDetails ) {
		if( true == valObj( $jsonInspectionDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonInspectionDetails', $jsonInspectionDetails );
		} elseif( true == valJsonString( $jsonInspectionDetails ) ) {
			$this->set( 'm_jsonInspectionDetails', CStrings::strToJson( $jsonInspectionDetails ) );
		} else {
			$this->set( 'm_jsonInspectionDetails', NULL ); 
		}
		unset( $this->m_strInspectionDetails );
	}

	public function getInspectionDetails() {
		if( true == isset( $this->m_strInspectionDetails ) ) {
			$this->m_jsonInspectionDetails = CStrings::strToJson( $this->m_strInspectionDetails );
			unset( $this->m_strInspectionDetails );
		}
		return $this->m_jsonInspectionDetails;
	}

	public function sqlInspectionDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getInspectionDetails() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getInspectionDetails() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setMaintenanceRequestId( $intMaintenanceRequestId ) {
		$this->set( 'm_intMaintenanceRequestId', CStrings::strToIntDef( $intMaintenanceRequestId, NULL, false ) );
	}

	public function getMaintenanceRequestId() {
		return $this->m_intMaintenanceRequestId;
	}

	public function sqlMaintenanceRequestId() {
		return ( true == isset( $this->m_intMaintenanceRequestId ) ) ? ( string ) $this->m_intMaintenanceRequestId : 'NULL';
	}

	public function setSignatureFileId( $intSignatureFileId ) {
		$this->set( 'm_intSignatureFileId', CStrings::strToIntDef( $intSignatureFileId, NULL, false ) );
	}

	public function getSignatureFileId() {
		return $this->m_intSignatureFileId;
	}

	public function sqlSignatureFileId() {
		return ( true == isset( $this->m_intSignatureFileId ) ) ? ( string ) $this->m_intSignatureFileId : 'NULL';
	}

	public function setInspectionFileId( $intInspectionFileId ) {
		$this->set( 'm_intInspectionFileId', CStrings::strToIntDef( $intInspectionFileId, NULL, false ) );
	}

	public function getInspectionFileId() {
		return $this->m_intInspectionFileId;
	}

	public function sqlInspectionFileId() {
		return ( true == isset( $this->m_intInspectionFileId ) ) ? ( string ) $this->m_intInspectionFileId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setScheduledTaskId( $intScheduledTaskId ) {
		$this->set( 'm_intScheduledTaskId', CStrings::strToIntDef( $intScheduledTaskId, NULL, false ) );
	}

	public function getScheduledTaskId() {
		return $this->m_intScheduledTaskId;
	}

	public function sqlScheduledTaskId() {
		return ( true == isset( $this->m_intScheduledTaskId ) ) ? ( string ) $this->m_intScheduledTaskId : 'NULL';
	}

	public function setMaintenanceGroupId( $intMaintenanceGroupId ) {
		$this->set( 'm_intMaintenanceGroupId', CStrings::strToIntDef( $intMaintenanceGroupId, NULL, false ) );
	}

	public function getMaintenanceGroupId() {
		return $this->m_intMaintenanceGroupId;
	}

	public function sqlMaintenanceGroupId() {
		return ( true == isset( $this->m_intMaintenanceGroupId ) ) ? ( string ) $this->m_intMaintenanceGroupId : 'NULL';
	}

	public function setScheduledDate( $strScheduledDate ) {
		$this->set( 'm_strScheduledDate', CStrings::strTrimDef( $strScheduledDate, -1, NULL, true ) );
	}

	public function getScheduledDate() {
		return $this->m_strScheduledDate;
	}

	public function sqlScheduledDate() {
		return ( true == isset( $this->m_strScheduledDate ) ) ? '\'' . $this->m_strScheduledDate . '\'' : 'NULL';
	}

	public function setInspectionDueDate( $strInspectionDueDate ) {
		$this->set( 'm_strInspectionDueDate', CStrings::strTrimDef( $strInspectionDueDate, -1, NULL, true ) );
	}

	public function getInspectionDueDate() {
		return $this->m_strInspectionDueDate;
	}

	public function sqlInspectionDueDate() {
		return ( true == isset( $this->m_strInspectionDueDate ) ) ? '\'' . $this->m_strInspectionDueDate . '\'' : 'NULL';
	}

	public function setNotes( $strNotes, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ), $strLocaleCode );
	}

	public function getNotes( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strNotes', $strLocaleCode );
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setIsResidentVisible( $intIsResidentVisible ) {
		$this->set( 'm_intIsResidentVisible', CStrings::strToIntDef( $intIsResidentVisible, NULL, false ) );
	}

	public function getIsResidentVisible() {
		return $this->m_intIsResidentVisible;
	}

	public function sqlIsResidentVisible() {
		return ( true == isset( $this->m_intIsResidentVisible ) ) ? ( string ) $this->m_intIsResidentVisible : '0';
	}

	public function setIsStaffInspection( $intIsStaffInspection ) {
		$this->set( 'm_intIsStaffInspection', CStrings::strToIntDef( $intIsStaffInspection, NULL, false ) );
	}

	public function getIsStaffInspection() {
		return $this->m_intIsStaffInspection;
	}

	public function sqlIsStaffInspection() {
		return ( true == isset( $this->m_intIsStaffInspection ) ) ? ( string ) $this->m_intIsStaffInspection : '1';
	}

	public function setSignatureDatetime( $strSignatureDatetime ) {
		$this->set( 'm_strSignatureDatetime', CStrings::strTrimDef( $strSignatureDatetime, -1, NULL, true ) );
	}

	public function getSignatureDatetime() {
		return $this->m_strSignatureDatetime;
	}

	public function sqlSignatureDatetime() {
		return ( true == isset( $this->m_strSignatureDatetime ) ) ? '\'' . $this->m_strSignatureDatetime . '\'' : 'NULL';
	}

	public function setInspectedBy( $intInspectedBy ) {
		$this->set( 'm_intInspectedBy', CStrings::strToIntDef( $intInspectedBy, NULL, false ) );
	}

	public function getInspectedBy() {
		return $this->m_intInspectedBy;
	}

	public function sqlInspectedBy() {
		return ( true == isset( $this->m_intInspectedBy ) ) ? ( string ) $this->m_intInspectedBy : 'NULL';
	}

	public function setInspectedOn( $strInspectedOn ) {
		$this->set( 'm_strInspectedOn', CStrings::strTrimDef( $strInspectedOn, -1, NULL, true ) );
	}

	public function getInspectedOn() {
		return $this->m_strInspectedOn;
	}

	public function sqlInspectedOn() {
		return ( true == isset( $this->m_strInspectedOn ) ) ? '\'' . $this->m_strInspectedOn . '\'' : 'NULL';
	}

	public function setReviewedBy( $intReviewedBy ) {
		$this->set( 'm_intReviewedBy', CStrings::strToIntDef( $intReviewedBy, NULL, false ) );
	}

	public function getReviewedBy() {
		return $this->m_intReviewedBy;
	}

	public function sqlReviewedBy() {
		return ( true == isset( $this->m_intReviewedBy ) ) ? ( string ) $this->m_intReviewedBy : 'NULL';
	}

	public function setReviewedOn( $strReviewedOn ) {
		$this->set( 'm_strReviewedOn', CStrings::strTrimDef( $strReviewedOn, -1, NULL, true ) );
	}

	public function getReviewedOn() {
		return $this->m_strReviewedOn;
	}

	public function sqlReviewedOn() {
		return ( true == isset( $this->m_strReviewedOn ) ) ? '\'' . $this->m_strReviewedOn . '\'' : 'NULL';
	}

	public function setInspectionTypeId( $intInspectionTypeId ) {
		$this->set( 'm_intInspectionTypeId', CStrings::strToIntDef( $intInspectionTypeId, NULL, false ) );
	}

	public function getInspectionTypeId() {
		return $this->m_intInspectionTypeId;
	}

	public function sqlInspectionTypeId() {
		return ( true == isset( $this->m_intInspectionTypeId ) ) ? ( string ) $this->m_intInspectionTypeId : '1';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, inspection_form_id, inspection_details, property_id, property_unit_id, unit_space_id, customer_id, lease_id, maintenance_request_id, signature_file_id, inspection_file_id, ps_product_id, remote_primary_key, scheduled_task_id, maintenance_group_id, scheduled_date, inspection_due_date, notes, is_resident_visible, is_staff_inspection, signature_datetime, inspected_by, inspected_on, reviewed_by, reviewed_on, inspection_type_id, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlInspectionFormId() . ', ' .
						$this->sqlInspectionDetails() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyUnitId() . ', ' .
						$this->sqlUnitSpaceId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlMaintenanceRequestId() . ', ' .
						$this->sqlSignatureFileId() . ', ' .
						$this->sqlInspectionFileId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlScheduledTaskId() . ', ' .
						$this->sqlMaintenanceGroupId() . ', ' .
						$this->sqlScheduledDate() . ', ' .
						$this->sqlInspectionDueDate() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlIsResidentVisible() . ', ' .
						$this->sqlIsStaffInspection() . ', ' .
						$this->sqlSignatureDatetime() . ', ' .
						$this->sqlInspectedBy() . ', ' .
						$this->sqlInspectedOn() . ', ' .
						$this->sqlReviewedBy() . ', ' .
						$this->sqlReviewedOn() . ', ' .
						$this->sqlInspectionTypeId() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inspection_form_id = ' . $this->sqlInspectionFormId(). ',' ; } elseif( true == array_key_exists( 'InspectionFormId', $this->getChangedColumns() ) ) { $strSql .= ' inspection_form_id = ' . $this->sqlInspectionFormId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inspection_details = ' . $this->sqlInspectionDetails(). ',' ; } elseif( true == array_key_exists( 'InspectionDetails', $this->getChangedColumns() ) ) { $strSql .= ' inspection_details = ' . $this->sqlInspectionDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceRequestId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signature_file_id = ' . $this->sqlSignatureFileId(). ',' ; } elseif( true == array_key_exists( 'SignatureFileId', $this->getChangedColumns() ) ) { $strSql .= ' signature_file_id = ' . $this->sqlSignatureFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inspection_file_id = ' . $this->sqlInspectionFileId(). ',' ; } elseif( true == array_key_exists( 'InspectionFileId', $this->getChangedColumns() ) ) { $strSql .= ' inspection_file_id = ' . $this->sqlInspectionFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_task_id = ' . $this->sqlScheduledTaskId(). ',' ; } elseif( true == array_key_exists( 'ScheduledTaskId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_task_id = ' . $this->sqlScheduledTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_group_id = ' . $this->sqlMaintenanceGroupId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceGroupId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_group_id = ' . $this->sqlMaintenanceGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_date = ' . $this->sqlScheduledDate(). ',' ; } elseif( true == array_key_exists( 'ScheduledDate', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_date = ' . $this->sqlScheduledDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inspection_due_date = ' . $this->sqlInspectionDueDate(). ',' ; } elseif( true == array_key_exists( 'InspectionDueDate', $this->getChangedColumns() ) ) { $strSql .= ' inspection_due_date = ' . $this->sqlInspectionDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_resident_visible = ' . $this->sqlIsResidentVisible(). ',' ; } elseif( true == array_key_exists( 'IsResidentVisible', $this->getChangedColumns() ) ) { $strSql .= ' is_resident_visible = ' . $this->sqlIsResidentVisible() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_staff_inspection = ' . $this->sqlIsStaffInspection(). ',' ; } elseif( true == array_key_exists( 'IsStaffInspection', $this->getChangedColumns() ) ) { $strSql .= ' is_staff_inspection = ' . $this->sqlIsStaffInspection() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signature_datetime = ' . $this->sqlSignatureDatetime(). ',' ; } elseif( true == array_key_exists( 'SignatureDatetime', $this->getChangedColumns() ) ) { $strSql .= ' signature_datetime = ' . $this->sqlSignatureDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inspected_by = ' . $this->sqlInspectedBy(). ',' ; } elseif( true == array_key_exists( 'InspectedBy', $this->getChangedColumns() ) ) { $strSql .= ' inspected_by = ' . $this->sqlInspectedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inspected_on = ' . $this->sqlInspectedOn(). ',' ; } elseif( true == array_key_exists( 'InspectedOn', $this->getChangedColumns() ) ) { $strSql .= ' inspected_on = ' . $this->sqlInspectedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reviewed_by = ' . $this->sqlReviewedBy(). ',' ; } elseif( true == array_key_exists( 'ReviewedBy', $this->getChangedColumns() ) ) { $strSql .= ' reviewed_by = ' . $this->sqlReviewedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reviewed_on = ' . $this->sqlReviewedOn(). ',' ; } elseif( true == array_key_exists( 'ReviewedOn', $this->getChangedColumns() ) ) { $strSql .= ' reviewed_on = ' . $this->sqlReviewedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inspection_type_id = ' . $this->sqlInspectionTypeId(). ',' ; } elseif( true == array_key_exists( 'InspectionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' inspection_type_id = ' . $this->sqlInspectionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'inspection_form_id' => $this->getInspectionFormId(),
			'inspection_details' => $this->getInspectionDetails(),
			'property_id' => $this->getPropertyId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'customer_id' => $this->getCustomerId(),
			'lease_id' => $this->getLeaseId(),
			'maintenance_request_id' => $this->getMaintenanceRequestId(),
			'signature_file_id' => $this->getSignatureFileId(),
			'inspection_file_id' => $this->getInspectionFileId(),
			'ps_product_id' => $this->getPsProductId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'scheduled_task_id' => $this->getScheduledTaskId(),
			'maintenance_group_id' => $this->getMaintenanceGroupId(),
			'scheduled_date' => $this->getScheduledDate(),
			'inspection_due_date' => $this->getInspectionDueDate(),
			'notes' => $this->getNotes(),
			'is_resident_visible' => $this->getIsResidentVisible(),
			'is_staff_inspection' => $this->getIsStaffInspection(),
			'signature_datetime' => $this->getSignatureDatetime(),
			'inspected_by' => $this->getInspectedBy(),
			'inspected_on' => $this->getInspectedOn(),
			'reviewed_by' => $this->getReviewedBy(),
			'reviewed_on' => $this->getReviewedOn(),
			'inspection_type_id' => $this->getInspectionTypeId(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>