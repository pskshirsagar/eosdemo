<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceProblemApCodes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceProblemApCodes extends CEosPluralBase {

	/**
	 * @return CMaintenanceProblemApCode[]
	 */
	public static function fetchMaintenanceProblemApCodes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CMaintenanceProblemApCode', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenanceProblemApCode
	 */
	public static function fetchMaintenanceProblemApCode( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMaintenanceProblemApCode', $objDatabase );
	}

	public static function fetchMaintenanceProblemApCodeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_problem_ap_codes', $objDatabase );
	}

	public static function fetchMaintenanceProblemApCodeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceProblemApCode( sprintf( 'SELECT * FROM maintenance_problem_ap_codes WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceProblemApCodesByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenanceProblemApCodes( sprintf( 'SELECT * FROM maintenance_problem_ap_codes WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceProblemApCodesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceProblemApCodes( sprintf( 'SELECT * FROM maintenance_problem_ap_codes WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceProblemApCodesByMaintenanceProblemIdByCid( $intMaintenanceProblemId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceProblemApCodes( sprintf( 'SELECT * FROM maintenance_problem_ap_codes WHERE maintenance_problem_id = %d AND cid = %d', ( int ) $intMaintenanceProblemId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceProblemApCodesByApCodeIdByCid( $intApCodeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceProblemApCodes( sprintf( 'SELECT * FROM maintenance_problem_ap_codes WHERE ap_code_id = %d AND cid = %d', ( int ) $intApCodeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceProblemApCodesByUnitOfMeasureIdByCid( $intUnitOfMeasureId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceProblemApCodes( sprintf( 'SELECT * FROM maintenance_problem_ap_codes WHERE unit_of_measure_id = %d AND cid = %d', ( int ) $intUnitOfMeasureId, ( int ) $intCid ), $objDatabase );
	}

}
?>