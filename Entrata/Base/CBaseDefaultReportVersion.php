<?php

class CBaseDefaultReportVersion extends CEosSingularBase {

	const TABLE_NAME = 'public.default_report_versions';

	protected $m_intId;
	protected $m_intDefaultReportId;
	protected $m_intMajor;
	protected $m_intMinor;
	protected $m_intUpdatedBy;
	protected $m_intCreatedBy;
	protected $m_strTitleAddendum;
	protected $m_strDefinition;
	protected $m_strExpiration;
	protected $m_boolIsDefault;
	protected $m_strUpdatedOn;
	protected $m_strCreatedOn;
	protected $m_boolIsLatest;

	public function __construct() {
		parent::__construct();

		$this->m_intMajor = '1';
		$this->m_intMinor = '0';
		$this->m_boolIsLatest = true;
		$this->m_boolIsDefault = false;
		$this->m_intUpdatedBy = '1';
		$this->m_intCreatedBy = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['default_report_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultReportId', trim( $arrValues['default_report_id'] ) ); elseif( isset( $arrValues['default_report_id'] ) ) $this->setDefaultReportId( $arrValues['default_report_id'] );
		if( isset( $arrValues['major'] ) && $boolDirectSet ) $this->set( 'm_intMajor', trim( $arrValues['major'] ) ); elseif( isset( $arrValues['major'] ) ) $this->setMajor( $arrValues['major'] );
		if( isset( $arrValues['minor'] ) && $boolDirectSet ) $this->set( 'm_intMinor', trim( $arrValues['minor'] ) ); elseif( isset( $arrValues['minor'] ) ) $this->setMinor( $arrValues['minor'] );
		if( isset( $arrValues['title_addendum'] ) && $boolDirectSet ) $this->set( 'm_strTitleAddendum', trim( stripcslashes( $arrValues['title_addendum'] ) ) ); elseif( isset( $arrValues['title_addendum'] ) ) $this->setTitleAddendum( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title_addendum'] ) : $arrValues['title_addendum'] );
		if( isset( $arrValues['definition'] ) ) $this->set( 'm_strDefinition', trim( $arrValues['definition'] ) );
		if( isset( $arrValues['expiration'] ) && $boolDirectSet ) $this->set( 'm_strExpiration', trim( $arrValues['expiration'] ) ); elseif( isset( $arrValues['expiration'] ) ) $this->setExpiration( $arrValues['expiration'] );
		if( isset( $arrValues['is_latest'] ) && $boolDirectSet ) $this->set( 'm_boolIsLatest', trim( stripcslashes( $arrValues['is_latest'] ) ) ); elseif( isset( $arrValues['is_latest'] ) ) $this->setIsLatest( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_latest'] ) : $arrValues['is_latest'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_boolIsDefault', trim( stripcslashes( $arrValues['is_default'] ) ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_default'] ) : $arrValues['is_default'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDefaultReportId( $intDefaultReportId ) {
		$this->set( 'm_intDefaultReportId', CStrings::strToIntDef( $intDefaultReportId, NULL, false ) );
	}

	public function getDefaultReportId() {
		return $this->m_intDefaultReportId;
	}

	public function sqlDefaultReportId() {
		return ( true == isset( $this->m_intDefaultReportId ) ) ? ( string ) $this->m_intDefaultReportId : 'NULL';
	}

	public function setMajor( $intMajor ) {
		$this->set( 'm_intMajor', CStrings::strToIntDef( $intMajor, NULL, false ) );
	}

	public function getMajor() {
		return $this->m_intMajor;
	}

	public function sqlMajor() {
		return ( true == isset( $this->m_intMajor ) ) ? ( string ) $this->m_intMajor : '1';
	}

	public function setMinor( $intMinor ) {
		$this->set( 'm_intMinor', CStrings::strToIntDef( $intMinor, NULL, false ) );
	}

	public function getMinor() {
		return $this->m_intMinor;
	}

	public function sqlMinor() {
		return ( true == isset( $this->m_intMinor ) ) ? ( string ) $this->m_intMinor : '0';
	}

	public function setTitleAddendum( $strTitleAddendum ) {
		$this->set( 'm_strTitleAddendum', CStrings::strTrimDef( $strTitleAddendum, 240, NULL, true ) );
	}

	public function getTitleAddendum() {
		return $this->m_strTitleAddendum;
	}

	public function sqlTitleAddendum() {
		return ( true == isset( $this->m_strTitleAddendum ) ) ? '\'' . addslashes( $this->m_strTitleAddendum ) . '\'' : 'NULL';
	}

	public function setDefinition( $strDefinition ) {
		$this->m_strDefinition = CStrings::strTrimDef( $strDefinition, -1, NULL, true );
	}

	public function getDefinition() {
		return $this->m_strDefinition;
	}

	public function sqlDefinition() {
		return ( true == isset( $this->m_strDefinition ) ) ? '\'' . addslashes( $this->m_strDefinition ) . '\'' : 'NULL';
	}

	public function setExpiration( $strExpiration ) {
		$this->set( 'm_strExpiration', CStrings::strTrimDef( $strExpiration, -1, NULL, true ) );
	}

	public function getExpiration() {
		return $this->m_strExpiration;
	}

	public function sqlExpiration() {
		return ( true == isset( $this->m_strExpiration ) ) ? '\'' . $this->m_strExpiration . '\'' : 'NULL';
	}

	public function setIsLatest( $boolIsLatest ) {
		$this->set( 'm_boolIsLatest', CStrings::strToBool( $boolIsLatest ) );
	}

	public function getIsLatest() {
		return $this->m_boolIsLatest;
	}

	public function sqlIsLatest() {
		return ( true == isset( $this->m_boolIsLatest ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLatest ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDefault( $boolIsDefault ) {
		$this->set( 'm_boolIsDefault', CStrings::strToBool( $boolIsDefault ) );
	}

	public function getIsDefault() {
		return $this->m_boolIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_boolIsDefault ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDefault ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : '1';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $objDatabase, $boolReturnSqlOnly = false ) {


		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
					' . static::TABLE_NAME . '
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlDefaultReportId() . ', ' .
						$this->sqlMajor() . ', ' .
						$this->sqlMinor() . ', ' .
						$this->sqlTitleAddendum() . ', ' .
						$this->sqlDefinition() . ', ' .
						$this->sqlExpiration() . ', ' .
						$this->sqlIsLatest() . ', ' .
						$this->sqlIsDefault() . ', ' .
						$this->sqlUpdatedBy() . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$this->sqlCreatedBy() . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $objDatabase, $boolReturnSqlOnly = false ) {

		$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = array();
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_report_id = ' . $this->sqlDefaultReportId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDefaultReportId() ) != $this->getOriginalValueByFieldName ( 'default_report_id' ) ) { $arrstrOriginalValueChanges['default_report_id'] = $this->sqlDefaultReportId(); $strSql .= ' default_report_id = ' . $this->sqlDefaultReportId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' major = ' . $this->sqlMajor() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlMajor() ) != $this->getOriginalValueByFieldName ( 'major' ) ) { $arrstrOriginalValueChanges['major'] = $this->sqlMajor(); $strSql .= ' major = ' . $this->sqlMajor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minor = ' . $this->sqlMinor() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlMinor() ) != $this->getOriginalValueByFieldName ( 'minor' ) ) { $arrstrOriginalValueChanges['minor'] = $this->sqlMinor(); $strSql .= ' minor = ' . $this->sqlMinor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title_addendum = ' . $this->sqlTitleAddendum() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlTitleAddendum() ) != $this->getOriginalValueByFieldName ( 'title_addendum' ) ) { $arrstrOriginalValueChanges['title_addendum'] = $this->sqlTitleAddendum(); $strSql .= ' title_addendum = ' . $this->sqlTitleAddendum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' definition = E' . $this->sqlDefinition() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDefinition() ) != $this->getOriginalValueByFieldName ( 'definition' ) ) { $arrstrOriginalValueChanges['definition'] = $this->sqlDefinition(); $strSql .= ' definition = E' . $this->sqlDefinition() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expiration = ' . $this->sqlExpiration() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlExpiration() ) != $this->getOriginalValueByFieldName ( 'expiration' ) ) { $arrstrOriginalValueChanges['expiration'] = $this->sqlExpiration(); $strSql .= ' expiration = ' . $this->sqlExpiration() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_latest = ' . $this->sqlIsLatest() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsLatest() ) != $this->getOriginalValueByFieldName ( 'is_latest' ) ) { $arrstrOriginalValueChanges['is_latest'] = $this->sqlIsLatest(); $strSql .= ' is_latest = ' . $this->sqlIsLatest() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; } elseif( true == array_key_exists( 'IsDefault', $this->getChangedColumns() ) ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' updated_by = ' . $this->sqlUpdatedBy() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlUpdatedBy() ) != $this->getOriginalValueByFieldName ( 'updated_by' ) ) { $arrstrOriginalValueChanges['updated_by'] = $this->sqlUpdatedBy(); $strSql .= ' updated_by = ' . $this->sqlUpdatedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' updated_on = ' . $this->sqlUpdatedOn() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlUpdatedOn() ) != $this->getOriginalValueByFieldName ( 'updated_on' ) ) { $arrstrOriginalValueChanges['updated_on'] = $this->sqlUpdatedOn(); $strSql .= ' updated_on = ' . $this->sqlUpdatedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function copyReportDocumentationToNewVersion( $intMaxReportDatasetId, $intMaxReportDatasetColumnId, $intMaxReporFilterDescriptionId, $intDefaultReportVersionId, $intMajor, $intMinor, $objClientDatabase, $boolReturnSqlOnly = false ) {
		$objDataset = $objClientDatabase->createDataset();
		$strSql = 'SELECT
						*
						FROM
						public.func_copy_report_documentation (
		 					' . $intMaxReportDatasetId . ', ' . $intMaxReportDatasetColumnId . ', ' . $intMaxReporFilterDescriptionId . ', ' . $intDefaultReportVersionId . ', ' .$intMajor . ', ' . $intMinor . ' );';

		if( $boolReturnSqlOnly ) return $strSql;

		if( false == $objDataset->execute( $strSql ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert to add modules. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objClientDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}
		$objDataset->cleanup();

		return true;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'default_report_id' => $this->getDefaultReportId(),
			'major' => $this->getMajor(),
			'minor' => $this->getMinor(),
			'title_addendum' => $this->getTitleAddendum(),
			'definition' => $this->getDefinition(),
			'expiration' => $this->getExpiration(),
			'is_latest' => $this->getIsLatest(),
			'is_default' => $this->getIsDefault(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>