<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlHeaders
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlHeaders extends CEosPluralBase {

	/**
	 * @return CGlHeader[]
	 */
	public static function fetchGlHeaders( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CGlHeader::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CGlHeader
	 */
	public static function fetchGlHeader( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGlHeader::class, $objDatabase );
	}

	public static function fetchGlHeaderCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_headers', $objDatabase );
	}

	public static function fetchGlHeaderByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchGlHeader( sprintf( 'SELECT * FROM gl_headers WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeadersByCid( $intCid, $objDatabase ) {
		return self::fetchGlHeaders( sprintf( 'SELECT * FROM gl_headers WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeadersByGlHeaderTypeIdByCid( $intGlHeaderTypeId, $intCid, $objDatabase ) {
		return self::fetchGlHeaders( sprintf( 'SELECT * FROM gl_headers WHERE gl_header_type_id = %d AND cid = %d', ( int ) $intGlHeaderTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeadersByGlTransactionTypeIdByCid( $intGlTransactionTypeId, $intCid, $objDatabase ) {
		return self::fetchGlHeaders( sprintf( 'SELECT * FROM gl_headers WHERE gl_transaction_type_id = %d AND cid = %d', ( int ) $intGlTransactionTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeadersByGlHeaderStatusTypeIdByCid( $intGlHeaderStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchGlHeaders( sprintf( 'SELECT * FROM gl_headers WHERE gl_header_status_type_id = %d AND cid = %d', ( int ) $intGlHeaderStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeadersByGlBookIdByCid( $intGlBookId, $intCid, $objDatabase ) {
		return self::fetchGlHeaders( sprintf( 'SELECT * FROM gl_headers WHERE gl_book_id = %d AND cid = %d', ( int ) $intGlBookId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeadersByReferenceIdByCid( $intReferenceId, $intCid, $objDatabase ) {
		return self::fetchGlHeaders( sprintf( 'SELECT * FROM gl_headers WHERE reference_id = %d AND cid = %d', ( int ) $intReferenceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeadersByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchGlHeaders( sprintf( 'SELECT * FROM gl_headers WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeadersByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchGlHeaders( sprintf( 'SELECT * FROM gl_headers WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeadersByOffsettingGlHeaderIdByCid( $intOffsettingGlHeaderId, $intCid, $objDatabase ) {
		return self::fetchGlHeaders( sprintf( 'SELECT * FROM gl_headers WHERE offsetting_gl_header_id = %d AND cid = %d', ( int ) $intOffsettingGlHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeadersByTemplateGlHeaderIdByCid( $intTemplateGlHeaderId, $intCid, $objDatabase ) {
		return self::fetchGlHeaders( sprintf( 'SELECT * FROM gl_headers WHERE template_gl_header_id = %d AND cid = %d', ( int ) $intTemplateGlHeaderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeadersByClosePeriodIdByCid( $intClosePeriodId, $intCid, $objDatabase ) {
		return self::fetchGlHeaders( sprintf( 'SELECT * FROM gl_headers WHERE close_period_id = %d AND cid = %d', ( int ) $intClosePeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeadersByBulkPropertyIdByCid( $intBulkPropertyId, $intCid, $objDatabase ) {
		return self::fetchGlHeaders( sprintf( 'SELECT * FROM gl_headers WHERE bulk_property_id = %d AND cid = %d', ( int ) $intBulkPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeadersByBulkPropertyUnitIdByCid( $intBulkPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchGlHeaders( sprintf( 'SELECT * FROM gl_headers WHERE bulk_property_unit_id = %d AND cid = %d', ( int ) $intBulkPropertyUnitId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeadersByBulkCompanyDepartmentIdByCid( $intBulkCompanyDepartmentId, $intCid, $objDatabase ) {
		return self::fetchGlHeaders( sprintf( 'SELECT * FROM gl_headers WHERE bulk_company_department_id = %d AND cid = %d', ( int ) $intBulkCompanyDepartmentId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeadersByBulkGlDimensionIdByCid( $intBulkGlDimensionId, $intCid, $objDatabase ) {
		return self::fetchGlHeaders( sprintf( 'SELECT * FROM gl_headers WHERE bulk_gl_dimension_id = %d AND cid = %d', ( int ) $intBulkGlDimensionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeadersByBulkJobPhaseIdByCid( $intBulkJobPhaseId, $intCid, $objDatabase ) {
		return self::fetchGlHeaders( sprintf( 'SELECT * FROM gl_headers WHERE bulk_job_phase_id = %d AND cid = %d', ( int ) $intBulkJobPhaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeadersByBulkApContractIdByCid( $intBulkApContractId, $intCid, $objDatabase ) {
		return self::fetchGlHeaders( sprintf( 'SELECT * FROM gl_headers WHERE bulk_ap_contract_id = %d AND cid = %d', ( int ) $intBulkApContractId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeadersByBulkPropertyBuildingIdByCid( $intBulkPropertyBuildingId, $intCid, $objDatabase ) {
		return self::fetchGlHeaders( sprintf( 'SELECT * FROM gl_headers WHERE bulk_property_building_id = %d AND cid = %d', ( int ) $intBulkPropertyBuildingId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeadersByGlHeaderScheduleIdByCid( $intGlHeaderScheduleId, $intCid, $objDatabase ) {
		return self::fetchGlHeaders( sprintf( 'SELECT * FROM gl_headers WHERE gl_header_schedule_id = %d AND cid = %d', ( int ) $intGlHeaderScheduleId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeadersByApRoutingTagIdByCid( $intApRoutingTagId, $intCid, $objDatabase ) {
		return self::fetchGlHeaders( sprintf( 'SELECT * FROM gl_headers WHERE ap_routing_tag_id = %d AND cid = %d', ( int ) $intApRoutingTagId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGlHeadersByReclassGlHeaderIdByCid( $intReclassGlHeaderId, $intCid, $objDatabase ) {
		return self::fetchGlHeaders( sprintf( 'SELECT * FROM gl_headers WHERE reclass_gl_header_id = %d AND cid = %d', ( int ) $intReclassGlHeaderId, ( int ) $intCid ), $objDatabase );
	}

}
?>