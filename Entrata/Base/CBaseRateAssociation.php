<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRateAssociation extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.rate_associations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyFloorplanId;
	protected $m_intUnitTypeId;
	protected $m_intUnitSpaceId;
	protected $m_intArCascadeId;
	protected $m_intArCascadeReferenceId;
	protected $m_intArOriginId;
	protected $m_intArOriginReferenceId;
	protected $m_intCompanyMediaFileId;
	protected $m_strRemotePrimaryKey;
	protected $m_strExternalVideoUrl;
	protected $m_strDescription;
	protected $m_strNotes;
	protected $m_strPetRestrictions;
	protected $m_intPetWeightLimit;
	protected $m_intPetMaxAllowed;
	protected $m_boolPetCare;
	protected $m_boolHideDescription;
	protected $m_boolIsPetToAllUnits;
	protected $m_boolIsDefault;
	protected $m_boolIsItemized;
	protected $m_boolIsFeatured;
	protected $m_boolIsOptional;
	protected $m_boolIsPublished;
	protected $m_boolHideRates;
	protected $m_strExpiresOn;
	protected $m_intOrderNum;
	protected $m_strImportedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intMappingId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intMarketingMediaAssociationId;

	public function __construct() {
		parent::__construct();

		$this->m_intPetWeightLimit = '0';
		$this->m_boolPetCare = false;
		$this->m_boolHideDescription = false;
		$this->m_boolIsPetToAllUnits = false;
		$this->m_boolIsDefault = false;
		$this->m_boolIsItemized = false;
		$this->m_boolIsFeatured = false;
		$this->m_boolIsOptional = true;
		$this->m_boolIsPublished = true;
		$this->m_boolHideRates = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorplanId', trim( $arrValues['property_floorplan_id'] ) ); elseif( isset( $arrValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrValues['property_floorplan_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['ar_cascade_id'] ) && $boolDirectSet ) $this->set( 'm_intArCascadeId', trim( $arrValues['ar_cascade_id'] ) ); elseif( isset( $arrValues['ar_cascade_id'] ) ) $this->setArCascadeId( $arrValues['ar_cascade_id'] );
		if( isset( $arrValues['ar_cascade_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intArCascadeReferenceId', trim( $arrValues['ar_cascade_reference_id'] ) ); elseif( isset( $arrValues['ar_cascade_reference_id'] ) ) $this->setArCascadeReferenceId( $arrValues['ar_cascade_reference_id'] );
		if( isset( $arrValues['ar_origin_id'] ) && $boolDirectSet ) $this->set( 'm_intArOriginId', trim( $arrValues['ar_origin_id'] ) ); elseif( isset( $arrValues['ar_origin_id'] ) ) $this->setArOriginId( $arrValues['ar_origin_id'] );
		if( isset( $arrValues['ar_origin_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intArOriginReferenceId', trim( $arrValues['ar_origin_reference_id'] ) ); elseif( isset( $arrValues['ar_origin_reference_id'] ) ) $this->setArOriginReferenceId( $arrValues['ar_origin_reference_id'] );
		if( isset( $arrValues['company_media_file_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMediaFileId', trim( $arrValues['company_media_file_id'] ) ); elseif( isset( $arrValues['company_media_file_id'] ) ) $this->setCompanyMediaFileId( $arrValues['company_media_file_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['external_video_url'] ) && $boolDirectSet ) $this->set( 'm_strExternalVideoUrl', trim( $arrValues['external_video_url'] ) ); elseif( isset( $arrValues['external_video_url'] ) ) $this->setExternalVideoUrl( $arrValues['external_video_url'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['pet_restrictions'] ) && $boolDirectSet ) $this->set( 'm_strPetRestrictions', trim( $arrValues['pet_restrictions'] ) ); elseif( isset( $arrValues['pet_restrictions'] ) ) $this->setPetRestrictions( $arrValues['pet_restrictions'] );
		if( isset( $arrValues['pet_weight_limit'] ) && $boolDirectSet ) $this->set( 'm_intPetWeightLimit', trim( $arrValues['pet_weight_limit'] ) ); elseif( isset( $arrValues['pet_weight_limit'] ) ) $this->setPetWeightLimit( $arrValues['pet_weight_limit'] );
		if( isset( $arrValues['pet_max_allowed'] ) && $boolDirectSet ) $this->set( 'm_intPetMaxAllowed', trim( $arrValues['pet_max_allowed'] ) ); elseif( isset( $arrValues['pet_max_allowed'] ) ) $this->setPetMaxAllowed( $arrValues['pet_max_allowed'] );
		if( isset( $arrValues['pet_care'] ) && $boolDirectSet ) $this->set( 'm_boolPetCare', trim( stripcslashes( $arrValues['pet_care'] ) ) ); elseif( isset( $arrValues['pet_care'] ) ) $this->setPetCare( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['pet_care'] ) : $arrValues['pet_care'] );
		if( isset( $arrValues['hide_description'] ) && $boolDirectSet ) $this->set( 'm_boolHideDescription', trim( stripcslashes( $arrValues['hide_description'] ) ) ); elseif( isset( $arrValues['hide_description'] ) ) $this->setHideDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hide_description'] ) : $arrValues['hide_description'] );
		if( isset( $arrValues['is_pet_to_all_units'] ) && $boolDirectSet ) $this->set( 'm_boolIsPetToAllUnits', trim( stripcslashes( $arrValues['is_pet_to_all_units'] ) ) ); elseif( isset( $arrValues['is_pet_to_all_units'] ) ) $this->setIsPetToAllUnits( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_pet_to_all_units'] ) : $arrValues['is_pet_to_all_units'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_boolIsDefault', trim( stripcslashes( $arrValues['is_default'] ) ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_default'] ) : $arrValues['is_default'] );
		if( isset( $arrValues['is_itemized'] ) && $boolDirectSet ) $this->set( 'm_boolIsItemized', trim( stripcslashes( $arrValues['is_itemized'] ) ) ); elseif( isset( $arrValues['is_itemized'] ) ) $this->setIsItemized( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_itemized'] ) : $arrValues['is_itemized'] );
		if( isset( $arrValues['is_featured'] ) && $boolDirectSet ) $this->set( 'm_boolIsFeatured', trim( stripcslashes( $arrValues['is_featured'] ) ) ); elseif( isset( $arrValues['is_featured'] ) ) $this->setIsFeatured( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_featured'] ) : $arrValues['is_featured'] );
		if( isset( $arrValues['is_optional'] ) && $boolDirectSet ) $this->set( 'm_boolIsOptional', trim( stripcslashes( $arrValues['is_optional'] ) ) ); elseif( isset( $arrValues['is_optional'] ) ) $this->setIsOptional( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_optional'] ) : $arrValues['is_optional'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['hide_rates'] ) && $boolDirectSet ) $this->set( 'm_boolHideRates', trim( stripcslashes( $arrValues['hide_rates'] ) ) ); elseif( isset( $arrValues['hide_rates'] ) ) $this->setHideRates( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hide_rates'] ) : $arrValues['hide_rates'] );
		if( isset( $arrValues['expires_on'] ) && $boolDirectSet ) $this->set( 'm_strExpiresOn', trim( $arrValues['expires_on'] ) ); elseif( isset( $arrValues['expires_on'] ) ) $this->setExpiresOn( $arrValues['expires_on'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['imported_on'] ) && $boolDirectSet ) $this->set( 'm_strImportedOn', trim( $arrValues['imported_on'] ) ); elseif( isset( $arrValues['imported_on'] ) ) $this->setImportedOn( $arrValues['imported_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['mapping_id'] ) && $boolDirectSet ) $this->set( 'm_intMappingId', trim( $arrValues['mapping_id'] ) ); elseif( isset( $arrValues['mapping_id'] ) ) $this->setMappingId( $arrValues['mapping_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['marketing_media_association_id'] ) && $boolDirectSet ) $this->set( 'm_intMarketingMediaAssociationId', trim( $arrValues['marketing_media_association_id'] ) ); elseif( isset( $arrValues['marketing_media_association_id'] ) ) $this->setMarketingMediaAssociationId( $arrValues['marketing_media_association_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->set( 'm_intPropertyFloorplanId', CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false ) );
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function sqlPropertyFloorplanId() {
		return ( true == isset( $this->m_intPropertyFloorplanId ) ) ? ( string ) $this->m_intPropertyFloorplanId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setArCascadeId( $intArCascadeId ) {
		$this->set( 'm_intArCascadeId', CStrings::strToIntDef( $intArCascadeId, NULL, false ) );
	}

	public function getArCascadeId() {
		return $this->m_intArCascadeId;
	}

	public function sqlArCascadeId() {
		return ( true == isset( $this->m_intArCascadeId ) ) ? ( string ) $this->m_intArCascadeId : 'NULL';
	}

	public function setArCascadeReferenceId( $intArCascadeReferenceId ) {
		$this->set( 'm_intArCascadeReferenceId', CStrings::strToIntDef( $intArCascadeReferenceId, NULL, false ) );
	}

	public function getArCascadeReferenceId() {
		return $this->m_intArCascadeReferenceId;
	}

	public function sqlArCascadeReferenceId() {
		return ( true == isset( $this->m_intArCascadeReferenceId ) ) ? ( string ) $this->m_intArCascadeReferenceId : 'NULL';
	}

	public function setArOriginId( $intArOriginId ) {
		$this->set( 'm_intArOriginId', CStrings::strToIntDef( $intArOriginId, NULL, false ) );
	}

	public function getArOriginId() {
		return $this->m_intArOriginId;
	}

	public function sqlArOriginId() {
		return ( true == isset( $this->m_intArOriginId ) ) ? ( string ) $this->m_intArOriginId : 'NULL';
	}

	public function setArOriginReferenceId( $intArOriginReferenceId ) {
		$this->set( 'm_intArOriginReferenceId', CStrings::strToIntDef( $intArOriginReferenceId, NULL, false ) );
	}

	public function getArOriginReferenceId() {
		return $this->m_intArOriginReferenceId;
	}

	public function sqlArOriginReferenceId() {
		return ( true == isset( $this->m_intArOriginReferenceId ) ) ? ( string ) $this->m_intArOriginReferenceId : 'NULL';
	}

	public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
		$this->set( 'm_intCompanyMediaFileId', CStrings::strToIntDef( $intCompanyMediaFileId, NULL, false ) );
	}

	public function getCompanyMediaFileId() {
		return $this->m_intCompanyMediaFileId;
	}

	public function sqlCompanyMediaFileId() {
		return ( true == isset( $this->m_intCompanyMediaFileId ) ) ? ( string ) $this->m_intCompanyMediaFileId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setExternalVideoUrl( $strExternalVideoUrl ) {
		$this->set( 'm_strExternalVideoUrl', CStrings::strTrimDef( $strExternalVideoUrl, 4096, NULL, true ) );
	}

	public function getExternalVideoUrl() {
		return $this->m_strExternalVideoUrl;
	}

	public function sqlExternalVideoUrl() {
		return ( true == isset( $this->m_strExternalVideoUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strExternalVideoUrl ) : '\'' . addslashes( $this->m_strExternalVideoUrl ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 240, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setPetRestrictions( $strPetRestrictions ) {
		$this->set( 'm_strPetRestrictions', CStrings::strTrimDef( $strPetRestrictions, -1, NULL, true ) );
	}

	public function getPetRestrictions() {
		return $this->m_strPetRestrictions;
	}

	public function sqlPetRestrictions() {
		return ( true == isset( $this->m_strPetRestrictions ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPetRestrictions ) : '\'' . addslashes( $this->m_strPetRestrictions ) . '\'' ) : 'NULL';
	}

	public function setPetWeightLimit( $intPetWeightLimit ) {
		$this->set( 'm_intPetWeightLimit', CStrings::strToIntDef( $intPetWeightLimit, NULL, false ) );
	}

	public function getPetWeightLimit() {
		return $this->m_intPetWeightLimit;
	}

	public function sqlPetWeightLimit() {
		return ( true == isset( $this->m_intPetWeightLimit ) ) ? ( string ) $this->m_intPetWeightLimit : '0';
	}

	public function setPetMaxAllowed( $intPetMaxAllowed ) {
		$this->set( 'm_intPetMaxAllowed', CStrings::strToIntDef( $intPetMaxAllowed, NULL, false ) );
	}

	public function getPetMaxAllowed() {
		return $this->m_intPetMaxAllowed;
	}

	public function sqlPetMaxAllowed() {
		return ( true == isset( $this->m_intPetMaxAllowed ) ) ? ( string ) $this->m_intPetMaxAllowed : 'NULL';
	}

	public function setPetCare( $boolPetCare ) {
		$this->set( 'm_boolPetCare', CStrings::strToBool( $boolPetCare ) );
	}

	public function getPetCare() {
		return $this->m_boolPetCare;
	}

	public function sqlPetCare() {
		return ( true == isset( $this->m_boolPetCare ) ) ? '\'' . ( true == ( bool ) $this->m_boolPetCare ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHideDescription( $boolHideDescription ) {
		$this->set( 'm_boolHideDescription', CStrings::strToBool( $boolHideDescription ) );
	}

	public function getHideDescription() {
		return $this->m_boolHideDescription;
	}

	public function sqlHideDescription() {
		return ( true == isset( $this->m_boolHideDescription ) ) ? '\'' . ( true == ( bool ) $this->m_boolHideDescription ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPetToAllUnits( $boolIsPetToAllUnits ) {
		$this->set( 'm_boolIsPetToAllUnits', CStrings::strToBool( $boolIsPetToAllUnits ) );
	}

	public function getIsPetToAllUnits() {
		return $this->m_boolIsPetToAllUnits;
	}

	public function sqlIsPetToAllUnits() {
		return ( true == isset( $this->m_boolIsPetToAllUnits ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPetToAllUnits ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDefault( $boolIsDefault ) {
		$this->set( 'm_boolIsDefault', CStrings::strToBool( $boolIsDefault ) );
	}

	public function getIsDefault() {
		return $this->m_boolIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_boolIsDefault ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDefault ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsItemized( $boolIsItemized ) {
		$this->set( 'm_boolIsItemized', CStrings::strToBool( $boolIsItemized ) );
	}

	public function getIsItemized() {
		return $this->m_boolIsItemized;
	}

	public function sqlIsItemized() {
		return ( true == isset( $this->m_boolIsItemized ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsItemized ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsFeatured( $boolIsFeatured ) {
		$this->set( 'm_boolIsFeatured', CStrings::strToBool( $boolIsFeatured ) );
	}

	public function getIsFeatured() {
		return $this->m_boolIsFeatured;
	}

	public function sqlIsFeatured() {
		return ( true == isset( $this->m_boolIsFeatured ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFeatured ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsOptional( $boolIsOptional ) {
		$this->set( 'm_boolIsOptional', CStrings::strToBool( $boolIsOptional ) );
	}

	public function getIsOptional() {
		return $this->m_boolIsOptional;
	}

	public function sqlIsOptional() {
		return ( true == isset( $this->m_boolIsOptional ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOptional ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHideRates( $boolHideRates ) {
		$this->set( 'm_boolHideRates', CStrings::strToBool( $boolHideRates ) );
	}

	public function getHideRates() {
		return $this->m_boolHideRates;
	}

	public function sqlHideRates() {
		return ( true == isset( $this->m_boolHideRates ) ) ? '\'' . ( true == ( bool ) $this->m_boolHideRates ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setExpiresOn( $strExpiresOn ) {
		$this->set( 'm_strExpiresOn', CStrings::strTrimDef( $strExpiresOn, -1, NULL, true ) );
	}

	public function getExpiresOn() {
		return $this->m_strExpiresOn;
	}

	public function sqlExpiresOn() {
		return ( true == isset( $this->m_strExpiresOn ) ) ? '\'' . $this->m_strExpiresOn . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setImportedOn( $strImportedOn ) {
		$this->set( 'm_strImportedOn', CStrings::strTrimDef( $strImportedOn, -1, NULL, true ) );
	}

	public function getImportedOn() {
		return $this->m_strImportedOn;
	}

	public function sqlImportedOn() {
		return ( true == isset( $this->m_strImportedOn ) ) ? '\'' . $this->m_strImportedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setMappingId( $intMappingId ) {
		$this->set( 'm_intMappingId', CStrings::strToIntDef( $intMappingId, NULL, false ) );
	}

	public function getMappingId() {
		return $this->m_intMappingId;
	}

	public function sqlMappingId() {
		return ( true == isset( $this->m_intMappingId ) ) ? ( string ) $this->m_intMappingId : 'NULL';
	}

	public function setMarketingMediaAssociationId( $intMarketingMediaAssociationId ) {
		$this->set( 'm_intMarketingMediaAssociationId', CStrings::strToIntDef( $intMarketingMediaAssociationId, NULL, false ) );
	}

	public function getMarketingMediaAssociationId() {
		return $this->m_intMarketingMediaAssociationId;
	}

	public function sqlMarketingMediaAssociationId() {
		return ( true == isset( $this->m_intMarketingMediaAssociationId ) ) ? ( string ) $this->m_intMarketingMediaAssociationId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_floorplan_id, unit_type_id, unit_space_id, ar_cascade_id, ar_cascade_reference_id, ar_origin_id, ar_origin_reference_id, company_media_file_id, remote_primary_key, external_video_url, description, notes, pet_restrictions, pet_weight_limit, pet_max_allowed, pet_care, hide_description, is_pet_to_all_units, is_default, is_itemized, is_featured, is_optional, is_published, hide_rates, expires_on, order_num, imported_on, updated_by, updated_on, created_by, created_on, mapping_id, details, marketing_media_association_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyFloorplanId() . ', ' .
						$this->sqlUnitTypeId() . ', ' .
						$this->sqlUnitSpaceId() . ', ' .
						$this->sqlArCascadeId() . ', ' .
						$this->sqlArCascadeReferenceId() . ', ' .
						$this->sqlArOriginId() . ', ' .
						$this->sqlArOriginReferenceId() . ', ' .
						$this->sqlCompanyMediaFileId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlExternalVideoUrl() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlPetRestrictions() . ', ' .
						$this->sqlPetWeightLimit() . ', ' .
						$this->sqlPetMaxAllowed() . ', ' .
						$this->sqlPetCare() . ', ' .
						$this->sqlHideDescription() . ', ' .
						$this->sqlIsPetToAllUnits() . ', ' .
						$this->sqlIsDefault() . ', ' .
						$this->sqlIsItemized() . ', ' .
						$this->sqlIsFeatured() . ', ' .
						$this->sqlIsOptional() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlHideRates() . ', ' .
						$this->sqlExpiresOn() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlImportedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlMappingId() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlMarketingMediaAssociationId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId(). ',' ; } elseif( true == array_key_exists( 'PropertyFloorplanId', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_cascade_id = ' . $this->sqlArCascadeId(). ',' ; } elseif( true == array_key_exists( 'ArCascadeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_cascade_id = ' . $this->sqlArCascadeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_cascade_reference_id = ' . $this->sqlArCascadeReferenceId(). ',' ; } elseif( true == array_key_exists( 'ArCascadeReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' ar_cascade_reference_id = ' . $this->sqlArCascadeReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_origin_id = ' . $this->sqlArOriginId(). ',' ; } elseif( true == array_key_exists( 'ArOriginId', $this->getChangedColumns() ) ) { $strSql .= ' ar_origin_id = ' . $this->sqlArOriginId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_origin_reference_id = ' . $this->sqlArOriginReferenceId(). ',' ; } elseif( true == array_key_exists( 'ArOriginReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' ar_origin_reference_id = ' . $this->sqlArOriginReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId(). ',' ; } elseif( true == array_key_exists( 'CompanyMediaFileId', $this->getChangedColumns() ) ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_video_url = ' . $this->sqlExternalVideoUrl(). ',' ; } elseif( true == array_key_exists( 'ExternalVideoUrl', $this->getChangedColumns() ) ) { $strSql .= ' external_video_url = ' . $this->sqlExternalVideoUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pet_restrictions = ' . $this->sqlPetRestrictions(). ',' ; } elseif( true == array_key_exists( 'PetRestrictions', $this->getChangedColumns() ) ) { $strSql .= ' pet_restrictions = ' . $this->sqlPetRestrictions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pet_weight_limit = ' . $this->sqlPetWeightLimit(). ',' ; } elseif( true == array_key_exists( 'PetWeightLimit', $this->getChangedColumns() ) ) { $strSql .= ' pet_weight_limit = ' . $this->sqlPetWeightLimit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pet_max_allowed = ' . $this->sqlPetMaxAllowed(). ',' ; } elseif( true == array_key_exists( 'PetMaxAllowed', $this->getChangedColumns() ) ) { $strSql .= ' pet_max_allowed = ' . $this->sqlPetMaxAllowed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pet_care = ' . $this->sqlPetCare(). ',' ; } elseif( true == array_key_exists( 'PetCare', $this->getChangedColumns() ) ) { $strSql .= ' pet_care = ' . $this->sqlPetCare() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_description = ' . $this->sqlHideDescription(). ',' ; } elseif( true == array_key_exists( 'HideDescription', $this->getChangedColumns() ) ) { $strSql .= ' hide_description = ' . $this->sqlHideDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_pet_to_all_units = ' . $this->sqlIsPetToAllUnits(). ',' ; } elseif( true == array_key_exists( 'IsPetToAllUnits', $this->getChangedColumns() ) ) { $strSql .= ' is_pet_to_all_units = ' . $this->sqlIsPetToAllUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault(). ',' ; } elseif( true == array_key_exists( 'IsDefault', $this->getChangedColumns() ) ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_itemized = ' . $this->sqlIsItemized(). ',' ; } elseif( true == array_key_exists( 'IsItemized', $this->getChangedColumns() ) ) { $strSql .= ' is_itemized = ' . $this->sqlIsItemized() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_featured = ' . $this->sqlIsFeatured(). ',' ; } elseif( true == array_key_exists( 'IsFeatured', $this->getChangedColumns() ) ) { $strSql .= ' is_featured = ' . $this->sqlIsFeatured() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_optional = ' . $this->sqlIsOptional(). ',' ; } elseif( true == array_key_exists( 'IsOptional', $this->getChangedColumns() ) ) { $strSql .= ' is_optional = ' . $this->sqlIsOptional() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hide_rates = ' . $this->sqlHideRates(). ',' ; } elseif( true == array_key_exists( 'HideRates', $this->getChangedColumns() ) ) { $strSql .= ' hide_rates = ' . $this->sqlHideRates() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expires_on = ' . $this->sqlExpiresOn(). ',' ; } elseif( true == array_key_exists( 'ExpiresOn', $this->getChangedColumns() ) ) { $strSql .= ' expires_on = ' . $this->sqlExpiresOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn(). ',' ; } elseif( true == array_key_exists( 'ImportedOn', $this->getChangedColumns() ) ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mapping_id = ' . $this->sqlMappingId(). ',' ; } elseif( true == array_key_exists( 'MappingId', $this->getChangedColumns() ) ) { $strSql .= ' mapping_id = ' . $this->sqlMappingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_media_association_id = ' . $this->sqlMarketingMediaAssociationId(). ',' ; } elseif( true == array_key_exists( 'MarketingMediaAssociationId', $this->getChangedColumns() ) ) { $strSql .= ' marketing_media_association_id = ' . $this->sqlMarketingMediaAssociationId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_floorplan_id' => $this->getPropertyFloorplanId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'ar_cascade_id' => $this->getArCascadeId(),
			'ar_cascade_reference_id' => $this->getArCascadeReferenceId(),
			'ar_origin_id' => $this->getArOriginId(),
			'ar_origin_reference_id' => $this->getArOriginReferenceId(),
			'company_media_file_id' => $this->getCompanyMediaFileId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'external_video_url' => $this->getExternalVideoUrl(),
			'description' => $this->getDescription(),
			'notes' => $this->getNotes(),
			'pet_restrictions' => $this->getPetRestrictions(),
			'pet_weight_limit' => $this->getPetWeightLimit(),
			'pet_max_allowed' => $this->getPetMaxAllowed(),
			'pet_care' => $this->getPetCare(),
			'hide_description' => $this->getHideDescription(),
			'is_pet_to_all_units' => $this->getIsPetToAllUnits(),
			'is_default' => $this->getIsDefault(),
			'is_itemized' => $this->getIsItemized(),
			'is_featured' => $this->getIsFeatured(),
			'is_optional' => $this->getIsOptional(),
			'is_published' => $this->getIsPublished(),
			'hide_rates' => $this->getHideRates(),
			'expires_on' => $this->getExpiresOn(),
			'order_num' => $this->getOrderNum(),
			'imported_on' => $this->getImportedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'mapping_id' => $this->getMappingId(),
			'details' => $this->getDetails(),
			'marketing_media_association_id' => $this->getMarketingMediaAssociationId()
		);
	}

}
?>