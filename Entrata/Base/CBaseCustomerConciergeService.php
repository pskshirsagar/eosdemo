<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerConciergeService extends CEosSingularBase {

	const TABLE_NAME = 'public.customer_concierge_services';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_strPropertyUnitNumber;
	protected $m_intCustomerId;
	protected $m_strCustomerNameFirstEncrypted;
	protected $m_strCustomerNameLastEncrypted;
	protected $m_strCustomerPhoneNumberEncrypted;
	protected $m_strCustomerEmailAddressEncrypted;
	protected $m_strConciergeSpecifics;
	protected $m_strDatetimeSubmitted;
	protected $m_strDatetimeCompleted;
	protected $m_intCompanyEmployeeId;
	protected $m_strCompanyEmployeeName;
	protected $m_intViewedBy;
	protected $m_strViewedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['property_unit_number'] ) && $boolDirectSet ) $this->set( 'm_strPropertyUnitNumber', trim( stripcslashes( $arrValues['property_unit_number'] ) ) ); elseif( isset( $arrValues['property_unit_number'] ) ) $this->setPropertyUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_unit_number'] ) : $arrValues['property_unit_number'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['customer_name_first_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCustomerNameFirstEncrypted', trim( stripcslashes( $arrValues['customer_name_first_encrypted'] ) ) ); elseif( isset( $arrValues['customer_name_first_encrypted'] ) ) $this->setCustomerNameFirstEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_name_first_encrypted'] ) : $arrValues['customer_name_first_encrypted'] );
		if( isset( $arrValues['customer_name_last_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCustomerNameLastEncrypted', trim( stripcslashes( $arrValues['customer_name_last_encrypted'] ) ) ); elseif( isset( $arrValues['customer_name_last_encrypted'] ) ) $this->setCustomerNameLastEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_name_last_encrypted'] ) : $arrValues['customer_name_last_encrypted'] );
		if( isset( $arrValues['customer_phone_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCustomerPhoneNumberEncrypted', trim( stripcslashes( $arrValues['customer_phone_number_encrypted'] ) ) ); elseif( isset( $arrValues['customer_phone_number_encrypted'] ) ) $this->setCustomerPhoneNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_phone_number_encrypted'] ) : $arrValues['customer_phone_number_encrypted'] );
		if( isset( $arrValues['customer_email_address_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCustomerEmailAddressEncrypted', trim( stripcslashes( $arrValues['customer_email_address_encrypted'] ) ) ); elseif( isset( $arrValues['customer_email_address_encrypted'] ) ) $this->setCustomerEmailAddressEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['customer_email_address_encrypted'] ) : $arrValues['customer_email_address_encrypted'] );
		if( isset( $arrValues['concierge_specifics'] ) && $boolDirectSet ) $this->set( 'm_strConciergeSpecifics', trim( stripcslashes( $arrValues['concierge_specifics'] ) ) ); elseif( isset( $arrValues['concierge_specifics'] ) ) $this->setConciergeSpecifics( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['concierge_specifics'] ) : $arrValues['concierge_specifics'] );
		if( isset( $arrValues['datetime_submitted'] ) && $boolDirectSet ) $this->set( 'm_strDatetimeSubmitted', trim( $arrValues['datetime_submitted'] ) ); elseif( isset( $arrValues['datetime_submitted'] ) ) $this->setDatetimeSubmitted( $arrValues['datetime_submitted'] );
		if( isset( $arrValues['datetime_completed'] ) && $boolDirectSet ) $this->set( 'm_strDatetimeCompleted', trim( $arrValues['datetime_completed'] ) ); elseif( isset( $arrValues['datetime_completed'] ) ) $this->setDatetimeCompleted( $arrValues['datetime_completed'] );
		if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
		if( isset( $arrValues['company_employee_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyEmployeeName', trim( stripcslashes( $arrValues['company_employee_name'] ) ) ); elseif( isset( $arrValues['company_employee_name'] ) ) $this->setCompanyEmployeeName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_employee_name'] ) : $arrValues['company_employee_name'] );
		if( isset( $arrValues['viewed_by'] ) && $boolDirectSet ) $this->set( 'm_intViewedBy', trim( $arrValues['viewed_by'] ) ); elseif( isset( $arrValues['viewed_by'] ) ) $this->setViewedBy( $arrValues['viewed_by'] );
		if( isset( $arrValues['viewed_on'] ) && $boolDirectSet ) $this->set( 'm_strViewedOn', trim( $arrValues['viewed_on'] ) ); elseif( isset( $arrValues['viewed_on'] ) ) $this->setViewedOn( $arrValues['viewed_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setPropertyUnitNumber( $strPropertyUnitNumber ) {
		$this->set( 'm_strPropertyUnitNumber', CStrings::strTrimDef( $strPropertyUnitNumber, 50, NULL, true ) );
	}

	public function getPropertyUnitNumber() {
		return $this->m_strPropertyUnitNumber;
	}

	public function sqlPropertyUnitNumber() {
		return ( true == isset( $this->m_strPropertyUnitNumber ) ) ? '\'' . addslashes( $this->m_strPropertyUnitNumber ) . '\'' : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setCustomerNameFirstEncrypted( $strCustomerNameFirstEncrypted ) {
		$this->set( 'm_strCustomerNameFirstEncrypted', CStrings::strTrimDef( $strCustomerNameFirstEncrypted, 240, NULL, true ) );
	}

	public function getCustomerNameFirstEncrypted() {
		return $this->m_strCustomerNameFirstEncrypted;
	}

	public function sqlCustomerNameFirstEncrypted() {
		return ( true == isset( $this->m_strCustomerNameFirstEncrypted ) ) ? '\'' . addslashes( $this->m_strCustomerNameFirstEncrypted ) . '\'' : 'NULL';
	}

	public function setCustomerNameLastEncrypted( $strCustomerNameLastEncrypted ) {
		$this->set( 'm_strCustomerNameLastEncrypted', CStrings::strTrimDef( $strCustomerNameLastEncrypted, 240, NULL, true ) );
	}

	public function getCustomerNameLastEncrypted() {
		return $this->m_strCustomerNameLastEncrypted;
	}

	public function sqlCustomerNameLastEncrypted() {
		return ( true == isset( $this->m_strCustomerNameLastEncrypted ) ) ? '\'' . addslashes( $this->m_strCustomerNameLastEncrypted ) . '\'' : 'NULL';
	}

	public function setCustomerPhoneNumberEncrypted( $strCustomerPhoneNumberEncrypted ) {
		$this->set( 'm_strCustomerPhoneNumberEncrypted', CStrings::strTrimDef( $strCustomerPhoneNumberEncrypted, 240, NULL, true ) );
	}

	public function getCustomerPhoneNumberEncrypted() {
		return $this->m_strCustomerPhoneNumberEncrypted;
	}

	public function sqlCustomerPhoneNumberEncrypted() {
		return ( true == isset( $this->m_strCustomerPhoneNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strCustomerPhoneNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setCustomerEmailAddressEncrypted( $strCustomerEmailAddressEncrypted ) {
		$this->set( 'm_strCustomerEmailAddressEncrypted', CStrings::strTrimDef( $strCustomerEmailAddressEncrypted, 240, NULL, true ) );
	}

	public function getCustomerEmailAddressEncrypted() {
		return $this->m_strCustomerEmailAddressEncrypted;
	}

	public function sqlCustomerEmailAddressEncrypted() {
		return ( true == isset( $this->m_strCustomerEmailAddressEncrypted ) ) ? '\'' . addslashes( $this->m_strCustomerEmailAddressEncrypted ) . '\'' : 'NULL';
	}

	public function setConciergeSpecifics( $strConciergeSpecifics ) {
		$this->set( 'm_strConciergeSpecifics', CStrings::strTrimDef( $strConciergeSpecifics, 2000, NULL, true ) );
	}

	public function getConciergeSpecifics() {
		return $this->m_strConciergeSpecifics;
	}

	public function sqlConciergeSpecifics() {
		return ( true == isset( $this->m_strConciergeSpecifics ) ) ? '\'' . addslashes( $this->m_strConciergeSpecifics ) . '\'' : 'NULL';
	}

	public function setDatetimeSubmitted( $strDatetimeSubmitted ) {
		$this->set( 'm_strDatetimeSubmitted', CStrings::strTrimDef( $strDatetimeSubmitted, -1, NULL, true ) );
	}

	public function getDatetimeSubmitted() {
		return $this->m_strDatetimeSubmitted;
	}

	public function sqlDatetimeSubmitted() {
		return ( true == isset( $this->m_strDatetimeSubmitted ) ) ? '\'' . $this->m_strDatetimeSubmitted . '\'' : 'NULL';
	}

	public function setDatetimeCompleted( $strDatetimeCompleted ) {
		$this->set( 'm_strDatetimeCompleted', CStrings::strTrimDef( $strDatetimeCompleted, -1, NULL, true ) );
	}

	public function getDatetimeCompleted() {
		return $this->m_strDatetimeCompleted;
	}

	public function sqlDatetimeCompleted() {
		return ( true == isset( $this->m_strDatetimeCompleted ) ) ? '\'' . $this->m_strDatetimeCompleted . '\'' : 'NULL';
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function sqlCompanyEmployeeId() {
		return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
	}

	public function setCompanyEmployeeName( $strCompanyEmployeeName ) {
		$this->set( 'm_strCompanyEmployeeName', CStrings::strTrimDef( $strCompanyEmployeeName, 100, NULL, true ) );
	}

	public function getCompanyEmployeeName() {
		return $this->m_strCompanyEmployeeName;
	}

	public function sqlCompanyEmployeeName() {
		return ( true == isset( $this->m_strCompanyEmployeeName ) ) ? '\'' . addslashes( $this->m_strCompanyEmployeeName ) . '\'' : 'NULL';
	}

	public function setViewedBy( $intViewedBy ) {
		$this->set( 'm_intViewedBy', CStrings::strToIntDef( $intViewedBy, NULL, false ) );
	}

	public function getViewedBy() {
		return $this->m_intViewedBy;
	}

	public function sqlViewedBy() {
		return ( true == isset( $this->m_intViewedBy ) ) ? ( string ) $this->m_intViewedBy : 'NULL';
	}

	public function setViewedOn( $strViewedOn ) {
		$this->set( 'm_strViewedOn', CStrings::strTrimDef( $strViewedOn, -1, NULL, true ) );
	}

	public function getViewedOn() {
		return $this->m_strViewedOn;
	}

	public function sqlViewedOn() {
		return ( true == isset( $this->m_strViewedOn ) ) ? '\'' . $this->m_strViewedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_unit_id, unit_space_id, property_unit_number, customer_id, customer_name_first_encrypted, customer_name_last_encrypted, customer_phone_number_encrypted, customer_email_address_encrypted, concierge_specifics, datetime_submitted, datetime_completed, company_employee_id, company_employee_name, viewed_by, viewed_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyUnitId() . ', ' .
 						$this->sqlUnitSpaceId() . ', ' .
 						$this->sqlPropertyUnitNumber() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlCustomerNameFirstEncrypted() . ', ' .
 						$this->sqlCustomerNameLastEncrypted() . ', ' .
 						$this->sqlCustomerPhoneNumberEncrypted() . ', ' .
 						$this->sqlCustomerEmailAddressEncrypted() . ', ' .
 						$this->sqlConciergeSpecifics() . ', ' .
 						$this->sqlDatetimeSubmitted() . ', ' .
 						$this->sqlDatetimeCompleted() . ', ' .
 						$this->sqlCompanyEmployeeId() . ', ' .
 						$this->sqlCompanyEmployeeName() . ', ' .
 						$this->sqlViewedBy() . ', ' .
 						$this->sqlViewedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_number = ' . $this->sqlPropertyUnitNumber() . ','; } elseif( true == array_key_exists( 'PropertyUnitNumber', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_number = ' . $this->sqlPropertyUnitNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_name_first_encrypted = ' . $this->sqlCustomerNameFirstEncrypted() . ','; } elseif( true == array_key_exists( 'CustomerNameFirstEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' customer_name_first_encrypted = ' . $this->sqlCustomerNameFirstEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_name_last_encrypted = ' . $this->sqlCustomerNameLastEncrypted() . ','; } elseif( true == array_key_exists( 'CustomerNameLastEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' customer_name_last_encrypted = ' . $this->sqlCustomerNameLastEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_phone_number_encrypted = ' . $this->sqlCustomerPhoneNumberEncrypted() . ','; } elseif( true == array_key_exists( 'CustomerPhoneNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' customer_phone_number_encrypted = ' . $this->sqlCustomerPhoneNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_email_address_encrypted = ' . $this->sqlCustomerEmailAddressEncrypted() . ','; } elseif( true == array_key_exists( 'CustomerEmailAddressEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' customer_email_address_encrypted = ' . $this->sqlCustomerEmailAddressEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' concierge_specifics = ' . $this->sqlConciergeSpecifics() . ','; } elseif( true == array_key_exists( 'ConciergeSpecifics', $this->getChangedColumns() ) ) { $strSql .= ' concierge_specifics = ' . $this->sqlConciergeSpecifics() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' datetime_submitted = ' . $this->sqlDatetimeSubmitted() . ','; } elseif( true == array_key_exists( 'DatetimeSubmitted', $this->getChangedColumns() ) ) { $strSql .= ' datetime_submitted = ' . $this->sqlDatetimeSubmitted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' datetime_completed = ' . $this->sqlDatetimeCompleted() . ','; } elseif( true == array_key_exists( 'DatetimeCompleted', $this->getChangedColumns() ) ) { $strSql .= ' datetime_completed = ' . $this->sqlDatetimeCompleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; } elseif( true == array_key_exists( 'CompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_name = ' . $this->sqlCompanyEmployeeName() . ','; } elseif( true == array_key_exists( 'CompanyEmployeeName', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_name = ' . $this->sqlCompanyEmployeeName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' viewed_by = ' . $this->sqlViewedBy() . ','; } elseif( true == array_key_exists( 'ViewedBy', $this->getChangedColumns() ) ) { $strSql .= ' viewed_by = ' . $this->sqlViewedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' viewed_on = ' . $this->sqlViewedOn() . ','; } elseif( true == array_key_exists( 'ViewedOn', $this->getChangedColumns() ) ) { $strSql .= ' viewed_on = ' . $this->sqlViewedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'property_unit_number' => $this->getPropertyUnitNumber(),
			'customer_id' => $this->getCustomerId(),
			'customer_name_first_encrypted' => $this->getCustomerNameFirstEncrypted(),
			'customer_name_last_encrypted' => $this->getCustomerNameLastEncrypted(),
			'customer_phone_number_encrypted' => $this->getCustomerPhoneNumberEncrypted(),
			'customer_email_address_encrypted' => $this->getCustomerEmailAddressEncrypted(),
			'concierge_specifics' => $this->getConciergeSpecifics(),
			'datetime_submitted' => $this->getDatetimeSubmitted(),
			'datetime_completed' => $this->getDatetimeCompleted(),
			'company_employee_id' => $this->getCompanyEmployeeId(),
			'company_employee_name' => $this->getCompanyEmployeeName(),
			'viewed_by' => $this->getViewedBy(),
			'viewed_on' => $this->getViewedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>