<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceRequestResponseTypes
 * Do not add any new functions to this class.
 */

class CBaseMaintenanceRequestResponseTypes extends CEosPluralBase {

	/**
	 * @return CMaintenanceRequestResponseType[]
	 */
	public static function fetchMaintenanceRequestResponseTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMaintenanceRequestResponseType::class, $objDatabase );
	}

	/**
	 * @return CMaintenanceRequestResponseType
	 */
	public static function fetchMaintenanceRequestResponseType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceRequestResponseType::class, $objDatabase );
	}

	public static function fetchMaintenanceRequestResponseTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_request_response_types', $objDatabase );
	}

	public static function fetchMaintenanceRequestResponseTypeById( $intId, $objDatabase ) {
		return self::fetchMaintenanceRequestResponseType( sprintf( 'SELECT * FROM maintenance_request_response_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>