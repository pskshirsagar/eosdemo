<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseTypes
 * Do not add any new functions to this class.
 */

class CBaseLeaseTypes extends CEosPluralBase {

	/**
	 * @return CLeaseType[]
	 */
	public static function fetchLeaseTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CLeaseType::class, $objDatabase );
	}

	/**
	 * @return CLeaseType
	 */
	public static function fetchLeaseType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CLeaseType::class, $objDatabase );
	}

	public static function fetchLeaseTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'lease_types', $objDatabase );
	}

	public static function fetchLeaseTypeById( $intId, $objDatabase ) {
		return self::fetchLeaseType( sprintf( 'SELECT * FROM lease_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>