<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyGroupAssociations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyGroupAssociations extends CEosPluralBase {

	/**
	 * @return CPropertyGroupAssociation[]
	 */
	public static function fetchPropertyGroupAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertyGroupAssociation', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertyGroupAssociation
	 */
	public static function fetchPropertyGroupAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyGroupAssociation', $objDatabase );
	}

	public static function fetchPropertyGroupAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_group_associations', $objDatabase );
	}

	public static function fetchPropertyGroupAssociationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertyGroupAssociation( sprintf( 'SELECT * FROM property_group_associations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyGroupAssociationsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyGroupAssociations( sprintf( 'SELECT * FROM property_group_associations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyGroupAssociationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyGroupAssociations( sprintf( 'SELECT * FROM property_group_associations WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyGroupAssociationsByPropertyGroupIdByCid( $intPropertyGroupId, $intCid, $objDatabase ) {
		return self::fetchPropertyGroupAssociations( sprintf( 'SELECT * FROM property_group_associations WHERE property_group_id = %d AND cid = %d', ( int ) $intPropertyGroupId, ( int ) $intCid ), $objDatabase );
	}

}
?>