<?php

class CBaseDefaultGlGroup extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_gl_groups';

	protected $m_intId;
	protected $m_intGlGroupTypeId;
	protected $m_intDefaultGlTreeId;
	protected $m_intParentDefaultGlGroupId;
	protected $m_intOriginDefaultGlGroupId;
	protected $m_intGlBranchTypeId;
	protected $m_strSystemCode;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strTotalLabel;
	protected $m_intHideOnReports;
	protected $m_intIsTotaled;
	protected $m_intIsUnderlined;
	protected $m_intIsBold;
	protected $m_intIsSystem;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intGlAccountTypeId;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intGlGroupTypeId = '1';
		$this->m_intGlBranchTypeId = '1';
		$this->m_intHideOnReports = '0';
		$this->m_intIsTotaled = '0';
		$this->m_intIsUnderlined = '0';
		$this->m_intIsBold = '0';
		$this->m_intIsSystem = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['gl_group_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlGroupTypeId', trim( $arrValues['gl_group_type_id'] ) ); elseif( isset( $arrValues['gl_group_type_id'] ) ) $this->setGlGroupTypeId( $arrValues['gl_group_type_id'] );
		if( isset( $arrValues['default_gl_tree_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultGlTreeId', trim( $arrValues['default_gl_tree_id'] ) ); elseif( isset( $arrValues['default_gl_tree_id'] ) ) $this->setDefaultGlTreeId( $arrValues['default_gl_tree_id'] );
		if( isset( $arrValues['parent_default_gl_group_id'] ) && $boolDirectSet ) $this->set( 'm_intParentDefaultGlGroupId', trim( $arrValues['parent_default_gl_group_id'] ) ); elseif( isset( $arrValues['parent_default_gl_group_id'] ) ) $this->setParentDefaultGlGroupId( $arrValues['parent_default_gl_group_id'] );
		if( isset( $arrValues['origin_default_gl_group_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginDefaultGlGroupId', trim( $arrValues['origin_default_gl_group_id'] ) ); elseif( isset( $arrValues['origin_default_gl_group_id'] ) ) $this->setOriginDefaultGlGroupId( $arrValues['origin_default_gl_group_id'] );
		if( isset( $arrValues['gl_branch_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlBranchTypeId', trim( $arrValues['gl_branch_type_id'] ) ); elseif( isset( $arrValues['gl_branch_type_id'] ) ) $this->setGlBranchTypeId( $arrValues['gl_branch_type_id'] );
		if( isset( $arrValues['system_code'] ) && $boolDirectSet ) $this->set( 'm_strSystemCode', trim( $arrValues['system_code'] ) ); elseif( isset( $arrValues['system_code'] ) ) $this->setSystemCode( $arrValues['system_code'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['total_label'] ) && $boolDirectSet ) $this->set( 'm_strTotalLabel', trim( $arrValues['total_label'] ) ); elseif( isset( $arrValues['total_label'] ) ) $this->setTotalLabel( $arrValues['total_label'] );
		if( isset( $arrValues['hide_on_reports'] ) && $boolDirectSet ) $this->set( 'm_intHideOnReports', trim( $arrValues['hide_on_reports'] ) ); elseif( isset( $arrValues['hide_on_reports'] ) ) $this->setHideOnReports( $arrValues['hide_on_reports'] );
		if( isset( $arrValues['is_totaled'] ) && $boolDirectSet ) $this->set( 'm_intIsTotaled', trim( $arrValues['is_totaled'] ) ); elseif( isset( $arrValues['is_totaled'] ) ) $this->setIsTotaled( $arrValues['is_totaled'] );
		if( isset( $arrValues['is_underlined'] ) && $boolDirectSet ) $this->set( 'm_intIsUnderlined', trim( $arrValues['is_underlined'] ) ); elseif( isset( $arrValues['is_underlined'] ) ) $this->setIsUnderlined( $arrValues['is_underlined'] );
		if( isset( $arrValues['is_bold'] ) && $boolDirectSet ) $this->set( 'm_intIsBold', trim( $arrValues['is_bold'] ) ); elseif( isset( $arrValues['is_bold'] ) ) $this->setIsBold( $arrValues['is_bold'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['gl_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlAccountTypeId', trim( $arrValues['gl_account_type_id'] ) ); elseif( isset( $arrValues['gl_account_type_id'] ) ) $this->setGlAccountTypeId( $arrValues['gl_account_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setGlGroupTypeId( $intGlGroupTypeId ) {
		$this->set( 'm_intGlGroupTypeId', CStrings::strToIntDef( $intGlGroupTypeId, NULL, false ) );
	}

	public function getGlGroupTypeId() {
		return $this->m_intGlGroupTypeId;
	}

	public function sqlGlGroupTypeId() {
		return ( true == isset( $this->m_intGlGroupTypeId ) ) ? ( string ) $this->m_intGlGroupTypeId : '1';
	}

	public function setDefaultGlTreeId( $intDefaultGlTreeId ) {
		$this->set( 'm_intDefaultGlTreeId', CStrings::strToIntDef( $intDefaultGlTreeId, NULL, false ) );
	}

	public function getDefaultGlTreeId() {
		return $this->m_intDefaultGlTreeId;
	}

	public function sqlDefaultGlTreeId() {
		return ( true == isset( $this->m_intDefaultGlTreeId ) ) ? ( string ) $this->m_intDefaultGlTreeId : 'NULL';
	}

	public function setParentDefaultGlGroupId( $intParentDefaultGlGroupId ) {
		$this->set( 'm_intParentDefaultGlGroupId', CStrings::strToIntDef( $intParentDefaultGlGroupId, NULL, false ) );
	}

	public function getParentDefaultGlGroupId() {
		return $this->m_intParentDefaultGlGroupId;
	}

	public function sqlParentDefaultGlGroupId() {
		return ( true == isset( $this->m_intParentDefaultGlGroupId ) ) ? ( string ) $this->m_intParentDefaultGlGroupId : 'NULL';
	}

	public function setOriginDefaultGlGroupId( $intOriginDefaultGlGroupId ) {
		$this->set( 'm_intOriginDefaultGlGroupId', CStrings::strToIntDef( $intOriginDefaultGlGroupId, NULL, false ) );
	}

	public function getOriginDefaultGlGroupId() {
		return $this->m_intOriginDefaultGlGroupId;
	}

	public function sqlOriginDefaultGlGroupId() {
		return ( true == isset( $this->m_intOriginDefaultGlGroupId ) ) ? ( string ) $this->m_intOriginDefaultGlGroupId : 'NULL';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function setGlBranchTypeId( $intGlBranchTypeId ) {
		$this->set( 'm_intGlBranchTypeId', CStrings::strToIntDef( $intGlBranchTypeId, NULL, false ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function getGlBranchTypeId() {
		return $this->m_intGlBranchTypeId;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Jul 14 2021.
	 */
	public function sqlGlBranchTypeId() {
		return ( true == isset( $this->m_intGlBranchTypeId ) ) ? ( string ) $this->m_intGlBranchTypeId : '1';
	}

	public function setSystemCode( $strSystemCode ) {
		$this->set( 'm_strSystemCode', CStrings::strTrimDef( $strSystemCode, 15, NULL, true ) );
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	public function sqlSystemCode() {
		return ( true == isset( $this->m_strSystemCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSystemCode ) : '\'' . addslashes( $this->m_strSystemCode ) . '\'' ) : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setTotalLabel( $strTotalLabel ) {
		$this->set( 'm_strTotalLabel', CStrings::strTrimDef( $strTotalLabel, 50, NULL, true ) );
	}

	public function getTotalLabel() {
		return $this->m_strTotalLabel;
	}

	public function sqlTotalLabel() {
		return ( true == isset( $this->m_strTotalLabel ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTotalLabel ) : '\'' . addslashes( $this->m_strTotalLabel ) . '\'' ) : 'NULL';
	}

	public function setHideOnReports( $intHideOnReports ) {
		$this->set( 'm_intHideOnReports', CStrings::strToIntDef( $intHideOnReports, NULL, false ) );
	}

	public function getHideOnReports() {
		return $this->m_intHideOnReports;
	}

	public function sqlHideOnReports() {
		return ( true == isset( $this->m_intHideOnReports ) ) ? ( string ) $this->m_intHideOnReports : '0';
	}

	public function setIsTotaled( $intIsTotaled ) {
		$this->set( 'm_intIsTotaled', CStrings::strToIntDef( $intIsTotaled, NULL, false ) );
	}

	public function getIsTotaled() {
		return $this->m_intIsTotaled;
	}

	public function sqlIsTotaled() {
		return ( true == isset( $this->m_intIsTotaled ) ) ? ( string ) $this->m_intIsTotaled : '0';
	}

	public function setIsUnderlined( $intIsUnderlined ) {
		$this->set( 'm_intIsUnderlined', CStrings::strToIntDef( $intIsUnderlined, NULL, false ) );
	}

	public function getIsUnderlined() {
		return $this->m_intIsUnderlined;
	}

	public function sqlIsUnderlined() {
		return ( true == isset( $this->m_intIsUnderlined ) ) ? ( string ) $this->m_intIsUnderlined : '0';
	}

	public function setIsBold( $intIsBold ) {
		$this->set( 'm_intIsBold', CStrings::strToIntDef( $intIsBold, NULL, false ) );
	}

	public function getIsBold() {
		return $this->m_intIsBold;
	}

	public function sqlIsBold() {
		return ( true == isset( $this->m_intIsBold ) ) ? ( string ) $this->m_intIsBold : '0';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setGlAccountTypeId( $intGlAccountTypeId ) {
		$this->set( 'm_intGlAccountTypeId', CStrings::strToIntDef( $intGlAccountTypeId, NULL, false ) );
	}

	public function getGlAccountTypeId() {
		return $this->m_intGlAccountTypeId;
	}

	public function sqlGlAccountTypeId() {
		return ( true == isset( $this->m_intGlAccountTypeId ) ) ? ( string ) $this->m_intGlAccountTypeId : 'NULL';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'gl_group_type_id' => $this->getGlGroupTypeId(),
			'default_gl_tree_id' => $this->getDefaultGlTreeId(),
			'parent_default_gl_group_id' => $this->getParentDefaultGlGroupId(),
			'origin_default_gl_group_id' => $this->getOriginDefaultGlGroupId(),
			'gl_branch_type_id' => $this->getGlBranchTypeId(),
			'system_code' => $this->getSystemCode(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'total_label' => $this->getTotalLabel(),
			'hide_on_reports' => $this->getHideOnReports(),
			'is_totaled' => $this->getIsTotaled(),
			'is_underlined' => $this->getIsUnderlined(),
			'is_bold' => $this->getIsBold(),
			'is_system' => $this->getIsSystem(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails(),
			'gl_account_type_id' => $this->getGlAccountTypeId()
		);
	}

}
?>