<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceRequestNotes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceRequestNotes extends CEosPluralBase {

	/**
	 * @return CMaintenanceRequestNote[]
	 */
	public static function fetchMaintenanceRequestNotes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMaintenanceRequestNote::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenanceRequestNote
	 */
	public static function fetchMaintenanceRequestNote( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceRequestNote::class, $objDatabase );
	}

	public static function fetchMaintenanceRequestNoteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_request_notes', $objDatabase );
	}

	public static function fetchMaintenanceRequestNoteByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestNote( sprintf( 'SELECT * FROM maintenance_request_notes WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestNotesByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestNotes( sprintf( 'SELECT * FROM maintenance_request_notes WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestNotesByMaintenanceRequestIdByCid( $intMaintenanceRequestId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestNotes( sprintf( 'SELECT * FROM maintenance_request_notes WHERE maintenance_request_id = %d AND cid = %d', $intMaintenanceRequestId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestNotesByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestNotes( sprintf( 'SELECT * FROM maintenance_request_notes WHERE company_user_id = %d AND cid = %d', $intCompanyUserId, $intCid ), $objDatabase );
	}

}
?>