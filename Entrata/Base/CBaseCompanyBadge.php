<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyBadge extends CEosSingularBase {

	const TABLE_NAME = 'public.company_badges';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDefaultBadgeId;
	protected $m_intCompanyActivityPointId;
	protected $m_strFileHandle;
	protected $m_strName;
	protected $m_strBaseDescription;
	protected $m_strProDescription;
	protected $m_intBaseCriteria;
	protected $m_intProCriteria;
	protected $m_intIsPenalty;
	protected $m_intDisplayToAll;
	protected $m_intDisplayToCustomer;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPenalty = '0';
		$this->m_intDisplayToCustomer = '0';
		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['default_badge_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultBadgeId', trim( $arrValues['default_badge_id'] ) ); elseif( isset( $arrValues['default_badge_id'] ) ) $this->setDefaultBadgeId( $arrValues['default_badge_id'] );
		if( isset( $arrValues['company_activity_point_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyActivityPointId', trim( $arrValues['company_activity_point_id'] ) ); elseif( isset( $arrValues['company_activity_point_id'] ) ) $this->setCompanyActivityPointId( $arrValues['company_activity_point_id'] );
		if( isset( $arrValues['file_handle'] ) && $boolDirectSet ) $this->set( 'm_strFileHandle', trim( stripcslashes( $arrValues['file_handle'] ) ) ); elseif( isset( $arrValues['file_handle'] ) ) $this->setFileHandle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_handle'] ) : $arrValues['file_handle'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['base_description'] ) && $boolDirectSet ) $this->set( 'm_strBaseDescription', trim( stripcslashes( $arrValues['base_description'] ) ) ); elseif( isset( $arrValues['base_description'] ) ) $this->setBaseDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['base_description'] ) : $arrValues['base_description'] );
		if( isset( $arrValues['pro_description'] ) && $boolDirectSet ) $this->set( 'm_strProDescription', trim( stripcslashes( $arrValues['pro_description'] ) ) ); elseif( isset( $arrValues['pro_description'] ) ) $this->setProDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['pro_description'] ) : $arrValues['pro_description'] );
		if( isset( $arrValues['base_criteria'] ) && $boolDirectSet ) $this->set( 'm_intBaseCriteria', trim( $arrValues['base_criteria'] ) ); elseif( isset( $arrValues['base_criteria'] ) ) $this->setBaseCriteria( $arrValues['base_criteria'] );
		if( isset( $arrValues['pro_criteria'] ) && $boolDirectSet ) $this->set( 'm_intProCriteria', trim( $arrValues['pro_criteria'] ) ); elseif( isset( $arrValues['pro_criteria'] ) ) $this->setProCriteria( $arrValues['pro_criteria'] );
		if( isset( $arrValues['is_penalty'] ) && $boolDirectSet ) $this->set( 'm_intIsPenalty', trim( $arrValues['is_penalty'] ) ); elseif( isset( $arrValues['is_penalty'] ) ) $this->setIsPenalty( $arrValues['is_penalty'] );
		if( isset( $arrValues['display_to_all'] ) && $boolDirectSet ) $this->set( 'm_intDisplayToAll', trim( $arrValues['display_to_all'] ) ); elseif( isset( $arrValues['display_to_all'] ) ) $this->setDisplayToAll( $arrValues['display_to_all'] );
		if( isset( $arrValues['display_to_customer'] ) && $boolDirectSet ) $this->set( 'm_intDisplayToCustomer', trim( $arrValues['display_to_customer'] ) ); elseif( isset( $arrValues['display_to_customer'] ) ) $this->setDisplayToCustomer( $arrValues['display_to_customer'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDefaultBadgeId( $intDefaultBadgeId ) {
		$this->set( 'm_intDefaultBadgeId', CStrings::strToIntDef( $intDefaultBadgeId, NULL, false ) );
	}

	public function getDefaultBadgeId() {
		return $this->m_intDefaultBadgeId;
	}

	public function sqlDefaultBadgeId() {
		return ( true == isset( $this->m_intDefaultBadgeId ) ) ? ( string ) $this->m_intDefaultBadgeId : 'NULL';
	}

	public function setCompanyActivityPointId( $intCompanyActivityPointId ) {
		$this->set( 'm_intCompanyActivityPointId', CStrings::strToIntDef( $intCompanyActivityPointId, NULL, false ) );
	}

	public function getCompanyActivityPointId() {
		return $this->m_intCompanyActivityPointId;
	}

	public function sqlCompanyActivityPointId() {
		return ( true == isset( $this->m_intCompanyActivityPointId ) ) ? ( string ) $this->m_intCompanyActivityPointId : 'NULL';
	}

	public function setFileHandle( $strFileHandle ) {
		$this->set( 'm_strFileHandle', CStrings::strTrimDef( $strFileHandle, 50, NULL, true ) );
	}

	public function getFileHandle() {
		return $this->m_strFileHandle;
	}

	public function sqlFileHandle() {
		return ( true == isset( $this->m_strFileHandle ) ) ? '\'' . addslashes( $this->m_strFileHandle ) . '\'' : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setBaseDescription( $strBaseDescription ) {
		$this->set( 'm_strBaseDescription', CStrings::strTrimDef( $strBaseDescription, -1, NULL, true ) );
	}

	public function getBaseDescription() {
		return $this->m_strBaseDescription;
	}

	public function sqlBaseDescription() {
		return ( true == isset( $this->m_strBaseDescription ) ) ? '\'' . addslashes( $this->m_strBaseDescription ) . '\'' : 'NULL';
	}

	public function setProDescription( $strProDescription ) {
		$this->set( 'm_strProDescription', CStrings::strTrimDef( $strProDescription, -1, NULL, true ) );
	}

	public function getProDescription() {
		return $this->m_strProDescription;
	}

	public function sqlProDescription() {
		return ( true == isset( $this->m_strProDescription ) ) ? '\'' . addslashes( $this->m_strProDescription ) . '\'' : 'NULL';
	}

	public function setBaseCriteria( $intBaseCriteria ) {
		$this->set( 'm_intBaseCriteria', CStrings::strToIntDef( $intBaseCriteria, NULL, false ) );
	}

	public function getBaseCriteria() {
		return $this->m_intBaseCriteria;
	}

	public function sqlBaseCriteria() {
		return ( true == isset( $this->m_intBaseCriteria ) ) ? ( string ) $this->m_intBaseCriteria : 'NULL';
	}

	public function setProCriteria( $intProCriteria ) {
		$this->set( 'm_intProCriteria', CStrings::strToIntDef( $intProCriteria, NULL, false ) );
	}

	public function getProCriteria() {
		return $this->m_intProCriteria;
	}

	public function sqlProCriteria() {
		return ( true == isset( $this->m_intProCriteria ) ) ? ( string ) $this->m_intProCriteria : 'NULL';
	}

	public function setIsPenalty( $intIsPenalty ) {
		$this->set( 'm_intIsPenalty', CStrings::strToIntDef( $intIsPenalty, NULL, false ) );
	}

	public function getIsPenalty() {
		return $this->m_intIsPenalty;
	}

	public function sqlIsPenalty() {
		return ( true == isset( $this->m_intIsPenalty ) ) ? ( string ) $this->m_intIsPenalty : '0';
	}

	public function setDisplayToAll( $intDisplayToAll ) {
		$this->set( 'm_intDisplayToAll', CStrings::strToIntDef( $intDisplayToAll, NULL, false ) );
	}

	public function getDisplayToAll() {
		return $this->m_intDisplayToAll;
	}

	public function sqlDisplayToAll() {
		return ( true == isset( $this->m_intDisplayToAll ) ) ? ( string ) $this->m_intDisplayToAll : 'NULL';
	}

	public function setDisplayToCustomer( $intDisplayToCustomer ) {
		$this->set( 'm_intDisplayToCustomer', CStrings::strToIntDef( $intDisplayToCustomer, NULL, false ) );
	}

	public function getDisplayToCustomer() {
		return $this->m_intDisplayToCustomer;
	}

	public function sqlDisplayToCustomer() {
		return ( true == isset( $this->m_intDisplayToCustomer ) ) ? ( string ) $this->m_intDisplayToCustomer : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, default_badge_id, company_activity_point_id, file_handle, name, base_description, pro_description, base_criteria, pro_criteria, is_penalty, display_to_all, display_to_customer, is_published, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlDefaultBadgeId() . ', ' .
 						$this->sqlCompanyActivityPointId() . ', ' .
 						$this->sqlFileHandle() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlBaseDescription() . ', ' .
 						$this->sqlProDescription() . ', ' .
 						$this->sqlBaseCriteria() . ', ' .
 						$this->sqlProCriteria() . ', ' .
 						$this->sqlIsPenalty() . ', ' .
 						$this->sqlDisplayToAll() . ', ' .
 						$this->sqlDisplayToCustomer() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_badge_id = ' . $this->sqlDefaultBadgeId() . ','; } elseif( true == array_key_exists( 'DefaultBadgeId', $this->getChangedColumns() ) ) { $strSql .= ' default_badge_id = ' . $this->sqlDefaultBadgeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_activity_point_id = ' . $this->sqlCompanyActivityPointId() . ','; } elseif( true == array_key_exists( 'CompanyActivityPointId', $this->getChangedColumns() ) ) { $strSql .= ' company_activity_point_id = ' . $this->sqlCompanyActivityPointId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_handle = ' . $this->sqlFileHandle() . ','; } elseif( true == array_key_exists( 'FileHandle', $this->getChangedColumns() ) ) { $strSql .= ' file_handle = ' . $this->sqlFileHandle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_description = ' . $this->sqlBaseDescription() . ','; } elseif( true == array_key_exists( 'BaseDescription', $this->getChangedColumns() ) ) { $strSql .= ' base_description = ' . $this->sqlBaseDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pro_description = ' . $this->sqlProDescription() . ','; } elseif( true == array_key_exists( 'ProDescription', $this->getChangedColumns() ) ) { $strSql .= ' pro_description = ' . $this->sqlProDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_criteria = ' . $this->sqlBaseCriteria() . ','; } elseif( true == array_key_exists( 'BaseCriteria', $this->getChangedColumns() ) ) { $strSql .= ' base_criteria = ' . $this->sqlBaseCriteria() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pro_criteria = ' . $this->sqlProCriteria() . ','; } elseif( true == array_key_exists( 'ProCriteria', $this->getChangedColumns() ) ) { $strSql .= ' pro_criteria = ' . $this->sqlProCriteria() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_penalty = ' . $this->sqlIsPenalty() . ','; } elseif( true == array_key_exists( 'IsPenalty', $this->getChangedColumns() ) ) { $strSql .= ' is_penalty = ' . $this->sqlIsPenalty() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' display_to_all = ' . $this->sqlDisplayToAll() . ','; } elseif( true == array_key_exists( 'DisplayToAll', $this->getChangedColumns() ) ) { $strSql .= ' display_to_all = ' . $this->sqlDisplayToAll() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' display_to_customer = ' . $this->sqlDisplayToCustomer() . ','; } elseif( true == array_key_exists( 'DisplayToCustomer', $this->getChangedColumns() ) ) { $strSql .= ' display_to_customer = ' . $this->sqlDisplayToCustomer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'default_badge_id' => $this->getDefaultBadgeId(),
			'company_activity_point_id' => $this->getCompanyActivityPointId(),
			'file_handle' => $this->getFileHandle(),
			'name' => $this->getName(),
			'base_description' => $this->getBaseDescription(),
			'pro_description' => $this->getProDescription(),
			'base_criteria' => $this->getBaseCriteria(),
			'pro_criteria' => $this->getProCriteria(),
			'is_penalty' => $this->getIsPenalty(),
			'display_to_all' => $this->getDisplayToAll(),
			'display_to_customer' => $this->getDisplayToCustomer(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>