<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFeeCalculationTypes
 * Do not add any new functions to this class.
 */

class CBaseFeeCalculationTypes extends CEosPluralBase {

	/**
	 * @return CFeeCalculationType[]
	 */
	public static function fetchFeeCalculationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CFeeCalculationType::class, $objDatabase );
	}

	/**
	 * @return CFeeCalculationType
	 */
	public static function fetchFeeCalculationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFeeCalculationType::class, $objDatabase );
	}

	public static function fetchFeeCalculationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'fee_calculation_types', $objDatabase );
	}

	public static function fetchFeeCalculationTypeById( $intId, $objDatabase ) {
		return self::fetchFeeCalculationType( sprintf( 'SELECT * FROM fee_calculation_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>