<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRoommateInterests
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRoommateInterests extends CEosPluralBase {

	/**
	 * @return CRoommateInterest[]
	 */
	public static function fetchRoommateInterests( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CRoommateInterest::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRoommateInterest
	 */
	public static function fetchRoommateInterest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRoommateInterest::class, $objDatabase );
	}

	public static function fetchRoommateInterestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'roommate_interests', $objDatabase );
	}

	public static function fetchRoommateInterestByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRoommateInterest( sprintf( 'SELECT * FROM roommate_interests WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchRoommateInterestsByCid( $intCid, $objDatabase ) {
		return self::fetchRoommateInterests( sprintf( 'SELECT * FROM roommate_interests WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchRoommateInterestsByInputTypeIdByCid( $intInputTypeId, $intCid, $objDatabase ) {
		return self::fetchRoommateInterests( sprintf( 'SELECT * FROM roommate_interests WHERE input_type_id = %d AND cid = %d', $intInputTypeId, $intCid ), $objDatabase );
	}

	public static function fetchRoommateInterestsByRoommateInterestCategoryIdByCid( $intRoommateInterestCategoryId, $intCid, $objDatabase ) {
		return self::fetchRoommateInterests( sprintf( 'SELECT * FROM roommate_interests WHERE roommate_interest_category_id = %d AND cid = %d', $intRoommateInterestCategoryId, $intCid ), $objDatabase );
	}

}
?>