<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCachedUnitSpacePeriod extends CEosSingularBase {

	const TABLE_NAME = 'public.cached_unit_space_periods';

	protected $m_intCid;
	protected $m_intPeriodId;
	protected $m_intPropertyId;
	protected $m_intPropertyFloorplanId;
	protected $m_intUnitTypeId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_intUnitSpaceRateLogId;
	protected $m_intUnitSpaceLogId;
	protected $m_intUnitSpaceStatusTypeId;
	protected $m_intUnitExclusionReasonTypeId;
	protected $m_intGroupUnitSpaceId;
	protected $m_intApplicationLeaseId;
	protected $m_intHoldLeaseId;
	protected $m_intFutureLeaseId;
	protected $m_intCurrentLeaseId;
	protected $m_intPastLeaseId;
	protected $m_strPostMonth;
	protected $m_strMoveOutDate;
	protected $m_strBuildingName;
	protected $m_strUnitNumberCache;
	protected $m_intDisplayNumber;
	protected $m_intFinancialDays;
	protected $m_intDaysVacant;
	protected $m_intDaysVacantAdjustment;
	protected $m_intConsecutiveDaysVacant;
	protected $m_fltGprRent;
	protected $m_fltBudgetedRent;
	protected $m_fltMarketRent;
	protected $m_fltGainOnLease;
	protected $m_fltLossOnLease;
	protected $m_fltGainLossOnLease;
	protected $m_fltPotentialRent;
	protected $m_fltCurrentVacancyLoss;
	protected $m_fltGainPriorVacancyAdjustment;
	protected $m_fltLossPriorVacancyAdjustment;
	protected $m_fltTotalPriorVacancyAdjustment;
	protected $m_fltTotalVacancyLoss;
	protected $m_fltConsecutiveVacancyLoss;
	protected $m_fltScheduledCurrentRentSetup;
	protected $m_fltScheduledCurrentMiscSetup;
	protected $m_fltScheduledCurrentProratedRentSetup;
	protected $m_fltScheduledCurrentProratedMiscSetup;
	protected $m_fltScheduledFutureRentSetup;
	protected $m_fltScheduledFutureMiscSetup;
	protected $m_fltScheduledFutureProratedRentSetup;
	protected $m_fltScheduledFutureProratedMiscSetup;
	protected $m_fltAcceleratedRent;
	protected $m_fltApplicationFees;
	protected $m_fltEarlyTerminationFees;
	protected $m_fltInsufficientNoticeFees;
	protected $m_fltMonthToMonthFees;
	protected $m_fltReturnItemFees;
	protected $m_fltTransferFees;
	protected $m_fltWriteOffs;
	protected $m_fltTaxFees;
	protected $m_fltLateFees;
	protected $m_fltWorkOrderFees;
	protected $m_fltDepositInterestCredits;
	protected $m_fltUtilityFees;
	protected $m_fltBeginningPrePayments;
	protected $m_fltBeginningRentReceivable;
	protected $m_fltBeginningReceivables;
	protected $m_fltBeginningDepositBalance;
	protected $m_fltBeginningDepositHeld;
	protected $m_fltPayments;
	protected $m_fltPaymentsInKind;
	protected $m_fltRentWrittenOff;
	protected $m_fltRentOutOfPeriod;
	protected $m_fltRentScheduled;
	protected $m_fltRentAdjustments;
	protected $m_fltRentBase;
	protected $m_fltRentAmenities;
	protected $m_fltRentPets;
	protected $m_fltRentAddOns;
	protected $m_fltRentRisk;
	protected $m_fltRentSpecials;
	protected $m_fltRentMaintenance;
	protected $m_fltRent;
	protected $m_fltOtherIncomeBase;
	protected $m_fltOtherIncomeAmenities;
	protected $m_fltOtherIncomePets;
	protected $m_fltOtherIncomeAddOns;
	protected $m_fltOtherIncomeRisk;
	protected $m_fltOtherIncomeSpecials;
	protected $m_fltOtherIncomeMaintenance;
	protected $m_fltOtherIncome;
	protected $m_fltTotalNewIncomeReceivable;
	protected $m_fltExpenses;
	protected $m_fltAssets;
	protected $m_fltEquity;
	protected $m_fltTotalOtherCharges;
	protected $m_fltDepositAllocations;
	protected $m_fltDepositCreditAllocations;
	protected $m_fltRefunds;
	protected $m_fltOtherLiabilities;
	protected $m_fltTotalNewNonIncomeReceivable;
	protected $m_fltTotalNewReceivables;
	protected $m_fltDepositsBase;
	protected $m_fltDepositsAmenities;
	protected $m_fltDepositsPets;
	protected $m_fltDepositsAddOns;
	protected $m_fltDepositsRisk;
	protected $m_fltDepositsSpecials;
	protected $m_fltDepositsMaintenance;
	protected $m_fltDeposits;
	protected $m_fltDepositCredits;
	protected $m_fltPaymentsAllocatedToRent;
	protected $m_fltPaymentsAllocatedToOtherIncome;
	protected $m_fltPaymentsAllocatedToExpenses;
	protected $m_fltPaymentsAllocatedToAssets;
	protected $m_fltPaymentsAllocatedToEquity;
	protected $m_fltPaymentsAllocatedToDeposits;
	protected $m_fltPaymentsAllocatedToDepositCredits;
	protected $m_fltPaymentsAllocatedToRefunds;
	protected $m_fltPaymentsAllocatedToOtherLiabilities;
	protected $m_fltPaymentsApplied;
	protected $m_fltRentAllocatedToCurrentPayments;
	protected $m_fltRentAllocatedToPrePayments;
	protected $m_fltRentAllocatedToPostToCashRent;
	protected $m_fltRentAllocatedToPostToCashOtherCharges;
	protected $m_fltRentAllocatedToPayments;
	protected $m_fltRentAllocatedToPaymentsInKind;
	protected $m_fltRentAllocatedToOtherCharges;
	protected $m_fltRentAllocatedToDeposits;
	protected $m_fltRentAllocatedToDepositCredits;
	protected $m_fltRentAllocatedToRefunds;
	protected $m_fltRentAllocatedToOtherLiabilities;
	protected $m_fltRentPaid;
	protected $m_fltDepositsAllocatedToPayments;
	protected $m_fltDepositsAllocatedToPaymentsInKind;
	protected $m_fltDepositsAllocatedToRent;
	protected $m_fltDepositsAllocatedToOtherCharges;
	protected $m_fltDepositsAllocatedToRefunds;
	protected $m_fltDepositsAllocatedToOtherLiabilities;
	protected $m_fltDepositsPaid;
	protected $m_fltDepositCreditsAllocatedToPayments;
	protected $m_fltDepositCreditsAllocatedToPaymentsInKind;
	protected $m_fltDepositCreditsAllocatedToRent;
	protected $m_fltDepositCreditsAllocatedToOtherCharges;
	protected $m_fltDepositCreditsAllocatedToRefunds;
	protected $m_fltDepositCreditsAllocatedToOtherLiabilities;
	protected $m_fltDepositCreditsPaid;
	protected $m_fltRefundsAllocatedToPayments;
	protected $m_fltRefundsAllocatedToPaymentsInKind;
	protected $m_fltRefundsAllocatedToRent;
	protected $m_fltRefundsAllocatedToOtherCharges;
	protected $m_fltRefundsAllocatedToDeposits;
	protected $m_fltRefundsAllocatedToDepositCredits;
	protected $m_fltRefundsAllocatedToRefunds;
	protected $m_fltRefundsAllocatedToOtherLiabilities;
	protected $m_fltRefundsPaid;
	protected $m_fltReceivablesAllocatedToPayments;
	protected $m_fltReceivablesAllocatedToPaymentsInKind;
	protected $m_fltTotalReceivablesPaid;
	protected $m_fltEndingPrePayments;
	protected $m_fltEndingRentReceivable;
	protected $m_fltEndingReceivables;
	protected $m_fltEndingDepositBalance;
	protected $m_fltEndingDepositHeld;
	protected $m_intScheduledChargeCount;
	protected $m_intTransactionCount;
	protected $m_intAllocationCount;
	protected $m_intLeasesApproved;
	protected $m_intMoveIns;
	protected $m_intNotices;
	protected $m_intMoveOuts;
	protected $m_intFinancialMoveOuts;
	protected $m_intRenewals;
	protected $m_intSkips;
	protected $m_intNoShows;
	protected $m_intFirstLateNotices;
	protected $m_intSecondLateNotices;
	protected $m_intThirdLateNotices;
	protected $m_intMtmStarted;
	protected $m_intMtmOpen;
	protected $m_intMtmConverted;
	protected $m_intMtmNotices;
	protected $m_intMtmMoveOuts;
	protected $m_intCollectionsStarted;
	protected $m_intCollectionsOpen;
	protected $m_intCollectionsCompleted;
	protected $m_intEvictionsStarted;
	protected $m_intEvictionsOpen;
	protected $m_intEvictionsCancelled;
	protected $m_intEvictionsCompleted;
	protected $m_intTransfersStarted;
	protected $m_intTransfersOpen;
	protected $m_intTransfersCancelled;
	protected $m_intTransfersCompleted;
	protected $m_intTotalEvents;
	protected $m_boolIsDeleted;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intFinancialDays = '0';
		$this->m_intDaysVacant = '0';
		$this->m_intDaysVacantAdjustment = '0';
		$this->m_intConsecutiveDaysVacant = '0';
		$this->m_fltGprRent = '0';
		$this->m_fltBudgetedRent = '0';
		$this->m_fltMarketRent = '0';
		$this->m_fltGainOnLease = '0';
		$this->m_fltLossOnLease = '0';
		$this->m_fltGainLossOnLease = '0';
		$this->m_fltPotentialRent = '0';
		$this->m_fltCurrentVacancyLoss = '0';
		$this->m_fltGainPriorVacancyAdjustment = '0';
		$this->m_fltLossPriorVacancyAdjustment = '0';
		$this->m_fltTotalPriorVacancyAdjustment = '0';
		$this->m_fltTotalVacancyLoss = '0';
		$this->m_fltConsecutiveVacancyLoss = '0';
		$this->m_fltScheduledCurrentRentSetup = '0';
		$this->m_fltScheduledCurrentMiscSetup = '0';
		$this->m_fltScheduledCurrentProratedRentSetup = '0';
		$this->m_fltScheduledCurrentProratedMiscSetup = '0';
		$this->m_fltScheduledFutureRentSetup = '0';
		$this->m_fltScheduledFutureMiscSetup = '0';
		$this->m_fltScheduledFutureProratedRentSetup = '0';
		$this->m_fltScheduledFutureProratedMiscSetup = '0';
		$this->m_fltAcceleratedRent = '0';
		$this->m_fltApplicationFees = '0';
		$this->m_fltEarlyTerminationFees = '0';
		$this->m_fltInsufficientNoticeFees = '0';
		$this->m_fltMonthToMonthFees = '0';
		$this->m_fltReturnItemFees = '0';
		$this->m_fltTransferFees = '0';
		$this->m_fltWriteOffs = '0';
		$this->m_fltTaxFees = '0';
		$this->m_fltLateFees = '0';
		$this->m_fltWorkOrderFees = '0';
		$this->m_fltDepositInterestCredits = '0';
		$this->m_fltUtilityFees = '0';
		$this->m_fltBeginningPrePayments = '0';
		$this->m_fltBeginningRentReceivable = '0';
		$this->m_fltBeginningReceivables = '0';
		$this->m_fltBeginningDepositBalance = '0';
		$this->m_fltBeginningDepositHeld = '0';
		$this->m_fltPayments = '0';
		$this->m_fltPaymentsInKind = '0';
		$this->m_fltRentWrittenOff = '0';
		$this->m_fltRentOutOfPeriod = '0';
		$this->m_fltRentScheduled = '0';
		$this->m_fltRentAdjustments = '0';
		$this->m_fltRentBase = '0';
		$this->m_fltRentAmenities = '0';
		$this->m_fltRentPets = '0';
		$this->m_fltRentAddOns = '0';
		$this->m_fltRentRisk = '0';
		$this->m_fltRentSpecials = '0';
		$this->m_fltRentMaintenance = '0';
		$this->m_fltRent = '0';
		$this->m_fltOtherIncomeBase = '0';
		$this->m_fltOtherIncomeAmenities = '0';
		$this->m_fltOtherIncomePets = '0';
		$this->m_fltOtherIncomeAddOns = '0';
		$this->m_fltOtherIncomeRisk = '0';
		$this->m_fltOtherIncomeSpecials = '0';
		$this->m_fltOtherIncomeMaintenance = '0';
		$this->m_fltOtherIncome = '0';
		$this->m_fltTotalNewIncomeReceivable = '0';
		$this->m_fltExpenses = '0';
		$this->m_fltAssets = '0';
		$this->m_fltEquity = '0';
		$this->m_fltTotalOtherCharges = '0';
		$this->m_fltDepositAllocations = '0';
		$this->m_fltDepositCreditAllocations = '0';
		$this->m_fltRefunds = '0';
		$this->m_fltOtherLiabilities = '0';
		$this->m_fltTotalNewNonIncomeReceivable = '0';
		$this->m_fltTotalNewReceivables = '0';
		$this->m_fltDepositsBase = '0';
		$this->m_fltDepositsAmenities = '0';
		$this->m_fltDepositsPets = '0';
		$this->m_fltDepositsAddOns = '0';
		$this->m_fltDepositsRisk = '0';
		$this->m_fltDepositsSpecials = '0';
		$this->m_fltDepositsMaintenance = '0';
		$this->m_fltDeposits = '0';
		$this->m_fltDepositCredits = '0';
		$this->m_fltPaymentsAllocatedToRent = '0';
		$this->m_fltPaymentsAllocatedToOtherIncome = '0';
		$this->m_fltPaymentsAllocatedToExpenses = '0';
		$this->m_fltPaymentsAllocatedToAssets = '0';
		$this->m_fltPaymentsAllocatedToEquity = '0';
		$this->m_fltPaymentsAllocatedToDeposits = '0';
		$this->m_fltPaymentsAllocatedToDepositCredits = '0';
		$this->m_fltPaymentsAllocatedToRefunds = '0';
		$this->m_fltPaymentsAllocatedToOtherLiabilities = '0';
		$this->m_fltPaymentsApplied = '0';
		$this->m_fltRentAllocatedToCurrentPayments = '0';
		$this->m_fltRentAllocatedToPrePayments = '0';
		$this->m_fltRentAllocatedToPostToCashRent = '0';
		$this->m_fltRentAllocatedToPostToCashOtherCharges = '0';
		$this->m_fltRentAllocatedToPayments = '0';
		$this->m_fltRentAllocatedToPaymentsInKind = '0';
		$this->m_fltRentAllocatedToOtherCharges = '0';
		$this->m_fltRentAllocatedToDeposits = '0';
		$this->m_fltRentAllocatedToDepositCredits = '0';
		$this->m_fltRentAllocatedToRefunds = '0';
		$this->m_fltRentAllocatedToOtherLiabilities = '0';
		$this->m_fltRentPaid = '0';
		$this->m_fltDepositsAllocatedToPayments = '0';
		$this->m_fltDepositsAllocatedToPaymentsInKind = '0';
		$this->m_fltDepositsAllocatedToRent = '0';
		$this->m_fltDepositsAllocatedToOtherCharges = '0';
		$this->m_fltDepositsAllocatedToRefunds = '0';
		$this->m_fltDepositsAllocatedToOtherLiabilities = '0';
		$this->m_fltDepositsPaid = '0';
		$this->m_fltDepositCreditsAllocatedToPayments = '0';
		$this->m_fltDepositCreditsAllocatedToPaymentsInKind = '0';
		$this->m_fltDepositCreditsAllocatedToRent = '0';
		$this->m_fltDepositCreditsAllocatedToOtherCharges = '0';
		$this->m_fltDepositCreditsAllocatedToRefunds = '0';
		$this->m_fltDepositCreditsAllocatedToOtherLiabilities = '0';
		$this->m_fltDepositCreditsPaid = '0';
		$this->m_fltRefundsAllocatedToPayments = '0';
		$this->m_fltRefundsAllocatedToPaymentsInKind = '0';
		$this->m_fltRefundsAllocatedToRent = '0';
		$this->m_fltRefundsAllocatedToOtherCharges = '0';
		$this->m_fltRefundsAllocatedToDeposits = '0';
		$this->m_fltRefundsAllocatedToDepositCredits = '0';
		$this->m_fltRefundsAllocatedToRefunds = '0';
		$this->m_fltRefundsAllocatedToOtherLiabilities = '0';
		$this->m_fltRefundsPaid = '0';
		$this->m_fltReceivablesAllocatedToPayments = '0';
		$this->m_fltReceivablesAllocatedToPaymentsInKind = '0';
		$this->m_fltTotalReceivablesPaid = '0';
		$this->m_fltEndingPrePayments = '0';
		$this->m_fltEndingRentReceivable = '0';
		$this->m_fltEndingReceivables = '0';
		$this->m_fltEndingDepositBalance = '0';
		$this->m_fltEndingDepositHeld = '0';
		$this->m_intScheduledChargeCount = '0';
		$this->m_intTransactionCount = '0';
		$this->m_intAllocationCount = '0';
		$this->m_intLeasesApproved = '0';
		$this->m_intMoveIns = '0';
		$this->m_intNotices = '0';
		$this->m_intMoveOuts = '0';
		$this->m_intFinancialMoveOuts = '0';
		$this->m_intRenewals = '0';
		$this->m_intSkips = '0';
		$this->m_intNoShows = '0';
		$this->m_intFirstLateNotices = '0';
		$this->m_intSecondLateNotices = '0';
		$this->m_intThirdLateNotices = '0';
		$this->m_intMtmStarted = '0';
		$this->m_intMtmOpen = '0';
		$this->m_intMtmConverted = '0';
		$this->m_intMtmNotices = '0';
		$this->m_intMtmMoveOuts = '0';
		$this->m_intCollectionsStarted = '0';
		$this->m_intCollectionsOpen = '0';
		$this->m_intCollectionsCompleted = '0';
		$this->m_intEvictionsStarted = '0';
		$this->m_intEvictionsOpen = '0';
		$this->m_intEvictionsCancelled = '0';
		$this->m_intEvictionsCompleted = '0';
		$this->m_intTransfersStarted = '0';
		$this->m_intTransfersOpen = '0';
		$this->m_intTransfersCancelled = '0';
		$this->m_intTransfersCompleted = '0';
		$this->m_intTotalEvents = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['period_id'] ) && $boolDirectSet ) $this->set( 'm_intPeriodId', trim( $arrValues['period_id'] ) ); elseif( isset( $arrValues['period_id'] ) ) $this->setPeriodId( $arrValues['period_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorplanId', trim( $arrValues['property_floorplan_id'] ) ); elseif( isset( $arrValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrValues['property_floorplan_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['unit_space_rate_log_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceRateLogId', trim( $arrValues['unit_space_rate_log_id'] ) ); elseif( isset( $arrValues['unit_space_rate_log_id'] ) ) $this->setUnitSpaceRateLogId( $arrValues['unit_space_rate_log_id'] );
		if( isset( $arrValues['unit_space_log_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceLogId', trim( $arrValues['unit_space_log_id'] ) ); elseif( isset( $arrValues['unit_space_log_id'] ) ) $this->setUnitSpaceLogId( $arrValues['unit_space_log_id'] );
		if( isset( $arrValues['unit_space_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceStatusTypeId', trim( $arrValues['unit_space_status_type_id'] ) ); elseif( isset( $arrValues['unit_space_status_type_id'] ) ) $this->setUnitSpaceStatusTypeId( $arrValues['unit_space_status_type_id'] );
		if( isset( $arrValues['unit_exclusion_reason_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitExclusionReasonTypeId', trim( $arrValues['unit_exclusion_reason_type_id'] ) ); elseif( isset( $arrValues['unit_exclusion_reason_type_id'] ) ) $this->setUnitExclusionReasonTypeId( $arrValues['unit_exclusion_reason_type_id'] );
		if( isset( $arrValues['group_unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intGroupUnitSpaceId', trim( $arrValues['group_unit_space_id'] ) ); elseif( isset( $arrValues['group_unit_space_id'] ) ) $this->setGroupUnitSpaceId( $arrValues['group_unit_space_id'] );
		if( isset( $arrValues['application_lease_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationLeaseId', trim( $arrValues['application_lease_id'] ) ); elseif( isset( $arrValues['application_lease_id'] ) ) $this->setApplicationLeaseId( $arrValues['application_lease_id'] );
		if( isset( $arrValues['hold_lease_id'] ) && $boolDirectSet ) $this->set( 'm_intHoldLeaseId', trim( $arrValues['hold_lease_id'] ) ); elseif( isset( $arrValues['hold_lease_id'] ) ) $this->setHoldLeaseId( $arrValues['hold_lease_id'] );
		if( isset( $arrValues['future_lease_id'] ) && $boolDirectSet ) $this->set( 'm_intFutureLeaseId', trim( $arrValues['future_lease_id'] ) ); elseif( isset( $arrValues['future_lease_id'] ) ) $this->setFutureLeaseId( $arrValues['future_lease_id'] );
		if( isset( $arrValues['current_lease_id'] ) && $boolDirectSet ) $this->set( 'm_intCurrentLeaseId', trim( $arrValues['current_lease_id'] ) ); elseif( isset( $arrValues['current_lease_id'] ) ) $this->setCurrentLeaseId( $arrValues['current_lease_id'] );
		if( isset( $arrValues['past_lease_id'] ) && $boolDirectSet ) $this->set( 'm_intPastLeaseId', trim( $arrValues['past_lease_id'] ) ); elseif( isset( $arrValues['past_lease_id'] ) ) $this->setPastLeaseId( $arrValues['past_lease_id'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['move_out_date'] ) && $boolDirectSet ) $this->set( 'm_strMoveOutDate', trim( $arrValues['move_out_date'] ) ); elseif( isset( $arrValues['move_out_date'] ) ) $this->setMoveOutDate( $arrValues['move_out_date'] );
		if( isset( $arrValues['building_name'] ) && $boolDirectSet ) $this->set( 'm_strBuildingName', trim( stripcslashes( $arrValues['building_name'] ) ) ); elseif( isset( $arrValues['building_name'] ) ) $this->setBuildingName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['building_name'] ) : $arrValues['building_name'] );
		if( isset( $arrValues['unit_number_cache'] ) && $boolDirectSet ) $this->set( 'm_strUnitNumberCache', trim( stripcslashes( $arrValues['unit_number_cache'] ) ) ); elseif( isset( $arrValues['unit_number_cache'] ) ) $this->setUnitNumberCache( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_number_cache'] ) : $arrValues['unit_number_cache'] );
		if( isset( $arrValues['display_number'] ) && $boolDirectSet ) $this->set( 'm_intDisplayNumber', trim( $arrValues['display_number'] ) ); elseif( isset( $arrValues['display_number'] ) ) $this->setDisplayNumber( $arrValues['display_number'] );
		if( isset( $arrValues['financial_days'] ) && $boolDirectSet ) $this->set( 'm_intFinancialDays', trim( $arrValues['financial_days'] ) ); elseif( isset( $arrValues['financial_days'] ) ) $this->setFinancialDays( $arrValues['financial_days'] );
		if( isset( $arrValues['days_vacant'] ) && $boolDirectSet ) $this->set( 'm_intDaysVacant', trim( $arrValues['days_vacant'] ) ); elseif( isset( $arrValues['days_vacant'] ) ) $this->setDaysVacant( $arrValues['days_vacant'] );
		if( isset( $arrValues['days_vacant_adjustment'] ) && $boolDirectSet ) $this->set( 'm_intDaysVacantAdjustment', trim( $arrValues['days_vacant_adjustment'] ) ); elseif( isset( $arrValues['days_vacant_adjustment'] ) ) $this->setDaysVacantAdjustment( $arrValues['days_vacant_adjustment'] );
		if( isset( $arrValues['consecutive_days_vacant'] ) && $boolDirectSet ) $this->set( 'm_intConsecutiveDaysVacant', trim( $arrValues['consecutive_days_vacant'] ) ); elseif( isset( $arrValues['consecutive_days_vacant'] ) ) $this->setConsecutiveDaysVacant( $arrValues['consecutive_days_vacant'] );
		if( isset( $arrValues['gpr_rent'] ) && $boolDirectSet ) $this->set( 'm_fltGprRent', trim( $arrValues['gpr_rent'] ) ); elseif( isset( $arrValues['gpr_rent'] ) ) $this->setGprRent( $arrValues['gpr_rent'] );
		if( isset( $arrValues['budgeted_rent'] ) && $boolDirectSet ) $this->set( 'm_fltBudgetedRent', trim( $arrValues['budgeted_rent'] ) ); elseif( isset( $arrValues['budgeted_rent'] ) ) $this->setBudgetedRent( $arrValues['budgeted_rent'] );
		if( isset( $arrValues['market_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMarketRent', trim( $arrValues['market_rent'] ) ); elseif( isset( $arrValues['market_rent'] ) ) $this->setMarketRent( $arrValues['market_rent'] );
		if( isset( $arrValues['gain_on_lease'] ) && $boolDirectSet ) $this->set( 'm_fltGainOnLease', trim( $arrValues['gain_on_lease'] ) ); elseif( isset( $arrValues['gain_on_lease'] ) ) $this->setGainOnLease( $arrValues['gain_on_lease'] );
		if( isset( $arrValues['loss_on_lease'] ) && $boolDirectSet ) $this->set( 'm_fltLossOnLease', trim( $arrValues['loss_on_lease'] ) ); elseif( isset( $arrValues['loss_on_lease'] ) ) $this->setLossOnLease( $arrValues['loss_on_lease'] );
		if( isset( $arrValues['gain_loss_on_lease'] ) && $boolDirectSet ) $this->set( 'm_fltGainLossOnLease', trim( $arrValues['gain_loss_on_lease'] ) ); elseif( isset( $arrValues['gain_loss_on_lease'] ) ) $this->setGainLossOnLease( $arrValues['gain_loss_on_lease'] );
		if( isset( $arrValues['potential_rent'] ) && $boolDirectSet ) $this->set( 'm_fltPotentialRent', trim( $arrValues['potential_rent'] ) ); elseif( isset( $arrValues['potential_rent'] ) ) $this->setPotentialRent( $arrValues['potential_rent'] );
		if( isset( $arrValues['current_vacancy_loss'] ) && $boolDirectSet ) $this->set( 'm_fltCurrentVacancyLoss', trim( $arrValues['current_vacancy_loss'] ) ); elseif( isset( $arrValues['current_vacancy_loss'] ) ) $this->setCurrentVacancyLoss( $arrValues['current_vacancy_loss'] );
		if( isset( $arrValues['gain_prior_vacancy_adjustment'] ) && $boolDirectSet ) $this->set( 'm_fltGainPriorVacancyAdjustment', trim( $arrValues['gain_prior_vacancy_adjustment'] ) ); elseif( isset( $arrValues['gain_prior_vacancy_adjustment'] ) ) $this->setGainPriorVacancyAdjustment( $arrValues['gain_prior_vacancy_adjustment'] );
		if( isset( $arrValues['loss_prior_vacancy_adjustment'] ) && $boolDirectSet ) $this->set( 'm_fltLossPriorVacancyAdjustment', trim( $arrValues['loss_prior_vacancy_adjustment'] ) ); elseif( isset( $arrValues['loss_prior_vacancy_adjustment'] ) ) $this->setLossPriorVacancyAdjustment( $arrValues['loss_prior_vacancy_adjustment'] );
		if( isset( $arrValues['total_prior_vacancy_adjustment'] ) && $boolDirectSet ) $this->set( 'm_fltTotalPriorVacancyAdjustment', trim( $arrValues['total_prior_vacancy_adjustment'] ) ); elseif( isset( $arrValues['total_prior_vacancy_adjustment'] ) ) $this->setTotalPriorVacancyAdjustment( $arrValues['total_prior_vacancy_adjustment'] );
		if( isset( $arrValues['total_vacancy_loss'] ) && $boolDirectSet ) $this->set( 'm_fltTotalVacancyLoss', trim( $arrValues['total_vacancy_loss'] ) ); elseif( isset( $arrValues['total_vacancy_loss'] ) ) $this->setTotalVacancyLoss( $arrValues['total_vacancy_loss'] );
		if( isset( $arrValues['consecutive_vacancy_loss'] ) && $boolDirectSet ) $this->set( 'm_fltConsecutiveVacancyLoss', trim( $arrValues['consecutive_vacancy_loss'] ) ); elseif( isset( $arrValues['consecutive_vacancy_loss'] ) ) $this->setConsecutiveVacancyLoss( $arrValues['consecutive_vacancy_loss'] );
		if( isset( $arrValues['scheduled_current_rent_setup'] ) && $boolDirectSet ) $this->set( 'm_fltScheduledCurrentRentSetup', trim( $arrValues['scheduled_current_rent_setup'] ) ); elseif( isset( $arrValues['scheduled_current_rent_setup'] ) ) $this->setScheduledCurrentRentSetup( $arrValues['scheduled_current_rent_setup'] );
		if( isset( $arrValues['scheduled_current_misc_setup'] ) && $boolDirectSet ) $this->set( 'm_fltScheduledCurrentMiscSetup', trim( $arrValues['scheduled_current_misc_setup'] ) ); elseif( isset( $arrValues['scheduled_current_misc_setup'] ) ) $this->setScheduledCurrentMiscSetup( $arrValues['scheduled_current_misc_setup'] );
		if( isset( $arrValues['scheduled_current_prorated_rent_setup'] ) && $boolDirectSet ) $this->set( 'm_fltScheduledCurrentProratedRentSetup', trim( $arrValues['scheduled_current_prorated_rent_setup'] ) ); elseif( isset( $arrValues['scheduled_current_prorated_rent_setup'] ) ) $this->setScheduledCurrentProratedRentSetup( $arrValues['scheduled_current_prorated_rent_setup'] );
		if( isset( $arrValues['scheduled_current_prorated_misc_setup'] ) && $boolDirectSet ) $this->set( 'm_fltScheduledCurrentProratedMiscSetup', trim( $arrValues['scheduled_current_prorated_misc_setup'] ) ); elseif( isset( $arrValues['scheduled_current_prorated_misc_setup'] ) ) $this->setScheduledCurrentProratedMiscSetup( $arrValues['scheduled_current_prorated_misc_setup'] );
		if( isset( $arrValues['scheduled_future_rent_setup'] ) && $boolDirectSet ) $this->set( 'm_fltScheduledFutureRentSetup', trim( $arrValues['scheduled_future_rent_setup'] ) ); elseif( isset( $arrValues['scheduled_future_rent_setup'] ) ) $this->setScheduledFutureRentSetup( $arrValues['scheduled_future_rent_setup'] );
		if( isset( $arrValues['scheduled_future_misc_setup'] ) && $boolDirectSet ) $this->set( 'm_fltScheduledFutureMiscSetup', trim( $arrValues['scheduled_future_misc_setup'] ) ); elseif( isset( $arrValues['scheduled_future_misc_setup'] ) ) $this->setScheduledFutureMiscSetup( $arrValues['scheduled_future_misc_setup'] );
		if( isset( $arrValues['scheduled_future_prorated_rent_setup'] ) && $boolDirectSet ) $this->set( 'm_fltScheduledFutureProratedRentSetup', trim( $arrValues['scheduled_future_prorated_rent_setup'] ) ); elseif( isset( $arrValues['scheduled_future_prorated_rent_setup'] ) ) $this->setScheduledFutureProratedRentSetup( $arrValues['scheduled_future_prorated_rent_setup'] );
		if( isset( $arrValues['scheduled_future_prorated_misc_setup'] ) && $boolDirectSet ) $this->set( 'm_fltScheduledFutureProratedMiscSetup', trim( $arrValues['scheduled_future_prorated_misc_setup'] ) ); elseif( isset( $arrValues['scheduled_future_prorated_misc_setup'] ) ) $this->setScheduledFutureProratedMiscSetup( $arrValues['scheduled_future_prorated_misc_setup'] );
		if( isset( $arrValues['accelerated_rent'] ) && $boolDirectSet ) $this->set( 'm_fltAcceleratedRent', trim( $arrValues['accelerated_rent'] ) ); elseif( isset( $arrValues['accelerated_rent'] ) ) $this->setAcceleratedRent( $arrValues['accelerated_rent'] );
		if( isset( $arrValues['application_fees'] ) && $boolDirectSet ) $this->set( 'm_fltApplicationFees', trim( $arrValues['application_fees'] ) ); elseif( isset( $arrValues['application_fees'] ) ) $this->setApplicationFees( $arrValues['application_fees'] );
		if( isset( $arrValues['early_termination_fees'] ) && $boolDirectSet ) $this->set( 'm_fltEarlyTerminationFees', trim( $arrValues['early_termination_fees'] ) ); elseif( isset( $arrValues['early_termination_fees'] ) ) $this->setEarlyTerminationFees( $arrValues['early_termination_fees'] );
		if( isset( $arrValues['insufficient_notice_fees'] ) && $boolDirectSet ) $this->set( 'm_fltInsufficientNoticeFees', trim( $arrValues['insufficient_notice_fees'] ) ); elseif( isset( $arrValues['insufficient_notice_fees'] ) ) $this->setInsufficientNoticeFees( $arrValues['insufficient_notice_fees'] );
		if( isset( $arrValues['month_to_month_fees'] ) && $boolDirectSet ) $this->set( 'm_fltMonthToMonthFees', trim( $arrValues['month_to_month_fees'] ) ); elseif( isset( $arrValues['month_to_month_fees'] ) ) $this->setMonthToMonthFees( $arrValues['month_to_month_fees'] );
		if( isset( $arrValues['return_item_fees'] ) && $boolDirectSet ) $this->set( 'm_fltReturnItemFees', trim( $arrValues['return_item_fees'] ) ); elseif( isset( $arrValues['return_item_fees'] ) ) $this->setReturnItemFees( $arrValues['return_item_fees'] );
		if( isset( $arrValues['transfer_fees'] ) && $boolDirectSet ) $this->set( 'm_fltTransferFees', trim( $arrValues['transfer_fees'] ) ); elseif( isset( $arrValues['transfer_fees'] ) ) $this->setTransferFees( $arrValues['transfer_fees'] );
		if( isset( $arrValues['write_offs'] ) && $boolDirectSet ) $this->set( 'm_fltWriteOffs', trim( $arrValues['write_offs'] ) ); elseif( isset( $arrValues['write_offs'] ) ) $this->setWriteOffs( $arrValues['write_offs'] );
		if( isset( $arrValues['tax_fees'] ) && $boolDirectSet ) $this->set( 'm_fltTaxFees', trim( $arrValues['tax_fees'] ) ); elseif( isset( $arrValues['tax_fees'] ) ) $this->setTaxFees( $arrValues['tax_fees'] );
		if( isset( $arrValues['late_fees'] ) && $boolDirectSet ) $this->set( 'm_fltLateFees', trim( $arrValues['late_fees'] ) ); elseif( isset( $arrValues['late_fees'] ) ) $this->setLateFees( $arrValues['late_fees'] );
		if( isset( $arrValues['work_order_fees'] ) && $boolDirectSet ) $this->set( 'm_fltWorkOrderFees', trim( $arrValues['work_order_fees'] ) ); elseif( isset( $arrValues['work_order_fees'] ) ) $this->setWorkOrderFees( $arrValues['work_order_fees'] );
		if( isset( $arrValues['deposit_interest_credits'] ) && $boolDirectSet ) $this->set( 'm_fltDepositInterestCredits', trim( $arrValues['deposit_interest_credits'] ) ); elseif( isset( $arrValues['deposit_interest_credits'] ) ) $this->setDepositInterestCredits( $arrValues['deposit_interest_credits'] );
		if( isset( $arrValues['utility_fees'] ) && $boolDirectSet ) $this->set( 'm_fltUtilityFees', trim( $arrValues['utility_fees'] ) ); elseif( isset( $arrValues['utility_fees'] ) ) $this->setUtilityFees( $arrValues['utility_fees'] );
		if( isset( $arrValues['beginning_pre_payments'] ) && $boolDirectSet ) $this->set( 'm_fltBeginningPrePayments', trim( $arrValues['beginning_pre_payments'] ) ); elseif( isset( $arrValues['beginning_pre_payments'] ) ) $this->setBeginningPrePayments( $arrValues['beginning_pre_payments'] );
		if( isset( $arrValues['beginning_rent_receivable'] ) && $boolDirectSet ) $this->set( 'm_fltBeginningRentReceivable', trim( $arrValues['beginning_rent_receivable'] ) ); elseif( isset( $arrValues['beginning_rent_receivable'] ) ) $this->setBeginningRentReceivable( $arrValues['beginning_rent_receivable'] );
		if( isset( $arrValues['beginning_receivables'] ) && $boolDirectSet ) $this->set( 'm_fltBeginningReceivables', trim( $arrValues['beginning_receivables'] ) ); elseif( isset( $arrValues['beginning_receivables'] ) ) $this->setBeginningReceivables( $arrValues['beginning_receivables'] );
		if( isset( $arrValues['beginning_deposit_balance'] ) && $boolDirectSet ) $this->set( 'm_fltBeginningDepositBalance', trim( $arrValues['beginning_deposit_balance'] ) ); elseif( isset( $arrValues['beginning_deposit_balance'] ) ) $this->setBeginningDepositBalance( $arrValues['beginning_deposit_balance'] );
		if( isset( $arrValues['beginning_deposit_held'] ) && $boolDirectSet ) $this->set( 'm_fltBeginningDepositHeld', trim( $arrValues['beginning_deposit_held'] ) ); elseif( isset( $arrValues['beginning_deposit_held'] ) ) $this->setBeginningDepositHeld( $arrValues['beginning_deposit_held'] );
		if( isset( $arrValues['payments'] ) && $boolDirectSet ) $this->set( 'm_fltPayments', trim( $arrValues['payments'] ) ); elseif( isset( $arrValues['payments'] ) ) $this->setPayments( $arrValues['payments'] );
		if( isset( $arrValues['payments_in_kind'] ) && $boolDirectSet ) $this->set( 'm_fltPaymentsInKind', trim( $arrValues['payments_in_kind'] ) ); elseif( isset( $arrValues['payments_in_kind'] ) ) $this->setPaymentsInKind( $arrValues['payments_in_kind'] );
		if( isset( $arrValues['rent_written_off'] ) && $boolDirectSet ) $this->set( 'm_fltRentWrittenOff', trim( $arrValues['rent_written_off'] ) ); elseif( isset( $arrValues['rent_written_off'] ) ) $this->setRentWrittenOff( $arrValues['rent_written_off'] );
		if( isset( $arrValues['rent_out_of_period'] ) && $boolDirectSet ) $this->set( 'm_fltRentOutOfPeriod', trim( $arrValues['rent_out_of_period'] ) ); elseif( isset( $arrValues['rent_out_of_period'] ) ) $this->setRentOutOfPeriod( $arrValues['rent_out_of_period'] );
		if( isset( $arrValues['rent_scheduled'] ) && $boolDirectSet ) $this->set( 'm_fltRentScheduled', trim( $arrValues['rent_scheduled'] ) ); elseif( isset( $arrValues['rent_scheduled'] ) ) $this->setRentScheduled( $arrValues['rent_scheduled'] );
		if( isset( $arrValues['rent_adjustments'] ) && $boolDirectSet ) $this->set( 'm_fltRentAdjustments', trim( $arrValues['rent_adjustments'] ) ); elseif( isset( $arrValues['rent_adjustments'] ) ) $this->setRentAdjustments( $arrValues['rent_adjustments'] );
		if( isset( $arrValues['rent_base'] ) && $boolDirectSet ) $this->set( 'm_fltRentBase', trim( $arrValues['rent_base'] ) ); elseif( isset( $arrValues['rent_base'] ) ) $this->setRentBase( $arrValues['rent_base'] );
		if( isset( $arrValues['rent_amenities'] ) && $boolDirectSet ) $this->set( 'm_fltRentAmenities', trim( $arrValues['rent_amenities'] ) ); elseif( isset( $arrValues['rent_amenities'] ) ) $this->setRentAmenities( $arrValues['rent_amenities'] );
		if( isset( $arrValues['rent_pets'] ) && $boolDirectSet ) $this->set( 'm_fltRentPets', trim( $arrValues['rent_pets'] ) ); elseif( isset( $arrValues['rent_pets'] ) ) $this->setRentPets( $arrValues['rent_pets'] );
		if( isset( $arrValues['rent_add_ons'] ) && $boolDirectSet ) $this->set( 'm_fltRentAddOns', trim( $arrValues['rent_add_ons'] ) ); elseif( isset( $arrValues['rent_add_ons'] ) ) $this->setRentAddOns( $arrValues['rent_add_ons'] );
		if( isset( $arrValues['rent_risk'] ) && $boolDirectSet ) $this->set( 'm_fltRentRisk', trim( $arrValues['rent_risk'] ) ); elseif( isset( $arrValues['rent_risk'] ) ) $this->setRentRisk( $arrValues['rent_risk'] );
		if( isset( $arrValues['rent_specials'] ) && $boolDirectSet ) $this->set( 'm_fltRentSpecials', trim( $arrValues['rent_specials'] ) ); elseif( isset( $arrValues['rent_specials'] ) ) $this->setRentSpecials( $arrValues['rent_specials'] );
		if( isset( $arrValues['rent_maintenance'] ) && $boolDirectSet ) $this->set( 'm_fltRentMaintenance', trim( $arrValues['rent_maintenance'] ) ); elseif( isset( $arrValues['rent_maintenance'] ) ) $this->setRentMaintenance( $arrValues['rent_maintenance'] );
		if( isset( $arrValues['rent'] ) && $boolDirectSet ) $this->set( 'm_fltRent', trim( $arrValues['rent'] ) ); elseif( isset( $arrValues['rent'] ) ) $this->setRent( $arrValues['rent'] );
		if( isset( $arrValues['other_income_base'] ) && $boolDirectSet ) $this->set( 'm_fltOtherIncomeBase', trim( $arrValues['other_income_base'] ) ); elseif( isset( $arrValues['other_income_base'] ) ) $this->setOtherIncomeBase( $arrValues['other_income_base'] );
		if( isset( $arrValues['other_income_amenities'] ) && $boolDirectSet ) $this->set( 'm_fltOtherIncomeAmenities', trim( $arrValues['other_income_amenities'] ) ); elseif( isset( $arrValues['other_income_amenities'] ) ) $this->setOtherIncomeAmenities( $arrValues['other_income_amenities'] );
		if( isset( $arrValues['other_income_pets'] ) && $boolDirectSet ) $this->set( 'm_fltOtherIncomePets', trim( $arrValues['other_income_pets'] ) ); elseif( isset( $arrValues['other_income_pets'] ) ) $this->setOtherIncomePets( $arrValues['other_income_pets'] );
		if( isset( $arrValues['other_income_add_ons'] ) && $boolDirectSet ) $this->set( 'm_fltOtherIncomeAddOns', trim( $arrValues['other_income_add_ons'] ) ); elseif( isset( $arrValues['other_income_add_ons'] ) ) $this->setOtherIncomeAddOns( $arrValues['other_income_add_ons'] );
		if( isset( $arrValues['other_income_risk'] ) && $boolDirectSet ) $this->set( 'm_fltOtherIncomeRisk', trim( $arrValues['other_income_risk'] ) ); elseif( isset( $arrValues['other_income_risk'] ) ) $this->setOtherIncomeRisk( $arrValues['other_income_risk'] );
		if( isset( $arrValues['other_income_specials'] ) && $boolDirectSet ) $this->set( 'm_fltOtherIncomeSpecials', trim( $arrValues['other_income_specials'] ) ); elseif( isset( $arrValues['other_income_specials'] ) ) $this->setOtherIncomeSpecials( $arrValues['other_income_specials'] );
		if( isset( $arrValues['other_income_maintenance'] ) && $boolDirectSet ) $this->set( 'm_fltOtherIncomeMaintenance', trim( $arrValues['other_income_maintenance'] ) ); elseif( isset( $arrValues['other_income_maintenance'] ) ) $this->setOtherIncomeMaintenance( $arrValues['other_income_maintenance'] );
		if( isset( $arrValues['other_income'] ) && $boolDirectSet ) $this->set( 'm_fltOtherIncome', trim( $arrValues['other_income'] ) ); elseif( isset( $arrValues['other_income'] ) ) $this->setOtherIncome( $arrValues['other_income'] );
		if( isset( $arrValues['total_new_income_receivable'] ) && $boolDirectSet ) $this->set( 'm_fltTotalNewIncomeReceivable', trim( $arrValues['total_new_income_receivable'] ) ); elseif( isset( $arrValues['total_new_income_receivable'] ) ) $this->setTotalNewIncomeReceivable( $arrValues['total_new_income_receivable'] );
		if( isset( $arrValues['expenses'] ) && $boolDirectSet ) $this->set( 'm_fltExpenses', trim( $arrValues['expenses'] ) ); elseif( isset( $arrValues['expenses'] ) ) $this->setExpenses( $arrValues['expenses'] );
		if( isset( $arrValues['assets'] ) && $boolDirectSet ) $this->set( 'm_fltAssets', trim( $arrValues['assets'] ) ); elseif( isset( $arrValues['assets'] ) ) $this->setAssets( $arrValues['assets'] );
		if( isset( $arrValues['equity'] ) && $boolDirectSet ) $this->set( 'm_fltEquity', trim( $arrValues['equity'] ) ); elseif( isset( $arrValues['equity'] ) ) $this->setEquity( $arrValues['equity'] );
		if( isset( $arrValues['total_other_charges'] ) && $boolDirectSet ) $this->set( 'm_fltTotalOtherCharges', trim( $arrValues['total_other_charges'] ) ); elseif( isset( $arrValues['total_other_charges'] ) ) $this->setTotalOtherCharges( $arrValues['total_other_charges'] );
		if( isset( $arrValues['deposit_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltDepositAllocations', trim( $arrValues['deposit_allocations'] ) ); elseif( isset( $arrValues['deposit_allocations'] ) ) $this->setDepositAllocations( $arrValues['deposit_allocations'] );
		if( isset( $arrValues['deposit_credit_allocations'] ) && $boolDirectSet ) $this->set( 'm_fltDepositCreditAllocations', trim( $arrValues['deposit_credit_allocations'] ) ); elseif( isset( $arrValues['deposit_credit_allocations'] ) ) $this->setDepositCreditAllocations( $arrValues['deposit_credit_allocations'] );
		if( isset( $arrValues['refunds'] ) && $boolDirectSet ) $this->set( 'm_fltRefunds', trim( $arrValues['refunds'] ) ); elseif( isset( $arrValues['refunds'] ) ) $this->setRefunds( $arrValues['refunds'] );
		if( isset( $arrValues['other_liabilities'] ) && $boolDirectSet ) $this->set( 'm_fltOtherLiabilities', trim( $arrValues['other_liabilities'] ) ); elseif( isset( $arrValues['other_liabilities'] ) ) $this->setOtherLiabilities( $arrValues['other_liabilities'] );
		if( isset( $arrValues['total_new_non_income_receivable'] ) && $boolDirectSet ) $this->set( 'm_fltTotalNewNonIncomeReceivable', trim( $arrValues['total_new_non_income_receivable'] ) ); elseif( isset( $arrValues['total_new_non_income_receivable'] ) ) $this->setTotalNewNonIncomeReceivable( $arrValues['total_new_non_income_receivable'] );
		if( isset( $arrValues['total_new_receivables'] ) && $boolDirectSet ) $this->set( 'm_fltTotalNewReceivables', trim( $arrValues['total_new_receivables'] ) ); elseif( isset( $arrValues['total_new_receivables'] ) ) $this->setTotalNewReceivables( $arrValues['total_new_receivables'] );
		if( isset( $arrValues['deposits_base'] ) && $boolDirectSet ) $this->set( 'm_fltDepositsBase', trim( $arrValues['deposits_base'] ) ); elseif( isset( $arrValues['deposits_base'] ) ) $this->setDepositsBase( $arrValues['deposits_base'] );
		if( isset( $arrValues['deposits_amenities'] ) && $boolDirectSet ) $this->set( 'm_fltDepositsAmenities', trim( $arrValues['deposits_amenities'] ) ); elseif( isset( $arrValues['deposits_amenities'] ) ) $this->setDepositsAmenities( $arrValues['deposits_amenities'] );
		if( isset( $arrValues['deposits_pets'] ) && $boolDirectSet ) $this->set( 'm_fltDepositsPets', trim( $arrValues['deposits_pets'] ) ); elseif( isset( $arrValues['deposits_pets'] ) ) $this->setDepositsPets( $arrValues['deposits_pets'] );
		if( isset( $arrValues['deposits_add_ons'] ) && $boolDirectSet ) $this->set( 'm_fltDepositsAddOns', trim( $arrValues['deposits_add_ons'] ) ); elseif( isset( $arrValues['deposits_add_ons'] ) ) $this->setDepositsAddOns( $arrValues['deposits_add_ons'] );
		if( isset( $arrValues['deposits_risk'] ) && $boolDirectSet ) $this->set( 'm_fltDepositsRisk', trim( $arrValues['deposits_risk'] ) ); elseif( isset( $arrValues['deposits_risk'] ) ) $this->setDepositsRisk( $arrValues['deposits_risk'] );
		if( isset( $arrValues['deposits_specials'] ) && $boolDirectSet ) $this->set( 'm_fltDepositsSpecials', trim( $arrValues['deposits_specials'] ) ); elseif( isset( $arrValues['deposits_specials'] ) ) $this->setDepositsSpecials( $arrValues['deposits_specials'] );
		if( isset( $arrValues['deposits_maintenance'] ) && $boolDirectSet ) $this->set( 'm_fltDepositsMaintenance', trim( $arrValues['deposits_maintenance'] ) ); elseif( isset( $arrValues['deposits_maintenance'] ) ) $this->setDepositsMaintenance( $arrValues['deposits_maintenance'] );
		if( isset( $arrValues['deposits'] ) && $boolDirectSet ) $this->set( 'm_fltDeposits', trim( $arrValues['deposits'] ) ); elseif( isset( $arrValues['deposits'] ) ) $this->setDeposits( $arrValues['deposits'] );
		if( isset( $arrValues['deposit_credits'] ) && $boolDirectSet ) $this->set( 'm_fltDepositCredits', trim( $arrValues['deposit_credits'] ) ); elseif( isset( $arrValues['deposit_credits'] ) ) $this->setDepositCredits( $arrValues['deposit_credits'] );
		if( isset( $arrValues['payments_allocated_to_rent'] ) && $boolDirectSet ) $this->set( 'm_fltPaymentsAllocatedToRent', trim( $arrValues['payments_allocated_to_rent'] ) ); elseif( isset( $arrValues['payments_allocated_to_rent'] ) ) $this->setPaymentsAllocatedToRent( $arrValues['payments_allocated_to_rent'] );
		if( isset( $arrValues['payments_allocated_to_other_income'] ) && $boolDirectSet ) $this->set( 'm_fltPaymentsAllocatedToOtherIncome', trim( $arrValues['payments_allocated_to_other_income'] ) ); elseif( isset( $arrValues['payments_allocated_to_other_income'] ) ) $this->setPaymentsAllocatedToOtherIncome( $arrValues['payments_allocated_to_other_income'] );
		if( isset( $arrValues['payments_allocated_to_expenses'] ) && $boolDirectSet ) $this->set( 'm_fltPaymentsAllocatedToExpenses', trim( $arrValues['payments_allocated_to_expenses'] ) ); elseif( isset( $arrValues['payments_allocated_to_expenses'] ) ) $this->setPaymentsAllocatedToExpenses( $arrValues['payments_allocated_to_expenses'] );
		if( isset( $arrValues['payments_allocated_to_assets'] ) && $boolDirectSet ) $this->set( 'm_fltPaymentsAllocatedToAssets', trim( $arrValues['payments_allocated_to_assets'] ) ); elseif( isset( $arrValues['payments_allocated_to_assets'] ) ) $this->setPaymentsAllocatedToAssets( $arrValues['payments_allocated_to_assets'] );
		if( isset( $arrValues['payments_allocated_to_equity'] ) && $boolDirectSet ) $this->set( 'm_fltPaymentsAllocatedToEquity', trim( $arrValues['payments_allocated_to_equity'] ) ); elseif( isset( $arrValues['payments_allocated_to_equity'] ) ) $this->setPaymentsAllocatedToEquity( $arrValues['payments_allocated_to_equity'] );
		if( isset( $arrValues['payments_allocated_to_deposits'] ) && $boolDirectSet ) $this->set( 'm_fltPaymentsAllocatedToDeposits', trim( $arrValues['payments_allocated_to_deposits'] ) ); elseif( isset( $arrValues['payments_allocated_to_deposits'] ) ) $this->setPaymentsAllocatedToDeposits( $arrValues['payments_allocated_to_deposits'] );
		if( isset( $arrValues['payments_allocated_to_deposit_credits'] ) && $boolDirectSet ) $this->set( 'm_fltPaymentsAllocatedToDepositCredits', trim( $arrValues['payments_allocated_to_deposit_credits'] ) ); elseif( isset( $arrValues['payments_allocated_to_deposit_credits'] ) ) $this->setPaymentsAllocatedToDepositCredits( $arrValues['payments_allocated_to_deposit_credits'] );
		if( isset( $arrValues['payments_allocated_to_refunds'] ) && $boolDirectSet ) $this->set( 'm_fltPaymentsAllocatedToRefunds', trim( $arrValues['payments_allocated_to_refunds'] ) ); elseif( isset( $arrValues['payments_allocated_to_refunds'] ) ) $this->setPaymentsAllocatedToRefunds( $arrValues['payments_allocated_to_refunds'] );
		if( isset( $arrValues['payments_allocated_to_other_liabilities'] ) && $boolDirectSet ) $this->set( 'm_fltPaymentsAllocatedToOtherLiabilities', trim( $arrValues['payments_allocated_to_other_liabilities'] ) ); elseif( isset( $arrValues['payments_allocated_to_other_liabilities'] ) ) $this->setPaymentsAllocatedToOtherLiabilities( $arrValues['payments_allocated_to_other_liabilities'] );
		if( isset( $arrValues['payments_applied'] ) && $boolDirectSet ) $this->set( 'm_fltPaymentsApplied', trim( $arrValues['payments_applied'] ) ); elseif( isset( $arrValues['payments_applied'] ) ) $this->setPaymentsApplied( $arrValues['payments_applied'] );
		if( isset( $arrValues['rent_allocated_to_current_payments'] ) && $boolDirectSet ) $this->set( 'm_fltRentAllocatedToCurrentPayments', trim( $arrValues['rent_allocated_to_current_payments'] ) ); elseif( isset( $arrValues['rent_allocated_to_current_payments'] ) ) $this->setRentAllocatedToCurrentPayments( $arrValues['rent_allocated_to_current_payments'] );
		if( isset( $arrValues['rent_allocated_to_pre_payments'] ) && $boolDirectSet ) $this->set( 'm_fltRentAllocatedToPrePayments', trim( $arrValues['rent_allocated_to_pre_payments'] ) ); elseif( isset( $arrValues['rent_allocated_to_pre_payments'] ) ) $this->setRentAllocatedToPrePayments( $arrValues['rent_allocated_to_pre_payments'] );
		if( isset( $arrValues['rent_allocated_to_post_to_cash_rent'] ) && $boolDirectSet ) $this->set( 'm_fltRentAllocatedToPostToCashRent', trim( $arrValues['rent_allocated_to_post_to_cash_rent'] ) ); elseif( isset( $arrValues['rent_allocated_to_post_to_cash_rent'] ) ) $this->setRentAllocatedToPostToCashRent( $arrValues['rent_allocated_to_post_to_cash_rent'] );
		if( isset( $arrValues['rent_allocated_to_post_to_cash_other_charges'] ) && $boolDirectSet ) $this->set( 'm_fltRentAllocatedToPostToCashOtherCharges', trim( $arrValues['rent_allocated_to_post_to_cash_other_charges'] ) ); elseif( isset( $arrValues['rent_allocated_to_post_to_cash_other_charges'] ) ) $this->setRentAllocatedToPostToCashOtherCharges( $arrValues['rent_allocated_to_post_to_cash_other_charges'] );
		if( isset( $arrValues['rent_allocated_to_payments'] ) && $boolDirectSet ) $this->set( 'm_fltRentAllocatedToPayments', trim( $arrValues['rent_allocated_to_payments'] ) ); elseif( isset( $arrValues['rent_allocated_to_payments'] ) ) $this->setRentAllocatedToPayments( $arrValues['rent_allocated_to_payments'] );
		if( isset( $arrValues['rent_allocated_to_payments_in_kind'] ) && $boolDirectSet ) $this->set( 'm_fltRentAllocatedToPaymentsInKind', trim( $arrValues['rent_allocated_to_payments_in_kind'] ) ); elseif( isset( $arrValues['rent_allocated_to_payments_in_kind'] ) ) $this->setRentAllocatedToPaymentsInKind( $arrValues['rent_allocated_to_payments_in_kind'] );
		if( isset( $arrValues['rent_allocated_to_other_charges'] ) && $boolDirectSet ) $this->set( 'm_fltRentAllocatedToOtherCharges', trim( $arrValues['rent_allocated_to_other_charges'] ) ); elseif( isset( $arrValues['rent_allocated_to_other_charges'] ) ) $this->setRentAllocatedToOtherCharges( $arrValues['rent_allocated_to_other_charges'] );
		if( isset( $arrValues['rent_allocated_to_deposits'] ) && $boolDirectSet ) $this->set( 'm_fltRentAllocatedToDeposits', trim( $arrValues['rent_allocated_to_deposits'] ) ); elseif( isset( $arrValues['rent_allocated_to_deposits'] ) ) $this->setRentAllocatedToDeposits( $arrValues['rent_allocated_to_deposits'] );
		if( isset( $arrValues['rent_allocated_to_deposit_credits'] ) && $boolDirectSet ) $this->set( 'm_fltRentAllocatedToDepositCredits', trim( $arrValues['rent_allocated_to_deposit_credits'] ) ); elseif( isset( $arrValues['rent_allocated_to_deposit_credits'] ) ) $this->setRentAllocatedToDepositCredits( $arrValues['rent_allocated_to_deposit_credits'] );
		if( isset( $arrValues['rent_allocated_to_refunds'] ) && $boolDirectSet ) $this->set( 'm_fltRentAllocatedToRefunds', trim( $arrValues['rent_allocated_to_refunds'] ) ); elseif( isset( $arrValues['rent_allocated_to_refunds'] ) ) $this->setRentAllocatedToRefunds( $arrValues['rent_allocated_to_refunds'] );
		if( isset( $arrValues['rent_allocated_to_other_liabilities'] ) && $boolDirectSet ) $this->set( 'm_fltRentAllocatedToOtherLiabilities', trim( $arrValues['rent_allocated_to_other_liabilities'] ) ); elseif( isset( $arrValues['rent_allocated_to_other_liabilities'] ) ) $this->setRentAllocatedToOtherLiabilities( $arrValues['rent_allocated_to_other_liabilities'] );
		if( isset( $arrValues['rent_paid'] ) && $boolDirectSet ) $this->set( 'm_fltRentPaid', trim( $arrValues['rent_paid'] ) ); elseif( isset( $arrValues['rent_paid'] ) ) $this->setRentPaid( $arrValues['rent_paid'] );
		if( isset( $arrValues['deposits_allocated_to_payments'] ) && $boolDirectSet ) $this->set( 'm_fltDepositsAllocatedToPayments', trim( $arrValues['deposits_allocated_to_payments'] ) ); elseif( isset( $arrValues['deposits_allocated_to_payments'] ) ) $this->setDepositsAllocatedToPayments( $arrValues['deposits_allocated_to_payments'] );
		if( isset( $arrValues['deposits_allocated_to_payments_in_kind'] ) && $boolDirectSet ) $this->set( 'm_fltDepositsAllocatedToPaymentsInKind', trim( $arrValues['deposits_allocated_to_payments_in_kind'] ) ); elseif( isset( $arrValues['deposits_allocated_to_payments_in_kind'] ) ) $this->setDepositsAllocatedToPaymentsInKind( $arrValues['deposits_allocated_to_payments_in_kind'] );
		if( isset( $arrValues['deposits_allocated_to_rent'] ) && $boolDirectSet ) $this->set( 'm_fltDepositsAllocatedToRent', trim( $arrValues['deposits_allocated_to_rent'] ) ); elseif( isset( $arrValues['deposits_allocated_to_rent'] ) ) $this->setDepositsAllocatedToRent( $arrValues['deposits_allocated_to_rent'] );
		if( isset( $arrValues['deposits_allocated_to_other_charges'] ) && $boolDirectSet ) $this->set( 'm_fltDepositsAllocatedToOtherCharges', trim( $arrValues['deposits_allocated_to_other_charges'] ) ); elseif( isset( $arrValues['deposits_allocated_to_other_charges'] ) ) $this->setDepositsAllocatedToOtherCharges( $arrValues['deposits_allocated_to_other_charges'] );
		if( isset( $arrValues['deposits_allocated_to_refunds'] ) && $boolDirectSet ) $this->set( 'm_fltDepositsAllocatedToRefunds', trim( $arrValues['deposits_allocated_to_refunds'] ) ); elseif( isset( $arrValues['deposits_allocated_to_refunds'] ) ) $this->setDepositsAllocatedToRefunds( $arrValues['deposits_allocated_to_refunds'] );
		if( isset( $arrValues['deposits_allocated_to_other_liabilities'] ) && $boolDirectSet ) $this->set( 'm_fltDepositsAllocatedToOtherLiabilities', trim( $arrValues['deposits_allocated_to_other_liabilities'] ) ); elseif( isset( $arrValues['deposits_allocated_to_other_liabilities'] ) ) $this->setDepositsAllocatedToOtherLiabilities( $arrValues['deposits_allocated_to_other_liabilities'] );
		if( isset( $arrValues['deposits_paid'] ) && $boolDirectSet ) $this->set( 'm_fltDepositsPaid', trim( $arrValues['deposits_paid'] ) ); elseif( isset( $arrValues['deposits_paid'] ) ) $this->setDepositsPaid( $arrValues['deposits_paid'] );
		if( isset( $arrValues['deposit_credits_allocated_to_payments'] ) && $boolDirectSet ) $this->set( 'm_fltDepositCreditsAllocatedToPayments', trim( $arrValues['deposit_credits_allocated_to_payments'] ) ); elseif( isset( $arrValues['deposit_credits_allocated_to_payments'] ) ) $this->setDepositCreditsAllocatedToPayments( $arrValues['deposit_credits_allocated_to_payments'] );
		if( isset( $arrValues['deposit_credits_allocated_to_payments_in_kind'] ) && $boolDirectSet ) $this->set( 'm_fltDepositCreditsAllocatedToPaymentsInKind', trim( $arrValues['deposit_credits_allocated_to_payments_in_kind'] ) ); elseif( isset( $arrValues['deposit_credits_allocated_to_payments_in_kind'] ) ) $this->setDepositCreditsAllocatedToPaymentsInKind( $arrValues['deposit_credits_allocated_to_payments_in_kind'] );
		if( isset( $arrValues['deposit_credits_allocated_to_rent'] ) && $boolDirectSet ) $this->set( 'm_fltDepositCreditsAllocatedToRent', trim( $arrValues['deposit_credits_allocated_to_rent'] ) ); elseif( isset( $arrValues['deposit_credits_allocated_to_rent'] ) ) $this->setDepositCreditsAllocatedToRent( $arrValues['deposit_credits_allocated_to_rent'] );
		if( isset( $arrValues['deposit_credits_allocated_to_other_charges'] ) && $boolDirectSet ) $this->set( 'm_fltDepositCreditsAllocatedToOtherCharges', trim( $arrValues['deposit_credits_allocated_to_other_charges'] ) ); elseif( isset( $arrValues['deposit_credits_allocated_to_other_charges'] ) ) $this->setDepositCreditsAllocatedToOtherCharges( $arrValues['deposit_credits_allocated_to_other_charges'] );
		if( isset( $arrValues['deposit_credits_allocated_to_refunds'] ) && $boolDirectSet ) $this->set( 'm_fltDepositCreditsAllocatedToRefunds', trim( $arrValues['deposit_credits_allocated_to_refunds'] ) ); elseif( isset( $arrValues['deposit_credits_allocated_to_refunds'] ) ) $this->setDepositCreditsAllocatedToRefunds( $arrValues['deposit_credits_allocated_to_refunds'] );
		if( isset( $arrValues['deposit_credits_allocated_to_other_liabilities'] ) && $boolDirectSet ) $this->set( 'm_fltDepositCreditsAllocatedToOtherLiabilities', trim( $arrValues['deposit_credits_allocated_to_other_liabilities'] ) ); elseif( isset( $arrValues['deposit_credits_allocated_to_other_liabilities'] ) ) $this->setDepositCreditsAllocatedToOtherLiabilities( $arrValues['deposit_credits_allocated_to_other_liabilities'] );
		if( isset( $arrValues['deposit_credits_paid'] ) && $boolDirectSet ) $this->set( 'm_fltDepositCreditsPaid', trim( $arrValues['deposit_credits_paid'] ) ); elseif( isset( $arrValues['deposit_credits_paid'] ) ) $this->setDepositCreditsPaid( $arrValues['deposit_credits_paid'] );
		if( isset( $arrValues['refunds_allocated_to_payments'] ) && $boolDirectSet ) $this->set( 'm_fltRefundsAllocatedToPayments', trim( $arrValues['refunds_allocated_to_payments'] ) ); elseif( isset( $arrValues['refunds_allocated_to_payments'] ) ) $this->setRefundsAllocatedToPayments( $arrValues['refunds_allocated_to_payments'] );
		if( isset( $arrValues['refunds_allocated_to_payments_in_kind'] ) && $boolDirectSet ) $this->set( 'm_fltRefundsAllocatedToPaymentsInKind', trim( $arrValues['refunds_allocated_to_payments_in_kind'] ) ); elseif( isset( $arrValues['refunds_allocated_to_payments_in_kind'] ) ) $this->setRefundsAllocatedToPaymentsInKind( $arrValues['refunds_allocated_to_payments_in_kind'] );
		if( isset( $arrValues['refunds_allocated_to_rent'] ) && $boolDirectSet ) $this->set( 'm_fltRefundsAllocatedToRent', trim( $arrValues['refunds_allocated_to_rent'] ) ); elseif( isset( $arrValues['refunds_allocated_to_rent'] ) ) $this->setRefundsAllocatedToRent( $arrValues['refunds_allocated_to_rent'] );
		if( isset( $arrValues['refunds_allocated_to_other_charges'] ) && $boolDirectSet ) $this->set( 'm_fltRefundsAllocatedToOtherCharges', trim( $arrValues['refunds_allocated_to_other_charges'] ) ); elseif( isset( $arrValues['refunds_allocated_to_other_charges'] ) ) $this->setRefundsAllocatedToOtherCharges( $arrValues['refunds_allocated_to_other_charges'] );
		if( isset( $arrValues['refunds_allocated_to_deposits'] ) && $boolDirectSet ) $this->set( 'm_fltRefundsAllocatedToDeposits', trim( $arrValues['refunds_allocated_to_deposits'] ) ); elseif( isset( $arrValues['refunds_allocated_to_deposits'] ) ) $this->setRefundsAllocatedToDeposits( $arrValues['refunds_allocated_to_deposits'] );
		if( isset( $arrValues['refunds_allocated_to_deposit_credits'] ) && $boolDirectSet ) $this->set( 'm_fltRefundsAllocatedToDepositCredits', trim( $arrValues['refunds_allocated_to_deposit_credits'] ) ); elseif( isset( $arrValues['refunds_allocated_to_deposit_credits'] ) ) $this->setRefundsAllocatedToDepositCredits( $arrValues['refunds_allocated_to_deposit_credits'] );
		if( isset( $arrValues['refunds_allocated_to_refunds'] ) && $boolDirectSet ) $this->set( 'm_fltRefundsAllocatedToRefunds', trim( $arrValues['refunds_allocated_to_refunds'] ) ); elseif( isset( $arrValues['refunds_allocated_to_refunds'] ) ) $this->setRefundsAllocatedToRefunds( $arrValues['refunds_allocated_to_refunds'] );
		if( isset( $arrValues['refunds_allocated_to_other_liabilities'] ) && $boolDirectSet ) $this->set( 'm_fltRefundsAllocatedToOtherLiabilities', trim( $arrValues['refunds_allocated_to_other_liabilities'] ) ); elseif( isset( $arrValues['refunds_allocated_to_other_liabilities'] ) ) $this->setRefundsAllocatedToOtherLiabilities( $arrValues['refunds_allocated_to_other_liabilities'] );
		if( isset( $arrValues['refunds_paid'] ) && $boolDirectSet ) $this->set( 'm_fltRefundsPaid', trim( $arrValues['refunds_paid'] ) ); elseif( isset( $arrValues['refunds_paid'] ) ) $this->setRefundsPaid( $arrValues['refunds_paid'] );
		if( isset( $arrValues['receivables_allocated_to_payments'] ) && $boolDirectSet ) $this->set( 'm_fltReceivablesAllocatedToPayments', trim( $arrValues['receivables_allocated_to_payments'] ) ); elseif( isset( $arrValues['receivables_allocated_to_payments'] ) ) $this->setReceivablesAllocatedToPayments( $arrValues['receivables_allocated_to_payments'] );
		if( isset( $arrValues['receivables_allocated_to_payments_in_kind'] ) && $boolDirectSet ) $this->set( 'm_fltReceivablesAllocatedToPaymentsInKind', trim( $arrValues['receivables_allocated_to_payments_in_kind'] ) ); elseif( isset( $arrValues['receivables_allocated_to_payments_in_kind'] ) ) $this->setReceivablesAllocatedToPaymentsInKind( $arrValues['receivables_allocated_to_payments_in_kind'] );
		if( isset( $arrValues['total_receivables_paid'] ) && $boolDirectSet ) $this->set( 'm_fltTotalReceivablesPaid', trim( $arrValues['total_receivables_paid'] ) ); elseif( isset( $arrValues['total_receivables_paid'] ) ) $this->setTotalReceivablesPaid( $arrValues['total_receivables_paid'] );
		if( isset( $arrValues['ending_pre_payments'] ) && $boolDirectSet ) $this->set( 'm_fltEndingPrePayments', trim( $arrValues['ending_pre_payments'] ) ); elseif( isset( $arrValues['ending_pre_payments'] ) ) $this->setEndingPrePayments( $arrValues['ending_pre_payments'] );
		if( isset( $arrValues['ending_rent_receivable'] ) && $boolDirectSet ) $this->set( 'm_fltEndingRentReceivable', trim( $arrValues['ending_rent_receivable'] ) ); elseif( isset( $arrValues['ending_rent_receivable'] ) ) $this->setEndingRentReceivable( $arrValues['ending_rent_receivable'] );
		if( isset( $arrValues['ending_receivables'] ) && $boolDirectSet ) $this->set( 'm_fltEndingReceivables', trim( $arrValues['ending_receivables'] ) ); elseif( isset( $arrValues['ending_receivables'] ) ) $this->setEndingReceivables( $arrValues['ending_receivables'] );
		if( isset( $arrValues['ending_deposit_balance'] ) && $boolDirectSet ) $this->set( 'm_fltEndingDepositBalance', trim( $arrValues['ending_deposit_balance'] ) ); elseif( isset( $arrValues['ending_deposit_balance'] ) ) $this->setEndingDepositBalance( $arrValues['ending_deposit_balance'] );
		if( isset( $arrValues['ending_deposit_held'] ) && $boolDirectSet ) $this->set( 'm_fltEndingDepositHeld', trim( $arrValues['ending_deposit_held'] ) ); elseif( isset( $arrValues['ending_deposit_held'] ) ) $this->setEndingDepositHeld( $arrValues['ending_deposit_held'] );
		if( isset( $arrValues['scheduled_charge_count'] ) && $boolDirectSet ) $this->set( 'm_intScheduledChargeCount', trim( $arrValues['scheduled_charge_count'] ) ); elseif( isset( $arrValues['scheduled_charge_count'] ) ) $this->setScheduledChargeCount( $arrValues['scheduled_charge_count'] );
		if( isset( $arrValues['transaction_count'] ) && $boolDirectSet ) $this->set( 'm_intTransactionCount', trim( $arrValues['transaction_count'] ) ); elseif( isset( $arrValues['transaction_count'] ) ) $this->setTransactionCount( $arrValues['transaction_count'] );
		if( isset( $arrValues['allocation_count'] ) && $boolDirectSet ) $this->set( 'm_intAllocationCount', trim( $arrValues['allocation_count'] ) ); elseif( isset( $arrValues['allocation_count'] ) ) $this->setAllocationCount( $arrValues['allocation_count'] );
		if( isset( $arrValues['leases_approved'] ) && $boolDirectSet ) $this->set( 'm_intLeasesApproved', trim( $arrValues['leases_approved'] ) ); elseif( isset( $arrValues['leases_approved'] ) ) $this->setLeasesApproved( $arrValues['leases_approved'] );
		if( isset( $arrValues['move_ins'] ) && $boolDirectSet ) $this->set( 'm_intMoveIns', trim( $arrValues['move_ins'] ) ); elseif( isset( $arrValues['move_ins'] ) ) $this->setMoveIns( $arrValues['move_ins'] );
		if( isset( $arrValues['notices'] ) && $boolDirectSet ) $this->set( 'm_intNotices', trim( $arrValues['notices'] ) ); elseif( isset( $arrValues['notices'] ) ) $this->setNotices( $arrValues['notices'] );
		if( isset( $arrValues['move_outs'] ) && $boolDirectSet ) $this->set( 'm_intMoveOuts', trim( $arrValues['move_outs'] ) ); elseif( isset( $arrValues['move_outs'] ) ) $this->setMoveOuts( $arrValues['move_outs'] );
		if( isset( $arrValues['financial_move_outs'] ) && $boolDirectSet ) $this->set( 'm_intFinancialMoveOuts', trim( $arrValues['financial_move_outs'] ) ); elseif( isset( $arrValues['financial_move_outs'] ) ) $this->setFinancialMoveOuts( $arrValues['financial_move_outs'] );
		if( isset( $arrValues['renewals'] ) && $boolDirectSet ) $this->set( 'm_intRenewals', trim( $arrValues['renewals'] ) ); elseif( isset( $arrValues['renewals'] ) ) $this->setRenewals( $arrValues['renewals'] );
		if( isset( $arrValues['skips'] ) && $boolDirectSet ) $this->set( 'm_intSkips', trim( $arrValues['skips'] ) ); elseif( isset( $arrValues['skips'] ) ) $this->setSkips( $arrValues['skips'] );
		if( isset( $arrValues['no_shows'] ) && $boolDirectSet ) $this->set( 'm_intNoShows', trim( $arrValues['no_shows'] ) ); elseif( isset( $arrValues['no_shows'] ) ) $this->setNoShows( $arrValues['no_shows'] );
		if( isset( $arrValues['first_late_notices'] ) && $boolDirectSet ) $this->set( 'm_intFirstLateNotices', trim( $arrValues['first_late_notices'] ) ); elseif( isset( $arrValues['first_late_notices'] ) ) $this->setFirstLateNotices( $arrValues['first_late_notices'] );
		if( isset( $arrValues['second_late_notices'] ) && $boolDirectSet ) $this->set( 'm_intSecondLateNotices', trim( $arrValues['second_late_notices'] ) ); elseif( isset( $arrValues['second_late_notices'] ) ) $this->setSecondLateNotices( $arrValues['second_late_notices'] );
		if( isset( $arrValues['third_late_notices'] ) && $boolDirectSet ) $this->set( 'm_intThirdLateNotices', trim( $arrValues['third_late_notices'] ) ); elseif( isset( $arrValues['third_late_notices'] ) ) $this->setThirdLateNotices( $arrValues['third_late_notices'] );
		if( isset( $arrValues['mtm_started'] ) && $boolDirectSet ) $this->set( 'm_intMtmStarted', trim( $arrValues['mtm_started'] ) ); elseif( isset( $arrValues['mtm_started'] ) ) $this->setMtmStarted( $arrValues['mtm_started'] );
		if( isset( $arrValues['mtm_open'] ) && $boolDirectSet ) $this->set( 'm_intMtmOpen', trim( $arrValues['mtm_open'] ) ); elseif( isset( $arrValues['mtm_open'] ) ) $this->setMtmOpen( $arrValues['mtm_open'] );
		if( isset( $arrValues['mtm_converted'] ) && $boolDirectSet ) $this->set( 'm_intMtmConverted', trim( $arrValues['mtm_converted'] ) ); elseif( isset( $arrValues['mtm_converted'] ) ) $this->setMtmConverted( $arrValues['mtm_converted'] );
		if( isset( $arrValues['mtm_notices'] ) && $boolDirectSet ) $this->set( 'm_intMtmNotices', trim( $arrValues['mtm_notices'] ) ); elseif( isset( $arrValues['mtm_notices'] ) ) $this->setMtmNotices( $arrValues['mtm_notices'] );
		if( isset( $arrValues['mtm_move_outs'] ) && $boolDirectSet ) $this->set( 'm_intMtmMoveOuts', trim( $arrValues['mtm_move_outs'] ) ); elseif( isset( $arrValues['mtm_move_outs'] ) ) $this->setMtmMoveOuts( $arrValues['mtm_move_outs'] );
		if( isset( $arrValues['collections_started'] ) && $boolDirectSet ) $this->set( 'm_intCollectionsStarted', trim( $arrValues['collections_started'] ) ); elseif( isset( $arrValues['collections_started'] ) ) $this->setCollectionsStarted( $arrValues['collections_started'] );
		if( isset( $arrValues['collections_open'] ) && $boolDirectSet ) $this->set( 'm_intCollectionsOpen', trim( $arrValues['collections_open'] ) ); elseif( isset( $arrValues['collections_open'] ) ) $this->setCollectionsOpen( $arrValues['collections_open'] );
		if( isset( $arrValues['collections_completed'] ) && $boolDirectSet ) $this->set( 'm_intCollectionsCompleted', trim( $arrValues['collections_completed'] ) ); elseif( isset( $arrValues['collections_completed'] ) ) $this->setCollectionsCompleted( $arrValues['collections_completed'] );
		if( isset( $arrValues['evictions_started'] ) && $boolDirectSet ) $this->set( 'm_intEvictionsStarted', trim( $arrValues['evictions_started'] ) ); elseif( isset( $arrValues['evictions_started'] ) ) $this->setEvictionsStarted( $arrValues['evictions_started'] );
		if( isset( $arrValues['evictions_open'] ) && $boolDirectSet ) $this->set( 'm_intEvictionsOpen', trim( $arrValues['evictions_open'] ) ); elseif( isset( $arrValues['evictions_open'] ) ) $this->setEvictionsOpen( $arrValues['evictions_open'] );
		if( isset( $arrValues['evictions_cancelled'] ) && $boolDirectSet ) $this->set( 'm_intEvictionsCancelled', trim( $arrValues['evictions_cancelled'] ) ); elseif( isset( $arrValues['evictions_cancelled'] ) ) $this->setEvictionsCancelled( $arrValues['evictions_cancelled'] );
		if( isset( $arrValues['evictions_completed'] ) && $boolDirectSet ) $this->set( 'm_intEvictionsCompleted', trim( $arrValues['evictions_completed'] ) ); elseif( isset( $arrValues['evictions_completed'] ) ) $this->setEvictionsCompleted( $arrValues['evictions_completed'] );
		if( isset( $arrValues['transfers_started'] ) && $boolDirectSet ) $this->set( 'm_intTransfersStarted', trim( $arrValues['transfers_started'] ) ); elseif( isset( $arrValues['transfers_started'] ) ) $this->setTransfersStarted( $arrValues['transfers_started'] );
		if( isset( $arrValues['transfers_open'] ) && $boolDirectSet ) $this->set( 'm_intTransfersOpen', trim( $arrValues['transfers_open'] ) ); elseif( isset( $arrValues['transfers_open'] ) ) $this->setTransfersOpen( $arrValues['transfers_open'] );
		if( isset( $arrValues['transfers_cancelled'] ) && $boolDirectSet ) $this->set( 'm_intTransfersCancelled', trim( $arrValues['transfers_cancelled'] ) ); elseif( isset( $arrValues['transfers_cancelled'] ) ) $this->setTransfersCancelled( $arrValues['transfers_cancelled'] );
		if( isset( $arrValues['transfers_completed'] ) && $boolDirectSet ) $this->set( 'm_intTransfersCompleted', trim( $arrValues['transfers_completed'] ) ); elseif( isset( $arrValues['transfers_completed'] ) ) $this->setTransfersCompleted( $arrValues['transfers_completed'] );
		if( isset( $arrValues['total_events'] ) && $boolDirectSet ) $this->set( 'm_intTotalEvents', trim( $arrValues['total_events'] ) ); elseif( isset( $arrValues['total_events'] ) ) $this->setTotalEvents( $arrValues['total_events'] );
		if( isset( $arrValues['is_deleted'] ) && $boolDirectSet ) $this->set( 'm_boolIsDeleted', trim( stripcslashes( $arrValues['is_deleted'] ) ) ); elseif( isset( $arrValues['is_deleted'] ) ) $this->setIsDeleted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_deleted'] ) : $arrValues['is_deleted'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPeriodId( $intPeriodId ) {
		$this->set( 'm_intPeriodId', CStrings::strToIntDef( $intPeriodId, NULL, false ) );
	}

	public function getPeriodId() {
		return $this->m_intPeriodId;
	}

	public function sqlPeriodId() {
		return ( true == isset( $this->m_intPeriodId ) ) ? ( string ) $this->m_intPeriodId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->set( 'm_intPropertyFloorplanId', CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false ) );
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function sqlPropertyFloorplanId() {
		return ( true == isset( $this->m_intPropertyFloorplanId ) ) ? ( string ) $this->m_intPropertyFloorplanId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setUnitSpaceRateLogId( $intUnitSpaceRateLogId ) {
		$this->set( 'm_intUnitSpaceRateLogId', CStrings::strToIntDef( $intUnitSpaceRateLogId, NULL, false ) );
	}

	public function getUnitSpaceRateLogId() {
		return $this->m_intUnitSpaceRateLogId;
	}

	public function sqlUnitSpaceRateLogId() {
		return ( true == isset( $this->m_intUnitSpaceRateLogId ) ) ? ( string ) $this->m_intUnitSpaceRateLogId : 'NULL';
	}

	public function setUnitSpaceLogId( $intUnitSpaceLogId ) {
		$this->set( 'm_intUnitSpaceLogId', CStrings::strToIntDef( $intUnitSpaceLogId, NULL, false ) );
	}

	public function getUnitSpaceLogId() {
		return $this->m_intUnitSpaceLogId;
	}

	public function sqlUnitSpaceLogId() {
		return ( true == isset( $this->m_intUnitSpaceLogId ) ) ? ( string ) $this->m_intUnitSpaceLogId : 'NULL';
	}

	public function setUnitSpaceStatusTypeId( $intUnitSpaceStatusTypeId ) {
		$this->set( 'm_intUnitSpaceStatusTypeId', CStrings::strToIntDef( $intUnitSpaceStatusTypeId, NULL, false ) );
	}

	public function getUnitSpaceStatusTypeId() {
		return $this->m_intUnitSpaceStatusTypeId;
	}

	public function sqlUnitSpaceStatusTypeId() {
		return ( true == isset( $this->m_intUnitSpaceStatusTypeId ) ) ? ( string ) $this->m_intUnitSpaceStatusTypeId : 'NULL';
	}

	public function setUnitExclusionReasonTypeId( $intUnitExclusionReasonTypeId ) {
		$this->set( 'm_intUnitExclusionReasonTypeId', CStrings::strToIntDef( $intUnitExclusionReasonTypeId, NULL, false ) );
	}

	public function getUnitExclusionReasonTypeId() {
		return $this->m_intUnitExclusionReasonTypeId;
	}

	public function sqlUnitExclusionReasonTypeId() {
		return ( true == isset( $this->m_intUnitExclusionReasonTypeId ) ) ? ( string ) $this->m_intUnitExclusionReasonTypeId : 'NULL';
	}

	public function setGroupUnitSpaceId( $intGroupUnitSpaceId ) {
		$this->set( 'm_intGroupUnitSpaceId', CStrings::strToIntDef( $intGroupUnitSpaceId, NULL, false ) );
	}

	public function getGroupUnitSpaceId() {
		return $this->m_intGroupUnitSpaceId;
	}

	public function sqlGroupUnitSpaceId() {
		return ( true == isset( $this->m_intGroupUnitSpaceId ) ) ? ( string ) $this->m_intGroupUnitSpaceId : 'NULL';
	}

	public function setApplicationLeaseId( $intApplicationLeaseId ) {
		$this->set( 'm_intApplicationLeaseId', CStrings::strToIntDef( $intApplicationLeaseId, NULL, false ) );
	}

	public function getApplicationLeaseId() {
		return $this->m_intApplicationLeaseId;
	}

	public function sqlApplicationLeaseId() {
		return ( true == isset( $this->m_intApplicationLeaseId ) ) ? ( string ) $this->m_intApplicationLeaseId : 'NULL';
	}

	public function setHoldLeaseId( $intHoldLeaseId ) {
		$this->set( 'm_intHoldLeaseId', CStrings::strToIntDef( $intHoldLeaseId, NULL, false ) );
	}

	public function getHoldLeaseId() {
		return $this->m_intHoldLeaseId;
	}

	public function sqlHoldLeaseId() {
		return ( true == isset( $this->m_intHoldLeaseId ) ) ? ( string ) $this->m_intHoldLeaseId : 'NULL';
	}

	public function setFutureLeaseId( $intFutureLeaseId ) {
		$this->set( 'm_intFutureLeaseId', CStrings::strToIntDef( $intFutureLeaseId, NULL, false ) );
	}

	public function getFutureLeaseId() {
		return $this->m_intFutureLeaseId;
	}

	public function sqlFutureLeaseId() {
		return ( true == isset( $this->m_intFutureLeaseId ) ) ? ( string ) $this->m_intFutureLeaseId : 'NULL';
	}

	public function setCurrentLeaseId( $intCurrentLeaseId ) {
		$this->set( 'm_intCurrentLeaseId', CStrings::strToIntDef( $intCurrentLeaseId, NULL, false ) );
	}

	public function getCurrentLeaseId() {
		return $this->m_intCurrentLeaseId;
	}

	public function sqlCurrentLeaseId() {
		return ( true == isset( $this->m_intCurrentLeaseId ) ) ? ( string ) $this->m_intCurrentLeaseId : 'NULL';
	}

	public function setPastLeaseId( $intPastLeaseId ) {
		$this->set( 'm_intPastLeaseId', CStrings::strToIntDef( $intPastLeaseId, NULL, false ) );
	}

	public function getPastLeaseId() {
		return $this->m_intPastLeaseId;
	}

	public function sqlPastLeaseId() {
		return ( true == isset( $this->m_intPastLeaseId ) ) ? ( string ) $this->m_intPastLeaseId : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->set( 'm_strMoveOutDate', CStrings::strTrimDef( $strMoveOutDate, -1, NULL, true ) );
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function sqlMoveOutDate() {
		return ( true == isset( $this->m_strMoveOutDate ) ) ? '\'' . $this->m_strMoveOutDate . '\'' : 'NULL';
	}

	public function setBuildingName( $strBuildingName ) {
		$this->set( 'm_strBuildingName', CStrings::strTrimDef( $strBuildingName, 50, NULL, true ) );
	}

	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	public function sqlBuildingName() {
		return ( true == isset( $this->m_strBuildingName ) ) ? '\'' . addslashes( $this->m_strBuildingName ) . '\'' : 'NULL';
	}

	public function setUnitNumberCache( $strUnitNumberCache ) {
		$this->set( 'm_strUnitNumberCache', CStrings::strTrimDef( $strUnitNumberCache, 50, NULL, true ) );
	}

	public function getUnitNumberCache() {
		return $this->m_strUnitNumberCache;
	}

	public function sqlUnitNumberCache() {
		return ( true == isset( $this->m_strUnitNumberCache ) ) ? '\'' . addslashes( $this->m_strUnitNumberCache ) . '\'' : 'NULL';
	}

	public function setDisplayNumber( $intDisplayNumber ) {
		$this->set( 'm_intDisplayNumber', CStrings::strToIntDef( $intDisplayNumber, NULL, false ) );
	}

	public function getDisplayNumber() {
		return $this->m_intDisplayNumber;
	}

	public function sqlDisplayNumber() {
		return ( true == isset( $this->m_intDisplayNumber ) ) ? ( string ) $this->m_intDisplayNumber : 'NULL';
	}

	public function setFinancialDays( $intFinancialDays ) {
		$this->set( 'm_intFinancialDays', CStrings::strToIntDef( $intFinancialDays, NULL, false ) );
	}

	public function getFinancialDays() {
		return $this->m_intFinancialDays;
	}

	public function sqlFinancialDays() {
		return ( true == isset( $this->m_intFinancialDays ) ) ? ( string ) $this->m_intFinancialDays : '0';
	}

	public function setDaysVacant( $intDaysVacant ) {
		$this->set( 'm_intDaysVacant', CStrings::strToIntDef( $intDaysVacant, NULL, false ) );
	}

	public function getDaysVacant() {
		return $this->m_intDaysVacant;
	}

	public function sqlDaysVacant() {
		return ( true == isset( $this->m_intDaysVacant ) ) ? ( string ) $this->m_intDaysVacant : '0';
	}

	public function setDaysVacantAdjustment( $intDaysVacantAdjustment ) {
		$this->set( 'm_intDaysVacantAdjustment', CStrings::strToIntDef( $intDaysVacantAdjustment, NULL, false ) );
	}

	public function getDaysVacantAdjustment() {
		return $this->m_intDaysVacantAdjustment;
	}

	public function sqlDaysVacantAdjustment() {
		return ( true == isset( $this->m_intDaysVacantAdjustment ) ) ? ( string ) $this->m_intDaysVacantAdjustment : '0';
	}

	public function setConsecutiveDaysVacant( $intConsecutiveDaysVacant ) {
		$this->set( 'm_intConsecutiveDaysVacant', CStrings::strToIntDef( $intConsecutiveDaysVacant, NULL, false ) );
	}

	public function getConsecutiveDaysVacant() {
		return $this->m_intConsecutiveDaysVacant;
	}

	public function sqlConsecutiveDaysVacant() {
		return ( true == isset( $this->m_intConsecutiveDaysVacant ) ) ? ( string ) $this->m_intConsecutiveDaysVacant : '0';
	}

	public function setGprRent( $fltGprRent ) {
		$this->set( 'm_fltGprRent', CStrings::strToFloatDef( $fltGprRent, NULL, false, 2 ) );
	}

	public function getGprRent() {
		return $this->m_fltGprRent;
	}

	public function sqlGprRent() {
		return ( true == isset( $this->m_fltGprRent ) ) ? ( string ) $this->m_fltGprRent : '0';
	}

	public function setBudgetedRent( $fltBudgetedRent ) {
		$this->set( 'm_fltBudgetedRent', CStrings::strToFloatDef( $fltBudgetedRent, NULL, false, 2 ) );
	}

	public function getBudgetedRent() {
		return $this->m_fltBudgetedRent;
	}

	public function sqlBudgetedRent() {
		return ( true == isset( $this->m_fltBudgetedRent ) ) ? ( string ) $this->m_fltBudgetedRent : '0';
	}

	public function setMarketRent( $fltMarketRent ) {
		$this->set( 'm_fltMarketRent', CStrings::strToFloatDef( $fltMarketRent, NULL, false, 2 ) );
	}

	public function getMarketRent() {
		return $this->m_fltMarketRent;
	}

	public function sqlMarketRent() {
		return ( true == isset( $this->m_fltMarketRent ) ) ? ( string ) $this->m_fltMarketRent : '0';
	}

	public function setGainOnLease( $fltGainOnLease ) {
		$this->set( 'm_fltGainOnLease', CStrings::strToFloatDef( $fltGainOnLease, NULL, false, 2 ) );
	}

	public function getGainOnLease() {
		return $this->m_fltGainOnLease;
	}

	public function sqlGainOnLease() {
		return ( true == isset( $this->m_fltGainOnLease ) ) ? ( string ) $this->m_fltGainOnLease : '0';
	}

	public function setLossOnLease( $fltLossOnLease ) {
		$this->set( 'm_fltLossOnLease', CStrings::strToFloatDef( $fltLossOnLease, NULL, false, 2 ) );
	}

	public function getLossOnLease() {
		return $this->m_fltLossOnLease;
	}

	public function sqlLossOnLease() {
		return ( true == isset( $this->m_fltLossOnLease ) ) ? ( string ) $this->m_fltLossOnLease : '0';
	}

	public function setGainLossOnLease( $fltGainLossOnLease ) {
		$this->set( 'm_fltGainLossOnLease', CStrings::strToFloatDef( $fltGainLossOnLease, NULL, false, 2 ) );
	}

	public function getGainLossOnLease() {
		return $this->m_fltGainLossOnLease;
	}

	public function sqlGainLossOnLease() {
		return ( true == isset( $this->m_fltGainLossOnLease ) ) ? ( string ) $this->m_fltGainLossOnLease : '0';
	}

	public function setPotentialRent( $fltPotentialRent ) {
		$this->set( 'm_fltPotentialRent', CStrings::strToFloatDef( $fltPotentialRent, NULL, false, 2 ) );
	}

	public function getPotentialRent() {
		return $this->m_fltPotentialRent;
	}

	public function sqlPotentialRent() {
		return ( true == isset( $this->m_fltPotentialRent ) ) ? ( string ) $this->m_fltPotentialRent : '0';
	}

	public function setCurrentVacancyLoss( $fltCurrentVacancyLoss ) {
		$this->set( 'm_fltCurrentVacancyLoss', CStrings::strToFloatDef( $fltCurrentVacancyLoss, NULL, false, 2 ) );
	}

	public function getCurrentVacancyLoss() {
		return $this->m_fltCurrentVacancyLoss;
	}

	public function sqlCurrentVacancyLoss() {
		return ( true == isset( $this->m_fltCurrentVacancyLoss ) ) ? ( string ) $this->m_fltCurrentVacancyLoss : '0';
	}

	public function setGainPriorVacancyAdjustment( $fltGainPriorVacancyAdjustment ) {
		$this->set( 'm_fltGainPriorVacancyAdjustment', CStrings::strToFloatDef( $fltGainPriorVacancyAdjustment, NULL, false, 2 ) );
	}

	public function getGainPriorVacancyAdjustment() {
		return $this->m_fltGainPriorVacancyAdjustment;
	}

	public function sqlGainPriorVacancyAdjustment() {
		return ( true == isset( $this->m_fltGainPriorVacancyAdjustment ) ) ? ( string ) $this->m_fltGainPriorVacancyAdjustment : '0';
	}

	public function setLossPriorVacancyAdjustment( $fltLossPriorVacancyAdjustment ) {
		$this->set( 'm_fltLossPriorVacancyAdjustment', CStrings::strToFloatDef( $fltLossPriorVacancyAdjustment, NULL, false, 2 ) );
	}

	public function getLossPriorVacancyAdjustment() {
		return $this->m_fltLossPriorVacancyAdjustment;
	}

	public function sqlLossPriorVacancyAdjustment() {
		return ( true == isset( $this->m_fltLossPriorVacancyAdjustment ) ) ? ( string ) $this->m_fltLossPriorVacancyAdjustment : '0';
	}

	public function setTotalPriorVacancyAdjustment( $fltTotalPriorVacancyAdjustment ) {
		$this->set( 'm_fltTotalPriorVacancyAdjustment', CStrings::strToFloatDef( $fltTotalPriorVacancyAdjustment, NULL, false, 2 ) );
	}

	public function getTotalPriorVacancyAdjustment() {
		return $this->m_fltTotalPriorVacancyAdjustment;
	}

	public function sqlTotalPriorVacancyAdjustment() {
		return ( true == isset( $this->m_fltTotalPriorVacancyAdjustment ) ) ? ( string ) $this->m_fltTotalPriorVacancyAdjustment : '0';
	}

	public function setTotalVacancyLoss( $fltTotalVacancyLoss ) {
		$this->set( 'm_fltTotalVacancyLoss', CStrings::strToFloatDef( $fltTotalVacancyLoss, NULL, false, 2 ) );
	}

	public function getTotalVacancyLoss() {
		return $this->m_fltTotalVacancyLoss;
	}

	public function sqlTotalVacancyLoss() {
		return ( true == isset( $this->m_fltTotalVacancyLoss ) ) ? ( string ) $this->m_fltTotalVacancyLoss : '0';
	}

	public function setConsecutiveVacancyLoss( $fltConsecutiveVacancyLoss ) {
		$this->set( 'm_fltConsecutiveVacancyLoss', CStrings::strToFloatDef( $fltConsecutiveVacancyLoss, NULL, false, 2 ) );
	}

	public function getConsecutiveVacancyLoss() {
		return $this->m_fltConsecutiveVacancyLoss;
	}

	public function sqlConsecutiveVacancyLoss() {
		return ( true == isset( $this->m_fltConsecutiveVacancyLoss ) ) ? ( string ) $this->m_fltConsecutiveVacancyLoss : '0';
	}

	public function setScheduledCurrentRentSetup( $fltScheduledCurrentRentSetup ) {
		$this->set( 'm_fltScheduledCurrentRentSetup', CStrings::strToFloatDef( $fltScheduledCurrentRentSetup, NULL, false, 2 ) );
	}

	public function getScheduledCurrentRentSetup() {
		return $this->m_fltScheduledCurrentRentSetup;
	}

	public function sqlScheduledCurrentRentSetup() {
		return ( true == isset( $this->m_fltScheduledCurrentRentSetup ) ) ? ( string ) $this->m_fltScheduledCurrentRentSetup : '0';
	}

	public function setScheduledCurrentMiscSetup( $fltScheduledCurrentMiscSetup ) {
		$this->set( 'm_fltScheduledCurrentMiscSetup', CStrings::strToFloatDef( $fltScheduledCurrentMiscSetup, NULL, false, 2 ) );
	}

	public function getScheduledCurrentMiscSetup() {
		return $this->m_fltScheduledCurrentMiscSetup;
	}

	public function sqlScheduledCurrentMiscSetup() {
		return ( true == isset( $this->m_fltScheduledCurrentMiscSetup ) ) ? ( string ) $this->m_fltScheduledCurrentMiscSetup : '0';
	}

	public function setScheduledCurrentProratedRentSetup( $fltScheduledCurrentProratedRentSetup ) {
		$this->set( 'm_fltScheduledCurrentProratedRentSetup', CStrings::strToFloatDef( $fltScheduledCurrentProratedRentSetup, NULL, false, 2 ) );
	}

	public function getScheduledCurrentProratedRentSetup() {
		return $this->m_fltScheduledCurrentProratedRentSetup;
	}

	public function sqlScheduledCurrentProratedRentSetup() {
		return ( true == isset( $this->m_fltScheduledCurrentProratedRentSetup ) ) ? ( string ) $this->m_fltScheduledCurrentProratedRentSetup : '0';
	}

	public function setScheduledCurrentProratedMiscSetup( $fltScheduledCurrentProratedMiscSetup ) {
		$this->set( 'm_fltScheduledCurrentProratedMiscSetup', CStrings::strToFloatDef( $fltScheduledCurrentProratedMiscSetup, NULL, false, 2 ) );
	}

	public function getScheduledCurrentProratedMiscSetup() {
		return $this->m_fltScheduledCurrentProratedMiscSetup;
	}

	public function sqlScheduledCurrentProratedMiscSetup() {
		return ( true == isset( $this->m_fltScheduledCurrentProratedMiscSetup ) ) ? ( string ) $this->m_fltScheduledCurrentProratedMiscSetup : '0';
	}

	public function setScheduledFutureRentSetup( $fltScheduledFutureRentSetup ) {
		$this->set( 'm_fltScheduledFutureRentSetup', CStrings::strToFloatDef( $fltScheduledFutureRentSetup, NULL, false, 2 ) );
	}

	public function getScheduledFutureRentSetup() {
		return $this->m_fltScheduledFutureRentSetup;
	}

	public function sqlScheduledFutureRentSetup() {
		return ( true == isset( $this->m_fltScheduledFutureRentSetup ) ) ? ( string ) $this->m_fltScheduledFutureRentSetup : '0';
	}

	public function setScheduledFutureMiscSetup( $fltScheduledFutureMiscSetup ) {
		$this->set( 'm_fltScheduledFutureMiscSetup', CStrings::strToFloatDef( $fltScheduledFutureMiscSetup, NULL, false, 2 ) );
	}

	public function getScheduledFutureMiscSetup() {
		return $this->m_fltScheduledFutureMiscSetup;
	}

	public function sqlScheduledFutureMiscSetup() {
		return ( true == isset( $this->m_fltScheduledFutureMiscSetup ) ) ? ( string ) $this->m_fltScheduledFutureMiscSetup : '0';
	}

	public function setScheduledFutureProratedRentSetup( $fltScheduledFutureProratedRentSetup ) {
		$this->set( 'm_fltScheduledFutureProratedRentSetup', CStrings::strToFloatDef( $fltScheduledFutureProratedRentSetup, NULL, false, 2 ) );
	}

	public function getScheduledFutureProratedRentSetup() {
		return $this->m_fltScheduledFutureProratedRentSetup;
	}

	public function sqlScheduledFutureProratedRentSetup() {
		return ( true == isset( $this->m_fltScheduledFutureProratedRentSetup ) ) ? ( string ) $this->m_fltScheduledFutureProratedRentSetup : '0';
	}

	public function setScheduledFutureProratedMiscSetup( $fltScheduledFutureProratedMiscSetup ) {
		$this->set( 'm_fltScheduledFutureProratedMiscSetup', CStrings::strToFloatDef( $fltScheduledFutureProratedMiscSetup, NULL, false, 2 ) );
	}

	public function getScheduledFutureProratedMiscSetup() {
		return $this->m_fltScheduledFutureProratedMiscSetup;
	}

	public function sqlScheduledFutureProratedMiscSetup() {
		return ( true == isset( $this->m_fltScheduledFutureProratedMiscSetup ) ) ? ( string ) $this->m_fltScheduledFutureProratedMiscSetup : '0';
	}

	public function setAcceleratedRent( $fltAcceleratedRent ) {
		$this->set( 'm_fltAcceleratedRent', CStrings::strToFloatDef( $fltAcceleratedRent, NULL, false, 2 ) );
	}

	public function getAcceleratedRent() {
		return $this->m_fltAcceleratedRent;
	}

	public function sqlAcceleratedRent() {
		return ( true == isset( $this->m_fltAcceleratedRent ) ) ? ( string ) $this->m_fltAcceleratedRent : '0';
	}

	public function setApplicationFees( $fltApplicationFees ) {
		$this->set( 'm_fltApplicationFees', CStrings::strToFloatDef( $fltApplicationFees, NULL, false, 2 ) );
	}

	public function getApplicationFees() {
		return $this->m_fltApplicationFees;
	}

	public function sqlApplicationFees() {
		return ( true == isset( $this->m_fltApplicationFees ) ) ? ( string ) $this->m_fltApplicationFees : '0';
	}

	public function setEarlyTerminationFees( $fltEarlyTerminationFees ) {
		$this->set( 'm_fltEarlyTerminationFees', CStrings::strToFloatDef( $fltEarlyTerminationFees, NULL, false, 2 ) );
	}

	public function getEarlyTerminationFees() {
		return $this->m_fltEarlyTerminationFees;
	}

	public function sqlEarlyTerminationFees() {
		return ( true == isset( $this->m_fltEarlyTerminationFees ) ) ? ( string ) $this->m_fltEarlyTerminationFees : '0';
	}

	public function setInsufficientNoticeFees( $fltInsufficientNoticeFees ) {
		$this->set( 'm_fltInsufficientNoticeFees', CStrings::strToFloatDef( $fltInsufficientNoticeFees, NULL, false, 2 ) );
	}

	public function getInsufficientNoticeFees() {
		return $this->m_fltInsufficientNoticeFees;
	}

	public function sqlInsufficientNoticeFees() {
		return ( true == isset( $this->m_fltInsufficientNoticeFees ) ) ? ( string ) $this->m_fltInsufficientNoticeFees : '0';
	}

	public function setMonthToMonthFees( $fltMonthToMonthFees ) {
		$this->set( 'm_fltMonthToMonthFees', CStrings::strToFloatDef( $fltMonthToMonthFees, NULL, false, 2 ) );
	}

	public function getMonthToMonthFees() {
		return $this->m_fltMonthToMonthFees;
	}

	public function sqlMonthToMonthFees() {
		return ( true == isset( $this->m_fltMonthToMonthFees ) ) ? ( string ) $this->m_fltMonthToMonthFees : '0';
	}

	public function setReturnItemFees( $fltReturnItemFees ) {
		$this->set( 'm_fltReturnItemFees', CStrings::strToFloatDef( $fltReturnItemFees, NULL, false, 2 ) );
	}

	public function getReturnItemFees() {
		return $this->m_fltReturnItemFees;
	}

	public function sqlReturnItemFees() {
		return ( true == isset( $this->m_fltReturnItemFees ) ) ? ( string ) $this->m_fltReturnItemFees : '0';
	}

	public function setTransferFees( $fltTransferFees ) {
		$this->set( 'm_fltTransferFees', CStrings::strToFloatDef( $fltTransferFees, NULL, false, 2 ) );
	}

	public function getTransferFees() {
		return $this->m_fltTransferFees;
	}

	public function sqlTransferFees() {
		return ( true == isset( $this->m_fltTransferFees ) ) ? ( string ) $this->m_fltTransferFees : '0';
	}

	public function setWriteOffs( $fltWriteOffs ) {
		$this->set( 'm_fltWriteOffs', CStrings::strToFloatDef( $fltWriteOffs, NULL, false, 2 ) );
	}

	public function getWriteOffs() {
		return $this->m_fltWriteOffs;
	}

	public function sqlWriteOffs() {
		return ( true == isset( $this->m_fltWriteOffs ) ) ? ( string ) $this->m_fltWriteOffs : '0';
	}

	public function setTaxFees( $fltTaxFees ) {
		$this->set( 'm_fltTaxFees', CStrings::strToFloatDef( $fltTaxFees, NULL, false, 2 ) );
	}

	public function getTaxFees() {
		return $this->m_fltTaxFees;
	}

	public function sqlTaxFees() {
		return ( true == isset( $this->m_fltTaxFees ) ) ? ( string ) $this->m_fltTaxFees : '0';
	}

	public function setLateFees( $fltLateFees ) {
		$this->set( 'm_fltLateFees', CStrings::strToFloatDef( $fltLateFees, NULL, false, 2 ) );
	}

	public function getLateFees() {
		return $this->m_fltLateFees;
	}

	public function sqlLateFees() {
		return ( true == isset( $this->m_fltLateFees ) ) ? ( string ) $this->m_fltLateFees : '0';
	}

	public function setWorkOrderFees( $fltWorkOrderFees ) {
		$this->set( 'm_fltWorkOrderFees', CStrings::strToFloatDef( $fltWorkOrderFees, NULL, false, 2 ) );
	}

	public function getWorkOrderFees() {
		return $this->m_fltWorkOrderFees;
	}

	public function sqlWorkOrderFees() {
		return ( true == isset( $this->m_fltWorkOrderFees ) ) ? ( string ) $this->m_fltWorkOrderFees : '0';
	}

	public function setDepositInterestCredits( $fltDepositInterestCredits ) {
		$this->set( 'm_fltDepositInterestCredits', CStrings::strToFloatDef( $fltDepositInterestCredits, NULL, false, 2 ) );
	}

	public function getDepositInterestCredits() {
		return $this->m_fltDepositInterestCredits;
	}

	public function sqlDepositInterestCredits() {
		return ( true == isset( $this->m_fltDepositInterestCredits ) ) ? ( string ) $this->m_fltDepositInterestCredits : '0';
	}

	public function setUtilityFees( $fltUtilityFees ) {
		$this->set( 'm_fltUtilityFees', CStrings::strToFloatDef( $fltUtilityFees, NULL, false, 2 ) );
	}

	public function getUtilityFees() {
		return $this->m_fltUtilityFees;
	}

	public function sqlUtilityFees() {
		return ( true == isset( $this->m_fltUtilityFees ) ) ? ( string ) $this->m_fltUtilityFees : '0';
	}

	public function setBeginningPrePayments( $fltBeginningPrePayments ) {
		$this->set( 'm_fltBeginningPrePayments', CStrings::strToFloatDef( $fltBeginningPrePayments, NULL, false, 2 ) );
	}

	public function getBeginningPrePayments() {
		return $this->m_fltBeginningPrePayments;
	}

	public function sqlBeginningPrePayments() {
		return ( true == isset( $this->m_fltBeginningPrePayments ) ) ? ( string ) $this->m_fltBeginningPrePayments : '0';
	}

	public function setBeginningRentReceivable( $fltBeginningRentReceivable ) {
		$this->set( 'm_fltBeginningRentReceivable', CStrings::strToFloatDef( $fltBeginningRentReceivable, NULL, false, 2 ) );
	}

	public function getBeginningRentReceivable() {
		return $this->m_fltBeginningRentReceivable;
	}

	public function sqlBeginningRentReceivable() {
		return ( true == isset( $this->m_fltBeginningRentReceivable ) ) ? ( string ) $this->m_fltBeginningRentReceivable : '0';
	}

	public function setBeginningReceivables( $fltBeginningReceivables ) {
		$this->set( 'm_fltBeginningReceivables', CStrings::strToFloatDef( $fltBeginningReceivables, NULL, false, 2 ) );
	}

	public function getBeginningReceivables() {
		return $this->m_fltBeginningReceivables;
	}

	public function sqlBeginningReceivables() {
		return ( true == isset( $this->m_fltBeginningReceivables ) ) ? ( string ) $this->m_fltBeginningReceivables : '0';
	}

	public function setBeginningDepositBalance( $fltBeginningDepositBalance ) {
		$this->set( 'm_fltBeginningDepositBalance', CStrings::strToFloatDef( $fltBeginningDepositBalance, NULL, false, 2 ) );
	}

	public function getBeginningDepositBalance() {
		return $this->m_fltBeginningDepositBalance;
	}

	public function sqlBeginningDepositBalance() {
		return ( true == isset( $this->m_fltBeginningDepositBalance ) ) ? ( string ) $this->m_fltBeginningDepositBalance : '0';
	}

	public function setBeginningDepositHeld( $fltBeginningDepositHeld ) {
		$this->set( 'm_fltBeginningDepositHeld', CStrings::strToFloatDef( $fltBeginningDepositHeld, NULL, false, 2 ) );
	}

	public function getBeginningDepositHeld() {
		return $this->m_fltBeginningDepositHeld;
	}

	public function sqlBeginningDepositHeld() {
		return ( true == isset( $this->m_fltBeginningDepositHeld ) ) ? ( string ) $this->m_fltBeginningDepositHeld : '0';
	}

	public function setPayments( $fltPayments ) {
		$this->set( 'm_fltPayments', CStrings::strToFloatDef( $fltPayments, NULL, false, 2 ) );
	}

	public function getPayments() {
		return $this->m_fltPayments;
	}

	public function sqlPayments() {
		return ( true == isset( $this->m_fltPayments ) ) ? ( string ) $this->m_fltPayments : '0';
	}

	public function setPaymentsInKind( $fltPaymentsInKind ) {
		$this->set( 'm_fltPaymentsInKind', CStrings::strToFloatDef( $fltPaymentsInKind, NULL, false, 2 ) );
	}

	public function getPaymentsInKind() {
		return $this->m_fltPaymentsInKind;
	}

	public function sqlPaymentsInKind() {
		return ( true == isset( $this->m_fltPaymentsInKind ) ) ? ( string ) $this->m_fltPaymentsInKind : '0';
	}

	public function setRentWrittenOff( $fltRentWrittenOff ) {
		$this->set( 'm_fltRentWrittenOff', CStrings::strToFloatDef( $fltRentWrittenOff, NULL, false, 2 ) );
	}

	public function getRentWrittenOff() {
		return $this->m_fltRentWrittenOff;
	}

	public function sqlRentWrittenOff() {
		return ( true == isset( $this->m_fltRentWrittenOff ) ) ? ( string ) $this->m_fltRentWrittenOff : '0';
	}

	public function setRentOutOfPeriod( $fltRentOutOfPeriod ) {
		$this->set( 'm_fltRentOutOfPeriod', CStrings::strToFloatDef( $fltRentOutOfPeriod, NULL, false, 2 ) );
	}

	public function getRentOutOfPeriod() {
		return $this->m_fltRentOutOfPeriod;
	}

	public function sqlRentOutOfPeriod() {
		return ( true == isset( $this->m_fltRentOutOfPeriod ) ) ? ( string ) $this->m_fltRentOutOfPeriod : '0';
	}

	public function setRentScheduled( $fltRentScheduled ) {
		$this->set( 'm_fltRentScheduled', CStrings::strToFloatDef( $fltRentScheduled, NULL, false, 2 ) );
	}

	public function getRentScheduled() {
		return $this->m_fltRentScheduled;
	}

	public function sqlRentScheduled() {
		return ( true == isset( $this->m_fltRentScheduled ) ) ? ( string ) $this->m_fltRentScheduled : '0';
	}

	public function setRentAdjustments( $fltRentAdjustments ) {
		$this->set( 'm_fltRentAdjustments', CStrings::strToFloatDef( $fltRentAdjustments, NULL, false, 2 ) );
	}

	public function getRentAdjustments() {
		return $this->m_fltRentAdjustments;
	}

	public function sqlRentAdjustments() {
		return ( true == isset( $this->m_fltRentAdjustments ) ) ? ( string ) $this->m_fltRentAdjustments : '0';
	}

	public function setRentBase( $fltRentBase ) {
		$this->set( 'm_fltRentBase', CStrings::strToFloatDef( $fltRentBase, NULL, false, 2 ) );
	}

	public function getRentBase() {
		return $this->m_fltRentBase;
	}

	public function sqlRentBase() {
		return ( true == isset( $this->m_fltRentBase ) ) ? ( string ) $this->m_fltRentBase : '0';
	}

	public function setRentAmenities( $fltRentAmenities ) {
		$this->set( 'm_fltRentAmenities', CStrings::strToFloatDef( $fltRentAmenities, NULL, false, 2 ) );
	}

	public function getRentAmenities() {
		return $this->m_fltRentAmenities;
	}

	public function sqlRentAmenities() {
		return ( true == isset( $this->m_fltRentAmenities ) ) ? ( string ) $this->m_fltRentAmenities : '0';
	}

	public function setRentPets( $fltRentPets ) {
		$this->set( 'm_fltRentPets', CStrings::strToFloatDef( $fltRentPets, NULL, false, 2 ) );
	}

	public function getRentPets() {
		return $this->m_fltRentPets;
	}

	public function sqlRentPets() {
		return ( true == isset( $this->m_fltRentPets ) ) ? ( string ) $this->m_fltRentPets : '0';
	}

	public function setRentAddOns( $fltRentAddOns ) {
		$this->set( 'm_fltRentAddOns', CStrings::strToFloatDef( $fltRentAddOns, NULL, false, 2 ) );
	}

	public function getRentAddOns() {
		return $this->m_fltRentAddOns;
	}

	public function sqlRentAddOns() {
		return ( true == isset( $this->m_fltRentAddOns ) ) ? ( string ) $this->m_fltRentAddOns : '0';
	}

	public function setRentRisk( $fltRentRisk ) {
		$this->set( 'm_fltRentRisk', CStrings::strToFloatDef( $fltRentRisk, NULL, false, 2 ) );
	}

	public function getRentRisk() {
		return $this->m_fltRentRisk;
	}

	public function sqlRentRisk() {
		return ( true == isset( $this->m_fltRentRisk ) ) ? ( string ) $this->m_fltRentRisk : '0';
	}

	public function setRentSpecials( $fltRentSpecials ) {
		$this->set( 'm_fltRentSpecials', CStrings::strToFloatDef( $fltRentSpecials, NULL, false, 2 ) );
	}

	public function getRentSpecials() {
		return $this->m_fltRentSpecials;
	}

	public function sqlRentSpecials() {
		return ( true == isset( $this->m_fltRentSpecials ) ) ? ( string ) $this->m_fltRentSpecials : '0';
	}

	public function setRentMaintenance( $fltRentMaintenance ) {
		$this->set( 'm_fltRentMaintenance', CStrings::strToFloatDef( $fltRentMaintenance, NULL, false, 2 ) );
	}

	public function getRentMaintenance() {
		return $this->m_fltRentMaintenance;
	}

	public function sqlRentMaintenance() {
		return ( true == isset( $this->m_fltRentMaintenance ) ) ? ( string ) $this->m_fltRentMaintenance : '0';
	}

	public function setRent( $fltRent ) {
		$this->set( 'm_fltRent', CStrings::strToFloatDef( $fltRent, NULL, false, 2 ) );
	}

	public function getRent() {
		return $this->m_fltRent;
	}

	public function sqlRent() {
		return ( true == isset( $this->m_fltRent ) ) ? ( string ) $this->m_fltRent : '0';
	}

	public function setOtherIncomeBase( $fltOtherIncomeBase ) {
		$this->set( 'm_fltOtherIncomeBase', CStrings::strToFloatDef( $fltOtherIncomeBase, NULL, false, 2 ) );
	}

	public function getOtherIncomeBase() {
		return $this->m_fltOtherIncomeBase;
	}

	public function sqlOtherIncomeBase() {
		return ( true == isset( $this->m_fltOtherIncomeBase ) ) ? ( string ) $this->m_fltOtherIncomeBase : '0';
	}

	public function setOtherIncomeAmenities( $fltOtherIncomeAmenities ) {
		$this->set( 'm_fltOtherIncomeAmenities', CStrings::strToFloatDef( $fltOtherIncomeAmenities, NULL, false, 2 ) );
	}

	public function getOtherIncomeAmenities() {
		return $this->m_fltOtherIncomeAmenities;
	}

	public function sqlOtherIncomeAmenities() {
		return ( true == isset( $this->m_fltOtherIncomeAmenities ) ) ? ( string ) $this->m_fltOtherIncomeAmenities : '0';
	}

	public function setOtherIncomePets( $fltOtherIncomePets ) {
		$this->set( 'm_fltOtherIncomePets', CStrings::strToFloatDef( $fltOtherIncomePets, NULL, false, 2 ) );
	}

	public function getOtherIncomePets() {
		return $this->m_fltOtherIncomePets;
	}

	public function sqlOtherIncomePets() {
		return ( true == isset( $this->m_fltOtherIncomePets ) ) ? ( string ) $this->m_fltOtherIncomePets : '0';
	}

	public function setOtherIncomeAddOns( $fltOtherIncomeAddOns ) {
		$this->set( 'm_fltOtherIncomeAddOns', CStrings::strToFloatDef( $fltOtherIncomeAddOns, NULL, false, 2 ) );
	}

	public function getOtherIncomeAddOns() {
		return $this->m_fltOtherIncomeAddOns;
	}

	public function sqlOtherIncomeAddOns() {
		return ( true == isset( $this->m_fltOtherIncomeAddOns ) ) ? ( string ) $this->m_fltOtherIncomeAddOns : '0';
	}

	public function setOtherIncomeRisk( $fltOtherIncomeRisk ) {
		$this->set( 'm_fltOtherIncomeRisk', CStrings::strToFloatDef( $fltOtherIncomeRisk, NULL, false, 2 ) );
	}

	public function getOtherIncomeRisk() {
		return $this->m_fltOtherIncomeRisk;
	}

	public function sqlOtherIncomeRisk() {
		return ( true == isset( $this->m_fltOtherIncomeRisk ) ) ? ( string ) $this->m_fltOtherIncomeRisk : '0';
	}

	public function setOtherIncomeSpecials( $fltOtherIncomeSpecials ) {
		$this->set( 'm_fltOtherIncomeSpecials', CStrings::strToFloatDef( $fltOtherIncomeSpecials, NULL, false, 2 ) );
	}

	public function getOtherIncomeSpecials() {
		return $this->m_fltOtherIncomeSpecials;
	}

	public function sqlOtherIncomeSpecials() {
		return ( true == isset( $this->m_fltOtherIncomeSpecials ) ) ? ( string ) $this->m_fltOtherIncomeSpecials : '0';
	}

	public function setOtherIncomeMaintenance( $fltOtherIncomeMaintenance ) {
		$this->set( 'm_fltOtherIncomeMaintenance', CStrings::strToFloatDef( $fltOtherIncomeMaintenance, NULL, false, 2 ) );
	}

	public function getOtherIncomeMaintenance() {
		return $this->m_fltOtherIncomeMaintenance;
	}

	public function sqlOtherIncomeMaintenance() {
		return ( true == isset( $this->m_fltOtherIncomeMaintenance ) ) ? ( string ) $this->m_fltOtherIncomeMaintenance : '0';
	}

	public function setOtherIncome( $fltOtherIncome ) {
		$this->set( 'm_fltOtherIncome', CStrings::strToFloatDef( $fltOtherIncome, NULL, false, 2 ) );
	}

	public function getOtherIncome() {
		return $this->m_fltOtherIncome;
	}

	public function sqlOtherIncome() {
		return ( true == isset( $this->m_fltOtherIncome ) ) ? ( string ) $this->m_fltOtherIncome : '0';
	}

	public function setTotalNewIncomeReceivable( $fltTotalNewIncomeReceivable ) {
		$this->set( 'm_fltTotalNewIncomeReceivable', CStrings::strToFloatDef( $fltTotalNewIncomeReceivable, NULL, false, 2 ) );
	}

	public function getTotalNewIncomeReceivable() {
		return $this->m_fltTotalNewIncomeReceivable;
	}

	public function sqlTotalNewIncomeReceivable() {
		return ( true == isset( $this->m_fltTotalNewIncomeReceivable ) ) ? ( string ) $this->m_fltTotalNewIncomeReceivable : '0';
	}

	public function setExpenses( $fltExpenses ) {
		$this->set( 'm_fltExpenses', CStrings::strToFloatDef( $fltExpenses, NULL, false, 2 ) );
	}

	public function getExpenses() {
		return $this->m_fltExpenses;
	}

	public function sqlExpenses() {
		return ( true == isset( $this->m_fltExpenses ) ) ? ( string ) $this->m_fltExpenses : '0';
	}

	public function setAssets( $fltAssets ) {
		$this->set( 'm_fltAssets', CStrings::strToFloatDef( $fltAssets, NULL, false, 2 ) );
	}

	public function getAssets() {
		return $this->m_fltAssets;
	}

	public function sqlAssets() {
		return ( true == isset( $this->m_fltAssets ) ) ? ( string ) $this->m_fltAssets : '0';
	}

	public function setEquity( $fltEquity ) {
		$this->set( 'm_fltEquity', CStrings::strToFloatDef( $fltEquity, NULL, false, 2 ) );
	}

	public function getEquity() {
		return $this->m_fltEquity;
	}

	public function sqlEquity() {
		return ( true == isset( $this->m_fltEquity ) ) ? ( string ) $this->m_fltEquity : '0';
	}

	public function setTotalOtherCharges( $fltTotalOtherCharges ) {
		$this->set( 'm_fltTotalOtherCharges', CStrings::strToFloatDef( $fltTotalOtherCharges, NULL, false, 2 ) );
	}

	public function getTotalOtherCharges() {
		return $this->m_fltTotalOtherCharges;
	}

	public function sqlTotalOtherCharges() {
		return ( true == isset( $this->m_fltTotalOtherCharges ) ) ? ( string ) $this->m_fltTotalOtherCharges : '0';
	}

	public function setDepositAllocations( $fltDepositAllocations ) {
		$this->set( 'm_fltDepositAllocations', CStrings::strToFloatDef( $fltDepositAllocations, NULL, false, 2 ) );
	}

	public function getDepositAllocations() {
		return $this->m_fltDepositAllocations;
	}

	public function sqlDepositAllocations() {
		return ( true == isset( $this->m_fltDepositAllocations ) ) ? ( string ) $this->m_fltDepositAllocations : '0';
	}

	public function setDepositCreditAllocations( $fltDepositCreditAllocations ) {
		$this->set( 'm_fltDepositCreditAllocations', CStrings::strToFloatDef( $fltDepositCreditAllocations, NULL, false, 2 ) );
	}

	public function getDepositCreditAllocations() {
		return $this->m_fltDepositCreditAllocations;
	}

	public function sqlDepositCreditAllocations() {
		return ( true == isset( $this->m_fltDepositCreditAllocations ) ) ? ( string ) $this->m_fltDepositCreditAllocations : '0';
	}

	public function setRefunds( $fltRefunds ) {
		$this->set( 'm_fltRefunds', CStrings::strToFloatDef( $fltRefunds, NULL, false, 2 ) );
	}

	public function getRefunds() {
		return $this->m_fltRefunds;
	}

	public function sqlRefunds() {
		return ( true == isset( $this->m_fltRefunds ) ) ? ( string ) $this->m_fltRefunds : '0';
	}

	public function setOtherLiabilities( $fltOtherLiabilities ) {
		$this->set( 'm_fltOtherLiabilities', CStrings::strToFloatDef( $fltOtherLiabilities, NULL, false, 2 ) );
	}

	public function getOtherLiabilities() {
		return $this->m_fltOtherLiabilities;
	}

	public function sqlOtherLiabilities() {
		return ( true == isset( $this->m_fltOtherLiabilities ) ) ? ( string ) $this->m_fltOtherLiabilities : '0';
	}

	public function setTotalNewNonIncomeReceivable( $fltTotalNewNonIncomeReceivable ) {
		$this->set( 'm_fltTotalNewNonIncomeReceivable', CStrings::strToFloatDef( $fltTotalNewNonIncomeReceivable, NULL, false, 2 ) );
	}

	public function getTotalNewNonIncomeReceivable() {
		return $this->m_fltTotalNewNonIncomeReceivable;
	}

	public function sqlTotalNewNonIncomeReceivable() {
		return ( true == isset( $this->m_fltTotalNewNonIncomeReceivable ) ) ? ( string ) $this->m_fltTotalNewNonIncomeReceivable : '0';
	}

	public function setTotalNewReceivables( $fltTotalNewReceivables ) {
		$this->set( 'm_fltTotalNewReceivables', CStrings::strToFloatDef( $fltTotalNewReceivables, NULL, false, 2 ) );
	}

	public function getTotalNewReceivables() {
		return $this->m_fltTotalNewReceivables;
	}

	public function sqlTotalNewReceivables() {
		return ( true == isset( $this->m_fltTotalNewReceivables ) ) ? ( string ) $this->m_fltTotalNewReceivables : '0';
	}

	public function setDepositsBase( $fltDepositsBase ) {
		$this->set( 'm_fltDepositsBase', CStrings::strToFloatDef( $fltDepositsBase, NULL, false, 2 ) );
	}

	public function getDepositsBase() {
		return $this->m_fltDepositsBase;
	}

	public function sqlDepositsBase() {
		return ( true == isset( $this->m_fltDepositsBase ) ) ? ( string ) $this->m_fltDepositsBase : '0';
	}

	public function setDepositsAmenities( $fltDepositsAmenities ) {
		$this->set( 'm_fltDepositsAmenities', CStrings::strToFloatDef( $fltDepositsAmenities, NULL, false, 2 ) );
	}

	public function getDepositsAmenities() {
		return $this->m_fltDepositsAmenities;
	}

	public function sqlDepositsAmenities() {
		return ( true == isset( $this->m_fltDepositsAmenities ) ) ? ( string ) $this->m_fltDepositsAmenities : '0';
	}

	public function setDepositsPets( $fltDepositsPets ) {
		$this->set( 'm_fltDepositsPets', CStrings::strToFloatDef( $fltDepositsPets, NULL, false, 2 ) );
	}

	public function getDepositsPets() {
		return $this->m_fltDepositsPets;
	}

	public function sqlDepositsPets() {
		return ( true == isset( $this->m_fltDepositsPets ) ) ? ( string ) $this->m_fltDepositsPets : '0';
	}

	public function setDepositsAddOns( $fltDepositsAddOns ) {
		$this->set( 'm_fltDepositsAddOns', CStrings::strToFloatDef( $fltDepositsAddOns, NULL, false, 2 ) );
	}

	public function getDepositsAddOns() {
		return $this->m_fltDepositsAddOns;
	}

	public function sqlDepositsAddOns() {
		return ( true == isset( $this->m_fltDepositsAddOns ) ) ? ( string ) $this->m_fltDepositsAddOns : '0';
	}

	public function setDepositsRisk( $fltDepositsRisk ) {
		$this->set( 'm_fltDepositsRisk', CStrings::strToFloatDef( $fltDepositsRisk, NULL, false, 2 ) );
	}

	public function getDepositsRisk() {
		return $this->m_fltDepositsRisk;
	}

	public function sqlDepositsRisk() {
		return ( true == isset( $this->m_fltDepositsRisk ) ) ? ( string ) $this->m_fltDepositsRisk : '0';
	}

	public function setDepositsSpecials( $fltDepositsSpecials ) {
		$this->set( 'm_fltDepositsSpecials', CStrings::strToFloatDef( $fltDepositsSpecials, NULL, false, 2 ) );
	}

	public function getDepositsSpecials() {
		return $this->m_fltDepositsSpecials;
	}

	public function sqlDepositsSpecials() {
		return ( true == isset( $this->m_fltDepositsSpecials ) ) ? ( string ) $this->m_fltDepositsSpecials : '0';
	}

	public function setDepositsMaintenance( $fltDepositsMaintenance ) {
		$this->set( 'm_fltDepositsMaintenance', CStrings::strToFloatDef( $fltDepositsMaintenance, NULL, false, 2 ) );
	}

	public function getDepositsMaintenance() {
		return $this->m_fltDepositsMaintenance;
	}

	public function sqlDepositsMaintenance() {
		return ( true == isset( $this->m_fltDepositsMaintenance ) ) ? ( string ) $this->m_fltDepositsMaintenance : '0';
	}

	public function setDeposits( $fltDeposits ) {
		$this->set( 'm_fltDeposits', CStrings::strToFloatDef( $fltDeposits, NULL, false, 2 ) );
	}

	public function getDeposits() {
		return $this->m_fltDeposits;
	}

	public function sqlDeposits() {
		return ( true == isset( $this->m_fltDeposits ) ) ? ( string ) $this->m_fltDeposits : '0';
	}

	public function setDepositCredits( $fltDepositCredits ) {
		$this->set( 'm_fltDepositCredits', CStrings::strToFloatDef( $fltDepositCredits, NULL, false, 2 ) );
	}

	public function getDepositCredits() {
		return $this->m_fltDepositCredits;
	}

	public function sqlDepositCredits() {
		return ( true == isset( $this->m_fltDepositCredits ) ) ? ( string ) $this->m_fltDepositCredits : '0';
	}

	public function setPaymentsAllocatedToRent( $fltPaymentsAllocatedToRent ) {
		$this->set( 'm_fltPaymentsAllocatedToRent', CStrings::strToFloatDef( $fltPaymentsAllocatedToRent, NULL, false, 2 ) );
	}

	public function getPaymentsAllocatedToRent() {
		return $this->m_fltPaymentsAllocatedToRent;
	}

	public function sqlPaymentsAllocatedToRent() {
		return ( true == isset( $this->m_fltPaymentsAllocatedToRent ) ) ? ( string ) $this->m_fltPaymentsAllocatedToRent : '0';
	}

	public function setPaymentsAllocatedToOtherIncome( $fltPaymentsAllocatedToOtherIncome ) {
		$this->set( 'm_fltPaymentsAllocatedToOtherIncome', CStrings::strToFloatDef( $fltPaymentsAllocatedToOtherIncome, NULL, false, 2 ) );
	}

	public function getPaymentsAllocatedToOtherIncome() {
		return $this->m_fltPaymentsAllocatedToOtherIncome;
	}

	public function sqlPaymentsAllocatedToOtherIncome() {
		return ( true == isset( $this->m_fltPaymentsAllocatedToOtherIncome ) ) ? ( string ) $this->m_fltPaymentsAllocatedToOtherIncome : '0';
	}

	public function setPaymentsAllocatedToExpenses( $fltPaymentsAllocatedToExpenses ) {
		$this->set( 'm_fltPaymentsAllocatedToExpenses', CStrings::strToFloatDef( $fltPaymentsAllocatedToExpenses, NULL, false, 2 ) );
	}

	public function getPaymentsAllocatedToExpenses() {
		return $this->m_fltPaymentsAllocatedToExpenses;
	}

	public function sqlPaymentsAllocatedToExpenses() {
		return ( true == isset( $this->m_fltPaymentsAllocatedToExpenses ) ) ? ( string ) $this->m_fltPaymentsAllocatedToExpenses : '0';
	}

	public function setPaymentsAllocatedToAssets( $fltPaymentsAllocatedToAssets ) {
		$this->set( 'm_fltPaymentsAllocatedToAssets', CStrings::strToFloatDef( $fltPaymentsAllocatedToAssets, NULL, false, 2 ) );
	}

	public function getPaymentsAllocatedToAssets() {
		return $this->m_fltPaymentsAllocatedToAssets;
	}

	public function sqlPaymentsAllocatedToAssets() {
		return ( true == isset( $this->m_fltPaymentsAllocatedToAssets ) ) ? ( string ) $this->m_fltPaymentsAllocatedToAssets : '0';
	}

	public function setPaymentsAllocatedToEquity( $fltPaymentsAllocatedToEquity ) {
		$this->set( 'm_fltPaymentsAllocatedToEquity', CStrings::strToFloatDef( $fltPaymentsAllocatedToEquity, NULL, false, 2 ) );
	}

	public function getPaymentsAllocatedToEquity() {
		return $this->m_fltPaymentsAllocatedToEquity;
	}

	public function sqlPaymentsAllocatedToEquity() {
		return ( true == isset( $this->m_fltPaymentsAllocatedToEquity ) ) ? ( string ) $this->m_fltPaymentsAllocatedToEquity : '0';
	}

	public function setPaymentsAllocatedToDeposits( $fltPaymentsAllocatedToDeposits ) {
		$this->set( 'm_fltPaymentsAllocatedToDeposits', CStrings::strToFloatDef( $fltPaymentsAllocatedToDeposits, NULL, false, 2 ) );
	}

	public function getPaymentsAllocatedToDeposits() {
		return $this->m_fltPaymentsAllocatedToDeposits;
	}

	public function sqlPaymentsAllocatedToDeposits() {
		return ( true == isset( $this->m_fltPaymentsAllocatedToDeposits ) ) ? ( string ) $this->m_fltPaymentsAllocatedToDeposits : '0';
	}

	public function setPaymentsAllocatedToDepositCredits( $fltPaymentsAllocatedToDepositCredits ) {
		$this->set( 'm_fltPaymentsAllocatedToDepositCredits', CStrings::strToFloatDef( $fltPaymentsAllocatedToDepositCredits, NULL, false, 2 ) );
	}

	public function getPaymentsAllocatedToDepositCredits() {
		return $this->m_fltPaymentsAllocatedToDepositCredits;
	}

	public function sqlPaymentsAllocatedToDepositCredits() {
		return ( true == isset( $this->m_fltPaymentsAllocatedToDepositCredits ) ) ? ( string ) $this->m_fltPaymentsAllocatedToDepositCredits : '0';
	}

	public function setPaymentsAllocatedToRefunds( $fltPaymentsAllocatedToRefunds ) {
		$this->set( 'm_fltPaymentsAllocatedToRefunds', CStrings::strToFloatDef( $fltPaymentsAllocatedToRefunds, NULL, false, 2 ) );
	}

	public function getPaymentsAllocatedToRefunds() {
		return $this->m_fltPaymentsAllocatedToRefunds;
	}

	public function sqlPaymentsAllocatedToRefunds() {
		return ( true == isset( $this->m_fltPaymentsAllocatedToRefunds ) ) ? ( string ) $this->m_fltPaymentsAllocatedToRefunds : '0';
	}

	public function setPaymentsAllocatedToOtherLiabilities( $fltPaymentsAllocatedToOtherLiabilities ) {
		$this->set( 'm_fltPaymentsAllocatedToOtherLiabilities', CStrings::strToFloatDef( $fltPaymentsAllocatedToOtherLiabilities, NULL, false, 2 ) );
	}

	public function getPaymentsAllocatedToOtherLiabilities() {
		return $this->m_fltPaymentsAllocatedToOtherLiabilities;
	}

	public function sqlPaymentsAllocatedToOtherLiabilities() {
		return ( true == isset( $this->m_fltPaymentsAllocatedToOtherLiabilities ) ) ? ( string ) $this->m_fltPaymentsAllocatedToOtherLiabilities : '0';
	}

	public function setPaymentsApplied( $fltPaymentsApplied ) {
		$this->set( 'm_fltPaymentsApplied', CStrings::strToFloatDef( $fltPaymentsApplied, NULL, false, 2 ) );
	}

	public function getPaymentsApplied() {
		return $this->m_fltPaymentsApplied;
	}

	public function sqlPaymentsApplied() {
		return ( true == isset( $this->m_fltPaymentsApplied ) ) ? ( string ) $this->m_fltPaymentsApplied : '0';
	}

	public function setRentAllocatedToCurrentPayments( $fltRentAllocatedToCurrentPayments ) {
		$this->set( 'm_fltRentAllocatedToCurrentPayments', CStrings::strToFloatDef( $fltRentAllocatedToCurrentPayments, NULL, false, 2 ) );
	}

	public function getRentAllocatedToCurrentPayments() {
		return $this->m_fltRentAllocatedToCurrentPayments;
	}

	public function sqlRentAllocatedToCurrentPayments() {
		return ( true == isset( $this->m_fltRentAllocatedToCurrentPayments ) ) ? ( string ) $this->m_fltRentAllocatedToCurrentPayments : '0';
	}

	public function setRentAllocatedToPrePayments( $fltRentAllocatedToPrePayments ) {
		$this->set( 'm_fltRentAllocatedToPrePayments', CStrings::strToFloatDef( $fltRentAllocatedToPrePayments, NULL, false, 2 ) );
	}

	public function getRentAllocatedToPrePayments() {
		return $this->m_fltRentAllocatedToPrePayments;
	}

	public function sqlRentAllocatedToPrePayments() {
		return ( true == isset( $this->m_fltRentAllocatedToPrePayments ) ) ? ( string ) $this->m_fltRentAllocatedToPrePayments : '0';
	}

	public function setRentAllocatedToPostToCashRent( $fltRentAllocatedToPostToCashRent ) {
		$this->set( 'm_fltRentAllocatedToPostToCashRent', CStrings::strToFloatDef( $fltRentAllocatedToPostToCashRent, NULL, false, 2 ) );
	}

	public function getRentAllocatedToPostToCashRent() {
		return $this->m_fltRentAllocatedToPostToCashRent;
	}

	public function sqlRentAllocatedToPostToCashRent() {
		return ( true == isset( $this->m_fltRentAllocatedToPostToCashRent ) ) ? ( string ) $this->m_fltRentAllocatedToPostToCashRent : '0';
	}

	public function setRentAllocatedToPostToCashOtherCharges( $fltRentAllocatedToPostToCashOtherCharges ) {
		$this->set( 'm_fltRentAllocatedToPostToCashOtherCharges', CStrings::strToFloatDef( $fltRentAllocatedToPostToCashOtherCharges, NULL, false, 2 ) );
	}

	public function getRentAllocatedToPostToCashOtherCharges() {
		return $this->m_fltRentAllocatedToPostToCashOtherCharges;
	}

	public function sqlRentAllocatedToPostToCashOtherCharges() {
		return ( true == isset( $this->m_fltRentAllocatedToPostToCashOtherCharges ) ) ? ( string ) $this->m_fltRentAllocatedToPostToCashOtherCharges : '0';
	}

	public function setRentAllocatedToPayments( $fltRentAllocatedToPayments ) {
		$this->set( 'm_fltRentAllocatedToPayments', CStrings::strToFloatDef( $fltRentAllocatedToPayments, NULL, false, 2 ) );
	}

	public function getRentAllocatedToPayments() {
		return $this->m_fltRentAllocatedToPayments;
	}

	public function sqlRentAllocatedToPayments() {
		return ( true == isset( $this->m_fltRentAllocatedToPayments ) ) ? ( string ) $this->m_fltRentAllocatedToPayments : '0';
	}

	public function setRentAllocatedToPaymentsInKind( $fltRentAllocatedToPaymentsInKind ) {
		$this->set( 'm_fltRentAllocatedToPaymentsInKind', CStrings::strToFloatDef( $fltRentAllocatedToPaymentsInKind, NULL, false, 2 ) );
	}

	public function getRentAllocatedToPaymentsInKind() {
		return $this->m_fltRentAllocatedToPaymentsInKind;
	}

	public function sqlRentAllocatedToPaymentsInKind() {
		return ( true == isset( $this->m_fltRentAllocatedToPaymentsInKind ) ) ? ( string ) $this->m_fltRentAllocatedToPaymentsInKind : '0';
	}

	public function setRentAllocatedToOtherCharges( $fltRentAllocatedToOtherCharges ) {
		$this->set( 'm_fltRentAllocatedToOtherCharges', CStrings::strToFloatDef( $fltRentAllocatedToOtherCharges, NULL, false, 2 ) );
	}

	public function getRentAllocatedToOtherCharges() {
		return $this->m_fltRentAllocatedToOtherCharges;
	}

	public function sqlRentAllocatedToOtherCharges() {
		return ( true == isset( $this->m_fltRentAllocatedToOtherCharges ) ) ? ( string ) $this->m_fltRentAllocatedToOtherCharges : '0';
	}

	public function setRentAllocatedToDeposits( $fltRentAllocatedToDeposits ) {
		$this->set( 'm_fltRentAllocatedToDeposits', CStrings::strToFloatDef( $fltRentAllocatedToDeposits, NULL, false, 2 ) );
	}

	public function getRentAllocatedToDeposits() {
		return $this->m_fltRentAllocatedToDeposits;
	}

	public function sqlRentAllocatedToDeposits() {
		return ( true == isset( $this->m_fltRentAllocatedToDeposits ) ) ? ( string ) $this->m_fltRentAllocatedToDeposits : '0';
	}

	public function setRentAllocatedToDepositCredits( $fltRentAllocatedToDepositCredits ) {
		$this->set( 'm_fltRentAllocatedToDepositCredits', CStrings::strToFloatDef( $fltRentAllocatedToDepositCredits, NULL, false, 2 ) );
	}

	public function getRentAllocatedToDepositCredits() {
		return $this->m_fltRentAllocatedToDepositCredits;
	}

	public function sqlRentAllocatedToDepositCredits() {
		return ( true == isset( $this->m_fltRentAllocatedToDepositCredits ) ) ? ( string ) $this->m_fltRentAllocatedToDepositCredits : '0';
	}

	public function setRentAllocatedToRefunds( $fltRentAllocatedToRefunds ) {
		$this->set( 'm_fltRentAllocatedToRefunds', CStrings::strToFloatDef( $fltRentAllocatedToRefunds, NULL, false, 2 ) );
	}

	public function getRentAllocatedToRefunds() {
		return $this->m_fltRentAllocatedToRefunds;
	}

	public function sqlRentAllocatedToRefunds() {
		return ( true == isset( $this->m_fltRentAllocatedToRefunds ) ) ? ( string ) $this->m_fltRentAllocatedToRefunds : '0';
	}

	public function setRentAllocatedToOtherLiabilities( $fltRentAllocatedToOtherLiabilities ) {
		$this->set( 'm_fltRentAllocatedToOtherLiabilities', CStrings::strToFloatDef( $fltRentAllocatedToOtherLiabilities, NULL, false, 2 ) );
	}

	public function getRentAllocatedToOtherLiabilities() {
		return $this->m_fltRentAllocatedToOtherLiabilities;
	}

	public function sqlRentAllocatedToOtherLiabilities() {
		return ( true == isset( $this->m_fltRentAllocatedToOtherLiabilities ) ) ? ( string ) $this->m_fltRentAllocatedToOtherLiabilities : '0';
	}

	public function setRentPaid( $fltRentPaid ) {
		$this->set( 'm_fltRentPaid', CStrings::strToFloatDef( $fltRentPaid, NULL, false, 2 ) );
	}

	public function getRentPaid() {
		return $this->m_fltRentPaid;
	}

	public function sqlRentPaid() {
		return ( true == isset( $this->m_fltRentPaid ) ) ? ( string ) $this->m_fltRentPaid : '0';
	}

	public function setDepositsAllocatedToPayments( $fltDepositsAllocatedToPayments ) {
		$this->set( 'm_fltDepositsAllocatedToPayments', CStrings::strToFloatDef( $fltDepositsAllocatedToPayments, NULL, false, 2 ) );
	}

	public function getDepositsAllocatedToPayments() {
		return $this->m_fltDepositsAllocatedToPayments;
	}

	public function sqlDepositsAllocatedToPayments() {
		return ( true == isset( $this->m_fltDepositsAllocatedToPayments ) ) ? ( string ) $this->m_fltDepositsAllocatedToPayments : '0';
	}

	public function setDepositsAllocatedToPaymentsInKind( $fltDepositsAllocatedToPaymentsInKind ) {
		$this->set( 'm_fltDepositsAllocatedToPaymentsInKind', CStrings::strToFloatDef( $fltDepositsAllocatedToPaymentsInKind, NULL, false, 2 ) );
	}

	public function getDepositsAllocatedToPaymentsInKind() {
		return $this->m_fltDepositsAllocatedToPaymentsInKind;
	}

	public function sqlDepositsAllocatedToPaymentsInKind() {
		return ( true == isset( $this->m_fltDepositsAllocatedToPaymentsInKind ) ) ? ( string ) $this->m_fltDepositsAllocatedToPaymentsInKind : '0';
	}

	public function setDepositsAllocatedToRent( $fltDepositsAllocatedToRent ) {
		$this->set( 'm_fltDepositsAllocatedToRent', CStrings::strToFloatDef( $fltDepositsAllocatedToRent, NULL, false, 2 ) );
	}

	public function getDepositsAllocatedToRent() {
		return $this->m_fltDepositsAllocatedToRent;
	}

	public function sqlDepositsAllocatedToRent() {
		return ( true == isset( $this->m_fltDepositsAllocatedToRent ) ) ? ( string ) $this->m_fltDepositsAllocatedToRent : '0';
	}

	public function setDepositsAllocatedToOtherCharges( $fltDepositsAllocatedToOtherCharges ) {
		$this->set( 'm_fltDepositsAllocatedToOtherCharges', CStrings::strToFloatDef( $fltDepositsAllocatedToOtherCharges, NULL, false, 2 ) );
	}

	public function getDepositsAllocatedToOtherCharges() {
		return $this->m_fltDepositsAllocatedToOtherCharges;
	}

	public function sqlDepositsAllocatedToOtherCharges() {
		return ( true == isset( $this->m_fltDepositsAllocatedToOtherCharges ) ) ? ( string ) $this->m_fltDepositsAllocatedToOtherCharges : '0';
	}

	public function setDepositsAllocatedToRefunds( $fltDepositsAllocatedToRefunds ) {
		$this->set( 'm_fltDepositsAllocatedToRefunds', CStrings::strToFloatDef( $fltDepositsAllocatedToRefunds, NULL, false, 2 ) );
	}

	public function getDepositsAllocatedToRefunds() {
		return $this->m_fltDepositsAllocatedToRefunds;
	}

	public function sqlDepositsAllocatedToRefunds() {
		return ( true == isset( $this->m_fltDepositsAllocatedToRefunds ) ) ? ( string ) $this->m_fltDepositsAllocatedToRefunds : '0';
	}

	public function setDepositsAllocatedToOtherLiabilities( $fltDepositsAllocatedToOtherLiabilities ) {
		$this->set( 'm_fltDepositsAllocatedToOtherLiabilities', CStrings::strToFloatDef( $fltDepositsAllocatedToOtherLiabilities, NULL, false, 2 ) );
	}

	public function getDepositsAllocatedToOtherLiabilities() {
		return $this->m_fltDepositsAllocatedToOtherLiabilities;
	}

	public function sqlDepositsAllocatedToOtherLiabilities() {
		return ( true == isset( $this->m_fltDepositsAllocatedToOtherLiabilities ) ) ? ( string ) $this->m_fltDepositsAllocatedToOtherLiabilities : '0';
	}

	public function setDepositsPaid( $fltDepositsPaid ) {
		$this->set( 'm_fltDepositsPaid', CStrings::strToFloatDef( $fltDepositsPaid, NULL, false, 2 ) );
	}

	public function getDepositsPaid() {
		return $this->m_fltDepositsPaid;
	}

	public function sqlDepositsPaid() {
		return ( true == isset( $this->m_fltDepositsPaid ) ) ? ( string ) $this->m_fltDepositsPaid : '0';
	}

	public function setDepositCreditsAllocatedToPayments( $fltDepositCreditsAllocatedToPayments ) {
		$this->set( 'm_fltDepositCreditsAllocatedToPayments', CStrings::strToFloatDef( $fltDepositCreditsAllocatedToPayments, NULL, false, 2 ) );
	}

	public function getDepositCreditsAllocatedToPayments() {
		return $this->m_fltDepositCreditsAllocatedToPayments;
	}

	public function sqlDepositCreditsAllocatedToPayments() {
		return ( true == isset( $this->m_fltDepositCreditsAllocatedToPayments ) ) ? ( string ) $this->m_fltDepositCreditsAllocatedToPayments : '0';
	}

	public function setDepositCreditsAllocatedToPaymentsInKind( $fltDepositCreditsAllocatedToPaymentsInKind ) {
		$this->set( 'm_fltDepositCreditsAllocatedToPaymentsInKind', CStrings::strToFloatDef( $fltDepositCreditsAllocatedToPaymentsInKind, NULL, false, 2 ) );
	}

	public function getDepositCreditsAllocatedToPaymentsInKind() {
		return $this->m_fltDepositCreditsAllocatedToPaymentsInKind;
	}

	public function sqlDepositCreditsAllocatedToPaymentsInKind() {
		return ( true == isset( $this->m_fltDepositCreditsAllocatedToPaymentsInKind ) ) ? ( string ) $this->m_fltDepositCreditsAllocatedToPaymentsInKind : '0';
	}

	public function setDepositCreditsAllocatedToRent( $fltDepositCreditsAllocatedToRent ) {
		$this->set( 'm_fltDepositCreditsAllocatedToRent', CStrings::strToFloatDef( $fltDepositCreditsAllocatedToRent, NULL, false, 2 ) );
	}

	public function getDepositCreditsAllocatedToRent() {
		return $this->m_fltDepositCreditsAllocatedToRent;
	}

	public function sqlDepositCreditsAllocatedToRent() {
		return ( true == isset( $this->m_fltDepositCreditsAllocatedToRent ) ) ? ( string ) $this->m_fltDepositCreditsAllocatedToRent : '0';
	}

	public function setDepositCreditsAllocatedToOtherCharges( $fltDepositCreditsAllocatedToOtherCharges ) {
		$this->set( 'm_fltDepositCreditsAllocatedToOtherCharges', CStrings::strToFloatDef( $fltDepositCreditsAllocatedToOtherCharges, NULL, false, 2 ) );
	}

	public function getDepositCreditsAllocatedToOtherCharges() {
		return $this->m_fltDepositCreditsAllocatedToOtherCharges;
	}

	public function sqlDepositCreditsAllocatedToOtherCharges() {
		return ( true == isset( $this->m_fltDepositCreditsAllocatedToOtherCharges ) ) ? ( string ) $this->m_fltDepositCreditsAllocatedToOtherCharges : '0';
	}

	public function setDepositCreditsAllocatedToRefunds( $fltDepositCreditsAllocatedToRefunds ) {
		$this->set( 'm_fltDepositCreditsAllocatedToRefunds', CStrings::strToFloatDef( $fltDepositCreditsAllocatedToRefunds, NULL, false, 2 ) );
	}

	public function getDepositCreditsAllocatedToRefunds() {
		return $this->m_fltDepositCreditsAllocatedToRefunds;
	}

	public function sqlDepositCreditsAllocatedToRefunds() {
		return ( true == isset( $this->m_fltDepositCreditsAllocatedToRefunds ) ) ? ( string ) $this->m_fltDepositCreditsAllocatedToRefunds : '0';
	}

	public function setDepositCreditsAllocatedToOtherLiabilities( $fltDepositCreditsAllocatedToOtherLiabilities ) {
		$this->set( 'm_fltDepositCreditsAllocatedToOtherLiabilities', CStrings::strToFloatDef( $fltDepositCreditsAllocatedToOtherLiabilities, NULL, false, 2 ) );
	}

	public function getDepositCreditsAllocatedToOtherLiabilities() {
		return $this->m_fltDepositCreditsAllocatedToOtherLiabilities;
	}

	public function sqlDepositCreditsAllocatedToOtherLiabilities() {
		return ( true == isset( $this->m_fltDepositCreditsAllocatedToOtherLiabilities ) ) ? ( string ) $this->m_fltDepositCreditsAllocatedToOtherLiabilities : '0';
	}

	public function setDepositCreditsPaid( $fltDepositCreditsPaid ) {
		$this->set( 'm_fltDepositCreditsPaid', CStrings::strToFloatDef( $fltDepositCreditsPaid, NULL, false, 2 ) );
	}

	public function getDepositCreditsPaid() {
		return $this->m_fltDepositCreditsPaid;
	}

	public function sqlDepositCreditsPaid() {
		return ( true == isset( $this->m_fltDepositCreditsPaid ) ) ? ( string ) $this->m_fltDepositCreditsPaid : '0';
	}

	public function setRefundsAllocatedToPayments( $fltRefundsAllocatedToPayments ) {
		$this->set( 'm_fltRefundsAllocatedToPayments', CStrings::strToFloatDef( $fltRefundsAllocatedToPayments, NULL, false, 2 ) );
	}

	public function getRefundsAllocatedToPayments() {
		return $this->m_fltRefundsAllocatedToPayments;
	}

	public function sqlRefundsAllocatedToPayments() {
		return ( true == isset( $this->m_fltRefundsAllocatedToPayments ) ) ? ( string ) $this->m_fltRefundsAllocatedToPayments : '0';
	}

	public function setRefundsAllocatedToPaymentsInKind( $fltRefundsAllocatedToPaymentsInKind ) {
		$this->set( 'm_fltRefundsAllocatedToPaymentsInKind', CStrings::strToFloatDef( $fltRefundsAllocatedToPaymentsInKind, NULL, false, 2 ) );
	}

	public function getRefundsAllocatedToPaymentsInKind() {
		return $this->m_fltRefundsAllocatedToPaymentsInKind;
	}

	public function sqlRefundsAllocatedToPaymentsInKind() {
		return ( true == isset( $this->m_fltRefundsAllocatedToPaymentsInKind ) ) ? ( string ) $this->m_fltRefundsAllocatedToPaymentsInKind : '0';
	}

	public function setRefundsAllocatedToRent( $fltRefundsAllocatedToRent ) {
		$this->set( 'm_fltRefundsAllocatedToRent', CStrings::strToFloatDef( $fltRefundsAllocatedToRent, NULL, false, 2 ) );
	}

	public function getRefundsAllocatedToRent() {
		return $this->m_fltRefundsAllocatedToRent;
	}

	public function sqlRefundsAllocatedToRent() {
		return ( true == isset( $this->m_fltRefundsAllocatedToRent ) ) ? ( string ) $this->m_fltRefundsAllocatedToRent : '0';
	}

	public function setRefundsAllocatedToOtherCharges( $fltRefundsAllocatedToOtherCharges ) {
		$this->set( 'm_fltRefundsAllocatedToOtherCharges', CStrings::strToFloatDef( $fltRefundsAllocatedToOtherCharges, NULL, false, 2 ) );
	}

	public function getRefundsAllocatedToOtherCharges() {
		return $this->m_fltRefundsAllocatedToOtherCharges;
	}

	public function sqlRefundsAllocatedToOtherCharges() {
		return ( true == isset( $this->m_fltRefundsAllocatedToOtherCharges ) ) ? ( string ) $this->m_fltRefundsAllocatedToOtherCharges : '0';
	}

	public function setRefundsAllocatedToDeposits( $fltRefundsAllocatedToDeposits ) {
		$this->set( 'm_fltRefundsAllocatedToDeposits', CStrings::strToFloatDef( $fltRefundsAllocatedToDeposits, NULL, false, 2 ) );
	}

	public function getRefundsAllocatedToDeposits() {
		return $this->m_fltRefundsAllocatedToDeposits;
	}

	public function sqlRefundsAllocatedToDeposits() {
		return ( true == isset( $this->m_fltRefundsAllocatedToDeposits ) ) ? ( string ) $this->m_fltRefundsAllocatedToDeposits : '0';
	}

	public function setRefundsAllocatedToDepositCredits( $fltRefundsAllocatedToDepositCredits ) {
		$this->set( 'm_fltRefundsAllocatedToDepositCredits', CStrings::strToFloatDef( $fltRefundsAllocatedToDepositCredits, NULL, false, 2 ) );
	}

	public function getRefundsAllocatedToDepositCredits() {
		return $this->m_fltRefundsAllocatedToDepositCredits;
	}

	public function sqlRefundsAllocatedToDepositCredits() {
		return ( true == isset( $this->m_fltRefundsAllocatedToDepositCredits ) ) ? ( string ) $this->m_fltRefundsAllocatedToDepositCredits : '0';
	}

	public function setRefundsAllocatedToRefunds( $fltRefundsAllocatedToRefunds ) {
		$this->set( 'm_fltRefundsAllocatedToRefunds', CStrings::strToFloatDef( $fltRefundsAllocatedToRefunds, NULL, false, 2 ) );
	}

	public function getRefundsAllocatedToRefunds() {
		return $this->m_fltRefundsAllocatedToRefunds;
	}

	public function sqlRefundsAllocatedToRefunds() {
		return ( true == isset( $this->m_fltRefundsAllocatedToRefunds ) ) ? ( string ) $this->m_fltRefundsAllocatedToRefunds : '0';
	}

	public function setRefundsAllocatedToOtherLiabilities( $fltRefundsAllocatedToOtherLiabilities ) {
		$this->set( 'm_fltRefundsAllocatedToOtherLiabilities', CStrings::strToFloatDef( $fltRefundsAllocatedToOtherLiabilities, NULL, false, 2 ) );
	}

	public function getRefundsAllocatedToOtherLiabilities() {
		return $this->m_fltRefundsAllocatedToOtherLiabilities;
	}

	public function sqlRefundsAllocatedToOtherLiabilities() {
		return ( true == isset( $this->m_fltRefundsAllocatedToOtherLiabilities ) ) ? ( string ) $this->m_fltRefundsAllocatedToOtherLiabilities : '0';
	}

	public function setRefundsPaid( $fltRefundsPaid ) {
		$this->set( 'm_fltRefundsPaid', CStrings::strToFloatDef( $fltRefundsPaid, NULL, false, 2 ) );
	}

	public function getRefundsPaid() {
		return $this->m_fltRefundsPaid;
	}

	public function sqlRefundsPaid() {
		return ( true == isset( $this->m_fltRefundsPaid ) ) ? ( string ) $this->m_fltRefundsPaid : '0';
	}

	public function setReceivablesAllocatedToPayments( $fltReceivablesAllocatedToPayments ) {
		$this->set( 'm_fltReceivablesAllocatedToPayments', CStrings::strToFloatDef( $fltReceivablesAllocatedToPayments, NULL, false, 2 ) );
	}

	public function getReceivablesAllocatedToPayments() {
		return $this->m_fltReceivablesAllocatedToPayments;
	}

	public function sqlReceivablesAllocatedToPayments() {
		return ( true == isset( $this->m_fltReceivablesAllocatedToPayments ) ) ? ( string ) $this->m_fltReceivablesAllocatedToPayments : '0';
	}

	public function setReceivablesAllocatedToPaymentsInKind( $fltReceivablesAllocatedToPaymentsInKind ) {
		$this->set( 'm_fltReceivablesAllocatedToPaymentsInKind', CStrings::strToFloatDef( $fltReceivablesAllocatedToPaymentsInKind, NULL, false, 2 ) );
	}

	public function getReceivablesAllocatedToPaymentsInKind() {
		return $this->m_fltReceivablesAllocatedToPaymentsInKind;
	}

	public function sqlReceivablesAllocatedToPaymentsInKind() {
		return ( true == isset( $this->m_fltReceivablesAllocatedToPaymentsInKind ) ) ? ( string ) $this->m_fltReceivablesAllocatedToPaymentsInKind : '0';
	}

	public function setTotalReceivablesPaid( $fltTotalReceivablesPaid ) {
		$this->set( 'm_fltTotalReceivablesPaid', CStrings::strToFloatDef( $fltTotalReceivablesPaid, NULL, false, 2 ) );
	}

	public function getTotalReceivablesPaid() {
		return $this->m_fltTotalReceivablesPaid;
	}

	public function sqlTotalReceivablesPaid() {
		return ( true == isset( $this->m_fltTotalReceivablesPaid ) ) ? ( string ) $this->m_fltTotalReceivablesPaid : '0';
	}

	public function setEndingPrePayments( $fltEndingPrePayments ) {
		$this->set( 'm_fltEndingPrePayments', CStrings::strToFloatDef( $fltEndingPrePayments, NULL, false, 2 ) );
	}

	public function getEndingPrePayments() {
		return $this->m_fltEndingPrePayments;
	}

	public function sqlEndingPrePayments() {
		return ( true == isset( $this->m_fltEndingPrePayments ) ) ? ( string ) $this->m_fltEndingPrePayments : '0';
	}

	public function setEndingRentReceivable( $fltEndingRentReceivable ) {
		$this->set( 'm_fltEndingRentReceivable', CStrings::strToFloatDef( $fltEndingRentReceivable, NULL, false, 2 ) );
	}

	public function getEndingRentReceivable() {
		return $this->m_fltEndingRentReceivable;
	}

	public function sqlEndingRentReceivable() {
		return ( true == isset( $this->m_fltEndingRentReceivable ) ) ? ( string ) $this->m_fltEndingRentReceivable : '0';
	}

	public function setEndingReceivables( $fltEndingReceivables ) {
		$this->set( 'm_fltEndingReceivables', CStrings::strToFloatDef( $fltEndingReceivables, NULL, false, 2 ) );
	}

	public function getEndingReceivables() {
		return $this->m_fltEndingReceivables;
	}

	public function sqlEndingReceivables() {
		return ( true == isset( $this->m_fltEndingReceivables ) ) ? ( string ) $this->m_fltEndingReceivables : '0';
	}

	public function setEndingDepositBalance( $fltEndingDepositBalance ) {
		$this->set( 'm_fltEndingDepositBalance', CStrings::strToFloatDef( $fltEndingDepositBalance, NULL, false, 2 ) );
	}

	public function getEndingDepositBalance() {
		return $this->m_fltEndingDepositBalance;
	}

	public function sqlEndingDepositBalance() {
		return ( true == isset( $this->m_fltEndingDepositBalance ) ) ? ( string ) $this->m_fltEndingDepositBalance : '0';
	}

	public function setEndingDepositHeld( $fltEndingDepositHeld ) {
		$this->set( 'm_fltEndingDepositHeld', CStrings::strToFloatDef( $fltEndingDepositHeld, NULL, false, 2 ) );
	}

	public function getEndingDepositHeld() {
		return $this->m_fltEndingDepositHeld;
	}

	public function sqlEndingDepositHeld() {
		return ( true == isset( $this->m_fltEndingDepositHeld ) ) ? ( string ) $this->m_fltEndingDepositHeld : '0';
	}

	public function setScheduledChargeCount( $intScheduledChargeCount ) {
		$this->set( 'm_intScheduledChargeCount', CStrings::strToIntDef( $intScheduledChargeCount, NULL, false ) );
	}

	public function getScheduledChargeCount() {
		return $this->m_intScheduledChargeCount;
	}

	public function sqlScheduledChargeCount() {
		return ( true == isset( $this->m_intScheduledChargeCount ) ) ? ( string ) $this->m_intScheduledChargeCount : '0';
	}

	public function setTransactionCount( $intTransactionCount ) {
		$this->set( 'm_intTransactionCount', CStrings::strToIntDef( $intTransactionCount, NULL, false ) );
	}

	public function getTransactionCount() {
		return $this->m_intTransactionCount;
	}

	public function sqlTransactionCount() {
		return ( true == isset( $this->m_intTransactionCount ) ) ? ( string ) $this->m_intTransactionCount : '0';
	}

	public function setAllocationCount( $intAllocationCount ) {
		$this->set( 'm_intAllocationCount', CStrings::strToIntDef( $intAllocationCount, NULL, false ) );
	}

	public function getAllocationCount() {
		return $this->m_intAllocationCount;
	}

	public function sqlAllocationCount() {
		return ( true == isset( $this->m_intAllocationCount ) ) ? ( string ) $this->m_intAllocationCount : '0';
	}

	public function setLeasesApproved( $intLeasesApproved ) {
		$this->set( 'm_intLeasesApproved', CStrings::strToIntDef( $intLeasesApproved, NULL, false ) );
	}

	public function getLeasesApproved() {
		return $this->m_intLeasesApproved;
	}

	public function sqlLeasesApproved() {
		return ( true == isset( $this->m_intLeasesApproved ) ) ? ( string ) $this->m_intLeasesApproved : '0';
	}

	public function setMoveIns( $intMoveIns ) {
		$this->set( 'm_intMoveIns', CStrings::strToIntDef( $intMoveIns, NULL, false ) );
	}

	public function getMoveIns() {
		return $this->m_intMoveIns;
	}

	public function sqlMoveIns() {
		return ( true == isset( $this->m_intMoveIns ) ) ? ( string ) $this->m_intMoveIns : '0';
	}

	public function setNotices( $intNotices ) {
		$this->set( 'm_intNotices', CStrings::strToIntDef( $intNotices, NULL, false ) );
	}

	public function getNotices() {
		return $this->m_intNotices;
	}

	public function sqlNotices() {
		return ( true == isset( $this->m_intNotices ) ) ? ( string ) $this->m_intNotices : '0';
	}

	public function setMoveOuts( $intMoveOuts ) {
		$this->set( 'm_intMoveOuts', CStrings::strToIntDef( $intMoveOuts, NULL, false ) );
	}

	public function getMoveOuts() {
		return $this->m_intMoveOuts;
	}

	public function sqlMoveOuts() {
		return ( true == isset( $this->m_intMoveOuts ) ) ? ( string ) $this->m_intMoveOuts : '0';
	}

	public function setFinancialMoveOuts( $intFinancialMoveOuts ) {
		$this->set( 'm_intFinancialMoveOuts', CStrings::strToIntDef( $intFinancialMoveOuts, NULL, false ) );
	}

	public function getFinancialMoveOuts() {
		return $this->m_intFinancialMoveOuts;
	}

	public function sqlFinancialMoveOuts() {
		return ( true == isset( $this->m_intFinancialMoveOuts ) ) ? ( string ) $this->m_intFinancialMoveOuts : '0';
	}

	public function setRenewals( $intRenewals ) {
		$this->set( 'm_intRenewals', CStrings::strToIntDef( $intRenewals, NULL, false ) );
	}

	public function getRenewals() {
		return $this->m_intRenewals;
	}

	public function sqlRenewals() {
		return ( true == isset( $this->m_intRenewals ) ) ? ( string ) $this->m_intRenewals : '0';
	}

	public function setSkips( $intSkips ) {
		$this->set( 'm_intSkips', CStrings::strToIntDef( $intSkips, NULL, false ) );
	}

	public function getSkips() {
		return $this->m_intSkips;
	}

	public function sqlSkips() {
		return ( true == isset( $this->m_intSkips ) ) ? ( string ) $this->m_intSkips : '0';
	}

	public function setNoShows( $intNoShows ) {
		$this->set( 'm_intNoShows', CStrings::strToIntDef( $intNoShows, NULL, false ) );
	}

	public function getNoShows() {
		return $this->m_intNoShows;
	}

	public function sqlNoShows() {
		return ( true == isset( $this->m_intNoShows ) ) ? ( string ) $this->m_intNoShows : '0';
	}

	public function setFirstLateNotices( $intFirstLateNotices ) {
		$this->set( 'm_intFirstLateNotices', CStrings::strToIntDef( $intFirstLateNotices, NULL, false ) );
	}

	public function getFirstLateNotices() {
		return $this->m_intFirstLateNotices;
	}

	public function sqlFirstLateNotices() {
		return ( true == isset( $this->m_intFirstLateNotices ) ) ? ( string ) $this->m_intFirstLateNotices : '0';
	}

	public function setSecondLateNotices( $intSecondLateNotices ) {
		$this->set( 'm_intSecondLateNotices', CStrings::strToIntDef( $intSecondLateNotices, NULL, false ) );
	}

	public function getSecondLateNotices() {
		return $this->m_intSecondLateNotices;
	}

	public function sqlSecondLateNotices() {
		return ( true == isset( $this->m_intSecondLateNotices ) ) ? ( string ) $this->m_intSecondLateNotices : '0';
	}

	public function setThirdLateNotices( $intThirdLateNotices ) {
		$this->set( 'm_intThirdLateNotices', CStrings::strToIntDef( $intThirdLateNotices, NULL, false ) );
	}

	public function getThirdLateNotices() {
		return $this->m_intThirdLateNotices;
	}

	public function sqlThirdLateNotices() {
		return ( true == isset( $this->m_intThirdLateNotices ) ) ? ( string ) $this->m_intThirdLateNotices : '0';
	}

	public function setMtmStarted( $intMtmStarted ) {
		$this->set( 'm_intMtmStarted', CStrings::strToIntDef( $intMtmStarted, NULL, false ) );
	}

	public function getMtmStarted() {
		return $this->m_intMtmStarted;
	}

	public function sqlMtmStarted() {
		return ( true == isset( $this->m_intMtmStarted ) ) ? ( string ) $this->m_intMtmStarted : '0';
	}

	public function setMtmOpen( $intMtmOpen ) {
		$this->set( 'm_intMtmOpen', CStrings::strToIntDef( $intMtmOpen, NULL, false ) );
	}

	public function getMtmOpen() {
		return $this->m_intMtmOpen;
	}

	public function sqlMtmOpen() {
		return ( true == isset( $this->m_intMtmOpen ) ) ? ( string ) $this->m_intMtmOpen : '0';
	}

	public function setMtmConverted( $intMtmConverted ) {
		$this->set( 'm_intMtmConverted', CStrings::strToIntDef( $intMtmConverted, NULL, false ) );
	}

	public function getMtmConverted() {
		return $this->m_intMtmConverted;
	}

	public function sqlMtmConverted() {
		return ( true == isset( $this->m_intMtmConverted ) ) ? ( string ) $this->m_intMtmConverted : '0';
	}

	public function setMtmNotices( $intMtmNotices ) {
		$this->set( 'm_intMtmNotices', CStrings::strToIntDef( $intMtmNotices, NULL, false ) );
	}

	public function getMtmNotices() {
		return $this->m_intMtmNotices;
	}

	public function sqlMtmNotices() {
		return ( true == isset( $this->m_intMtmNotices ) ) ? ( string ) $this->m_intMtmNotices : '0';
	}

	public function setMtmMoveOuts( $intMtmMoveOuts ) {
		$this->set( 'm_intMtmMoveOuts', CStrings::strToIntDef( $intMtmMoveOuts, NULL, false ) );
	}

	public function getMtmMoveOuts() {
		return $this->m_intMtmMoveOuts;
	}

	public function sqlMtmMoveOuts() {
		return ( true == isset( $this->m_intMtmMoveOuts ) ) ? ( string ) $this->m_intMtmMoveOuts : '0';
	}

	public function setCollectionsStarted( $intCollectionsStarted ) {
		$this->set( 'm_intCollectionsStarted', CStrings::strToIntDef( $intCollectionsStarted, NULL, false ) );
	}

	public function getCollectionsStarted() {
		return $this->m_intCollectionsStarted;
	}

	public function sqlCollectionsStarted() {
		return ( true == isset( $this->m_intCollectionsStarted ) ) ? ( string ) $this->m_intCollectionsStarted : '0';
	}

	public function setCollectionsOpen( $intCollectionsOpen ) {
		$this->set( 'm_intCollectionsOpen', CStrings::strToIntDef( $intCollectionsOpen, NULL, false ) );
	}

	public function getCollectionsOpen() {
		return $this->m_intCollectionsOpen;
	}

	public function sqlCollectionsOpen() {
		return ( true == isset( $this->m_intCollectionsOpen ) ) ? ( string ) $this->m_intCollectionsOpen : '0';
	}

	public function setCollectionsCompleted( $intCollectionsCompleted ) {
		$this->set( 'm_intCollectionsCompleted', CStrings::strToIntDef( $intCollectionsCompleted, NULL, false ) );
	}

	public function getCollectionsCompleted() {
		return $this->m_intCollectionsCompleted;
	}

	public function sqlCollectionsCompleted() {
		return ( true == isset( $this->m_intCollectionsCompleted ) ) ? ( string ) $this->m_intCollectionsCompleted : '0';
	}

	public function setEvictionsStarted( $intEvictionsStarted ) {
		$this->set( 'm_intEvictionsStarted', CStrings::strToIntDef( $intEvictionsStarted, NULL, false ) );
	}

	public function getEvictionsStarted() {
		return $this->m_intEvictionsStarted;
	}

	public function sqlEvictionsStarted() {
		return ( true == isset( $this->m_intEvictionsStarted ) ) ? ( string ) $this->m_intEvictionsStarted : '0';
	}

	public function setEvictionsOpen( $intEvictionsOpen ) {
		$this->set( 'm_intEvictionsOpen', CStrings::strToIntDef( $intEvictionsOpen, NULL, false ) );
	}

	public function getEvictionsOpen() {
		return $this->m_intEvictionsOpen;
	}

	public function sqlEvictionsOpen() {
		return ( true == isset( $this->m_intEvictionsOpen ) ) ? ( string ) $this->m_intEvictionsOpen : '0';
	}

	public function setEvictionsCancelled( $intEvictionsCancelled ) {
		$this->set( 'm_intEvictionsCancelled', CStrings::strToIntDef( $intEvictionsCancelled, NULL, false ) );
	}

	public function getEvictionsCancelled() {
		return $this->m_intEvictionsCancelled;
	}

	public function sqlEvictionsCancelled() {
		return ( true == isset( $this->m_intEvictionsCancelled ) ) ? ( string ) $this->m_intEvictionsCancelled : '0';
	}

	public function setEvictionsCompleted( $intEvictionsCompleted ) {
		$this->set( 'm_intEvictionsCompleted', CStrings::strToIntDef( $intEvictionsCompleted, NULL, false ) );
	}

	public function getEvictionsCompleted() {
		return $this->m_intEvictionsCompleted;
	}

	public function sqlEvictionsCompleted() {
		return ( true == isset( $this->m_intEvictionsCompleted ) ) ? ( string ) $this->m_intEvictionsCompleted : '0';
	}

	public function setTransfersStarted( $intTransfersStarted ) {
		$this->set( 'm_intTransfersStarted', CStrings::strToIntDef( $intTransfersStarted, NULL, false ) );
	}

	public function getTransfersStarted() {
		return $this->m_intTransfersStarted;
	}

	public function sqlTransfersStarted() {
		return ( true == isset( $this->m_intTransfersStarted ) ) ? ( string ) $this->m_intTransfersStarted : '0';
	}

	public function setTransfersOpen( $intTransfersOpen ) {
		$this->set( 'm_intTransfersOpen', CStrings::strToIntDef( $intTransfersOpen, NULL, false ) );
	}

	public function getTransfersOpen() {
		return $this->m_intTransfersOpen;
	}

	public function sqlTransfersOpen() {
		return ( true == isset( $this->m_intTransfersOpen ) ) ? ( string ) $this->m_intTransfersOpen : '0';
	}

	public function setTransfersCancelled( $intTransfersCancelled ) {
		$this->set( 'm_intTransfersCancelled', CStrings::strToIntDef( $intTransfersCancelled, NULL, false ) );
	}

	public function getTransfersCancelled() {
		return $this->m_intTransfersCancelled;
	}

	public function sqlTransfersCancelled() {
		return ( true == isset( $this->m_intTransfersCancelled ) ) ? ( string ) $this->m_intTransfersCancelled : '0';
	}

	public function setTransfersCompleted( $intTransfersCompleted ) {
		$this->set( 'm_intTransfersCompleted', CStrings::strToIntDef( $intTransfersCompleted, NULL, false ) );
	}

	public function getTransfersCompleted() {
		return $this->m_intTransfersCompleted;
	}

	public function sqlTransfersCompleted() {
		return ( true == isset( $this->m_intTransfersCompleted ) ) ? ( string ) $this->m_intTransfersCompleted : '0';
	}

	public function setTotalEvents( $intTotalEvents ) {
		$this->set( 'm_intTotalEvents', CStrings::strToIntDef( $intTotalEvents, NULL, false ) );
	}

	public function getTotalEvents() {
		return $this->m_intTotalEvents;
	}

	public function sqlTotalEvents() {
		return ( true == isset( $this->m_intTotalEvents ) ) ? ( string ) $this->m_intTotalEvents : '0';
	}

	public function setIsDeleted( $boolIsDeleted ) {
		$this->set( 'm_boolIsDeleted', CStrings::strToBool( $boolIsDeleted ) );
	}

	public function getIsDeleted() {
		return $this->m_boolIsDeleted;
	}

	public function sqlIsDeleted() {
		return ( true == isset( $this->m_boolIsDeleted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDeleted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function toArray() {
		return array(
			'cid' => $this->getCid(),
			'period_id' => $this->getPeriodId(),
			'property_id' => $this->getPropertyId(),
			'property_floorplan_id' => $this->getPropertyFloorplanId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'unit_space_rate_log_id' => $this->getUnitSpaceRateLogId(),
			'unit_space_log_id' => $this->getUnitSpaceLogId(),
			'unit_space_status_type_id' => $this->getUnitSpaceStatusTypeId(),
			'unit_exclusion_reason_type_id' => $this->getUnitExclusionReasonTypeId(),
			'group_unit_space_id' => $this->getGroupUnitSpaceId(),
			'application_lease_id' => $this->getApplicationLeaseId(),
			'hold_lease_id' => $this->getHoldLeaseId(),
			'future_lease_id' => $this->getFutureLeaseId(),
			'current_lease_id' => $this->getCurrentLeaseId(),
			'past_lease_id' => $this->getPastLeaseId(),
			'post_month' => $this->getPostMonth(),
			'move_out_date' => $this->getMoveOutDate(),
			'building_name' => $this->getBuildingName(),
			'unit_number_cache' => $this->getUnitNumberCache(),
			'display_number' => $this->getDisplayNumber(),
			'financial_days' => $this->getFinancialDays(),
			'days_vacant' => $this->getDaysVacant(),
			'days_vacant_adjustment' => $this->getDaysVacantAdjustment(),
			'consecutive_days_vacant' => $this->getConsecutiveDaysVacant(),
			'gpr_rent' => $this->getGprRent(),
			'budgeted_rent' => $this->getBudgetedRent(),
			'market_rent' => $this->getMarketRent(),
			'gain_on_lease' => $this->getGainOnLease(),
			'loss_on_lease' => $this->getLossOnLease(),
			'gain_loss_on_lease' => $this->getGainLossOnLease(),
			'potential_rent' => $this->getPotentialRent(),
			'current_vacancy_loss' => $this->getCurrentVacancyLoss(),
			'gain_prior_vacancy_adjustment' => $this->getGainPriorVacancyAdjustment(),
			'loss_prior_vacancy_adjustment' => $this->getLossPriorVacancyAdjustment(),
			'total_prior_vacancy_adjustment' => $this->getTotalPriorVacancyAdjustment(),
			'total_vacancy_loss' => $this->getTotalVacancyLoss(),
			'consecutive_vacancy_loss' => $this->getConsecutiveVacancyLoss(),
			'scheduled_current_rent_setup' => $this->getScheduledCurrentRentSetup(),
			'scheduled_current_misc_setup' => $this->getScheduledCurrentMiscSetup(),
			'scheduled_current_prorated_rent_setup' => $this->getScheduledCurrentProratedRentSetup(),
			'scheduled_current_prorated_misc_setup' => $this->getScheduledCurrentProratedMiscSetup(),
			'scheduled_future_rent_setup' => $this->getScheduledFutureRentSetup(),
			'scheduled_future_misc_setup' => $this->getScheduledFutureMiscSetup(),
			'scheduled_future_prorated_rent_setup' => $this->getScheduledFutureProratedRentSetup(),
			'scheduled_future_prorated_misc_setup' => $this->getScheduledFutureProratedMiscSetup(),
			'accelerated_rent' => $this->getAcceleratedRent(),
			'application_fees' => $this->getApplicationFees(),
			'early_termination_fees' => $this->getEarlyTerminationFees(),
			'insufficient_notice_fees' => $this->getInsufficientNoticeFees(),
			'month_to_month_fees' => $this->getMonthToMonthFees(),
			'return_item_fees' => $this->getReturnItemFees(),
			'transfer_fees' => $this->getTransferFees(),
			'write_offs' => $this->getWriteOffs(),
			'tax_fees' => $this->getTaxFees(),
			'late_fees' => $this->getLateFees(),
			'work_order_fees' => $this->getWorkOrderFees(),
			'deposit_interest_credits' => $this->getDepositInterestCredits(),
			'utility_fees' => $this->getUtilityFees(),
			'beginning_pre_payments' => $this->getBeginningPrePayments(),
			'beginning_rent_receivable' => $this->getBeginningRentReceivable(),
			'beginning_receivables' => $this->getBeginningReceivables(),
			'beginning_deposit_balance' => $this->getBeginningDepositBalance(),
			'beginning_deposit_held' => $this->getBeginningDepositHeld(),
			'payments' => $this->getPayments(),
			'payments_in_kind' => $this->getPaymentsInKind(),
			'rent_written_off' => $this->getRentWrittenOff(),
			'rent_out_of_period' => $this->getRentOutOfPeriod(),
			'rent_scheduled' => $this->getRentScheduled(),
			'rent_adjustments' => $this->getRentAdjustments(),
			'rent_base' => $this->getRentBase(),
			'rent_amenities' => $this->getRentAmenities(),
			'rent_pets' => $this->getRentPets(),
			'rent_add_ons' => $this->getRentAddOns(),
			'rent_risk' => $this->getRentRisk(),
			'rent_specials' => $this->getRentSpecials(),
			'rent_maintenance' => $this->getRentMaintenance(),
			'rent' => $this->getRent(),
			'other_income_base' => $this->getOtherIncomeBase(),
			'other_income_amenities' => $this->getOtherIncomeAmenities(),
			'other_income_pets' => $this->getOtherIncomePets(),
			'other_income_add_ons' => $this->getOtherIncomeAddOns(),
			'other_income_risk' => $this->getOtherIncomeRisk(),
			'other_income_specials' => $this->getOtherIncomeSpecials(),
			'other_income_maintenance' => $this->getOtherIncomeMaintenance(),
			'other_income' => $this->getOtherIncome(),
			'total_new_income_receivable' => $this->getTotalNewIncomeReceivable(),
			'expenses' => $this->getExpenses(),
			'assets' => $this->getAssets(),
			'equity' => $this->getEquity(),
			'total_other_charges' => $this->getTotalOtherCharges(),
			'deposit_allocations' => $this->getDepositAllocations(),
			'deposit_credit_allocations' => $this->getDepositCreditAllocations(),
			'refunds' => $this->getRefunds(),
			'other_liabilities' => $this->getOtherLiabilities(),
			'total_new_non_income_receivable' => $this->getTotalNewNonIncomeReceivable(),
			'total_new_receivables' => $this->getTotalNewReceivables(),
			'deposits_base' => $this->getDepositsBase(),
			'deposits_amenities' => $this->getDepositsAmenities(),
			'deposits_pets' => $this->getDepositsPets(),
			'deposits_add_ons' => $this->getDepositsAddOns(),
			'deposits_risk' => $this->getDepositsRisk(),
			'deposits_specials' => $this->getDepositsSpecials(),
			'deposits_maintenance' => $this->getDepositsMaintenance(),
			'deposits' => $this->getDeposits(),
			'deposit_credits' => $this->getDepositCredits(),
			'payments_allocated_to_rent' => $this->getPaymentsAllocatedToRent(),
			'payments_allocated_to_other_income' => $this->getPaymentsAllocatedToOtherIncome(),
			'payments_allocated_to_expenses' => $this->getPaymentsAllocatedToExpenses(),
			'payments_allocated_to_assets' => $this->getPaymentsAllocatedToAssets(),
			'payments_allocated_to_equity' => $this->getPaymentsAllocatedToEquity(),
			'payments_allocated_to_deposits' => $this->getPaymentsAllocatedToDeposits(),
			'payments_allocated_to_deposit_credits' => $this->getPaymentsAllocatedToDepositCredits(),
			'payments_allocated_to_refunds' => $this->getPaymentsAllocatedToRefunds(),
			'payments_allocated_to_other_liabilities' => $this->getPaymentsAllocatedToOtherLiabilities(),
			'payments_applied' => $this->getPaymentsApplied(),
			'rent_allocated_to_current_payments' => $this->getRentAllocatedToCurrentPayments(),
			'rent_allocated_to_pre_payments' => $this->getRentAllocatedToPrePayments(),
			'rent_allocated_to_post_to_cash_rent' => $this->getRentAllocatedToPostToCashRent(),
			'rent_allocated_to_post_to_cash_other_charges' => $this->getRentAllocatedToPostToCashOtherCharges(),
			'rent_allocated_to_payments' => $this->getRentAllocatedToPayments(),
			'rent_allocated_to_payments_in_kind' => $this->getRentAllocatedToPaymentsInKind(),
			'rent_allocated_to_other_charges' => $this->getRentAllocatedToOtherCharges(),
			'rent_allocated_to_deposits' => $this->getRentAllocatedToDeposits(),
			'rent_allocated_to_deposit_credits' => $this->getRentAllocatedToDepositCredits(),
			'rent_allocated_to_refunds' => $this->getRentAllocatedToRefunds(),
			'rent_allocated_to_other_liabilities' => $this->getRentAllocatedToOtherLiabilities(),
			'rent_paid' => $this->getRentPaid(),
			'deposits_allocated_to_payments' => $this->getDepositsAllocatedToPayments(),
			'deposits_allocated_to_payments_in_kind' => $this->getDepositsAllocatedToPaymentsInKind(),
			'deposits_allocated_to_rent' => $this->getDepositsAllocatedToRent(),
			'deposits_allocated_to_other_charges' => $this->getDepositsAllocatedToOtherCharges(),
			'deposits_allocated_to_refunds' => $this->getDepositsAllocatedToRefunds(),
			'deposits_allocated_to_other_liabilities' => $this->getDepositsAllocatedToOtherLiabilities(),
			'deposits_paid' => $this->getDepositsPaid(),
			'deposit_credits_allocated_to_payments' => $this->getDepositCreditsAllocatedToPayments(),
			'deposit_credits_allocated_to_payments_in_kind' => $this->getDepositCreditsAllocatedToPaymentsInKind(),
			'deposit_credits_allocated_to_rent' => $this->getDepositCreditsAllocatedToRent(),
			'deposit_credits_allocated_to_other_charges' => $this->getDepositCreditsAllocatedToOtherCharges(),
			'deposit_credits_allocated_to_refunds' => $this->getDepositCreditsAllocatedToRefunds(),
			'deposit_credits_allocated_to_other_liabilities' => $this->getDepositCreditsAllocatedToOtherLiabilities(),
			'deposit_credits_paid' => $this->getDepositCreditsPaid(),
			'refunds_allocated_to_payments' => $this->getRefundsAllocatedToPayments(),
			'refunds_allocated_to_payments_in_kind' => $this->getRefundsAllocatedToPaymentsInKind(),
			'refunds_allocated_to_rent' => $this->getRefundsAllocatedToRent(),
			'refunds_allocated_to_other_charges' => $this->getRefundsAllocatedToOtherCharges(),
			'refunds_allocated_to_deposits' => $this->getRefundsAllocatedToDeposits(),
			'refunds_allocated_to_deposit_credits' => $this->getRefundsAllocatedToDepositCredits(),
			'refunds_allocated_to_refunds' => $this->getRefundsAllocatedToRefunds(),
			'refunds_allocated_to_other_liabilities' => $this->getRefundsAllocatedToOtherLiabilities(),
			'refunds_paid' => $this->getRefundsPaid(),
			'receivables_allocated_to_payments' => $this->getReceivablesAllocatedToPayments(),
			'receivables_allocated_to_payments_in_kind' => $this->getReceivablesAllocatedToPaymentsInKind(),
			'total_receivables_paid' => $this->getTotalReceivablesPaid(),
			'ending_pre_payments' => $this->getEndingPrePayments(),
			'ending_rent_receivable' => $this->getEndingRentReceivable(),
			'ending_receivables' => $this->getEndingReceivables(),
			'ending_deposit_balance' => $this->getEndingDepositBalance(),
			'ending_deposit_held' => $this->getEndingDepositHeld(),
			'scheduled_charge_count' => $this->getScheduledChargeCount(),
			'transaction_count' => $this->getTransactionCount(),
			'allocation_count' => $this->getAllocationCount(),
			'leases_approved' => $this->getLeasesApproved(),
			'move_ins' => $this->getMoveIns(),
			'notices' => $this->getNotices(),
			'move_outs' => $this->getMoveOuts(),
			'financial_move_outs' => $this->getFinancialMoveOuts(),
			'renewals' => $this->getRenewals(),
			'skips' => $this->getSkips(),
			'no_shows' => $this->getNoShows(),
			'first_late_notices' => $this->getFirstLateNotices(),
			'second_late_notices' => $this->getSecondLateNotices(),
			'third_late_notices' => $this->getThirdLateNotices(),
			'mtm_started' => $this->getMtmStarted(),
			'mtm_open' => $this->getMtmOpen(),
			'mtm_converted' => $this->getMtmConverted(),
			'mtm_notices' => $this->getMtmNotices(),
			'mtm_move_outs' => $this->getMtmMoveOuts(),
			'collections_started' => $this->getCollectionsStarted(),
			'collections_open' => $this->getCollectionsOpen(),
			'collections_completed' => $this->getCollectionsCompleted(),
			'evictions_started' => $this->getEvictionsStarted(),
			'evictions_open' => $this->getEvictionsOpen(),
			'evictions_cancelled' => $this->getEvictionsCancelled(),
			'evictions_completed' => $this->getEvictionsCompleted(),
			'transfers_started' => $this->getTransfersStarted(),
			'transfers_open' => $this->getTransfersOpen(),
			'transfers_cancelled' => $this->getTransfersCancelled(),
			'transfers_completed' => $this->getTransfersCompleted(),
			'total_events' => $this->getTotalEvents(),
			'is_deleted' => $this->getIsDeleted(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>