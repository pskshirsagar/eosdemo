<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGenders
 * Do not add any new functions to this class.
 */

class CBaseGenders extends CEosPluralBase {

	/**
	 * @return CGender[]
	 */
	public static function fetchGenders( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CGender::class, $objDatabase );
	}

	/**
	 * @return CGender
	 */
	public static function fetchGender( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGender::class, $objDatabase );
	}

	public static function fetchGenderCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'genders', $objDatabase );
	}

	public static function fetchGenderById( $intId, $objDatabase ) {
		return self::fetchGender( sprintf( 'SELECT * FROM genders WHERE id = %d', $intId ), $objDatabase );
	}

}
?>