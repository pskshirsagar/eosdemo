<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBankLoginTypes
 * Do not add any new functions to this class.
 */

class CBaseBankLoginTypes extends CEosPluralBase {

	/**
	 * @return CBankLoginType[]
	 */
	public static function fetchBankLoginTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CBankLoginType::class, $objDatabase );
	}

	/**
	 * @return CBankLoginType
	 */
	public static function fetchBankLoginType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBankLoginType::class, $objDatabase );
	}

	public static function fetchBankLoginTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'bank_login_types', $objDatabase );
	}

	public static function fetchBankLoginTypeById( $intId, $objDatabase ) {
		return self::fetchBankLoginType( sprintf( 'SELECT * FROM bank_login_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>