<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSmartDeviceBrands
 * Do not add any new functions to this class.
 */

class CBaseSmartDeviceBrands extends CEosPluralBase {

	/**
	 * @return CSmartDeviceBrand[]
	 */
	public static function fetchSmartDeviceBrands( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSmartDeviceBrand::class, $objDatabase );
	}

	/**
	 * @return CSmartDeviceBrand
	 */
	public static function fetchSmartDeviceBrand( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSmartDeviceBrand::class, $objDatabase );
	}

	public static function fetchSmartDeviceBrandCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'smart_device_brands', $objDatabase );
	}

	public static function fetchSmartDeviceBrandById( $intId, $objDatabase ) {
		return self::fetchSmartDeviceBrand( sprintf( 'SELECT * FROM smart_device_brands WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchSmartDeviceBrandsByApCodeId( $intApCodeId, $objDatabase ) {
		return self::fetchSmartDeviceBrands( sprintf( 'SELECT * FROM smart_device_brands WHERE ap_code_id = %d', $intApCodeId ), $objDatabase );
	}

	public static function fetchSmartDeviceBrandsBySmartDeviceTypeId( $intSmartDeviceTypeId, $objDatabase ) {
		return self::fetchSmartDeviceBrands( sprintf( 'SELECT * FROM smart_device_brands WHERE smart_device_type_id = %d', $intSmartDeviceTypeId ), $objDatabase );
	}

}
?>