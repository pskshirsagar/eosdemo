<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseExportActivityLog extends CEosSingularBase {

	const TABLE_NAME = 'public.export_activity_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intAccountingExportBatchId;
	protected $m_strActivityType;
	protected $m_strBatchSnapshotDetails;
	protected $m_jsonBatchSnapshotDetails;
	protected $m_strActivityDatetime;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['accounting_export_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountingExportBatchId', trim( $arrValues['accounting_export_batch_id'] ) ); elseif( isset( $arrValues['accounting_export_batch_id'] ) ) $this->setAccountingExportBatchId( $arrValues['accounting_export_batch_id'] );
		if( isset( $arrValues['activity_type'] ) && $boolDirectSet ) $this->set( 'm_strActivityType', trim( stripcslashes( $arrValues['activity_type'] ) ) ); elseif( isset( $arrValues['activity_type'] ) ) $this->setActivityType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['activity_type'] ) : $arrValues['activity_type'] );
		if( isset( $arrValues['batch_snapshot_details'] ) ) $this->set( 'm_strBatchSnapshotDetails', trim( $arrValues['batch_snapshot_details'] ) );
		if( isset( $arrValues['activity_datetime'] ) && $boolDirectSet ) $this->set( 'm_strActivityDatetime', trim( $arrValues['activity_datetime'] ) ); elseif( isset( $arrValues['activity_datetime'] ) ) $this->setActivityDatetime( $arrValues['activity_datetime'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setAccountingExportBatchId( $intAccountingExportBatchId ) {
		$this->set( 'm_intAccountingExportBatchId', CStrings::strToIntDef( $intAccountingExportBatchId, NULL, false ) );
	}

	public function getAccountingExportBatchId() {
		return $this->m_intAccountingExportBatchId;
	}

	public function sqlAccountingExportBatchId() {
		return ( true == isset( $this->m_intAccountingExportBatchId ) ) ? ( string ) $this->m_intAccountingExportBatchId : 'NULL';
	}

	public function setActivityType( $strActivityType ) {
		$this->set( 'm_strActivityType', CStrings::strTrimDef( $strActivityType, 20, NULL, true ) );
	}

	public function getActivityType() {
		return $this->m_strActivityType;
	}

	public function sqlActivityType() {
		return ( true == isset( $this->m_strActivityType ) ) ? '\'' . addslashes( $this->m_strActivityType ) . '\'' : 'NULL';
	}

	public function setBatchSnapshotDetails( $jsonBatchSnapshotDetails ) {
		if( true == valObj( $jsonBatchSnapshotDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonBatchSnapshotDetails', $jsonBatchSnapshotDetails );
		} elseif( true == valJsonString( $jsonBatchSnapshotDetails ) ) {
			$this->set( 'm_jsonBatchSnapshotDetails', CStrings::strToJson( $jsonBatchSnapshotDetails ) );
		} else {
			$this->set( 'm_jsonBatchSnapshotDetails', NULL ); 
		}
		unset( $this->m_strBatchSnapshotDetails );
	}

	public function getBatchSnapshotDetails() {
		if( true == isset( $this->m_strBatchSnapshotDetails ) ) {
			$this->m_jsonBatchSnapshotDetails = CStrings::strToJson( $this->m_strBatchSnapshotDetails );
			unset( $this->m_strBatchSnapshotDetails );
		}
		return $this->m_jsonBatchSnapshotDetails;
	}

	public function sqlBatchSnapshotDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getBatchSnapshotDetails() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getBatchSnapshotDetails() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setActivityDatetime( $strActivityDatetime ) {
		$this->set( 'm_strActivityDatetime', CStrings::strTrimDef( $strActivityDatetime, -1, NULL, true ) );
	}

	public function getActivityDatetime() {
		return $this->m_strActivityDatetime;
	}

	public function sqlActivityDatetime() {
		return ( true == isset( $this->m_strActivityDatetime ) ) ? '\'' . $this->m_strActivityDatetime . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, accounting_export_batch_id, activity_type, batch_snapshot_details, activity_datetime, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlAccountingExportBatchId() . ', ' .
 						$this->sqlActivityType() . ', ' .
 						$this->sqlBatchSnapshotDetails() . ', ' .
 						$this->sqlActivityDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accounting_export_batch_id = ' . $this->sqlAccountingExportBatchId() . ','; } elseif( true == array_key_exists( 'AccountingExportBatchId', $this->getChangedColumns() ) ) { $strSql .= ' accounting_export_batch_id = ' . $this->sqlAccountingExportBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' activity_type = ' . $this->sqlActivityType() . ','; } elseif( true == array_key_exists( 'ActivityType', $this->getChangedColumns() ) ) { $strSql .= ' activity_type = ' . $this->sqlActivityType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_snapshot_details = ' . $this->sqlBatchSnapshotDetails() . ','; } elseif( true == array_key_exists( 'BatchSnapshotDetails', $this->getChangedColumns() ) ) { $strSql .= ' batch_snapshot_details = ' . $this->sqlBatchSnapshotDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' activity_datetime = ' . $this->sqlActivityDatetime() . ','; } elseif( true == array_key_exists( 'ActivityDatetime', $this->getChangedColumns() ) ) { $strSql .= ' activity_datetime = ' . $this->sqlActivityDatetime() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'accounting_export_batch_id' => $this->getAccountingExportBatchId(),
			'activity_type' => $this->getActivityType(),
			'batch_snapshot_details' => $this->getBatchSnapshotDetails(),
			'activity_datetime' => $this->getActivityDatetime(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>