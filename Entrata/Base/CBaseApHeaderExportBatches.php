<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApHeaderExportBatches
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApHeaderExportBatches extends CEosPluralBase {

	/**
	 * @return CApHeaderExportBatch[]
	 */
	public static function fetchApHeaderExportBatches( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApHeaderExportBatch::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApHeaderExportBatch
	 */
	public static function fetchApHeaderExportBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApHeaderExportBatch::class, $objDatabase );
	}

	public static function fetchApHeaderExportBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_header_export_batches', $objDatabase );
	}

	public static function fetchApHeaderExportBatchByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApHeaderExportBatch( sprintf( 'SELECT * FROM ap_header_export_batches WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderExportBatchesByCid( $intCid, $objDatabase ) {
		return self::fetchApHeaderExportBatches( sprintf( 'SELECT * FROM ap_header_export_batches WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchApHeaderExportBatchesByScheduledApHeaderExportBatchIdByCid( $intScheduledApHeaderExportBatchId, $intCid, $objDatabase ) {
		return self::fetchApHeaderExportBatches( sprintf( 'SELECT * FROM ap_header_export_batches WHERE scheduled_ap_header_export_batch_id = %d AND cid = %d', $intScheduledApHeaderExportBatchId, $intCid ), $objDatabase );
	}

	public static function fetchApHeaderExportBatchesByApHeaderExportFileFormatTypeIdByCid( $intApHeaderExportFileFormatTypeId, $intCid, $objDatabase ) {
		return self::fetchApHeaderExportBatches( sprintf( 'SELECT * FROM ap_header_export_batches WHERE ap_header_export_file_format_type_id = %d AND cid = %d', $intApHeaderExportFileFormatTypeId, $intCid ), $objDatabase );
	}

}
?>