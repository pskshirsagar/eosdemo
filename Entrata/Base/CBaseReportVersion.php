<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseReportVersion extends CEosSingularBase {

	const TABLE_NAME = 'public.report_versions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDefaultReportVersionId;
	protected $m_intReportId;
	protected $m_intMajor;
	protected $m_intMinor;
	protected $m_strTitleAddendum;
	protected $m_strDefinition;
	protected $m_strExpiration;
	protected $m_boolIsLatest;
	protected $m_boolIsDefault;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intMajor = '1';
		$this->m_intMinor = '0';
		$this->m_boolIsLatest = true;
		$this->m_boolIsDefault = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['default_report_version_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultReportVersionId', trim( $arrValues['default_report_version_id'] ) ); elseif( isset( $arrValues['default_report_version_id'] ) ) $this->setDefaultReportVersionId( $arrValues['default_report_version_id'] );
		if( isset( $arrValues['report_id'] ) && $boolDirectSet ) $this->set( 'm_intReportId', trim( $arrValues['report_id'] ) ); elseif( isset( $arrValues['report_id'] ) ) $this->setReportId( $arrValues['report_id'] );
		if( isset( $arrValues['major'] ) && $boolDirectSet ) $this->set( 'm_intMajor', trim( $arrValues['major'] ) ); elseif( isset( $arrValues['major'] ) ) $this->setMajor( $arrValues['major'] );
		if( isset( $arrValues['minor'] ) && $boolDirectSet ) $this->set( 'm_intMinor', trim( $arrValues['minor'] ) ); elseif( isset( $arrValues['minor'] ) ) $this->setMinor( $arrValues['minor'] );
		if( isset( $arrValues['title_addendum'] ) && $boolDirectSet ) $this->set( 'm_strTitleAddendum', trim( stripcslashes( $arrValues['title_addendum'] ) ) ); elseif( isset( $arrValues['title_addendum'] ) ) $this->setTitleAddendum( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title_addendum'] ) : $arrValues['title_addendum'] );
		if( isset( $arrValues['definition'] ) && $boolDirectSet ) $this->set( 'definition', trim( stripcslashes( $arrValues['definition'] ) ) ); elseif( isset( $arrValues['definition'] ) ) $this->setDefinition( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['definition'] ) : $arrValues['definition'] );
		if( isset( $arrValues['expiration'] ) && $boolDirectSet ) $this->set( 'm_strExpiration', trim( $arrValues['expiration'] ) ); elseif( isset( $arrValues['expiration'] ) ) $this->setExpiration( $arrValues['expiration'] );
		if( isset( $arrValues['is_latest'] ) && $boolDirectSet ) $this->set( 'm_boolIsLatest', trim( stripcslashes( $arrValues['is_latest'] ) ) ); elseif( isset( $arrValues['is_latest'] ) ) $this->setIsLatest( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_latest'] ) : $arrValues['is_latest'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_boolIsDefault', trim( stripcslashes( $arrValues['is_default'] ) ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_default'] ) : $arrValues['is_default'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDefaultReportVersionId( $intDefaultReportVersionId ) {
		$this->set( 'm_intDefaultReportVersionId', CStrings::strToIntDef( $intDefaultReportVersionId, NULL, false ) );
	}

	public function getDefaultReportVersionId() {
		return $this->m_intDefaultReportVersionId;
	}

	public function sqlDefaultReportVersionId() {
		return ( true == isset( $this->m_intDefaultReportVersionId ) ) ? ( string ) $this->m_intDefaultReportVersionId : 'NULL';
	}

	public function setReportId( $intReportId ) {
		$this->set( 'm_intReportId', CStrings::strToIntDef( $intReportId, NULL, false ) );
	}

	public function getReportId() {
		return $this->m_intReportId;
	}

	public function sqlReportId() {
		return ( true == isset( $this->m_intReportId ) ) ? ( string ) $this->m_intReportId : 'NULL';
	}

	public function setMajor( $intMajor ) {
		$this->set( 'm_intMajor', CStrings::strToIntDef( $intMajor, NULL, false ) );
	}

	public function getMajor() {
		return $this->m_intMajor;
	}

	public function sqlMajor() {
		return ( true == isset( $this->m_intMajor ) ) ? ( string ) $this->m_intMajor : '1';
	}

	public function setMinor( $intMinor ) {
		$this->set( 'm_intMinor', CStrings::strToIntDef( $intMinor, NULL, false ) );
	}

	public function getMinor() {
		return $this->m_intMinor;
	}

	public function sqlMinor() {
		return ( true == isset( $this->m_intMinor ) ) ? ( string ) $this->m_intMinor : '0';
	}

	public function setTitleAddendum( $strTitleAddendum ) {
		$this->set( 'm_strTitleAddendum', CStrings::strTrimDef( $strTitleAddendum, 240, NULL, true ) );
	}

	public function getTitleAddendum() {
		return $this->m_strTitleAddendum;
	}

	public function sqlTitleAddendum() {
		return ( true == isset( $this->m_strTitleAddendum ) ) ? '\'' . addslashes( $this->m_strTitleAddendum ) . '\'' : 'NULL';
	}

	public function setDefinition( $strDefinition ) {
		$this->set( 'm_strDefinition', CStrings::strTrimDef( $strDefinition, -1, NULL, true ) );
	}

	public function getDefinition() {
		return $this->m_strDefinition;
	}

	public function sqlDefinition() {
		return ( true == isset( $this->m_strDefinition ) ) ? '\'' . addslashes( $this->m_strDefinition ) . '\'' : 'NULL';
	}

	public function setExpiration( $strExpiration ) {
		$this->set( 'm_strExpiration', CStrings::strTrimDef( $strExpiration, -1, NULL, true ) );
	}

	public function getExpiration() {
		return $this->m_strExpiration;
	}

	public function sqlExpiration() {
		return ( true == isset( $this->m_strExpiration ) ) ? '\'' . $this->m_strExpiration . '\'' : 'NULL';
	}

	public function setIsLatest( $boolIsLatest ) {
		$this->set( 'm_boolIsLatest', CStrings::strToBool( $boolIsLatest ) );
	}

	public function getIsLatest() {
		return $this->m_boolIsLatest;
	}

	public function sqlIsLatest() {
		return ( true == isset( $this->m_boolIsLatest ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLatest ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDefault( $boolIsDefault ) {
		$this->set( 'm_boolIsDefault', CStrings::strToBool( $boolIsDefault ) );
	}

	public function getIsDefault() {
		return $this->m_boolIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_boolIsDefault ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDefault ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, default_report_version_id, report_id, major, minor, title_addendum, definition, expiration, is_latest, is_default, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlDefaultReportVersionId() . ', ' .
 						$this->sqlReportId() . ', ' .
 						$this->sqlMajor() . ', ' .
 						$this->sqlMinor() . ', ' .
 						$this->sqlTitleAddendum() . ', ' .
 						$this->sqlDefinition() . ', ' .
						$this->sqlExpiration() . ', ' .
						$this->sqlIsLatest() . ', ' .
						$this->sqlIsDefault() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
					' . static::TABLE_NAME . '
				SET ';
					if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
					if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_report_version_id = ' . $this->sqlDefaultReportVersionId() . ','; } elseif( true == array_key_exists( 'DefaultReportVersionId', $this->getChangedColumns() ) ) { $strSql .= ' default_report_version_id = ' . $this->sqlDefaultReportVersionId() . ','; $boolUpdate = true; }
					if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_id = ' . $this->sqlReportId() . ','; } elseif( true == array_key_exists( 'ReportId', $this->getChangedColumns() ) ) { $strSql .= ' report_id = ' . $this->sqlReportId() . ','; $boolUpdate = true; }
					if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' major = ' . $this->sqlMajor() . ','; } elseif( true == array_key_exists( 'Major', $this->getChangedColumns() ) ) { $strSql .= ' major = ' . $this->sqlMajor() . ','; $boolUpdate = true; }
					if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minor = ' . $this->sqlMinor() . ','; } elseif( true == array_key_exists( 'Minor', $this->getChangedColumns() ) ) { $strSql .= ' minor = ' . $this->sqlMinor() . ','; $boolUpdate = true; }
					if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title_addendum = ' . $this->sqlTitleAddendum() . ','; } elseif( true == array_key_exists( 'TitleAddendum', $this->getChangedColumns() ) ) { $strSql .= ' title_addendum = ' . $this->sqlTitleAddendum() . ','; $boolUpdate = true; }
					if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' definition = ' . $this->sqlDefinition() . ','; } elseif( true == array_key_exists( 'Definition', $this->getChangedColumns() ) ) { $strSql .= ' definition = ' . $this->sqlDefinition() . ','; $boolUpdate = true; }
					if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expiration = ' . $this->sqlExpiration() . ','; } elseif( true == array_key_exists( 'Expiration', $this->getChangedColumns() ) ) { $strSql .= ' expiration = ' . $this->sqlExpiration() . ','; $boolUpdate = true; }
					if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_latest = ' . $this->sqlIsLatest() . ','; } elseif( true == array_key_exists( 'IsLatest', $this->getChangedColumns() ) ) { $strSql .= ' is_latest = ' . $this->sqlIsLatest() . ','; $boolUpdate = true; }
					if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; } elseif( true == array_key_exists( 'IsDefault', $this->getChangedColumns() ) ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
					$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
					$strSql .= ' updated_on = \'NOW()\'
				WHERE
					id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'default_report_version_id' => $this->getDefaultReportVersionId(),
			'report_id' => $this->getReportId(),
			'major' => $this->getMajor(),
			'minor' => $this->getMinor(),
			'title_addendum' => $this->getTitleAddendum(),
			'definition' => $this->getDefinition(),
			'expiration' => $this->getExpiration(),
			'is_latest' => $this->getIsLatest(),
			'is_default' => $this->getIsDefault(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>