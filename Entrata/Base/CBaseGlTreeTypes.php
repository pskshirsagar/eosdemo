<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlTreeTypes
 * Do not add any new functions to this class.
 */

class CBaseGlTreeTypes extends CEosPluralBase {

	/**
	 * @return CGlTreeType[]
	 */
	public static function fetchGlTreeTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CGlTreeType::class, $objDatabase );
	}

	/**
	 * @return CGlTreeType
	 */
	public static function fetchGlTreeType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGlTreeType::class, $objDatabase );
	}

	public static function fetchGlTreeTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_tree_types', $objDatabase );
	}

	public static function fetchGlTreeTypeById( $intId, $objDatabase ) {
		return self::fetchGlTreeType( sprintf( 'SELECT * FROM gl_tree_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>