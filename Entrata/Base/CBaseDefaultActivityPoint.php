<?php

class CBaseDefaultActivityPoint extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.default_activity_points';

	protected $m_intId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intNoBadgePoints;
	protected $m_intBaseBadgePoints;
	protected $m_intProBadgePoints;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intNoBadgePoints = '0';
		$this->m_intBaseBadgePoints = '0';
		$this->m_intProBadgePoints = '0';
		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['no_badge_points'] ) && $boolDirectSet ) $this->set( 'm_intNoBadgePoints', trim( $arrValues['no_badge_points'] ) ); elseif( isset( $arrValues['no_badge_points'] ) ) $this->setNoBadgePoints( $arrValues['no_badge_points'] );
		if( isset( $arrValues['base_badge_points'] ) && $boolDirectSet ) $this->set( 'm_intBaseBadgePoints', trim( $arrValues['base_badge_points'] ) ); elseif( isset( $arrValues['base_badge_points'] ) ) $this->setBaseBadgePoints( $arrValues['base_badge_points'] );
		if( isset( $arrValues['pro_badge_points'] ) && $boolDirectSet ) $this->set( 'm_intProBadgePoints', trim( $arrValues['pro_badge_points'] ) ); elseif( isset( $arrValues['pro_badge_points'] ) ) $this->setProBadgePoints( $arrValues['pro_badge_points'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setNoBadgePoints( $intNoBadgePoints ) {
		$this->set( 'm_intNoBadgePoints', CStrings::strToIntDef( $intNoBadgePoints, NULL, false ) );
	}

	public function getNoBadgePoints() {
		return $this->m_intNoBadgePoints;
	}

	public function sqlNoBadgePoints() {
		return ( true == isset( $this->m_intNoBadgePoints ) ) ? ( string ) $this->m_intNoBadgePoints : '0';
	}

	public function setBaseBadgePoints( $intBaseBadgePoints ) {
		$this->set( 'm_intBaseBadgePoints', CStrings::strToIntDef( $intBaseBadgePoints, NULL, false ) );
	}

	public function getBaseBadgePoints() {
		return $this->m_intBaseBadgePoints;
	}

	public function sqlBaseBadgePoints() {
		return ( true == isset( $this->m_intBaseBadgePoints ) ) ? ( string ) $this->m_intBaseBadgePoints : '0';
	}

	public function setProBadgePoints( $intProBadgePoints ) {
		$this->set( 'm_intProBadgePoints', CStrings::strToIntDef( $intProBadgePoints, NULL, false ) );
	}

	public function getProBadgePoints() {
		return $this->m_intProBadgePoints;
	}

	public function sqlProBadgePoints() {
		return ( true == isset( $this->m_intProBadgePoints ) ) ? ( string ) $this->m_intProBadgePoints : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'no_badge_points' => $this->getNoBadgePoints(),
			'base_badge_points' => $this->getBaseBadgePoints(),
			'pro_badge_points' => $this->getProBadgePoints(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'details' => $this->getDetails()
		);
	}

}
?>