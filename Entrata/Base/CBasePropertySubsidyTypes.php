<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertySubsidyTypes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertySubsidyTypes extends CEosPluralBase {

	/**
	 * @return CPropertySubsidyType[]
	 */
	public static function fetchPropertySubsidyTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CPropertySubsidyType', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CPropertySubsidyType
	 */
	public static function fetchPropertySubsidyType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertySubsidyType', $objDatabase );
	}

	public static function fetchPropertySubsidyTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_subsidy_types', $objDatabase );
	}

	public static function fetchPropertySubsidyTypeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchPropertySubsidyType( sprintf( 'SELECT * FROM property_subsidy_types WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertySubsidyTypesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertySubsidyTypes( sprintf( 'SELECT * FROM property_subsidy_types WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertySubsidyTypesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertySubsidyTypes( sprintf( 'SELECT * FROM property_subsidy_types WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertySubsidyTypesBySubsidyTypeIdByCid( $intSubsidyTypeId, $intCid, $objDatabase ) {
		return self::fetchPropertySubsidyTypes( sprintf( 'SELECT * FROM property_subsidy_types WHERE subsidy_type_id = %d AND cid = %d', ( int ) $intSubsidyTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertySubsidyTypesBySubsidyIncomeLimitVersionIdByCid( $intSubsidyIncomeLimitVersionId, $intCid, $objDatabase ) {
		return self::fetchPropertySubsidyTypes( sprintf( 'SELECT * FROM property_subsidy_types WHERE subsidy_income_limit_version_id = %d AND cid = %d', ( int ) $intSubsidyIncomeLimitVersionId, ( int ) $intCid ), $objDatabase );
	}

}
?>