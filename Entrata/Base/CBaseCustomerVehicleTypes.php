<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerVehicleTypes
 * Do not add any new functions to this class.
 */

class CBaseCustomerVehicleTypes extends CEosPluralBase {

	/**
	 * @return CCustomerVehicleType[]
	 */
	public static function fetchCustomerVehicleTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCustomerVehicleType::class, $objDatabase );
	}

	/**
	 * @return CCustomerVehicleType
	 */
	public static function fetchCustomerVehicleType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerVehicleType::class, $objDatabase );
	}

	public static function fetchCustomerVehicleTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_vehicle_types', $objDatabase );
	}

	public static function fetchCustomerVehicleTypeById( $intId, $objDatabase ) {
		return self::fetchCustomerVehicleType( sprintf( 'SELECT * FROM customer_vehicle_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>