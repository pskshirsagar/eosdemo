<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueUnitTypePreferences
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseRevenueUnitTypePreferences extends CEosPluralBase {

	/**
	 * @return CRevenueUnitTypePreference[]
	 */
	public static function fetchRevenueUnitTypePreferences( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CRevenueUnitTypePreference', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CRevenueUnitTypePreference
	 */
	public static function fetchRevenueUnitTypePreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRevenueUnitTypePreference', $objDatabase );
	}

	public static function fetchRevenueUnitTypePreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_unit_type_preferences', $objDatabase );
	}

	public static function fetchRevenueUnitTypePreferenceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRevenueUnitTypePreference( sprintf( 'SELECT * FROM revenue_unit_type_preferences WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueUnitTypePreferencesByCid( $intCid, $objDatabase ) {
		return self::fetchRevenueUnitTypePreferences( sprintf( 'SELECT * FROM revenue_unit_type_preferences WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueUnitTypePreferencesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchRevenueUnitTypePreferences( sprintf( 'SELECT * FROM revenue_unit_type_preferences WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueUnitTypePreferencesByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchRevenueUnitTypePreferences( sprintf( 'SELECT * FROM revenue_unit_type_preferences WHERE unit_type_id = %d AND cid = %d', ( int ) $intUnitTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueUnitTypePreferencesByPropertySettingKeyIdByCid( $intPropertySettingKeyId, $intCid, $objDatabase ) {
		return self::fetchRevenueUnitTypePreferences( sprintf( 'SELECT * FROM revenue_unit_type_preferences WHERE property_setting_key_id = %d AND cid = %d', ( int ) $intPropertySettingKeyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueUnitTypePreferencesByPropertyPreferenceTypeIdByCid( $intPropertyPreferenceTypeId, $intCid, $objDatabase ) {
		return self::fetchRevenueUnitTypePreferences( sprintf( 'SELECT * FROM revenue_unit_type_preferences WHERE property_preference_type_id = %d AND cid = %d', ( int ) $intPropertyPreferenceTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueUnitTypePreferencesByUnitTypeIdByPropertyIdByCid( $intUnitTypeId, $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchRevenueUnitTypePreferences( sprintf( 'SELECT * FROM revenue_unit_type_preferences WHERE unit_type_id = %d AND  property_id = %d AND cid = %d', ( int ) $intUnitTypeId, ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}
}
?>