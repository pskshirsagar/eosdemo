<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceRequestLogs
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceRequestLogs extends CEosPluralBase {

	/**
	 * @return CMaintenanceRequestLog[]
	 */
	public static function fetchMaintenanceRequestLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CMaintenanceRequestLog', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenanceRequestLog
	 */
	public static function fetchMaintenanceRequestLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMaintenanceRequestLog', $objDatabase );
	}

	public static function fetchMaintenanceRequestLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_request_logs', $objDatabase );
	}

	public static function fetchMaintenanceRequestLogByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLog( sprintf( 'SELECT * FROM maintenance_request_logs WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLogsByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLogs( sprintf( 'SELECT * FROM maintenance_request_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLogsByMaintenanceRequestIdByCid( $intMaintenanceRequestId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLogs( sprintf( 'SELECT * FROM maintenance_request_logs WHERE maintenance_request_id = %d AND cid = %d', ( int ) $intMaintenanceRequestId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLogsByMaintenanceRequestTypeIdByCid( $intMaintenanceRequestTypeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLogs( sprintf( 'SELECT * FROM maintenance_request_logs WHERE maintenance_request_type_id = %d AND cid = %d', ( int ) $intMaintenanceRequestTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLogsByPriorMaintenanceRequestLogIdByCid( $intPriorMaintenanceRequestLogId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLogs( sprintf( 'SELECT * FROM maintenance_request_logs WHERE prior_maintenance_request_log_id = %d AND cid = %d', ( int ) $intPriorMaintenanceRequestLogId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLogsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLogs( sprintf( 'SELECT * FROM maintenance_request_logs WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLogsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLogs( sprintf( 'SELECT * FROM maintenance_request_logs WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLogsByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLogs( sprintf( 'SELECT * FROM maintenance_request_logs WHERE property_unit_id = %d AND cid = %d', ( int ) $intPropertyUnitId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLogsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLogs( sprintf( 'SELECT * FROM maintenance_request_logs WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLogsByPeriodIdByCid( $intPeriodId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLogs( sprintf( 'SELECT * FROM maintenance_request_logs WHERE period_id = %d AND cid = %d', ( int ) $intPeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLogsByReportingPeriodIdByCid( $intReportingPeriodId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLogs( sprintf( 'SELECT * FROM maintenance_request_logs WHERE reporting_period_id = %d AND cid = %d', ( int ) $intReportingPeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLogsByEffectivePeriodIdByCid( $intEffectivePeriodId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLogs( sprintf( 'SELECT * FROM maintenance_request_logs WHERE effective_period_id = %d AND cid = %d', ( int ) $intEffectivePeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLogsByOriginalPeriodIdByCid( $intOriginalPeriodId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLogs( sprintf( 'SELECT * FROM maintenance_request_logs WHERE original_period_id = %d AND cid = %d', ( int ) $intOriginalPeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLogsByOldMaintenanceStatusIdByCid( $intOldMaintenanceStatusId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLogs( sprintf( 'SELECT * FROM maintenance_request_logs WHERE old_maintenance_status_id = %d AND cid = %d', ( int ) $intOldMaintenanceStatusId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLogsByMaintenanceStatusIdByCid( $intMaintenanceStatusId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLogs( sprintf( 'SELECT * FROM maintenance_request_logs WHERE maintenance_status_id = %d AND cid = %d', ( int ) $intMaintenanceStatusId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLogsByOldMaintenanceStatusTypeIdByCid( $intOldMaintenanceStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLogs( sprintf( 'SELECT * FROM maintenance_request_logs WHERE old_maintenance_status_type_id = %d AND cid = %d', ( int ) $intOldMaintenanceStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLogsByMaintenanceStatusTypeIdByCid( $intMaintenanceStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLogs( sprintf( 'SELECT * FROM maintenance_request_logs WHERE maintenance_status_type_id = %d AND cid = %d', ( int ) $intMaintenanceStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLogsByOldMaintenancePriorityIdByCid( $intOldMaintenancePriorityId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLogs( sprintf( 'SELECT * FROM maintenance_request_logs WHERE old_maintenance_priority_id = %d AND cid = %d', ( int ) $intOldMaintenancePriorityId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceRequestLogsByMaintenancePriorityIdByCid( $intMaintenancePriorityId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceRequestLogs( sprintf( 'SELECT * FROM maintenance_request_logs WHERE maintenance_priority_id = %d AND cid = %d', ( int ) $intMaintenancePriorityId, ( int ) $intCid ), $objDatabase );
	}

}
?>