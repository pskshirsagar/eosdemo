<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CNotificationModules
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseNotificationModules extends CEosPluralBase {

	/**
	 * @return CNotificationModule[]
	 */
	public static function fetchNotificationModules( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CNotificationModule', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CNotificationModule
	 */
	public static function fetchNotificationModule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CNotificationModule', $objDatabase );
	}

	public static function fetchNotificationModuleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'notification_modules', $objDatabase );
	}

	public static function fetchNotificationModuleByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchNotificationModule( sprintf( 'SELECT * FROM notification_modules WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchNotificationModulesByCid( $intCid, $objDatabase ) {
		return self::fetchNotificationModules( sprintf( 'SELECT * FROM notification_modules WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchNotificationModulesByNotificationIdByCid( $intNotificationId, $intCid, $objDatabase ) {
		return self::fetchNotificationModules( sprintf( 'SELECT * FROM notification_modules WHERE notification_id = %d AND cid = %d', ( int ) $intNotificationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchNotificationModulesByModuleIdByCid( $intModuleId, $intCid, $objDatabase ) {
		return self::fetchNotificationModules( sprintf( 'SELECT * FROM notification_modules WHERE module_id = %d AND cid = %d', ( int ) $intModuleId, ( int ) $intCid ), $objDatabase );
	}

}
?>