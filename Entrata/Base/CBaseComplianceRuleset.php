<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseComplianceRuleset extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.compliance_rulesets';

	protected $m_intId;
	protected $m_intCid;
	protected $m_strName;
	protected $m_strNotes;
	protected $m_strRequireWaiverOfSubrogationDescription;
	protected $m_intPriorityNum;
	protected $m_boolIncludeAllProperties;
	protected $m_boolIncludeAllCategories;
	protected $m_boolIncludeAllVendors;
	protected $m_boolIsOnsite;
	protected $m_boolRequireWaiverOfSubrogation;
	protected $m_boolAllowNotCompliantPurchaseOrders;
	protected $m_boolAllowRequiredPurchaseOrders;
	protected $m_boolAllowPendingPurchaseOrders;
	protected $m_boolAllowNotCompliantInvoices;
	protected $m_boolAllowRequiredInvoices;
	protected $m_boolAllowPendingInvoices;
	protected $m_boolAllowNotCompliantPayments;
	protected $m_boolAllowRequiredPayments;
	protected $m_boolAllowPendingPayments;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_boolIsIncludeAllOwnerTypes;

	public function __construct() {
		parent::__construct();

		$this->m_intPriorityNum = '1';
		$this->m_boolIncludeAllProperties = true;
		$this->m_boolIncludeAllCategories = true;
		$this->m_boolIncludeAllVendors = true;
		$this->m_boolIsOnsite = false;
		$this->m_boolRequireWaiverOfSubrogation = false;
		$this->m_boolAllowNotCompliantPurchaseOrders = false;
		$this->m_boolAllowRequiredPurchaseOrders = false;
		$this->m_boolAllowPendingPurchaseOrders = false;
		$this->m_boolAllowNotCompliantInvoices = false;
		$this->m_boolAllowRequiredInvoices = false;
		$this->m_boolAllowPendingInvoices = false;
		$this->m_boolAllowNotCompliantPayments = false;
		$this->m_boolAllowRequiredPayments = false;
		$this->m_boolAllowPendingPayments = false;
		$this->m_boolIsIncludeAllOwnerTypes = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['notes'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['require_waiver_of_subrogation_description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strRequireWaiverOfSubrogationDescription', trim( $arrValues['require_waiver_of_subrogation_description'] ) ); elseif( isset( $arrValues['require_waiver_of_subrogation_description'] ) ) $this->setRequireWaiverOfSubrogationDescription( $arrValues['require_waiver_of_subrogation_description'] );
		if( isset( $arrValues['priority_num'] ) && $boolDirectSet ) $this->set( 'm_intPriorityNum', trim( $arrValues['priority_num'] ) ); elseif( isset( $arrValues['priority_num'] ) ) $this->setPriorityNum( $arrValues['priority_num'] );
		if( isset( $arrValues['include_all_properties'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeAllProperties', trim( stripcslashes( $arrValues['include_all_properties'] ) ) ); elseif( isset( $arrValues['include_all_properties'] ) ) $this->setIncludeAllProperties( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_all_properties'] ) : $arrValues['include_all_properties'] );
		if( isset( $arrValues['include_all_categories'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeAllCategories', trim( stripcslashes( $arrValues['include_all_categories'] ) ) ); elseif( isset( $arrValues['include_all_categories'] ) ) $this->setIncludeAllCategories( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_all_categories'] ) : $arrValues['include_all_categories'] );
		if( isset( $arrValues['include_all_vendors'] ) && $boolDirectSet ) $this->set( 'm_boolIncludeAllVendors', trim( stripcslashes( $arrValues['include_all_vendors'] ) ) ); elseif( isset( $arrValues['include_all_vendors'] ) ) $this->setIncludeAllVendors( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['include_all_vendors'] ) : $arrValues['include_all_vendors'] );
		if( isset( $arrValues['is_onsite'] ) && $boolDirectSet ) $this->set( 'm_boolIsOnsite', trim( stripcslashes( $arrValues['is_onsite'] ) ) ); elseif( isset( $arrValues['is_onsite'] ) ) $this->setIsOnsite( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_onsite'] ) : $arrValues['is_onsite'] );
		if( isset( $arrValues['require_waiver_of_subrogation'] ) && $boolDirectSet ) $this->set( 'm_boolRequireWaiverOfSubrogation', trim( stripcslashes( $arrValues['require_waiver_of_subrogation'] ) ) ); elseif( isset( $arrValues['require_waiver_of_subrogation'] ) ) $this->setRequireWaiverOfSubrogation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['require_waiver_of_subrogation'] ) : $arrValues['require_waiver_of_subrogation'] );
		if( isset( $arrValues['allow_not_compliant_purchase_orders'] ) && $boolDirectSet ) $this->set( 'm_boolAllowNotCompliantPurchaseOrders', trim( stripcslashes( $arrValues['allow_not_compliant_purchase_orders'] ) ) ); elseif( isset( $arrValues['allow_not_compliant_purchase_orders'] ) ) $this->setAllowNotCompliantPurchaseOrders( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_not_compliant_purchase_orders'] ) : $arrValues['allow_not_compliant_purchase_orders'] );
		if( isset( $arrValues['allow_required_purchase_orders'] ) && $boolDirectSet ) $this->set( 'm_boolAllowRequiredPurchaseOrders', trim( stripcslashes( $arrValues['allow_required_purchase_orders'] ) ) ); elseif( isset( $arrValues['allow_required_purchase_orders'] ) ) $this->setAllowRequiredPurchaseOrders( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_required_purchase_orders'] ) : $arrValues['allow_required_purchase_orders'] );
		if( isset( $arrValues['allow_pending_purchase_orders'] ) && $boolDirectSet ) $this->set( 'm_boolAllowPendingPurchaseOrders', trim( stripcslashes( $arrValues['allow_pending_purchase_orders'] ) ) ); elseif( isset( $arrValues['allow_pending_purchase_orders'] ) ) $this->setAllowPendingPurchaseOrders( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_pending_purchase_orders'] ) : $arrValues['allow_pending_purchase_orders'] );
		if( isset( $arrValues['allow_not_compliant_invoices'] ) && $boolDirectSet ) $this->set( 'm_boolAllowNotCompliantInvoices', trim( stripcslashes( $arrValues['allow_not_compliant_invoices'] ) ) ); elseif( isset( $arrValues['allow_not_compliant_invoices'] ) ) $this->setAllowNotCompliantInvoices( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_not_compliant_invoices'] ) : $arrValues['allow_not_compliant_invoices'] );
		if( isset( $arrValues['allow_required_invoices'] ) && $boolDirectSet ) $this->set( 'm_boolAllowRequiredInvoices', trim( stripcslashes( $arrValues['allow_required_invoices'] ) ) ); elseif( isset( $arrValues['allow_required_invoices'] ) ) $this->setAllowRequiredInvoices( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_required_invoices'] ) : $arrValues['allow_required_invoices'] );
		if( isset( $arrValues['allow_pending_invoices'] ) && $boolDirectSet ) $this->set( 'm_boolAllowPendingInvoices', trim( stripcslashes( $arrValues['allow_pending_invoices'] ) ) ); elseif( isset( $arrValues['allow_pending_invoices'] ) ) $this->setAllowPendingInvoices( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_pending_invoices'] ) : $arrValues['allow_pending_invoices'] );
		if( isset( $arrValues['allow_not_compliant_payments'] ) && $boolDirectSet ) $this->set( 'm_boolAllowNotCompliantPayments', trim( stripcslashes( $arrValues['allow_not_compliant_payments'] ) ) ); elseif( isset( $arrValues['allow_not_compliant_payments'] ) ) $this->setAllowNotCompliantPayments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_not_compliant_payments'] ) : $arrValues['allow_not_compliant_payments'] );
		if( isset( $arrValues['allow_required_payments'] ) && $boolDirectSet ) $this->set( 'm_boolAllowRequiredPayments', trim( stripcslashes( $arrValues['allow_required_payments'] ) ) ); elseif( isset( $arrValues['allow_required_payments'] ) ) $this->setAllowRequiredPayments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_required_payments'] ) : $arrValues['allow_required_payments'] );
		if( isset( $arrValues['allow_pending_payments'] ) && $boolDirectSet ) $this->set( 'm_boolAllowPendingPayments', trim( stripcslashes( $arrValues['allow_pending_payments'] ) ) ); elseif( isset( $arrValues['allow_pending_payments'] ) ) $this->setAllowPendingPayments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_pending_payments'] ) : $arrValues['allow_pending_payments'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['is_include_all_owner_types'] ) && $boolDirectSet ) $this->set( 'm_boolIsIncludeAllOwnerTypes', trim( stripcslashes( $arrValues['is_include_all_owner_types'] ) ) ); elseif( isset( $arrValues['is_include_all_owner_types'] ) ) $this->setIsIncludeAllOwnerTypes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_include_all_owner_types'] ) : $arrValues['is_include_all_owner_types'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setNotes( $strNotes, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ), $strLocaleCode );
	}

	public function getNotes( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strNotes', $strLocaleCode );
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setRequireWaiverOfSubrogationDescription( $strRequireWaiverOfSubrogationDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strRequireWaiverOfSubrogationDescription', CStrings::strTrimDef( $strRequireWaiverOfSubrogationDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getRequireWaiverOfSubrogationDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strRequireWaiverOfSubrogationDescription', $strLocaleCode );
	}

	public function sqlRequireWaiverOfSubrogationDescription() {
		return ( true == isset( $this->m_strRequireWaiverOfSubrogationDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRequireWaiverOfSubrogationDescription ) : '\'' . addslashes( $this->m_strRequireWaiverOfSubrogationDescription ) . '\'' ) : 'NULL';
	}

	public function setPriorityNum( $intPriorityNum ) {
		$this->set( 'm_intPriorityNum', CStrings::strToIntDef( $intPriorityNum, NULL, false ) );
	}

	public function getPriorityNum() {
		return $this->m_intPriorityNum;
	}

	public function sqlPriorityNum() {
		return ( true == isset( $this->m_intPriorityNum ) ) ? ( string ) $this->m_intPriorityNum : '1';
	}

	public function setIncludeAllProperties( $boolIncludeAllProperties ) {
		$this->set( 'm_boolIncludeAllProperties', CStrings::strToBool( $boolIncludeAllProperties ) );
	}

	public function getIncludeAllProperties() {
		return $this->m_boolIncludeAllProperties;
	}

	public function sqlIncludeAllProperties() {
		return ( true == isset( $this->m_boolIncludeAllProperties ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeAllProperties ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIncludeAllCategories( $boolIncludeAllCategories ) {
		$this->set( 'm_boolIncludeAllCategories', CStrings::strToBool( $boolIncludeAllCategories ) );
	}

	public function getIncludeAllCategories() {
		return $this->m_boolIncludeAllCategories;
	}

	public function sqlIncludeAllCategories() {
		return ( true == isset( $this->m_boolIncludeAllCategories ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeAllCategories ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIncludeAllVendors( $boolIncludeAllVendors ) {
		$this->set( 'm_boolIncludeAllVendors', CStrings::strToBool( $boolIncludeAllVendors ) );
	}

	public function getIncludeAllVendors() {
		return $this->m_boolIncludeAllVendors;
	}

	public function sqlIncludeAllVendors() {
		return ( true == isset( $this->m_boolIncludeAllVendors ) ) ? '\'' . ( true == ( bool ) $this->m_boolIncludeAllVendors ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsOnsite( $boolIsOnsite ) {
		$this->set( 'm_boolIsOnsite', CStrings::strToBool( $boolIsOnsite ) );
	}

	public function getIsOnsite() {
		return $this->m_boolIsOnsite;
	}

	public function sqlIsOnsite() {
		return ( true == isset( $this->m_boolIsOnsite ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOnsite ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequireWaiverOfSubrogation( $boolRequireWaiverOfSubrogation ) {
		$this->set( 'm_boolRequireWaiverOfSubrogation', CStrings::strToBool( $boolRequireWaiverOfSubrogation ) );
	}

	public function getRequireWaiverOfSubrogation() {
		return $this->m_boolRequireWaiverOfSubrogation;
	}

	public function sqlRequireWaiverOfSubrogation() {
		return ( true == isset( $this->m_boolRequireWaiverOfSubrogation ) ) ? '\'' . ( true == ( bool ) $this->m_boolRequireWaiverOfSubrogation ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowNotCompliantPurchaseOrders( $boolAllowNotCompliantPurchaseOrders ) {
		$this->set( 'm_boolAllowNotCompliantPurchaseOrders', CStrings::strToBool( $boolAllowNotCompliantPurchaseOrders ) );
	}

	public function getAllowNotCompliantPurchaseOrders() {
		return $this->m_boolAllowNotCompliantPurchaseOrders;
	}

	public function sqlAllowNotCompliantPurchaseOrders() {
		return ( true == isset( $this->m_boolAllowNotCompliantPurchaseOrders ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowNotCompliantPurchaseOrders ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowRequiredPurchaseOrders( $boolAllowRequiredPurchaseOrders ) {
		$this->set( 'm_boolAllowRequiredPurchaseOrders', CStrings::strToBool( $boolAllowRequiredPurchaseOrders ) );
	}

	public function getAllowRequiredPurchaseOrders() {
		return $this->m_boolAllowRequiredPurchaseOrders;
	}

	public function sqlAllowRequiredPurchaseOrders() {
		return ( true == isset( $this->m_boolAllowRequiredPurchaseOrders ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowRequiredPurchaseOrders ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowPendingPurchaseOrders( $boolAllowPendingPurchaseOrders ) {
		$this->set( 'm_boolAllowPendingPurchaseOrders', CStrings::strToBool( $boolAllowPendingPurchaseOrders ) );
	}

	public function getAllowPendingPurchaseOrders() {
		return $this->m_boolAllowPendingPurchaseOrders;
	}

	public function sqlAllowPendingPurchaseOrders() {
		return ( true == isset( $this->m_boolAllowPendingPurchaseOrders ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowPendingPurchaseOrders ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowNotCompliantInvoices( $boolAllowNotCompliantInvoices ) {
		$this->set( 'm_boolAllowNotCompliantInvoices', CStrings::strToBool( $boolAllowNotCompliantInvoices ) );
	}

	public function getAllowNotCompliantInvoices() {
		return $this->m_boolAllowNotCompliantInvoices;
	}

	public function sqlAllowNotCompliantInvoices() {
		return ( true == isset( $this->m_boolAllowNotCompliantInvoices ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowNotCompliantInvoices ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowRequiredInvoices( $boolAllowRequiredInvoices ) {
		$this->set( 'm_boolAllowRequiredInvoices', CStrings::strToBool( $boolAllowRequiredInvoices ) );
	}

	public function getAllowRequiredInvoices() {
		return $this->m_boolAllowRequiredInvoices;
	}

	public function sqlAllowRequiredInvoices() {
		return ( true == isset( $this->m_boolAllowRequiredInvoices ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowRequiredInvoices ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowPendingInvoices( $boolAllowPendingInvoices ) {
		$this->set( 'm_boolAllowPendingInvoices', CStrings::strToBool( $boolAllowPendingInvoices ) );
	}

	public function getAllowPendingInvoices() {
		return $this->m_boolAllowPendingInvoices;
	}

	public function sqlAllowPendingInvoices() {
		return ( true == isset( $this->m_boolAllowPendingInvoices ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowPendingInvoices ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowNotCompliantPayments( $boolAllowNotCompliantPayments ) {
		$this->set( 'm_boolAllowNotCompliantPayments', CStrings::strToBool( $boolAllowNotCompliantPayments ) );
	}

	public function getAllowNotCompliantPayments() {
		return $this->m_boolAllowNotCompliantPayments;
	}

	public function sqlAllowNotCompliantPayments() {
		return ( true == isset( $this->m_boolAllowNotCompliantPayments ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowNotCompliantPayments ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowRequiredPayments( $boolAllowRequiredPayments ) {
		$this->set( 'm_boolAllowRequiredPayments', CStrings::strToBool( $boolAllowRequiredPayments ) );
	}

	public function getAllowRequiredPayments() {
		return $this->m_boolAllowRequiredPayments;
	}

	public function sqlAllowRequiredPayments() {
		return ( true == isset( $this->m_boolAllowRequiredPayments ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowRequiredPayments ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowPendingPayments( $boolAllowPendingPayments ) {
		$this->set( 'm_boolAllowPendingPayments', CStrings::strToBool( $boolAllowPendingPayments ) );
	}

	public function getAllowPendingPayments() {
		return $this->m_boolAllowPendingPayments;
	}

	public function sqlAllowPendingPayments() {
		return ( true == isset( $this->m_boolAllowPendingPayments ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowPendingPayments ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsIncludeAllOwnerTypes( $boolIsIncludeAllOwnerTypes ) {
		$this->set( 'm_boolIsIncludeAllOwnerTypes', CStrings::strToBool( $boolIsIncludeAllOwnerTypes ) );
	}

	public function getIsIncludeAllOwnerTypes() {
		return $this->m_boolIsIncludeAllOwnerTypes;
	}

	public function sqlIsIncludeAllOwnerTypes() {
		return ( true == isset( $this->m_boolIsIncludeAllOwnerTypes ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsIncludeAllOwnerTypes ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, name, notes, require_waiver_of_subrogation_description, priority_num, include_all_properties, include_all_categories, include_all_vendors, is_onsite, require_waiver_of_subrogation, allow_not_compliant_purchase_orders, allow_required_purchase_orders, allow_pending_purchase_orders, allow_not_compliant_invoices, allow_required_invoices, allow_pending_invoices, allow_not_compliant_payments, allow_required_payments, allow_pending_payments, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details, is_include_all_owner_types )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlRequireWaiverOfSubrogationDescription() . ', ' .
						$this->sqlPriorityNum() . ', ' .
						$this->sqlIncludeAllProperties() . ', ' .
						$this->sqlIncludeAllCategories() . ', ' .
						$this->sqlIncludeAllVendors() . ', ' .
						$this->sqlIsOnsite() . ', ' .
						$this->sqlRequireWaiverOfSubrogation() . ', ' .
						$this->sqlAllowNotCompliantPurchaseOrders() . ', ' .
						$this->sqlAllowRequiredPurchaseOrders() . ', ' .
						$this->sqlAllowPendingPurchaseOrders() . ', ' .
						$this->sqlAllowNotCompliantInvoices() . ', ' .
						$this->sqlAllowRequiredInvoices() . ', ' .
						$this->sqlAllowPendingInvoices() . ', ' .
						$this->sqlAllowNotCompliantPayments() . ', ' .
						$this->sqlAllowRequiredPayments() . ', ' .
						$this->sqlAllowPendingPayments() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlIsIncludeAllOwnerTypes() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_waiver_of_subrogation_description = ' . $this->sqlRequireWaiverOfSubrogationDescription(). ',' ; } elseif( true == array_key_exists( 'RequireWaiverOfSubrogationDescription', $this->getChangedColumns() ) ) { $strSql .= ' require_waiver_of_subrogation_description = ' . $this->sqlRequireWaiverOfSubrogationDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' priority_num = ' . $this->sqlPriorityNum(). ',' ; } elseif( true == array_key_exists( 'PriorityNum', $this->getChangedColumns() ) ) { $strSql .= ' priority_num = ' . $this->sqlPriorityNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_all_properties = ' . $this->sqlIncludeAllProperties(). ',' ; } elseif( true == array_key_exists( 'IncludeAllProperties', $this->getChangedColumns() ) ) { $strSql .= ' include_all_properties = ' . $this->sqlIncludeAllProperties() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_all_categories = ' . $this->sqlIncludeAllCategories(). ',' ; } elseif( true == array_key_exists( 'IncludeAllCategories', $this->getChangedColumns() ) ) { $strSql .= ' include_all_categories = ' . $this->sqlIncludeAllCategories() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' include_all_vendors = ' . $this->sqlIncludeAllVendors(). ',' ; } elseif( true == array_key_exists( 'IncludeAllVendors', $this->getChangedColumns() ) ) { $strSql .= ' include_all_vendors = ' . $this->sqlIncludeAllVendors() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_onsite = ' . $this->sqlIsOnsite(). ',' ; } elseif( true == array_key_exists( 'IsOnsite', $this->getChangedColumns() ) ) { $strSql .= ' is_onsite = ' . $this->sqlIsOnsite() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_waiver_of_subrogation = ' . $this->sqlRequireWaiverOfSubrogation(). ',' ; } elseif( true == array_key_exists( 'RequireWaiverOfSubrogation', $this->getChangedColumns() ) ) { $strSql .= ' require_waiver_of_subrogation = ' . $this->sqlRequireWaiverOfSubrogation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_not_compliant_purchase_orders = ' . $this->sqlAllowNotCompliantPurchaseOrders(). ',' ; } elseif( true == array_key_exists( 'AllowNotCompliantPurchaseOrders', $this->getChangedColumns() ) ) { $strSql .= ' allow_not_compliant_purchase_orders = ' . $this->sqlAllowNotCompliantPurchaseOrders() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_required_purchase_orders = ' . $this->sqlAllowRequiredPurchaseOrders(). ',' ; } elseif( true == array_key_exists( 'AllowRequiredPurchaseOrders', $this->getChangedColumns() ) ) { $strSql .= ' allow_required_purchase_orders = ' . $this->sqlAllowRequiredPurchaseOrders() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_pending_purchase_orders = ' . $this->sqlAllowPendingPurchaseOrders(). ',' ; } elseif( true == array_key_exists( 'AllowPendingPurchaseOrders', $this->getChangedColumns() ) ) { $strSql .= ' allow_pending_purchase_orders = ' . $this->sqlAllowPendingPurchaseOrders() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_not_compliant_invoices = ' . $this->sqlAllowNotCompliantInvoices(). ',' ; } elseif( true == array_key_exists( 'AllowNotCompliantInvoices', $this->getChangedColumns() ) ) { $strSql .= ' allow_not_compliant_invoices = ' . $this->sqlAllowNotCompliantInvoices() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_required_invoices = ' . $this->sqlAllowRequiredInvoices(). ',' ; } elseif( true == array_key_exists( 'AllowRequiredInvoices', $this->getChangedColumns() ) ) { $strSql .= ' allow_required_invoices = ' . $this->sqlAllowRequiredInvoices() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_pending_invoices = ' . $this->sqlAllowPendingInvoices(). ',' ; } elseif( true == array_key_exists( 'AllowPendingInvoices', $this->getChangedColumns() ) ) { $strSql .= ' allow_pending_invoices = ' . $this->sqlAllowPendingInvoices() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_not_compliant_payments = ' . $this->sqlAllowNotCompliantPayments(). ',' ; } elseif( true == array_key_exists( 'AllowNotCompliantPayments', $this->getChangedColumns() ) ) { $strSql .= ' allow_not_compliant_payments = ' . $this->sqlAllowNotCompliantPayments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_required_payments = ' . $this->sqlAllowRequiredPayments(). ',' ; } elseif( true == array_key_exists( 'AllowRequiredPayments', $this->getChangedColumns() ) ) { $strSql .= ' allow_required_payments = ' . $this->sqlAllowRequiredPayments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_pending_payments = ' . $this->sqlAllowPendingPayments(). ',' ; } elseif( true == array_key_exists( 'AllowPendingPayments', $this->getChangedColumns() ) ) { $strSql .= ' allow_pending_payments = ' . $this->sqlAllowPendingPayments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_include_all_owner_types = ' . $this->sqlIsIncludeAllOwnerTypes(). ',' ; } elseif( true == array_key_exists( 'IsIncludeAllOwnerTypes', $this->getChangedColumns() ) ) { $strSql .= ' is_include_all_owner_types = ' . $this->sqlIsIncludeAllOwnerTypes() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'name' => $this->getName(),
			'notes' => $this->getNotes(),
			'require_waiver_of_subrogation_description' => $this->getRequireWaiverOfSubrogationDescription(),
			'priority_num' => $this->getPriorityNum(),
			'include_all_properties' => $this->getIncludeAllProperties(),
			'include_all_categories' => $this->getIncludeAllCategories(),
			'include_all_vendors' => $this->getIncludeAllVendors(),
			'is_onsite' => $this->getIsOnsite(),
			'require_waiver_of_subrogation' => $this->getRequireWaiverOfSubrogation(),
			'allow_not_compliant_purchase_orders' => $this->getAllowNotCompliantPurchaseOrders(),
			'allow_required_purchase_orders' => $this->getAllowRequiredPurchaseOrders(),
			'allow_pending_purchase_orders' => $this->getAllowPendingPurchaseOrders(),
			'allow_not_compliant_invoices' => $this->getAllowNotCompliantInvoices(),
			'allow_required_invoices' => $this->getAllowRequiredInvoices(),
			'allow_pending_invoices' => $this->getAllowPendingInvoices(),
			'allow_not_compliant_payments' => $this->getAllowNotCompliantPayments(),
			'allow_required_payments' => $this->getAllowRequiredPayments(),
			'allow_pending_payments' => $this->getAllowPendingPayments(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'is_include_all_owner_types' => $this->getIsIncludeAllOwnerTypes()
		);
	}

}
?>