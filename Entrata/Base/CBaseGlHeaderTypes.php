<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlHeaderTypes
 * Do not add any new functions to this class.
 */

class CBaseGlHeaderTypes extends CEosPluralBase {

	/**
	 * @return CGlHeaderType[]
	 */
	public static function fetchGlHeaderTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CGlHeaderType::class, $objDatabase );
	}

	/**
	 * @return CGlHeaderType
	 */
	public static function fetchGlHeaderType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGlHeaderType::class, $objDatabase );
	}

	public static function fetchGlHeaderTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_header_types', $objDatabase );
	}

	public static function fetchGlHeaderTypeById( $intId, $objDatabase ) {
		return self::fetchGlHeaderType( sprintf( 'SELECT * FROM gl_header_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>