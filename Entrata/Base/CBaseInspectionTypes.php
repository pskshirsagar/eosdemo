<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CInspectionTypes
 * Do not add any new functions to this class.
 */

class CBaseInspectionTypes extends CEosPluralBase {

	/**
	 * @return CInspectionType[]
	 */
	public static function fetchInspectionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInspectionType::class, $objDatabase );
	}

	/**
	 * @return CInspectionType
	 */
	public static function fetchInspectionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInspectionType::class, $objDatabase );
	}

	public static function fetchInspectionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'inspection_types', $objDatabase );
	}

	public static function fetchInspectionTypeById( $intId, $objDatabase ) {
		return self::fetchInspectionType( sprintf( 'SELECT * FROM inspection_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>