<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultScheduledTasks
 * Do not add any new functions to this class.
 */

class CBaseDefaultScheduledTasks extends CEosPluralBase {

	/**
	 * @return CDefaultScheduledTask[]
	 */
	public static function fetchDefaultScheduledTasks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDefaultScheduledTask::class, $objDatabase );
	}

	/**
	 * @return CDefaultScheduledTask
	 */
	public static function fetchDefaultScheduledTask( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDefaultScheduledTask::class, $objDatabase );
	}

	public static function fetchDefaultScheduledTaskCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'default_scheduled_tasks', $objDatabase );
	}

	public static function fetchDefaultScheduledTaskById( $intId, $objDatabase ) {
		return self::fetchDefaultScheduledTask( sprintf( 'SELECT * FROM default_scheduled_tasks WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDefaultScheduledTasksByScheduledTaskTypeId( $intScheduledTaskTypeId, $objDatabase ) {
		return self::fetchDefaultScheduledTasks( sprintf( 'SELECT * FROM default_scheduled_tasks WHERE scheduled_task_type_id = %d', $intScheduledTaskTypeId ), $objDatabase );
	}

	public static function fetchDefaultScheduledTasksByScheduleTypeId( $intScheduleTypeId, $objDatabase ) {
		return self::fetchDefaultScheduledTasks( sprintf( 'SELECT * FROM default_scheduled_tasks WHERE schedule_type_id = %d', $intScheduleTypeId ), $objDatabase );
	}

	public static function fetchDefaultScheduledTasksByDefaultScheduledTaskId( $intDefaultScheduledTaskId, $objDatabase ) {
		return self::fetchDefaultScheduledTasks( sprintf( 'SELECT * FROM default_scheduled_tasks WHERE default_scheduled_task_id = %d', $intDefaultScheduledTaskId ), $objDatabase );
	}

	public static function fetchDefaultScheduledTasksByScheduledTaskGroupId( $intScheduledTaskGroupId, $objDatabase ) {
		return self::fetchDefaultScheduledTasks( sprintf( 'SELECT * FROM default_scheduled_tasks WHERE scheduled_task_group_id = %d', $intScheduledTaskGroupId ), $objDatabase );
	}

	public static function fetchDefaultScheduledTasksByEventTypeId( $intEventTypeId, $objDatabase ) {
		return self::fetchDefaultScheduledTasks( sprintf( 'SELECT * FROM default_scheduled_tasks WHERE event_type_id = %d', $intEventTypeId ), $objDatabase );
	}

	public static function fetchDefaultScheduledTasksByEventSubTypeId( $intEventSubTypeId, $objDatabase ) {
		return self::fetchDefaultScheduledTasks( sprintf( 'SELECT * FROM default_scheduled_tasks WHERE event_sub_type_id = %d', $intEventSubTypeId ), $objDatabase );
	}

}
?>