<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseGlHeader extends CEosSingularBase {

	const TABLE_NAME = 'public.gl_headers';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intGlHeaderTypeId;
	protected $m_intGlTransactionTypeId;
	protected $m_intGlHeaderStatusTypeId;
	protected $m_intGlBookId;
	protected $m_intReferenceId;
	protected $m_intPropertyId;
	protected $m_intLeaseId;
	protected $m_intOffsettingGlHeaderId;
	protected $m_intTemplateGlHeaderId;
	protected $m_intClosePeriodId;
	protected $m_intBulkPropertyId;
	protected $m_intBulkPropertyUnitId;
	protected $m_intBulkCompanyDepartmentId;
	protected $m_intBulkGlDimensionId;
	protected $m_intBulkJobPhaseId;
	protected $m_intBulkApContractId;
	protected $m_intBulkPropertyBuildingId;
	protected $m_intGlHeaderScheduleId;
	protected $m_intApRoutingTagId;
	protected $m_intReclassGlHeaderId;
	protected $m_intHeaderNumber;
	protected $m_strTransactionDatetime;
	protected $m_strPostMonth;
	protected $m_strReversePostMonth;
	protected $m_strPostDate;
	protected $m_strReference;
	protected $m_strMemo;
	protected $m_strExternalUrl;
	protected $m_strTemplateName;
	protected $m_boolBulkIsConfidential;
	protected $m_boolIsTemplate;
	protected $m_boolIsReverse;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intGlHeaderStatusTypeId = '1';
		$this->m_boolBulkIsConfidential = false;
		$this->m_boolIsTemplate = false;
		$this->m_boolIsReverse = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['gl_header_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlHeaderTypeId', trim( $arrValues['gl_header_type_id'] ) ); elseif( isset( $arrValues['gl_header_type_id'] ) ) $this->setGlHeaderTypeId( $arrValues['gl_header_type_id'] );
		if( isset( $arrValues['gl_transaction_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlTransactionTypeId', trim( $arrValues['gl_transaction_type_id'] ) ); elseif( isset( $arrValues['gl_transaction_type_id'] ) ) $this->setGlTransactionTypeId( $arrValues['gl_transaction_type_id'] );
		if( isset( $arrValues['gl_header_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlHeaderStatusTypeId', trim( $arrValues['gl_header_status_type_id'] ) ); elseif( isset( $arrValues['gl_header_status_type_id'] ) ) $this->setGlHeaderStatusTypeId( $arrValues['gl_header_status_type_id'] );
		if( isset( $arrValues['gl_book_id'] ) && $boolDirectSet ) $this->set( 'm_intGlBookId', trim( $arrValues['gl_book_id'] ) ); elseif( isset( $arrValues['gl_book_id'] ) ) $this->setGlBookId( $arrValues['gl_book_id'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['offsetting_gl_header_id'] ) && $boolDirectSet ) $this->set( 'm_intOffsettingGlHeaderId', trim( $arrValues['offsetting_gl_header_id'] ) ); elseif( isset( $arrValues['offsetting_gl_header_id'] ) ) $this->setOffsettingGlHeaderId( $arrValues['offsetting_gl_header_id'] );
		if( isset( $arrValues['template_gl_header_id'] ) && $boolDirectSet ) $this->set( 'm_intTemplateGlHeaderId', trim( $arrValues['template_gl_header_id'] ) ); elseif( isset( $arrValues['template_gl_header_id'] ) ) $this->setTemplateGlHeaderId( $arrValues['template_gl_header_id'] );
		if( isset( $arrValues['close_period_id'] ) && $boolDirectSet ) $this->set( 'm_intClosePeriodId', trim( $arrValues['close_period_id'] ) ); elseif( isset( $arrValues['close_period_id'] ) ) $this->setClosePeriodId( $arrValues['close_period_id'] );
		if( isset( $arrValues['bulk_property_id'] ) && $boolDirectSet ) $this->set( 'm_intBulkPropertyId', trim( $arrValues['bulk_property_id'] ) ); elseif( isset( $arrValues['bulk_property_id'] ) ) $this->setBulkPropertyId( $arrValues['bulk_property_id'] );
		if( isset( $arrValues['bulk_property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intBulkPropertyUnitId', trim( $arrValues['bulk_property_unit_id'] ) ); elseif( isset( $arrValues['bulk_property_unit_id'] ) ) $this->setBulkPropertyUnitId( $arrValues['bulk_property_unit_id'] );
		if( isset( $arrValues['bulk_company_department_id'] ) && $boolDirectSet ) $this->set( 'm_intBulkCompanyDepartmentId', trim( $arrValues['bulk_company_department_id'] ) ); elseif( isset( $arrValues['bulk_company_department_id'] ) ) $this->setBulkCompanyDepartmentId( $arrValues['bulk_company_department_id'] );
		if( isset( $arrValues['bulk_gl_dimension_id'] ) && $boolDirectSet ) $this->set( 'm_intBulkGlDimensionId', trim( $arrValues['bulk_gl_dimension_id'] ) ); elseif( isset( $arrValues['bulk_gl_dimension_id'] ) ) $this->setBulkGlDimensionId( $arrValues['bulk_gl_dimension_id'] );
		if( isset( $arrValues['bulk_job_phase_id'] ) && $boolDirectSet ) $this->set( 'm_intBulkJobPhaseId', trim( $arrValues['bulk_job_phase_id'] ) ); elseif( isset( $arrValues['bulk_job_phase_id'] ) ) $this->setBulkJobPhaseId( $arrValues['bulk_job_phase_id'] );
		if( isset( $arrValues['bulk_ap_contract_id'] ) && $boolDirectSet ) $this->set( 'm_intBulkApContractId', trim( $arrValues['bulk_ap_contract_id'] ) ); elseif( isset( $arrValues['bulk_ap_contract_id'] ) ) $this->setBulkApContractId( $arrValues['bulk_ap_contract_id'] );
		if( isset( $arrValues['bulk_property_building_id'] ) && $boolDirectSet ) $this->set( 'm_intBulkPropertyBuildingId', trim( $arrValues['bulk_property_building_id'] ) ); elseif( isset( $arrValues['bulk_property_building_id'] ) ) $this->setBulkPropertyBuildingId( $arrValues['bulk_property_building_id'] );
		if( isset( $arrValues['gl_header_schedule_id'] ) && $boolDirectSet ) $this->set( 'm_intGlHeaderScheduleId', trim( $arrValues['gl_header_schedule_id'] ) ); elseif( isset( $arrValues['gl_header_schedule_id'] ) ) $this->setGlHeaderScheduleId( $arrValues['gl_header_schedule_id'] );
		if( isset( $arrValues['ap_routing_tag_id'] ) && $boolDirectSet ) $this->set( 'm_intApRoutingTagId', trim( $arrValues['ap_routing_tag_id'] ) ); elseif( isset( $arrValues['ap_routing_tag_id'] ) ) $this->setApRoutingTagId( $arrValues['ap_routing_tag_id'] );
		if( isset( $arrValues['reclass_gl_header_id'] ) && $boolDirectSet ) $this->set( 'm_intReclassGlHeaderId', trim( $arrValues['reclass_gl_header_id'] ) ); elseif( isset( $arrValues['reclass_gl_header_id'] ) ) $this->setReclassGlHeaderId( $arrValues['reclass_gl_header_id'] );
		if( isset( $arrValues['header_number'] ) && $boolDirectSet ) $this->set( 'm_intHeaderNumber', trim( $arrValues['header_number'] ) ); elseif( isset( $arrValues['header_number'] ) ) $this->setHeaderNumber( $arrValues['header_number'] );
		if( isset( $arrValues['transaction_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTransactionDatetime', trim( $arrValues['transaction_datetime'] ) ); elseif( isset( $arrValues['transaction_datetime'] ) ) $this->setTransactionDatetime( $arrValues['transaction_datetime'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['reverse_post_month'] ) && $boolDirectSet ) $this->set( 'm_strReversePostMonth', trim( $arrValues['reverse_post_month'] ) ); elseif( isset( $arrValues['reverse_post_month'] ) ) $this->setReversePostMonth( $arrValues['reverse_post_month'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['reference'] ) && $boolDirectSet ) $this->set( 'm_strReference', trim( $arrValues['reference'] ) ); elseif( isset( $arrValues['reference'] ) ) $this->setReference( $arrValues['reference'] );
		if( isset( $arrValues['memo'] ) && $boolDirectSet ) $this->set( 'm_strMemo', trim( $arrValues['memo'] ) ); elseif( isset( $arrValues['memo'] ) ) $this->setMemo( $arrValues['memo'] );
		if( isset( $arrValues['external_url'] ) && $boolDirectSet ) $this->set( 'm_strExternalUrl', trim( $arrValues['external_url'] ) ); elseif( isset( $arrValues['external_url'] ) ) $this->setExternalUrl( $arrValues['external_url'] );
		if( isset( $arrValues['template_name'] ) && $boolDirectSet ) $this->set( 'm_strTemplateName', trim( $arrValues['template_name'] ) ); elseif( isset( $arrValues['template_name'] ) ) $this->setTemplateName( $arrValues['template_name'] );
		if( isset( $arrValues['bulk_is_confidential'] ) && $boolDirectSet ) $this->set( 'm_boolBulkIsConfidential', trim( stripcslashes( $arrValues['bulk_is_confidential'] ) ) ); elseif( isset( $arrValues['bulk_is_confidential'] ) ) $this->setBulkIsConfidential( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bulk_is_confidential'] ) : $arrValues['bulk_is_confidential'] );
		if( isset( $arrValues['is_template'] ) && $boolDirectSet ) $this->set( 'm_boolIsTemplate', trim( stripcslashes( $arrValues['is_template'] ) ) ); elseif( isset( $arrValues['is_template'] ) ) $this->setIsTemplate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_template'] ) : $arrValues['is_template'] );
		if( isset( $arrValues['is_reverse'] ) && $boolDirectSet ) $this->set( 'm_boolIsReverse', trim( stripcslashes( $arrValues['is_reverse'] ) ) ); elseif( isset( $arrValues['is_reverse'] ) ) $this->setIsReverse( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_reverse'] ) : $arrValues['is_reverse'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setGlHeaderTypeId( $intGlHeaderTypeId ) {
		$this->set( 'm_intGlHeaderTypeId', CStrings::strToIntDef( $intGlHeaderTypeId, NULL, false ) );
	}

	public function getGlHeaderTypeId() {
		return $this->m_intGlHeaderTypeId;
	}

	public function sqlGlHeaderTypeId() {
		return ( true == isset( $this->m_intGlHeaderTypeId ) ) ? ( string ) $this->m_intGlHeaderTypeId : 'NULL';
	}

	public function setGlTransactionTypeId( $intGlTransactionTypeId ) {
		$this->set( 'm_intGlTransactionTypeId', CStrings::strToIntDef( $intGlTransactionTypeId, NULL, false ) );
	}

	public function getGlTransactionTypeId() {
		return $this->m_intGlTransactionTypeId;
	}

	public function sqlGlTransactionTypeId() {
		return ( true == isset( $this->m_intGlTransactionTypeId ) ) ? ( string ) $this->m_intGlTransactionTypeId : 'NULL';
	}

	public function setGlHeaderStatusTypeId( $intGlHeaderStatusTypeId ) {
		$this->set( 'm_intGlHeaderStatusTypeId', CStrings::strToIntDef( $intGlHeaderStatusTypeId, NULL, false ) );
	}

	public function getGlHeaderStatusTypeId() {
		return $this->m_intGlHeaderStatusTypeId;
	}

	public function sqlGlHeaderStatusTypeId() {
		return ( true == isset( $this->m_intGlHeaderStatusTypeId ) ) ? ( string ) $this->m_intGlHeaderStatusTypeId : '1';
	}

	public function setGlBookId( $intGlBookId ) {
		$this->set( 'm_intGlBookId', CStrings::strToIntDef( $intGlBookId, NULL, false ) );
	}

	public function getGlBookId() {
		return $this->m_intGlBookId;
	}

	public function sqlGlBookId() {
		return ( true == isset( $this->m_intGlBookId ) ) ? ( string ) $this->m_intGlBookId : 'NULL';
	}

	public function setReferenceId( $intReferenceId ) {
		$this->set( 'm_intReferenceId', CStrings::strToIntDef( $intReferenceId, NULL, false ) );
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_intReferenceId ) ) ? ( string ) $this->m_intReferenceId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setOffsettingGlHeaderId( $intOffsettingGlHeaderId ) {
		$this->set( 'm_intOffsettingGlHeaderId', CStrings::strToIntDef( $intOffsettingGlHeaderId, NULL, false ) );
	}

	public function getOffsettingGlHeaderId() {
		return $this->m_intOffsettingGlHeaderId;
	}

	public function sqlOffsettingGlHeaderId() {
		return ( true == isset( $this->m_intOffsettingGlHeaderId ) ) ? ( string ) $this->m_intOffsettingGlHeaderId : 'NULL';
	}

	public function setTemplateGlHeaderId( $intTemplateGlHeaderId ) {
		$this->set( 'm_intTemplateGlHeaderId', CStrings::strToIntDef( $intTemplateGlHeaderId, NULL, false ) );
	}

	public function getTemplateGlHeaderId() {
		return $this->m_intTemplateGlHeaderId;
	}

	public function sqlTemplateGlHeaderId() {
		return ( true == isset( $this->m_intTemplateGlHeaderId ) ) ? ( string ) $this->m_intTemplateGlHeaderId : 'NULL';
	}

	public function setClosePeriodId( $intClosePeriodId ) {
		$this->set( 'm_intClosePeriodId', CStrings::strToIntDef( $intClosePeriodId, NULL, false ) );
	}

	public function getClosePeriodId() {
		return $this->m_intClosePeriodId;
	}

	public function sqlClosePeriodId() {
		return ( true == isset( $this->m_intClosePeriodId ) ) ? ( string ) $this->m_intClosePeriodId : 'NULL';
	}

	public function setBulkPropertyId( $intBulkPropertyId ) {
		$this->set( 'm_intBulkPropertyId', CStrings::strToIntDef( $intBulkPropertyId, NULL, false ) );
	}

	public function getBulkPropertyId() {
		return $this->m_intBulkPropertyId;
	}

	public function sqlBulkPropertyId() {
		return ( true == isset( $this->m_intBulkPropertyId ) ) ? ( string ) $this->m_intBulkPropertyId : 'NULL';
	}

	public function setBulkPropertyUnitId( $intBulkPropertyUnitId ) {
		$this->set( 'm_intBulkPropertyUnitId', CStrings::strToIntDef( $intBulkPropertyUnitId, NULL, false ) );
	}

	public function getBulkPropertyUnitId() {
		return $this->m_intBulkPropertyUnitId;
	}

	public function sqlBulkPropertyUnitId() {
		return ( true == isset( $this->m_intBulkPropertyUnitId ) ) ? ( string ) $this->m_intBulkPropertyUnitId : 'NULL';
	}

	public function setBulkCompanyDepartmentId( $intBulkCompanyDepartmentId ) {
		$this->set( 'm_intBulkCompanyDepartmentId', CStrings::strToIntDef( $intBulkCompanyDepartmentId, NULL, false ) );
	}

	public function getBulkCompanyDepartmentId() {
		return $this->m_intBulkCompanyDepartmentId;
	}

	public function sqlBulkCompanyDepartmentId() {
		return ( true == isset( $this->m_intBulkCompanyDepartmentId ) ) ? ( string ) $this->m_intBulkCompanyDepartmentId : 'NULL';
	}

	public function setBulkGlDimensionId( $intBulkGlDimensionId ) {
		$this->set( 'm_intBulkGlDimensionId', CStrings::strToIntDef( $intBulkGlDimensionId, NULL, false ) );
	}

	public function getBulkGlDimensionId() {
		return $this->m_intBulkGlDimensionId;
	}

	public function sqlBulkGlDimensionId() {
		return ( true == isset( $this->m_intBulkGlDimensionId ) ) ? ( string ) $this->m_intBulkGlDimensionId : 'NULL';
	}

	public function setBulkJobPhaseId( $intBulkJobPhaseId ) {
		$this->set( 'm_intBulkJobPhaseId', CStrings::strToIntDef( $intBulkJobPhaseId, NULL, false ) );
	}

	public function getBulkJobPhaseId() {
		return $this->m_intBulkJobPhaseId;
	}

	public function sqlBulkJobPhaseId() {
		return ( true == isset( $this->m_intBulkJobPhaseId ) ) ? ( string ) $this->m_intBulkJobPhaseId : 'NULL';
	}

	public function setBulkApContractId( $intBulkApContractId ) {
		$this->set( 'm_intBulkApContractId', CStrings::strToIntDef( $intBulkApContractId, NULL, false ) );
	}

	public function getBulkApContractId() {
		return $this->m_intBulkApContractId;
	}

	public function sqlBulkApContractId() {
		return ( true == isset( $this->m_intBulkApContractId ) ) ? ( string ) $this->m_intBulkApContractId : 'NULL';
	}

	public function setBulkPropertyBuildingId( $intBulkPropertyBuildingId ) {
		$this->set( 'm_intBulkPropertyBuildingId', CStrings::strToIntDef( $intBulkPropertyBuildingId, NULL, false ) );
	}

	public function getBulkPropertyBuildingId() {
		return $this->m_intBulkPropertyBuildingId;
	}

	public function sqlBulkPropertyBuildingId() {
		return ( true == isset( $this->m_intBulkPropertyBuildingId ) ) ? ( string ) $this->m_intBulkPropertyBuildingId : 'NULL';
	}

	public function setGlHeaderScheduleId( $intGlHeaderScheduleId ) {
		$this->set( 'm_intGlHeaderScheduleId', CStrings::strToIntDef( $intGlHeaderScheduleId, NULL, false ) );
	}

	public function getGlHeaderScheduleId() {
		return $this->m_intGlHeaderScheduleId;
	}

	public function sqlGlHeaderScheduleId() {
		return ( true == isset( $this->m_intGlHeaderScheduleId ) ) ? ( string ) $this->m_intGlHeaderScheduleId : 'NULL';
	}

	public function setApRoutingTagId( $intApRoutingTagId ) {
		$this->set( 'm_intApRoutingTagId', CStrings::strToIntDef( $intApRoutingTagId, NULL, false ) );
	}

	public function getApRoutingTagId() {
		return $this->m_intApRoutingTagId;
	}

	public function sqlApRoutingTagId() {
		return ( true == isset( $this->m_intApRoutingTagId ) ) ? ( string ) $this->m_intApRoutingTagId : 'NULL';
	}

	public function setReclassGlHeaderId( $intReclassGlHeaderId ) {
		$this->set( 'm_intReclassGlHeaderId', CStrings::strToIntDef( $intReclassGlHeaderId, NULL, false ) );
	}

	public function getReclassGlHeaderId() {
		return $this->m_intReclassGlHeaderId;
	}

	public function sqlReclassGlHeaderId() {
		return ( true == isset( $this->m_intReclassGlHeaderId ) ) ? ( string ) $this->m_intReclassGlHeaderId : 'NULL';
	}

	public function setHeaderNumber( $intHeaderNumber ) {
		$this->set( 'm_intHeaderNumber', CStrings::strToIntDef( $intHeaderNumber, NULL, false ) );
	}

	public function getHeaderNumber() {
		return $this->m_intHeaderNumber;
	}

	public function sqlHeaderNumber() {
		return ( true == isset( $this->m_intHeaderNumber ) ) ? ( string ) $this->m_intHeaderNumber : 'NULL';
	}

	public function setTransactionDatetime( $strTransactionDatetime ) {
		$this->set( 'm_strTransactionDatetime', CStrings::strTrimDef( $strTransactionDatetime, -1, NULL, true ) );
	}

	public function getTransactionDatetime() {
		return $this->m_strTransactionDatetime;
	}

	public function sqlTransactionDatetime() {
		return ( true == isset( $this->m_strTransactionDatetime ) ) ? '\'' . $this->m_strTransactionDatetime . '\'' : 'NOW()';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setReversePostMonth( $strReversePostMonth ) {
		$this->set( 'm_strReversePostMonth', CStrings::strTrimDef( $strReversePostMonth, -1, NULL, true ) );
	}

	public function getReversePostMonth() {
		return $this->m_strReversePostMonth;
	}

	public function sqlReversePostMonth() {
		return ( true == isset( $this->m_strReversePostMonth ) ) ? '\'' . $this->m_strReversePostMonth . '\'' : 'NULL';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setReference( $strReference ) {
		$this->set( 'm_strReference', CStrings::strTrimDef( $strReference, 240, NULL, true ) );
	}

	public function getReference() {
		return $this->m_strReference;
	}

	public function sqlReference() {
		return ( true == isset( $this->m_strReference ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReference ) : '\'' . addslashes( $this->m_strReference ) . '\'' ) : 'NULL';
	}

	public function setMemo( $strMemo ) {
		$this->set( 'm_strMemo', CStrings::strTrimDef( $strMemo, -1, NULL, true ) );
	}

	public function getMemo() {
		return $this->m_strMemo;
	}

	public function sqlMemo() {
		return ( true == isset( $this->m_strMemo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMemo ) : '\'' . addslashes( $this->m_strMemo ) . '\'' ) : 'NULL';
	}

	public function setExternalUrl( $strExternalUrl ) {
		$this->set( 'm_strExternalUrl', CStrings::strTrimDef( $strExternalUrl, -1, NULL, true ) );
	}

	public function getExternalUrl() {
		return $this->m_strExternalUrl;
	}

	public function sqlExternalUrl() {
		return ( true == isset( $this->m_strExternalUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strExternalUrl ) : '\'' . addslashes( $this->m_strExternalUrl ) . '\'' ) : 'NULL';
	}

	public function setTemplateName( $strTemplateName ) {
		$this->set( 'm_strTemplateName', CStrings::strTrimDef( $strTemplateName, 240, NULL, true ) );
	}

	public function getTemplateName() {
		return $this->m_strTemplateName;
	}

	public function sqlTemplateName() {
		return ( true == isset( $this->m_strTemplateName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTemplateName ) : '\'' . addslashes( $this->m_strTemplateName ) . '\'' ) : 'NULL';
	}

	public function setBulkIsConfidential( $boolBulkIsConfidential ) {
		$this->set( 'm_boolBulkIsConfidential', CStrings::strToBool( $boolBulkIsConfidential ) );
	}

	public function getBulkIsConfidential() {
		return $this->m_boolBulkIsConfidential;
	}

	public function sqlBulkIsConfidential() {
		return ( true == isset( $this->m_boolBulkIsConfidential ) ) ? '\'' . ( true == ( bool ) $this->m_boolBulkIsConfidential ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTemplate( $boolIsTemplate ) {
		$this->set( 'm_boolIsTemplate', CStrings::strToBool( $boolIsTemplate ) );
	}

	public function getIsTemplate() {
		return $this->m_boolIsTemplate;
	}

	public function sqlIsTemplate() {
		return ( true == isset( $this->m_boolIsTemplate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTemplate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsReverse( $boolIsReverse ) {
		$this->set( 'm_boolIsReverse', CStrings::strToBool( $boolIsReverse ) );
	}

	public function getIsReverse() {
		return $this->m_boolIsReverse;
	}

	public function sqlIsReverse() {
		return ( true == isset( $this->m_boolIsReverse ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReverse ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, gl_header_type_id, gl_transaction_type_id, gl_header_status_type_id, gl_book_id, reference_id, property_id, lease_id, offsetting_gl_header_id, template_gl_header_id, close_period_id, bulk_property_id, bulk_property_unit_id, bulk_company_department_id, bulk_gl_dimension_id, bulk_job_phase_id, bulk_ap_contract_id, bulk_property_building_id, gl_header_schedule_id, ap_routing_tag_id, reclass_gl_header_id, header_number, transaction_datetime, post_month, reverse_post_month, post_date, reference, memo, external_url, template_name, bulk_is_confidential, is_template, is_reverse, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlGlHeaderTypeId() . ', ' .
						$this->sqlGlTransactionTypeId() . ', ' .
						$this->sqlGlHeaderStatusTypeId() . ', ' .
						$this->sqlGlBookId() . ', ' .
						$this->sqlReferenceId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlOffsettingGlHeaderId() . ', ' .
						$this->sqlTemplateGlHeaderId() . ', ' .
						$this->sqlClosePeriodId() . ', ' .
						$this->sqlBulkPropertyId() . ', ' .
						$this->sqlBulkPropertyUnitId() . ', ' .
						$this->sqlBulkCompanyDepartmentId() . ', ' .
						$this->sqlBulkGlDimensionId() . ', ' .
						$this->sqlBulkJobPhaseId() . ', ' .
						$this->sqlBulkApContractId() . ', ' .
						$this->sqlBulkPropertyBuildingId() . ', ' .
						$this->sqlGlHeaderScheduleId() . ', ' .
						$this->sqlApRoutingTagId() . ', ' .
						$this->sqlReclassGlHeaderId() . ', ' .
						$this->sqlHeaderNumber() . ', ' .
						$this->sqlTransactionDatetime() . ', ' .
						$this->sqlPostMonth() . ', ' .
						$this->sqlReversePostMonth() . ', ' .
						$this->sqlPostDate() . ', ' .
						$this->sqlReference() . ', ' .
						$this->sqlMemo() . ', ' .
						$this->sqlExternalUrl() . ', ' .
						$this->sqlTemplateName() . ', ' .
						$this->sqlBulkIsConfidential() . ', ' .
						$this->sqlIsTemplate() . ', ' .
						$this->sqlIsReverse() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_header_type_id = ' . $this->sqlGlHeaderTypeId(). ',' ; } elseif( true == array_key_exists( 'GlHeaderTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_header_type_id = ' . $this->sqlGlHeaderTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_transaction_type_id = ' . $this->sqlGlTransactionTypeId(). ',' ; } elseif( true == array_key_exists( 'GlTransactionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_transaction_type_id = ' . $this->sqlGlTransactionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_header_status_type_id = ' . $this->sqlGlHeaderStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'GlHeaderStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' gl_header_status_type_id = ' . $this->sqlGlHeaderStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_book_id = ' . $this->sqlGlBookId(). ',' ; } elseif( true == array_key_exists( 'GlBookId', $this->getChangedColumns() ) ) { $strSql .= ' gl_book_id = ' . $this->sqlGlBookId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId(). ',' ; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' offsetting_gl_header_id = ' . $this->sqlOffsettingGlHeaderId(). ',' ; } elseif( true == array_key_exists( 'OffsettingGlHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' offsetting_gl_header_id = ' . $this->sqlOffsettingGlHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_gl_header_id = ' . $this->sqlTemplateGlHeaderId(). ',' ; } elseif( true == array_key_exists( 'TemplateGlHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' template_gl_header_id = ' . $this->sqlTemplateGlHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' close_period_id = ' . $this->sqlClosePeriodId(). ',' ; } elseif( true == array_key_exists( 'ClosePeriodId', $this->getChangedColumns() ) ) { $strSql .= ' close_period_id = ' . $this->sqlClosePeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bulk_property_id = ' . $this->sqlBulkPropertyId(). ',' ; } elseif( true == array_key_exists( 'BulkPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' bulk_property_id = ' . $this->sqlBulkPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bulk_property_unit_id = ' . $this->sqlBulkPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'BulkPropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' bulk_property_unit_id = ' . $this->sqlBulkPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bulk_company_department_id = ' . $this->sqlBulkCompanyDepartmentId(). ',' ; } elseif( true == array_key_exists( 'BulkCompanyDepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' bulk_company_department_id = ' . $this->sqlBulkCompanyDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bulk_gl_dimension_id = ' . $this->sqlBulkGlDimensionId(). ',' ; } elseif( true == array_key_exists( 'BulkGlDimensionId', $this->getChangedColumns() ) ) { $strSql .= ' bulk_gl_dimension_id = ' . $this->sqlBulkGlDimensionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bulk_job_phase_id = ' . $this->sqlBulkJobPhaseId(). ',' ; } elseif( true == array_key_exists( 'BulkJobPhaseId', $this->getChangedColumns() ) ) { $strSql .= ' bulk_job_phase_id = ' . $this->sqlBulkJobPhaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bulk_ap_contract_id = ' . $this->sqlBulkApContractId(). ',' ; } elseif( true == array_key_exists( 'BulkApContractId', $this->getChangedColumns() ) ) { $strSql .= ' bulk_ap_contract_id = ' . $this->sqlBulkApContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bulk_property_building_id = ' . $this->sqlBulkPropertyBuildingId(). ',' ; } elseif( true == array_key_exists( 'BulkPropertyBuildingId', $this->getChangedColumns() ) ) { $strSql .= ' bulk_property_building_id = ' . $this->sqlBulkPropertyBuildingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gl_header_schedule_id = ' . $this->sqlGlHeaderScheduleId(). ',' ; } elseif( true == array_key_exists( 'GlHeaderScheduleId', $this->getChangedColumns() ) ) { $strSql .= ' gl_header_schedule_id = ' . $this->sqlGlHeaderScheduleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_routing_tag_id = ' . $this->sqlApRoutingTagId(). ',' ; } elseif( true == array_key_exists( 'ApRoutingTagId', $this->getChangedColumns() ) ) { $strSql .= ' ap_routing_tag_id = ' . $this->sqlApRoutingTagId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reclass_gl_header_id = ' . $this->sqlReclassGlHeaderId(). ',' ; } elseif( true == array_key_exists( 'ReclassGlHeaderId', $this->getChangedColumns() ) ) { $strSql .= ' reclass_gl_header_id = ' . $this->sqlReclassGlHeaderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' header_number = ' . $this->sqlHeaderNumber(). ',' ; } elseif( true == array_key_exists( 'HeaderNumber', $this->getChangedColumns() ) ) { $strSql .= ' header_number = ' . $this->sqlHeaderNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime(). ',' ; } elseif( true == array_key_exists( 'TransactionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth(). ',' ; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reverse_post_month = ' . $this->sqlReversePostMonth(). ',' ; } elseif( true == array_key_exists( 'ReversePostMonth', $this->getChangedColumns() ) ) { $strSql .= ' reverse_post_month = ' . $this->sqlReversePostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate(). ',' ; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference = ' . $this->sqlReference(). ',' ; } elseif( true == array_key_exists( 'Reference', $this->getChangedColumns() ) ) { $strSql .= ' reference = ' . $this->sqlReference() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' memo = ' . $this->sqlMemo(). ',' ; } elseif( true == array_key_exists( 'Memo', $this->getChangedColumns() ) ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_url = ' . $this->sqlExternalUrl(). ',' ; } elseif( true == array_key_exists( 'ExternalUrl', $this->getChangedColumns() ) ) { $strSql .= ' external_url = ' . $this->sqlExternalUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_name = ' . $this->sqlTemplateName(). ',' ; } elseif( true == array_key_exists( 'TemplateName', $this->getChangedColumns() ) ) { $strSql .= ' template_name = ' . $this->sqlTemplateName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bulk_is_confidential = ' . $this->sqlBulkIsConfidential(). ',' ; } elseif( true == array_key_exists( 'BulkIsConfidential', $this->getChangedColumns() ) ) { $strSql .= ' bulk_is_confidential = ' . $this->sqlBulkIsConfidential() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_template = ' . $this->sqlIsTemplate(). ',' ; } elseif( true == array_key_exists( 'IsTemplate', $this->getChangedColumns() ) ) { $strSql .= ' is_template = ' . $this->sqlIsTemplate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reverse = ' . $this->sqlIsReverse(). ',' ; } elseif( true == array_key_exists( 'IsReverse', $this->getChangedColumns() ) ) { $strSql .= ' is_reverse = ' . $this->sqlIsReverse() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'gl_header_type_id' => $this->getGlHeaderTypeId(),
			'gl_transaction_type_id' => $this->getGlTransactionTypeId(),
			'gl_header_status_type_id' => $this->getGlHeaderStatusTypeId(),
			'gl_book_id' => $this->getGlBookId(),
			'reference_id' => $this->getReferenceId(),
			'property_id' => $this->getPropertyId(),
			'lease_id' => $this->getLeaseId(),
			'offsetting_gl_header_id' => $this->getOffsettingGlHeaderId(),
			'template_gl_header_id' => $this->getTemplateGlHeaderId(),
			'close_period_id' => $this->getClosePeriodId(),
			'bulk_property_id' => $this->getBulkPropertyId(),
			'bulk_property_unit_id' => $this->getBulkPropertyUnitId(),
			'bulk_company_department_id' => $this->getBulkCompanyDepartmentId(),
			'bulk_gl_dimension_id' => $this->getBulkGlDimensionId(),
			'bulk_job_phase_id' => $this->getBulkJobPhaseId(),
			'bulk_ap_contract_id' => $this->getBulkApContractId(),
			'bulk_property_building_id' => $this->getBulkPropertyBuildingId(),
			'gl_header_schedule_id' => $this->getGlHeaderScheduleId(),
			'ap_routing_tag_id' => $this->getApRoutingTagId(),
			'reclass_gl_header_id' => $this->getReclassGlHeaderId(),
			'header_number' => $this->getHeaderNumber(),
			'transaction_datetime' => $this->getTransactionDatetime(),
			'post_month' => $this->getPostMonth(),
			'reverse_post_month' => $this->getReversePostMonth(),
			'post_date' => $this->getPostDate(),
			'reference' => $this->getReference(),
			'memo' => $this->getMemo(),
			'external_url' => $this->getExternalUrl(),
			'template_name' => $this->getTemplateName(),
			'bulk_is_confidential' => $this->getBulkIsConfidential(),
			'is_template' => $this->getIsTemplate(),
			'is_reverse' => $this->getIsReverse(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>