<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyCertificationReasonTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyCertificationReasonTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyCertificationReasonType[]
	 */
	public static function fetchSubsidyCertificationReasonTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyCertificationReasonType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyCertificationReasonType
	 */
	public static function fetchSubsidyCertificationReasonType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyCertificationReasonType::class, $objDatabase );
	}

	public static function fetchSubsidyCertificationReasonTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_certification_reason_types', $objDatabase );
	}

	public static function fetchSubsidyCertificationReasonTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyCertificationReasonType( sprintf( 'SELECT * FROM subsidy_certification_reason_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchSubsidyCertificationReasonTypesBySubsidyCertificationTypeId( $intSubsidyCertificationTypeId, $objDatabase ) {
		return self::fetchSubsidyCertificationReasonTypes( sprintf( 'SELECT * FROM subsidy_certification_reason_types WHERE subsidy_certification_type_id = %d', $intSubsidyCertificationTypeId ), $objDatabase );
	}

}
?>