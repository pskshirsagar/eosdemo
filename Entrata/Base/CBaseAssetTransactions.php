<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAssetTransactions
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseAssetTransactions extends CEosPluralBase {

	/**
	 * @return CAssetTransaction[]
	 */
	public static function fetchAssetTransactions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CAssetTransaction', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CAssetTransaction
	 */
	public static function fetchAssetTransaction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAssetTransaction', $objDatabase );
	}

	public static function fetchAssetTransactionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'asset_transactions', $objDatabase );
	}

	public static function fetchAssetTransactionByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchAssetTransaction( sprintf( 'SELECT * FROM asset_transactions WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetTransactionsByCid( $intCid, $objDatabase ) {
		return self::fetchAssetTransactions( sprintf( 'SELECT * FROM asset_transactions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetTransactionsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchAssetTransactions( sprintf( 'SELECT * FROM asset_transactions WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetTransactionsByAssetTransactionTypeIdByCid( $intAssetTransactionTypeId, $intCid, $objDatabase ) {
		return self::fetchAssetTransactions( sprintf( 'SELECT * FROM asset_transactions WHERE asset_transaction_type_id = %d AND cid = %d', ( int ) $intAssetTransactionTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetTransactionsByApCodeIdByCid( $intApCodeId, $intCid, $objDatabase ) {
		return self::fetchAssetTransactions( sprintf( 'SELECT * FROM asset_transactions WHERE ap_code_id = %d AND cid = %d', ( int ) $intApCodeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetTransactionsByUnitOfMeasureIdByCid( $intUnitOfMeasureId, $intCid, $objDatabase ) {
		return self::fetchAssetTransactions( sprintf( 'SELECT * FROM asset_transactions WHERE unit_of_measure_id = %d AND cid = %d', ( int ) $intUnitOfMeasureId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetTransactionsByDisplayUnitOfMeasureIdByCid( $intDisplayUnitOfMeasureId, $intCid, $objDatabase ) {
		return self::fetchAssetTransactions( sprintf( 'SELECT * FROM asset_transactions WHERE display_unit_of_measure_id = %d AND cid = %d', ( int ) $intDisplayUnitOfMeasureId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetTransactionsByPoApDetailIdByCid( $intPoApDetailId, $intCid, $objDatabase ) {
		return self::fetchAssetTransactions( sprintf( 'SELECT * FROM asset_transactions WHERE po_ap_detail_id = %d AND cid = %d', ( int ) $intPoApDetailId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetTransactionsByInvoiceApDetailIdByCid( $intInvoiceApDetailId, $intCid, $objDatabase ) {
		return self::fetchAssetTransactions( sprintf( 'SELECT * FROM asset_transactions WHERE invoice_ap_detail_id = %d AND cid = %d', ( int ) $intInvoiceApDetailId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetTransactionsByDebitGlAccountIdByCid( $intDebitGlAccountId, $intCid, $objDatabase ) {
		return self::fetchAssetTransactions( sprintf( 'SELECT * FROM asset_transactions WHERE debit_gl_account_id = %d AND cid = %d', ( int ) $intDebitGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetTransactionsByCreditGlAccountIdByCid( $intCreditGlAccountId, $intCid, $objDatabase ) {
		return self::fetchAssetTransactions( sprintf( 'SELECT * FROM asset_transactions WHERE credit_gl_account_id = %d AND cid = %d', ( int ) $intCreditGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetTransactionsByMaintenanceRequestIdByCid( $intMaintenanceRequestId, $intCid, $objDatabase ) {
		return self::fetchAssetTransactions( sprintf( 'SELECT * FROM asset_transactions WHERE maintenance_request_id = %d AND cid = %d', ( int ) $intMaintenanceRequestId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetTransactionsByAssetIdByCid( $intAssetId, $intCid, $objDatabase ) {
		return self::fetchAssetTransactions( sprintf( 'SELECT * FROM asset_transactions WHERE asset_id = %d AND cid = %d', ( int ) $intAssetId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetTransactionsByAssetLocationIdByCid( $intAssetLocationId, $intCid, $objDatabase ) {
		return self::fetchAssetTransactions( sprintf( 'SELECT * FROM asset_transactions WHERE asset_location_id = %d AND cid = %d', ( int ) $intAssetLocationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetTransactionsByAssetTransactionIdByCid( $intAssetTransactionId, $intCid, $objDatabase ) {
		return self::fetchAssetTransactions( sprintf( 'SELECT * FROM asset_transactions WHERE asset_transaction_id = %d AND cid = %d', ( int ) $intAssetTransactionId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetTransactionsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchAssetTransactions( sprintf( 'SELECT * FROM asset_transactions WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetTransactionsByMaintenanceLocationIdByCid( $intMaintenanceLocationId, $intCid, $objDatabase ) {
		return self::fetchAssetTransactions( sprintf( 'SELECT * FROM asset_transactions WHERE maintenance_location_id = %d AND cid = %d', ( int ) $intMaintenanceLocationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetTransactionsByPeriodIdByCid( $intPeriodId, $intCid, $objDatabase ) {
		return self::fetchAssetTransactions( sprintf( 'SELECT * FROM asset_transactions WHERE period_id = %d AND cid = %d', ( int ) $intPeriodId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchAssetTransactionsByMaintenanceRequestMaterialIdByCid( $intMaintenanceRequestMaterialId, $intCid, $objDatabase ) {
		return self::fetchAssetTransactions( sprintf( 'SELECT * FROM asset_transactions WHERE maintenance_request_material_id = %d AND cid = %d', ( int ) $intMaintenanceRequestMaterialId, ( int ) $intCid ), $objDatabase );
	}

}
?>