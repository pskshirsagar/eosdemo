<?php

class CBaseCompanyService extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intServiceId;
    protected $m_intIsAllowed;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        $this->m_intIsAllowed = '0';
        $this->m_strUpdatedOn = 'now()';
        $this->m_strCreatedOn = 'now()';

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if ( isset( $arrValues['id'] )) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if ( isset( $arrValues['cid'] )) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['service_id'] ) && $boolDirectSet ) $this->m_intServiceId = trim( $arrValues['service_id'] ); else if ( isset( $arrValues['service_id'] )) $this->setServiceId( $arrValues['service_id'] );
        if( isset( $arrValues['is_allowed'] ) && $boolDirectSet ) $this->m_intIsAllowed = trim( $arrValues['is_allowed'] ); else if ( isset( $arrValues['is_allowed'] )) $this->setIsAllowed( $arrValues['is_allowed'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if ( isset( $arrValues['updated_by'] )) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if ( isset( $arrValues['updated_on'] )) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if ( isset( $arrValues['created_by'] )) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if ( isset( $arrValues['created_on'] )) $this->setCreatedOn( $arrValues['created_on'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return  ( true == isset( $this->m_intId )) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return  ( true == isset( $this->m_intCid )) ? (string) $this->m_intCid : 'NULL';
    }

    public function setServiceId( $intServiceId ) {
        $this->m_intServiceId = CStrings::strToIntDef( $intServiceId, NULL, false );
    }

    public function getServiceId() {
        return $this->m_intServiceId;
    }

    public function sqlServiceId() {
        return  ( true == isset( $this->m_intServiceId )) ? (string) $this->m_intServiceId : 'NULL';
    }

    public function setIsAllowed( $intIsAllowed ) {
        $this->m_intIsAllowed = CStrings::strToIntDef( $intIsAllowed, NULL, false );
    }

    public function getIsAllowed() {
        return $this->m_intIsAllowed;
    }

    public function sqlIsAllowed() {
        return  ( true == isset( $this->m_intIsAllowed )) ? (string) $this->m_intIsAllowed : '0';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return  ( true == isset( $this->m_intUpdatedBy )) ? (string) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return  ( true == isset( $this->m_strUpdatedOn )) ? '\'' . $this->m_strUpdatedOn . '\'' : 'now()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return  ( true == isset( $this->m_intCreatedBy )) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return  ( true == isset( $this->m_strCreatedOn )) ? '\'' . $this->m_strCreatedOn . '\'' : 'now()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() )) ?  'nextval( \'public.company_services_id_seq\' )' : (int) $this->m_intId;

        $strSql = 'INSERT INTO
					  public.company_services
					VALUES ( ' .
	                    $strId . ', ' .
	                    $this->sqlCid() . ', ' .
	                    $this->sqlServiceId() . ', ' .
	                    $this->sqlIsAllowed() . ', ' .
	                    (int) $intCurrentUserId . ', ' .
	                    $this->sqlUpdatedOn() . ', ' .
	                    (int) $intCurrentUserId . ', ' .
	                    $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if ( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate()) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }

        $strSql = 'UPDATE 
                      public.company_services
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid()) != $this->getOriginalValueByFieldName ( 'cid' )) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_id = ' . $this->sqlServiceId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlServiceId()) != $this->getOriginalValueByFieldName ( 'service_id' )) { $arrstrOriginalValueChanges['service_id'] = $this->sqlServiceId(); $strSql .= ' service_id = ' . $this->sqlServiceId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_allowed = ' . $this->sqlIsAllowed() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsAllowed()) != $this->getOriginalValueByFieldName ( 'is_allowed' )) { $arrstrOriginalValueChanges['is_allowed'] = $this->sqlIsAllowed(); $strSql .= ' is_allowed = ' . $this->sqlIsAllowed() . ','; $boolUpdate = true; }
                      $strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
                      $strSql .= ' updated_on = \'NOW()\'
					WHERE
					    id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
        } else {
			if( true == $boolUpdate ){
			    if( true == $this->executeSql( $strSql, $this, $objDatabase )){
				    if( true == $this->getAllowDifferentialUpdate() ) {
				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );return true;
				    }
			    } else {
			        return false;
				}
			}
			return true;
		}
	}

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.company_services WHERE id = ' . (int) $this->sqlId() . ' AND cid = ' . (int) $this->sqlCid() . ';';

        if ( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase ) {
        return parent::fetchNextId( 'public.company_services_id_seq', $objDatabase );
    }
}
?>