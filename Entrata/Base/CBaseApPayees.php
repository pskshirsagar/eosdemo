<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayees
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseApPayees extends CEosPluralBase {

	/**
	 * @return CApPayee[]
	 */
	public static function fetchApPayees( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CApPayee::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CApPayee
	 */
	public static function fetchApPayee( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CApPayee::class, $objDatabase );
	}

	public static function fetchApPayeeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ap_payees', $objDatabase );
	}

	public static function fetchApPayeeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchApPayee( sprintf( 'SELECT * FROM ap_payees WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeesByCid( $intCid, $objDatabase ) {
		return self::fetchApPayees( sprintf( 'SELECT * FROM ap_payees WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeesByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objDatabase ) {
		return self::fetchApPayees( sprintf( 'SELECT * FROM ap_payees WHERE integration_database_id = %d AND cid = %d', ( int ) $intIntegrationDatabaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeesByApPayeeTermIdByCid( $intApPayeeTermId, $intCid, $objDatabase ) {
		return self::fetchApPayees( sprintf( 'SELECT * FROM ap_payees WHERE ap_payee_term_id = %d AND cid = %d', ( int ) $intApPayeeTermId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeesByDefaultApRemittanceIdByCid( $intDefaultApRemittanceId, $intCid, $objDatabase ) {
		return self::fetchApPayees( sprintf( 'SELECT * FROM ap_payees WHERE default_ap_remittance_id = %d AND cid = %d', ( int ) $intDefaultApRemittanceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeesByGlAccountIdByCid( $intGlAccountId, $intCid, $objDatabase ) {
		return self::fetchApPayees( sprintf( 'SELECT * FROM ap_payees WHERE gl_account_id = %d AND cid = %d', ( int ) $intGlAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeesByVendorIdByCid( $intVendorId, $intCid, $objDatabase ) {
		return self::fetchApPayees( sprintf( 'SELECT * FROM ap_payees WHERE vendor_id = %d AND cid = %d', ( int ) $intVendorId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeesByApPayeeTypeIdByCid( $intApPayeeTypeId, $intCid, $objDatabase ) {
		return self::fetchApPayees( sprintf( 'SELECT * FROM ap_payees WHERE ap_payee_type_id = %d AND cid = %d', ( int ) $intApPayeeTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeesByApPayeeStatusTypeIdByCid( $intApPayeeStatusTypeId, $intCid, $objDatabase ) {
		return self::fetchApPayees( sprintf( 'SELECT * FROM ap_payees WHERE ap_payee_status_type_id = %d AND cid = %d', ( int ) $intApPayeeStatusTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeesByCategorizationTypeIdByCid( $intCategorizationTypeId, $intCid, $objDatabase ) {
		return self::fetchApPayees( sprintf( 'SELECT * FROM ap_payees WHERE categorization_type_id = %d AND cid = %d', ( int ) $intCategorizationTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeesByConfidentialityAllowanceTypeIdByCid( $intConfidentialityAllowanceTypeId, $intCid, $objDatabase ) {
		return self::fetchApPayees( sprintf( 'SELECT * FROM ap_payees WHERE confidentiality_allowance_type_id = %d AND cid = %d', ( int ) $intConfidentialityAllowanceTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeesByCamAllowanceTypeIdByCid( $intCamAllowanceTypeId, $intCid, $objDatabase ) {
		return self::fetchApPayees( sprintf( 'SELECT * FROM ap_payees WHERE cam_allowance_type_id = %d AND cid = %d', ( int ) $intCamAllowanceTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeesByVendorCategoryTypeIdByCid( $intVendorCategoryTypeId, $intCid, $objDatabase ) {
		return self::fetchApPayees( sprintf( 'SELECT * FROM ap_payees WHERE vendor_category_type_id = %d AND cid = %d', ( int ) $intVendorCategoryTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeesByComplianceRulsetIdByCid( $intComplianceRulsetId, $intCid, $objDatabase ) {
		return self::fetchApPayees( sprintf( 'SELECT * FROM ap_payees WHERE compliance_rulset_id = %d AND cid = %d', ( int ) $intComplianceRulsetId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeesByReferenceIdByCid( $intReferenceId, $intCid, $objDatabase ) {
		return self::fetchApPayees( sprintf( 'SELECT * FROM ap_payees WHERE reference_id = %d AND cid = %d', ( int ) $intReferenceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchApPayeesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchApPayees( sprintf( 'SELECT * FROM ap_payees WHERE customer_id = %d AND cid = %d', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

}
?>