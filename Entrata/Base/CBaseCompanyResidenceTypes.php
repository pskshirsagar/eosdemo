<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyResidenceTypes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyResidenceTypes extends CEosPluralBase {

	/**
	 * @return CCompanyResidenceType[]
	 */
	public static function fetchCompanyResidenceTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCompanyResidenceType', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCompanyResidenceType
	 */
	public static function fetchCompanyResidenceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyResidenceType', $objDatabase );
	}

	public static function fetchCompanyResidenceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_residence_types', $objDatabase );
	}

	public static function fetchCompanyResidenceTypeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCompanyResidenceType( sprintf( 'SELECT * FROM company_residence_types WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyResidenceTypesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyResidenceTypes( sprintf( 'SELECT * FROM company_residence_types WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>