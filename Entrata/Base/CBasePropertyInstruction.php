<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyInstruction extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.property_instructions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyInstructionId;
	protected $m_strInstruction;
	protected $m_strInstructionDetails;
	protected $m_boolIsVisible;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intCompanyEmployeeContactTypeId;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = '1';
		$this->m_boolIsVisible = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_instruction_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyInstructionId', trim( $arrValues['property_instruction_id'] ) ); elseif( isset( $arrValues['property_instruction_id'] ) ) $this->setPropertyInstructionId( $arrValues['property_instruction_id'] );
		if( isset( $arrValues['instruction'] ) && $boolDirectSet ) $this->set( 'm_strInstruction', trim( $arrValues['instruction'] ) ); elseif( isset( $arrValues['instruction'] ) ) $this->setInstruction( $arrValues['instruction'] );
		if( isset( $arrValues['instruction_details'] ) && $boolDirectSet ) $this->set( 'm_strInstructionDetails', trim( $arrValues['instruction_details'] ) ); elseif( isset( $arrValues['instruction_details'] ) ) $this->setInstructionDetails( $arrValues['instruction_details'] );
		if( isset( $arrValues['is_visible'] ) && $boolDirectSet ) $this->set( 'm_boolIsVisible', trim( stripcslashes( $arrValues['is_visible'] ) ) ); elseif( isset( $arrValues['is_visible'] ) ) $this->setIsVisible( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_visible'] ) : $arrValues['is_visible'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['company_employee_contact_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeContactTypeId', trim( $arrValues['company_employee_contact_type_id'] ) ); elseif( isset( $arrValues['company_employee_contact_type_id'] ) ) $this->setCompanyEmployeeContactTypeId( $arrValues['company_employee_contact_type_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : '1';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyInstructionId( $intPropertyInstructionId ) {
		$this->set( 'm_intPropertyInstructionId', CStrings::strToIntDef( $intPropertyInstructionId, NULL, false ) );
	}

	public function getPropertyInstructionId() {
		return $this->m_intPropertyInstructionId;
	}

	public function sqlPropertyInstructionId() {
		return ( true == isset( $this->m_intPropertyInstructionId ) ) ? ( string ) $this->m_intPropertyInstructionId : 'NULL';
	}

	public function setInstruction( $strInstruction ) {
		$this->set( 'm_strInstruction', CStrings::strTrimDef( $strInstruction, 1024, NULL, true ) );
	}

	public function getInstruction() {
		return $this->m_strInstruction;
	}

	public function sqlInstruction() {
		return ( true == isset( $this->m_strInstruction ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInstruction ) : '\'' . addslashes( $this->m_strInstruction ) . '\'' ) : 'NULL';
	}

	public function setInstructionDetails( $strInstructionDetails ) {
		$this->set( 'm_strInstructionDetails', CStrings::strTrimDef( $strInstructionDetails, 540, NULL, true ) );
	}

	public function getInstructionDetails() {
		return $this->m_strInstructionDetails;
	}

	public function sqlInstructionDetails() {
		return ( true == isset( $this->m_strInstructionDetails ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInstructionDetails ) : '\'' . addslashes( $this->m_strInstructionDetails ) . '\'' ) : 'NULL';
	}

	public function setIsVisible( $boolIsVisible ) {
		$this->set( 'm_boolIsVisible', CStrings::strToBool( $boolIsVisible ) );
	}

	public function getIsVisible() {
		return $this->m_boolIsVisible;
	}

	public function sqlIsVisible() {
		return ( true == isset( $this->m_boolIsVisible ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsVisible ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCompanyEmployeeContactTypeId( $intCompanyEmployeeContactTypeId ) {
		$this->set( 'm_intCompanyEmployeeContactTypeId', CStrings::strToIntDef( $intCompanyEmployeeContactTypeId, NULL, true ) );
	}

	public function getCompanyEmployeeContactTypeId() {
		return $this->m_intCompanyEmployeeContactTypeId;
	}

	public function sqlCompanyEmployeeContactTypeId() {
		return ( true == isset( $this->m_intCompanyEmployeeContactTypeId ) ) ? ( string ) $this->m_intCompanyEmployeeContactTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_instruction_id, instruction, instruction_details, is_visible, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, company_employee_contact_type_id, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyInstructionId() . ', ' .
						$this->sqlInstruction() . ', ' .
						$this->sqlInstructionDetails() . ', ' .
						$this->sqlIsVisible() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlCompanyEmployeeContactTypeId() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_instruction_id = ' . $this->sqlPropertyInstructionId(). ',' ; } elseif( true == array_key_exists( 'PropertyInstructionId', $this->getChangedColumns() ) ) { $strSql .= ' property_instruction_id = ' . $this->sqlPropertyInstructionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' instruction = ' . $this->sqlInstruction(). ',' ; } elseif( true == array_key_exists( 'Instruction', $this->getChangedColumns() ) ) { $strSql .= ' instruction = ' . $this->sqlInstruction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' instruction_details = ' . $this->sqlInstructionDetails(). ',' ; } elseif( true == array_key_exists( 'InstructionDetails', $this->getChangedColumns() ) ) { $strSql .= ' instruction_details = ' . $this->sqlInstructionDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_visible = ' . $this->sqlIsVisible(). ',' ; } elseif( true == array_key_exists( 'IsVisible', $this->getChangedColumns() ) ) { $strSql .= ' is_visible = ' . $this->sqlIsVisible() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_contact_type_id = ' . $this->sqlCompanyEmployeeContactTypeId(). ',' ; } elseif( true == array_key_exists( 'CompanyEmployeeContactTypeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_contact_type_id = ' . $this->sqlCompanyEmployeeContactTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_instruction_id' => $this->getPropertyInstructionId(),
			'instruction' => $this->getInstruction(),
			'instruction_details' => $this->getInstructionDetails(),
			'is_visible' => $this->getIsVisible(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'company_employee_contact_type_id' => $this->getCompanyEmployeeContactTypeId(),
			'details' => $this->getDetails()
		);
	}

}
?>