<?php

class CBaseReportItemDescription extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.report_item_descriptions';

	protected $m_intId;
	protected $m_intDefaultCid;
	protected $m_intDefaultReportVersionId;
	protected $m_intReportItemTypeId;
	protected $m_strItemKey;
	protected $m_strItemName;
	protected $m_strItemDescription;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intDefaultCid = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['default_cid'] ) && $boolDirectSet ) $this->set( 'm_intDefaultCid', trim( $arrValues['default_cid'] ) ); elseif( isset( $arrValues['default_cid'] ) ) $this->setDefaultCid( $arrValues['default_cid'] );
		if( isset( $arrValues['default_report_version_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultReportVersionId', trim( $arrValues['default_report_version_id'] ) ); elseif( isset( $arrValues['default_report_version_id'] ) ) $this->setDefaultReportVersionId( $arrValues['default_report_version_id'] );
		if( isset( $arrValues['report_item_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReportItemTypeId', trim( $arrValues['report_item_type_id'] ) ); elseif( isset( $arrValues['report_item_type_id'] ) ) $this->setReportItemTypeId( $arrValues['report_item_type_id'] );
		if( isset( $arrValues['item_key'] ) && $boolDirectSet ) $this->set( 'm_strItemKey', trim( stripcslashes( $arrValues['item_key'] ) ) ); elseif( isset( $arrValues['item_key'] ) ) $this->setItemKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['item_key'] ) : $arrValues['item_key'] );
		if( isset( $arrValues['item_name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strItemName', trim( stripcslashes( $arrValues['item_name'] ) ) ); elseif( isset( $arrValues['item_name'] ) ) $this->setItemName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['item_name'] ) : $arrValues['item_name'] );
		if( isset( $arrValues['item_description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strItemDescription', trim( stripcslashes( $arrValues['item_description'] ) ) ); elseif( isset( $arrValues['item_description'] ) ) $this->setItemDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['item_description'] ) : $arrValues['item_description'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDefaultCid( $intDefaultCid ) {
		$this->set( 'm_intDefaultCid', CStrings::strToIntDef( $intDefaultCid, NULL, false ) );
	}

	public function getDefaultCid() {
		return $this->m_intDefaultCid;
	}

	public function sqlDefaultCid() {
		return ( true == isset( $this->m_intDefaultCid ) ) ? ( string ) $this->m_intDefaultCid : '1';
	}

	public function setDefaultReportVersionId( $intDefaultReportVersionId ) {
		$this->set( 'm_intDefaultReportVersionId', CStrings::strToIntDef( $intDefaultReportVersionId, NULL, false ) );
	}

	public function getDefaultReportVersionId() {
		return $this->m_intDefaultReportVersionId;
	}

	public function sqlDefaultReportVersionId() {
		return ( true == isset( $this->m_intDefaultReportVersionId ) ) ? ( string ) $this->m_intDefaultReportVersionId : 'NULL';
	}

	public function setReportItemTypeId( $intReportItemTypeId ) {
		$this->set( 'm_intReportItemTypeId', CStrings::strToIntDef( $intReportItemTypeId, NULL, false ) );
	}

	public function getReportItemTypeId() {
		return $this->m_intReportItemTypeId;
	}

	public function sqlReportItemTypeId() {
		return ( true == isset( $this->m_intReportItemTypeId ) ) ? ( string ) $this->m_intReportItemTypeId : 'NULL';
	}

	public function setItemKey( $strItemKey ) {
		$this->set( 'm_strItemKey', CStrings::strTrimDef( $strItemKey, 50, NULL, true ) );
	}

	public function getItemKey() {
		return $this->m_strItemKey;
	}

	public function sqlItemKey() {
		return ( true == isset( $this->m_strItemKey ) ) ? '\'' . addslashes( $this->m_strItemKey ) . '\'' : 'NULL';
	}

	public function setItemName( $strItemName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strItemName', CStrings::strTrimDef( $strItemName, 50, NULL, true ), $strLocaleCode );
	}

	public function getItemName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strItemName', $strLocaleCode );
	}

	public function sqlItemName() {
		return ( true == isset( $this->m_strItemName ) ) ? '\'' . addslashes( $this->m_strItemName ) . '\'' : 'NULL';
	}

	public function setItemDescription( $strItemDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strItemDescription', CStrings::strTrimDef( $strItemDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getItemDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strItemDescription', $strLocaleCode );
	}

	public function sqlItemDescription() {
		return ( true == isset( $this->m_strItemDescription ) ) ? '\'' . addslashes( $this->m_strItemDescription ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, default_cid, default_report_version_id, report_item_type_id, item_key, item_name, item_description, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlDefaultCid() . ', ' .
						$this->sqlDefaultReportVersionId() . ', ' .
						$this->sqlReportItemTypeId() . ', ' .
						$this->sqlItemKey() . ', ' .
						$this->sqlItemName() . ', ' .
						$this->sqlItemDescription() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_cid = ' . $this->sqlDefaultCid(). ',' ; } elseif( true == array_key_exists( 'DefaultCid', $this->getChangedColumns() ) ) { $strSql .= ' default_cid = ' . $this->sqlDefaultCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_report_version_id = ' . $this->sqlDefaultReportVersionId(). ',' ; } elseif( true == array_key_exists( 'DefaultReportVersionId', $this->getChangedColumns() ) ) { $strSql .= ' default_report_version_id = ' . $this->sqlDefaultReportVersionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_item_type_id = ' . $this->sqlReportItemTypeId(). ',' ; } elseif( true == array_key_exists( 'ReportItemTypeId', $this->getChangedColumns() ) ) { $strSql .= ' report_item_type_id = ' . $this->sqlReportItemTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' item_key = ' . $this->sqlItemKey(). ',' ; } elseif( true == array_key_exists( 'ItemKey', $this->getChangedColumns() ) ) { $strSql .= ' item_key = ' . $this->sqlItemKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' item_name = ' . $this->sqlItemName(). ',' ; } elseif( true == array_key_exists( 'ItemName', $this->getChangedColumns() ) ) { $strSql .= ' item_name = ' . $this->sqlItemName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' item_description = ' . $this->sqlItemDescription(). ',' ; } elseif( true == array_key_exists( 'ItemDescription', $this->getChangedColumns() ) ) { $strSql .= ' item_description = ' . $this->sqlItemDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'default_cid' => $this->getDefaultCid(),
			'default_report_version_id' => $this->getDefaultReportVersionId(),
			'report_item_type_id' => $this->getReportItemTypeId(),
			'item_key' => $this->getItemKey(),
			'item_name' => $this->getItemName(),
			'item_description' => $this->getItemDescription(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>