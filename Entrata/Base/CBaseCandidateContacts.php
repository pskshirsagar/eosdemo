<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCandidateContacts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCandidateContacts extends CEosPluralBase {

	/**
	 * @return CCandidateContact[]
	 */
	public static function fetchCandidateContacts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCandidateContact', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCandidateContact
	 */
	public static function fetchCandidateContact( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCandidateContact', $objDatabase );
	}

	public static function fetchCandidateContactCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'candidate_contacts', $objDatabase );
	}

	public static function fetchCandidateContactByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCandidateContact( sprintf( 'SELECT * FROM candidate_contacts WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCandidateContactsByCid( $intCid, $objDatabase ) {
		return self::fetchCandidateContacts( sprintf( 'SELECT * FROM candidate_contacts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCandidateContactsByCandidateIdByCid( $intCandidateId, $intCid, $objDatabase ) {
		return self::fetchCandidateContacts( sprintf( 'SELECT * FROM candidate_contacts WHERE candidate_id = %d AND cid = %d', ( int ) $intCandidateId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCandidateContactsByCandidateContactTypeIdByCid( $intCandidateContactTypeId, $intCid, $objDatabase ) {
		return self::fetchCandidateContacts( sprintf( 'SELECT * FROM candidate_contacts WHERE candidate_contact_type_id = %d AND cid = %d', ( int ) $intCandidateContactTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>