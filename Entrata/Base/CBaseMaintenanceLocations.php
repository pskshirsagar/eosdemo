<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceLocations
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceLocations extends CEosPluralBase {

	/**
	 * @return CMaintenanceLocation[]
	 */
	public static function fetchMaintenanceLocations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMaintenanceLocation::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMaintenanceLocation
	 */
	public static function fetchMaintenanceLocation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceLocation::class, $objDatabase );
	}

	public static function fetchMaintenanceLocationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_locations', $objDatabase );
	}

	public static function fetchMaintenanceLocationByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceLocation( sprintf( 'SELECT * FROM maintenance_locations WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceLocationsByCid( $intCid, $objDatabase ) {
		return self::fetchMaintenanceLocations( sprintf( 'SELECT * FROM maintenance_locations WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMaintenanceLocationsByMaintenanceLocationTypeIdByCid( $intMaintenanceLocationTypeId, $intCid, $objDatabase ) {
		return self::fetchMaintenanceLocations( sprintf( 'SELECT * FROM maintenance_locations WHERE maintenance_location_type_id = %d AND cid = %d', $intMaintenanceLocationTypeId, $intCid ), $objDatabase );
	}

}
?>