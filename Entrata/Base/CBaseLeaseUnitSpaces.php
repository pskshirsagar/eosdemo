<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseUnitSpaces
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseUnitSpaces extends CEosPluralBase {

	/**
	 * @return CLeaseUnitSpace[]
	 */
	public static function fetchLeaseUnitSpaces( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CLeaseUnitSpace', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CLeaseUnitSpace
	 */
	public static function fetchLeaseUnitSpace( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CLeaseUnitSpace', $objDatabase );
	}

	public static function fetchLeaseUnitSpaceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'lease_unit_spaces', $objDatabase );
	}

	public static function fetchLeaseUnitSpaceByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchLeaseUnitSpace( sprintf( 'SELECT * FROM lease_unit_spaces WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseUnitSpacesByCid( $intCid, $objDatabase ) {
		return self::fetchLeaseUnitSpaces( sprintf( 'SELECT * FROM lease_unit_spaces WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseUnitSpacesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchLeaseUnitSpaces( sprintf( 'SELECT * FROM lease_unit_spaces WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseUnitSpacesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchLeaseUnitSpaces( sprintf( 'SELECT * FROM lease_unit_spaces WHERE lease_id = %d AND cid = %d', ( int ) $intLeaseId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseUnitSpacesByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchLeaseUnitSpaces( sprintf( 'SELECT * FROM lease_unit_spaces WHERE unit_space_id = %d AND cid = %d', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

}
?>