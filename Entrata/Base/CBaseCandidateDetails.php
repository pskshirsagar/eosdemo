<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCandidateDetails
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCandidateDetails extends CEosPluralBase {

	/**
	 * @return CCandidateDetail[]
	 */
	public static function fetchCandidateDetails( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCandidateDetail', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCandidateDetail
	 */
	public static function fetchCandidateDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCandidateDetail', $objDatabase );
	}

	public static function fetchCandidateDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'candidate_details', $objDatabase );
	}

	public static function fetchCandidateDetailByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCandidateDetail( sprintf( 'SELECT * FROM candidate_details WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCandidateDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchCandidateDetails( sprintf( 'SELECT * FROM candidate_details WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCandidateDetailsByCandidateIdByCid( $intCandidateId, $intCid, $objDatabase ) {
		return self::fetchCandidateDetails( sprintf( 'SELECT * FROM candidate_details WHERE candidate_id = %d AND cid = %d', ( int ) $intCandidateId, ( int ) $intCid ), $objDatabase );
	}

}
?>