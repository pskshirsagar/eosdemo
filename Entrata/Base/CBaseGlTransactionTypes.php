<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlTransactionTypes
 * Do not add any new functions to this class.
 */

class CBaseGlTransactionTypes extends CEosPluralBase {

	/**
	 * @return CGlTransactionType[]
	 */
	public static function fetchGlTransactionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CGlTransactionType::class, $objDatabase );
	}

	/**
	 * @return CGlTransactionType
	 */
	public static function fetchGlTransactionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGlTransactionType::class, $objDatabase );
	}

	public static function fetchGlTransactionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'gl_transaction_types', $objDatabase );
	}

	public static function fetchGlTransactionTypeById( $intId, $objDatabase ) {
		return self::fetchGlTransactionType( sprintf( 'SELECT * FROM gl_transaction_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchGlTransactionTypesByGlLedgerTypeId( $intGlLedgerTypeId, $objDatabase ) {
		return self::fetchGlTransactionTypes( sprintf( 'SELECT * FROM gl_transaction_types WHERE gl_ledger_type_id = %d', $intGlLedgerTypeId ), $objDatabase );
	}

}
?>