<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChoreTypes
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseChoreTypes extends CEosPluralBase {

	/**
	 * @return CChoreType[]
	 */
	public static function fetchChoreTypes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CChoreType::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CChoreType
	 */
	public static function fetchChoreType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CChoreType::class, $objDatabase );
	}

	public static function fetchChoreTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'chore_types', $objDatabase );
	}

	public static function fetchChoreTypeByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchChoreType( sprintf( 'SELECT * FROM chore_types WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchChoreTypesByCid( $intCid, $objDatabase ) {
		return self::fetchChoreTypes( sprintf( 'SELECT * FROM chore_types WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchChoreTypesByDefaultChoreTypeIdByCid( $intDefaultChoreTypeId, $intCid, $objDatabase ) {
		return self::fetchChoreTypes( sprintf( 'SELECT * FROM chore_types WHERE default_chore_type_id = %d AND cid = %d', $intDefaultChoreTypeId, $intCid ), $objDatabase );
	}

	public static function fetchChoreTypesByChoreCategoryIdByCid( $intChoreCategoryId, $intCid, $objDatabase ) {
		return self::fetchChoreTypes( sprintf( 'SELECT * FROM chore_types WHERE chore_category_id = %d AND cid = %d', $intChoreCategoryId, $intCid ), $objDatabase );
	}

}
?>