<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBankAccountMerchantAccounts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBankAccountMerchantAccounts extends CEosPluralBase {

	/**
	 * @return CBankAccountMerchantAccount[]
	 */
	public static function fetchBankAccountMerchantAccounts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CBankAccountMerchantAccount', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CBankAccountMerchantAccount
	 */
	public static function fetchBankAccountMerchantAccount( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CBankAccountMerchantAccount', $objDatabase );
	}

	public static function fetchBankAccountMerchantAccountCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'bank_account_merchant_accounts', $objDatabase );
	}

	public static function fetchBankAccountMerchantAccountByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchBankAccountMerchantAccount( sprintf( 'SELECT * FROM bank_account_merchant_accounts WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBankAccountMerchantAccountsByCid( $intCid, $objDatabase ) {
		return self::fetchBankAccountMerchantAccounts( sprintf( 'SELECT * FROM bank_account_merchant_accounts WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBankAccountMerchantAccountsByBankAccountIdByCid( $intBankAccountId, $intCid, $objDatabase ) {
		return self::fetchBankAccountMerchantAccounts( sprintf( 'SELECT * FROM bank_account_merchant_accounts WHERE bank_account_id = %d AND cid = %d', ( int ) $intBankAccountId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBankAccountMerchantAccountsByMerchantAccountIdByCid( $intMerchantAccountId, $intCid, $objDatabase ) {
		return self::fetchBankAccountMerchantAccounts( sprintf( 'SELECT * FROM bank_account_merchant_accounts WHERE merchant_account_id = %d AND cid = %d', ( int ) $intMerchantAccountId, ( int ) $intCid ), $objDatabase );
	}

}
?>