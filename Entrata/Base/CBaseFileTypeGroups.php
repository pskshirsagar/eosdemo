<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileTypeGroups
 * Do not add any new functions to this class.
 */

class CBaseFileTypeGroups extends CEosPluralBase {

	/**
	 * @return CFileTypeGroup[]
	 */
	public static function fetchFileTypeGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CFileTypeGroup::class, $objDatabase );
	}

	/**
	 * @return CFileTypeGroup
	 */
	public static function fetchFileTypeGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFileTypeGroup::class, $objDatabase );
	}

	public static function fetchFileTypeGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'file_type_groups', $objDatabase );
	}

	public static function fetchFileTypeGroupById( $intId, $objDatabase ) {
		return self::fetchFileTypeGroup( sprintf( 'SELECT * FROM file_type_groups WHERE id = %d', $intId ), $objDatabase );
	}

}
?>