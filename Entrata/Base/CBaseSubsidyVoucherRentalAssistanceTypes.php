<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyVoucherRentalAssistanceTypes
 * Do not add any new functions to this class.
 */

class CBaseSubsidyVoucherRentalAssistanceTypes extends CEosPluralBase {

	/**
	 * @return CSubsidyVoucherRentalAssistanceType[]
	 */
	public static function fetchSubsidyVoucherRentalAssistanceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSubsidyVoucherRentalAssistanceType::class, $objDatabase );
	}

	/**
	 * @return CSubsidyVoucherRentalAssistanceType
	 */
	public static function fetchSubsidyVoucherRentalAssistanceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSubsidyVoucherRentalAssistanceType::class, $objDatabase );
	}

	public static function fetchSubsidyVoucherRentalAssistanceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'subsidy_voucher_rental_assistance_types', $objDatabase );
	}

	public static function fetchSubsidyVoucherRentalAssistanceTypeById( $intId, $objDatabase ) {
		return self::fetchSubsidyVoucherRentalAssistanceType( sprintf( 'SELECT * FROM subsidy_voucher_rental_assistance_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>