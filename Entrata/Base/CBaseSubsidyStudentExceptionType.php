<?php

class CBaseSubsidyStudentExceptionType extends CEosSingularBase {

	const TABLE_NAME = 'public.subsidy_student_exception_types';

	protected $m_intId;
	protected $m_strExceptionCode;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_arrintSubsidyTypeIds;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['exception_code'] ) && $boolDirectSet ) $this->set( 'm_strExceptionCode', trim( stripcslashes( $arrValues['exception_code'] ) ) ); elseif( isset( $arrValues['exception_code'] ) ) $this->setExceptionCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['exception_code'] ) : $arrValues['exception_code'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['subsidy_type_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintSubsidyTypeIds', trim( $arrValues['subsidy_type_ids'] ) ); elseif( isset( $arrValues['subsidy_type_ids'] ) ) $this->setSubsidyTypeIds( $arrValues['subsidy_type_ids'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setExceptionCode( $strExceptionCode ) {
		$this->set( 'm_strExceptionCode', CStrings::strTrimDef( $strExceptionCode, 30, NULL, true ) );
	}

	public function getExceptionCode() {
		return $this->m_strExceptionCode;
	}

	public function sqlExceptionCode() {
		return ( true == isset( $this->m_strExceptionCode ) ) ? '\'' . addslashes( $this->m_strExceptionCode ) . '\'' : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 500, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setSubsidyTypeIds( $arrintSubsidyTypeIds ) {
		$this->set( 'm_arrintSubsidyTypeIds', CStrings::strToArrIntDef( $arrintSubsidyTypeIds, NULL ) );
	}

	public function getSubsidyTypeIds() {
		return $this->m_arrintSubsidyTypeIds;
	}

	public function sqlSubsidyTypeIds() {
		return ( true == isset( $this->m_arrintSubsidyTypeIds ) && true == valArr( $this->m_arrintSubsidyTypeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintSubsidyTypeIds, NULL ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'exception_code' => $this->getExceptionCode(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'subsidy_type_ids' => $this->getSubsidyTypeIds(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>