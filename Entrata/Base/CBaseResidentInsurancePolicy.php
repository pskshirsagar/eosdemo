<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseResidentInsurancePolicy extends CEosSingularBase {

	const TABLE_NAME = 'public.resident_insurance_policies';

	protected $m_intId;
	protected $m_intEntityId;
	protected $m_intCid;
	protected $m_intInsurancePolicyId;
	protected $m_intPropertyId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_intLeadSourceTypeId;
	protected $m_intInsurancePolicyStatusTypeId;
	protected $m_intInsurancePolicyTypeId;
	protected $m_strRemotePrimaryKey;
	protected $m_strPolicyNumber;
	protected $m_fltPremiumAmount;
	protected $m_intIsIntegrated;
	protected $m_intIsArchived;
	protected $m_boolIsViewed;
	protected $m_intReplacedInsurancePolicyId;
	protected $m_strInsuranceCarrierName;
	protected $m_strPrimaryInsuredName;
	protected $m_strAdditionalInsureds;
	protected $m_fltDeductible;
	protected $m_fltLiabilityLimit;
	protected $m_fltPersonalLimit;
	protected $m_strUnitNumber;
	protected $m_strAgentDetails;
	protected $m_jsonAgentDetails;
	protected $m_strProofOfCoverageFileName;
	protected $m_strProofOfCoverageFilePath;
	protected $m_strProofOfCoverageUploadedOn;
	protected $m_strEffectiveDate;
	protected $m_strEndDate;
	protected $m_strLapsedOn;
	protected $m_strCancelledOn;
	protected $m_strConfirmedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intInsurancePolicyStatusTypeId = '1';
		$this->m_intInsurancePolicyTypeId = '1';
		$this->m_intIsIntegrated = '0';
		$this->m_intIsArchived = '0';
		$this->m_boolIsViewed = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['entity_id'] ) && $boolDirectSet ) $this->set( 'm_intEntityId', trim( $arrValues['entity_id'] ) ); elseif( isset( $arrValues['entity_id'] ) ) $this->setEntityId( $arrValues['entity_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['insurance_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyId', trim( $arrValues['insurance_policy_id'] ) ); elseif( isset( $arrValues['insurance_policy_id'] ) ) $this->setInsurancePolicyId( $arrValues['insurance_policy_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['lead_source_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeadSourceTypeId', trim( $arrValues['lead_source_type_id'] ) ); elseif( isset( $arrValues['lead_source_type_id'] ) ) $this->setLeadSourceTypeId( $arrValues['lead_source_type_id'] );
		if( isset( $arrValues['insurance_policy_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyStatusTypeId', trim( $arrValues['insurance_policy_status_type_id'] ) ); elseif( isset( $arrValues['insurance_policy_status_type_id'] ) ) $this->setInsurancePolicyStatusTypeId( $arrValues['insurance_policy_status_type_id'] );
		if( isset( $arrValues['insurance_policy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyTypeId', trim( $arrValues['insurance_policy_type_id'] ) ); elseif( isset( $arrValues['insurance_policy_type_id'] ) ) $this->setInsurancePolicyTypeId( $arrValues['insurance_policy_type_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['policy_number'] ) && $boolDirectSet ) $this->set( 'm_strPolicyNumber', trim( stripcslashes( $arrValues['policy_number'] ) ) ); elseif( isset( $arrValues['policy_number'] ) ) $this->setPolicyNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['policy_number'] ) : $arrValues['policy_number'] );
		if( isset( $arrValues['premium_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPremiumAmount', trim( $arrValues['premium_amount'] ) ); elseif( isset( $arrValues['premium_amount'] ) ) $this->setPremiumAmount( $arrValues['premium_amount'] );
		if( isset( $arrValues['is_integrated'] ) && $boolDirectSet ) $this->set( 'm_intIsIntegrated', trim( $arrValues['is_integrated'] ) ); elseif( isset( $arrValues['is_integrated'] ) ) $this->setIsIntegrated( $arrValues['is_integrated'] );
		if( isset( $arrValues['is_archived'] ) && $boolDirectSet ) $this->set( 'm_intIsArchived', trim( $arrValues['is_archived'] ) ); elseif( isset( $arrValues['is_archived'] ) ) $this->setIsArchived( $arrValues['is_archived'] );
		if( isset( $arrValues['is_viewed'] ) && $boolDirectSet ) $this->set( 'm_boolIsViewed', trim( stripcslashes( $arrValues['is_viewed'] ) ) ); elseif( isset( $arrValues['is_viewed'] ) ) $this->setIsViewed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_viewed'] ) : $arrValues['is_viewed'] );
		if( isset( $arrValues['replaced_insurance_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intReplacedInsurancePolicyId', trim( $arrValues['replaced_insurance_policy_id'] ) ); elseif( isset( $arrValues['replaced_insurance_policy_id'] ) ) $this->setReplacedInsurancePolicyId( $arrValues['replaced_insurance_policy_id'] );
		if( isset( $arrValues['insurance_carrier_name'] ) && $boolDirectSet ) $this->set( 'm_strInsuranceCarrierName', trim( stripcslashes( $arrValues['insurance_carrier_name'] ) ) ); elseif( isset( $arrValues['insurance_carrier_name'] ) ) $this->setInsuranceCarrierName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['insurance_carrier_name'] ) : $arrValues['insurance_carrier_name'] );
		if( isset( $arrValues['primary_insured_name'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryInsuredName', trim( stripcslashes( $arrValues['primary_insured_name'] ) ) ); elseif( isset( $arrValues['primary_insured_name'] ) ) $this->setPrimaryInsuredName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['primary_insured_name'] ) : $arrValues['primary_insured_name'] );
		if( isset( $arrValues['additional_insureds'] ) && $boolDirectSet ) $this->set( 'm_strAdditionalInsureds', trim( stripcslashes( $arrValues['additional_insureds'] ) ) ); elseif( isset( $arrValues['additional_insureds'] ) ) $this->setAdditionalInsureds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['additional_insureds'] ) : $arrValues['additional_insureds'] );
		if( isset( $arrValues['deductible'] ) && $boolDirectSet ) $this->set( 'm_fltDeductible', trim( $arrValues['deductible'] ) ); elseif( isset( $arrValues['deductible'] ) ) $this->setDeductible( $arrValues['deductible'] );
		if( isset( $arrValues['liability_limit'] ) && $boolDirectSet ) $this->set( 'm_fltLiabilityLimit', trim( $arrValues['liability_limit'] ) ); elseif( isset( $arrValues['liability_limit'] ) ) $this->setLiabilityLimit( $arrValues['liability_limit'] );
		if( isset( $arrValues['personal_limit'] ) && $boolDirectSet ) $this->set( 'm_fltPersonalLimit', trim( $arrValues['personal_limit'] ) ); elseif( isset( $arrValues['personal_limit'] ) ) $this->setPersonalLimit( $arrValues['personal_limit'] );
		if( isset( $arrValues['unit_number'] ) && $boolDirectSet ) $this->set( 'm_strUnitNumber', trim( stripcslashes( $arrValues['unit_number'] ) ) ); elseif( isset( $arrValues['unit_number'] ) ) $this->setUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_number'] ) : $arrValues['unit_number'] );
		if( isset( $arrValues['agent_details'] ) ) $this->set( 'm_strAgentDetails', trim( $arrValues['agent_details'] ) );
		if( isset( $arrValues['proof_of_coverage_file_name'] ) && $boolDirectSet ) $this->set( 'm_strProofOfCoverageFileName', trim( stripcslashes( $arrValues['proof_of_coverage_file_name'] ) ) ); elseif( isset( $arrValues['proof_of_coverage_file_name'] ) ) $this->setProofOfCoverageFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['proof_of_coverage_file_name'] ) : $arrValues['proof_of_coverage_file_name'] );
		if( isset( $arrValues['proof_of_coverage_file_path'] ) && $boolDirectSet ) $this->set( 'm_strProofOfCoverageFilePath', trim( stripcslashes( $arrValues['proof_of_coverage_file_path'] ) ) ); elseif( isset( $arrValues['proof_of_coverage_file_path'] ) ) $this->setProofOfCoverageFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['proof_of_coverage_file_path'] ) : $arrValues['proof_of_coverage_file_path'] );
		if( isset( $arrValues['proof_of_coverage_uploaded_on'] ) && $boolDirectSet ) $this->set( 'm_strProofOfCoverageUploadedOn', trim( $arrValues['proof_of_coverage_uploaded_on'] ) ); elseif( isset( $arrValues['proof_of_coverage_uploaded_on'] ) ) $this->setProofOfCoverageUploadedOn( $arrValues['proof_of_coverage_uploaded_on'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['lapsed_on'] ) && $boolDirectSet ) $this->set( 'm_strLapsedOn', trim( $arrValues['lapsed_on'] ) ); elseif( isset( $arrValues['lapsed_on'] ) ) $this->setLapsedOn( $arrValues['lapsed_on'] );
		if( isset( $arrValues['cancelled_on'] ) && $boolDirectSet ) $this->set( 'm_strCancelledOn', trim( $arrValues['cancelled_on'] ) ); elseif( isset( $arrValues['cancelled_on'] ) ) $this->setCancelledOn( $arrValues['cancelled_on'] );
		if( isset( $arrValues['confirmed_on'] ) && $boolDirectSet ) $this->set( 'm_strConfirmedOn', trim( $arrValues['confirmed_on'] ) ); elseif( isset( $arrValues['confirmed_on'] ) ) $this->setConfirmedOn( $arrValues['confirmed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEntityId( $intEntityId ) {
		$this->set( 'm_intEntityId', CStrings::strToIntDef( $intEntityId, NULL, false ) );
	}

	public function getEntityId() {
		return $this->m_intEntityId;
	}

	public function sqlEntityId() {
		return ( true == isset( $this->m_intEntityId ) ) ? ( string ) $this->m_intEntityId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setInsurancePolicyId( $intInsurancePolicyId ) {
		$this->set( 'm_intInsurancePolicyId', CStrings::strToIntDef( $intInsurancePolicyId, NULL, false ) );
	}

	public function getInsurancePolicyId() {
		return $this->m_intInsurancePolicyId;
	}

	public function sqlInsurancePolicyId() {
		return ( true == isset( $this->m_intInsurancePolicyId ) ) ? ( string ) $this->m_intInsurancePolicyId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setLeadSourceTypeId( $intLeadSourceTypeId ) {
		$this->set( 'm_intLeadSourceTypeId', CStrings::strToIntDef( $intLeadSourceTypeId, NULL, false ) );
	}

	public function getLeadSourceTypeId() {
		return $this->m_intLeadSourceTypeId;
	}

	public function sqlLeadSourceTypeId() {
		return ( true == isset( $this->m_intLeadSourceTypeId ) ) ? ( string ) $this->m_intLeadSourceTypeId : 'NULL';
	}

	public function setInsurancePolicyStatusTypeId( $intInsurancePolicyStatusTypeId ) {
		$this->set( 'm_intInsurancePolicyStatusTypeId', CStrings::strToIntDef( $intInsurancePolicyStatusTypeId, NULL, false ) );
	}

	public function getInsurancePolicyStatusTypeId() {
		return $this->m_intInsurancePolicyStatusTypeId;
	}

	public function sqlInsurancePolicyStatusTypeId() {
		return ( true == isset( $this->m_intInsurancePolicyStatusTypeId ) ) ? ( string ) $this->m_intInsurancePolicyStatusTypeId : '1';
	}

	public function setInsurancePolicyTypeId( $intInsurancePolicyTypeId ) {
		$this->set( 'm_intInsurancePolicyTypeId', CStrings::strToIntDef( $intInsurancePolicyTypeId, NULL, false ) );
	}

	public function getInsurancePolicyTypeId() {
		return $this->m_intInsurancePolicyTypeId;
	}

	public function sqlInsurancePolicyTypeId() {
		return ( true == isset( $this->m_intInsurancePolicyTypeId ) ) ? ( string ) $this->m_intInsurancePolicyTypeId : '1';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setPolicyNumber( $strPolicyNumber ) {
		$this->set( 'm_strPolicyNumber', CStrings::strTrimDef( $strPolicyNumber, 64, NULL, true ) );
	}

	public function getPolicyNumber() {
		return $this->m_strPolicyNumber;
	}

	public function sqlPolicyNumber() {
		return ( true == isset( $this->m_strPolicyNumber ) ) ? '\'' . addslashes( $this->m_strPolicyNumber ) . '\'' : 'NULL';
	}

	public function setPremiumAmount( $fltPremiumAmount ) {
		$this->set( 'm_fltPremiumAmount', CStrings::strToFloatDef( $fltPremiumAmount, NULL, false, 2 ) );
	}

	public function getPremiumAmount() {
		return $this->m_fltPremiumAmount;
	}

	public function sqlPremiumAmount() {
		return ( true == isset( $this->m_fltPremiumAmount ) ) ? ( string ) $this->m_fltPremiumAmount : 'NULL';
	}

	public function setIsIntegrated( $intIsIntegrated ) {
		$this->set( 'm_intIsIntegrated', CStrings::strToIntDef( $intIsIntegrated, NULL, false ) );
	}

	public function getIsIntegrated() {
		return $this->m_intIsIntegrated;
	}

	public function sqlIsIntegrated() {
		return ( true == isset( $this->m_intIsIntegrated ) ) ? ( string ) $this->m_intIsIntegrated : '0';
	}

	public function setIsArchived( $intIsArchived ) {
		$this->set( 'm_intIsArchived', CStrings::strToIntDef( $intIsArchived, NULL, false ) );
	}

	public function getIsArchived() {
		return $this->m_intIsArchived;
	}

	public function sqlIsArchived() {
		return ( true == isset( $this->m_intIsArchived ) ) ? ( string ) $this->m_intIsArchived : '0';
	}

	public function setIsViewed( $boolIsViewed ) {
		$this->set( 'm_boolIsViewed', CStrings::strToBool( $boolIsViewed ) );
	}

	public function getIsViewed() {
		return $this->m_boolIsViewed;
	}

	public function sqlIsViewed() {
		return ( true == isset( $this->m_boolIsViewed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsViewed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setReplacedInsurancePolicyId( $intReplacedInsurancePolicyId ) {
		$this->set( 'm_intReplacedInsurancePolicyId', CStrings::strToIntDef( $intReplacedInsurancePolicyId, NULL, false ) );
	}

	public function getReplacedInsurancePolicyId() {
		return $this->m_intReplacedInsurancePolicyId;
	}

	public function sqlReplacedInsurancePolicyId() {
		return ( true == isset( $this->m_intReplacedInsurancePolicyId ) ) ? ( string ) $this->m_intReplacedInsurancePolicyId : 'NULL';
	}

	public function setInsuranceCarrierName( $strInsuranceCarrierName ) {
		$this->set( 'm_strInsuranceCarrierName', CStrings::strTrimDef( $strInsuranceCarrierName, 50, NULL, true ) );
	}

	public function getInsuranceCarrierName() {
		return $this->m_strInsuranceCarrierName;
	}

	public function sqlInsuranceCarrierName() {
		return ( true == isset( $this->m_strInsuranceCarrierName ) ) ? '\'' . addslashes( $this->m_strInsuranceCarrierName ) . '\'' : 'NULL';
	}

	public function setPrimaryInsuredName( $strPrimaryInsuredName ) {
		$this->set( 'm_strPrimaryInsuredName', CStrings::strTrimDef( $strPrimaryInsuredName, 50, NULL, true ) );
	}

	public function getPrimaryInsuredName() {
		return $this->m_strPrimaryInsuredName;
	}

	public function sqlPrimaryInsuredName() {
		return ( true == isset( $this->m_strPrimaryInsuredName ) ) ? '\'' . addslashes( $this->m_strPrimaryInsuredName ) . '\'' : 'NULL';
	}

	public function setAdditionalInsureds( $strAdditionalInsureds ) {
		$this->set( 'm_strAdditionalInsureds', CStrings::strTrimDef( $strAdditionalInsureds, 200, NULL, true ) );
	}

	public function getAdditionalInsureds() {
		return $this->m_strAdditionalInsureds;
	}

	public function sqlAdditionalInsureds() {
		return ( true == isset( $this->m_strAdditionalInsureds ) ) ? '\'' . addslashes( $this->m_strAdditionalInsureds ) . '\'' : 'NULL';
	}

	public function setDeductible( $fltDeductible ) {
		$this->set( 'm_fltDeductible', CStrings::strToFloatDef( $fltDeductible, NULL, false, 2 ) );
	}

	public function getDeductible() {
		return $this->m_fltDeductible;
	}

	public function sqlDeductible() {
		return ( true == isset( $this->m_fltDeductible ) ) ? ( string ) $this->m_fltDeductible : 'NULL';
	}

	public function setLiabilityLimit( $fltLiabilityLimit ) {
		$this->set( 'm_fltLiabilityLimit', CStrings::strToFloatDef( $fltLiabilityLimit, NULL, false, 2 ) );
	}

	public function getLiabilityLimit() {
		return $this->m_fltLiabilityLimit;
	}

	public function sqlLiabilityLimit() {
		return ( true == isset( $this->m_fltLiabilityLimit ) ) ? ( string ) $this->m_fltLiabilityLimit : 'NULL';
	}

	public function setPersonalLimit( $fltPersonalLimit ) {
		$this->set( 'm_fltPersonalLimit', CStrings::strToFloatDef( $fltPersonalLimit, NULL, false, 2 ) );
	}

	public function getPersonalLimit() {
		return $this->m_fltPersonalLimit;
	}

	public function sqlPersonalLimit() {
		return ( true == isset( $this->m_fltPersonalLimit ) ) ? ( string ) $this->m_fltPersonalLimit : 'NULL';
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->set( 'm_strUnitNumber', CStrings::strTrimDef( $strUnitNumber, 50, NULL, true ) );
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function sqlUnitNumber() {
		return ( true == isset( $this->m_strUnitNumber ) ) ? '\'' . addslashes( $this->m_strUnitNumber ) . '\'' : 'NULL';
	}

	public function setAgentDetails( $jsonAgentDetails ) {
		if( true == valObj( $jsonAgentDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonAgentDetails', $jsonAgentDetails );
		} else {
			$this->set( 'm_jsonAgentDetails', NULL ); 
		}
		unset( $this->m_strAgentDetails );
	}

	public function getAgentDetails() {
		if( true == isset( $this->m_strAgentDetails ) ) {
			$this->m_jsonAgentDetails = CStrings::strToJson( $this->m_strAgentDetails );
			unset( $this->m_strAgentDetails );
		}
		return $this->m_jsonAgentDetails;
	}

	public function sqlAgentDetails() {
		if( true == isset( $this->m_jsonAgentDetails ) && true == valObj( $this->m_jsonAgentDetails, 'stdClass' ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->m_jsonAgentDetails ) ) . '\'';
		} elseif( true == isset( $this->m_strAgentDetails ) && 0 < strlen( trim( $this->m_strAgentDetails ) ) && true == valJsonString( $this->m_strAgentDetails ) ) {
			return	'\'' . addslashes( $this->m_strAgentDetails ) . '\'';
		}
		return 'NULL';
	}

	public function setProofOfCoverageFileName( $strProofOfCoverageFileName ) {
		$this->set( 'm_strProofOfCoverageFileName', CStrings::strTrimDef( $strProofOfCoverageFileName, 240, NULL, true ) );
	}

	public function getProofOfCoverageFileName() {
		return $this->m_strProofOfCoverageFileName;
	}

	public function sqlProofOfCoverageFileName() {
		return ( true == isset( $this->m_strProofOfCoverageFileName ) ) ? '\'' . addslashes( $this->m_strProofOfCoverageFileName ) . '\'' : 'NULL';
	}

	public function setProofOfCoverageFilePath( $strProofOfCoverageFilePath ) {
		$this->set( 'm_strProofOfCoverageFilePath', CStrings::strTrimDef( $strProofOfCoverageFilePath, -1, NULL, true ) );
	}

	public function getProofOfCoverageFilePath() {
		return $this->m_strProofOfCoverageFilePath;
	}

	public function sqlProofOfCoverageFilePath() {
		return ( true == isset( $this->m_strProofOfCoverageFilePath ) ) ? '\'' . addslashes( $this->m_strProofOfCoverageFilePath ) . '\'' : 'NULL';
	}

	public function setProofOfCoverageUploadedOn( $strProofOfCoverageUploadedOn ) {
		$this->set( 'm_strProofOfCoverageUploadedOn', CStrings::strTrimDef( $strProofOfCoverageUploadedOn, -1, NULL, true ) );
	}

	public function getProofOfCoverageUploadedOn() {
		return $this->m_strProofOfCoverageUploadedOn;
	}

	public function sqlProofOfCoverageUploadedOn() {
		return ( true == isset( $this->m_strProofOfCoverageUploadedOn ) ) ? '\'' . $this->m_strProofOfCoverageUploadedOn . '\'' : 'NULL';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NOW()';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setLapsedOn( $strLapsedOn ) {
		$this->set( 'm_strLapsedOn', CStrings::strTrimDef( $strLapsedOn, -1, NULL, true ) );
	}

	public function getLapsedOn() {
		return $this->m_strLapsedOn;
	}

	public function sqlLapsedOn() {
		return ( true == isset( $this->m_strLapsedOn ) ) ? '\'' . $this->m_strLapsedOn . '\'' : 'NULL';
	}

	public function setCancelledOn( $strCancelledOn ) {
		$this->set( 'm_strCancelledOn', CStrings::strTrimDef( $strCancelledOn, -1, NULL, true ) );
	}

	public function getCancelledOn() {
		return $this->m_strCancelledOn;
	}

	public function sqlCancelledOn() {
		return ( true == isset( $this->m_strCancelledOn ) ) ? '\'' . $this->m_strCancelledOn . '\'' : 'NULL';
	}

	public function setConfirmedOn( $strConfirmedOn ) {
		$this->set( 'm_strConfirmedOn', CStrings::strTrimDef( $strConfirmedOn, -1, NULL, true ) );
	}

	public function getConfirmedOn() {
		return $this->m_strConfirmedOn;
	}

	public function sqlConfirmedOn() {
		return ( true == isset( $this->m_strConfirmedOn ) ) ? '\'' . $this->m_strConfirmedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, entity_id, cid, insurance_policy_id, property_id, property_unit_id, unit_space_id, lead_source_type_id, insurance_policy_status_type_id, insurance_policy_type_id, remote_primary_key, policy_number, premium_amount, is_integrated, is_archived, is_viewed, replaced_insurance_policy_id, insurance_carrier_name, primary_insured_name, additional_insureds, deductible, liability_limit, personal_limit, unit_number, agent_details, proof_of_coverage_file_name, proof_of_coverage_file_path, proof_of_coverage_uploaded_on, effective_date, end_date, lapsed_on, cancelled_on, confirmed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEntityId() . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlInsurancePolicyId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyUnitId() . ', ' .
 						$this->sqlUnitSpaceId() . ', ' .
 						$this->sqlLeadSourceTypeId() . ', ' .
 						$this->sqlInsurancePolicyStatusTypeId() . ', ' .
 						$this->sqlInsurancePolicyTypeId() . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlPolicyNumber() . ', ' .
 						$this->sqlPremiumAmount() . ', ' .
 						$this->sqlIsIntegrated() . ', ' .
 						$this->sqlIsArchived() . ', ' .
 						$this->sqlIsViewed() . ', ' .
 						$this->sqlReplacedInsurancePolicyId() . ', ' .
 						$this->sqlInsuranceCarrierName() . ', ' .
 						$this->sqlPrimaryInsuredName() . ', ' .
 						$this->sqlAdditionalInsureds() . ', ' .
 						$this->sqlDeductible() . ', ' .
 						$this->sqlLiabilityLimit() . ', ' .
 						$this->sqlPersonalLimit() . ', ' .
 						$this->sqlUnitNumber() . ', ' .
 						$this->sqlAgentDetails() . ', ' .
 						$this->sqlProofOfCoverageFileName() . ', ' .
 						$this->sqlProofOfCoverageFilePath() . ', ' .
 						$this->sqlProofOfCoverageUploadedOn() . ', ' .
 						$this->sqlEffectiveDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlLapsedOn() . ', ' .
 						$this->sqlCancelledOn() . ', ' .
 						$this->sqlConfirmedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entity_id = ' . $this->sqlEntityId(). ',' ; } elseif( true == array_key_exists( 'EntityId', $this->getChangedColumns() ) ) { $strSql .= ' entity_id = ' . $this->sqlEntityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId(). ',' ; } elseif( true == array_key_exists( 'InsurancePolicyId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_source_type_id = ' . $this->sqlLeadSourceTypeId(). ',' ; } elseif( true == array_key_exists( 'LeadSourceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lead_source_type_id = ' . $this->sqlLeadSourceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_status_type_id = ' . $this->sqlInsurancePolicyStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'InsurancePolicyStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_status_type_id = ' . $this->sqlInsurancePolicyStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_type_id = ' . $this->sqlInsurancePolicyTypeId(). ',' ; } elseif( true == array_key_exists( 'InsurancePolicyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_type_id = ' . $this->sqlInsurancePolicyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' policy_number = ' . $this->sqlPolicyNumber(). ',' ; } elseif( true == array_key_exists( 'PolicyNumber', $this->getChangedColumns() ) ) { $strSql .= ' policy_number = ' . $this->sqlPolicyNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' premium_amount = ' . $this->sqlPremiumAmount(). ',' ; } elseif( true == array_key_exists( 'PremiumAmount', $this->getChangedColumns() ) ) { $strSql .= ' premium_amount = ' . $this->sqlPremiumAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_integrated = ' . $this->sqlIsIntegrated(). ',' ; } elseif( true == array_key_exists( 'IsIntegrated', $this->getChangedColumns() ) ) { $strSql .= ' is_integrated = ' . $this->sqlIsIntegrated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived(). ',' ; } elseif( true == array_key_exists( 'IsArchived', $this->getChangedColumns() ) ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_viewed = ' . $this->sqlIsViewed(). ',' ; } elseif( true == array_key_exists( 'IsViewed', $this->getChangedColumns() ) ) { $strSql .= ' is_viewed = ' . $this->sqlIsViewed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' replaced_insurance_policy_id = ' . $this->sqlReplacedInsurancePolicyId(). ',' ; } elseif( true == array_key_exists( 'ReplacedInsurancePolicyId', $this->getChangedColumns() ) ) { $strSql .= ' replaced_insurance_policy_id = ' . $this->sqlReplacedInsurancePolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_carrier_name = ' . $this->sqlInsuranceCarrierName(). ',' ; } elseif( true == array_key_exists( 'InsuranceCarrierName', $this->getChangedColumns() ) ) { $strSql .= ' insurance_carrier_name = ' . $this->sqlInsuranceCarrierName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_insured_name = ' . $this->sqlPrimaryInsuredName(). ',' ; } elseif( true == array_key_exists( 'PrimaryInsuredName', $this->getChangedColumns() ) ) { $strSql .= ' primary_insured_name = ' . $this->sqlPrimaryInsuredName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' additional_insureds = ' . $this->sqlAdditionalInsureds(). ',' ; } elseif( true == array_key_exists( 'AdditionalInsureds', $this->getChangedColumns() ) ) { $strSql .= ' additional_insureds = ' . $this->sqlAdditionalInsureds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deductible = ' . $this->sqlDeductible(). ',' ; } elseif( true == array_key_exists( 'Deductible', $this->getChangedColumns() ) ) { $strSql .= ' deductible = ' . $this->sqlDeductible() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' liability_limit = ' . $this->sqlLiabilityLimit(). ',' ; } elseif( true == array_key_exists( 'LiabilityLimit', $this->getChangedColumns() ) ) { $strSql .= ' liability_limit = ' . $this->sqlLiabilityLimit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' personal_limit = ' . $this->sqlPersonalLimit(). ',' ; } elseif( true == array_key_exists( 'PersonalLimit', $this->getChangedColumns() ) ) { $strSql .= ' personal_limit = ' . $this->sqlPersonalLimit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber(). ',' ; } elseif( true == array_key_exists( 'UnitNumber', $this->getChangedColumns() ) ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' agent_details = ' . $this->sqlAgentDetails(). ',' ; } elseif( true == array_key_exists( 'AgentDetails', $this->getChangedColumns() ) ) { $strSql .= ' agent_details = ' . $this->sqlAgentDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proof_of_coverage_file_name = ' . $this->sqlProofOfCoverageFileName(). ',' ; } elseif( true == array_key_exists( 'ProofOfCoverageFileName', $this->getChangedColumns() ) ) { $strSql .= ' proof_of_coverage_file_name = ' . $this->sqlProofOfCoverageFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proof_of_coverage_file_path = ' . $this->sqlProofOfCoverageFilePath(). ',' ; } elseif( true == array_key_exists( 'ProofOfCoverageFilePath', $this->getChangedColumns() ) ) { $strSql .= ' proof_of_coverage_file_path = ' . $this->sqlProofOfCoverageFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proof_of_coverage_uploaded_on = ' . $this->sqlProofOfCoverageUploadedOn(). ',' ; } elseif( true == array_key_exists( 'ProofOfCoverageUploadedOn', $this->getChangedColumns() ) ) { $strSql .= ' proof_of_coverage_uploaded_on = ' . $this->sqlProofOfCoverageUploadedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'EffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate(). ',' ; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lapsed_on = ' . $this->sqlLapsedOn(). ',' ; } elseif( true == array_key_exists( 'LapsedOn', $this->getChangedColumns() ) ) { $strSql .= ' lapsed_on = ' . $this->sqlLapsedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cancelled_on = ' . $this->sqlCancelledOn(). ',' ; } elseif( true == array_key_exists( 'CancelledOn', $this->getChangedColumns() ) ) { $strSql .= ' cancelled_on = ' . $this->sqlCancelledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn(). ',' ; } elseif( true == array_key_exists( 'ConfirmedOn', $this->getChangedColumns() ) ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'entity_id' => $this->getEntityId(),
			'cid' => $this->getCid(),
			'insurance_policy_id' => $this->getInsurancePolicyId(),
			'property_id' => $this->getPropertyId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'lead_source_type_id' => $this->getLeadSourceTypeId(),
			'insurance_policy_status_type_id' => $this->getInsurancePolicyStatusTypeId(),
			'insurance_policy_type_id' => $this->getInsurancePolicyTypeId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'policy_number' => $this->getPolicyNumber(),
			'premium_amount' => $this->getPremiumAmount(),
			'is_integrated' => $this->getIsIntegrated(),
			'is_archived' => $this->getIsArchived(),
			'is_viewed' => $this->getIsViewed(),
			'replaced_insurance_policy_id' => $this->getReplacedInsurancePolicyId(),
			'insurance_carrier_name' => $this->getInsuranceCarrierName(),
			'primary_insured_name' => $this->getPrimaryInsuredName(),
			'additional_insureds' => $this->getAdditionalInsureds(),
			'deductible' => $this->getDeductible(),
			'liability_limit' => $this->getLiabilityLimit(),
			'personal_limit' => $this->getPersonalLimit(),
			'unit_number' => $this->getUnitNumber(),
			'agent_details' => $this->getAgentDetails(),
			'proof_of_coverage_file_name' => $this->getProofOfCoverageFileName(),
			'proof_of_coverage_file_path' => $this->getProofOfCoverageFilePath(),
			'proof_of_coverage_uploaded_on' => $this->getProofOfCoverageUploadedOn(),
			'effective_date' => $this->getEffectiveDate(),
			'end_date' => $this->getEndDate(),
			'lapsed_on' => $this->getLapsedOn(),
			'cancelled_on' => $this->getCancelledOn(),
			'confirmed_on' => $this->getConfirmedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>