<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseScreeningApplicantResult extends CEosSingularBase {

	const TABLE_NAME = 'public.screening_applicant_results';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intScreeningApplicationRequestId;
	protected $m_intApplicationId;
	protected $m_intApplicantId;
	protected $m_intScreeningRecommendationTypeId;
	protected $m_strAdverseActionLetterSentOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strAdditionalDetails;
	protected $m_jsonAdditionalDetails;
	protected $m_intRequestStatusTypeId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['screening_application_request_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningApplicationRequestId', trim( $arrValues['screening_application_request_id'] ) ); elseif( isset( $arrValues['screening_application_request_id'] ) ) $this->setScreeningApplicationRequestId( $arrValues['screening_application_request_id'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantId', trim( $arrValues['applicant_id'] ) ); elseif( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
		if( isset( $arrValues['screening_recommendation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScreeningRecommendationTypeId', trim( $arrValues['screening_recommendation_type_id'] ) ); elseif( isset( $arrValues['screening_recommendation_type_id'] ) ) $this->setScreeningRecommendationTypeId( $arrValues['screening_recommendation_type_id'] );
		if( isset( $arrValues['adverse_action_letter_sent_on'] ) && $boolDirectSet ) $this->set( 'm_strAdverseActionLetterSentOn', trim( $arrValues['adverse_action_letter_sent_on'] ) ); elseif( isset( $arrValues['adverse_action_letter_sent_on'] ) ) $this->setAdverseActionLetterSentOn( $arrValues['adverse_action_letter_sent_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['additional_details'] ) ) $this->set( 'm_strAdditionalDetails', trim( $arrValues['additional_details'] ) );
		if( isset( $arrValues['request_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRequestStatusTypeId', trim( $arrValues['request_status_type_id'] ) ); elseif( isset( $arrValues['request_status_type_id'] ) ) $this->setRequestStatusTypeId( $arrValues['request_status_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setScreeningApplicationRequestId( $intScreeningApplicationRequestId ) {
		$this->set( 'm_intScreeningApplicationRequestId', CStrings::strToIntDef( $intScreeningApplicationRequestId, NULL, false ) );
	}

	public function getScreeningApplicationRequestId() {
		return $this->m_intScreeningApplicationRequestId;
	}

	public function sqlScreeningApplicationRequestId() {
		return ( true == isset( $this->m_intScreeningApplicationRequestId ) ) ? ( string ) $this->m_intScreeningApplicationRequestId : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setApplicantId( $intApplicantId ) {
		$this->set( 'm_intApplicantId', CStrings::strToIntDef( $intApplicantId, NULL, false ) );
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function sqlApplicantId() {
		return ( true == isset( $this->m_intApplicantId ) ) ? ( string ) $this->m_intApplicantId : 'NULL';
	}

	public function setScreeningRecommendationTypeId( $intScreeningRecommendationTypeId ) {
		$this->set( 'm_intScreeningRecommendationTypeId', CStrings::strToIntDef( $intScreeningRecommendationTypeId, NULL, false ) );
	}

	public function getScreeningRecommendationTypeId() {
		return $this->m_intScreeningRecommendationTypeId;
	}

	public function sqlScreeningRecommendationTypeId() {
		return ( true == isset( $this->m_intScreeningRecommendationTypeId ) ) ? ( string ) $this->m_intScreeningRecommendationTypeId : 'NULL';
	}

	public function setAdverseActionLetterSentOn( $strAdverseActionLetterSentOn ) {
		$this->set( 'm_strAdverseActionLetterSentOn', CStrings::strTrimDef( $strAdverseActionLetterSentOn, -1, NULL, true ) );
	}

	public function getAdverseActionLetterSentOn() {
		return $this->m_strAdverseActionLetterSentOn;
	}

	public function sqlAdverseActionLetterSentOn() {
		return ( true == isset( $this->m_strAdverseActionLetterSentOn ) ) ? '\'' . $this->m_strAdverseActionLetterSentOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setAdditionalDetails( $jsonAdditionalDetails ) {
		if( true == valObj( $jsonAdditionalDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonAdditionalDetails', $jsonAdditionalDetails );
		} elseif( true == valJsonString( $jsonAdditionalDetails ) ) {
			$this->set( 'm_jsonAdditionalDetails', CStrings::strToJson( $jsonAdditionalDetails ) );
		} else {
			$this->set( 'm_jsonAdditionalDetails', NULL );
		}
		unset( $this->m_strAdditionalDetails );
	}

	public function getAdditionalDetails() {
		if( true == isset( $this->m_strAdditionalDetails ) ) {
			$this->m_jsonAdditionalDetails = CStrings::strToJson( $this->m_strAdditionalDetails );
			unset( $this->m_strAdditionalDetails );
		}
		return $this->m_jsonAdditionalDetails;
	}

	public function sqlAdditionalDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getAdditionalDetails() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getAdditionalDetails() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setRequestStatusTypeId( $intRequestStatusTypeId ) {
		$this->set( 'm_intRequestStatusTypeId', CStrings::strToIntDef( $intRequestStatusTypeId, NULL, false ) );
	}

	public function getRequestStatusTypeId() {
		return $this->m_intRequestStatusTypeId;
	}

	public function sqlRequestStatusTypeId() {
		return ( true == isset( $this->m_intRequestStatusTypeId ) ) ? ( string ) $this->m_intRequestStatusTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, screening_application_request_id, application_id, applicant_id, screening_recommendation_type_id, adverse_action_letter_sent_on, updated_by, updated_on, created_by, created_on, additional_details, request_status_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlScreeningApplicationRequestId() . ', ' .
						$this->sqlApplicationId() . ', ' .
						$this->sqlApplicantId() . ', ' .
						$this->sqlScreeningRecommendationTypeId() . ', ' .
						$this->sqlAdverseActionLetterSentOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlAdditionalDetails() . ', ' .
						$this->sqlRequestStatusTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_application_request_id = ' . $this->sqlScreeningApplicationRequestId(). ',' ; } elseif( true == array_key_exists( 'ScreeningApplicationRequestId', $this->getChangedColumns() ) ) { $strSql .= ' screening_application_request_id = ' . $this->sqlScreeningApplicationRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId(). ',' ; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId(). ',' ; } elseif( true == array_key_exists( 'ApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' screening_recommendation_type_id = ' . $this->sqlScreeningRecommendationTypeId(). ',' ; } elseif( true == array_key_exists( 'ScreeningRecommendationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' screening_recommendation_type_id = ' . $this->sqlScreeningRecommendationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' adverse_action_letter_sent_on = ' . $this->sqlAdverseActionLetterSentOn(). ',' ; } elseif( true == array_key_exists( 'AdverseActionLetterSentOn', $this->getChangedColumns() ) ) { $strSql .= ' adverse_action_letter_sent_on = ' . $this->sqlAdverseActionLetterSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' additional_details = ' . $this->sqlAdditionalDetails(). ',' ; } elseif( true == array_key_exists( 'AdditionalDetails', $this->getChangedColumns() ) ) { $strSql .= ' additional_details = ' . $this->sqlAdditionalDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_status_type_id = ' . $this->sqlRequestStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'RequestStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' request_status_type_id = ' . $this->sqlRequestStatusTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'screening_application_request_id' => $this->getScreeningApplicationRequestId(),
			'application_id' => $this->getApplicationId(),
			'applicant_id' => $this->getApplicantId(),
			'screening_recommendation_type_id' => $this->getScreeningRecommendationTypeId(),
			'adverse_action_letter_sent_on' => $this->getAdverseActionLetterSentOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'additional_details' => $this->getAdditionalDetails(),
			'request_status_type_id' => $this->getRequestStatusTypeId()
		);
	}

}
?>