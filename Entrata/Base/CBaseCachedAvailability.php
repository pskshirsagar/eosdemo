<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCachedAvailability extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.cached_availabilities';

	protected $m_intId;
	protected $m_strAvailableDate;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyFloorplanId;
	protected $m_intUnitTypeId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_intOccupancyTypeId;
	protected $m_intGenderId;
	protected $m_boolHasAmenities;
	protected $m_boolHasPets;
	protected $m_boolHasSpecials;
	protected $m_boolIsAvailable;
	protected $m_boolIsAdvertised;
	protected $m_boolHasApplicant;
	protected $m_boolIsPreleased;
	protected $m_boolIsReserved;
	protected $m_boolIsMarketed;
	protected $m_boolIsOccupied;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolHasAmenities = false;
		$this->m_boolHasPets = false;
		$this->m_boolHasSpecials = false;
		$this->m_boolIsAvailable = false;
		$this->m_boolIsAdvertised = false;
		$this->m_boolHasApplicant = false;
		$this->m_boolIsPreleased = false;
		$this->m_boolIsReserved = false;
		$this->m_boolIsMarketed = true;
		$this->m_boolIsOccupied = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['available_date'] ) && $boolDirectSet ) $this->set( 'm_strAvailableDate', trim( $arrValues['available_date'] ) ); elseif( isset( $arrValues['available_date'] ) ) $this->setAvailableDate( $arrValues['available_date'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorplanId', trim( $arrValues['property_floorplan_id'] ) ); elseif( isset( $arrValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrValues['property_floorplan_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['occupancy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccupancyTypeId', trim( $arrValues['occupancy_type_id'] ) ); elseif( isset( $arrValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrValues['occupancy_type_id'] );
		if( isset( $arrValues['gender_id'] ) && $boolDirectSet ) $this->set( 'm_intGenderId', trim( $arrValues['gender_id'] ) ); elseif( isset( $arrValues['gender_id'] ) ) $this->setGenderId( $arrValues['gender_id'] );
		if( isset( $arrValues['has_amenities'] ) && $boolDirectSet ) $this->set( 'm_boolHasAmenities', trim( stripcslashes( $arrValues['has_amenities'] ) ) ); elseif( isset( $arrValues['has_amenities'] ) ) $this->setHasAmenities( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_amenities'] ) : $arrValues['has_amenities'] );
		if( isset( $arrValues['has_pets'] ) && $boolDirectSet ) $this->set( 'm_boolHasPets', trim( stripcslashes( $arrValues['has_pets'] ) ) ); elseif( isset( $arrValues['has_pets'] ) ) $this->setHasPets( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_pets'] ) : $arrValues['has_pets'] );
		if( isset( $arrValues['has_specials'] ) && $boolDirectSet ) $this->set( 'm_boolHasSpecials', trim( stripcslashes( $arrValues['has_specials'] ) ) ); elseif( isset( $arrValues['has_specials'] ) ) $this->setHasSpecials( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_specials'] ) : $arrValues['has_specials'] );
		if( isset( $arrValues['is_available'] ) && $boolDirectSet ) $this->set( 'm_boolIsAvailable', trim( stripcslashes( $arrValues['is_available'] ) ) ); elseif( isset( $arrValues['is_available'] ) ) $this->setIsAvailable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_available'] ) : $arrValues['is_available'] );
		if( isset( $arrValues['is_advertised'] ) && $boolDirectSet ) $this->set( 'm_boolIsAdvertised', trim( stripcslashes( $arrValues['is_advertised'] ) ) ); elseif( isset( $arrValues['is_advertised'] ) ) $this->setIsAdvertised( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_advertised'] ) : $arrValues['is_advertised'] );
		if( isset( $arrValues['has_applicant'] ) && $boolDirectSet ) $this->set( 'm_boolHasApplicant', trim( stripcslashes( $arrValues['has_applicant'] ) ) ); elseif( isset( $arrValues['has_applicant'] ) ) $this->setHasApplicant( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_applicant'] ) : $arrValues['has_applicant'] );
		if( isset( $arrValues['is_preleased'] ) && $boolDirectSet ) $this->set( 'm_boolIsPreleased', trim( stripcslashes( $arrValues['is_preleased'] ) ) ); elseif( isset( $arrValues['is_preleased'] ) ) $this->setIsPreleased( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_preleased'] ) : $arrValues['is_preleased'] );
		if( isset( $arrValues['is_reserved'] ) && $boolDirectSet ) $this->set( 'm_boolIsReserved', trim( stripcslashes( $arrValues['is_reserved'] ) ) ); elseif( isset( $arrValues['is_reserved'] ) ) $this->setIsReserved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_reserved'] ) : $arrValues['is_reserved'] );
		if( isset( $arrValues['is_marketed'] ) && $boolDirectSet ) $this->set( 'm_boolIsMarketed', trim( stripcslashes( $arrValues['is_marketed'] ) ) ); elseif( isset( $arrValues['is_marketed'] ) ) $this->setIsMarketed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_marketed'] ) : $arrValues['is_marketed'] );
		if( isset( $arrValues['is_occupied'] ) && $boolDirectSet ) $this->set( 'm_boolIsOccupied', trim( stripcslashes( $arrValues['is_occupied'] ) ) ); elseif( isset( $arrValues['is_occupied'] ) ) $this->setIsOccupied( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_occupied'] ) : $arrValues['is_occupied'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setAvailableDate( $strAvailableDate ) {
		$this->set( 'm_strAvailableDate', CStrings::strTrimDef( $strAvailableDate, -1, NULL, true ) );
	}

	public function getAvailableDate() {
		return $this->m_strAvailableDate;
	}

	public function sqlAvailableDate() {
		return ( true == isset( $this->m_strAvailableDate ) ) ? '\'' . $this->m_strAvailableDate . '\'' : 'NOW()';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->set( 'm_intPropertyFloorplanId', CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false ) );
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function sqlPropertyFloorplanId() {
		return ( true == isset( $this->m_intPropertyFloorplanId ) ) ? ( string ) $this->m_intPropertyFloorplanId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->set( 'm_intOccupancyTypeId', CStrings::strToIntDef( $intOccupancyTypeId, NULL, false ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function sqlOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? ( string ) $this->m_intOccupancyTypeId : 'NULL';
	}

	public function setGenderId( $intGenderId ) {
		$this->set( 'm_intGenderId', CStrings::strToIntDef( $intGenderId, NULL, false ) );
	}

	public function getGenderId() {
		return $this->m_intGenderId;
	}

	public function sqlGenderId() {
		return ( true == isset( $this->m_intGenderId ) ) ? ( string ) $this->m_intGenderId : 'NULL';
	}

	public function setHasAmenities( $boolHasAmenities ) {
		$this->set( 'm_boolHasAmenities', CStrings::strToBool( $boolHasAmenities ) );
	}

	public function getHasAmenities() {
		return $this->m_boolHasAmenities;
	}

	public function sqlHasAmenities() {
		return ( true == isset( $this->m_boolHasAmenities ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasAmenities ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasPets( $boolHasPets ) {
		$this->set( 'm_boolHasPets', CStrings::strToBool( $boolHasPets ) );
	}

	public function getHasPets() {
		return $this->m_boolHasPets;
	}

	public function sqlHasPets() {
		return ( true == isset( $this->m_boolHasPets ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasPets ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasSpecials( $boolHasSpecials ) {
		$this->set( 'm_boolHasSpecials', CStrings::strToBool( $boolHasSpecials ) );
	}

	public function getHasSpecials() {
		return $this->m_boolHasSpecials;
	}

	public function sqlHasSpecials() {
		return ( true == isset( $this->m_boolHasSpecials ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasSpecials ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsAvailable( $boolIsAvailable ) {
		$this->set( 'm_boolIsAvailable', CStrings::strToBool( $boolIsAvailable ) );
	}

	public function getIsAvailable() {
		return $this->m_boolIsAvailable;
	}

	public function sqlIsAvailable() {
		return ( true == isset( $this->m_boolIsAvailable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAvailable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsAdvertised( $boolIsAdvertised ) {
		$this->set( 'm_boolIsAdvertised', CStrings::strToBool( $boolIsAdvertised ) );
	}

	public function getIsAdvertised() {
		return $this->m_boolIsAdvertised;
	}

	public function sqlIsAdvertised() {
		return ( true == isset( $this->m_boolIsAdvertised ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAdvertised ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setHasApplicant( $boolHasApplicant ) {
		$this->set( 'm_boolHasApplicant', CStrings::strToBool( $boolHasApplicant ) );
	}

	public function getHasApplicant() {
		return $this->m_boolHasApplicant;
	}

	public function sqlHasApplicant() {
		return ( true == isset( $this->m_boolHasApplicant ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasApplicant ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPreleased( $boolIsPreleased ) {
		$this->set( 'm_boolIsPreleased', CStrings::strToBool( $boolIsPreleased ) );
	}

	public function getIsPreleased() {
		return $this->m_boolIsPreleased;
	}

	public function sqlIsPreleased() {
		return ( true == isset( $this->m_boolIsPreleased ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPreleased ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsReserved( $boolIsReserved ) {
		$this->set( 'm_boolIsReserved', CStrings::strToBool( $boolIsReserved ) );
	}

	public function getIsReserved() {
		return $this->m_boolIsReserved;
	}

	public function sqlIsReserved() {
		return ( true == isset( $this->m_boolIsReserved ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReserved ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsMarketed( $boolIsMarketed ) {
		$this->set( 'm_boolIsMarketed', CStrings::strToBool( $boolIsMarketed ) );
	}

	public function getIsMarketed() {
		return $this->m_boolIsMarketed;
	}

	public function sqlIsMarketed() {
		return ( true == isset( $this->m_boolIsMarketed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsMarketed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsOccupied( $boolIsOccupied ) {
		$this->set( 'm_boolIsOccupied', CStrings::strToBool( $boolIsOccupied ) );
	}

	public function getIsOccupied() {
		return $this->m_boolIsOccupied;
	}

	public function sqlIsOccupied() {
		return ( true == isset( $this->m_boolIsOccupied ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOccupied ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, available_date, cid, property_id, property_floorplan_id, unit_type_id, property_unit_id, unit_space_id, occupancy_type_id, gender_id, has_amenities, has_pets, has_specials, is_available, is_advertised, has_applicant, is_preleased, is_reserved, is_marketed, is_occupied, details, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlAvailableDate() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyFloorplanId() . ', ' .
						$this->sqlUnitTypeId() . ', ' .
						$this->sqlPropertyUnitId() . ', ' .
						$this->sqlUnitSpaceId() . ', ' .
						$this->sqlOccupancyTypeId() . ', ' .
						$this->sqlGenderId() . ', ' .
						$this->sqlHasAmenities() . ', ' .
						$this->sqlHasPets() . ', ' .
						$this->sqlHasSpecials() . ', ' .
						$this->sqlIsAvailable() . ', ' .
						$this->sqlIsAdvertised() . ', ' .
						$this->sqlHasApplicant() . ', ' .
						$this->sqlIsPreleased() . ', ' .
						$this->sqlIsReserved() . ', ' .
						$this->sqlIsMarketed() . ', ' .
						$this->sqlIsOccupied() . ', ' .
						$this->sqlDetails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' available_date = ' . $this->sqlAvailableDate(). ',' ; } elseif( true == array_key_exists( 'AvailableDate', $this->getChangedColumns() ) ) { $strSql .= ' available_date = ' . $this->sqlAvailableDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId(). ',' ; } elseif( true == array_key_exists( 'PropertyFloorplanId', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gender_id = ' . $this->sqlGenderId(). ',' ; } elseif( true == array_key_exists( 'GenderId', $this->getChangedColumns() ) ) { $strSql .= ' gender_id = ' . $this->sqlGenderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_amenities = ' . $this->sqlHasAmenities(). ',' ; } elseif( true == array_key_exists( 'HasAmenities', $this->getChangedColumns() ) ) { $strSql .= ' has_amenities = ' . $this->sqlHasAmenities() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_pets = ' . $this->sqlHasPets(). ',' ; } elseif( true == array_key_exists( 'HasPets', $this->getChangedColumns() ) ) { $strSql .= ' has_pets = ' . $this->sqlHasPets() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_specials = ' . $this->sqlHasSpecials(). ',' ; } elseif( true == array_key_exists( 'HasSpecials', $this->getChangedColumns() ) ) { $strSql .= ' has_specials = ' . $this->sqlHasSpecials() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_available = ' . $this->sqlIsAvailable(). ',' ; } elseif( true == array_key_exists( 'IsAvailable', $this->getChangedColumns() ) ) { $strSql .= ' is_available = ' . $this->sqlIsAvailable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_advertised = ' . $this->sqlIsAdvertised(). ',' ; } elseif( true == array_key_exists( 'IsAdvertised', $this->getChangedColumns() ) ) { $strSql .= ' is_advertised = ' . $this->sqlIsAdvertised() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_applicant = ' . $this->sqlHasApplicant(). ',' ; } elseif( true == array_key_exists( 'HasApplicant', $this->getChangedColumns() ) ) { $strSql .= ' has_applicant = ' . $this->sqlHasApplicant() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_preleased = ' . $this->sqlIsPreleased(). ',' ; } elseif( true == array_key_exists( 'IsPreleased', $this->getChangedColumns() ) ) { $strSql .= ' is_preleased = ' . $this->sqlIsPreleased() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reserved = ' . $this->sqlIsReserved(). ',' ; } elseif( true == array_key_exists( 'IsReserved', $this->getChangedColumns() ) ) { $strSql .= ' is_reserved = ' . $this->sqlIsReserved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_marketed = ' . $this->sqlIsMarketed(). ',' ; } elseif( true == array_key_exists( 'IsMarketed', $this->getChangedColumns() ) ) { $strSql .= ' is_marketed = ' . $this->sqlIsMarketed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_occupied = ' . $this->sqlIsOccupied(). ',' ; } elseif( true == array_key_exists( 'IsOccupied', $this->getChangedColumns() ) ) { $strSql .= ' is_occupied = ' . $this->sqlIsOccupied() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'available_date' => $this->getAvailableDate(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_floorplan_id' => $this->getPropertyFloorplanId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'occupancy_type_id' => $this->getOccupancyTypeId(),
			'gender_id' => $this->getGenderId(),
			'has_amenities' => $this->getHasAmenities(),
			'has_pets' => $this->getHasPets(),
			'has_specials' => $this->getHasSpecials(),
			'is_available' => $this->getIsAvailable(),
			'is_advertised' => $this->getIsAdvertised(),
			'has_applicant' => $this->getHasApplicant(),
			'is_preleased' => $this->getIsPreleased(),
			'is_reserved' => $this->getIsReserved(),
			'is_marketed' => $this->getIsMarketed(),
			'is_occupied' => $this->getIsOccupied(),
			'details' => $this->getDetails(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>