<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CStateMergeFieldGroups
 * Do not add any new functions to this class.
 */

class CBaseStateMergeFieldGroups extends CEosPluralBase {

	/**
	 * @return CStateMergeFieldGroup[]
	 */
	public static function fetchStateMergeFieldGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CStateMergeFieldGroup', $objDatabase );
	}

	/**
	 * @return CStateMergeFieldGroup
	 */
	public static function fetchStateMergeFieldGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CStateMergeFieldGroup', $objDatabase );
	}

	public static function fetchStateMergeFieldGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'state_merge_field_groups', $objDatabase );
	}

	public static function fetchStateMergeFieldGroupById( $intId, $objDatabase ) {
		return self::fetchStateMergeFieldGroup( sprintf( 'SELECT * FROM state_merge_field_groups WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchStateMergeFieldGroupsByMergeFieldGroupId( $intMergeFieldGroupId, $objDatabase ) {
		return self::fetchStateMergeFieldGroups( sprintf( 'SELECT * FROM state_merge_field_groups WHERE merge_field_group_id = %d', ( int ) $intMergeFieldGroupId ), $objDatabase );
	}

}
?>