<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseDrawRequest extends CEosSingularBase {

	const TABLE_NAME = 'public.draw_requests';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApPayeeId;
	protected $m_intPropertyId;
	protected $m_intJobId;
	protected $m_intLeaseId;
	protected $m_intArCodeId;
	protected $m_intArTransactionId;
	protected $m_intBankAccountId;
	protected $m_intArDepositTransactionId;
	protected $m_strName;
	protected $m_fltDrawAmount;
	protected $m_strDrawDate;
	protected $m_strDescription;
	protected $m_boolIsSendEmail;
	protected $m_strEmailMessage;
	protected $m_boolIsInitialImport;
	protected $m_strEmailedOn;
	protected $m_intDepositedBy;
	protected $m_strDepositedOn;
	protected $m_intPostedBy;
	protected $m_strPostedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltDrawAmount = '0';
		$this->m_boolIsSendEmail = false;
		$this->m_boolIsInitialImport = false;
		$this->m_intUpdatedBy = '1';
		$this->m_strUpdatedOn = 'now()';
		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ap_payee_id'] ) && $boolDirectSet ) $this->set( 'm_intApPayeeId', trim( $arrValues['ap_payee_id'] ) ); elseif( isset( $arrValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrValues['ap_payee_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['job_id'] ) && $boolDirectSet ) $this->set( 'm_intJobId', trim( $arrValues['job_id'] ) ); elseif( isset( $arrValues['job_id'] ) ) $this->setJobId( $arrValues['job_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['ar_code_id'] ) && $boolDirectSet ) $this->set( 'm_intArCodeId', trim( $arrValues['ar_code_id'] ) ); elseif( isset( $arrValues['ar_code_id'] ) ) $this->setArCodeId( $arrValues['ar_code_id'] );
		if( isset( $arrValues['ar_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intArTransactionId', trim( $arrValues['ar_transaction_id'] ) ); elseif( isset( $arrValues['ar_transaction_id'] ) ) $this->setArTransactionId( $arrValues['ar_transaction_id'] );
		if( isset( $arrValues['bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intBankAccountId', trim( $arrValues['bank_account_id'] ) ); elseif( isset( $arrValues['bank_account_id'] ) ) $this->setBankAccountId( $arrValues['bank_account_id'] );
		if( isset( $arrValues['ar_deposit_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intArDepositTransactionId', trim( $arrValues['ar_deposit_transaction_id'] ) ); elseif( isset( $arrValues['ar_deposit_transaction_id'] ) ) $this->setArDepositTransactionId( $arrValues['ar_deposit_transaction_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['draw_amount'] ) && $boolDirectSet ) $this->set( 'm_fltDrawAmount', trim( $arrValues['draw_amount'] ) ); elseif( isset( $arrValues['draw_amount'] ) ) $this->setDrawAmount( $arrValues['draw_amount'] );
		if( isset( $arrValues['draw_date'] ) && $boolDirectSet ) $this->set( 'm_strDrawDate', trim( $arrValues['draw_date'] ) ); elseif( isset( $arrValues['draw_date'] ) ) $this->setDrawDate( $arrValues['draw_date'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_send_email'] ) && $boolDirectSet ) $this->set( 'm_boolIsSendEmail', trim( stripcslashes( $arrValues['is_send_email'] ) ) ); elseif( isset( $arrValues['is_send_email'] ) ) $this->setIsSendEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_send_email'] ) : $arrValues['is_send_email'] );
		if( isset( $arrValues['email_message'] ) && $boolDirectSet ) $this->set( 'm_strEmailMessage', trim( stripcslashes( $arrValues['email_message'] ) ) ); elseif( isset( $arrValues['email_message'] ) ) $this->setEmailMessage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_message'] ) : $arrValues['email_message'] );
		if( isset( $arrValues['is_initial_import'] ) && $boolDirectSet ) $this->set( 'm_boolIsInitialImport', trim( stripcslashes( $arrValues['is_initial_import'] ) ) ); elseif( isset( $arrValues['is_initial_import'] ) ) $this->setIsInitialImport( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_initial_import'] ) : $arrValues['is_initial_import'] );
		if( isset( $arrValues['emailed_on'] ) && $boolDirectSet ) $this->set( 'm_strEmailedOn', trim( $arrValues['emailed_on'] ) ); elseif( isset( $arrValues['emailed_on'] ) ) $this->setEmailedOn( $arrValues['emailed_on'] );
		if( isset( $arrValues['deposited_by'] ) && $boolDirectSet ) $this->set( 'm_intDepositedBy', trim( $arrValues['deposited_by'] ) ); elseif( isset( $arrValues['deposited_by'] ) ) $this->setDepositedBy( $arrValues['deposited_by'] );
		if( isset( $arrValues['deposited_on'] ) && $boolDirectSet ) $this->set( 'm_strDepositedOn', trim( $arrValues['deposited_on'] ) ); elseif( isset( $arrValues['deposited_on'] ) ) $this->setDepositedOn( $arrValues['deposited_on'] );
		if( isset( $arrValues['posted_by'] ) && $boolDirectSet ) $this->set( 'm_intPostedBy', trim( $arrValues['posted_by'] ) ); elseif( isset( $arrValues['posted_by'] ) ) $this->setPostedBy( $arrValues['posted_by'] );
		if( isset( $arrValues['posted_on'] ) && $boolDirectSet ) $this->set( 'm_strPostedOn', trim( $arrValues['posted_on'] ) ); elseif( isset( $arrValues['posted_on'] ) ) $this->setPostedOn( $arrValues['posted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->set( 'm_intApPayeeId', CStrings::strToIntDef( $intApPayeeId, NULL, false ) );
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function sqlApPayeeId() {
		return ( true == isset( $this->m_intApPayeeId ) ) ? ( string ) $this->m_intApPayeeId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setJobId( $intJobId ) {
		$this->set( 'm_intJobId', CStrings::strToIntDef( $intJobId, NULL, false ) );
	}

	public function getJobId() {
		return $this->m_intJobId;
	}

	public function sqlJobId() {
		return ( true == isset( $this->m_intJobId ) ) ? ( string ) $this->m_intJobId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setArCodeId( $intArCodeId ) {
		$this->set( 'm_intArCodeId', CStrings::strToIntDef( $intArCodeId, NULL, false ) );
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function sqlArCodeId() {
		return ( true == isset( $this->m_intArCodeId ) ) ? ( string ) $this->m_intArCodeId : 'NULL';
	}

	public function setArTransactionId( $intArTransactionId ) {
		$this->set( 'm_intArTransactionId', CStrings::strToIntDef( $intArTransactionId, NULL, false ) );
	}

	public function getArTransactionId() {
		return $this->m_intArTransactionId;
	}

	public function sqlArTransactionId() {
		return ( true == isset( $this->m_intArTransactionId ) ) ? ( string ) $this->m_intArTransactionId : 'NULL';
	}

	public function setBankAccountId( $intBankAccountId ) {
		$this->set( 'm_intBankAccountId', CStrings::strToIntDef( $intBankAccountId, NULL, false ) );
	}

	public function getBankAccountId() {
		return $this->m_intBankAccountId;
	}

	public function sqlBankAccountId() {
		return ( true == isset( $this->m_intBankAccountId ) ) ? ( string ) $this->m_intBankAccountId : 'NULL';
	}

	public function setArDepositTransactionId( $intArDepositTransactionId ) {
		$this->set( 'm_intArDepositTransactionId', CStrings::strToIntDef( $intArDepositTransactionId, NULL, false ) );
	}

	public function getArDepositTransactionId() {
		return $this->m_intArDepositTransactionId;
	}

	public function sqlArDepositTransactionId() {
		return ( true == isset( $this->m_intArDepositTransactionId ) ) ? ( string ) $this->m_intArDepositTransactionId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDrawAmount( $fltDrawAmount ) {
		$this->set( 'm_fltDrawAmount', CStrings::strToFloatDef( $fltDrawAmount, NULL, false, 2 ) );
	}

	public function getDrawAmount() {
		return $this->m_fltDrawAmount;
	}

	public function sqlDrawAmount() {
		return ( true == isset( $this->m_fltDrawAmount ) ) ? ( string ) $this->m_fltDrawAmount : '0';
	}

	public function setDrawDate( $strDrawDate ) {
		$this->set( 'm_strDrawDate', CStrings::strTrimDef( $strDrawDate, -1, NULL, true ) );
	}

	public function getDrawDate() {
		return $this->m_strDrawDate;
	}

	public function sqlDrawDate() {
		return ( true == isset( $this->m_strDrawDate ) ) ? '\'' . $this->m_strDrawDate . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsSendEmail( $boolIsSendEmail ) {
		$this->set( 'm_boolIsSendEmail', CStrings::strToBool( $boolIsSendEmail ) );
	}

	public function getIsSendEmail() {
		return $this->m_boolIsSendEmail;
	}

	public function sqlIsSendEmail() {
		return ( true == isset( $this->m_boolIsSendEmail ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSendEmail ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setEmailMessage( $strEmailMessage ) {
		$this->set( 'm_strEmailMessage', CStrings::strTrimDef( $strEmailMessage, -1, NULL, true ) );
	}

	public function getEmailMessage() {
		return $this->m_strEmailMessage;
	}

	public function sqlEmailMessage() {
		return ( true == isset( $this->m_strEmailMessage ) ) ? '\'' . addslashes( $this->m_strEmailMessage ) . '\'' : 'NULL';
	}

	public function setIsInitialImport( $boolIsInitialImport ) {
		$this->set( 'm_boolIsInitialImport', CStrings::strToBool( $boolIsInitialImport ) );
	}

	public function getIsInitialImport() {
		return $this->m_boolIsInitialImport;
	}

	public function sqlIsInitialImport() {
		return ( true == isset( $this->m_boolIsInitialImport ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInitialImport ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setEmailedOn( $strEmailedOn ) {
		$this->set( 'm_strEmailedOn', CStrings::strTrimDef( $strEmailedOn, -1, NULL, true ) );
	}

	public function getEmailedOn() {
		return $this->m_strEmailedOn;
	}

	public function sqlEmailedOn() {
		return ( true == isset( $this->m_strEmailedOn ) ) ? '\'' . $this->m_strEmailedOn . '\'' : 'NULL';
	}

	public function setDepositedBy( $intDepositedBy ) {
		$this->set( 'm_intDepositedBy', CStrings::strToIntDef( $intDepositedBy, NULL, false ) );
	}

	public function getDepositedBy() {
		return $this->m_intDepositedBy;
	}

	public function sqlDepositedBy() {
		return ( true == isset( $this->m_intDepositedBy ) ) ? ( string ) $this->m_intDepositedBy : 'NULL';
	}

	public function setDepositedOn( $strDepositedOn ) {
		$this->set( 'm_strDepositedOn', CStrings::strTrimDef( $strDepositedOn, -1, NULL, true ) );
	}

	public function getDepositedOn() {
		return $this->m_strDepositedOn;
	}

	public function sqlDepositedOn() {
		return ( true == isset( $this->m_strDepositedOn ) ) ? '\'' . $this->m_strDepositedOn . '\'' : 'NULL';
	}

	public function setPostedBy( $intPostedBy ) {
		$this->set( 'm_intPostedBy', CStrings::strToIntDef( $intPostedBy, NULL, false ) );
	}

	public function getPostedBy() {
		return $this->m_intPostedBy;
	}

	public function sqlPostedBy() {
		return ( true == isset( $this->m_intPostedBy ) ) ? ( string ) $this->m_intPostedBy : 'NULL';
	}

	public function setPostedOn( $strPostedOn ) {
		$this->set( 'm_strPostedOn', CStrings::strTrimDef( $strPostedOn, -1, NULL, true ) );
	}

	public function getPostedOn() {
		return $this->m_strPostedOn;
	}

	public function sqlPostedOn() {
		return ( true == isset( $this->m_strPostedOn ) ) ? '\'' . $this->m_strPostedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : '1';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ap_payee_id, property_id, job_id, lease_id, ar_code_id, ar_transaction_id, bank_account_id, ar_deposit_transaction_id, name, draw_amount, draw_date, description, is_send_email, email_message, is_initial_import, emailed_on, deposited_by, deposited_on, posted_by, posted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlApPayeeId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlJobId() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlArCodeId() . ', ' .
 						$this->sqlArTransactionId() . ', ' .
 						$this->sqlBankAccountId() . ', ' .
 						$this->sqlArDepositTransactionId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDrawAmount() . ', ' .
 						$this->sqlDrawDate() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlIsSendEmail() . ', ' .
 						$this->sqlEmailMessage() . ', ' .
 						$this->sqlIsInitialImport() . ', ' .
 						$this->sqlEmailedOn() . ', ' .
 						$this->sqlDepositedBy() . ', ' .
 						$this->sqlDepositedOn() . ', ' .
 						$this->sqlPostedBy() . ', ' .
 						$this->sqlPostedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; } elseif( true == array_key_exists( 'ApPayeeId', $this->getChangedColumns() ) ) { $strSql .= ' ap_payee_id = ' . $this->sqlApPayeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_id = ' . $this->sqlJobId() . ','; } elseif( true == array_key_exists( 'JobId', $this->getChangedColumns() ) ) { $strSql .= ' job_id = ' . $this->sqlJobId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId() . ','; } elseif( true == array_key_exists( 'ArCodeId', $this->getChangedColumns() ) ) { $strSql .= ' ar_code_id = ' . $this->sqlArCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId() . ','; } elseif( true == array_key_exists( 'ArTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' ar_transaction_id = ' . $this->sqlArTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_account_id = ' . $this->sqlBankAccountId() . ','; } elseif( true == array_key_exists( 'BankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' bank_account_id = ' . $this->sqlBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_deposit_transaction_id = ' . $this->sqlArDepositTransactionId() . ','; } elseif( true == array_key_exists( 'ArDepositTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' ar_deposit_transaction_id = ' . $this->sqlArDepositTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' draw_amount = ' . $this->sqlDrawAmount() . ','; } elseif( true == array_key_exists( 'DrawAmount', $this->getChangedColumns() ) ) { $strSql .= ' draw_amount = ' . $this->sqlDrawAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' draw_date = ' . $this->sqlDrawDate() . ','; } elseif( true == array_key_exists( 'DrawDate', $this->getChangedColumns() ) ) { $strSql .= ' draw_date = ' . $this->sqlDrawDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_send_email = ' . $this->sqlIsSendEmail() . ','; } elseif( true == array_key_exists( 'IsSendEmail', $this->getChangedColumns() ) ) { $strSql .= ' is_send_email = ' . $this->sqlIsSendEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_message = ' . $this->sqlEmailMessage() . ','; } elseif( true == array_key_exists( 'EmailMessage', $this->getChangedColumns() ) ) { $strSql .= ' email_message = ' . $this->sqlEmailMessage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_initial_import = ' . $this->sqlIsInitialImport() . ','; } elseif( true == array_key_exists( 'IsInitialImport', $this->getChangedColumns() ) ) { $strSql .= ' is_initial_import = ' . $this->sqlIsInitialImport() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' emailed_on = ' . $this->sqlEmailedOn() . ','; } elseif( true == array_key_exists( 'EmailedOn', $this->getChangedColumns() ) ) { $strSql .= ' emailed_on = ' . $this->sqlEmailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposited_by = ' . $this->sqlDepositedBy() . ','; } elseif( true == array_key_exists( 'DepositedBy', $this->getChangedColumns() ) ) { $strSql .= ' deposited_by = ' . $this->sqlDepositedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposited_on = ' . $this->sqlDepositedOn() . ','; } elseif( true == array_key_exists( 'DepositedOn', $this->getChangedColumns() ) ) { $strSql .= ' deposited_on = ' . $this->sqlDepositedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' posted_by = ' . $this->sqlPostedBy() . ','; } elseif( true == array_key_exists( 'PostedBy', $this->getChangedColumns() ) ) { $strSql .= ' posted_by = ' . $this->sqlPostedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' posted_on = ' . $this->sqlPostedOn() . ','; } elseif( true == array_key_exists( 'PostedOn', $this->getChangedColumns() ) ) { $strSql .= ' posted_on = ' . $this->sqlPostedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ap_payee_id' => $this->getApPayeeId(),
			'property_id' => $this->getPropertyId(),
			'job_id' => $this->getJobId(),
			'lease_id' => $this->getLeaseId(),
			'ar_code_id' => $this->getArCodeId(),
			'ar_transaction_id' => $this->getArTransactionId(),
			'bank_account_id' => $this->getBankAccountId(),
			'ar_deposit_transaction_id' => $this->getArDepositTransactionId(),
			'name' => $this->getName(),
			'draw_amount' => $this->getDrawAmount(),
			'draw_date' => $this->getDrawDate(),
			'description' => $this->getDescription(),
			'is_send_email' => $this->getIsSendEmail(),
			'email_message' => $this->getEmailMessage(),
			'is_initial_import' => $this->getIsInitialImport(),
			'emailed_on' => $this->getEmailedOn(),
			'deposited_by' => $this->getDepositedBy(),
			'deposited_on' => $this->getDepositedOn(),
			'posted_by' => $this->getPostedBy(),
			'posted_on' => $this->getPostedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>