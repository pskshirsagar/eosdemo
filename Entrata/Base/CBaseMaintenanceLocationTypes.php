<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaintenanceLocationTypes
 * Do not add any new functions to this class.
 */

class CBaseMaintenanceLocationTypes extends CEosPluralBase {

	/**
	 * @return CMaintenanceLocationType[]
	 */
	public static function fetchMaintenanceLocationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMaintenanceLocationType::class, $objDatabase );
	}

	/**
	 * @return CMaintenanceLocationType
	 */
	public static function fetchMaintenanceLocationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMaintenanceLocationType::class, $objDatabase );
	}

	public static function fetchMaintenanceLocationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'maintenance_location_types', $objDatabase );
	}

	public static function fetchMaintenanceLocationTypeById( $intId, $objDatabase ) {
		return self::fetchMaintenanceLocationType( sprintf( 'SELECT * FROM maintenance_location_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>