<?php

class CBaseDataExportGroupHierarchy extends CEosSingularBase {

	const TABLE_NAME = 'public.data_export_group_hierarchies';

	protected $m_intId;
	protected $m_intDataExportGroupParentId;
	protected $m_intDataExportGroupChildId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['data_export_group_parent_id'] ) && $boolDirectSet ) $this->set( 'm_intDataExportGroupParentId', trim( $arrValues['data_export_group_parent_id'] ) ); elseif( isset( $arrValues['data_export_group_parent_id'] ) ) $this->setDataExportGroupParentId( $arrValues['data_export_group_parent_id'] );
		if( isset( $arrValues['data_export_group_child_id'] ) && $boolDirectSet ) $this->set( 'm_intDataExportGroupChildId', trim( $arrValues['data_export_group_child_id'] ) ); elseif( isset( $arrValues['data_export_group_child_id'] ) ) $this->setDataExportGroupChildId( $arrValues['data_export_group_child_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDataExportGroupParentId( $intDataExportGroupParentId ) {
		$this->set( 'm_intDataExportGroupParentId', CStrings::strToIntDef( $intDataExportGroupParentId, NULL, false ) );
	}

	public function getDataExportGroupParentId() {
		return $this->m_intDataExportGroupParentId;
	}

	public function sqlDataExportGroupParentId() {
		return ( true == isset( $this->m_intDataExportGroupParentId ) ) ? ( string ) $this->m_intDataExportGroupParentId : 'NULL';
	}

	public function setDataExportGroupChildId( $intDataExportGroupChildId ) {
		$this->set( 'm_intDataExportGroupChildId', CStrings::strToIntDef( $intDataExportGroupChildId, NULL, false ) );
	}

	public function getDataExportGroupChildId() {
		return $this->m_intDataExportGroupChildId;
	}

	public function sqlDataExportGroupChildId() {
		return ( true == isset( $this->m_intDataExportGroupChildId ) ) ? ( string ) $this->m_intDataExportGroupChildId : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'data_export_group_parent_id' => $this->getDataExportGroupParentId(),
			'data_export_group_child_id' => $this->getDataExportGroupChildId()
		);
	}

}
?>