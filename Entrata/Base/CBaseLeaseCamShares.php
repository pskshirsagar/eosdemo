<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseCamShares
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseLeaseCamShares extends CEosPluralBase {

	/**
	 * @return CLeaseCamShare[]
	 */
	public static function fetchLeaseCamShares( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CLeaseCamShare', $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CLeaseCamShare
	 */
	public static function fetchLeaseCamShare( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CLeaseCamShare', $objDatabase );
	}

	public static function fetchLeaseCamShareCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'lease_cam_shares', $objDatabase );
	}

	public static function fetchLeaseCamShareByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchLeaseCamShare( sprintf( 'SELECT * FROM lease_cam_shares WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseCamSharesByCid( $intCid, $objDatabase ) {
		return self::fetchLeaseCamShares( sprintf( 'SELECT * FROM lease_cam_shares WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseCamSharesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchLeaseCamShares( sprintf( 'SELECT * FROM lease_cam_shares WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseCamSharesByLeaseCamPoolIdByCid( $intLeaseCamPoolId, $intCid, $objDatabase ) {
		return self::fetchLeaseCamShares( sprintf( 'SELECT * FROM lease_cam_shares WHERE lease_cam_pool_id = %d AND cid = %d', ( int ) $intLeaseCamPoolId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseCamSharesByApCodeIdByCid( $intApCodeId, $intCid, $objDatabase ) {
		return self::fetchLeaseCamShares( sprintf( 'SELECT * FROM lease_cam_shares WHERE ap_code_id = %d AND cid = %d', ( int ) $intApCodeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseCamSharesByCamCalculationTypeIdByCid( $intCamCalculationTypeId, $intCid, $objDatabase ) {
		return self::fetchLeaseCamShares( sprintf( 'SELECT * FROM lease_cam_shares WHERE cam_calculation_type_id = %d AND cid = %d', ( int ) $intCamCalculationTypeId, ( int ) $intCid ), $objDatabase );
	}

}
?>