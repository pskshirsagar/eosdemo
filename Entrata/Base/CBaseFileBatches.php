<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileBatches
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseFileBatches extends CEosPluralBase {

	/**
	 * @return CFileBatch[]
	 */
	public static function fetchFileBatches( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CFileBatch::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CFileBatch
	 */
	public static function fetchFileBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFileBatch::class, $objDatabase );
	}

	public static function fetchFileBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'file_batches', $objDatabase );
	}

	public static function fetchFileBatchByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchFileBatch( sprintf( 'SELECT * FROM file_batches WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchFileBatchesByCid( $intCid, $objDatabase ) {
		return self::fetchFileBatches( sprintf( 'SELECT * FROM file_batches WHERE cid = %d', $intCid ), $objDatabase );
	}

}
?>