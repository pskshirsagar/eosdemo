<?php

class CBaseDefaultArRule extends CEosSingularBase {

    protected $m_intId;
    protected $m_intArOriginId;
    protected $m_intArCodeTypeId;
    protected $m_intArTriggerId;
    protected $m_intArActionId;
    protected $m_intStoreLeaseTerms;
    protected $m_intStoreLeaseStartWindows;
    protected $m_intStoreUnitOptions;
    protected $m_intUseNonStandardFrequencies;
    protected $m_intUseEffectiveDates;
    protected $m_intUseRateOffsets;
    protected $m_intUseArTiers;
    protected $m_intUseArMultipliers;
    protected $m_intUsePercentages;
    protected $m_intUseUnitOptions;
    protected $m_intUseMinMaxes;
    protected $m_intRequireCustomerRelationship;
    protected $m_intSeparateProspectVsRenewal;
    protected $m_intAllowDeactivationDate;
    protected $m_intRequireTriggerTypeCodes;
    protected $m_intRequireTriggerCodes;
    protected $m_intIsEnabled;
    protected $m_intOrderNum;

    public function __construct() {
        parent::__construct();

        $this->m_intStoreLeaseTerms = '0';
        $this->m_intStoreLeaseStartWindows = '0';
        $this->m_intStoreUnitOptions = '0';
        $this->m_intUseNonStandardFrequencies = '0';
        $this->m_intUseEffectiveDates = '0';
        $this->m_intUseRateOffsets = '0';
        $this->m_intUseArTiers = '0';
        $this->m_intUseArMultipliers = '0';
        $this->m_intUsePercentages = '0';
        $this->m_intUseUnitOptions = '0';
        $this->m_intUseMinMaxes = '0';
        $this->m_intRequireCustomerRelationship = '0';
        $this->m_intSeparateProspectVsRenewal = '0';
        $this->m_intAllowDeactivationDate = '0';
        $this->m_intRequireTriggerTypeCodes = '0';
        $this->m_intRequireTriggerCodes = '0';
        $this->m_intIsEnabled = '0';
        $this->m_intOrderNum = '0';

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['ar_origin_id'] ) && $boolDirectSet ) $this->m_intArOriginId = trim( $arrValues['ar_origin_id'] ); else if( isset( $arrValues['ar_origin_id'] ) ) $this->setArOriginId( $arrValues['ar_origin_id'] );
        if( isset( $arrValues['ar_code_type_id'] ) && $boolDirectSet ) $this->m_intArCodeTypeId = trim( $arrValues['ar_code_type_id'] ); else if( isset( $arrValues['ar_code_type_id'] ) ) $this->setArCodeTypeId( $arrValues['ar_code_type_id'] );
        if( isset( $arrValues['ar_trigger_id'] ) && $boolDirectSet ) $this->m_intArTriggerId = trim( $arrValues['ar_trigger_id'] ); else if( isset( $arrValues['ar_trigger_id'] ) ) $this->setArTriggerId( $arrValues['ar_trigger_id'] );
        if( isset( $arrValues['ar_action_id'] ) && $boolDirectSet ) $this->m_intArActionId = trim( $arrValues['ar_action_id'] ); else if( isset( $arrValues['ar_action_id'] ) ) $this->setArActionId( $arrValues['ar_action_id'] );
        if( isset( $arrValues['store_lease_terms'] ) && $boolDirectSet ) $this->m_intStoreLeaseTerms = trim( $arrValues['store_lease_terms'] ); else if( isset( $arrValues['store_lease_terms'] ) ) $this->setStoreLeaseTerms( $arrValues['store_lease_terms'] );
        if( isset( $arrValues['store_lease_start_windows'] ) && $boolDirectSet ) $this->m_intStoreLeaseStartWindows = trim( $arrValues['store_lease_start_windows'] ); else if( isset( $arrValues['store_lease_start_windows'] ) ) $this->setStoreLeaseStartWindows( $arrValues['store_lease_start_windows'] );
        if( isset( $arrValues['store_unit_options'] ) && $boolDirectSet ) $this->m_intStoreUnitOptions = trim( $arrValues['store_unit_options'] ); else if( isset( $arrValues['store_unit_options'] ) ) $this->setStoreUnitOptions( $arrValues['store_unit_options'] );
        if( isset( $arrValues['use_non_standard_frequencies'] ) && $boolDirectSet ) $this->m_intUseNonStandardFrequencies = trim( $arrValues['use_non_standard_frequencies'] ); else if( isset( $arrValues['use_non_standard_frequencies'] ) ) $this->setUseNonStandardFrequencies( $arrValues['use_non_standard_frequencies'] );
        if( isset( $arrValues['use_effective_dates'] ) && $boolDirectSet ) $this->m_intUseEffectiveDates = trim( $arrValues['use_effective_dates'] ); else if( isset( $arrValues['use_effective_dates'] ) ) $this->setUseEffectiveDates( $arrValues['use_effective_dates'] );
        if( isset( $arrValues['use_rate_offsets'] ) && $boolDirectSet ) $this->m_intUseRateOffsets = trim( $arrValues['use_rate_offsets'] ); else if( isset( $arrValues['use_rate_offsets'] ) ) $this->setUseRateOffsets( $arrValues['use_rate_offsets'] );
        if( isset( $arrValues['use_ar_tiers'] ) && $boolDirectSet ) $this->m_intUseArTiers = trim( $arrValues['use_ar_tiers'] ); else if( isset( $arrValues['use_ar_tiers'] ) ) $this->setUseArTiers( $arrValues['use_ar_tiers'] );
        if( isset( $arrValues['use_ar_multipliers'] ) && $boolDirectSet ) $this->m_intUseArMultipliers = trim( $arrValues['use_ar_multipliers'] ); else if( isset( $arrValues['use_ar_multipliers'] ) ) $this->setUseArMultipliers( $arrValues['use_ar_multipliers'] );
        if( isset( $arrValues['use_percentages'] ) && $boolDirectSet ) $this->m_intUsePercentages = trim( $arrValues['use_percentages'] ); else if( isset( $arrValues['use_percentages'] ) ) $this->setUsePercentages( $arrValues['use_percentages'] );
        if( isset( $arrValues['use_unit_options'] ) && $boolDirectSet ) $this->m_intUseUnitOptions = trim( $arrValues['use_unit_options'] ); else if( isset( $arrValues['use_unit_options'] ) ) $this->setUseUnitOptions( $arrValues['use_unit_options'] );
        if( isset( $arrValues['use_min_maxes'] ) && $boolDirectSet ) $this->m_intUseMinMaxes = trim( $arrValues['use_min_maxes'] ); else if( isset( $arrValues['use_min_maxes'] ) ) $this->setUseMinMaxes( $arrValues['use_min_maxes'] );
        if( isset( $arrValues['require_customer_relationship'] ) && $boolDirectSet ) $this->m_intRequireCustomerRelationship = trim( $arrValues['require_customer_relationship'] ); else if( isset( $arrValues['require_customer_relationship'] ) ) $this->setRequireCustomerRelationship( $arrValues['require_customer_relationship'] );
        if( isset( $arrValues['separate_prospect_vs_renewal'] ) && $boolDirectSet ) $this->m_intSeparateProspectVsRenewal = trim( $arrValues['separate_prospect_vs_renewal'] ); else if( isset( $arrValues['separate_prospect_vs_renewal'] ) ) $this->setSeparateProspectVsRenewal( $arrValues['separate_prospect_vs_renewal'] );
        if( isset( $arrValues['allow_deactivation_date'] ) && $boolDirectSet ) $this->m_intAllowDeactivationDate = trim( $arrValues['allow_deactivation_date'] ); else if( isset( $arrValues['allow_deactivation_date'] ) ) $this->setAllowDeactivationDate( $arrValues['allow_deactivation_date'] );
        if( isset( $arrValues['require_trigger_type_codes'] ) && $boolDirectSet ) $this->m_intRequireTriggerTypeCodes = trim( $arrValues['require_trigger_type_codes'] ); else if( isset( $arrValues['require_trigger_type_codes'] ) ) $this->setRequireTriggerTypeCodes( $arrValues['require_trigger_type_codes'] );
        if( isset( $arrValues['require_trigger_codes'] ) && $boolDirectSet ) $this->m_intRequireTriggerCodes = trim( $arrValues['require_trigger_codes'] ); else if( isset( $arrValues['require_trigger_codes'] ) ) $this->setRequireTriggerCodes( $arrValues['require_trigger_codes'] );
        if( isset( $arrValues['is_enabled'] ) && $boolDirectSet ) $this->m_intIsEnabled = trim( $arrValues['is_enabled'] ); else if( isset( $arrValues['is_enabled'] ) ) $this->setIsEnabled( $arrValues['is_enabled'] );
        if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->m_intOrderNum = trim( $arrValues['order_num'] ); else if( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setArOriginId( $intArOriginId ) {
        $this->m_intArOriginId = CStrings::strToIntDef( $intArOriginId, NULL, false );
    }

    public function getArOriginId() {
        return $this->m_intArOriginId;
    }

    public function sqlArOriginId() {
        return ( true == isset( $this->m_intArOriginId ) ) ? (string) $this->m_intArOriginId : 'NULL';
    }

    public function setArCodeTypeId( $intArCodeTypeId ) {
        $this->m_intArCodeTypeId = CStrings::strToIntDef( $intArCodeTypeId, NULL, false );
    }

    public function getArCodeTypeId() {
        return $this->m_intArCodeTypeId;
    }

    public function sqlArCodeTypeId() {
        return ( true == isset( $this->m_intArCodeTypeId ) ) ? (string) $this->m_intArCodeTypeId : 'NULL';
    }

    public function setArTriggerId( $intArTriggerId ) {
        $this->m_intArTriggerId = CStrings::strToIntDef( $intArTriggerId, NULL, false );
    }

    public function getArTriggerId() {
        return $this->m_intArTriggerId;
    }

    public function sqlArTriggerId() {
        return ( true == isset( $this->m_intArTriggerId ) ) ? (string) $this->m_intArTriggerId : 'NULL';
    }

    public function setArActionId( $intArActionId ) {
        $this->m_intArActionId = CStrings::strToIntDef( $intArActionId, NULL, false );
    }

    public function getArActionId() {
        return $this->m_intArActionId;
    }

    public function sqlArActionId() {
        return ( true == isset( $this->m_intArActionId ) ) ? (string) $this->m_intArActionId : 'NULL';
    }

    public function setStoreLeaseTerms( $intStoreLeaseTerms ) {
        $this->m_intStoreLeaseTerms = CStrings::strToIntDef( $intStoreLeaseTerms, NULL, false );
    }

    public function getStoreLeaseTerms() {
        return $this->m_intStoreLeaseTerms;
    }

    public function sqlStoreLeaseTerms() {
        return ( true == isset( $this->m_intStoreLeaseTerms ) ) ? (string) $this->m_intStoreLeaseTerms : '0';
    }

    public function setStoreLeaseStartWindows( $intStoreLeaseStartWindows ) {
        $this->m_intStoreLeaseStartWindows = CStrings::strToIntDef( $intStoreLeaseStartWindows, NULL, false );
    }

    public function getStoreLeaseStartWindows() {
        return $this->m_intStoreLeaseStartWindows;
    }

    public function sqlStoreLeaseStartWindows() {
        return ( true == isset( $this->m_intStoreLeaseStartWindows ) ) ? (string) $this->m_intStoreLeaseStartWindows : '0';
    }

    public function setStoreUnitOptions( $intStoreUnitOptions ) {
        $this->m_intStoreUnitOptions = CStrings::strToIntDef( $intStoreUnitOptions, NULL, false );
    }

    public function getStoreUnitOptions() {
        return $this->m_intStoreUnitOptions;
    }

    public function sqlStoreUnitOptions() {
        return ( true == isset( $this->m_intStoreUnitOptions ) ) ? (string) $this->m_intStoreUnitOptions : '0';
    }

    public function setUseNonStandardFrequencies( $intUseNonStandardFrequencies ) {
        $this->m_intUseNonStandardFrequencies = CStrings::strToIntDef( $intUseNonStandardFrequencies, NULL, false );
    }

    public function getUseNonStandardFrequencies() {
        return $this->m_intUseNonStandardFrequencies;
    }

    public function sqlUseNonStandardFrequencies() {
        return ( true == isset( $this->m_intUseNonStandardFrequencies ) ) ? (string) $this->m_intUseNonStandardFrequencies : '0';
    }

    public function setUseEffectiveDates( $intUseEffectiveDates ) {
        $this->m_intUseEffectiveDates = CStrings::strToIntDef( $intUseEffectiveDates, NULL, false );
    }

    public function getUseEffectiveDates() {
        return $this->m_intUseEffectiveDates;
    }

    public function sqlUseEffectiveDates() {
        return ( true == isset( $this->m_intUseEffectiveDates ) ) ? (string) $this->m_intUseEffectiveDates : '0';
    }

    public function setUseRateOffsets( $intUseRateOffsets ) {
        $this->m_intUseRateOffsets = CStrings::strToIntDef( $intUseRateOffsets, NULL, false );
    }

    public function getUseRateOffsets() {
        return $this->m_intUseRateOffsets;
    }

    public function sqlUseRateOffsets() {
        return ( true == isset( $this->m_intUseRateOffsets ) ) ? (string) $this->m_intUseRateOffsets : '0';
    }

    public function setUseArTiers( $intUseArTiers ) {
        $this->m_intUseArTiers = CStrings::strToIntDef( $intUseArTiers, NULL, false );
    }

    public function getUseArTiers() {
        return $this->m_intUseArTiers;
    }

    public function sqlUseArTiers() {
        return ( true == isset( $this->m_intUseArTiers ) ) ? (string) $this->m_intUseArTiers : '0';
    }

    public function setUseArMultipliers( $intUseArMultipliers ) {
        $this->m_intUseArMultipliers = CStrings::strToIntDef( $intUseArMultipliers, NULL, false );
    }

    public function getUseArMultipliers() {
        return $this->m_intUseArMultipliers;
    }

    public function sqlUseArMultipliers() {
        return ( true == isset( $this->m_intUseArMultipliers ) ) ? (string) $this->m_intUseArMultipliers : '0';
    }

    public function setUsePercentages( $intUsePercentages ) {
        $this->m_intUsePercentages = CStrings::strToIntDef( $intUsePercentages, NULL, false );
    }

    public function getUsePercentages() {
        return $this->m_intUsePercentages;
    }

    public function sqlUsePercentages() {
        return ( true == isset( $this->m_intUsePercentages ) ) ? (string) $this->m_intUsePercentages : '0';
    }

    public function setUseUnitOptions( $intUseUnitOptions ) {
        $this->m_intUseUnitOptions = CStrings::strToIntDef( $intUseUnitOptions, NULL, false );
    }

    public function getUseUnitOptions() {
        return $this->m_intUseUnitOptions;
    }

    public function sqlUseUnitOptions() {
        return ( true == isset( $this->m_intUseUnitOptions ) ) ? (string) $this->m_intUseUnitOptions : '0';
    }

    public function setUseMinMaxes( $intUseMinMaxes ) {
        $this->m_intUseMinMaxes = CStrings::strToIntDef( $intUseMinMaxes, NULL, false );
    }

    public function getUseMinMaxes() {
        return $this->m_intUseMinMaxes;
    }

    public function sqlUseMinMaxes() {
        return ( true == isset( $this->m_intUseMinMaxes ) ) ? (string) $this->m_intUseMinMaxes : '0';
    }

    public function setRequireCustomerRelationship( $intRequireCustomerRelationship ) {
        $this->m_intRequireCustomerRelationship = CStrings::strToIntDef( $intRequireCustomerRelationship, NULL, false );
    }

    public function getRequireCustomerRelationship() {
        return $this->m_intRequireCustomerRelationship;
    }

    public function sqlRequireCustomerRelationship() {
        return ( true == isset( $this->m_intRequireCustomerRelationship ) ) ? (string) $this->m_intRequireCustomerRelationship : '0';
    }

    public function setSeparateProspectVsRenewal( $intSeparateProspectVsRenewal ) {
        $this->m_intSeparateProspectVsRenewal = CStrings::strToIntDef( $intSeparateProspectVsRenewal, NULL, false );
    }

    public function getSeparateProspectVsRenewal() {
        return $this->m_intSeparateProspectVsRenewal;
    }

    public function sqlSeparateProspectVsRenewal() {
        return ( true == isset( $this->m_intSeparateProspectVsRenewal ) ) ? (string) $this->m_intSeparateProspectVsRenewal : '0';
    }

    public function setAllowDeactivationDate( $intAllowDeactivationDate ) {
        $this->m_intAllowDeactivationDate = CStrings::strToIntDef( $intAllowDeactivationDate, NULL, false );
    }

    public function getAllowDeactivationDate() {
        return $this->m_intAllowDeactivationDate;
    }

    public function sqlAllowDeactivationDate() {
        return ( true == isset( $this->m_intAllowDeactivationDate ) ) ? (string) $this->m_intAllowDeactivationDate : '0';
    }

    public function setRequireTriggerTypeCodes( $intRequireTriggerTypeCodes ) {
        $this->m_intRequireTriggerTypeCodes = CStrings::strToIntDef( $intRequireTriggerTypeCodes, NULL, false );
    }

    public function getRequireTriggerTypeCodes() {
        return $this->m_intRequireTriggerTypeCodes;
    }

    public function sqlRequireTriggerTypeCodes() {
        return ( true == isset( $this->m_intRequireTriggerTypeCodes ) ) ? (string) $this->m_intRequireTriggerTypeCodes : '0';
    }

    public function setRequireTriggerCodes( $intRequireTriggerCodes ) {
        $this->m_intRequireTriggerCodes = CStrings::strToIntDef( $intRequireTriggerCodes, NULL, false );
    }

    public function getRequireTriggerCodes() {
        return $this->m_intRequireTriggerCodes;
    }

    public function sqlRequireTriggerCodes() {
        return ( true == isset( $this->m_intRequireTriggerCodes ) ) ? (string) $this->m_intRequireTriggerCodes : '0';
    }

    public function setIsEnabled( $intIsEnabled ) {
        $this->m_intIsEnabled = CStrings::strToIntDef( $intIsEnabled, NULL, false );
    }

    public function getIsEnabled() {
        return $this->m_intIsEnabled;
    }

    public function sqlIsEnabled() {
        return ( true == isset( $this->m_intIsEnabled ) ) ? (string) $this->m_intIsEnabled : '0';
    }

    public function setOrderNum( $intOrderNum ) {
        $this->m_intOrderNum = CStrings::strToIntDef( $intOrderNum, NULL, false );
    }

    public function getOrderNum() {
        return $this->m_intOrderNum;
    }

    public function sqlOrderNum() {
        return ( true == isset( $this->m_intOrderNum ) ) ? (string) $this->m_intOrderNum : '0';
    }

}
?>