<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBasePropertyHour extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.property_hours';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPropertyHourTypeId;
	protected $m_intPropertyHolidayId;
	protected $m_strRemotePrimaryKey;
	protected $m_strOfficeHours;
	protected $m_strOpenTime;
	protected $m_strCloseTime;
	protected $m_intDay;
	protected $m_boolByAppointmentOnly;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_strImportedOn;
	protected $m_boolIsOldFormat;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_boolByAppointmentOnly = false;
		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';
		$this->m_boolIsOldFormat = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_hour_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyHourTypeId', trim( $arrValues['property_hour_type_id'] ) ); elseif( isset( $arrValues['property_hour_type_id'] ) ) $this->setPropertyHourTypeId( $arrValues['property_hour_type_id'] );
		if( isset( $arrValues['property_holiday_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyHolidayId', trim( $arrValues['property_holiday_id'] ) ); elseif( isset( $arrValues['property_holiday_id'] ) ) $this->setPropertyHolidayId( $arrValues['property_holiday_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['office_hours'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strOfficeHours', trim( stripcslashes( $arrValues['office_hours'] ) ) ); elseif( isset( $arrValues['office_hours'] ) ) $this->setOfficeHours( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['office_hours'] ) : $arrValues['office_hours'] );
		if( isset( $arrValues['open_time'] ) && $boolDirectSet ) $this->set( 'm_strOpenTime', trim( $arrValues['open_time'] ) ); elseif( isset( $arrValues['open_time'] ) ) $this->setOpenTime( $arrValues['open_time'] );
		if( isset( $arrValues['close_time'] ) && $boolDirectSet ) $this->set( 'm_strCloseTime', trim( $arrValues['close_time'] ) ); elseif( isset( $arrValues['close_time'] ) ) $this->setCloseTime( $arrValues['close_time'] );
		if( isset( $arrValues['day'] ) && $boolDirectSet ) $this->set( 'm_intDay', trim( $arrValues['day'] ) ); elseif( isset( $arrValues['day'] ) ) $this->setDay( $arrValues['day'] );
		if( isset( $arrValues['by_appointment_only'] ) && $boolDirectSet ) $this->set( 'm_boolByAppointmentOnly', trim( stripcslashes( $arrValues['by_appointment_only'] ) ) ); elseif( isset( $arrValues['by_appointment_only'] ) ) $this->setByAppointmentOnly( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['by_appointment_only'] ) : $arrValues['by_appointment_only'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['imported_on'] ) && $boolDirectSet ) $this->set( 'm_strImportedOn', trim( $arrValues['imported_on'] ) ); elseif( isset( $arrValues['imported_on'] ) ) $this->setImportedOn( $arrValues['imported_on'] );
		if( isset( $arrValues['is_old_format'] ) && $boolDirectSet ) $this->set( 'm_boolIsOldFormat', trim( stripcslashes( $arrValues['is_old_format'] ) ) ); elseif( isset( $arrValues['is_old_format'] ) ) $this->setIsOldFormat( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_old_format'] ) : $arrValues['is_old_format'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyHourTypeId( $intPropertyHourTypeId ) {
		$this->set( 'm_intPropertyHourTypeId', CStrings::strToIntDef( $intPropertyHourTypeId, NULL, false ) );
	}

	public function getPropertyHourTypeId() {
		return $this->m_intPropertyHourTypeId;
	}

	public function sqlPropertyHourTypeId() {
		return ( true == isset( $this->m_intPropertyHourTypeId ) ) ? ( string ) $this->m_intPropertyHourTypeId : 'NULL';
	}

	public function setPropertyHolidayId( $intPropertyHolidayId ) {
		$this->set( 'm_intPropertyHolidayId', CStrings::strToIntDef( $intPropertyHolidayId, NULL, false ) );
	}

	public function getPropertyHolidayId() {
		return $this->m_intPropertyHolidayId;
	}

	public function sqlPropertyHolidayId() {
		return ( true == isset( $this->m_intPropertyHolidayId ) ) ? ( string ) $this->m_intPropertyHolidayId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setOfficeHours( $strOfficeHours, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strOfficeHours', CStrings::strTrimDef( $strOfficeHours, 240, NULL, true ), $strLocaleCode );
	}

	public function getOfficeHours( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strOfficeHours', $strLocaleCode );
	}

	public function sqlOfficeHours() {
		return ( true == isset( $this->m_strOfficeHours ) ) ? '\'' . addslashes( $this->m_strOfficeHours ) . '\'' : 'NULL';
	}

	public function setOpenTime( $strOpenTime ) {
		$this->set( 'm_strOpenTime', CStrings::strTrimDef( $strOpenTime, NULL, NULL, true ) );
	}

	public function getOpenTime() {
		return $this->m_strOpenTime;
	}

	public function sqlOpenTime() {
		return ( true == isset( $this->m_strOpenTime ) ) ? '\'' . $this->m_strOpenTime . '\'' : 'NULL';
	}

	public function setCloseTime( $strCloseTime ) {
		$this->set( 'm_strCloseTime', CStrings::strTrimDef( $strCloseTime, NULL, NULL, true ) );
	}

	public function getCloseTime() {
		return $this->m_strCloseTime;
	}

	public function sqlCloseTime() {
		return ( true == isset( $this->m_strCloseTime ) ) ? '\'' . $this->m_strCloseTime . '\'' : 'NULL';
	}

	public function setDay( $intDay ) {
		$this->set( 'm_intDay', CStrings::strToIntDef( $intDay, NULL, false ) );
	}

	public function getDay() {
		return $this->m_intDay;
	}

	public function sqlDay() {
		return ( true == isset( $this->m_intDay ) ) ? ( string ) $this->m_intDay : 'NULL';
	}

	public function setByAppointmentOnly( $boolByAppointmentOnly ) {
		$this->set( 'm_boolByAppointmentOnly', CStrings::strToBool( $boolByAppointmentOnly ) );
	}

	public function getByAppointmentOnly() {
		return $this->m_boolByAppointmentOnly;
	}

	public function sqlByAppointmentOnly() {
		return ( true == isset( $this->m_boolByAppointmentOnly ) ) ? '\'' . ( true == ( bool ) $this->m_boolByAppointmentOnly ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setImportedOn( $strImportedOn ) {
		$this->set( 'm_strImportedOn', CStrings::strTrimDef( $strImportedOn, -1, NULL, true ) );
	}

	public function getImportedOn() {
		return $this->m_strImportedOn;
	}

	public function sqlImportedOn() {
		return ( true == isset( $this->m_strImportedOn ) ) ? '\'' . $this->m_strImportedOn . '\'' : 'NULL';
	}

	public function setIsOldFormat( $boolIsOldFormat ) {
		$this->set( 'm_boolIsOldFormat', CStrings::strToBool( $boolIsOldFormat ) );
	}

	public function getIsOldFormat() {
		return $this->m_boolIsOldFormat;
	}

	public function sqlIsOldFormat() {
		return ( true == isset( $this->m_boolIsOldFormat ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOldFormat ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_hour_type_id, property_holiday_id, remote_primary_key, office_hours, open_time, close_time, day, by_appointment_only, is_published, order_num, imported_on, is_old_format, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPropertyHourTypeId() . ', ' .
						$this->sqlPropertyHolidayId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlOfficeHours() . ', ' .
						$this->sqlOpenTime() . ', ' .
						$this->sqlCloseTime() . ', ' .
						$this->sqlDay() . ', ' .
						$this->sqlByAppointmentOnly() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlImportedOn() . ', ' .
						$this->sqlIsOldFormat() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_hour_type_id = ' . $this->sqlPropertyHourTypeId(). ',' ; } elseif( true == array_key_exists( 'PropertyHourTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_hour_type_id = ' . $this->sqlPropertyHourTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_holiday_id = ' . $this->sqlPropertyHolidayId(). ',' ; } elseif( true == array_key_exists( 'PropertyHolidayId', $this->getChangedColumns() ) ) { $strSql .= ' property_holiday_id = ' . $this->sqlPropertyHolidayId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' office_hours = ' . $this->sqlOfficeHours(). ',' ; } elseif( true == array_key_exists( 'OfficeHours', $this->getChangedColumns() ) ) { $strSql .= ' office_hours = ' . $this->sqlOfficeHours() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' open_time = ' . $this->sqlOpenTime(). ',' ; } elseif( true == array_key_exists( 'OpenTime', $this->getChangedColumns() ) ) { $strSql .= ' open_time = ' . $this->sqlOpenTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' close_time = ' . $this->sqlCloseTime(). ',' ; } elseif( true == array_key_exists( 'CloseTime', $this->getChangedColumns() ) ) { $strSql .= ' close_time = ' . $this->sqlCloseTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' day = ' . $this->sqlDay(). ',' ; } elseif( true == array_key_exists( 'Day', $this->getChangedColumns() ) ) { $strSql .= ' day = ' . $this->sqlDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' by_appointment_only = ' . $this->sqlByAppointmentOnly(). ',' ; } elseif( true == array_key_exists( 'ByAppointmentOnly', $this->getChangedColumns() ) ) { $strSql .= ' by_appointment_only = ' . $this->sqlByAppointmentOnly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn(). ',' ; } elseif( true == array_key_exists( 'ImportedOn', $this->getChangedColumns() ) ) { $strSql .= ' imported_on = ' . $this->sqlImportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_old_format = ' . $this->sqlIsOldFormat(). ',' ; } elseif( true == array_key_exists( 'IsOldFormat', $this->getChangedColumns() ) ) { $strSql .= ' is_old_format = ' . $this->sqlIsOldFormat() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_hour_type_id' => $this->getPropertyHourTypeId(),
			'property_holiday_id' => $this->getPropertyHolidayId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'office_hours' => $this->getOfficeHours(),
			'open_time' => $this->getOpenTime(),
			'close_time' => $this->getCloseTime(),
			'day' => $this->getDay(),
			'by_appointment_only' => $this->getByAppointmentOnly(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'imported_on' => $this->getImportedOn(),
			'is_old_format' => $this->getIsOldFormat(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>