<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportAdhocTypes
 * Do not add any new functions to this class.
 */

class CBaseReportAdhocTypes extends CEosPluralBase {

	/**
	 * @return CReportAdhocType[]
	 */
	public static function fetchReportAdhocTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportAdhocType::class, $objDatabase );
	}

	/**
	 * @return CReportAdhocType
	 */
	public static function fetchReportAdhocType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportAdhocType::class, $objDatabase );
	}

	public static function fetchReportAdhocTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_adhoc_types', $objDatabase );
	}

	public static function fetchReportAdhocTypeById( $intId, $objDatabase ) {
		return self::fetchReportAdhocType( sprintf( 'SELECT * FROM report_adhoc_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>