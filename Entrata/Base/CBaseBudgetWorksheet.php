<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseBudgetWorksheet extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.budget_worksheets';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intBudgetWorkbookId;
	protected $m_intBudgetWorksheetTypeId;
	protected $m_intBudgetDataSourceTypeId;
	protected $m_intBudgetDataSourceSubTypeId;
	protected $m_strName;
	protected $m_fltAdjustmentPercent;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_boolAllowItems;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsConfidential;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltAdjustmentPercent = '0';
		$this->m_boolAllowItems = false;
		$this->m_intOrderNum = '0';
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';
		$this->m_boolIsConfidential = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['budget_workbook_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetWorkbookId', trim( $arrValues['budget_workbook_id'] ) ); elseif( isset( $arrValues['budget_workbook_id'] ) ) $this->setBudgetWorkbookId( $arrValues['budget_workbook_id'] );
		if( isset( $arrValues['budget_worksheet_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetWorksheetTypeId', trim( $arrValues['budget_worksheet_type_id'] ) ); elseif( isset( $arrValues['budget_worksheet_type_id'] ) ) $this->setBudgetWorksheetTypeId( $arrValues['budget_worksheet_type_id'] );
		if( isset( $arrValues['budget_data_source_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetDataSourceTypeId', trim( $arrValues['budget_data_source_type_id'] ) ); elseif( isset( $arrValues['budget_data_source_type_id'] ) ) $this->setBudgetDataSourceTypeId( $arrValues['budget_data_source_type_id'] );
		if( isset( $arrValues['budget_data_source_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBudgetDataSourceSubTypeId', trim( $arrValues['budget_data_source_sub_type_id'] ) ); elseif( isset( $arrValues['budget_data_source_sub_type_id'] ) ) $this->setBudgetDataSourceSubTypeId( $arrValues['budget_data_source_sub_type_id'] );
		if( isset( $arrValues['name'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['adjustment_percent'] ) && $boolDirectSet ) $this->set( 'm_fltAdjustmentPercent', trim( $arrValues['adjustment_percent'] ) ); elseif( isset( $arrValues['adjustment_percent'] ) ) $this->setAdjustmentPercent( $arrValues['adjustment_percent'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['allow_items'] ) && $boolDirectSet ) $this->set( 'm_boolAllowItems', trim( stripcslashes( $arrValues['allow_items'] ) ) ); elseif( isset( $arrValues['allow_items'] ) ) $this->setAllowItems( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_items'] ) : $arrValues['allow_items'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_confidential'] ) && $boolDirectSet ) $this->set( 'm_boolIsConfidential', trim( stripcslashes( $arrValues['is_confidential'] ) ) ); elseif( isset( $arrValues['is_confidential'] ) ) $this->setIsConfidential( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_confidential'] ) : $arrValues['is_confidential'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setBudgetWorkbookId( $intBudgetWorkbookId ) {
		$this->set( 'm_intBudgetWorkbookId', CStrings::strToIntDef( $intBudgetWorkbookId, NULL, false ) );
	}

	public function getBudgetWorkbookId() {
		return $this->m_intBudgetWorkbookId;
	}

	public function sqlBudgetWorkbookId() {
		return ( true == isset( $this->m_intBudgetWorkbookId ) ) ? ( string ) $this->m_intBudgetWorkbookId : 'NULL';
	}

	public function setBudgetWorksheetTypeId( $intBudgetWorksheetTypeId ) {
		$this->set( 'm_intBudgetWorksheetTypeId', CStrings::strToIntDef( $intBudgetWorksheetTypeId, NULL, false ) );
	}

	public function getBudgetWorksheetTypeId() {
		return $this->m_intBudgetWorksheetTypeId;
	}

	public function sqlBudgetWorksheetTypeId() {
		return ( true == isset( $this->m_intBudgetWorksheetTypeId ) ) ? ( string ) $this->m_intBudgetWorksheetTypeId : 'NULL';
	}

	public function setBudgetDataSourceTypeId( $intBudgetDataSourceTypeId ) {
		$this->set( 'm_intBudgetDataSourceTypeId', CStrings::strToIntDef( $intBudgetDataSourceTypeId, NULL, false ) );
	}

	public function getBudgetDataSourceTypeId() {
		return $this->m_intBudgetDataSourceTypeId;
	}

	public function sqlBudgetDataSourceTypeId() {
		return ( true == isset( $this->m_intBudgetDataSourceTypeId ) ) ? ( string ) $this->m_intBudgetDataSourceTypeId : 'NULL';
	}

	public function setBudgetDataSourceSubTypeId( $intBudgetDataSourceSubTypeId ) {
		$this->set( 'm_intBudgetDataSourceSubTypeId', CStrings::strToIntDef( $intBudgetDataSourceSubTypeId, NULL, false ) );
	}

	public function getBudgetDataSourceSubTypeId() {
		return $this->m_intBudgetDataSourceSubTypeId;
	}

	public function sqlBudgetDataSourceSubTypeId() {
		return ( true == isset( $this->m_intBudgetDataSourceSubTypeId ) ) ? ( string ) $this->m_intBudgetDataSourceSubTypeId : 'NULL';
	}

	public function setName( $strName, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strName', CStrings::strTrimDef( $strName, 100, NULL, true ), $strLocaleCode );
	}

	public function getName( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setAdjustmentPercent( $fltAdjustmentPercent ) {
		$this->set( 'm_fltAdjustmentPercent', CStrings::strToFloatDef( $fltAdjustmentPercent, NULL, false, 4 ) );
	}

	public function getAdjustmentPercent() {
		return $this->m_fltAdjustmentPercent;
	}

	public function sqlAdjustmentPercent() {
		return ( true == isset( $this->m_fltAdjustmentPercent ) ) ? ( string ) $this->m_fltAdjustmentPercent : '0';
	}

	public function setAllowItems( $boolAllowItems ) {
		$this->set( 'm_boolAllowItems', CStrings::strToBool( $boolAllowItems ) );
	}

	public function getAllowItems() {
		return $this->m_boolAllowItems;
	}

	public function sqlAllowItems() {
		return ( true == isset( $this->m_boolAllowItems ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowItems ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsConfidential( $boolIsConfidential ) {
		$this->set( 'm_boolIsConfidential', CStrings::strToBool( $boolIsConfidential ) );
	}

	public function getIsConfidential() {
		return $this->m_boolIsConfidential;
	}

	public function sqlIsConfidential() {
		return ( true == isset( $this->m_boolIsConfidential ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsConfidential ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, budget_workbook_id, budget_worksheet_type_id, budget_data_source_type_id, budget_data_source_sub_type_id, name, adjustment_percent, details, allow_items, order_num, updated_by, updated_on, created_by, created_on, is_confidential, deleted_by, deleted_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlBudgetWorkbookId() . ', ' .
						$this->sqlBudgetWorksheetTypeId() . ', ' .
						$this->sqlBudgetDataSourceTypeId() . ', ' .
						$this->sqlBudgetDataSourceSubTypeId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlAdjustmentPercent() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlAllowItems() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsConfidential() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_workbook_id = ' . $this->sqlBudgetWorkbookId(). ',' ; } elseif( true == array_key_exists( 'BudgetWorkbookId', $this->getChangedColumns() ) ) { $strSql .= ' budget_workbook_id = ' . $this->sqlBudgetWorkbookId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_worksheet_type_id = ' . $this->sqlBudgetWorksheetTypeId(). ',' ; } elseif( true == array_key_exists( 'BudgetWorksheetTypeId', $this->getChangedColumns() ) ) { $strSql .= ' budget_worksheet_type_id = ' . $this->sqlBudgetWorksheetTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_data_source_type_id = ' . $this->sqlBudgetDataSourceTypeId(). ',' ; } elseif( true == array_key_exists( 'BudgetDataSourceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' budget_data_source_type_id = ' . $this->sqlBudgetDataSourceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_data_source_sub_type_id = ' . $this->sqlBudgetDataSourceSubTypeId(). ',' ; } elseif( true == array_key_exists( 'BudgetDataSourceSubTypeId', $this->getChangedColumns() ) ) { $strSql .= ' budget_data_source_sub_type_id = ' . $this->sqlBudgetDataSourceSubTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' adjustment_percent = ' . $this->sqlAdjustmentPercent(). ',' ; } elseif( true == array_key_exists( 'AdjustmentPercent', $this->getChangedColumns() ) ) { $strSql .= ' adjustment_percent = ' . $this->sqlAdjustmentPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_items = ' . $this->sqlAllowItems(). ',' ; } elseif( true == array_key_exists( 'AllowItems', $this->getChangedColumns() ) ) { $strSql .= ' allow_items = ' . $this->sqlAllowItems() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_confidential = ' . $this->sqlIsConfidential(). ',' ; } elseif( true == array_key_exists( 'IsConfidential', $this->getChangedColumns() ) ) { $strSql .= ' is_confidential = ' . $this->sqlIsConfidential() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'budget_workbook_id' => $this->getBudgetWorkbookId(),
			'budget_worksheet_type_id' => $this->getBudgetWorksheetTypeId(),
			'budget_data_source_type_id' => $this->getBudgetDataSourceTypeId(),
			'budget_data_source_sub_type_id' => $this->getBudgetDataSourceSubTypeId(),
			'name' => $this->getName(),
			'adjustment_percent' => $this->getAdjustmentPercent(),
			'details' => $this->getDetails(),
			'allow_items' => $this->getAllowItems(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_confidential' => $this->getIsConfidential(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn()
		);
	}

}
?>