<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerPaymentAccounts
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCustomerPaymentAccounts extends CEosPluralBase {

	/**
	 * @return CCustomerPaymentAccount[]
	 */
	public static function fetchCustomerPaymentAccounts( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CCustomerPaymentAccount::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CCustomerPaymentAccount
	 */
	public static function fetchCustomerPaymentAccount( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCustomerPaymentAccount::class, $objDatabase );
	}

	public static function fetchCustomerPaymentAccountCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'customer_payment_accounts', $objDatabase );
	}

	public static function fetchCustomerPaymentAccountByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomerPaymentAccount( sprintf( 'SELECT * FROM customer_payment_accounts WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerPaymentAccountsByCid( $intCid, $objDatabase ) {
		return self::fetchCustomerPaymentAccounts( sprintf( 'SELECT * FROM customer_payment_accounts WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCustomerPaymentAccountsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerPaymentAccounts( sprintf( 'SELECT * FROM customer_payment_accounts WHERE customer_id = %d AND cid = %d', $intCustomerId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerPaymentAccountsByApplicantIdByCid( $intApplicantId, $intCid, $objDatabase ) {
		return self::fetchCustomerPaymentAccounts( sprintf( 'SELECT * FROM customer_payment_accounts WHERE applicant_id = %d AND cid = %d', $intApplicantId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerPaymentAccountsByPaymentTypeIdByCid( $intPaymentTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerPaymentAccounts( sprintf( 'SELECT * FROM customer_payment_accounts WHERE payment_type_id = %d AND cid = %d', $intPaymentTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerPaymentAccountsByPsProductIdByCid( $intPsProductId, $intCid, $objDatabase ) {
		return self::fetchCustomerPaymentAccounts( sprintf( 'SELECT * FROM customer_payment_accounts WHERE ps_product_id = %d AND cid = %d', $intPsProductId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerPaymentAccountsByPsProductOptionIdByCid( $intPsProductOptionId, $intCid, $objDatabase ) {
		return self::fetchCustomerPaymentAccounts( sprintf( 'SELECT * FROM customer_payment_accounts WHERE ps_product_option_id = %d AND cid = %d', $intPsProductOptionId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerPaymentAccountsByAccountVerificationLogIdByCid( $intAccountVerificationLogId, $intCid, $objDatabase ) {
		return self::fetchCustomerPaymentAccounts( sprintf( 'SELECT * FROM customer_payment_accounts WHERE account_verification_log_id = %d AND cid = %d', $intAccountVerificationLogId, $intCid ), $objDatabase );
	}

	public static function fetchCustomerPaymentAccountsByCheckAccountTypeIdByCid( $intCheckAccountTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerPaymentAccounts( sprintf( 'SELECT * FROM customer_payment_accounts WHERE check_account_type_id = %d AND cid = %d', $intCheckAccountTypeId, $intCid ), $objDatabase );
	}

}
?>