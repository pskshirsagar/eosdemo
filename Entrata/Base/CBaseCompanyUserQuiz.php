<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCompanyUserQuiz extends CEosSingularBase {

	const TABLE_NAME = 'public.company_user_quizzes';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDefaultCid;
	protected $m_intCompanyUserAssessmentId;
	protected $m_intHelpQuizId;
	protected $m_intScore;
	protected $m_intNumQuizAttempts;
	protected $m_strCompletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intNumQuizAttempts = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['default_cid'] ) && $boolDirectSet ) $this->set( 'm_intDefaultCid', trim( $arrValues['default_cid'] ) ); elseif( isset( $arrValues['default_cid'] ) ) $this->setDefaultCid( $arrValues['default_cid'] );
		if( isset( $arrValues['company_user_assessment_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserAssessmentId', trim( $arrValues['company_user_assessment_id'] ) ); elseif( isset( $arrValues['company_user_assessment_id'] ) ) $this->setCompanyUserAssessmentId( $arrValues['company_user_assessment_id'] );
		if( isset( $arrValues['help_quiz_id'] ) && $boolDirectSet ) $this->set( 'm_intHelpQuizId', trim( $arrValues['help_quiz_id'] ) ); elseif( isset( $arrValues['help_quiz_id'] ) ) $this->setHelpQuizId( $arrValues['help_quiz_id'] );
		if( isset( $arrValues['score'] ) && $boolDirectSet ) $this->set( 'm_intScore', trim( $arrValues['score'] ) ); elseif( isset( $arrValues['score'] ) ) $this->setScore( $arrValues['score'] );
		if( isset( $arrValues['num_quiz_attempts'] ) && $boolDirectSet ) $this->set( 'm_intNumQuizAttempts', trim( $arrValues['num_quiz_attempts'] ) ); elseif( isset( $arrValues['num_quiz_attempts'] ) ) $this->setNumQuizAttempts( $arrValues['num_quiz_attempts'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDefaultCid( $intDefaultCid ) {
		$this->set( 'm_intDefaultCid', CStrings::strToIntDef( $intDefaultCid, NULL, false ) );
	}

	public function getDefaultCid() {
		return $this->m_intDefaultCid;
	}

	public function sqlDefaultCid() {
		return ( true == isset( $this->m_intDefaultCid ) ) ? ( string ) $this->m_intDefaultCid : 'NULL';
	}

	public function setCompanyUserAssessmentId( $intCompanyUserAssessmentId ) {
		$this->set( 'm_intCompanyUserAssessmentId', CStrings::strToIntDef( $intCompanyUserAssessmentId, NULL, false ) );
	}

	public function getCompanyUserAssessmentId() {
		return $this->m_intCompanyUserAssessmentId;
	}

	public function sqlCompanyUserAssessmentId() {
		return ( true == isset( $this->m_intCompanyUserAssessmentId ) ) ? ( string ) $this->m_intCompanyUserAssessmentId : 'NULL';
	}

	public function setHelpQuizId( $intHelpQuizId ) {
		$this->set( 'm_intHelpQuizId', CStrings::strToIntDef( $intHelpQuizId, NULL, false ) );
	}

	public function getHelpQuizId() {
		return $this->m_intHelpQuizId;
	}

	public function sqlHelpQuizId() {
		return ( true == isset( $this->m_intHelpQuizId ) ) ? ( string ) $this->m_intHelpQuizId : 'NULL';
	}

	public function setScore( $intScore ) {
		$this->set( 'm_intScore', CStrings::strToIntDef( $intScore, NULL, false ) );
	}

	public function getScore() {
		return $this->m_intScore;
	}

	public function sqlScore() {
		return ( true == isset( $this->m_intScore ) ) ? ( string ) $this->m_intScore : 'NULL';
	}

	public function setNumQuizAttempts( $intNumQuizAttempts ) {
		$this->set( 'm_intNumQuizAttempts', CStrings::strToIntDef( $intNumQuizAttempts, NULL, false ) );
	}

	public function getNumQuizAttempts() {
		return $this->m_intNumQuizAttempts;
	}

	public function sqlNumQuizAttempts() {
		return ( true == isset( $this->m_intNumQuizAttempts ) ) ? ( string ) $this->m_intNumQuizAttempts : '0';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, default_cid, company_user_assessment_id, help_quiz_id, score, num_quiz_attempts, completed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlDefaultCid() . ', ' .
 						$this->sqlCompanyUserAssessmentId() . ', ' .
 						$this->sqlHelpQuizId() . ', ' .
 						$this->sqlScore() . ', ' .
 						$this->sqlNumQuizAttempts() . ', ' .
 						$this->sqlCompletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_cid = ' . $this->sqlDefaultCid() . ','; } elseif( true == array_key_exists( 'DefaultCid', $this->getChangedColumns() ) ) { $strSql .= ' default_cid = ' . $this->sqlDefaultCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_assessment_id = ' . $this->sqlCompanyUserAssessmentId() . ','; } elseif( true == array_key_exists( 'CompanyUserAssessmentId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_assessment_id = ' . $this->sqlCompanyUserAssessmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' help_quiz_id = ' . $this->sqlHelpQuizId() . ','; } elseif( true == array_key_exists( 'HelpQuizId', $this->getChangedColumns() ) ) { $strSql .= ' help_quiz_id = ' . $this->sqlHelpQuizId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' score = ' . $this->sqlScore() . ','; } elseif( true == array_key_exists( 'Score', $this->getChangedColumns() ) ) { $strSql .= ' score = ' . $this->sqlScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' num_quiz_attempts = ' . $this->sqlNumQuizAttempts() . ','; } elseif( true == array_key_exists( 'NumQuizAttempts', $this->getChangedColumns() ) ) { $strSql .= ' num_quiz_attempts = ' . $this->sqlNumQuizAttempts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'default_cid' => $this->getDefaultCid(),
			'company_user_assessment_id' => $this->getCompanyUserAssessmentId(),
			'help_quiz_id' => $this->getHelpQuizId(),
			'score' => $this->getScore(),
			'num_quiz_attempts' => $this->getNumQuizAttempts(),
			'completed_on' => $this->getCompletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>