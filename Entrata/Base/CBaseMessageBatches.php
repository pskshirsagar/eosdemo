<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMessageBatches
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMessageBatches extends CEosPluralBase {

	/**
	 * @return CMessageBatch[]
	 */
	public static function fetchMessageBatches( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, CMessageBatch::class, $objDatabase, $boolIsReturnKeyedArray );
	}

	/**
	 * @return CMessageBatch
	 */
	public static function fetchMessageBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMessageBatch::class, $objDatabase );
	}

	public static function fetchMessageBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'message_batches', $objDatabase );
	}

	public static function fetchMessageBatchByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMessageBatch( sprintf( 'SELECT * FROM message_batches WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchMessageBatchesByCid( $intCid, $objDatabase ) {
		return self::fetchMessageBatches( sprintf( 'SELECT * FROM message_batches WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMessageBatchesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchMessageBatches( sprintf( 'SELECT * FROM message_batches WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchMessageBatchesByMessageBatchTypeIdByCid( $intMessageBatchTypeId, $intCid, $objDatabase ) {
		return self::fetchMessageBatches( sprintf( 'SELECT * FROM message_batches WHERE message_batch_type_id = %d AND cid = %d', $intMessageBatchTypeId, $intCid ), $objDatabase );
	}

}
?>