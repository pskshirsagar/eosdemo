<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseMaintenanceRequestLog extends CEosSingularBase {

	const TABLE_NAME = 'public.maintenance_request_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intMaintenanceRequestId;
	protected $m_intMaintenanceRequestTypeId;
	protected $m_intPriorMaintenanceRequestLogId;
	protected $m_intPropertyId;
	protected $m_intLeaseId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_intPeriodId;
	protected $m_intReportingPeriodId;
	protected $m_intEffectivePeriodId;
	protected $m_intOriginalPeriodId;
	protected $m_intOldMaintenanceStatusId;
	protected $m_intMaintenanceStatusId;
	protected $m_intOldMaintenanceStatusTypeId;
	protected $m_intMaintenanceStatusTypeId;
	protected $m_intOldMaintenancePriorityId;
	protected $m_intMaintenancePriorityId;
	protected $m_strPostMonth;
	protected $m_strPostDate;
	protected $m_strReportingPostMonth;
	protected $m_strApplyThroughPostMonth;
	protected $m_strReportingPostDate;
	protected $m_strApplyThroughPostDate;
	protected $m_strLogDatetime;
	protected $m_strEffectiveDate;
	protected $m_intNewIsDeleted;
	protected $m_intOldIsDeleted;
	protected $m_intIsPostMonthIgnored;
	protected $m_intIsPostDateIgnored;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUpdatedPsProductId;

	public function __construct() {
		parent::__construct();

		$this->m_strApplyThroughPostMonth = '12/01/2099';
		$this->m_strApplyThroughPostDate = '12/31/2099';
		$this->m_intNewIsDeleted = '0';
		$this->m_intOldIsDeleted = '0';
		$this->m_intIsPostMonthIgnored = '0';
		$this->m_intIsPostDateIgnored = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['maintenance_request_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceRequestId', trim( $arrValues['maintenance_request_id'] ) ); elseif( isset( $arrValues['maintenance_request_id'] ) ) $this->setMaintenanceRequestId( $arrValues['maintenance_request_id'] );
		if( isset( $arrValues['maintenance_request_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceRequestTypeId', trim( $arrValues['maintenance_request_type_id'] ) ); elseif( isset( $arrValues['maintenance_request_type_id'] ) ) $this->setMaintenanceRequestTypeId( $arrValues['maintenance_request_type_id'] );
		if( isset( $arrValues['prior_maintenance_request_log_id'] ) && $boolDirectSet ) $this->set( 'm_intPriorMaintenanceRequestLogId', trim( $arrValues['prior_maintenance_request_log_id'] ) ); elseif( isset( $arrValues['prior_maintenance_request_log_id'] ) ) $this->setPriorMaintenanceRequestLogId( $arrValues['prior_maintenance_request_log_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceId', trim( $arrValues['unit_space_id'] ) ); elseif( isset( $arrValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrValues['unit_space_id'] );
		if( isset( $arrValues['period_id'] ) && $boolDirectSet ) $this->set( 'm_intPeriodId', trim( $arrValues['period_id'] ) ); elseif( isset( $arrValues['period_id'] ) ) $this->setPeriodId( $arrValues['period_id'] );
		if( isset( $arrValues['reporting_period_id'] ) && $boolDirectSet ) $this->set( 'm_intReportingPeriodId', trim( $arrValues['reporting_period_id'] ) ); elseif( isset( $arrValues['reporting_period_id'] ) ) $this->setReportingPeriodId( $arrValues['reporting_period_id'] );
		if( isset( $arrValues['effective_period_id'] ) && $boolDirectSet ) $this->set( 'm_intEffectivePeriodId', trim( $arrValues['effective_period_id'] ) ); elseif( isset( $arrValues['effective_period_id'] ) ) $this->setEffectivePeriodId( $arrValues['effective_period_id'] );
		if( isset( $arrValues['original_period_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginalPeriodId', trim( $arrValues['original_period_id'] ) ); elseif( isset( $arrValues['original_period_id'] ) ) $this->setOriginalPeriodId( $arrValues['original_period_id'] );
		if( isset( $arrValues['old_maintenance_status_id'] ) && $boolDirectSet ) $this->set( 'm_intOldMaintenanceStatusId', trim( $arrValues['old_maintenance_status_id'] ) ); elseif( isset( $arrValues['old_maintenance_status_id'] ) ) $this->setOldMaintenanceStatusId( $arrValues['old_maintenance_status_id'] );
		if( isset( $arrValues['maintenance_status_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceStatusId', trim( $arrValues['maintenance_status_id'] ) ); elseif( isset( $arrValues['maintenance_status_id'] ) ) $this->setMaintenanceStatusId( $arrValues['maintenance_status_id'] );
		if( isset( $arrValues['old_maintenance_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOldMaintenanceStatusTypeId', trim( $arrValues['old_maintenance_status_type_id'] ) ); elseif( isset( $arrValues['old_maintenance_status_type_id'] ) ) $this->setOldMaintenanceStatusTypeId( $arrValues['old_maintenance_status_type_id'] );
		if( isset( $arrValues['maintenance_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceStatusTypeId', trim( $arrValues['maintenance_status_type_id'] ) ); elseif( isset( $arrValues['maintenance_status_type_id'] ) ) $this->setMaintenanceStatusTypeId( $arrValues['maintenance_status_type_id'] );
		if( isset( $arrValues['old_maintenance_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intOldMaintenancePriorityId', trim( $arrValues['old_maintenance_priority_id'] ) ); elseif( isset( $arrValues['old_maintenance_priority_id'] ) ) $this->setOldMaintenancePriorityId( $arrValues['old_maintenance_priority_id'] );
		if( isset( $arrValues['maintenance_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenancePriorityId', trim( $arrValues['maintenance_priority_id'] ) ); elseif( isset( $arrValues['maintenance_priority_id'] ) ) $this->setMaintenancePriorityId( $arrValues['maintenance_priority_id'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['reporting_post_month'] ) && $boolDirectSet ) $this->set( 'm_strReportingPostMonth', trim( $arrValues['reporting_post_month'] ) ); elseif( isset( $arrValues['reporting_post_month'] ) ) $this->setReportingPostMonth( $arrValues['reporting_post_month'] );
		if( isset( $arrValues['apply_through_post_month'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostMonth', trim( $arrValues['apply_through_post_month'] ) ); elseif( isset( $arrValues['apply_through_post_month'] ) ) $this->setApplyThroughPostMonth( $arrValues['apply_through_post_month'] );
		if( isset( $arrValues['reporting_post_date'] ) && $boolDirectSet ) $this->set( 'm_strReportingPostDate', trim( $arrValues['reporting_post_date'] ) ); elseif( isset( $arrValues['reporting_post_date'] ) ) $this->setReportingPostDate( $arrValues['reporting_post_date'] );
		if( isset( $arrValues['apply_through_post_date'] ) && $boolDirectSet ) $this->set( 'm_strApplyThroughPostDate', trim( $arrValues['apply_through_post_date'] ) ); elseif( isset( $arrValues['apply_through_post_date'] ) ) $this->setApplyThroughPostDate( $arrValues['apply_through_post_date'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['new_is_deleted'] ) && $boolDirectSet ) $this->set( 'm_intNewIsDeleted', trim( $arrValues['new_is_deleted'] ) ); elseif( isset( $arrValues['new_is_deleted'] ) ) $this->setNewIsDeleted( $arrValues['new_is_deleted'] );
		if( isset( $arrValues['old_is_deleted'] ) && $boolDirectSet ) $this->set( 'm_intOldIsDeleted', trim( $arrValues['old_is_deleted'] ) ); elseif( isset( $arrValues['old_is_deleted'] ) ) $this->setOldIsDeleted( $arrValues['old_is_deleted'] );
		if( isset( $arrValues['is_post_month_ignored'] ) && $boolDirectSet ) $this->set( 'm_intIsPostMonthIgnored', trim( $arrValues['is_post_month_ignored'] ) ); elseif( isset( $arrValues['is_post_month_ignored'] ) ) $this->setIsPostMonthIgnored( $arrValues['is_post_month_ignored'] );
		if( isset( $arrValues['is_post_date_ignored'] ) && $boolDirectSet ) $this->set( 'm_intIsPostDateIgnored', trim( $arrValues['is_post_date_ignored'] ) ); elseif( isset( $arrValues['is_post_date_ignored'] ) ) $this->setIsPostDateIgnored( $arrValues['is_post_date_ignored'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedPsProductId', trim( $arrValues['updated_ps_product_id'] ) ); elseif( isset( $arrValues['updated_ps_product_id'] ) ) $this->setUpdatedPsProductId( $arrValues['updated_ps_product_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setMaintenanceRequestId( $intMaintenanceRequestId ) {
		$this->set( 'm_intMaintenanceRequestId', CStrings::strToIntDef( $intMaintenanceRequestId, NULL, false ) );
	}

	public function getMaintenanceRequestId() {
		return $this->m_intMaintenanceRequestId;
	}

	public function sqlMaintenanceRequestId() {
		return ( true == isset( $this->m_intMaintenanceRequestId ) ) ? ( string ) $this->m_intMaintenanceRequestId : 'NULL';
	}

	public function setMaintenanceRequestTypeId( $intMaintenanceRequestTypeId ) {
		$this->set( 'm_intMaintenanceRequestTypeId', CStrings::strToIntDef( $intMaintenanceRequestTypeId, NULL, false ) );
	}

	public function getMaintenanceRequestTypeId() {
		return $this->m_intMaintenanceRequestTypeId;
	}

	public function sqlMaintenanceRequestTypeId() {
		return ( true == isset( $this->m_intMaintenanceRequestTypeId ) ) ? ( string ) $this->m_intMaintenanceRequestTypeId : 'NULL';
	}

	public function setPriorMaintenanceRequestLogId( $intPriorMaintenanceRequestLogId ) {
		$this->set( 'm_intPriorMaintenanceRequestLogId', CStrings::strToIntDef( $intPriorMaintenanceRequestLogId, NULL, false ) );
	}

	public function getPriorMaintenanceRequestLogId() {
		return $this->m_intPriorMaintenanceRequestLogId;
	}

	public function sqlPriorMaintenanceRequestLogId() {
		return ( true == isset( $this->m_intPriorMaintenanceRequestLogId ) ) ? ( string ) $this->m_intPriorMaintenanceRequestLogId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->set( 'm_intUnitSpaceId', CStrings::strToIntDef( $intUnitSpaceId, NULL, false ) );
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function sqlUnitSpaceId() {
		return ( true == isset( $this->m_intUnitSpaceId ) ) ? ( string ) $this->m_intUnitSpaceId : 'NULL';
	}

	public function setPeriodId( $intPeriodId ) {
		$this->set( 'm_intPeriodId', CStrings::strToIntDef( $intPeriodId, NULL, false ) );
	}

	public function getPeriodId() {
		return $this->m_intPeriodId;
	}

	public function sqlPeriodId() {
		return ( true == isset( $this->m_intPeriodId ) ) ? ( string ) $this->m_intPeriodId : 'NULL';
	}

	public function setReportingPeriodId( $intReportingPeriodId ) {
		$this->set( 'm_intReportingPeriodId', CStrings::strToIntDef( $intReportingPeriodId, NULL, false ) );
	}

	public function getReportingPeriodId() {
		return $this->m_intReportingPeriodId;
	}

	public function sqlReportingPeriodId() {
		return ( true == isset( $this->m_intReportingPeriodId ) ) ? ( string ) $this->m_intReportingPeriodId : 'NULL';
	}

	public function setEffectivePeriodId( $intEffectivePeriodId ) {
		$this->set( 'm_intEffectivePeriodId', CStrings::strToIntDef( $intEffectivePeriodId, NULL, false ) );
	}

	public function getEffectivePeriodId() {
		return $this->m_intEffectivePeriodId;
	}

	public function sqlEffectivePeriodId() {
		return ( true == isset( $this->m_intEffectivePeriodId ) ) ? ( string ) $this->m_intEffectivePeriodId : 'NULL';
	}

	public function setOriginalPeriodId( $intOriginalPeriodId ) {
		$this->set( 'm_intOriginalPeriodId', CStrings::strToIntDef( $intOriginalPeriodId, NULL, false ) );
	}

	public function getOriginalPeriodId() {
		return $this->m_intOriginalPeriodId;
	}

	public function sqlOriginalPeriodId() {
		return ( true == isset( $this->m_intOriginalPeriodId ) ) ? ( string ) $this->m_intOriginalPeriodId : 'NULL';
	}

	public function setOldMaintenanceStatusId( $intOldMaintenanceStatusId ) {
		$this->set( 'm_intOldMaintenanceStatusId', CStrings::strToIntDef( $intOldMaintenanceStatusId, NULL, false ) );
	}

	public function getOldMaintenanceStatusId() {
		return $this->m_intOldMaintenanceStatusId;
	}

	public function sqlOldMaintenanceStatusId() {
		return ( true == isset( $this->m_intOldMaintenanceStatusId ) ) ? ( string ) $this->m_intOldMaintenanceStatusId : 'NULL';
	}

	public function setMaintenanceStatusId( $intMaintenanceStatusId ) {
		$this->set( 'm_intMaintenanceStatusId', CStrings::strToIntDef( $intMaintenanceStatusId, NULL, false ) );
	}

	public function getMaintenanceStatusId() {
		return $this->m_intMaintenanceStatusId;
	}

	public function sqlMaintenanceStatusId() {
		return ( true == isset( $this->m_intMaintenanceStatusId ) ) ? ( string ) $this->m_intMaintenanceStatusId : 'NULL';
	}

	public function setOldMaintenanceStatusTypeId( $intOldMaintenanceStatusTypeId ) {
		$this->set( 'm_intOldMaintenanceStatusTypeId', CStrings::strToIntDef( $intOldMaintenanceStatusTypeId, NULL, false ) );
	}

	public function getOldMaintenanceStatusTypeId() {
		return $this->m_intOldMaintenanceStatusTypeId;
	}

	public function sqlOldMaintenanceStatusTypeId() {
		return ( true == isset( $this->m_intOldMaintenanceStatusTypeId ) ) ? ( string ) $this->m_intOldMaintenanceStatusTypeId : 'NULL';
	}

	public function setMaintenanceStatusTypeId( $intMaintenanceStatusTypeId ) {
		$this->set( 'm_intMaintenanceStatusTypeId', CStrings::strToIntDef( $intMaintenanceStatusTypeId, NULL, false ) );
	}

	public function getMaintenanceStatusTypeId() {
		return $this->m_intMaintenanceStatusTypeId;
	}

	public function sqlMaintenanceStatusTypeId() {
		return ( true == isset( $this->m_intMaintenanceStatusTypeId ) ) ? ( string ) $this->m_intMaintenanceStatusTypeId : 'NULL';
	}

	public function setOldMaintenancePriorityId( $intOldMaintenancePriorityId ) {
		$this->set( 'm_intOldMaintenancePriorityId', CStrings::strToIntDef( $intOldMaintenancePriorityId, NULL, false ) );
	}

	public function getOldMaintenancePriorityId() {
		return $this->m_intOldMaintenancePriorityId;
	}

	public function sqlOldMaintenancePriorityId() {
		return ( true == isset( $this->m_intOldMaintenancePriorityId ) ) ? ( string ) $this->m_intOldMaintenancePriorityId : 'NULL';
	}

	public function setMaintenancePriorityId( $intMaintenancePriorityId ) {
		$this->set( 'm_intMaintenancePriorityId', CStrings::strToIntDef( $intMaintenancePriorityId, NULL, false ) );
	}

	public function getMaintenancePriorityId() {
		return $this->m_intMaintenancePriorityId;
	}

	public function sqlMaintenancePriorityId() {
		return ( true == isset( $this->m_intMaintenancePriorityId ) ) ? ( string ) $this->m_intMaintenancePriorityId : 'NULL';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NOW()';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NOW()';
	}

	public function setReportingPostMonth( $strReportingPostMonth ) {
		$this->set( 'm_strReportingPostMonth', CStrings::strTrimDef( $strReportingPostMonth, -1, NULL, true ) );
	}

	public function getReportingPostMonth() {
		return $this->m_strReportingPostMonth;
	}

	public function sqlReportingPostMonth() {
		return ( true == isset( $this->m_strReportingPostMonth ) ) ? '\'' . $this->m_strReportingPostMonth . '\'' : 'NOW()';
	}

	public function setApplyThroughPostMonth( $strApplyThroughPostMonth ) {
		$this->set( 'm_strApplyThroughPostMonth', CStrings::strTrimDef( $strApplyThroughPostMonth, -1, NULL, true ) );
	}

	public function getApplyThroughPostMonth() {
		return $this->m_strApplyThroughPostMonth;
	}

	public function sqlApplyThroughPostMonth() {
		return ( true == isset( $this->m_strApplyThroughPostMonth ) ) ? '\'' . $this->m_strApplyThroughPostMonth . '\'' : 'NOW()';
	}

	public function setReportingPostDate( $strReportingPostDate ) {
		$this->set( 'm_strReportingPostDate', CStrings::strTrimDef( $strReportingPostDate, -1, NULL, true ) );
	}

	public function getReportingPostDate() {
		return $this->m_strReportingPostDate;
	}

	public function sqlReportingPostDate() {
		return ( true == isset( $this->m_strReportingPostDate ) ) ? '\'' . $this->m_strReportingPostDate . '\'' : 'NOW()';
	}

	public function setApplyThroughPostDate( $strApplyThroughPostDate ) {
		$this->set( 'm_strApplyThroughPostDate', CStrings::strTrimDef( $strApplyThroughPostDate, -1, NULL, true ) );
	}

	public function getApplyThroughPostDate() {
		return $this->m_strApplyThroughPostDate;
	}

	public function sqlApplyThroughPostDate() {
		return ( true == isset( $this->m_strApplyThroughPostDate ) ) ? '\'' . $this->m_strApplyThroughPostDate . '\'' : 'NOW()';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NOW()';
	}

	public function setNewIsDeleted( $intNewIsDeleted ) {
		$this->set( 'm_intNewIsDeleted', CStrings::strToIntDef( $intNewIsDeleted, NULL, false ) );
	}

	public function getNewIsDeleted() {
		return $this->m_intNewIsDeleted;
	}

	public function sqlNewIsDeleted() {
		return ( true == isset( $this->m_intNewIsDeleted ) ) ? ( string ) $this->m_intNewIsDeleted : '0';
	}

	public function setOldIsDeleted( $intOldIsDeleted ) {
		$this->set( 'm_intOldIsDeleted', CStrings::strToIntDef( $intOldIsDeleted, NULL, false ) );
	}

	public function getOldIsDeleted() {
		return $this->m_intOldIsDeleted;
	}

	public function sqlOldIsDeleted() {
		return ( true == isset( $this->m_intOldIsDeleted ) ) ? ( string ) $this->m_intOldIsDeleted : '0';
	}

	public function setIsPostMonthIgnored( $intIsPostMonthIgnored ) {
		$this->set( 'm_intIsPostMonthIgnored', CStrings::strToIntDef( $intIsPostMonthIgnored, NULL, false ) );
	}

	public function getIsPostMonthIgnored() {
		return $this->m_intIsPostMonthIgnored;
	}

	public function sqlIsPostMonthIgnored() {
		return ( true == isset( $this->m_intIsPostMonthIgnored ) ) ? ( string ) $this->m_intIsPostMonthIgnored : '0';
	}

	public function setIsPostDateIgnored( $intIsPostDateIgnored ) {
		$this->set( 'm_intIsPostDateIgnored', CStrings::strToIntDef( $intIsPostDateIgnored, NULL, false ) );
	}

	public function getIsPostDateIgnored() {
		return $this->m_intIsPostDateIgnored;
	}

	public function sqlIsPostDateIgnored() {
		return ( true == isset( $this->m_intIsPostDateIgnored ) ) ? ( string ) $this->m_intIsPostDateIgnored : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedPsProductId( $intUpdatedPsProductId ) {
		$this->set( 'm_intUpdatedPsProductId', CStrings::strToIntDef( $intUpdatedPsProductId, NULL, false ) );
	}

	public function getUpdatedPsProductId() {
		return $this->m_intUpdatedPsProductId;
	}

	public function sqlUpdatedPsProductId() {
		return ( true == isset( $this->m_intUpdatedPsProductId ) ) ? ( string ) $this->m_intUpdatedPsProductId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, maintenance_request_id, maintenance_request_type_id, prior_maintenance_request_log_id, property_id, lease_id, property_unit_id, unit_space_id, period_id, reporting_period_id, effective_period_id, original_period_id, old_maintenance_status_id, maintenance_status_id, old_maintenance_status_type_id, maintenance_status_type_id, old_maintenance_priority_id, maintenance_priority_id, post_month, post_date, reporting_post_month, apply_through_post_month, reporting_post_date, apply_through_post_date, log_datetime, effective_date, new_is_deleted, old_is_deleted, is_post_month_ignored, is_post_date_ignored, updated_by, updated_on, created_by, created_on, updated_ps_product_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlMaintenanceRequestId() . ', ' .
						$this->sqlMaintenanceRequestTypeId() . ', ' .
						$this->sqlPriorMaintenanceRequestLogId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlPropertyUnitId() . ', ' .
						$this->sqlUnitSpaceId() . ', ' .
						$this->sqlPeriodId() . ', ' .
						$this->sqlReportingPeriodId() . ', ' .
						$this->sqlEffectivePeriodId() . ', ' .
						$this->sqlOriginalPeriodId() . ', ' .
						$this->sqlOldMaintenanceStatusId() . ', ' .
						$this->sqlMaintenanceStatusId() . ', ' .
						$this->sqlOldMaintenanceStatusTypeId() . ', ' .
						$this->sqlMaintenanceStatusTypeId() . ', ' .
						$this->sqlOldMaintenancePriorityId() . ', ' .
						$this->sqlMaintenancePriorityId() . ', ' .
						$this->sqlPostMonth() . ', ' .
						$this->sqlPostDate() . ', ' .
						$this->sqlReportingPostMonth() . ', ' .
						$this->sqlApplyThroughPostMonth() . ', ' .
						$this->sqlReportingPostDate() . ', ' .
						$this->sqlApplyThroughPostDate() . ', ' .
						$this->sqlLogDatetime() . ', ' .
						$this->sqlEffectiveDate() . ', ' .
						$this->sqlNewIsDeleted() . ', ' .
						$this->sqlOldIsDeleted() . ', ' .
						$this->sqlIsPostMonthIgnored() . ', ' .
						$this->sqlIsPostDateIgnored() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlUpdatedPsProductId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceRequestId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_request_type_id = ' . $this->sqlMaintenanceRequestTypeId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceRequestTypeId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_request_type_id = ' . $this->sqlMaintenanceRequestTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prior_maintenance_request_log_id = ' . $this->sqlPriorMaintenanceRequestLogId(). ',' ; } elseif( true == array_key_exists( 'PriorMaintenanceRequestLogId', $this->getChangedColumns() ) ) { $strSql .= ' prior_maintenance_request_log_id = ' . $this->sqlPriorMaintenanceRequestLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_id = ' . $this->sqlUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' period_id = ' . $this->sqlPeriodId(). ',' ; } elseif( true == array_key_exists( 'PeriodId', $this->getChangedColumns() ) ) { $strSql .= ' period_id = ' . $this->sqlPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_period_id = ' . $this->sqlReportingPeriodId(). ',' ; } elseif( true == array_key_exists( 'ReportingPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' reporting_period_id = ' . $this->sqlReportingPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_period_id = ' . $this->sqlEffectivePeriodId(). ',' ; } elseif( true == array_key_exists( 'EffectivePeriodId', $this->getChangedColumns() ) ) { $strSql .= ' effective_period_id = ' . $this->sqlEffectivePeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_period_id = ' . $this->sqlOriginalPeriodId(). ',' ; } elseif( true == array_key_exists( 'OriginalPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' original_period_id = ' . $this->sqlOriginalPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_maintenance_status_id = ' . $this->sqlOldMaintenanceStatusId(). ',' ; } elseif( true == array_key_exists( 'OldMaintenanceStatusId', $this->getChangedColumns() ) ) { $strSql .= ' old_maintenance_status_id = ' . $this->sqlOldMaintenanceStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_status_id = ' . $this->sqlMaintenanceStatusId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceStatusId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_status_id = ' . $this->sqlMaintenanceStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_maintenance_status_type_id = ' . $this->sqlOldMaintenanceStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'OldMaintenanceStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' old_maintenance_status_type_id = ' . $this->sqlOldMaintenanceStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_status_type_id = ' . $this->sqlMaintenanceStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_status_type_id = ' . $this->sqlMaintenanceStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_maintenance_priority_id = ' . $this->sqlOldMaintenancePriorityId(). ',' ; } elseif( true == array_key_exists( 'OldMaintenancePriorityId', $this->getChangedColumns() ) ) { $strSql .= ' old_maintenance_priority_id = ' . $this->sqlOldMaintenancePriorityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_priority_id = ' . $this->sqlMaintenancePriorityId(). ',' ; } elseif( true == array_key_exists( 'MaintenancePriorityId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_priority_id = ' . $this->sqlMaintenancePriorityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth(). ',' ; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate(). ',' ; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_post_month = ' . $this->sqlReportingPostMonth(). ',' ; } elseif( true == array_key_exists( 'ReportingPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' reporting_post_month = ' . $this->sqlReportingPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_month = ' . $this->sqlApplyThroughPostMonth(). ',' ; } elseif( true == array_key_exists( 'ApplyThroughPostMonth', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_month = ' . $this->sqlApplyThroughPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate(). ',' ; } elseif( true == array_key_exists( 'ReportingPostDate', $this->getChangedColumns() ) ) { $strSql .= ' reporting_post_date = ' . $this->sqlReportingPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate(). ',' ; } elseif( true == array_key_exists( 'ApplyThroughPostDate', $this->getChangedColumns() ) ) { $strSql .= ' apply_through_post_date = ' . $this->sqlApplyThroughPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime(). ',' ; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'EffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_is_deleted = ' . $this->sqlNewIsDeleted(). ',' ; } elseif( true == array_key_exists( 'NewIsDeleted', $this->getChangedColumns() ) ) { $strSql .= ' new_is_deleted = ' . $this->sqlNewIsDeleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_is_deleted = ' . $this->sqlOldIsDeleted(). ',' ; } elseif( true == array_key_exists( 'OldIsDeleted', $this->getChangedColumns() ) ) { $strSql .= ' old_is_deleted = ' . $this->sqlOldIsDeleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_month_ignored = ' . $this->sqlIsPostMonthIgnored(). ',' ; } elseif( true == array_key_exists( 'IsPostMonthIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_month_ignored = ' . $this->sqlIsPostMonthIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored(). ',' ; } elseif( true == array_key_exists( 'IsPostDateIgnored', $this->getChangedColumns() ) ) { $strSql .= ' is_post_date_ignored = ' . $this->sqlIsPostDateIgnored() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' updated_ps_product_id = ' . $this->sqlUpdatedPsProductId(). ',' ; } elseif( true == array_key_exists( 'UpdatedPsProductId', $this->getChangedColumns() ) ) { $strSql .= ' updated_ps_product_id = ' . $this->sqlUpdatedPsProductId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'maintenance_request_id' => $this->getMaintenanceRequestId(),
			'maintenance_request_type_id' => $this->getMaintenanceRequestTypeId(),
			'prior_maintenance_request_log_id' => $this->getPriorMaintenanceRequestLogId(),
			'property_id' => $this->getPropertyId(),
			'lease_id' => $this->getLeaseId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'unit_space_id' => $this->getUnitSpaceId(),
			'period_id' => $this->getPeriodId(),
			'reporting_period_id' => $this->getReportingPeriodId(),
			'effective_period_id' => $this->getEffectivePeriodId(),
			'original_period_id' => $this->getOriginalPeriodId(),
			'old_maintenance_status_id' => $this->getOldMaintenanceStatusId(),
			'maintenance_status_id' => $this->getMaintenanceStatusId(),
			'old_maintenance_status_type_id' => $this->getOldMaintenanceStatusTypeId(),
			'maintenance_status_type_id' => $this->getMaintenanceStatusTypeId(),
			'old_maintenance_priority_id' => $this->getOldMaintenancePriorityId(),
			'maintenance_priority_id' => $this->getMaintenancePriorityId(),
			'post_month' => $this->getPostMonth(),
			'post_date' => $this->getPostDate(),
			'reporting_post_month' => $this->getReportingPostMonth(),
			'apply_through_post_month' => $this->getApplyThroughPostMonth(),
			'reporting_post_date' => $this->getReportingPostDate(),
			'apply_through_post_date' => $this->getApplyThroughPostDate(),
			'log_datetime' => $this->getLogDatetime(),
			'effective_date' => $this->getEffectiveDate(),
			'new_is_deleted' => $this->getNewIsDeleted(),
			'old_is_deleted' => $this->getOldIsDeleted(),
			'is_post_month_ignored' => $this->getIsPostMonthIgnored(),
			'is_post_date_ignored' => $this->getIsPostDateIgnored(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'updated_ps_product_id' => $this->getUpdatedPsProductId()
		);
	}

}
?>